package com.maple.maple.util;

import java.util.Enumeration;
import java.util.Vector;


 class MClient{
	 String ipaddress;
	 String port;
 }

public class RegisteredClients {
	 private static  Vector listeners = new Vector();
	 public static void register(MClient mclient ) {
	        listeners.addElement(mclient);
	    }
	 
	 
	 public void sendMessage(String message) {
	        for (Enumeration e = listeners.elements(); e.hasMoreElements();) {
	        	
	        	
	        	MClient mc = (MClient) e.nextElement();
	        	
	        	
	        	String sourceIP =mc.ipaddress;
	        	String sourcePort = mc.port;
	        	
	        	if (null != sourceIP && null != sourcePort) {
	        		SocketClient socketConnection = new SocketClient();

	        		socketConnection.sendToSocket(sourceIP, sourcePort,
	        				message);
	        
	        	}
	        	
	        	
	        }
	    }
}
