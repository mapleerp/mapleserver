package com.maple.maple.util;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@ComponentScan
public class SocketClient {
	private static final Logger logger = LoggerFactory.getLogger(SocketClient.class);
	
	private Socket		 socket = null; 
	private ServerSocket server = null; 
	private DataInputStream in	 = null; 
	private int PORT ;

	
	
	
	    private static Socket s;
	    
	    private static InputStreamReader isr;
	    private static BufferedReader br;
	    private static PrintWriter pr;
	    
	    
	    public boolean sendToSocket(String SourceIP, String SourcePORT , String msg) {
	    	
			 int m_port = Integer.parseInt(SourcePORT);
            
           try {
				s= new Socket(SourceIP,m_port);
				
				
				  pr = new PrintWriter(s.getOutputStream());

                   pr.write(msg);
                   


                   pr.flush();
                   pr.close();
                   s.close();
                   
                   
                   
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
				return false;
			}
           return true;
	    }
	    
	    
}

/*
String sourceIP = salestranshdr.getSourceIP();
String sourcePort = salestranshdr.getSourcePort();
if (null != sourceIP && null != sourcePort) {
	SocketConnection socketConnection = new SocketConnection();

	socketConnection.sendToSocket(sourceIP, sourcePort,
			salestranshdr.getId() + ":" + "POS_SAVED" + ":" + salestranshdr.getServingTableName());
	// socketConnection.sendToSocket(sourceIP, sourcePort, "OVER");
}
*/