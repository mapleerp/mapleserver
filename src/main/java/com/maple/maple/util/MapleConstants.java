package com.maple.maple.util;

public class MapleConstants {
	public static final String Store = "MAIN";
	public static final String Nobatch = "NOBATCH";
	public static final String AcceptStockStatusPending = "PENDING";
	public static final String StockTransferPrefix="ST";
	
	public static final String FOR_A_SPECIFIC_QTY_OF_CATEGORY="2";
	public static final String FOR_A_SPECIFIC_QTY_OF_ITEM_NAME ="1";
	public static final String FOR_TOTAL_INVOICE_AMOUNT ="7";
	public static final String FREE_QUANTITY ="2";
	public static final String AMOUNT_DISCOUNT ="1";
	public static final String PERCENTAGE_DISCOUNT ="4";
	public static final String FAILED="FAILED";
	
	public static final String SUPPLIER="SUPPLIER";
	public static final String CUSTOMER="CUSTOMER";
	public static final String LIABILITY="LIABLITY";
	public static final String ASSET="ASSETS";
	public static final String SUNDRYCREDITORS="SUNDRY CREDITORS";
	public static final String SUNDRYDEBTORS="SUNDRY DEBTORS";
	public static final String NO="NO";
	public static final String  STOCKOK = "STOCKOK";
	public static final String  NOTINSTOCK = "NOT IN STOCK";
	
	public static final String  STOCKTRANSFERINPARTICULARS = "STOCK TRANSFER IN";
	public static final String  STOCKTRANSFEROUTPARTICULARS = "STOCK TRANSFER TO";
	public static final String  SALESPARTICULARS = "SALES";
	public static final String  PHYSICALSTOCKPARTICULAR = "PHYSICAL STOCK ENTRY";
	
	
	public static final String SALESTYPE = "SALES";
	public static final String PURCHASETYPE = "PURCHASE";
	public static final String ITEMBATCHDTLTYPE = "ITEMBATCHDTL";
	
	public static final String TOSERVER = "SERVER";
	public static final String  msgfilextention = ".dat";
	
	public static final String SUCCESS ="SUCCESS";
	
	public static final String OpeningStock ="OPENINGSTOCK";
}
