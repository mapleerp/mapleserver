package com.maple.maple.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.maple.restserver.entity.TaxTypeMst;


/*
 * By Regy George
 * MapleERP , 2020, Jan 2
 * 
 * Utility class to manage tax.
 * 
 */

public class TaxManager {
	private static final Logger logger = LoggerFactory.getLogger(TaxManager.class);

	public BigDecimal getTax(String taxId, HashMap hm) {
		BigDecimal bd = new BigDecimal("0");
		
		
		String  taxClass = getTaxClassById(taxId);
		
		try {
			TaxCalculater gstTax = createInstanceOfClass(taxClass);
			
			
			
			  bd = gstTax.execute(hm);
			System.out.println(bd.toPlainString());
			
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		return bd;
		
	}
	public static TaxCalculater createInstanceOfClass(String className) throws ClassNotFoundException, InstantiationException, IllegalAccessException{


        Class classTemp = Class.forName(className);

        TaxCalculater obj =(TaxCalculater) classTemp.newInstance();
		return obj;



    }




 

public static String getTaxClassById(String id) {
	ArrayList  al = getTaxCalcualtorIds();

	String TaxClass = "";
	
	Iterator iter = al.iterator();
	while(iter.hasNext()) {
		HashMap hm = (HashMap) iter.next();
		
		if(id.equalsIgnoreCase((String) hm.get("ID"))) {
			TaxClass = (String) hm.get("CLASS");
			break;
		}
		
		
		 
	}
	
	return TaxClass;
	
}


public static String getTaxClassByName(String id) {
	ArrayList  al = getTaxCalcualtorIds();

	String TaxClass = "";
	
	Iterator iter = al.iterator();
	while(iter.hasNext()) {
		HashMap hm = (HashMap) iter.next();
		
		if(id.equalsIgnoreCase((String) hm.get("NAME"))) {
			TaxClass = (String) hm.get("CLASS");
			break;
		}
		
		
		 
	}
	
	return TaxClass;
	
}
/*
 * when a new tax calculation method is introduced, please update the below Hash Map.
 */
public static ArrayList getTaxCalcualtorIds() {
	HashMap  hm = new HashMap();
	
	hm.put("ID", "ASV");
	hm.put("CLASS", "com.maple.maple.util.GSTCalculator");
	hm.put("NAME", "ON ASSESSABLE VALUE");
	
	
	ArrayList al = new ArrayList();
	al.add(hm);
	
	 

	return al;
	
}



/*
 * Get Predefined TaxIDs
 */
public static ArrayList getTaxIds() {
	
	TaxTypeMst sgst = new TaxTypeMst();
	TaxTypeMst cgst = new TaxTypeMst();
	TaxTypeMst igst = new TaxTypeMst();
	TaxTypeMst kfs = new TaxTypeMst();
	
	
	sgst.setCalculationId("ASV");
	sgst.setTaxId("SGST");
	sgst.setTaxName("SGST");
	
	
	cgst.setCalculationId("ASV");
	cgst.setTaxId("CGST");
	cgst.setTaxName("CGST");
	
	
	
	igst.setCalculationId("ASV");
	igst.setTaxId("IGST");
	igst.setTaxName("IGST");
	
	
	kfs.setCalculationId("ASV");
	kfs.setTaxId("KFC");
	kfs.setTaxName("KERALA FLOOD CESS");
	
	
	
	ArrayList  al = new ArrayList();
 
	al.add(sgst);
	al.add(cgst);
	al.add(igst);
	al.add(kfs);
	 

	return al;
	
}

}
