package com.maple.maple.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.naming.java.javaURLContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.FinancialYearMst;
import com.maple.restserver.repository.FinancialYearMstRepository;

@Component

public class ClientSystemSetting {
	private static final Logger logger = LoggerFactory.getLogger(ClientSystemSetting.class);

	@Value("${tallyServer}")
	public static String TallyServer;

	// @Autowired
	// private static Environment env;
	// public static UserMst user; ;

	private static FinancialYearMst financialYear;

	private static String posFormat;
	private static String logo_name;

	private static String reportpath;

	private static String cashDrawerPresent;

	// private static final Logger logger =
	// Logger.getLogger(MapleclientApplication.class);

	public static ArrayList<String> user_roles = new ArrayList();
	public static Date systemDate;
	public static String systemBranch;
	public static String userId;
	public static String reportPath;
	public static boolean debugUser = false; // if debugUser then all menu permission is enabled

	public static String version = "2.0";

	public static int logox;
	public static int logoy;
	public static int logow;
	public static int logoh;

	public static String posinvoicetitle1;
	public static String posinvoicetitle2;
	public static String posinvoicetitle3;
	public static String posinvoicetitle4;
	public static String posinvoicetitle5;

	public static int title1x;
	public static int title1y;
	public static int title2x;
	public static int title2y;
	public static int title3x;
	public static int title3y;
	public static int title4x;
	public static int title4y;
	public static int title5x;
	public static int title5y;

	public static String posInvoiceBottomLine;

	@Autowired
	static FinancialYearMstRepository financialYearMstRepository;

	public static String ITEM_CODE_GENERATOR_ID = "ICD";

	public ClientSystemSetting() {
		systemDate = new java.util.Date();
	}

	public static Date getSystemDate() {
		systemDate = new java.util.Date();
		return systemDate;
	}

	public static java.util.Date localToUtilDate(LocalDate lDate) {

		Date date = Date.from(lDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		return date;
	}

	public static LocalDate utilToLocaDate(java.util.Date uDate) {

		LocalDate date = uDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		return date;
	}

	/*
	 * public Pair<Date, Date> getDateRange() { Date begining, end;
	 * 
	 * { Calendar calendar = getCalendarForNow();
	 * calendar.set(Calendar.DAY_OF_MONTH,
	 * calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
	 * setTimeToBeginningOfDay(calendar); begining = calendar.getTime(); }
	 * 
	 * { Calendar calendar = getCalendarForNow();
	 * calendar.set(Calendar.DAY_OF_MONTH,
	 * calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	 * setTimeToEndofDay(calendar); end = calendar.getTime(); }
	 * 
	 * return Pair.of(begining, end); }
	 */

	private static Calendar getCalendarForNow() {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(new Date());
		return calendar;
	}

	private static void setTimeToBeginningOfDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}

	private static void setTimeToEndofDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
	}

	public String getCurrentLocalDateTimeStampAsString() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
	}

	public Timestamp getCurrentDatTime() {

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		Timestamp time = new Timestamp(cal.getTimeInMillis());

		return time;

	}

	public Date addDaysToDate(Date date, int days) {

		// First convert to local Date
		LocalDate lDate = utilToLocaDate(date);
		lDate = lDate.plusDays(days);

		// Now convert back to util date and return
		return localToUtilDate(lDate);

	}

	public boolean isFutureDate(Date fDate) {

		boolean isfuture = false;

		Date current = new Date();

		// compare both dates
		if (fDate.after(current)) {
			isfuture = true;
		} else {
			isfuture = false;
		}
		return isfuture;
	}

	public long DateLeft(Date fDate) {

		Date current = new Date();
		long diff = fDate.getTime() - current.getTime();

		long diffSeconds = diff / 1000;

		long diffMinutes = diffSeconds / 60;

		long diffHr = diffMinutes / 60;
		long diffDays = diffHr / 24;

		return diffDays;
	}

	public static boolean pingServer(String IP) {
		boolean pinged = false;
		try {
			Process p = Runtime.getRuntime().exec("ping " + IP);
			BufferedReader inputStream = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String s = "";
			// reading output stream of the command
			while ((s = inputStream.readLine()) != null) {
				System.out.println(s);
				pinged = true;
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			return pinged;
		}
		return true;
	}

	public static boolean hostAvailabilityCheck(String SERVER_ADDRESS, String url, int TCP_SERVER_PORT) {

		Socket s = null;

		try {

			s = new Socket(SERVER_ADDRESS, TCP_SERVER_PORT);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			// logger.debug("Check and fouind server down");
		}
		boolean available = true;
		try {
			if (s.isConnected()) {
				s.close();
			}
		} catch (UnknownHostException e) { // unknown host
			available = false;
			
			try {
				s.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			s = null;
		} catch (IOException e) { // io exception, service probably not running
			available = false;
			
			try {
				s.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			s = null;
		} catch (NullPointerException e) {
			available = false;
			
			try {
				s.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			s = null;
		}
		
		try {
			s.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return available;
	}

	public boolean isValidEmailAddress(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}

	public static String padLeft(String s, int n) {
		// return String.format("%1$" + n + "s", s);
		return String.format("%1$" + n + "s", s);
	}

	public static double round(double value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	public static double round(java.math.BigDecimal value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		value = value.setScale(places, BigDecimal.ROUND_HALF_UP);
		return value.doubleValue();
	}

	public static boolean IsValidNumber(String inString) {
		Pattern p = Pattern.compile("^[0-9]\\d*(\\.\\d+)?$");
		Matcher m = p.matcher(inString);
		boolean b = m.matches();
		return b;
	}

	public static java.util.Date StringToUtilDate(String strDate, String Format) {

		// String ReplaceStringDate = strDate.replace("-", "/");
		java.util.Date dt = StringToutilDateSlash(strDate, Format);
		return dt;
	}

	public static java.sql.Date StringToSqlDate(String strDate, String Format) {

		String ReplaceStringDate = strDate.replace("-", "/");
		String ReplaceFormat = Format.replace("-", "/");

		java.sql.Date dt = StringToSqlDateSlash(ReplaceStringDate, ReplaceFormat);

		return dt;

//	SimpleDateFormat sdf1= new SimpleDateFormat(Format);
//	Date date = null;
//	try {
//		date = sdf1.parse(strDate);
//	} catch (ParseException e) {
//		// TODO Auto-generated catch block
//		logger.error(e.getMessage());
//	}
//	java.sql.Date sqlStrDate = new java.sql.Date(date.getTime());
//	
//	return sqlStrDate;
//	

	}

	private static java.util.Date StringToutilDateSlash(String strDate, String Format) {
		SimpleDateFormat formatter = new SimpleDateFormat(Format); // "dd/MM/yyyy"
		// String dateInString = "7-Jun-2013";
		Date date = null;
		try {

			date = formatter.parse(strDate);
			System.out.println(date);
			System.out.println(formatter.format(date));

		} catch (ParseException e) {
			// logger.debug(e.toString());
		}
		return date;
	}

	private static java.sql.Date StringToSqlDateSlash(String strDate, String Format) {
		SimpleDateFormat formatter = new SimpleDateFormat(Format);// "dd/MM/yyyy"

		Date dt = StringToutilDateSlash(strDate, Format);
		java.sql.Date sqlDate = new java.sql.Date(dt.getTime());

		return sqlDate;
	}

	public String StringToStringFormatChange(String strDate, String inFormat, String outFormat) {

		String ddmmyyDate = "";
		SimpleDateFormat formatter = new SimpleDateFormat(inFormat);// "dd/MM/yyyy"

		Date dt = StringToutilDateSlash(strDate, inFormat);
		java.sql.Date sqlDate = new java.sql.Date(dt.getTime());

		if (outFormat.equals("dd/MM/yyyy")) {
			ddmmyyDate = SqlDateTostring(sqlDate);
		} else if (outFormat.equals("yyyy-MM-dd")) {
			ddmmyyDate = SqlDateTostringYYMMDD(sqlDate);
		} else {
			ddmmyyDate = "OutFormat not supported";
		}

		return ddmmyyDate;
	}

	public static String SqlDateTostring(Date date) {

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String text = df.format(date);

		return text;
	}

	public java.util.Date SqlDateToUtilDate(java.sql.Date date) {

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String text = df.format(date);

		java.util.Date uDate = StringToutilDateSlash(text, "dd/MM/yyyy");
		return uDate;
	}

	public static String SqlDateTostringYYMMDD(Date date) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String text = formatter.format(date);

		return text;
	}

	public static String UtilDateToString(java.util.Date dt) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String reportDate = df.format(dt);
		// logger.debug("Util To String input ="+dt);
		// logger.debug("Util To String output ="+reportDate);
		return reportDate;
	}

	public static String ddMMMyyUtilDateToString(java.util.Date dt) {
		DateFormat df = new SimpleDateFormat("dd-MMM-yy");
		String reportDate = df.format(dt);
		// logger.debug("Util To String input ="+dt);
		// logger.debug("Util To String output ="+reportDate);
		return reportDate;
	}

	public static String UtilDateToString(java.util.Date dt, String foamat) {
		DateFormat df = new SimpleDateFormat(foamat);
		String reportDate = df.format(dt);
		// logger.debug("Util To String input ="+dt);
		// logger.debug("Util To String output ="+reportDate);
		return reportDate;
	}

	public static java.sql.Date UtilDateToSQLDate(java.util.Date dt) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String reportDate = df.format(dt);

		java.sql.Date sqlDate = StringToSqlDateSlash(reportDate, "dd/MM/yyyy");

		return sqlDate;
	}

	public boolean isValidDate(String dateString) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date Systemdate = new Date();
		int Sysyear = Calendar.getInstance().get(Calendar.YEAR);

		dateString = dateString.replace("/", "");
		dateString = dateString.replace("-", "");
		if (dateString.length() == 7) {
			dateString = "0".concat(dateString);
		}

		if (dateString == null || (dateString.length() != "yyyyMMdd".length())) {
			return false;
		}
		// ddMMyyyy

		String strDD = dateString.substring(0, 2);
		String strMon = dateString.substring(2, 4);
		String strYy = dateString.substring(4, 8);
		dateString = strYy + strMon + strDD;

		int date;
		try {
			date = Integer.parseInt(dateString);
		} catch (NumberFormatException e) {
			return false;
		}

		int year = date / 10000;
		int month = (date % 10000) / 100;
		int day = date % 100;

		// leap years calculation not valid before 1581
		boolean yearOk = (year >= Sysyear - 1) && (year <= Sysyear + 1);
		boolean monthOk = (month >= 1) && (month <= 12);
		boolean dayOk = (day >= 1) && (day <= daysInMonth(year, month));

		return (yearOk && monthOk && dayOk);
	}

	private int daysInMonth(int year, int month) {
		int daysInMonth;
		switch (month) {
		case 1: // fall through
		case 3: // fall through
		case 5: // fall through
		case 7: // fall through
		case 8: // fall through
		case 10: // fall through
		case 12:
			daysInMonth = 31;
			break;
		case 2:
			if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
				daysInMonth = 29;
			} else {
				daysInMonth = 28;
			}
			break;
		default:
			// returns 30 even for nonexistant months
			daysInMonth = 30;
		}
		return daysInMonth;
	}

	public String AmountInWords(String StrAmt, String RupeName, String PaiseName) {

		String AmtWord = "";
		ToWordsCrore TwC = new ToWordsCrore();

		System.out.println("StrAmt = " + StrAmt);

		int pos = StrAmt.indexOf(".");
		String StrIPart = "";
		String StrDPart = "";
		if (pos > 0) {
			StrIPart = StrAmt.substring(0, pos);
			StrDPart = StrAmt.substring(pos + 1);

		} else {
			StrIPart = StrAmt;
		}

		System.out.println("Int Part =" + StrIPart);
		System.out.println("Deci Part =" + StrDPart);
		if (StrDPart.length() > 2) {
			StrDPart = StrDPart.substring(0, 2);
			System.out.println("Modi Deci Part =" + StrDPart);
		}

		if (StrDPart.length() == 0) {
			StrDPart = "0";
		}

		long iPart = Long.parseLong(StrIPart);

		long fPart = Long.parseLong(StrDPart);

		AmtWord = TwC.convertNumberToWords(iPart);
		System.out.println("in words ipart  =" + AmtWord);

		String Paise;
		if (fPart > 0) {
			String strfp = fPart + "";
			if (StrDPart.length() == 1) {
				fPart = fPart * 10;

			}
			Paise = TwC.convertNumberToWords(fPart);
			AmtWord = AmtWord + " " + RupeName + " And " + Paise + " " + PaiseName;
			System.out.println("in words paise  =" + Paise);

		} else {
			AmtWord = AmtWord + " " + RupeName;
		}
		// System.out.println("in words paise =" + AmtWord);

		return AmtWord;
	}

	public int randomNumberGeneration(int i) {
		Random rand = new Random();
		int n = rand.nextInt(i);
		n += 1;
		return n;
	}

	public static String getSystemBranch() {
		return systemBranch;
	}

	public static void setSystemBranch(String systemBranch) {
		ClientSystemSetting.systemBranch = systemBranch;
	}

	public static String getUserId() {
		return userId;
	}

	public static void setUserId(String userId) {
		ClientSystemSetting.userId = userId;
	}

	public static boolean deleteFile(String filePath) {
		// initialize File object
		File file = new File(filePath);

		boolean result = false;
		try {
			// delete the file specified
			result = file.delete();
			// test if successfully deleted the file
			if (result) {
				System.out.println("Successfully deleted: " + file.getCanonicalPath());
			} else {
				System.out.println("Failed deleting " + file.getCanonicalPath());
			}
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return result;
	}

	public static boolean UserHasRole(String RoleName) {

		for (Iterator itr = user_roles.iterator(); itr.hasNext();) {
			String aRole = (String) itr.next();

			if (RoleName.equalsIgnoreCase(aRole)) {

				return true;
			}
		}

		if (ClientSystemSetting.debugUser) {
			return true;
		}

		return false;

	}

	public static ArrayList<String> getUser_roles(String userId) {
		/*
		 * Read User Roles from Rest Call here
		 */
		if (null == userId || userId.length() == 0) {
			return null;
		}
//		ArrayList user_roles = new ArrayList();
		// RestTemplate restTemplate1 = new RestTemplate();
		// user_roles = RestCaller.getUserRoleByUserId(userId);

		return user_roles;

	}

	public static String getPosFormat() {

		return posFormat;
	}

	public static boolean hasCashDrawer() {

		if (cashDrawerPresent.equalsIgnoreCase("YES") || cashDrawerPresent.equalsIgnoreCase("Y")) {
			return true;
		} else {
			return false;
		}

	}

	public static void setTallyServer(String tallyServer) {
		ClientSystemSetting.TallyServer = tallyServer;
	}

	public static String getCashDrawerPresent() {
		return cashDrawerPresent;
	}

	public static void setCashDrawerPresent(String cashDrawerPresent) {
		ClientSystemSetting.cashDrawerPresent = cashDrawerPresent;
	}

	public static void setPosFormat(String posFormat) {
		ClientSystemSetting.posFormat = posFormat;
	}

	public static String getLogo_name() {
		return logo_name;
	}

	public static void setLogo_name(String logo_name) {
		ClientSystemSetting.logo_name = logo_name;
	}

	public static String getReportpath() {
		return reportpath;
	}

	public static void setReportpath(String reportpath) {
		ClientSystemSetting.reportpath = reportpath;
	}

	public static int getLogox() {
		return logox;
	}

	public static void setLogox(int logox) {
		ClientSystemSetting.logox = logox;
	}

	public static int getLogoy() {
		return logoy;
	}

	public static void setLogoy(int logoy) {
		ClientSystemSetting.logoy = logoy;
	}

	public static int getLogow() {
		return logow;
	}

	public static void setLogow(int logow) {
		ClientSystemSetting.logow = logow;
	}

	public static int getLogoh() {
		return logoh;
	}

	public static void setLogoh(int logoh) {
		ClientSystemSetting.logoh = logoh;
	}

	public static String getPosinvoicetitle1() {
		return posinvoicetitle1;
	}

	public static void setPosinvoicetitle1(String posinvoicetitle1) {
		ClientSystemSetting.posinvoicetitle1 = posinvoicetitle1;
	}

	public static String getPosinvoicetitle2() {
		return posinvoicetitle2;
	}

	public static void setPosinvoicetitle2(String posinvoicetitle2) {
		ClientSystemSetting.posinvoicetitle2 = posinvoicetitle2;
	}

	public static String getPosinvoicetitle3() {
		return posinvoicetitle3;
	}

	public static void setPosinvoicetitle3(String posinvoicetitle3) {
		ClientSystemSetting.posinvoicetitle3 = posinvoicetitle3;
	}

	public static String getPosinvoicetitle4() {
		return posinvoicetitle4;
	}

	public static void setPosinvoicetitle4(String posinvoicetitle4) {
		ClientSystemSetting.posinvoicetitle4 = posinvoicetitle4;
	}

	public static String getPosinvoicetitle5() {
		return posinvoicetitle5;
	}

	public static void setPosinvoicetitle5(String posinvoicetitle5) {
		ClientSystemSetting.posinvoicetitle5 = posinvoicetitle5;
	}

	public static int getTitle1x() {
		return title1x;
	}

	public static void setTitle1x(int title1x) {
		ClientSystemSetting.title1x = title1x;
	}

	public static int getTitle1y() {
		return title1y;
	}

	public static void setTitle1y(int title1y) {
		ClientSystemSetting.title1y = title1y;
	}

	public static int getTitle2x() {
		return title2x;
	}

	public static void setTitle2x(int title2x) {
		ClientSystemSetting.title2x = title2x;
	}

	public static int getTitle2y() {
		return title2y;
	}

	public static void setTitle2y(int title2y) {
		ClientSystemSetting.title2y = title2y;
	}

	public static int getTitle3x() {
		return title3x;
	}

	public static void setTitle3x(int title3x) {
		ClientSystemSetting.title3x = title3x;
	}

	public static int getTitle3y() {
		return title3y;
	}

	public static void setTitle3y(int title3y) {
		ClientSystemSetting.title3y = title3y;
	}

	public static int getTitle4x() {
		return title4x;
	}

	public static void setTitle4x(int title4x) {
		ClientSystemSetting.title4x = title4x;
	}

	public static int getTitle4y() {
		return title4y;
	}

	public static void setTitle4y(int title4y) {
		ClientSystemSetting.title4y = title4y;
	}

	public static int getTitle5x() {
		return title5x;
	}

	public static void setTitle5x(int title5x) {
		ClientSystemSetting.title5x = title5x;
	}

	public static int getTitle5y() {
		return title5y;
	}

	public static void setTitle5y(int title5y) {
		ClientSystemSetting.title5y = title5y;
	}

	public static ArrayList<String> getUser_roles() {
		return user_roles;
	}

	public static void setUser_roles(ArrayList<String> user_roles) {
		ClientSystemSetting.user_roles = user_roles;
	}

	public static String getReportPath() {
		return reportPath;
	}

	public static void setReportPath(String reportPath) {
		ClientSystemSetting.reportPath = reportPath;
	}

	public static boolean isDebugUser() {
		return debugUser;
	}

	public static void setDebugUser(boolean debugUser) {
		ClientSystemSetting.debugUser = debugUser;
	}

	public static String getPosInvoiceBottomLine() {
		return posInvoiceBottomLine;
	}

	public static void setPosInvoiceBottomLine(String posInvoiceBottomLine) {
		ClientSystemSetting.posInvoiceBottomLine = posInvoiceBottomLine;
	}

	public static String getITEM_CODE_GENERATOR_ID() {
		return ITEM_CODE_GENERATOR_ID;
	}

	public static void setITEM_CODE_GENERATOR_ID(String iTEM_CODE_GENERATOR_ID) {
		ITEM_CODE_GENERATOR_ID = iTEM_CODE_GENERATOR_ID;
	}

	public static String getTallyServer() {
		return TallyServer;
	}

	public static void setSystemDate(Date systemDate) {
		ClientSystemSetting.systemDate = systemDate;
	}

	public static String getFinancialYear() {
		if (null != financialYear) {
			return financialYear.getFinancialYear();
		} else {
			return null;
		}

	}

	public static String setFinancialYear(FinancialYearMst financialYearP) {
		// FinancialYearMst financialYear =
		// financialYearMstRepository.getCurrentFinancialYear();
		// FinancialYearMst financialYear =
		// financialYearMstRepository.getCurrentFinancialYear();
		financialYear = financialYearP;

		return financialYear.getFinancialYear();
	}

}
