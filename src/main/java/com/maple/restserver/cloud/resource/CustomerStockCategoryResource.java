package com.maple.restserver.cloud.resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.CustomerStockCategory;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.report.entity.CustomerCategorywiseStockReport;
import com.maple.restserver.report.entity.CustomerSotckCategoryReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CustomerStockCategoryRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.ItemStockPopUpRepository;
import com.maple.restserver.service.CustomerStockCategoryService;

@RestController
public class CustomerStockCategoryResource {

	@Autowired
	CustomerStockCategoryRepository customerStockCategoryRepository;
	
	@Autowired
	private ItemStockPopUpRepository  itemStockPopUpRepository;
	
	@Autowired
	private ItemMstRepository itemMstRepository;
	
	
//	@Autowired
//	CustomerMstRepository customerMstRepository;
	
	@Autowired
	CategoryMstRepository categoryMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	
	@Autowired
	CustomerStockCategoryService customerStockCategoryService;
	
	
	@GetMapping("{companymstid}/customerstockcategory/{categoryname}/{customername}")
	public CustomerStockCategory createCustomerStockCategory(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "categoryname") String
			  categoryname,@PathVariable(value = "customername") String
			  customername
			  ){
		
		categoryname = categoryname.replaceAll("%20", " ");
		customername = customername.replaceAll("%20", " ");

		CustomerStockCategory  customerStockCategory=new CustomerStockCategory();
		
		Optional<CompanyMst> companyMst= companyMstRepository.findById(companymstid);
		customerStockCategory.setCompanyMst(companyMst.get());
		//Optional<CustomerMst> CustomerMst= customerMstRepository.findByCustomerNameAndCompanyMstId(customername,companymstid);
		AccountHeads accountHeads= accountHeadsRepository.findByAccountNameAndCompanyMstId(customername,companymstid);
		
		customerStockCategory.setAccountHeads(accountHeads);
		CategoryMst categoryMst= categoryMstRepository.findByCompanyMstIdAndCategoryName(companymstid,categoryname);
		customerStockCategory.setCategoryMst(categoryMst);
		customerStockCategory.setStatus("ACTIVE");
		return  customerStockCategoryRepository.saveAndFlush(customerStockCategory);
		
			
	}
	
	@GetMapping("{companymstid}/customerstockcategorys")
	public List<CustomerSotckCategoryReport> retrieveAllCustomerStockCategory(@PathVariable(value = "companymstid") String
			  companymstid){
		return customerStockCategoryService.fetchCustomerNameAndCategoryName(companymstid);
	}
	

	
	@GetMapping("{companymstid}/fetchallcustomerstockcategory")
	public List<CustomerStockCategory> fetchAllCustomerStockCategory(@PathVariable(value = "companymstid") String
			  companymstid){
		
		Optional<CompanyMst> companyMst= companyMstRepository.findById(companymstid);
		
		return customerStockCategoryRepository.findByCompanyMst(companyMst.get());
	}
	
	@GetMapping("{companymstid}/customerstockcategory/fetchcategorywisestockreport/{customerphoneno}")
	public List<CustomerCategorywiseStockReport> fetchCustomerCategorywiseStockReport(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "customerphoneno") String
			  customerphoneno){
		
		
		
		
		return customerStockCategoryService.fetchCustomerCategorywiseStockReport(companymstid,customerphoneno);
	}
	
	
	@GetMapping("{companymstid}/customerstockcategory/fetchcategorywiseitemreport/{customerphoneno}")
	public  @ResponseBody List<Object> fetchCustomerCategorywiseItems(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "customerphoneno") String
			  customerphoneno, @RequestParam("data") String searchstring){
		
		Optional<CompanyMst> companyMst= companyMstRepository.findById(companymstid);
		//CustomerMst customerMst = customerMstRepository.findByCustomerContact(customerphoneno);
		AccountHeads accountHeads=accountHeadsRepository.findByCustomerContact(customerphoneno);
		
		searchstring = searchstring.replaceAll("20%", " ");
		return itemStockPopUpRepository.findSearchItemsCustomerStockCategory(companymstid,"%"+searchstring.toLowerCase()+"%",accountHeads.getId()) ;
		
	}
	
	@GetMapping("{companymstid}/customerstockcategory/fetchcategorywisestockreportbyitem/{customerphoneno}/{itemname}")
	public List<CustomerCategorywiseStockReport> fetchCustomerCategorywiseStockReportByItem(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "customerphoneno") String
			  customerphoneno, @PathVariable(value = "itemname") String
			  itemname){
		
		itemname = itemname.replaceAll("%20", " ");
		ItemMst itemMst = itemMstRepository.findByItemName(itemname);
		
		
		return customerStockCategoryService.fetchCustomerCategorywiseStockReportByItem(companymstid,customerphoneno,itemMst.getId());
	}
	
	

	
	
}
