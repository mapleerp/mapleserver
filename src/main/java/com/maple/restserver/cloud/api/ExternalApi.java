package com.maple.restserver.cloud.api;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

 
import javax.validation.Valid;

import org.hibernate.query.criteria.internal.expression.function.SubstringFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.net.MediaType;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CommonReceiveMsgEntity;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.GoodReceiveNoteHdr;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchExpiryDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.LastBestDate;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.LmsQueueTallyMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.OtherBranchSalesDtl;
import com.maple.restserver.entity.OtherBranchSalesMessageEntity;
import com.maple.restserver.entity.OtherBranchSalesTransHdr;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.PurchaseHdrMessageEntity;
import com.maple.restserver.entity.PurchasePriceDefinitionMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesMessageEntity;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.ServerFailureStatusMst;
import com.maple.restserver.entity.StockTransferInDtl;
import com.maple.restserver.entity.StockTransferInHdr;
import com.maple.restserver.entity.StockTransferMessageEntity;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.message.entity.AccountMessage;
import com.maple.restserver.message.entity.PurchaseMessageEntity;
import com.maple.restserver.message.entity.SummaryOfDailyStock;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.AccountReceivableRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchExpiryDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.LmsQueueTallyMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.ParamValueConfigRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.PurchasePriceDefinitionMstRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.ServerFailureStatusRepository;
import com.maple.restserver.repository.StockTransferInDtlRepository;
import com.maple.restserver.repository.StockTransferInHdrRepository;
import com.maple.restserver.repository.StockTransferOutDtlRepository;
import com.maple.restserver.repository.StockTransferOutHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ItemBatchDtlService;

import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.service.accounting.task.SalesAccounting;
import com.maple.restserver.service.task.SalesDayEndReporting;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Component
public class ExternalApi {


	@Value("${camunda.host}")
	private String CLOUDURL;

	@Value("${maple.port}")
	private String maple;

	@Value("${mycompany}")
	private String mycompany;

	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	PurchasePriceDefinitionMstRepository purchasePriceDefinitionRepository;

	@Autowired
	ServerFailureStatusRepository serverFailureStatusRepository;
	
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;
	
	@Autowired
	ItemMstRepository itemMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;

	// ----------version 4.19 Surya end

//	@Autowired
	// RestTemplate restTemplate ;

	RestTemplate restTemplate = new RestTemplate();

	public ResponseEntity<StockTransferMessageEntity> sendstockTransferToCloud(
			StockTransferMessageEntity stockoutDtlList) {

//		String path = CLOUDURL + "stocktransferouthdr/acceptstock";

//	     final HttpEntity<StockTransferMessageEntity> entity = new HttpEntity<StockTransferMessageEntity> (stockoutDtlList);

		// Execute the method writing your HttpEntity to the request
		// ResponseEntity<Map> response =
		// restTemplate.exchange("https://httpbin.org/user-agent", HttpMethod.GET,
		// entity, Map.class);
		// System.out.println(response.getBody());

		return restTemplate.postForEntity(CLOUDURL + "stocktransferouthdr/acceptstock", stockoutDtlList,
				StockTransferMessageEntity.class);
		//
//		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<String>() {
//				});
		//
//		return response;
	}

	public ResponseEntity<String> sendpurchaseToCloud(PurchaseHdrMessageEntity purchaseDtlList) {

		// Execute the method writing your HttpEntity to the request
		// ResponseEntity<Map> response =
		// restTemplate.exchange("https://httpbin.org/user-agent", HttpMethod.GET,
		// entity, Map.class);
		// System.out.println(response.getBody());

		return restTemplate.postForEntity(CLOUDURL + "purchasehdrresource/sendpurchase", purchaseDtlList, String.class);

		//
//ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
//		new ParameterizedTypeReference<String>() {
//		});
//
//return response;

	}

	public ResponseEntity<String> getLoyaltyResult(String mobileNumber, String api, String hash) {
		final HttpHeaders headers = new HttpHeaders();

		headers.set("Hash-Key", hash);
		headers.set("API-Key", api);
		String path = "http://staging.mysearchindia.in/pos/loyalty/details?mobile_number=" + mobileNumber;

		final HttpEntity<String> entity = new HttpEntity<String>(headers);

		// Execute the method writing your HttpEntity to the request
		// ResponseEntity<Map> response =
		// restTemplate.exchange("https://httpbin.org/user-agent", HttpMethod.GET,
		// entity, Map.class);
		// System.out.println(response.getBody());

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, entity,
				new ParameterizedTypeReference<String>() {
				});

		return response;
	}

	public ResponseEntity<String> sendSalesMessageEntityToCloud(SalesMessageEntity salesMessageEntity) {

		return restTemplate.postForEntity(CLOUDURL + "salestranshdr/savesalesmessageentity", salesMessageEntity,
				String.class);

	}

	public ResponseEntity<String> sendAccountMessageEntityToCloud(AccountMessage accountMessage) {

		return restTemplate.postForEntity(CLOUDURL + "salestranshdr/savesalesmessageentity", accountMessage,
				String.class);
	}

	public ResponseEntity<String> sendItemBatchDtlToCloud(ItemBatchDtl itemBatchDtl) {

		return restTemplate.postForEntity(CLOUDURL + "itembatchmst/saveitembatchdtl", itemBatchDtl, String.class);

	}

	public ResponseEntity<String> sendItemBatchExpDtlToCloud(ItemBatchExpiryDtl itemBatchExpDtl) {

		return restTemplate.postForEntity(CLOUDURL + "itembatchexpirydtl/saveitembatchexpirydtl", itemBatchExpDtl,
				String.class);

	}

	public String getStocktransferFromServer(String companyid, String stocktransferouthdrid) {

		// @GetMapping("{companymstid}/stocktransferouthdr/{stocktransferouthdrid}/stocktransfermessage")

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}

		// @GetMapping("{companymstid}/stocktransfermessageentity/{hdrid}")
		String apiPath = CLOUDURL + companyid + "/" + "stocktransfermessageentity/" + stocktransferouthdrid;
		System.out.println(apiPath);
		ResponseEntity<StockTransferMessageEntity> response = restTemplate.exchange(apiPath, HttpMethod.GET, null,
				new ParameterizedTypeReference<StockTransferMessageEntity>() {
				});
		StockTransferMessageEntity stockTransferMessageEntity = response.getBody();
		// processStockTransferId(stockTransferMessageEntity);

		//messageObjectService.processStockTransferMessage(stockTransferMessageEntity);

		return "OK";

	}

	public String getOtherBranchSalesFromServer(String companyid, String hdrid) {

		// @GetMapping("{companymstid}/stocktransferouthdr/{stocktransferouthdrid}/stocktransfermessage")

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}

		String apiPath = CLOUDURL + companyid + "/" + "otherbranchmessagebyhdrid/" + hdrid;
		System.out.println(apiPath);
		ResponseEntity<OtherBranchSalesMessageEntity> response = restTemplate.exchange(apiPath, HttpMethod.GET, null,
				new ParameterizedTypeReference<OtherBranchSalesMessageEntity>() {
				});
		OtherBranchSalesMessageEntity otherBranchSalesMessageEntity = response.getBody();

		//messageObjectService.processOtherBranchSales(otherBranchSalesMessageEntity);

		return "OK";

	}

	public ResponseEntity<String> sendOtherbranchSalesMessage(OtherBranchSalesMessageEntity salesMessageEntity,
			String companyid) {

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companyid + "/" + "sendotherbranchsalemessage";
		System.out.println(path);

		return restTemplate.postForEntity(path, salesMessageEntity, String.class);

	}

	public String deleteAllTask() {

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}

		String path = CLOUDURL + "/rest/process-instance";
		System.out.println(path);

		ResponseEntity<ArrayList> ResponseList = restTemplate.getForEntity(path, ArrayList.class);

		ArrayList processList = ResponseList.getBody();
		Iterator itr = processList.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			// Iterator itr2 = lm.keySet().iterator();
			// while(itr2.hasNext()) {

			String processInstanceId = (String) lm.get("id");

			path = CLOUDURL + "/rest/process-instance" + "/" + processInstanceId;

			ResponseEntity<OtherBranchSalesMessageEntity> response = restTemplate.exchange(path, HttpMethod.DELETE,
					null, new ParameterizedTypeReference<OtherBranchSalesMessageEntity>() {
					});

		}

		return "OK";

	}

	public String getPuchaseAndSavePriceDef(String companymstid, String hdrid) {

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}

		String apiPath = CLOUDURL + companymstid + "/purchasehdr/" + hdrid + "/purchasedtl";
		System.out.println(apiPath);
		ResponseEntity<List<PurchaseDtl>> response = restTemplate.exchange(apiPath, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseDtl>>() {
				});

		List<PurchaseDtl> purchaseDtlList = response.getBody();
		if (purchaseDtlList.size() > 0) {
			savePurchasePriceDefinition(purchaseDtlList);

		}
		return "OK";

	}

	private void savePurchasePriceDefinition(List<PurchaseDtl> purchaseDtlList) {

		for (PurchaseDtl purchase : purchaseDtlList) {
			PurchasePriceDefinitionMst purchasePriceDefinitionMst = new PurchasePriceDefinitionMst();

			purchasePriceDefinitionMst.setBatch(purchase.getBatch());
			purchasePriceDefinitionMst.setBranchCode(purchase.getPurchaseHdr().getBranchCode());
			purchasePriceDefinitionMst.setItemId(purchase.getItemId());
			purchasePriceDefinitionMst.setCompanyMst(purchase.getPurchaseHdr().getCompanyMst());
			purchasePriceDefinitionMst.setItemName(purchase.getItemName());
			purchasePriceDefinitionMst.setPurchaseHdrId(purchase.getPurchaseHdr().getId());
			purchasePriceDefinitionMst.setPurchaseVoucherDate(purchase.getPurchaseHdr().getVoucherDate());
			purchasePriceDefinitionMst.setPurchaseVoucherNumber(purchase.getPurchaseHdr().getVoucherNumber());
			purchasePriceDefinitionMst.setStatus("N");
//			purchasePriceDefinitionMst.setUnitName(purchase.getu);
			purchasePriceDefinitionMst.setUnitId(purchase.getUnitId());

			purchasePriceDefinitionMst = purchasePriceDefinitionRepository.save(purchasePriceDefinitionMst);
		}
	}

	/*
	 * private void processStockTransferId(StockTransferMessageEntity msg) {
	 * StockTransferInHdr stockTransferInHdr = null;
	 * 
	 * StockTransferOutHdr stockTransferOutHdr = msg.getStockTransferOutHdr();
	 * StockTransferInHdr stkinhdr =
	 * stockTransferInHdrRepo.findByVoucherDateAndVoucherNumber(
	 * stockTransferOutHdr.getVoucherDate(),
	 * stockTransferOutHdr.getVoucherNumber());
	 * 
	 * Iterator iter0 = msg.getCategoryList().iterator(); while (iter0.hasNext()) {
	 * CategoryMst categoryMst = (CategoryMst) iter0.next(); CategoryMst categorymst
	 * = categoryMstRepo.findByCompanyMstIdAndCategoryName(
	 * categoryMst.getCompanyMst().getId(), categoryMst.getCategoryName()); if (null
	 * == categorymst) { categoryMstRepo.save(categoryMst); } }
	 * 
	 * Iterator iterunit = msg.getUnitMstList().iterator(); while
	 * (iterunit.hasNext()) { UnitMst unitmst = (UnitMst)iterunit.next(); UnitMst
	 * unit = unitMstRepo.findByUnitNameAndCompanyMstId(unitmst.getUnitName(),
	 * unitmst.getCompanyMst().getId()); if(null == unit) {
	 * unitMstRepo.save(unitmst); } }
	 * 
	 * Iterator iter1 = msg.getItemmstList().iterator(); while (iter1.hasNext()) {
	 * ItemMst itemmst = (ItemMst) iter1.next(); Optional<ItemMst> itemByIdOP =
	 * itemmstrepo.findById(itemmst.getId()); if (null == itemByIdOP ||
	 * !itemByIdOP.isPresent()) { try { if (null == itemmst.getBestBefore()) {
	 * itemmst.setBestBefore(0); } if (null == itemmst.getServiceOrGoods()) {
	 * itemmst.setServiceOrGoods("GOODS"); } if (null ==
	 * itemmst.getReorderWaitingPeriod()) { itemmst.setReorderWaitingPeriod(0); }
	 * itemmst.setRank(0); itemmstrepo.save(itemmst); } catch (Exception e) {
	 * System.out.println(e.toString()); } } } Iterator iter2 =
	 * msg.getPriceDefinitionMstList().iterator(); while (iter2.hasNext()) {
	 * PriceDefenitionMst priceDefMst = (PriceDefenitionMst) iter2.next();
	 * PriceDefenitionMst priceDef = priceDefinitionMstRepo
	 * .findByCompanyMstAndPriceLevelName(priceDefMst.getCompanyMst(),
	 * priceDefMst.getPriceLevelName()); try { if (null == priceDef) {
	 * priceDefinitionMstRepo.save(priceDefMst); } } catch (Exception e) { // TODO:
	 * handle exception } }
	 * 
	 * Iterator iter3 = msg.getPriceDefinitionList().iterator(); while
	 * (iter3.hasNext()) { PriceDefinition priceDef = (PriceDefinition)
	 * iter3.next(); PriceDefinition priceDefinition =
	 * priceDefinitionRepo.findPriceDefenitionByItemUnitPriceAndDate(
	 * priceDef.getItemId(), priceDef.getPriceId(), priceDef.getUnitId(),
	 * priceDef.getCompanyMst().getId()); try { if (null == priceDefinition) {
	 * priceDefinitionRepo.save(priceDef); } } catch (Exception e) { // TODO: handle
	 * exception } } if (null == stkinhdr) { stockTransferInHdr = new
	 * StockTransferInHdr();
	 * stockTransferInHdr.setIntentNumber(stockTransferOutHdr.getIntentNumber());
	 * stockTransferInHdr.setInVoucherDate(stockTransferOutHdr.getVoucherDate());
	 * stockTransferInHdr.setInVoucherNumber(stockTransferOutHdr.getVoucherNumber())
	 * ; stockTransferInHdr.setFromBranch(stockTransferOutHdr.getFromBranch());
	 * stockTransferInHdr.setStatusAcceptStock("PENDING");
	 * stockTransferInHdr.setVoucherType("STOCKTRANSFER");
	 * stockTransferInHdr.setVoucherDate(ClientSystemSetting.localToUtilDate(
	 * LocalDate.now()));
	 * stockTransferInHdr.setCompanyMst(stockTransferOutHdr.getCompanyMst());
	 * stockTransferInHdr.setStockTransferOutHdrId(stockTransferOutHdr.getId()); ;
	 * StockTransferInHdr stkInHdr =
	 * stockTransferInHdrRepo.findByInVoucherDateAndInVoucherNumber(
	 * stockTransferOutHdr.getVoucherDate(),
	 * stockTransferOutHdr.getVoucherNumber()); if (null != stkInHdr) { return; }
	 * stockTransferInHdrRepo.save(stockTransferInHdr); }
	 * 
	 * Iterator iter = msg.getStockTransferOutDtlList().iterator(); while
	 * (iter.hasNext()) { StockTransferInDtl stockTransferInDtl = new
	 * StockTransferInDtl(); StockTransferOutDtl stockTransferOutDtl =
	 * (StockTransferOutDtl) iter.next();
	 * 
	 * stockTransferInDtl.setAmount(stockTransferOutDtl.getAmount());
	 * stockTransferInDtl.setBarcode(stockTransferOutDtl.getBarcode());
	 * stockTransferInDtl.setBatch(stockTransferOutDtl.getBatch());
	 * stockTransferInDtl.setExpiryDate(stockTransferOutDtl.getExpiryDate());
	 * stockTransferInDtl.setItemId(stockTransferOutDtl.getItemId());
	 * stockTransferInDtl.setItemCode(stockTransferOutDtl.getItemCode());
	 * stockTransferInDtl.setMrp(stockTransferOutDtl.getMrp());
	 * stockTransferInDtl.setQty(stockTransferOutDtl.getQty());
	 * stockTransferInDtl.setRate(stockTransferOutDtl.getRate());
	 * 
	 * stockTransferInDtl.setStockTransferInHdr(stockTransferInHdr);
	 * 
	 * stockTransferInDtl.setTaxRate(stockTransferOutDtl.getTaxRate());
	 * stockTransferInDtl.setUnitId(stockTransferOutDtl.getUnitId());
	 * stockTransferInDtl.setCompanyMst(stockTransferOutDtl.getCompanyMst());
	 * stockTransferInDtl = stockTransferInDtlRepository.save(stockTransferInDtl);
	 * 
	 * if (!stockTransferInDtl.getBatch().equalsIgnoreCase("NOBATCH") && null !=
	 * stockTransferInDtl.getExpiryDate()) { ItemBatchExpiryDtl itemBatchExpiryDtl =
	 * new ItemBatchExpiryDtl(); List<ItemBatchExpiryDtl> items =
	 * itemBatchExpiryDtlRepo.findByCompanyMstAndItemIdAndBatch(
	 * stockTransferInDtl.getCompanyMst(), stockTransferInDtl.getItemId(),
	 * stockTransferInDtl.getBatch()); if (items.isEmpty()) {
	 * itemBatchExpiryDtl.setBatch(stockTransferInDtl.getBatch());
	 * itemBatchExpiryDtl.setCompanyMst(stockTransferInDtl.getCompanyMst());
	 * itemBatchExpiryDtl.setExpiryDate(stockTransferInDtl.getExpiryDate());
	 * itemBatchExpiryDtl.setItemId(stockTransferInDtl.getItemId());
	 * itemBatchExpiryDtl.setStockTransferInDtl(stockTransferInDtl);
	 * 
	 * itemBatchExpiryDtl.setUpdatedDate(ClientSystemSetting.getSystemDate());
	 * itemBatchExpiryDtlRepo.save(itemBatchExpiryDtl); } else {
	 * itemBatchExpiryDtl.setBatch(stockTransferInDtl.getBatch());
	 * itemBatchExpiryDtl.setCompanyMst(stockTransferInDtl.getCompanyMst());
	 * itemBatchExpiryDtl.setExpiryDate(stockTransferInDtl.getExpiryDate());
	 * itemBatchExpiryDtl.setItemId(stockTransferInDtl.getItemId());
	 * itemBatchExpiryDtl.setStockTransferInDtl(stockTransferInDtl);
	 * itemBatchExpiryDtl.setUpdatedDate(ClientSystemSetting.getSystemDate());
	 * itemBatchExpiryDtlRepo.UpdateItemBatchExpiryDtl(stockTransferInDtl.getItemId(
	 * ), stockTransferInDtl.getExpiryDate(), stockTransferInDtl.getBatch()); } } }
	 * 
	 * 
	 * }
	 * 
	 * public String completeTask(String taskId) {
	 * 
	 * String path = CLOUDURL + "/rest/task/" + taskId + "/complete"; HashMap
	 * variables = new HashMap(); variables.put("voucherNumber", "123");
	 * restTemplate.postForEntity(path, variables, String.class); return "ok"; }
	 * 
	 * 
	 * 
	 * 
	 */

	public String geSubscriptionDate(String userId, String password, String companyMstId, String branchcode) {

		String apiPath = "http://www.mapleerp.com:" + maple + "/" + companyMstId
				+ "/subscriptionmstresource/getsubscriptiondate?userid=" + userId + "&password=" + password
				+ "&branchcode=" + branchcode;
		System.out.println(apiPath);
		ResponseEntity<String> response = restTemplate.exchange(apiPath, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		String subscriptionDate = response.getBody();

		return subscriptionDate;

	}

//--------------------------------------publish onlineline-----------------------------
	public ResponseEntity<ItemMst> sendItemMstOnLineToCloud(String companymstid, @Valid ItemMst itemMst1) {

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companymstid + "/" + "itemmst";
		System.out.println("###################################################################333");

		System.out.println(path);

//		return restTemplate.postForEntity(path, itemMst1, ItemMst.class);

		try {
			ResponseEntity<ItemMst> result = restTemplate.postForEntity(path, itemMst1, ItemMst.class);
			return result;
		} catch (Exception e) {
			return null;
		}

	}

//	public ResponseEntity<CustomerMst> sendCustomerMstOffLineMessageToCloud(String companymstid,
//			@Valid CustomerMst customerReg) {
//		if (!CLOUDURL.endsWith("/")) {
//			CLOUDURL = CLOUDURL + "/";
//		}
//		System.out.println(" CLOUDURL@@" + CLOUDURL);
//		String path = CLOUDURL + companymstid + "/" + "customerregistration";
//		System.out.println(path);
//
//		return restTemplate.postForEntity(path, customerReg, CustomerMst.class);
//
//	}

//	public ResponseEntity<Supplier> sendSupplierOffLineMessageToCloud(String companymstid, @Valid Supplier supplier) {
//		if (!CLOUDURL.endsWith("/")) {
//			CLOUDURL = CLOUDURL + "/";
//		}
//		System.out.println(" CLOUDURL@@" + CLOUDURL);
//		String path = CLOUDURL + companymstid + "/" + "supplier";
//		System.out.println(path);
//
//		return restTemplate.postForEntity(path, supplier, Supplier.class);
//
//	}

	public ResponseEntity<KitDefinitionMst> sendKitDefinitionMstOffLineMessageToCloud(String companymstid,
			@Valid KitDefinitionMst kitdefMst) {
		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companymstid + "/" + "kitdefinitionmst";
		System.out.println(path);

		return restTemplate.postForEntity(path, kitdefMst, KitDefinitionMst.class);
	}

	public ResponseEntity<KitDefenitionDtl> sendKitDefenitionDtlOffLineMessageToCloud(String companymstid,
			@Valid KitDefenitionDtl kitdefinitionDtlRequest, String kitdefinitionmstId) {
		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companymstid + "/" + "kitdefinitionmsts/" + kitdefinitionmstId + "/kitdefinitondtl";
		System.out.println(path);

		return restTemplate.postForEntity(path, kitdefinitionDtlRequest, KitDefenitionDtl.class);

	}

	public ResponseEntity<UnitMst> sendunitMstOffLineMessageToCloud(String companymstid, @Valid UnitMst unitMst1) {
		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companymstid + "/" + "unitmst";
		System.out.println(path);

		return restTemplate.postForEntity(path, unitMst1, UnitMst.class);

	}

	public ResponseEntity<CategoryMst> sendcategoryMstOffLineMessageToCloud(String companymstid,
			@Valid CategoryMst categoryMstReq) {
		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companymstid + "/" + "categorymst";
		System.out.println(path);

		return restTemplate.postForEntity(path, categoryMstReq, CategoryMst.class);

	}

	public ResponseEntity<KitDefinitionMst> sendKitDefenitionMstOffLineMessageToCloud(String companymstid,
			@Valid KitDefinitionMst kitDefinitionMstRequest) {
		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companymstid + "/" + "kitdefinitionmst";
		System.out.println(path);

		return restTemplate.postForEntity(path, kitDefinitionMstRequest, KitDefinitionMst.class);
	}

	public ResponseEntity<KitDefinitionMst> sendKitDefenitionMstUpdateOffLineMessageToCloud(String companymstid,
			String kitdefinitionmstid, @Valid KitDefinitionMst kitdefinitionRequest) {
		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companymstid + "/" + "kitdefinitionmst/" + kitdefinitionmstid;
		System.out.println(path);

		return restTemplate.postForEntity(path, kitdefinitionRequest, KitDefinitionMst.class);
	}

	public ResponseEntity<String> salesMessageEntityToCloud(String companymstid,
			SalesMessageEntity salesMessageEntity) {
		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companymstid + "/" + "salestranshdr/savesalesmessageentity";

		System.out.println(path);
		return restTemplate.postForEntity(path, salesMessageEntity, String.class);
	}

//===============saving sales dtls to cloud==============by anandu===============04-08-2021=======
//public ResponseEntity<SalesDtl> salesDtlsSavedToCloud(String companymstid, SalesDtl addedItem) {
//	if(!CLOUDURL.endsWith("/")) {
//		CLOUDURL =CLOUDURL+"/";
//	}
//	System.out.println(" CLOUDURL@@" + CLOUDURL);
//	String path = CLOUDURL +companymstid+"/"+ "salestranshdr/"+addedItem.getSalesTransHdr().getId()+"/salesdtl";
//
//	System.out.println(path);
//	
//	return restTemplate.postForEntity(path, addedItem,
//			SalesDtl.class);
//}
//=================end saving sales dtls to cloud===============================

//===============saving sales trans hdr to cloud==============by anandu===============05-08-2021=======
//public ResponseEntity<SalesTransHdr> salesTransHdrSavedToCloud(String companymstid, SalesTransHdr saveSalesTransHdr) {
//	if(!CLOUDURL.endsWith("/")) {
//		CLOUDURL =CLOUDURL+"/";
//	}
//	System.out.println(" CLOUDURL@@" + CLOUDURL);
//	String path = CLOUDURL +companymstid+"/"+ "salestranshdr";
//
//	System.out.println(path);
//	
//	return restTemplate.postForEntity(path, saveSalesTransHdr,
//			SalesTransHdr.class);
//}
//=================end saving sales hdr to cloud===============================

//===============saving customer mst to cloud==============by anandu===============05-08-2021=======
//public ResponseEntity<CustomerMst> customerDtlSavedToCloud(String companymstid, CustomerMst savedCustomer) {
//	if(!CLOUDURL.endsWith("/")) {
//		CLOUDURL =CLOUDURL+"/";
//	}
//	System.out.println(" CLOUDURL@@" + CLOUDURL);
//	String path = CLOUDURL +companymstid+"/"+ "customerregistration";
//
//	System.out.println(path);
//	
//	return restTemplate.postForEntity(path, savedCustomer,
//			CustomerMst.class);
//}
//=================end saving customer mst to cloud===============================

//===============updating sales trans hdr to cloud==============by anandu===============05-08-2021=======
//public void  salesTransHdrUpdatedToCloud(String companymstid, SalesTransHdr salestranshdr) {
//	if(!CLOUDURL.endsWith("/")) {
//		CLOUDURL =CLOUDURL+"/";
//	}
//	System.out.println(" CLOUDURL@@" + CLOUDURL);
//	String path = CLOUDURL +companymstid+"/"+ "salestranshdrfinalsave/"+salestranshdr.getId();
//
//	System.out.println(path);
//	
//	 restTemplate.put(path, salestranshdr);
//	 return;
//}
//=================end updating sales trans hdr to cloud===============================

//=================currently not used==========================anandu==============
//public ResponseEntity<GoodReceiveNoteHdr> sendGoodReceiveNoteHdrUpdateOffLineMessageToCloud(String companymstid,
//		  String goodReceiveNoteHdrid, @Valid GoodReceiveNoteHdr goodReceiveNoteHdrRequest) {
//	if(!CLOUDURL.endsWith("/")) {
//		CLOUDURL =CLOUDURL+"/";
//	}
//	System.out.println(" CLOUDURL@@" + CLOUDURL);
//	String path = CLOUDURL +companymstid+"/"+ "goodreceivenotehdrid/"+goodReceiveNoteHdrid;
//	System.out.println(path);
//	
//	return restTemplate.postForEntity(path, goodReceiveNoteHdrRequest,
//			GoodReceiveNoteHdr.class);
//}

//API for cloudmessage receive  

//	public ResponseEntity<CustomerMst> saveCustomer(String companymstid, @Valid CustomerMst customer) {
//		if (!CLOUDURL.endsWith("/")) {
//			CLOUDURL = CLOUDURL + "/";
//		}
//		System.out.println(" CLOUDURL@@" + CLOUDURL);
//		String path = CLOUDURL + companymstid + "/" + "customerregistration";
//		System.out.println(path);
//
//		return restTemplate.postForEntity(path, customer, CustomerMst.class);
//
//	}

	public ResponseEntity<ItemMst> itemCreationToCloud(String companymstid, ItemMst item) {
		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companymstid + "/" + "itemmst";

		System.out.println(path);
		return restTemplate.postForEntity(path, item, ItemMst.class);
	}

	public ResponseEntity<CategoryMst> categoryCreationToCloud(String companymstid, CategoryMst category) {
		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companymstid + "/" + "categorymst";

		System.out.println(path);
		return restTemplate.postForEntity(path, category, CategoryMst.class);
	}

	public ResponseEntity<UnitMst> unitCreationToCloud(String companymstid, UnitMst unitMst) {
		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companymstid + "/" + "unitmst";

		System.out.println(path);
		return restTemplate.postForEntity(path, unitMst, UnitMst.class);
	}

//	public ResponseEntity<ItemBatchDtl> itemBatchDtlToCloud(String companymstid, List<ItemBatchDtl> itemBatchDtl) {
//		if (!CLOUDURL.endsWith("/")) {
//			CLOUDURL = CLOUDURL + "/";
//		}
//		System.out.println(" CLOUDURL@@" + CLOUDURL);
//		String path = CLOUDURL + companymstid + "/" + "";
//
//		System.out.println(path);
//		return restTemplate.postForEntity(path, itemBatchDtl, ItemBatchDtl.class);
//	}

	public ResponseEntity<String> commonReceiveMsgToCloud(CommonReceiveMsgEntity messageObjectMst,
			String companyMstId) {

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companyMstId + "/" + "commonreceivemsg/commonreceivemsgpublishing";

		System.out.println(path);
		return restTemplate.postForEntity(path, messageObjectMst, String.class);
	}

	public List<LastBestDate> countVerificationTocloud(String companyMstId, String branchcode,
			List<LastBestDate> lastbest) {

		System.out.println("Entered in to externalApi.countVerificationTocloud");
		if (!CLOUDURL.endsWith("/")) {

			CLOUDURL = CLOUDURL + "/";

		}

		System.out.println(" CLOUDURL@@" + CLOUDURL);

		String path = CLOUDURL + companyMstId + "/" + "lastbestdateresource/gettotalcountofsalespurchasestock/"
				+ branchcode;

		System.out.println(path);
		ResponseEntity<List> result = null;

		try {
			result = restTemplate.postForEntity(path, lastbest, List.class);
			System.out.println("The result of externalApi.countVerificationTocloud is:" + result);

			if (null == result) {
				for (LastBestDate lastDate : lastbest) {

					lastDate.setCount(0.0);
				}
				return lastbest;
			}
			return result.getBody();
		} catch (Exception e) {

			// ==========MAP-137--server-failure-status-updation========================anandu======
			ServerFailureStatusMst serverFailureStatusMst = new ServerFailureStatusMst();

			Date date = new Date();
			java.sql.Date retryDate = new java.sql.Date(date.getTime());
			serverFailureStatusMst.setDateOfRetry(retryDate);

			if (e.getMessage().length() > 100) {
				serverFailureStatusMst.setDescription(e.getMessage().substring(0, 150));
			} else {
				serverFailureStatusMst.setDescription(e.getMessage());
			}

			serverFailureStatusRepository.saveAndFlush(serverFailureStatusMst);

			return null;
		}

	}

	// ================MAP-103-separate-api-call-of-single-purchase-save================anandu=========
	public ResponseEntity<String> purchaseSingleSaveToCloud(CommonReceiveMsgEntity messageObjectMst,
			String companyMstId) {

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companyMstId + "/" + "commonreceivemsgresource/purchasesavesingle";

		System.out.println(path);
		return restTemplate.postForEntity(path, messageObjectMst, String.class);
	}

	// ================MAP-103-separate-api-call-of-single-sales-save================anandu=========
	public ResponseEntity<String> salesSingleSaveToCloud(CommonReceiveMsgEntity messageObjectMst, String companyMstId) {

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}
		System.out.println(" CLOUDURL@@" + CLOUDURL);
		String path = CLOUDURL + companyMstId + "/" + "commonreceivemsgresource/salessavesingle";

		System.out.println(path);
		return restTemplate.postForEntity(path, messageObjectMst, String.class);
	}

	public List<String> BulkSalesToCloud(List<SalesMessageEntity> salesMessageEntityarray, String companyMstId) {
		if (!CLOUDURL.endsWith("/")) {

			CLOUDURL = CLOUDURL + "/";

		}

		System.out.println("Entered into externalApi.BulkSalesToCloud() method****************************");

		System.out.println(" CLOUDURL@@" + CLOUDURL);

		String path = CLOUDURL + companyMstId + "/" + "commonreceivemsgresource/salessavebulk";

		System.out.println(path);

		ResponseEntity<List> result = restTemplate.postForEntity(path, salesMessageEntityarray, List.class);
		System.out.println("the result of externalapi.bulksalestocloud() is : " + result.getBody());
		return result.getBody();
	}

	public List<String> BulkPurchaseToCloud(List<PurchaseHdrMessageEntity> purchaseMessageEntityArray,
			String companyMstId) {

		if (!CLOUDURL.endsWith("/")) {

			CLOUDURL = CLOUDURL + "/";

		}

		System.out.println("Entered into externalApi.BulkPurchaseToCloud() method****************************");

		System.out.println(" CLOUDURL@@" + CLOUDURL);

		String path = CLOUDURL + companyMstId + "/" + "commonreceivemsgresource/purchasesavemultiple";

		System.out.println(path);
		ResponseEntity<List> result = restTemplate.postForEntity(path, purchaseMessageEntityArray, List.class);
		System.out.println(
				"The result of PurchaseDataSync.retry().externalApi.BulkPurchaseToCloud() is : " + result.getBody());
		return result.getBody();

	}

	public List<String> BulkStockTransferToCloud(List<StockTransferMessageEntity> stocktransferMessageEntityarray,
			String companymstid) {

		if (!CLOUDURL.endsWith("/")) {

			CLOUDURL = CLOUDURL + "/";

		}

		System.out.println(" CLOUDURL@@" + CLOUDURL);

		String path = CLOUDURL + companymstid + "/" + "commonreceivemsg/commonreceivemsgbulkstockpublishing";

		System.out.println(path);

		ResponseEntity<List> result = restTemplate.postForEntity(path, stocktransferMessageEntityarray, List.class);
		return result.getBody();
	}

	public ResponseEntity<String> bulkitemBatchDtlToCloud(List<ItemBatchDtl> itemBatchDtlarray, String companymstid) {
		if (!CLOUDURL.endsWith("/")) {

			CLOUDURL = CLOUDURL + "/";

		}

		System.out.println(" CLOUDURL@@" + CLOUDURL);

		String path = CLOUDURL + companymstid + "/" + "commonreceivemsg/commonreceivemsgbulkitembatchdtlpublishing";

		System.out.println(path);

		return (ResponseEntity<String>) restTemplate.postForEntity(path, itemBatchDtlarray, String.class);

	}

	public String SalesTransHdrIdsFromCloud(String date) {

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}

		System.out.println(" CLOUDURL@@" + CLOUDURL);

		String path = CLOUDURL + mycompany + "/" + "salestranshdr/salestranshdridbydate/" + mybranch + "?date=" + date;

		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response.getBody();

	}

	public String PurchaseHdrIdsFromCloud(String date) {

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}

		System.out.println(" CLOUDURL@@" + CLOUDURL);

		String path = CLOUDURL + mycompany + "/" + "purchasehdr/purchasehdridbydate/" + mybranch + "?date=" + date;

		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response.getBody();
	}

	public String ItemBatchDtlIdsFromCloud(String date) {

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}

		System.out.println(" CLOUDURL@@" + CLOUDURL);

		String path = CLOUDURL + mycompany + "/" + "itembatchmst/itembatchidbydate/" + mybranch + "?date=" + date;

		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response.getBody();
	}
	
	/*
	 * get summary of daily stock from cloud. 
	 * Then delete the old data of item batch dtl.
	 * save the summary of daily stock in to item batch dtl.
	 */
	public void getDailyStockSummaryFromCloudToItemBatchDtl() {

		if (!CLOUDURL.endsWith("/")) {
			CLOUDURL = CLOUDURL + "/";
		}

		System.out.println(" CLOUDURL@@" + CLOUDURL);

		String path = CLOUDURL + mycompany + "/" + "dayendclosurehdrresource/getsummaryofdailystock/" + mybranch ;

		System.out.println(path);

		ResponseEntity<List<SummaryOfDailyStock>> summaryOfDailyStockList = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SummaryOfDailyStock>>() {
				});

		if(summaryOfDailyStockList.getBody().size() > 0) {
			List<ItemBatchDtl> getItemBatchDtlList = itemBatchDtlRepository.findAll();
			if(getItemBatchDtlList.size() > 0) {
				for(ItemBatchDtl itemBatchDtl : getItemBatchDtlList) {
					itemBatchDtlRepository.delete(itemBatchDtl);
				}
			}
			
			
			for(SummaryOfDailyStock summaryOfDailyStock : summaryOfDailyStockList.getBody()) {
				ItemBatchDtl itembatchdtl = new ItemBatchDtl();
				itembatchdtl.setBatch(summaryOfDailyStock.getBatch());
				itembatchdtl.setBranchCode(summaryOfDailyStock.getBranchCode());
				itembatchdtl.setItemId(summaryOfDailyStock.getItemId());
				itembatchdtl.setExpDate(summaryOfDailyStock.getExpiryDate());
				itembatchdtl.setQtyIn(summaryOfDailyStock.getStockIn());
				itembatchdtl.setQtyOut(summaryOfDailyStock.getStockOut());
				itembatchdtl.setStore(MapleConstants.Store);
				
				Optional<CompanyMst> companyMst = companyMstRepository.findById(mycompany);
				itembatchdtl.setCompanyMst(companyMst.get());
				
				Optional<ItemMst> itemmst = itemMstRepository.findById(summaryOfDailyStock.getItemId());
				itembatchdtl.setBarcode(itemmst.get().getBarCode());
				itembatchdtl.setMrp(itemmst.get().getStandardPrice());
				
				Date currentDate = new Date();				
				itembatchdtl.setVoucherDate(currentDate);
				itembatchdtl.setVoucherNumber(currentDate.toString());
				itembatchdtl.setUpdatedTime(currentDate.toInstant().atZone(ZoneId.systemDefault())
					      .toLocalDateTime());
				itembatchdtl.setParticulars(MapleConstants.OpeningStock);
				itemBatchDtlRepository.save(itembatchdtl);
			}
		}
		
	}
	

}
