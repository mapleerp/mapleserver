package com.maple.restserver.utils;

import java.util.concurrent.Executors;

import org.springframework.stereotype.Component;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;

@Component
public class EventBusFactory {
private static EventBus eventBus = new AsyncEventBus(Executors.newCachedThreadPool());

public static EventBus getEventBus() {
	return eventBus;
	
}
}
