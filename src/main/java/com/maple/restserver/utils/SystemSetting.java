package com.maple.restserver.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.FinancialYearMst;

import com.maple.restserver.entity.PurchaseHdrMessageEntity;
import com.maple.restserver.entity.StockTransferMessageEntity;

import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchExpiryDtl;
import com.maple.restserver.entity.ParamValueConfig;
import com.maple.restserver.entity.SalesMessageEntity;
import com.maple.restserver.entity.StockTransferMessageEntity;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.message.entity.AccountMessage;
import com.maple.restserver.repository.FinancialYearMstRepository;

import com.maple.restserver.repository.ParamValueConfigRepository;

@Component
public class SystemSetting {

 
	@Autowired
	ParamValueConfigRepository paramValueconfigRepo;

 

 
	public static ArrayList<String> user_roles = new ArrayList();
	public static String BARCODE_FORMAT = "FOMAT0";

	public static String BARCODE_SIZE = "SIZE 3,1.5";

	public static String DEFAULT_UNIT = "NOS";
	public static double DEFAULT_CESS = 0.0;
	public static double DEFAULT_GST = 18;
	public static double DEFAULT_TAXRATE = 18;
	public static String DEFAULT_BATCH = MapleConstants.Nobatch;
	public static String DEFAULT_ITEMCODE = "";
	public static String DEFAULT_HSNCODE = "";
	public static double DEFAULT_SELLINGPRICE = 0.0;
	public static double DEFAULT_STATECESS = 0.0;

	public static String ITEM_CODE_GENERATOR_ID = "ICD";
	public static String VOUCHER_NAME_GENERATOR_ID = "VNO";

	private static FinancialYearMst financialYear;

	private static final Logger logger = LoggerFactory.getLogger(SystemSetting.class);

	public static Date systemDate;

	public SystemSetting() {
		systemDate = new java.util.Date();
	}

	public static Date getSystemDate() {
		systemDate = new java.util.Date();
		return systemDate;
	}

	public static void setSystemDate(Date systemDate) {
		SystemSetting.systemDate = systemDate;
	}

	public static java.util.Date localToUtilDate(LocalDate lDate) {

		Date date = Date.from(lDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		return date;
	}

	public static LocalDate utilToLocaDate(java.util.Date uDate) {

		if (null == uDate) {

			System.out.println("Null Value as Date ");
			return null;
		}

		LocalDate date = uDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		return date;
	}

	/*
	 * public Pair<Date, Date> getDateRange() { Date begining, end;
	 * 
	 * { Calendar calendar = getCalendarForNow();
	 * calendar.set(Calendar.DAY_OF_MONTH,
	 * calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
	 * setTimeToBeginningOfDay(calendar); begining = calendar.getTime(); }
	 * 
	 * { Calendar calendar = getCalendarForNow();
	 * calendar.set(Calendar.DAY_OF_MONTH,
	 * calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	 * setTimeToEndofDay(calendar); end = calendar.getTime(); }
	 * 
	 * return Pair.of(begining, end); }
	 */

	private static Calendar getCalendarForNow() {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(new Date());
		return calendar;
	}

	private static void setTimeToBeginningOfDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}

	private static void setTimeToEndofDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
	}

	public String getCurrentLocalDateTimeStampAsString() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
	}

	public Timestamp getCurrentDatTime() {

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		Timestamp time = new Timestamp(cal.getTimeInMillis());

		return time;

	}

	public boolean isFutureDate(Date fDate) {

		boolean isfuture = false;

		Date current = new Date();

		// compare both dates
		if (fDate.after(current)) {
			isfuture = true;
		} else {
			isfuture = false;
		}
		return isfuture;
	}

	public static Date yesterday() {
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}

	public static Date nextDay(Date date) {

		LocalDate lDate = utilToLocaDate(date);

		lDate = lDate.plusDays(1);

		System.out.println("ORG DATE " + lDate);

		lDate = lDate.plusDays(1);
		System.out.println("Plus 1" + lDate);

		return localToUtilDate(lDate);
	}

	public static Date previousDay(Date date) {

		LocalDate lDate = utilToLocaDate(date);
		lDate = lDate.minusDays(1);

		return localToUtilDate(lDate);
	}

	public long DateLeft(Date fDate) {

		Date current = new Date();
		long diff = fDate.getTime() - current.getTime();

		long diffSeconds = diff / 1000;

		long diffMinutes = diffSeconds / 60;

		long diffHr = diffMinutes / 60;
		long diffDays = diffHr / 24;

		return diffDays;
	}

	public static boolean pingServer(String IP) {
		boolean pinged = false;
		try {
			Process p = Runtime.getRuntime().exec("ping " + IP);
			BufferedReader inputStream = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String s = "";
			// reading output stream of the command
			while ((s = inputStream.readLine()) != null) {
				System.out.println(s);
				pinged = true;
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			return pinged;
		}
		return true;
	}

	public static boolean hostAvailabilityCheck(String SERVER_ADDRESS, String url, int TCP_SERVER_PORT) {

		Socket s = null;

		try {

			s = new Socket(SERVER_ADDRESS, TCP_SERVER_PORT);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			logger.debug("Check and fouind server down");
		}
		boolean available = true;
		try {
			if (s.isConnected()) {
				s.close();
			}
		} catch (UnknownHostException e) { // unknown host
			available = false;
			
			try {
				s.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			s = null;
		} catch (IOException e) { // io exception, service probably not running
			available = false;
			
			try {
				s.close();
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			s = null;
		} catch (NullPointerException e) {
			available = false;
			
			try {
				s.close();
			} catch (IOException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			
			s = null;
		}
		
		try {
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return available;
	}

	public boolean isValidEmailAddress(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}

	public static String padLeft(String s, int n) {
		// return String.format("%1$" + n + "s", s);
		return String.format("%1$" + n + "s", s);
	}

	public static double round(double value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	public static double round(java.math.BigDecimal value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		value = value.setScale(places, BigDecimal.ROUND_HALF_UP);
		return value.doubleValue();
	}

	public static boolean IsValidNumber(String inString) {
		Pattern p = Pattern.compile("^[0-9]\\d*(\\.\\d+)?$");
		Matcher m = p.matcher(inString);
		boolean b = m.matches();
		return b;
	}

	public static java.util.Date StringToUtilDate(String strDate, String Format) {

		// String ReplaceStringDate = strDate.replace("-", "/");
		java.util.Date dt = StringToutilDateSlash(strDate, Format);
		return dt;
	}

	public static java.sql.Date StringToSqlDate(String strDate, String Format) {

		String ReplaceStringDate = strDate.replace("-", "/");
		String ReplaceFormat = Format.replace("-", "/");
		java.sql.Date dt = StringToSqlDateSlash(ReplaceStringDate, ReplaceFormat);
		return dt;
	}

	private static java.util.Date StringToutilDateSlash(String strDate, String Format) {
		SimpleDateFormat formatter = new SimpleDateFormat(Format); // "dd/MM/yyyy"
		// String dateInString = "7-Jun-2013";
		Date date = null;
		try {

			date = formatter.parse(strDate);
			System.out.println(date);
			System.out.println(formatter.format(date));

		} catch (ParseException e) {
			logger.debug(e.toString());
		}
		return date;
	}

	private static java.sql.Date StringToSqlDateSlash(String strDate, String Format) {
		SimpleDateFormat formatter = new SimpleDateFormat(Format);// "dd/MM/yyyy"

		Date dt = StringToutilDateSlash(strDate, Format);
		java.sql.Date sqlDate = new java.sql.Date(dt.getTime());

		return sqlDate;
	}

	public String StringToStringFormatChange(String strDate, String inFormat, String outFormat) {

		String ddmmyyDate = "";
		SimpleDateFormat formatter = new SimpleDateFormat(inFormat);// "dd/MM/yyyy"

		Date dt = StringToutilDateSlash(strDate, inFormat);
		java.sql.Date sqlDate = new java.sql.Date(dt.getTime());

		if (outFormat.equals("dd/MM/yyyy")) {
			ddmmyyDate = SqlDateTostring(sqlDate);
		} else if (outFormat.equals("yyyy-MM-dd")) {
			ddmmyyDate = SqlDateTostringYYMMDD(sqlDate);
		} else {
			ddmmyyDate = "OutFormat not supported";
		}

		return ddmmyyDate;
	}

	public static String SqlDateTostring(Date date) {

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String text = df.format(date);

		return text;
	}

	public static String SqlDateTostring(Date date, String Format) {

		DateFormat df = new SimpleDateFormat(Format);
		String text = df.format(date);

		return text;
	}

	public static java.util.Date SqlDateToUtilDate(java.sql.Date date) {

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String text = df.format(date);

		java.util.Date uDate = StringToutilDateSlash(text, "dd/MM/yyyy");
		return uDate;
	}

	public static String SqlDateTostringYYMMDD(Date date) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String text = formatter.format(date);

		return text;
	}

	public static String UtilDateToString(java.util.Date dt) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String reportDate = df.format(dt);
		logger.debug("Util To String input =" + dt);
		logger.debug("Util To String output =" + reportDate);
		return reportDate;
	}
	
	public static String UtilDateToString(java.util.Date dt, String foamat) {
		DateFormat df = new SimpleDateFormat(foamat);
		String reportDate = df.format(dt);
		logger.debug("Util To String input =" + dt);
		logger.debug("Util To String output =" + reportDate);
		return reportDate;
	}

	public static java.sql.Date UtilDateToSQLDate(java.util.Date dt) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String reportDate = df.format(dt);

		java.sql.Date sqlDate = StringToSqlDateSlash(reportDate, "dd/MM/yyyy");

		return sqlDate;
	}

	public boolean isValidDate(String dateString) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date Systemdate = new Date();
		int Sysyear = Calendar.getInstance().get(Calendar.YEAR);

		dateString = dateString.replace("/", "");
		dateString = dateString.replace("-", "");
		if (dateString.length() == 7) {
			dateString = "0".concat(dateString);
		}

		if (dateString == null || (dateString.length() != "yyyyMMdd".length())) {
			return false;
		}
		// ddMMyyyy

		String strDD = dateString.substring(0, 2);
		String strMon = dateString.substring(2, 4);
		String strYy = dateString.substring(4, 8);
		dateString = strYy + strMon + strDD;

		int date;
		try {
			date = Integer.parseInt(dateString);
		} catch (NumberFormatException e) {
			return false;
		}

		int year = date / 10000;
		int month = (date % 10000) / 100;
		int day = date % 100;

		// leap years calculation not valid before 1581
		boolean yearOk = (year >= Sysyear - 1) && (year <= Sysyear + 1);
		boolean monthOk = (month >= 1) && (month <= 12);
		boolean dayOk = (day >= 1) && (day <= daysInMonth(year, month));

		return (yearOk && monthOk && dayOk);
	}

	private int daysInMonth(int year, int month) {
		int daysInMonth;
		switch (month) {
		case 1: // fall through
		case 3: // fall through
		case 5: // fall through
		case 7: // fall through
		case 8: // fall through
		case 10: // fall through
		case 12:
			daysInMonth = 31;
			break;
		case 2:
			if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
				daysInMonth = 29;
			} else {
				daysInMonth = 28;
			}
			break;
		default:
			// returns 30 even for nonexistant months
			daysInMonth = 30;
		}
		return daysInMonth;
	}

	public static String AmountInWords(String StrAmt, String RupeName, String PaiseName) {

		String AmtWord = "";
		ToWordsCrore TwC = new ToWordsCrore();

		System.out.println("StrAmt = " + StrAmt);

		int pos = StrAmt.indexOf(".");
		String StrIPart = "";
		String StrDPart = "";
		if (pos > 0) {
			StrIPart = StrAmt.substring(0, pos);
			StrDPart = StrAmt.substring(pos + 1);

		} else {
			StrIPart = StrAmt;
		}

		System.out.println("Int Part =" + StrIPart);
		System.out.println("Deci Part =" + StrDPart);
		if (StrDPart.length() > 2) {
			StrDPart = StrDPart.substring(0, 2);
			System.out.println("Modi Deci Part =" + StrDPart);
		}

		if (StrDPart.length() == 0) {
			StrDPart = "0";
		}

		long iPart = Long.parseLong(StrIPart);

		long fPart = Long.parseLong(StrDPart);

		AmtWord = TwC.convertNumberToWords(iPart);
		System.out.println("in words ipart  =" + AmtWord);

		String Paise;
		if (fPart > 0) {
			String strfp = fPart + "";
			if (StrDPart.length() == 1) {
				fPart = fPart * 10;

			}
			Paise = TwC.convertNumberToWords(fPart);
			AmtWord = AmtWord + " " + RupeName + " And " + Paise + " " + PaiseName;
			System.out.println("in words paise  =" + Paise);

		} else {
			AmtWord = AmtWord + " " + RupeName;
		}
		// System.out.println("in words paise =" + AmtWord);

		return AmtWord;
	}

	public static boolean UserHasRole(String RoleName) {

		for (Iterator itr = user_roles.iterator(); itr.hasNext();) {
			String aRole = (String) itr.next();

			if (RoleName.equalsIgnoreCase(aRole)) {

				return true;
			}
		}

		return false;

	}

	public static ArrayList<String> getUser_roles() {
		/*
		 * Read User Roles from Database here
		 */

		return user_roles;
	}

	public static String getFinancialYear() {
		if (null != financialYear) {
			return financialYear.getFinancialYear();
		} else {
			return null;
		}

	}

	public static String setFinancialYear(FinancialYearMst financialYearP) {
		// FinancialYearMst financialYear =
		// financialYearMstRepository.getCurrentFinancialYear();
		// FinancialYearMst financialYear =
		// financialYearMstRepository.getCurrentFinancialYear();
		financialYear = financialYearP;

		return financialYear.getFinancialYear();
	}


	public static Date getstartOfDay(Date date) {
		Calendar calender = Calendar.getInstance();
		int year = calender.get(Calendar.YEAR);
		int month = calender.get(Calendar.MONTH);
		int day = calender.get(Calendar.DATE);
		calender.set(year, month, day, 0, 0, 0);
		return calender.getTime();

	}
	
	public static Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
	    ByteArrayInputStream in = new ByteArrayInputStream(data);
	    ObjectInputStream is = new ObjectInputStream(in);
	    return is.readObject();
	}
 
	
	public static byte[] objectToByteArray(Object obj) throws IOException {
		 byte[] resultBytes  =null;
		 
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream out = null;
		try {
		  out = new ObjectOutputStream(bos);   
		  out.writeObject(obj);
		  out.flush();
		   resultBytes = bos.toByteArray();
		  
		} finally {
		  try {
		    bos.close();
		  } catch (IOException ex) {
		    // ignore close exception
		  }
		}
		
		return resultBytes;
	}
}
