package com.maple.restserver.his.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;

@Entity
public class LPODtl {
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	private String id;
	@Column(length = 50)
	private String itemId;
	
	private Double qty;
	@Column(length = 50)
	private String itemName;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "lpoMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	LPOMst lpoMst;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public LPOMst getLpoMst() {
		return lpoMst;
	}

	public void setLpoMst(LPOMst lpoMst) {
		this.lpoMst = lpoMst;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	@Override
	public String toString() {
		return "LPODtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", itemName=" + itemName + ", lpoMst="
				+ lpoMst + ", companyMst=" + companyMst + ", getId()=" + getId() + ", getItemId()=" + getItemId()
				+ ", getQty()=" + getQty() + ", getLpoMst()=" + getLpoMst() + ", getCompanyMst()=" + getCompanyMst()
				+ ", getItemName()=" + getItemName() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}


	
}
