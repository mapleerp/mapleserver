package com.maple.restserver.his.entity;


public class PharmacySupplierAndItemReport {
	
	String itemName;
	String itemGenericName;
	String supplierName;
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemGenericName() {
		return itemGenericName;
	}
	public void setItemGenericName(String itemGenericName) {
		this.itemGenericName = itemGenericName;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	
	

}
