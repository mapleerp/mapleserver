package com.maple.restserver.his.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.restserver.entity.CompanyMst;

@Entity

public class BedMst {
	@Id
	@GeneratedValue
	Integer Id;
	public Integer getId() {
		return Id;
	}
	@JsonProperty("UserId")
	String UserId;
	public String getUserId() {
		return UserId;
	}
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String getDeleted() {
		return Deleted;
	}
	public void setDeleted(String deleted) {
		Deleted = deleted;
	}
	@JsonProperty("Deleted")
	String Deleted;
	
	@Override
	public String toString() {
		return "BedMst [Id=" + Id + ", UserId=" + UserId + ", Deleted=" + Deleted + "]";
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
}

