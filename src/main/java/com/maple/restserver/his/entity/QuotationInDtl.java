package com.maple.restserver.his.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;

@Entity
public class QuotationInDtl {
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)

	private String id;
	private Double offerQty;
	private Double mrp;
	private Double ratePerUnit;
	private Double margin;
	@Column(length = 50)
	private String itemId;
	@Column(length = 50)
	private String itemName;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;

	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "quotationInHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	QuotationInHdr quotationInHdr;


	
	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public Double getOfferQty() {
		return offerQty;
	}


	public void setOfferQty(Double offerQty) {
		this.offerQty = offerQty;
	}


	public Double getMrp() {
		return mrp;
	}


	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}


	public Double getRatePerUnit() {
		return ratePerUnit;
	}


	public void setRatePerUnit(Double ratePerUnit) {
		this.ratePerUnit = ratePerUnit;
	}


	public Double getMargin() {
		return margin;
	}


	public void setMargin(Double margin) {
		this.margin = margin;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public QuotationInHdr getQuotationInHdr() {
		return quotationInHdr;
	}


	public void setQuotationInHdr(QuotationInHdr quotationInHdr) {
		this.quotationInHdr = quotationInHdr;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public String getItemId() {
		return itemId;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	@Override
	public String toString() {
		return "QuotationInDtl [id=" + id + ", offerQty=" + offerQty + ", mrp=" + mrp + ", ratePerUnit=" + ratePerUnit
				+ ", margin=" + margin + ", itemId=" + itemId + ", companyMst=" + companyMst + ", quotationInHdr="
				+ quotationInHdr + ", getId()=" + getId() + ", getOfferQty()=" + getOfferQty() + ", getMrp()="
				+ getMrp() + ", getRatePerUnit()=" + getRatePerUnit() + ", getMargin()=" + getMargin()
				+ ", getCompanyMst()=" + getCompanyMst() + ", getQuotationInHdr()=" + getQuotationInHdr()
				+ ", getItemId()=" + getItemId() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}


	
	
}
