package com.maple.restserver.his.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class ProcudereEquipmentScheduler {
	
	@Id
	@GeneratedValue
	Integer id;
	
	@JsonProperty("userId")
	Integer userId;
	
	@JsonProperty("deleted")
	String deleted;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "ProcudereEquipmentScheduler [id=" + id + ", userId=" + userId + ", deleted=" + deleted + "]";
	}
	
	

}
