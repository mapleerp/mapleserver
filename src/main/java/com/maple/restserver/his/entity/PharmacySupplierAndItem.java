package com.maple.restserver.his.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;

@Entity
public class PharmacySupplierAndItem {
	
	@Id
	   @GeneratedValue(generator = "uuid")
	   @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	   private String id;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "accountHeads", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private AccountHeads accountHeads;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "itemMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ItemMst itemMst;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	

	public AccountHeads getAccountHeads() {
		return accountHeads;
	}

	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}

	public ItemMst getItemMst() {
		return itemMst;
	}

	public void setItemMst(ItemMst itemMst) {
		this.itemMst = itemMst;
	}

	@Override
	public String toString() {
		return "PharmacySupplierAndItem [id=" + id + ", companyMst=" + companyMst + ", accountHeads=" + accountHeads
				+ ", itemMst=" + itemMst + "]";
	}
	
	
	

}
