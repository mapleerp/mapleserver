
package com.maple.restserver.his.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity

public class BedTypeMst {
	@Id
	@GeneratedValue
	Integer Id;
	public Integer getId() {
		return Id;
	}
	@JsonProperty("UserId")
	String UserId;
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String getDeleted() {
		return Deleted;
	}
	public void setDeleted(String deleted) {
		Deleted = deleted;
	}
	@JsonProperty("Deleted")
	String Deleted;
	
	@Override
	public String toString() {
		return "BedTypeMst [Id=" + Id + ", UserId=" + UserId + ", Deleted=" + Deleted + "]";
	}
	
}

