package com.maple.restserver.his.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class DoctorMst {
	@Id
	@GeneratedValue
	Integer Id;
	@JsonProperty("UserId")
	String UserId;
	@JsonProperty("Deleted")
	String Deleted;
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String getDeleted() {
		return Deleted;
	}
	public void setDeleted(String deleted) {
		Deleted = deleted;
	}
	@Override
	public String toString() {
		return "DoctorMst [Id=" + Id + ", UserId=" + UserId + ", Deleted=" + Deleted + "]";
	}
	
	
}
