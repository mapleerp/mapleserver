package com.maple.restserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.jamesmurty.utils.XMLBuilder;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.AccountClassfc;
import com.maple.restserver.accounting.entity.AccountMergeMst;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.CreditClassfc;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.DebitClassfc;
import com.maple.restserver.accounting.entity.InventoryLedgerMst;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.entity.LedgerClassfc;
import com.maple.restserver.accounting.entity.LinkedAccounts;
import com.maple.restserver.accounting.entity.OpeningBalanceConfigMst;
import com.maple.restserver.accounting.entity.OpeningBalanceMst;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.AccountClassfcRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.CreditClassfcRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.DebitClassfcRepository;
import com.maple.restserver.accounting.repository.InventoryLedgerMstRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassfcRepository;
import com.maple.restserver.accounting.repository.LinkedAccountsRepository;
import com.maple.restserver.accounting.repository.OpeningBalanceConfigMstRepository;
import com.maple.restserver.accounting.repository.OpeningBalanceMstRepository;
import com.maple.restserver.entity.*;
import com.maple.restserver.jms.send.KafkaMapleEvent;
import com.maple.restserver.jms.send.KafkaMapleEventType;

import com.maple.restserver.message.entity.AcceptStockMessage;
import com.maple.restserver.message.entity.JournalHdrAndDtlMessage;
import com.maple.restserver.message.entity.PaymentHdrAndDtlMessage;
import com.maple.restserver.message.entity.PurchaseMessageEntity;
import com.maple.restserver.message.entity.ReceiptHdrAndDtlMessage;
import com.maple.restserver.message.entity.SalesHdrAndDtlMessageEntity;
import com.maple.restserver.repository.*;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.tally.integration.task.CCommonXML;
import com.maple.restserver.tally.integration.task.SaleIntegrationTally;
import com.maple.restserver.utils.SystemSetting;
import com.maple.restserver.xml.purchase.ALLINVENTORYENTRIESLIST;
import com.maple.restserver.xml.purchase.ENVELOPE;
import com.maple.restserver.xml.purchase.LEDGERENTRIESLIST;
import com.maple.restserver.xml.sales.ALLINVENTORYENTRIES;
import com.maple.restserver.xml.sales.SALESDR_LEDGERENTRIES;
import com.maple.restserver.xml.sales.SALES_ENVELOPE;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import java.io.IOException;

import java.nio.file.Files;

import java.nio.file.Path;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import static java.nio.file.StandardCopyOption.*;

import static java.nio.file.LinkOption.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

@Component
public class MapleTallyConnect {
	private final Logger logger = LoggerFactory.getLogger(MapleTallyConnect.class);

	 

	@Autowired
	SaleIntegrationTally saleIntegrationTally;

	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	@Autowired
	ItemMstRepository itemMstRepository;

	@Autowired
	UnitMstRepository unitMstRepository;

	@Autowired
	CategoryMstRepository categoryMstRepository;

	@Autowired
	FileMover fileMover;

	@Autowired
	ItemMstRepository itemMstRepo;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Autowired
	UserMstRepository userMstRepo;
	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	BranchMstRepository branchMstRepo;
	@Autowired
	PurchaseHdrRepository purchaseHdrRepo;
	/*
	 * @Autowired CustomerMstRepository customerMstRepo;
	 */
	@Autowired
	PurchaseDtlRepository purchaseDtlRepo;
	@Autowired
	UnitMstRepository unitMstRepo;

	@Autowired
	AccountClassRepository accountClassRepo;
	@Autowired
	AccountClassfcRepository accountClassfcRepo;
	@Autowired
	AccountMergeRepository accountMergeRepo;
	@Autowired
	CreditClassRepository creditClassRepo;
	@Autowired
	CreditClassfcRepository creditClassfcRepo;
	@Autowired
	DebitClassRepository debitClassRepo;
	@Autowired
	DebitClassfcRepository debitClassfcRepo;
	@Autowired
	InventoryLedgerMstRepository inventoryLedgerMstRepo;
	@Autowired
	LedgerClassRepository ledgerClassRepo;
	@Autowired
	LedgerClassfcRepository ledgerClassfcRepo;
	@Autowired
	LinkedAccountsRepository linkedAccountsRepo;
	@Autowired
	OpeningBalanceConfigMstRepository openingBalanceConfigMstRepository;
	@Autowired
	OpeningBalanceMstRepository openingBalanceMstRepo;
	@Autowired
	StoreMstRepository storeMstRepo;
	@Autowired
	SalesTypeMstRepository salesTypeMstRepo;
	@Autowired
	SchEligiAttrListDefRepository schEligiAttrListDefRepo;
	@Autowired
	SchEligibilityAttribInstRepository schEligibilityAttribInstRepo;
	@Autowired
	SchEligibilityDefRepository schEligibilityDefRepo;
	@Autowired
	SchemeInstanceRepository schemeInstanceRepo;
	@Autowired
	SchOfferAttrInstRepository schOfferAttrInstRepo;
	@Autowired
	SchOfferAttrListDefRepository schOfferAttrListDefRepo;
	@Autowired
	SchOfferDefRepository schOfferDefRepo;
	@Autowired
	SchSelectAttrListDefReoository schSelectAttrListDefReoo;
	@Autowired
	SchSelectDefRepository schSelectDefRepo;
	@Autowired
	SchSelectionAttribInstRepository schSelectionAttribInstRepo;
	@Autowired
	ServiceInHdrRepository serviceInHdrRepo;
	@Autowired
	ServiceInDtlRepository serviceInDtlRepo;
	@Autowired
	StoreChangeMstRepository storeChangeMstRepo;
	@Autowired
	StoreChangeDtlRepository storeChangeDtlRepo;
	@Autowired
	URSMstRepository uRSMstRepo;
	@Autowired
	VoucherNumberRepository voucherNumberRepo;
	@Autowired
	IntentDtlRepository intentDtlRepo;
	@Autowired
	IntentHdrRepository intentHdrRepo;
	@Autowired
	InvoiceEditEnableRepository invoiceEditEnableRepo;
	@Autowired
	OtherBranchSalesDtlRepository otherBranchSalesDtlRepo;
	@Autowired
	OtherBranchSalesTransHdrRepository otherBranchSalesTransHdrRepo;
	@Autowired
	OtherBranchPurchaseHdrRepository otherBranchPurchaseHdrRepo;
	@Autowired
	OtherBranchPurchaseDtlRepository otherBranchPurchaseDtlRepo;
	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepo;
	@Autowired
	SalesOrderDtlRepository salesOrderDtlRepo;
	@Autowired
	SalesDetailsRepository salesDetailsRepo;
	@Autowired
	SalesReceiptsRepository salesReceiptsRepo;
	@Autowired
	SalesDtlDeletedRepository salesDtlDeletedRepo;

	@Autowired
	CategoryMstRepository categoryMstRepo;
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepo;
	@Autowired
	ItemMergeMstRepository itemMergeMstRepo;
	@Autowired
	PaymentHdrRepository paymentHdrRepo;
	@Autowired
	PaymentDtlRepository paymentDtlRepo;
	@Autowired
	PurchaseSchemeMstRepository purchaseSchemeMstRepo;
	@Autowired
	ReceiptHdrRepository receiptHdrRepo;
	@Autowired
	ReceiptDtlRepository receiptDtlRepo;
	@Autowired
	ReciptModeMstRepository reciptModeMstRepo;
	@Autowired
	ActualProductionHdrRepository actualProductionHdrRepo;
	@Autowired

	ActualProductionDtlRepository actualProductionDtlRepo;
	@Autowired
	StockTransferOutHdrRepository stockTransferOutHdrRepo;
	@Autowired
	StockTransferOutDtlRepository stockTransferOutDtlRepo;
	@Autowired
	StockTransferInHdrRepository stockTransferInHdrRepo;
	@Autowired
	StockTransferInDtlRepository stockTransferInDtlRepo;
	@Autowired
	AccountHeadsRepository accountHeadsRepo;
	@Autowired
	AccountPayableRepository accountPayableRepo;
	@Autowired
	AccountReceivableRepository accountReceivableRepo;
	@Autowired
	AdditionalExpenseRepository additionalExpenseRepo;
	@Autowired
	BatchPriceDefinitionRepository batchPriceDefinitionRepo;
	@Autowired
	BrandMstRepository brandMstRepo;
	@Autowired
	ConsumptionDtlRepository consumptionDtlRepo;
	@Autowired
	ConsumptionReasonMstRepository consumptionReasonMstRepo;
	@Autowired
	DamageHdrRepository damageHdrRepo;
	@Autowired
	DamageDtlRepository damageDtlRepo;
	@Autowired
	DayBookRepository dayBookRepo;
	@Autowired
	DayEndClosureRepository dayEndClosureRepo;
	@Autowired
	DayEndClosureDtlRepository dayEndClosureDtlRepo;
	@Autowired
	DayEndReportStoreRepository dayEndReportStoreRepo;
	@Autowired
	DelivaryBoyMstRepository delivaryBoyMstRepo;
	@Autowired
	GoodReceiveNoteDtlRepository goodReceiveNoteDtlRepo;
	@Autowired
	GoodReceiveNoteHdrRepository goodReceiveNoteHdrRepo;
	@Autowired
	GroupMstRepository groupMstRepo;
	@Autowired
	GroupPermissionRepository groupPermissionRepo;
	@Autowired
	IntentInHdrRepository intentInHdrRepo;
	@Autowired
	IntentInDtlRepository intentInDtlRepo;
	@Autowired
	ItemBatchExpiryDtlRepository itemBatchExpiryDtlRepo;
	@Autowired
	SalesRetunDtlRepository salesRetunDtlRepo;
	@Autowired
	SalesReturnHdrRepository salesReturnHdrRepo;
	@Autowired
	ReorderMstRepository reorderMstRepo;
	@Autowired
	SaleOrderReceiptRepository saleOrderReceiptRepo;
	@Autowired
	ReceiptInvoiceDtlRepository receiptInvoiceDtlRepo;
	@Autowired
	RawMaterialReturnHdrRepository rawMaterialReturnHdrRepo;
	@Autowired
	RawMaterialReturnDtlRepository rawMaterialReturnDtlRepo;
	@Autowired
	RawMaterialIssueDtlRepository rawMaterialIssueDtlRepo;
	@Autowired
	RawMaterialMstRepository rawMaterialMstRepo;
	@Autowired
	PurchaseOrderDtlRepository purchaseOrderDtlRepo;
	@Autowired
	PurchaseOrderHdrRepository purchaseOrderHdrRepo;
	@Autowired
	ProcessMstRepository processMstRepo;
	@Autowired
	ProcessPermissionRepository processPermissionRepo;
	@Autowired
	ProductConfigurationMstRepository productConversionConfigMstRepo;
	@Autowired
	ProductConversionDtlRepository productConversionDtlRepo;
	@Autowired
	ProductMstRepository productMstRepo;
	@Autowired
	ProductConversionMstRepository productConversionMstRepo;
	@Autowired
	PriceDefinitionRepository priceDefinitionRepo;
	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepo;
	@Autowired
	OwnAccountSettlementDtlRepository ownAccountSettlementDtlRepo;
	@Autowired
	OwnAccountSettlementMstRepository ownAccountSettlementMstRepo;
	@Autowired
	PaymentInvoiceDtlRepository paymentInvoiceDtlRepo;
	@Autowired
	OwnAccountRepository ownAccountRepo;
	@Autowired
	OrderTakerMstRepository orderTakerMstRepo;
	@Autowired
	PDCPaymentRepository pDCPaymentRepo;
	@Autowired
	PDCReceiptRepository pDCReceiptRepo;
	@Autowired
	PDCReconcileHdrRepository pDCReconcileHdrRepo;
	@Autowired
	PDCReconcileDtlRepository pDCReconcileDtlRepo;
	@Autowired
	MultiUnitMstRepository multiUnitMstRepo;

	@Autowired
	LocationMstRepository locationMstRepo;
	@Autowired
	LocalCustomerRepository localCustomerRepo;
	@Autowired
	LocalCustomerDtlRepository localCustomerDtlRepo;
	@Autowired
	AddKotTableRepository addKotTableRepo;
	@Autowired
	AddKotWaiterRepository addKotWaiterRepo;
	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepo;
	@Autowired
	KitDefenitionDtlRepository kitDefenitionDtlRepo;
	@Autowired
	JournalHdrRepository journalHdrRepo;
	@Autowired
	JournalDtlRepository journalDtlRepo;
	@Autowired
	JobCardHdrRepository jobCardHdrRepo;
	@Autowired
	JobCardDtlRepository jobCardDtlRepo;

	@Autowired
	VoucherNumberService voucherNumberService;

	@Autowired
	SysDateMstRepository sysDateMstRepository;

	@Value("${mybranch}")
	private String mybranch;
	@Value("${mycompany}")
	private String mycompany;

	@Autowired
	ObjectMapper objectMapper;

	@Value("${tally_outgoing_folder}")
	private String tally_outgoing_folder;

	@Value("${tally_xml_outgoing_folder}")
	private String tally_xml_outgoing_folder;

	@Value("${tally_xml_outgoing_processed_folder}")
	private String tally_xml_outgoing_processed_folder;

	@Value("${tally_outgoing_processed_folder}")
	private String tally_outgoing_processed_folder;

	@Value("${outgoing_processed_archived_folder}")
	private String outgoing_processed_archived_folder;

	@Value("${incoming_folder}")
	private String incoming_folder;

	@Value("${incoming_processed_folder}")
	private String incoming_processed_folder;

	@Value("${incoming_processed_archived_folder}")
	private String incoming_processed_archived_folder;

	 

	private static FileWriter file;
	private String file_sufix = ".dat";
	private String name_differentiator = "$";

	@Value("${tallyServer}")
	private String tallyServer;

	@Value("${TALLY_COMPANY}")
	private String TALLY_COMPANY;

	@Autowired
	CCommonXML cCommonXML;

	public void readAndProcessOutGoingFile() throws IOException, ParseException {

		File theDir = new File(tally_outgoing_processed_folder);
		if (!theDir.exists()) {
			theDir.mkdirs();
		}

		File folder = new File(tally_outgoing_folder);

		for (File file : folder.listFiles()) {
			if (!file.isDirectory()) {

				if (processOutGoingFileToGenerateTallyXml(file.getAbsolutePath())) {

					File to = new File(tally_outgoing_processed_folder + File.separatorChar + file.getName());

					boolean fileCopied = false;

					fileCopied = copyFileToProcessed(file, to);

					if (fileCopied) {

						file.delete();

					}

				}

			}
		}

		return;
	}

	public void readAndProcessOutGoingTallyXmlFile() throws IOException, ParseException, UnsupportedOperationException,
			SQLException, ParserConfigurationException, SAXException {

		File theDir = new File(tally_xml_outgoing_processed_folder);
		if (!theDir.exists()) {
			theDir.mkdirs();
		}

		File folder = new File(tally_xml_outgoing_folder);

		for (File file : folder.listFiles()) {
			if (!file.isDirectory()) {

				if (processOutgoingTallyXmlFile(file.getAbsolutePath())) {

					File to = new File(tally_xml_outgoing_processed_folder + File.separatorChar + file.getName());

					boolean fileCopied = false;

					fileCopied = copyFileToProcessed(file, to);

					if (fileCopied) {

						file.delete();

					}

				}

			}
		}

		return;
	}

	private boolean copyFileToProcessed(File src, File dest) {
		try {

			Path bytes = Files.copy(

					new java.io.File(src.getAbsolutePath()).toPath(),

					new java.io.File(dest.getAbsolutePath()).toPath(),

					REPLACE_EXISTING,

					COPY_ATTRIBUTES,

					NOFOLLOW_LINKS);

			System.out.println("File successfully copied using Java 7 way");

		} catch (IOException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();
			return false;

		}
		return true;

	}

	private boolean processOutGoingFileToGenerateTallyXml(String fileName) throws IOException, ParseException {
		JSONParser parser = new JSONParser();

		 
		FileReader fr = new FileReader(fileName);
		Object obj = parser.parse(fr);

		fr.close();

		JSONObject jsonObject = (JSONObject) obj;

		String libraryEventType = (String) jsonObject.get("libraryEventType");
		String payLoad = (String) jsonObject.get("payLoad");
		String desination = (String) jsonObject.get("destination");
		String voucherNumber = (String) jsonObject.get("voucherNumber");
		String source = (String) jsonObject.get("source");
		String objectType = (String) jsonObject.get("objectType");
		Long libraryEventId = (Long) jsonObject.get("libraryEventId");
		Long hashCodeInFile = (Long) jsonObject.get("hashCode");

		int hashCodeComputed = payLoad.hashCode();
		if (hashCodeInFile != hashCodeComputed) {
			logger.error("Data file tampered. Not processing");
			return false;
		}
		;
		ObjectMapper mapper = new ObjectMapper().registerModule(new ParameterNamesModule())
				.registerModule(new Jdk8Module()).registerModule(new JavaTimeModule());

		KafkaMapleEventType kafkaMapleEventType = KafkaMapleEventType.valueOf(libraryEventType);

		switch (kafkaMapleEventType) {
		case SALESTRANSDTLARRAY:

			try {

				SalesHdrAndDtlMessageEntity salesHdrAndDtlMessageEntity = mapper.readValue(payLoad,
						SalesHdrAndDtlMessageEntity.class);

				SalesTransHdr salesTransHdr = salesHdrAndDtlMessageEntity.getSalesTransHdr();

				//companyMst = salesTransHdr.getCompanyMst();

				logger.info("SalesTransHdr : {} ", salesTransHdr);
				publishSalesToTally(salesTransHdr);

			} catch (IOException e) {

				e.printStackTrace();
				return false;
			}

			break;

		case PURCHASEDTLARRAY:

			PurchaseMessageEntity purchaseMessageEntity = mapper.readValue(payLoad, PurchaseMessageEntity.class);

			PurchaseHdr purchaseHdr = new PurchaseHdr();

			purchaseHdr = purchaseMessageEntity.getPurchaseHdr();
			
			//companyMst = purchaseHdr.getCompanyMst();

			publishPurchaseToTallyStageOne(purchaseHdr);

			break;

		case RECEIPTHDRANDDTL:

			ReceiptHdrAndDtlMessage ReceiptHdrAndDtlMessage = mapper.readValue(payLoad, ReceiptHdrAndDtlMessage.class);
			ReceiptHdr receiptHdr = ReceiptHdrAndDtlMessage.getReceiptHdr();
			//companyMst = receiptHdr.getCompanyMst();
			
			publishReceiptsToTallyStageOne(receiptHdr);
			break;

		case PAYMENTHDRANDDTL:

			PaymentHdrAndDtlMessage paymentHdrAndDtlMessage = mapper.readValue(payLoad, PaymentHdrAndDtlMessage.class);
			PaymentHdr paymentHdr = paymentHdrAndDtlMessage.getPaymentHdr();

			//companyMst = paymentHdr.getCompanyMst();
			publishPaymentsToTallyStageOne(paymentHdr);
			break;
		case JOURNALHDRANDDTL:
			JournalHdrAndDtlMessage journalHdrAndDtlMessage = mapper.readValue(payLoad, JournalHdrAndDtlMessage.class);
			JournalHdr journalHdr = journalHdrAndDtlMessage.getJournalHdr();

			//companyMst = journalHdr.getCompanyMst();
			publishJournalToTallySatageOne(journalHdr);
			break;
		}

		return true;

	}

	private boolean processOutgoingTallyXmlFile(String fileName) throws SQLException, ClientProtocolException,
			IOException, ParserConfigurationException, UnsupportedOperationException, SAXException {
		String VoucherId = "";

		String ErrorCode = "";
		String Linerror = "";
		boolean hasJournal = false;
		boolean JournalCompleted = true;

		final HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
		// httpParams
		HttpClient httpclient = new DefaultHttpClient(httpParams);

		HttpPost httppost = new HttpPost(tallyServer);
		File file = new File(fileName);
		FileEntity entity = new FileEntity(file, "text/plain; charset=\"UTF-8\""); // ,

		httppost.setEntity(entity);
		System.out.println("executing request " + httppost.getRequestLine());
		HttpResponse response = httpclient.execute(httppost);
		HttpEntity resEntity = response.getEntity();
		System.out.println("----------------------------------------");
		System.out.println(response.getStatusLine());
		if (resEntity != null) {

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(resEntity.getContent());

			NodeList nList = doc.getElementsByTagName("RESPONSE");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;

					try {
						VoucherId = eElement.getElementsByTagName("LASTVCHID").item(0).getTextContent();
					} catch (Exception e) {
						return true;
					}
					System.out.println("Response VoucherId =  " + VoucherId);

					ErrorCode = eElement.getElementsByTagName("ERRORS").item(0).getTextContent();

					if (ErrorCode.equalsIgnoreCase("0")) {

						if (hasJournal && !JournalCompleted) {
							return false;
						} else if (!hasJournal) {

							return true;
						}
					}
					System.out.println("ErrorCode =  " + ErrorCode);

					Linerror = eElement.getElementsByTagName("LINEERROR").item(0).getTextContent();
					System.out.println("Linerror =  " + Linerror);

					if (Linerror.contains("Voucher Number") && Linerror.contains("already exists!")) {

						System.out.println(Linerror);
						return true;

					}
					processReturnMain(Linerror,   tallyServer, TALLY_COMPANY, "Sales");

					if (ErrorCode.equalsIgnoreCase("1")) {

						return false;
					}

				}

			}

		}

		return true;

	}

	private boolean publishSalesToTally(SalesTransHdr salesTransHdr) {
		BigDecimal zero = new BigDecimal("0");

		String voucherNumber = salesTransHdr.getVoucherNumber();
		Date voucherDate = salesTransHdr.getVoucherDate();

		boolean result = false;

		System.out.println("VOUCHER NUBMER =  " + voucherNumber);
		System.out.println("VOUCHER DATE =  " + voucherDate);

		String sourvceId = salesTransHdr.getId();
		// String lmsqueueId = jobInfo.getId();

		String id = salesTransHdr.getId();

		String companyMst = salesTransHdr.getCompanyMst().getId();

		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		String branchMst = salesTransHdr.getBranchCode();

		String goDownName = branchMst;

		String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

		System.out.println("Company ID " + companyMst);

		List<AccountClass> accountClassPreviousList = accountClassRepo
				.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

		if (accountClassPreviousList.size() > 0) {
			logger.info("Found previous records.. Deleting");

			Iterator iter = accountClassPreviousList.iterator();
			while (iter.hasNext()) {
				AccountClass accountClass = (AccountClass) iter.next();
				logger.info("Found previous records.. " + accountClass.getSourceVoucherNumber()
						+ accountClass.getBrachCode());

				List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
				List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

				List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

				if (creditClassList.size() > 0) {

					try {
						result = generateTallyXmlForSales(accountClass.getCompanyMst(), salesTransHdr.getBranchCode(),
								"GODOWN", salesTransHdr.getSalesMode(), voucherNumber, strVoucherDate,
								salesTransHdr.getAccountHeads(), debitClassList, creditClassList);

					} catch (SQLException e) {

						return false;

					}
				}

			}
		}
		return result;

	}

	private void publishPurchaseToTallyStageOne(PurchaseHdr purchaseHdr) {
		BigDecimal zero = new BigDecimal("0");

		String voucherNumber = purchaseHdr.getVoucherNumber();
		Date voucherDate = purchaseHdr.getVoucherDate();

		String sourvceId = purchaseHdr.getId();
		// String lmsqueueId = jobInfo.getId();

		String id = purchaseHdr.getId();

		String companyMst = purchaseHdr.getCompanyMst().getId();
		// (String) context.getJobDetail().getJobDataMap().get("companyid");

		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		String branchMst = purchaseHdr.getBranchCode();

		String goDownName = branchMst;

		String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

		System.out.println("Company ID " + companyMst);

		List<AccountClass> accountClassPreviousList = accountClassRepo
				.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

		if (accountClassPreviousList.size() > 0) {
			logger.info("Found previous records.. Deleting");

			Iterator iter = accountClassPreviousList.iterator();
			while (iter.hasNext()) {
				AccountClass accountClass = (AccountClass) iter.next();
				logger.info("Found previous records.. Deleting" + accountClass.getSourceVoucherNumber()
						+ accountClass.getBrachCode());

				List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
				List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

				List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

				if (creditClassList.size() > 0) {

					try {
						boolean result = generateTallyXmlForPurchase(purchaseHdr.getCompanyMst(), tallyServer,
								purchaseHdr.getBranchCode(), voucherNumber, strVoucherDate,
								purchaseHdr.getSupplierInvDate(), purchaseHdr.getSupplierInvNo(),
								purchaseHdr.getSupplierId(), debitClassList, creditClassList, "PURCHASE");

					} catch (SQLException e) {
						// TODO Auto-generated catch block
						logger.error(e.getMessage());
					}
				}

			}
		}

	}

	private boolean generateTallyXmlForSales(CompanyMst companyMst, String costCenter, String Godown,
			String tallyVoucherType, String voucherNumber, String invoiceDate, AccountHeads ach,
			List<DebitClass> debitList, List<CreditClass> creditList) throws SQLException {

		File theDir = new File(tally_xml_outgoing_folder);
		if (!theDir.exists()) {
			theDir.mkdirs();
		}

		boolean hasJournal = false;
		boolean JournalCompleted = true;

		String VoucherId = "";
		String ErrorCode = "";
		String Linerror = "";

		invoiceDate = invoiceDate.replace("-", "");

		SALES_ENVELOPE env = new SALES_ENVELOPE();

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setNatureofsales("Domestic");

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setVCHTYPE(tallyVoucherType);
		// env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setClassname("VAT");

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setVouchertypename(tallyVoucherType);

		if (voucherNumber.length() > 15) {

			voucherNumber = voucherNumber.replaceAll("2019-2020", "19-20");
			voucherNumber = voucherNumber.replaceAll("2020-2021", "20-21");
			voucherNumber = voucherNumber.replaceAll("2021-2022", "21-22");

		}
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setVouchernumber(voucherNumber);

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setFbtpaymenttype("Default");

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher()
				.setPersistedview("Invoice Voucher View");

		env.getBody().getImpdate().getSrequestdec().setReportname("Vouchers");
		env.getBody().getImpdate().getSrequestdec().getStaticvar().setSvcurrentcompany(TALLY_COMPANY);

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setExciseopening("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setUseforfinalproduction("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIscancelled("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setHascashflow("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIspostdated("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setUsetrackingnumber("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIsinvoice("Yes");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setMfjournal("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setHasdiscounts("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setAspayslip("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIscostcenter("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIsstxnonrealizedvch("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIsdeleted("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setAsorginal("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setVchfromsync("No");

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getBasicbuyeraddresslist()
				.setBASICBUYERADDRESS(ach.getAccountName());
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getBasicbuyeraddresslist()
				.setBASICBUYERADDRESS2(ach.getAccountName());
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIsvatoverridden("Yes");

		boolean CustomerFound = true;
		boolean InsuranceFound = false;
		String DebitAccountName = "";
		String tempDebitAccountName = "";
		String InsuranceDebitAccountName = "";
		String CustomerDebitAccountName = "";

		BigDecimal bInsuranceAmount = new BigDecimal("0");

		String Remark = "";

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher()
				.setPartyledgername(ach.getAccountName());
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setPartyname(ach.getAccountName());
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setBasicbuyername(ach.getAccountName());

		for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			DebitClass debitclass = (DebitClass) itr.next();

			BigDecimal bdDrAmount = new BigDecimal("0");

			bdDrAmount = debitclass.getDrAmount();

			BigDecimal crediutAmt = getCreditTotal(creditList);

			BigDecimal diff = bdDrAmount.subtract(crediutAmt);

			Remark = debitclass.getRemark();

			String drAccountID = debitclass.getAccountId();

			AccountHeads accountHeadsDR = accountHeadsRepository.findByIdAndCompanyMstId(drAccountID,
					companyMst.getId());

			BigDecimal bdRDrAmount = bdDrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			env.getHdr().setTallyrequest("Import Data");

			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setDate(invoiceDate);
			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setNarration(Remark);
			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher()
					.setBasicdatetimeofinvoice(invoiceDate);
			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher()
					.setBasicdatetimeofremoval(invoiceDate);

			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setEnteredby("125");
			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setDiffactualqty("No");

			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setEffectivedate(invoiceDate);

			SALESDR_LEDGERENTRIES sl = new SALESDR_LEDGERENTRIES();

			sl.getSalesoldauditentry().setOLDAUDITENTRYIDS("-1");

			sl.setLEDGERNAME(accountHeadsDR.getAccountName());
			// sl.setISDEEMEDPOSITIVE("Yes");

			sl.setLEDGERFROMITEM("No");
			sl.setREMOVEZEROENTRIES("No");
			sl.setISPARTYLEDGER("Yes");

			if (bdDrAmount.doubleValue() < 0) {
				sl.setISLASTDEEMEDPOSITIVE("No");
				sl.setAMOUNT(bdRDrAmount.abs().toPlainString());
			} else {
				sl.setISLASTDEEMEDPOSITIVE("Yes");
				sl.setAMOUNT("-" + bdRDrAmount.toPlainString());
			}

			sl.getSalesbillalloc().setNAME(voucherNumber);
			sl.getSalesbillalloc().setBILLTYPE("New Ref");
			sl.getSalesbillalloc().setTDSDEDUCTEEISSPECIALRATE("No");
			sl.getSalesbillalloc().setAMOUNT("-" + bdRDrAmount.toPlainString());

			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getLedgerentries().add(sl);
		}

		if (!CustomerFound) {
			throw new SQLException("Error CUSTOMER_NOT_FOUND ");
		}

		for (Iterator citr = creditList.iterator(); citr.hasNext();) {
			CreditClass creditclass = (CreditClass) citr.next();

			AccountHeads accountHeadsCR = accountHeadsRepository.findByIdAndCompanyMstId(creditclass.getAccountId(),
					companyMst.getId());

			String creditAccountName = accountHeadsCR.getAccountName();
			if (null != creditclass.getItemId() && creditclass.getItemId().length() > 0) {

				manageInventoryItem(creditclass, creditAccountName, costCenter, env);

			} else {

				SALESDR_LEDGERENTRIES sl12 = new SALESDR_LEDGERENTRIES();

				sl12.setLEDGERNAME(creditAccountName);

				sl12.setISDEEMEDPOSITIVE("No");
				sl12.setLEDGERFROMITEM("No");

				BigDecimal bdTaxAmt = creditclass.getCrAmount();

				BigDecimal bdRTaxAmt = bdTaxAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				sl12.setAMOUNT(bdRTaxAmt.toPlainString());

				env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getLedgerentries().add(sl12);

			}
		}

		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(SALES_ENVELOPE.class);

			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			String fileName = tally_xml_outgoing_folder + File.separatorChar + voucherNumber + invoiceDate + ".xml";
			jaxbMarshaller.marshal(env, new File(fileName));
			jaxbMarshaller.marshal(env, System.out);
		} catch (Exception e) {
			return false;
		}
		return true;

	}

	private boolean manageInventoryItem(CreditClass creditclass, String creditAccountName, String CostCenter,
			SALES_ENVELOPE env) {
		ItemMst item = itemMstRepository.findById(creditclass.getItemId()).get();

		String unitid = item.getUnitId();

		UnitMst unitMst = unitMstRepository.findById(unitid).get();

		String unitname = unitMst.getUnitName();

		BigDecimal bdRate = new BigDecimal(creditclass.getRate());
		BigDecimal bdRRate = bdRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		String RatePerUnit = bdRRate.toPlainString() + "/" + unitname;
		BigDecimal bdQty = new BigDecimal(creditclass.getQty());
		BigDecimal bdRQty = bdQty.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		String QTYandUnit = bdRQty.toPlainString() + " " + unitname;

		BigDecimal bdAssessibleValue = creditclass.getCrAmount();
		BigDecimal bdRAssessibleValue = bdAssessibleValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		BigDecimal QTYINTORATE = bdRQty.multiply(bdRRate);
		BigDecimal TaxRate = new BigDecimal(item.getTaxRate());

		TaxRate = TaxRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		BigDecimal TAXINTOAMOUNT = QTYINTORATE.multiply(TaxRate);
		BigDecimal Hundred = new BigDecimal("100");
		BigDecimal bdVatAmount = TAXINTOAMOUNT.divide(Hundred);
		bdVatAmount = bdVatAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		ALLINVENTORYENTRIES alinv = new ALLINVENTORYENTRIES();

		alinv.setISDEEMEDPOSITIVE("No");
		alinv.setISLASTDEEMEDPOSITIVE("No");
		alinv.setISAUTONEGATE("No");
		alinv.setRATE(RatePerUnit);
		alinv.setAMOUNT(bdRAssessibleValue.toPlainString());
		alinv.setACTUALQTY(QTYandUnit);
		alinv.setBILLEDQTY(QTYandUnit);

		alinv.getBatchAllocation().setGODOWNNAME(CostCenter);
		// alinv.getBatchAllocation().setBATCHNAME("Primary Batch");
		alinv.getBatchAllocation().setAMOUNT(bdRAssessibleValue.toPlainString());
		alinv.getBatchAllocation().setACTUALQUANTITY(QTYandUnit);
		alinv.getBatchAllocation().setBILLEDQUANTITY(QTYandUnit);
		// alinv.getAccountingAllocation().setTAXCLASSIFICATIONNAME("Output VAT@5%");

		alinv.getAccountingAllocation().setLEDGERNAME(creditAccountName);

		alinv.getAccountingAllocation().setCLASSRATE("0.00000");
		alinv.getAccountingAllocation().setISDEEMEDPOSITIVE("No");
		alinv.getAccountingAllocation().setLEDGERFROMITEM("No");
		alinv.getAccountingAllocation().setREMOVEZEROENTRIES("No");
		alinv.getAccountingAllocation().setISPARTYLEDGER("No");
		alinv.getAccountingAllocation().setISLASTDEEMEDPOSITIVE("No");
		alinv.getAccountingAllocation().setAMOUNT(bdRAssessibleValue.toPlainString());
		alinv.getAccountingAllocation().getCatagoryallocation().setCATEGORY("Primary Cost Category");
		alinv.getAccountingAllocation().getCatagoryallocation().setISDEEMEDPOSITIVE("No");
		// alinv.getAccountingAllocation().getCatagoryallocation().getCostcenterallocation().setNAME(CostCenter);
		// alinv.getAccountingAllocation().getCatagoryallocation().getCostcenterallocation()
		// .setAMOUNT(bdRAssessibleValue.toPlainString());

		alinv.setSTOCKITEMNAME(item.getItemName());
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getAllinventoryList().add(alinv);

		if (bdVatAmount.doubleValue() > 0) {
			alinv.getAccountingAllocation().setSTATNATURENAME("Sales Taxable");
			alinv.getAccountingAllocation().setVATTAXRATE(TaxRate.toPlainString());
		} else {
			alinv.getAccountingAllocation().setSTATNATURENAME("Sales Exempt");
		}
		return true;
	}

	public BigDecimal getCreditTotal(List<CreditClass> creditList) {
		double roundOff = 0.0;
		BigDecimal bdAssessibleValue = new BigDecimal("0");

		for (Iterator citr = creditList.iterator(); citr.hasNext();) {
			CreditClass creditclass = (CreditClass) citr.next();

			BigDecimal bdTaxAmt = creditclass.getCrAmount();
			bdAssessibleValue = bdAssessibleValue.add(bdTaxAmt);

		}

		return bdAssessibleValue;
	}

	public BigDecimal getDebitTotal(List<DebitClass> debitList) {
		double roundOff = 0.0;
		BigDecimal bdAssessibleValue = new BigDecimal("0");

		for (Iterator citr = debitList.iterator(); citr.hasNext();) {
			DebitClass debitclass = (DebitClass) citr.next();

			if (null != debitclass.getDrAmount()) {

				bdAssessibleValue = bdAssessibleValue.add(debitclass.getDrAmount());
			}

		}

		return bdAssessibleValue;
	}

	public boolean generateTallyXmlForPurchase(CompanyMst companyMst, String TallyServer, String branchCode,
			String voucherNumber, String invoiceDate, Date SupplierInvoiceDate, String SupplierInvoice,
			String supplierId, List<DebitClass> debitList, List<CreditClass> creditList, String voucherType)
			throws SQLException {

		String ErrorCode = "";
		String Linerror = "";

		String VoucherId = "";

		String CostCenter = branchCode;
		String Godown = "GODOWN";

		invoiceDate = invoiceDate.replace("-", "");

		String SupinvoiceDate = ClientSystemSetting.SqlDateTostringYYMMDD(SupplierInvoiceDate);

		SupinvoiceDate = SupinvoiceDate.replace("-", "");

		ENVELOPE env = new ENVELOPE();

		env.getHEADER().setTALLYREQUEST("Import Data");

		env.getBODY().getIMPORTDATA().getREQUESTDESC().setREPORTNAME("Vouchers");
		env.getBODY().getIMPORTDATA().getREQUESTDESC().getSTATICVARIABLES().setSVCURRENTCOMPANY(TALLY_COMPANY);
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setACTION("Create");
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER()
				.setOBJVIEW("Invoice Voucher View");

		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER()
				.setPERSISTEDVIEW("Invoice Voucher View");
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().getOLDAUDITENTRYIDSLIST()
				.setOLDAUDITENTRYIDS("-1");
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setDATE(invoiceDate);
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setREFERENCEDATE(SupinvoiceDate);
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setREFERENCE(SupplierInvoice);
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setEFFECTIVEDATE(invoiceDate);

		BigDecimal bdCrAmount = new BigDecimal("0");

		String CreditAccountName = "";
		boolean FoundSupplier = false;
		for (Iterator itr = creditList.iterator(); itr.hasNext();) {
			CreditClass creditclass = (CreditClass) itr.next();

			creditclass.setCrAmount(creditclass.getCrAmount().setScale(2, BigDecimal.ROUND_HALF_EVEN));

			bdCrAmount = bdCrAmount.add(creditclass.getCrAmount());
		}

		BigDecimal bdRCrAmount = bdCrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		BigDecimal bdDrAmount = new BigDecimal("0");

		for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			DebitClass debitclass = (DebitClass) itr.next();

			debitclass.setDrAmount(debitclass.getDrAmount().setScale(2, BigDecimal.ROUND_HALF_EVEN));

			bdDrAmount = bdDrAmount.add(debitclass.getDrAmount());
		}

		BigDecimal bd = bdRCrAmount.subtract(bdDrAmount);
		if (bd.doubleValue() > 0) {
			bdRCrAmount = bdCrAmount.subtract(bd);
		}

		if (bd.doubleValue() < 0) {
			bdRCrAmount = bdCrAmount.add(bd.abs());
		}

		bdRCrAmount = bdRCrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		AccountHeads sup = accountHeadsRepo.findById(supplierId).get();

		if (null != sup) {
			FoundSupplier = true;
		}

		if (!FoundSupplier) {

			throw new SQLException("Supplier Not Found");

		}
		CreditAccountName = sup.getAccountName();

		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER()
				.setSTATENAME(sup.getCustomerState());

		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER()
				.setBASICBUYERSSALESTAXNO(sup.getPartyGst());

		// BigDecimal bdCrAmount = creditclass.getCrAmount();

		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setPARTYNAME(CreditAccountName);
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setVOUCHERTYPENAME(voucherType);

		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setVOUCHERNUMBER(voucherNumber);

		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER()
				.setPARTYLEDGERNAME(CreditAccountName);
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER()
				.setBASICBASEPARTYNAME(CreditAccountName);
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setISINVOICE("Yes");
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setISVATOVERRIDDEN("Yes");
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER()
				.setBASICDATETIMEOFINVOICE(invoiceDate);

		LEDGERENTRIESLIST sl = new LEDGERENTRIESLIST();
		sl.getOLDAUDITENTRYIDSLIST().setOLDAUDITENTRYIDS("1");
		sl.setLEDGERNAME(CreditAccountName);
		sl.setISDEEMEDPOSITIVE("No");
		sl.setLEDGERFROMITEM("No");

		sl.setAMOUNT(bdRCrAmount.toPlainString());

		sl.getBILLALLOCATIONSLIST().setNAME(SupplierInvoice);
		sl.getBILLALLOCATIONSLIST().setBILLTYPE("New Ref");
		sl.getBILLALLOCATIONSLIST().setTDSDEDUCTEEISSPECIALRATE("No");
		sl.getBILLALLOCATIONSLIST().setAMOUNT(bdRCrAmount.toPlainString());

		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().getLEDGERENTRIESLIST().add(sl);

		for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			DebitClass debitclass = (DebitClass) itr.next();

			AccountHeads accountHeadsCR = accountHeadsRepository.findByIdAndCompanyMstId(debitclass.getAccountId(),
					companyMst.getId());
			String AccountName = accountHeadsCR.getAccountName();

			if ((AccountName.contains("CGST")) || (AccountName.contains("SGST")) || (AccountName.contains("IGST"))) {

				LEDGERENTRIESLIST sl12 = new LEDGERENTRIESLIST();
				sl12.getOLDAUDITENTRYIDSLIST().setOLDAUDITENTRYIDS("1");
				sl12.setLEDGERNAME(AccountName);
				sl12.setISDEEMEDPOSITIVE("Yes");
				sl12.setLEDGERFROMITEM("No");

				BigDecimal bdTaxAmt = debitclass.getDrAmount();

				BigDecimal bdRTaxAmt = bdTaxAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				String MinusBdRTaxAmt = "-" + bdRTaxAmt.toPlainString();

				sl12.setAMOUNT(MinusBdRTaxAmt);

				env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().getLEDGERENTRIESLIST()
						.add(sl12);
			} else if (null != debitclass.getItemId() && debitclass.getItemId().length() > 0) {

				ItemMst item = itemMstRepository.findById(debitclass.getItemId()).get();

				String unitid = item.getUnitId();
				UnitMst unit = unitMstRepository.findById(unitid).get();
				// APlicationWindow.cSqlFunctions.getUnitNameFormID(unitid,
				// APlicationWindow.LocalConn);
				BigDecimal bdRate = new BigDecimal(debitclass.getRate());
				BigDecimal bdRRate = bdRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				String RatePerUnit = bdRRate.toPlainString() + "/" + unit.getUnitName();

				BigDecimal bdQty = new BigDecimal(debitclass.getQty());
				BigDecimal bdRQty = bdQty.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				String QTYandUnit = bdRQty.toPlainString() + " " + unit.getUnitName();
				BigDecimal QTYINTORATE = bdRQty.multiply(bdRRate);
				BigDecimal TaxRate = new BigDecimal(item.getTaxRate());

				TaxRate = TaxRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				BigDecimal TAXINTOAMOUNT = QTYINTORATE.multiply(TaxRate);
				BigDecimal Hundred = new BigDecimal("100");
				BigDecimal bdVatAmount = TAXINTOAMOUNT.divide(Hundred);
				bdVatAmount = bdVatAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				String MinusVatAcceptedTaxAmt = "-" + bdVatAmount.toPlainString();

				BigDecimal bdAssessibleValue = debitclass.getDrAmount();
				BigDecimal bdRAssessibleValue = bdAssessibleValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				String MinusBdrAssessibleValue = "-" + bdRAssessibleValue.toPlainString();

				ALLINVENTORYENTRIESLIST ae1 = new ALLINVENTORYENTRIESLIST();
				ae1.setSTOCKITEMNAME(item.getItemName());
				ae1.setISDEEMEDPOSITIVE("Yes");
				ae1.setISAUTONEGATE("No");
				ae1.setRATE(RatePerUnit);

				ae1.setAMOUNT(MinusBdrAssessibleValue);
				ae1.setACTUALQTY(QTYandUnit);
				ae1.setBILLEDQTY(QTYandUnit);
				ae1.getBATCHALLOCATIONSLIST().setGODOWNNAME(Godown);
				ae1.getBATCHALLOCATIONSLIST().setBATCHNAME("Primary Batch");
				ae1.getBATCHALLOCATIONSLIST().setAMOUNT(MinusBdrAssessibleValue);
				ae1.getBATCHALLOCATIONSLIST().setACTUALQTY(QTYandUnit);
				ae1.getBATCHALLOCATIONSLIST().setBILLEDQTY(QTYandUnit);
				ae1.getACCOUNTINGALLOCATIONSLIST().getOLDAUDITENTRYIDSLIST().setOLDAUDITENTRYIDS("-1");
				ae1.getACCOUNTINGALLOCATIONSLIST().setLEDGERNAME(AccountName);
				if (bdVatAmount.doubleValue() > 0) {
					ae1.getACCOUNTINGALLOCATIONSLIST().setSTATNATURENAME("Purchase Taxable");
					ae1.getACCOUNTINGALLOCATIONSLIST().setVATTAXRATE(TaxRate.toPlainString());
				} else {
					ae1.getACCOUNTINGALLOCATIONSLIST().setSTATNATURENAME("Purchase Exempt");
				}

				ae1.getACCOUNTINGALLOCATIONSLIST().setISLASTDEEMEDPOSITIVE("yes");
				ae1.getACCOUNTINGALLOCATIONSLIST().setLEDGERFROMITEM("No");
				ae1.getACCOUNTINGALLOCATIONSLIST().setISPARTYLEDGER("No");
				ae1.getACCOUNTINGALLOCATIONSLIST().setAMOUNT(MinusBdrAssessibleValue);
				ae1.getACCOUNTINGALLOCATIONSLIST().setVATACCEPTEDTAXAMT(MinusVatAcceptedTaxAmt);

				env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER()
						.getALLINVENTORYENTRIESLIST().add(ae1);

			} else {

				LEDGERENTRIESLIST sl12 = new LEDGERENTRIESLIST();
				sl12.getOLDAUDITENTRYIDSLIST().setOLDAUDITENTRYIDS("1");
				sl12.setLEDGERNAME(AccountName);
				sl12.setISDEEMEDPOSITIVE("Yes");
				sl12.setLEDGERFROMITEM("No");

				BigDecimal bdTaxAmt = debitclass.getDrAmount();

				BigDecimal bdRTaxAmt = bdTaxAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				String MinusBdRTaxAmt = "-" + bdRTaxAmt.toPlainString();

				sl12.setAMOUNT(MinusBdRTaxAmt);

				env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().getLEDGERENTRIESLIST()
						.add(sl12);
			}

		}

		/* init jaxb marshaler */
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(ENVELOPE.class);

			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			String fileName = tally_xml_outgoing_folder + File.separatorChar + voucherNumber + invoiceDate + ".xml";

			jaxbMarshaller.marshal(env, new File(fileName));
			jaxbMarshaller.marshal(env, System.out);

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return true;

	}

	private void publishReceiptsToTallyStageOne(ReceiptHdr receiptHdr) {
		BigDecimal zero = new BigDecimal("0");

		String voucherNumber = receiptHdr.getVoucherNumber();
		Date voucherDate = receiptHdr.getVoucherDate();

		String sourvceId = receiptHdr.getId();
		// String lmsqueueId = jobInfo.getId();

		String id = receiptHdr.getId();

		String companyMst = receiptHdr.getCompanyMst().getId();
		// (String) context.getJobDetail().getJobDataMap().get("companyid");

		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		String branchMst = receiptHdr.getBranchCode();

		String goDownName = branchMst;

		String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

		System.out.println("Company ID " + companyMst);

		List<AccountClass> accountClassPreviousList = accountClassRepo
				.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

		if (accountClassPreviousList.size() > 0) {
			logger.info("Found previous records.. Deleting");

			Iterator iter = accountClassPreviousList.iterator();
			while (iter.hasNext()) {
				AccountClass accountClass = (AccountClass) iter.next();
				logger.info("Found previous records.. Deleting" + accountClass.getSourceVoucherNumber()
						+ accountClass.getBrachCode());

				List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
				List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

				List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

				if (creditClassList.size() > 0) {

					try {

						boolean result = generateReceiptToTallyXml(accountClass.getCompanyMst(), branchMst, "RECEIPT",
								branchMst, voucherNumber, strVoucherDate, debitClassList, creditClassList, tallyServer);

					} catch (TransformerException | IOException | ParserConfigurationException
							| FactoryConfigurationError e) {
						// TODO Auto-generated catch block
						logger.error(e.getMessage());
					}
				}

			}
		}

	}

	public boolean generateReceiptToTallyXml(CompanyMst companyMst, String CostCenter, String tallyVoucherType,
			String Godown, String voucherNumber, String invoiceDate, List<DebitClass> debitList,
			List<CreditClass> creditList, String tallyServer) throws TransformerException, ClientProtocolException,
			IOException, ParserConfigurationException, FactoryConfigurationError {

		String VoucherId = "";
		String ErrorCode = "";
		String Linerror = "";

		invoiceDate = invoiceDate.replace("-", "");

		// String VoucherType = APlicationWindow.MachineName+"_RECEIPTS" ;

		String ItemLoopXML = "";
		String ItemXML = "";
		XMLBuilder TAXOBJL = null;
		XMLBuilder LEDGERENTRYL2 = null;
		String AccountName = "";
		String remark = "";
		BigDecimal bdCrAmount = new BigDecimal("0");
		BigDecimal bdRCrAmount = new BigDecimal("0");

		for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			DebitClass debitclass = (DebitClass) itr.next();

			String drAccountID = debitclass.getAccountId();

			AccountHeads accountHeadsDR = accountHeadsRepository.findByIdAndCompanyMstId(drAccountID,
					companyMst.getId());

			remark = debitclass.getRemark();
			bdCrAmount = debitclass.getDrAmount();
			bdRCrAmount = bdCrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		}

		XMLBuilder en = XMLBuilder.create("ENVELOPE");
		XMLBuilder hd = en.element("HEADER");
		XMLBuilder tr = hd.element("TALLYREQUEST");
		tr.t("ImportData");

		XMLBuilder bd = en.element("BODY");
		XMLBuilder id = bd.element("IMPORTDATA");
		XMLBuilder rd = id.element("REQUESTDESC");
		XMLBuilder rn = rd.element("REPORTNAME");
		rn.t("Vouchers");

		XMLBuilder sv = rd.element("STATICVARIABLES");
		XMLBuilder cc = sv.element("SVCURRENTCOMPANY");
		cc.t(TALLY_COMPANY);

		XMLBuilder rdt = id.element("REQUESTDATA");
		XMLBuilder tm = rdt.element("TALLYMESSAGE");
		tm.a("xmlns:UDF", "TallyUDF");

		XMLBuilder vchr = tm.element("VOUCHER");

		vchr.a("VCHTYPE", tallyVoucherType);
		vchr.a("ACTION", "Create");
		vchr.a("OBJVIEW", "Accounting Voucher View");

		XMLBuilder dt = vchr.element("DATE");
		dt.t(invoiceDate);

		XMLBuilder gd = vchr.element("GUID");
		gd.t("");
		XMLBuilder vtn = vchr.element("VOUCHERTYPENAME");
		vtn.t(tallyVoucherType);
		XMLBuilder vn = vchr.element("VOUCHERNUMBER");
		vn.t(voucherNumber);
		XMLBuilder pldn = vchr.element("PARTYLEDGERNAME");
		pldn.t(AccountName);
		XMLBuilder fpt = vchr.element("FBTPAYMENTTYPE");
		fpt.t("Default");

		XMLBuilder pvw = vchr.element("PERSISTEDVIEW");
		pvw.t("Accounting Voucher View");
		XMLBuilder eby = vchr.element("ENTEREDBY");
		eby.t("System");
		XMLBuilder difq = vchr.element("DIFFACTUALQTY");
		difq.t("NO");
		XMLBuilder audi = vchr.element("AUDITED");
		audi.t("NO");
		XMLBuilder fjbc = vchr.element("FORJOBCOSTING");
		fjbc.t("NO");
		XMLBuilder isoptn = vchr.element("ISOPTIONAL");
		isoptn.t("NO");

		XMLBuilder efdt = vchr.element("EFFECTIVEDATE");
		efdt.t(invoiceDate);
		XMLBuilder frjbw = vchr.element("ISFORJOBWORKIN");
		frjbw.t("NO");
		XMLBuilder alwcm = vchr.element("ALLOWCONSUMPTION");
		alwcm.t("NO");
		XMLBuilder usfintr = vchr.element("USEFORINTEREST");
		usfintr.t("NO");
		XMLBuilder usfrgl = vchr.element("USEFORGAINLOSS");
		usfrgl.t("NO");

		XMLBuilder gdwntrans = vchr.element("USEFORGODOWNTRANSFER");
		gdwntrans.t("NO");
		XMLBuilder usfrcom = vchr.element("USEFORCOMPOUND");
		usfrcom.t("NO");
		XMLBuilder altrid = vchr.element("ALTERID");
		altrid.t("");
		XMLBuilder exopn = vchr.element("EXCISEOPENING");
		exopn.t("NO");

		XMLBuilder usfin = vchr.element("USEFORFINALPRODUCTION");
		usfin.t("NO");
		XMLBuilder can = vchr.element("ISCANCELLED");
		can.t("NO");
		XMLBuilder hahflw = vchr.element("HASCASHFLOW");
		hahflw.t("Yes");

		XMLBuilder pstdt = vchr.element("ISPOSTDATED");
		pstdt.t("NO");
		XMLBuilder trkno = vchr.element("USETRACKINGNUMBER");
		trkno.t("NO");
		XMLBuilder invno = vchr.element("ISINVOICE");
		invno.t("NO");
		XMLBuilder mfgj = vchr.element("MFGJOURNAL");
		mfgj.t("NO");
		XMLBuilder hdis = vchr.element("HASDISCOUNTS");
		hdis.t("NO");
		XMLBuilder pays = vchr.element("ASPAYSLIP");
		pays.t("NO");
		XMLBuilder cst = vchr.element("ISCOSTCENTRE");
		cst.t("NO");

		XMLBuilder txrl = vchr.element("ISSTXNONREALIZEDVCH");
		txrl.t("NO");

		XMLBuilder isdel = vchr.element("ISDELETED");
		isdel.t("NO");
		XMLBuilder orgn = vchr.element("ASORIGINAL");
		orgn.t("NO");
		XMLBuilder frmsyn = vchr.element("VCHISFROMSYNC");
		frmsyn.t("NO");
		XMLBuilder mastid = vchr.element("MASTERID");
		mastid.t("");
		XMLBuilder vchky = vchr.element("VOUCHERKEY");
		vchky.t("");

		XMLBuilder Nar = vchr.element("NARRATION");
		Nar.t(remark);

		for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			DebitClass debitclass = (DebitClass) itr.next();

			String drAccountID = debitclass.getAccountId();

			AccountHeads accountHeadsDR = accountHeadsRepository.findByIdAndCompanyMstId(drAccountID,
					companyMst.getId());

			BigDecimal bdDrAmount = debitclass.getDrAmount();
			BigDecimal bdRDrAmount = bdDrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			XMLBuilder alleden = vchr.element("ALLLEDGERENTRIES.LIST");

			XMLBuilder ldrnm = alleden.element("LEDGERNAME");
			// ldrnm.t(aHdr.getSuppliername());
			ldrnm.t(accountHeadsDR.getAccountName());

			XMLBuilder idempst = alleden.element("ISDEEMEDPOSITIVE");
			idempst.t("Yes");
			XMLBuilder ledfrm = alleden.element("LEDGERFROMITEM");
			ledfrm.t("NO");
			XMLBuilder remzren = alleden.element("REMOVEZEROENTRIES");
			remzren.t("NO");
			XMLBuilder partyld = alleden.element("ISPARTYLEDGER");
			partyld.t("YES");
			XMLBuilder ldp = alleden.element("ISLASTDEEMEDPOSITIVE");
			ldp.t("Yes");
			XMLBuilder anmt = alleden.element("AMOUNT");
			anmt.t("-" + bdRDrAmount.toPlainString());

		}

		for (Iterator itr = creditList.iterator(); itr.hasNext();) {
			CreditClass creditclass = (CreditClass) itr.next();

			String crAccountID = creditclass.getAccountId();

			AccountHeads accountHeadsCR = accountHeadsRepository.findByIdAndCompanyMstId(crAccountID,
					companyMst.getId());

			BigDecimal bdDrAmount = creditclass.getCrAmount();
			BigDecimal bdRDrAmount = bdDrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			XMLBuilder alleden1 = vchr.element("ALLLEDGERENTRIES.LIST");

			XMLBuilder ldrnm1 = alleden1.element("LEDGERNAME");

			ldrnm1.t(accountHeadsCR.getAccountName());

			XMLBuilder idempst1 = alleden1.element("ISDEEMEDPOSITIVE");
			idempst1.t("No");
			XMLBuilder ledfrm1 = alleden1.element("LEDGERFROMITEM");
			ledfrm1.t("NO");
			XMLBuilder remzren1 = alleden1.element("REMOVEZEROENTRIES");
			remzren1.t("NO");
			XMLBuilder partyld1 = alleden1.element("ISPARTYLEDGER");
			partyld1.t("Yes");
			XMLBuilder ldp1 = alleden1.element("ISLASTDEEMEDPOSITIVE");
			ldp1.t("No");
			XMLBuilder anmt1 = alleden1.element("AMOUNT");
			anmt1.t(bdRDrAmount.toPlainString());

		}

		XMLBuilder tmsg = rdt.element("TALLYMESSAGE");
		tmsg.a("xmlns:UDF", "TallyUDF");
		XMLBuilder cmpy = tmsg.element("COMPANY");

		XMLBuilder remtinf = cmpy.element("REMOTECMPINFO.LIST");
		remtinf.a("MERGE", "Yes");
		XMLBuilder nme = remtinf.element("NAME");
		nme.t("");
		XMLBuilder remn = remtinf.element("REMOTECMPNAME");
		remn.t(TALLY_COMPANY);
		XMLBuilder remst = remtinf.element("REMOTECMPSTATE");
		remst.t("Kerala");

		Properties outputProperties = new Properties();

		outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");

		outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
		outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");

		outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

		String fileName = tally_xml_outgoing_folder + File.separatorChar + voucherNumber + invoiceDate + ".xml";

		PrintWriter writer = new PrintWriter(new FileOutputStream(fileName));
		en.toWriter(writer, outputProperties);

		writer.close();

		return true;
	}

	private boolean publishPaymentsToTallyStageOne(PaymentHdr paymentHdr) {
		BigDecimal zero = new BigDecimal("0");

		String voucherNumber = paymentHdr.getVoucherNumber();
		Date voucherDate = paymentHdr.getVoucherDate();

		String sourvceId = paymentHdr.getId();
		// String lmsqueueId = jobInfo.getId();

		String id = paymentHdr.getId();

		String companyMst = paymentHdr.getCompanyMst().getId();
		// (String) context.getJobDetail().getJobDataMap().get("companyid");

		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		String branchMst = paymentHdr.getBranchCode();

		String goDownName = branchMst;

		String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

		System.out.println("Company ID " + companyMst);

		List<AccountClass> accountClassPreviousList = accountClassRepo
				.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

		if (accountClassPreviousList.size() > 0) {
			logger.info("Found previous records.. Deleting");

			Iterator iter = accountClassPreviousList.iterator();
			while (iter.hasNext()) {
				AccountClass accountClass = (AccountClass) iter.next();
				logger.info("Found previous records.. Deleting" + accountClass.getSourceVoucherNumber()
						+ accountClass.getBrachCode());

				List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
				List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

				List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

				if (creditClassList.size() > 0) {

					try {

						boolean result = generatePaymentToTallyXml(accountClass.getCompanyMst(), branchMst, "PAYMENTS",
								branchMst, voucherNumber, strVoucherDate, debitClassList, creditClassList, tallyServer);

						/*
						 * boolean result = cPaymentsToTally..(accountClass.getCompanyMst(), branchMst,
						 * "RECEIPT", branchMst, voucherNumber, strVoucherDate, debitClassList,
						 * creditClassList,tallyServer);
						 * 
						 */

					} catch (TransformerException | IOException | ParserConfigurationException
							| FactoryConfigurationError e) {
						// TODO Auto-generated catch block
						logger.error(e.getMessage());
					}
				}

			}
		}
		return true;
	}

	public boolean generatePaymentToTallyXml(CompanyMst companyMst, String CostCenter, String tallyVoucherType,
			String Godown, String voucherNumber, String invoiceDate, List<DebitClass> debitList,
			List<CreditClass> creditList, String tallyServer) throws TransformerException, ClientProtocolException,
			IOException, ParserConfigurationException, FactoryConfigurationError {

		String VoucherId = "";
		String ErrorCode = "";
		String Linerror = "";

		invoiceDate = invoiceDate.replace("-", "");

		// String VoucherType = APlicationWindow.MachineName+"_RECEIPTS" ;

		String ItemLoopXML = "";
		String ItemXML = "";
		XMLBuilder TAXOBJL = null;
		XMLBuilder LEDGERENTRYL2 = null;
		String AccountName = "";
		String remark = "";
		BigDecimal bdCrAmount = new BigDecimal("0");
		BigDecimal bdRCrAmount = new BigDecimal("0");

		for (Iterator itr = creditList.iterator(); itr.hasNext();) {
			// for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			// DebitClass debitclass = (DebitClass) itr.next();
			CreditClass creditclass = (CreditClass) itr.next();
			String drAccountID = creditclass.getAccountId();

			AccountHeads accountHeadsCR = accountHeadsRepository.findByIdAndCompanyMstId(drAccountID,
					companyMst.getId());

			remark = creditclass.getRemark();
			bdCrAmount = creditclass.getCrAmount();
			bdRCrAmount = bdCrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			AccountName = accountHeadsCR.getAccountName();
		}

		XMLBuilder en = XMLBuilder.create("ENVELOPE");
		XMLBuilder hd = en.element("HEADER");
		XMLBuilder tr = hd.element("TALLYREQUEST");
		tr.t("ImportData");

		XMLBuilder bd = en.element("BODY");
		XMLBuilder id = bd.element("IMPORTDATA");
		XMLBuilder rd = id.element("REQUESTDESC");
		XMLBuilder rn = rd.element("REPORTNAME");
		rn.t("Vouchers");

		XMLBuilder sv = rd.element("STATICVARIABLES");
		XMLBuilder cc = sv.element("SVCURRENTCOMPANY");
		cc.t(TALLY_COMPANY);

		XMLBuilder rdt = id.element("REQUESTDATA");
		XMLBuilder tm = rdt.element("TALLYMESSAGE");
		tm.a("xmlns:UDF", "TallyUDF");

		XMLBuilder vchr = tm.element("VOUCHER");

		vchr.a("VCHTYPE", tallyVoucherType);
		vchr.a("ACTION", "Create");
		vchr.a("OBJVIEW", "Accounting Voucher View");

		XMLBuilder dt = vchr.element("DATE");
		dt.t(invoiceDate);

		XMLBuilder gd = vchr.element("GUID");
		gd.t("");
		XMLBuilder vtn = vchr.element("VOUCHERTYPENAME");
		vtn.t(tallyVoucherType);
		XMLBuilder vn = vchr.element("VOUCHERNUMBER");
		vn.t(voucherNumber);
		XMLBuilder pldn = vchr.element("PARTYLEDGERNAME");
		pldn.t(AccountName);
		XMLBuilder fpt = vchr.element("FBTPAYMENTTYPE");
		fpt.t("Default");

		XMLBuilder pvw = vchr.element("PERSISTEDVIEW");
		pvw.t("Accounting Voucher View");
		XMLBuilder eby = vchr.element("ENTEREDBY");
		eby.t("System");
		XMLBuilder difq = vchr.element("DIFFACTUALQTY");
		difq.t("NO");
		XMLBuilder audi = vchr.element("AUDITED");
		audi.t("NO");
		XMLBuilder fjbc = vchr.element("FORJOBCOSTING");
		fjbc.t("NO");
		XMLBuilder isoptn = vchr.element("ISOPTIONAL");
		isoptn.t("NO");

		XMLBuilder efdt = vchr.element("EFFECTIVEDATE");
		efdt.t(invoiceDate);
		XMLBuilder frjbw = vchr.element("ISFORJOBWORKIN");
		frjbw.t("NO");
		XMLBuilder alwcm = vchr.element("ALLOWCONSUMPTION");
		alwcm.t("NO");
		XMLBuilder usfintr = vchr.element("USEFORINTEREST");
		usfintr.t("NO");
		XMLBuilder usfrgl = vchr.element("USEFORGAINLOSS");
		usfrgl.t("NO");

		XMLBuilder gdwntrans = vchr.element("USEFORGODOWNTRANSFER");
		gdwntrans.t("NO");
		XMLBuilder usfrcom = vchr.element("USEFORCOMPOUND");
		usfrcom.t("NO");
		XMLBuilder altrid = vchr.element("ALTERID");
		altrid.t("");
		XMLBuilder exopn = vchr.element("EXCISEOPENING");
		exopn.t("NO");

		XMLBuilder usfin = vchr.element("USEFORFINALPRODUCTION");
		usfin.t("NO");
		XMLBuilder can = vchr.element("ISCANCELLED");
		can.t("NO");
		XMLBuilder hahflw = vchr.element("HASCASHFLOW");
		hahflw.t("Yes");

		XMLBuilder pstdt = vchr.element("ISPOSTDATED");
		pstdt.t("NO");
		XMLBuilder trkno = vchr.element("USETRACKINGNUMBER");
		trkno.t("NO");
		XMLBuilder invno = vchr.element("ISINVOICE");
		invno.t("NO");
		XMLBuilder mfgj = vchr.element("MFGJOURNAL");
		mfgj.t("NO");
		XMLBuilder hdis = vchr.element("HASDISCOUNTS");
		hdis.t("NO");
		XMLBuilder pays = vchr.element("ASPAYSLIP");
		pays.t("NO");
		XMLBuilder cst = vchr.element("ISCOSTCENTRE");
		cst.t("NO");

		XMLBuilder txrl = vchr.element("ISSTXNONREALIZEDVCH");
		txrl.t("NO");

		XMLBuilder isdel = vchr.element("ISDELETED");
		isdel.t("NO");
		XMLBuilder orgn = vchr.element("ASORIGINAL");
		orgn.t("NO");
		XMLBuilder frmsyn = vchr.element("VCHISFROMSYNC");
		frmsyn.t("NO");
		XMLBuilder mastid = vchr.element("MASTERID");
		mastid.t("");
		XMLBuilder vchky = vchr.element("VOUCHERKEY");
		vchky.t("");

		XMLBuilder Nar = vchr.element("NARRATION");
		Nar.t(remark);
		// for (Iterator itr = creditList.iterator(); itr.hasNext();) {
		for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			DebitClass debitclass = (DebitClass) itr.next();
			// CreditClass creditclass = (CreditClass) itr.next();
			String drAccountID = debitclass.getAccountId();

			AccountHeads accountHeadsDR = accountHeadsRepository.findByIdAndCompanyMstId(drAccountID,
					companyMst.getId());

			BigDecimal bdDrAmount = debitclass.getDrAmount();
			BigDecimal bdRDrAmount = bdDrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			XMLBuilder alleden = vchr.element("ALLLEDGERENTRIES.LIST");

			XMLBuilder ldrnm = alleden.element("LEDGERNAME");
			// ldrnm.t(aHdr.getSuppliername());
			ldrnm.t(accountHeadsDR.getAccountName());

			XMLBuilder idempst = alleden.element("ISDEEMEDPOSITIVE");
			idempst.t("Yes");
			XMLBuilder ledfrm = alleden.element("LEDGERFROMITEM");
			ledfrm.t("NO");
			XMLBuilder remzren = alleden.element("REMOVEZEROENTRIES");
			remzren.t("NO");
			XMLBuilder partyld = alleden.element("ISPARTYLEDGER");
			partyld.t("No");
			XMLBuilder ldp = alleden.element("ISLASTDEEMEDPOSITIVE");
			ldp.t("Yes");
			XMLBuilder anmt = alleden.element("AMOUNT");
			anmt.t("-" + bdRDrAmount.toPlainString());

		}

		for (Iterator itr = creditList.iterator(); itr.hasNext();) {
			// for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			CreditClass creditclass = (CreditClass) itr.next();
			// DebitClass debitclass = (DebitClass) itr.next();
			String crAccountID = creditclass.getAccountId();

			AccountHeads accountHeadsCR = accountHeadsRepository.findByIdAndCompanyMstId(crAccountID,
					companyMst.getId());

			BigDecimal bdDrAmount = creditclass.getCrAmount();
			BigDecimal bdRDrAmount = bdDrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			XMLBuilder alleden1 = vchr.element("ALLLEDGERENTRIES.LIST");

			XMLBuilder ldrnm1 = alleden1.element("LEDGERNAME");

			ldrnm1.t(accountHeadsCR.getAccountName());

			XMLBuilder idempst1 = alleden1.element("ISDEEMEDPOSITIVE");
			idempst1.t("No");
			XMLBuilder ledfrm1 = alleden1.element("LEDGERFROMITEM");
			ledfrm1.t("NO");
			XMLBuilder remzren1 = alleden1.element("REMOVEZEROENTRIES");
			remzren1.t("NO");
			XMLBuilder partyld1 = alleden1.element("ISPARTYLEDGER");
			partyld1.t("Yes");
			XMLBuilder ldp1 = alleden1.element("ISLASTDEEMEDPOSITIVE");
			ldp1.t("No");
			XMLBuilder anmt1 = alleden1.element("AMOUNT");
			anmt1.t(bdRDrAmount.toPlainString());

		}

		XMLBuilder tmsg = rdt.element("TALLYMESSAGE");
		tmsg.a("xmlns:UDF", "TallyUDF");
		XMLBuilder cmpy = tmsg.element("COMPANY");

		XMLBuilder remtinf = cmpy.element("REMOTECMPINFO.LIST");
		remtinf.a("MERGE", "Yes");
		XMLBuilder nme = remtinf.element("NAME");
		nme.t("");
		XMLBuilder remn = remtinf.element("REMOTECMPNAME");
		remn.t(TALLY_COMPANY);
		XMLBuilder remst = remtinf.element("REMOTECMPSTATE");
		remst.t("Kerala");

		Properties outputProperties = new Properties();

		outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");

		outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
		outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");

		outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

		String fileName = tally_xml_outgoing_folder + File.separatorChar + voucherNumber + invoiceDate + ".xml";

		PrintWriter writer = new PrintWriter(new FileOutputStream(fileName));
		en.toWriter(writer, outputProperties);

		writer.close();

		return true;
	}

	private boolean publishJournalToTallySatageOne(JournalHdr journalTransHdr) {
		BigDecimal zero = new BigDecimal("0");

		String voucherNumber = journalTransHdr.getVoucherNumber();
		Date voucherDate = journalTransHdr.getVoucherDate();

		System.out.println("VOUCHER NUBMER =  " + voucherNumber);
		System.out.println("VOUCHER DATE =  " + voucherDate);

		String sourvceId = journalTransHdr.getId();
		// String lmsqueueId = jobInfo.getId();

		String id = journalTransHdr.getId();

		String companyMst = journalTransHdr.getCompanyMst().getId();
		// (String) context.getJobDetail().getJobDataMap().get("companyid");

		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		String branchMst = journalTransHdr.getBranchCode();

		String goDownName = branchMst;

		String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

		System.out.println("Company ID " + companyMst);

		List<AccountClass> accountClassPreviousList = accountClassRepo
				.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

		if (accountClassPreviousList.size() > 0) {
			logger.info("Found previous records.. Deleting");

			Iterator iter = accountClassPreviousList.iterator();
			while (iter.hasNext()) {
				AccountClass accountClass = (AccountClass) iter.next();
				logger.info("Found previous records.. " + accountClass.getSourceVoucherNumber()
						+ accountClass.getBrachCode());

				List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
				List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

				List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

				if (creditClassList.size() > 0) {

					try {
						boolean result = generateJournalToTallyXml(accountClass.getCompanyMst(),
								journalTransHdr.getBranchCode(), "GODOWN", "JNL", voucherNumber, strVoucherDate,
								debitClassList, creditClassList, tallyServer);

					} catch (SQLException e) {
						// TODO Auto-generated catch block
						logger.error(e.getMessage());
						return false;

					}
				}

			}
		}
		return true;
	}

	public boolean generateJournalToTallyXml(CompanyMst companyMst, String CostCenter, String Godown,
			String tallyVoucherType, String voucherNumber, String invoiceDate, List<DebitClass> debitList,
			List<CreditClass> creditList, String tallyServer) throws SQLException {

		boolean hasJournal = false;
		boolean JournalCompleted = true;

		String VoucherId = "";
		String ErrorCode = "";
		String Linerror = "";

		// "http://localhost:9050";

		invoiceDate = invoiceDate.replace("-", "");

		SALES_ENVELOPE env = new SALES_ENVELOPE();

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setNatureofsales("Domestic");

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setVCHTYPE(tallyVoucherType);
		// env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setClassname("VAT");

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setVouchertypename(tallyVoucherType);

		if (voucherNumber.length() > 15) {
			voucherNumber = voucherNumber.replaceAll("2019-2020", "19-20");

		}
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setVouchernumber(voucherNumber);

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setFbtpaymenttype("Default");

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher()
				.setPersistedview("Invoice Voucher View");

		env.getBody().getImpdate().getSrequestdec().setReportname("Vouchers");
		env.getBody().getImpdate().getSrequestdec().getStaticvar().setSvcurrentcompany(TALLY_COMPANY);

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setExciseopening("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setUseforfinalproduction("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIscancelled("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setHascashflow("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIspostdated("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setUsetrackingnumber("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIsinvoice("Yes");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setMfjournal("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setHasdiscounts("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setAspayslip("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIscostcenter("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIsstxnonrealizedvch("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIsdeleted("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setAsorginal("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setVchfromsync("No");

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIsvatoverridden("Yes");

		boolean CustomerFound = true;
		boolean InsuranceFound = false;
		String DebitAccountName = "";
		String tempDebitAccountName = "";
		String InsuranceDebitAccountName = "";
		String CustomerDebitAccountName = "";

		BigDecimal bInsuranceAmount = new BigDecimal("0");

		String Remark = "";

		for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			DebitClass debitclass = (DebitClass) itr.next();

			BigDecimal bdDrAmount = new BigDecimal("0");

			bdDrAmount = debitclass.getDrAmount();

			BigDecimal crediutAmt = getCreditTotal(creditList);

			BigDecimal diff = bdDrAmount.subtract(crediutAmt);

			Remark = debitclass.getRemark();

			String drAccountID = debitclass.getAccountId();

			AccountHeads accountHeadsDR = accountHeadsRepository.findByIdAndCompanyMstId(drAccountID,
					companyMst.getId());

			BigDecimal bdRDrAmount = bdDrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			env.getHdr().setTallyrequest("Import Data");

			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setDate(invoiceDate);
			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setNarration(Remark);
			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher()
					.setBasicdatetimeofinvoice(invoiceDate);
			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher()
					.setBasicdatetimeofremoval(invoiceDate);

			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setEnteredby("125");
			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setDiffactualqty("No");

			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setEffectivedate(invoiceDate);

			SALESDR_LEDGERENTRIES sl = new SALESDR_LEDGERENTRIES();

			sl.getSalesoldauditentry().setOLDAUDITENTRYIDS("-1");

			sl.setLEDGERNAME(accountHeadsDR.getAccountName());

			sl.setLEDGERFROMITEM("No");
			sl.setREMOVEZEROENTRIES("No");
			sl.setISPARTYLEDGER("Yes");

			if (bdDrAmount.doubleValue() < 0) {
				sl.setISLASTDEEMEDPOSITIVE("No");
				sl.setAMOUNT(bdRDrAmount.abs().toPlainString());
			} else {
				sl.setISLASTDEEMEDPOSITIVE("Yes");
				sl.setAMOUNT("-" + bdRDrAmount.toPlainString());
			}

			sl.getSalesbillalloc().setNAME(voucherNumber);
			sl.getSalesbillalloc().setBILLTYPE("New Ref");
			sl.getSalesbillalloc().setTDSDEDUCTEEISSPECIALRATE("No");
			sl.getSalesbillalloc().setAMOUNT("-" + bdRDrAmount.toPlainString());

			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getLedgerentries().add(sl);
		}

		if (!CustomerFound) {
			throw new SQLException("Error CUSTOMER_NOT_FOUND ");
		}

		for (Iterator citr = creditList.iterator(); citr.hasNext();) {
			CreditClass creditclass = (CreditClass) citr.next();

			AccountHeads accountHeadsCR = accountHeadsRepository.findByIdAndCompanyMstId(creditclass.getAccountId(),
					companyMst.getId());

			String CreditAccountName = accountHeadsCR.getAccountName();
			if (null != creditclass.getItemId() && creditclass.getItemId().length() > 0) {

				ItemMst item = itemMstRepository.findById(creditclass.getItemId()).get();

				String unitid = item.getUnitId();

				UnitMst unitMst = unitMstRepository.findById(unitid).get();

				String unitname = unitMst.getUnitName();

				BigDecimal bdRate = new BigDecimal(creditclass.getRate());
				BigDecimal bdRRate = bdRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				String RatePerUnit = bdRRate.toPlainString() + "/" + unitname;
				BigDecimal bdQty = new BigDecimal(creditclass.getQty());
				BigDecimal bdRQty = bdQty.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				String QTYandUnit = bdRQty.toPlainString() + " " + unitname;

				BigDecimal bdAssessibleValue = creditclass.getCrAmount();
				BigDecimal bdRAssessibleValue = bdAssessibleValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				BigDecimal QTYINTORATE = bdRQty.multiply(bdRRate);
				BigDecimal TaxRate = new BigDecimal(item.getTaxRate());

				TaxRate = TaxRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				BigDecimal TAXINTOAMOUNT = QTYINTORATE.multiply(TaxRate);
				BigDecimal Hundred = new BigDecimal("100");
				BigDecimal bdVatAmount = TAXINTOAMOUNT.divide(Hundred);
				bdVatAmount = bdVatAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				ALLINVENTORYENTRIES alinv = new ALLINVENTORYENTRIES();

				alinv.setISDEEMEDPOSITIVE("No");
				alinv.setISLASTDEEMEDPOSITIVE("No");
				alinv.setISAUTONEGATE("No");
				alinv.setRATE(RatePerUnit);
				alinv.setAMOUNT(bdRAssessibleValue.toPlainString());
				alinv.setACTUALQTY(QTYandUnit);
				alinv.setBILLEDQTY(QTYandUnit);

				alinv.getBatchAllocation().setGODOWNNAME(CostCenter);

				alinv.getBatchAllocation().setAMOUNT(bdRAssessibleValue.toPlainString());
				alinv.getBatchAllocation().setACTUALQUANTITY(QTYandUnit);
				alinv.getBatchAllocation().setBILLEDQUANTITY(QTYandUnit);

				alinv.getAccountingAllocation().setLEDGERNAME(CreditAccountName);

				alinv.getAccountingAllocation().setCLASSRATE("0.00000");
				alinv.getAccountingAllocation().setISDEEMEDPOSITIVE("No");
				alinv.getAccountingAllocation().setLEDGERFROMITEM("No");
				alinv.getAccountingAllocation().setREMOVEZEROENTRIES("No");
				alinv.getAccountingAllocation().setISPARTYLEDGER("No");
				alinv.getAccountingAllocation().setISLASTDEEMEDPOSITIVE("No");
				alinv.getAccountingAllocation().setAMOUNT(bdRAssessibleValue.toPlainString());
				alinv.getAccountingAllocation().getCatagoryallocation().setCATEGORY("Primary Cost Category");
				alinv.getAccountingAllocation().getCatagoryallocation().setISDEEMEDPOSITIVE("No");
				// alinv.getAccountingAllocation().getCatagoryallocation().getCostcenterallocation().setNAME(CostCenter);
				// alinv.getAccountingAllocation().getCatagoryallocation().getCostcenterallocation()
				// .setAMOUNT(bdRAssessibleValue.toPlainString());

				alinv.setSTOCKITEMNAME(item.getItemName());
				env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getAllinventoryList()
						.add(alinv);

				if (bdVatAmount.doubleValue() > 0) {
					alinv.getAccountingAllocation().setSTATNATURENAME("Sales Taxable");
					alinv.getAccountingAllocation().setVATTAXRATE(TaxRate.toPlainString());
				} else {
					alinv.getAccountingAllocation().setSTATNATURENAME("Sales Exempt");
				}

			} else {

				SALESDR_LEDGERENTRIES sl12 = new SALESDR_LEDGERENTRIES();

				sl12.setLEDGERNAME(CreditAccountName);

				sl12.setISDEEMEDPOSITIVE("No");
				sl12.setLEDGERFROMITEM("No");

				BigDecimal bdTaxAmt = creditclass.getCrAmount();

				BigDecimal bdRTaxAmt = bdTaxAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				sl12.setAMOUNT(bdRTaxAmt.toPlainString());

				env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getLedgerentries().add(sl12);

			}
		}

		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(SALES_ENVELOPE.class);

			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			String fileName = tally_xml_outgoing_folder + File.separatorChar + voucherNumber + invoiceDate + ".xml";

			jaxbMarshaller.marshal(env, new File(fileName));
			jaxbMarshaller.marshal(env, System.out);

		} catch (JAXBException e) { // TODO Auto-generated catch block
			logger.error(e.getMessage());
			return false;

		}

		return true;

	}
	
	private boolean processReturnMain(String Linerror,  String TallyServer, String TALLY_COMPANY,
			String voucherType) throws SQLException {
		if (Linerror.contains("Voucher totals do not match!")) {

			throw new SQLException("Error" + Linerror);
		}

		if (Linerror.contains("No Accounting Allocations for")) {

			throw new SQLException("Error" + Linerror);
		}

		if (Linerror.contains("Group") && Linerror.contains("does not exist!")) {
			 processReturnGroup(  Linerror,       TallyServer,   TALLY_COMPANY,
						  voucherType);
		}
		
		if (Linerror.contains("Cost Centre") && Linerror.contains("does not exist!")) {
			 

			 processReturnCostCentre(  Linerror,      TallyServer,   TALLY_COMPANY,
						  voucherType);
		}
		
		if (Linerror.contains("Ledger") && Linerror.contains("does not exist!")) {
			 processReturnLedger(  Linerror,       TallyServer,   TALLY_COMPANY,
						  voucherType) ;
			 
		}
		if (Linerror.contains("Voucher Type") && Linerror.contains("does not exist!")) {
			processReturnVoucherType(  Linerror,    TallyServer,   TALLY_COMPANY,
					  voucherType) ;
		}
		if (Linerror.contains("Stock Item") && Linerror.contains("does not exist!")) {
			processReturnStockItem(  Linerror,      TallyServer,   TALLY_COMPANY,
					  voucherType) ;
			
		}
		
		return false;
	}
	
	private boolean processReturnGroup(String Linerror,   String TallyServer, String TALLY_COMPANY,
			String voucherType) throws SQLException {
		String GroupName = Linerror.replace("Group", "").replace("does not exist!", "").replace("'", "").trim();

		System.out.println("missingLedger =  " + GroupName);

		String ParentAccountHead = getParentAccount(GroupName);

		String XML_PATH = "";

		LedgerGroupXML(GroupName, ParentAccountHead, TallyServer, TALLY_COMPANY, true);

		return false;
	}
	
	private boolean processReturnCostCentre(String Linerror,   String TallyServer, String TALLY_COMPANY,
			String voucherType) throws SQLException {
		
		String goDownName = Linerror.replace("Cost Centre", "").replace("does not exist!", "").replace("'", "")
				.trim();

		System.out.println("missingLedger =  " + goDownName);

		String XML_PATH = "";

		CreateCostCenterXML(goDownName, TallyServer, TALLY_COMPANY);

		return false;
		
	}
	
	private boolean processReturnLedger(String Linerror,   String TallyServer, String TALLY_COMPANY,
			String voucherType) throws SQLException {
			String missingLedger = Linerror.replace("Ledger", "").replace("does not exist!", "").replace("'", "")
					.trim();
			System.out.println("missingLedger =  " + missingLedger);
			String parentAccount = getParentAccount(missingLedger);
			String affectstock = "YES";
			if (null != parentAccount) {
				if (parentAccount.equalsIgnoreCase("0")) {
					if (missingLedger.contains("Sales@")) {
						parentAccount = "Sales Accounts";
						missingLedger = missingLedger  ;

					} else if (missingLedger.contains("Purchase@")) {
						parentAccount = "Purchase Accounts";
						missingLedger = missingLedger ;
					} else if (missingLedger.contains("Output VAT@")) {
						parentAccount = "Duties & Taxes";
						missingLedger = missingLedger  ;
						affectstock = "YES";
					} else if (missingLedger.contains("_CASH")) {
						parentAccount = "Cash-in-hand";
						affectstock = "NO";
					}
				}
			} else if (missingLedger.contains("-CASH")) {
				parentAccount = "Cash-in-hand";
				affectstock = "NO";
			} else {

				affectstock = "YES";
			}

			AccountHeads CusOpt = accountHeadsRepository.findByAccountNameAndCompanyMstId(missingLedger,
					TALLY_COMPANY);

			if (null != CusOpt) {
				AccountHeads customerMst = CusOpt;
				parentAccount = getParentAccount(missingLedger);

				if (null == parentAccount || parentAccount.equalsIgnoreCase("")) {
					parentAccount = "Sundry Debtors";
					affectstock = "NO";
				}
				CreateSupplier(missingLedger, customerMst.getPartyGst(), customerMst.getPartyAddress1(),
						customerMst.getId(), parentAccount, TallyServer, TALLY_COMPANY, affectstock,
						customerMst.getCustomerPin(), customerMst.getCustomerCountry(),
						customerMst.getCustomerGstType(), customerMst.getCustomerState());

			} else {

				String OrgParent = parentAccount;
				while (true) {

					String tempParent = parentAccount;

					if (missingLedger.equalsIgnoreCase("SALES ACCOUNT")) {
						affectstock = "YES";
						parentAccount = "Sales Accounts";
						OrgParent = "Sales Accounts";

						LedgerXML(missingLedger, "", "", "", OrgParent, TallyServer,  affectstock,
								TALLY_COMPANY);

						break;

					} else if (missingLedger.equalsIgnoreCase("SGST ACCOUNT")) {
						OrgParent = "Duties & Taxes";
						parentAccount = "Duties & Taxes";

						LedgerGroupXML(missingLedger, parentAccount, TallyServer, TALLY_COMPANY, true);

						break;

					} else if (missingLedger.equalsIgnoreCase("CGST ACCOUNT")) {
						OrgParent = "Duties & Taxes";
						parentAccount = "Duties & Taxes";

						LedgerGroupXML(missingLedger, parentAccount, TallyServer, TALLY_COMPANY, true);

						break;

					} else if (missingLedger.equalsIgnoreCase("KERALA FLOOD CESS")) {
						OrgParent = "Duties & Taxes";
						parentAccount = "Duties & Taxes";

						LedgerGroupXML(missingLedger, parentAccount, TallyServer, TALLY_COMPANY, true);
						break;

					} else if (missingLedger.contains("-CASH")) {
						OrgParent = "Cash-in-hand";
						parentAccount = "Cash-in-hand";

						LedgerGroupXML(missingLedger, parentAccount, TallyServer, TALLY_COMPANY, true);

						break;

					} else if (parentAccount.equals("Liabilities")) {
						break;
					} else if (parentAccount.equals("Duties & Taxes")) {

						break;
					} else if (parentAccount.equals("Sales Accounts")) {
						break;
					} else if (parentAccount.equals("Assets")) {
						break;
					} else if (parentAccount.equals("Income")) {
						break;
					} else if (parentAccount.equals("Expenses")) {
						break;
					} else if (parentAccount.equals("Cash-in-hand")) {
						break;
					} else {
						parentAccount = getParentAccount(parentAccount);
						if (null == parentAccount) {
							parentAccount = "Assets";
						}
					}

					if (null != tempParent && !tempParent.equalsIgnoreCase(parentAccount)) {

						LedgerGroupXML(tempParent, parentAccount, TallyServer, TALLY_COMPANY, true);
						break;
					}

				}

				affectstock = "YES";

				LedgerXML(missingLedger, "", "", "", OrgParent, TallyServer,  affectstock, TALLY_COMPANY);

			}


			return false;
	}
	
	private boolean processReturnVoucherType(String Linerror,  String TallyServer, String TALLY_COMPANY,
			String voucherType) throws SQLException {
		
			String missingLedger = Linerror.replace("Voucher Type", "").replace("does not exist!", "").replace("'", "")
					.trim();

			System.out.println("missingLedger =  " + missingLedger);

			String parentAccount = voucherType;

			String XML_PATH = "";

			boolean rtnValue = CreateVoucherTypeXML(missingLedger, parentAccount, "Manual", "Yes", TallyServer,
					TALLY_COMPANY);

			 

			return false;
	}
	
	private boolean processReturnStockItem(String Linerror,  String TallyServer, String TALLY_COMPANY,
			String voucherType) throws SQLException {
	
			String missingStockItem = Linerror.replace("Stock Item", "").replace("does not exist!", "").replace("'", "")
					.trim();

			System.out.println("missingLedger =  " + missingStockItem);

			ItemMst itemMst = itemMstRepository.findByItemName(missingStockItem);
			UnitMst unitMst = unitMstRepository.findById(itemMst.getUnitId()).get();

			String affectstock = "YES";
			CategoryMst categoryMst = null;

			if (null == itemMst.getCategoryId()) {
				categoryMst = categoryMstRepository.findSearchAll().get(0);
			} else {
				categoryMst = categoryMstRepository.findById(itemMst.getCategoryId()).get();
			}

			CreateUnitXML(unitMst.getUnitName(), "2", unitMst.getUnitDescription(), TallyServer, TALLY_COMPANY);

			NewStockGroup(categoryMst.getCategoryName(), TallyServer, unitMst.getUnitName(), itemMst.getBarCode(),
					TALLY_COMPANY, affectstock);

			String GSTDTLAPPLICABLEFROM = "2020-04-01";

			GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("-", "");
			GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("/", "");

			NewItemVerson5(missingStockItem, TallyServer, itemMst.getTaxRate(), unitMst.getUnitName(),
					itemMst.getBarCode(), TALLY_COMPANY, affectstock, categoryMst.getCategoryName(),
					itemMst.getHsnCode(), GSTDTLAPPLICABLEFROM);

			 
		 return false;

	}

 

	private String getParentAccount(String accountName) {

		AccountHeads accountHeads = accountHeadsRepository.findByAccountName(accountName);

		if (null == accountHeads.getParentId()) {
			return null;
		}

		if (accountHeads.getParentId().equalsIgnoreCase("0")) {
			return null;
		}

		AccountHeads parentAccountHeads = accountHeadsRepository.findById(accountHeads.getParentId()).get();

		return parentAccountHeads.getAccountName();
	}

	private boolean LedgerXML(String AcName, String TinNo, String Address, String Alias, String ParentAccountHead,
			String TallyServer,   String affectstock, String TALLY_COMPANY) {

		String iNfileName = tally_xml_outgoing_folder + File.separatorChar + "ledger" + AcName + ".xml";

		try {

			XMLBuilder en = XMLBuilder.create("ENVELOPE");
			XMLBuilder Hd = en.element("HEADER");
			XMLBuilder vr = Hd.element("VERSION");
			vr.t("1");
			XMLBuilder TRq = Hd.element("TALLYREQUEST");
			TRq.t("import");
			XMLBuilder tp = Hd.element("TYPE");
			tp.t("Data");
			XMLBuilder id = Hd.element("ID");
			id.t("All Masters");
			XMLBuilder bd = en.element("BODY");
			XMLBuilder de = bd.element("DESC");
			XMLBuilder stv = de.element("STATICVARIABLES");
			XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
			cc.t(TALLY_COMPANY);
			XMLBuilder idu = stv.element("IMPORTDUPS");
			idu.t("@@DUPCOMBINE");

			XMLBuilder dt = bd.element("DATA");
			XMLBuilder tm = dt.element("TALLYMESSAGE");
			XMLBuilder lg = tm.element("LEDGER");
			lg.a("Action", "Create");
			lg.a("NAME", AcName);
			XMLBuilder nm = lg.element("NAME");
			nm.t(AcName);
			XMLBuilder pt = lg.element("PARENT");
			pt.t(ParentAccountHead);

			if (ParentAccountHead.equalsIgnoreCase("Duties & Taxes")) {
				XMLBuilder TAXTYPE = lg.element("TAXTYPE");
				TAXTYPE.t("GST");

				if (AcName.contains("CGST")) {
					XMLBuilder GSTDUTYHEAD = lg.element("GSTDUTYHEAD");
					GSTDUTYHEAD.t("Central Tax");

				} else if (AcName.contains("SGST")) {
					XMLBuilder GSTDUTYHEAD = lg.element("GSTDUTYHEAD");
					GSTDUTYHEAD.t("State Tax");
				}
			}
			XMLBuilder adlst = lg.element("ADDRESS.LIST");
			adlst.a("TYPE", "String");
			if (null != Address) {
				XMLBuilder adr = adlst.element("ADDRESS");
				adr.t(Address);
			}
			XMLBuilder mailname = adlst.element("MAILINGNAME.LIST");
			mailname.a("TYPE", "String");
			XMLBuilder maname = adlst.element("MAILINGNAME");
			maname.t(AcName);

			if (null != TinNo) {
				XMLBuilder intax = lg.element("VATTINNUMBER");
				intax.t(TinNo);
			}

			XMLBuilder afstk = lg.element("AFFECTSSTOCK");
			afstk.t(affectstock);

			XMLBuilder ASORIGINAL = lg.element("ASORIGINAL");
			ASORIGINAL.t("Yes");

			XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
			XMLBuilder langnmlst = lg.element("NAME.LIST");
			langlst.a("TYPE", "String");
			XMLBuilder lname = langnmlst.element("NAME");
			lname.t(AcName);
			if (null != Alias) {
				XMLBuilder alname = langnmlst.element("NAME");
				alname.t(Alias);
			}

			Properties outputProperties = new Properties();
			// Explicitly identify the output as an XML document
			outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
			// Pretty-print the XML output (doesn't work in all cases)
			outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
			// Get 2-space indenting when using the Apache transformer
			outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
			// Omit the XML declaration header
			outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

			PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
			en.toWriter(writer, outputProperties);
			writer.close();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return true;

	}

	public void LedgerGroupXML(String Group, String ParentAccountHead, String TallyServer, String CmpNmae,
			boolean DeleteXML) {

		/*
		 * Var declaration
		 */

		String iNfileName = tally_xml_outgoing_folder + File.separatorChar + "ledgergroup" + Group + ".xml";

		DateFormat formatter;
		formatter = new SimpleDateFormat("yyyyMMdd");

		try {

			XMLBuilder en = XMLBuilder.create("ENVELOPE");
			XMLBuilder Hd = en.element("HEADER");
			XMLBuilder vr = Hd.element("VERSION");
			vr.t("1");
			XMLBuilder TRq = Hd.element("TALLYREQUEST");
			TRq.t("import");
			XMLBuilder tp = Hd.element("TYPE");
			tp.t("Data");
			XMLBuilder id = Hd.element("ID");
			id.t("All Masters");
			XMLBuilder bd = en.element("BODY");
			XMLBuilder de = bd.element("DESC");
			XMLBuilder stv = de.element("STATICVARIABLES");

			XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
			cc.t(CmpNmae);

			XMLBuilder idu = stv.element("IMPORTDUPS");
			idu.t("@@DUPCOMBINE");
			XMLBuilder dt = bd.element("DATA");
			XMLBuilder tm = dt.element("TALLYMESSAGE");
			/*
			 * Create Group
			 */
			XMLBuilder lg = tm.element("GROUP");
			lg.a("Action", "Create");
			lg.a("NAME", Group);
			XMLBuilder subleger = lg.element("ISSUBLEDGER");
			subleger.t("No");
			XMLBuilder nm = lg.element("NAME");
			nm.t(Group);
			XMLBuilder pt = lg.element("PARENT");
			pt.t(ParentAccountHead);

			/*
			 * Code below will create Alias for the account
			 */
			XMLBuilder languageNameList = lg.element("LANGUAGENAME.LIST");
			XMLBuilder Nlist = languageNameList.element("NAME.LIST");
			Nlist.a("TYPE", "String");
			XMLBuilder Nme = Nlist.element("NAME");
			Nme.t(Group);
			// XMLBuilder Nme2 = Nlist.element("NAME");
			// Nme2.t(Alias);

			Properties outputProperties = new Properties();
			// Explicitly identify the output as an XML document
			outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
			// Pretty-print the XML output (doesn't work in all cases)
			outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
			// Get 2-space indenting when using the Apache transformer
			outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
			// Omit the XML declaration header
			outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

			PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
			en.toWriter(writer, outputProperties);

			writer.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}

	}

	public boolean CreateCostCenterXML(String goDownName, String TallyServer, String CompanyName) {

		String iNfileName = tally_xml_outgoing_folder + File.separatorChar + "cc" + goDownName + ".xml";
		try {

			System.out.println("Company " + CompanyName);

			System.out.println("cost center  " + goDownName);

			XMLBuilder en = XMLBuilder.create("ENVELOPE");
			XMLBuilder Hd = en.element("HEADER");
			// XMLBuilder vr = Hd.element("VERSION");

			// vr.t("1");
			XMLBuilder TRq = Hd.element("TALLYREQUEST");
			TRq.t("import Data");

			XMLBuilder bd = en.element("BODY");
			XMLBuilder de = bd.element("IMPORTDATA");
			XMLBuilder req = de.element("REQUESTDESC");
			XMLBuilder rep = req.element("REPORTNAME");

			rep.t("All Masters"); // All Masters Units

			XMLBuilder stv = req.element("STATICVARIABLES");
			XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
			cc.t(CompanyName);
			XMLBuilder idu = stv.element("IMPORTDUPS");
			idu.t("@@DUPCOMBINE");

			XMLBuilder dt = bd.element("DATA");
			XMLBuilder tm = dt.element("TALLYMESSAGE");
			// ****Start from Here ********//

			XMLBuilder lg = tm.element("COSTCENTRE");

			lg.a("NAME", goDownName);
			lg.a("RESERVEDNAME", "");

			XMLBuilder cg = tm.element("CATEGORY");
			cg.t("Primary Cost Category");

			XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
			XMLBuilder langnmlst = langlst.element("NAME.LIST");
			langlst.a("TYPE", "String");
			XMLBuilder lname = langnmlst.element("NAME");
			lname.t(goDownName);
			// XMLBuilder alname = langnmlst.element("NAME");
			// alname.t(AccountName);

			Properties outputProperties = new Properties();
			// Explicitly identify the output as an XML document
			outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
			// Pretty-print the XML output (doesn't work in all cases)
			outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
			// Get 2-space indenting when using the Apache transformer
			outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
			// Omit the XML declaration header
			outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

			PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
			en.toWriter(writer, outputProperties);
			writer.close();

		} catch (Exception e) {
			System.out.println(e.toString());
			return false;
		}

		return true;
	}

	private boolean CreateSupplier(String AcName, String TinNo, String Address, String Alias, String ParentAccountHead,
			String TallyServer, String CompanyName, String affectstock, String PinCode, String country,
			String dealerType, String state) {

		String outFile = "";

		String iNfileName = tally_xml_outgoing_folder + File.separatorChar + "supplier" + AcName + ".xml";

		try {

			XMLBuilder en = XMLBuilder.create("ENVELOPE");
			XMLBuilder Hd = en.element("HEADER");
			XMLBuilder vr = Hd.element("VERSION");
			vr.t("1");
			XMLBuilder TRq = Hd.element("TALLYREQUEST");
			TRq.t("import");
			XMLBuilder tp = Hd.element("TYPE");
			tp.t("Data");
			XMLBuilder id = Hd.element("ID");
			id.t("All Masters");
			XMLBuilder bd = en.element("BODY");
			XMLBuilder de = bd.element("DESC");
			XMLBuilder stv = de.element("STATICVARIABLES");
			XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
			cc.t(CompanyName);
			XMLBuilder idu = stv.element("IMPORTDUPS");
			idu.t("@@DUPCOMBINE");

			XMLBuilder dt = bd.element("DATA");
			XMLBuilder tm = dt.element("TALLYMESSAGE");
			XMLBuilder lg = tm.element("LEDGER");
			lg.a("Action", "Create");
			lg.a("NAME", AcName);
			XMLBuilder nm = lg.element("NAME");
			nm.t(AcName);
			XMLBuilder pt = lg.element("PARENT");
			pt.t(ParentAccountHead);
			XMLBuilder adlst = lg.element("ADDRESS.LIST");
			adlst.a("TYPE", "String");
			if (null != Address) {
				XMLBuilder adr = adlst.element("ADDRESS");
				adr.t(Address);
			}

			XMLBuilder COUNTRYOFRESIDENCE = lg.element("COUNTRYOFRESIDENCE");
			COUNTRYOFRESIDENCE.t("India");

			XMLBuilder COUNTRYNAME = lg.element("COUNTRYNAME");
			COUNTRYNAME.t("India");

			XMLBuilder GSTREGISTRATIONTYPE = lg.element("GSTREGISTRATIONTYPE");

			if (null != TinNo && TinNo.length() > 7) {
				GSTREGISTRATIONTYPE.t("Regular");
			} else {
				GSTREGISTRATIONTYPE.t("Unregistered");
			}

			XMLBuilder PARTYGSTIN = lg.element("PARTYGSTIN");
			if (null == TinNo) {
				TinNo = "";
			}
			PARTYGSTIN.t(TinNo);

			XMLBuilder LEDSTATENAME = lg.element("LEDSTATENAME");

			LEDSTATENAME.t("Kerala");

			XMLBuilder mailname = adlst.element("MAILINGNAME.LIST");
			mailname.a("TYPE", "String");
			XMLBuilder maname = adlst.element("MAILINGNAME");
			maname.t(AcName);

			if (null != TinNo) {
				XMLBuilder intax = lg.element("VATTINNUMBER");
				intax.t(TinNo);
			}

			if (null != PinCode) {
				XMLBuilder intax = lg.element("PINCODE");
				intax.t(PinCode);
			}
			if (null != country) {
				XMLBuilder intax = lg.element("COUNTRYNAME");
				intax.t(country);
			}
			if (null != dealerType) {
				XMLBuilder intax = lg.element("VATDEALERTYPE");
				intax.t(dealerType);
			}

			if (null != state) {
				XMLBuilder intax = lg.element("LEDSTATENAME");
				intax.t(state);
			}
			XMLBuilder afstk = lg.element("AFFECTSSTOCK");
			afstk.t(affectstock);

			XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
			XMLBuilder langnmlst = lg.element("NAME.LIST");
			langlst.a("TYPE", "String");
			XMLBuilder lname = langnmlst.element("NAME");
			lname.t(AcName);
			if (null != Alias) {
				XMLBuilder alname = langnmlst.element("NAME");
				alname.t(Alias);
			}

			Properties outputProperties = new Properties();
			// Explicitly identify the output as an XML document
			outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
			// Pretty-print the XML output (doesn't work in all cases)
			outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
			// Get 2-space indenting when using the Apache transformer
			outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
			// Omit the XML declaration header
			outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

			PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
			en.toWriter(writer, outputProperties);
			writer.close();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean CreateVoucherTypeXML(String VoucherName, String ParentName, String NumberingMethod,
			String PreventDuplicate, String TallyServer, String CompanyName) {

		// String iNfileName = "vouchertype.xml";

		String iNfileName = tally_xml_outgoing_folder + File.separatorChar + "VoucherName" + VoucherName + ".xml";

		try {

			XMLBuilder en = XMLBuilder.create("ENVELOPE");
			XMLBuilder Hd = en.element("HEADER");
			// XMLBuilder vr = Hd.element("VERSION");

			// vr.t("1");
			XMLBuilder TRq = Hd.element("TALLYREQUEST");
			TRq.t("import Data");

			XMLBuilder bd = en.element("BODY");
			XMLBuilder de = bd.element("IMPORTDATA");
			XMLBuilder req = de.element("REQUESTDESC");
			XMLBuilder rep = req.element("REPORTNAME");

			rep.t("All Masters"); // All Masters Units

			XMLBuilder stv = req.element("STATICVARIABLES");
			XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
			cc.t(CompanyName);
			XMLBuilder idu = stv.element("IMPORTDUPS");
			idu.t("@@DUPCOMBINE");

			XMLBuilder dt = bd.element("DATA");
			XMLBuilder tm = dt.element("TALLYMESSAGE");
			// ****Start from Here ********//

			XMLBuilder lg = tm.element("VOUCHERTYPE");

			lg.a("NAME", VoucherName);
			lg.a("RESERVEDNAME", "");
			XMLBuilder unm = lg.element("PARENT");

			unm.t(ParentName);
			XMLBuilder nm = lg.element("MAILINGNAME");
			nm.t(ParentName);
			XMLBuilder pt = lg.element("NUMBERINGMETHOD");
			pt.t(NumberingMethod);
			XMLBuilder issim = lg.element("ISDEEMEDPOSITIVE");
			issim.t("Yes");
			XMLBuilder prevdup = lg.element("PREVENTDUPLICATES");
			prevdup.t(PreventDuplicate);

			XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
			XMLBuilder langnmlst = langlst.element("NAME.LIST");
			langlst.a("TYPE", "String");
			XMLBuilder lname = langnmlst.element("NAME");
			lname.t(VoucherName);
			// XMLBuilder alname = langnmlst.element("NAME");
			// alname.t(AccountName);

			Properties outputProperties = new Properties();
			// Explicitly identify the output as an XML document
			outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
			// Pretty-print the XML output (doesn't work in all cases)
			outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
			// Get 2-space indenting when using the Apache transformer
			outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
			// Omit the XML declaration header
			outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

			PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
			en.toWriter(writer, outputProperties);
			writer.close();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public void NewItemVerson5(String StockItem, String TallyServer, double vat, String Unit, String barcode,
			String CompanyName, String affectstock, String parentgroup, String hsnCode, String GSTDTLAPPLICABLEFROM) {
		// REGY

		String outFile = "";

		double cess = 0;

		String iNfileName = tally_xml_outgoing_folder + File.separatorChar + "StockItem" + StockItem + ".xml";

		BigDecimal bdIgst = new BigDecimal(vat);
		bdIgst = bdIgst.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		BigDecimal bdCess = new BigDecimal(cess);
		bdCess = bdCess.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		double sgst = vat / 2;
		double cgst = vat / 2;

		BigDecimal bdSgst = new BigDecimal(sgst);
		BigDecimal bdCgst = new BigDecimal(cgst);

		bdSgst = bdSgst.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		bdCgst = bdCgst.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		try {

			XMLBuilder en = XMLBuilder.create("ENVELOPE");
			XMLBuilder Hd = en.element("HEADER");
			XMLBuilder vr = Hd.element("VERSION");
			vr.t("1");
			XMLBuilder TRq = Hd.element("TALLYREQUEST");
			TRq.t("import");
			XMLBuilder tYP = Hd.element("TYPE");
			tYP.t("Data");
			XMLBuilder rid = Hd.element("ID");
			rid.t("All Masters");

			XMLBuilder id = en.element("BODY");
			XMLBuilder rd = id.element("DESC");
			XMLBuilder stv = rd.element("STATICVARIABLES");
			XMLBuilder imp = stv.element("IMPORTDUPS");
			imp.t("@@DUPIGNORECOMBINE");
			XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
			cc.t(CompanyName);

			XMLBuilder rdata = id.element("DATA");
			XMLBuilder tm = rdata.element("TALLYMESSAGE");
			XMLBuilder mstr = tm.element("STOCKITEM");
			mstr.a("Action", "Create");
			mstr.a("NAME", StockItem);

			XMLBuilder pr = mstr.element("PARENT");
			pr.t(parentgroup);
			// XMLBuilder namelst = mstr.element("NAME.LIST");

			// XMLBuilder name1 = namelst.element("NAME");
			// name1.t(StockItem);
			// XMLBuilder name2 = namelst.element("NAME");
			// name2.t(barcode);
			XMLBuilder base = mstr.element("BASEUNITS");

			String str1 = StockItem;

			base.t(Unit);

			// XMLBuilder vatrate = namelst.element("RATEOFVAT");
			// vatrate.t(vat+" ");

			XMLBuilder gstapl = mstr.element("GSTAPPLICABLE");

			gstapl.t("Applicable");
			XMLBuilder gstsply = mstr.element("GSTTYPEOFSUPPLY");

			gstsply.t("Goods");

			// XMLBuilder vatapl = mstr.element("VATAPPLICABLE");
			// vatapl.t("Not Applicable");

			XMLBuilder gstdtl = mstr.element("GSTDETAILS.LIST");
			XMLBuilder aplfrom = gstdtl.element("APPLICABLEFROM");
			aplfrom.t(GSTDTLAPPLICABLEFROM);
			XMLBuilder gslcalc = gstdtl.element("CALCULATIONTYPE");
			gslcalc.t("On Value");

			XMLBuilder gsttax = gstdtl.element("TAXABILITY");

			if (vat == 0) {
				gsttax.t("Exempt");
			} else {
				gsttax.t("Taxable");
			}

			XMLBuilder nongst = gstdtl.element("ISNONGSTGOODS");
			nongst.t("No");

			XMLBuilder statedtl = gstdtl.element("STATEWISEDETAILS.LIST");
			XMLBuilder stname = statedtl.element("STATENAME");
			stname.t("Any");

			XMLBuilder RATEDETAILSLIST = statedtl.element("RATEDETAILS.LIST");
			XMLBuilder GSTRATEDUTYHEAD = RATEDETAILSLIST.element("GSTRATEDUTYHEAD");
			GSTRATEDUTYHEAD.t("Central Tax");
			XMLBuilder GSTRATEVALUATIONTYPE = RATEDETAILSLIST.element("GSTRATEVALUATIONTYPE");
			GSTRATEVALUATIONTYPE.t("Based on Value");
			XMLBuilder GSTRATE = RATEDETAILSLIST.element("GSTRATE");
			GSTRATE.t(bdCgst.toPlainString());

			XMLBuilder RATEDETAILSLIST2 = statedtl.element("RATEDETAILS.LIST");
			XMLBuilder GSTRATEDUTYHEAD2 = RATEDETAILSLIST2.element("GSTRATEDUTYHEAD");
			GSTRATEDUTYHEAD2.t("State Tax");
			XMLBuilder GSTRATEVALUATIONTYPE2 = RATEDETAILSLIST2.element("GSTRATEVALUATIONTYPE");
			GSTRATEVALUATIONTYPE2.t("Based on Value");
			XMLBuilder GSTRATE2 = RATEDETAILSLIST2.element("GSTRATE");
			GSTRATE2.t(bdSgst.toPlainString());

			XMLBuilder RATEDETAILSLIST3 = statedtl.element("RATEDETAILS.LIST");
			XMLBuilder GSTRATEDUTYHEAD3 = RATEDETAILSLIST3.element("GSTRATEDUTYHEAD");
			GSTRATEDUTYHEAD3.t("Integrated Tax");
			XMLBuilder GSTRATEVALUATIONTYPE3 = RATEDETAILSLIST3.element("GSTRATEVALUATIONTYPE");
			GSTRATEVALUATIONTYPE3.t("Based on Value");
			XMLBuilder GSTRATE3 = RATEDETAILSLIST3.element("GSTRATE");
			GSTRATE3.t(bdIgst.toPlainString());

			XMLBuilder RATEDETAILSLIST4 = statedtl.element("RATEDETAILS.LIST");
			XMLBuilder GSTRATEDUTYHEAD4 = RATEDETAILSLIST4.element("GSTRATEDUTYHEAD");
			GSTRATEDUTYHEAD4.t("Cess");
			XMLBuilder GSTRATEVALUATIONTYPE4 = RATEDETAILSLIST4.element("GSTRATEVALUATIONTYPE");
			GSTRATEVALUATIONTYPE4.t("Based on Value");
			XMLBuilder GSTRATE4 = RATEDETAILSLIST4.element("GSTRATE");
			GSTRATE4.t(bdCess.toPlainString());

			XMLBuilder HSN = gstdtl.element("HSNCODE");

			HSN.t(hsnCode);

			XMLBuilder LANGUAGENAMELIST = mstr.element("LANGUAGENAME.LIST");
			XMLBuilder NAMELIST = LANGUAGENAMELIST.element("NAME.LIST");
			NAMELIST.a("TYPE", "String");
			XMLBuilder NAME = NAMELIST.element("NAME");
			NAME.t(StockItem);

			Properties outputProperties = new Properties();
			// Explicitly identify the output as an XML document
			outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
			// Pretty-print the XML output (doesn't work in all cases)
			outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
			// Get 2-space indenting when using the Apache transformer
			outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
			// Omit the XML declaration header
			outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

			PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
			en.toWriter(writer, outputProperties);
			writer.close();

			String search = "<GSTAPPLICABLE>Applicable</GSTAPPLICABLE>"; // <- changed to work with String.replaceAll()
			String replacement = "<GSTAPPLICABLE>&#4; Applicable</GSTAPPLICABLE>";

			outFile = "xout.xml";
			setAmbersandForGST(iNfileName, search, replacement, outFile);

			// <GSTAPPLICABLE>Applicable</GSTAPPLICABLE>
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

	private void setAmbersandForGST(String fileName, String search, String replacement, String outFile)
			throws FileNotFoundException {

		try {
			PrintWriter writer = new PrintWriter(outFile, "UTF-8");

			File log = new File(fileName);

			String search2 = "<STATENAME>Any</STATENAME>";
			String replace2 = "<STATENAME>&#4; Any</STATENAME>";

			String search3 = "<GSTAPPLICABLE>Applicable</GSTAPPLICABLE>"; // <- changed to work with String.replaceAll()
			String replacemen3 = "<GSTAPPLICABLE>&#4; Applicable</GSTAPPLICABLE>";

			FileReader fr = new FileReader(log);
			String s;

			BufferedReader br = new BufferedReader(fr);

			while ((s = br.readLine()) != null) {
				s = s.replaceAll(search2, replace2);
				s = s.replaceAll(search3, replacemen3);
				writer.print(s);
			}
			writer.close();
		} catch (Exception e) {

		}
	}

	public boolean NewStockGroup(String StockItem, String TallyServer, String Unit, String barcode, String CompanyName,
			String affectstock) {

		String iNfileName = tally_xml_outgoing_folder + File.separatorChar + "NewStockGroup" + StockItem + ".xml";

		try {

			XMLBuilder en = XMLBuilder.create("ENVELOPE");
			XMLBuilder Hd = en.element("HEADER");
			XMLBuilder vr = Hd.element("VERSION");
			vr.t("1");
			XMLBuilder TRq = Hd.element("TALLYREQUEST");
			TRq.t("import");
			XMLBuilder tYP = Hd.element("TYPE");
			tYP.t("Data");
			XMLBuilder rid = Hd.element("ID");
			rid.t("All Masters");

			XMLBuilder id = en.element("BODY");
			XMLBuilder rd = id.element("DESC");
			XMLBuilder stv = rd.element("STATICVARIABLES");
			XMLBuilder imp = stv.element("IMPORTDUPS");
			imp.t("@@DUPIGNORECOMBINE");
			XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
			cc.t(CompanyName);

			XMLBuilder rdata = id.element("DATA");
			XMLBuilder tm = rdata.element("TALLYMESSAGE");
			XMLBuilder mstr = tm.element("STOCKGROUP");
			mstr.a("Action", "Create");
			mstr.a("NAME", StockItem);
			XMLBuilder namelst = mstr.element("NAME.LIST");
			XMLBuilder name1 = namelst.element("NAME");
			name1.t(StockItem);
			// XMLBuilder name2 = namelst.element("NAME");
			// name2.t(barcode);
			XMLBuilder base = namelst.element("BASEUNITS");

			// String str1 = StockItem;

			// base.t(Unit);

			// XMLBuilder vatrate = namelst.element("RATEOFVAT");
			// vatrate.t(vat+" ");

			Properties outputProperties = new Properties();
			// Explicitly identify the output as an XML document
			outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
			// Pretty-print the XML output (doesn't work in all cases)
			outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
			// Get 2-space indenting when using the Apache transformer
			outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
			// Omit the XML declaration header
			outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

			PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
			en.toWriter(writer, outputProperties);
			writer.close();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean CreateUnitXML(String UnitName, String DecimalPalce, String FormalName, String TallyServer,
			String CompanyName) {

		String iNfileName = "Unit" + UnitName + ".xml";
		try {

			XMLBuilder en = XMLBuilder.create("ENVELOPE");
			XMLBuilder Hd = en.element("HEADER");
			// XMLBuilder vr = Hd.element("VERSION");

			// vr.t("1");
			XMLBuilder TRq = Hd.element("TALLYREQUEST");
			TRq.t("import Data");

			XMLBuilder bd = en.element("BODY");
			XMLBuilder de = bd.element("IMPORTDATA");
			XMLBuilder req = de.element("REQUESTDESC");
			XMLBuilder rep = req.element("REPORTNAME");

			rep.t("All Masters"); // All Masters Units

			XMLBuilder stv = req.element("STATICVARIABLES");
			XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
			cc.t(CompanyName);
			XMLBuilder idu = stv.element("IMPORTDUPS");
			idu.t("@@DUPCOMBINE");

			XMLBuilder dt = bd.element("DATA");
			XMLBuilder tm = dt.element("TALLYMESSAGE");

			XMLBuilder lg = tm.element("UNIT");

			lg.a("NAME", UnitName);
			lg.a("RESERVEDNAME", "");
			XMLBuilder unm = lg.element("NAME");

			unm.t(UnitName);
			XMLBuilder nm = lg.element("ORIGINALNAME");
			nm.t(FormalName);
			XMLBuilder pt = lg.element("DECIMALPLACES");
			pt.t(DecimalPalce);
			XMLBuilder issim = lg.element("ISSIMPLEUNIT");
			issim.t("Yes");

			Properties outputProperties = new Properties();
			// Explicitly identify the output as an XML document
			outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
			// Pretty-print the XML output (doesn't work in all cases)
			outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
			// Get 2-space indenting when using the Apache transformer
			outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
			// Omit the XML declaration header
			outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

			PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
			en.toWriter(writer, outputProperties);
			writer.close();

		} catch (Exception e) {
			System.out.println(e.toString());
			return false;
		}
		return true;
	}

}
