package com.maple.restserver.analytics.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.analytics.repository.PurchaseSummaryDailyRepository;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ExpenseSummaryDailyReport;
import com.maple.restserver.report.entity.PurchaseSummaryReport;
import com.maple.restserver.repository.BranchMstRepository;
@Service
@Transactional
@Component
public class PurchaseSummaryDailyServiceImpl  implements PurchaseSummaryDailyService{

	@Autowired
	BranchMstRepository branchMstRepository;
	
	@Autowired
	PurchaseSummaryDailyRepository purchaseSummaryDailyRepository;
	@Override
	public List<PurchaseSummaryReport> getPurchaseSummaryMonthlyReport(Date fromdate, Date todate,
			CompanyMst companyMst) {

		 List<PurchaseSummaryReport> reportList=new ArrayList();
			List<BranchMst>branchMstList=branchMstRepository.findAll();
			
			
		//	for(BranchMst branch :branchMstList) {
				
			//	Optional<BranchMst> branchMstOpt=branchMstRepository.findById(branch.getId());
				
				List<Object> obj=purchaseSummaryDailyRepository.getSummaryMonthlyReport(fromdate,todate,companyMst);
	
				
				 for(int i=0;i<obj.size();i++)
				 {
					 Object[] objAray = (Object[]) obj.get(i);
					 PurchaseSummaryReport report = new PurchaseSummaryReport();
					 report.setBranchCode((String) objAray[1]);
					 report.setBranchPurchaseAmount((BigDecimal) objAray[0]);
					 
				//	 BigDecimal sumOfPurchase=purchaseSummaryDailyRepository.getSumOfPurchase(fromdate,todate,companyMst);
				//	 report.setCompanyPurchaseAmount(sumOfPurchase);
					 reportList.add(report);
				 }
					
			return reportList;
	}

}
