package com.maple.restserver.analytics.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.analytics.repository.SalesSummaryDailyRepository;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.PurchaseSummaryReport;
import com.maple.restserver.report.entity.SalesSummaryDailyReport;
import com.maple.restserver.repository.BranchMstRepository;
@Service
@Transactional
@Component
public class SalesSummaryDailyServiceImpl implements SalesSummaryDailyService{

	
	@Autowired
	BranchMstRepository branchMstRepository;
	@Autowired
	SalesSummaryDailyRepository salesSummaryDailyRepository;
	@Override
	public List<SalesSummaryDailyReport> getSalesSummaryMonthlyReport(Date fromdate, Date todate, 
			CompanyMst companyMst) {

		 List<SalesSummaryDailyReport> reportList=new ArrayList();
			List<BranchMst>branchMstList=branchMstRepository.findAll();
			
			
			for(BranchMst branch :branchMstList) {
				
				Optional<BranchMst> branchMstOpt=branchMstRepository.findById(branch.getId());
				
				List<Object> obj=salesSummaryDailyRepository.getSalesSummaryMonthlyReport(fromdate,todate,branchMstOpt.get(),companyMst);
				 for(int i=0;i<obj.size();i++)
				 {
					 Object[] objAray = (Object[]) obj.get(i);
					 SalesSummaryDailyReport report = new SalesSummaryDailyReport();
					 report.setBranchCode((String) objAray[1]);
					 report.setBranchSales((BigDecimal) objAray[0]);
					// BigDecimal sumOfSales=salesSummaryDailyRepository.getSumOfSales(fromdate,todate,companyMst);
					// report.setCompanySales(sumOfSales);
					 reportList.add(report);
				 }
			}
			return reportList;
		
	}

}
