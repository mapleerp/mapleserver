package com.maple.restserver.analytics.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.maple.restserver.RestserverApplication;
import com.maple.restserver.accounting.entity.InventoryLedgerMst;
import com.maple.restserver.accounting.repository.InventoryLedgerMstRepository;
import com.maple.restserver.analytics.entity.ItemBatchCostingMst;
import com.maple.restserver.analytics.entity.YtdItemProfit;
import com.maple.restserver.analytics.repository.ExpenseSummaryDailyRepository;
import com.maple.restserver.analytics.repository.ItemBatchCostingMstRepository;
import com.maple.restserver.analytics.repository.YtdItemProfitRepository;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.jms.send.KafkaMapleEvent;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.report.entity.ExpenseSummaryDailyReport;
import com.maple.restserver.repository.BranchMstRepository;


@Service
@Transactional
@Component
public class ProcessTransactionServiceImpl implements ProcessTransactionService {
	private static final Logger logger = LoggerFactory.getLogger(RestserverApplication.class);
	
	
	
	@Autowired
	ItemBatchCostingMstRepository itemBatchCostingMstRepository;
	
	
 
	
	
	@Autowired
	InventoryLedgerMstRepository inventoryLedgerMstRepository;
	
	@Autowired
	YtdItemProfitRepository  ytdItemProfitRepository;
 
	@Override
	public void processTransactionFromKafkaEvent(KafkaMapleEvent kafkaMapleEvent) {
		
		logger.info("processing transaction from Kafka " );

		String payLoad = "";
		KafkaMapleEventType objectType = null;

		ObjectMapper mapper = new ObjectMapper()
				 .registerModule(new ParameterNamesModule())
				   .registerModule(new Jdk8Module())
				   .registerModule(new JavaTimeModule());
				 
 
		payLoad = kafkaMapleEvent.getPayLoad();
		objectType = kafkaMapleEvent.getLibraryEventType();

		logger.info("objectType : {} ",objectType );
		
		switch (objectType) {
		case SALESTRANSHDR:

			try {
				SalesTransHdr salesTransHdr = mapper.readValue(payLoad, SalesTransHdr.class);

				logger.info("ProcessTransactionServiceImpl SalesTransHdr : {} ", salesTransHdr);

				 

			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
			
		case SALESDTL:

			try {
				SalesDtl salesDtl = mapper.readValue(payLoad, SalesDtl.class);
				
				SalesTransHdr salesTransHdr = salesDtl.getSalesTransHdr();
			
				double Rate = salesDtl.getRate();
				// get cost
				ItemBatchCostingMst itemBatchCostingMst  = itemBatchCostingMstRepository.findByItemIdAndBatchCodeAndCompanyMstAndBranchCode(
						salesDtl.getItemId(),
						salesDtl.getBatch(),
						salesTransHdr.getCompanyMst(),
						salesTransHdr.getBranchCode());
				
				double profit = Rate - itemBatchCostingMst.getCostprice();
				double qty  = salesDtl.getQty();
				profit = profit * qty;
				

				YtdItemProfit ytdItemProfit = new YtdItemProfit();
				
				ytdItemProfit.setBatchCode(salesDtl.getBatch());
				ytdItemProfit.setSourceDtlId(salesDtl.getId());
				ytdItemProfit.setBranchCode(salesTransHdr.getBranchCode());
				ytdItemProfit.setCompanyMst(salesTransHdr.getCompanyMst());
				ytdItemProfit.setItemId(salesDtl.getItemId());
				
				ytdItemProfit.setFinYear(profit);
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(salesTransHdr.getVoucherDate());
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				int year = cal.get(Calendar.YEAR);
				
				switch (month) {
				case 0 :
					ytdItemProfit.setMonth1(profit);
					
					break;
					
				case 1 :
					ytdItemProfit.setMonth2(profit);
					
					break;
				case 2 :
					ytdItemProfit.setMonth3(profit);
					
					break;
				case 3:
					ytdItemProfit.setMonth4(profit);
					
					break;
				case 4 :
					ytdItemProfit.setMonth5(profit);
					
					break;
					
				case 5 :
					ytdItemProfit.setMonth6(profit);
					
					break;
				case 6 :
					ytdItemProfit.setMonth7(profit);
					
					break;
				case 7:
					ytdItemProfit.setMonth8(profit);
					
					break;
				case 8 :
					ytdItemProfit.setMonth9(profit);
					
					break;
				case 9 :
					ytdItemProfit.setMonth10(profit);
					
					break;
				case 10:
					ytdItemProfit.setMonth11(profit);
					
					break;
				case 12:
					ytdItemProfit.setMonth12(profit);
					
					break;
				}
				
				ytdItemProfitRepository.save(ytdItemProfit);
				
				
				logger.info("ProcessTransactionServiceImpl ytdItemProfit : {} ", ytdItemProfit);

				 

			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case INVENTORYLEDGERMST:
			
			logger.info("INVENTORYLEDGERMST   ");
			ItemBatchCostingMst itemBatchCostingMst = new ItemBatchCostingMst();
			try {
				InventoryLedgerMst inventoryLedgerMst = mapper.readValue(payLoad, InventoryLedgerMst.class);

				logger.info("ProcessTransactionServiceImpl InventoryLedgerMst : {} ", inventoryLedgerMst);

				// check if Dr entry. If so , adjust the costing.
				
				BigDecimal DrAmount = inventoryLedgerMst.getDrAmount();
				double DrQty = inventoryLedgerMst.getDrQty();
				
				logger.info("DrQty {}   ",DrQty);
				
				if(DrAmount.doubleValue()>0 && DrQty>0) {
					String batch = inventoryLedgerMst.getBatch();
					logger.info("batch {}   ",batch);
					
					ItemMst itemMst = inventoryLedgerMst.getItemMst();
					
					logger.info("itemMst {}   ",itemMst);
					
					double sumQty  = inventoryLedgerMstRepository.findBySumQtyItemMstAndatchAndCompanyMstAndBranchMst(
							itemMst.getId(),batch,inventoryLedgerMst.getCompanyMst().getId(),
					inventoryLedgerMst.getBranchMst().getId());
	
					logger.info("sumQty {}   ",sumQty);
					
					double sumAmt  = inventoryLedgerMstRepository.findBySumAmtItemMstAndatchAndCompanyMstAndBranchMst(
							itemMst.getId(),batch,inventoryLedgerMst.getCompanyMst().getId(),
					inventoryLedgerMst.getBranchMst().getId());
	
					logger.info("sumAmt {}   ",sumAmt);
					double costPrice = DrAmount.doubleValue() / DrQty;
					
					logger.info("costPrice {}   ",costPrice);
					// update cost proce for the batch
					
					
				 
					itemBatchCostingMst  = itemBatchCostingMstRepository.findByItemIdAndBatchCodeAndCompanyMstAndBranchCode(
							itemMst.getId(),batch,inventoryLedgerMst.getCompanyMst(),
							inventoryLedgerMst.getBranchMst().getBranchCode());
					
					logger.info("itemBatchCostingMst {}   ",itemBatchCostingMst);
					if(null==itemBatchCostingMst || null==itemBatchCostingMst.getId()) {
						
						if(null==  itemBatchCostingMst  ) {
							  itemBatchCostingMst = new ItemBatchCostingMst();
						}
						itemBatchCostingMst.setBatchCode(batch);
						itemBatchCostingMst.setBranchCode(inventoryLedgerMst.getBranchMst().getBranchCode());
						itemBatchCostingMst.setCompanyMst(inventoryLedgerMst.getCompanyMst());
						itemBatchCostingMst.setCostprice(costPrice);
						itemBatchCostingMst.setItemId(itemMst.getId());
						
					}else {
						itemBatchCostingMst.setCostprice(costPrice);
					}
					
					logger.info("before save itemBatchCostingMst {}   ",itemBatchCostingMst);
					
					itemBatchCostingMstRepository.save(itemBatchCostingMst);
					
				}
				

			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
			
		}
		
	}

	 
}
