package com.maple.restserver.analytics.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.analytics.repository.ExpenseSummaryDailyRepository;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ExpenseSummaryDailyReport;
import com.maple.restserver.repository.BranchMstRepository;
@Service
@Transactional
@Component
public class ExpenseSummaryDailyServiceImpl implements ExpenseSummaryDailyService {

	@Autowired
	BranchMstRepository branchMstRepository;
	@Autowired
	ExpenseSummaryDailyRepository expenseSummaryDailyRepository;
	@Override
	public List<ExpenseSummaryDailyReport> getExpenseSummaryMonthlyReport(Date fromdate, Date todate, 
			CompanyMst companyMst) {
		 List<ExpenseSummaryDailyReport> reportList=new ArrayList();
		List<BranchMst>branchMstList=branchMstRepository.findAll();
		
		
		for(BranchMst branch :branchMstList) {
			
			Optional<BranchMst> branchMstOpt=branchMstRepository.findById(branch.getId());
			
			List<Object> obj=expenseSummaryDailyRepository.getExpenseSummaryMonthlyReport(fromdate,todate,branchMstOpt.get(),companyMst);
			 for(int i=0;i<obj.size();i++)
			 {
				 Object[] objAray = (Object[]) obj.get(i);
				 ExpenseSummaryDailyReport report = new ExpenseSummaryDailyReport();
				 report.setBranchCode((String) objAray[1]);
				 report.setSumOfAccountHeadWiseExpense((BigDecimal) objAray[0]);
				 BigDecimal sumOfExpence=expenseSummaryDailyRepository.getSumOfAccountHeadsExpense(fromdate,todate,companyMst);
				 report.setSumOfExpance(sumOfExpence);
				 reportList.add(report);
			 }
		}
		
		
		return reportList;
	}

}
