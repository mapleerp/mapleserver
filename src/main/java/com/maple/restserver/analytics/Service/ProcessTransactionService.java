package com.maple.restserver.analytics.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.jms.send.KafkaMapleEvent;


public interface ProcessTransactionService {
	
	public void processTransactionFromKafkaEvent(KafkaMapleEvent kafkaMapleEvent);
	 
 
}
