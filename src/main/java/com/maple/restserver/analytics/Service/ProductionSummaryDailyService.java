package com.maple.restserver.analytics.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProductionDtlDtl;
import com.maple.restserver.report.entity.SalesSummaryDailyReport;

public interface ProductionSummaryDailyService {

	List<HashMap<String ,Double>> getProductionAgainstRawMaterial(Date fromdate, Date todate, CompanyMst companyMst,
			String branch);

	List<HashMap<String ,Double>> getProductionRawMaterialValue(Date fromdate, Date todate, CompanyMst companyMst,
			String branch);
	List<HashMap<String,Double>> getsumOfActualQty(Date fudate, Date tudate, String itemid, String branchcode);

	List<HashMap<String, Double>> getsumOfPlaningQty(Date fudate, Date tudate, String itemid, String branchcode);

	
}
