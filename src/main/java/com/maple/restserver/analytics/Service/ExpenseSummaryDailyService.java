package com.maple.restserver.analytics.Service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ExpenseSummaryDailyReport;

public interface ExpenseSummaryDailyService {

	
	List<ExpenseSummaryDailyReport>  getExpenseSummaryMonthlyReport(Date fromdate,Date todate,CompanyMst companyMst);
}
