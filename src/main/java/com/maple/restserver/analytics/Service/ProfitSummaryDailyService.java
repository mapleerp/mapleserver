package com.maple.restserver.analytics.Service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ProfitSummaryDailyReport;

public interface ProfitSummaryDailyService {
	public List<ProfitSummaryDailyReport> getProfitSummaryMonthlyReport(Date fromdate,Date todate,CompanyMst companyMst);
}
