package com.maple.restserver.analytics.Service;

import java.math.BigDecimal;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.analytics.repository.ExpenseSummaryDailyRepository;
import com.maple.restserver.analytics.repository.ProfitSummaryDailyRepository;
import com.maple.restserver.analytics.repository.PurchaseSummaryDailyRepository;
import com.maple.restserver.analytics.repository.SalesSummaryDailyRepository;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ExpenseSummaryDailyReport;
import com.maple.restserver.report.entity.ProfitSummaryDailyReport;
import com.maple.restserver.report.entity.PurchaseSummaryReport;
import com.maple.restserver.report.entity.SalesSummaryDailyReport;
import com.maple.restserver.repository.BranchMstRepository;

@Service 
@Transactional
@Component
public class AnalyticsConsolidateReportServiceImpl implements AnalyticsConsolidatedReportService{
@Autowired
SalesSummaryDailyRepository salesSummaryDailyRepository;
	@Autowired
	PurchaseSummaryDailyRepository purchaseSummaryDailyRepository;
	@Autowired
	ExpenseSummaryDailyRepository expenseSummaryDailyRepository;
	
	@Autowired
	ProfitSummaryDailyRepository profitSummaryDailyRepository;
	
	@Autowired
	BranchMstRepository branchMstRepository;
	@Override
	public Map getConsolidatedReport(Date fromdate, Date todate, CompanyMst companyMst,String branchcodes,String reporttype) {
	
		List<PurchaseSummaryReport> purchaeSummaryList= new ArrayList();
		List<SalesSummaryDailyReport> salesSummaryList= new ArrayList();
		List<ExpenseSummaryDailyReport>expenseSummaryList=new ArrayList();
		List<ProfitSummaryDailyReport>profitSummaryList=new ArrayList();
		Map<String, Object> hashMap=new HashMap<String, Object>();
		List<Object> objSales=new ArrayList<Object>();
		List<Object> objPurchase=new ArrayList<Object>();
		List<Object> objExpense=new ArrayList<Object>();
		List<Object> objProfit=new ArrayList<Object>();
        String barnchCode=branchcodes;
		
		String[]branchArray=barnchCode.split(";");
		if(reporttype.equalsIgnoreCase("allreport")){
		for(String branch:branchArray ) {
			Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
			BranchMst branchMst=branchMstOPt.get();
			
		objSales=salesSummaryDailyRepository.getSalesSummaryReport(fromdate, todate, branchMst.getId(), companyMst);
		objPurchase=purchaseSummaryDailyRepository.getPurchaseSummaryReport(fromdate, todate, companyMst,branchMst.getId());
		objExpense=expenseSummaryDailyRepository.getExpenceSummaryReport(fromdate,todate, companyMst,branchMst.getId());
		objProfit=profitSummaryDailyRepository.getProfitSummaryReport(fromdate,todate, companyMst,branchMst.getId());
		}

		  for(int i=0;i<objSales.size();i++)
			 {
				 Object[] objAray = (Object[]) objSales.get(i);
				 SalesSummaryDailyReport report=new SalesSummaryDailyReport();
				 report.setBranchCode((String) objAray[0]);
				 report.setBranchSales((BigDecimal) objAray[1]);
				 salesSummaryList.add(report);
			 }
		hashMap.put("X", branchArray);
		hashMap.put("Ys", salesSummaryList);
		 for(int i=0;i<objPurchase.size();i++)
		 {
			 Object[] objAray = (Object[]) objPurchase.get(i);
			 PurchaseSummaryReport report=new PurchaseSummaryReport();
			 report.setBranchCode((String) objAray[0]);
			 report.setBranchPurchaseAmount((BigDecimal) objAray[1]);
			 purchaeSummaryList.add(report);
		 }
		 
		hashMap.put("Yp", purchaeSummaryList);
		
			
			  for(int i=0;i<objExpense.size();i++) { Object[] objAray = (Object[])
			  objExpense.get(i); 
			  ExpenseSummaryDailyReport report=new  ExpenseSummaryDailyReport(); 
			  report.setBranchCode((String) objAray[0]);
			  report.setSumOfExpance((BigDecimal) objAray[1]);
			  expenseSummaryList.add(report); }
			  hashMap.put("Ye",expenseSummaryList);
			  
			  for(int i=0;i<objProfit.size();i++) { Object[] objAray = (Object[])
			  objProfit.get(i); 
			  ProfitSummaryDailyReport report=new ProfitSummaryDailyReport(); 
			  report.setBranchCode((String) objAray[0]);
			  report.setProfitSummaryBranch((BigDecimal) objAray[1]);
			  profitSummaryList.add(report); } hashMap.put("Ypro",profitSummaryList);
			 
		}if(reporttype.equalsIgnoreCase("SALES SUMMARY")){
			for(String branch:branchArray ) {
				Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
				BranchMst branchMst=branchMstOPt.get();
				objSales=salesSummaryDailyRepository.getSalesSummaryReport(fromdate, todate,branchMst.getId(), companyMst);
			
				  for(int i=0;i<objSales.size();i++)
					 {
						 Object[] objAray = (Object[]) objSales.get(i);
						 SalesSummaryDailyReport report=new SalesSummaryDailyReport();
						 report.setBranchCode((String) objAray[0]);
						 report.setBranchSales((BigDecimal) objAray[1]);
						 salesSummaryList.add(report);
					 }
				  hashMap.put("X", branchArray);
				  hashMap.put("Ys", salesSummaryList);
			}
			
		}if(reporttype.equalsIgnoreCase("PURCHASE SUMMARY")) {
			for(String branch:branchArray ) {
				Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
				BranchMst branchMst=branchMstOPt.get();
				
			objPurchase=purchaseSummaryDailyRepository.getPurchaseSummaryReport(fromdate, todate, companyMst,branchMst.getId());
			  for(int i=0;i<objPurchase.size();i++)
				 {
				  
					 Object[] objAray = (Object[]) objPurchase.get(i);
					 PurchaseSummaryReport report=new PurchaseSummaryReport();
					 report.setBranchCode((String) objAray[0]);
					 report.setBranchPurchaseAmount((BigDecimal) objAray[1]);
					 purchaeSummaryList.add(report);
				 }
			
			hashMap.put("Yp", purchaeSummaryList);
			hashMap.put("X", branchArray);
		}
			
			
		}if(reporttype.equalsIgnoreCase("EXPENSES SUMMARY")) {
			for(String branch:branchArray ) {
				Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
				BranchMst branchMst=branchMstOPt.get();
				
				objExpense=expenseSummaryDailyRepository.getExpenceSummaryReport(fromdate,todate, companyMst,branchMst.getId());
			
				for(int i=0;i<objExpense.size();i++)
				 {
					 Object[] objAray = (Object[]) objExpense.get(i);
					 ExpenseSummaryDailyReport report=new ExpenseSummaryDailyReport();
					 report.setBranchCode((String) objAray[0]);
					 report.setSumOfExpance((BigDecimal) objAray[1]);
					 expenseSummaryList.add(report);
				 }
				hashMap.put("X", branchArray);
				hashMap.put("Ye",expenseSummaryList);
			
			}
			
		}
		if(reporttype.equalsIgnoreCase("PROFIT SUMMARY")) {
			for(String branch:branchArray ) {
				Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
				BranchMst branchMst=branchMstOPt.get();
				objProfit=profitSummaryDailyRepository.getProfitSummaryReport(fromdate,todate, companyMst,branchMst.getId());

				for(int i=0;i<objProfit.size();i++)
				 {
					 Object[] objAray = (Object[]) objProfit.get(i);
					 ProfitSummaryDailyReport report=new ProfitSummaryDailyReport();
					 report.setBranchCode((String) objAray[0]);
					 report.setProfitSummaryBranch((BigDecimal) objAray[1]);
					 profitSummaryList.add(report);
				 }
				hashMap.put("X", branchArray);
				hashMap.put("Ypro",profitSummaryList);
			}
			
		}
		return hashMap;
	}
	@Override
	public Map getMonthilyCosolidatedReport(Date fromdate, Date todate, CompanyMst companyMst, String branchcodes,
			String reporttype) {
	
		
		

		List<PurchaseSummaryReport> purchaeSummaryList= new ArrayList();
		List<SalesSummaryDailyReport> salesSummaryList= new ArrayList();
		List<ExpenseSummaryDailyReport>expenseSummaryList=new ArrayList();
		List<ProfitSummaryDailyReport>profitSummaryList=new ArrayList();
		Map<String, Object> hashMap=new HashMap<String, Object>();
		List<Object> objSales=new ArrayList<Object>();
		List<Object> objPurchase=new ArrayList<Object>();
		List<Object> objExpense=new ArrayList<Object>();
		List<Object> objProfit=new ArrayList<Object>();
        String barnchCode=branchcodes;
		
		String[]branchArray=barnchCode.split(";");
		if(reporttype.equalsIgnoreCase("allreport")){
		for(String branch:branchArray ) {
			Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
			BranchMst branchMst=branchMstOPt.get();
			
		objSales=salesSummaryDailyRepository.getSalesSummaryReport(fromdate, todate, branchMst.getId(), companyMst);
		objPurchase=purchaseSummaryDailyRepository.getPurchaseSummaryReport(fromdate, todate, companyMst,branchMst.getId());
		objExpense=expenseSummaryDailyRepository.getExpenceSummaryReport(fromdate,todate, companyMst,branchMst.getId());
		objProfit=profitSummaryDailyRepository.getProfitSummaryReport(fromdate,todate, companyMst,branchMst.getId());
		}

		  for(int i=0;i<objSales.size();i++)
			 {
				 Object[] objAray = (Object[]) objSales.get(i);
				 SalesSummaryDailyReport report=new SalesSummaryDailyReport();
				 report.setBranchCode((String) objAray[0]);
				 report.setBranchSales((BigDecimal) objAray[1]);
				 salesSummaryList.add(report);
			 }
		hashMap.put("X", branchArray);
		hashMap.put("Ys", salesSummaryList);
		 for(int i=0;i<objPurchase.size();i++)
		 {
			 Object[] objAray = (Object[]) objPurchase.get(i);
			 PurchaseSummaryReport report=new PurchaseSummaryReport();
			 report.setBranchCode((String) objAray[0]);
			 report.setBranchPurchaseAmount((BigDecimal) objAray[1]);
			 purchaeSummaryList.add(report);
		 }
		 
		hashMap.put("Yp", purchaeSummaryList);
		
			
			  for(int i=0;i<objExpense.size();i++) { Object[] objAray = (Object[])
			  objExpense.get(i); 
			  ExpenseSummaryDailyReport report=new  ExpenseSummaryDailyReport(); 
			  report.setBranchCode((String) objAray[0]);
			  report.setSumOfExpance((BigDecimal) objAray[1]);
			  expenseSummaryList.add(report); }
			  hashMap.put("Ye",expenseSummaryList);
			  
			  for(int i=0;i<objProfit.size();i++) { Object[] objAray = (Object[])
			  objProfit.get(i); 
			  ProfitSummaryDailyReport report=new ProfitSummaryDailyReport(); 
			  report.setBranchCode((String) objAray[0]);
			  report.setProfitSummaryBranch((BigDecimal) objAray[1]);
			  profitSummaryList.add(report); } hashMap.put("Ypro",profitSummaryList);
			 
		}if(reporttype.equalsIgnoreCase("MONTHLY SALES SUMMARY")){
			for(String branch:branchArray ) {
				Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
				BranchMst branchMst=branchMstOPt.get();
				objSales=salesSummaryDailyRepository.getMonthlySalesSummaryReport(fromdate, todate,branchMst.getId(), companyMst);
			
				  for(int i=0;i<objSales.size();i++)
					 {
						 Object[] objAray = (Object[]) objSales.get(i);
						 SalesSummaryDailyReport report=new SalesSummaryDailyReport();
						 report.setBranchCode((String) objAray[0]);
						 report.setBranchSales((BigDecimal) objAray[1]);
						 report.setVoucherDate((Month) objAray[2]);
						 salesSummaryList.add(report);
					 }
				  hashMap.put("X", branchArray);
				  hashMap.put("Ys", salesSummaryList);
			}
			
		}if(reporttype.equalsIgnoreCase("MONTHLY PURCHASE SUMMARY")) {
			for(String branch:branchArray ) {
				Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
				BranchMst branchMst=branchMstOPt.get();
				
			objPurchase=purchaseSummaryDailyRepository.getMonthlyPurchaseSummaryReport(fromdate, todate, companyMst,branchMst.getId());
			  for(int i=0;i<objPurchase.size();i++)
				 {
				  
					 Object[] objAray = (Object[]) objPurchase.get(i);
					 PurchaseSummaryReport report=new PurchaseSummaryReport();
					 report.setBranchCode((String) objAray[0]);
					 report.setBranchPurchaseAmount((BigDecimal) objAray[1]);
					 report.setVoucherDate((Month) objAray[2]);
					 purchaeSummaryList.add(report);
				 }
			
			hashMap.put("Yp", purchaeSummaryList);
			hashMap.put("X", branchArray);
		}
			
			
		}if(reporttype.equalsIgnoreCase("MONTHLY EXPENSES SUMMARY")) {
			for(String branch:branchArray ) {
				Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
				BranchMst branchMst=branchMstOPt.get();
				
				objExpense=expenseSummaryDailyRepository.getMonthExpenceSummaryReport(fromdate,todate, companyMst,branchMst.getId());
			
				for(int i=0;i<objExpense.size();i++)
				 {
					 Object[] objAray = (Object[]) objExpense.get(i);
					 ExpenseSummaryDailyReport report=new ExpenseSummaryDailyReport();
					 report.setBranchCode((String) objAray[0]);
					 report.setSumOfExpance((BigDecimal) objAray[1]);
					 report.setMonthOfExpence((Month) objAray[2]);
					 expenseSummaryList.add(report);
				 }
				hashMap.put("X", branchArray);
				hashMap.put("Ye",expenseSummaryList);
			
			}
			
		}
		if(reporttype.equalsIgnoreCase("MONTHLY PROFIT SUMMARY")) {
			for(String branch:branchArray ) {
				Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
				BranchMst branchMst=branchMstOPt.get();
				objProfit=profitSummaryDailyRepository.getMonthlyProfitSummaryReport(fromdate,todate, companyMst,branchMst.getId());

				for(int i=0;i<objProfit.size();i++)
				 {
					 Object[] objAray = (Object[]) objProfit.get(i);
					 ProfitSummaryDailyReport report=new ProfitSummaryDailyReport();
					 report.setBranchCode((String) objAray[0]);
					 report.setProfitSummaryBranch((BigDecimal) objAray[1]);
					 report.setMonnthOfProfit((Month) objAray[2]);
					 profitSummaryList.add(report);
				 }
				hashMap.put("X", branchArray);
				hashMap.put("Ypro",profitSummaryList);
			}
			
		}
		return hashMap;
	}
	@Override
	public Map getYearlyCosolidatedReport(Date fromdate, Date todate, CompanyMst companyMst, String branchcodes,
			String reporttype) {

		List<PurchaseSummaryReport> purchaeSummaryList= new ArrayList();
		List<SalesSummaryDailyReport> salesSummaryList= new ArrayList();
		List<ExpenseSummaryDailyReport>expenseSummaryList=new ArrayList();
		List<ProfitSummaryDailyReport>profitSummaryList=new ArrayList();
		Map<String, Object> hashMap=new HashMap<String, Object>();
		List<Object> objSales=new ArrayList<Object>();
		List<Object> objPurchase=new ArrayList<Object>();
		List<Object> objExpense=new ArrayList<Object>();
		List<Object> objProfit=new ArrayList<Object>();
        String barnchCode=branchcodes;
		
		String[]branchArray=barnchCode.split(";");
		if(reporttype.equalsIgnoreCase("allreport")){
		for(String branch:branchArray ) {
			Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
			BranchMst branchMst=branchMstOPt.get();
			
		objSales=salesSummaryDailyRepository.getSalesSummaryReport(fromdate, todate, branchMst.getId(), companyMst);
		objPurchase=purchaseSummaryDailyRepository.getPurchaseSummaryReport(fromdate, todate, companyMst,branchMst.getId());
		objExpense=expenseSummaryDailyRepository.getExpenceSummaryReport(fromdate,todate, companyMst,branchMst.getId());
		objProfit=profitSummaryDailyRepository.getProfitSummaryReport(fromdate,todate, companyMst,branchMst.getId());
		}

		  for(int i=0;i<objSales.size();i++)
			 {
				 Object[] objAray = (Object[]) objSales.get(i);
				 SalesSummaryDailyReport report=new SalesSummaryDailyReport();
				 report.setBranchCode((String) objAray[0]);
				 report.setBranchSales((BigDecimal) objAray[1]);
				 salesSummaryList.add(report);
			 }
		hashMap.put("X", branchArray);
		hashMap.put("Ys", salesSummaryList);
		 for(int i=0;i<objPurchase.size();i++)
		 {
			 Object[] objAray = (Object[]) objPurchase.get(i);
			 PurchaseSummaryReport report=new PurchaseSummaryReport();
			 report.setBranchCode((String) objAray[0]);
			 report.setBranchPurchaseAmount((BigDecimal) objAray[1]);
			 purchaeSummaryList.add(report);
		 }
		 
		hashMap.put("Yp", purchaeSummaryList);
		
			
			  for(int i=0;i<objExpense.size();i++) { Object[] objAray = (Object[])
			  objExpense.get(i); 
			  ExpenseSummaryDailyReport report=new  ExpenseSummaryDailyReport(); 
			  report.setBranchCode((String) objAray[0]);
			  report.setSumOfExpance((BigDecimal) objAray[1]);
			  expenseSummaryList.add(report); }
			  hashMap.put("Ye",expenseSummaryList);
			  
			  for(int i=0;i<objProfit.size();i++) { Object[] objAray = (Object[])
			  objProfit.get(i); 
			  ProfitSummaryDailyReport report=new ProfitSummaryDailyReport(); 
			  report.setBranchCode((String) objAray[0]);
			  report.setProfitSummaryBranch((BigDecimal) objAray[1]);
			  profitSummaryList.add(report); } hashMap.put("Ypro",profitSummaryList);
			 
		}if(reporttype.equalsIgnoreCase("YEARLY SALES SUMMARY")){
			for(String branch:branchArray ) {
				Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
				BranchMst branchMst=branchMstOPt.get();
				objSales=salesSummaryDailyRepository.getYearlylySalesSummaryReport(fromdate, todate,branchMst.getId(), companyMst);
			
				  for(int i=0;i<objSales.size();i++)
					 {
						 Object[] objAray = (Object[]) objSales.get(i);
						 SalesSummaryDailyReport report=new SalesSummaryDailyReport();
						 report.setBranchCode((String) objAray[0]);
						 report.setBranchSales((BigDecimal) objAray[1]);
						 report.setYearOfSales((Year) objAray[2]);
						 salesSummaryList.add(report);
					 }
				  hashMap.put("X", branchArray);
				  hashMap.put("Ys", salesSummaryList);
			}
			
		}if(reporttype.equalsIgnoreCase("YEARLY PURCHASE SUMMARY")) {
			for(String branch:branchArray ) {
				Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
				BranchMst branchMst=branchMstOPt.get();
				
			objPurchase=purchaseSummaryDailyRepository.getYearlyPurchaseSummaryReport(fromdate, todate, companyMst,branchMst.getId());
			  for(int i=0;i<objPurchase.size();i++)
				 {
				  
					 Object[] objAray = (Object[]) objPurchase.get(i);
					 PurchaseSummaryReport report=new PurchaseSummaryReport();
					 report.setBranchCode((String) objAray[0]);
					 report.setBranchPurchaseAmount((BigDecimal) objAray[1]);
					 report.setYearOfPurchease((Year) objAray[2]);
					 purchaeSummaryList.add(report);
				 }
			
			hashMap.put("Yp", purchaeSummaryList);
			hashMap.put("X", branchArray);
		}
			
			
		}if(reporttype.equalsIgnoreCase("YEARLY EXPENSES SUMMARY")) {
			for(String branch:branchArray ) {
				Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
				BranchMst branchMst=branchMstOPt.get();
				
				objExpense=expenseSummaryDailyRepository.getYearlyExpenceSummaryReport(fromdate,todate, companyMst,branchMst.getId());
			
				for(int i=0;i<objExpense.size();i++)
				 {
					 Object[] objAray = (Object[]) objExpense.get(i);
					 ExpenseSummaryDailyReport report=new ExpenseSummaryDailyReport();
					 report.setBranchCode((String) objAray[0]);
					 report.setSumOfExpance((BigDecimal) objAray[1]);
					 report.setYearOfExpense((Year) objAray[2]);
					 expenseSummaryList.add(report);
				 }
				hashMap.put("X", branchArray);
				hashMap.put("Ye",expenseSummaryList);
			
			}
			
		}
		if(reporttype.equalsIgnoreCase("YEARLY PROFIT SUMMARY")) {
			for(String branch:branchArray ) {
				Optional<BranchMst> branchMstOPt =branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companyMst.getId());
				BranchMst branchMst=branchMstOPt.get();
				objProfit=profitSummaryDailyRepository.getYearlyProfitSummaryReport(fromdate,todate, companyMst,branchMst.getId());

				for(int i=0;i<objProfit.size();i++)
				 {
					 Object[] objAray = (Object[]) objProfit.get(i);
					 ProfitSummaryDailyReport report=new ProfitSummaryDailyReport();
					 report.setBranchCode((String) objAray[0]);
					 report.setProfitSummaryBranch((BigDecimal) objAray[1]);
					 report.setYearOfProfit((Year) objAray[1]);
					 profitSummaryList.add(report);
				 }
				hashMap.put("X", branchArray);
				hashMap.put("Ypro",profitSummaryList);
			}
			
		}
		return hashMap;
	}

}
