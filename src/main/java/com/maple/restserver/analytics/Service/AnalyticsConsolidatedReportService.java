package com.maple.restserver.analytics.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;

public interface AnalyticsConsolidatedReportService {
	
	
	public Map getConsolidatedReport(Date fromdate,Date todate,CompanyMst companyMst,String branchcodes,String reporttype);




	public Map	getMonthilyCosolidatedReport(Date fromdate,Date todate,CompanyMst companyMst,String branchcodes,String reporttype);

	
	public Map getYearlyCosolidatedReport(Date fromdate,Date todate,CompanyMst companyMst,String branchlist,String reporttype);


}
