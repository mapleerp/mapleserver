package com.maple.restserver.analytics.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.analytics.repository.SalesSummaryDailyRepository;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProductionDtlDtl;
import com.maple.restserver.report.entity.PurchaseSummaryReport;
import com.maple.restserver.report.entity.SalesSummaryDailyReport;
import com.maple.restserver.repository.ActualProductionDtlRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.ProductionDetailRepository;
import com.maple.restserver.repository.ProductionDtlDtlRepository;
@Service
@Transactional
@Component
public class ProductionSummaryDailyServiceImpl implements ProductionSummaryDailyService{
	
	@Autowired
	ProductionDtlDtlRepository productionDtlDtlRepository;
	
	@Autowired
	ProductionDetailRepository productionDetailRepository;
	
	@Autowired
	ActualProductionDtlRepository actualProductionDtlRepository;
	

	@Override
	public List<HashMap<String ,Double>> getProductionAgainstRawMaterial(Date fromdate, Date todate, CompanyMst companyMst,
			String branch) {
		List<HashMap<String ,Double>> listOfDateAndValue = new ArrayList<HashMap<String,Double>>();
		HashMap<String, Double> productionValue= null;
		
		List<Object> objList = actualProductionDtlRepository.
				findProductionValue(fromdate,todate,companyMst,branch);
		
		for(int i=0; i< objList.size(); i++)
		{
			productionValue=new HashMap<String, Double>();
			Object[] objectArray = (Object[]) objList.get(i);
			
			productionValue.put(ClientSystemSetting.UtilDateToString((Date)objectArray[1], "yyyy-MM-dd"), (Double) objectArray[0]);
			listOfDateAndValue.add(productionValue);
		}
		
		
		
		return listOfDateAndValue;
	}

	@Override
	public List<HashMap<String ,Double>> getProductionRawMaterialValue(Date fromdate, Date todate, CompanyMst companyMst,
			String branch) {
		
		HashMap<String, Double> hashMap= null;
	
		List<HashMap<String ,Double>> listOfDateAndValue = new ArrayList<HashMap<String,Double>>();
		
		List<Object> objList = productionDtlDtlRepository.findRawMeterialValue(fromdate,todate,
				companyMst,branch);
		
		
		for(int i=0; i<objList.size(); i++)
		{
			 hashMap=new HashMap<String, Double>();
			 
			 Object[] objAray = (Object[]) objList.get(i);

			 hashMap.put(ClientSystemSetting.UtilDateToString((Date)objAray[1],"yyyy-MM-dd"), (Double) objAray[0]);
			 
			 listOfDateAndValue.add(hashMap);
		}
		return listOfDateAndValue;

	}
	
	@Override
	public List<HashMap<String ,Double>> getsumOfActualQty(Date fudate, Date tudate, String itemid, String branchcode) {
		HashMap<String, Double> hashMap= null;
		List<HashMap<String ,Double>> listOfDateAndValue = new ArrayList<HashMap<String,Double>>();
		
		List<Object> obj = actualProductionDtlRepository.getSumOfActualQty(fudate,tudate,itemid,branchcode);
		 for (int i =0;i<obj.size();i++)
		 {
			 hashMap=new HashMap<String, Double>();
			 Object[] objAray = (Object[]) obj.get(i);
			 hashMap.put(ClientSystemSetting.UtilDateToString((Date) objAray[1],"yyyy-MM-dd"), (Double) objAray[0]);
			 listOfDateAndValue.add(hashMap);
		 }
		
		return listOfDateAndValue;
	}

	@Override
	public List<HashMap<String, Double>> getsumOfPlaningQty(Date fudate, Date tudate, String itemid, String branchcode) {
		HashMap<String, Double> hashMap= null;
		List<HashMap<String ,Double>> listOfDateAndValue = new ArrayList<HashMap<String,Double>>();
		
		List<Object> obj = productionDetailRepository.getSumOfPlaningQty(fudate,tudate,itemid,branchcode);
		 for (int i =0;i<obj.size();i++)
		 {
			 hashMap=new HashMap<String, Double>();
			 Object[] objAray = (Object[]) obj.get(i);
			 hashMap.put(ClientSystemSetting.UtilDateToString((Date) objAray[1],"yyyy-MM-dd"), (Double) objAray[0]);
			 listOfDateAndValue.add(hashMap);
		 }
		
		return listOfDateAndValue;
	}



}
