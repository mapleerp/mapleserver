package com.maple.restserver.analytics.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;

@Entity

public class SalesSummaryDaily implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	private String id;

	Date voucherDate;
	 
	BigDecimal totalSales;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "branchMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private BranchMst branchMst;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public Date getVoucherDate() {
		return voucherDate;
	}


	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}


	public BigDecimal getTotalSales() {
		return totalSales;
	}


	public void setTotalSales(BigDecimal totalSales) {
		this.totalSales = totalSales;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public BranchMst getBranchMst() {
		return branchMst;
	}


	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}


	@Override
	public String toString() {
		return "SalesSummaryDaily [id=" + id + ", voucherDate=" + voucherDate + ", totalSales=" + totalSales
				+ ", companyMst=" + companyMst + ", branchMst=" + branchMst + "]";
	}


}
