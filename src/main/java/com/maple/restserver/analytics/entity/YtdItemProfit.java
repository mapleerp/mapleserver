package com.maple.restserver.analytics.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;

@Entity

public class YtdItemProfit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	private String id;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	String sourceDtlId;
	String branchCode;
	String ItemId;
	String BatchCode;
	double FinYear;
	double Month1;
	double Month2;
	double Month3;
	double Month4;
	double Month5;
	double Month6;
	double Month7;
	double Month8;
	double Month9;
	double Month10;
	double Month11;
	double Month12;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getItemId() {
		return ItemId;
	}
	public void setItemId(String itemId) {
		ItemId = itemId;
	}
	public String getBatchCode() {
		return BatchCode;
	}
	public void setBatchCode(String batchCode) {
		BatchCode = batchCode;
	}
	public double getFinYear() {
		return FinYear;
	}
	public void setFinYear(double finYear) {
		FinYear = finYear;
	}
	public double getMonth1() {
		return Month1;
	}
	public void setMonth1(double month1) {
		Month1 = month1;
	}
	public double getMonth2() {
		return Month2;
	}
	public void setMonth2(double month2) {
		Month2 = month2;
	}
	public double getMonth3() {
		return Month3;
	}
	public void setMonth3(double month3) {
		Month3 = month3;
	}
	public double getMonth4() {
		return Month4;
	}
	public void setMonth4(double month4) {
		Month4 = month4;
	}
	public double getMonth5() {
		return Month5;
	}
	public void setMonth5(double month5) {
		Month5 = month5;
	}
	public double getMonth6() {
		return Month6;
	}
	public void setMonth6(double month6) {
		Month6 = month6;
	}
	public double getMonth7() {
		return Month7;
	}
	public void setMonth7(double month7) {
		Month7 = month7;
	}
	public double getMonth8() {
		return Month8;
	}
	public void setMonth8(double month8) {
		Month8 = month8;
	}
	public double getMonth9() {
		return Month9;
	}
	public void setMonth9(double month9) {
		Month9 = month9;
	}
	public double getMonth10() {
		return Month10;
	}
	public void setMonth10(double month10) {
		Month10 = month10;
	}
	public double getMonth11() {
		return Month11;
	}
	public void setMonth11(double month11) {
		Month11 = month11;
	}
	public double getMonth12() {
		return Month12;
	}
	public void setMonth12(double month12) {
		Month12 = month12;
	}
	
	public String getSourceDtlId() {
		return sourceDtlId;
	}
	public void setSourceDtlId(String sourceDtlId) {
		this.sourceDtlId = sourceDtlId;
	}
	 

	
	
	
	
	
	 

}
