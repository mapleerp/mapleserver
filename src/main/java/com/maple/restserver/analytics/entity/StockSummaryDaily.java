package com.maple.restserver.analytics.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;

@Entity

public class StockSummaryDaily implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	private String id;

	Date voucherDate;
	 
	
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "branchMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private BranchMst branchMst;
	
	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "itemMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ItemMst itemMst;
	
	BigDecimal openingStockValue;
	BigDecimal closingStockValue;
	 
	
	/*
	 * Opening Stock Value and Closing Stock Value will be same when first record is 
	 * inserted into the table.
	 * One each sales of the item closing stock value is decremented.
	 * Workflow system will send message to client anf fetch previous date voucher count, when the day end message arrives. 
	 * If the count match, and if summary is not done, then tables are summarized for the date.
	 * 
	 */
	
	BigDecimal openingQty;
	BigDecimal closingQty;

}
