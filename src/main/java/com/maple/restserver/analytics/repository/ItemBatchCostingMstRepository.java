package com.maple.restserver.analytics.repository;
 

import org.springframework.data.jpa.repository.JpaRepository;
 
import org.springframework.stereotype.Repository;

import com.maple.restserver.analytics.entity.ItemBatchCostingMst;
import com.maple.restserver.analytics.entity.YtdItemProfit;
import com.maple.restserver.entity.CompanyMst;
 

@Repository
public interface ItemBatchCostingMstRepository extends JpaRepository<ItemBatchCostingMst,String> {

	ItemBatchCostingMst findByItemIdAndBatchCodeAndCompanyMstAndBranchCode(String id, String batch,
			CompanyMst companyMst, String branchCode);

	
}
