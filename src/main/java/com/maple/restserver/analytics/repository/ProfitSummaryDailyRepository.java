package com.maple.restserver.analytics.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.analytics.entity.ProfitSummaryDaily;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;

@Repository
public interface ProfitSummaryDailyRepository extends JpaRepository<ProfitSummaryDaily,String> {

	
	@Query(nativeQuery = true,value="select sum(item_wise_profit),branch_mst from profit_summary_daily where branch_mst=:branchMst and company_mst=:companyMst  and date(voucher_date)  between :fromdate and :todate group by branch_mst ")
	List<Object>getProfitSummaryMonthlyReport(Date fromdate,Date todate,BranchMst branchMst,CompanyMst companyMst);

	
	@Query(nativeQuery = true,value="select sum(item_wise_profit) from profit_summary_daily where company_mst=:companyMst and date(voucher_date)  between :fromdate and :todate")
	BigDecimal getSumOfProfit(Date fromdate,Date todate,CompanyMst companyMst);

	@Query(nativeQuery = true,value="select b.branch_code, sum(p.item_wise_profit)  from profit_summary_daily p,branch_mst b company_mst=:companyMst and date(p.voucher_date)  between :fromdate and :todate and b.id=p.branch_mst and branch_mst=:branchmstid group by b.branch_code group by b.branch_code  ")
	List<Object>getProfitSummaryReport(Date fromdate,Date todate,CompanyMst companyMst,String branchmstid);

	
	
	@Query(nativeQuery = true,value="select b.branch_code, sum(p.item_wise_profit),month(p.voucher_date) from profit_summary_daily p,branch_mst b company_mst=:companyMst and date(p.voucher_date)  between :fromdate and :todate and b.id=p.branch_mst and branch_mst=:branchmstid group by b.branch_code group by b.branch_code,month(p.voucher_date) ")
	List<Object>getMonthlyProfitSummaryReport(Date fromdate,Date todate,CompanyMst companyMst,String branchmstid);

	@Query(nativeQuery = true,value="select b.branch_code, sum(p.item_wise_profit),year(p.voucher_date) from profit_summary_daily p,branch_mst b company_mst=:companyMst and date(p.voucher_date)  between :fromdate and :todate and b.id=p.branch_mst and branch_mst=:branchmstid group by b.branch_code group by b.branch_code,year(p.voucher_date) ")
	List<Object>getYearlyProfitSummaryReport(Date fromdate,Date todate,CompanyMst companyMst,String branchmstid);
	

}
