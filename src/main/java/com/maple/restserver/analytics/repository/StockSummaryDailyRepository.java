package com.maple.restserver.analytics.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.analytics.entity.StockSummaryDaily;
@Repository
public interface StockSummaryDailyRepository extends JpaRepository<StockSummaryDaily, String>{

}
