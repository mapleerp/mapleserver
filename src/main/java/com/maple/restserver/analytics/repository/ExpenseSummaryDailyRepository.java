package com.maple.restserver.analytics.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.analytics.entity.ExpenseSummaryDaily;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;

@Repository
public interface ExpenseSummaryDailyRepository extends JpaRepository<ExpenseSummaryDaily,String> {

	@Query(nativeQuery = true,value="select sum(account_heads_wise_expense),branch_mst from expense_summary_daily where branch_mst=:branchMst and company_mst=:companyMst and date(voucher_date)  between :fromdate and :todate group by branch_mst")
	List<Object> getExpenseSummaryMonthlyReport(Date fromdate,Date todate,BranchMst branchMst,CompanyMst companyMst);

@Query(nativeQuery = true,value="select sum(account_heads_wise_expense) where  company_mst=:companyMst and date(voucher_date)  between :fromdate and :todate")
BigDecimal getSumOfAccountHeadsExpense(Date fromdate,Date todate,CompanyMst companyMst);


@Query(nativeQuery = true,value="select b.branch_code , sum(e.account_heads_wise_expense) from expense_summary_daily e,branch_mst b where company_mst=:companyMst and date(voucher_date)  between :fromdate and :todate and e.branch_mst=:branchmstid and b.id=e.branch_mst group by b.branch_code group by b.branch_code ")
List<Object>getExpenceSummaryReport(Date fromdate,Date todate,CompanyMst companyMst,String branchmstid);

@Query(nativeQuery = true,value="select b.branch_code , sum(e.account_heads_wise_expense),month(e.voucher_date) from expense_summary_daily e,branch_mst b where company_mst=:companyMst and date(voucher_date)  between :fromdate and :todate and e.branch_mst=:branchmstid and b.id=e.branch_mst group by b.branch_code group by b.branch_code,month(e.voucher_date) ")
List<Object>getMonthExpenceSummaryReport(Date fromdate,Date todate,CompanyMst companyMst,String branchmstid);


@Query(nativeQuery = true,value="select b.branch_code , sum(e.account_heads_wise_expense),year(e.voucher_date) from expense_summary_daily e,branch_mst b where company_mst=:companyMst and date(voucher_date)  between :fromdate and :todate and e.branch_mst=:branchmstid and b.id=e.branch_mst group by b.branch_code group by b.branch_code,year(e.voucher_date) ")
List<Object>getYearlyExpenceSummaryReport(Date fromdate,Date todate,CompanyMst companyMst,String branchmstid);
}
