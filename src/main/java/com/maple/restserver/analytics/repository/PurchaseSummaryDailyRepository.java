package com.maple.restserver.analytics.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.analytics.entity.PurchaseSummaryDaily;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;

@Repository
public interface PurchaseSummaryDailyRepository extends JpaRepository<PurchaseSummaryDaily, String>{

	
	@Query(nativeQuery = true,value="select p.total_purchase,b.branch_code from purchase_summary_daily p,branch_mst b where  p.company_mst=:companyMst and date(p.voucher_date)  between :fromdate and :todate and b.id=p.branch_mst")
	List<Object>getSummaryMonthlyReport(Date fromdate,Date todate ,CompanyMst companyMst);

@Query(nativeQuery = true,value="select * from purchase_summary_daily ")

	BigDecimal getSumOfPurchase(Date fromdate,Date todate,CompanyMst companyMst);


@Query(nativeQuery = true,value="select b.branch_code,sum(p.total_purchase) from purchase_summary_daily p,branch_mst b where  p.company_mst=:companyMst and date(p.voucher_date)  between :fromdate and :todate and b.id=p.branch_mst and p.branch_mst=:branchmstid group by b.branch_code")
List<Object>getPurchaseSummaryReport(Date fromdate,Date todate,CompanyMst companyMst,String branchmstid);


@Query(nativeQuery = true,value="select b.branch_code,sum(p.total_purchase),month(p.voucher_date) from purchase_summary_daily p,branch_mst b where  p.company_mst=:companyMst and date(p.voucher_date)  between :fromdate and :todate and b.id=p.branch_mst and p.branch_mst=:branchmstid group by b.branch_code,month(p.voucher_date) ")
List<Object> getMonthlyPurchaseSummaryReport(Date fromdate,Date todate,CompanyMst companyMst,String branchmstid);


@Query(nativeQuery = true,value="select b.branch_code,sum(p.total_purchase),year(p.voucher_date) from purchase_summary_daily p,branch_mst b where  p.company_mst=:companyMst and date(p.voucher_date)  between :fromdate and :todate and b.id=p.branch_mst and p.branch_mst=:branchmstid group by b.branch_code,year(p.voucher_date) ")
List<Object>getYearlyPurchaseSummaryReport(Date fromdate,Date todate,CompanyMst companyMst,String branchmstid);
}
