package com.maple.restserver.analytics.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.analytics.entity.SalesSummaryDaily;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;

@Repository
public interface SalesSummaryDailyRepository extends JpaRepository<SalesSummaryDaily, String>{

	@Query(nativeQuery = true,value="select sum(total_sales),branch__mst from sales_summary_daily where branch_mst=:branchMst and company_mat=:companyMstand date(voucher_date)  between :fromdate and :todate group by branch_mst")
	List<Object>getSalesSummaryMonthlyReport(Date fromdate,Date todate,BranchMst branchMst,CompanyMst companyMst);

	@Query(nativeQuery = true,value="select b.branch_code, sum(total_sales) from sales_summary_daily s,branch_mst b where s.company_mst=:companyMst and b.id=s.branch_mst and date(voucher_date)  between :fromdate and :todate and s.branch_mst=:branchmstid group by b.branch_code")
	List<Object>getSalesSummaryReport(Date fromdate, Date todate, String branchmstid,CompanyMst companyMst);


	@Query(nativeQuery = true,value="select b.branch_code, sum(s.total_sales),month(s.voucher_date) from sales_summary_daily s,branch_mst b where s.company_mst=:companyMst and b.id=s.branch_mst and date(voucher_date)  between :fromdate and :todate and s.branch_mst=:branchmstid group by b.branch_code,month(s.voucher_date)")
	List<Object> getMonthlySalesSummaryReport(Date fromdate,Date todate,String branchmstid,CompanyMst  companyMst);

	
	
	
	@Query(nativeQuery = true,value="select b.branch_code, sum(s.total_sales),year(s.voucher_date) from sales_summary_daily s,branch_mst b where s.company_mst=:companyMst and b.id=s.branch_mst and date(voucher_date)  between :fromdate and :todate and s.branch_mst=:branchmstid group by b.branch_code,year(s.voucher_date)")
	List<Object>getYearlylySalesSummaryReport(Date fromdate,Date todate,String branchmstid,CompanyMst  companyMst);
}





