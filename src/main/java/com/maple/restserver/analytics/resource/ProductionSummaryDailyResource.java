package com.maple.restserver.analytics.resource;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.analytics.Service.ProductionSummaryDailyService;
import com.maple.restserver.analytics.Service.PurchaseSummaryDailyService;
import com.maple.restserver.analytics.entity.ExpenseSummaryDaily;
import com.maple.restserver.analytics.entity.ProfitSummaryDaily;
import com.maple.restserver.analytics.entity.PurchaseSummaryDaily;
import com.maple.restserver.analytics.entity.SalesSummaryDaily;
import com.maple.restserver.analytics.repository.ProfitSummaryDailyRepository;
import com.maple.restserver.analytics.repository.PurchaseSummaryDailyRepository;
import com.maple.restserver.analytics.repository.SalesSummaryDailyRepository;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMergeMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ProductionDtlDtl;
import com.maple.restserver.report.entity.PurchaseSummaryReport;
import com.maple.restserver.repository.ActualProductionDtlRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.ProductionDtlDtlRepository;
import com.maple.restserver.utils.SystemSetting;

import ch.qos.logback.classic.pattern.Util;

@RestController
public class ProductionSummaryDailyResource 
{
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	ActualProductionDtlRepository actualProductionDtlRepo;

	@Autowired
	ProductionSummaryDailyService productionSummaryDailyService;
	
	@GetMapping("/{companymstid}/productionsummarydailyresource/productionvalueagainstrawmaterial/{branch}")
	public List<HashMap<String ,Double>>  getProductionValueAgainstRawmaterial(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fdate") String fdate, 
			@RequestParam("tdate") String tdate,
			@PathVariable(value = "branch") String branch){
		
		java.util.Date fromdate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);

		List<HashMap<String ,Double>>  productionValue= productionSummaryDailyService.getProductionAgainstRawMaterial
				(fromdate,todate,companyMstOpt.get(),branch);

	    return productionValue;
	}
	
	@GetMapping("/{companymstid}/productionsummarydailyresource/productionrawmaterialvalue/{branch}")
	public List<HashMap<String ,Double>>  getProductionRawmaterialValue(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fdate") String fdate, 
			@RequestParam("tdate") String tdate,
			@PathVariable(value = "branch") String branch){
		
		java.util.Date fromdate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);

		List<HashMap<String ,Double>>  productionValue = productionSummaryDailyService.getProductionRawMaterialValue
				(fromdate,todate,companyMstOpt.get(),branch);

	    return productionValue;
	}



@GetMapping("{companymstid}/productionsummarydaily/sumofactualqty/{branchcode}/{itemid}")
public List<HashMap<String,Double>> retrieveAllProductionDtlByHdrId(
		@PathVariable (value = "companymstid") String companymstid,
		@PathVariable (value = "itemid") String itemid,
		@PathVariable (value = "branchcode") String branchcode,
		@RequestParam(value ="fdate") String fdate,
		@RequestParam(value ="tdate") String tdate){
	
	java.util.Date fudate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
	java.util.Date tudate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
	return productionSummaryDailyService.getsumOfActualQty(fudate,tudate,itemid,branchcode);
}
	
@GetMapping("{companymstid}/productionsummarydaily/sumofaplaningqty/{branchcode}/{itemid}")
public List<HashMap<String ,Double>> retrieveAllPlaningQty(
		@PathVariable (value = "companymstid") String companymstid,
		@PathVariable (value = "itemid") String itemid,
		@PathVariable (value = "branchcode") String branchcode,
		@RequestParam(value ="fdate") String fdate,
		@RequestParam(value ="tdate") String tdate){
	
	java.util.Date fudate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
	java.util.Date tudate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
	return productionSummaryDailyService.getsumOfPlaningQty(fudate,tudate,itemid,branchcode);
}
}
