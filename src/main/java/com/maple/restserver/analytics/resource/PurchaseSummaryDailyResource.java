package com.maple.restserver.analytics.resource;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.analytics.Service.PurchaseSummaryDailyService;
import com.maple.restserver.analytics.entity.ExpenseSummaryDaily;
import com.maple.restserver.analytics.entity.ProfitSummaryDaily;
import com.maple.restserver.analytics.entity.PurchaseSummaryDaily;
import com.maple.restserver.analytics.entity.SalesSummaryDaily;
import com.maple.restserver.analytics.repository.ProfitSummaryDailyRepository;
import com.maple.restserver.analytics.repository.PurchaseSummaryDailyRepository;
import com.maple.restserver.analytics.repository.SalesSummaryDailyRepository;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMergeMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.report.entity.PurchaseSummaryReport;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.utils.SystemSetting;

import ch.qos.logback.classic.pattern.Util;

@RestController
public class PurchaseSummaryDailyResource 
{
	
	@Autowired
	ItemMstRepository itemMstRepo;
@Autowired
CompanyMstRepository companyMstRepository;
@Autowired
PurchaseSummaryDailyRepository purchaseSummaryDailyRepository;
@Autowired
BranchMstRepository branchMstRepository;
@Autowired
SalesSummaryDailyRepository salesSummaryDailyRepository;
@Autowired
PurchaseSummaryDailyService purchaseSummaryDailyService;
@Autowired
ProfitSummaryDailyRepository profitSummaryDailyRepository;
@GetMapping("/{companymstid}/purchasesummary/purchasesummarymonthlyreport")
public List<PurchaseSummaryReport>  getPurchaseSummaryMonthlyReport(@PathVariable(value = "companymstid") String
		  companymstid,
		  @RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fromdate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	java.util.Date todate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
	Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
	return purchaseSummaryDailyService.getPurchaseSummaryMonthlyReport(fromdate,todate,companyMstOpt.get());
}




@GetMapping("{companymstid}/purchasesummary/analaticstest")
public String createPurchaseSummaryDaily(@PathVariable(value = "companymstid") String
		  companymstid)
{
	Optional<CompanyMst> companyMstoPt=companyMstRepository.findById(companymstid);
	List<BranchMst> branchMstList=branchMstRepository.findAll();
	List<ItemMst> itemList = itemMstRepo.findAll();
	for(BranchMst branch:branchMstList) {
//		
//	PurchaseSummaryDaily purchaseSummaryDaily=new PurchaseSummaryDaily();
//	purchaseSummaryDaily.setCompanyMst(companyMstoPt.get());
//	purchaseSummaryDaily.setTotalPurchase(new BigDecimal("1000000"));
//	purchaseSummaryDaily.setVoucherDate(Date.valueOf(LocalDate.now()));
	//Optional<BranchMst> branchMstOPt=branchMstRepository.findById(branch.getId());
//	purchaseSummaryDaily.setBranchMst(branchMstOPt.get());
//    purchaseSummaryDailyRepository.saveAndFlush(purchaseSummaryDaily);
//    
    ProfitSummaryDaily profitSummaryDaily=new ProfitSummaryDaily();
    profitSummaryDaily.setCompanyMst(companyMstoPt.get());
    profitSummaryDaily.setItemWiseProfit(new BigDecimal("1000000"));
    profitSummaryDaily.setVoucherDate(Date.valueOf(LocalDate.now()));
    profitSummaryDaily.setBranchMst(branch);
    profitSummaryDaily.setItemMst(itemList.get(0));
	profitSummaryDailyRepository.saveAndFlush(profitSummaryDaily);
	}
//	PurchaseSummaryDaily purchaseSummary=new PurchaseSummaryDaily();
//	purchaseSummary.setCompanyMst(companyMstoPt.get());
//	purchaseSummary.setTotalPurchase(new BigDecimal("500000"));
//	purchaseSummary.setVoucherDate(Date.valueOf(LocalDate.now()));
//	Optional<BranchMst> branchMstOPtional=branchMstRepository.findById(branchMstList.get(1).getId());
//	purchaseSummaryDaily.setBranchMst(branchMstOPtional.get());
//    purchaseSummaryDailyRepository.saveAndFlush(purchaseSummaryDaily);
//    SalesSummaryDaily salesSummaryDaily=new SalesSummaryDaily();
//    salesSummaryDaily.setBranchMst(branchMstOPt.get());
//    salesSummaryDaily.setVoucherDate(Date.valueOf(LocalDate.now()));
//    salesSummaryDaily.setCompanyMst(companyMstoPt.get());
//    salesSummaryDaily.setTotalSales(new BigDecimal("1000000"));
//    salesSummaryDailyRepository.saveAndFlush(salesSummaryDaily);
   
			/*
			 * ProfitSummaryDaily profiSummaryDaily=new ProfitSummaryDaily();
			 * profiSummaryDaily.setCompanyMst(companyMstoPt.get());
			 * profiSummaryDaily.setBranchMst(branchMstOPt.get());
			 * profiSummaryDaily.setItemWiseProfit(new BigDecimal("1000000"));
			 * profiSummaryDaily.setItemMst(itemMst);
			 * profitSummaryDailyRepository.save(profiSummaryDaily);
			 */
//}
	 return "Success";

}
}
