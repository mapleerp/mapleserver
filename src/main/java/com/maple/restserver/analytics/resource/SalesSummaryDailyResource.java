package com.maple.restserver.analytics.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.analytics.Service.SalesSummaryDailyService;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.SalesSummaryDailyReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class SalesSummaryDailyResource {

	@Autowired
	SalesSummaryDailyService salesSummaryDailyService;
	@Autowired
	CompanyMstRepository companyMstRepository;
	@GetMapping("/{companymstid}/salessummarydailyresource/salessummarymonthlyreport")
	public List<SalesSummaryDailyReport>  getSalesSummaryMonthlyReport(@PathVariable(value = "companymstid") String
			  companymstid,
			   @RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
		java.util.Date fromdate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		return salesSummaryDailyService.getSalesSummaryMonthlyReport(fromdate,todate,companyMstOpt.get());
}
}
