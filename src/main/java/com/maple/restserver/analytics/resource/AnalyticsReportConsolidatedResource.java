package com.maple.restserver.analytics.resource;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.analytics.Service.AnalyticsConsolidatedReportService;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class AnalyticsReportConsolidatedResource {

	@Autowired
	AnalyticsConsolidatedReportService analyticsConsolidatedReportService;
	@Autowired
	CompanyMstRepository companyMstRepository;
		@GetMapping("/{companymstid}/analyticsreportconsolidatedresource/analyticsreportconsolidated/{reporttype}")
		public Map  getCosolidatedReportbetweenDates(@PathVariable(value = "companymstid") String
				  companymstid,
				  @RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate,
				  @RequestParam("branchlist")String  branchlist,@PathVariable("reporttype")String reporttype){
			java.util.Date fromdate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
			java.util.Date todate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
			Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
			Map<String, Object> hashMap=new HashMap<String, Object>();
			hashMap= analyticsConsolidatedReportService.getConsolidatedReport(fromdate,todate,companyMstOpt.get(),branchlist,reporttype);
		    return hashMap;
		}
	
		@GetMapping("/{companymstid}/analyticsreportconsolidatedresource/monthlyanalyticsreportconsolidated/{reporttype}")
		public Map  getMonthilyCosolidatedReport(@PathVariable(value = "companymstid") String
				  companymstid,
				  @RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate,
				  @RequestParam("branchlist")String  branchlist,@PathVariable("reporttype")String reporttype){
			java.util.Date fromdate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
			java.util.Date todate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
			Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
			Map<String, Object> hashMap=new HashMap<String, Object>();
			hashMap= analyticsConsolidatedReportService.getMonthilyCosolidatedReport(fromdate,todate,companyMstOpt.get(),branchlist,reporttype);
		    return hashMap;
		}
		

		@GetMapping("/{companymstid}/analyticsreportconsolidatedresource/yearlyanalyticsreportconsolidated/{reporttype}")
		public Map  getYearlyCosolidatedReport(@PathVariable(value = "companymstid") String
				  companymstid,
				  @RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate,
				  @RequestParam("branchlist")String  branchlist,@PathVariable("reporttype")String reporttype){
			java.util.Date fromdate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
			java.util.Date todate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
			Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
			Map<String, Object> hashMap=new HashMap<String, Object>();
			hashMap= analyticsConsolidatedReportService.getYearlyCosolidatedReport(fromdate,todate,companyMstOpt.get(),branchlist,reporttype);
		    return hashMap;
		}
	
}
