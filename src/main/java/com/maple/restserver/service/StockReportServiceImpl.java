package com.maple.restserver.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.ItemStockEnquiryReport;
import com.maple.restserver.report.entity.StockReport;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;

@Service
@Transactional
@Component
public class StockReportServiceImpl implements StockReportService {

	@Value("${spring.datasource.url}")
	private String dbType;
	
	@Autowired
	ItemBatchMstRepository itemBatchMstRepository;
	
	@Autowired
	PurchaseDtlRepository purchaseDtlRepository;
	
	@Autowired
	SalesDetailsRepository salesDetailsRepository;
	
	@Override
	public List<StockReport> getStockByBranchCodeAndDateAndCompanyMstId(String branchcode, 
			String companymstid, Date fromdate, String categoryid) {
      List<StockReport> stockReportList =   new ArrayList();
      List<Object> obj=new ArrayList();
      if (dbType.contains("mysql")) {
		  obj =   itemBatchMstRepository.findByBranchCodeAndDateAndCompanyMstId(  branchcode,companymstid,fromdate, categoryid);
      }
      else if (dbType.contains("derby")) {
    	  obj =   itemBatchMstRepository.findByBranchCodeAndDateAndCompanyMstIdDerby(  branchcode,companymstid,fromdate, categoryid);
      }
	
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 StockReport stockReport=new StockReport();
			 stockReport.setItemName((String) objAray[0]);
			 stockReport.setQty((Double) objAray[1]);
			 stockReport.setStandardPrice((Double) objAray[2]);
			 stockReport.setBranchName((String) objAray[3]);
			 stockReport.setCategory((String) objAray[4]);
			 
			 stockReport.setBatch((String) objAray[5]);
			 stockReport.setExpiryDate((java.util.Date) objAray[6]);
			 stockReportList.add(stockReport);
	
		 }
		 
		return stockReportList;
		 
	}

	@Override
	public List<StockReport>  getDailyStockReport(String branchcode, String companymstid, Date fromdate) {
		List<StockReport>  stockReportList =   new ArrayList();
			

			 List<Object> obj =   itemBatchMstRepository.getDailyStockReport(branchcode,companymstid,fromdate);
		 
			 for(int i=0;i<obj.size();i++)
			 {
				 Object[] objAray = (Object[]) obj.get(i);
				 StockReport stockReport=new StockReport();
				 stockReport.setItemName((String) objAray[0]);
				 stockReport.setQty((Double) objAray[1]);
				 stockReport.setStandardPrice((Double) objAray[2]);
				 stockReport.setBranchName((String) objAray[3]);
				 stockReportList.add(stockReport);
		
			 }
			 
			return stockReportList;
	}

	@Override
	public List<StockReport>  getItemWiseStockReport(String branchcode, String companymstid, java.util.Date date,String itemid) {
		List<StockReport>  stockReportList =   new ArrayList();
		
		 List<Object> obj =   itemBatchMstRepository.getItemWiseStockReport(branchcode,companymstid,date,itemid);
			System.out.print(companymstid+"cmpany mst id izzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
		 System.out.print(date+"date issssssssssssssssssssssssssssssssssss");
	 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);

			 StockReport stockReport=new StockReport();
			 stockReport.setItemName((String) objAray[0]);
			 stockReport.setQty((Double) objAray[1]);
			 stockReport.setUnitName((String) objAray[2]);
			 stockReportList.add(stockReport);
	
		 }
		 
		return stockReportList;
	}

	@Override
	public List<ItemStockEnquiryReport> getItemStockReport(String companymstid, String branchcode, String itemid,
			String vouchertype) {
		
		List<ItemStockEnquiryReport> stockReportList = new ArrayList<ItemStockEnquiryReport>();
		
		List<Object> objList = new ArrayList<Object>();
		
		if(vouchertype.equalsIgnoreCase("PURCHASE"))
		{
			objList = purchaseDtlRepository.getItemStockReport(companymstid,branchcode,itemid);
		}
		
		if(vouchertype.equalsIgnoreCase("SALES"))
		{
			objList = salesDetailsRepository.getItemStockReport(companymstid,branchcode,itemid);
		}
		
		for(int i=0 ; i<objList.size(); i++)
		{
			Object[] obj = (Object[]) objList.get(i);
			ItemStockEnquiryReport stockReport = new ItemStockEnquiryReport();
			stockReport.setAmount((Double) obj[2]);
			stockReport.setItemName((String) obj[0]);
			stockReport.setQty((Double) obj[1]);
			stockReport.setVoucherDate((java.util.Date) obj[3]);
			stockReport.setVoucherNumber((String) obj[4]);
			
			stockReportList.add(stockReport);
		}
		
		return stockReportList;
	}

	@Override
	public List<StockReport> getDailyMobileStockReport(String branchcode, String companymstid, Date date) {
		List<StockReport>  stockReportList =   new ArrayList();
		

		 List<Object> obj =   itemBatchMstRepository.getDailyMobileStockReport(branchcode, companymstid, date);
	 
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 StockReport stockReport=new StockReport();
			 stockReport.setItemName((String) objAray[0]);
			 stockReport.setQty((Double) objAray[1]);
			 stockReport.setStandardPrice((Double) objAray[2]);
			 stockReport.setBranchName((String) objAray[3]);
			 stockReport.setCategory((String) objAray[4]);
			 stockReportList.add(stockReport);
	
		 }
		 
		return stockReportList;
	}

	@Override
	public List<StockReport> getDailyMobileStockReportByCategory(String branchcode, String companymstid, Date date,
			String[] array) {
		List<StockReport>  stockReportList =   new ArrayList();
		

		 List<Object> obj =   itemBatchMstRepository.getDailyMobileStockReportByCategory(branchcode, companymstid, date,array);
	 
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 StockReport stockReport=new StockReport();
			 stockReport.setItemName((String) objAray[0]);
			 stockReport.setQty((Double) objAray[1]);
			 stockReport.setStandardPrice((Double) objAray[2]);
			 stockReport.setBranchName((String) objAray[3]);
			 stockReport.setCategory((String) objAray[4]);
			 stockReportList.add(stockReport);
	
		 }
		 
		return stockReportList;
	}

	@Override
	public List<StockReport> getDailyMobileStockReportByItemName(String branchcode, String companymstid, Date date,
			String itemname) {
		List<StockReport>  stockReportList =   new ArrayList();
		

		 List<Object> obj =   itemBatchMstRepository.getDailyMobileStockReportByItemName(branchcode, companymstid, date,itemname);
	 
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 StockReport stockReport=new StockReport();
			 stockReport.setItemName((String) objAray[0]);
			 stockReport.setQty((Double) objAray[1]);
			 stockReport.setStandardPrice((Double) objAray[2]);
			 stockReport.setBranchName((String) objAray[3]);
			 stockReport.setCategory((String) objAray[4]);
			 stockReportList.add(stockReport);
	
		 }
		 
		return stockReportList;	}

	@Override
	public List<StockReport> getStoreWiseStockReport(String branchcode, String companymstid, String store) {
		List<StockReport>  stockReportList =   new ArrayList();
		

		 List<Object> obj =   itemBatchMstRepository.getStoreWiseStockReport(branchcode, companymstid, store);
	 
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 StockReport stockReport=new StockReport();
			 stockReport.setItemName((String) objAray[0]);
			 stockReport.setQty((Double) objAray[1]);
			 stockReport.setStore(store);
			
			 stockReportList.add(stockReport);
	
		 }
		 
		return stockReportList;
	}
	
	
	@Override
	public List<StockReport> getPriceTypeWiseStockSummaryReport(java.util.Date fdate, java.util.Date tdate, String pricetype,
			String branchcode) {
		List<StockReport>  stockReportList1 =   new ArrayList();
		

		 List<Object> obj =   itemBatchMstRepository.getPricetypeWiseStockReport(fdate,tdate,pricetype,branchcode);
	 
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 StockReport stockReport=new StockReport();
			 stockReport.setItemName((String) objAray[0]);
			 stockReport.setQty((Double) objAray[1]);
			 stockReport.setStandardPrice((Double) objAray[2]);
			 stockReport.setBranchName((String) objAray[3]);
			 stockReport.setCategory((String) objAray[4]);
			 stockReport.setTotalAmount((Double) objAray[5]);
			
			 stockReportList1.add(stockReport);
	
		 }
		 
		return stockReportList1;
	}
}
