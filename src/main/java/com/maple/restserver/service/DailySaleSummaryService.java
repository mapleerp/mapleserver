package com.maple.restserver.service;

import java.util.Date;
import java.util.List;


import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DailySalesSummary;
import com.maple.restserver.report.entity.ReceiptModeReport;
import com.maple.restserver.report.entity.SaleModeWiseSummaryReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;

public interface DailySaleSummaryService {

	List<Object> getDailySaleSummary(String branch, Date reportdate, String companymstid/* , String accoutName */);

	List<DailySalesSummary> getDailySales(String pdfFileName);

	List<SaleModeWiseSummaryReport> getPaymentModeWiseReport(String branch, Date date, CompanyMst companymst);

	Double getSumOfOnlineSale(String branch, Date date, String companymstid, String customerid);

	List<ReceiptModeReport> getsalesReceipts(String branch, java.sql.Date fdate, String companyMst);

	List<ReceiptModeReport> getSalesReceiptsByMode(String branch, java.sql.Date fdate, String companyMst, String mode);

	List<ReceiptModeReport> getsalesReceiptsWeb(Date date, String companymstid);

	List<ReceiptModeReport> getSaleOrdersalesReceipts(Date date, String companymstid);

	List<ReceiptModeReport> getSaleOrdersalesReceiptsByMode(Date date, String companymstid, String mode);

	Double getTotalCard(Date vdate, String companymstid, String branchcode);

	Double getTotalCardSO(String companymstid, java.sql.Date sdate);

	List<ReceiptModeReport> getSalesReceiptsByModeByAccountId(String branch, java.sql.Date sqlDate, String companymstid,
			String accountid);

	Double getCard2SalesSummary(String branchCode, Date date, String companyid);

	List<SalesInvoiceReport> getBranchWiseSalesSummary(Date date, String companymstid);

}