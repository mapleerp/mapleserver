package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DailyCreditSales;
import com.maple.restserver.entity.DailySalesDtl;
import com.maple.restserver.entity.DailySalesSummary;
import com.maple.restserver.report.entity.DailySalesReport;
import com.maple.restserver.report.entity.DailySalesSummaryReport;
import com.maple.restserver.report.entity.DayEndPettyCashPaymentAndReceipt;
import com.maple.restserver.report.entity.ReceiptModeReport;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DailyCashFromOrderRepository;
import com.maple.restserver.repository.DailyCreditSalesRepository;
import com.maple.restserver.repository.DailySaleSummary2Repository;
import com.maple.restserver.repository.DailySalesDtlRepository;
import com.maple.restserver.repository.DailySalesSummaryRepository;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;

@Service
@Transactional
@Component
public class DailySaleReportServiceimpl implements DailySaleReportService {
	
	@Autowired
	DailySalesSummaryRepository dailySaleSummaryRepo;
	@Autowired
	DailyCashFromOrderRepository dailyCashFromOrderRepo;
	@Autowired
	DailyCreditSalesRepository dailyCreditSaleRepo;

	@Autowired
	DailySaleSummary2Repository dailySaleSummary2Repository;
	@Autowired
	DailyCreditSalesRepository dailyCreditSalesRepository;
	@Autowired
	DailySalesDtlRepository dailysalesrepository;
	@Autowired
	DayEndClosureRepository dayEndClosureRepository;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Autowired
	BranchMstRepository branchMstRepo;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Override
	public DailySalesSummary getDailySaleSummary(Date date, String branchCode) {

		return dailySaleSummaryRepo.findByRecordDateAndBranchCode(date, branchCode);
	}

	@Override
	public DailySalesSummaryReport getOpeningPettyCash(Date date, String branchCode) {
		DailySalesSummary dailysalesummary = dailySaleSummaryRepo.findByRecordDateAndBranchCode(date, branchCode);
		DailySalesSummaryReport reportSummary = new DailySalesSummaryReport();
		reportSummary.setCashReceived(dailysalesummary.getCashReceived());
		reportSummary.setExpensePaid(dailysalesummary.getExpensePaid());
		reportSummary.setClosingBalance(dailysalesummary.getClosingBalance());
		return reportSummary;
	}

	@Override
	public DailySalesSummaryReport advanceRecivedFromOrders(Date date, String branchCode) {
		DailySalesSummary dailysalesummary = dailySaleSummaryRepo.findByRecordDateAndBranchCode(date, branchCode);
		DailySalesSummaryReport reportSummary = new DailySalesSummaryReport();
		reportSummary.setCashSalesOrder(dailysalesummary.getCashSalesOrder());
		reportSummary.setOnlineSalesOrder(dailysalesummary.getOnlineSalesOrder());
		reportSummary.setDebitCardSalesOrder(dailysalesummary.getDebitCardSalesOrder());
		;
		return reportSummary;

	}

	@Override
	public DailySalesSummaryReport getDailySalesDtls(Date date, String branchCode) {
		DailySalesDtl dailysalesummary = dailysalesrepository.findByReportDateAndBranchCode(date, branchCode);
		DailySalesSummaryReport reportSummary = new DailySalesSummaryReport();
		reportSummary.setCashSales(dailysalesummary.getCashSales());
		reportSummary.setCreditSales(dailysalesummary.getCreditSales());
		dailysalesummary.setCreditCardSales(dailysalesummary.getCreditCardSales());
		dailysalesummary.setSodexoSales(dailysalesummary.getSodexoSales());
		dailysalesummary.setPaytmSales(dailysalesummary.getPaytmSales());

		return reportSummary;
	}

	@Override
	public DailySalesSummaryReport dailyCreditSalesDtl(Date date, String branchCode) {
		DailyCreditSales dailysalesummary = dailyCreditSalesRepository.findByReportDateAndBranchCode(date, branchCode);
		DailySalesSummaryReport reportSummary = new DailySalesSummaryReport();
		reportSummary.setB2BSales(dailysalesummary.getB2BSales());
		reportSummary.setUberSales(dailysalesummary.getUberSales());
		reportSummary.setFoodPandaSales(dailysalesummary.getFoodPandaSales());
		reportSummary.setsWiggySales(dailysalesummary.getsWiggySales());
		reportSummary.setZomatoSales(dailysalesummary.getZomatoSales());
		reportSummary.setOnlineSalesOrder(dailysalesummary.getOnlineSales());
		return reportSummary;
	}

	@Override
	public List<DailySalesReport> dailySalesReport(Date date, String companymstid) {

		List<BranchMst> branchMstList = branchMstRepo.findByCompanyMstId(companymstid);
		List<DailySalesReport> reportSummaryList = new ArrayList<DailySalesReport>();

		for (BranchMst branch : branchMstList) {
			DailySalesReport reportSummary = new DailySalesReport();
			List<Object> objList = salesTransHdrRepository.findDailySalesReport(date, branch.getBranchCode());
			Object[] objAray = (Object[]) objList.get(0);
			reportSummary.setTotalSales((Double) objAray[0]);
			reportSummary.setCashPay((Double) objAray[1]);
			reportSummary.setCard1((Double) objAray[2]);
			reportSummary.setBranchCode(branch.getBranchCode());

			List<Object> objctList = salesTransHdrRepository.findCard2Report(date, branch.getBranchCode());

			if (null != objctList) {
				Object[] objctAray = (Object[]) objctList.get(0);
				if (null != objctAray) {
					reportSummary.setCard2((Double) objctAray[0]);
				} else {
					reportSummary.setCard2(0.0);
				}
			} else {
				reportSummary.setCard2(0.0);
			}

			Double physicalCash = dayEndClosureRepository.getPhysicalCash(date, branch.getBranchCode());
			if (physicalCash != null) {
				reportSummary.setPhysicalCash(physicalCash);
			} else {
				reportSummary.setPhysicalCash(0.0);
			}
			reportSummaryList.add(reportSummary);
		}
		return reportSummaryList;
	}

	@Override
	public List<DailySalesSummary> allbranchdailySalesSummaryReport(Date date, String companymstid) {
		List<BranchMst> branchMstList = branchMstRepo.findByCompanyMstId(companymstid);
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		List<DailySalesSummary> reportSummaryList = new ArrayList<DailySalesSummary>();
		for (BranchMst branch : branchMstList) {
			reportSummaryList = dailySaleSummaryRepo.findByBranchCodeAndRecordDateAndCompanyMst(date,
					branch.getBranchCode(), companyMst.get());

		}
		return reportSummaryList;
	}

	@Override

	public List<ReceiptModeReport> receiptModeWiseReportSalesAndSoConverted(Date date, String companymstid,
			String branchcode) {

		List<ReceiptModeReport> receiptModeWiseReportList = new ArrayList<ReceiptModeReport>();
		;
		List<Object> obj = dailySaleSummaryRepo.receiptModeWiseReportSalesAndSoConverted(date, companymstid,
				branchcode);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);

			ReceiptModeReport receiptModeWiseReport = new ReceiptModeReport();
			receiptModeWiseReport.setReceiptMode((String) objAray[0]);
			;
			receiptModeWiseReport.setAmount((Double) objAray[1]);
			receiptModeWiseReportList.add(receiptModeWiseReport);
		}
		return receiptModeWiseReportList;
	}

	@Override
	public List<ReceiptModeReport> soAdvanceReceivedToday(Date date, String companymstid, String branchcode) {

		List<ReceiptModeReport> receiptModeWiseReportList = new ArrayList<ReceiptModeReport>();
		List<Object> obj = dailySaleSummaryRepo.soAdvanceReceivedToday(date, companymstid, branchcode);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			ReceiptModeReport receiptModeWiseReport = new ReceiptModeReport();
			receiptModeWiseReport.setReceiptMode((String) objAray[0]);
			;
			receiptModeWiseReport.setAmount((Double) objAray[1]);
			receiptModeWiseReportList.add(receiptModeWiseReport);
		}
		return receiptModeWiseReportList;

	}

	@Override
	public List<ReceiptModeReport> dayEndReceiptModewiseReport(Date date, String companymstid, String branchcode) {
		List<ReceiptModeReport> receiptModeWiseReportList = new ArrayList<ReceiptModeReport>();
		List<Object> obj = dailySaleSummaryRepo.dayEndReceiptModewiseReport(date, companymstid, branchcode);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			ReceiptModeReport receiptModeWiseReport = new ReceiptModeReport();
			receiptModeWiseReport.setReceiptMode((String) objAray[0]);
			
			receiptModeWiseReport.setAmount((Double) objAray[1]);
			receiptModeWiseReportList.add(receiptModeWiseReport);
		}
		return receiptModeWiseReportList;
	}

	@Override
	public DayEndPettyCashPaymentAndReceipt dayEndPettyCashReceiptAndPettyCashPayment(Date date, String companymstid,
			String branchcode, String accountId) {

		Double pettyCashReceipt = dailySaleSummaryRepo.dayEndPettyCashReceipt(date, companymstid, branchcode,
				accountId);
		Double pettyCashPayment = dailySaleSummaryRepo.dayEndPettyCashPayment(date, companymstid, branchcode,
				accountId);

		DayEndPettyCashPaymentAndReceipt Report = new DayEndPettyCashPaymentAndReceipt();
		Report.setPettyCashPayment(pettyCashPayment);
		Report.setPettyCasReceipt(pettyCashReceipt);

		return Report;

	}

	@Override
	public List<ReceiptModeReport> dayEndReceptModeWisePreviousSalesReport(Date date, String companymstid,
			String branchcode) {

		List<ReceiptModeReport> receiptModeWiseReportList = new ArrayList<ReceiptModeReport>();
		List<Object> obj = dailySaleSummaryRepo.dayEndReceptModeWisePreviousSalesReport(date, companymstid, branchcode);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			ReceiptModeReport receiptModeWiseReport = new ReceiptModeReport();
			receiptModeWiseReport.setReceiptMode((String) objAray[0]);
			receiptModeWiseReport.setAmount((Double) objAray[1]);
			receiptModeWiseReportList.add(receiptModeWiseReport);
		}
		return receiptModeWiseReportList;
	
	}

	

}
