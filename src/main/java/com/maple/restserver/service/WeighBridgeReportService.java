package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.WeighBridgeWeights;

@Service
public interface WeighBridgeReportService {


	 List<WeighBridgeWeights> getWeighBridgeReport( String companymstid ,Date fdate,Date tdate);
	
}
