package com.maple.restserver.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.SupplierPriceMst;
import com.maple.restserver.report.entity.BranchSaleReport;
import com.maple.restserver.report.entity.DailySalesReportDtl;
import com.maple.restserver.report.entity.ItemSaleReport;
import com.maple.restserver.report.entity.SaleOrderinoiceReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.TaxSummaryMst;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;

@Service
@Transactional
@Component
public class SalesInvoiceServiceImpl implements SalesInvoiceService {

	@Autowired
	SalesTransHdrRepository SalesTransHdrRepo;
	
	@Autowired
	SalesDetailsRepository salesDetailsRepository;

	@Override
	public List<SalesInvoiceReport> getSalesInvoice(String companymstid, String voucherNumber, Date voucherDate) {

		// List<TaxSummaryMst> taxReport = getSalesInvoiceTax( companymstid,
		// voucherNumber, voucherDate) ;
		
		List<SalesInvoiceReport> salesInvoiceReportList = new ArrayList();


		List<Object> obj = SalesTransHdrRepo.salesInvoice(companymstid, voucherNumber, voucherDate);
		
		Double taxableAmount = 0.0;
		
		for (int i = 0; i < obj.size(); i++) {
			
			Object[] objAray = (Object[]) obj.get(i);
			
			SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
			salesInvoiceReport.setBranchName((String) objAray[0]);
			// salesInvoiceReport.setBranchName("TEST BRANCH NAME");
			salesInvoiceReport.setBranchAddress1((String) objAray[1]);
			salesInvoiceReport.setBranchAddress2((String) objAray[2]);
			salesInvoiceReport.setBranchPlace((String) objAray[3]);
			salesInvoiceReport.setBranchTelNo((String) objAray[4]);
			salesInvoiceReport.setBranchGst((String) objAray[5]);
			salesInvoiceReport.setBranchState((String) objAray[6]);
			salesInvoiceReport.setBranchEmail((String) objAray[7]);
			salesInvoiceReport.setBankName((String) objAray[8]);
			salesInvoiceReport.setAccountNumber((String) objAray[9]);
			salesInvoiceReport.setBankBranch((String) objAray[10]);
			salesInvoiceReport.setIfsc((String) objAray[11]);
			salesInvoiceReport.setVoucherDate((Date) objAray[12]);
			salesInvoiceReport.setVoucherNumber((String) objAray[13]);
			salesInvoiceReport.setCustomerName((String) objAray[14]);
			salesInvoiceReport.setCustomerPlace((String) objAray[15]);
			salesInvoiceReport.setCustomerState((String) objAray[16]);
			salesInvoiceReport.setCustomerGst((String) objAray[17]);
			salesInvoiceReport.setItemName((String) objAray[18]);
			if (null != objAray[42]) {
				salesInvoiceReport.setMrp((Double) objAray[40]);
			} else {
				salesInvoiceReport.setMrp(0.0);
			}

			if (null != objAray[19]) {

				salesInvoiceReport.setTaxRate((Double) objAray[19]);
			} else {
				salesInvoiceReport.setTaxRate(0.0);
			}

			if (null != objAray[20]) {
				salesInvoiceReport.setHsnCode((String) objAray[20]);
			} else {
				salesInvoiceReport.setHsnCode("");

			}

			if (null != objAray[21]) {
				salesInvoiceReport.setRate((Double) objAray[21]);
			} else {

			}
			if (null != objAray[22]) {
				salesInvoiceReport.setQty((Double) objAray[22]);
			} else {
				salesInvoiceReport.setQty(0.0);
			}

			if (null != objAray[23]) {
				salesInvoiceReport.setUnitName((String) objAray[23]);
			} else {

			}

			if (null != objAray[24]) {

				salesInvoiceReport.setDiscount((Double) objAray[24]);
			} else {
				salesInvoiceReport.setDiscount(0.0);
			}

			if (null != objAray[25]) {
				salesInvoiceReport.setAmount((Double) objAray[25]);
			} else {
				salesInvoiceReport.setAmount(0.0);
			}

			if (null != objAray[26]) {
				salesInvoiceReport.setSgstAmount(((Double) objAray[26]));
			} else {
				salesInvoiceReport.setSgstAmount(0.0);
			}

			if (null != objAray[27]) {
				salesInvoiceReport.setCgstAmount(((Double) objAray[27]));
			} else {
				salesInvoiceReport.setCgstAmount(0.0);
			}

			if (null != objAray[28]) {
				salesInvoiceReport.setIgstAmount(((Double) objAray[28]));
			} else {
				salesInvoiceReport.setIgstAmount(0.0);
			}

			if (null != objAray[29]) {
				salesInvoiceReport.setSgstRate((Double) objAray[29]);
			} else {
				salesInvoiceReport.setSgstRate(0.0);

			}

			if (null != objAray[30]) {
				salesInvoiceReport.setCgstRate((Double) objAray[30]);
			} else {
				salesInvoiceReport.setCgstRate(0.0);
			}

			if (null != objAray[31]) {
				salesInvoiceReport.setIgstRate((Double) objAray[31]);
			} else {
				salesInvoiceReport.setIgstRate(0.0);
			}

			if (null != objAray[32]) {
				salesInvoiceReport.setCessRate((Double) objAray[32]);
			} else {
				salesInvoiceReport.setCessRate(0.0);
			}

			if (null != objAray[33]) {
				salesInvoiceReport.setCessAmount((Double) objAray[33]);
			} else {
				salesInvoiceReport.setCessAmount(0.0);
			}
			if (null != objAray[34]) {
				salesInvoiceReport.setBranchWebsite((String) objAray[34]);
			} else {
				salesInvoiceReport.setBranchWebsite("");
			}
			salesInvoiceReport.setCustomerPhone((String) objAray[35]);

			if (null != objAray[36]) {
				salesInvoiceReport.setTaxableAmount((Double) objAray[36]);
			} else {
				salesInvoiceReport.setTaxableAmount(0.0);
			}

			if (null != objAray[37]) {
				salesInvoiceReport.setDiscountPercent((String) objAray[37]);
			} else {
				salesInvoiceReport.setDiscountPercent("");
			}

			if (null != objAray[38]) {
				salesInvoiceReport.setInvoiceDiscount(-1 * (Double) objAray[38]);
			} else {
				salesInvoiceReport.setInvoiceDiscount(0.0);
			}
			
			if(null != objAray[43])
			{
				salesInvoiceReport.setWarranty((String) objAray[43]);
			}
			
			if(null != objAray[42])
			{
				salesInvoiceReport.setStandrdPrice((Double) objAray[42]);
			} else {
				salesInvoiceReport.setStandrdPrice(0.0);

			}
			
			salesInvoiceReport.setListPrice((Double) objAray[44]);

			salesInvoiceReport.setSalesMode((String) objAray[39]);
			salesInvoiceReport.setItemCode((String) objAray[45]);
			salesInvoiceReport.setExpiryDate((Date) objAray[46]);
			taxableAmount = SalesTransHdrRepo.getTaxableAmount(voucherNumber, voucherDate, companymstid);
			salesInvoiceReport.setTaxableAmount(taxableAmount);

			/*
			 * List<Object> object=SalesTransHdrRepo.getTaxInvoiceCustomer(companymstid,
			 * voucherNumber, voucherDate); for (int k = 0; k < object.size(); k++) {
			 * Object[] objctAray = (Object[]) object.get(k);
			 * salesInvoiceReport.setLocalCustomerName((String) objctAray[0]);
			 * salesInvoiceReport.setAddressLine1((String) objctAray[1]);
			 * salesInvoiceReport.setAddressLine2((String) objctAray[2]);
			 * salesInvoiceReport.setLocalCustomerPhone((String) objctAray[3]); }
			 */
			salesInvoiceReport.setBatch((String) objAray[41]);
			salesInvoiceReport.setStandrdPrice((Double) objAray[42]);
			salesInvoiceReportList.add(salesInvoiceReport);

		}

		return salesInvoiceReportList;
	}

	@Override
	public List<SalesInvoiceReport> getAllSalesInvoice(String companymstid) {

		List<SalesInvoiceReport> salesInvoiceReportList = new ArrayList();

		List<Object> obj = SalesTransHdrRepo.salesInvoiceAll(companymstid);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
			salesInvoiceReport.setBranchName((String) objAray[0]);
			// salesInvoiceReport.setBranchName("TEST BRANCH NAME");

			salesInvoiceReport.setBranchAddress1((String) objAray[1]);
			salesInvoiceReport.setBranchAddress2((String) objAray[2]);
			salesInvoiceReport.setBranchPlace((String) objAray[3]);
			salesInvoiceReport.setBranchTelNo((String) objAray[4]);
			salesInvoiceReport.setBranchGst((String) objAray[5]);
			salesInvoiceReport.setBranchState((String) objAray[6]);
			salesInvoiceReport.setBranchEmail((String) objAray[7]);
			salesInvoiceReport.setBankName((String) objAray[8]);
			salesInvoiceReport.setAccountNumber((String) objAray[9]);
			salesInvoiceReport.setBankBranch((String) objAray[10]);
			salesInvoiceReport.setIfsc((String) objAray[11]);
			salesInvoiceReport.setVoucherDate((Date) objAray[12]);
			salesInvoiceReport.setVoucherNumber((String) objAray[13]);
			salesInvoiceReport.setCustomerName((String) objAray[14]);
			salesInvoiceReport.setCustomerPlace((String) objAray[15]);
			salesInvoiceReport.setCustomerState((String) objAray[16]);
			salesInvoiceReport.setCustomerGst((String) objAray[17]);
			salesInvoiceReport.setItemName((String) objAray[18]);
			salesInvoiceReport.setHsnCode((String) objAray[20]);
			salesInvoiceReport.setRate((Double) objAray[21]);
			salesInvoiceReport.setQty((Double) objAray[22]);
			salesInvoiceReport.setUnitName((String) objAray[23]);
			salesInvoiceReport.setDiscount((Double) objAray[24]);
			salesInvoiceReport.setAmount((Double) objAray[25]);
			salesInvoiceReport.setSgstAmount((Double) objAray[26]);
			salesInvoiceReport.setCgstAmount((Double) objAray[27]);
			salesInvoiceReport.setIgstAmount((Double) objAray[28]);
			salesInvoiceReport.setSgstRate((Double) objAray[29]);
			salesInvoiceReport.setCgstRate((Double) objAray[30]);
			salesInvoiceReport.setIgstRate((Double) objAray[31]);

			salesInvoiceReportList.add(salesInvoiceReport);

		}

		return salesInvoiceReportList;
	}
	/*
	 * 
	 * select bm.branch_name, bm.branch_address1, bm.branch_address2, bm.place,
	 * bm.teln_no, bm.gstno bm.state_name, bm.email, bm.bank_name,
	 * bm.account_number, bm.bank_branch, bm.ifsc,
	 * sh.voucher_date,sh.voucher_number, cm.customer_name,cm.custopemr_place,
	 * cm.customer_state, cm.gstno, im.item_name,im.tax_rate, im.hsn, d.rate, d.qty,
	 * u.unit_name,d.discount, ((d.rate*d.qty*d.tax_rate/100) +
	 * (d.rate*d.qtSalesTransHdrRepoy*)) as amount , ( d.rate*d.qty*d.tax_rate/200 )
	 * as sgst_amount , ( d.rate*d.qty*d.tax_rate/200 ) as cgst_amount from
	 * branch_mst bm, sales_trans_hdr sh, sales_dtl d, customer_mst cm, unit_mst u
	 * where sh.branch_code = bm.branchcode and sh.customer_id = cm.id and sh.id =
	 * d.sales_trans_hdr_id and d.item_id = im.id and d.unit_id = u.id and
	 * sh.voucher_nuber = :vnumber and sh.voucher_date = :vdate
	 */

	@Override
	public List<TaxSummaryMst> getSalesInvoiceTax(String companymstid, String voucherNumber, Date date) {

		List<TaxSummaryMst> taxSummaryList = new ArrayList();

		List<Object> obj = SalesTransHdrRepo.getTaxSummary(companymstid, voucherNumber, date);

		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			TaxSummaryMst taxSummary = new TaxSummaryMst();
			Double taxPercent = ((Double) objAray[0]);
			taxSummary.setTaxAmount((Double) objAray[1]);

			Double sgstpercent = taxPercent;
			Double taxAmount = ((Double) objAray[1]);

			taxSummary.setTaxPercentage("GST" + sgstpercent + "%");

			taxSummary.setSgstAmount((Double) objAray[2]);
			taxSummary.setCgstAmount((Double) objAray[3]);
			taxSummary.setIgstAmount((Double) objAray[4]);

			taxSummary.setSgstTaxRate(((Double) objAray[5]));
			taxSummary.setCgstTaxRate(((Double) objAray[6]).doubleValue());
			taxSummary.setIgstTaxRate(((Double) objAray[7]).doubleValue());

			taxSummary.setAmount(((Double) objAray[8]).doubleValue());
			taxSummary.setTaxRate(((Double) objAray[0]).doubleValue());

			taxSummaryList.add(taxSummary);

			/*
			 * TaxSummaryMst taxSummary2 = new TaxSummaryMst();
			 * 
			 * taxSummary2.setTaxPercentage("CGST"+sgstpercent+"%");
			 * taxSummary2.setTaxAmount(sgstAmount); taxSummaryList.add(taxSummary2);
			 */

		}
		return taxSummaryList;

	}

	@Override
	public SaleOrderinoiceReport getSaleOrderInvoiceReport(String voucherNumber, Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ItemSaleReport> getItemSaleReport(String itemid, Date date) {

		List<ItemSaleReport> itemSaleReportList = new ArrayList();

		List<Object> obj = SalesTransHdrRepo.getItemSaleReport(itemid, date);

		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			ItemSaleReport itemSaleSummary = new ItemSaleReport();
			itemSaleSummary.setUnitName((String) objAray[0]);
			itemSaleSummary.setSoldOutQty((Double) objAray[1]);
			itemSaleSummary.setRate((Double) objAray[2]);
			itemSaleSummary.setCategoryName((String) objAray[3]);
			itemSaleReportList.add(itemSaleSummary);

		}
		return itemSaleReportList;
	}

	@Override
	public List<SalesInvoiceReport> getTaxinvoiceCustomer(String companymstid, String voucherNumber, Date date) {
		List<SalesInvoiceReport> salesInvoiceReportList = new ArrayList();
		List<Object> object = SalesTransHdrRepo.getTaxInvoiceCustomer(companymstid, voucherNumber, date);
		List<Object> objects = SalesTransHdrRepo.getTaxInvoiceGstCustomer(companymstid, voucherNumber, date);

		SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
		for (int l = 0; l < objects.size(); l++) {
			Object[] objectArray = (Object[]) objects.get(l);
			salesInvoiceReport.setCustomerName((String) objectArray[0]);
			salesInvoiceReport.setCustomerAddress((String) objectArray[1]);
			salesInvoiceReport.setCustomerPlace((String) objectArray[2]);
			salesInvoiceReport.setCustomerPhone((String) objectArray[3]);
			salesInvoiceReport.setCustomerGst((String) objectArray[4]);

		}
		for (int k = 0; k < object.size(); k++) {
			Object[] objctAray = (Object[]) object.get(k);

			salesInvoiceReport.setLocalCustomerName((String) objctAray[0]);
			salesInvoiceReport.setLocalCustomerAddres((String) objctAray[1]);
			salesInvoiceReport.setLocalCustomerPlace((String) objctAray[3]);
			salesInvoiceReport.setLocalCustomerState((String) objctAray[4]);
			salesInvoiceReport.setLocalCustomerPhone((String) objctAray[2]);
		}

		salesInvoiceReportList.add(salesInvoiceReport);

		return salesInvoiceReportList;
	}

	@Override
	public Double getCessAmount(String vouchernumber, Date date) {

		return SalesTransHdrRepo.getCessAmount(vouchernumber, date);
	}

	@Override
	public List<TaxSummaryMst> getSumOfTaxAmounts(String companymstid, String voucherNumber, Date date) {

		List<TaxSummaryMst> taxSumReportList = new ArrayList();

		List<Object> obj = SalesTransHdrRepo.getSumOfTaxAmounts(companymstid, voucherNumber, date);

		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			TaxSummaryMst tSummary = new TaxSummaryMst();
			tSummary.setSumOfsgstAmount(((Double) objAray[0]));
			tSummary.setSumOfcgstAmount(((Double) objAray[1]));
			tSummary.setSumOfigstAmount(((Double) objAray[2]));
			taxSumReportList.add(tSummary);

		}
		return taxSumReportList;

	}

	@Override
	public Double getNonTaxableAmount(String vouchernumber, Date date) {

		Double nonTaxableAmount = SalesTransHdrRepo.getNonTaxableAmount(vouchernumber, date);

		return nonTaxableAmount;
	}

	public List<BranchSaleReport> getBranchSaleHdrReport(String companymstid, String branchcode, Date sdate,
			Date edate) {

		List<BranchSaleReport> branchSaleReportList = new ArrayList<BranchSaleReport>();

		List<Object> objList = SalesTransHdrRepo.getBranchSaleHdrReport(companymstid, branchcode, sdate, edate);
		for (int i = 0; i < objList.size(); i++) {
			Object[] objAray = (Object[]) objList.get(i);

			BranchSaleReport branchSaleReport = new BranchSaleReport();
			branchSaleReport.setCustomerName((String) objAray[2]);
			branchSaleReport.setInvoiceTotal((Double) objAray[3]);
			branchSaleReport.setVoucherDate((Date) objAray[1]);
			branchSaleReport.setVoucherNumber((String) objAray[0]);
			branchSaleReport.setToBranchName((String) objAray[4]);

			branchSaleReportList.add(branchSaleReport);
		}

		return branchSaleReportList;
	}

	public List<DailySalesReportDtl> getSalesReportByPriceTypeReport(String companymstid, String branchcode,
			String pricetype, Date sdate, Date edate) {

		List<DailySalesReportDtl> salesReportList = new ArrayList<DailySalesReportDtl>();

		List<Object> objList = SalesTransHdrRepo.getSalesReportByPriceTypeReport(companymstid, branchcode,pricetype, sdate, edate);
		for (int i = 0; i < objList.size(); i++) {
			Object[] objAray = (Object[]) objList.get(i);

			DailySalesReportDtl dailySalesReportDtl = new DailySalesReportDtl();
			dailySalesReportDtl.setAmount((Double) objAray[3]);
			dailySalesReportDtl.setCustomerName((String) objAray[2]);
			dailySalesReportDtl.setVoucherDate((Date) objAray[1]);
			dailySalesReportDtl.setVoucherNumber((String) objAray[0]);

			salesReportList.add(dailySalesReportDtl);
		}

		return salesReportList;

	}

	
	///////--------------------------------branch wise sales--------------------

	public List<SaleOrderinoiceReport> getTotalBranchWiseSales(String companymstid, Date date) {

		List<SaleOrderinoiceReport> saleOrderinoiceReportList = new ArrayList<SaleOrderinoiceReport>();

		List<Object> objlist = SalesTransHdrRepo.findallSaleOrderinoiceReport(companymstid,date);
		for (int i = 0; i < objlist.size(); i++) {

			Object[] objAray = (Object[]) objlist.get(i);
			
			SaleOrderinoiceReport report = new SaleOrderinoiceReport();
			report.setBranchName((String) objAray[0]);
			report.setTotalSale((Double) objAray[1]);
		
			
			
			saleOrderinoiceReportList.add(report);

		}
		
		return saleOrderinoiceReportList;

	}

	
	public List<BranchSaleReport> getBranchSaleHdrReportSummary(String companymstid, String branchcode, Date sdate,
			Date edate) {

		List<BranchSaleReport> branchSaleReportList = new ArrayList<BranchSaleReport>();

		List<Object> objList = SalesTransHdrRepo.getBranchSaleHdrReportSummary(companymstid, branchcode, sdate, edate);
		for (int i = 0; i < objList.size(); i++) {
			Object[] objAray = (Object[]) objList.get(i);

			BranchSaleReport branchSaleReport = new BranchSaleReport();
			branchSaleReport.setCustomerName((String) objAray[2]);
			branchSaleReport.setInvoiceTotal((Double) objAray[3]);
			branchSaleReport.setVoucherDate((Date) objAray[1]);
			branchSaleReport.setVoucherNumber((String) objAray[0]);
			branchSaleReport.setToBranchName((String) objAray[4]);
			branchSaleReport.setBranchVoucherNo((String) objAray[5]);

			branchSaleReportList.add(branchSaleReport);
		}

		return branchSaleReportList;
	}

	
	@Override
	public List<BranchSaleReport> getTopSellingProducts(String companymstid, Date udate) {
		
		List<BranchSaleReport> branchSaleReportList = new ArrayList<BranchSaleReport>();

		
		Pageable topFifty = PageRequest.of(0, 5);

		
		List<Object> objList = salesDetailsRepository.getTopSellingProducts(companymstid,udate,topFifty);
		
		for(int i = 0; i < objList.size(); i++)
		{
			Object[] objAray = (Object[]) objList.get(i);
			BranchSaleReport branchSaleReport = new  BranchSaleReport();
			
			branchSaleReport.setItemName((String) objAray[0]);
			branchSaleReport.setQty((Double) objAray[1]);
			
			branchSaleReportList.add(branchSaleReport);

			
		}
		
	
		return branchSaleReportList;
	}


}
