package com.maple.restserver.service;

import java.sql.Date;
import java.util.ArrayList;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.entity.LastBestDate;
import com.maple.restserver.entity.StockTransferMessageEntity;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.LastBestDateRepository;
import com.maple.restserver.repository.StockTransferOutDtlRepository;
import com.maple.restserver.repository.StockTransferOutHdrRepository;

@Component
public class StockTransferDataSync implements DataSynchronisationService {
	
	


	String branchCode;
	String companyMstId;

	private int stockTransferCount = 0;
	private LastBestDate lastBestDate = null;

	private CloudDataCount cloudDataCount;

	public CloudDataCount getCloudDataCount() {
		return cloudDataCount;
	}

	public void setCloudDataCount(CloudDataCount cloudDataCount) {
		this.cloudDataCount = cloudDataCount;
	}

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	BranchMstRepository branchMstRepository;

	@Autowired
	StockTransferOutHdrService stockTransferOutHdrService;

	@Autowired
	DayEndClosureRepository dayEndClosureRepository;

	@Autowired
	StockTransferOutHdrRepository stockTransferOutHdrRepository;

	@Autowired
	StockTransferOutDtlRepository stockTransferOutDtlRepo;

	@Autowired
	ExternalApi externalApi;

	@Autowired
	LastBestDateRepository lastBestDateRepository;

	@Autowired
	LastBestDateServiceStockTransfer lastBestDateServiceStockTransfer;

	@Autowired
	DataSynchronisationService dataSynchronisationService;

	@Override
	public int getLocalCount() {

		stockTransferCount = stockTransferOutHdrRepository.getCountofStockTransferForDateWindow(companyMstId,
				lastBestDate.getLastSuccessDate(), lastBestDate.getDayEndDate(), branchCode);

		return stockTransferCount;
	}

	@Override
	public void countComparAndRetry() {

		boolean canRetry = false;

		Double serverDataCountInDouble = cloudDataCount.getServerCount("STOCKTRANSFER");
		int serverDataCount = serverDataCountInDouble.intValue();

		int localDataCount = getLocalCount();

		canRetry = serverDataCount != localDataCount;

		if (canRetry) {
			retry();

		} else {
			updateLastBestDate();

		}

	}

	@Override
	public String retry() {
		
		int startPage = 0;
		int endPage = 50;


		int recordCount = 0;


		while (true) {
			
			
			Pageable topFifty = PageRequest.of(startPage, endPage);
			
			ArrayList<StockTransferMessageEntity> stocktransferMessageEntityarray = new ArrayList();


			List<StockTransferOutHdr> hdrIdS = stockTransferOutHdrRepository
					.getStockTransferOutHdrBetweenDate(lastBestDate.getLastSuccessDate(), lastBestDate.getDayEndDate(),topFifty);
			
			if(hdrIdS.size() == 0)
			{
				break;
			}
				

			Iterator iter = hdrIdS.iterator();

			while (iter.hasNext()) {

				System.out.println("Building stocktransferMessageEntity recordCount" + recordCount);

				StockTransferMessageEntity stockTransferMessageEntity = new StockTransferMessageEntity();

				StockTransferOutHdr transHdr = (StockTransferOutHdr) iter.next();

				stockTransferMessageEntity.setStockTransferOutHdr(transHdr);

				List<StockTransferOutDtl> stockTransferOutDtl = stockTransferOutDtlRepo

						.findByStockTransferOutHdrId(transHdr.getId());

				stockTransferMessageEntity.getStockTransferOutDtlList().addAll(stockTransferOutDtl);

				stocktransferMessageEntityarray.add(stockTransferMessageEntity);

				recordCount++;

				System.out.println("Building stocktransferMessageEntity recordCount" + recordCount);

			}

			System.out.println("Send stocktransferMessageEntity Array*******************************");

			List<String> message = externalApi.BulkStockTransferToCloud(stocktransferMessageEntityarray,
					lastBestDate.getCompanyMst().getId());
			
			
			startPage = startPage + endPage;
			
			
		}


		return "OK";

	}

	private void updateLastBestDate() {

		lastBestDate.setLastSuccessDate(lastBestDate.getDayEndDate());

		lastBestDateRepository.save(lastBestDate);

	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCompanyMstId() {
		return companyMstId;
	}

	public void setCompanyMstId(String companyMstId) {
		this.companyMstId = companyMstId;
	}

	@Override
	public LastBestDate getLastBestDate() {

		lastBestDateServiceStockTransfer.setBranchcode(branchCode);
		lastBestDateServiceStockTransfer.setCompanymstid(companyMstId);
		lastBestDate = lastBestDateServiceStockTransfer.findLastBestDate();

		return lastBestDate;
	}
}
