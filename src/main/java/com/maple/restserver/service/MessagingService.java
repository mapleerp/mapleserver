package com.maple.restserver.service;

import java.util.List;

import com.maple.restserver.entity.ItemBatchMst;

public interface MessagingService {

	void sendItemBatchMstList(List<ItemBatchMst> itemBatchMstList);

	void sendMessage(String msgto, List dataList);

	void sendMessageString(String msgto, String message);


}
