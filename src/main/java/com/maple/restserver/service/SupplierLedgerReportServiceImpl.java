package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.report.entity.SaleOrderReport;
import com.maple.restserver.report.entity.StockSummaryReport;
import com.maple.restserver.report.entity.SupplierBillwiseDueDayReport;
import com.maple.restserver.report.entity.SupplierMonthlySummary;
import com.maple.restserver.report.entity.supplierLedger;
import com.maple.restserver.repository.AccountHeadsRepository;

@Service
@Transactional
@Component
public class SupplierLedgerReportServiceImpl  implements SupplierLedgerReportService{

	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	
	@Override
	public List<supplierLedger> getSupplierLedgerReport(String companymstid, String supplierid, String date,String branchcode) {

		
		List<supplierLedger> ledgerList= new ArrayList();
		
	//	 List<Object> obj =  SupplierRepository.getSupplierLedgerReport( companymstid,  supplierid,  date,branchcode);
		/* for(int i=0;i<obj.size();i++)
		 {*/
		
		
	//	List<Object> objList=SupplierRepository.getSupplierLedgerReport(companymstid,supplierid,date,branchcode);
		 
	//		 Object[] objAray = (Object[]) obj.get(i);
			 supplierLedger sReport =new supplierLedger();
	
			 sReport.setCompanyName("cmp");
			 sReport.setBranchAddress1("branch address");
			 sReport.setBranchName("branch name");
			 sReport.setBranchEmail("branchEmail");
			 sReport.setBranchWebsite("branch website");
			 sReport.setVoucherType("voucher type");
			 sReport.setCredit(5000.0);
			 sReport.setBranchPhoneNo("phone no");
			 sReport.setCustomerAccount("customer account");
			 sReport.setCredit(340.8);
			 sReport.setVoucherNumber("a34324");
			 sReport.setParticulars("particulars");
			 sReport.setCustomerName("customer name");
			 ledgerList.add(sReport);
			 
//	}

		 return ledgerList;

	}

	@Override
	public List<SupplierMonthlySummary> getMonthlySupplierLedgerReport(String companymstid, String supplierid,
			Date fromdate, Date todate, String branchcode) {
		
		List<SupplierMonthlySummary> supplierLedgerList= new ArrayList();
		List<Object> obj=accountHeadsRepository.getMonthlyAccountHeadsLedgerReport(companymstid,supplierid,branchcode,fromdate,todate);
	
 
		for(int i=0;i<obj.size();i++)
		 {
	
		Object[] objAray = (Object[]) obj.get(i);
		
		SupplierMonthlySummary monthlySummary=new SupplierMonthlySummary();
		monthlySummary.setCompanyName((String) objAray[0]);
		monthlySummary.setBranchName((String) objAray[1]);
		monthlySummary.setBranchAddress1((String) objAray[2]);
		monthlySummary.setBranchAddress2((String) objAray[3]);
		monthlySummary.setBranchState((String) objAray[4]);
		monthlySummary.setBranchGst((String) objAray[5]);
		monthlySummary.setBranchEmail((String) objAray[6]);
		monthlySummary.setBranchWebsite((String) objAray[7]);
		monthlySummary.setBranchPhoneNo((String) objAray[8]);
		monthlySummary.setSupplierName((String) objAray[9]);
		monthlySummary.setSupplierCompany((String) objAray[10]);
		monthlySummary.setSupplierPhoneNo((String) objAray[11]);
		monthlySummary.setSupplierEmail((String) objAray[12]);
		monthlySummary.setParticulars((String) objAray[13]);
		monthlySummary.setDebit((Double) objAray[14]);
		monthlySummary.setCredit((Double) objAray[15]);
		monthlySummary.setRefNo((String) objAray[16]);
		monthlySummary.setPendingAmount((Double) objAray[18]);
		monthlySummary.setStartDate((Date) objAray[17]);
//		monthlySummary.setClosingBalance((Double) objAray[13]);
		Double openingBalance=accountHeadsRepository.openingBalance(companymstid, fromdate, supplierid);
		
		monthlySummary.setOpeningAmount(openingBalance);
	
		
		supplierLedgerList.add(monthlySummary);
		
		 }
		return supplierLedgerList;
	}
	


	public List<SupplierBillwiseDueDayReport> getSupplierDueDayReport(String companymstid, String supplierid, Date fromDate,
			Date toDate) {
		List<SupplierBillwiseDueDayReport> supplierLedgerList= new ArrayList();

		List<Object> obj=accountHeadsRepository.getAccountHeadsDueDayReport(companymstid,supplierid,fromDate,toDate);

		for(int i=0;i<obj.size();i++)
		 {
	
		Object[] objAray = (Object[]) obj.get(i);
		
		SupplierBillwiseDueDayReport dueday=new SupplierBillwiseDueDayReport();
		dueday.setRefNo((String) objAray[0]);
		dueday.setSupplierName((String) objAray[1]);
		dueday.setSupplierPhoneNo((String) objAray[2]);
		dueday.setSupplierEmail((String) objAray[3]);
		dueday.setDate((Date) objAray[4]);
		dueday.setDueDate((Date) objAray[5]);
		dueday.setSupplierCompany((String) objAray[8]);
		dueday.setDueAmount((Double) objAray[6]);
		dueday.setPendingAmount((Double) objAray[9]);
		
		Double openingBalance=accountHeadsRepository.getOpeningBalanceFromLedgerClass(companymstid,supplierid,fromDate);
		Double closingBalance=accountHeadsRepository.getOpeningBalanceFromLedgerClass(companymstid,supplierid,toDate);

		dueday.setClosingAmount(closingBalance);
		dueday.setOpeningAmount(openingBalance);
		supplierLedgerList.add(dueday);

		
		 }
		
				return supplierLedgerList;
		
	}
}
