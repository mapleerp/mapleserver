package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.report.entity.DayEndProcessing;
import com.maple.restserver.report.entity.DayEndReceiptMode;
import com.maple.restserver.report.entity.DayEndReport;
import com.maple.restserver.report.entity.DayEndWebReport;
import com.maple.restserver.report.entity.PharmacyDayClosingReport;
import com.maple.restserver.report.entity.ReceiptModeReport;
import com.maple.restserver.report.entity.VoucherNumberDtlReport;
import com.maple.restserver.report.entity.WholeSaleDetailReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DailySalesSummaryRepository;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.ReceiptDtlRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component
public class DayEndReportServiceImpl implements DayEndReportService {

	
	@Autowired
SalesTransHdrRepository salesTransHdrRepository;
	

	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepo;
	@Autowired
	ReceiptDtlRepository receiptDtlRepository;
	@Autowired
	ReceiptHdrRepository receiptHdrRepository;
	
	@Autowired
	SalesReceiptsRepository salesReceiptsRepository;
	@Autowired
	PaymentHdrRepository PaymentHdrRepository;
	
	@Autowired
	DailySalesSummaryRepository dailySaleSummaryRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	DayEndClosureRepository dayEndClosureRepository;
	
	 
	
	@Override
	public DayEndReport getDayEndReport(String branch, Date date, String companymstid) {
		DayEndReport report =new DayEndReport();
		
		java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(date);
		
		
		List<Object> obj =  dailySaleSummaryRepo.getSalesReceipts( branch, sqlDate, companymstid);
		for(int i=0;i<obj.size();i++)
		{
			Object[] objAray = (Object[]) obj.get(i);
			ReceiptModeReport receiptModeReport =  new ReceiptModeReport();
			receiptModeReport.setAmount((Double) objAray[1]);
			receiptModeReport.setReceiptMode((String)objAray[0]);
			Double receiptAmount=+receiptModeReport.getAmount();
			report.setReceipt(receiptAmount);
	
		}

		ArrayList<DayEndReport> dayEndList = new ArrayList();
	
		//List<SalesTransHdr> salesTransList=salesTransHdrRepository.findAllByOrderByVoucherDateAsc(date);
        String receiptMode=branch+"-CASH";
		Double cashSales=salesTransHdrRepository.getDailyCashSales(receiptMode,date);
		
		Double receiptAmount=receiptHdrRepository.getDailyReceiptAmount(date,branch,companymstid);
		
		Double paymentAmount=PaymentHdrRepository.getDailyPaymentAmount(date, branch,companymstid);
		if(null!=cashSales) {
			report.setCashSales(cashSales);
		}
		
		if(null!=receiptAmount) {
		report.setReceipt(receiptAmount);
		}
		
		if(null!=paymentAmount) {
			report.setPayment(paymentAmount);
		}
		
		/*
		 * if(null!=cardSales) { report.setCardSales(cardSales);
		 * 
		 * }
		 */
		return report;
	}


	@Override
	public VoucherNumberDtlReport getVoucherNumberDetailReport(String branch, Date vDate, String companymstid) {
	
		VoucherNumberDtlReport vboucherNumberDtlReport= new VoucherNumberDtlReport();
		Integer posStartingVoucherNumber=salesTransHdrRepository.postStartingVoucherNumber(vDate,branch,companymstid);
		Integer posEndingVoucherNumber=salesTransHdrRepository.posEndingVoucherNumber(vDate,branch,companymstid);
		
		 if(null==posStartingVoucherNumber) {
			 posStartingVoucherNumber=0;
		 }

		 if(null==posEndingVoucherNumber) {
			 posEndingVoucherNumber=0;
		 }

		
		Integer posBillCount=posEndingVoucherNumber-posStartingVoucherNumber;
		
		

		 if(null==posBillCount) {
			 posBillCount=0;
		 }

		 
		 if(posEndingVoucherNumber>0) {
			posBillCount++;
		}
		
		
		
		Integer wholeSaleStartingVBillNumber=salesTransHdrRepository.wholeSaleStartingVBillNumber(vDate,branch,companymstid);
		Integer wholeSaleEndingVBillNumber=salesTransHdrRepository.wholeSaleEndingVBillNumber(vDate,branch,companymstid);
		

		 if(null==wholeSaleStartingVBillNumber) {
			 wholeSaleStartingVBillNumber=0;
		 }

		 if(null==wholeSaleEndingVBillNumber) {
			 wholeSaleEndingVBillNumber=0;
		 }

		Integer	wholeSaleBillCount=wholeSaleEndingVBillNumber-wholeSaleStartingVBillNumber;
		
		 if(null==wholeSaleBillCount) {
			 wholeSaleBillCount=0;
		 }

		if(wholeSaleEndingVBillNumber>0) {
			wholeSaleBillCount++;
		}
		
		vboucherNumberDtlReport.setPosStartingBillNumber(posStartingVoucherNumber);
		vboucherNumberDtlReport.setPosEndingBillNumber(posEndingVoucherNumber);
		vboucherNumberDtlReport.setPosBillCount(posBillCount);
		vboucherNumberDtlReport.setWholeSaleStartingBillNumber(wholeSaleStartingVBillNumber);
		vboucherNumberDtlReport.setWholeSaleEndingBillNumber(wholeSaleEndingVBillNumber);
		vboucherNumberDtlReport.setWholeSaleBillCount(wholeSaleBillCount);
		return vboucherNumberDtlReport;
	
	}
	@Override
	public Double getCreditSaleAdjustment(Date date, String companymstid, String branchcode) {
		
		Double cash = 0.0;
		
		 cash = receiptDtlRepository.getCreditSaleAdjustment(date,branchcode+"-CASH");
		if(null == cash)
		{
			cash = 0.0;
		}
		return cash;
	}


	@Override
	public Double getOtherCashSale(Date date, String companymstid, String branchcode) {
		Double cash = 0.0;
		
		 cash = receiptDtlRepository.getOtherCashSale(date,companymstid,branchcode,branchcode+"-CASH");
		if(null == cash)
		{
			cash = 0.0;
		}
		return cash;
	}


	@Override
	public List<DayEndProcessing> getDayEndReceiptMode(String companymstid, String branchcode, Date date) {

		List<DayEndReceiptMode> dayEndreceiptModeList = new ArrayList<DayEndReceiptMode>();
		List<DayEndProcessing> dayEndProcessingList = new ArrayList<DayEndProcessing>();
		List<Object> objSales = salesTransHdrRepository.getSalesTransHdrByDateAndBranchCodeAndCompanyMstId(date,branchcode,companymstid);
		for(int i=0;i<objSales.size();i++)
		{
			Object[] objAray = (Object[]) objSales.get(i);
			DayEndReceiptMode dayEndReceiptMode =  new DayEndReceiptMode();
			String receiptMode = (String) objAray[0];
			if(receiptMode.equalsIgnoreCase("CREDIT"))
			{
				List<Object> objSalesCredit = salesTransHdrRepository.getSalesTransHdrCreditSplit(date,branchcode,companymstid);
				{
					for(int ii=0;ii<objSalesCredit.size();ii++)
					{
						 dayEndReceiptMode =  new DayEndReceiptMode();
					Object[] objAray1 = (Object[]) objSalesCredit.get(ii);
					dayEndReceiptMode.setAmount((Double) objAray1[1]);
					dayEndReceiptMode.setReceiptMode((String) objAray1[0]);
					dayEndReceiptMode.setType("SALES");
					dayEndreceiptModeList.add(dayEndReceiptMode);
					}
				}
				List<Object> objSalesOnline = salesTransHdrRepository.getSalesTransHdrOnline(date,branchcode,companymstid);
				{
					for(int iij=0;iij<objSalesOnline.size();iij++)
					{
						 dayEndReceiptMode =  new DayEndReceiptMode();
					Object[] objAray1 = (Object[]) objSalesOnline.get(iij);
					dayEndReceiptMode.setAmount((Double) objAray1[1]);
					dayEndReceiptMode.setReceiptMode((String) objAray1[0]);
					dayEndReceiptMode.setType("SALES");
					dayEndreceiptModeList.add(dayEndReceiptMode);
					}
				}
			}
			else
			{
			dayEndReceiptMode.setAmount((Double) objAray[1]);
			dayEndReceiptMode.setReceiptMode((String) objAray[0]);
			dayEndReceiptMode.setType("SALES");
			}
			dayEndreceiptModeList.add(dayEndReceiptMode);
		}
		
		List<Object> objSoRealized  = salesOrderTransHdrRepo.getSaleOrderReceiptByBranchCodeDateAndCompanymst(date,branchcode,companymstid);		
		for(int i=0;i<objSoRealized.size();i++)
		{
			Object[] objAray = (Object[]) objSoRealized.get(i);
			DayEndReceiptMode dayEndReceiptMode =  new DayEndReceiptMode();
			
			Double totalAmount = 0.0;
			if(null != (Double) objAray[1])
			{
				totalAmount = (Double) objAray[1];
			}
			
			Double realizedAmount = 0.0;
			if(null != (Double) objAray[2])
			{
				realizedAmount = (Double) objAray[2];
			}
			
			dayEndReceiptMode.setAmount(totalAmount-realizedAmount);
			dayEndReceiptMode.setReceiptMode((String) objAray[0]);
			dayEndReceiptMode.setType("SOREALIZED");
			dayEndreceiptModeList.add(dayEndReceiptMode);
		}
		List<Object> objSoOrdered = salesOrderTransHdrRepo.getSalesOrderByDateBranchCodeAndCompanyMst(date,branchcode,companymstid);
		for(int i=0;i<objSoOrdered.size();i++)
		{
			Object[] objAray = (Object[]) objSoOrdered.get(i);
			DayEndReceiptMode dayEndReceiptMode =  new DayEndReceiptMode();
			dayEndReceiptMode.setAmount((Double) objAray[1]);
			dayEndReceiptMode.setReceiptMode((String) objAray[0]);
			dayEndReceiptMode.setType("SO");
			dayEndreceiptModeList.add(dayEndReceiptMode);
		}
		
		for(int i=0;i<dayEndreceiptModeList.size();i++)
		 {
			
			boolean status=false;
			
			 DayEndProcessing dayendProcess = new DayEndProcessing();
			for(DayEndProcessing day:dayEndProcessingList)
			{
				if(day.getReceiptMode().equalsIgnoreCase(dayEndreceiptModeList.get(i).getReceiptMode()))
				{
					status = true;
					break;
				}
			}
			if(status)
			{
				continue;
			}

			 dayendProcess.setReceiptMode(dayEndreceiptModeList.get(i).getReceiptMode());
			 for(int j =0;j<dayEndreceiptModeList.size();j++)
			 {
				 if(dayEndreceiptModeList.get(j).getReceiptMode().equalsIgnoreCase(dayendProcess.getReceiptMode()))
				 {
					 if(dayEndreceiptModeList.get(j).getType().equalsIgnoreCase("SALES"))
					 {
						 dayendProcess.setSalesAmount(dayEndreceiptModeList.get(j).getAmount());
					 }
					 else if(dayEndreceiptModeList.get(j).getType().equalsIgnoreCase("SOREALIZED"))
					 {
						 dayendProcess.setRealizedAmount(dayEndreceiptModeList.get(j).getAmount());
					 }
					 else
					 {
						 dayendProcess.setSoAmount(dayEndreceiptModeList.get(j).getAmount());
					 }
				 }
			 }
			 dayEndProcessingList.add(dayendProcess);
		 }
		
		for(DayEndProcessing dayEnd : dayEndProcessingList)
		{
			if(null == dayEnd.getRealizedAmount())
			{
				dayEnd.setRealizedAmount(0.0);
			}
			
			if(null == dayEnd.getSalesAmount())
			{
				dayEnd.setSalesAmount(0.0);
			}
			
			if(null == dayEnd.getSoAmount())
			{
				dayEnd.setSoAmount(0.0);
			}
		}

		return dayEndProcessingList;
	}


	@Override
	public Double getPaymentbyaccountId(Date date, String companymstid, String branchcode, String accountid) {
		
		
		return dailySaleSummaryRepo.dayendreportsummarybyaccountid(date ,companymstid,branchcode, accountid);
		//companymstid, branchcode,
		
	}


	@Override
	public VoucherNumberDtlReport getPosVoucherNumberSummary(String branch, Date vDate, String companymstid,
			String salesprefix) {

		
		VoucherNumberDtlReport vboucherNumberDtlReport= new VoucherNumberDtlReport();
		Integer posStartingVoucherNumber=salesTransHdrRepository.postStartingVoucherNumberByPreFix(vDate,branch,companymstid,"%"+salesprefix+"%");
		Integer posEndingVoucherNumber=salesTransHdrRepository.posEndingVoucherNumberByPreFix(vDate,branch,companymstid,"%"+salesprefix+"%");
		
		 if(null==posStartingVoucherNumber) {
			 posStartingVoucherNumber=0;
		 }

		 if(null==posEndingVoucherNumber) {
			 posEndingVoucherNumber=0;
		 }

		
		Integer posBillCount=posEndingVoucherNumber-posStartingVoucherNumber;
		
		

		 if(null==posBillCount) {
			 posBillCount=0;
		 }

		 
		 if(posEndingVoucherNumber>0) {
			posBillCount++;
		}
		
		
		
		
		
		vboucherNumberDtlReport.setPosStartingBillNumber(posStartingVoucherNumber);
		vboucherNumberDtlReport.setPosEndingBillNumber(posEndingVoucherNumber);
		vboucherNumberDtlReport.setPosBillCount(posBillCount);
		
		
		return vboucherNumberDtlReport;
	
		
	
	}

	@Override
	public List<PharmacyDayClosingReport> findPharmacyDayClosingReport(Date date) {
		
		List<PharmacyDayClosingReport> pharmacyDayClosingReportList = new ArrayList<PharmacyDayClosingReport>();
		List<Object> obj = salesReceiptsRepository.findPharmacyDayClosingReport(date);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			PharmacyDayClosingReport pharmacyDayClosingReport = new PharmacyDayClosingReport();
			
			pharmacyDayClosingReport.setBranchCode((String) objAray[0]);
			pharmacyDayClosingReport.setTotalcreditsale((Double) objAray[1]);
			pharmacyDayClosingReport.setTotalcashSale((Double) objAray[2]);
			pharmacyDayClosingReport.setTotalcardSale((Double) objAray[3]);
			pharmacyDayClosingReport.setTotalinsurancesales((Double) objAray[4]);
			pharmacyDayClosingReport.setTotalbanksales((Double) objAray[5]);
			pharmacyDayClosingReport.setGrandtotal((Double) objAray[6]);
			
			
			pharmacyDayClosingReport.setPhysicalCash((Double) objAray[7]);
			pharmacyDayClosingReport.setDiffenceamount((Double) objAray[8]);
			
			
			pharmacyDayClosingReportList.add(pharmacyDayClosingReport);
			
		 }

		return pharmacyDayClosingReportList;
	
	}


	@Override
	public List<DayEndWebReport> getDayEndWebReportForWithTotalSalesHC(Date date, String companymstid) {

		List<DayEndWebReport> dayEndWebReportList = new ArrayList();

		java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(date);

		List<Object> listObj = salesTransHdrRepository.getDayEndWebReportForHC(sqlDate, companymstid);
		for (int i = 0; i < listObj.size(); i++) {
			Object[] objectArray = (Object[]) listObj.get(i);

			DayEndWebReport dayEndWebReport = new DayEndWebReport();

			dayEndWebReport.setCardSales((Double) objectArray[2]);
			dayEndWebReport.setCashSales((Double) objectArray[1]);
			dayEndWebReport.setBranchCode((String) objectArray[0]);
			dayEndWebReport.setCreditSales((Double) objectArray[3]);
			dayEndWebReport.setCard2((Double) objectArray[4]);

			DayEndClosureHdr dayEndClosureHdr = dayEndClosureRepository.getDayEndClosureHdrByDateAndBranchCode(sqlDate,
					companymstid, dayEndWebReport.getBranchCode());
			if (null != dayEndClosureHdr) {
				if (null != dayEndClosureHdr.getPhysicalCash()) {
					dayEndWebReport.setPhysicalCash(dayEndClosureHdr.getPhysicalCash());

				}

				if (null != dayEndClosureHdr.getTotalSales()) {
					dayEndWebReport.setTotalSales(dayEndClosureHdr.getTotalSales());
//					dayEndWebReport.setCashSales(dayEndClosureHdr.getTotalSales());

				}
			} else {
				dayEndWebReport.setPhysicalCash(0.0);
			}
			dayEndWebReportList.add(dayEndWebReport);
		}

		return dayEndWebReportList;

	}
		
		
		
		
		
	
}
