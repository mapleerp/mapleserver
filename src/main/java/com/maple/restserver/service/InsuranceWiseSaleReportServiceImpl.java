package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 

import com.maple.restserver.report.entity.InsuranceWiseSalesReport;
import com.maple.restserver.repository.InsuranceDtlRepository;

@Service
@Transactional

public class InsuranceWiseSaleReportServiceImpl implements InsuranceWiseSaleReportService {
	
	@Autowired
	InsuranceDtlRepository insuranceDtlRepository;

	@Override
	public List<InsuranceWiseSalesReport> getInsurancewisesalesreport(Date sdate, Date edate) {
		
		List<InsuranceWiseSalesReport> getInsurancewisesalesList= new ArrayList<InsuranceWiseSalesReport>();
		List<Object> obj = insuranceDtlRepository.findInsurancewisesales(sdate,edate);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			InsuranceWiseSalesReport insuranceWiseSalesReport = new InsuranceWiseSalesReport();
			
			
			insuranceWiseSalesReport.setInvoiceDate((String) objAray[0].toString());
			insuranceWiseSalesReport.setVocherNumber((String) objAray[1]);
			insuranceWiseSalesReport.setPatientId((String) objAray[2]);
			insuranceWiseSalesReport.setInvoiceAmount((Double) objAray[3]);
			insuranceWiseSalesReport.setCreditAmount((Double) objAray[4]);
			insuranceWiseSalesReport.setCashPaid((Double) objAray[5]);
			insuranceWiseSalesReport.setCardPaid((Double) objAray[6]);
			insuranceWiseSalesReport.setCustomerName((String) objAray[7]);
			insuranceWiseSalesReport.setUsrName((String) objAray[8]);
			insuranceWiseSalesReport.setInsuCard((String) objAray[9]);
			insuranceWiseSalesReport.setPolicyType((String) objAray[10]);
			insuranceWiseSalesReport.setInsComapany((String) objAray[11]);
			
		
			
			
		
			getInsurancewisesalesList.add(insuranceWiseSalesReport);
			
		 }
		
		return getInsurancewisesalesList;
	}
	
	

	
	
	
}
