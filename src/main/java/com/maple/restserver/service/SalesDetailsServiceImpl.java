package com.maple.restserver.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
 
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.InsuranceCompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesMessageEntity;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.report.entity.HsnCodeSaleReport;
import com.maple.restserver.report.entity.InsuranceCompanySalesSummaryReport;
import com.maple.restserver.report.entity.PurchaseReport;
import com.maple.restserver.report.entity.RetailSalesDetailReport;
import com.maple.restserver.report.entity.RetailSalesSummaryReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.SalesReport;
import com.maple.restserver.repository.AccountReceivableRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.InsuranceCompanyRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.UserMstRepository;
import com.maple.restserver.utils.SystemSetting;
 

 
@Service
@Transactional

public class SalesDetailsServiceImpl implements SalesDetailsService{
	
	@Autowired
	SalesDetailsRepository salesdtlDtlRepo;
	boolean recurssionOff =false;
	@Autowired
	SalesReceiptsRepository salesReceiptRepo;
	@Autowired
	InsuranceCompanyRepository insuranceCompanyMstRepo;
	@Autowired
	CategoryMstRepository categoryMstRepo;
	@Autowired
	MultiUnitMstRepository multiUnitMstRepository;
	@Autowired 
	SalesDetailsRepository salesDetailsRepository;
	
	@Autowired ItemMstRepository itemMstRepository;

	@Autowired
	AccountReceivableRepository accountReceivableRepo;
	@Autowired
	UserMstRepository userMstRepo;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepo;

	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@Autowired
	InsuranceService insuranceService;
	
	@Autowired
	BranchMstRepository branchMstRepo;
	@Override
	public Double getSalesDetailItemQty(String companyMstId,String salesTransHdrID, String itemId, String batch) 
	{
		
		List<SalesDtl> salesDtlList = salesDetailsRepository.findBySalesTransHdrIdAndItemIdAndBatch(salesTransHdrID,itemId,batch);
		double sourceQty = 0,source = 0;
		for(SalesDtl salesDtl : salesDtlList)
		{
			sourceQty = salesDtl.getQty();
			 recurssionOff =false;	
			Optional<ItemMst> item = itemMstRepository.findById(salesDtl.getItemId());
			String targetUnit = item.get().getUnitId();
			String sourceUnit = salesDtl.getUnitId();
			if(salesDtl.getUnitId().equalsIgnoreCase(item.get().getUnitId()))
			{
				 if(recurssionOff) {
					 return sourceQty;
				 }
				 MultiUnitMst  multiUnitMstList =  multiUnitMstRepository.findByCompanyMstIdAndItemIdAndUnit1(companyMstId, itemId, sourceUnit);
				
				 while (!multiUnitMstList.getUnit2().equalsIgnoreCase(targetUnit)) {
				
					 if(recurssionOff) {
						break;
					 }
					 sourceUnit = multiUnitMstList.getUnit2();
					 
				 
					 sourceQty = sourceQty * multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
					 
					 getConvertionQty(  companyMstId ,   itemId, sourceUnit,   targetUnit, sourceQty) ;
					
				 
				 }
				 sourceQty = sourceQty * multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
				 recurssionOff = true;
				 //sourceQty = sourceQty * multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
				
			}
			source = salesDtl.getQty()+sourceQty;
		}
		return source;
	
	
		
	}
	private double getConvertionQty(String companyMstId , String itemId, String sourceUnit, String targetUnit, double sourceQty ) {
		 if(recurssionOff) {
	 recurssionOff =false;	
		
		
			 if(recurssionOff) {
				 return sourceQty;
			 }
			 MultiUnitMst  multiUnitMstList =  multiUnitMstRepository.findByCompanyMstIdAndItemIdAndUnit1(companyMstId, itemId, sourceUnit);
			
			 while (!multiUnitMstList.getUnit2().equalsIgnoreCase(targetUnit)) {
			
				 if(recurssionOff) {
					break;
				 }
				 sourceUnit = multiUnitMstList.getUnit2();
				 
			 
				 sourceQty = sourceQty * multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
				 
				 getConvertionQty(  companyMstId ,   itemId, sourceUnit,   targetUnit, sourceQty) ;
				
			 
			 }
			 sourceQty = sourceQty * multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
			 recurssionOff = true;
			 //sourceQty = sourceQty * multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
			
		}
		 return sourceQty;
}
	@Override
	public List<SalesDtl> findBySalesTransHdrAndCategoryIdd(String salestranshdrId, String categoryid) {
	
		List<Object> objectList = salesDetailsRepository.findBySalesTransHdrAndCategoryIdd(salestranshdrId,categoryid);

		List<SalesDtl> salesDtlList = new ArrayList();
		
		for(int i=0; i<objectList.size(); i++)
		{
			
			Object[] objAray = (Object[]) objectList.get(i);
			Optional<SalesDtl>  sales =  salesDetailsRepository.findById((String) objAray[4]);
			
			
			salesDtlList.add(sales.get());
		}
		
		
		return salesDtlList;
	}
	@Override
	public List<HsnCodeSaleReport> getHSNCodeWiseSalesDtl(Date fdate, Date tdate,CompanyMst companyMst, String branchCode) {
		
		List<HsnCodeSaleReport> hsnCodeReportList = new ArrayList<HsnCodeSaleReport>();
		List<Object> obj = salesDetailsRepository.getHSNCodeWiseSalesDtl(fdate,  tdate,companyMst , branchCode);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			HsnCodeSaleReport hsnCodeSaleReport = new HsnCodeSaleReport();
			hsnCodeSaleReport.setHsnCode((String) objAray[0]);
			hsnCodeSaleReport.setQty((Double) objAray[1]);
			hsnCodeSaleReport.setCgst((Double) objAray[2]);
			hsnCodeSaleReport.setSgst((Double) objAray[3]);
			hsnCodeSaleReport.setValue((Double) objAray[4]);
			hsnCodeSaleReport.setVoucherDate((Date) objAray[5]);
			hsnCodeReportList.add(hsnCodeSaleReport);
		 }

		return hsnCodeReportList;
	}
	@Override
	public List<SalesInvoiceReport> getCustomerWiseSalesReportWithCategoryList(Date fudate, Date tudate, CompanyMst companyMst,
			String branchcode, String[] array, String customername) {
	
		List<SalesInvoiceReport> salesReportList = new ArrayList();
		for(String categoryName:array) {
			CategoryMst categoryMst=categoryMstRepo.findByCategoryNameAndCompanyMst(categoryName,companyMst);
		List<Object> obj = salesDetailsRepository.getCustomerwiseSalesByCategoryAndDate(fudate,tudate,companyMst,categoryMst.getId(),customername,branchcode);
		for(int i=0;i<obj.size();i++)
		 {
			
			 
			
			 Object[] objAray = (Object[]) obj.get(i);
			 SalesInvoiceReport salesreport = new SalesInvoiceReport();
			 salesreport.setAmount((Double) objAray[6]);
			 salesreport.setBatch((String) objAray[8]);
			 salesreport.setCustomerName((String) objAray[7]);
			 salesreport.setItemName((String) objAray[2]);
			 salesreport.setVoucherNumber((String) objAray[0]);
			 salesreport.setVoucherDate((Date) objAray[1]);
			 salesreport.setIgstAmount((Double) objAray[3]);
			 salesreport.setRate((Double) objAray[5]);
			 salesreport.setQty((Double) objAray[4]);
			 salesReportList.add(salesreport);
		 }
		}
		return salesReportList;
	}
	@Override
	public List<SalesInvoiceReport> getCustomerWiseSalesReport(Date fudate, Date tudate, CompanyMst companyMst,
			String branchcode, String customername) {
		List<SalesInvoiceReport> salesReportList = new ArrayList();
		List<Object> obj = salesDetailsRepository.getCustomerwiseSalesByDate(fudate,tudate,companyMst,customername,branchcode);
		for(int i=0;i<obj.size();i++)
		 {
			
			 
			
			 Object[] objAray = (Object[]) obj.get(i);
			 SalesInvoiceReport salesreport = new SalesInvoiceReport();
			 salesreport.setAmount((Double) objAray[6]);
			 salesreport.setBatch((String) objAray[8]);
			 salesreport.setCustomerName((String) objAray[7]);
			 salesreport.setItemName((String) objAray[2]);
			 salesreport.setVoucherNumber((String) objAray[0]);
			 salesreport.setVoucherDate((Date) objAray[1]);
			 salesreport.setIgstAmount((Double) objAray[3]);
			 salesreport.setRate((Double) objAray[5]);
			 salesreport.setQty((Double) objAray[4]);
			 salesReportList.add(salesreport);
		 }
		return salesReportList;
		}
	@Override
	public List<InsuranceCompanySalesSummaryReport> getSalesSummaryWithInsurance(Date fudate, Date tudate, String branchcode) {

		List<InsuranceCompanySalesSummaryReport> insuranceCompanySalesSummaryReportList = new ArrayList();
		List<InsuranceCompanyMst> insuranceCompanyMstlist = insuranceCompanyMstRepo.findAll();
		for(int i =0;i<insuranceCompanyMstlist.size();i++)
		{
			List<SalesReceipts> salesReceiptList = salesReceiptRepo.findByAccountIdAndDate(insuranceCompanyMstlist.get(i).getId(),fudate,tudate);
			for(int j =0;j<salesReceiptList.size();j++)
			{
				
				List<Object> obj = salesDetailsRepository.getSalesSummaryByInsurance(salesReceiptList.get(j).getSalesTransHdr().getId(),branchcode);
				for(int k=0;k<obj.size();k++)
				 {
					 Object[] objAray = (Object[]) obj.get(k);
					 InsuranceCompanySalesSummaryReport insuranceCompanySalesSummaryReport = new InsuranceCompanySalesSummaryReport();
//					insuranceCompanySalesSummaryReport.setCardPaid((Double) objAray[6]);
//					insuranceCompanySalesSummaryReport.setCashPaid((Double) objAray[5]);
//					insuranceCompanySalesSummaryReport.setInvoiceAmount((Double) objAray[7]);
//					insuranceCompanySalesSummaryReport.setCreditAmount(insuranceCompanySalesSummaryReport.getInvoiceAmount()-(insuranceCompanySalesSummaryReport.getCardPaid()+insuranceCompanySalesSummaryReport.getCashPaid()));
//					insuranceCompanySalesSummaryReport.setCustomerName((String) objAray[2]);
//					insuranceCompanySalesSummaryReport.setInsuranceAmount(salesReceiptList.get(j).getReceiptAmount());
//					insuranceCompanySalesSummaryReport.setIsuranceCompanyName(insuranceCompanyMstlist.get(i).getInsuranceCompanyName());
//					insuranceCompanySalesSummaryReport.setVoucherDate((Date) objAray[0]);
//					insuranceCompanySalesSummaryReport.setVoucherNumber((String) objAray[1]);
//					insuranceCompanySalesSummaryReport.setUserName((String) objAray[8]);
//					insuranceCompanySalesSummaryReport.setPolicyType((String) objAray[4]);
//					insuranceCompanySalesSummaryReport.setInsuranceCard((String) objAray[3]);
//					insuranceCompanySalesSummaryReportList.add(insuranceCompanySalesSummaryReport);
					
					
					
					insuranceCompanySalesSummaryReport.setCardPaid((Double) objAray[5]);
					insuranceCompanySalesSummaryReport.setCashPaid((Double) objAray[4]);
					insuranceCompanySalesSummaryReport.setInvoiceAmount((Double) objAray[6]);
					insuranceCompanySalesSummaryReport.setCreditAmount(insuranceCompanySalesSummaryReport.getInvoiceAmount()-(insuranceCompanySalesSummaryReport.getCardPaid()+insuranceCompanySalesSummaryReport.getCashPaid()));
					//insuranceCompanySalesSummaryReport.setCustomerName((String) objAray[2]);
					insuranceCompanySalesSummaryReport.setInsuranceAmount(salesReceiptList.get(j).getReceiptAmount());
					insuranceCompanySalesSummaryReport.setIsuranceCompanyName(insuranceCompanyMstlist.get(i).getInsuranceCompanyName());
					insuranceCompanySalesSummaryReport.setVoucherDate((Date) objAray[0]);
					insuranceCompanySalesSummaryReport.setVoucherNumber((String) objAray[1]);
					insuranceCompanySalesSummaryReport.setUserName((String) objAray[7]);
					insuranceCompanySalesSummaryReport.setPolicyType((String) objAray[3]);
					insuranceCompanySalesSummaryReport.setPatientName((String) objAray[8]);	
					insuranceCompanySalesSummaryReport.setInsuranceCard((String) objAray[2]);
					insuranceCompanySalesSummaryReportList.add(insuranceCompanySalesSummaryReport);
					
			
//					insuranceCompanySalesSummaryReport.set
					
				
				}
			}
			
			
			
		}

		return insuranceCompanySalesSummaryReportList;
	}
	@Override
	public List<InsuranceCompanySalesSummaryReport> getSalesSummaryWithInsuranceName(Date fudate, Date tudate,
			String branchcode, String[] insurancenameList) {
		List<InsuranceCompanySalesSummaryReport> insuranceCompanySalesSummaryReportList = new ArrayList();
		for(String insurancename:insurancenameList)
		{
		List<InsuranceCompanyMst> insuranceCompanyMstlist = insuranceCompanyMstRepo.findByInsuranceCompanyName(insurancename);
		for(int i =0;i<insuranceCompanyMstlist.size();i++)
		{
			List<SalesReceipts> salesReceiptList = salesReceiptRepo.findByAccountIdAndDate(insuranceCompanyMstlist.get(i).getId(),fudate,tudate);
			for(int j =0;j<salesReceiptList.size();j++)
			{
				List<Object> obj = salesDetailsRepository.getSalesSummaryByInsurance(salesReceiptList.get(j).getSalesTransHdr().getId(),branchcode);
				for(int k=0;k<obj.size();k++)
				 {
					 Object[] objAray = (Object[]) obj.get(k);
					 InsuranceCompanySalesSummaryReport insuranceCompanySalesSummaryReport = new InsuranceCompanySalesSummaryReport();
//					insuranceCompanySalesSummaryReport.setCardPaid((Double) objAray[6]);
//					insuranceCompanySalesSummaryReport.setCashPaid((Double) objAray[5]);
//					insuranceCompanySalesSummaryReport.setInvoiceAmount((Double) objAray[7]);
//					insuranceCompanySalesSummaryReport.setCreditAmount(insuranceCompanySalesSummaryReport.getInvoiceAmount()-(insuranceCompanySalesSummaryReport.getCardPaid()+insuranceCompanySalesSummaryReport.getCashPaid()));
//					insuranceCompanySalesSummaryReport.setCustomerName((String) objAray[2]);
//					insuranceCompanySalesSummaryReport.setInsuranceAmount(salesReceiptList.get(j).getReceiptAmount());
//					insuranceCompanySalesSummaryReport.setIsuranceCompanyName(insuranceCompanyMstlist.get(i).getInsuranceCompanyName());
//					insuranceCompanySalesSummaryReport.setVoucherDate((Date) objAray[0]);
//					insuranceCompanySalesSummaryReport.setVoucherNumber((String) objAray[1]);
//					insuranceCompanySalesSummaryReport.setUserName((String) objAray[8]);
//					insuranceCompanySalesSummaryReport.setPolicyType((String) objAray[4]);
//					insuranceCompanySalesSummaryReport.setInsuranceCard((String) objAray[3]);
//					insuranceCompanySalesSummaryReport.setPatientName((String) objAray[9]);			
//					insuranceCompanySalesSummaryReportList.add(insuranceCompanySalesSummaryReport);
					 
					
					
					insuranceCompanySalesSummaryReport.setCardPaid((Double) objAray[5]);
					insuranceCompanySalesSummaryReport.setCashPaid((Double) objAray[4]);
					
					insuranceCompanySalesSummaryReport.setInvoiceAmount((Double) objAray[6]);
					insuranceCompanySalesSummaryReport.setCreditAmount(insuranceCompanySalesSummaryReport.getInvoiceAmount()-(insuranceCompanySalesSummaryReport.getCardPaid()+insuranceCompanySalesSummaryReport.getCashPaid()));
//					insuranceCompanySalesSummaryReport.setCustomerName((String) objAray[2]);
					insuranceCompanySalesSummaryReport.setInsuranceAmount(salesReceiptList.get(j).getReceiptAmount());
					insuranceCompanySalesSummaryReport.setIsuranceCompanyName(insuranceCompanyMstlist.get(i).getInsuranceCompanyName());
					insuranceCompanySalesSummaryReport.setVoucherDate((Date) objAray[0]);
					insuranceCompanySalesSummaryReport.setVoucherNumber((String) objAray[1]);
					insuranceCompanySalesSummaryReport.setUserName((String) objAray[7]);
					insuranceCompanySalesSummaryReport.setPolicyType((String) objAray[3]);
					insuranceCompanySalesSummaryReport.setInsuranceCard((String) objAray[2]);
					insuranceCompanySalesSummaryReport.setPatientName((String) objAray[8]);			
					insuranceCompanySalesSummaryReportList.add(insuranceCompanySalesSummaryReport);
				
			
				 
				 
				 }
			}
			
		}
		}
		

		
		return insuranceCompanySalesSummaryReportList;
	}
	@Override
	public void resendBranchSales(String id) {


		// String voucherNumber = lmsQueueMst.getVoucherNumber();

		SalesTransHdr hdrIdS = salesTransHdrRepo.findById(id).get();

	

		SalesMessageEntity salesMessageEntity = new SalesMessageEntity();

		salesMessageEntity.setSalesTransHdr(hdrIdS);
		List<SalesDtl> salesDtl = salesdtlDtlRepo.findBySalesTransHdrId(id);

		salesMessageEntity.getSalesDtlList().addAll(salesDtl);

		List<SalesReceipts> salesReceipts = salesReceiptRepo.findBySalesTransHdr(hdrIdS);
		salesMessageEntity.getSalesReceiptsList().addAll(salesReceipts);

		List<AccountReceivable> accReceivableList = accountReceivableRepo
				.findByCompanyMstAndSalesTransHdr(hdrIdS.getCompanyMst(), hdrIdS);
		salesMessageEntity.getAccountReceivable().addAll(accReceivableList);

		try {
			// ----------version 4.19 Surya
			if (!hdrIdS.getIsBranchSales().equalsIgnoreCase("Y")) {
				Optional<BranchMst> branchMst = branchMstRepo.findById(hdrIdS.getAccountHeads().getId());

				//jmsTemplate.convertAndSend(branchMst.get().getBranchCode() + ".sales", salesMessageEntity);

				System.out.println(branchMst.get().getBranchCode() + ".sales");

				// jmsTemplate.convertAndSend(branchMst.get().getBranchCode()+".accountreceivable",
				// accReceivableList);
			}
			// ----------version 4.19 Surya end

			//jmsTemplate.convertAndSend("server.sales", salesMessageEntity);

		} catch (Throwable e) {
			System.out.print(e.toString());
		}
		String isBranchSales = hdrIdS.getIsBranchSales();
		if (null != isBranchSales) {
			if (isBranchSales.equalsIgnoreCase("Y")) {
				Optional<BranchMst> branchMst = branchMstRepo.findById(hdrIdS.getAccountHeads().getId());
				BranchMst branchMst2 = branchMstRepo.getMyBranch();
				salesMessageEntity.setBranchMst(branchMst2);
				try {
					//jmsTemplate.convertAndSend(branchMst.get().getBranchCode() + ".branchsales",
						//	salesMessageEntity);

				} catch (Throwable e) {
					System.out.print(e.toString());
				}
			}
		}


		
		
	}
	

	
	
	@Override
	public List<RetailSalesDetailReport> getRetailSalesDetailReport(Date fromDate, Date toDate) {
		List<RetailSalesDetailReport> retailSalesDetailReportList = new ArrayList<RetailSalesDetailReport>();
		List<Object> obj = salesDetailsRepository.getRetailSalesDetailReportNew(fromDate,toDate);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			RetailSalesDetailReport retailSalesDetailReport = new RetailSalesDetailReport();
			
			retailSalesDetailReport.setInvoiceDate(SystemSetting.UtilDateToString((Date) objAray[0], "yyyy-MM-dd"));
			retailSalesDetailReport.setVoucherNum((String) objAray[1]);
			retailSalesDetailReport.setCustomerName((String) objAray[2]);
//			retailSalesDetailReport.setPatientID((String) objAray[3]);
//			retailSalesDetailReport.setPatientName((String) objAray[4]);
			retailSalesDetailReport.setItemName((String) objAray[3]);
			retailSalesDetailReport.setGroupName((String) objAray[4]);
			if(null != objAray[5]) {
			retailSalesDetailReport.setExpiryDate(SystemSetting.UtilDateToString((Date) objAray[5], "yyyy-MM-dd"));
			}
			retailSalesDetailReport.setBatchCode((String) objAray[6]);
			retailSalesDetailReport.setItemCode((String) objAray[7]);
			retailSalesDetailReport.setQuantity((Double) objAray[8]);
			retailSalesDetailReport.setRate((Double) objAray[9]);
			retailSalesDetailReport.setTaxRate((Double) objAray[10]);
			retailSalesDetailReport.setSaleValue((Double) objAray[11]);
			retailSalesDetailReport.setCost((Double) objAray[12]);
			retailSalesDetailReport.setCostValue((Double) objAray[13]);
			retailSalesDetailReportList.add(retailSalesDetailReport);
			
		 }
		

		return retailSalesDetailReportList;
	
	}

	@Override
	public List<RetailSalesSummaryReport> getRetailSalesSummaryReport(Date fromDate, Date toDate) {
		List<RetailSalesSummaryReport> retailSalesSummaryReportList = new ArrayList<RetailSalesSummaryReport>();
		List<Object> obj = salesDetailsRepository.getRetailSalesSummaryReportNew(fromDate,toDate);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			RetailSalesSummaryReport retailSalesSummaryReport = new RetailSalesSummaryReport();
			
			retailSalesSummaryReport.setInvoiceDate(SystemSetting.UtilDateToString((Date) objAray[0], "yyyy-MM-dd"));
			retailSalesSummaryReport.setVoucherNum((String) objAray[1]);
			retailSalesSummaryReport.setCustomerName((String) objAray[2]);
//			retailSalesSummaryReport.setPatientID((String) objAray[3]);
//			retailSalesSummaryReport.setPatientName((String) objAray[4]);
//			retailSalesSummaryReport.setDoctorName((String) objAray[5]);
			retailSalesSummaryReport.setBeforeGST((Double) objAray[3]);
			retailSalesSummaryReport.setGst((Double) objAray[4]);
			retailSalesSummaryReport.setInvoiceAmount((Double) objAray[5]);
			retailSalesSummaryReport.setTotalPaid((Double) objAray[6]);
			retailSalesSummaryReport.setInsurance((Double) objAray[11]);
//			retailSalesSummaryReport.setInsurance(0.0);
			retailSalesSummaryReport.setCredit((Double) objAray[7]);
			retailSalesSummaryReport.setCardReceipt((Double) objAray[10]);
//			retailSalesSummaryReport.setCardReceipt(0.0);
			retailSalesSummaryReport.setCashReceipt((Double) objAray[9]);
//			retailSalesSummaryReport.setCashReceipt(0.0);
			retailSalesSummaryReport.setUser((String) objAray[8]);
			retailSalesSummaryReportList.add(retailSalesSummaryReport);
			
		 }
		

		return retailSalesSummaryReportList;
	
	}
	
	
	//save salesDtl for online POS====================14-12-2021=============
	@Override
	public SalesDtl saveSalesDtlForOnlinePos(SalesDtl salesDtlRequest,String branch) {
		
		String permormaPrinted = "NO";

		if (null != salesDtlRequest.getSalesTransHdr().getPerformaInvoicePrinted()) {
			permormaPrinted = salesDtlRequest.getSalesTransHdr().getPerformaInvoicePrinted();
		} else {
			permormaPrinted = "NO";
		}

		
		
		if (permormaPrinted.equalsIgnoreCase("NO")) {
			SalesDtl addedItem = salesDetailsRepository.findBySalesTransHdrIdAndItemIdAndBatchAndBarcodeAndUnitIdAndRate(
					salesDtlRequest.getSalesTransHdr().getId(), salesDtlRequest.getItemId(), salesDtlRequest.getBatch(),
					salesDtlRequest.getBarcode(), salesDtlRequest.getUnitId(), salesDtlRequest.getRate());

			if (null != addedItem && null != addedItem.getId()
					&& salesDtlRequest.getUnitId().equalsIgnoreCase(addedItem.getUnitId())
					&& !salesDtlRequest.getSalesTransHdr().getSalesMode().equalsIgnoreCase("KOT")) {

				addedItem.setQty(addedItem.getQty() + salesDtlRequest.getQty());
				addedItem.setCgstAmount(addedItem.getCgstAmount() + salesDtlRequest.getCgstAmount());
				addedItem.setSgstAmount(addedItem.getSgstAmount() + salesDtlRequest.getSgstAmount());
				addedItem.setCessAmount(addedItem.getCessAmount() + salesDtlRequest.getCessAmount());
				addedItem.setAmount(addedItem.getQty() * (salesDtlRequest.getRate()));
				double includingTax = (addedItem.getAmount() * salesDtlRequest.getTaxRate()) / 100;
				BigDecimal BrateAfterDiscount = new BigDecimal(
						includingTax + addedItem.getAmount() + addedItem.getCessAmount());
				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_CEILING);
				addedItem.setAmount(BrateAfterDiscount.doubleValue());
				addedItem.setRate(salesDtlRequest.getRate());
				addedItem.setMrp(salesDtlRequest.getMrp());

				
				//addedItem=saveAndPublishService.saveSalesDtl(addedItem, branch);
				addedItem = salesDetailsRepository.save(addedItem);
				
				
//				offerChecking(addedItem, addedItem.getCompanyMst(), loginDate);
				
				
				return addedItem;

			} 
				else {

			
				//SalesDtl salesDtl = saveAndPublishService.saveSalesDtl(salesDtlRequest, branch);
				SalesDtl salesDtl  = salesDetailsRepository.save(salesDtlRequest);
				
				if(null != salesDtl)
				{
					insuranceService.parmacyItemAddInsuranceCalculate(salesDtl.getSalesTransHdr());
				}
//				offerChecking(salesDtl, salesDtl.getCompanyMst(), loginDate);
				
				
				return salesDtl;
			}
		}
		return null;

	}

		
}
