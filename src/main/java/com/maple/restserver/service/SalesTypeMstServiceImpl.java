package com.maple.restserver.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesTypeMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesTypeMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;

@Service
@Transactional
public class SalesTypeMstServiceImpl implements SalesTypeMstService {
	@Autowired
	private VoucherNumberService voucherService;
	@Autowired
	SalesTypeMstRepository 	salesTypeMstRepository; 
	
	@Autowired
	CompanyMstRepository companyMstRepository;

	@Override
	public String initializeCorporateSale(String companymstid,String branchcode) {
          Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		
		if(!companyOpt.isPresent())
		{
			return "Failed";
		}
		
		SalesTypeMst salesTypeMst = new SalesTypeMst();
		
		List<SalesTypeMst> salesTypeMstList = salesTypeMstRepository.findBySalesTypeAndCompanyMst("CORPORATESALE",companyOpt.get());
		if(salesTypeMstList.size()==0)
		{
			salesTypeMst = new SalesTypeMst();
			 VoucherNumber voucherNo = voucherService.generateInvoice(branchcode,"VTYPE");

			 salesTypeMst.setId(voucherNo.getCode());
			salesTypeMst.setSalesType("CORPORATESALE");
			salesTypeMst.setSalesPrefix("CS");
			
			salesTypeMst.setCompanyMst(companyOpt.get());
			
			salesTypeMst = salesTypeMstRepository.save(salesTypeMst);
		}
		
		List<SalesTypeMst> salesTypeMstListsSOC = salesTypeMstRepository.findBySalesTypeAndCompanyMst("ORDER-CONVERTION",companyOpt.get());
		if(salesTypeMstListsSOC.size()==0)
		{
			salesTypeMst = new SalesTypeMst();
			 VoucherNumber voucherNo = voucherService.generateInvoice(branchcode,"VTYPE");

			 salesTypeMst.setId(voucherNo.getCode());
			salesTypeMst.setSalesType("ORDER-CONVERTION");
			salesTypeMst.setSalesPrefix("OC");
			
			salesTypeMst.setCompanyMst(companyOpt.get());
			
			salesTypeMst = salesTypeMstRepository.save(salesTypeMst);
		}
		
		
		
		
		
		return "Success";
		
	}
	

}
