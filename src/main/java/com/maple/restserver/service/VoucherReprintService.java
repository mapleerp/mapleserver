package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.VoucherReprintDtl;

@Component
public interface VoucherReprintService {
	List<VoucherReprintDtl>	getVoucherReprint(String companymstid,String vouchernumber,String branchcode);
	
	List<VoucherReprintDtl>	getSalesReturnVoucherReprint(String companymstid,String vouchernumber,String branchcode);

	List<VoucherReprintDtl> getVoucherReprint(String companymstid, String vouchernumber, String branchcode, Date date);

}
