package com.maple.restserver.service;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public interface AccountPayableService {

	Double supplierBillAdjustment(Date rdate, String accountid, Double paidamount,String paymentHdrs);

}
