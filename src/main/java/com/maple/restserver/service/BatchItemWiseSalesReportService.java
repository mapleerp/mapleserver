package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.report.entity.BatchItemWiseSalesReport;
import com.maple.restserver.report.entity.BatchWiseCustomerSalesReport;

public interface BatchItemWiseSalesReportService {

	
	public List<BatchItemWiseSalesReport> itemWiseSalesReport(String companymstid,String branchcode,Date fDate,Date TDate,List<String>itemList);

}
