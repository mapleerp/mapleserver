package com.maple.restserver.service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.report.entity.SaleOrderReport;
import com.maple.restserver.report.entity.StockSummaryCategoryWiseReport;
import com.maple.restserver.report.entity.StockSummaryDtlReport;
import com.maple.restserver.report.entity.StockSummaryReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component
public class StockSummaryReportServiceImpl implements StockSummaryReportService {

	@Autowired
	ItemBatchMstRepository itemBatchMstRepository;
	
	@Autowired
	PriceDefinitionRepository priceDefinitionRepository;

	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepository;

	@Autowired
	ItemMstRepository itemMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Override
	public List<StockSummaryCategoryWiseReport> getCategoryWiseStockSummaryReport(String companymstid,
			String categoryid, Date fromdate, Date todate, String branchCode) {
		List<StockSummaryCategoryWiseReport> stockRepoList = new ArrayList();

		List<Object> obj = itemBatchMstRepository.getCategoryWiseStockSummaryReport(companymstid, categoryid, fromdate,
				todate, branchCode);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			StockSummaryCategoryWiseReport sReport = new StockSummaryCategoryWiseReport();
			sReport.setValue((Double) objAray[0]);
			sReport.setItemName((String) objAray[1]);
			sReport.setQty((Double) objAray[2]);
			sReport.setRate((Double) objAray[3]);
			sReport.setTransDate((Date) objAray[4]);
			stockRepoList.add(sReport);

		}

		return stockRepoList;

	}

	@Override
	public List<StockSummaryReport> getAllCategoryStockSummaryReport(String companymstid, Date fromdate, Date todate,
			String branchCode) {
		List<SaleOrderReport> saleOrderHomeList = new ArrayList();

		List<StockSummaryReport> stockRepoList = new ArrayList();

		List<Object> obj = itemBatchMstRepository.getAllCategorySalesSummaryReport(companymstid, fromdate, todate,
				branchCode);

		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			StockSummaryReport sReport = new StockSummaryReport();
			sReport.setParticulars((String) objAray[0]);
			sReport.setValue((Double) objAray[1]);
			sReport.setId((String) objAray[2]);
			stockRepoList.add(sReport);

		}

		return stockRepoList;
	}

	@Override
	public List<StockSummaryDtlReport> getStockSummaryDtls(String companymstid, String itemid, Date fromdate,
			Date todate, String branchCode) {
		double doubleOpeningQty = 0.0;

		ItemMst item = itemMstRepository.findById(itemid).get();

		List<Object> object = itemBatchMstRepository.getClosingQty(companymstid, itemid, fromdate, branchCode);
		for (int k = 0; k < object.size(); k++) {
			Object[] objctArray = (Object[]) object.get(k);
			String openingQty = String.valueOf((Double) objctArray[0] + (String) objctArray[1]);
			doubleOpeningQty = (Double) objctArray[0];

		}

		List<StockSummaryDtlReport> stockRepoList = new ArrayList();

		List<Object> obj = itemBatchMstRepository.getStockSummaryDtls(companymstid, itemid, fromdate, todate,
				branchCode);

		double prevClosing = 0;
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			StockSummaryDtlReport sReport = new StockSummaryDtlReport();
			sReport.setUnitName((String) objAray[0]);
			sReport.setCompanyName((String) objAray[1]);
			sReport.setItemName((String) objAray[2]);
			sReport.setVoucherNo((String) objAray[3]);
			sReport.setInwardQty((Double) objAray[4]);
			sReport.setOutwardQty((Double) objAray[5]);
			;
			sReport.setInwardValue((Double) objAray[6]);
			sReport.setOutwardValue((Double) objAray[7]);

			sReport.setrDate((Date) objAray[8]);
			sReport.setBranchCode((String) objAray[9]);
			sReport.setVoucherNumber((String) objAray[12]);

			sReport.setClosingQty((Double) objAray[10] + doubleOpeningQty + prevClosing);

			sReport.setParticulars((String) objAray[11]);
			sReport.setFromDate(fromdate);
			sReport.setToDate(todate);

			sReport.setOpeningStock(doubleOpeningQty);
			sReport.setClosingValue(sReport.getClosingQty() * item.getStandardPrice());
			
			Timestamp updatedTime = (Timestamp) objAray[13];
			
			LocalDateTime date = updatedTime.toLocalDateTime();
			LocalDate localDate = date.toLocalDate();
			
			Date udate = SystemSetting.localToUtilDate(localDate);
			
			sReport.setUpdatedTime(udate);

			stockRepoList.add(sReport);
			prevClosing = (Double) objAray[10] + prevClosing;
		

		}

		return stockRepoList;
	}

	@Override
	public List<StockSummaryDtlReport> getAllStockSummaryValue(String companymstid, Date todate, String branchCode,
			String categoryid) {
		// TODO Auto-generated method stub

		List<StockSummaryDtlReport> stockRepoList = new ArrayList();

		List<Object> obj = itemBatchMstRepository.getAllStockSummaryValue(companymstid,  todate,   branchCode, categoryid);
		System.out.print(obj.size() + "size of object list isssssssssssssssssssss");
		double prevClosing = 0;
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			StockSummaryDtlReport sReport = new StockSummaryDtlReport();
			sReport.setUnitName((String) objAray[0]);
			sReport.setCompanyName((String) objAray[1]);
			sReport.setItemName((String) objAray[2]);
			sReport.setBranchCode((String) objAray[3]);
			sReport.setClosingQty((Double) objAray[4]);
			sReport.setClosingValue((Double) objAray[5]);

			stockRepoList.add(sReport);

		}

		return stockRepoList;

	}

	@Override
	public List<StockSummaryDtlReport> getAllStockSummaryByCostPrice(String companymstid, Date tDate, String branchCode,
			String categoryid) {

		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		List<StockSummaryDtlReport> stockRepoList = new ArrayList();

		PriceDefenitionMst priceDef = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyOpt.get(),
				"COST PRICE");

		List<Object> obj = itemBatchMstRepository.getAllStockSummaryByCostPrice(companymstid, tDate, branchCode,
				categoryid);
		System.out.print(obj.size() + "size of object list isssssssssssssssssssss");
		double prevClosing = 0;
		for (int i = 0; i < obj.size(); i++) {
			

			Object[] objAray = (Object[]) obj.get(i);
			
			Double value = 0.0;
			
			PriceDefinition price = priceDefinitionRepository.findPriceDefenitionByItemUnitPriceAndDate((String)objAray[5], 
					priceDef.getId(), (String)objAray[6], companymstid);
			 
			
			if(null != price)
			{
				value = (Double) objAray[4] * price.getAmount();
				StockSummaryDtlReport sReport = new StockSummaryDtlReport();
				sReport.setUnitName((String) objAray[0]);
				sReport.setCompanyName((String) objAray[1]);
				sReport.setItemName((String) objAray[2]);
				sReport.setBranchCode((String) objAray[3]);
				sReport.setClosingQty((Double) objAray[4]);
				sReport.setClosingValue(value);

				stockRepoList.add(sReport);
			}
	

		}

		return stockRepoList;
	}

	@Override
	public List<StockSummaryDtlReport> getStockSummaryDtls(String companymstid, Date fDate, Date tDate,
			String branchCode) {
		// TODO Auto-generated method stub
		return null;
	}

}
