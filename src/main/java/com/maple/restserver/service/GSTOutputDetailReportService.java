package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.GSTOutputDetailReport;

@Service
public interface GSTOutputDetailReportService {

	List<GSTOutputDetailReport> findLedgerDetails(Date fdate, Date tdate);

}
