package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.customerLedgerReport;

@Service
@Component
public interface CustomerLedgerReportService {


	List<customerLedgerReport> getCustomerLedgerReport(CompanyMst companymstid, String customerid, String startdate,
			String enddate, String branchcode);

}
