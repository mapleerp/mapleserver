package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ReorderMst;
import com.maple.restserver.entity.StockMovementView;

@Component
public interface StockService {
	
	List<ItemBatchMst> getAllStock();
	List<ReorderMst> getAllStockLessThanMinQty(String companymstid,String branchCode);
	Double  getStockOfItem(String companymstid, String itemId);
	List<StockMovementView> getAllItemMovement(String companymstid, java.sql.Date startDate, java.sql.Date endDate,
			String branchcode);
}
