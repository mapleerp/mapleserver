package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.AMDCDailySalesSummaryReport;
import com.maple.restserver.report.entity.PharmacyDailySalesTransactionReport;


@Service
public interface AMDCDailySalesSummaryReportService {

	List<AMDCDailySalesSummaryReport> findAMDCDailySalesSummaryReport(Date cDate);

	List<PharmacyDailySalesTransactionReport> findPharmacyDailySalesTransactionReport(Date fromDate, Date toDate);
	
	

}
