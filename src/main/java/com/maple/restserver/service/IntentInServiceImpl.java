package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.IntentInReport;
import com.maple.restserver.report.entity.SaleOrderTax;
import com.maple.restserver.repository.IntentInDtlRepository;
import com.maple.restserver.repository.IntentInHdrRepository;

@Service
@Transactional
@Component
public class IntentInServiceImpl implements IntentInReportService {

	@Autowired
	IntentInHdrRepository intentInHdrRepository;
	
	@Autowired
	IntentInDtlRepository intentInDtlRepository;

	@Override
	public List<IntentInReport> getIntentInReport(String voucherNumber, Date date) {

		List<IntentInReport> intentInReportList = new ArrayList();

		List<Object> obj = intentInHdrRepository.getIntentInReport(voucherNumber, date);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			IntentInReport intentInReport = new IntentInReport();

			intentInReport.setBranchCode((String) objAray[0]);
			intentInReport.setInVoucherNumber((String) objAray[1]);
			intentInReport.setFromBranch((String) objAray[2]);
			intentInReport.setIntentStatus((String) objAray[3]);
			intentInReport.setInVoucherDate((Date) objAray[4]);
			intentInReport.setQty((Double) objAray[5]);
			intentInReport.setItemName((String) objAray[6]);
			intentInReport.setUnitName((String) objAray[7]);
			intentInReportList.add(intentInReport);
		}

		return intentInReportList;
	}

	@Override
	public List<Object> getPendingIntent(Date fdate, Date tdate) {

		List<Object> obj = intentInHdrRepository.getPendingIntent(fdate, tdate);

		return obj;
	}

	@Override
	public List<IntentInReport> getItemWiseIntentSummary(String branchcode, String companymstid, Date fdate) {
	
		List<IntentInReport> intentInReportList = new ArrayList();

		List<Object> obj = intentInDtlRepository.getItemWiseIntentSummary(branchcode,companymstid,fdate);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			IntentInReport intentInReport = new IntentInReport();
			intentInReport.setItemName((String) objAray[0]);
			intentInReport.setQty((Double) objAray[1]);
			
			intentInReportList.add(intentInReport);
		}
		
		return intentInReportList;
	}

}
