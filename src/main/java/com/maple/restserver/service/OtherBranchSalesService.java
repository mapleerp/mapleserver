package com.maple.restserver.service;

import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.report.entity.TaxSummaryMst;

@Service
@Transactional
public interface OtherBranchSalesService {

	List<SalesInvoiceReport> getSalesInvoice(String companymstid, String voucherNumber, Date date);

	List<TaxSummaryMst> getSalesInvoiceTax(String companymstid, String vouchernumber, Date date);

	List<TaxSummaryMst> getSumOfTaxAmounts(String companymstid, String vouchernumber, Date date);

	List<SalesInvoiceReport> getTaxinvoiceCustomer(String companymstid, String vouchernumber, Date date);

	Double getCessAmount(String vouchernumber, java.util.Date date);

	Double getNonTaxableAmount(String vouchernumber, java.util.Date date);

	List<StockTransferOutReport> getOtherBranchStockTransfer(String vouchernumber, java.util.Date date);

	void resendOtherBranchSales(String hdrid);

}
