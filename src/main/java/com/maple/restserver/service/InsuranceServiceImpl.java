package com.maple.restserver.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CurrencyConversionMst;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.entity.InsuranceCompanyMst;
import com.maple.restserver.entity.PatientMst;
import com.maple.restserver.entity.PurchaseAdditionalExpenseDtl;
import com.maple.restserver.entity.PurchaseAdditionalExpenseHdr;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.report.entity.IntentInReport;
import com.maple.restserver.report.entity.SaleOrderTax;
import com.maple.restserver.repository.CurrencyConversionMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;
import com.maple.restserver.repository.InsuranceCompanyRepository;
import com.maple.restserver.repository.IntentInDtlRepository;
import com.maple.restserver.repository.IntentInHdrRepository;
import com.maple.restserver.repository.PurchaseAdditionalExpenseDtlRepository;
import com.maple.restserver.repository.PurchaseAdditionalExpenseHdrRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;


@Service
@Transactional
@Component
public class InsuranceServiceImpl implements InsuranceService {

	@Autowired
	PurchaseDtlRepository purchaseDtlRepository;
	
	@Autowired
	PurchaseAdditionalExpenseHdrRepository purchaseAdditionalExpenseHdrRepository;
	
	@Autowired
	PurchaseAdditionalExpenseDtlRepository purchaseAdditionalExpenseDtlRepository;
	
 
	@Autowired
	SalesDetailsRepository salesDetailsRepository;
	
	
	@Autowired
	SalesReceiptsRepository salesReceiptsRepository;

	
	@Autowired
	InsuranceCompanyRepository insuranceCompanyRepository;

	
	@Autowired
	CurrencyConversionMstRepository currencyConversionMstRepo;


	@Override
	public SalesReceipts parmacyItemAddInsuranceCalculate(SalesTransHdr salesTransHdr) {
		 // Get Patient from Sales Trans Hdr
		PatientMst patientMst = salesTransHdr.getPatientMst();
		SalesReceipts salesReceipts = null;
		//Instead of String, Patient Master should contain InsuranceMst
		
		//insuranceCompanyId will be account ID
		if(null==patientMst) {
			return null;
		}
		
		if(null == patientMst.getInsuranceCompanyId()) {
			return null;

		}
		
		if(null == patientMst.getInsuranceCompanyId())
		{
			return null;
		}
		String insuranceCompanyId = patientMst.getInsuranceCompanyId();
	
		Optional<InsuranceCompanyMst> insuranceCompanyOpt = insuranceCompanyRepository.findById(insuranceCompanyId);
 
		if (!insuranceCompanyOpt.isPresent()) {
			
			return null;
		}
		
	
		
		InsuranceCompanyMst insuranceCompanyMst = insuranceCompanyOpt.get();
		
		if(insuranceCompanyMst.getInsuranceCompanyName().equalsIgnoreCase("NO INSURANCE"))
		{
			return null;
		}
	 
		
		Double insuranceCoverage = patientMst.getPercentageCoverage();
		
		if(null==insuranceCoverage) {
			return null;
		}
		if(insuranceCoverage.doubleValue()==0) {
			return null;
		}
		// Get Total Amount from Sales Dtl.
		Double totalItemAmount  = salesDetailsRepository.findSumSalesDtlByHdrId(salesTransHdr.getId());
		// Find Insurance Amount 
		Double insuranceAmount = totalItemAmount * insuranceCoverage/ 100;
		
		List<SalesReceipts> salesReceiptsList = salesReceiptsRepository.findBySalesTransHdrIdAndReceiptMode(salesTransHdr.getId(),"INSURANCE");
		
		if(salesReceiptsList.size()>0) {
			salesReceipts = salesReceiptsList.get(0);
			salesReceipts.setReceiptAmount(insuranceAmount);
			salesReceiptsRepository.save(salesReceipts);
		}else {
			
			salesReceipts = new SalesReceipts();
			salesReceipts.setSalesTransHdr(salesTransHdr);
			salesReceipts.setAccountId(insuranceCompanyId);
			salesReceipts.setBranchCode(salesTransHdr.getBranchCode());
			salesReceipts.setCompanyMst(salesTransHdr.getCompanyMst());
			salesReceipts.setReceiptAmount(insuranceAmount);
			salesReceipts.setReceiptMode("INSURANCE");
			salesReceiptsRepository.save(salesReceipts);
		}
		
		return salesReceipts;
	}
	
	 
}
