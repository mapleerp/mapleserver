package com.maple.restserver.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.repository.BranchMstRepository;

@Service
@Transactional
@Component
public class BrachMstServiceImpl implements BranchMstService{

	@Autowired
	private BranchMstRepository brnchMstRepo;

	@Override
	public String findByBranchName(String branchName) {
		BranchMst branchMst =brnchMstRepo.findByBranchName(branchName);
		String branch=branchMst.getBranchCode();
		return branch;
	}
	
	
	
}
