package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ItemDetailPopUp;

@Service
@Transactional
public interface ItemDetailPopUpService {

	List<ItemDetailPopUp> getItemDetailPopUp(String searchstring,Pageable topFifty,CompanyMst companyMst,Date sdate);
}
