package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.SaleOrderDetailsReport;
import com.maple.restserver.report.entity.SaleOrderDueReport;
import com.maple.restserver.report.entity.SaleOrderReceiptReport;
import com.maple.restserver.report.entity.SaleOrderReport;
import com.maple.restserver.report.entity.SaleOrderTax;
import com.maple.restserver.report.entity.SaleOrderinoiceReport;
import com.maple.restserver.report.entity.SalesOrderAdvanceBalanceReport;

@Component
public interface SaleOrderReportService {

	
	public List<SaleOrderDetailsReport>   getSaleOrderPendingReport(Date  date,String branchCode);
	
	
	
	public List<SaleOrderDetailsReport>   getSaleOrderRealizedrReport(Date  date,String branchCode);
	
	public List<SaleOrderDueReport>  getSaleOrderDueReport(Date date,String vouchernumber,String branchcode);
	
	public List<SaleOrderinoiceReport>  getSaleOrderInvoiceReport(Date date,String vouchernumber,String branchCode);


	public  List<SaleOrderTax> getSaleOrderTax(String vouchernumber, Date date,String branchCode);


	public List<SalesOrderAdvanceBalanceReport> getSaleOrderBalanceAdvanceReportByDate(Date date,String branchCode);

	
	
	public List<SaleOrderReport>getHomeDelivaryReportByDate(Date date,String branchCode);
	
	List<SaleOrderReport> getSaleOrderReportByDeliverBoy(Date date,String deliveryboyid,String
			  branchCode);
	
	
	List<SaleOrderReport>	getSaleOrderReportByBranchCodeAndDate(Date date,String branchCode);



	public List<SaleOrderDetailsReport> getPendinSaleOrderReport(Date date,Date tudate, String branchcode);



	public List<SaleOrderReport> findBySalesOrderTransHdrVoucherDate(Date date,String companymstid);



	List<SaleOrderReceiptReport> getSaleOrderReceiptByDateAndMode(Date date, String mode);
	public List<SaleOrderReport> getDeliveryBoyPendingReport(Date date, String companymstid, String deliveryboyid,
			String branchcode);



	public List<SaleOrderReceiptReport> getSaleOrderReceiptByDateAndCard(Date vdate);



	public List<SaleOrderReceiptReport> getSaleOrderReceiptPreviousCard(Date vdate);


	//------------------version 4.9
	public List<SaleOrderReport> getSaleOrderHomeDeliveryReport(Date date, String branchCode, String companymstid);
	//------------------version 4.9 end

	//---------------version 4.10

		public List<SaleOrderReport> getDailyOrderSummuryReport(Date date, String branchCode, String companymstid);
		//---------------version 4.10 end
	//---------------version 4.8

		public List<SaleOrderReport> getDailyCakeSettingReport(Date date, String categoryid, String branchCode, String companymstid);



		public List<SaleOrderReport> getSaleOrderConvertedReport(Date date, String branchCode, String companymstid);


	//------------------version 4.8 end
}
