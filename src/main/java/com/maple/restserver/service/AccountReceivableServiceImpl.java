package com.maple.restserver.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.OwnAccount;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.ReceiptInvoiceDtl;
import com.maple.restserver.report.entity.ActualProductionReport;
import com.maple.restserver.report.entity.CustomerBalanceReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.AccountReceivableRepository;
import com.maple.restserver.repository.OwnAccountRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.repository.ReceiptInvoiceDtlRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
public class AccountReceivableServiceImpl implements AccountReceivableService {

	@Autowired
	VoucherNumberService voucherNumberService;
	@Autowired
	ReceiptHdrRepository receiptHdrRepo;
	/*
	 * @Autowired CustomerMstRepository customerMstRepo;
	 */
	
	@Autowired
	ReceiptInvoiceDtlRepository receiptInvoiceDtlRepo;
	
	@Autowired
	OwnAccountRepository ownAccountRepo;
	
	@Autowired
	AccountReceivableRepository accountReceivableRepository;
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	@Override
	public List<CustomerBalanceReport> findByCustomerBalanceById(CompanyMst companyMst, String id, Date fDate,
			Date tDate) {
		List<CustomerBalanceReport> CustomerBalanceReportList= new ArrayList();
		
		 List<Object> obj =   accountReceivableRepository.findByCustomerBalanceById(companyMst,id,fDate,tDate);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 CustomerBalanceReport customerBalanceReport =new CustomerBalanceReport();
			 customerBalanceReport.setCustomerName((String) objAray[0]);
			 customerBalanceReport.setVoucherNumber((String) objAray[1]);
			 customerBalanceReport.setBalanceAmount((Double) objAray[2]);
			 customerBalanceReport.setRemark((String) objAray[3]);
			 customerBalanceReport.setVoucherDate((Date) objAray[4]);
			 CustomerBalanceReportList.add(customerBalanceReport);
		
	}
		 return CustomerBalanceReportList;
}
	
	@Override
	public List<AccountReceivable> findByCompanyMstAndAccountId(CompanyMst companyMst, String id) {
		List<AccountReceivable> accountLists= new ArrayList();
		
		List<AccountReceivable> accountList=accountReceivableRepository.
				findByCompanyMstAndAccountId(companyMst,id);
	
	
		return accountList;
	}

	@Override
	public List<CustomerBalanceReport> findByAllCustomerBalance(CompanyMst companyMst, Date dateFrom, Date dateTo) {
		List<CustomerBalanceReport> CustomerBalanceReportList= new ArrayList();
		
		 List<Object> obj =   accountReceivableRepository.findByAllCustomerBalance(companyMst,dateFrom,dateTo);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 CustomerBalanceReport customerBalanceReport =new CustomerBalanceReport();
			 customerBalanceReport.setCustomerName((String) objAray[0]);
			 customerBalanceReport.setVoucherNumber((String) objAray[1]);
			 customerBalanceReport.setBalanceAmount((Double) objAray[2]);
			 customerBalanceReport.setRemark((String) objAray[3]);
			 customerBalanceReport.setVoucherDate((Date) objAray[4]);
			 CustomerBalanceReportList.add(customerBalanceReport);
		
	}
		 return CustomerBalanceReportList;
	}

	@Override
	public Double billwiseAdjustment(Date udate, CompanyMst companyMst, String receipthdr, Double receivedamount,
			String accountid) {
		
		List<AccountReceivable> accountReceivableList = accountReceivableRepository.findAccountReceivablewithBalancegreaterThanzero(udate,companyMst,accountid);
		for(int i=0;i<accountReceivableList.size();i++)
		{
			if(receivedamount>0)
			{
				AccountReceivable accountReceivable = accountReceivableList.get(i);
				Double balanceAmount = accountReceivable.getDueAmount()-accountReceivable.getPaidAmount();
				BigDecimal bdBalanceAmount = new BigDecimal(balanceAmount);
				bdBalanceAmount = bdBalanceAmount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
				balanceAmount= bdBalanceAmount.doubleValue();
				if(balanceAmount>=receivedamount)
				{
					saveToReceiptInvoiceDtl(receivedamount,receipthdr,companyMst,accountid,udate,accountReceivable);
					accountReceivable.setPaidAmount(accountReceivable.getPaidAmount()+receivedamount);
					receivedamount = 0.0;
					
				}
				else
				{
					receivedamount = receivedamount-balanceAmount;
					BigDecimal bdreceivedamount = new BigDecimal(receivedamount);
					bdreceivedamount = bdreceivedamount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
					receivedamount=bdreceivedamount.doubleValue();
					accountReceivable.setPaidAmount(accountReceivable.getDueAmount());
					saveToReceiptInvoiceDtl(balanceAmount,receipthdr,companyMst,accountid,udate,accountReceivable);
				}
				accountReceivable=accountReceivableRepository.save(accountReceivable);
				
			}
		}
		if(receivedamount>0)
		{
			saveToOwnAccount(udate,accountid,companyMst,receivedamount,receipthdr);
		}
		return receivedamount;
	}
	private void saveToReceiptInvoiceDtl(Double receivedAmount,String receipthdr,CompanyMst companyMst,String accountid,Date udate,AccountReceivable accountReceivable)
	{
		ReceiptHdr receiptHdr = receiptHdrRepo.findById(receipthdr).get();
		AccountHeads customerMst = accountHeadsRepository.findById(accountid).get();
		ReceiptInvoiceDtl receiptInvoiceDtl = new ReceiptInvoiceDtl();
		receiptInvoiceDtl.setCompanyMst(companyMst);
		receiptInvoiceDtl.setAccountHeads(customerMst);
		receiptInvoiceDtl.setInvoiceDate(accountReceivable.getVoucherDate());
		receiptInvoiceDtl.setInvoiceNumber(accountReceivable.getSalesTransHdr().getVoucherNumber());
		receiptInvoiceDtl.setReceiptDate(udate);
		
		receiptInvoiceDtl.setReceiptHdr(receiptHdr);
		receiptInvoiceDtl.setReceiptNumber(receiptHdr.getVoucherNumber());
		receiptInvoiceDtl.setRecievedAmount(receivedAmount);
		receiptInvoiceDtl.setSalesTransHdr(accountReceivable.getSalesTransHdr());
		receiptInvoiceDtlRepo.save(receiptInvoiceDtl);
	}
	private void saveToOwnAccount(Date udate,String accountid,CompanyMst companyMst,Double amount,String receipthdrid)
	{
		ReceiptHdr receiptHdr = receiptHdrRepo.findById(receipthdrid).get();
		OwnAccount ownAccount = new OwnAccount();
		ownAccount.setAccountId(accountid);
		ownAccount.setAccountType("CUSTOMER");
		ownAccount.setCompanyMst(companyMst);
		ownAccount.setCreditAmount(amount);
		ownAccount.setDebitAmount(0.0);
		ownAccount.setOwnAccountStatus("PENDING");
		ownAccount.setRealizedAmount(0.0);
		ownAccount.setReceiptHdr(receiptHdr);
		ownAccount.setVoucherDate(SystemSetting.UtilDateToSQLDate(udate));
		VoucherNumber vno = voucherNumberService.generateInvoice("OWNACC",companyMst.getId());
		ownAccount.setVoucherNo(vno.getCode());
		ownAccountRepo.save(ownAccount);
	}
}
