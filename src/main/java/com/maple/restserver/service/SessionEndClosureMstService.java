package com.maple.restserver.service;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.SessionEndClosureMst;

@Component
public interface SessionEndClosureMstService {

	
	  SessionEndClosureMst retrieveSessionEndClosure(Date date,String userId); 
}
