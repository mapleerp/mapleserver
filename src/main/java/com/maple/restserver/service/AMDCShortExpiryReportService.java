package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.AMDCShortExpiryReport;

@Service
public interface AMDCShortExpiryReportService {

	List<AMDCShortExpiryReport> findAMDCShortExpiryReport(Date date);

}
