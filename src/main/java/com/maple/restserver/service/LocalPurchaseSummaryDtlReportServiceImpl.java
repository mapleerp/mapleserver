package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.maple.restserver.report.entity.LocalPurchaseSummaryReport;
import com.maple.restserver.repository.PurchaseHdrRepository;

@Service
@Transactional
public class LocalPurchaseSummaryDtlReportServiceImpl implements LocalPurchaseSummaryDtlReportService{
	
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;
	
	
	@Override	
	public List<LocalPurchaseSummaryReport> getLocalPurchaseSummaryDtlReport(Date fdate, Date tdate) {
			
			List<LocalPurchaseSummaryReport> localPurchaseSummaryReportList = new ArrayList<LocalPurchaseSummaryReport>();
			List<Object> obj = purchaseHdrRepository.getLocalPurchaseSummaryReport(fdate,  tdate);
			 for(int i=0;i<obj.size();i++)
			 {
				Object[] objAray = (Object[]) obj.get(i);
				LocalPurchaseSummaryReport localPurchaseSummaryReport = new LocalPurchaseSummaryReport();
				
				localPurchaseSummaryReport.setInvoiceDate((Date) objAray[5]);
				localPurchaseSummaryReport.setInvoiceNumber((String) objAray[1]);
				localPurchaseSummaryReport.setSupplier((String) objAray[2]);
			
				localPurchaseSummaryReport.setGst((Double)objAray[9]);
				localPurchaseSummaryReport.setSupplierInvoiceNumber((String) objAray[4]);
			
				
			     localPurchaseSummaryReport.setUser((String) objAray[10]);
				//localPurchaseSummaryReport.setTinNo((String) objAray[]);
				localPurchaseSummaryReport.setBeforeGst((Double) objAray[8]);
				localPurchaseSummaryReport.setpONumber((String) objAray[6]);
				localPurchaseSummaryReport.setNetInvoice((Double) objAray[7]);
				localPurchaseSummaryReport.setDate((Date) objAray[0]);
				
				
				
				
			
				localPurchaseSummaryReportList.add(localPurchaseSummaryReport);
			 }

			return localPurchaseSummaryReportList;

	}
	
	
}
