package com.maple.restserver.service;


import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.BatchWiseStockEvaluationReport;

@Service
public interface BatchWiseStockEvaluationReportService {


	List<BatchWiseStockEvaluationReport> getStockByCompanyMstIdAndDate(String itemid, String batch,
			Date startdate, Date enddate);

	
	

}
