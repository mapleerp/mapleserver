package com.maple.restserver.service;

import java.util.List;

import com.maple.restserver.entity.GoodReceiveNoteDtl;

public interface GoodReceiveNoteDtlService {

	public    List<GoodReceiveNoteDtl> updateGoodReceiveNoteDtlDisplaySerial(String goodReceiveNoteHdr);

}
