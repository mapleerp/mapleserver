package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.SalesPropertiesConfigMst;
import com.maple.restserver.entity.SalesPropertyTransHdr;
import com.maple.restserver.repository.SalesPropertiesConfigMstRepository;
import com.maple.restserver.repository.SalesPropertyTransHdrRepository;

 
@Service
@Transactional
public class SalesPropertiesServiceImpl implements SalesPropertiesService{
	

	@Autowired
	SalesPropertyTransHdrRepository salesPropertyTransHdrRepo;
	
	@Autowired
	SalesPropertiesConfigMstRepository salesPropertiesConfigMstRepository;

	@Override
	public List<SalesPropertyTransHdr> findBySalesTransHdrId(String hdrid) {
		
//		List<SalesPropertyTransHdr> salesPropertiesList = new ArrayList<SalesPropertyTransHdr>();
		List<SalesPropertyTransHdr> salesPropertiesList =  salesPropertyTransHdrRepo.findBySalesTransHdrId(hdrid);
		
		for(SalesPropertyTransHdr salesPropertyTransHdr : salesPropertiesList)
		{
			Optional<SalesPropertiesConfigMst> salesPropertiesConfigMst = salesPropertiesConfigMstRepository.
					findById(salesPropertyTransHdr.getPropertyId());
			
			if(salesPropertiesConfigMst.isPresent())
			{
				salesPropertyTransHdr.setPropertyName(salesPropertiesConfigMst.get().getPropertyName());
			}
		}
		return salesPropertiesList;
	}

}
