package com.maple.restserver.service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.InsuranceCompanyDtl;
import com.maple.restserver.entity.InsuranceCompanyMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.InsuranceCompanyDtlRepository;
import com.maple.restserver.repository.InsuranceCompanyRepository;
import com.maple.restserver.repository.InsuranceDtlRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component


public class InsuranceTypeinitializeServiceimpl implements InsuranceTypeinitializeService{
	
	@Autowired
	InsuranceCompanyRepository insuranceCompanyRepository;
	
	
	@Autowired
	InsuranceCompanyDtlRepository insuranceCompanyDtlRepository;
	
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	private VoucherNumberService voucherService;
	
	
	public String initializeInsuranceType(String companymstid , String branchcode)
	  {
	
		    InsuranceCompanyMst insuranceCompanyMst = new InsuranceCompanyMst();
			insuranceCompanyMst = insuranceCompanyRepository.findByInsuranceCompanyNameAndCompanyMstId("NO INSURANCE", companymstid);
			Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
			CompanyMst companyMst = CompanyMstOpt.get();
			if (null == insuranceCompanyMst) {
				
				VoucherNumber voucherNo = voucherService.generateInvoice("INS",branchcode);
				String vcNo = voucherNo.getCode();
			
				insuranceCompanyMst = new InsuranceCompanyMst();
				insuranceCompanyMst.setInsuranceCompanyName("NO INSURANCE");
				
				insuranceCompanyMst.setCompanyMst(companyMst);
				insuranceCompanyMst.setId(vcNo);
				
				
				insuranceCompanyMst = insuranceCompanyRepository.save(insuranceCompanyMst);


				
				//............Sibi.. surya will completed today...//
				
				InsuranceCompanyDtl insuranceCompanyDtl = new InsuranceCompanyDtl();
				
				VoucherNumber voucherNoDtl = voucherService.generateInvoice("INSD",branchcode);
				String vcNoDtl = voucherNo.getCode();
				insuranceCompanyDtl.setId(vcNoDtl);
				insuranceCompanyDtl.setPolicyType("NO INSURANCE");
				insuranceCompanyDtl.setPercentageCoverage("0");
				insuranceCompanyDtl.setCompanyMst(companyMst);
				insuranceCompanyDtl.setInsuranceCompanyMst(insuranceCompanyMst);
				
				insuranceCompanyDtl = insuranceCompanyDtlRepository.save(insuranceCompanyDtl);
				
				return "saved";
			} else {
				return "already saved";
			}
		}
}
	



