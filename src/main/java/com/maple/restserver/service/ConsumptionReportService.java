package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ConsumptionHdrReport;
import com.maple.restserver.report.entity.ConsumptionReport;
import com.maple.restserver.report.entity.ConsumptionReportDtl;

@Service
@Component
public interface ConsumptionReportService {

	
	List<ConsumptionReport>getConsumptionReport(String branchcode,Date  fromdate,Date todate,String companymstid );

public List<ConsumptionHdrReport> getConsumptionHdrReport(String branchcode,Date fromdate,Date todate,String companymstid,String reasonid );

List< ConsumptionReportDtl> getConsumptionDtlReport(String reasonid,Date fromdate,Date todate );
Double getConsumptionAmount(String branchcode,Date fromdate,Date todate,String companymstid);

 List<ConsumptionHdrReport> getConsumptionReason(String branchcode, Date fromdate,Date todate,String companymstid );

List<ConsumptionReport> getdepartmentWiseConsumption(String department, String[] array, String branchcode,
		Date strfromdate, Date strtodate,CompanyMst companyMst);

List<ConsumptionReport> getItemWiseConsumption(String department, String[] array, String branchcode, Date fromdate,
		Date todate, CompanyMst companyMst);

List<ConsumptionReport> getConsumptionreportsummary(String branchcode, Date fromdate, Date todate);

List<ConsumptionReport> getConsumptionDetail(CompanyMst companyMst,Date fromdate, Date todate, String[] array, String username);

List<ConsumptionReport> getConsumptionSummary(CompanyMst companyMst, Date fromdate, Date todate, String[] array, String username);

List<ConsumptionReport> getConsumptionDetailWithoutCategory(CompanyMst companyMst, Date fromdate, Date todate, String username);

List<ConsumptionReport> getConsumptionSummaryWithoutCategory(CompanyMst companyMst, Date fromdate, Date todate, String username);


}
