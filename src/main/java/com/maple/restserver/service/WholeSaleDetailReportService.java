package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.WholeSaleDetailReport;

@Service
public interface WholeSaleDetailReportService {

	List<WholeSaleDetailReport> findWholeSaleDetailReport(Date fromDate, Date toDate);

	

}
