package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.PurchaseReport;
import com.maple.restserver.repository.PurchaseOrderHdrRepository;

@Service
@Transactional
@Component
public class PurchaseOrderReportServiceImpl implements  PurchaseOrderReportService {

	@Autowired
	PurchaseOrderHdrRepository purchaseOrderHdrRepository;

	@Override
	public List<PurchaseReport> retrieveReportPurchaseOrder(String vouchernumber, String branchcode, Date date) {
		List< PurchaseReport> purchaseReportList =   new ArrayList();
		 
		 List<Object> obj = purchaseOrderHdrRepository.retrieveReportPurchaseOrder( vouchernumber, branchcode,  date);  
			 for(int i=0;i<obj.size();i++)
			 {
				 Object[] objAray = (Object[]) obj.get(i);
				 PurchaseReport purchaseReport = new PurchaseReport();
		         purchaseReport.setSupplierName((String) objAray[0]);
				 purchaseReport.setVoucherNumber(( String) objAray[1]);
				 purchaseReport.setItemName((String) objAray[2]);
				 purchaseReport.setQty(( Double) objAray[3]);
				 purchaseReport.setPurchseRate(( Double) objAray[4]);
				 purchaseReport.setTaxRate(( Double) objAray[5]);
				 purchaseReport.setVoucherDate(( Date) objAray[6]);
				 purchaseReport.setSupplierInvNo((String) objAray[7]);
				 purchaseReport.setUnit((String) objAray[8]);
				 purchaseReport.setAmount((Double) objAray[9]);
				 purchaseReport.setSupplierGst((String) objAray[10]);
				 Double totalAmount= purchaseOrderHdrRepository.retrieveTotalAmount( vouchernumber, branchcode, date);
				 purchaseReport.setAmountTotal(totalAmount);
				 purchaseReportList.add(purchaseReport);
				 
			 }
			 return purchaseReportList;
	}
	
	
}
