package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseReturnDtl;
import com.maple.restserver.report.entity.PurchaseHdrReport;
import com.maple.restserver.report.entity.PurchaseReturnDetailAndSummaryReport;
import com.maple.restserver.report.entity.PurchaseReturnInvoice;


@Service
public interface PurchaseReturnService {

	
	public List<PurchaseHdrReport>getPuchaseHdrByDate(CompanyMst companyMst,Date voucherdate);

	
	
	public List<PurchaseReturnDetailAndSummaryReport> getPurchaseReturnDetailAndSummary(Date fdate, Date tdate);



	public List<PurchaseReturnInvoice> getpurchaseReturnInvoiceById(String purchasehdrid, CompanyMst companyMst);



	public Double getpurchaseReturnDtlQty(String purchasedtlId);
}
