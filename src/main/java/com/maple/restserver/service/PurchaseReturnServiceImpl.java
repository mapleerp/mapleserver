package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseReturnDtl;
import com.maple.restserver.report.entity.PurchaseHdrReport;
import com.maple.restserver.report.entity.PurchaseReturnDetailAndSummaryReport;
import com.maple.restserver.report.entity.PurchaseReturnInvoice;
import com.maple.restserver.repository.PurchaseHdrRepository;

@Service

public class PurchaseReturnServiceImpl implements PurchaseReturnService {
	
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;

	@Override
	public List<PurchaseHdrReport> getPuchaseHdrByDate(CompanyMst companyMst,Date voucherdate) {
		
		List<PurchaseHdrReport> purchaseReportList =   new ArrayList();
		 
		 List<Object> obj = purchaseHdrRepository.getPurchaseHdrReport(companyMst,voucherdate);  
			 for(int i=0;i<obj.size();i++)
			 {
				 Object[] objAray = (Object[]) obj.get(i);
				 PurchaseHdrReport purchaseReport = new PurchaseHdrReport();
				 purchaseReport.setVoucherNumber(( String) objAray[0]);
		         purchaseReport.setSupplierName((String) objAray[1]);
			
				
				 purchaseReportList.add(purchaseReport);
				 
			 }
			 return purchaseReportList;
		
		
	
	}
	
	


	@Override
	public List<PurchaseReturnDetailAndSummaryReport> getPurchaseReturnDetailAndSummary(Date fdate, Date tdate) {
		List<PurchaseReturnDetailAndSummaryReport> purchaseReturnDetailAndSummaryReportList =   new ArrayList();
		 
		 List<Object> obj = purchaseHdrRepository.getPurchaseReturnDetailAndSummaryData(fdate,tdate);  
			 for(int i=0;i<obj.size();i++)
			 {
				 Object[] objAray = (Object[]) obj.get(i);
				 PurchaseReturnDetailAndSummaryReport purchaseReturnDetailAndSummaryReport = new PurchaseReturnDetailAndSummaryReport();
				 purchaseReturnDetailAndSummaryReport.setReturnVoucherDate(( String) objAray[0].toString());
				 purchaseReturnDetailAndSummaryReport.setPurchaseReturnVoucher((String) objAray[3]);
				 purchaseReturnDetailAndSummaryReport.setSupplierName((String) objAray[2]);
				 purchaseReturnDetailAndSummaryReport.setReturnVoucherNum((String) objAray[1]);
				 purchaseReturnDetailAndSummaryReport.setItemName((String) objAray[4]);
				 purchaseReturnDetailAndSummaryReport.setGroupName((String) objAray[5]);
				 purchaseReturnDetailAndSummaryReport.setItemCode((String) objAray[6]);
				 purchaseReturnDetailAndSummaryReport.setBatch((String) objAray[7]);
				 purchaseReturnDetailAndSummaryReport.setExpiryDate((String) objAray[10].toString());
				 purchaseReturnDetailAndSummaryReport.setQty((Double) objAray[11]);
				 purchaseReturnDetailAndSummaryReport.setPurchaseRate((Double) objAray[8]);
				 purchaseReturnDetailAndSummaryReport.setAmount((Double) objAray[9]);
				 purchaseReturnDetailAndSummaryReport.setBeforeGST((Double) objAray[12]);
				 purchaseReturnDetailAndSummaryReport.setGst((Double) objAray[13]);
				 purchaseReturnDetailAndSummaryReport.setTotal((Double) objAray[14]);
				 purchaseReturnDetailAndSummaryReport.setUserName((String) objAray[15]);
			
				 purchaseReturnDetailAndSummaryReportList.add(purchaseReturnDetailAndSummaryReport);
				 
			 }
			 return purchaseReturnDetailAndSummaryReportList;
	}




	@Override
	public List<PurchaseReturnInvoice> getpurchaseReturnInvoiceById(String purchasehdrid, CompanyMst companyMst) {
		
		List<PurchaseReturnInvoice> purchaseReturnInvoiceList =   new ArrayList();
		 
		 List<Object> obj = purchaseHdrRepository.getPurchaseReturnInvoiceReport(companyMst,purchasehdrid);  
			 for(int i=0;i<obj.size();i++)
			 {
				 Object[] objAray = (Object[]) obj.get(i);
				 PurchaseReturnInvoice purchaseReturnInvoiceReport = new PurchaseReturnInvoice();
				 //purchaseReturnInvoiceReport.setVoucherNumber(( String) objAray[1]);
				 purchaseReturnInvoiceReport.setSupplierName((String) objAray[2]);
				 purchaseReturnInvoiceReport.setBatch((String) objAray[7]);
				 purchaseReturnInvoiceReport.setDescription((String) objAray[4]);
				 purchaseReturnInvoiceReport.setExpDate((String) objAray[10].toString());
				 purchaseReturnInvoiceReport.setGst((Double) objAray[13]);
				 purchaseReturnInvoiceReport.setInvoiceDate((String) objAray[16]);
				 purchaseReturnInvoiceReport.setQty((Double) objAray[11]);
				 purchaseReturnInvoiceReport.setReturnVoucherNo((String) objAray[1]);
				 purchaseReturnInvoiceReport.setSupplierAdd((String) objAray[18]);
				 purchaseReturnInvoiceReport.setSupplierInv((String) objAray[17]);
				 purchaseReturnInvoiceReport.setSupplierEmail((String) objAray[19]);
				 purchaseReturnInvoiceReport.setSupplierState((String) objAray[20]);
				 //purchaseReturnInvoiceReport.setSupplierTin((String) objAray[]);
				purchaseReturnInvoiceReport.setUnit((String) objAray[22]);
				 purchaseReturnInvoiceReport.setVoucherDate((String) objAray[0]);
				 purchaseReturnInvoiceReport.setUnitPrice((Double) objAray[8]);
				 purchaseReturnInvoiceReport.setItemcode((String) objAray[6]);
				 
			
				
				 purchaseReturnInvoiceList.add(purchaseReturnInvoiceReport);
				 
			 }
			 return purchaseReturnInvoiceList;
		
	}




	@Override
	public Double getpurchaseReturnDtlQty(String purchasedtlId) {
		
		
		Double qty = purchaseHdrRepository.getpurchaseReturnDtlQty(purchasedtlId);
		 
	
		return qty;
	}
	

}
