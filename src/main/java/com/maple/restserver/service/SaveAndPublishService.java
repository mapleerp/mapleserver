package com.maple.restserver.service;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.AccountClassfc;
import com.maple.restserver.accounting.entity.AccountMergeMst;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.CreditClassfc;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.DebitClassfc;
import com.maple.restserver.accounting.entity.InventoryLedgerMst;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.entity.LedgerClassfc;
import com.maple.restserver.accounting.entity.LinkedAccounts;
import com.maple.restserver.accounting.entity.OpeningBalanceConfigMst;
import com.maple.restserver.accounting.entity.OpeningBalanceMst;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.entity.*;
import com.maple.restserver.message.entity.AcceptStockMessage;

 
@Service
@Transactional
public interface SaveAndPublishService {

	public SalesTransHdr saveSalesTransHdr(SalesTransHdr salesTransHdr, String branchCode);
	public ItemMst saveItemMst(ItemMst itemMst, String branchCode);
	public CompanyMst saveCompanyMst(CompanyMst companyMst,String branchCode);
	public BranchMst saveBranchMst(BranchMst branchMst, String branchCode);
	public UserMst saveUserMst(UserMst UserMst, String branchCode );
	public PurchaseHdr savePurchaseHdr(PurchaseHdr purchaseHdr, String branchCode);

//	public Supplier saveSupplier(Supplier supplier,String branchCode );
	public PurchaseDtl savePurchaseDtl(PurchaseDtl purchaseDtl, String branchCode);
	public ItemBatchDtl saveItemBatchDtl(ItemBatchDtl itemBatchDtl, String branchCode);
	public ItemMergeMst saveItemMergeMst(ItemMergeMst itemMergeMst, String branchCode);
	public CategoryMst saveCategoryMst(CategoryMst categoryMst, String branchCode);
	public PaymentHdr savePaymentHdr(PaymentHdr paymentHdr, String branchCode);
	public PaymentDtl savePaymentDtl(PaymentDtl paymentDtl, String branchCode);
	public ActualProductionHdr saveActualProductionHdr(ActualProductionHdr actualProductionHdr, String branchCode);
	public ActualProductionDtl saveActualProductionDtl(ActualProductionDtl actualProductionDtl, String branchCode);
	public PurchaseSchemeMst savePurchaseSchemeMst(PurchaseSchemeMst purchaseSchemeMst, String branchCode);
	public ReceiptHdr saveReceiptHdr(ReceiptHdr receiptHdr, String branchCode);
	public ReceiptDtl saveReceiptDtl(ReceiptDtl receiptDtl, String branchCode);
	public ReceiptModeMst saveReceiptModeMst(ReceiptModeMst receiptModeMst, String branchCode);
	public StoreMst saveStoreMst(StoreMst storeMst, String branchCode);
	public UnitMst saveUnitMst(UnitMst unitMst, String branchCode);
	public AccountClass saveAccountClass(AccountClass accountClass,String branchCode);
	public AccountClassfc saveAccountClassfc(AccountClassfc accountClassfc, String branchCode);
	public AccountMergeMst saveAccountMergeMst(AccountMergeMst accountMergeMst,String branchCode);
	public CreditClass saveCreditClass(CreditClass creditClass,String branchCode);
	public CreditClassfc saveCreditClassfc(CreditClassfc creditClassfc,String branchCode);
	public DebitClass saveDebitClass(DebitClass debitClass,String branchCode);
	public DebitClassfc saveDebitClassfc(DebitClassfc debitClassfc,String branchCode);
	public InventoryLedgerMst saveInventoryLedgerMst(InventoryLedgerMst inventoryLedgerMst,String branchCode);
	public LedgerClass saveLedgerClass(LedgerClass ledgerClass,String branchCode);
	public LedgerClassfc saveLedgerClassfc(LedgerClassfc ledgerClassfc,String branchCode);
	public LinkedAccounts saveLinkedAccounts(LinkedAccounts linkedAccounts,String branchCode);
	public OpeningBalanceConfigMst saveOpeningBalanceConfigMst(OpeningBalanceConfigMst openingBalanceConfigMst,String branchCode);
	public OpeningBalanceMst saveOpeningBalanceMst(OpeningBalanceMst openingBalanceMst,String branchCode);
	public StockTransferOutHdr 	saveStockTransferOutHdr(StockTransferOutHdr stockTransferOutHdr,String branchCode,String toBranch);
	public StockTransferOutDtl saveStockTransferOutDtl(StockTransferOutDtl stockTransferOutDtl,String branchCode,String toBranch);
 
	public StockTransferInHdr saveStockTransferInHdr(@Valid StockTransferInHdr stockTransferInHdr, String mybranch,
			String fromBranch);
	public StockTransferInDtl saveStockTransferInDtl(@Valid StockTransferInDtl stockTransferInDtl, String mybranch,
			String fromBranch);
 
 
	public StockTransferInDtl saveAndPublishService(@Valid StockTransferInDtl stockTransferInDtl, String mybranch,
			String fromBranch);
 
 

    public AccountHeads saveAccountHeads(AccountHeads accountHeads,String branchCode);
    public AccountPayable saveAccountPayable(AccountPayable accountPayable ,String branchCode);
    public AccountReceivable saveAccountReceivable(AccountReceivable accountReceivable ,String branchCode);
    public AdditionalExpense saveAdditionalExpense(AdditionalExpense additionalExpense ,String branchCode);
    public BatchPriceDefinition saveBatchPriceDefinition(BatchPriceDefinition batchPriceDefinition ,String branchCode);
    public BrandMst saveBrandMst( BrandMst brandMst,String branchCode);
    public ConsumptionDtl saveConsumptionDtl( ConsumptionDtl consumptionDtl,String branchCode);
    public ConsumptionReasonMst  saveConsumptionReasonMst(ConsumptionReasonMst consumptionReasonMst ,String branchCode);
    public DamageHdr saveDamageHdr(DamageHdr damageHdr,String branchCode);
    public DamageDtl saveDamageDtl(DamageDtl damageDtl,String branchCode);
    public DayBook  saveDayBook(DayBook dayBook,String branchCode);
    public DayEndClosureHdr saveDayEndClosureHdr(DayEndClosureHdr dayEndClosureHdr,String branchCode);
    public DayEndClosureDtl  saveDayEndClosureDtl(DayEndClosureDtl dayEndClosureDtl,String branchCode);
    public DayEndReportStore  saveDayEndReportStore(DayEndReportStore dayEndReportStore,String branchCode);
    public DeliveryBoyMst saveDeliveryBoyMst(DeliveryBoyMst deliveryBoyMst,String branchCode);
    public GoodReceiveNoteDtl saveGoodReceiveNoteDtl(GoodReceiveNoteDtl goodReceiveNoteDtl,String branchCode);
    public GoodReceiveNoteHdr saveGoodReceiveNoteHr(GoodReceiveNoteHdr goodReceiveNoteHdr,String branchCode);
    public GroupMst saveGroupMst (GroupMst groupMst,String branchCode);
    public GroupPermissionMst saveGroupPermissionMst(GroupPermissionMst groupPermissionMst,String branchCode);
     public IntentInHdr saveIntentInHdr(IntentInHdr intentInHdr,String branchCode);
    public IntentInDtl  saveIntentInDtl(IntentInDtl intentInDtl,String branchCode);
    public ItemBatchExpiryDtl saveItemBatchExpiryDtl(ItemBatchExpiryDtl itemBatchExpiryDtl,String branchCode);
    public KitDefenitionDtl saveKitDefenitionDtl(KitDefenitionDtl kitDefenitionDtl,String branchCode);
    public KitDefinitionMst saveKitDefinitionMst(KitDefinitionMst kitDefinitionMst,String branchCode);
    public AddKotTable saveAddKotTable(AddKotTable addKotTable,String branchCode);
    public AddKotWaiter saveAddKotWaiter(AddKotWaiter addKotWaiter,String branchCode);
    public LocalCustomerMst saveLocalCustomerMst(LocalCustomerMst localCustomerMst,String branchCode);
    public LocalCustomerDtl saveLocalCustomerDtl(LocalCustomerDtl localCustomerDtl,String branchCode);
    public LocationMst saveLocationMst(LocationMst locationMst,String branchCode);
    public MultiUnitMst saveMultiUnitMst(MultiUnitMst multiUnitMst,String branchCode);
    public OrderTakerMst saveOrderTakerMst(OrderTakerMst orderTakerMst,String branchCode);
    public PDCPayment savePDCPayment(PDCPayment PDCPayment,String branchCode);
    public PDCReceipts savePDCReceipts(PDCReceipts PDCReceipts,String branchCode);
    public PDCReconcileHdr savePDCReconcileHdr(PDCReconcileHdr PDCReconcileHdr,String branchCode);
    public PDCReconcileDtl savePDCReconcileDtl(PDCReconcileDtl PDCReconcileDtl ,String branchCode);
    public PriceDefenitionMst savePriceDefenitionMst(PriceDefenitionMst priceDefenitionMst,String branchCode);
    public PriceDefinition savePriceDefinition(PriceDefinition priceDefinition,String branchCode);
    public ProcessMst saveProcessMst(ProcessMst processMst,String branchCode);
    public ProcessPermissionMst saveProcessPermissionMst(ProcessPermissionMst processPermissionMst,String branchCode);
    public ProductConversionConfigMst saveProductConversionConfigMst(ProductConversionConfigMst productConversionConfigMst,String branchCode);
    public ProductConversionDtl  saveProductConversionDtl(ProductConversionDtl productConversionDtl,String branchCode);
    public ProductMst saveProductMst(ProductMst productMst,String branchCode);
    public PurchaseOrderDtl savePurchaseOrderDtl(PurchaseOrderDtl purchaseOrderDtl,String branchCode);
    public PurchaseOrderHdr savePurchaseOrderHdr(PurchaseOrderHdr purchaseOrderHdr,String branchCode);
    public RawMaterialIssueDtl saveRawMaterialIssueDtl(RawMaterialIssueDtl rawMaterialIssueDtl,String branchCode);
    public RawMaterialIssueHdr  saveRawMaterialIssueHdr(RawMaterialIssueHdr rawMaterialIssueHdr, String branchCode);
    public RawMaterialReturnHdr saveRawMaterialReturnHdr(RawMaterialReturnHdr rawMaterialReturnHdr,String branchCode);
    public RawMaterialReturnDtl saveRawMaterialReturnDtl(RawMaterialReturnDtl rawMaterialReturnDtl,String branchCode);
    public ReceiptInvoiceDtl  saveReceiptInvoiceDtl(ReceiptInvoiceDtl receiptInvoiceDtl,String branchCode);
    public ReorderMst saveReorderMst(ReorderMst reorderMst,String branchCode);
    public IntentDtl  saveIntentDtl(IntentDtl intentDtl,String branchCode,String destinationBranch);
    public IntentHdr saveIntentHdr(IntentHdr intentHdr,String branchCode,String destinationBranch);
    public InvoiceEditEnableMst saveInvoiceEditEnableMst(InvoiceEditEnableMst invoiceEditEnableMst,String branchCode,String destinationBranch);
    public OtherBranchSalesDtl saveOtherBranchSalesDtl(OtherBranchSalesDtl otherBranchSalesDtl,String branchCode,String destinationBranch);
    public OtherBranchSalesTransHdr saveOtherBranchSalesTransHdr(OtherBranchSalesTransHdr otherBranchSalesTransHdr,String branchCode,String destinationBranch);
    public OtherBranchPurchaseHdr  saveOtherBranchPurchaseHdr(OtherBranchPurchaseHdr otherBranchPurchaseHdr,String branchCode,String destinationBranch);
    public OtherBranchPurchaseDtl saveOtherBranchPurchaseDtl(OtherBranchPurchaseDtl otherBranchPurchaseDtl,String branchCode,String destinationBranch);
    public OwnAccount saveOwnAccount(OwnAccount OwnAccount,String branchCode);
    public OwnAccountSettlementDtl  saveOwnAccountSettlementDtl(OwnAccountSettlementDtl ownAccountSettlementDtl,String branchCode);
    public OwnAccountSettlementMst  saveOwnAccountSettlementMst(OwnAccountSettlementMst ownAccountSettlementMst,String branchCode);
    public PaymentInvoiceDtl savePaymentInvoiceDtl(PaymentInvoiceDtl PaymentInvoiceDtl,String branchCode);
    public SaleOrderReceipt saveSaleOrderReceipt(SaleOrderReceipt saleOrderReceipt,String branchCode);
    public SalesOrderTransHdr   saveSalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr,String branchCode);
    public SalesOrderDtl   saveSalesOrderDtl(SalesOrderDtl salesOrderDtl,String branchCode);
    public SalesDtl  saveSalesDtl(SalesDtl salesDtl,String branchCode);
    public SalesDtl publishSalesDtl(SalesDtl salesDtl,String branchCode) ;
    
    public SalesReceipts  saveSalesReceipts(SalesReceipts salesReceipts,String branchCode);
    public SalesDtlDeleted saveSalesDtlDeleted(SalesDtlDeleted salesDtlDeleted,String branchCode);
    public SalesReturnDtl saveSalesReturnDtl(SalesReturnDtl salesReturnDtl,String branchCode);
    public SalesReturnHdr saveSalesReturnHdr(SalesReturnHdr salesReturnHdr,String branchCode);
    public SalesTypeMst  saveSalesTypeMst(SalesTypeMst salesTypeMst,String branchCode);
    public SchEligiAttrListDef saveSchEligiAttrListDef(SchEligiAttrListDef schEligiAttrListDef,String branchCode);
    public SchEligibilityAttribInst saveSchEligibilityAttribInst(SchEligibilityAttribInst schEligibilityAttribInst,String branchCode);
    public SchEligibilityDef saveSchEligibilityDef(SchEligibilityDef schEligibilityDef,String branchCode);
    public SchemeInstance saveSchemeInstance(SchemeInstance schemeInstance,String branchCode);
    public SchOfferAttrListDef saveSchOfferAttrListDef(SchOfferAttrListDef schOfferAttrListDef,String branchCode);
    public SchOfferAttrInst saveSchOfferAttrInst(SchOfferAttrInst schOfferAttrInst,String branchCode);
    public SchOfferDef saveSchOfferDef(SchOfferDef schOfferDef,String branchCode);
    public SchSelectAttrListDef saveSchSelectAttrListDef(SchSelectAttrListDef schSelectAttrListDef,String branchCode);
    public SchSelectDef saveSchSelectDef(SchSelectDef schSelectDef,String branchCode);
    public SchSelectionAttribInst saveSchSelectionAttribInst(SchSelectionAttribInst schSelectionAttribInst,String branchCode);
    public ServiceInHdr  saveServiceInHdr(ServiceInHdr serviceInHdr,String branchCode);
    public ServiceInDtl  saveServiceInDtl(ServiceInDtl serviceInDtl,String branchCode);
    public StoreChangeDtl saveStoreChangeDtl(StoreChangeDtl storeChangeDtl,String branchCode);
    public StoreChangeMst  saveStoreChangeMst(StoreChangeMst toreChangeMst,String branchCode);
    public UrsMst saveUrsMst(UrsMst ursMst,String branchCode);
	public AcceptStockMessage saveAcceptStockMessage(AcceptStockMessage acceptStockMessage, String mybranch, String fromBranch);
	
	public void publishObjectDeletion(Object Object , String branchCode,KafkaMapleEventType eventType,String id );
	
	public void publishObjectToKafkaEvent(Object Object , String branchCode,KafkaMapleEventType eventType,KafkaMapleEventType destination, String voucherNumber);
	public void publishStockTransferToKafkaEvent(Object Object , String branchCode,KafkaMapleEventType eventType,String destination,String voucherNumber);
	public StockTransferOutHdr saveStockTransferOutHdrRep(StockTransferOutHdr stockTransferOutHdr, String branchCode,
			String toBranch);
	
	public void salesFinalSaveToKafkaEvent(SalesTransHdr salesTransHdr,String branchCode,KafkaMapleEventType destination);
	public void purchaseFinalSaveToKafkaEvent(PurchaseHdr purchase, String mybranch, KafkaMapleEventType server);
	public void publishObjectToServer(Object Object, String branchCode, KafkaMapleEventType eventType,
			String destination, String voucherNumber);
    
 
}

