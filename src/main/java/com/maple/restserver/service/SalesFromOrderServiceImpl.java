package com.maple.restserver.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
 
import org.springframework.stereotype.Service;

import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.BatchPriceDefinition;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.InsuranceCompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.LocalCustomerMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.SaleOrderReceipt;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesMessageEntity;
import com.maple.restserver.entity.SalesOrderDtl;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.report.entity.HsnCodeSaleReport;
import com.maple.restserver.report.entity.InsuranceCompanySalesSummaryReport;
import com.maple.restserver.report.entity.PurchaseReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.SalesReport;
import com.maple.restserver.repository.AccountReceivableRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;

import com.maple.restserver.repository.InsuranceCompanyRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.LocalCustomerRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.SaleOrderReceiptRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesOrderDtlRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.UserMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.utils.SystemSetting;
 
 
 
@Service
@Transactional

public class SalesFromOrderServiceImpl implements SalesFromOrderService {
	
	  @Autowired
	   VoucherNumberService voucherService;
	 
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	
	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepository;

	
	LocalCustomerRepository localCustomerRepository;
	
	
	@Autowired
	SaleOrderReceiptRepository saleOrderReceiptRepository;
	
	
	@Autowired
	SalesReceiptsRepository salesReceiptsRepository;
	
	
	@Autowired
	SalesOrderDtlRepository salesOrderDtlRepository;
	
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	
	@Autowired
	KitDefinitionMstRepository kitdefMstRepository;
	
	
	@Autowired
	ItemMstRepository itemMstRepository;
	
	
	@Autowired
	PriceDefinitionRepository priceDefinitionRepository;
	
	
	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepository;
	
	@Autowired
	BatchPriceDefinitionService batchPriceDefinitionService;
	
	@Autowired
	SalesDetailsRepository salesDetailsRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Override
	public void convertSaleOrderToSales(SalesOrderTransHdr salesOrderTransHdr, String userID) throws PartialAccountingException {
		 /*
		  * 

		

			RestCaller.deleteSalesTransHdrBySalesOrderTransHdr(salesOrderTransHdr.getId());

			

			if (null != salesOrderTransHdr) {
				Double totalPaidAmount = 0.0;

				totalPaidAmount = salesOrderTransHdr.getPaidAmount();
				if (null != salesOrderTransHdr.getCashPay()) {
					txtTotalPaidAmount.setText(Double.toString(salesOrderTransHdr.getCashPay()));
				} else {
					txtTotalPaidAmount.setText("0.0");
				}

				if (null == salesTransHdr) {

					salesTransHdr = new SalesTransHdr();
					salesTransHdr.setSaleOrderHrdId(salesOrderTransHdr.getId());

					salesTransHdr.setPaidAmount(totalPaidAmount);
					salesTransHdr.setCustomerId(salesOrderTransHdr.getCustomerId().getId());
					salesTransHdr.setSalesMode("ORDER-CONVERTION");
					salesTransHdr.setCreditOrCash("CASH");
					salesTransHdr.setIsBranchSales("N");
					salesTransHdr.setUserId(SystemSetting.getUserId());
					salesTransHdr.setBranchCode(SystemSetting.systemBranch);
					if (null != salesOrderTransHdr.getCustomerId()) {
						CustomerMst customerMst = RestCaller.getCustomerById(salesOrderTransHdr.getCustomerId().getId())
								.getBody();

						salesTransHdr.setCustomerMst(customerMst);
						if(customerMst.getCustomerName().equalsIgnoreCase("POS"))
						{
							salesTransHdr.setSalesMode("POS");

						} else {
							if (null == customerMst.getCustomerGst() || customerMst.getCustomerGst().length() < 13) {
								salesTransHdr.setSalesMode("B2C");
							} else {
								salesTransHdr.setSalesMode("B2B");
							}
						}
						
					}
					// salesTransHdr.setDiscount(Double.toString(d));
					if (null != salesOrderTransHdr.getLocalCustomerId()) {
						LocalCustomerMst localCustomerMst = RestCaller
								.getLocalCustomerById(salesOrderTransHdr.getLocalCustomerId().getId()).getBody();
						salesTransHdr.setLocalCustomerMst(localCustomerMst);
					}
					salesTransHdr.setDiscount(Double.toString(salesTransHdr.getCustomerMst().getCustomerDiscount()));
					ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
					salesTransHdr = respentity.getBody();
					System.out.println("=======salesTransHdr======" + salesTransHdr);

					ResponseEntity<List<SaleOrderReceipt>> saleOrderReceiptList = RestCaller
							.getAllSaleOrderReceiptsBySalesTranOrdersHdr(salesOrderTransHdr.getId());
					Double advanceAmount = 0.0;
					if (null != salesTransHdr) {
						for (SaleOrderReceipt saleOrderReceipt : saleOrderReceiptList.getBody()) {
							if (!saleOrderReceipt.getReceiptMode().equals("CASH")) {
								salesReceipts = new SalesReceipts();
								salesReceipts.setReceiptMode(saleOrderReceipt.getReceiptMode());
								salesReceipts.setReceiptAmount(saleOrderReceipt.getReceiptAmount());
								salesReceipts.setUserId(SystemSetting.getUser().getId());
								salesReceipts.setBranchCode(SystemSetting.systemBranch);
								salesReceipts.setSoStatus("CONVERTED");
								if (null == salesReceiptVoucherNo) {
									salesReceiptVoucherNo = RestCaller
											.getVoucherNumber(SystemSetting.getSystemBranch());
									salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
								} else {
									salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
								}

								salesReceipts.setAccountId(saleOrderReceipt.getAccountId());
								LocalDate date = LocalDate.now();
								java.util.Date udate = SystemSetting.localToUtilDate(date);
								salesReceipts.setReceiptDate(udate);
								salesReceipts.setSalesTransHdr(salesTransHdr);

								ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
								salesReceipts = respEntity.getBody();
								if (null != salesReceipts) {
									salesReceiptsList.add(salesReceipts);

								}
								filltableCardAmount();
							} else {

								if (null != saleOrderReceipt.getReceiptAmount()) {
									advanceAmount = advanceAmount + saleOrderReceipt.getReceiptAmount();

									txtTotalPaidAmount.setText(Double.toString(advanceAmount));
								} else {
									txtTotalPaidAmount.setText("0.0");
								}
							}
						}
					}

				}

				if (null != salesTransHdr) {
					if (null != salesTransHdr.getId()) {

						ResponseEntity<List<SalesOrderDtl>> respentity = RestCaller
								.getSalesOrderTransDtlById(salesOrderTransHdr.getId());

						salesOrderDtlList = respentity.getBody();
						System.out.println("salesOrderDtlList--" + salesOrderDtlList.size());

						Boolean notinstock = false;
						List<String> itemNotInStockList = new ArrayList<String>();
 

						int i = 0;
						int j = 0;
						for (SalesOrderDtl salesOrderDtl : salesOrderDtlList) {

							System.out.println("SalesOrderDtl =" + i);

							Map<String, Double> map = new HashMap<String, Double>();
							map = RestCaller.getRequiredMaterial(salesOrderDtl.getItemId(), salesOrderDtl.getQty());

							if (map.size() == 0 || null == map || map.isEmpty()) {
								ResponseEntity<KitDefinitionMst> getKitMst = RestCaller
										.getKitByItemId(salesOrderDtl.getItemId());
								if (null != getKitMst.getBody()) {
									map.put("NOBATCH", salesOrderDtl.getQty());
								} else {
									notinstock = true;
									itemNotInStockList
											.add(salesOrderDtl.getItemName() + "(" + salesOrderDtl.getQty() + ")");
								}
							}

							for (Map.Entry<String, Double> batchAndQty : map.entrySet()) {

								System.out.println("batchAndQty =" + j);
								j++;

								salesDtl = new SalesDtl();
								salesDtl.setSalesTransHdr(salesTransHdr);
								salesDtl.setItemId(salesOrderDtl.getItemId());
								salesDtl.setBarcode(salesOrderDtl.getBarcode());
								salesDtl.setBatchCode(batchAndQty.getKey());

								ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(salesOrderDtl.getItemId());
								if (null != salesOrderDtl.getCessRate()) {
									salesDtl.setCessRate(salesOrderDtl.getCessRate());
								} else {
									salesDtl.setCessRate(0.0);
								}
								salesDtl.setExpiryDate(salesOrderDtl.getexpiryDate());
								salesDtl.setItemCode(salesOrderDtl.getItemCode());
								salesDtl.setItemName(salesOrderDtl.getItemName());
								java.util.Date udate = SystemSetting.getApplicationDate();
								String sdate1 = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

								ResponseEntity<PriceDefinition> pricebyItem = RestCaller
										.getPriceDefenitionByItemIdAndUnit(salesOrderDtl.getItemId(),
												salesTransHdr.getCustomerMst().getPriceTypeId(),
												salesOrderDtl.getUnitId(), sdate1);
								PriceDefinition priceDefinition = pricebyItem.getBody();
								if (null != priceDefinition) {

									salesDtl.setMrp(priceDefinition.getAmount());

									Double rate = (100 * priceDefinition.getAmount())
											/ (100 + salesOrderDtl.getTaxRate());
									salesDtl.setRate(rate);
								} else {
									salesDtl.setMrp(salesOrderDtl.getMrp());
									salesDtl.setRate(salesOrderDtl.getRate());
								}

								salesDtl.setQty(batchAndQty.getValue());
								salesDtl.setUnitId(salesOrderDtl.getUnitId());
								salesDtl.setUnitName(salesOrderDtl.getUnitName());
								if (null != salesOrderDtl.getSgstTaxRate()) {
									salesDtl.setSgstTaxRate(salesOrderDtl.getSgstTaxRate());
								} else {
									salesDtl.setSgstTaxRate(0.0);
								}
								if (null != salesOrderDtl.getCgstAmount()) {
									salesDtl.setCgstAmount(salesOrderDtl.getCgstAmount());
								} else {
									salesDtl.setCgstAmount(0.0);
								}
								if (null != salesOrderDtl.getSgstAmount()) {
									salesDtl.setSgstAmount(salesOrderDtl.getSgstAmount());
								} else {
									salesDtl.setSgstAmount(0.0);
								}
								if (null != salesOrderDtl.getCgstTaxRate()) {
									salesDtl.setCgstTaxRate(salesOrderDtl.getCgstTaxRate());
								} else {
									salesDtl.setCgstTaxRate(0.0);
								}

								salesDtl.setTaxRate(salesOrderDtl.getTaxRate());
								salesDtl.setCessRate(salesOrderDtl.getCessRate());
								salesDtl.setCessAmount(salesOrderDtl.getCessAmount());
								salesDtl.setMrp(salesOrderDtl.getMrp());
								salesDtl.setStandardPrice(salesOrderDtl.getMrp());
								salesDtl.setRate(salesOrderDtl.getRate());
								double cessAmount = 0.0;
								double cessRate = 0.0;
								double rateBeforeTax = 0.0;
 

								BigDecimal settoamount = new BigDecimal((salesDtl.getQty() * salesDtl.getRate()));

								double includingTax = (settoamount.doubleValue() * salesDtl.getTaxRate()) / 100;
								double amount = settoamount.doubleValue() + includingTax + salesDtl.getCessAmount();
								BigDecimal setamount = new BigDecimal(amount);
								setamount = setamount.setScale(2, BigDecimal.ROUND_CEILING);
								salesDtl.setAmount(setamount.doubleValue());

								ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
										.getPriceDefenitionMstByName("COST PRICE");
								PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
								if (null != priceDefenitionMst) {
									String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate,
											"yyyy-MM-dd");
									ResponseEntity<BatchPriceDefinition> batchpriceDef = RestCaller
											.getBatchPriceDefinition(salesDtl.getItemId(), priceDefenitionMst.getId(),
													salesDtl.getUnitId(), salesDtl.getBatch(), sdate);
									if (null != batchpriceDef.getBody()) {
										salesDtl.setCostPrice(batchpriceDef.getBody().getAmount());
									} else {
										ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
												.getPriceDefenitionByCostPrice(salesDtl.getItemId(),
														priceDefenitionMst.getId(), salesDtl.getUnitId(), sdate);

										PriceDefinition priceDefinition1 = priceDefenitionResp.getBody();
										if (null != priceDefinition1) {
											salesDtl.setCostPrice(priceDefinition1.getAmount());
										}
									}
								}

								ResponseEntity<SalesDtl> salesDtlrespentity = RestCaller.saveSalesDtl(salesDtl);
								salesDtl = salesDtlrespentity.getBody();
								System.out.println("=======salesDtl======" + salesDtl);
								System.out.println("=======salesDtl ID======" + salesDtl.getId());

							}

						}

						if (notinstock) {
							notifyMessage(5, "Not in Stock " + itemNotInStockList.toString() + "...!");
							txtCustomerName.clear();
							txtAccount.clear();
							txtCustomerPhoneNo.clear();
							txtSaleOrderHdrId.clear();
							txtTotalPaidAmount.clear();
							txtCashtopay.clear();
							txtBalance.clear();
							txtPaidamount.clear();
							txtChangeamount.clear();
							salesTransHdr = null;
							return false;
						}

					}

				}

			}

		}

		return true;
	
		  */
		
		
		// 1. Delete existing Sales based on SO Trans Hdr
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(salesOrderTransHdr.getCompanyMst().getId());
		
		salesTransHdrRepository.deleteBySaleOrderHrdId(salesOrderTransHdr.getId());
		Double totalPaidAmount = 0.0;

		// 2. Find Total Paid
		totalPaidAmount = salesOrderTransHdr.getPaidAmount();
		
		
		
		
		SalesTransHdr salesTransHdr = new SalesTransHdr();
		salesTransHdr.setSaleOrderHrdId(salesOrderTransHdr.getId());

		salesTransHdr.setPaidAmount(totalPaidAmount);
		salesTransHdr.setCustomerId(salesOrderTransHdr.getAccountHeads().getId());
		salesTransHdr.setSalesMode("ORDER-CONVERTION");
		salesTransHdr.setCreditOrCash("CASH");
		salesTransHdr.setIsBranchSales("N");
		salesTransHdr.setUserId(userID);
		salesTransHdr.setBranchCode(salesOrderTransHdr.getBranchCode());
		salesTransHdr.setCompanyMst(companyMstOpt.get());
		
	     AccountHeads  customerMst =  salesOrderTransHdr.getAccountHeads();
	     salesTransHdr.setAccountHeads(customerMst);
				
	 	if(customerMst.getAccountName().equalsIgnoreCase("POS"))
		{
			salesTransHdr.setSalesMode("POS");

		} else {
			if (null == customerMst.getPartyGst() || customerMst.getPartyGst().length() < 13) {
				salesTransHdr.setSalesMode("B2C");
			} else {
				salesTransHdr.setSalesMode("B2B");
			}
		}
	 	
	 	
 
		if (null != salesOrderTransHdr.getLocalCustomerId()) {
			//Optional<LocalCustomerMst> localCustomerMstOpt = localCustomerRepository.findById(salesOrderTransHdr.getLocalCustomerId());
			//if(localCustomerMstOpt.isPresent()) {
				salesTransHdr.setLocalCustomerMst(salesOrderTransHdr.getLocalCustomerId());
			//}
		}
		salesTransHdr.setDiscount(Double.toString(salesTransHdr.getAccountHeads().getCustomerDiscount()));
		
		salesTransHdrRepository.save(salesTransHdr);
	 
		
		List<SaleOrderReceipt>  SaleOrderReceiptLkst = saleOrderReceiptRepository.findByCompanyMstAndSalesOrderTransHdr(salesOrderTransHdr.getCompanyMst(),salesOrderTransHdr);
		double advanceAmount =0.0;
		for (SaleOrderReceipt saleOrderReceipt : SaleOrderReceiptLkst) {
			//if (!saleOrderReceipt.getReceiptMode().equals("CASH")) {
				SalesReceipts salesReceipts = new SalesReceipts();
				salesReceipts.setReceiptMode(saleOrderReceipt.getReceiptMode());
				salesReceipts.setReceiptAmount(saleOrderReceipt.getReceiptAmount());
				salesReceipts.setUserId(userID);
				salesReceipts.setBranchCode(salesOrderTransHdr.getBranchCode());
				salesReceipts.setSoStatus("CONVERTED");
				
				
				VoucherNumber voucherNo = voucherService.generateInvoice(salesOrderTransHdr.getBranchCode(),salesOrderTransHdr.getCompanyMst().getId());
				salesReceipts.setVoucherNumber(voucherNo.getCode());
			 

				salesReceipts.setAccountId(saleOrderReceipt.getAccountId());
				LocalDate date = LocalDate.now();
				java.util.Date udate = SystemSetting.localToUtilDate(date);
				salesReceipts.setReceiptDate(udate);
				salesReceipts.setSalesTransHdr(salesTransHdr);
				salesReceipts.setCompanyMst(salesTransHdr.getCompanyMst());
				
				
				salesReceiptsRepository.save(salesReceipts);
				
				 
				if (null != saleOrderReceipt.getReceiptAmount()) {
					advanceAmount = advanceAmount + saleOrderReceipt.getReceiptAmount();
 				 
				}
	
		}
		
		
		List<SalesOrderDtl> salesOrderDtlList = salesOrderDtlRepository.findBySalesOrderTransHdrId(salesOrderTransHdr.getId());
		for (SalesOrderDtl salesOrderDtl : salesOrderDtlList) {
			JSONArray map =  itemBatchDtlService.getBatchWiseQty(salesOrderDtl.getItemId(), salesOrderDtl.getQty());
			
			
			
			if ( null == map || map.isEmpty()) {
		 

				List<KitDefinitionMst> kitMstList =  kitdefMstRepository.findByItemId(salesOrderDtl.getItemId());
				
				if (kitMstList.size()>0) {
					//map.put("NOBATCH", salesOrderDtl.getQty());
					
					JSONObject obj = new JSONObject();
				    obj.put("batch", MapleConstants.Nobatch);
				    obj.put("qty", salesOrderDtl.getQty());
				    
				    
				    LocalDate date = LocalDate.now();
					java.util.Date udate = SystemSetting.localToUtilDate(date);
					
					
				    obj.put("exp", udate);
				    map.put(obj);
				} else {
					//notinstock = true;
					//itemNotInStockList
							//.add(salesOrderDtl.getItemName() + "(" + salesOrderDtl.getQty() + ")");
					throw new PartialAccountingException(salesOrderTransHdr.getId());
					
				}
			}
			
			int j = 0;
			
			Iterator<Object> objectIterator =  map.iterator();
		    
		    while(objectIterator.hasNext()) {
		        JSONObject object = (JSONObject) objectIterator.next();
		        String batch = (String) object.get("batch");
		        double qty = (double) object.get("qty");
		        java.sql.Date expDate = ( java.sql.Date) object.get("exp");
		       
		   // }
		    
			//for (Map.Entry<String, Double> batchAndQty : map.entrySet()) {

				System.out.println("batchAndQty =" + j);
				j++;

				Optional<ItemMst> itemOpt = itemMstRepository.findById(salesOrderDtl.getItemId());
				ItemMst item = itemOpt.get();
				
				
				SalesDtl salesDtl = new SalesDtl();
				salesDtl.setSalesTransHdr(salesTransHdr);
				salesDtl.setItemId(salesOrderDtl.getItemId());
				salesDtl.setBarcode(item.getBarCode());
				salesDtl.setBatch(batch);
				salesDtl.setCompanyMst(companyMstOpt.get());
			 
				salesDtl.setCessRate(item.getCess());
				 
				salesDtl.setExpiryDate(expDate);
				//salesDtl.setItemCode(item.getItemCode());
				salesDtl.setItemName(salesOrderDtl.getItemName());
				
				
				  LocalDate date = LocalDate.now();
					java.util.Date udate = SystemSetting.localToUtilDate(date);
					
					java.sql.Date sqlDateNow = SystemSetting.UtilDateToSQLDate(udate);
					
					
					
					List<PriceDefinition> priceList = priceDefinitionRepository.findByItemIdAndUnitId(item.getId(), salesTransHdr.getAccountHeads().getPriceTypeId(), 
							salesOrderDtl.getUnitId(), udate);
					
					if(priceList.size()==0)
					{
					 
					 
						  priceList = priceDefinitionRepository.findByItemIdAndUnitIdEnddateNull(
								item.getId(), salesTransHdr.getAccountHeads().getPriceTypeId(), 
								salesOrderDtl.getUnitId(), udate);
						 
					}
					
					
			 
				
				if (null != priceList &&  priceList.size()>0) {
					PriceDefinition priceDefinition = priceList.get(0);
					
					
					salesDtl.setMrp(priceDefinition.getAmount());

					Double rate = (100 * priceDefinition.getAmount())
							/ (100 + salesOrderDtl.getTaxRate());
					salesDtl.setRate(rate);
				} else {
					salesDtl.setMrp(salesOrderDtl.getMrp());
					salesDtl.setRate(salesOrderDtl.getRate());
				}

				salesDtl.setQty(qty);
				salesDtl.setUnitId(salesOrderDtl.getUnitId());
				salesDtl.setUnitName(salesOrderDtl.getUnitName());
				if (null != salesOrderDtl.getSgstTaxRate()) {
					salesDtl.setSgstTaxRate(salesOrderDtl.getSgstTaxRate());
				} else {
					salesDtl.setSgstTaxRate(0.0);
				}
				if (null != salesOrderDtl.getCgstAmount()) {
					salesDtl.setCgstAmount(salesOrderDtl.getCgstAmount());
				} else {
					salesDtl.setCgstAmount(0.0);
				}
				if (null != salesOrderDtl.getSgstAmount()) {
					salesDtl.setSgstAmount(salesOrderDtl.getSgstAmount());
				} else {
					salesDtl.setSgstAmount(0.0);
				}
				if (null != salesOrderDtl.getCgstTaxRate()) {
					salesDtl.setCgstTaxRate(salesOrderDtl.getCgstTaxRate());
				} else {
					salesDtl.setCgstTaxRate(0.0);
				}

				salesDtl.setTaxRate(salesOrderDtl.getTaxRate());
				salesDtl.setCessRate(salesOrderDtl.getCessRate());
				salesDtl.setCessAmount(salesOrderDtl.getCessAmount());
				salesDtl.setMrp(salesOrderDtl.getMrp());
				salesDtl.setStandardPrice(salesOrderDtl.getMrp());
				salesDtl.setRate(salesOrderDtl.getRate());
				salesDtl.setAddCessAmount(0.0);
				if( null != salesOrderDtl.getAddCessAmount())
				{
					salesDtl.setAddCessAmount(salesOrderDtl.getAddCessAmount());

				}
				salesDtl.setAddCessRate(0.0);

				if( null != salesOrderDtl.getAddCessRate())
				{
					salesDtl.setAddCessRate(salesOrderDtl.getAddCessRate());

				}
				salesDtl.setDiscount(0.0);
				
				if(null != salesOrderDtl.getDiscount())
				{
					salesDtl.setDiscount(salesOrderDtl.getDiscount());

				}
			 
				if(null==salesDtl.getIgstAmount()) {
					salesDtl.setIgstAmount(0.0);
				}
				
				if(null != salesOrderDtl.getDiscount())
				{
					salesDtl.setDiscount(salesOrderDtl.getDiscount());

				}
				
				if(null==salesDtl.getIgstTaxRate()) {
					salesDtl.setIgstTaxRate(0.0);
				}
				
				double cessAmount = 0.0;
				double cessRate = 0.0;
				double rateBeforeTax = 0.0;


				BigDecimal settoamount = new BigDecimal((salesDtl.getQty() * salesDtl.getRate()));

				double includingTax = (settoamount.doubleValue() * salesDtl.getTaxRate()) / 100;
				double amount = settoamount.doubleValue() + includingTax + salesDtl.getCessAmount();
				BigDecimal setamount = new BigDecimal(amount);
				setamount = setamount.setScale(2, BigDecimal.ROUND_CEILING);
				salesDtl.setAmount(setamount.doubleValue());

				
		 
				
				List<PriceDefenitionMst> priceDefenitionMstRespList = priceDefinitionMstRepository.findByPriceLevelName("COST PRICE");
				
					 
				
				if (null != priceDefenitionMstRespList && priceDefenitionMstRespList.size()>0) {
					
				
					PriceDefenitionMst priceDefenitionMst = priceDefenitionMstRespList.get(0);
					
					BatchPriceDefinition batchPriceDefinition = batchPriceDefinitionService.priceDefinitionByItemIdAndUnitAndBatch(item.getId(),
							priceDefenitionMst.getId(),salesDtl.getUnitId(), salesDtl.getBatch(), sqlDateNow);
					
				 
				 
					 
					if (null !=batchPriceDefinition) {
						salesDtl.setCostPrice(batchPriceDefinition.getAmount());
					} else {
						
						PriceDefinition priceDefinition = priceDefinitionRepository.findByItemIdAndStartDateCostprice(item.getId(), 
								priceDefenitionMst.getId(),sqlDateNow, salesDtl.getUnitId());
						
					 
						  
						if (null != priceDefinition) {
							salesDtl.setCostPrice(priceDefinition.getAmount());
						}
					}
				}

				salesDetailsRepository.save(salesDtl);
				

			}
			
		}
	 
 
	}
}
