package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.report.entity.DailyPaymentSummaryReport;
import com.maple.restserver.report.entity.OrgPaymentVoucher;
import com.maple.restserver.report.entity.PaymentReports;
import com.maple.restserver.report.entity.PaymentVoucher;

@Service
@Transactional
public interface PaymentReportService {


	public  List<Object> getDailyPayment(String voucherNumber, Date voucherDate);
	public List<PaymentVoucher> getPayments( );
	
	List<PaymentVoucher>	getPaymentReport(String vouchernumber,Date date, String branchCode);

	
	 List<OrgPaymentVoucher> getOrgPaymentReport(String vouchernumber,Date date,String branchCode,String memberid); 
		
	List<PaymentDtl>	findByAccountIdAndInstrumentDate(String accountid,Date reportdate);
	
	public List<DailyPaymentSummaryReport> getDailyPaymentSummaryReport(String companymstid, Date date);

//version 3.1
List<PaymentVoucher>getPaymentDtlReport(Date sdate,Date  edate,CompanyMst companyMst	,String branchCode);

//version 3.1

//-------------------version 4.14
public DailyPaymentSummaryReport getDailyPaymentSummaryReportByRecieptMode(String companymstid, Date date,
		String paymentMode);
//----------------version 4.14


List<PaymentHdr>fetchPaymnetReportHdr(CompanyMst companyMst,String branchcode,Date fromDate,	Date toDate);

List<PaymentReports>exportPaymentReport( CompanyMst companyMst,Date fromDate,Date toDate,String branchcode);


List<PaymentReports>paymentReportReportPrinting( CompanyMst companyMst,Date fromDate,Date toDate,String branchcode);

List<PaymentReports>paymentReport(CompanyMst companyMst,Date fromDate,String branchcode);
public List<PaymentReports> pettCashpaymentReport(CompanyMst companyMst, Date fromDate, String branchcode);

List<PaymentDtl>fetchBypaymenthdrId(String paymenthdrId);
}
