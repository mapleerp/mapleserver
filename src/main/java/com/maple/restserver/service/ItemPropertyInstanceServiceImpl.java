package com.maple.restserver.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemPropertyConfig;
import com.maple.restserver.entity.ItemPropertyInstance;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.repository.ItemPropertyConfigRepository;
import com.maple.restserver.repository.ItemPropertyInstanceRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;

@Service
@Transactional
@Component
public class ItemPropertyInstanceServiceImpl implements ItemPropertyInstanceService{

	@Autowired
	PurchaseDtlRepository purchaseDtlRepo;
	@Autowired
	ItemPropertyConfigRepository itempropertyConfigRepo;
	
	@Autowired
	ItemPropertyInstanceRepository itemPropertyInstanceRepo;
	@Override
	public Boolean checkForItemPropertyInstance(String itemid, String purchasedtl,CompanyMst companymst,String branchcode) {
		
		List<ItemPropertyConfig> itemPropertyConfigList = itempropertyConfigRepo.findByItemId(itemid);
		PurchaseDtl purchaseDtl = purchaseDtlRepo.findById(purchasedtl).get();

		List<ItemPropertyInstance> itempropertyInstanceList = itemPropertyInstanceRepo.findByCompanyMstAndBranchCodeAndPurchaseDtl(companymst, branchcode, purchaseDtl);
		if(itemPropertyConfigList.size() != itempropertyInstanceList.size())
		{
			return false;
		}
		else
		{
			return true;
		}
	}

}
