package com.maple.restserver.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.lang.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.google.common.eventbus.EventBus;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.AccountClassfc;
import com.maple.restserver.accounting.entity.AccountMergeMst;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.CreditClassfc;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.DebitClassfc;
import com.maple.restserver.accounting.entity.InventoryLedgerMst;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.entity.LedgerClassfc;
import com.maple.restserver.accounting.entity.LinkedAccounts;
import com.maple.restserver.accounting.entity.OpeningBalanceConfigMst;
import com.maple.restserver.accounting.entity.OpeningBalanceMst;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.AccountClassfcRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.CreditClassfcRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.DebitClassfcRepository;
import com.maple.restserver.accounting.repository.InventoryLedgerMstRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassfcRepository;
import com.maple.restserver.accounting.repository.LinkedAccountsRepository;
import com.maple.restserver.accounting.repository.OpeningBalanceConfigMstRepository;
import com.maple.restserver.accounting.repository.OpeningBalanceMstRepository;
import com.maple.restserver.entity.*;

import com.maple.restserver.jms.send.KafkaMapleEvent;
import com.maple.restserver.jms.send.KafkaMapleEventType;

import com.maple.restserver.message.entity.AcceptStockMessage;
import com.maple.restserver.message.entity.PurchaseMessageEntity;
import com.maple.restserver.message.entity.SalesHdrAndDtlMessageEntity;
import com.maple.restserver.repository.*;
import com.maple.restserver.utils.EventBusFactory;

@Service

public class SaveAndPublishServiceImpl implements SaveAndPublishService {
	private final Logger logger = LoggerFactory.getLogger(SaveAndPublishService.class);
	Integer partitionKey = 1;

	EventBus eventBus = EventBusFactory.getEventBus();
	
	@Value("${outgoing_folder}")
	private String outgoing_folder;
	@Value("${outgoing_processed_folder}")
	private String outgoing_processed_folder;
	
	private String file_sufix = ".dat";
	private String name_differentiator = "$";

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@Autowired
	ItemMstRepository itemMstRepository;
 
	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	BranchMstRepository branchMstRepository;

	@Autowired
	UserMstRepository userMstRepository;

	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;

	

	@Autowired
	PurchaseDtlRepository purchaseDtlRepository;
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;
	@Autowired
	ItemMergeMstRepository itemMergeMstRepository;
	@Autowired
	CategoryMstRepository categoryMstRepository;
	@Autowired
	PaymentHdrRepository paymentHdrRepository;
	@Autowired
	PaymentDtlRepository paymentDtlRepository;
	@Autowired
	ActualProductionHdrRepository actualProductionHdrRepository;
	@Autowired
	ActualProductionDtlRepository actualProductionDtlRepository;
	@Autowired
	PurchaseSchemeMstRepository purchaseSchemeMstRepository;
	@Autowired
	ReceiptHdrRepository receiptHdrRepository;
	@Autowired
	ReceiptDtlRepository receiptDtlRepository;
	@Autowired
	ReciptModeMstRepository receiptModeMstRepository;
	@Autowired
	StoreMstRepository storeMstRepository;
	@Autowired
	UnitMstRepository unitMstRepository;
	@Autowired
	AccountClassRepository accountClassRepository;
	@Autowired
	AccountClassfcRepository accountClassfcRepository;
	@Autowired
	AccountMergeRepository accountMergeRepository;
	@Autowired
	CreditClassRepository creditClassRepository;

	@Autowired
	CreditClassfcRepository creditClassfcRepository;
	@Autowired
	DebitClassRepository debitClassRepository;
	@Autowired
	DebitClassfcRepository debitClassfcRepository;
	@Autowired
	InventoryLedgerMstRepository inventoryLedgerMstRepository;
	@Autowired
	LedgerClassRepository ledgerClassRepository;
	@Autowired
	LedgerClassfcRepository ledgerClassfcRepository;
	@Autowired
	LinkedAccountsRepository linkedAccountsRepository;
	@Autowired
	OpeningBalanceConfigMstRepository openingBalanceConfigMstRepository;
	@Autowired
	OpeningBalanceMstRepository openingBalanceMstRepository;
	@Autowired
	StockTransferOutHdrRepository stockTransferOutHdrRepository;
	@Autowired
	StockTransferOutDtlRepository stockTransferOutDtlRepository;

	@Autowired
	StockTransferInHdrRepository stockTransferInHdrRepository;

	@Autowired
	StockTransferInDtlRepository stockTransferInDtlRepository;
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	@Autowired
	AccountPayableRepository accountPayableRepository;
	@Autowired
	AccountReceivableRepository accountReceivableRepository;
	@Autowired
	AdditionalExpenseRepository additionalExpenseRepository;
	@Autowired
	BatchPriceDefinitionRepository batchPriceDefinitionRepository;
	@Autowired
	BrandMstRepository brandMstRepository;
	@Autowired
	ConsumptionDtlRepository consumptionDtlRepository;
	@Autowired
	ConsumptionReasonMstRepository consumptionReasonMstRepository;
	@Autowired
	PDCReconcileDtlRepository pDCReconcileDtlRepository;
	@Autowired
	DamageDtlRepository damageDtlRepository;
	@Autowired
	PDCReconcileHdrRepository PDCReconcileHdrRepository;
	@Autowired
	DayBookRepository dayBookRepository;
	@Autowired
	DayEndClosureRepository dayEndClosureHdrRepository;

	@Autowired
	DayEndClosureDtlRepository dayEndClosureDtlRepository;
	@Autowired
	DayEndReportStoreRepository dayEndReportStoreRepository;

	@Autowired
	DelivaryBoyMstRepository deliveryBoyMstRepository;
	@Autowired
	GoodReceiveNoteDtlRepository goodReceiveNoteDtlRepository;

	@Autowired
	GoodReceiveNoteHdrRepository goodReceiveNoteHdrRepository;
	@Autowired
	IntentInHdrRepository intentInHdrRepository;
	@Autowired
	IntentInDtlRepository intentInDtlRepository;
	@Autowired
	GroupMstRepository groupMstRepository;
	@Autowired
	GroupPermissionRepository groupPermissionMstRepository;
	@Autowired
	ItemBatchExpiryDtlRepository itemBatchExpiryDtlRepository;

	@Autowired
	KitDefenitionDtlRepository kitDefenitionDtlRepository;
	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepository;

	@Autowired
	AddKotTableRepository addKotTableRepository;
	@Autowired
	AddKotWaiterRepository addKotWaiterRepository;
	@Autowired
	LocalCustomerRepository localCustomerMstRepository;
	@Autowired
	LocalCustomerDtlRepository localCustomerDtlRepository;

	@Autowired
	PDCReceiptRepository PDCReceiptsRepository;
	@Autowired
	LocationMstRepository locationMstRepository;
	@Autowired
	PDCPaymentRepository PDCPaymentRepository;

	@Autowired
	MultiUnitMstRepository multiUnitMstRepository;

	@Autowired
	OrderTakerMstRepository orderTakerMstRepository;
	@Autowired
	DamageHdrRepository damageHdrRepository;
	@Autowired
	ProductMstRepository productMstRepository;
	@Autowired
	PriceDefinitionRepository priceDefinitionRepository;

	@Autowired
	PriceDefinitionMstRepository priceDefenitionMstRepository;
	@Autowired
	ProductConversionDtlRepository productConversionDtlRepository;

	@Autowired
	ProcessMstRepository processMstRepository;
	@Autowired
	ProcessPermissionRepository processPermissionMstRepository;
	@Autowired
	ProductConfigurationMstRepository productConversionConfigMstRepository;
	@Autowired
	PurchaseOrderDtlRepository purchaseOrderDtlRepository;
	@Autowired
	RawMaterialIssueDtlRepository rawMaterialIssueDtlRepository;
	@Autowired
	PurchaseOrderHdrRepository purchaseOrderHdrRepository;
	@Autowired
	RawMaterialMstRepository rawMaterialIssueHdrRepository;
	@Autowired
	RawMaterialReturnHdrRepository rawMaterialReturnHdrRepository;
	@Autowired
	ReceiptInvoiceDtlRepository receiptInvoiceDtlRepository;
	@Autowired
	RawMaterialReturnDtlRepository rawMaterialReturnDtlRepository;
	@Autowired
	ReorderMstRepository reorderMstRepository;
	@Autowired
	PaymentInvoiceDtlRepository paymentInvoiceDtlRepository;
	@Autowired
	OwnAccountSettlementMstRepository ownAccountSettlementMstRepository;
	@Autowired
	OwnAccountSettlementDtlRepository ownAccountSettlementDtlRepository;
	@Autowired
	OwnAccountRepository OwnAccountRepository;

	@Autowired
	OtherBranchPurchaseDtlRepository otherBranchPurchaseDtlRepository;

	@Autowired
	OtherBranchPurchaseHdrRepository otherBranchPurchaseHdrRepository;
	@Autowired
	OtherBranchSalesTransHdrRepository otherBranchSalesTransHdrRepository;

	@Autowired
	OtherBranchSalesDtlRepository otherBranchSalesDtlRepository;
	@Autowired
	IntentDtlRepository intentDtlRepository;

	@Autowired
	IntentHdrRepository intentHdrRepository;
	@Autowired
	InvoiceEditEnableRepository invoiceEditEnableMstRepository;

	@Autowired
	URSMstRepository ursMstRepository;
	@Autowired
	SaleOrderReceiptRepository saleOrderReceiptRepository;
	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepository;
	@Autowired
	SalesReceiptsRepository salesReceiptsRepository;
	@Autowired
	SalesTypeMstRepository salesTypeMstRepository;
	@Autowired
	SchEligibilityAttribInstRepository schEligibilityAttribInstRepository;
	@Autowired
	SchemeInstanceRepository schemeInstanceRepository;
	@Autowired
	SchSelectAttrListDefReoository schSelectAttrListDefRepository;
	@Autowired
	SchOfferAttrInstRepository schOfferAttrInstRepository;
	@Autowired
	SchSelectDefRepository schSelectDefRepository;
	@Autowired
	ServiceInHdrRepository serviceInHdrRepository;
	@Autowired
	StoreChangeDtlRepository storeChangeDtlRepository;
	@Autowired
	SalesOrderDtlRepository salesOrderDtlRepository;
	@Autowired
	SalesDtlDeletedRepository salesDtlDeletedRepository;
	@Autowired
	SalesDetailsRepository salesDtlRepository;
	@Autowired
	SalesRetunDtlRepository salesReturnDtlRepository;
	@Autowired
	SalesReturnHdrRepository salesReturnHdrRepository;
	@Autowired
	SchEligiAttrListDefRepository schEligiAttrListDefRepository;
	@Autowired
	StoreChangeMstRepository storeChangeMstRepository;
	@Autowired
	ServiceInDtlRepository serviceInDtlRepository;
	@Autowired
	SchSelectionAttribInstRepository schSelectionAttribInstRepository;
	@Autowired
	SchOfferDefRepository schOfferDefRepository;
	@Autowired
	SchOfferAttrListDefRepository schOfferAttrListDefRepository;
	@Autowired
	SchEligibilityDefRepository schEligibilityDefRepository;
	@Autowired
	StockTransferInDtlService stockTransferInDtlService;
	
	@Value("${mybranch}")
	private String mybranch;

	@Override
	@Transactional
	public SalesTransHdr saveSalesTransHdr(SalesTransHdr salesTransHdr, String branchCode) {

		String destination = "SERVER";

		salesTransHdr = salesTransHdrRepository.save(salesTransHdr);

//		String jsonStr = "";
//
//		ObjectMapper Obj = new ObjectMapper();
//		try {
//			// Converting the Java object into a JSON string
//			jsonStr = Obj.writeValueAsString(salesTransHdr);
//			logger.info("Converted to JSON String: {} ", jsonStr);
//		} catch (IOException e) {
//			System.out.println(jsonStr);
//			logger.info("Exception : {} ", e);
//		}
//		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
//		kafkaMapleEvent.setLibraryEventId(partitionKey);
//		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SALESTRANSHDR);
//		kafkaMapleEvent.setPayLoad(jsonStr);
//		kafkaMapleEvent.setSource(branchCode);
//		kafkaMapleEvent.setDestination(destination);
//		kafkaMapleEvent.setSource(branchCode);
//		kafkaMapleEvent.setDestination(destination);
//		eventBus.post(kafkaMapleEvent);

		/*
		 * try { sendToKafkaTopic.sendKafkaMessageLibraryEvent(kafkaMapleEvent,
		 * branchCode, destination); } catch (JsonProcessingException e) {
		 * logger.info("JsonProcessingException: {} ", e); e.printStackTrace(); }
		 */

		return salesTransHdr;

	}

	@Override
	public ItemMst saveItemMst(ItemMst itemMst, String branchCode) {

		String destination="";
		
		itemMst = itemMstRepository.save(itemMst);
		
		
		String categoryId = itemMst.getCategoryId();
		Optional<CategoryMst> categoryMstOpt = categoryMstRepository.findById(categoryId);
		CategoryMst categoryMst = categoryMstOpt.get();
	
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
		
		//=========category publishing==================MAP-549
		publishObject(categoryMst,mybranch,destination);

	
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(itemMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ITEMMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
								 itemMst.getId()+KafkaMapleEventType.ITEMMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);
		
		kafkaMapleEvent.setVoucherNumber(itemMst.getId());
		eventBus.post(kafkaMapleEvent);


			}
		}
		return itemMst;
	}

	private void publishObject(CategoryMst categoryMst, String branchCode, String destinationBranch ) {
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			
			jsonStr = Obj.writeValueAsString(categoryMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.CATEGORYMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destinationBranch);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 categoryMst.getId()+KafkaMapleEventType.CATEGORYMST + file_sufix;
        kafkaMapleEvent.setSourceFile(sourceFileName);
		kafkaMapleEvent.setVoucherNumber(categoryMst.getId());
		eventBus.post(kafkaMapleEvent);

		
	}

	@Override
	public CompanyMst saveCompanyMst(CompanyMst companyMst, String branchCode) {
		String destination = "";

		companyMst = companyMstRepository.save(companyMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(companyMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.COMPANYMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 companyMst.getId()+KafkaMapleEventType.COMPANYMST + file_sufix;
        kafkaMapleEvent.setSourceFile(sourceFileName);

        kafkaMapleEvent.setVoucherNumber(companyMst.getId());
		
		eventBus.post(kafkaMapleEvent);
			}
		}


		return companyMst;
	}

	@Override
	public BranchMst saveBranchMst(BranchMst branchMst, String branchCode) {
		String destination = "";

		branchMst = branchMstRepository.save(branchMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(branchMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.BRANCHMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 branchMst.getId()+KafkaMapleEventType.BRANCHMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(branchMst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return branchMst;
	}

	@Override
	public UserMst saveUserMst(UserMst userMst, String branchCode) {
		String destination = "";

		userMst = userMstRepository.save(userMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(userMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.USERMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 userMst.getId()+KafkaMapleEventType.USERMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(userMst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return userMst;
	}

	@Override
	@Transactional
	public PurchaseHdr savePurchaseHdr(PurchaseHdr purchaseHdr, String branchCode) {
		

		purchaseHdr = purchaseHdrRepository.save(purchaseHdr);



		return purchaseHdr;
	}

	
	@Override
	@Transactional
	public PurchaseDtl savePurchaseDtl(PurchaseDtl purchaseDtl, String branchCode) {
		

		purchaseDtl = purchaseDtlRepository.save(purchaseDtl);

		

		return purchaseDtl;
	}

	@Override
	@Transactional
	public ItemBatchDtl saveItemBatchDtl(ItemBatchDtl itemBatchDtl, String branchCode) {
		

		itemBatchDtl = itemBatchDtlRepository.save(itemBatchDtl);


		return itemBatchDtl;
	}

	@Override
	public ItemMergeMst saveItemMergeMst(ItemMergeMst itemMergeMst, String branchCode) {
		String destination = "";

		itemMergeMst = itemMergeMstRepository.save(itemMergeMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(itemMergeMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ITEMMERGEMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 itemMergeMst.getId()+KafkaMapleEventType.ITEMMERGEMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(itemMergeMst.getId());
		eventBus.post(kafkaMapleEvent);
			}
		}

		return itemMergeMst;
	}

	@Override
	public CategoryMst saveCategoryMst(CategoryMst categoryMst, String branchCode) {
		String destination = "";

		categoryMst = categoryMstRepository.save(categoryMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
			
			
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(categoryMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.CATEGORYMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setVoucherNumber(categoryMst.getId());
		kafkaMapleEvent.setDestination(destination);
		
		
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 categoryMst.getId()+KafkaMapleEventType.CATEGORYMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(categoryMst.getId());
		eventBus.post(kafkaMapleEvent);
			}
		}
		
		return categoryMst;
	}

	@Override
	public PaymentHdr savePaymentHdr(PaymentHdr paymentHdr, String branchCode) {
		String destination = "SERVER";

		paymentHdr = paymentHdrRepository.save(paymentHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(paymentHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PAYMENTHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 paymentHdr.getId()+KafkaMapleEventType.PAYMENTHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(paymentHdr.getId());
		eventBus.post(kafkaMapleEvent);

		return paymentHdr;
	}

	@Override
	public PaymentDtl savePaymentDtl(PaymentDtl paymentDtl, String branchCode) {
		String destination = "SERVER";

		paymentDtl = paymentDtlRepository.save(paymentDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(paymentDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PAYMENTDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 paymentDtl.getId()+KafkaMapleEventType.PAYMENTDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(paymentDtl.getId());
		eventBus.post(kafkaMapleEvent);

		return paymentDtl;
	}

	@Override
	public ActualProductionHdr saveActualProductionHdr(ActualProductionHdr actualProductionHdr, String branchCode) {
		String destination = "SERVER";

		actualProductionHdr = actualProductionHdrRepository.save(actualProductionHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(actualProductionHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ACTUALPRODUCTIONHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 actualProductionHdr.getId()+KafkaMapleEventType.ACTUALPRODUCTIONHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(actualProductionHdr.getId());
		eventBus.post(kafkaMapleEvent);

		return actualProductionHdr;
	}

	@Override
	public ActualProductionDtl saveActualProductionDtl(ActualProductionDtl actualProductionDtl, String branchCode) {
		String destination = "SERVER";

		actualProductionDtl = actualProductionDtlRepository.save(actualProductionDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(actualProductionDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ACTUALPRODUCTIONDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 actualProductionDtl.getId()+KafkaMapleEventType.ACTUALPRODUCTIONDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(actualProductionDtl.getId());
		eventBus.post(kafkaMapleEvent);

		return actualProductionDtl;
	}

	@Override
	public PurchaseSchemeMst savePurchaseSchemeMst(PurchaseSchemeMst purchaseSchemeMst, String branchCode) {
		String destination = "";

		purchaseSchemeMst = purchaseSchemeMstRepository.save(purchaseSchemeMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(purchaseSchemeMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PURCHASESCHEMEMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 purchaseSchemeMst.getId()+KafkaMapleEventType.PURCHASESCHEMEMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(purchaseSchemeMst.getId());
		eventBus.post(kafkaMapleEvent);
			}
		}

		return purchaseSchemeMst;
	}

	@Override
	public ReceiptHdr saveReceiptHdr(ReceiptHdr receiptHdr, String branchCode) {
		String destination = "SERVER";

		receiptHdr = receiptHdrRepository.save(receiptHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(receiptHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.RECEIPTHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 receiptHdr.getId()+KafkaMapleEventType.RECEIPTHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(receiptHdr.getId());
		eventBus.post(kafkaMapleEvent);

		return receiptHdr;
	}

	@Override
	public ReceiptDtl saveReceiptDtl(ReceiptDtl receiptDtl, String branchCode) {
		String destination = "SERVER";

		receiptDtl = receiptDtlRepository.save(receiptDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(receiptDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.RECEIPTDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 receiptDtl.getId()+KafkaMapleEventType.RECEIPTDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(receiptDtl.getId());
		eventBus.post(kafkaMapleEvent);

		return receiptDtl;
	}

	@Override
	public ReceiptModeMst saveReceiptModeMst(ReceiptModeMst receiptModeMst, String branchCode) {
		String destination = "";

		receiptModeMst = receiptModeMstRepository.save(receiptModeMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(receiptModeMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.RECEIPTMODEMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 receiptModeMst.getId()+KafkaMapleEventType.RECEIPTMODEMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(receiptModeMst.getId());
		eventBus.post(kafkaMapleEvent);
			}
		}

		return receiptModeMst;
	}

	@Override
	public StoreMst saveStoreMst(StoreMst storeMst, String branchCode) {
		String destination = "";

		storeMst = storeMstRepository.save(storeMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(storeMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.STOREMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 storeMst.getId()+KafkaMapleEventType.STOREMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(storeMst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}
		return storeMst;
	}

	@Override
	public UnitMst saveUnitMst(UnitMst unitMst, String branchCode) {
		
		
		String destination="";

		unitMst = unitMstRepository.save(unitMst);
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(unitMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.UNITMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				unitMst.getId()+KafkaMapleEventType.UNITMST + file_sufix;
		
		
        kafkaMapleEvent.setSourceFile(sourceFileName);
		kafkaMapleEvent.setVoucherNumber(unitMst.getId());
		eventBus.post(kafkaMapleEvent);
			}
		}
		
		return unitMst;
	}

	@Override
	@Transactional
	public AccountClass saveAccountClass(AccountClass accountClass, String branchCode) {
		String destination = "SERVER";

		accountClass = accountClassRepository.save(accountClass);
	
		/*
		 * String jsonStr = "";
		 * 
		 * ObjectMapper Obj = new ObjectMapper(); try { // Converting the Java object
		 * into a JSON string jsonStr = Obj.writeValueAsString(accountClass);
		 * logger.info("Converted to JSON String: {} ", jsonStr); } catch (IOException
		 * e) { System.out.println(jsonStr); logger.info("Exception : {} ", e); }
		 * KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		 * kafkaMapleEvent.setLibraryEventId(partitionKey);
		 * kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ACCOUNTCLASS);
		 * kafkaMapleEvent.setPayLoad(jsonStr);
		 * 
		 * kafkaMapleEvent.setSource(branchCode);
		 * kafkaMapleEvent.setDestination(destination); String sourceFileName =
		 * kafkaMapleEvent.getDestination() + name_differentiator +
		 * accountClass.getId()+KafkaMapleEventType.ACCOUNTCLASS + file_sufix;
		 * kafkaMapleEvent.setSourceFile(sourceFileName);
		 * 
		 * kafkaMapleEvent.setVoucherNumber(accountClass.getId());
		 * eventBus.post(kafkaMapleEvent);
		 */

		

		return accountClass;
	}

	@Override
	public AccountClassfc saveAccountClassfc(AccountClassfc accountClassfc, String branchCode) {
		String destination = "SERVER";

		accountClassfc = accountClassfcRepository.save(accountClassfc);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(accountClassfc);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ACCOUNTCLASSFC);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 accountClassfc.getId()+KafkaMapleEventType.ACCOUNTCLASSFC + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(accountClassfc.getId());
		eventBus.post(kafkaMapleEvent);

		return accountClassfc;
	}

	@Override
	public AccountMergeMst saveAccountMergeMst(AccountMergeMst accountMergeMst, String branchCode) {
		String destination = "SERVER";

		accountMergeMst = accountMergeRepository.save(accountMergeMst);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(accountMergeMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ACCOUNTMERGEMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 accountMergeMst.getId()+KafkaMapleEventType.ACCOUNTMERGEMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(accountMergeMst.getId());
		eventBus.post(kafkaMapleEvent);

		return accountMergeMst;
	}

	@Override
	@Transactional
	public CreditClass saveCreditClass(CreditClass creditClass, String branchCode) {

		creditClass = creditClassRepository.save(creditClass);

		String destination = "SERVER";
		/*
		 * String jsonStr = "";
		 * 
		 * ObjectMapper Obj = new ObjectMapper(); try { // Converting the Java object
		 * into a JSON string jsonStr = Obj.writeValueAsString(creditClass);
		 * logger.info("Converted to JSON String: {} ", jsonStr); } catch (IOException
		 * e) { System.out.println(jsonStr); logger.info("Exception : {} ", e); }
		 * KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		 * kafkaMapleEvent.setLibraryEventId(partitionKey);
		 * kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.CREDITCLASS);
		 * kafkaMapleEvent.setPayLoad(jsonStr);
		 * 
		 * kafkaMapleEvent.setSource(branchCode);
		 * kafkaMapleEvent.setDestination(destination); String sourceFileName =
		 * kafkaMapleEvent.getDestination() + name_differentiator +
		 * creditClass.getId()+KafkaMapleEventType.CREDITCLASS + file_sufix;
		 * kafkaMapleEvent.setSourceFile(sourceFileName);
		 * 
		 * kafkaMapleEvent.setVoucherNumber(creditClass.getId());
		 * eventBus.post(kafkaMapleEvent);
		 */

		return creditClass;
	}

	@Override
	public CreditClassfc saveCreditClassfc(CreditClassfc creditClassfc, String branchCode) {
		String destination = "SERVER";

		creditClassfc = creditClassfcRepository.save(creditClassfc);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(creditClassfc);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.CREDITCLASSFC);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 creditClassfc.getId()+KafkaMapleEventType.CREDITCLASSFC + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(creditClassfc.getId());
		eventBus.post(kafkaMapleEvent);

		return creditClassfc;
	}

	@Override
	@Transactional
	public DebitClass saveDebitClass(DebitClass debitClass, String branchCode) {

		debitClass = debitClassRepository.save(debitClass);
		String destination = "SERVER";
		/*
		 * String jsonStr = "";
		 * 
		 * ObjectMapper Obj = new ObjectMapper(); try { // Converting the Java object
		 * into a JSON string jsonStr = Obj.writeValueAsString(debitClass);
		 * logger.info("Converted to JSON String: {} ", jsonStr); } catch (IOException
		 * e) { System.out.println(jsonStr); logger.info("Exception : {} ", e); }
		 * KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		 * kafkaMapleEvent.setLibraryEventId(partitionKey);
		 * kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.DEBITCLASS);
		 * kafkaMapleEvent.setPayLoad(jsonStr);
		 * 
		 * kafkaMapleEvent.setSource(branchCode);
		 * kafkaMapleEvent.setDestination(destination); String sourceFileName =
		 * kafkaMapleEvent.getDestination() + name_differentiator +
		 * debitClass.getId()+KafkaMapleEventType.DEBITCLASS + file_sufix;
		 * kafkaMapleEvent.setSourceFile(sourceFileName);
		 * 
		 * kafkaMapleEvent.setVoucherNumber(debitClass.getId());
		 * eventBus.post(kafkaMapleEvent);
		 */
		
		return debitClass;
	}

	@Override
	public DebitClassfc saveDebitClassfc(DebitClassfc debitClassfc, String branchCode) {
		String destination = "SERVER";

		debitClassfc = debitClassfcRepository.save(debitClassfc);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(debitClassfc);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.DEBITCLASSFC);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 debitClassfc.getId()+KafkaMapleEventType.DEBITCLASSFC + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(debitClassfc.getId());
		eventBus.post(kafkaMapleEvent);

		return debitClassfc;
	}

	@Override
	public InventoryLedgerMst saveInventoryLedgerMst(InventoryLedgerMst inventoryLedgerMst, String branchCode) {
		String destination = "SERVER";

		inventoryLedgerMst = inventoryLedgerMstRepository.save(inventoryLedgerMst);

		/*
		 * String jsonStr = "";
		 * 
		 * ObjectMapper Obj = new ObjectMapper(); try { // Converting the Java object
		 * into a JSON string jsonStr = Obj.writeValueAsString(inventoryLedgerMst);
		 * logger.info("Converted to JSON String: {} ", jsonStr); } catch (IOException
		 * e) { System.out.println(jsonStr); logger.info("Exception : {} ", e); }
		 * KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		 * kafkaMapleEvent.setLibraryEventId(partitionKey);
		 * kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.INVENTORYLEDGERMST);
		 * kafkaMapleEvent.setPayLoad(jsonStr);
		 * 
		 * kafkaMapleEvent.setSource(branchCode);
		 * kafkaMapleEvent.setDestination(destination); String sourceFileName =
		 * kafkaMapleEvent.getDestination() + name_differentiator +
		 * inventoryLedgerMst.getId()+KafkaMapleEventType.INVENTORYLEDGERMST +
		 * file_sufix; kafkaMapleEvent.setSourceFile(sourceFileName);
		 * 
		 * kafkaMapleEvent.setVoucherNumber(inventoryLedgerMst.getId());
		 * eventBus.post(kafkaMapleEvent);
		 */

		return inventoryLedgerMst;
	}

	@Override
	@Transactional
	public LedgerClass saveLedgerClass(LedgerClass ledgerClass, String branchCode) {
		

		ledgerClass = ledgerClassRepository.save(ledgerClass);
		
		String destination = "SERVER";
		/*
		 * String jsonStr = "";
		 * 
		 * ObjectMapper Obj = new ObjectMapper(); try { // Converting the Java object
		 * into a JSON string jsonStr = Obj.writeValueAsString(ledgerClass);
		 * logger.info("Converted to JSON String: {} ", jsonStr); } catch (IOException
		 * e) { System.out.println(jsonStr); logger.info("Exception : {} ", e); }
		 * KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		 * kafkaMapleEvent.setLibraryEventId(partitionKey);
		 * kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.LEDGERCLASS);
		 * kafkaMapleEvent.setPayLoad(jsonStr);
		 * 
		 * kafkaMapleEvent.setSource(branchCode);
		 * kafkaMapleEvent.setDestination(destination); String sourceFileName =
		 * kafkaMapleEvent.getDestination() + name_differentiator +
		 * ledgerClass.getId()+KafkaMapleEventType.LEDGERCLASS + file_sufix;
		 * kafkaMapleEvent.setSourceFile(sourceFileName);
		 * 
		 * kafkaMapleEvent.setVoucherNumber(ledgerClass.getId());
		 * eventBus.post(kafkaMapleEvent);
		 */

		return ledgerClass;
	}

	@Override
	public LedgerClassfc saveLedgerClassfc(LedgerClassfc ledgerClassfc, String branchCode) {
		String destination = "SERVER";

		ledgerClassfc = ledgerClassfcRepository.save(ledgerClassfc);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(ledgerClassfc);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.LEDGERCLASSFC);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 ledgerClassfc.getId()+KafkaMapleEventType.LEDGERCLASSFC + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(ledgerClassfc.getId());
		eventBus.post(kafkaMapleEvent);

		return ledgerClassfc;
	}

	@Override
	public LinkedAccounts saveLinkedAccounts(LinkedAccounts linkedAccounts, String branchCode) {
		String destination = "SERVER";

		linkedAccounts = linkedAccountsRepository.save(linkedAccounts);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(linkedAccounts);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.LINKEDACCOUNTS);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 linkedAccounts.getId()+KafkaMapleEventType.LINKEDACCOUNTS + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(linkedAccounts.getId());
		eventBus.post(kafkaMapleEvent);

		return linkedAccounts;
	}

	@Override
	@Transactional
	public OpeningBalanceConfigMst saveOpeningBalanceConfigMst(OpeningBalanceConfigMst openingBalanceConfigMst,
			String branchCode) {

		openingBalanceConfigMst = openingBalanceConfigMstRepository.save(openingBalanceConfigMst);


		return openingBalanceConfigMst;
	}

	@Override
	@Transactional
	public OpeningBalanceMst saveOpeningBalanceMst(OpeningBalanceMst openingBalanceMst, String branchCode) {


		openingBalanceMst = openingBalanceMstRepository.save(openingBalanceMst);


		return openingBalanceMst;
	}

	@Override
	@Transactional
	public StockTransferOutHdr saveStockTransferOutHdrRep(StockTransferOutHdr stockTransferOutHdr, String branchCode,
			String toBranch) {
		
		
		


		stockTransferOutHdr = stockTransferOutHdrRepository.save(stockTransferOutHdr);

				return stockTransferOutHdr;
	}
	@Override
	public StockTransferOutHdr saveStockTransferOutHdr(StockTransferOutHdr stockTransferOutHdr, String branchCode,
			String toBranch) {
		
//		publishStockTransferToBranch("SERVER",stockTransferOutHdr,branchCode);
		publishStockTransferToBranch(toBranch,stockTransferOutHdr,branchCode);
		
				return stockTransferOutHdr;
	}

	private void publishStockTransferToServer(String destination, StockTransferOutHdr stockTransferOutHdr, String sourcBranch) {

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(stockTransferOutHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.STOCKTRANSFEROUTHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(sourcBranch);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName1 =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 stockTransferOutHdr.getId()+KafkaMapleEventType.STOCKTRANSFEROUTHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName1);

		kafkaMapleEvent.setVoucherNumber(stockTransferOutHdr.getId());
		eventBus.post(kafkaMapleEvent);
		 
		
  

		// Sending detail records

		List<StockTransferOutDtl> stockDtlArray = stockTransferOutDtlRepository
				.findByStockTransferOutHdrId(stockTransferOutHdr.getId());

		for (StockTransferOutDtl stockTransferOutDtl : stockDtlArray) {

			try {
				stockTransferOutDtl.setStockTransferOutHdr(stockTransferOutHdr);		// Converting the Java object into a JSON string
				jsonStr = Obj.writeValueAsString(stockTransferOutDtl);
				logger.info("Converted to JSON String: {} ", jsonStr);
			} catch (IOException e) {
				System.out.println(jsonStr);
				logger.info("Exception : {} ", e);
			}

			kafkaMapleEvent = new KafkaMapleEvent();
			kafkaMapleEvent.setLibraryEventId(partitionKey);
			kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.STOCKTRANSFEROUTDTL);
			kafkaMapleEvent.setPayLoad(jsonStr);

			kafkaMapleEvent.setSource(sourcBranch);
			kafkaMapleEvent.setDestination(destination);
			String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
					 stockTransferOutDtl.getId()+KafkaMapleEventType.STOCKTRANSFEROUTDTL + file_sufix;
			kafkaMapleEvent.setSourceFile(sourceFileName);

			kafkaMapleEvent.setVoucherNumber(stockTransferOutDtl.getId());
			eventBus.post(kafkaMapleEvent);

			 

		}


		
	}
	
	private void publishStockTransferToBranch(String destination, StockTransferOutHdr stockTransferOutHdr, String sourcBranch) {

		String jsonStr = "";
	 
		
		
		ArrayList<StockTransferInDtl> stockTransferInDtlsArray = new ArrayList();
		

		ObjectMapper Obj = new ObjectMapper();
		
		StockTransferInHdr stockTransferInHdrs= new StockTransferInHdr();
		stockTransferInHdrs.setCompanyMst(stockTransferOutHdr.getCompanyMst());
		stockTransferInHdrs.setFromBranch(destination);
		stockTransferInHdrs.setBranchCode(sourcBranch);
		stockTransferInHdrs.setIntentNumber(stockTransferOutHdr.getIntentNumber());
		stockTransferInHdrs.setInVoucherDate(stockTransferOutHdr.getInVoucherDate());
		stockTransferInHdrs.setInVoucherNumber(stockTransferOutHdr.getInVoucherNumber());
		stockTransferInHdrs.setStatusAcceptStock(stockTransferOutHdr.getStatus());
		stockTransferInHdrs.setStockTransferOutHdrId(stockTransferOutHdr.getId());
		stockTransferInHdrs.setVoucherDate(stockTransferOutHdr.getInVoucherDate());
	 
		// Sending detail records

		List<StockTransferOutDtl> stockDtlArray = stockTransferOutDtlRepository
				.findByStockTransferOutHdrId(stockTransferOutHdr.getId());

//		for (StockTransferOutDtl stockTransferOutDtl : stockDtlArray) {
//
//		 
//
//				StockTransferInDtl stockTransferInDtls= new StockTransferInDtl();
//				stockTransferInDtls.setAmount(stockTransferOutDtl.getAmount());
//				stockTransferInDtls.setBarcode(stockTransferOutDtl.getBarcode());
//				stockTransferInDtls.setBatch(stockTransferOutDtl.getBatch());
//				stockTransferInDtls.setCompanyMst(stockTransferOutDtl.getCompanyMst());
//				stockTransferInDtls.setExpiryDate(stockTransferOutDtl.getExpiryDate());
//				stockTransferInDtls.setId(stockTransferOutDtl.getId());
//				stockTransferInDtls.setItemCode(stockTransferOutDtl.getItemCode());
//				stockTransferInDtls.setItemId(stockTransferOutDtl.getItemId());
//				stockTransferInDtls.setMrp(stockTransferOutDtl.getMrp());
//				stockTransferInDtls.setQty(stockTransferOutDtl.getQty());
//				stockTransferInDtls.setRate(stockTransferOutDtl.getRate());
//				stockTransferInDtls.setTaxRate(stockTransferOutDtl.getTaxRate());
//				stockTransferInDtls.setStockTransferInHdr(stockTransferInHdrs);
//				
//				stockTransferInDtlsArray.add(stockTransferInDtls);
//		  
//
//		}
		
		//stockDtlArray;
		
		
		StockTransferMessageEntity stockTransferMessageEntity = new StockTransferMessageEntity();
		
		stockTransferMessageEntity.getStockTransferOutDtlList().addAll(stockDtlArray);
		stockTransferMessageEntity.setStockTransferOutHdr(stockTransferOutHdr);
	 
		try {
			jsonStr = Obj.writeValueAsString(stockTransferMessageEntity);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.STOCKTRANSFEROUTDTLARRAY);
		kafkaMapleEvent.setPayLoad(jsonStr);
		kafkaMapleEvent.setSource(sourcBranch);
		kafkaMapleEvent.setDestination(destination);
		
		
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 stockTransferOutHdr.getId()+KafkaMapleEventType.STOCKTRANSFEROUTDTLARRAY + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(stockTransferOutHdr.getId());
		
		eventBus.post(kafkaMapleEvent);
		logger.info("Converted to JSON String: {} ", jsonStr);
	}

	@Override
	public StockTransferOutDtl saveStockTransferOutDtl(StockTransferOutDtl stockTransferOutDtl, String branchCode,
			String toBranch) {
		

		stockTransferOutDtl = stockTransferOutDtlRepository.save(stockTransferOutDtl);

	 

		return stockTransferOutDtl;
	}

	@Override
	@Transactional
	public StockTransferInHdr saveStockTransferInHdr(StockTransferInHdr stockTransferInHdr, String branchCode,
			String fromBranch) {
		

		stockTransferInHdr = stockTransferInHdrRepository.save(stockTransferInHdr);

	
		return stockTransferInHdr;
	}

	@Override
	@Transactional
	public StockTransferInDtl saveStockTransferInDtl(StockTransferInDtl stockTransferInDtl, String branchCode,
			String fromBranch) {
		

		stockTransferInDtl = stockTransferInDtlRepository.save(stockTransferInDtl);

		return stockTransferInDtl;
	}

	@Override
	public AccountHeads saveAccountHeads(AccountHeads accountHeads, String branchCode) {
		String destination = "";

		accountHeads = accountHeadsRepository.save(accountHeads);

		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
			
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(accountHeads);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ACCOUNTHEADS);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 accountHeads.getId()+KafkaMapleEventType.ACCOUNTHEADS + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(accountHeads.getId());
		eventBus.post(kafkaMapleEvent);
			}
		}
		return accountHeads;
	}

	@Override
	@Transactional
	public AccountPayable saveAccountPayable(AccountPayable accountPayable, String branchCode) {
		String destination = "SERVER";

		accountPayable = accountPayableRepository.save(accountPayable);

		/*
		 * String jsonStr = "";
		 * 
		 * ObjectMapper Obj = new ObjectMapper(); try { // Converting the Java object
		 * into a JSON string jsonStr = Obj.writeValueAsString(accountPayable);
		 * logger.info("Converted to JSON String: {} ", jsonStr); } catch (IOException
		 * e) { System.out.println(jsonStr); logger.info("Exception : {} ", e); }
		 * KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		 * kafkaMapleEvent.setLibraryEventId(partitionKey);
		 * kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ACCOUNTPAYABLE);
		 * kafkaMapleEvent.setPayLoad(jsonStr);
		 * 
		 * kafkaMapleEvent.setSource(branchCode);
		 * kafkaMapleEvent.setDestination(destination); String sourceFileName =
		 * kafkaMapleEvent.getDestination() + name_differentiator +
		 * accountPayable.getId()+KafkaMapleEventType.ACCOUNTPAYABLE + file_sufix;
		 * kafkaMapleEvent.setSourceFile(sourceFileName);
		 * 
		 * kafkaMapleEvent.setVoucherNumber(accountPayable.getId());
		 * eventBus.post(kafkaMapleEvent);
		 */

		return accountPayable;
	}

	@Override
	@Transactional
	public AccountReceivable saveAccountReceivable(AccountReceivable accountReceivable, String branchCode) {
		String destination = "SERVER";

		accountReceivable = accountReceivableRepository.save(accountReceivable);
		/*
		 * String jsonStr = "";
		 * 
		 * ObjectMapper Obj = new ObjectMapper(); try { // Converting the Java object
		 * into a JSON string jsonStr = Obj.writeValueAsString(accountReceivable);
		 * logger.info("Converted to JSON String: {} ", jsonStr); } catch (IOException
		 * e) { System.out.println(jsonStr); logger.info("Exception : {} ", e); }
		 * KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		 * kafkaMapleEvent.setLibraryEventId(partitionKey);
		 * kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ACCOUNTRECEIVABLE);
		 * kafkaMapleEvent.setPayLoad(jsonStr);
		 * 
		 * kafkaMapleEvent.setSource(branchCode);
		 * kafkaMapleEvent.setDestination(destination); String sourceFileName =
		 * kafkaMapleEvent.getDestination() + name_differentiator +
		 * accountReceivable.getId()+KafkaMapleEventType.ACCOUNTRECEIVABLE + file_sufix;
		 * kafkaMapleEvent.setSourceFile(sourceFileName);
		 * 
		 * kafkaMapleEvent.setVoucherNumber(accountReceivable.getId());
		 * eventBus.post(kafkaMapleEvent);
		 */

		return accountReceivable;
	}

	@Override
	public AdditionalExpense saveAdditionalExpense(AdditionalExpense additionalExpense, String branchCode) {
		String destination = "SERVER";

		additionalExpense = additionalExpenseRepository.save(additionalExpense);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(additionalExpense);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ADDITIONALEXPENSE);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 additionalExpense.getId()+KafkaMapleEventType.ADDITIONALEXPENSE + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(additionalExpense.getId());
		eventBus.post(kafkaMapleEvent);



		return additionalExpense;
	}

	@Override
	public BatchPriceDefinition saveBatchPriceDefinition(BatchPriceDefinition batchPriceDefinition, String branchCode) {
		String destination = "";

		batchPriceDefinition = batchPriceDefinitionRepository.save(batchPriceDefinition);

		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
		
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(batchPriceDefinition);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.BATCHPRICEDEFINITION);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 batchPriceDefinition.getId()+KafkaMapleEventType.BATCHPRICEDEFINITION + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(batchPriceDefinition.getId());
		eventBus.post(kafkaMapleEvent);
			}
		}
		return batchPriceDefinition;
	}

	@Override
	public BrandMst saveBrandMst(BrandMst brandMst, String branchCode) {
		String destination = "";

		brandMst = brandMstRepository.save(brandMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(brandMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.BRANDMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 brandMst.getId()+KafkaMapleEventType.BRANDMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(brandMst.getId());
		eventBus.post(kafkaMapleEvent);
			}
		}

		return brandMst;
	}

	@Override
	public ConsumptionDtl saveConsumptionDtl(ConsumptionDtl consumptionDtl, String branchCode) {
		String destination = "SERVER";

		consumptionDtl = consumptionDtlRepository.save(consumptionDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(consumptionDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.CONSUMPTIONDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 consumptionDtl.getId()+KafkaMapleEventType.CONSUMPTIONDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(consumptionDtl.getId());
		eventBus.post(kafkaMapleEvent);

		return consumptionDtl;
	}

	@Override
	public ConsumptionReasonMst saveConsumptionReasonMst(ConsumptionReasonMst consumptionReasonMst, String branchCode) {
		String destination = "";

		consumptionReasonMst = consumptionReasonMstRepository.save(consumptionReasonMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(consumptionReasonMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.CONSUMPTIONREASONMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 consumptionReasonMst.getId()+KafkaMapleEventType.CONSUMPTIONREASONMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(consumptionReasonMst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return consumptionReasonMst;
	}

	@Override
	public DamageDtl saveDamageDtl(DamageDtl damageDtl, String branchCode) {
		String destination = "SERVER";

		damageDtl = damageDtlRepository.save(damageDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(damageDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.DAMAGEDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 damageDtl.getId()+KafkaMapleEventType.DAMAGEDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(damageDtl.getId());
		eventBus.post(kafkaMapleEvent);

		return damageDtl;
	}

	@Override
	public DayBook saveDayBook(DayBook dayBook, String branchCode) {
		String destination = "SERVER";

		dayBook = dayBookRepository.save(dayBook);

		/*
		 * String jsonStr = "";
		 * 
		 * ObjectMapper Obj = new ObjectMapper(); try { // Converting the Java object
		 * into a JSON string jsonStr = Obj.writeValueAsString(dayBook);
		 * logger.info("Converted to JSON String: {} ", jsonStr); } catch (IOException
		 * e) { System.out.println(jsonStr); logger.info("Exception : {} ", e); }
		 * KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		 * kafkaMapleEvent.setLibraryEventId(partitionKey);
		 * kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.DAYBOOK);
		 * kafkaMapleEvent.setPayLoad(jsonStr);
		 * 
		 * 
		 * kafkaMapleEvent.setSource(branchCode);
		 * kafkaMapleEvent.setDestination(destination); String sourceFileName =
		 * kafkaMapleEvent.getDestination() + name_differentiator +
		 * dayBook.getId()+KafkaMapleEventType.DAYBOOK + file_sufix;
		 * kafkaMapleEvent.setSourceFile(sourceFileName);
		 * 
		 * kafkaMapleEvent.setVoucherNumber(dayBook.getId());
		 * eventBus.post(kafkaMapleEvent);
		 */


		return dayBook;
	}

	@Override
	public DayEndClosureHdr saveDayEndClosureHdr(DayEndClosureHdr dayEndClosureHdr, String branchCode) {
		String destination = "SERVER";

		dayEndClosureHdr = dayEndClosureHdrRepository.save(dayEndClosureHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(dayEndClosureHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.DAYENDCLOSUREHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

	

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 dayEndClosureHdr.getId()+KafkaMapleEventType.DAYENDCLOSUREHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(dayEndClosureHdr.getId());
		eventBus.post(kafkaMapleEvent);

		return dayEndClosureHdr;
	}

	@Override
	public DayEndClosureDtl saveDayEndClosureDtl(DayEndClosureDtl dayEndClosureDtl, String branchCode) {
		String destination = "SERVER";

		dayEndClosureDtl = dayEndClosureDtlRepository.save(dayEndClosureDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(dayEndClosureDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.DAYENDCLOSUREDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 dayEndClosureDtl.getId()+KafkaMapleEventType.DAYENDCLOSUREDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(dayEndClosureDtl.getId());
		eventBus.post(kafkaMapleEvent);

		return dayEndClosureDtl;
	}

	@Override
	public DayEndReportStore saveDayEndReportStore(DayEndReportStore dayEndReportStore, String branchCode) {
		String destination = "SERVER";

		dayEndReportStore = dayEndReportStoreRepository.save(dayEndReportStore);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(dayEndReportStore);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.DAYENDREPORTSTORE);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 dayEndReportStore.getId()+KafkaMapleEventType.DAYENDREPORTSTORE + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(dayEndReportStore.getId());
		eventBus.post(kafkaMapleEvent);

		return dayEndReportStore;
	}

	@Override
	public DeliveryBoyMst saveDeliveryBoyMst(DeliveryBoyMst deliveryBoyMst, String branchCode) {
		String destination = "SERVER";

		deliveryBoyMst = deliveryBoyMstRepository.save(deliveryBoyMst);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(deliveryBoyMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.DELIVERYBOYMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 deliveryBoyMst.getId()+KafkaMapleEventType.DELIVERYBOYMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(deliveryBoyMst.getId());
		eventBus.post(kafkaMapleEvent);

		return deliveryBoyMst;
	}

	@Override
	public GoodReceiveNoteDtl saveGoodReceiveNoteDtl(GoodReceiveNoteDtl goodReceiveNoteDtl, String branchCode) {
		String destination = "SERVER";

		goodReceiveNoteDtl = goodReceiveNoteDtlRepository.save(goodReceiveNoteDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(goodReceiveNoteDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.GOODRECEIVENOTEDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 goodReceiveNoteDtl.getId()+KafkaMapleEventType.GOODRECEIVENOTEDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(goodReceiveNoteDtl.getId());
		eventBus.post(kafkaMapleEvent);

		return goodReceiveNoteDtl;
	}

	@Override
	public GoodReceiveNoteHdr saveGoodReceiveNoteHr(GoodReceiveNoteHdr goodReceiveNoteHdr, String branchCode) {
		String destination = "SERVER";

		goodReceiveNoteHdr = goodReceiveNoteHdrRepository.save(goodReceiveNoteHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(goodReceiveNoteHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.GOODRECEIVENOTEHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 goodReceiveNoteHdr.getId()+KafkaMapleEventType.GOODRECEIVENOTEHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(goodReceiveNoteHdr.getId());
		eventBus.post(kafkaMapleEvent);



		return goodReceiveNoteHdr;
	}

	@Override
	public GroupMst saveGroupMst(GroupMst groupMst, String branchCode) {
		String destination = "SERVER";

		groupMst = groupMstRepository.save(groupMst);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(groupMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.GROUPMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 groupMst.getId()+KafkaMapleEventType.GROUPMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(groupMst.getId());
		eventBus.post(kafkaMapleEvent);



		return groupMst;
	}

	@Override
	public GroupPermissionMst saveGroupPermissionMst(GroupPermissionMst groupPermissionMst, String branchCode) {
		String destination = "SERVER";

		groupPermissionMst = groupPermissionMstRepository.save(groupPermissionMst);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(groupPermissionMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.GROUPPERMISSIONMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 groupPermissionMst.getId()+KafkaMapleEventType.GROUPPERMISSIONMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(groupPermissionMst.getId());
		eventBus.post(kafkaMapleEvent);



		return groupPermissionMst;
	}

	@Override
	public IntentInHdr saveIntentInHdr(IntentInHdr intentInHdr, String branchCode) {
		String destination = "SERVER";

		intentInHdr = intentInHdrRepository.save(intentInHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(intentInHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.INTENTINHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

	
		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 intentInHdr.getId()+KafkaMapleEventType.INTENTINHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(intentInHdr.getId());
		eventBus.post(kafkaMapleEvent);



		return intentInHdr;
	}

	@Override
	public IntentInDtl saveIntentInDtl(IntentInDtl intentInDtl, String branchCode) {
		String destination = "SERVER";

		intentInDtl = intentInDtlRepository.save(intentInDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(intentInDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.INTENTINDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 intentInDtl.getId()+KafkaMapleEventType.INTENTINDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(intentInDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return intentInDtl;
	}

	@Override
	@Transactional
	public ItemBatchExpiryDtl saveItemBatchExpiryDtl(ItemBatchExpiryDtl itemBatchExpiryDtl, String branchCode) {
		

		itemBatchExpiryDtl = itemBatchExpiryDtlRepository.save(itemBatchExpiryDtl);



		return itemBatchExpiryDtl;
	}

	@Override
	public KitDefenitionDtl saveKitDefenitionDtl(KitDefenitionDtl kitDefenitionDtl, String branchCode) {
		String destination = "";

		kitDefenitionDtl = kitDefenitionDtlRepository.save(kitDefenitionDtl);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(kitDefenitionDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.KITDEFENITIONDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 kitDefenitionDtl.getId()+KafkaMapleEventType.KITDEFENITIONDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(kitDefenitionDtl.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return kitDefenitionDtl;
	}

	@Override
	public KitDefinitionMst saveKitDefinitionMst(KitDefinitionMst kitDefinitionMst, String branchCode) {
		String destination = "";

		kitDefinitionMst = kitDefinitionMstRepository.save(kitDefinitionMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(kitDefinitionMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.KITDEFINITIONMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 kitDefinitionMst.getId()+KafkaMapleEventType.KITDEFINITIONMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(kitDefinitionMst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return kitDefinitionMst;
	}

	@Override
	public AddKotTable saveAddKotTable(AddKotTable addKotTable, String branchCode) {
		String destination = "SERVER";

		addKotTable = addKotTableRepository.save(addKotTable);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(addKotTable);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ADDKOTTABLE);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 addKotTable.getId()+KafkaMapleEventType.ADDKOTTABLE + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(addKotTable.getId());
		eventBus.post(kafkaMapleEvent);



		return addKotTable;
	}

	@Override
	public AddKotWaiter saveAddKotWaiter(AddKotWaiter addKotWaiter, String branchCode) {
		String destination = "SERVER";

		addKotWaiter = addKotWaiterRepository.save(addKotWaiter);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(addKotWaiter);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ADDKOTWAITER);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 addKotWaiter.getId()+KafkaMapleEventType.ADDKOTWAITER + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(addKotWaiter.getId());
		eventBus.post(kafkaMapleEvent);



		return addKotWaiter;
	}

	@Override
	@Transactional
	public LocalCustomerMst saveLocalCustomerMst(LocalCustomerMst localCustomerMst, String branchCode) {
		

		localCustomerMst = localCustomerMstRepository.save(localCustomerMst);

	

		return localCustomerMst;
	}

	@Override
	public LocalCustomerDtl saveLocalCustomerDtl(LocalCustomerDtl localCustomerDtl, String branchCode) {
		String destination = "";

		localCustomerDtl = localCustomerDtlRepository.save(localCustomerDtl);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(localCustomerDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.LOCALCUSTOMERDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 localCustomerDtl.getId()+KafkaMapleEventType.LOCALCUSTOMERDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(localCustomerDtl.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return localCustomerDtl;
	}

	@Override
	public LocationMst saveLocationMst(LocationMst locationMst, String branchCode) {
		String destination = "";

		locationMst = locationMstRepository.save(locationMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(locationMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.LOCATIONMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 locationMst.getId()+KafkaMapleEventType.LOCATIONMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(locationMst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}
		

		return locationMst;
	}

	@Override
	public MultiUnitMst saveMultiUnitMst(MultiUnitMst multiUnitMst, String branchCode) {
		String destination = "";

		multiUnitMst = multiUnitMstRepository.save(multiUnitMst);


    List<BranchMst> branchList = branchMstRepository.findAll();
    for(BranchMst branch : branchList) {
	if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
	destination = branch.getBranchCode();

	String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(multiUnitMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.MULTIUNITMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		
		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 multiUnitMst.getId()+KafkaMapleEventType.MULTIUNITMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(multiUnitMst.getId());
		eventBus.post(kafkaMapleEvent);

				}
    	}
    

		return multiUnitMst;
	}

	@Override
	public OrderTakerMst saveOrderTakerMst(OrderTakerMst orderTakerMst, String branchCode) {
		String destination = "SERVER";

		orderTakerMst = orderTakerMstRepository.save(orderTakerMst);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(orderTakerMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ORDERTAKERMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

	

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 orderTakerMst.getId()+KafkaMapleEventType.ORDERTAKERMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(orderTakerMst.getId());
		eventBus.post(kafkaMapleEvent);



		return orderTakerMst;
	}

	@Override
	public PDCPayment savePDCPayment(PDCPayment PDCPayment, String branchCode) {
		String destination = "SERVER";

		PDCPayment = PDCPaymentRepository.save(PDCPayment);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(PDCPayment);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PDCPAYMENT);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 PDCPayment.getId()+KafkaMapleEventType.PDCPAYMENT + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(PDCPayment.getId());
		eventBus.post(kafkaMapleEvent);



		return PDCPayment;
	}

	@Override
	public PDCReceipts savePDCReceipts(PDCReceipts PDCReceipts, String branchCode) {
		String destination = "SERVER";

		PDCReceipts = PDCReceiptsRepository.save(PDCReceipts);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(PDCReceipts);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PDCRECEIPTS);
		kafkaMapleEvent.setPayLoad(jsonStr);

	

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 PDCReceipts.getId()+KafkaMapleEventType.PDCRECEIPTS + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(PDCReceipts.getId());
		eventBus.post(kafkaMapleEvent);



		return PDCReceipts;
	}

	@Override
	public PDCReconcileHdr savePDCReconcileHdr(PDCReconcileHdr PDCReconcileHdr, String branchCode) {
		String destination = "SERVER";

		PDCReconcileHdr = PDCReconcileHdrRepository.save(PDCReconcileHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(PDCReconcileHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PDCRECONCILEHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

	

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 PDCReconcileHdr.getId()+KafkaMapleEventType.PDCRECONCILEHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(PDCReconcileHdr.getId());
		eventBus.post(kafkaMapleEvent);


		return PDCReconcileHdr;
	}

	@Override
	public PDCReconcileDtl savePDCReconcileDtl(PDCReconcileDtl PDCReconcileDtl, String branchCode) {
		String destination = "SERVER";

		PDCReconcileDtl = pDCReconcileDtlRepository.save(PDCReconcileDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(PDCReconcileDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PDCRECONCILEDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

	

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 PDCReconcileDtl.getId()+KafkaMapleEventType.PDCRECONCILEDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(PDCReconcileDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return PDCReconcileDtl;
	}

	@Override
	public DamageHdr saveDamageHdr(DamageHdr damageHdr, String branchCode) {
		String destination = "SERVER";

		damageHdr = damageHdrRepository.save(damageHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(damageHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.DAMAGEHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 damageHdr.getId()+KafkaMapleEventType.DAMAGEHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(damageHdr.getId());
		eventBus.post(kafkaMapleEvent);


		return damageHdr;
	}

	@Override
	public PriceDefenitionMst savePriceDefenitionMst(PriceDefenitionMst priceDefenitionMst, String branchCode) {
		String destination = "";

		priceDefenitionMst = priceDefenitionMstRepository.save(priceDefenitionMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(priceDefenitionMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PRICEDEFENITIONMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 priceDefenitionMst.getId()+KafkaMapleEventType.PRICEDEFENITIONMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(priceDefenitionMst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return priceDefenitionMst;
	}

	@Override
	public PriceDefinition savePriceDefinition(PriceDefinition priceDefinition, String branchCode) {


	
		publishPriceDefinitionToServer("SERVER",priceDefinition,branchCode);
		publishPriceDefinitionToBranch(priceDefinition,branchCode);
		
				return priceDefinition;
		
		
		
	}

	private void publishPriceDefinitionToBranch(PriceDefinition priceDefinition, String branchCode) {
		String destination = "";

		priceDefinition = priceDefinitionRepository.save(priceDefinition);

		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
		
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(priceDefinition);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PRICEDEFINITION);
		kafkaMapleEvent.setPayLoad(jsonStr);

	

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 priceDefinition.getId()+KafkaMapleEventType.PRICEDEFINITION + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(priceDefinition.getId());
		eventBus.post(kafkaMapleEvent);


			}
		}
		
	}

	private void publishPriceDefinitionToServer(String destination, PriceDefinition priceDefinition, String branchCode) {
		destination = "SERVER";

		priceDefinition = priceDefinitionRepository.save(priceDefinition);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(priceDefinition);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PRICEDEFINITION);
		kafkaMapleEvent.setPayLoad(jsonStr);

	

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				priceDefinition.getId()+KafkaMapleEventType.PRICEDEFINITION + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(priceDefinition.getId());
		eventBus.post(kafkaMapleEvent);
		
	}

	@Override
	public ProcessMst saveProcessMst(ProcessMst processMst, String branchCode) {
		String destination = "";

		processMst = processMstRepository.save(processMst);

		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(processMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PROCESSMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

	

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 processMst.getId()+KafkaMapleEventType.PROCESSMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(processMst.getId());
		eventBus.post(kafkaMapleEvent);


			}
			}
		return processMst;
	}

	@Override
	public ProcessPermissionMst saveProcessPermissionMst(ProcessPermissionMst processPermissionMst, String branchCode) {
		String destination = "";

		processPermissionMst = processPermissionMstRepository.save(processPermissionMst);

		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(processPermissionMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PROCESSPERMISSIONMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

	

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 processPermissionMst.getId()+KafkaMapleEventType.PROCESSPERMISSIONMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(processPermissionMst.getId());
		eventBus.post(kafkaMapleEvent);


			}
			}
		return processPermissionMst;
	}

	@Override
	public ProductConversionConfigMst saveProductConversionConfigMst(
			ProductConversionConfigMst productConversionConfigMst, String branchCode) {
		String destination = "";

		productConversionConfigMst = productConversionConfigMstRepository.save(productConversionConfigMst);

		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
			
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(productConversionConfigMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PRODUCTCONVERSIONCONFIGMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

	
		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 productConversionConfigMst.getId()+KafkaMapleEventType.PRODUCTCONVERSIONCONFIGMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(productConversionConfigMst.getId());
		eventBus.post(kafkaMapleEvent);


			}
		}
		return productConversionConfigMst;
	}

	@Override
	public ProductConversionDtl saveProductConversionDtl(ProductConversionDtl productConversionDtl, String branchCode) {
		String destination = "";

		productConversionDtl = productConversionDtlRepository.save(productConversionDtl);

		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(productConversionDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PRODUCTCONVERSIONDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 productConversionDtl.getId()+KafkaMapleEventType.PRODUCTCONVERSIONDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(productConversionDtl.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return productConversionDtl;
	}

	@Override
	public ProductMst saveProductMst(ProductMst productMst, String branchCode) {
		String destination = "";

		productMst = productMstRepository.save(productMst);

		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(productMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PRODUCTMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

	
		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 productMst.getId()+KafkaMapleEventType.PRODUCTMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(productMst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}
		return productMst;
	}

	@Override
	public PurchaseOrderDtl savePurchaseOrderDtl(PurchaseOrderDtl purchaseOrderDtl, String branchCode) {
		String destination = "SERVER";

		purchaseOrderDtl = purchaseOrderDtlRepository.save(purchaseOrderDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(purchaseOrderDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PURCHASEORDERDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 purchaseOrderDtl.getId()+KafkaMapleEventType.PURCHASEORDERDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(purchaseOrderDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return purchaseOrderDtl;
	}

	@Override
	public PurchaseOrderHdr savePurchaseOrderHdr(PurchaseOrderHdr purchaseOrderHdr, String branchCode) {
		String destination = "SERVER";

		purchaseOrderHdr = purchaseOrderHdrRepository.save(purchaseOrderHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(purchaseOrderHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PURCHASEORDERHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

	

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 purchaseOrderHdr.getId()+KafkaMapleEventType.PURCHASEORDERHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(purchaseOrderHdr.getId());
		eventBus.post(kafkaMapleEvent);



		return purchaseOrderHdr;
	}

	@Override
	public RawMaterialIssueDtl saveRawMaterialIssueDtl(RawMaterialIssueDtl rawMaterialIssueDtl, String branchCode) {
		String destination = "SERVER";

		rawMaterialIssueDtl = rawMaterialIssueDtlRepository.save(rawMaterialIssueDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(rawMaterialIssueDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.RAWMATERIALISSUEDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);



		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 rawMaterialIssueDtl.getId()+KafkaMapleEventType.RAWMATERIALISSUEDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(rawMaterialIssueDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return rawMaterialIssueDtl;
	}

	@Override
	public RawMaterialIssueHdr saveRawMaterialIssueHdr(RawMaterialIssueHdr rawMaterialIssueHdr, String branchCode) {
		String destination = "SERVER";

		rawMaterialIssueHdr = rawMaterialIssueHdrRepository.save(rawMaterialIssueHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(rawMaterialIssueHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.RAWMATERIALISSUEHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);



		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 rawMaterialIssueHdr.getId()+KafkaMapleEventType.RAWMATERIALISSUEHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(rawMaterialIssueHdr.getId());
		eventBus.post(kafkaMapleEvent);


		return rawMaterialIssueHdr;
	}

	@Override
	public RawMaterialReturnHdr saveRawMaterialReturnHdr(RawMaterialReturnHdr rawMaterialReturnHdr, String branchCode) {
		String destination = "SERVER";

		rawMaterialReturnHdr = rawMaterialReturnHdrRepository.save(rawMaterialReturnHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(rawMaterialReturnHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.RAWMATERIALRETURNHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);



		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 rawMaterialReturnHdr.getId()+KafkaMapleEventType.RAWMATERIALRETURNHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(rawMaterialReturnHdr.getId());
		eventBus.post(kafkaMapleEvent);



		return rawMaterialReturnHdr;
	}

	@Override
	public RawMaterialReturnDtl saveRawMaterialReturnDtl(RawMaterialReturnDtl rawMaterialReturnDtl, String branchCode) {
		String destination = "SERVER";

		rawMaterialReturnDtl = rawMaterialReturnDtlRepository.save(rawMaterialReturnDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(rawMaterialReturnDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.RAWMATERIALRETURNDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);


		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 rawMaterialReturnDtl.getId()+KafkaMapleEventType.RAWMATERIALRETURNDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(rawMaterialReturnDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return rawMaterialReturnDtl;
	}

	@Override
	public ReceiptInvoiceDtl saveReceiptInvoiceDtl(ReceiptInvoiceDtl receiptInvoiceDtl, String branchCode) {
		String destination = "SERVER";

		receiptInvoiceDtl = receiptInvoiceDtlRepository.save(receiptInvoiceDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(receiptInvoiceDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.RECEIPTINVOICEDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);



		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 receiptInvoiceDtl.getId()+KafkaMapleEventType.RECEIPTINVOICEDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(receiptInvoiceDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return receiptInvoiceDtl;
	}

	@Override
	public ReorderMst saveReorderMst(ReorderMst reorderMst, String branchCode) {
		String destination = "";

		reorderMst = reorderMstRepository.save(reorderMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(reorderMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.REORDERMST);
		kafkaMapleEvent.setPayLoad(jsonStr);


		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 reorderMst.getId()+KafkaMapleEventType.REORDERMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(reorderMst.getId());
		eventBus.post(kafkaMapleEvent);
			}
		}

		return reorderMst;
	}

	@Override
	public IntentDtl saveIntentDtl(IntentDtl intentDtl, String branchCode, String destinationBranch) {
		String destination = destinationBranch;

		intentDtl = intentDtlRepository.save(intentDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(intentDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.INTENTDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);



		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 intentDtl.getId()+KafkaMapleEventType.INTENTDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(intentDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return intentDtl;
	}

	@Override
	public IntentHdr saveIntentHdr(IntentHdr intentHdr, String branchCode, String destinationBranch) {
		String destination = destinationBranch;

		intentHdr = intentHdrRepository.save(intentHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(intentHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.INTENTHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);



		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 intentHdr.getId()+KafkaMapleEventType.INTENTHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(intentHdr.getId());
		eventBus.post(kafkaMapleEvent);


		return intentHdr;
	}

	@Override
	public InvoiceEditEnableMst saveInvoiceEditEnableMst(InvoiceEditEnableMst invoiceEditEnableMst, String branchCode,
			String destinationBranch) {
		String destination = destinationBranch;

		invoiceEditEnableMst = invoiceEditEnableMstRepository.save(invoiceEditEnableMst);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(invoiceEditEnableMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.INVOICEEDITENABLEMST);
		kafkaMapleEvent.setPayLoad(jsonStr);



		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 invoiceEditEnableMst.getId()+KafkaMapleEventType.INVOICEEDITENABLEMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(invoiceEditEnableMst.getId());
		eventBus.post(kafkaMapleEvent);



		return invoiceEditEnableMst;
	}

	@Override
	public OtherBranchSalesDtl saveOtherBranchSalesDtl(OtherBranchSalesDtl otherBranchSalesDtl, String branchCode,
			String destinationBranch) {
		String destination = destinationBranch;

		otherBranchSalesDtl = otherBranchSalesDtlRepository.save(otherBranchSalesDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(otherBranchSalesDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.OTHERBRANCHSALESDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);



		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 otherBranchSalesDtl.getId()+KafkaMapleEventType.OTHERBRANCHSALESDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(otherBranchSalesDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return otherBranchSalesDtl;
	}

	@Override
	public OtherBranchSalesTransHdr saveOtherBranchSalesTransHdr(OtherBranchSalesTransHdr otherBranchSalesTransHdr,
			String branchCode, String destinationBranch) {
		String destination = destinationBranch;

		otherBranchSalesTransHdr = otherBranchSalesTransHdrRepository.save(otherBranchSalesTransHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(otherBranchSalesTransHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.OTHERBRANCHSALESTRANSHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);


		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 otherBranchSalesTransHdr.getId()+KafkaMapleEventType.OTHERBRANCHSALESTRANSHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(otherBranchSalesTransHdr.getId());
		eventBus.post(kafkaMapleEvent);


		return otherBranchSalesTransHdr;
	}

	@Override
	public OtherBranchPurchaseHdr saveOtherBranchPurchaseHdr(OtherBranchPurchaseHdr otherBranchPurchaseHdr,
			String branchCode, String destinationBranch) {
		String destination = destinationBranch;

		otherBranchPurchaseHdr = otherBranchPurchaseHdrRepository.save(otherBranchPurchaseHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(otherBranchPurchaseHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.OTHERBRANCHPURCHASEHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);



		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 otherBranchPurchaseHdr.getId()+KafkaMapleEventType.OTHERBRANCHPURCHASEHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(otherBranchPurchaseHdr.getId());
		eventBus.post(kafkaMapleEvent);



		return otherBranchPurchaseHdr;
	}

	@Override
	public OtherBranchPurchaseDtl saveOtherBranchPurchaseDtl(OtherBranchPurchaseDtl otherBranchPurchaseDtl,
			String branchCode, String destinationBranch) {
		String destination = destinationBranch;

		otherBranchPurchaseDtl = otherBranchPurchaseDtlRepository.save(otherBranchPurchaseDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(otherBranchPurchaseDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.OTHERBRANCHPURCHASEDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		
		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 otherBranchPurchaseDtl.getId()+KafkaMapleEventType.OTHERBRANCHPURCHASEDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(otherBranchPurchaseDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return otherBranchPurchaseDtl;
	}

	@Override
	public OwnAccount saveOwnAccount(OwnAccount OwnAccount, String branchCode) {
		String destination = "SERVER";

		OwnAccount = OwnAccountRepository.save(OwnAccount);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(OwnAccount);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.OWNACCOUNT);
		kafkaMapleEvent.setPayLoad(jsonStr);



		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 OwnAccount.getId()+KafkaMapleEventType.OWNACCOUNT + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(OwnAccount.getId());
		eventBus.post(kafkaMapleEvent);


		return OwnAccount;
	}

	@Override
	public OwnAccountSettlementDtl saveOwnAccountSettlementDtl(OwnAccountSettlementDtl ownAccountSettlementDtl,
			String branchCode) {
		String destination = "SERVER";

		ownAccountSettlementDtl = ownAccountSettlementDtlRepository.save(ownAccountSettlementDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(ownAccountSettlementDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.OWNACCOUNTSETTLEMENTDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 ownAccountSettlementDtl.getId()+KafkaMapleEventType.OWNACCOUNTSETTLEMENTDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(ownAccountSettlementDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return ownAccountSettlementDtl;
	}

	@Override
	public OwnAccountSettlementMst saveOwnAccountSettlementMst(OwnAccountSettlementMst ownAccountSettlementMst,
			String branchCode) {
		String destination = "SERVER";

		ownAccountSettlementMst = ownAccountSettlementMstRepository.save(ownAccountSettlementMst);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(ownAccountSettlementMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.OWNACCOUNTSETTLEMENTMST);
		kafkaMapleEvent.setPayLoad(jsonStr);



		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 ownAccountSettlementMst.getId()+KafkaMapleEventType.OWNACCOUNTSETTLEMENTMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(ownAccountSettlementMst.getId());
		eventBus.post(kafkaMapleEvent);



		return ownAccountSettlementMst;
	}

	@Override
	public PaymentInvoiceDtl savePaymentInvoiceDtl(PaymentInvoiceDtl PaymentInvoiceDtl, String branchCode) {
		String destination = "SERVER";

		PaymentInvoiceDtl = paymentInvoiceDtlRepository.save(PaymentInvoiceDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(PaymentInvoiceDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PAYMENTINVOICEDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 PaymentInvoiceDtl.getId()+KafkaMapleEventType.PAYMENTINVOICEDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(PaymentInvoiceDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return PaymentInvoiceDtl;
	}

	@Override
	public SaleOrderReceipt saveSaleOrderReceipt(SaleOrderReceipt saleOrderReceipt, String branchCode) {
		String destination = "SERVER";

		saleOrderReceipt = saleOrderReceiptRepository.save(saleOrderReceipt);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(saleOrderReceipt);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SALEORDERRECEIPT);
		kafkaMapleEvent.setPayLoad(jsonStr);

	

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 saleOrderReceipt.getId()+KafkaMapleEventType.SALEORDERRECEIPT + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(saleOrderReceipt.getId());
		eventBus.post(kafkaMapleEvent);



		return saleOrderReceipt;
	}

	@Override
	public SalesOrderTransHdr saveSalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr, String branchCode) {
		String destination = "SERVER";

		salesOrderTransHdr = salesOrderTransHdrRepository.save(salesOrderTransHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(salesOrderTransHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SALESORDERTRANSHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 salesOrderTransHdr.getId()+KafkaMapleEventType.SALESORDERTRANSHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(salesOrderTransHdr.getId());
		eventBus.post(kafkaMapleEvent);



		return salesOrderTransHdr;
	}

	@Override
	public SalesOrderDtl saveSalesOrderDtl(SalesOrderDtl salesOrderDtl, String branchCode) {
		String destination = "SERVER";

		salesOrderDtl = salesOrderDtlRepository.save(salesOrderDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(salesOrderDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SALESORDERDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

	

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 salesOrderDtl.getId()+KafkaMapleEventType.SALESORDERDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(salesOrderDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return salesOrderDtl;
	}

	@Override
	@Transactional
	public SalesDtl saveSalesDtl(SalesDtl salesDtl, String branchCode) {
		

		salesDtl = salesDtlRepository.save(salesDtl);
		
		
		
		return salesDtl;
	}

	public SalesDtl publishSalesDtl(SalesDtl salesDtl,String branchCode) {
		String destination = "SERVER";
		Optional<SalesDtl> salesDtlOpt=salesDtlRepository.findById(salesDtl.getId());
		salesDtl=salesDtlOpt.get();
		System.out.println(salesDtl.getId());

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper()

				.registerModule(new ParameterNamesModule()).registerModule(new Jdk8Module())
				.registerModule(new JavaTimeModule());

		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(salesDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SALESDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 salesDtl.getId()+KafkaMapleEventType.SALESDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(salesDtl.getId());
		eventBus.post(kafkaMapleEvent);

		
		
		return salesDtl;
	}
	@Override
	@Transactional
	public SalesReceipts saveSalesReceipts(SalesReceipts salesReceipts, String branchCode) {
		String destination = "SERVER";

		salesReceipts = salesReceiptsRepository.save(salesReceipts);

		
		

		return salesReceipts;
	}

	@Override
	public SalesDtlDeleted saveSalesDtlDeleted(SalesDtlDeleted salesDtlDeleted, String branchCode) {
		String destination = "SERVER";

		salesDtlDeleted = salesDtlDeletedRepository.save(salesDtlDeleted);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(salesDtlDeleted);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SALESDTLDELETED);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 salesDtlDeleted.getId()+KafkaMapleEventType.SALESDTLDELETED + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(salesDtlDeleted.getId());
		eventBus.post(kafkaMapleEvent);



		return salesDtlDeleted;
	}

	@Override
	public SalesReturnDtl saveSalesReturnDtl(SalesReturnDtl salesReturnDtl, String branchCode) {
		String destination = "SERVER";

		salesReturnDtl = salesReturnDtlRepository.save(salesReturnDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(salesReturnDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SALESRETURNDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 salesReturnDtl.getId()+KafkaMapleEventType.SALESRETURNDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(salesReturnDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return salesReturnDtl;
	}

	@Override
	public SalesReturnHdr saveSalesReturnHdr(SalesReturnHdr salesReturnHdr, String branchCode) {
		String destination = "SERVER";

		salesReturnHdr = salesReturnHdrRepository.save(salesReturnHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(salesReturnHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SALESRETURNHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 salesReturnHdr.getId()+KafkaMapleEventType.SALESRETURNHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(salesReturnHdr.getId());
		eventBus.post(kafkaMapleEvent);



		return salesReturnHdr;
	}

	@Override
	public SalesTypeMst saveSalesTypeMst(SalesTypeMst salesTypeMst, String branchCode) {
		String destination = "";

		salesTypeMst = salesTypeMstRepository.save(salesTypeMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(salesTypeMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SALESTYPEMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 salesTypeMst.getId()+KafkaMapleEventType.SALESTYPEMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(salesTypeMst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return salesTypeMst;
	}

	@Override
	public SchEligiAttrListDef saveSchEligiAttrListDef(SchEligiAttrListDef schEligiAttrListDef, String branchCode) {
		String destination = "";

		schEligiAttrListDef = schEligiAttrListDefRepository.save(schEligiAttrListDef);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(schEligiAttrListDef);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SCHELIGIATTRLISTDEF);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 schEligiAttrListDef.getId()+KafkaMapleEventType.SCHELIGIATTRLISTDEF + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(schEligiAttrListDef.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return schEligiAttrListDef;
	}

	@Override
	public SchEligibilityAttribInst saveSchEligibilityAttribInst(SchEligibilityAttribInst schEligibilityAttribInst,
			String branchCode) {
		String destination = "";

		schEligibilityAttribInst = schEligibilityAttribInstRepository.save(schEligibilityAttribInst);
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(schEligibilityAttribInst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SCHELIGIBILITYATTRIBINST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 schEligibilityAttribInst.getId()+KafkaMapleEventType.SCHELIGIBILITYATTRIBINST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(schEligibilityAttribInst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return schEligibilityAttribInst;
	}

	@Override
	public SchEligibilityDef saveSchEligibilityDef(SchEligibilityDef schEligibilityDef, String branchCode) {
		String destination = "";

		schEligibilityDef = schEligibilityDefRepository.save(schEligibilityDef);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(schEligibilityDef);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SCHELIGIBILITYDEF);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 schEligibilityDef.getId()+KafkaMapleEventType.SCHELIGIATTRLISTDEF + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(schEligibilityDef.getId());
		eventBus.post(kafkaMapleEvent);

	}
}

		return schEligibilityDef;
	}

	@Override
	public SchemeInstance saveSchemeInstance(SchemeInstance schemeInstance, String branchCode) {
		String destination = "";

		schemeInstance = schemeInstanceRepository.save(schemeInstance);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(schemeInstance);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SCHEMEINSTANCE);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 schemeInstance.getId()+KafkaMapleEventType.SCHEMEINSTANCE + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(schemeInstance.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return schemeInstance;
	}

	@Override
	public SchOfferAttrListDef saveSchOfferAttrListDef(SchOfferAttrListDef schOfferAttrListDef, String branchCode) {
		String destination = "";

		schOfferAttrListDef = schOfferAttrListDefRepository.save(schOfferAttrListDef);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(schOfferAttrListDef);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SCHOFFERATTRLISTDEF);
		kafkaMapleEvent.setPayLoad(jsonStr);

		String value = "";
		try {
			value = Obj.writeValueAsString(kafkaMapleEvent);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 schOfferAttrListDef.getId()+KafkaMapleEventType.SCHOFFERATTRLISTDEF + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(schOfferAttrListDef.getId());
		eventBus.post(kafkaMapleEvent);
			}
		}

		return schOfferAttrListDef;
	}

	@Override
	public SchOfferAttrInst saveSchOfferAttrInst(SchOfferAttrInst schOfferAttrInst, String branchCode) {
		String destination = "";

		schOfferAttrInst = schOfferAttrInstRepository.save(schOfferAttrInst);

		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
			
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(schOfferAttrInst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SCHOFFERATTRINST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 schOfferAttrInst.getId()+KafkaMapleEventType.SCHOFFERATTRINST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(schOfferAttrInst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return schOfferAttrInst;
	}

	@Override
	public SchOfferDef saveSchOfferDef(SchOfferDef schOfferDef, String branchCode) {
		String destination = "";

		schOfferDef = schOfferDefRepository.save(schOfferDef);

		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(schOfferDef);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SCHOFFERDEF);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 schOfferDef.getId()+KafkaMapleEventType.SCHOFFERDEF + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(schOfferDef.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return schOfferDef;
	}

	@Override
	public SchSelectAttrListDef saveSchSelectAttrListDef(SchSelectAttrListDef schSelectAttrListDef, String branchCode) {
		String destination = "";

		schSelectAttrListDef = schSelectAttrListDefRepository.save(schSelectAttrListDef);

		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
			
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(schSelectAttrListDef);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SCHSELECTATTRLISTDEF);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 schSelectAttrListDef.getId()+KafkaMapleEventType.SCHSELECTATTRLISTDEF + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(schSelectAttrListDef.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return schSelectAttrListDef;
	}

	@Override
	public SchSelectDef saveSchSelectDef(SchSelectDef schSelectDef, String branchCode) {
		String destination = "";

		schSelectDef = schSelectDefRepository.save(schSelectDef);

		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(schSelectDef);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SCHSELECTDEF);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 schSelectDef.getId()+KafkaMapleEventType.SCHSELECTDEF + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(schSelectDef.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return schSelectDef;
	}

	@Override
	public SchSelectionAttribInst saveSchSelectionAttribInst(SchSelectionAttribInst schSelectionAttribInst,
			String branchCode) {
		String destination = "";

		schSelectionAttribInst = schSelectionAttribInstRepository.save(schSelectionAttribInst);

		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(schSelectionAttribInst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SCHSELECTIONATTRIBINST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 schSelectionAttribInst.getId()+KafkaMapleEventType.SCHSELECTIONATTRIBINST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(schSelectionAttribInst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return schSelectionAttribInst;
	}

	@Override
	public ServiceInHdr saveServiceInHdr(ServiceInHdr serviceInHdr, String branchCode) {
		String destination = "SERVER";

		serviceInHdr = serviceInHdrRepository.save(serviceInHdr);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(serviceInHdr);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SERVICEINHDR);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 serviceInHdr.getId()+KafkaMapleEventType.SERVICEINHDR + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(serviceInHdr.getId());
		eventBus.post(kafkaMapleEvent);



		return serviceInHdr;
	}

	@Override
	public ServiceInDtl saveServiceInDtl(ServiceInDtl serviceInDtl, String branchCode) {
		String destination = "SERVER";

		serviceInDtl = serviceInDtlRepository.save(serviceInDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(serviceInDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SERVICEINDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 serviceInDtl.getId()+KafkaMapleEventType.SERVICEINDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(serviceInDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return serviceInDtl;
	}

	@Override
	public StoreChangeDtl saveStoreChangeDtl(StoreChangeDtl storeChangeDtl, String branchCode) {
		String destination = "SERVER";

		storeChangeDtl = storeChangeDtlRepository.save(storeChangeDtl);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(storeChangeDtl);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.STORECHANGEDTL);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 storeChangeDtl.getId()+KafkaMapleEventType.STORECHANGEDTL + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(storeChangeDtl.getId());
		eventBus.post(kafkaMapleEvent);



		return storeChangeDtl;
	}

	@Override
	public StoreChangeMst saveStoreChangeMst(StoreChangeMst storeChangeMst, String branchCode) {
		String destination = "SERVER";

		storeChangeMst = storeChangeMstRepository.save(storeChangeMst);

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(storeChangeMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.STORECHANGEMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				storeChangeMst.getId()+KafkaMapleEventType.STORECHANGEMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(storeChangeMst.getId());
		
		eventBus.post(kafkaMapleEvent);



		return storeChangeMst;
	}

	@Override
	public UrsMst saveUrsMst(UrsMst ursMst, String branchCode) {
		String destination = "";

		ursMst = ursMstRepository.save(ursMst);
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
			

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(ursMst);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.URSMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 ursMst.getId()+KafkaMapleEventType.URSMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(ursMst.getId());
		eventBus.post(kafkaMapleEvent);

			}
		}

		return ursMst;
	}

	@Override
	
	public AcceptStockMessage saveAcceptStockMessage(AcceptStockMessage acceptStockMessage, String branchCode,
			String fromBranch) {

		String destination = fromBranch;

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(acceptStockMessage);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ACCEPTSTOCKMESSAGE);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				acceptStockMessage.getVoucherNumber()+KafkaMapleEventType.ACCEPTSTOCKMESSAGE + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(acceptStockMessage.getVoucherNumber());
		eventBus.post(kafkaMapleEvent);



		return acceptStockMessage;

	}

	@Override
	public StockTransferInDtl saveAndPublishService(@Valid StockTransferInDtl stockTransferInDtl, String mybranch,
			String fromBranch) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void publishObjectDeletion(Object Object, String branchCode, KafkaMapleEventType eventType, String id) {
	
		String destination = "";
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
			destination = branch.getBranchCode();
			
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
		
			jsonStr = Obj.writeValueAsString(Object);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(eventType);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				id + eventType + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);
		kafkaMapleEvent.setVoucherNumber(id);
		
		
		eventBus.post(kafkaMapleEvent);
			}
		}
	}

	@Override
	public void publishObjectToKafkaEvent(Object Object, String branchCode,
			KafkaMapleEventType eventType,KafkaMapleEventType destination, String voucherNumber) {
		
		if(branchCode.equalsIgnoreCase(KafkaMapleEventType.SERVER.toString()))
		{
			return ;
		}
	if(destination.equals(KafkaMapleEventType.ALL)) {
		
		
		String destinationtobranch="";
		
		List<BranchMst> branchList = branchMstRepository.findAll();
		for(BranchMst branch : branchList) {
			if(!branch.getBranchCode().equalsIgnoreCase(mybranch)) {
				destinationtobranch = branch.getBranchCode();
		
		 String jsonStr = "";

			ObjectMapper Obj = new ObjectMapper()
					.registerModule(new ParameterNamesModule()).registerModule(new Jdk8Module())
					.registerModule(new JavaTimeModule());

			try {
				// Converting the Java object into a JSON string
			
				jsonStr = Obj.writeValueAsString(Object);
				logger.info("Converted to JSON String: {} ", jsonStr);
			} catch (IOException e) {
				
				System.out.println(jsonStr);
				logger.info("Exception : {} ", e);
			}
			KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
			kafkaMapleEvent.setLibraryEventId(partitionKey);
			kafkaMapleEvent.setLibraryEventType(eventType);
			kafkaMapleEvent.setPayLoad(jsonStr);

			kafkaMapleEvent.setSource(branchCode);
			kafkaMapleEvent.setDestination(destinationtobranch.toString());
			String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
					 voucherNumber+eventType + file_sufix;
			kafkaMapleEvent.setSourceFile(sourceFileName);

			kafkaMapleEvent.setVoucherNumber(voucherNumber);
			eventBus.post(kafkaMapleEvent);
			}
		}
		
		
	}else {
       String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper()
				.registerModule(new ParameterNamesModule()).registerModule(new Jdk8Module())
				.registerModule(new JavaTimeModule());

		try {
			// Converting the Java object into a JSON string
		
			jsonStr = Obj.writeValueAsString(Object);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(eventType);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination.toString());
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 voucherNumber+eventType + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(voucherNumber);
		eventBus.post(kafkaMapleEvent);
	}
	}

	@Override
	public void publishStockTransferToKafkaEvent(Object Object, String branchCode, KafkaMapleEventType eventType,
			String destination,String voucherNumber) {
		
		if(branchCode.equalsIgnoreCase(KafkaMapleEventType.SERVER.toString()))
		{
			return ;
		}
		
		  String jsonStr = "";

			ObjectMapper Obj = new ObjectMapper()
					.registerModule(new ParameterNamesModule()).registerModule(new Jdk8Module())
					.registerModule(new JavaTimeModule());

			try {
				// Converting the Java object into a JSON string
			
				jsonStr = Obj.writeValueAsString(Object);
				logger.info("Converted to JSON String: {} ", jsonStr);
			} catch (IOException e) {
				
				System.out.println(jsonStr);
				logger.info("Exception : {} ", e);
			}
			KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
			kafkaMapleEvent.setLibraryEventId(partitionKey);
			kafkaMapleEvent.setLibraryEventType(eventType);
			kafkaMapleEvent.setPayLoad(jsonStr);

			kafkaMapleEvent.setSource(branchCode);
			kafkaMapleEvent.setDestination(destination);
			String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
					 voucherNumber+eventType + file_sufix;
			kafkaMapleEvent.setSourceFile(sourceFileName);

			kafkaMapleEvent.setVoucherNumber(voucherNumber);
			eventBus.post(kafkaMapleEvent);
		
	}
	
	
	@Override
	public void salesFinalSaveToKafkaEvent(SalesTransHdr salesTransHdr,String branchCode, KafkaMapleEventType destination)
	{
		
		if(branchCode.equalsIgnoreCase(KafkaMapleEventType.SERVER.toString()))
		{
			return ;
		}
	
		SalesHdrAndDtlMessageEntity salesHdrAndDtlMessageEntity = new SalesHdrAndDtlMessageEntity();
		
		List<SalesDtl> SalesDtlArray=salesDtlRepository.findBySalesTransHdr(salesTransHdr);
		salesHdrAndDtlMessageEntity.setSalesTransHdr(salesTransHdr);
		salesHdrAndDtlMessageEntity.getSalesDtlList().addAll(SalesDtlArray);
		
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper()
				.registerModule(new ParameterNamesModule()).registerModule(new Jdk8Module())
				.registerModule(new JavaTimeModule());
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(salesHdrAndDtlMessageEntity);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.SALESTRANSDTLARRAY);
		kafkaMapleEvent.setPayLoad(jsonStr);
        
		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination.toString());
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 salesTransHdr.getId()+KafkaMapleEventType.SALESTRANSDTLARRAY + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(salesTransHdr.getId());
		eventBus.post(kafkaMapleEvent);
		
		
			 

		}

	@Override
	public void purchaseFinalSaveToKafkaEvent(PurchaseHdr purchase, String mybranch, KafkaMapleEventType destination) {
		if(mybranch.equalsIgnoreCase(KafkaMapleEventType.SERVER.toString()))
		{
			return ;
		}
		
		PurchaseMessageEntity purchaseMessageEntity = new PurchaseMessageEntity();
		
		List<PurchaseDtl> PurchaseDtlArray=purchaseDtlRepository.findByPurchaseHdrId(purchase.getId());
		purchaseMessageEntity.setPurchaseHdr(purchase);
		purchaseMessageEntity.getPurchaseDtlList().addAll(PurchaseDtlArray);
		
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(purchaseMessageEntity);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.PURCHASEDTLARRAY);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(mybranch);
		kafkaMapleEvent.setDestination(destination.toString());
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				 purchase.getId()+KafkaMapleEventType.PURCHASEDTLARRAY + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(purchase.getId());
		eventBus.post(kafkaMapleEvent);
		
		
	}
	@Override
	public void publishObjectToServer(Object Object, String branchCode,
			KafkaMapleEventType eventType,String destination, String voucherNumber) {
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(Object);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.ITEMMST);
		kafkaMapleEvent.setPayLoad(jsonStr);

		

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				voucherNumber+KafkaMapleEventType.ITEMMST + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);

		kafkaMapleEvent.setVoucherNumber(voucherNumber);
		eventBus.post(kafkaMapleEvent);
		
	}
		
	
	
	
	
	
	
	
		
		
	}
	
	


