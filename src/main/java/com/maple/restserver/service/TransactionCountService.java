package com.maple.restserver.service;

import java.sql.Date;

import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.TransactionCountModel;

@Component
public interface TransactionCountService {

	
	TransactionCountModel	getTransactionCount(String branch,Date sqlDate ,String companymstid);
}
