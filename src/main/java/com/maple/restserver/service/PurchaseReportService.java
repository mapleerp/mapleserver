package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PharmacyPurchaseReport;
import com.maple.restserver.report.entity.HsnCodeSaleReport;
import com.maple.restserver.report.entity.HsnWisePurchaseDtlReport;
import com.maple.restserver.report.entity.MonthlySupplierSummary;
import com.maple.restserver.report.entity.PurchaseReport;

@Service
@Transactional
public interface PurchaseReportService {
	
	public  List<Object> getDailyPurchase(String voucherNo);
	public   List<PurchaseReport> getAllPurchase();
	
	public List<PurchaseReport> retrieveReportPurchase(String vouchernumber,String branchcode,Date date);

	
	public List<PurchaseReport> retrieveTaxRate(String vouchernumber,String branchcode,Date date);

	List<HsnCodeSaleReport> getHSNCodeWiseSalesDtl(Date fdate,Date tdate,String branchCode);
	public List<MonthlySupplierSummary>retrievePurchaseReport( String vouchernumber,String branchcode,String startDate,String endDate);

  //version 3.1
	public List<PurchaseReport>   getPurchaseDtlReport(Date fromDate,Date toDate,String branchCode,CompanyMst companyMst);

	
	public List<PurchaseReport> purchaseExportToExcel(CompanyMst companyMst,String branchcode,Date fromDate,Date todate);
  //version 3.1 end

public List<PharmacyPurchaseReport>getPharmacyPurchaseReport(CompanyMst companyMst, Date fdate,Date tdate,String branchCode,String []array);


	public List<PurchaseReport> getSupplierWisePurchaseReport(Date fromDate, Date toDate, String[] array,
			String supname,CompanyMst  companymst);
	public List<PurchaseReport> getSupplierWisePurchaseReportWithDate(Date fromDate, Date toDate, String supname,
			CompanyMst companyMst);
	
	public List<HsnWisePurchaseDtlReport> getHsnWisePurchaseDtlReport(Date fromDate, Date toDate, String[] array,
			String branchcode, CompanyMst companyMst);
	public List<HsnWisePurchaseDtlReport> getHsnWisePurchaseDtlReportWithoutGroup(Date fromDate, Date toDate,
			String branchcode, CompanyMst companyMst);
	public String autoGenerateBatch(String itemid);
	
	
	public List<PurchaseReport> getImportPurchaseInvoiceDetail(String phdrid);
	
	public List<PurchaseReport> getImportPurchaseInvoiceReportByHdrId(String phdrid);

	public Double  getTotalAdditionalExpenseByHdrId(String phdrid);
	public Double getTotalImportExpense(String phdrid);


}


