package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.report.entity.DailyReceiptsSummaryReport;
import com.maple.restserver.report.entity.ReceiptInvoice;
import com.maple.restserver.report.entity.ReceiptReport;
import com.maple.restserver.repository.ReceiptDtlRepository;

@Service
@Transactional
@Component
public class ReciptDtlServiceImpl implements ReciptDtlService {
@Autowired
ReceiptDtlRepository receiptDtlRepository ;
	@Override
	public List<ReceiptDtl> reciptDtlServiceImpl(String accountid, Date reportdate) {
	

		List<ReceiptDtl> reciptList= new ArrayList();
		
		 List<Object> obj =   receiptDtlRepository.findByAccountAndInstrumentDate(accountid,reportdate);
		 for(int i=0;i<obj.size();i++)
		 {
			 
			 Object[] objAray = (Object[]) obj.get(i);
			 ReceiptDtl reciptdtl =new ReceiptDtl();
			 reciptdtl.setAmount((Double) objAray[0]);
			 reciptdtl.setRemark((String) objAray[1]);
				
			 reciptList.add(reciptdtl);
	}

		 return reciptList;
	}
	@Override
	public List<ReceiptInvoice> reciptDtlBetweenDate(String accountid, Date tdate,Date fdate) {
		List<ReceiptInvoice> reciptList= new ArrayList();
		
		 List<Object> obj =   receiptDtlRepository.findByReceiptHdrBetweenDate(accountid,tdate,fdate);
		 for(int i=0;i<obj.size();i++)
		 {
			 
			 Object[] objAray = (Object[]) obj.get(i);
			 ReceiptInvoice receiptReport =new ReceiptInvoice();
			 receiptReport.setVoucherNumber((String) objAray[0]);
			 receiptReport.setVoucherDate((Date)objAray[1]);
			 receiptReport.setRecievedAmount((Double)objAray[2]);
			 receiptReport.setId((String) objAray[3]);
			 receiptReport.setAccount((String) objAray[4]);
			 reciptList.add(receiptReport);
	}

		 return reciptList;
	}
	@Override
	public List<ReceiptInvoice> reciptDtlBetweenDate(Date tdate,Date fdate) {
		List<ReceiptInvoice> reciptList= new ArrayList();
		
		 List<Object> obj =   receiptDtlRepository.findByReceiptHdrBetweenDate(tdate,fdate);
		 for(int i=0;i<obj.size();i++)
		 {
			 
			 Object[] objAray = (Object[]) obj.get(i);
			 ReceiptInvoice receiptReport =new ReceiptInvoice();
			 receiptReport.setVoucherNumber((String) objAray[0]);
			 receiptReport.setVoucherDate((Date)objAray[1]);
			 receiptReport.setRecievedAmount((Double)objAray[2]);
			 receiptReport.setId((String) objAray[3]);
			 receiptReport.setAccount((String) objAray[4]);
			 reciptList.add(receiptReport);
	}

		 return reciptList;
	}
	// ------------------version 5.1 surya

		@Override
		public List<DailyReceiptsSummaryReport> getDailyReciptSummaryByDateAndAccount(String companymstid, Date date,
				String branchcode) {

			List<DailyReceiptsSummaryReport> receiptDtlSummary = new ArrayList<DailyReceiptsSummaryReport>();

			List<Object> objList = receiptDtlRepository.getDailyReciptSummaryByDateAndAccount(companymstid, date, branchcode);

			for (int i = 0; i < objList.size(); i++) {

				Object[] objAray = (Object[]) objList.get(i);
				DailyReceiptsSummaryReport dailyReceiptsSummaryReport = new DailyReceiptsSummaryReport();
				
				dailyReceiptsSummaryReport.setTotalCash((Double) objAray[0]);
				dailyReceiptsSummaryReport.setDebitAccount((String) objAray[1]);

				receiptDtlSummary.add(dailyReceiptsSummaryReport);
			}

			return receiptDtlSummary;
		}

		// ------------------version 5.1 surya end
	}


