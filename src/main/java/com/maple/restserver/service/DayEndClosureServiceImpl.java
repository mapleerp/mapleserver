package com.maple.restserver.service;

import java.io.DataOutput;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.SummaryOfDailyStock;
import com.maple.restserver.report.entity.DayEndClosureReport;
import com.maple.restserver.report.entity.SalesReport;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component
public class DayEndClosureServiceImpl implements DayEndClosureService {

	@Autowired
	DayEndClosureRepository dayEndClosureRepo;
	
	@Autowired
	BranchMstRepository branchMstRepository;
	
	@Autowired
	SaveAndPublishServiceImpl saveAndPublishServiceImpl;
	
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;
	
	@Autowired
	  private VoucherNumberService voucherService;
	
	@Override
	public DayEndClosureHdr getDayEndDetails(Date date,String companymstid) {
	
//		ArrayList<DayEndClosureHdr> dayEndList = new ArrayList();
		
		DayEndClosureHdr dayEndClosureHdr = dayEndClosureRepo.getDayEndClosureHdr(date);
		if(dayEndClosureHdr!=null) {
		List<Object> objList=dayEndClosureRepo.getDayEndDetails(date,companymstid);
		
//		
//		for(int i=0 ; i<=objList.size() ; i++)
//		{
		
		Object[] objAray = (Object[]) objList.get(0);


		dayEndClosureHdr.setCardSale((Double) objAray[1]);

		dayEndClosureHdr.setCashSale((Double) objAray[0]);
		dayEndClosureHdr.setCardSale2((Double) objAray[2]);
		
//		dayEndList.add(dayEndClosureHdr);
//		}
		}
		return dayEndClosureHdr;
	
}
	@Override
	public String retrieveDayEndStatus(Date date, String branchCode) {
		// TODO Auto-generated method stub
		return dayEndClosureRepo.gerDayEndCosureStatus( date,branchCode);
	}
	@Override
	public List<SalesReport> getDayEndReport(Date date, String companymstid) {

		ArrayList<SalesReport> salesList = new ArrayList();
		
		BranchMst branch = branchMstRepository.getMyBranch();
		String brachCash = branch.getBranchCode()+"-CASH";
		
//		String dayendcondition = " 'CREDIT' , '"+brachCash+"', 'CASH','UBER SALES', 'SWIGGY' ,'ZOMATO', 'FOOD PANDA', 'ONLINE'   ";
		List<Object> obj=dayEndClosureRepo.getDayEndReport(date,companymstid);

		 for(int i=0;i<obj.size();i++)
		 {
			 Double cash = 0.0;
			 Double card = 0.0;
			 Double card2 = 0.0;
			 
			 SalesReport report=new SalesReport();
			 Object[] objAray = (Object[]) obj.get(i);
		report.setBranch((String) objAray[0]);
		report.setInvovicetotal((Double) objAray[1]);
		
		if(null != objAray[2])
		{
			cash = (Double) objAray[2];
		} 

		if(null != (Double) objAray[3])
		{
			card = (Double) objAray[3];
		}
		if(null != objAray[4])
		{
			card2 = (Double) objAray[4];
		}
		
		
		
//		String receiptstr = "'CASH', '"+brachCash+"'";
		Double totalcash = dayEndClosureRepo.totalDayEndCash(date,companymstid);
		report.setCash(totalcash);
		report.setCard(card);
		report.setCard2(card2);
		report.setTotal(totalcash+card+card2);

		salesList.add(report);

		}
		
		return salesList;
		
	}
//	@Override
//	public List<DayEndClosureReport> retrieveDayEndClosureReport(String companymstid, Date voucherdate) {
//	
//
//		ArrayList<DayEndClosureReport> reportList = new ArrayList();
//
//		DayEndClosureReport report=new DayEndClosureReport();
//
//		List<Object> obj=dayEndClosureRepo.getDayEndClosureReport(voucherdate,companymstid);
//
//		 for(int i=0;i<obj.size();i++)
//		 {
//			 Object[] objAray = (Object[]) obj.get(i);
//		
//			 
//			 
//		reportList.add(report);
//
//		}
//		
//		return reportList;
//		
//	
//	}
	@Override
	public DayEndClosureHdr getDayEndTotalSalesDetails(Date date, String companymstid) {
		DayEndClosureHdr dayEndClosureHdr = new DayEndClosureHdr();
		List<Object> objList=dayEndClosureRepo.getDayEndDetails(date,companymstid);
		
		Object[] objAray = (Object[]) objList.get(0);
		dayEndClosureHdr.setCardSale((Double) objAray[1]);
		dayEndClosureHdr.setCashSale((Double) objAray[0]);
		dayEndClosureHdr.setCardSale2((Double) objAray[2]);
		

		return dayEndClosureHdr;
	
	}
	@Override
	public void dailyStockSummaryToCloud(String branch,String companyMstId) {
		List<SummaryOfDailyStock> summaryOfDailyStockList= new ArrayList();
		
	 Date day_end_date = getLastDayEndDate();
		java.sql.Date sqlDayEndDate = SystemSetting.UtilDateToSQLDate(day_end_date);
		
		List<Object> obj = itemBatchDtlRepository.getSummaryOfDailyStock(branch, sqlDayEndDate);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 SummaryOfDailyStock summaryOfDailyStock = new SummaryOfDailyStock();
			 summaryOfDailyStock.setStockIn((Double) objAray[0]);
			 summaryOfDailyStock.setStockOut((Double) objAray[1]);
			 summaryOfDailyStock.setBatch((String) objAray[2]);
			 summaryOfDailyStock.setItemId((String) objAray[3]);
			 summaryOfDailyStock.setExpiryDate((java.util.Date) objAray[4]);
			 summaryOfDailyStock.setBranchCode(branch);
			 summaryOfDailyStock.setProcessDate(day_end_date);
			 summaryOfDailyStockList.add(summaryOfDailyStock);
		
		 
		 }
		 
		 
		 VoucherNumber voucherNo = voucherService.generateInvoice(SystemSetting.getFinancialYear() + "STOCK_SUMMARY",
				 companyMstId);
		 
		saveAndPublishServiceImpl.publishObjectToKafkaEvent(summaryOfDailyStockList, 
					branch,
					KafkaMapleEventType.SUMMARYOFDAILYSTOCK, 
					KafkaMapleEventType.SERVER,voucherNo.toString());
		
	}
	private Date getLastDayEndDate() {
		 
		DayEndClosureHdr dayEndClosureHdr=dayEndClosureRepo.getLastDayEndClosureHdr();
		
		return dayEndClosureHdr.getProcessDate();
	}
	

}
