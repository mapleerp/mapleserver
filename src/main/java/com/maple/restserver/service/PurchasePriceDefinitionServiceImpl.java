package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.BatchPriceDefinition;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.PurchasePriceDefinitionDtl;
import com.maple.restserver.entity.PurchasePriceDefinitionMst;
import com.maple.restserver.repository.BatchPriceDefinitionRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.PurchasePriceDefinitionDtlRepository;
import com.maple.restserver.repository.PurchasePriceDefinitionMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
public class PurchasePriceDefinitionServiceImpl implements PurchasePriceDefinitionService {

	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	PurchasePriceDefinitionMstRepository purchasePriceDefinitionMstRepository;

	@Autowired
	PurchasePriceDefinitionDtlRepository purchasePriceDefinitionDtlRepository;

	@Autowired
	private ExternalApi externalApi;

	@Autowired
	BatchPriceDefinitionRepository batchPriceDefinitionRepository;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	


	@Autowired
	private VoucherNumberService voucherNumberService;

	@Override
	public List<PurchasePriceDefinitionMst> retrievePurchasePriceDefinitionMstByHdrId(String companymstid,
			String hdrid) {

		List<PurchasePriceDefinitionMst> purchasePriceDefList = new ArrayList<PurchasePriceDefinitionMst>();

		purchasePriceDefList = purchasePriceDefinitionMstRepository.findByPurchaseHdrIdAndCompanyMstId(hdrid,
				companymstid);

		if (purchasePriceDefList.size() == 0) {
			try {

				externalApi.getPuchaseAndSavePriceDef(companymstid, hdrid);

				purchasePriceDefList = purchasePriceDefinitionMstRepository.findByPurchaseHdrIdAndCompanyMstId(hdrid,
						companymstid);

			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}

		return purchasePriceDefList;
	}

	@Override
	public String getPurchasePriceDefinitionMstByHdrIdFromLocalDB(String companymstid, String hdrid) {

		List<PurchasePriceDefinitionDtl> purchasePriceDefList = purchasePriceDefinitionDtlRepository
				.findByPurchaseHdrIdAndCompanyMstId(hdrid, companymstid);

		for (PurchasePriceDefinitionDtl purchasePriceDefinitionDtl : purchasePriceDefList) {

			BatchPriceDefinition batchPriceDefinition = new BatchPriceDefinition();

			batchPriceDefinition.setAmount(purchasePriceDefinitionDtl.getAmount());
			batchPriceDefinition.setBranchCode(purchasePriceDefinitionDtl.getBranchCode());
			batchPriceDefinition.setCompanyMst(purchasePriceDefinitionDtl.getCompanyMst());
			batchPriceDefinition.setItemId(purchasePriceDefinitionDtl.getItemId());
			batchPriceDefinition.setPriceId(purchasePriceDefinitionDtl.getPriceId());
			batchPriceDefinition.setStartDate(purchasePriceDefinitionDtl.getStartDate());
			batchPriceDefinition.setUnitId(purchasePriceDefinitionDtl.getUnitId());
			batchPriceDefinition.setBatch(purchasePriceDefinitionDtl.getBatch());
			
			VoucherNumber vouvherObj = voucherNumberService.generateInvoice("BPD", companymstid);
			String vno = vouvherObj.getCode();
			batchPriceDefinition.setId(vno);

			purchasePriceDefinitionDtl.setPriceStatus("YES");
			purchasePriceDefinitionDtlRepository.save(purchasePriceDefinitionDtl);

			
			batchPriceDefinitionRepository.
			UpdatePriceDefinitionbyItemIdUnitIdBatch(batchPriceDefinition.getItemId(),
					batchPriceDefinition.getStartDate(), batchPriceDefinition.getPriceId(), batchPriceDefinition.getUnitId(),batchPriceDefinition.getBatch());

			batchPriceDefinition = batchPriceDefinitionRepository.save(batchPriceDefinition);

			
			  Map<String, Object> variables = new HashMap<String, Object>();

				variables.put("voucherNumber", batchPriceDefinition.getId());

				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("inet", 0);
				variables.put("id", batchPriceDefinition.getId());
				variables.put("companyid", batchPriceDefinition.getCompanyMst());
				variables.put("branchcode", batchPriceDefinition.getBranchCode());
				variables.put("REST", 1);
				variables.put("WF", "forwardBatchPriceDefinition");
				
				String workflow = (String) variables.get("WF");
				String voucherNumber = (String) variables.get("voucherNumber");
				String sourceID = (String) variables.get("id");

				LmsQueueMst lmsQueueMst = new LmsQueueMst();
				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
				//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
				java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
				java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
				lmsQueueMst.setVoucherDate(sqlVDate);
				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
				lmsQueueMst.setVoucherType(workflow);
				lmsQueueMst.setPostedToServer("NO");
				lmsQueueMst.setJobClass("forwardBatchPriceDefinition");
				lmsQueueMst.setCronJob(true);
				lmsQueueMst.setJobName(workflow + sourceID);
				lmsQueueMst.setJobGroup(workflow);
				lmsQueueMst.setRepeatTime(60000L);
				lmsQueueMst.setSourceObjectId(sourceID);
				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
				lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);


		}

		return "Success";
	}

}
