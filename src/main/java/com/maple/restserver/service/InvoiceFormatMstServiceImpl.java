package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.InvoiceFormatMst;
import com.maple.restserver.repository.InvoiceFormatMstRepository;

 
@Service
@Transactional


public class InvoiceFormatMstServiceImpl implements InvoiceFormatMstService{
	
	@Autowired
	private InvoiceFormatMstRepository jasperByPriceTypeRepository;

	@Override
	public List<InvoiceFormatMst> findByCompanyMstAndPriceTypeIdAndGstAndDiscount(CompanyMst companyMst,
			String pricetypeid, String gst, String discount,String batch) {
		
		List<InvoiceFormatMst> jasperList = new ArrayList<InvoiceFormatMst>();
		
		jasperList = jasperByPriceTypeRepository.
				findByCompanyMstAndPriceTypeIdAndGstAndDiscountAndBatch(companyMst,pricetypeid,gst,discount,batch);
		
		if(jasperList.size() == 0)
		{
			jasperList = jasperByPriceTypeRepository.findByCompanyMstAndPriceTypeId(companyMst,pricetypeid);
			if(jasperList.size() == 0)
			{
				jasperList = jasperByPriceTypeRepository.findByCompanyMstAndGstAndDiscountAndBatch(companyMst,gst,discount,batch);
				if(discount.equalsIgnoreCase("YES"))
				{
					jasperList = jasperByPriceTypeRepository.findByCompanyMstAndDiscount(companyMst,discount);
				}
				if(jasperList.size() == 0)
				{
					
					jasperList = jasperByPriceTypeRepository.findByCompanyMstAndGst(companyMst,gst);
					
					if(jasperList.size() == 0)
					{
						jasperList = jasperByPriceTypeRepository.findByCompanyMstAndStatus(companyMst,"DEFAULT");

					}

				}
			}

			
		}

		
		return jasperList ;
	}

}
