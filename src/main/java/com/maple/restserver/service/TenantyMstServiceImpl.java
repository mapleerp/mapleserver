package com.maple.restserver.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.TenantyMst;
import com.maple.restserver.repository.TenantyMstRepository;

 
@Service
@Transactional
public class TenantyMstServiceImpl implements TenantyMstService{
	
	@Autowired
	TenantyMstRepository tenantyMstRepository;

	@Override
	public boolean checkTenantyMst(CompanyMst companyMst) {
		boolean flag = false;

		TenantyMst tenantyMst = tenantyMstRepository.findByCompanyMst(companyMst);
		if (null != tenantyMst) {
			if (tenantyMst.getTenantyRoll().equalsIgnoreCase("SINGLE"))
				 
			{
				boolean tenantyKey = checkTenantyKey(tenantyMst.getTenantyKey());
				if (tenantyKey) {
					flag = true;
				}

			}
		}

		return flag;
	}
	
	private boolean checkTenantyKey(String tenantyKey) {
		// TODO Auto-generated method stub
		return true;
	}

}
