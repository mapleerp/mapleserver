package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.report.entity.PurchaseReport;

 

public interface PurchaseDtlService {

	public    List<PurchaseDtl> updatePurchaseDtlDisplaySerial(String purchaseHdr );

	public List<PurchaseReport> getPurchaseConsolidatedReport(Date fromDate, Date toDate, String branch);

}
