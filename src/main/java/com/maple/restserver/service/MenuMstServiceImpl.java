package com.maple.restserver.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MenuConfigMst;
import com.maple.restserver.entity.MenuMst;
import com.maple.restserver.repository.MenuConfigMstRepository;
import com.maple.restserver.repository.MenuMstRepository;

@Service
@Transactional
public class MenuMstServiceImpl implements MenuMstService {

	@Autowired
	MenuConfigMstRepository menuConfigMstRepository;

	@Autowired
	MenuMstRepository menuMstRepository;

	@Override
	public String InitializeCompanyMst(CompanyMst companyMst, String branchcode) {

		List<MenuConfigMst> menuConfigMstPos = menuConfigMstRepository.findByMenuNameAndCompanyMst("POS WINDOW",
				companyMst);
		if (menuConfigMstPos.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstPos.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigMstPos.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);
				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigReportsMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("REPORTS",
				companyMst);
		if (menuConfigReportsMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReportsMst.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigReportsMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		if (menuConfigReportsMst.size() > 0) {
			List<MenuConfigMst> menuConfigDailySalesReportMst = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("DAILY SALES REPORT", companyMst);
			if (menuConfigDailySalesReportMst.size() > 0) {
				MenuMst menuMst = menuMstRepository
						.findByMenuIdAndCompanyMst(menuConfigDailySalesReportMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigDailySalesReportMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigReportsMst.get(0).getId());
					menuMst.setBranchCode(branchcode);

					menuMstRepository.save(menuMst);

				}

			}

		}

		List<MenuConfigMst> menuConfigAccountMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("ACCOUNTS",
				companyMst);
		if (menuConfigAccountMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAccountMst.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigAccountMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigAdvancedFeaturesMst = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("ADVANCED FEATURES", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAdvancedFeaturesMst.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigKotMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("KOT POS",
				companyMst);
		if (menuConfigKotMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKotMst.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigKotMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigWholeSaleMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("WHOLESALE",
				companyMst);
		if (menuConfigWholeSaleMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigWholeSaleMst.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigWholeSaleMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigPosMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("POS WINDOW",
				companyMst);
		if (menuConfigPosMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPosMst.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPosMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigTakeOrderMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("TAKE ORDER",
				companyMst);
		if (menuConfigTakeOrderMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigTakeOrderMst.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigTakeOrderMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigOnlineSalesMst = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("ONLINE SALES", companyMst);
		if (menuConfigOnlineSalesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigOnlineSalesMst.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigOnlineSalesMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigProductionMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("PRODUCTION",
				companyMst);
		if (menuConfigProductionMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProductionMst.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigProductionMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		if (menuConfigProductionMst.size() > 0) {
			List<MenuConfigMst> menuConfigProductionPrePlanningMst = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("PRODUCTION PRE PLANNING", companyMst);

			if (menuConfigProductionPrePlanningMst.size() > 0) {

				MenuMst menuMst = menuMstRepository
						.findByMenuIdAndCompanyMst(menuConfigProductionPrePlanningMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigProductionPrePlanningMst.get(0).getId());
					menuMst.setParentId(menuConfigProductionMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setBranchCode(branchcode);


					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigProductionPlanningMst = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("PRODUCTION PLANNING", companyMst);

			if (menuConfigProductionPlanningMst.size() > 0) {

				MenuMst menuMst = menuMstRepository
						.findByMenuIdAndCompanyMst(menuConfigProductionPlanningMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigProductionPlanningMst.get(0).getId());
					menuMst.setParentId(menuConfigProductionMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setBranchCode(branchcode);


					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigActualProductionMst = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("ACTUAL PRODUCTION", companyMst);

			if (menuConfigActualProductionMst.size() > 0) {

				MenuMst menuMst = menuMstRepository
						.findByMenuIdAndCompanyMst(menuConfigActualProductionMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigActualProductionMst.get(0).getId());
					menuMst.setParentId(menuConfigProductionMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setBranchCode(branchcode);


					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigRawMaterialIssueMst = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("RAW MATERIAL ISSUE", companyMst);

			if (menuConfigRawMaterialIssueMst.size() > 0) {

				MenuMst menuMst = menuMstRepository
						.findByMenuIdAndCompanyMst(menuConfigRawMaterialIssueMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigRawMaterialIssueMst.get(0).getId());
					menuMst.setParentId(menuConfigProductionMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setBranchCode(branchcode);


					menuMstRepository.save(menuMst);

				}

			}
			
			
			List<MenuConfigMst> menuConfigRawMaterialReturnMst = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("RAW MATERIAL RETURN", companyMst);

			if (menuConfigRawMaterialReturnMst.size() > 0) {

				MenuMst menuMst = menuMstRepository
						.findByMenuIdAndCompanyMst(menuConfigRawMaterialReturnMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigRawMaterialReturnMst.get(0).getId());
					menuMst.setParentId(menuConfigProductionMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setBranchCode(branchcode);


					menuMstRepository.save(menuMst);

				}

			}

		}

		List<MenuConfigMst> menuConfigSOMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("SALEORDER",
				companyMst);
		if (menuConfigSOMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSOMst.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigSOMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigPurchaseMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("PURCHASE",
				companyMst);
		if (menuConfigPurchaseMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPurchaseMst.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPurchaseMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigCustMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("CUSTOMER",
				companyMst);
		if (menuConfigCustMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigCustMst.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigCustMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);

				menuMstRepository.save(menuMst);
				menuMst.setBranchCode(branchcode);


			}

		}

		List<MenuConfigMst> menuConfigAcceptStockMst = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("ACCEPT STOCK", companyMst);
		if (menuConfigAcceptStockMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAcceptStockMst.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigAcceptStockMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigSTMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("STOCK TRANSFER",
				companyMst);
		if (menuConfigSTMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSTMst.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigSTMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigDayEndMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("DAY END CLOSURE",
				companyMst);
		if (menuConfigDayEndMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDayEndMst.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigDayEndMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigInvoiceEditMst = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("INVOICE EDIT", companyMst);
		if (menuConfigInvoiceEditMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigInvoiceEditMst.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigInvoiceEditMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigSOEditMst = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("SALES ORDER EDIT", companyMst);
		if (menuConfigSOEditMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSOEditMst.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigSOEditMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setBranchCode(branchcode);


				menuMstRepository.save(menuMst);

			}

		}
		return "Success";
	}

}
