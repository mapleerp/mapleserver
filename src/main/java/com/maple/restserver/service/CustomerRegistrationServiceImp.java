/*
 * package com.maple.restserver.service;
 * 
 * import java.util.ArrayList; import java.util.List;
 * 
 * import org.springframework.stereotype.Service; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.data.domain.Pageable; import
 * org.springframework.stereotype.Component; import
 * org.springframework.transaction.annotation.Transactional;
 * 
 * import com.maple.restserver.entity.CompanyMst; import
 * com.maple.restserver.entity.CustomerMst; import
 * com.maple.restserver.repository.CustomerMstRepository;
 * 
 * 
 * @Service
 * 
 * @Transactional
 * 
 * @Component public class CustomerRegistrationServiceImp implements
 * CustomerRegistrationService {
 * 
 * @Autowired private CustomerMstRepository customerRegRepo;
 * 
 * 
 * public List<CustomerMst> searchLikeCustomerByName(String searchString,String
 * companymstid,Pageable topfifty) { return
 * customerRegRepo.findSearch(searchString.toLowerCase(),companymstid,topfifty);
 * }
 * 
 * 
 * @Override public List<CustomerMst> getCustomers(String pdfFileName) {
 * 
 * 
 * List<CustomerMst> customerRegistrationList = new ArrayList();
 * 
 * List<CustomerMst> customers = customerRegRepo.findAll();
 * 
 * // for(CustomerRegistration i :customers ) // { // // CustomerRegistration
 * customerRegistration= new CustomerRegistration(); //
 * customerRegistration.setCustomerAddress(i.getCustomerAddress()); //
 * customerRegistration.setCustomerContact(i.getCustomerContact()); //
 * customerRegistration.setCustomerGst(i.getCustomerGst()); //
 * customerRegistration.setCustomerMail(i.getCustomerMail()); //
 * customerRegistration.setCustomerName(i.getCustomerName()); //
 * customerRegistration.setCustomerPlace(i.getCustomerPlace()); //
 * customerRegistration.setCustomerState(i.getCustomerState()); //
 * customerRegistrationList.add(customerRegistration); // } //
 * 
 * 
 * 
 * return customers; }
 * 
 * }
 */