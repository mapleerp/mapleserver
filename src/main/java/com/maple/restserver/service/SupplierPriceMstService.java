package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SupplierPriceMst;



@Service

public interface SupplierPriceMstService {

 List<SupplierPriceMst> findAllSupplierPriceMst(CompanyMst companyMst);
}
