package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtlTemp;
import com.maple.restserver.report.entity.ItemBatchDtlReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlHstRepository;
import com.maple.restserver.repository.ItemBatchDtlTempRepository;

import javassist.bytecode.stackmap.BasicBlock.Catch;

@Service
@Transactional
@Component
public class YearEndServiceImpl implements YearEndService{

	@Autowired
	ItemBatchDtlHstRepository itemBatchDtlHstRepository;

	@Autowired
	ItemBatchDtlTempRepository itemBatchDtlTempRepository;
	@Autowired
	CompanyMstRepository  companyMstRepository;
	@Transactional
	@Override
	public  void YearEnd(String companyMstId) {
	
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companyMstId);
		itemBatchDtlHstRepository.insertingItemBatchDtlToItemBatchDtlHst();
		
		  ItemBatchDtlTemp temp=new ItemBatchDtlTemp(); //
//		  itemBatchDtlTempRepository.itembatchDtlInsertingIntoItemBatchDtlTemp();
		  List<ItemBatchDtlReport> itemBatchReportList = new ArrayList();
		
		  List<Object>Obj =itemBatchDtlTempRepository.fetchItemBatchDtl();
		  
		  System.out.println(Obj.size()+"object list size issssssssssssssssssssssssssssss");
		
		  for(int i=0;i<Obj.size();i++) {
		  
		  Object[] objAray = (Object[]) Obj.get(i);
		  ItemBatchDtlReport dtl=new ItemBatchDtlReport(); 
		  
		  dtl.setItemId((String) objAray[0]);
		  dtl.setBatch((String) objAray[1]);
		  dtl.setBarcode((String) objAray[2]);
		  dtl.setQtyIn((Double) objAray[3]);
		  dtl.setQtyOut((Double) objAray[4]);
		  dtl.setMrp((Double) objAray[5]); 
		  dtl.setExpDate((Date) objAray[6]);
		  dtl.setBranchcode((String) objAray[7]); 
		  dtl.setStore((String) objAray[8]);
		  dtl.setCompanyMst((String) objAray[9]); 
		  itemBatchReportList.add(dtl); }
		  for(ItemBatchDtlReport report:itemBatchReportList) {
		  for(int i=0;i<itemBatchReportList.size();i++)
		  System.out.print(itemBatchReportList.size()+"item id issssssssssssssssssssssssssssss" );
		  temp.setItemId(report.getItemId()); 
		  temp.setBatch(report.getBatch());
		  temp.setBranchCode(report.getBranchcode());
		  temp.setCompanyMst(companyMstOpt.get()); 
		  temp.setMrp(report.getMrp());
		  temp.setQtyIn(report.getQtyIn()); 
		  temp.setQtyOut(report.getQtyOut());
		  temp.setStore(report.getStore());
		  temp.setExpDate(report.getExpDate());
		  itemBatchDtlTempRepository.saveAndFlush(temp); }
		  
		  itemBatchDtlHstRepository.truncateItemBatchDtl();
		  itemBatchDtlTempRepository.InsertingInToItemBatchDtlFromItemBatchDtlTemp();
		 
		  
	}}
	
	


