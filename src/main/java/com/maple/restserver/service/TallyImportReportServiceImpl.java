package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.report.entity.TallyImportReport;
import com.maple.restserver.repository.SalesTransHdrRepository;
@Service
@Transactional
@Component
public class TallyImportReportServiceImpl implements TallyImportReportService {

	
	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	
	@Override
	public List<TallyImportReport> tallyImportReportReport(String companymstid, String branchcode, Date fDate,
			Date TDate) {
		
		/*
		 * 0 + " h.voucher_number," 1 + "h.voucher_date, " 2 + "h.voucher_type ," 3 +
		 * "cu.customer_name ," 4 + "cu.customer_address ," 5 + "cu.customer_contact," 6
		 * + "cu.customer_type ," 7 + "cu.customer_gst," 8 + "cu.customer_state," 9 +
		 * "cu.customer_country," 10 + "cu.bank_name," 11 + "cu.bank_account_name," 12 +
		 * "cu.bank_ifsc," 13 + "i.item_name," 14 + "i.hsn_code," 15 +
		 * "c.category_name ," 16 + "i.tax_rate ," 17 + " d.qty,"
		 * 
		 * //version 1.3.1 18 + "d.rate," //Was d.rate //version 1.3.1 Ends
		 * 
		 * 19 + "d.cgst_amount," 20 + "d.sgst_amount," 21 + "d.igst_amount," 22 +
		 * "d.cess_amount," 23 + "d.discount," 24 + "u.unit_name," 25 +
		 * "h.invoice_amount," 26 + "d.amount"
		 */
		

		List<TallyImportReport> tallyImportReportList= new ArrayList();
		
		 List<Object> obj = salesTransHdrRepository.tallyImportReportReport(companymstid,branchcode,fDate,TDate);
		 for(int i=0;i<obj.size();i++)
		 {
			 
			 try {
			 Object[] objAray = (Object[]) obj.get(i);
			 TallyImportReport tallyImportReport =new TallyImportReport();
			 if(null!=(String) objAray[0]) {
			 tallyImportReport.setVoucherNumber((String) objAray[0]);
			 }
			 if(null!=(Date) objAray[1]) {
			 String invoiceDate=ClientSystemSetting.ddMMMyyUtilDateToString((Date) objAray[1]);
			 tallyImportReport.setInvoiceDate(invoiceDate);
			 }
			 if(null!=(String) objAray[2])
			 tallyImportReport.setVoucherType((String) objAray[2]);
			 if(null!=(String) objAray[3]) {
				 String contactgNumber=(String) objAray[5];
				 String customerNameWithContact = (String) objAray[3];
				 String customerNameWithOutContact = customerNameWithContact.replace("-"+contactgNumber, "");
				 String customerNameWithOutSpaceAndContact=customerNameWithOutContact.replace(""+"contactgNumber","");
				 String customerNameWithoutHyphen=customerNameWithOutSpaceAndContact.replaceAll("-", " ");
			 tallyImportReport.setCustomerName(customerNameWithoutHyphen);
			 }
			 if(null!=(String) objAray[4]) {
			 tallyImportReport.setAddressLine1((String) objAray[4]);
			 }if(null!=(String) objAray[5]) {
			 tallyImportReport.setAddressLine2("");
			 }if(null!=(String) objAray[6]) {
			 tallyImportReport.setGstRegType((String) objAray[6]);
			 }if(null!=(String) objAray[7]) {
	    	 tallyImportReport.setGstNumber((String) objAray[7]);
			 }if(null!=(String) objAray[8]) {
				 tallyImportReport.setState((String) objAray[8]); 
			 }
			if(null!=(String) objAray[9]) {
			 tallyImportReport.setCountry((String) objAray[9]);
			}
			if(null!=(String) objAray[10]) {
			 tallyImportReport.setBankName((String) objAray[10]);
			}if(null!=(String) objAray[11]) {
			 tallyImportReport.setBankAccountName((String) objAray[11]);
			}if(null!=(String) objAray[12]) {
			 tallyImportReport.setBankIfsc((String) objAray[12]);
			}if(null!=(String) objAray[13]) {
			 tallyImportReport.setItemName((String) objAray[13]);
			}if(null!=(String) objAray[14]) {
			 tallyImportReport.setHsncode((String) objAray[14]);
			}if(null!=(String) objAray[15]) {
			 tallyImportReport.setGroup((String) objAray[15]);
			}if(null!=(Double) objAray[16]) {
				 tallyImportReport.setTax((Double) objAray[16]);
			}
			if(null!=(Double) objAray[17]) {
			 tallyImportReport.setQuantity((Double) objAray[17]);
			}
			tallyImportReport.setRate(0.0);
			if(null!=(Double) objAray[18]) {
	    	 
				
				tallyImportReport.setRate((Double) objAray[18]);
	    	 
			}if(null!=(Double) objAray[19]) {
			 tallyImportReport.setItemcgst((Double) objAray[19]);
			}if(null!=(Double) objAray[20]) {
			 tallyImportReport.setItemsgst((Double) objAray[20]);
			}if(null!=(Double) objAray[21]) {
			 tallyImportReport.setItemigst((Double) objAray[21]);
			}if(null!=(Double) objAray[22]) {
			 tallyImportReport.setKeralaFloodCess((Double) objAray[22]);
			}if(null!=(Double) objAray[23]) {
			 tallyImportReport.setDiscount((Double) objAray[23]);
			}if(null!=(String) objAray[24]) {
				 tallyImportReport.setUnit((String) objAray[24]);
			}
			if(null!=(Double) objAray[25]) {
			 tallyImportReport.setNetValue((Double) objAray[25]);
			}
			 if( tallyImportReport.getTax()>0) {
				 tallyImportReport.setTaxability("taxable");
			 }else {
				 tallyImportReport.setTaxability("nontaxable");
			 }
			 if(null!=(Date) objAray[1]) {
				 String datOfSupply=ClientSystemSetting.ddMMMyyUtilDateToString((Date) objAray[1]);
			 tallyImportReport.setDateOfSupply(datOfSupply);
			 }
			
			 tallyImportReport.setValue(tallyImportReport.getRate()*tallyImportReport.getQuantity());

		
			 if(null==tallyImportReport.getRateBeforeDiscount()) {
				 tallyImportReport.setRateBeforeDiscount(tallyImportReport.getRate());
			 }
			 
			 tallyImportReport.setRoundOff(0.0);
			 Double total=+tallyImportReport.getNetValue();
			 tallyImportReport.setTotalAnount(total);
			 if(null != (Double)objAray[27])
			 {
				 tallyImportReport.setRateBeforeDiscount((Double)objAray[27]);
				 tallyImportReport.setTaxable(tallyImportReport.getRateBeforeDiscount()*tallyImportReport.getQuantity());
			 }
			 
			 tallyImportReport.setNetValue(tallyImportReport.getTaxable()+tallyImportReport.getItemcgst()+tallyImportReport.getItemsgst()+tallyImportReport.getKeralaFloodCess()+tallyImportReport.getItemigst());

			 tallyImportReportList.add(tallyImportReport);
			 
			 }catch(Exception e) {
				 System.out.println(e.toString());
			 }

	}

		 return tallyImportReportList;
	
	}

}
