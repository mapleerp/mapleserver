package com.maple.restserver.service;

import java.util.ArrayList;

import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.maple.restserver.cloud.api.ExternalApi;

import com.maple.restserver.entity.LastBestDate;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesMessageEntity;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.LastBestDateRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.utils.SystemSetting;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SalesDataSync implements DataSynchronisationService {

	Logger logger = LogManager.getLogger(SalesDataSync.class);

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	BranchMstRepository branchMstRepository;

	@Autowired
	SalesTransHdrService salesTransHdrService;

	@Autowired
	DayEndClosureRepository dayEndClosureRepository;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepo;

	@Autowired
	SalesDetailsRepository salesdtlDtlRepo;

	@Autowired
	SalesReceiptsRepository salesReceiptsRepository;

	@Autowired
	ExternalApi externalApi;

	@Autowired
	LastBestDateRepository lastBestDateRepository;

	@Autowired
	LastBestDateServiceSales lastBestDateServiceSales;

	@Autowired
	DataSynchronisationService dataSynchronisationService;

	private int salesCount = 0;
	private LastBestDate lastBestDate = null;

	String branchCode;
	String companyMstId;

	private CloudDataCount cloudDataCount;

	public CloudDataCount getCloudDataCount() {
		return cloudDataCount;
	}

	public void setCloudDataCount(CloudDataCount cloudDataCount) {
		this.cloudDataCount = cloudDataCount;
	}

	@Override
	public int getLocalCount() {

		System.out.println("Entered into salesDataSync.getLocalCount() method****************************");
		
		if(null == lastBestDate)
		{
			return 0;
		}

		salesCount = salesTransHdrRepo.getCountofSalesForDateWindow(cloudDataCount.getCompanymstid(),
				lastBestDate.getLastSuccessDate(), lastBestDate.getLastSuccessDate(), cloudDataCount.getBranchcode());
		System.out.println("The result of salesDataSync.getLocalCount() is:" + salesCount);
		return salesCount;
	}

	@Override
	public void countComparAndRetry() {

		System.out.println("Entered into salesDataSync.countComparAndRetry() method****************************");

		boolean canRetry = false;

		Double serverDataCountInDouble = cloudDataCount.getServerCount("SALES");
		int serverDataCount = serverDataCountInDouble.intValue();

		int localDataCount = getLocalCount();

		System.out.println("The Local Server count of sales are ====> " + localDataCount);
		System.out.println("The Cloud Server count of sales are ====> " + serverDataCount);

		canRetry = serverDataCount < localDataCount;

		if (canRetry) {
			retry();

		} else {
			updateLastBestDate();

		}

	}

	@Override
	public String retry() {

		int pageNo = 0;
		int pageSize = 50;

		System.out.println("Entered into salesDataSync.retry() method****************************");

		logger.info("Building salesMessageEntity Starting");
		
		
		String date = SystemSetting.UtilDateToString(lastBestDate.getLastSuccessDate(), "yyyy-MM-dd");
		
		String hdrIdsFromCloud = externalApi.SalesTransHdrIdsFromCloud(date);
		
		if(null == hdrIdsFromCloud)
		{
			hdrIdsFromCloud = "";
		}


		int recordCount = 0;

		while (true) {

			Pageable topFifty = PageRequest.of(pageNo, pageSize);

			ArrayList<SalesMessageEntity> salesMessageEntityarray = new ArrayList<SalesMessageEntity>();
			
			

			List<SalesTransHdr> hdrIdS = salesTransHdrRepo.getSalesTransHdrBetweenDate(
					lastBestDate.getLastSuccessDate(), lastBestDate.getLastSuccessDate(), topFifty,hdrIdsFromCloud);

			if (hdrIdS.size() == 0) {
				break;
			}

			Iterator<SalesTransHdr> iter = hdrIdS.iterator();

			while (iter.hasNext()) {

				String msg = String.format("Building salesMessageEntity recordCount: %d", recordCount);

				logger.info(msg);

				SalesMessageEntity salesMessageEntity = new SalesMessageEntity();

				SalesTransHdr transHdr = (SalesTransHdr) iter.next();

				salesMessageEntity.setSalesTransHdr(transHdr);

				List<SalesDtl> salesDtl = salesdtlDtlRepo.findBySalesTransHdrId(transHdr.getId());

				List<SalesReceipts> salesReceipts = salesReceiptsRepository.findBySalesTransHdr(transHdr);

				salesMessageEntity.getSalesDtlList().addAll(salesDtl);

				salesMessageEntity.getSalesReceiptsList().addAll(salesReceipts);

				salesMessageEntityarray.add(salesMessageEntity);

				recordCount++;

			}

			logger.info("Send salesMessageEntity Array*******************************");

			List<String> message = externalApi.BulkSalesToCloud(salesMessageEntityarray,
					lastBestDate.getCompanyMst().getId());

			System.out
					.println("The result of externalApi.BulkSalesToCloud() in salesDataSync.retry() ====> " + message);

			pageNo++;
			logger.info("Send salesMessageEntity Array");
		}
		return "OK";

	}

	private void updateLastBestDate() {

		System.out.println("Entered into SalesDataSync.updateLastBestDate() method****************************");

//		lastBestDate.setLastSuccessDate(lastBestDate.getDayEndDate());
		System.out.println("The result of salesDataSync.updateLastBestDate() ====> " + lastBestDate);
		if(null == lastBestDate)
		{
			return;
		}
		lastBestDateRepository.save(lastBestDate);

	}

	@Override
	public LastBestDate getLastBestDate() {

		System.out.println("Entered into SalesDataSync.getLastBestDate() method****************************");

		lastBestDateServiceSales.setBranchcode(branchCode);
		lastBestDateServiceSales.setCompanymstid(companyMstId);
		lastBestDate = lastBestDateServiceSales.findLastBestDate();
		System.out.println("The result of SalesDataSync.getLastBestDate() method ====> " + lastBestDate);
		return lastBestDate;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCompanyMstId() {
		return companyMstId;
	}

	public void setCompanyMstId(String companyMstId) {
		this.companyMstId = companyMstId;
	}

}
