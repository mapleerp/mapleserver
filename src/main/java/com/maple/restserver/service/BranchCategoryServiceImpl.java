package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.BranchCategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.CustomerCategorywiseStockReport;
import com.maple.restserver.repository.BranchCategoryMstRepository;

@Service
public class BranchCategoryServiceImpl implements BranchCategoryMstService
{
	@Autowired
	BranchCategoryMstRepository branchCategoryMstRepository;

	@Override
	public List<BranchCategoryMst> retrieveBranchCategory(CompanyMst companyMst) {
		List<BranchCategoryMst> branchCategoryMstList=new ArrayList<BranchCategoryMst>();
		 
		List<Object> obj=branchCategoryMstRepository.retrieveBranchCategory(companyMst);
		System.out.print(obj.size()+"report list size is ");
		
		for(int i=0;i<obj.size();i++)
		 {
			BranchCategoryMst reportDtl=new BranchCategoryMst();
			
		Object[] objAray = (Object[]) obj.get(i);
		reportDtl.setBranchCode((String) objAray[0]);
		reportDtl.setCategoryId((String) objAray[1]);
		branchCategoryMstList.add(reportDtl);
		
	 }

		
	
		return branchCategoryMstList;
	}

}
