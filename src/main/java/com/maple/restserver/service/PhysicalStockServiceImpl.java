package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PhysicalStockDtl;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.report.entity.PharmacyPhysicalStockVarianceReport;
import com.maple.restserver.report.entity.PhysicalStockReport;
import com.maple.restserver.report.entity.PoliceReport;
import com.maple.restserver.report.entity.StockMigrationReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PhysicalStockHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
@Transactional
@Component
@Service
public class PhysicalStockServiceImpl implements PhysicalStockService {

	//----------------version 5.0 surya 
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	//----------------version 5.0 surya end
	
	@Autowired
	PhysicalStockHdrRepository physicalStockHdrRepository;
	
	@Autowired
	ItemMstRepository itemMstRepo;
	@Autowired
	UnitMstRepository unitMstRepo;
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepo;
	@Autowired
	ItemBatchMstRepository itemBatchMstRepo;
	@Autowired
	private VoucherNumberService voucherService;

	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	private CompanyMstRepository companyMstRepository;
	
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	@Override
	public ArrayList<ItemMst> updatePhyisicalStock(String barcode, String itemName, double qty, double mrp,
			String batchCode , Date expiryDate,String  companymstid,
			String branchCode , String voucherNumber , Date VoucherDate, String store ) {
		ArrayList<ItemMst> resultArray = new ArrayList();

		ItemMst item = itemMstRepo.findByItemName(itemName) ;

		ItemBatchMst itemBatchMst = null;
		
		 

		if (null == item || null == item.getItemName()) {
			
			// If item not found return
			return null;
		
			
			
			/*
           Optional<CompanyMst> companymst=companyMstRepository.findById(companymstid);
           
			item = new ItemMst();
			item.setCompanyMst(companymst.get());
			item.setBarCode(barcode);
			item.setItemName(itemName);

			String unitName = SystemSetting.DEFAULT_UNIT;
			UnitMst unitMst = unitMstRepo.findByUnitNameAndCompanyMstId(unitName,companymstid);

			Double taxrate = SystemSetting.DEFAULT_TAXRATE;
			item.setTaxRate(taxrate);

			VoucherNumber itemCode = voucherService.generateInvoice(SystemSetting.ITEM_CODE_GENERATOR_ID,companymstid);

			VoucherNumber itemId = voucherService.generateInvoice(SystemSetting.ITEM_CODE_GENERATOR_ID,companymstid);

			item.setId("MOB"+itemId.getCode());
			
			item.setItemCode(itemCode.getCode());

			Double sellprice = mrp;
			item.setStandardPrice(sellprice);

			Double statecess = SystemSetting.DEFAULT_STATECESS;
			item.setCess(statecess);

			String hsncode = "";
			item.setHsnCode(hsncode);

			item.setUnitId(unitMst.getId());

			itemMstRepo.save(item);
			
			*/
			

		} 

		// * 2. find if item batychmst has record . if found update else insert.

		// Physical stock should manged as delta stock

		Double systemQty = 0.0;
		
		

		// List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
		// .findItemNameByBarCodeAndQty(item.getBarCode() ,item.getItemName(), qty);

//		List<ItemBatchMst> itembatchmst = null;
//		
//		if(batchCode.trim().length()>0 ){
//		  itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
//				.findByItemIdAndBatchAndBarcode(item.getId(), batchCode, barcode);
//		}else  if(barcode.trim().length() >0 ){
//			 itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
//						.findByItemIdAndBarcode(item.getId(), barcode);
//		}else {
//			 itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
//						.findByItemId(item.getId() );
//		}
		
		
		
		 Optional<CompanyMst> companymst=companyMstRepository.findById(companymstid);
         
//		
//		if (itembatchmst.size() >= 1) {
//			itemBatchMstRepo.deleteByItemIdAndBatch(item.getId().trim(),batchCode.trim());
//
//			
//			itemBatchMst = new ItemBatchMst();
//			itemBatchMst.setCompanyMst(companymst.get());
//			itemBatchMst.setBarcode(item.getBarCode());
//			itemBatchMst.setBranchCode(branchCode);
//			itemBatchMst.setBatch(batchCode);
//			itemBatchMst.setMrp(item.getStandardPrice());
//			itemBatchMst.setQty(qty);
//			itemBatchMst.setItemId(item.getId());
//			itemBatchMst.setExpDate(expiryDate);
//			itemBatchMst = itemBatchMstRepo.saveAndFlush(itemBatchMst);
//			
			
			
//			itemBatchMst = itembatchmst.get(0);
//			
//		
//			
//		 
//			itemBatchMst = itembatchmst.get(0);
//			
//			itemBatchMst.setQty(qty);

//			itemBatchMst = itemBatchMstRepo.save(itemBatchMst);

//		} else {
//		
//			itemBatchMst = new ItemBatchMst();
//			itemBatchMst.setCompanyMst(companymst.get());
//			itemBatchMst.setBarcode(item.getBarCode());
//			itemBatchMst.setBranchCode(branchCode);
//			itemBatchMst.setBatch(batchCode);
//			itemBatchMst.setMrp(item.getStandardPrice());
//			itemBatchMst.setQty(qty);
//			itemBatchMst.setItemId(item.getId());
//			itemBatchMst.setExpDate(expiryDate);
//			itemBatchMstRepo.save(itemBatchMst);
//
//		}
		 String processInstanceId=null;
		 String taskId=null;
		 
		 itemBatchDtlService.itemBatchMstUpdation(item.getBarCode(),batchCode,branchCode,expiryDate,item.getId(),
				 item.getStandardPrice(),processInstanceId,qty,taskId,companymst.get(),store);
		 
		 
	//	if(null==item.getBarCode()) {
	//		return null;
	//	}
		 System.out.println("Date = "+VoucherDate.toString());
		 java.sql.Date VoucherDateSql = SystemSetting.UtilDateToSQLDate(VoucherDate);
		 
		 
		if(null!=batchCode && batchCode.trim().length()>0) {
		systemQty = itemBatchMstRepo.getClosingQtyItemIdAndBathAndBarcodeAsOnDate(
				companymst.get().getId(), 
				item.getId(),
				batchCode,
				item.getBarCode(),
				VoucherDateSql);
		}else {
			systemQty = itemBatchMstRepo.getClosingQtyItemIdAndAndBarcodeAsOnDate(
					companymst.get().getId(), 
					item.getId(),
				 
					item.getBarCode(),
					VoucherDateSql);
		}
		
		
		
		
		
		System.out.println("System Qty = " + systemQty);
		
		if(null==systemQty) {
			systemQty=0.0;
			
		}
		
		Double delta = systemQty - qty;
		Double QtyIn = 0.0;
		Double QtyOut = 0.0;
		if (delta < -0.01) {
			delta = delta * -1;
			QtyIn = delta;
			QtyOut = 0.0;
		}else if (delta > 0.01) {
			QtyOut = delta;
			QtyIn = 0.0;
			
		}else if (delta == 0) {
			QtyOut = 0.0;
			QtyIn = 0.0;
		}
		VoucherNumber voucherNo = voucherService.generateInvoice(SystemSetting.VOUCHER_NAME_GENERATOR_ID,companymst.get().getId());
		
		String sourceDetailId = null;
		String sourceParentId = null;
		//----------------version 5.0 surya 
//		itemBatchDtlService.updatePhysicalStock(companymst.get(),item,batchCode,mrp,expiryDate,systemQty,qty,voucherNumber,VoucherDate,branchCode);
		itemBatchDtlService.itemBatchDtlInsertionNew(item.getBarCode(),batchCode,branchCode,expiryDate,item.getId(),mrp,"PHYSICAL STOCK ENTRY",processInstanceId,
				QtyIn,QtyOut,sourceDetailId,sourceParentId,VoucherDate,voucherNumber,store,taskId,VoucherDate,voucherNo.getCode(),companymst.get());
		
		resultArray.add(item);

		//----------------version 5.0 surya end
//		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
//		itemBatchDtl.setCompanyMst(companymst.get());
//		itemBatchDtl.setBarcode(item.getBarCode());
//		itemBatchDtl.setBatch(batchCode);
//		itemBatchDtl.setItemId(item.getId());
//		itemBatchDtl.setBranchCode(branchCode);
//		itemBatchDtl.setParticulars("Physical Stock Entry");
//		itemBatchDtl.setMrp(mrp);
//		itemBatchDtl.setExpDate(expiryDate);
//
//	
//		double delta = systemQty - qty ;
//
//
//		System.out.println("delta Qty = " + delta);
//		
//		 
//if (delta < -0.01  ) {
//	delta = delta * -1 ;
//	itemBatchDtl.setQtyIn(delta);
//	itemBatchDtl.setQtyOut(0.0);
//	
//	
//	itemBatchDtl.setVoucherDate(VoucherDate);
//
//	VoucherNumber voucherNo = voucherService.generateInvoice(SystemSetting.VOUCHER_NAME_GENERATOR_ID,companymst.get().getId());
//
//	itemBatchDtl.setVoucherNumber(voucherNo.getCode());
//
//	itemBatchDtl.setSourceVoucherNumber(voucherNumber);
//	itemBatchDtl.setSourceVoucherDate(VoucherDate);
//	itemBatchDtl.setBranchCode(branchCode);
//	itemBatchDtl.setStore("MAIN");
//	itemBatchDtl = itemBatchDtlRepo.saveAndFlush(itemBatchDtl);
//
//	resultArray.add(item);
//	Map<String, Object> variables = new HashMap<String, Object>();
//
//	variables.put("voucherNumber", itemBatchDtl.getId());
//	variables.put("id", itemBatchDtl.getId());
//	variables.put("voucherDate", SystemSetting.getSystemDate());
//	variables.put("inet", 0);
//	variables.put("REST", 1);
//	variables.put("companyid", itemBatchDtl.getCompanyMst());
//	variables.put("WF", "forwardItemBatchDtl");
//
//	LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//	lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//
//	java.util.Date uDate = (Date) variables.get("voucherDate");
//	java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(uDate);
//
//	uDate = SystemSetting.SqlDateToUtilDate(sqlDate);
//	lmsQueueMst.setVoucherDate(sqlDate);
//
//	// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//	lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//	lmsQueueMst.setVoucherType("forwardItemBatchDtl");
//	lmsQueueMst.setPostedToServer("NO");
//	lmsQueueMst.setJobClass("forwardItemBatchDtl");
//	lmsQueueMst.setCronJob(true);
//	lmsQueueMst.setJobName("forwardItemBatchDtl" + itemBatchDtl.getId());
//	lmsQueueMst.setJobGroup("forwardItemBatchDtl");
//	lmsQueueMst.setRepeatTime(60000L);
//	lmsQueueMst.setSourceObjectId(itemBatchDtl.getId());
//
//	lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//	lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//	variables.put("lmsqid", lmsQueueMst.getId());
//
//	eventBus.post(variables);
//	
//	
//}else if(delta > 0.01 ){ 
//	
//	//delta = delta * -1 ;
//	itemBatchDtl.setQtyOut(delta);
//	itemBatchDtl.setQtyIn(0.0);
//	
//	
//	itemBatchDtl.setVoucherDate(VoucherDate);
//
//	VoucherNumber voucherNo = voucherService.generateInvoice(SystemSetting.VOUCHER_NAME_GENERATOR_ID,companymst.get().getId());
//
//	itemBatchDtl.setVoucherNumber(voucherNo.getCode());
//
//	itemBatchDtl.setSourceVoucherNumber(voucherNumber);
//	itemBatchDtl.setSourceVoucherDate(VoucherDate);
//	itemBatchDtl.setBranchCode(branchCode);
//	itemBatchDtl.setStore("MAIN");
//	itemBatchDtl = itemBatchDtlRepo.saveAndFlush(itemBatchDtl);
//	
//	resultArray.add(item);
//	
//	
//	Map<String, Object> variables = new HashMap<String, Object>();
//
//	variables.put("voucherNumber", itemBatchDtl.getId());
//	variables.put("id", itemBatchDtl.getId());
//	variables.put("voucherDate", SystemSetting.getSystemDate());
//	variables.put("inet", 0);
//	variables.put("REST", 1);
//	variables.put("companyid", itemBatchDtl.getCompanyMst());
//	variables.put("WF", "forwardItemBatchDtl");
//
//	LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//	lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//
//	java.util.Date uDate = (Date) variables.get("voucherDate");
//	java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(uDate);
//
//	uDate = SystemSetting.SqlDateToUtilDate(sqlDate);
//	lmsQueueMst.setVoucherDate(sqlDate);
//
//	// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//	lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//	lmsQueueMst.setVoucherType("forwardItemBatchDtl");
//	lmsQueueMst.setPostedToServer("NO");
//	lmsQueueMst.setJobClass("forwardItemBatchDtl");
//	lmsQueueMst.setCronJob(true);
//	lmsQueueMst.setJobName("forwardItemBatchDtl" + itemBatchDtl.getId());
//	lmsQueueMst.setJobGroup("forwardItemBatchDtl");
//	lmsQueueMst.setRepeatTime(60000L);
//	lmsQueueMst.setSourceObjectId(itemBatchDtl.getId());
//
//	lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//	lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//	variables.put("lmsqid", lmsQueueMst.getId());
//
//	eventBus.post(variables);
//	
//
//	
//}else if (delta==0) {
//	itemBatchDtl.setQtyOut(0.0);
//	itemBatchDtl.setQtyIn(0.0);
//
//}
//		
//		
//
//
//
//	
		

		return resultArray;

	}

	public List<PhysicalStockReport> getPhysicalStockDtlReport(String companymstid, Date sdate,
			Date edate) {
		
		List<PhysicalStockReport> physicalStockReportList = new ArrayList<PhysicalStockReport>();
		
		List<Object> objList = physicalStockHdrRepository.getPhysicalStockDtlReport(companymstid,sdate,edate);
		for(int i=0; i<objList.size();i++)
		{
			Object[] objArray = (Object[]) objList.get(i); 
			
			Double qty = 0.0;
			Double mrp = 0.0;
			
			PhysicalStockReport physicalStockReport = new PhysicalStockReport();
			
			physicalStockReport.setBatch((String) objArray[7]);
			physicalStockReport.setExpiryDate((Date) objArray[3]);
			physicalStockReport.setItemName((String) objArray[2]);
			physicalStockReport.setManufactureDate((Date) objArray[6]);
			physicalStockReport.setMrp((Double) objArray[4]);
			physicalStockReport.setQty((Double) objArray[5]);
			physicalStockReport.setVoucherDate((Date) objArray[1]);
			physicalStockReport.setVoucherNumber((String) objArray[0]);
			
			qty = (Double) objArray[5];
			mrp = (Double) objArray[4];
			if(null == qty)
			{
				qty = 0.0;
			}
			
			if(null == mrp)
			{
				mrp = 0.0;
			}
			physicalStockReport.setAmount(qty*mrp);

			physicalStockReportList.add(physicalStockReport);
		}
		
		return physicalStockReportList;
		
	}
	@Override
	public List<PharmacyPhysicalStockVarianceReport> findPhysicalStockVarianceReport(Date fromDate, Date toDate) {

		List<PharmacyPhysicalStockVarianceReport> PharmacyPhysicalStockVarianceReportList = new ArrayList<PharmacyPhysicalStockVarianceReport>();
		List<Object> obj = physicalStockHdrRepository.findPhysicalStockVarianceReport(fromDate, toDate);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			PharmacyPhysicalStockVarianceReport physicalStockVarianceReport = new PharmacyPhysicalStockVarianceReport();
			physicalStockVarianceReport.setStockcorrectionDate(SystemSetting.UtilDateToString((Date) objAray[0], "yyyy-MM-dd"));
			physicalStockVarianceReport.setVoucherNumber((String) objAray[1]);
			physicalStockVarianceReport.setItemGroup((String) objAray[2]);
			physicalStockVarianceReport.setItemName((String) objAray[2]);
			physicalStockVarianceReport.setItemCode((String) objAray[3]);
			physicalStockVarianceReport.setBatchCode((String) objAray[4]);
			physicalStockVarianceReport.setUserName((String) objAray[5]);
			physicalStockVarianceReport.setSystemQty((Double) objAray[6]);
			physicalStockVarianceReport.setPhysicalQty((Double) objAray[7]);
			physicalStockVarianceReport.setVarianceQty((Double) objAray[8]);
			physicalStockVarianceReport.setPrice((Double) objAray[8]);
			physicalStockVarianceReport.setCost((Double) objAray[8]);
			PharmacyPhysicalStockVarianceReportList.add(physicalStockVarianceReport);

		}

		return PharmacyPhysicalStockVarianceReportList;
	}

 
}
