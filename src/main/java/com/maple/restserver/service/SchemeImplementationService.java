package com.maple.restserver.service;

import java.util.Date;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesDtl;

@Service
public interface SchemeImplementationService {

	String offerCheckingAndOfferApply(SalesDtl salesDtl,CompanyMst companyMst,Date loginDate);

	String publishSchemeToAllBranches(String schemename, String companymstid);

	

//	void finalSchemePublish(String id, CompanyMst companyMst, String mybranch);

}
