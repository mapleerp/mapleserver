package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

 
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.report.entity.ReceiptReport;
import com.maple.restserver.report.entity.ReceiptReports;
@Service
@Transactional
public interface ReceiptReportService {
	
	public  List<Object> getDailyReceipt(String voucherNo);
	public   List<ReceiptReport> getAllreceipt();

	List<ReceiptHdr>fetchReceiptReportHdr(CompanyMst companyMst,String branchcode,java.util.Date fromDate,java.util.Date toDate);

	
	List<ReceiptReports>exportReceiptReport(CompanyMst companyMst,Date fromdate,Date todate,String branchcode);
	
	List<ReceiptDtl>fetchByReceiptHdrId(String receipthdrId);
	
	
	List<ReceiptReports>receiptReportPrinting(CompanyMst companyMst,Date fromdate,Date todate,String branchcode);
	
}
