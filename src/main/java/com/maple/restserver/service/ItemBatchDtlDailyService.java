package com.maple.restserver.service;

import java.net.UnknownHostException;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.StockTransferInDtl;
import com.maple.restserver.entity.StockTransferInHdr;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.report.entity.CategoryWiseStockMovement;

@Service

@Component
public interface ItemBatchDtlDailyService {

	void updateItemBatchDtlDailyFromItemBatchDtl(CompanyMst companyMst, Date aplicationDate);

	
	
	String stockVerificationForStkTransfer(String companymstid, String hdrId);
	
	String stockVerificationForDamageEntry(String companymstid, String hdrId);

	String stockVerificationForProdcutConversion(String companymstid, String prdcthdrid);
	
	
	
}
