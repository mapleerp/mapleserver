package com.maple.restserver.service;

import java.sql.Date;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;

@Component
public interface ItemBatchDtlDeleteTempService {

	void updateItemBatchDtlDeleteTempFromItemBatchDtl(CompanyMst companyMst, Date aplicationDate);

}
