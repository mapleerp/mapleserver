package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.WeighBridgeWeights;
import com.maple.restserver.repository.WeighBridgeWeightsRepository;

@Service
@Transactional
public class WeighBridgeReportServiceImpl  implements WeighBridgeReportService {

	@Autowired
	WeighBridgeWeightsRepository weighBridgeWeightsRepository;
	@Override
	public List<WeighBridgeWeights> getWeighBridgeReport( String companymstid, Date fdate,
			Date tdate) {
	
		List<WeighBridgeWeights>  weighBridgeReportList=weighBridgeWeightsRepository.getWeighBridgeReport( companymstid, fdate );
		
		/*
		 * 
		 * List<WeighBridgeWeights>obj=weighBridgeWeightsRepository.
		 * getWeighBridgeReport( companymstid, fdate ); // ,
		 * 
		 * System.out.print(obj.size()
		 * +"object size isssssssssssssssssssssssssssssssssssssssssssssss"); for(int
		 * i=0;i<obj.size();i++)
		 * 
		 * { Object[] objAray = (Object[]) obj.get(i);
		 * 
		 * WeighBridgeReport report=new WeighBridgeReport();
		 * report.setVehicleno((String) objAray[0]); report.setRate((Integer)
		 * objAray[1]); // java.util.Date voucherDate=SystemSetting.tToUtilDate(); //
		 * String vDate=SystemSetting.UtilDateToString(voucherDate);
		 * report.setVoucherDate((LocalDate) objAray[2]); report.setNetweight((String)
		 * objAray[3]); weighBridgeReportList.add(report);
		 * 
		 * System.out.print(weighBridgeReportList.size()
		 * +"weigh bridge report size isssssssssssssssssssssssss");
		 * 
		 * }
		 * 
		 */
		return weighBridgeReportList;
	}

}
