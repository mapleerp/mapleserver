package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DailySalesSummary;
import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.entity.ReceiptModeMst;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.report.entity.DailySalesReport;
import com.maple.restserver.report.entity.ReceiptModeReport;
import com.maple.restserver.report.entity.SaleModeWiseSummaryReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.DailySalesSummaryRepository;
import com.maple.restserver.repository.ReciptModeMstRepository;
import com.maple.restserver.repository.SaleOrderReceiptRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component
public class DailySaleSummaryServiceImpl implements DailySaleSummaryService {

	@Autowired
	SalesReceiptsRepository salesReceiptsRepo;
	// ------------version 6.7 surya
	@Autowired
	SalesDetailsRepository salesDetailsRepository;
	// ------------version 6.7 surya end

	@Autowired
	SaleOrderReceiptRepository saleOrderReceiptRepo;
	@Autowired
	private AccountHeadsRepository accountHeadsRepo;
	@Autowired
	BranchMstRepository brnchMstRepo;
	@Autowired
	DailySalesSummaryRepository dailySaleSummaryRepo;

	@Autowired
	ReciptModeMstRepository reciptModeMstRepository;

	@Override
	public List<Object> getDailySaleSummary(String branch, Date reportdate,
			String companymstid/* , String accoutName */) {

		BranchMst branchMst = brnchMstRepo.getMyBranch();
		String myBranchCode = branchMst.getBranchCode();
		// String accoutName=myBranchCode+"CASH";

		String accoutName = "CASH";
		AccountHeads account = accountHeadsRepo.findByAccountNameAndCompanyMstId(accoutName, companymstid);
		String accountid = account.getId();

		Date previousDay = SystemSetting.previousDay(reportdate);

		Date nextDay = SystemSetting.nextDay(reportdate);
		List<Object> obj = dailySaleSummaryRepo.billSummaryReport(branch, reportdate, nextDay,
				accountid /* ,previousDay */);

		return obj;

	}

	@Override
	public List<DailySalesSummary> getDailySales(String pdfFileName) {

		List<DailySalesSummary> dailysalessummaryList = new ArrayList();
		List<DailySalesSummary> dailysales = dailySaleSummaryRepo.findAll();

//		 for(DailySalesSummary i :dailysales )
//		 {
//			 DailySalesSummary dailysalessummary = new DailySalesSummary();
//			 dailysalessummary.setBranchCode(i.getBranchCode());
//			 dailysalessummary.setCashReceived(i.getCashReceived());
//			 dailysalessummary.setCashSalesOrder(i.getCashSalesOrder());
//			 dailysalessummary.setClosingBalance(i.getClosingBalance());
//			 dailysalessummary.setClosingBillNo(i.getClosingBillNo());
//			 dailysalessummary.setDebitCardSalesOrder(i.getDebitCardSalesOrder());
//			 dailysalessummary.setExpensePaid(i.getExpensePaid());
//			 dailysalessummary.setOnlineSalesOrder(i.getOnlineSalesOrder());
//			 dailysalessummary.setOpeningBillNo(i.getOpeningBillNo());
//			 dailysalessummary.setOpeningPettyCash(i.getOpeningPettyCash());
//			 dailysalessummary.setRecordDate(i.getRecordDate());
//			 dailysalessummary.setTotalBillNo(i.getTotalBillNo());
//			 dailysalessummaryList.add(dailysalessummary);
//			 
//			
//		 }
//		
		return dailysales;
	}

	@Override
	public List<SaleModeWiseSummaryReport> getPaymentModeWiseReport(String branch, Date date, CompanyMst companymst) {
		List<SaleModeWiseSummaryReport> reportSummaryList = new ArrayList<SaleModeWiseSummaryReport>();
		List<ReceiptModeMst> receiptList = reciptModeMstRepository.findByCompanyMst(companymst);
		Double advanceAmount = 0.0;
		Double totalAmount = 0.0;
		for (ReceiptModeMst rMode : receiptList) {

			List<Object> obj = dailySaleSummaryRepo.getPaymentModeWiseReport(branch, date, companymst,
					rMode.getReceiptMode());
			advanceAmount = dailySaleSummaryRepo.getAdvanceAmount(branch, date, companymst);
			totalAmount = dailySaleSummaryRepo.getpaymentModeTotal(branch, date, companymst);

			for (int i = 0; i < obj.size(); i++) {

				Object[] objAray = (Object[]) obj.get(i);
				SaleModeWiseSummaryReport rSummary = new SaleModeWiseSummaryReport();
				rSummary.setPaymentMode((String) objAray[0]);
				rSummary.setAmount((Double) objAray[1]);
				rSummary.setDate((Date) objAray[2]);
				if (advanceAmount != null) {
					rSummary.setAdvanceAmount(advanceAmount);
				} else {
					rSummary.setAdvanceAmount(0.0);
				}
				if (advanceAmount != null) {
					Double grandTotal = advanceAmount + totalAmount;
					rSummary.setTotalAmount(grandTotal);
				} else {
					rSummary.setTotalAmount(0.0);
				}
				rSummary.setDate(date);
				reportSummaryList.add(rSummary);
			}
		}
		return reportSummaryList;

	}

	@Override
	public Double getSumOfOnlineSale(String branch, Date date, String companymstid, String customerid) {

		return dailySaleSummaryRepo.getSumOfOnlineSale(branch, date, companymstid, customerid);
	}

	@Override
	public List<ReceiptModeReport> getsalesReceipts(String branch, java.sql.Date fdate, String companyMst) {

		List<ReceiptModeReport> ReceiptModeReportSummaryList = new ArrayList<ReceiptModeReport>();

		List<Object> obj = dailySaleSummaryRepo.getSalesReceipts(branch, fdate, companyMst);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			ReceiptModeReport receiptModeReport = new ReceiptModeReport();
			receiptModeReport.setAmount((Double) objAray[1]);
			receiptModeReport.setReceiptMode((String) objAray[0]);
			ReceiptModeReportSummaryList.add(receiptModeReport);
		}
		return ReceiptModeReportSummaryList;
	}

	@Override
	public List<ReceiptModeReport> getSalesReceiptsByMode(String branch, java.sql.Date fdate, String companyMst,
			String mode) {

		List<ReceiptModeReport> ReceiptModeReportSummaryList = new ArrayList<ReceiptModeReport>();

		List<Object> obj = dailySaleSummaryRepo.getSalesReceiptsByMode(branch, fdate, companyMst, mode);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			ReceiptModeReport receiptModeReport = new ReceiptModeReport();
			receiptModeReport.setAmount((Double) objAray[1]);
			receiptModeReport.setReceiptMode((String) objAray[0]);
			ReceiptModeReportSummaryList.add(receiptModeReport);
		}
		return ReceiptModeReportSummaryList;
	}

	@Override
	public List<ReceiptModeReport> getsalesReceiptsWeb(Date date, String companymstid) {

		List<ReceiptModeReport> ReceiptModeReportSummaryList = new ArrayList<ReceiptModeReport>();

		List<Object> obj = dailySaleSummaryRepo.getSalesReceiptsWeb(date, companymstid);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			ReceiptModeReport receiptModeReport = new ReceiptModeReport();
			receiptModeReport.setAmount((Double) objAray[1]);
			receiptModeReport.setReceiptMode((String) objAray[0]);
			ReceiptModeReportSummaryList.add(receiptModeReport);
		}
		return ReceiptModeReportSummaryList;
	}

	@Override
	public List<ReceiptModeReport> getSaleOrdersalesReceipts(Date date, String companymstid) {
		List<ReceiptModeReport> ReceiptModeReportSummaryList = new ArrayList<ReceiptModeReport>();

		List<Object> obj = dailySaleSummaryRepo.getSaleOrderReceiptsWeb(date, companymstid);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			ReceiptModeReport receiptModeReport = new ReceiptModeReport();
			receiptModeReport.setAmount((Double) objAray[1]);
			receiptModeReport.setReceiptMode((String) objAray[0]);
			ReceiptModeReportSummaryList.add(receiptModeReport);
		}
		return ReceiptModeReportSummaryList;
	}

	@Override
	public List<ReceiptModeReport> getSaleOrdersalesReceiptsByMode(Date date, String companymstid, String mode) {
		List<ReceiptModeReport> ReceiptModeReportSummaryList = new ArrayList<ReceiptModeReport>();

		List<Object> obj = dailySaleSummaryRepo.getSaleOrderReceiptsWebByMode(date, companymstid, mode);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			ReceiptModeReport receiptModeReport = new ReceiptModeReport();
			receiptModeReport.setAmount((Double) objAray[1]);
			receiptModeReport.setReceiptMode((String) objAray[0]);
			ReceiptModeReportSummaryList.add(receiptModeReport);
		}
		return ReceiptModeReportSummaryList;
	}

	@Override
	public Double getTotalCard(Date vdate, String companymstid, String branchcode) {

		return salesReceiptsRepo.sumOfCardAmount(vdate, companymstid, branchcode);
	}

	@Override
	public Double getTotalCardSO(String companymstid,java.sql.Date sdate) {
		return saleOrderReceiptRepo.getTotalCardSO(companymstid, sdate);
	}

	@Override
	public List<ReceiptModeReport> getSalesReceiptsByModeByAccountId(String branch, java.sql.Date sqlDate,
			String companymstid, String accountid) {
		List<ReceiptModeReport> ReceiptModeReportSummaryList = new ArrayList<ReceiptModeReport>();

		List<Object> obj = dailySaleSummaryRepo.getSalesReceiptsByModeByAccId(branch, sqlDate, companymstid, accountid);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			ReceiptModeReport receiptModeReport = new ReceiptModeReport();
			receiptModeReport.setAmount((Double) objAray[1]);
			receiptModeReport.setReceiptMode((String) objAray[0]);
			ReceiptModeReportSummaryList.add(receiptModeReport);
		}
		return ReceiptModeReportSummaryList;
	}

	// ----------------------version 6.7 surya

	@Override
	public Double getCard2SalesSummary(String branchCode, Date date, String companyid) {

		Double card2 = 0.0;
		card2 = salesDetailsRepository.getCard2SalesSummary(branchCode, date, companyid);
		if (null == card2) {
			card2 = 0.0;
		}

		return card2;
	}

	@Override
	public List<SalesInvoiceReport> getBranchWiseSalesSummary(Date date, String companymstid) {
		
		
		List<SalesInvoiceReport> salesSummaryList = new ArrayList<SalesInvoiceReport>();

		List<Object> obj = dailySaleSummaryRepo.getBranchWiseSalesSummary(date,companymstid);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			
			SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
			
			salesInvoiceReport.setBankName((String) objAray[0]);
			salesInvoiceReport.setAmount((Double) objAray[1]);
			
			salesSummaryList.add(salesInvoiceReport);
		}
		return salesSummaryList;
		
		
	}

	// ----------------------version 6.7 surya end
	
	
	
	

}
