package com.maple.restserver.service;

import java.util.List;

import com.maple.restserver.entity.ItemMst;

public interface TestService {

	ItemMst fetchRandomItem(int x);
	
}
