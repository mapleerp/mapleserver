package com.maple.restserver.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

 
@Service
@Transactional
public class IGSTCheckServiceImpl implements IGSTCheckService{

	@Override
	public String checkIgst(String companyState, String customerState, String companyCountry) {
	
		
		 if(!companyCountry.equalsIgnoreCase("INDIA")) {
			 return "IGST";
		 }
		 
		 
		 if(companyState.equalsIgnoreCase(customerState)) {
			 return "CGSTSGST";
		 }
		 
		 
		 if(!companyState.equalsIgnoreCase(customerState)) {
			 return "IGST";
		 }
		return null;
	}

}
