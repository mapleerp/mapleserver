package com.maple.restserver.service;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.PaymentDtl;

@Service
public interface PaymentEditService {

	public void deletePaymentAndOwnAccount(String companymstid, String paymentHdrId,PaymentDtl paymentDtls);
}
