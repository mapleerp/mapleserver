package com.maple.restserver.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.KotCategoryMst;
import com.maple.restserver.repository.KotCategoryMstRepository;

@Service
@Transactional
public class CategoryMstServiceImpl implements CategoryMstService {

	@Autowired
	KotCategoryMstRepository kotCategoryMstRepository;
	
	
	
	@Override
	public KotCategoryMst getPrinterByCategory(String categoryId) {
		 
		
		KotCategoryMst kotCategoryMst = kotCategoryMstRepository.findByCategoryId(categoryId);
		
		return kotCategoryMst;
	}

	
	
}
