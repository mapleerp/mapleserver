package com.maple.restserver.service;

import java.util.List;

import com.maple.restserver.entity.HeartBeatsMst;

public interface HeartBeatsService {

	List<HeartBeatsMst> findByCompanyMst(String companymstid);

}
