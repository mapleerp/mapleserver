package com.maple.restserver.service;

import java.net.URL;
import java.net.URLConnection;

public class InternetCheckService {

	public static boolean hasInternet(URL url) {

		try {

			URLConnection connection = url.openConnection();
			connection.connect();

			return true;

		} catch (Exception e) {

			return false;

		}

	}
}
