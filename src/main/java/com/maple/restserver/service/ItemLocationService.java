package com.maple.restserver.service;

import java.util.List;

import com.maple.restserver.report.entity.ItemLocationReport;

public interface ItemLocationService {

	List<ItemLocationReport> getItemLocationByItemId(String itemid);

	List<ItemLocationReport> getItemLocationByFloor(String floor);

	List<ItemLocationReport> getItemLocationByFloorAndShelf(String floor, String shelf);

	List<ItemLocationReport> getItemLocationByFloorAndShelfAndRack(String floor, String shelf, String rack);

}
