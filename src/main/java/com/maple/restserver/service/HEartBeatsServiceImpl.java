package com.maple.restserver.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.HeartBeatsMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.HeartBeatsMstRepository;

@Service
@Transactional
public class HEartBeatsServiceImpl implements HeartBeatsService {

	@Autowired
	HeartBeatsMstRepository heartBeatsMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Override
	public List<HeartBeatsMst> findByCompanyMst(String companymstid) {

		List<HeartBeatsMst> heartBeatsMstList = new ArrayList<>();

		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}

		List<HeartBeatsMst> heartBeatsMstResp = heartBeatsMstRepository.findByCompanyMst(companyOpt.get());

		for (HeartBeatsMst hearbeat : heartBeatsMstResp) {

			LocalDateTime dateTimeNow = LocalDateTime.now();

			if (dateTimeNow.getYear() == hearbeat.getUpdatedTime().getYear()) {
				if (dateTimeNow.getMonth() == hearbeat.getUpdatedTime().getMonth()) {
					if (dateTimeNow.getDayOfMonth() == hearbeat.getUpdatedTime().getDayOfMonth()) {
						if (dateTimeNow.getHour() == hearbeat.getUpdatedTime().getHour()) {
							if (dateTimeNow.getMinute() == hearbeat.getUpdatedTime().getMinute()
									|| (dateTimeNow.getMinute() - 1) >= hearbeat.getUpdatedTime().getMinute()) {
								heartBeatsMstList.add(hearbeat);
							}
						}
					}
				}
			}

		}

		return heartBeatsMstList;
	}

}
