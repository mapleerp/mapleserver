package com.maple.restserver.service;

import java.sql.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.SalesReturnInvoiceReport;
import com.maple.restserver.report.entity.TaxSummaryMst;

@Service
@Transactional
public interface SalesReturnInvoiceServoice {

	 List<SalesReturnInvoiceReport> getSalesReturnInvoice(String companymstid,String voucherNumber,Date  voucherDate);
	
	 
	 List<TaxSummaryMst>	 getSalesReturnInvoiceTax(String companymstid, String vouchernumber,Date date);


	 List<TaxSummaryMst> getSumOfSalesReturnTaxAmounts(String companymstid,String vouchernumber,Date date);

	 
	 List<SalesReturnInvoiceReport>  getTaxinvoiceCustomer(String companymstid, String vouchernumber,Date date);

	 Double getCessAmount(String vouchernumber, Date date );
	 
	 Double getNonTaxableAmount(String vouchernumber,Date  date );


}
