package com.maple.restserver.service;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchDtlDaily;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.ProductConversionDtl;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.StockTransferInDtl;
import com.maple.restserver.entity.StockTransferInHdr;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.report.entity.CategoryWiseStockMovement;
import com.maple.restserver.report.entity.IntentVoucherReport;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.DamageDtlRepository;
import com.maple.restserver.repository.ItemBatchDtlDailyRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;

import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.ProductConversionDtlRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.StockTransferOutDtlRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component
public class ItemBatchDtlDailyServiceimpl implements ItemBatchDtlDailyService {

	@Autowired
	ProductConversionDtlRepository productConversionDtlRepo;
	@Autowired
	DamageDtlRepository damageDtlRepo;
	@Autowired
	StockTransferOutDtlRepository stockTransferOutDtlRepo;

	@Autowired
	ItemMstRepository itemMstRepo;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepo;
	@Autowired
	KitDefinitionMstRepository kitDefinitiomMstRepo;

	@Autowired
	ItemBatchDtlDailyRepository itemBatchDtlDailyRepository;

	@Autowired
	BranchMstRepository branchMstRepository;

	@Autowired
	ItemBatchMstRepository itemBatchMstRepository;

	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepo;

	@Autowired
	SalesDetailsRepository salesDetailsRepo;

	@Override
	public void updateItemBatchDtlDailyFromItemBatchDtl(CompanyMst companyMst, java.sql.Date curDate) {

		itemBatchDtlDailyRepository.updateDailyStockFromDtlStep0();
		BranchMst branchMst = branchMstRepository.getMyBranch();
		itemBatchDtlDailyRepository.updateDailyStockFromDtlStep1(branchMst.getBranchCode());

		// itemBatchDtlRepo.

		// itemBatchMstRepo.updateStockFromDtlStep2withDate(curDate) ;
		List<Object> listObject = itemBatchMstRepository.getClosingStockAsOnDate(curDate);
		System.out.println("----------itemBatchMstRepository size----------" + listObject.size());
		boolean flag = false;
		for (int i = 0; i < listObject.size(); i++) {

			Object[] objAray = (Object[]) listObject.get(i);
			ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();

			itemBatchDtlDaily.setBarcode(objAray[0] == null ? "" : (String) objAray[0]);

			itemBatchDtlDaily.setBatch(objAray[1] == null ? "" : (String) objAray[1]);
			itemBatchDtlDaily.setBranchCode(objAray[2] == null ? "" : (String) objAray[2]);

//			itemBatchDtlDaily.setExpDate(objAray[3] == null ? null : (Date) objAray[3]);

			String id = UUID.randomUUID().toString();
			itemBatchDtlDaily.setId(id);

			// itemBatchDtlDaily.setId(objAray[3] == null ? "" : (String) objAray[3]);
			itemBatchDtlDaily.setItemId(objAray[3] == null ? "" : (String) objAray[3]);
			itemBatchDtlDaily.setQtyOut(0.0);

			itemBatchDtlDaily.setQtyIn(objAray[4] == null ? 0 : (double) objAray[4]);
			itemBatchDtlDaily.setCompanyMst(companyMst);
			itemBatchDtlDaily.setVoucherDate(curDate);
			itemBatchDtlDaily.setStore(objAray[5] == null ? MapleConstants.Store : (String) objAray[5]);

			itemBatchDtlDailyRepository.save(itemBatchDtlDaily);

		}

		itemBatchDtlDailyRepository.updateExpiryDateFromItemBatchExpiryDtl();

		// Update item batch mst with kit
		updateItemBatchDtlDailyWithKit();
		itemBatchDtlDailyRepository.updateDailyStockFromDtlStep3();

		// itemBatchDtlDailyRepository.correctNegativStock();

		// itemBatchMstRepo.updateStockFromDtlStep4();

		return;
	}

	private void updateItemBatchDtlDailyWithKit() {

		List<ItemBatchDtlDaily> itemBatchDtlDaily = itemBatchDtlDailyRepository.findAll();

		List<KitDefinitionMst> kitList = kitDefinitionMstRepo.findAll();
		boolean flag = false;
		for (int i = 0; i < kitList.size(); i++) {
			for (int j = 0; j < itemBatchDtlDaily.size(); j++) {
				if (kitList.get(i).getItemId().equalsIgnoreCase(itemBatchDtlDaily.get(j).getItemId())) {
					flag = true;
					break;
				}

			}
			if (!flag) {

				List<KitDefinitionMst> kitDefMstList = kitDefinitionMstRepo.findByItemId(kitList.get(i).getItemId());
				ItemBatchDtlDaily itemBatchMst = new ItemBatchDtlDaily();
				itemBatchMst.setBarcode(kitDefMstList.get(0).getBarcode());
				itemBatchMst.setBatch(MapleConstants.Nobatch);
				itemBatchMst.setBranchCode(kitDefMstList.get(0).getBarcode());
				itemBatchMst.setCompanyMst(kitDefMstList.get(0).getCompanyMst());
				itemBatchMst.setItemId(kitDefMstList.get(0).getItemId());
				Optional<ItemMst> itemMstOpt = itemMstRepo.findById(kitDefMstList.get(0).getItemId());
				if (itemMstOpt.isPresent()) {
					itemBatchMst.setMrp(itemMstOpt.get().getStandardPrice());
				} else {
					itemBatchMst.setMrp(0.0);
				}
				itemBatchMst.setQtyIn(0.0);
				itemBatchMst.setQtyOut(0.0);
				itemBatchMst.setStore(MapleConstants.Store);
				itemBatchMst = itemBatchDtlDailyRepository.save(itemBatchMst);
				// itemBatchDtlDailyRepository.updateItemBatchDtlDailyMstWithKit(kitList.get(i).getItemId());
			}
		}
	}

	

	@Override
	public String stockVerificationForStkTransfer(String companymstid, String hdrId) {
		String stockList = null;
		List<StockTransferOutDtl> stkList = stockTransferOutDtlRepo.findByStockTransferOutHdrId(hdrId);
		for (StockTransferOutDtl stockTransferOutDtl : stkList) {

			Double qty = itemBatchDtlDailyRepository.findQtyByItem(stockTransferOutDtl.getItemId().getId(),
					stockTransferOutDtl.getBarcode(), stockTransferOutDtl.getBatch());

			if (null == qty) {
				if (null == stockList) {
					stockList = "NOT IN STOCK";
				}
				ItemMst itemMst = itemMstRepo.findById(stockTransferOutDtl.getItemId().getId()).get();
				stockList = stockList + itemMst.getItemName() + "(" + stockTransferOutDtl.getQty() + "),";

			} else if (stockTransferOutDtl.getQty() > qty) {
				if (null == stockList) {
					stockList = "NOT IN STOCK";
				}
				Double balance = stockTransferOutDtl.getQty() - qty;
				ItemMst itemMst = itemMstRepo.findById(stockTransferOutDtl.getItemId().getId()).get();
				stockList = stockList + ", " + itemMst.getItemName() + "(" + balance + ")";
			}
		}

		return stockList;
	}

	@Override
	public String stockVerificationForDamageEntry(String companymstid, String hdrId) {

		String stockList = null;
		List<DamageDtl> damageDtlList = damageDtlRepo.findByDamageHdrIdAndCompanyMstId(hdrId, companymstid);
		for (DamageDtl damageDtl : damageDtlList) {

			Double qty = itemBatchDtlDailyRepository.findQtyByItemAndBatch(damageDtl.getItemId(),
					damageDtl.getBatchCode());

			if (null == qty) {
				if (null == stockList) {
					stockList = "NOT IN STOCK";
				}
				ItemMst itemMst = itemMstRepo.findById(damageDtl.getItemId()).get();
				stockList = stockList + itemMst.getItemName() + "(" + damageDtl.getQty() + "),";

			} else if (damageDtl.getQty() > qty) {
				if (null == stockList) {
					stockList = "NOT IN STOCK";
				}
				Double balance = damageDtl.getQty() - qty;
				ItemMst itemMst = itemMstRepo.findById(damageDtl.getItemId()).get();
				stockList = stockList + ", " + itemMst.getItemName() + "(" + balance + ")";
			}
		}

		return stockList;

	}

	@Override
	public String stockVerificationForProdcutConversion(String companymstid, String prdcthdrid) {

		String stockList = null;
		List<ProductConversionDtl> productConversionDtlList = productConversionDtlRepo
				.findByProductConversionMstId(prdcthdrid);
		for (ProductConversionDtl productConversionDtl : productConversionDtlList) {

			Double qty = itemBatchDtlDailyRepository.findQtyByItemAndBatch(productConversionDtl.getFromItem(),
					productConversionDtl.getBatchCode());

			if (null == qty) {
				if (null == stockList) {
					stockList = "NOT IN STOCK";
				}
				ItemMst itemMst = itemMstRepo.findById(productConversionDtl.getFromItem()).get();
				stockList = stockList + itemMst.getItemName() + "(" + productConversionDtl.getFromQty() + "),";

			} else if (productConversionDtl.getFromQty() > qty) {
				if (null == stockList) {
					stockList = "NOT IN STOCK";
				}
				Double balance = productConversionDtl.getFromQty() - qty;
				ItemMst itemMst = itemMstRepo.findById(productConversionDtl.getFromItem()).get();
				stockList = stockList + ", " + itemMst.getItemName() + "(" + balance + ")";
			}
		}

		return stockList;

	}

}
