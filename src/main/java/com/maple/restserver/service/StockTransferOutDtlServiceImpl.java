package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.repository.StockTransferOutDtlRepository;

@Service
@Transactional
@Component
public class StockTransferOutDtlServiceImpl implements StockTransferOutDtlService {

	@Autowired
	StockTransferOutDtlRepository stockTransferOutDtlRepo;

	@Override
	public List<Object> getStockTransfer(String batch, Date voucherDate) {

		// List<Object> obj = stockTransferOutDtlRepo.stocktrans(batch, (java.sql.Date)
		// voucherDate);

		return null;
	}

//	@Override
//	public  List<StockTransferOutDtl> getAllStockTransfer( ) {
//		
//
//	 List<StockTransferOutDtl> stocktransferDtlList =   stockTransferOutDtlRepo.findAll();
//	 
//
//		 
//	
//
//		return stocktransferDtlList;

//
	// }

	@Override
	public List<StockTransferOutDtl> getAllStockTransfer() {

		List<StockTransferOutDtl> stocktransferDtlList = stockTransferOutDtlRepo.findAll();

		// JRBeanCollectionDataSource jrBeanCollectionDataSource = new
		// JRBeanCollectionDataSource(stockTransferOutDtlReport);
		/*
		 * "jrxml/dailySalesSummaryVertical.jrxml", " dailysalessummary.pdf", null
		 */
//		 
//		 String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//				 "jrxml/stocktransferoutdtl.jrxml",
//				 pdfFileName ,null,
//				 jrBeanCollectionDataSource );

		/*
		 * Create SalesInoiceEntity Entity and pass value from object to Entity
		 */

		return stocktransferDtlList;

	}

	@Override
	public List<StockTransferOutDtl> updateStocktransferOutDisplaySerial(String stockTransferOutHdrId) {

		List<StockTransferOutDtl> stocktransferDtlList = stockTransferOutDtlRepo
				.findByStockTransferOutHdrIdOrderBySrl(stockTransferOutHdrId);
		
		int DisplaySrlNo=0;
		for (StockTransferOutDtl stockTransferOutDtl : stocktransferDtlList) {
			DisplaySrlNo++;
			stockTransferOutDtl.setDisplaySerial(DisplaySrlNo);
			stockTransferOutDtlRepo.save(stockTransferOutDtl);
			
			
		}

		return stocktransferDtlList;
	}

}
