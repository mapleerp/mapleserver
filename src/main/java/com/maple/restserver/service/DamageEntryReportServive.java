package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.DamageEntryReport;
import com.maple.restserver.report.entity.WriteOffDetailsAndSummaryReport;

@Component
public interface DamageEntryReportServive {

	
	List<DamageEntryReport>getDamageEntryReport(CompanyMst companymst, Date fromdate,Date todate,String branchCode);

	
	
	List<WriteOffDetailsAndSummaryReport> getWriteOffDetailAndSummary(CompanyMst companyMst, Date fDate, Date tDate,
			String branchCode, String reason);
}
