package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
 
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.report.entity.ReceiptReport;
import com.maple.restserver.report.entity.ReceiptReports;
import com.maple.restserver.repository.ReceiptDtlRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.utils.SystemSetting;
@Service
@Transactional
@Component
public class ReceiptReportServiceImpl implements ReceiptReportService {

	@Autowired
	ReceiptHdrRepository receiptHdrRepo;
	
	@Autowired
	ReceiptDtlRepository receiptDtlRepository;
	@Override
	public List<Object> getDailyReceipt(String voucherNo) {
		
		 List<Object> obj =  receiptHdrRepo.getDailyReceipt(voucherNo);
		return obj;
	}

	@Override
	public List<ReceiptReport> getAllreceipt() {
		List< ReceiptReport> receiptReportList =   new ArrayList();
		 
		 List<Object> obj = receiptHdrRepo.getAllReceipts();
			 for(int i=0;i<obj.size();i++)
			 {
				 Object[] objAray = (Object[]) obj.get(i);
				 ReceiptReport receiptReport = new ReceiptReport();
				 receiptReport.setSupplierName((String) objAray[0]);
				 receiptReport.setVoucherNumber(( String) objAray[1]);
				 receiptReport.setVoucherDate(( Date) objAray[2]);
				 receiptReport.setItemName((String) objAray[3]);
				 receiptReport.setAmount(( Double) objAray[4]);
			     receiptReportList.add(receiptReport);
				 
			 
			 }
			 return receiptReportList;

		}
	@Override
	public List<ReceiptHdr> fetchReceiptReportHdr(CompanyMst companyMst, String branchcode, Date fromDate,
			Date toDate) {
	
		List<ReceiptHdr> receiptReportList=new ArrayList<ReceiptHdr>();
		 
		List<Object> obj = receiptHdrRepo.fetchReceiptHdrReport(companyMst,branchcode,fromDate,toDate);
			
		
		for(int i=0;i<obj.size();i++) {
			
			Object[] objAray = (Object[]) obj.get(i);
			ReceiptHdr receiptHdr=new ReceiptHdr();
			receiptHdr.setVoucherDate((Date) objAray[0]);
			receiptHdr.setVoucherNumber((String) objAray[1]);
			receiptHdr.setTransdate((Date) objAray[2]);
			receiptHdr.setVoucherType((String) objAray[3]);
			receiptHdr.setId((String) objAray[4]);
			receiptReportList.add(receiptHdr);
		}
		
		return receiptReportList;
	}

	@Override
	public List<ReceiptReports> exportReceiptReport(CompanyMst companyMst, Date fromdate, Date todate,
			String branchcode) {
		List<ReceiptReports>receiptReportList = new ArrayList<ReceiptReports>();
		
		List<Object> obj=receiptHdrRepo.exportReceiptToExcel( companyMst,branchcode ,fromdate,todate);
		
		
		 for(int i=0;i<obj.size();i++)
		 {
			 
			 Object[] objAray = (Object[]) obj.get(i);
			 ReceiptReports receiptReport=new ReceiptReports();
			 
		
			 String voucherDate =SystemSetting.UtilDateToString((Date) objAray[0]);
			 receiptReport.setVoucherDate(voucherDate);
			 receiptReport.setVoucherNumber((String) objAray[1]);
			 String transDate =SystemSetting.UtilDateToString((Date) objAray[2]);
			 receiptReport.setTransdate(transDate);
			 receiptReport.setVoucherType((String) objAray[3]);
			 receiptReport.setAccountName((String) objAray[4]);
			 receiptReport.setAmount((Double) objAray[5]);
			 receiptReport.setBankAccountNumber((String) objAray[6]);
			 receiptReport.setDepositBank((String) objAray[7]);
			
			 receiptReport.setModeOfpayment((String) objAray[8]);
			 receiptReport.setDebitAccountId((String) objAray[9]);
			 
			 receiptReportList.add(receiptReport);
		 }
		 
		return receiptReportList;
	}

	@Override
	public List<ReceiptDtl> fetchByReceiptHdrId(String receipthdrId) {
	List<ReceiptDtl> receiptDtlReport=new ArrayList<ReceiptDtl>();
		List<Object> objects=receiptDtlRepository.fetchByReceiptHdrId(receipthdrId);
		for(int i=0;i<objects.size();i++) {
			Object[] objAray = (Object[]) objects.get(i);
			ReceiptDtl receiptDtl=new ReceiptDtl();
			
			receiptDtl.setAccount((String) objAray[0]);
			receiptDtl.setModeOfpayment((String) objAray[1]);
			receiptDtl.setAmount((Double) objAray[2]);
			receiptDtl.setDepositBank((String) objAray[3]);
			receiptDtl.setBankAccountNumber((String) objAray[4]);
			receiptDtl.setRemark((String) objAray[5]);
			receiptDtlReport.add(receiptDtl);
		}
		
		return receiptDtlReport;
	}

	@Override
	public List<ReceiptReports> receiptReportPrinting(CompanyMst companyMst, Date fromdate, Date todate,
			String branchcode) {
		
		
List<ReceiptReports>receiptReportList = new ArrayList<ReceiptReports>();
		
		List<Object> obj=receiptHdrRepo.receiptReportPrinting( companyMst,branchcode ,fromdate,todate);
		
		
		 for(int i=0;i<obj.size();i++)
		 {
			 
			 Object[] objAray = (Object[]) obj.get(i);
			 ReceiptReports receiptReport=new ReceiptReports();
			 
		
			 String voucherDate =SystemSetting.UtilDateToString((Date) objAray[0]);
			 receiptReport.setVoucherDate(voucherDate);
			 receiptReport.setVoucherNumber((String) objAray[1]);
			 String transDate =SystemSetting.UtilDateToString((Date) objAray[2]);
			 receiptReport.setTransdate(transDate);
			 receiptReport.setVoucherType((String) objAray[3]);
			 receiptReport.setAccountName((String) objAray[4]);
			 receiptReport.setAmount((Double) objAray[5]);
			 receiptReport.setBankAccountNumber((String) objAray[6]);
			 receiptReport.setDepositBank((String) objAray[7]);
			 receiptReport.setModeOfpayment((String) objAray[8]);
			 receiptReport.setDebitAccountId((String) objAray[9]);
			 
			 receiptReportList.add(receiptReport);
		 }
		 
		return receiptReportList;
	
	}

 
 
	}


