package com.maple.restserver.service;

import java.util.Date;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LastBestDate;

@Service
public abstract class LastBestDateService {

	protected String activityType;
	protected LastBestDate lasteBestDate;
	protected String companymstid;
	private String branchcode;
	
	protected LastBestDate lastBestDate;
	
	 public LastBestDate findLastBestDate() {
		 
		 return lastBestDate;
	 };
	 
	
	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getCompanymstid() {
		return companymstid;
	}

	public void setCompanymstid(String companymstid) {
		this.companymstid = companymstid;
	}

	public String getBranchcode() {
		return branchcode;
	}

	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}



  	

}
