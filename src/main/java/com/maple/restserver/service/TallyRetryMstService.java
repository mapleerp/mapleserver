package com.maple.restserver.service;

import java.util.Date;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.TallyRetryMst;

public interface TallyRetryMstService {

	public TallyRetryMst saveTallyRetryMst(CompanyMst companyMst,String voucherType,String voucherNumber,Date voucherDate);
	
	    
}
