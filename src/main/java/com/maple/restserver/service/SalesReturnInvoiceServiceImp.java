package com.maple.restserver.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.maple.restserver.report.entity.SalesReturnInvoiceReport;
import com.maple.restserver.report.entity.TaxSummaryMst;
import com.maple.restserver.repository.SalesReturnHdrRepository;

@Service
@Transactional
@Component
public class SalesReturnInvoiceServiceImp implements SalesReturnInvoiceServoice{

	
	@Autowired
	SalesReturnHdrRepository salesReturnHdrRepository;
	@Override
	public List<SalesReturnInvoiceReport> getSalesReturnInvoice(String companymstid, String voucherNumber, Date voucherDate) {
		
		List<Object> obj = salesReturnHdrRepository.salesrReturnInvoice(companymstid,voucherNumber, voucherDate);
		List<SalesReturnInvoiceReport> salesReturnInvoiceReportList = new ArrayList();
		Double taxableAmount=0.0;
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			SalesReturnInvoiceReport salesReturnInvoiceReport = new SalesReturnInvoiceReport();
			salesReturnInvoiceReport.setBranchName((String) objAray[0]);
			// salesInvoiceReport.setBranchName("TEST BRANCH NAME");
			salesReturnInvoiceReport.setBranchAddress1((String) objAray[1]);
			salesReturnInvoiceReport.setBranchAddress2((String) objAray[2]);
			salesReturnInvoiceReport.setBranchPlace((String) objAray[3]);
			salesReturnInvoiceReport.setBranchTelNo((String) objAray[4]);
			salesReturnInvoiceReport.setBranchGst((String) objAray[5]);
			salesReturnInvoiceReport.setBranchState((String) objAray[6]);
			salesReturnInvoiceReport.setBranchEmail((String) objAray[7]);
			salesReturnInvoiceReport.setBankName((String) objAray[8]);
			salesReturnInvoiceReport.setAccountNumber((String) objAray[9]);
			salesReturnInvoiceReport.setBankBranch((String) objAray[10]);
			salesReturnInvoiceReport.setIfsc((String) objAray[11]);
			salesReturnInvoiceReport.setVoucherDate((Date) objAray[12]);
			salesReturnInvoiceReport.setVoucherNumber((String) objAray[13]);
			salesReturnInvoiceReport.setCustomerName((String) objAray[14]);
			salesReturnInvoiceReport.setCustomerPlace((String) objAray[15]);
			salesReturnInvoiceReport.setCustomerState((String) objAray[16]);
			salesReturnInvoiceReport.setCustomerGst((String) objAray[17]);
			salesReturnInvoiceReport.setItemName((String) objAray[18]);
			if(null != objAray[37])
			{
				salesReturnInvoiceReport.setMrp((Double) objAray[37]);
			} else {
				salesReturnInvoiceReport.setMrp(0.0);
			}
			
			
			if(null!=objAray[19]) {
			
				salesReturnInvoiceReport.setTaxRate((Double) objAray[19]);
			}else {
				salesReturnInvoiceReport.setTaxRate(0.0);
			}
			
			if(null!=objAray[20]) {
				salesReturnInvoiceReport.setHsnCode((String) objAray[20]);
			}else {
				salesReturnInvoiceReport.setHsnCode("");
				
			}
			
			if(null!=objAray[21]) {
				salesReturnInvoiceReport.setRate((Double) objAray[21]);
			}else {
				
			}
			if(null!=objAray[22]) {
				salesReturnInvoiceReport.setQty((Double) objAray[22]);
			}else {
				salesReturnInvoiceReport.setQty(0.0);
			}
			
			if(null!=objAray[23]) {
				salesReturnInvoiceReport.setUnitName((String) objAray[23]);
			}else {
				
			}
			
			
			if(null!=objAray[24]) {
				salesReturnInvoiceReport.setAmount((Double) objAray[24]);
			}else {
				salesReturnInvoiceReport.setAmount(0.0);
			}
			
			if(null!=objAray[25]) {
				salesReturnInvoiceReport.setSgstAmount(((Double) objAray[25]));
			}else {
				salesReturnInvoiceReport.setSgstAmount(0.0);
			}
			
			if(null!=objAray[26]) {
				salesReturnInvoiceReport.setCgstAmount(((Double)objAray[26]));
			}else {
				salesReturnInvoiceReport.setCgstAmount(0.0);
			}
			
			if(null!=objAray[27]) {
				salesReturnInvoiceReport.setIgstAmount(((Double) objAray[27]));
			}else {
				salesReturnInvoiceReport.setIgstAmount(0.0);
			}
			
			if(null!=objAray[28]) {
				salesReturnInvoiceReport.setSgstRate((Double) objAray[28]);
			}else {
				salesReturnInvoiceReport.setSgstRate(0.0);
				
			}
			
			if(null!=objAray[29]) {
				salesReturnInvoiceReport.setCgstRate((Double) objAray[29]);
			}else {
				salesReturnInvoiceReport.setCgstRate(0.0);
			}
			
			if(null!=objAray[30]) {
				salesReturnInvoiceReport.setIgstRate((Double) objAray[30]);
			}else{
				salesReturnInvoiceReport.setIgstRate(0.0);
			}
			
			if(null!=objAray[31]) {
				salesReturnInvoiceReport.setCessRate((Double) objAray[31]);
			}else{
				salesReturnInvoiceReport.setCessRate(0.0);
			}
			
			if(null!=objAray[32]) {
				salesReturnInvoiceReport.setCessAmount((Double) objAray[32]);
			}else{
				salesReturnInvoiceReport.setCessAmount(0.0);
			}
			if(null!=objAray[33]) {
				salesReturnInvoiceReport.setBranchWebsite((String) objAray[33]);
			}else {
				salesReturnInvoiceReport.setBranchWebsite("");
			}
			salesReturnInvoiceReport.setCustomerPhone((String) objAray[34]);
			
			
			if(null!=objAray[35]) {
				salesReturnInvoiceReport.setTaxableAmount((Double) objAray[35]);
			}else {
				salesReturnInvoiceReport.setTaxableAmount(0.0);
			}
			
			
			salesReturnInvoiceReport.setSalesMode((String) objAray[36]);
			
			taxableAmount=salesReturnHdrRepository.getSalesReturnTaxableAmount(voucherNumber, voucherDate ,companymstid);
			salesReturnInvoiceReport.setTaxableAmount(taxableAmount);
			
			/*
			 * List<Object> object=SalesTransHdrRepo.getTaxInvoiceCustomer(companymstid,
			 * voucherNumber, voucherDate); for (int k = 0; k < object.size(); k++) {
			 * Object[] objctAray = (Object[]) object.get(k);
			 * salesInvoiceReport.setLocalCustomerName((String) objctAray[0]);
			 * salesInvoiceReport.setAddressLine1((String) objctAray[1]);
			 * salesInvoiceReport.setAddressLine2((String) objctAray[2]);
			 * salesInvoiceReport.setLocalCustomerPhone((String) objctAray[3]); }
			 */
			
			salesReturnInvoiceReportList.add(salesReturnInvoiceReport);

		}

		return salesReturnInvoiceReportList;
	}
	@Override
	public List<TaxSummaryMst> getSalesReturnInvoiceTax(String companymstid, String vouchernumber, Date date) {
		
		List<TaxSummaryMst> taxSummaryList = new ArrayList();

		List<Object> obj = salesReturnHdrRepository.getTaxSummary(companymstid, vouchernumber, date);
		
		
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			TaxSummaryMst taxSummary = new TaxSummaryMst();
			Double taxPercent = ((Double) objAray[0]);
			taxSummary.setTaxAmount( (Double)  objAray[1] );
			
			Double sgstpercent = taxPercent;
			Double taxAmount =  ((Double)objAray[1]);
			
			taxSummary.setTaxPercentage("GST"+sgstpercent+"%");
			
			taxSummary.setSgstAmount((Double) objAray[2]);
			taxSummary.setCgstAmount((Double) objAray[3]);
			taxSummary.setIgstAmount((Double) objAray[4]);
			
			taxSummary.setSgstTaxRate(((Double) objAray[5]));
			taxSummary.setCgstTaxRate(((Double) objAray[6]).doubleValue());
			taxSummary.setIgstTaxRate(((Double) objAray[7]).doubleValue());
			
			taxSummary.setAmount(((Double) objAray[8]).doubleValue());
			taxSummary.setTaxRate(((Double) objAray[0]).doubleValue());
			
			taxSummaryList.add(taxSummary);
			
			/*TaxSummaryMst taxSummary2 = new TaxSummaryMst();
			
			taxSummary2.setTaxPercentage("CGST"+sgstpercent+"%");
			taxSummary2.setTaxAmount(sgstAmount);
			taxSummaryList.add(taxSummary2);*/
			
			

		}
		return taxSummaryList;
		
		
		
	}
	@Override
	public List<TaxSummaryMst> getSumOfSalesReturnTaxAmounts(String companymstid, String vouchernumber, Date date) {
	
		
		List<TaxSummaryMst> taxSumReportList = new ArrayList();

		List<Object> obj = salesReturnHdrRepository.getSumOfTaxAmounts(companymstid,  vouchernumber,date);
	
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			TaxSummaryMst tSummary = new TaxSummaryMst();
			tSummary.setSumOfsgstAmount(((Double) objAray[0]));
			tSummary.setSumOfcgstAmount(((Double) objAray[1]));
			tSummary.setSumOfigstAmount(((Double) objAray[2]));
			taxSumReportList.add(tSummary);

		}
		return taxSumReportList;
		
		
	}
	@Override
	public List<SalesReturnInvoiceReport> getTaxinvoiceCustomer(String companymstid, String voucherNumber, Date date) {
		
		List<SalesReturnInvoiceReport> salesReturnInvoiceReportList = new ArrayList();
		  List<Object> object=salesReturnHdrRepository.getTaxInvoiceCustomer(companymstid,
				  voucherNumber, date); 
		  List<Object> objects=salesReturnHdrRepository.getTaxInvoiceGstCustomer(companymstid,
				  voucherNumber, date); 

		  SalesReturnInvoiceReport SalesReturnInvoiceReport = new SalesReturnInvoiceReport();
			  for (int l = 0; l < objects.size(); l++) {
				  Object[] objectArray = (Object[]) objects.get(l);
				  SalesReturnInvoiceReport.setCustomerName((String) objectArray[0]);
				  SalesReturnInvoiceReport.setCustomerAddress((String) objectArray[1]);
				  SalesReturnInvoiceReport.setCustomerPlace((String) objectArray[2]);
				  SalesReturnInvoiceReport.setCustomerPhone((String) objectArray[3]);
				  SalesReturnInvoiceReport.setCustomerGst((String) objectArray[4]);
			  
			  }
				  for (int k = 0; k < object.size(); k++) {
					  Object[] objctAray = (Object[]) object.get(k);
			
					  SalesReturnInvoiceReport.setLocalCustomerName((String) objctAray[0]);
					  SalesReturnInvoiceReport.setLocalCustomerAddres((String) objctAray[1]);
					  SalesReturnInvoiceReport.setLocalCustomerPlace((String) objctAray[3]);
					  SalesReturnInvoiceReport.setLocalCustomerState((String) objctAray[4]);
					  SalesReturnInvoiceReport.setLocalCustomerPhone((String) objctAray[2]);
				  }
	
				  salesReturnInvoiceReportList.add(SalesReturnInvoiceReport);
	
		return salesReturnInvoiceReportList;
		
	
	}
	@Override
	public Double getCessAmount(String vouchernumber, Date date) {
		
		return salesReturnHdrRepository.getCessAmount(vouchernumber, date);
	}
	@Override
	public Double getNonTaxableAmount(String vouchernumber, Date date) {
			return salesReturnHdrRepository.getNonTaxableAmount(vouchernumber, date);
			
	}

}
