package com.maple.restserver.service;

import java.math.BigDecimal;

import javax.validation.Valid;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.SalesDtl;

@Component
@Service
public interface TaxMstService {

	BigDecimal taxCalculator(Double taxpercent, Double mrp);

	SalesDtl calculateAndSetTaxToSalesDtl( SalesDtl salesDtl);

	
	
	

}
