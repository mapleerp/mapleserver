package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.ItemLocationMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.report.entity.ItemLocationReport;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemLocationMstRepository;
import com.maple.restserver.repository.ItemMstRepository;

@Service
@Component
@Transactional
public class ItemLocationServiceImpl implements ItemLocationService{

	@Autowired
	ItemMstRepository itemMstRepo;
	
	
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;
	@Autowired
	ItemLocationMstRepository itemLocationMstRepo;
	@Override
	public List<ItemLocationReport> getItemLocationByItemId(String itemid) 
	{
	
		ItemMst itemMst = itemMstRepo.findById(itemid).get();
		Double qty = itemBatchDtlRepository.getItemStock(itemid);
		List<ItemLocationReport> itemLocReport = new ArrayList();
		List<ItemLocationMst> itemLocationList = itemLocationMstRepo.findByItemId(itemid);
		for(int i =0;i<itemLocationList.size();i++)
		{
			ItemLocationReport itemLocationReport = new ItemLocationReport();
			itemLocationReport.setFloor(itemLocationList.get(i).getFloor());
			itemLocationReport.setItemName(itemMst.getItemName());
			itemLocationReport.setQty(qty);
			itemLocationReport.setRack(itemLocationList.get(i).getRack());
			itemLocationReport.setShelf(itemLocationList.get(i).getShelf());
			itemLocReport.add(itemLocationReport);
		}

		
		return itemLocReport;
	}
	@Override
	public List<ItemLocationReport> getItemLocationByFloor(String floor) {

		
		
		
		List<ItemLocationReport> itemLocReport = new ArrayList();
		List<ItemLocationMst> itemLocationList = itemLocationMstRepo.findByFloor(floor);
		for(int i =0;i<itemLocationList.size();i++)
		{
			ItemMst itemMst = itemMstRepo.findById(itemLocationList.get(i).getItemId()).get();
			Double qty = itemBatchDtlRepository.getItemStock(itemLocationList.get(i).getItemId());
			ItemLocationReport itemLocationReport = new ItemLocationReport();
			itemLocationReport.setFloor(itemLocationList.get(i).getFloor());
			itemLocationReport.setItemName(itemMst.getItemName());
			itemLocationReport.setQty(qty);
			itemLocationReport.setRack(itemLocationList.get(i).getRack());
			itemLocationReport.setShelf(itemLocationList.get(i).getShelf());
			itemLocReport.add(itemLocationReport);
		}

		
		return itemLocReport;
	}
	@Override
	public List<ItemLocationReport> getItemLocationByFloorAndShelf(String floor, String shelf) {

		List<ItemLocationReport> itemLocReport = new ArrayList();
		List<ItemLocationMst> itemLocationList = itemLocationMstRepo.findByFloorAndShelf(floor,shelf);
		for(int i =0;i<itemLocationList.size();i++)
		{
			ItemMst itemMst = itemMstRepo.findById(itemLocationList.get(i).getItemId()).get();
			Double qty = itemBatchDtlRepository.getItemStock(itemLocationList.get(i).getItemId());
			ItemLocationReport itemLocationReport = new ItemLocationReport();
			itemLocationReport.setFloor(itemLocationList.get(i).getFloor());
			itemLocationReport.setItemName(itemMst.getItemName());
			itemLocationReport.setQty(qty);
			itemLocationReport.setRack(itemLocationList.get(i).getRack());
			itemLocationReport.setShelf(itemLocationList.get(i).getShelf());
			itemLocReport.add(itemLocationReport);
		}

		
		return itemLocReport;
	}
	@Override
	public List<ItemLocationReport> getItemLocationByFloorAndShelfAndRack(String floor, String shelf, String rack) {
		List<ItemLocationReport> itemLocReport = new ArrayList();
		List<ItemLocationMst> itemLocationList = itemLocationMstRepo.findByFloorAndShelfAndRack(floor,shelf,rack);
		for(int i =0;i<itemLocationList.size();i++)
		{
			ItemMst itemMst = itemMstRepo.findById(itemLocationList.get(i).getItemId()).get();
			Double qty = itemBatchDtlRepository.getItemStock(itemLocationList.get(i).getItemId());
			ItemLocationReport itemLocationReport = new ItemLocationReport();
			itemLocationReport.setFloor(itemLocationList.get(i).getFloor());
			itemLocationReport.setItemName(itemMst.getItemName());
			itemLocationReport.setQty(qty);
			itemLocationReport.setRack(itemLocationList.get(i).getRack());
			itemLocationReport.setShelf(itemLocationList.get(i).getShelf());
			itemLocReport.add(itemLocationReport);
		}

		
		return itemLocReport;
	}

}
