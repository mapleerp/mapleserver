package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.CustomerCategorywiseStockReport;
import com.maple.restserver.report.entity.CustomerSotckCategoryReport;

@Component
public interface CustomerStockCategoryService {
	List<CustomerSotckCategoryReport>fetchCustomerNameAndCategoryName(String companyMstId);
	
	List<CustomerCategorywiseStockReport> fetchCustomerCategorywiseStockReport(String companyMstId,String customerphoneno);

	List<CustomerCategorywiseStockReport> fetchCustomerCategorywiseStockReportByItem(String companymstid,
			String customerphoneno, String itemid);
}
