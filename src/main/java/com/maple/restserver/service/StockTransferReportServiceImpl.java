package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.StatementOfAccountReport;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.StockTransferOutHdrRepository;

@Service
@Transactional
@Component
public class StockTransferReportServiceImpl implements StockTransferReportService {

	@Autowired
	StockTransferOutHdrRepository stockTransferOutHdrRepo;

	@Override
	public List<Object> getStockTransfer(String intentNumber) {

		// List<Object> obj = stockTransferOutHdrRepo.stockTransferOutReportAll();

		return null;
	}

	@Override
	public List<StockTransferOutReport> getStockTransferByVoucerAndDate(String vouchernumber, Date date,
			String companymstid) {

		List<StockTransferOutReport> stockTransferReportList = new ArrayList();

		List<Object> obj = stockTransferOutHdrRepo.stockTransferOutReportByvoucerNumberAndDate(vouchernumber, date,
				companymstid);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			StockTransferOutReport stockTransferOutReport = new StockTransferOutReport();

			stockTransferOutReport.setIntentNumber((String) objAray[0]);

			stockTransferOutReport.setVoucherDate((Date) objAray[1]);

			stockTransferOutReport.setVoucherNumber((String) objAray[2]);

			stockTransferOutReport.setVoucherType((String) objAray[3]);

			stockTransferOutReport.setToBranch((String) objAray[4]);
			stockTransferOutReport.setBatch((String) objAray[5]);
			stockTransferOutReport.setQty((Double) objAray[6]);
			stockTransferOutReport.setRate((Double) objAray[7]);
			stockTransferOutReport.setUnitId((String) objAray[8]);
			stockTransferOutReport.setBarcode((String) objAray[9]);
			stockTransferOutReport.setItemId((String) objAray[10]);
			stockTransferOutReport.setExpiryDate((Date) objAray[11]);
			stockTransferOutReport.setAmount((Double) objAray[12]);
			stockTransferOutReport.setTaxRate((Double) objAray[13]);
			stockTransferOutReport.setMrp((Double) objAray[14]);
			stockTransferOutReport.setSlNo((Integer) objAray[15]);
			stockTransferOutReport.setHsnCode((String) objAray[16]);

			stockTransferOutReport.setBranchName((String) objAray[17]);
			// stockTransferOutReport.setBranchAddress((String) objAray[18]);
			stockTransferOutReport.setBranchPlace((String) objAray[18]);

			stockTransferOutReport.setToBranchPlace((String) objAray[19]);
			stockTransferReportList.add(stockTransferOutReport);

		}

		return stockTransferReportList;

	}

	
	
	
	@Override
	public List<StockTransferOutReport> getStockTransferByHdrId(String hdrId) {

		List<StockTransferOutReport> stockTransferReportList = new ArrayList();

		List<Object> obj = stockTransferOutHdrRepo.stockTransferOutReportByHdrId(hdrId);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			StockTransferOutReport stockTransferOutReport = new StockTransferOutReport();

			stockTransferOutReport.setIntentNumber((String) objAray[0]);

			stockTransferOutReport.setVoucherDate((Date) objAray[1]);

			stockTransferOutReport.setVoucherNumber((String) objAray[2]);

			stockTransferOutReport.setVoucherType((String) objAray[3]);

			stockTransferOutReport.setToBranch((String) objAray[4]);
			stockTransferOutReport.setBatch((String) objAray[5]);
			stockTransferOutReport.setQty((Double) objAray[6]);
			stockTransferOutReport.setRate((Double) objAray[7]);
			stockTransferOutReport.setUnitId((String) objAray[8]);
			stockTransferOutReport.setBarcode((String) objAray[9]);
			stockTransferOutReport.setItemId((String) objAray[10]);
			stockTransferOutReport.setItemName((String) objAray[10]);
			stockTransferOutReport.setExpiryDate((Date) objAray[11]);
			stockTransferOutReport.setAmount((Double) objAray[12]);
			stockTransferOutReport.setTaxRate((Double) objAray[13]);
			stockTransferOutReport.setMrp((Double) objAray[14]);
			stockTransferOutReport.setSlNo((Integer) objAray[15]);
			stockTransferOutReport.setHsnCode((String) objAray[16]);

			stockTransferOutReport.setBranchName((String) objAray[17]);
			// stockTransferOutReport.setBranchAddress((String) objAray[18]);
			stockTransferOutReport.setBranchPlace((String) objAray[18]);

			stockTransferOutReport.setToBranchPlace((String) objAray[19]);
			stockTransferReportList.add(stockTransferOutReport);

		}

		return stockTransferReportList;

	}

	
	
	@Override
	public List<StockTransferOutReport> getStockTransferOutDtlBetweenDate(Date fudate, Date tudate, String branchcode,
			String companymstid) {
		List<StockTransferOutReport> stockTransferReportList = new ArrayList();
		List<Object> obj = stockTransferOutHdrRepo.getStockTransferDtlBetweenDate(fudate, tudate, branchcode,
				companymstid);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			StockTransferOutReport stockTransferOutReport = new StockTransferOutReport();
			stockTransferOutReport.setAmount((Double) objAray[11]);
			stockTransferOutReport.setBatch((String) objAray[5]);
			stockTransferOutReport.setExpiryDate((Date) objAray[7]);
			stockTransferOutReport.setItemCode((String) objAray[6]);
			stockTransferOutReport.setRate((Double) objAray[10]);
			stockTransferOutReport.setQty((Double) objAray[8]);
			stockTransferOutReport.setToBranch((String) objAray[2]);
			stockTransferOutReport.setUnitName((String) objAray[9]);
			stockTransferOutReport.setVoucherDate((Date) objAray[0]);
			stockTransferOutReport.setVoucherNumber((String) objAray[1]);
			stockTransferOutReport.setItemName((String) objAray[3]);
			stockTransferOutReport.setCategoryName((String) objAray[4]);
			stockTransferReportList.add(stockTransferOutReport);
		}
		return stockTransferReportList;
	}

	@Override
	public List<StockTransferOutReport> getStockTransferOutDtlBetweenDateAndToBranch(Date fudate, Date tudate,
			String branchcode, String companymstid, String tobranch) {
		List<StockTransferOutReport> stockTransferReportList = new ArrayList();
		List<Object> obj = stockTransferOutHdrRepo.getStockTransferDtlBetweenDateAndToBranch(fudate, tudate, branchcode,
				companymstid, tobranch);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			StockTransferOutReport stockTransferOutReport = new StockTransferOutReport();
			stockTransferOutReport.setAmount((Double) objAray[11]);
			stockTransferOutReport.setBatch((String) objAray[5]);
			stockTransferOutReport.setExpiryDate((Date) objAray[7]);
			stockTransferOutReport.setItemCode((String) objAray[6]);
			stockTransferOutReport.setRate((Double) objAray[10]);
			stockTransferOutReport.setQty((Double) objAray[8]);
			stockTransferOutReport.setToBranch((String) objAray[2]);
			stockTransferOutReport.setUnitName((String) objAray[9]);
			stockTransferOutReport.setVoucherDate((Date) objAray[0]);
			stockTransferOutReport.setVoucherNumber((String) objAray[1]);
			stockTransferOutReport.setItemName((String) objAray[3]);
			stockTransferOutReport.setCategoryName((String) objAray[4]);
			stockTransferReportList.add(stockTransferOutReport);
		}
		return stockTransferReportList;
	}

	@Override
	public List<StockTransferOutReport> getStockTransferOutDtlBetweenDateAndCategoryList(Date fudate, Date tudate,
			String branchcode, String companymstid, String[] array) {
		List<StockTransferOutReport> stockTransferReportList = new ArrayList();
		List<Object> obj = stockTransferOutHdrRepo.getStockTransferDtlBetweenDateAndCategoryList(fudate, tudate,
				branchcode, companymstid, array);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			StockTransferOutReport stockTransferOutReport = new StockTransferOutReport();
			stockTransferOutReport.setAmount((Double) objAray[11]);
			stockTransferOutReport.setBatch((String) objAray[5]);
			stockTransferOutReport.setExpiryDate((Date) objAray[7]);
			stockTransferOutReport.setItemCode((String) objAray[6]);
			stockTransferOutReport.setRate((Double) objAray[10]);
			stockTransferOutReport.setQty((Double) objAray[8]);
			stockTransferOutReport.setToBranch((String) objAray[2]);
			stockTransferOutReport.setUnitName((String) objAray[9]);
			stockTransferOutReport.setVoucherDate((Date) objAray[0]);
			stockTransferOutReport.setVoucherNumber((String) objAray[1]);
			stockTransferOutReport.setItemName((String) objAray[3]);
			stockTransferOutReport.setCategoryName((String) objAray[4]);
			stockTransferReportList.add(stockTransferOutReport);
		}
		return stockTransferReportList;
	}

	@Override
	public List<StockTransferOutReport> getStockTransferOutDtlBetweenDateAndCategoryListAndTobranch(Date fudate,
			Date tudate, String branchcode, String companymstid, String[] array, String tobranch) {
		List<StockTransferOutReport> stockTransferReportList = new ArrayList();
		List<Object> obj = stockTransferOutHdrRepo.getStockTransferDtlBetweenDateAndCategoryListAndToBranch(fudate,
				tudate, branchcode, companymstid, array, tobranch);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			StockTransferOutReport stockTransferOutReport = new StockTransferOutReport();
			stockTransferOutReport.setAmount((Double) objAray[11]);
			stockTransferOutReport.setBatch((String) objAray[5]);
			stockTransferOutReport.setExpiryDate((Date) objAray[7]);
			stockTransferOutReport.setItemCode((String) objAray[6]);
			stockTransferOutReport.setRate((Double) objAray[10]);
			stockTransferOutReport.setQty((Double) objAray[8]);
			stockTransferOutReport.setToBranch((String) objAray[2]);
			stockTransferOutReport.setUnitName((String) objAray[9]);
			stockTransferOutReport.setVoucherDate((Date) objAray[0]);
			stockTransferOutReport.setVoucherNumber((String) objAray[1]);
			stockTransferOutReport.setItemName((String) objAray[3]);
			stockTransferOutReport.setCategoryName((String) objAray[4]);
			stockTransferReportList.add(stockTransferOutReport);
		}
		return stockTransferReportList;
	}

	@Override
	public List<StockTransferOutReport> getStockTransferOutSummaryBetweenDate(Date fdate, Date tdate) {
		List<StockTransferOutReport> stockTransferReportList = new ArrayList();
		List<Object> obj = stockTransferOutHdrRepo.getStockTransferOutSummaryReport(fdate, tdate);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			StockTransferOutReport stockTransferOutReport = new StockTransferOutReport();
			stockTransferOutReport.setAmount((Double) objAray[3]);
			stockTransferOutReport.setVoucherDate((Date) objAray[0]);
			stockTransferOutReport.setVoucherNumber((String) objAray[1]);
			stockTransferOutReport.setToBranch((String) objAray[2]);
			stockTransferReportList.add(stockTransferOutReport);
		}
		return stockTransferReportList;
	}
}
