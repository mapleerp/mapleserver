package com.maple.restserver.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.ReorderMst;
import com.maple.restserver.repository.ReorderMstRepository;

@Component
public class ReorderService {

	@Autowired
	private ReorderMstRepository reordermstRepo;
	
	
	public 	List<ReorderMst>  searchLikeReorderByName(String searchString) {
		return reordermstRepo.findSearch(searchString);
	}

}
