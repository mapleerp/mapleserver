package com.maple.restserver.service;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardCopyOption.COPY_ATTRIBUTES;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.google.gson.Gson;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.FinancialYearMst;
import com.maple.restserver.entity.InstallationModelClass;
import com.maple.restserver.entity.InvoiceFormatMst;
import com.maple.restserver.entity.ProcessPermissionMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SalesTypeMst;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.SysDateMst;
import com.maple.restserver.entity.TermsAndConditionsMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.jms.send.KafkaMapleEvent;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.FinancialYearMstRepository;
import com.maple.restserver.repository.InvoiceFormatMstRepository;
import com.maple.restserver.repository.ProcessPermissionRepository;
import com.maple.restserver.repository.SalesTypeMstRepository;
import com.maple.restserver.repository.SysDateMstRepository;
import com.maple.restserver.repository.TermsAndConditionRepository;
import com.maple.restserver.repository.UserMstRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
public class InstallationModelClassServiceImpl implements InstallationModelClassService {

	
	@Autowired
	ObjectMapper objectMapper;

	@Value("${installation_folder}")
	private String installation_folder;
	

	@Value("${installation_archived_folder}")
	private String installation_archived_folder;
	
	

	@Value("${outgoing_processed_folder}")
	private String outgoing_processed_folder;

	@Value("${incoming_folder}")
	private String incoming_folder;

	@Value("${incoming_processed_folder}")
	private String incoming_processed_folder;

	@Value("${incoming_processed_archived_folder}")
	private String incoming_processed_archived_folder;

	private static FileWriter file;
	private String file_sufix = ".dat";
	private String name_differentiator = "$";
	
	@Value("${mybranch}")
	private String mybranch;
	
	
	@Autowired
	UserMstRepository UserMstRepository;
	
	@Autowired
	FinancialYearMstRepository FinancialYearMstRepository;
	
	@Autowired
	InvoiceFormatMstRepository InvoiceFormatMstRepository;
	
	
	@Autowired
	ProcessPermissionRepository ProcessPermissionRepository;
	
	@Autowired
	TermsAndConditionRepository TermsAndConditionRepository;
	
	@Autowired
	SalesTypeMstRepository SalesTypeMstRepository;
	
	@Autowired
	SysDateMstRepository SysDateMstRepository;
	
	@Autowired
	BranchMstRepository BranchMstRepository;
	
	
	@Autowired
	CompanyMstRepository CompanyMstRepository;

	private Calendar calendarDate;
	
	@Override
	public void InstallationModelFileCreation(CompanyMst companyMst) {
		InstallationModelClass installationModelClass= new InstallationModelClass();
		
		ArrayList<UserMst>  userMstList =new ArrayList();
		UserMst userMst=new UserMst();
		userMst.setId("1");
		userMst.setBranchCode(mybranch);
		userMst.setFullName("qwerty123");
		userMst.setPassword("qwerty123");
		userMst.setUserName("qwerty123");
		userMst.setCompanyMst(companyMst);
		userMstList.add(userMst);
		installationModelClass.setUserMstList(userMstList);
		
		
		SysDateMst sysDateMst=new SysDateMst();
		sysDateMst.setId("2");
		sysDateMst.setAplicationDate(Date.valueOf(SystemSetting.UtilDateToString(SystemSetting.systemDate,"yyyy-MM-dd")));
		sysDateMst.setBranchCode(mybranch);
		sysDateMst.setDayEndDone("NULL");
		sysDateMst.setSystemDate(Date.valueOf(SystemSetting.UtilDateToString(SystemSetting.systemDate,"yyyy-MM-dd")));
		sysDateMst.setCompanyMst(companyMst);
		installationModelClass.setSysDateMst(sysDateMst);
		
		
		
		BranchMst branchMst =new BranchMst();
		branchMst.setId("3");
		branchMst.setAccountNumber("-");
		branchMst.setBankBranch("-");
		branchMst.setBankName("-");
		branchMst.setBranchAddress1("-");
		branchMst.setBranchAddress2("-");
		branchMst.setBranchCode(mybranch);
		branchMst.setBranchEmail("-");
		branchMst.setBranchName(mybranch);
		branchMst.setBranchPlace("-");
		branchMst.setBranchState("KERALA");
		branchMst.setBranchTelNo("1");
		branchMst.setBranchWebsite("-");
		branchMst.setCompanyMst(companyMst);
		branchMst.setMyBranch("Y");
		installationModelClass.setBranchMst(branchMst);
		
		
		
		
		
		
	     	 
		
		
			
	    
		FinancialYearMst financialYearMst= new FinancialYearMst();
		financialYearMst.setId(4);
		financialYearMst.setUserName("System");
		financialYearMst.setCurrentFinancialYear(1);
		int year = getFiscalYear();
		int    LAST_FISCAL_MONTH  =Calendar.APRIL;
		int    FIRST_FISCAL_MONTH  =  Calendar.MAY;
		String enddate=(year + 1)+"-"+"0"+LAST_FISCAL_MONTH+"-"+"31";
		java.sql.Date endDate = SystemSetting.StringToSqlDate(enddate, "yyyy-MM-dd");
		financialYearMst.setEndDate(endDate);
		financialYearMst.setFinancialYear( year + "-" + (year + 1));
		financialYearMst.setOpenCloseStatus("OPEN");
		
		String startDate=year +"-"+"0"+FIRST_FISCAL_MONTH+"-"+"01";
		java.sql.Date startdate = SystemSetting.StringToSqlDate(startDate, "yyyy-MM-dd");
		financialYearMst.setStartDate(startdate);
		financialYearMst.setCompanyMst(companyMst);
		installationModelClass.setFinancialYearMst(financialYearMst);
		
		
		InvoiceFormatMst invoiceFormatMst = new InvoiceFormatMst();
		invoiceFormatMst.setId("5");
		invoiceFormatMst.setBatch("NO");
		invoiceFormatMst.setDiscount("NO");
		invoiceFormatMst.setGst("YES");
		invoiceFormatMst.setJasperName("HWTaxInvoice");
		invoiceFormatMst.setPriceTypeId("PD000002");
		invoiceFormatMst.setReportName("GSTYESBatchNODiscountNO");
		invoiceFormatMst.setStatus("DEFAULT");
		invoiceFormatMst.setCompanyMst(companyMst);
		installationModelClass.setInvoiceFormatMst(invoiceFormatMst);
		
		
		
		ArrayList<ProcessPermissionMst>  processPermissionMstList =new ArrayList();
		ProcessPermissionMst processPermissionMst=new ProcessPermissionMst();
		processPermissionMst.setId("6");
		processPermissionMst.setProcessId("PRC0014BS");
		processPermissionMst.setUserId("BS000002");
		processPermissionMst.setCompanyMst(companyMst);
		processPermissionMstList.add(processPermissionMst);
		installationModelClass.setProcessPermissionMstList(processPermissionMstList);
		
		
		
		ArrayList<TermsAndConditionsMst>  termsAndConditionsMstList =new ArrayList();
		TermsAndConditionsMst termsAndConditionsMst=new TermsAndConditionsMst();
		termsAndConditionsMst.setId("7");
		termsAndConditionsMst.setConditions("All Payments through bank only");
		termsAndConditionsMst.setInvoiceFormat("HWTaxInvoice");
		termsAndConditionsMst.setSerialNumber("1");
		termsAndConditionsMst.setCompanyMst(companyMst);
		termsAndConditionsMstList.add(termsAndConditionsMst);
		installationModelClass.setTermsAndConditionsMstList(termsAndConditionsMstList);
		
		
		ArrayList<SalesTypeMst>  salesTypeMstList =new ArrayList();
		SalesTypeMst salesTypeMst =new SalesTypeMst();
		salesTypeMst.setId("8");
		salesTypeMst.setBranchCode(mybranch);
		salesTypeMst.setSalesPrefix("wholesale");
		salesTypeMst.setSalesType("wholesale");
		salesTypeMst.setCompanyMst(companyMst);
		salesTypeMstList.add(salesTypeMst);
		installationModelClass.setSalesTypeMstList(salesTypeMstList);
		
		
		
		
		writeInstallationModelFile(installationModelClass);
		
		
		
		
	}
	

	
	
	    public int getFiscalYear() {
		int    FIRST_FISCAL_MONTH  = Calendar.MARCH;
		int month =Calendar.getInstance().get(Calendar.MONTH);
        int year =Calendar.getInstance().get(Calendar.YEAR);
        return (month >= FIRST_FISCAL_MONTH) ? year : year - 1;
         }
	
	
	public void writeInstallationModelFile(InstallationModelClass installationModelClass) {
		try {


			File theDir = new File(installation_folder);
			if(!theDir.exists()) {
				theDir.mkdirs();
			}
		 
			
			// Constructs a FileWriter given a file name, using the platform's default
			// charset
			 
			file = new FileWriter(installation_folder+File.separatorChar + "InstallationModelClass" + name_differentiator
					+ "File" + file_sufix);
			String outString = objectMapper.writeValueAsString(installationModelClass);
			file.write(outString);

		} catch (IOException e) {
			e.printStackTrace();

		} finally {

			try {
				file.flush();
				file.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void FileInstallation(CompanyMst companyMst) {

		File theDir = new File(installation_archived_folder);
		if(!theDir.exists()) {
			theDir.mkdirs();
		}
		
		File folder = new File(installation_folder);

		for (File file : folder.listFiles()) {
			if (!file.isDirectory()) {

				if (processFile(file.getAbsolutePath(), companyMst )) {

					File to = new File(installation_archived_folder + File.separatorChar + file.getName());

					boolean fileCopied = false;
					 
						fileCopied = copyFileToProcessed(file, to);
					 
					if (fileCopied) {
						 
						
						file.delete();
						
						 
					}

				}

			}
		}

		return;
		
	}
	private boolean processFile(String fileName , CompanyMst companyMst) {


		ObjectMapper mapper = new ObjectMapper()
				.registerModule(new ParameterNamesModule()).registerModule(new Jdk8Module())
				.registerModule(new JavaTimeModule());
	 

		
		JSONParser parser = new JSONParser();
		try {
			FileReader fr = new FileReader(fileName);
			Object obj = parser.parse(fr);
			JSONObject jsonObject = (JSONObject) obj;

			Gson gson = new Gson();
			
			ArrayList userMstListString  =  (ArrayList) jsonObject.get("userMstList");
			 for(int i =0 ;i <userMstListString.size();i++) {
				 JSONObject userString = (JSONObject) userMstListString.get(i);
				 UserMst user = gson.fromJson(userString.toString(), UserMst.class);
				 
				UserMstRepository.save(user);
			 
			 }

			JSONObject financialString = (JSONObject) jsonObject.get("financialYearMst");
			FinancialYearMst financialYearMst = mapper.readValue(financialString.toString(), FinancialYearMst.class);
			FinancialYearMstRepository.save(financialYearMst);
				
			JSONObject	sysDateMstString=(JSONObject) jsonObject.get("sysDateMst");
			SysDateMst sysDateMst = mapper.readValue(sysDateMstString.toString(), SysDateMst.class);
			SysDateMstRepository.save(sysDateMst);
			
			JSONObject	branchMstString=(JSONObject) jsonObject.get("branchMst");
			BranchMst branchMst = mapper.readValue(branchMstString.toString(), BranchMst.class);
			BranchMstRepository.save(branchMst);
			
			JSONObject invoiceFormatMstString = (JSONObject) jsonObject.get("invoiceFormatMst");
			InvoiceFormatMst invoiceFormat = mapper.readValue(invoiceFormatMstString.toString(), InvoiceFormatMst.class);
			invoiceFormat.setCompanyMst(companyMst);
			
			InvoiceFormatMstRepository.save(invoiceFormat);
			
			
			
			
			ArrayList processPermissionMstList = (ArrayList) jsonObject.get("processPermissionMstList");
		
			 for(int i =0 ;i <processPermissionMstList.size();i++) {
				 JSONObject processPermissionString = (JSONObject) processPermissionMstList.get(i);
				 ProcessPermissionMst processPermission = gson.fromJson(processPermissionString.toString(), ProcessPermissionMst.class);
				 ProcessPermissionRepository.save(processPermission);
				
			 
			 }
			
			 
			 
			 
			 
			 ArrayList termsAndConditionsMstList = (ArrayList) jsonObject.get("termsAndConditionsMstList");
				
			 for(int i =0 ;i <termsAndConditionsMstList.size();i++) {
				 JSONObject termsAndConditionsMstString = (JSONObject) termsAndConditionsMstList.get(i);
				 TermsAndConditionsMst termsAndConditionsMst = gson.fromJson(termsAndConditionsMstString.toString(), TermsAndConditionsMst.class);
				 TermsAndConditionRepository.save(termsAndConditionsMst);
				
			 
			 }
			 
			 
			 ArrayList salesTypeMstList = (ArrayList) jsonObject.get("salesTypeMstList");
				
			 for(int i =0 ;i <salesTypeMstList.size();i++) {
				 JSONObject salesTypeMstString = (JSONObject) salesTypeMstList.get(i);
				 SalesTypeMst salesTypeMst = gson.fromJson(salesTypeMstString.toString(), SalesTypeMst.class);
				 SalesTypeMstRepository.save(salesTypeMst);
				
			 
			 }
			 
			
			
		
			fr.close();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	private boolean copyFileToProcessed(File src, File dest) {
		try {

			Path bytes = Files.copy(

			new java.io.File(src.getAbsolutePath()).toPath(),

			new java.io.File(dest.getAbsolutePath()).toPath(),

			REPLACE_EXISTING,

			COPY_ATTRIBUTES,

			NOFOLLOW_LINKS);

			System.out.println("File successfully copied ");



			} catch (IOException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();
			return false;

			}
		return true;

		
	}
	
}
