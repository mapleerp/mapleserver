package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.report.entity.TallyImportReport;

public interface TallyImportReportService {

	
	public List<TallyImportReport> tallyImportReportReport(String companymstid,String branchcode,Date fDate,Date TDate);
}
