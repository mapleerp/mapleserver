package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.ProductionDtlsReport;

@Component
public interface ProductionReportService {
	List<ProductionDtlsReport> getProductionDtlReport(Date date,String branchcode,String vouchernumber);

	
	List<ProductionDtlsReport>fetchProductionDtlReport(String branchcode,Date date);


List<ProductionDtlsReport>fetchSaleOrderItem(String branchcode,Date date,String resCatId);
}
