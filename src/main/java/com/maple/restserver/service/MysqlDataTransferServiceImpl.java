package com.maple.restserver.service;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.apache.derby.drda.NetworkServerControl;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.RestserverApplication;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
 
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AddKotTable;
import com.maple.restserver.entity.AddKotWaiter;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.DamageHdr;
import com.maple.restserver.entity.DeliveryBoyMst;
import com.maple.restserver.entity.FinancialYearMst;
import com.maple.restserver.entity.GroupMst;
import com.maple.restserver.entity.GroupPermissionMst;
import com.maple.restserver.entity.InvoiceFormatMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemDeviceMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.KotCategoryMst;
import com.maple.restserver.entity.KotItemMst;
import com.maple.restserver.entity.LocalCustomerMst;
import com.maple.restserver.entity.MenuConfigMst;
import com.maple.restserver.entity.MenuMst;
import com.maple.restserver.entity.MenuWindowMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.OpeningStockDtl;
import com.maple.restserver.entity.OrderTakerMst;
import com.maple.restserver.entity.OtherBranchSalesTransHdr;
import com.maple.restserver.entity.ParamValueConfig;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PriceBatchMst;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.PrinterMst;
import com.maple.restserver.entity.ProcessMst;
import com.maple.restserver.entity.ProcessPermissionMst;
import com.maple.restserver.entity.ProductConversionConfigMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionDtlDtl;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.ReceiptModeMst;
import com.maple.restserver.entity.SaleOrderReceipt;
import com.maple.restserver.entity.SalesDltdDtl;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesManMst;
import com.maple.restserver.entity.SalesOrderDtl;
import com.maple.restserver.entity.SalesOrderReceipts;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesPropertiesConfigMst;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SalesTypeMst;
import com.maple.restserver.entity.SchEligiAttrListDef;
import com.maple.restserver.entity.SchEligibilityAttribInst;
import com.maple.restserver.entity.SchEligibilityDef;
import com.maple.restserver.entity.SchOfferAttrInst;
import com.maple.restserver.entity.SchOfferAttrListDef;
import com.maple.restserver.entity.SchOfferDef;
import com.maple.restserver.entity.SchSelectAttrListDef;
import com.maple.restserver.entity.SchSelectDef;
import com.maple.restserver.entity.SchSelectionAttribInst;
import com.maple.restserver.entity.SchemeInstance;
import com.maple.restserver.entity.SiteMst;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.entity.StoreMst;
import com.maple.restserver.entity.SupplierPriceMst;
import com.maple.restserver.entity.SysDateMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.AddKotTableRepository;
import com.maple.restserver.repository.AddKotWaiterRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DamageDtlRepository;
import com.maple.restserver.repository.DamageHdrRepository;
import com.maple.restserver.repository.DelivaryBoyMstRepository;
import com.maple.restserver.repository.FinancialYearMstRepository;
import com.maple.restserver.repository.GroupMstRepository;
import com.maple.restserver.repository.GroupPermissionRepository;
import com.maple.restserver.repository.InvoiceFormatMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemDeviceMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.JournalDtlRepository;
import com.maple.restserver.repository.JournalHdrRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.KotCategoryMstRepository;
import com.maple.restserver.repository.KotItemMstRepository;
import com.maple.restserver.repository.LocalCustomerRepository;
import com.maple.restserver.repository.MenuConfigMstRepository;
import com.maple.restserver.repository.MenuMstRepository;
import com.maple.restserver.repository.MenuWindowMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.OpeningStockDtlRepository;
import com.maple.restserver.repository.OrderTakerMstRepository;
import com.maple.restserver.repository.OtherBranchSalesTransHdrRepository;
import com.maple.restserver.repository.ParamValueConfigRepository;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.PriceBatchMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.PrinterMstRepository;
import com.maple.restserver.repository.ProcessMstRepository;
import com.maple.restserver.repository.ProcessPermissionRepository;
import com.maple.restserver.repository.ProductConfigurationMstRepository;
import com.maple.restserver.repository.ProductConversionMstRepository;
import com.maple.restserver.repository.ProductionDetailRepository;
import com.maple.restserver.repository.ProductionDtlDtlRepository;
import com.maple.restserver.repository.ProductionMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.ReceiptDtlRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.repository.ReciptModeMstRepository;
import com.maple.restserver.repository.SaleOrderReceiptRepository;
import com.maple.restserver.repository.SalesDeletedDtlRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesManMstRepository;
import com.maple.restserver.repository.SalesOrderDtlRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.repository.SalesPropertiesConfigMstRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.SalesTypeMstRepository;
import com.maple.restserver.repository.SchEligiAttrListDefRepository;
import com.maple.restserver.repository.SchEligibilityAttribInstRepository;
import com.maple.restserver.repository.SchEligibilityDefRepository;
import com.maple.restserver.repository.SchOfferAttrInstRepository;
import com.maple.restserver.repository.SchOfferAttrListDefRepository;
import com.maple.restserver.repository.SchOfferDefRepository;
import com.maple.restserver.repository.SchSelectAttrListDefReoository;
import com.maple.restserver.repository.SchSelectDefRepository;
import com.maple.restserver.repository.SchSelectionAttribInstRepository;
import com.maple.restserver.repository.SchemeInstanceRepository;
import com.maple.restserver.repository.SiteMstRepository;
import com.maple.restserver.repository.StockTransferOutDtlRepository;
import com.maple.restserver.repository.StockTransferOutHdrRepository;
import com.maple.restserver.repository.StoreMstRepository;
import com.maple.restserver.repository.SupplierPriceMstRepository;
import com.maple.restserver.repository.SysDateMstRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.repository.UserMstRepository;
import com.maple.restserver.resource.vouchernumber.service.Sequence;
import com.maple.restserver.resource.vouchernumber.service.SequenceRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Service

public class MysqlDataTransferServiceImpl implements MysqlDataTransferService {

	 
	private static final Logger logger = LoggerFactory.getLogger(MysqlDataTransferServiceImpl.class);

	@Autowired
	VoucherNumberRepository voucherNumberRepository;
	
	@Autowired
	OpeningStockDtlRepository  openingStockDtlRepository;

	@Autowired
	KotItemMstRepository kotItemMstRepository;

	// @Autowired
	// private RuntimeService runtimeService;
	@Autowired
	ItemDeviceMstRepository itemDeviceMstRepository;

	@Autowired
	ProcessPermissionRepository processPermissionRepository;

	@Autowired
	SysDateMstRepository sysDateMstRepository;

	@Autowired
	StockTransferOutDtlRepository stockTransferOutDtlRepository;

	@Autowired
	DelivaryBoyMstRepository deliveryBoyMstRepository;

	@Autowired
	AddKotWaiterRepository addKotWaiterRepository;

	@Autowired
	FinancialYearMstRepository financialYearMstRepository;

	@Autowired
	ProductionDtlDtlRepository productionDtlDtlRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;

	 

	@Autowired
	AccountClassRepository accountClassRepo;
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	UserMstRepository userMstRepository;
	@Autowired
	BranchMstRepository branchMstRepository;
	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	private ItemBatchMstRepository itemBatchMstRepo;

	@Autowired
	PriceDefinitionRepository priceDefinitionRepo;

	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	SchemeInstanceRepository schemeInstanceRepo;

	@Autowired
	SchEligibilityAttribInstRepository schEligibilityAttribInstRepo;

	@Autowired
	SchOfferAttrInstRepository schOfferAttrInstRepo;

	@Autowired
	SchSelectAttrListDefReoository schSelectAttrListDefReo;

	@Autowired
	SchSelectDefRepository schSelectDefRepo;

	@Autowired
	private CategoryMstRepository categoryMstRepository;

	@Autowired
	private ProductionMstRepository productionMstRepository;

	@Autowired
	private UnitMstRepository unitMstRepository;

	@Autowired
	private ItemMstRepository itemMstRepository;

	@Autowired
	private KitDefinitionMstRepository kitDefinitionMstRepository;

	@Autowired
	private KitDefenitionDtlRepository kitDefenitionDtlRepository;

	/*
	 * @Autowired private CustomerMstRepository customerMstRepository;
	 */

	@Autowired
	private AccountHeadsRepository accountHeadsRepository;


	@Autowired
	private SalesTransHdrRepository salesTransHdrRepository;

	@Autowired
	private SalesReceiptsRepository salesReceiptsRepository;
	@Autowired
	private SalesDetailsRepository salesDetailsRepository;
	@Autowired
	private PurchaseHdrRepository purchaseHdrRepository;
	@Autowired
	private PurchaseDtlRepository purchaseDtlRepository;

	@Autowired
	private ReceiptHdrRepository receiptHdrRepository;

	@Autowired
	private ReceiptDtlRepository receiptDtlRepository;

	@Autowired
	private JournalHdrRepository journalHdrRepository;

	@Autowired
	private JournalDtlRepository journalDtlRepository;

	@Autowired
	private PaymentHdrRepository paymentHdrRepository;

	@Autowired
	private PaymentDtlRepository paymentDtlRepository;

	@Autowired
	private VoucherNumberService voucherNumberService;

	@Autowired
	AddKotTableRepository addKotTableRepository;

	@Autowired
	GroupMstRepository groupMstRepository;

	@Autowired
	GroupPermissionRepository groupPermissionRepository;

	@Autowired
	ProductConfigurationMstRepository productConfigurationMstRepository;

	@Autowired
	DebitClassRepository debitClassRepository;

	@Autowired
	CreditClassRepository creditClassRepository;

	@Autowired
	MenuConfigMstRepository menuConfigMstRepository;

	@Autowired
	StockTransferOutHdrRepository stockTransferOutHdrRepository;

	@Autowired
	KotCategoryMstRepository kotCategoryMstRepository;

	@Autowired
	ProductionDetailRepository productionDtlRepository;

	@Autowired
	DamageHdrRepository damageHdrRepository;

	@Autowired
	DamageDtlRepository damageDtlRepository;

	@Autowired
	PriceDefinitionMstRepository priceDefenitionMstRepository;

	@Autowired
	PriceDefinitionRepository priceDefinitionRepository;

	@Autowired
	PriceBatchMstRepository priceBatchMstRepository;

	@Autowired
	SalesOrderDtlRepository salesOrderDtlRepository;

	@Autowired
	private SequenceRepository sequenceRepository;

	@Autowired
	private LedgerClassRepository ledgerClassRepository;

	@Autowired
	MenuMstRepository menuMstRepository;

	@Autowired
	StoreMstRepository storeMstRepository;

	@Autowired
	SalesDeletedDtlRepository salesDeletedDtlRepository;

	@Autowired
	PrinterMstRepository printerMstRepository;

	@Autowired
	OtherBranchSalesTransHdrRepository otherBranchSalesTransHdrRepository;

	@Autowired
	private SchEligibilityDefRepository schEligibilityDefRepository;

	@Autowired
	private SchEligiAttrListDefRepository schEligiAttrListDefRepository;

	@Autowired
	private SchEligibilityAttribInstRepository schEligibilityAttribInstRepository;

	@Autowired
	LocalCustomerRepository localCustomerMstRepository;

	@Autowired
	SalesManMstRepository salesManMstRepository;

	@Autowired
	SiteMstRepository siteMstRepository;

	@Autowired
	SupplierPriceMstRepository supplierPriceMstRepository;

	@Autowired
	SchemeInstanceRepository schemeInstanceRepository;

	@Autowired
	ProductConfigurationMstRepository productConversionConfigMstRepository;

	@Autowired
	MultiUnitMstRepository multiUnitMstRepository;

	@Autowired
	SaleOrderReceiptRepository salesOrderReceiptsRepository;

	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepository;

	@Autowired
	SchSelectDefRepository schSelectDefRepository;

	@Autowired
	SchSelectAttrListDefReoository schSelectAttrListDefRepository;

	@Autowired
	SalesTypeMstRepository salesTypeMstRepository;

	@Autowired
	SalesPropertiesConfigMstRepository salesPropertiesConfigMstRepository;

	@Autowired
	SchSelectionAttribInstRepository schSelectionAttribInstRepository;

	@Autowired
	SchOfferDefRepository schOfferDefRepository;

	@Autowired
	SchOfferAttrListDefRepository schOfferAttrListDefRepository;

	@Autowired
	SchOfferAttrInstRepository schOfferAttrInstRepository;

	@Autowired
	ParamValueConfigRepository paramValueConfigRepository;

	@Autowired
	OrderTakerMstRepository orderTakerMstRepository;

	@Autowired
	InvoiceFormatMstRepository invoiceFormatMstRepository;

	@Autowired
	ReciptModeMstRepository receiptModeMstRepository;

	@Autowired
	AccountClassRepository accountClassRepository;

	@Autowired
	private MenuWindowMstRepository menuWindowMstRepository;

	@Autowired
	private ProcessMstRepository processMstRepository;

	@Override
	public String transferAccountHeads(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select * from account_heads ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			AccountHeads accountByName = accountHeadsRepository.findByAccountName(rs2.getString("account_name"));

			if (null == accountByName) {

				AccountHeads accountHeads = new AccountHeads();

				accountHeads.setAccountName(rs2.getString("account_name"));
				accountHeads.setCompanyMst(companyMst);
				accountHeads.setCurrencyId(rs2.getString("currency_id"));
				accountHeads.setOldId(rs2.getString("old_id"));
				accountHeads.setGroupOnly(rs2.getString("group_only"));
				accountHeads.setId(rs2.getString("id"));
				accountHeads.setMachineId(rs2.getString("machine_id"));
				accountHeads.setParentId(rs2.getString("parent_id"));
				accountHeads.setRootParentId(rs2.getString("root_parent_id"));

				accountHeads.setSerialNumber(rs2.getInt("serial_number"));
				accountHeads.setTaxId(rs2.getString("tax_id"));
				accountHeads.setVoucherType(rs2.getString("voucher_type"));

				accountHeadsRepository.saveAndFlush(accountHeads);

			}

		}
		
		rs2.close();
		pst2.close();

		return "OK";
	}

	@Override
	public String transferStockFromFile(CompanyMst companyMst, String fileName) throws UnknownHostException, Exception {

		try {

			FileInputStream fstream = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			ArrayList list = new ArrayList();
			while ((strLine = br.readLine()) != null) {
				list.add(strLine);
			}
			Iterator itr;
			for (itr = list.iterator(); itr.hasNext();) {
				String str = itr.next().toString();
				String[] splitSt = str.split(";");
				String itemId = "", branch = "", stockqty = "", itemname = "";
				// for (int i = 0; i < splitSt.length; i++) {

				try {
					itemId = splitSt[0];

					if (itemId.trim().length() == 0) {
						continue;
					}

					branch = splitSt[1];
					branch = branch.trim();
					stockqty = splitSt[2];

					itemname = splitSt[3];
					itemname = itemname.trim();

				} catch (Exception e) {

				}
				// }

				if (itemId.trim().length() == 0) {
					continue;
				}
				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
				itemId = itemId.trim();

				itemBatchDtl.setItemId(itemId);
				Double dQty = Double.parseDouble(stockqty);

				itemBatchDtl.setQtyIn(dQty);
				itemBatchDtl.setBranchCode(branch);

				ItemMst item = itemMstRepository.findByItemName(itemname);

				if (null == item) {
					continue;
				}

				itemBatchDtl.setBarcode(item.getBarCode());

				// itemBatchDtl.setMrp(mrp);

				Date date1 = SystemSetting.StringToSqlDate("16/02/2021", "dd/MM/yyyy");

				itemBatchDtl.setVoucherDate(date1);
				itemBatchDtl.setVoucherNumber("Opening");
				// itemBatchDtl.setExpDate(expiry_date);
				itemBatchDtl.setBatch(MapleConstants.Nobatch);
				itemBatchDtl.setQtyOut(0.0);

				itemBatchDtl.setCompanyMst(companyMst);
				itemBatchDtl.setStore(MapleConstants.Store);

				itemBatchDtl.setItemId(item.getId());

				itemBatchDtlRepository.save(itemBatchDtl);

				itemBatchDtl.setCompanyMst(companyMst);

				// itemBatchDtlRepository.save(itemBatchDtl);

			}
			
			br.close();

		} catch (Exception e) {
			System.out.println("Error" + e.toString());
		}

		return null;
	}

	@Override
	public String doItemTransfermysql(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {

		/*
		 * ITEM MASTER
		 */

		String sqlstr2 = " select  * from item_mst   ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			ItemMst itemMst = new ItemMst();

			itemMst.setStandardPrice(rs2.getDouble("standard_price"));
			itemMst.setItemDiscount(rs2.getDouble("item_discount"));
			itemMst.setTaxRate(rs2.getDouble("tax_rate"));
			itemMst.setSgst(rs2.getDouble("sgst"));
			itemMst.setCgst(rs2.getDouble("cgst"));
			itemMst.setCess(rs2.getDouble("cess"));
			itemMst.setIsKit(rs2.getInt("is_kit"));
			itemMst.setBestBefore(rs2.getInt("best_before"));
			itemMst.setReorderWaitingPeriod(rs2.getInt("reorder_waiting_period"));
			itemMst.setOldId(rs2.getString("id"));
			itemMst.setBarCode(rs2.getString("bar_code"));
			itemMst.setItemName(rs2.getString("item_name"));
			itemMst.setCategoryId(rs2.getString("category_id"));
			itemMst.setId(rs2.getString("id"));
			itemMst.setServiceOrGoods(rs2.getString("service_or_goods"));
			itemMst.setUnitId(rs2.getString("unit_id"));
			itemMst.setBranchCode(rs2.getString("branch_code"));
			itemMst.setMachineId(rs2.getString("machine_id"));
			itemMst.setItemGroupId(rs2.getString("item_group_id"));
			itemMst.setItemGenericName(rs2.getString("item_generic_name"));
			itemMst.setItemCode(rs2.getString("item_code"));
			itemMst.setBinNo(rs2.getString("bin_no"));
			itemMst.setItemPriceLock(rs2.getString("item_price_lock"));
			itemMst.setItemDtl(rs2.getString("item_dtl"));
			itemMst.setBarCodeLine1(rs2.getString("bar_code_line1"));
			itemMst.setBarCodeLine2(rs2.getString("bar_code_line2"));
			itemMst.setSupplierId(rs2.getString("supplier_id"));
			itemMst.setHsnCode(rs2.getString("hsn_code"));
			itemMst.setIsDeleted(rs2.getString("is_deleted"));
			itemMst.setProductId(rs2.getString("product_id"));
			itemMst.setBrandId(rs2.getString("brand_id"));
			itemMst.setModel(rs2.getString("model"));
			itemMst.setItemDescription(rs2.getString("item_description"));
			itemMst.setNegativeBillLock(rs2.getString("negative_bill_lock"));
			itemMst.setNetWeight(rs2.getString("net_weight"));

			itemMst.setCompanyMst(companyMst);
			itemMst.setRank(0);

			itemMst = itemMstRepository.save(itemMst);

		}
		rs2.close();
		pst2.close();
		
		return "OK";

	}

	@Override
	public String menuConfigMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * MenuConfigMst
		 */

		String sqlstr2 = " select  * from menu_config_mst";

		 
		try {
			PreparedStatement 	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				MenuConfigMst menuConfigMst = new MenuConfigMst();

				menuConfigMst.setId(rs2.getString("id"));
				menuConfigMst.setOldId(rs2.getString("id"));
				menuConfigMst.setMenuName(rs2.getString("menu_name"));
				menuConfigMst.setMenuFxml(rs2.getString("menu_fxml"));
				menuConfigMst.setMenuDescription(rs2.getString("menu_description"));
				menuConfigMst.setCustomiseSalesMode(rs2.getString("customise_sales_mode"));

				menuConfigMst.setCompanyMst(companyMst);
				// menuConfigMst.setRank(0);

				menuConfigMst = menuConfigMstRepository.save(menuConfigMst);

				System.out.println(menuConfigMst);

			}
			
			rs2.close();
			
			pst2.close();

//			menuConfigMstRepository.updateMenuConfigId();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		
		
		return "OK";

	}

	@Override
	public String processPermissionMstTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		/*
		 * processPermissionMst
		 */

		String sqlstr2 = " select  * from  process_permission_mst";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			ProcessPermissionMst processPermissionMst = new ProcessPermissionMst();

			processPermissionMst.setId(rs2.getString("id"));
			processPermissionMst.setOldId(rs2.getString("old_id"));
			processPermissionMst.setUserId(rs2.getString("user_id"));
			processPermissionMst.setProcessId(rs2.getString("process_id"));
			processPermissionMst.setEndDate(rs2.getDate("end_date"));

			processPermissionMst.setCompanyMst(companyMst);
			// menuConfigMst.setRank(0);

			processPermissionMst = processPermissionRepository.save(processPermissionMst);

		}
		
		rs2.close();
		pst2.close();
		
		return "OK";

	}

	@Override

	public String sysDateMstTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		/*
		 * SysDateMst
		 */

		String sqlstr2 = " select  * from  sys_date_mst";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			SysDateMst sysDateMst = new SysDateMst();

			sysDateMst.setId(rs2.getString("id"));
			sysDateMst.setOldId(rs2.getString("old_id"));
			sysDateMst.setAplicationDate(rs2.getDate("aplication_date"));
			sysDateMst.setSystemDate(rs2.getDate("system_date"));
			sysDateMst.setDayEndDone(rs2.getString("day_end_done"));
			sysDateMst.setBranchCode(rs2.getString("branch_code"));

			sysDateMst.setCompanyMst(companyMst);

			sysDateMst = sysDateMstRepository.save(sysDateMst);

		}
		pst2.close();
		rs2.close();
		return "OK";

	}

	@Override

	public String stockTransferOutTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		/*
		 * StockTransferOutHdr
		 */

		String sqlstr2 = " select  * from  stock_transfer_out_hdr";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			StockTransferOutHdr stockTransferOutHdr = new StockTransferOutHdr();

			stockTransferOutHdr.setId(rs2.getString("id"));
			stockTransferOutHdr.setIntentNumber(rs2.getString("intentNumber"));
			stockTransferOutHdr.setVoucherDate(rs2.getDate("voucher_date"));
			stockTransferOutHdr.setVoucherNumber(rs2.getString("voucher_number"));
			stockTransferOutHdr.setVoucherType(rs2.getString("voucher_type"));
			stockTransferOutHdr.setToBranch(rs2.getString("to_branch"));
			stockTransferOutHdr.setDeleted(rs2.getString("deleted"));
			stockTransferOutHdr.setFromBranch(rs2.getString("from_branch"));
			stockTransferOutHdr.setInVoucherNumber(rs2.getString("in_voucher_number"));
			stockTransferOutHdr.setInVoucherDate(rs2.getDate("in_voucher_date"));
			stockTransferOutHdr.setStatus(rs2.getString("status"));

			stockTransferOutHdr.setCompanyMst(companyMst);

			stockTransferOutHdr = stockTransferOutHdrRepository.save(stockTransferOutHdr);

		}
		
		rs2.close();
		pst2.close();
		return "OK";

	}

	/*
	 * StockTransferOutDtl
	 */
	@Override

	public String stockTransferOutHdrTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		String sqlstr3 = " select  * from  stock_transfer_out_hdr";

		PreparedStatement pst3 = RestserverApplication.localCn.prepareStatement(sqlstr3);

		ResultSet rs3 = pst3.executeQuery();

		while (rs3.next()) {

			StockTransferOutDtl stockTransferOutDtl = new StockTransferOutDtl();

			stockTransferOutDtl.setId(rs3.getString("id"));
			stockTransferOutDtl.setSlNo(rs3.getInt("sl_no"));
			stockTransferOutDtl.setBatch(rs3.getString("batch"));
			stockTransferOutDtl.setQty(rs3.getDouble("qty"));
			stockTransferOutDtl.setRate(rs3.getDouble("rate"));
			stockTransferOutDtl.setItemCode(rs3.getString("item_code"));
			stockTransferOutDtl.setTaxRate(rs3.getDouble("tax_rate"));
			stockTransferOutDtl.setExpiryDate(rs3.getDate("expiry_date"));
			stockTransferOutDtl.setAmount(rs3.getDouble("amount"));
			stockTransferOutDtl.setUnitId(rs3.getString("unit_id"));
			Optional<ItemMst> itemmst = itemMstRepository.findById(rs3.getString("item_id"));
			stockTransferOutDtl.setItemId(itemmst.get());
			stockTransferOutDtl.setMrp(rs3.getDouble("mrp"));
			stockTransferOutDtl.setBarcode(rs3.getString("barcode"));
			stockTransferOutDtl.setIntentDtlId(rs3.getString("intent_dtl_id"));

			stockTransferOutDtl.setCompanyMst(companyMst);

			stockTransferOutDtl = stockTransferOutDtlRepository.save(stockTransferOutDtl);

		}
		
		rs3.close();
		pst3.close();
		return "OK";

	}

	@Override
	public String deliveryBoyMstTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		/*
		 * DeliveryBoyMst
		 */

		String sqlstr2 = " select  * from  delivery_boy_mst";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			DeliveryBoyMst deliveryBoyMst = new DeliveryBoyMst();
			deliveryBoyMst.setId(rs2.getString("id"));
			deliveryBoyMst.setOldId(rs2.getString("old_id"));
			deliveryBoyMst.setDeliveryBoyName(rs2.getString("delivery_boy_name"));
			deliveryBoyMst.setBranchCode(rs2.getString("branch_code"));
			deliveryBoyMst.setStatus(rs2.getString("status"));

			deliveryBoyMst.setCompanyMst(companyMst);

			deliveryBoyMst = deliveryBoyMstRepository.save(deliveryBoyMst);

		}	pst2.close();
		rs2.close();
		return "OK";

	}

	@Override

	public String addKotWaiterTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		/*
		 * AddKotWaiter
		 */

		String sqlstr2 = " select  * from  add_kot_waiter";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			AddKotWaiter addKotWaiter = new AddKotWaiter();

			addKotWaiter.setId(rs2.getString("id"));
			addKotWaiter.setOldId(rs2.getString("old_id"));
			addKotWaiter.setStatus(rs2.getString("status"));
			addKotWaiter.setWaiterName(rs2.getString("waiter_name"));
			addKotWaiter.setBranchCode(rs2.getString("branch_code"));

			addKotWaiter.setCompanyMst(companyMst);

			addKotWaiter = addKotWaiterRepository.save(addKotWaiter);

		}
		
		rs2.close();
		pst2.close();
		
		return "OK";

	}

	@Override
	public String financialYearMstTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		/*
		 * FinancialYearMst
		 */

		String sqlstr2 = " select  * from  financial_year_mst";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			FinancialYearMst financialYearMst = new FinancialYearMst();

			financialYearMst.setId(rs2.getInt("id"));
			financialYearMst.setOldId(rs2.getInt("id"));
			financialYearMst.setUserName(rs2.getString("user_name"));
			financialYearMst.setStartDate(rs2.getDate("start_date"));
			financialYearMst.setEndDate(rs2.getDate("end_date"));
			financialYearMst.setFinancialYear(rs2.getString("financial_year"));
			financialYearMst.setCurrentFinancialYear(rs2.getInt("current_financial_year"));
			financialYearMst.setOpenCloseStatus(rs2.getString("open_close_status"));

			financialYearMst.setCompanyMst(companyMst);

			financialYearMst = financialYearMstRepository.save(financialYearMst);

		}
		
		rs2.close();
		pst2.close();
		
		return "OK";

	}

	@Override

	public String kotCategoryMstTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		/*
		 * KotCategoryMst
		 */

		String sqlstr2 = " select  * from  kot_category_mst";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			KotCategoryMst kotCategoryMst = new KotCategoryMst();

			kotCategoryMst.setId(rs2.getString("id"));
			kotCategoryMst.setOldId(rs2.getString("id"));
			kotCategoryMst.setCategoryId(rs2.getString("category_id"));
			kotCategoryMst.setPrinterId(rs2.getString("printer_id"));

			kotCategoryMst.setCompanyMst(companyMst);

			kotCategoryMst = kotCategoryMstRepository.save(kotCategoryMst);

		}	pst2.close();
		rs2.close();
		return "OK";

	}

	@Override
	public String unitMstTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		/*
		 * UnitMst
		 */

		String sqlstr2 = " select  * from  unit_mst";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			UnitMst unitMstOpt = unitMstRepository.findByUnitName(rs2.getString("unit_name"));

			if (null == unitMstOpt) {

				UnitMst unitMst = new UnitMst();

				unitMst.setId(rs2.getString("id"));
				unitMst.setOldId(rs2.getString("id"));
				unitMst.setUnitName(rs2.getString("unit_name"));
				unitMst.setUnitDescription(rs2.getString("unit_description"));
				unitMst.setUnitPrecision(rs2.getDouble("unit_precision"));
				unitMst.setBranchCode(rs2.getString("branch_code"));

				unitMst.setCompanyMst(companyMst);

				unitMst = unitMstRepository.save(unitMst);

			}

		}	pst2.close();
		rs2.close();
		return "OK";

	}

	@Override
	public String productionTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		/*
		 * ProductionMst
		 */

		String sqlstr2 = " select  * from  production_mst";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			ProductionMst productionMst = new ProductionMst();

			productionMst.setId(rs2.getString("id"));
			productionMst.setOldId(rs2.getString("id"));
			productionMst.setBranchCode(rs2.getString("branch_code"));
			productionMst.setVoucherNumber(rs2.getString("voucher_number"));
			productionMst.setVoucherDate(rs2.getDate("voucher_date"));

			productionMst.setCompanyMst(companyMst);

			productionMst = productionMstRepository.save(productionMst);

		}
		
		rs2.close();
		pst2.close();
		return "OK";
	}

	/*
	 * ProductionDtl
	 */
	@Override
	public String productionDtlTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		String sqlstr3 = " select  * from  production_dtl";

		PreparedStatement pst3 = RestserverApplication.localCn.prepareStatement(sqlstr3);

		ResultSet rs3 = pst3.executeQuery();

		while (rs3.next()) {

			ProductionDtl productionDtl = new ProductionDtl();

			productionDtl.setId(rs3.getString("id"));
			productionDtl.setOldId(rs3.getString("id"));
			productionDtl.setItemId(rs3.getString("item_id"));
			productionDtl.setBatch(rs3.getString("batch"));
			productionDtl.setQty(rs3.getDouble("qty"));
			productionDtl.setStatus(rs3.getString("status"));
			productionDtl = productionDtlRepository.save(productionDtl);

		}

		/*
		 * ProductionDtlDtl
		 */

		String sqlstr4 = " select  * from  production_dtl_dtl";

		PreparedStatement pst4 = RestserverApplication.localCn.prepareStatement(sqlstr4);

		ResultSet rs4 = pst4.executeQuery();

		while (rs4.next()) {

			ProductionDtlDtl productionDtlDtl = new ProductionDtlDtl();

			productionDtlDtl.setId(rs4.getString("id"));
			productionDtlDtl.setOldId(rs4.getString("id"));
			productionDtlDtl.setRawMaterialItemId(rs4.getString("raw_material_item_id"));
			productionDtlDtl.setItemName(rs4.getString("item_name"));
			productionDtlDtl.setQty(rs4.getDouble("qty"));
			productionDtlDtl.setUnitId(rs4.getString("unitId"));

			productionDtlDtl = productionDtlDtlRepository.save(productionDtlDtl);

		}
		rs3.close();
		pst3.close();
		
		
		rs4.close();
		pst4.close();
		return "OK";

	}

	@Override
	public String damageTransfermysql(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {

		/*
		 * DamageHdr
		 */

		String sqlstr2 = " select  * from  damage_hdr";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			DamageHdr damageHdr = new DamageHdr();

			damageHdr.setId(rs2.getString("id"));
			damageHdr.setOldId(rs2.getString("id"));
			damageHdr.setVoucheNumber(rs2.getString("vouche_number"));
			damageHdr.setVoucherDate(rs2.getDate("voucher_date"));

			damageHdr.setCompanyMst(companyMst);

			damageHdr = damageHdrRepository.save(damageHdr);

		}
		
		
		rs2.close();
		pst2.close() ;
		

		/*
		 * DamageDtl
		 */

		String sqlstr3 = " select  * from  damage_dtl";

		PreparedStatement pst3 = RestserverApplication.localCn.prepareStatement(sqlstr3);

		ResultSet rs3 = pst3.executeQuery();

		while (rs3.next()) {

			DamageDtl damageDtl = new DamageDtl();

			damageDtl.setId(rs2.getString("id"));
			damageDtl.setOldId(rs2.getString("id"));
			damageDtl.setItemId(rs2.getString("item_id"));
			damageDtl.setQty(rs2.getDouble("qty"));
			damageDtl.setBatchCode(rs2.getString("batch_code"));
			damageDtl.setNarration(rs2.getString("narration"));

			damageDtl.setCompanyMst(companyMst);

			damageDtl = damageDtlRepository.save(damageDtl);

		}
		
		rs3.close();
		
		pst3.close();
		return "OK";

	}

	@Override
	public String priceDefenitionTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		/*
		 * PriceDefenitionMst
		 */

		String sqlstr2 = " select  * from  price_defenition_mst";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			PriceDefenitionMst priceDefenitionMstByName = priceDefenitionMstRepository
					.findByCompanyMstAndPriceLevelName(companyMst, rs2.getString("price_level_name"));

			if (null == priceDefenitionMstByName) {
				PriceDefenitionMst priceDefenitionMst = new PriceDefenitionMst();

				priceDefenitionMst.setId(rs2.getString("id"));
				priceDefenitionMst.setOldId(rs2.getString("id"));
				priceDefenitionMst.setPriceLevelName(rs2.getString("price_level_name"));
				priceDefenitionMst.setPriceCalculator(rs2.getString("price_calculator"));

				priceDefenitionMst.setCompanyMst(companyMst);

				priceDefenitionMst = priceDefenitionMstRepository.save(priceDefenitionMst);
			}

		}

		/*
		 * PriceDefinition
		 */

		String sqlstr3 = " select  * from  price_definition";

		PreparedStatement pst3 = RestserverApplication.localCn.prepareStatement(sqlstr3);

		ResultSet rs3 = pst3.executeQuery();

		while (rs3.next()) {

			PriceDefinition priceDefinition = new PriceDefinition();

			priceDefinition.setId(rs3.getString("id"));
			priceDefinition.setItemId(rs3.getString("item_id"));
			priceDefinition.setPriceId(rs3.getString("price_id"));
			priceDefinition.setAmount(rs3.getDouble("amount"));
			priceDefinition.setUnitId(rs3.getString("unit_id"));
			priceDefinition.setStartDate(rs3.getDate("start_date"));
			priceDefinition.setEndDate(rs3.getDate("end_date"));
			priceDefinition.setBranchCode(rs3.getString("branch_code"));

			priceDefinition.setCompanyMst(companyMst);
			priceDefinition = priceDefinitionRepository.save(priceDefinition);

		}

		/*
		 * PriceBatchMst
		 */

		String sqlstr4 = " select  * from  price_batch_mst";

		PreparedStatement pst4 = RestserverApplication.localCn.prepareStatement(sqlstr4);

		ResultSet rs4 = pst4.executeQuery();

		while (rs4.next()) {

			PriceBatchMst priceBatchMst = new PriceBatchMst();

			priceBatchMst.setId(rs4.getString("id"));
			priceBatchMst.setOldId(rs4.getString("id"));
			priceBatchMst.setBatchCode(rs4.getString("batch_code"));
			priceBatchMst.setBranchCode(rs4.getString("branch_code"));
			priceBatchMst.setPrice(rs4.getDouble("price"));

			priceBatchMst.setCompanyMst(companyMst);
			priceBatchMst = priceBatchMstRepository.save(priceBatchMst);

		}
		
		rs4.close();
		pst4.close() ;
		
		
		rs3.close();
		pst3.close() ;
		
		rs2.close();
		pst2.close() ;
		
		
		return "OK";

	}

	@Override
	public String salesOrderTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		/*
		 * SalesOrderDtl
		 */

		String sqlstr2 = " select  * from  sales_order_dtl";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			SalesOrderDtl salesOrderDtl = new SalesOrderDtl();

			salesOrderDtl.setId(rs2.getString("id"));
			salesOrderDtl.setItemId(rs2.getString("itemId"));
			salesOrderDtl.setRate(rs2.getDouble("rate"));
			salesOrderDtl.setCgstTaxRate(rs2.getDouble("cgst_tax_rate"));
			salesOrderDtl.setSgstTaxRate(rs2.getDouble("sgst_tax_rate"));
			salesOrderDtl.setCessRate(rs2.getDouble("cess_rate"));
			salesOrderDtl.setQty(rs2.getDouble("qty"));
			salesOrderDtl.setAddCessRate(rs2.getDouble("add_cess_rate"));
			salesOrderDtl.setRateBeforeDiscount(rs2.getDouble("rate_before_discount"));
			salesOrderDtl.setAddCessAmount(rs2.getDouble("add_cess_amount"));
			salesOrderDtl.setItemTaxId(rs2.getString("item_tax_id"));
			salesOrderDtl.setUnitId(rs2.getString("unit_id"));
			salesOrderDtl.setItemName(rs2.getString("item_name"));
			salesOrderDtl.setExpiryDate(rs2.getDate("expiry_date"));
			salesOrderDtl.setBatch(rs2.getString("batch"));
			salesOrderDtl.setBarode(rs2.getString("barode"));
			salesOrderDtl.setTaxRate(rs2.getDouble("tax_rate"));
			salesOrderDtl.setMrp(rs2.getDouble("mrp"));
			salesOrderDtl.setDiscount(rs2.getDouble("discount"));
			salesOrderDtl.setIgstTaxRate(rs2.getDouble("igst_tax_rate"));
			salesOrderDtl.setCgstAmount(rs2.getDouble("cgst_amount"));
			salesOrderDtl.setSgstAmount(rs2.getDouble("sgst_amount"));
			salesOrderDtl.setIgstAmount(rs2.getDouble("igst_amount"));
			salesOrderDtl.setCessAmount(rs2.getDouble("cess_amount"));
			salesOrderDtl.setAmount(rs2.getDouble("amount"));
			salesOrderDtl.setStandardPrice(rs2.getDouble("standard_price"));

			salesOrderDtl.setItemCode(rs2.getString("item_code"));
			salesOrderDtl.setUnitName(rs2.getString("unit_Name"));
			salesOrderDtl.setOrderMsg(rs2.getString("order_msg"));
			salesOrderDtl.setOrderAdvice(rs2.getString("order_advice"));

			salesOrderDtl = salesOrderDtlRepository.save(salesOrderDtl);

		}
		pst2.close();
		rs2.close();
		return "OK";

	}

	@Override
	public String transferKotTable(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {
		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select * from  add_kot_table where company_mst='" + companyMst + "' and branch_code='"
				+ branchCode + "'";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			String id = rs2.getString("id");
			String oldid = rs2.getString("id");
			String status = rs2.getString("status");
			String tableName = rs2.getString("table_name");

			AddKotTable addKotTable = new AddKotTable();

			addKotTable.setBranchCode(branchCode);
			addKotTable.setCompanyMst(companyMst);
			addKotTable.setId(id);
			addKotTable.setStatus(status);
			addKotTable.setTableName(tableName);

			addKotTable = addKotTableRepository.save(addKotTable);

		}
		pst2.close();
		rs2.close();
		return null;
	}

	@Override
	public String transferGroupMst(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {
		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select * from group_mst where company_mst='" + companyMst + "' and branch_code='" + branchCode
				+ "'";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			String id = rs2.getString("id");
			String oldid = rs2.getString("id");
			String groupName = rs2.getString("group_name");

			GroupMst groupMst = new GroupMst();

			groupMst.setBranchCode(branchCode);
			groupMst.setCompanyMst(companyMst);
			groupMst.setGroupName(groupName);
			groupMst.setId(id);

			groupMst = groupMstRepository.save(groupMst);

		}
		
		rs2.close();
		pst2.close() ;

		return null;
	}

	@Override
	public String transferGroupPermission(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {
		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select * from group_mst where company_mst='" + companyMst + "' and branch_code='" + branchCode
				+ "'";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			String id = rs2.getString("id");
			String oldid = rs2.getString("id");
			String groupId = rs2.getString("group_id");
			String processId = rs2.getString("process_id");

			GroupPermissionMst groupMst = new GroupPermissionMst();

			groupMst.setCompanyMst(companyMst);
			groupMst.setId(id);
			groupMst.setGroupId(groupId);
			groupMst.setProcessId(processId);

			groupMst = groupPermissionRepository.save(groupMst);

		}
		pst2.close();
		rs2.close();
		return null;
	}

	@Override
	public String transferProductConversion(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {
		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select * from product_conversion_config_mst where company_mst='" + companyMst
				+ "' and branch_code='" + branchCode + "'";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			Optional<ItemMst> itemMstOpt = itemMstRepository.findById(rs2.getString("from_item_id"));
			ItemMst itemMst = itemMstOpt.get();

			if (null == itemMst) {
//				transferItemMst(rs2.getString("from_item_id"));
			}

			Optional<ItemMst> itemMstOpt2 = itemMstRepository.findById(rs2.getString("to_item_id"));
			ItemMst itemMst2 = itemMstOpt2.get();

			if (null == itemMst2) {
//				transferItemMst(rs2.getString("from_item_id"));
			}

			ProductConversionConfigMst productConversionConfigMst = new ProductConversionConfigMst();
			productConversionConfigMst.setBranchCode(branchCode);
			productConversionConfigMst.setOldId(rs2.getString("id"));
			productConversionConfigMst.setCompanyMst(companyMst);
			productConversionConfigMst.setFromItemId(rs2.getString("from_item_id"));
			productConversionConfigMst.setId(rs2.getString("id"));
			productConversionConfigMst.setToItemId(rs2.getString("to_item_id"));

			productConversionConfigMst = productConfigurationMstRepository.save(productConversionConfigMst);

		}
		pst2.close();
		rs2.close();
		return null;
	}

	// String transferCompanyMst(CompanyMst companyMst)throws UnknownHostException,
	// Exception;

	// 2. CategoryMst

	@Override
	public String transferCategoryMst(CompanyMst companyMst, String branchCode) {

		try {

			String sqlstr2 = " select  * from category_mst   ";

			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				CategoryMst category = new CategoryMst();

				category.setId(rs2.getString("id"));

				category.setOldId(rs2.getString("id"));
				category.setCategoryName(rs2.getString("category_name"));
				category.setParentId(rs2.getString("parent_id"));
				category.setBranchCode(rs2.getString("branch_code"));
				category.setCompanyMst(companyMst);

				category = categoryMstRepository.saveAndFlush(category);

			}
			
			pst2.close();
			rs2.close();
		} catch (Exception e) {

		}
		return "OK";

	}
	// 3. BranchMst

	@Override
	public String transferBranchMst(CompanyMst companyMst, String branchCode) {

		try {

			String sqlstr2 = " select  * from branch_mst   ";

			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				BranchMst branch = new BranchMst();

				branch.setId(rs2.getString("id"));
				branch.setOldId(rs2.getString("id"));
				branch.setBranchName(rs2.getString("branch_name"));
				branch.setBranchGst(rs2.getString("branch_gst"));
				branch.setBranchCode(rs2.getString("branch_code"));
				branch.setBranchState(rs2.getString("branch_state"));
				branch.setMyBranch(rs2.getString("my_branch"));
				branch.setBranchAddress1(rs2.getString("branch_address1"));
				branch.setBranchAddress2(rs2.getString("branch_address2"));
				branch.setBranchTelNo(rs2.getString("branch_telNo"));
				;
				branch.setBranchEmail(rs2.getString("branch_email"));
				branch.setBankName(rs2.getString("bank_name"));
				branch.setAccountNumber(rs2.getString("account_number"));
				branch.setBankBranch(rs2.getString("bank_branch"));
				branch.setBranchPlace(rs2.getString("branch_place"));
				branch.setIfsc(rs2.getString("ifsc"));
				branch.setBranchWebsite(rs2.getString("branch_website"));

				branch.setCompanyMst(companyMst);

				branch = branchMstRepository.saveAndFlush(branch);

			}
			
			pst2.close();
			rs2.close();
			
		} catch (Exception e) {

		}
	 
		return "OK";

	}

	// 5. DebitClass

	// 6. LedgerClass

	// 7. menuMst

	@Override
	public String transferMenuMst(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from menu_mst   ";
//1
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				MenuMst menuMst = new MenuMst();

				menuMst.setId(rs2.getString("id"));
				menuMst.setOldId(rs2.getString("id"));
				menuMst.setMenuId(rs2.getString("menu_id"));
				menuMst.setParentId(rs2.getString("parent_id"));
				menuMst.setBranchCode(rs2.getString("branch_code"));

				menuMst.setCompanyMst(companyMst);

				menuMst = menuMstRepository.saveAndFlush(menuMst);

			}
			
			rs2.close();
			pst2.close() ;
			
			
		} catch (Exception e) {

		}
		return "OK";

	}
	// 8. StoreMst

	@Override
	public String transferStoreMst(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from store_mst   ";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				StoreMst storeMst = new StoreMst();

				storeMst.setId(rs2.getString("id"));
				storeMst.setOldId(rs2.getString("id"));
				storeMst.setName(rs2.getString("name"));
				storeMst.setShortCode(rs2.getString("short_code"));
				storeMst.setBranchCode(rs2.getString("branch_code"));
				storeMst.setMobile(rs2.getString("mobile"));

				storeMst.setCompanyMst(companyMst);

				storeMst = storeMstRepository.saveAndFlush(storeMst);

			}
			
			rs2.close();
			pst2.close() ;
			
		} catch (Exception e) {

		}
		return "OK";

	}

	// 9. UserMst

	@Override
	public String transferUserMstMysql(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from user_mst   ";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				UserMst userMsrt = userMstRepository.findByUserNameAndCompanyMstId(rs2.getString("user_name"),
						companyMst.getId());

				if (null == userMsrt) {

					UserMst userMst = new UserMst();

					userMst.setId(rs2.getString("id"));
					userMst.setOldId(rs2.getString("id"));
					userMst.setUserName(rs2.getString("user_name"));
					userMst.setBranchCode(rs2.getString("branch_code"));
					userMst.setFullName(rs2.getString("full_name"));
					userMst.setBranchName(rs2.getString("branch_name"));
					userMst.setPassword(rs2.getString("password"));
					userMst.setStatus(rs2.getString("status"));

					userMst.setCompanyMst(companyMst);

					userMst = userMstRepository.saveAndFlush(userMst);
				}

			}
			
			rs2.close();
			pst2.close() ;
			
			
		} catch (Exception e) {

		}
		return "OK";

	}

	// 10. Purchasehdr

	@Override
	public String transferPurchaseHdrMysql(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from purchase_hdr ";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				PurchaseHdr purchaseHdr = new PurchaseHdr();

				purchaseHdr.setId(rs2.getString("id"));
				purchaseHdr.setOldId(rs2.getString("id"));
				purchaseHdr.setSupplierId(rs2.getString("supplier_id"));
				purchaseHdr.setFinalSavedStatus(rs2.getString("final_saved_status"));
				purchaseHdr.setMachineId(rs2.getString("machine_id"));
				purchaseHdr.setBranchCode(rs2.getString("branch_code"));
				purchaseHdr.setDeletedStatus(rs2.getString("deleted_status"));
				purchaseHdr.setNarration(rs2.getString("narration"));
				purchaseHdr.setPurchaseType(rs2.getString("purchase_type"));
				purchaseHdr.setpONum(rs2.getString("pO_num"));
				purchaseHdr.setPoDate(rs2.getDate("po_date"));
				purchaseHdr.setSupplierInvNo(rs2.getString("supplier_invNo"));
				purchaseHdr.setVoucherNumber(rs2.getString("voucher_number"));
				purchaseHdr.setInvoiceTotal(rs2.getDouble("invoice_total"));
				purchaseHdr.setFcInvoiceTotal(rs2.getDouble("fcInvoice_total"));
				purchaseHdr.setCurrency(rs2.getString("currency"));
				purchaseHdr.setSupplierInvDate(rs2.getDate("supplier_inv_date"));
				purchaseHdr.setVoucherDate(rs2.getDate("voucher_date"));
				purchaseHdr.setTansactionEntryDate(rs2.getDate("tansaction_entry_date"));
				purchaseHdr.setEnableBatchStatus(rs2.getInt("enable_batch_status"));
				purchaseHdr.setUserId(rs2.getString("user_id"));
				purchaseHdr.setVoucherType(rs2.getString("voucher_type"));

				purchaseHdr.setCompanyMst(companyMst);

				purchaseHdr = purchaseHdrRepository.saveAndFlush(purchaseHdr);

				transferPurchaseDtl(purchaseHdr);

			}
			
			rs2.close();
			pst2.close() ;
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return "OK";

	}

	// 11. Purchasedtl
	private String transferPurchaseDtl(PurchaseHdr purchaseHdr) {

		String sqlstr2 = " select  * from purchase_dtl where  purchase_hdr_id='" + purchaseHdr.getOldId() + "'";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				PurchaseDtl purchaseDtl = new PurchaseDtl();

				String purchase_hdr_id = rs2.getString("purchase_hdr_id");

				purchaseDtl.setPurchaseHdr(purchaseHdr);

				purchaseDtl.setId(rs2.getString("id"));
				purchaseDtl.setPurchaseOrderDtl(rs2.getString("purchase_order_dtl"));
				purchaseDtl.setItemName(rs2.getString("item_name"));
				purchaseDtl.setPurchseRate(rs2.getDouble("purchse_rate"));
				purchaseDtl.setFcPurchaseRate(rs2.getDouble("fc_purchase_rate"));
				purchaseDtl.setAmount(rs2.getDouble("amount"));
				purchaseDtl.setFcAmount(rs2.getDouble("fc_amount"));
				purchaseDtl.setBatch(rs2.getString("batch"));
				purchaseDtl.setItemSerial(rs2.getInt("item_serial"));
				purchaseDtl.setBarcode(rs2.getString("barcode"));
				purchaseDtl.setTaxAmt(rs2.getDouble("tax_amt"));
				purchaseDtl.setFcTaxAmt(rs2.getDouble("fc_tax_amt"));
				purchaseDtl.setCessAmt(rs2.getDouble("cess_amt"));
				purchaseDtl.setFcCessAmt(rs2.getDouble("fc_cess_amt"));
				purchaseDtl.setPreviousMRP(rs2.getDouble("previous_m_r_p"));
				purchaseDtl.setExpiryDate(rs2.getDate("expiry_date"));
				purchaseDtl.setFreeQty(rs2.getInt("free_qty"));
				purchaseDtl.setQty(rs2.getDouble("qty"));
				purchaseDtl.setTaxRate(rs2.getDouble("tax_rate"));
				purchaseDtl.setFcTaxRate(rs2.getDouble("fc_tax_rate"));
				purchaseDtl.setDiscount(rs2.getDouble("discount"));
				purchaseDtl.setFcDiscount(rs2.getDouble("fc_discount"));
				purchaseDtl.setFcDiscount(rs2.getDouble("fc_discount"));
				purchaseDtl.setChangePriceStatus(rs2.getString("change_price_status"));
				purchaseDtl.setUnit(rs2.getInt("unit"));
				purchaseDtl.setMrp(rs2.getDouble("mrp"));
				purchaseDtl.setCessRate(rs2.getDouble("cess_rate"));
				purchaseDtl.setFcCessRate(rs2.getDouble("fc_cess_rate"));
				purchaseDtl.setManufactureDate(rs2.getDate("manufacture_date"));
				purchaseDtl.setItemId(rs2.getString("item_id"));
				purchaseDtl.setBinNo(rs2.getString("bin_no"));
				purchaseDtl.setFcNetCost(rs2.getDouble("fc_net_cost"));
				purchaseDtl.setUnitId(rs2.getString("unit_id"));
				purchaseDtl.setCessRate2(rs2.getDouble("Cess_rate2"));
				purchaseDtl.setFcCessRate2(rs2.getDouble("fc_cess_rate2"));
				purchaseDtl.setNetCost(rs2.getDouble("net_cost"));
				purchaseDtl.setFcMrp(rs2.getDouble("fc_mrp"));

				purchaseDtl = purchaseDtlRepository.saveAndFlush(purchaseDtl);

			}
			
			pst2.close();
			rs2.close();
			
		} catch (Exception e) {

		}
		return "OK";

	}

//12. Called as part Of sales Sales Hdr
	private String transferSalesDtl(CompanyMst companyMst, SalesTransHdr salesTransHdr) {

		String sqlstr2 = " select  * from sales_dtl  where  sales_trans_hdr_id ='" + salesTransHdr.getId() + "'";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SalesDtl salesDtl = new SalesDtl();

				// String sales_trans_hdr_id = rs2.getString("sales_trans_hdr_id");
				// SalesTransHdr salesTransHdr=
				// salesTransHdrRepository.findById(sales_trans_hdr_id).get();

				salesDtl.setSalesTransHdr(salesTransHdr);

//				salesDtl.setAddCessAmount(rs2.getString("add_cess_amount"));
//				salesDtl.setAddCessRate(rs2.getString("add_cess_rate"));
//				salesDtl.setAmount(rs2.getString("amount"));
//				salesDtl.setBarcode(rs2.getString("barcode"));
//				salesDtl.setBatch(rs2.getString("batch"));
//				salesDtl.setCessAmount(rs2.getString("cess_amount"));
//				salesDtl.setCessRate(rs2.getString("cess_rate"));
//				salesDtl.setCgstAmount(rs2.getString("cgst_amount"));
//				salesDtl.setCgstTaxRate(rs2.getString("cgst_tax_rate"));
//				salesDtl.setCompanyMst(companyMst);
//				salesDtl.setCostPrice(rs2.getString("cost_price"));
//				salesDtl.setDiscount(rs2.getString("discount"));
//				salesDtl.setExpiryDate(rs2.getString("expiry_date"));
//				salesDtl.setFbKey(rs2.getString("fb_key"));
////				salesDtl.setFcAmount(rs2.getString("id"));
////				salesDtl.setFcCessAmount(rs2.getString("id"));
////				salesDtl.setFcCessRate(rs2.getString("id"));
////				salesDtl.setFcCgst(rs2.getString("id"));
////				salesDtl.setFcDiscount(rs2.getString("id"));
////				salesDtl.setFcIgstAmount(rs2.getString("id"));
////				salesDtl.setFcIgstRate(rs2.getString("id"));
////				salesDtl.setFcMrp(rs2.getString("id"));
////				salesDtl.setFcRate(rs2.getString("id"));
////				salesDtl.setFcSgst(rs2.getString("id"));
////				salesDtl.setFcStandardPrice(rs2.getString("id"));
////				salesDtl.setFcTaxAmount(rs2.getString("id"));
////				salesDtl.setFcTaxRate(rs2.getString("id"));
//				salesDtl.setId(rs2.getString("id"));
//				salesDtl.setIgstAmount(rs2.getString("igst_amount"));
//				salesDtl.setIgstTaxRate(rs2.getString("igst_tax_rate"));
//				salesDtl.setItemId(rs2.getString("item_id"));
//				salesDtl.setItemName(rs2.getString("item_name"));
//				salesDtl.setItemTaxaxId(rs2.getString("item_taxax_id"));
//				salesDtl.setKotDescription(rs2.getString("kot_description"));
//				salesDtl.setListPrice(rs2.getString("list_price"));
//				salesDtl.setMrp(rs2.getString("mrp"));
//				salesDtl.setOfferReferenceId(rs2.getString("offer_reference_id"));
//				salesDtl.setOldId(rs2.getString("old_id"));
//				salesDtl.setPrintKotStatus(rs2.getString("print_kot_status"));
//				salesDtl.setProcessInstanceId(rs2.getString("process_instance_id"));
//				salesDtl.setQty(rs2.getString("qty"));
//				salesDtl.setRate(rs2.getString("rate"));
//				salesDtl.setRateBeforeDiscount(rs2.getString("rate_before_discount"));
//				salesDtl.setReturnedQty(rs2.getString("returned_qty"));
//				salesDtl.setSalesTransHdr(salesTransHdr);
//				salesDtl.setSchemeId(rs2.getString("scheme_id"));
//				salesDtl.setSgstAmount(rs2.getString("sgst_amount"));
//				salesDtl.setSgstTaxRate(rs2.getString("sgst_tax_rate"));
//				salesDtl.setStandardPrice(rs2.getString("standard_price"));
//				salesDtl.setStatus(rs2.getString("status"));
//				salesDtl.setTaskId(rs2.getString("task_id"));
//				salesDtl.setTaxRate(rs2.getString("tax_rate"));
//				salesDtl.setUnitId(rs2.getString("unit_id"));
//				salesDtl.setUnitName(rs2.getString("unit_name"));
//				salesDtl.setUpdatedTime(rs2.getString("updated_time"));
//				salesDtl.setWarrantySerial(rs2.getString("warranty_serial"));

				salesDtl.setId(rs2.getString("id"));
				salesDtl.setOldId(rs2.getString("id"));
				salesDtl.setItemId(rs2.getString("item_id"));
				salesDtl.setQty(rs2.getDouble("qty"));
				salesDtl.setRate(rs2.getDouble("rate"));
				salesDtl.setCgstTaxRate(rs2.getDouble("cgst_tax_rate"));
				salesDtl.setSgstTaxRate(rs2.getDouble("sgst_tax_rate"));
				salesDtl.setCessRate(rs2.getDouble("cess_rate"));
				salesDtl.setAddCessRate(rs2.getDouble("add_cess_rate"));
				salesDtl.setIgstTaxRate(rs2.getDouble("igst_tax_rate"));
				salesDtl.setItemTaxaxId(rs2.getString("item_taxax_id"));
				salesDtl.setUnitId(rs2.getString("unit_id"));
				salesDtl.setItemName(rs2.getString("item_name"));
				salesDtl.setExpiryDate(rs2.getDate("expiry_date"));
				salesDtl.setBatch(rs2.getString("batch"));
				salesDtl.setBarcode(rs2.getString("barcode"));
				salesDtl.setTaxRate(rs2.getDouble("tax_rate"));
				salesDtl.setMrp(rs2.getDouble("mrp"));
				salesDtl.setAmount(rs2.getDouble("amount"));
				salesDtl.setUnitName(rs2.getString("unit_name"));
				salesDtl.setDiscount(rs2.getDouble("discount"));
				salesDtl.setCgstAmount(rs2.getDouble("cgst_amount"));
				salesDtl.setSgstAmount(rs2.getDouble("sgst_amount"));
				salesDtl.setIgstAmount(rs2.getDouble("igst_amount"));
				salesDtl.setCessAmount(rs2.getDouble("cess_amount"));
				salesDtl.setReturnedQty(rs2.getDouble("returned_qty"));
				salesDtl.setStatus(rs2.getString("status"));
				salesDtl.setAddCessAmount(rs2.getDouble("addCess_amount"));
				salesDtl.setCostPrice(rs2.getDouble("cost_price"));
				salesDtl.setPrintKotStatus(rs2.getString("print_kot_status"));
				salesDtl.setFcAmount(rs2.getDouble("fc_amount"));
				salesDtl.setFcRate(rs2.getDouble("fc_rate"));
				salesDtl.setFcIgstRate(rs2.getDouble("fc_igst_rate"));
				salesDtl.setFcCgst(rs2.getDouble("fc_cgst"));
				salesDtl.setFcSgst(rs2.getDouble("fc_sgst"));
				salesDtl.setFcTaxRate(rs2.getDouble("fc_tax_rate"));
				salesDtl.setFcTaxAmount(rs2.getDouble("fc_tax_amount"));
				salesDtl.setFcCessRate(rs2.getDouble("fc_cess_rate"));
				salesDtl.setFcCessRate(rs2.getDouble("fc_cess_rate"));
				salesDtl.setFcMrp(rs2.getDouble("fc_mrp"));
				salesDtl.setFcStandardPrice(rs2.getDouble("fc_standard_price"));
				salesDtl.setFcDiscount(rs2.getDouble("fc_discount"));
				salesDtl.setFcIgstAmount(rs2.getDouble("fcIgst_amount"));
				salesDtl.setKotDescription(rs2.getString("kot_description"));
				salesDtl.setOfferReferenceId(rs2.getString("offer_reference_id"));
				salesDtl.setSchemeId(rs2.getString("scheme_id"));
				salesDtl.setStandardPrice(rs2.getDouble("standard_price"));
				salesDtl.setFbKey(rs2.getString("fb_key"));
				salesDtl.setWarrantySerial(rs2.getString("warranty_serial"));
				salesDtl.setListPrice(rs2.getDouble("list_price"));
				salesDtl.setRateBeforeDiscount(rs2.getDouble("rate_before_discount"));

				salesDtl.setCompanyMst(companyMst);

				salesDtl = salesDetailsRepository.saveAndFlush(salesDtl);

			}
			
			rs2.close();
			pst2.close() ;
			
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return "OK";

	}

//13. SalesDltDtl
	@Override
	public String transferSalesDltDtl(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from sales_dlt_dtl ";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SalesDltdDtl salesDltdDtl = new SalesDltdDtl();

				salesDltdDtl.setId(rs2.getString("id"));
				salesDltdDtl.setQty(rs2.getDouble("qty"));
				salesDltdDtl.setRate(rs2.getDouble("rate"));
				salesDltdDtl.setCgstTaxRate(rs2.getDouble("cgst_tax_rate"));
				salesDltdDtl.setSgstTaxRate(rs2.getDouble("sgst_tax_rate"));
				salesDltdDtl.setCessRate(rs2.getDouble("cess_rate"));
				salesDltdDtl.setAddCessRate(rs2.getDouble("add_cess_rate"));
				salesDltdDtl.setIgstTaxRate(rs2.getDouble("igst_tax_rate"));
				salesDltdDtl.setItemTaxaxId(rs2.getString("item_taxax_id"));
				salesDltdDtl.setUnitId(rs2.getString("unit_id"));
				salesDltdDtl.setItemName(rs2.getString("item_name"));
				salesDltdDtl.setExpiryDate(rs2.getDate("expiry_date"));
				salesDltdDtl.setBatch(rs2.getString("batch"));
				salesDltdDtl.setBarcode(rs2.getString("barcode"));
				salesDltdDtl.setTaxRate(rs2.getDouble("tax_rate"));
				salesDltdDtl.setMrp(rs2.getDouble("mrp"));
				salesDltdDtl.setAmount(rs2.getDouble("amount"));
				salesDltdDtl.setUnitName(rs2.getString("unit_name"));
				salesDltdDtl.setDiscount(rs2.getDouble("discount"));
				salesDltdDtl.setCgstAmount(rs2.getDouble("cgst_amount"));
				salesDltdDtl.setSgstAmount(rs2.getDouble("sgst_amount"));
				salesDltdDtl.setIgstAmount(rs2.getDouble("igst_amount"));
				salesDltdDtl.setCessAmount(rs2.getDouble("cess_amount"));
				salesDltdDtl.setReturnedQty(rs2.getDouble("returned_qty"));
				salesDltdDtl.setStatus(rs2.getString("status"));
				salesDltdDtl.setAddCessAmount(rs2.getDouble("add_cess_amount"));
				salesDltdDtl.setCostPrice(rs2.getDouble("cost_price"));
				salesDltdDtl.setBranchCode(rs2.getString("branch_code"));
				salesDltdDtl.setOfferReferenceId(rs2.getString("offer_reference_id"));
				salesDltdDtl.setSchemeId(rs2.getString("scheme_id"));
				salesDltdDtl.setStandardPrice(rs2.getDouble("standard_price"));
				salesDltdDtl.setVoucherDate(rs2.getDate("voucher_date"));
				salesDltdDtl.setFbKey(rs2.getString("fb_key"));

				salesDltdDtl.setCompanyMst(companyMst);

				salesDltdDtl = salesDeletedDtlRepository.saveAndFlush(salesDltdDtl);

			}
			
			rs2.close();
			pst2.close() ;
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return "OK";

	}

//14. SalesDltDtl
	@Override
	public String transferSalesFromDerbyToMysql(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from sales_trans_hdr";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SalesTransHdr salesTransHdr = new SalesTransHdr();

				String customerId = rs2.getString("customer_id");

				String localcaustomerId = rs2.getString("local_customer_mst");

				AccountHeads customerMst = accountHeadsRepository.findById(customerId).get();

				LocalCustomerMst localCustomerMst = null;
				if (null != localcaustomerId) {
					localCustomerMst = localCustomerMstRepository.findById(localcaustomerId).get();
				}

				salesTransHdr.setAccountHeads(customerMst);
				salesTransHdr.setLocalCustomerMst(localCustomerMst);

				salesTransHdr.setId(rs2.getString("id"));
				salesTransHdr.setOldId(rs2.getString("id"));
				salesTransHdr.setCustomerId(rs2.getString("customer_id"));
				salesTransHdr.setSalesManId(rs2.getString("sales_man_id"));
				salesTransHdr.setDeliveryBoyId(rs2.getString("delivery_boy_id"));
				salesTransHdr.setSalesMode(rs2.getString("sales_mode"));
				salesTransHdr.setCreditOrCash(rs2.getString("credit_or_cash"));
				salesTransHdr.setUserId(rs2.getString("user_id"));
				salesTransHdr.setKotNumber(rs2.getString("kot_number"));
				salesTransHdr.setTakeOrderNumber(rs2.getString("take_order_number"));
				salesTransHdr.setBranchCode(rs2.getString("branch_code"));

				salesTransHdr.setFbKey(rs2.getString("fb_key"));

				salesTransHdr.setBranchSaleCustomer(rs2.getString("branch_sale_customer"));
				salesTransHdr.setEditedStatus(rs2.getString("edited_status"));
				salesTransHdr.setInvoiceNumberPrefix("invoice_number_prefix");
				salesTransHdr.setNumericVoucherNumber(rs2.getLong("numeric_voucher_number"));
				salesTransHdr.setMachineId(rs2.getString("machine_id"));
				salesTransHdr.setVoucherNumber(rs2.getString("voucher_number"));
				salesTransHdr.setCustomiseSalesMode(rs2.getString("customise_sales_mode"));
				salesTransHdr.setVoucherDate(rs2.getDate("voucher_date"));
				salesTransHdr.setInvoiceAmount(rs2.getDouble("invoice_amount"));
				salesTransHdr.setFcInvoiceAmount(rs2.getDouble("fc_invoice_amount"));
				salesTransHdr.setInvoiceDiscount(rs2.getDouble("invoice_discount"));
				salesTransHdr.setSodexoAmount(rs2.getDouble("sodexo_amount"));

				salesTransHdr.setCompanyMst(companyMst);

				salesTransHdr = salesTransHdrRepository.save(salesTransHdr);

				transferSalesDtl(companyMst, salesTransHdr);

				salesReceiptsTransfermysql(companyMst, salesTransHdr);

			}
			
			rs2.close();
			pst2.close() ;
			
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return "OK";

	}

//15. PrinterMst
	@Override
	public String transferPrinterMst(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from printer_mst";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				PrinterMst printerMst = new PrinterMst();

				printerMst.setId(rs2.getString("id"));
				printerMst.setPrinterName(rs2.getString("printer_name"));
				printerMst.setPrinterDescription(rs2.getString("printer_description"));

				printerMst.setCompanyMst(companyMst);

				printerMst = printerMstRepository.saveAndFlush(printerMst);

			}
			
			rs2.close();
			pst2.close();
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return "OK";

	}

//15. OtherBranchSalesTransHdr
	@Override
	public String transferOtherBranchSalesTransHdr(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from other_branch_sales";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				OtherBranchSalesTransHdr otherBranchSalesTransHdr = new OtherBranchSalesTransHdr();

				otherBranchSalesTransHdr.setId(rs2.getString("id"));
				otherBranchSalesTransHdr.setCustomerId(rs2.getString("customer_id"));
				otherBranchSalesTransHdr.setSalesManId(rs2.getString("sales_man_id"));
				otherBranchSalesTransHdr.setDeliveryBoyId(rs2.getString("delivery_boy_id"));
				otherBranchSalesTransHdr.setSalesMode(rs2.getString("sales_mode"));
				otherBranchSalesTransHdr.setCreditOrCash(rs2.getString("credit_or_cash"));
				otherBranchSalesTransHdr.setUserId(rs2.getString("user_id"));
				otherBranchSalesTransHdr.setSourceBranchCode(rs2.getString("source_branch_code"));
				otherBranchSalesTransHdr.setTakeOrderNumber(rs2.getString("take_order_number"));
				otherBranchSalesTransHdr.setBranchCode(rs2.getString("branch_code"));
				otherBranchSalesTransHdr.setFbKey(rs2.getString("fb_key"));
				otherBranchSalesTransHdr.setBranchSaleCustomer(rs2.getString("branch_sale_customer"));
				otherBranchSalesTransHdr.setInvoiceNumberPrefix(rs2.getString("invoice_number_prefix"));
				otherBranchSalesTransHdr.setNumericVoucherNumber(rs2.getLong("numeric_voucher_number"));
				otherBranchSalesTransHdr.setMachineId(rs2.getString("machine_id"));
				otherBranchSalesTransHdr.setVoucherNumber(rs2.getString("voucher_number"));
				otherBranchSalesTransHdr.setVoucherDate(rs2.getDate("voucher_date"));
				otherBranchSalesTransHdr.setInvoiceAmount(rs2.getDouble("invoice_amount"));
				otherBranchSalesTransHdr.setInvoiceDiscount(rs2.getDouble("invoice_discount"));
				otherBranchSalesTransHdr.setCashPay(rs2.getDouble("cash_pay"));
				otherBranchSalesTransHdr.setDiscount(rs2.getString("discount"));
				otherBranchSalesTransHdr.setAmountTendered(rs2.getDouble("amount_tendered"));
				otherBranchSalesTransHdr.setItemDiscount(rs2.getDouble("item_discount"));
				otherBranchSalesTransHdr.setPaidAmount(rs2.getDouble("paid_amount"));
				otherBranchSalesTransHdr.setChangeAmount(rs2.getDouble("change_amount"));
				otherBranchSalesTransHdr.setCardNo(rs2.getString("card_no"));
				otherBranchSalesTransHdr.setCardType(rs2.getString("card_type"));
				otherBranchSalesTransHdr.setCardamount(rs2.getDouble("cardamount"));
				otherBranchSalesTransHdr.setVoucherType(rs2.getString("voucher_type"));
				otherBranchSalesTransHdr.setServingTableName(rs2.getString("serving_table_name"));
				otherBranchSalesTransHdr.setIsBranchSales(rs2.getString("is_branch_sales"));
				otherBranchSalesTransHdr.setSodexoAmount(rs2.getDouble("sodexo_amount"));
				otherBranchSalesTransHdr.setPaytmAmount(rs2.getDouble("paytm_amount"));
				otherBranchSalesTransHdr.setCreditAmount(rs2.getDouble("credit_amount"));
				otherBranchSalesTransHdr.setSaleOrderHrdId(rs2.getString("sale_order_hrd_id"));
				otherBranchSalesTransHdr.setSalesReceiptsVoucherNumber(rs2.getString("sales_receipts_voucher_number"));
				otherBranchSalesTransHdr.setPerformaInvoicePrinted(rs2.getString("performa_invoice_printed"));
				otherBranchSalesTransHdr.setSalesTransHdrId(rs2.getString("sales_trans_hdr_id"));
				otherBranchSalesTransHdr.setSourceIP(rs2.getString("source_iP"));
				otherBranchSalesTransHdr.setSourcePort(rs2.getString("source_port"));

				otherBranchSalesTransHdr.setCompanyMst(companyMst);

				otherBranchSalesTransHdr = otherBranchSalesTransHdrRepository.saveAndFlush(otherBranchSalesTransHdr);

			}
			
			rs2.close();
			pst2.close() ;
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return "OK";

	}

	@Override
	public String transferItemFromFile(CompanyMst companyMst, String fileName, String branchcode)
			throws UnknownHostException, Exception {

		try {

			FileInputStream fstream = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			ArrayList list = new ArrayList();
			while ((strLine = br.readLine()) != null) {
				list.add(strLine);
			}

			Iterator itr;
			for (itr = list.iterator(); itr.hasNext();) {

				try {
					String str = itr.next().toString();
					String[] splitSt = str.split(",");
					String itemcode = "", itemname = "", unit = "", hsn = "", barcode = "", category = "";
					String cessrateStr = "";
					String taxrateStr = "";
					double cessrate = 0.0;
					double taxrate = 0.0;

					// for (int i = 0; i < splitSt.length; i++) {

					try {
						itemcode = splitSt[0];

						if (itemcode.trim().length() == 0) {
							continue;
						}

						itemname = splitSt[1];
						itemname = itemname.trim();

						unit = splitSt[2];
						unit = unit.trim();

						hsn = splitSt[3];
						hsn = hsn.trim();

						barcode = splitSt[4];
						barcode = barcode.trim();

						category = splitSt[5];
						category = category.trim();

						taxrateStr = splitSt[6];
						taxrateStr = taxrateStr.trim();

						cessrateStr = splitSt[7];
						cessrateStr = cessrateStr.trim();

						cessrate = Double.parseDouble(cessrateStr);
						taxrate = Double.parseDouble(taxrateStr);

					} catch (Exception e) {

					}

					if (itemname.trim().length() == 0) {
						continue;
					}

					ItemMst item = itemMstRepository.findByItemName(itemname);

					if (null == item) {
						item = new ItemMst();

						VoucherNumber itemIdNo = voucherNumberService.generateInvoice(branchcode, companyMst.getId());

						item.setId(itemIdNo.getCode());
						item.setItemName(itemname);
						item.setBarCode(barcode);
						item.setHsnCode(hsn);
						item.setRank(0);

						/*
						 * Find Unit Id from unit name.
						 */

						UnitMst unitMst = unitMstRepository.findByUnitName(unit);

						if (null == unitMst) {
							unitMst = new UnitMst();

							VoucherNumber vNo = voucherNumberService.generateInvoice("UNIT", companyMst.getId());
							unitMst.setId(vNo.getCode());
							unitMst.setUnitName(unit);
							unitMst.setUnitDescription(unit);
							unitMst.setCompanyMst(companyMst);
							unitMst.setBranchCode(branchcode);
							unitMst = unitMstRepository.save(unitMst);
						}

						item.setUnitId(unitMst.getId());

						CategoryMst categoryMst = categoryMstRepository
								.findByCompanyMstIdAndCategoryName(companyMst.getId(), category);

						if (null == categoryMst) {
							categoryMst = new CategoryMst();

							VoucherNumber vNo = voucherNumberService.generateInvoice("CATEGORY", companyMst.getId());
							categoryMst.setId(vNo.getCode());
							categoryMst.setCategoryName(category);
							categoryMst.setParentId(null);
							categoryMst.setCompanyMst(companyMst);

							categoryMst.setBranchCode(branchcode);

							categoryMst = categoryMstRepository.save(categoryMst);
						}

						item.setCategoryId(categoryMst.getId());

						item.setCess(cessrate);
						item.setTaxRate(taxrate);
						item.setIsKit(0);
						item.setReorderWaitingPeriod(0);
						item.setServiceOrGoods("GOODS");
						item.setCompanyMst(companyMst);
						item.setBestBefore(100000);
						item = itemMstRepository.save(item);

					}

				} catch (Exception e) {

					System.out.println(e.toString());
					continue;
				}
			}

			
			br.close();
		} catch (Exception e) {
			System.out.println("Error" + e.toString());
		}

		return null;
	}

//	@Override
//	public String transferCustomerMysql(CompanyMst companyMst, String branchCode) throws SQLException {
////	Connection RestserverApplication.localCn = getDbConn("SOURCEDB");
//
//		String sqlstr2 = "select * from customer_mst ";
//
//		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
//
//		ResultSet rs2 = pst2.executeQuery();
//
//		while (rs2.next()) {
//			String customer_name = rs2.getString("customer_name");
//			String customer_id = rs2.getString("customer_id");
//			String tin_no = rs2.getString("tin_no");
//			// String customer_state = rs2.getString("customer_state");
//			// String customer_state = "KERALA" ; //rs2.getString("customer_state");
//			String customer_contry = "INDIA";
//			String price_level_id = rs2.getString("price_level_id");
//			String telephone_no = rs2.getString("telephone_no");
//			String address1 = rs2.getString("address1");
//			String mobileno = rs2.getString("address2");
//			String customerState = "KERALA";// rs2.getString("customer_state");
//			 
//
//			CustomerMst customerMst = new CustomerMst();
//			customerMst.setCompanyMst(companyMst);
//			customerMst.setCreditPeriod(0);
//			customerMst.setCustomerAddress(address1);
//			customerMst.setCustomerContact(mobileno);
//			customerMst.setCustomerGst(tin_no);
//			customerMst.setCustomerName(customer_name);
//			customerMst.setCustomerMail("-");
//			customerMst.setCustomerPlace(address1);
//			customerMst.setCustomerState("KERALA");
//			customerMst.setRank(0);
//			customerMst.setCustomerCountry("INDIA");
//			customerMst.setId(customer_id);
//			customerMst.setPriceTypeId(price_level_id);
//
//			CustomerMst savedCustomer = customerMstRepository.save(customerMst);
//
//			AccountHeads parentAccountHead = accountHeadsRepository
//					.findByAccountNameAndCompanyMstId(savedCustomer.getCustomerName(), companyMst.getId());
//			if (null == parentAccountHead || null == parentAccountHead.getId()) {
//
//				AccountHeads accountHeads = new AccountHeads();
//				accountHeads.setAccountName(savedCustomer.getCustomerName());
//				accountHeads.setGroupOnly("N");
//				accountHeads.setId(savedCustomer.getId());
//				accountHeads.setCompanyMst(savedCustomer.getCompanyMst());
//				accountHeadsRepository.save(accountHeads);
//
//			}
//
//			 
//
//		}
//		return null;
//	}
//

	// ----------------------------9-3-2021

	@Override
	public String transferSchemeEligibilityMysql(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from sch_eligibility_def where company_mst='" + companyMst + "' and barnch_code='"
				+ branchCode + "'";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SchEligibilityDef schEligibilityDef = new SchEligibilityDef();
				schEligibilityDef.setBranchCode(rs2.getString("branch_code"));
				schEligibilityDef.setCompanyMst(companyMst);
				schEligibilityDef.setEligibilityName(rs2.getString("eligibility_name"));
				schEligibilityDef.setId(rs2.getString("id"));

				schEligibilityDef = schEligibilityDefRepository.save(schEligibilityDef);
			}
			
			rs2.close();
			pst2.close();
			
		} catch (Exception e) {

		}
		return "OK";

	}

	@Override
	public String transferSchemeEligibilityAttribDefMysql(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from sch_eligi_attr_list_def where company_mst='" + companyMst
				+ "' and barnch_code='" + branchCode + "'";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SchEligiAttrListDef schEligiAttrListDef = new SchEligiAttrListDef();

				schEligiAttrListDef.setAttribName(rs2.getString("attrib_name"));
				schEligiAttrListDef.setAttribType(rs2.getString("attrib_type"));
				schEligiAttrListDef.setBranchCode(branchCode);
				schEligiAttrListDef.setCompanyMst(companyMst);
				schEligiAttrListDef.setEligibilityId(rs2.getString("eligibility_id"));
				schEligiAttrListDef.setId(rs2.getString("id"));

				schEligiAttrListDefRepository.save(schEligiAttrListDef);

			}
			
			rs2.close();
			pst2.close();
			
			
		} catch (Exception e) {

		}
		return "OK";

	}

	@Override
	public String transferSchemeEligibilityAttribInstMysql(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from sch_eligibility_attrib_inst where company_mst='" + companyMst
				+ "' and barnch_code='" + branchCode + "'";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SchEligibilityAttribInst schEligibilityAttribInst = new SchEligibilityAttribInst();

				schEligibilityAttribInst.setAttributeName(rs2.getString("attribute_name"));
				schEligibilityAttribInst.setAttributeType(rs2.getString("attribute_type"));
				schEligibilityAttribInst.setAttributeValue(rs2.getString("attribute_value"));
				schEligibilityAttribInst.setBranchCode(branchCode);
				schEligibilityAttribInst.setCompanyMst(companyMst);
				schEligibilityAttribInst.setEligibilityId(rs2.getString("eligibility_id"));
				schEligibilityAttribInst.setId(rs2.getString("id"));
				schEligibilityAttribInst.setSchemeId(rs2.getString("scheme_id"));

				schEligibilityAttribInstRepository.save(schEligibilityAttribInst);

			}
			
			rs2.close();
			pst2.close() ;
			
			
		} catch (Exception e) {

		}
		return "OK";

	}

	// ------------------9-3-2021 sibi

	@Override
	public String localCustomerMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * LocalCustomerMst
		 */

		String sqlstr2 = " select  * from local_customer_mst  ";

	 
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				LocalCustomerMst localCustomerMst = new LocalCustomerMst();

				localCustomerMst.setId(rs2.getString("id"));
				localCustomerMst.setOldId(rs2.getString("id"));
				localCustomerMst.setLocalcustomerName(rs2.getString("localcustomer_name"));
				localCustomerMst.setAddress(rs2.getString("address"));
				localCustomerMst.setPhoneNo(rs2.getString("phone_no"));
				localCustomerMst.setPhoneNO2(rs2.getString("phoneno2"));
				localCustomerMst.setCustomerId(rs2.getString("customer_id"));
				localCustomerMst.setAddressLine1(rs2.getString("address_line1"));
				localCustomerMst.setAddressLine2(rs2.getString("address_line2"));
				localCustomerMst.setLandMark(rs2.getString("land_mark"));
				localCustomerMst.setCompanyMstId(rs2.getString("company_mst_id"));
				localCustomerMst.setDateOfBirth(rs2.getDate("date_of_birth"));
				localCustomerMst.setWeddingDate(rs2.getDate("wedding_date"));

				localCustomerMst = localCustomerMstRepository.save(localCustomerMst);

			}
			
			rs2.close();
			pst2.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";
	}

	@Override
	public String salesManMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * SalesManMst
		 */

		String sqlstr2 = " select  * from sales_man_mst  ";

 
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SalesManMst salesManMst = new SalesManMst();

				salesManMst.setId(rs2.getString("id"));
				salesManMst.setOldId(rs2.getString("id"));
				salesManMst.setSalesManName(rs2.getString("sales_man_name"));
				salesManMst.setBranchCode(rs2.getString("branch_code"));

				salesManMst = salesManMstRepository.save(salesManMst);

			}
			
			rs2.close();
			pst2.close() ;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		
	;

		return "OK";
	}

	private String salesReceiptsTransfermysql(CompanyMst companyMst, SalesTransHdr salesTransHdr) {
		/*
		 * SalesReceipts
		 */

		String sqlstr2 = " select  * from sales_receipts  ";

		 
		try {
			PreparedStatement 	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SalesReceipts salesReceipts = new SalesReceipts();

				salesReceipts.setSalesTransHdr(salesTransHdr);

				salesReceipts.setId(rs2.getString("id"));
				salesReceipts.setReceiptMode(rs2.getString("receipt_mode"));
				salesReceipts.setBranchCode(rs2.getString("branch_code"));
				salesReceipts.setUserId(rs2.getString("user_id"));
				salesReceipts.setAccountId(rs2.getString("account_id"));
				salesReceipts.setReceiptAmount(rs2.getDouble("receipt_amount"));
				salesReceipts.setVoucherNumber(rs2.getString("voucher_number"));
				salesReceipts.setFcAmount(rs2.getDouble("fc_amount"));
				salesReceipts.setSoStatus(rs2.getString("so_status"));

				salesReceipts.setCompanyMst(companyMst);

				salesReceipts = salesReceiptsRepository.save(salesReceipts);

			}
			
			
			rs2.close();
			pst2.close() ;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		 
		return "OK";

	}

	@Override
	public String siteMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * SiteMst
		 */

		String sqlstr2 = " select  * from site_mst  ";

		 
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SiteMst siteMst = new SiteMst();

				siteMst.setId(rs2.getString("id"));
				siteMst.setOldId(rs2.getString("id"));
				siteMst.setSiteName(rs2.getString("site_name"));
				siteMst.setAddressLine1(rs2.getString("address_line1"));
				siteMst.setAddressLine2(rs2.getString("address_line2"));
				siteMst.setMobileNumber(rs2.getString("mobile_number"));
				siteMst.setLandMark(rs2.getString("land_mark"));
				siteMst.setCustomerName(rs2.getString("customer_name"));

				siteMst.setCompanyMst(companyMst);

				siteMst = siteMstRepository.save(siteMst);

			}
			
			rs2.close();
			pst2.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

	
		
		return "OK";

	}

	@Override
	public String supplierTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * Supplier
		 */

		String sqlstr2 = " select  * from supplier  ";

		 
		try {
			PreparedStatement	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				AccountHeads accountHeads = new AccountHeads();

				accountHeads.setId(rs2.getString("id"));
				accountHeads.setOldId(rs2.getString("old_id"));
				accountHeads.setCustomerCountry(rs2.getString("country"));
				accountHeads.setAccountName(rs2.getString("supplier_name"));
				accountHeads.setPartyAddress1(rs2.getString("address"));
				accountHeads.setCustomerContact(rs2.getString("phone_no"));
				accountHeads.setPartyMail(rs2.getString("emailid"));
				accountHeads.setPartyGst(rs2.getString("supgst"));
				accountHeads.setCustomerState(rs2.getString("state"));
				accountHeads.setCreditPeriod(rs2.getInt("cerdit_period"));
				accountHeads.setCurrencyId(rs2.getString("currency_id"));

				accountHeads.setCompanyMst(companyMst);

				try {
					accountHeads = accountHeadsRepository.save(accountHeads);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error(e.getMessage());
				}
			}
			
			rs2.close();
			pst2.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";

	}

	@Override
	public String supplierPriceMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * Supplier
		 */

		String sqlstr2 = " select  * from supplier_price_mst  ";

		 
		try {
			PreparedStatement 	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SupplierPriceMst supplierPriceMst = new SupplierPriceMst();

				supplierPriceMst.setId(rs2.getString("id"));
				supplierPriceMst.setOldId(rs2.getString("old_id"));
				supplierPriceMst.setSupplierId(rs2.getString("supplier_id"));
				supplierPriceMst.setItemId(rs2.getString("item_id"));
				supplierPriceMst.setdP(rs2.getDouble("d_p"));
				supplierPriceMst.setUpdatedDate(rs2.getDate("updated_date"));

				supplierPriceMst.setCompanyMst(companyMst);

				supplierPriceMst = supplierPriceMstRepository.save(supplierPriceMst);

			}
			
			rs2.close();
			pst2.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

	
		
		return "OK";

	}

	@Override
	public String schemeInstanceTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * SchemeInstance
		 */

		String sqlstr2 = " select  * from scheme_instance  ";

		 
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SchemeInstance schemeInstance = new SchemeInstance();

				schemeInstance.setId(rs2.getString("id"));
				schemeInstance.setOldId(rs2.getString("old_id"));
				schemeInstance.setSchemeName(rs2.getString("scheme_name"));
				schemeInstance.setSelectionId(rs2.getString("selection_id"));
				schemeInstance.setEligibilityId(rs2.getString("eligibility_id"));
				schemeInstance.setOfferId(rs2.getString("offer_id"));
				schemeInstance.setIsActive(rs2.getString("is_active"));
				schemeInstance.setSchemeWholesaleRetail(rs2.getString("scheme_wholesale_retail"));
				schemeInstance.setBranchCode(rs2.getString("branch_code"));

				schemeInstance.setCompanyMst(companyMst);

				schemeInstance = schemeInstanceRepository.save(schemeInstance);

			}
			
			pst2.close();
			rs2.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		
		return "OK";

	}

	@Override
	public String productConversionConfigMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * ProductConversionConfigMst
		 */

		String sqlstr2 = " select  * from product_conversion_config_mst ";

	 
		try {
			PreparedStatement 	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				ProductConversionConfigMst productConversionConfigMst = new ProductConversionConfigMst();

				productConversionConfigMst.setId(rs2.getString("id"));
				productConversionConfigMst.setOldId(rs2.getString("old_id"));
				productConversionConfigMst.setFromItemId(rs2.getString("from_item_id"));
				productConversionConfigMst.setToItemId(rs2.getString("to_item_id"));
				productConversionConfigMst.setBranchCode(rs2.getString("branch_code"));

				productConversionConfigMst.setCompanyMst(companyMst);

				productConversionConfigMst = productConversionConfigMstRepository.save(productConversionConfigMst);

			}
			
			rs2.close();
			pst2.close() ;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		
		
		return "OK";
	}

	@Override
	public String multiUnitMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * MultiUnitMst
		 */

		String sqlstr2 = " select  * from multi_unit_mst ";

		 
		try {
			PreparedStatement	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				MultiUnitMst multiUnitMst = new MultiUnitMst();

				multiUnitMst.setId(rs2.getString("id"));
				multiUnitMst.setOldId(rs2.getString("id"));
				multiUnitMst.setItemId(rs2.getString("item_id"));
				multiUnitMst.setUnit1(rs2.getString("unit1"));
				multiUnitMst.setUnit2(rs2.getString("unit2"));
				multiUnitMst.setQty1(rs2.getDouble("qty1"));
				multiUnitMst.setQty2(rs2.getDouble("qty2"));
				multiUnitMst.setBarCode(rs2.getString("bar_code"));
				multiUnitMst.setPrice(rs2.getDouble("price"));
				multiUnitMst.setBranchCode(rs2.getString("branch_code"));

				multiUnitMst.setCompanyMst(companyMst);

				multiUnitMst = multiUnitMstRepository.save(multiUnitMst);

			}
			
			rs2.close();
			pst2.close() ;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";

	}

	@Override
	public String salesOrderReceiptsTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * SalesOrderReceipts
		 */

		/*
		 * SalesOrderTransHdr
		 * 
		 * 
		 * @Override public String itemDeviceMstTransfermysql(CompanyMst companyMst,
		 * String branchCode) {
		 * 
		 * /* ItemDeviceMst
		 * 
		 */

		String sqlstr3 = " select * from sales_order_trans_hdr ";

		try {

			PreparedStatement pst3 = RestserverApplication.localCn.prepareStatement(sqlstr3);
			ResultSet rs3 = pst3.executeQuery();

			while (rs3.next()) {

				SalesOrderTransHdr salesOrderTransHdr = new SalesOrderTransHdr();

				salesOrderTransHdr.setId(rs3.getString("id"));
				salesOrderTransHdr.setOldId(rs3.getString("id"));
				salesOrderTransHdr.setSalesManId(rs3.getString("sales_man_id"));
				salesOrderTransHdr.setInvoiceAmount(rs3.getDouble("invoice_amount"));
				salesOrderTransHdr.setInvoiceDiscount(rs3.getDouble("invoice_discount"));
				salesOrderTransHdr.setDeliveryBoyId(rs3.getString("delivery_boy_id"));
				salesOrderTransHdr.setSalesMode(rs3.getString("sales_mode"));
				salesOrderTransHdr.setCreditOrCash(rs3.getString("credit_or_cash"));
				salesOrderTransHdr.setUserId(rs3.getString("user_id"));
				salesOrderTransHdr.setOrderDue(rs3.getDate("order_due"));
				salesOrderTransHdr.setBranchCode(rs3.getString("branch_code"));

				salesOrderTransHdr.setMachineId(rs3.getString("machine_id"));
				salesOrderTransHdr.setVoucherNumber(rs3.getString("voucher_number"));
				salesOrderTransHdr.setVoucherDate(rs3.getDate("voucher_date"));

				salesOrderTransHdr.setCashPay(rs3.getDouble("cash_pay"));
				salesOrderTransHdr.setItemDiscount(rs3.getDouble("item_discount"));
				salesOrderTransHdr.setPaidAmount(rs3.getDouble("paid_amount"));
				salesOrderTransHdr.setChangeAmount(rs3.getDouble("change_amount"));
				salesOrderTransHdr.setCardamount(rs3.getDouble("card_amount"));
				salesOrderTransHdr.setStatusDate(rs3.getDate("status_date"));
				salesOrderTransHdr.setSodexoAmount(rs3.getDouble("sodexo_amount"));

				salesOrderTransHdr.setCardNo(rs3.getString("card_no"));
				salesOrderTransHdr.setConvertedToInvoice(rs3.getString("converted_to_invoice"));

				salesOrderTransHdr.setConvertedToInvoiceDate(rs3.getDate("converted_to_invoice_date"));
				salesOrderTransHdr.setOrderMessageToPrint(rs3.getString("order_message_to_print"));
				salesOrderTransHdr.setOrderAdvice(rs3.getString("order_advice"));
				salesOrderTransHdr.setTargetDepartment(rs3.getString("target_department"));
				salesOrderTransHdr.setServingTableName(rs3.getString("serving_table_name"));
				salesOrderTransHdr.setOrderStatus(rs3.getString("order_status"));
				salesOrderTransHdr.setDeliveryMode(rs3.getString("delivery_mode"));
				salesOrderTransHdr.setPaymentMode(rs3.getString("payment_mode"));
				salesOrderTransHdr.setSaleOrderReceiptVoucherNumber(rs3.getString("sale_order_receipt_voucher_number"));
				salesOrderTransHdr.setOrderDueTime(rs3.getString("order_due_time"));
				salesOrderTransHdr.setOrderTimeMode(rs3.getString("order_time_mode"));
				salesOrderTransHdr.setOrderDueDay(rs3.getString("order_due_day"));
				salesOrderTransHdr.setOrdedBy(rs3.getString("orded_by"));
				salesOrderTransHdr.setCardType(rs3.getString("card_type"));
				salesOrderTransHdr.setCancelationReason(rs3.getString("cancelation_reason"));

				salesOrderTransHdr.setCompanyMst(companyMst);

				salesOrderTransHdr = salesOrderTransHdrRepository.save(salesOrderTransHdr);

				String sqlstr2 = " select  * from sales_order_receipt where sale_order_trans_hdr_id='"
						+ salesOrderTransHdr.getId() + "'";

				PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

				ResultSet rs2 = pst2.executeQuery();

				while (rs2.next()) {

					SaleOrderReceipt salesOrderReceipts = new SaleOrderReceipt();

					salesOrderReceipts.setBranchCode(branchCode);
					salesOrderReceipts.setCompanyMst(companyMst);
					salesOrderReceipts.setId(rs2.getString("id"));
					salesOrderReceipts.setOldId(rs2.getString("id"));
					salesOrderReceipts.setReceiptAmount(rs2.getDouble("receipt_amount"));
					salesOrderReceipts.setReceiptMode(rs2.getString("receipt_mode"));
					salesOrderReceipts.setRereceiptDate(rs2.getDate("rereceipt_date"));
					salesOrderReceipts.setSalesOrderTransHdr(salesOrderTransHdr);
					salesOrderReceipts.setUserId(rs2.getString("user_id"));

					salesOrderReceipts = salesOrderReceiptsRepository.save(salesOrderReceipts);

				}
				rs2.close();
				pst2.close() ;
			}
			
			
			rs3.close();
			pst3.close() ;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";

	}

	@Override
	public String schSelectDefTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * SchSelectDef
		 */

		String sqlstr2 = " select  * from sch_select_def   ";

		try {

			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SchSelectDef schSelectDef = new SchSelectDef();

				schSelectDef.setId(rs2.getString("id"));
				schSelectDef.setOldId(rs2.getString("id"));
				schSelectDef.setSelectionName(rs2.getString("selection_name"));
				schSelectDef.setBranchCode(rs2.getString("branch_code"));

				schSelectDef.setCompanyMst(companyMst);

				schSelectDef = schSelectDefRepository.save(schSelectDef);

			}

			
			pst2.close();
			rs2.close();
			
			/*
			 * SchSelectAttrListDef
			 */

			String sqlstr3 = " select  * from sch_select_attr_list_def   ";

			PreparedStatement pst3 = RestserverApplication.localCn.prepareStatement(sqlstr3);

			ResultSet rs3 = pst3.executeQuery();

			while (rs3.next()) {

				SchSelectAttrListDef schSelectAttrListDef = new SchSelectAttrListDef();

				schSelectAttrListDef.setId(rs3.getString("id"));
				schSelectAttrListDef.setOldId(rs3.getString("id"));
				schSelectAttrListDef.setSelectionId(rs3.getString("selection_id"));
				schSelectAttrListDef.setAttribName(rs3.getString("attrib_name"));
				schSelectAttrListDef.setAttrib_type(rs3.getString("attrib_type"));
				schSelectAttrListDef.setBranchCode(rs3.getString("branch_code"));

				schSelectAttrListDef.setCompanyMst(companyMst);

				schSelectAttrListDef = schSelectAttrListDefRepository.save(schSelectAttrListDef);

			}
			
			pst3.close();
			rs3.close();
		} catch (

		SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";

	}

	@Override
	public String salesTypeMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * SalesTypeMst
		 */

		String sqlstr2 = " select  * from sales_type_mst   ";

	 
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SalesTypeMst salesTypeMst = new SalesTypeMst();

				salesTypeMst.setId(rs2.getString("id"));
				salesTypeMst.setOldId(rs2.getString("id"));
				salesTypeMst.setSalesType(rs2.getString("sales_type"));
				salesTypeMst.setSalesPrefix(rs2.getString("sales_prefix"));
				salesTypeMst.setBranchCode(rs2.getString("branch_code"));

				salesTypeMst.setCompanyMst(companyMst);

				salesTypeMst = salesTypeMstRepository.save(salesTypeMst);

			}
			
			pst2.close();
			rs2.close();
			
		} catch (Exception e) {

		}
		
		return "OK";

	}

	@Override
	public String salesPropertiesConfigMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * KotItemMst
		 */

		String sqlstr2 = " select  * from sales_properties_config_mst   ";

		 

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SalesPropertiesConfigMst salesPropertiesConfigMst = new SalesPropertiesConfigMst();

				salesPropertiesConfigMst.setId(rs2.getString("id"));
				salesPropertiesConfigMst.setOldId(rs2.getString("id"));
				salesPropertiesConfigMst.setPropertyName(rs2.getString("property_name"));
				salesPropertiesConfigMst.setPropertyType(rs2.getString("property_type"));
				salesPropertiesConfigMst.setBranchCode(rs2.getString("branch_code"));

				salesPropertiesConfigMst.setCompanyMst(companyMst);

				salesPropertiesConfigMst = salesPropertiesConfigMstRepository.save(salesPropertiesConfigMst);

			}
			
			pst2.close();
			rs2.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		
		return "OK";

	}

	@Override
	public String schSelectionAttribInstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * SchSelectionAttribInst
		 */

		String sqlstr2 = " select  * from sch_selection_attrib_inst   ";

		 
		try {
			PreparedStatement	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SchSelectionAttribInst schSelectionAttribInst = new SchSelectionAttribInst();

				schSelectionAttribInst.setId(rs2.getString("id"));
				schSelectionAttribInst.setOldId(rs2.getString("id"));
				schSelectionAttribInst.setSchemeId(rs2.getString("scheme_id"));
				schSelectionAttribInst.setAttributeName(rs2.getString("attributeName"));
				schSelectionAttribInst.setBranchCode(rs2.getString("branch_code"));
				schSelectionAttribInst.setAttributeValue(rs2.getString("attribute_Value"));
				schSelectionAttribInst.setAttributeType(rs2.getString("attribute_type"));
				schSelectionAttribInst.setSelectionId(rs2.getString("selection_id"));

				schSelectionAttribInst.setCompanyMst(companyMst);

				schSelectionAttribInst = schSelectionAttribInstRepository.save(schSelectionAttribInst);

				
				 
			}
			
			 
			rs2.close();
			pst2.close();
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		 
		return "OK";

	}

	@Override
	public String schOfferDefTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * SchOfferDef
		 */
		String sqlstr2 = " select  * from sch_offer_def";

		try {

			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SchOfferDef schOfferDef = new SchOfferDef();
				schOfferDef.setId(rs2.getString("id"));
				schOfferDef.setOldId(rs2.getString("id"));
				schOfferDef.setOfferType(rs2.getString("offer_type"));
				schOfferDef.setBranchCode(rs2.getString("branch_code"));
				schOfferDef.setCompanyMst(companyMst);

			}
			
			rs2.close();
			pst2.close() ;
			

			/*
			 * SchOfferAttrListDef
			 */

			String sqlstr3 = " select  * from sch_offer_attr_list_def   ";

			PreparedStatement pst3 = RestserverApplication.localCn.prepareStatement(sqlstr3);

			ResultSet rs3 = pst3.executeQuery();

			while (rs3.next()) {

				SchOfferAttrListDef schOfferAttrListDef = new SchOfferAttrListDef();

				schOfferAttrListDef.setId(rs3.getString("id"));
				schOfferAttrListDef.setOldId(rs3.getString("id"));
				schOfferAttrListDef.setOfferId(rs3.getString("offer_id"));
				schOfferAttrListDef.setBranchCode(rs3.getString("branch_code"));
				schOfferAttrListDef.setAttribName(rs3.getString("attrib_name"));
				schOfferAttrListDef.setAttribType(rs3.getString("attrib_type"));

				schOfferAttrListDef.setCompanyMst(companyMst);

				schOfferAttrListDef = schOfferAttrListDefRepository.save(schOfferAttrListDef);

			}

			
			rs3.close();
			pst3.close() ;
			
			/*
			 * SchOfferAttrInst
			 */

			String sqlstr4 = " select  * from sch_offer_attr_inst   ";

			PreparedStatement pst4 = RestserverApplication.localCn.prepareStatement(sqlstr4);

			ResultSet rs4 = pst4.executeQuery();

			while (rs4.next()) {

				SchOfferAttrInst schOfferAttrInst = new SchOfferAttrInst();

				schOfferAttrInst.setId(rs4.getString("id"));
				schOfferAttrInst.setOldId(rs4.getString("id"));
				schOfferAttrInst.setSchemeId(rs4.getString("scheme_id"));
				schOfferAttrInst.setOfferId(rs4.getString("offer_id"));
				schOfferAttrInst.setAttributeName(rs4.getString("attribute_name"));
				schOfferAttrInst.setAttributeValue(rs4.getString("attribute_value"));
				schOfferAttrInst.setAttributeType(rs4.getString("attribute_type"));

				schOfferAttrInst.setBranchCode(rs4.getString("branch_code"));

				schOfferAttrInst.setCompanyMst(companyMst);

				schOfferAttrInst = schOfferAttrInstRepository.save(schOfferAttrInst);
			}
			
			rs4.close();
			pst4.close() ;
			
		} catch (Exception e) {

		}

		return "OK";

	}

	@Override
	public String paramValueConfigTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * SchSelectionAttribInst
		 */

		String sqlstr2 = " select  * from param_value_config   ";

		 
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				ParamValueConfig paramValueConfig = new ParamValueConfig();

				paramValueConfig.setBranchCode(branchCode);
				paramValueConfig.setCompanyMst(companyMst);
				paramValueConfig.setId(rs2.getString("id"));
				paramValueConfig.setOldId(rs2.getString("id"));
				paramValueConfig.setParam(rs2.getString("param"));
				paramValueConfig.setValue(rs2.getString("value"));

				paramValueConfigRepository.save(paramValueConfig);

			}
			
			rs2.close();
			pst2.close() ;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

	
		return "OK";

	}

	@Override
	public String kotItemMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * KotItemMst
		 */

		String sqlstr2 = " select  * from kot_item_mst";

	 
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				KotItemMst kotItemMst = new KotItemMst();
				kotItemMst.setId(rs2.getString("id"));
				kotItemMst.setOldId(rs2.getString("id"));
				kotItemMst.setItemId(rs2.getString("item_id"));

				kotItemMst.setCompanyMst(companyMst);

				kotItemMst = kotItemMstRepository.save(kotItemMst);

			}
			
			rs2.close();
			pst2.close() ;
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";

	}

	@Override
	public String branchMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * BranchMst
		 */

		String sqlstr2 = " select  * from branch_mst";

	 
		try {
			PreparedStatement	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				try {
					BranchMst branchMst = new BranchMst();
					branchMst.setId(rs2.getString("id"));
					branchMst.setOldId(rs2.getString("id"));
					branchMst.setBranchName(rs2.getString("branch_name"));
					branchMst.setBranchCode(rs2.getString("branch_code"));
					branchMst.setBranchGst(rs2.getString("branch_gst"));
					branchMst.setBranchState(rs2.getString("branch_state"));
					branchMst.setMyBranch(rs2.getString("my_branch"));
					branchMst.setBranchAddress1(rs2.getString("branch_address1"));
					branchMst.setBranchAddress2(rs2.getString("branch_address2"));
					branchMst.setBranchTelNo(rs2.getString("branch_tel_no"));
					branchMst.setBranchEmail(rs2.getString("branch_email"));
					branchMst.setBankName(rs2.getString("bank_name"));
					branchMst.setAccountNumber(rs2.getString("account_number"));
					branchMst.setBankBranch(rs2.getString("bank_branch"));
					branchMst.setBranchPlace(rs2.getString("branch_place"));
					branchMst.setIfsc(rs2.getString("ifsc"));
					branchMst.setBranchWebsite(rs2.getString("branch_website"));

					branchMst.setCompanyMst(companyMst);

					branchMst = branchMstRepository.save(branchMst);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					logger.error(e.getMessage());
				}

			}
			
			rs2.close();
			pst2.close() ;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";

	}

	@Override
	public String orderTakerMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * OrderTakerMst
		 */

		String sqlstr2 = " select  * from order_taker_mst";

		 
		try {
			PreparedStatement 	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				OrderTakerMst orderTakerMst = new OrderTakerMst();
				orderTakerMst.setId(rs2.getString("id"));
				orderTakerMst.setOldId(rs2.getString("id"));
				orderTakerMst.setOrderTakerName(rs2.getString("order_taker_name"));
				orderTakerMst.setBranchCode(rs2.getString("branch_code"));
				orderTakerMst.setStatus(rs2.getString("status"));

				orderTakerMst.setCompanyMst(companyMst);

				orderTakerMst = orderTakerMstRepository.save(orderTakerMst);

			}
			
			rs2.close();
			pst2.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";

	}

	@Override
	public String invoiceFormatMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * InvoiceFormatMst
		 */

		String sqlstr2 = " select  * from invoice_format_mst   ";

		 
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				InvoiceFormatMst invoiceFormatMst = new InvoiceFormatMst();

				invoiceFormatMst.setId(rs2.getString("id"));
				invoiceFormatMst.setOldId(rs2.getString("id"));
				invoiceFormatMst.setPriceTypeId(rs2.getString("price_type_id"));
				invoiceFormatMst.setJasperName(rs2.getString("jasper_name"));
				invoiceFormatMst.setGst(rs2.getString("gst"));
				invoiceFormatMst.setDiscount(rs2.getString("discount"));
				invoiceFormatMst.setBatch(rs2.getString("batch"));
				invoiceFormatMst.setReportName(rs2.getString("report_name"));
				invoiceFormatMst.setStatus(rs2.getString("status"));

				invoiceFormatMst.setCompanyMst(companyMst);
				invoiceFormatMst = invoiceFormatMstRepository.save(invoiceFormatMst);

			}
			
			rs2.close();
			pst2.close() ;
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";

	}

	@Override
	public String printerMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * PrinterMst
		 */

		String sqlstr2 = " select  * from printer_mst   ";

		 
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				PrinterMst printerMst = new PrinterMst();

				printerMst.setId(rs2.getString("id"));
				printerMst.setOldId(rs2.getString("id"));
				printerMst.setPrinterName(rs2.getString("printer_name"));
				printerMst.setPrinterDescription(rs2.getString("printer_description"));

				printerMst.setCompanyMst(companyMst);
				printerMst = printerMstRepository.save(printerMst);

			}
			
			rs2.close();
			pst2.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";

	}

	@Override
	public String customerMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * CustomerMst
		 */

		String sqlstr2 = " select  * from account_heads   ";

	 
		try {
			PreparedStatement 	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				AccountHeads customerMst = new AccountHeads();

				customerMst.setId(rs2.getString("id"));
				customerMst.setOldId(rs2.getString("id"));
				customerMst.setAccountName(rs2.getString("account_name"));
				customerMst.setPartyAddress1(rs2.getString("party_address1"));
				customerMst.setCustomerContact(rs2.getString("customer_contact"));
				customerMst.setPartyMail(rs2.getString("party_mail"));
				customerMst.setPartyGst(rs2.getString("party_gst"));
				customerMst.setCustomerState(rs2.getString("customer_state"));
				customerMst.setCustomerPlace(rs2.getString("customer_place"));
				customerMst.setPriceTypeId(rs2.getString("price_type_id"));
				customerMst.setCustomerCountry(rs2.getString("customer_country"));
				customerMst.setCustomerPin(rs2.getString("customer_pin"));
				customerMst.setBankName(rs2.getString("bank_name"));
				customerMst.setBankAccountName(rs2.getString("bank_account_name"));
				customerMst.setBankIfsc(rs2.getString("bank_ifsc"));
				customerMst.setCustomerGstType(rs2.getString("customer_gst_type"));
				customerMst.setDiscountProperty(rs2.getString("discount_property"));
				customerMst.setTaxInvoiceFormat(rs2.getString("tax_invoice_format"));
				customerMst.setCustomerDiscount(rs2.getDouble("customer_discount"));
				customerMst.setCustomerRank(rs2.getInt("customer_rank"));
				customerMst.setCustomerGroup(rs2.getString("customer_group"));
				customerMst.setCurrencyId(rs2.getString("currency_id"));
				customerMst.setCreditPeriod(rs2.getInt("credit_period"));

				customerMst.setDrugLicenseNumber(rs2.getString("drug_license_number"));
				customerMst.setCustomerType(rs2.getString("customer_type"));

				customerMst.setCompanyMst(companyMst);
				customerMst = accountHeadsRepository.save(customerMst);

			}
			
			rs2.close();
			pst2.close() ;
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";

	}

	@Override
	public String receiptModeMstTransfermysql(CompanyMst companyMst, String branchCode) {

		/*
		 * ReceiptModeMst
		 */

		String sqlstr2 = " select  * from receipt_mode_mst   ";

	 
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				ReceiptModeMst receiptModeMst = new ReceiptModeMst();

				receiptModeMst.setId(rs2.getString("id"));
				receiptModeMst.setOldId(rs2.getString("id"));
				receiptModeMst.setReceiptMode(rs2.getString("receipt_mode"));
				receiptModeMst.setStatus(rs2.getString("status"));
				receiptModeMst.setCreditCardStatus(rs2.getString("credit_card_status"));

				receiptModeMst.setCompanyMst(companyMst);
				receiptModeMst = receiptModeMstRepository.save(receiptModeMst);

			}
			
			rs2.close();
			pst2.close() ;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";

	}

	@Override
	public String itemDeviceMstTransfermysql(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from item_device_mst ";

		 
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				ItemDeviceMst itemDeviceMst = new ItemDeviceMst();

				itemDeviceMst.setId(rs2.getString("id"));
				itemDeviceMst.setOldId(rs2.getString("id"));

				itemDeviceMst.setItemId(rs2.getString("item_id"));
				itemDeviceMst.setDeviceType(rs2.getString("device_type"));

				itemDeviceMst.setCompanyMst(companyMst);

				itemDeviceMst = itemDeviceMstRepository.save(itemDeviceMst);

			}
			rs2.close();
			pst2.close() ;
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "OK";
	}

	@Override
	public String vouchernumbermysql(CompanyMst companyMst, String branchCode) {

		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");
		try {
			String sqlstr2 = "select * from voucher_number ";

			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {
				String code = rs2.getString("code");
				String company = rs2.getString("company");

				VoucherNumber voucherNumber = new VoucherNumber(code, company);

				voucherNumberRepository.save(voucherNumber);

			}

			rs2.close();
			pst2.close() ;
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return "OK";

	}

	@Override
	public String sequencemysql(CompanyMst companyMst, String branchCode) {

		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");
		try {
			String sqlstr2 = "select * from sequence ";

			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				String sname = rs2.getString("name");

				Long lvalue = rs2.getLong("value");

				Sequence sequence = new Sequence(sname, lvalue);
				sequenceRepository.save(sequence);

			}
			
			rs2.close();
			pst2.close() ;
			

		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return "OK";
	}

	@Override
	public String accountClassDataTransferFromDerbyToMysql(CompanyMst companyMst, String branchcode)
			throws UnknownHostException, Exception {
		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");
		String sqlstr2 = " select  * from account_class ";

		 
		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				AccountClass accountClass = new AccountClass();

				accountClass.setBrachCode(rs2.getString("branch_code"));
				accountClass.setCompanyMst(companyMst);
				accountClass.setFinancialYear(rs2.getString("financial_year"));
				accountClass.setId(rs2.getString("id"));

				accountClass.setMachineId(rs2.getString("machine_id"));
				accountClass.setOldId(rs2.getString("id"));
				accountClass.setProcessInstanceId("0");

				accountClass.setSourceParentId(rs2.getString("source_parent_id"));
				accountClass.setSourceVoucherNumber(rs2.getString("source_voucher_number"));
				accountClass.setTaskId(rs2.getString("task_id"));
				accountClass.setTotalCredit(rs2.getBigDecimal("total_credit"));
				accountClass.setTotalDebit(rs2.getBigDecimal("total_debit"));
				accountClass.setTransDate(rs2.getDate("trans_date"));
				accountClass.setVoucherType(rs2.getString("voucher_type"));
				accountClass.setYearEndProcessing(rs2.getString("year_end_processing"));

				accountClass = accountClassRepository.save(accountClass);
				ledgerClassDataTransferFromDerbyToMysql(companyMst, branchcode, accountClass);
				debitClassDataTransferFromDerbyToMysql(companyMst, branchcode, accountClass);
				creditClassDataTransferFromDerbyToMysql(companyMst, branchcode, accountClass);
			}
			
			rs2.close();
			pst2.close() ;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		return null;
	}

	private String ledgerClassDataTransferFromDerbyToMysql(CompanyMst companyMst, String branchcode,
			AccountClass accountClass) throws UnknownHostException, Exception {
		String sqlstr2 = " select  * from ledger_class where account_class_id = '" + accountClass.getId() + "'";

	 
		try {
			PreparedStatement 	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				LedgerClass ledgerClass = new LedgerClass();

				// String accountclassid = rs2.getString("account_class_id");

				// AccountClass accountClass =
				// accountClassRepository.findById(accountclassid).get();

				ledgerClass.setAccountClass(accountClass);
				ledgerClass.setAccountId(rs2.getString("account_id"));
				ledgerClass.setBranchCode(rs2.getString("branch_code"));
				ledgerClass.setCompanyMst(companyMst);
				ledgerClass.setCreditAmount(rs2.getBigDecimal("credit_amount"));
				ledgerClass.setDebitAmount(rs2.getBigDecimal("debit_amount"));
				ledgerClass.setFinacialYear(rs2.getString("financial_year"));
				ledgerClass.setId(rs2.getString("id"));
				ledgerClass.setLedgerVoucherId(rs2.getString("voucher_id"));

				ledgerClass.setRemark(rs2.getString("remark"));
				ledgerClass.setRemarkAccountName(rs2.getString("remark_account_name"));
				ledgerClass.setSlNo(rs2.getString("sl_no"));
				ledgerClass.setSourceParentId(rs2.getString("source_parent_id"));
				ledgerClass.setSourceVoucherId(rs2.getString("source_voucher_id"));
				ledgerClass.setSourceVoucherType(rs2.getString("source_voucher_type"));
				ledgerClass.setTransDate(rs2.getDate("trans_date"));
				ledgerClass.setVoucherType(rs2.getString("voucher_type"));

				ledgerClass = ledgerClassRepository.save(ledgerClass);

			}
			
			rs2.close();
			pst2.close() ;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		return null;
	}

	private String creditClassDataTransferFromDerbyToMysql(CompanyMst companyMst, String branchcode,
			AccountClass accountClass) throws UnknownHostException, Exception {
		String sqlstr2 = " select  * from credit_class  where account_class_id = '" + accountClass.getId() + "'";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				CreditClass credit = new CreditClass();

				// String accountclassid = rs2.getString("account_class_id");

				// AccountClass accountClass =
				// accountClassRepository.findById(accountclassid).get();

				credit.setAccountClass(accountClass);

				credit.setId(rs2.getString("id"));
				credit.setAccountId(rs2.getString("account_d"));

				credit.setTransDate(rs2.getDate("trans_date"));
				credit.setRemark(rs2.getString("remark"));
				credit.setSourceVoucherNumber(rs2.getString("source_voucher_number"));
				credit.setCrAmount(rs2.getBigDecimal("dr_amount"));

				credit.setCompanyMst(companyMst);

				credit = creditClassRepository.save(credit);

			}
			
			rs2.close();
			pst2.close() ;
			
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return "OK";

	}

	private String debitClassDataTransferFromDerbyToMysql(CompanyMst companyMst, String branchcode,
			AccountClass accountClass) throws UnknownHostException, Exception {
		String sqlstr2 = " select  * from debit_class  where account_class_id = '" + accountClass.getId() + "'";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				DebitClass debit = new DebitClass();

				// String accountclassid = rs2.getString("account_class_id");

				// AccountClass accountClass =
				// accountClassRepository.findById(accountclassid).get();

				debit.setAccountClass(accountClass);

				debit.setId(rs2.getString("id"));
				debit.setAccountId(rs2.getString("account_d"));

				debit.setTransDate(rs2.getDate("trans_date"));
				debit.setRemark(rs2.getString("remark"));
				debit.setSourceVoucherNumber(rs2.getString("source_voucher_number"));
				debit.setDrAmount(rs2.getBigDecimal("dr_amount"));

				debit.setCompanyMst(companyMst);

				debit = debitClassRepository.saveAndFlush(debit);

			}
			
			rs2.close();
			pst2.close() ;
			
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return "OK";

	}

	@Override
	public String paymentTransferToMysql(CompanyMst companyMst, String branchcode)
			throws UnknownHostException, Exception {
		String sqlstr2 = " select  * from payment_hdr ";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				PaymentHdr paymentHdr = new PaymentHdr();

				paymentHdr.setBranchCode(rs2.getString("branch_code"));
				paymentHdr.setCompanyMst(companyMst);
				paymentHdr.setId(rs2.getString("id"));
				paymentHdr.setOldId(rs2.getString("id"));
				paymentHdr.setMachineId(rs2.getString("machine_id"));
				paymentHdr.setProcessInstanceId(rs2.getString("process_instance_id"));
				paymentHdr.setTaskId(rs2.getString("task_id"));
				paymentHdr.setTVoucherDate(rs2.getDate("voucher_date"));
				paymentHdr.setUserId(rs2.getString("user_id"));
				paymentHdr.setVoucherDate(rs2.getDate("voucher_date"));
				paymentHdr.setVoucherNumber(rs2.getString("voucher_number"));
				paymentHdr.setVoucherType(rs2.getString("voucher_type"));

				paymentHdr = paymentHdrRepository.saveAndFlush(paymentHdr);

				savePaymentDtl(paymentHdr);

			}
			
			rs2.close();
			pst2.close() ;
			
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return "OK";

	}

	private void savePaymentDtl(PaymentHdr paymentHdr) {

		String sqlstr2 = " select  * from payment_dtl where paymenthdr_id='" + paymentHdr.getOldId() + "'";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				PaymentDtl paymentDtl = new PaymentDtl();

				paymentDtl.setAccountId(rs2.getString("account_id"));
				paymentDtl.setAmount(rs2.getDouble("amount"));
				paymentDtl.setBankAccountNumber(rs2.getString("bank_account_number"));
				paymentDtl.setCreditAccountId(rs2.getString("credit_account_id"));
				paymentDtl.setId(rs2.getString("id"));
				paymentDtl.setOldId(rs2.getString("id"));
				paymentDtl.setInstrumentDate(rs2.getDate("instrument_date"));
				paymentDtl.setInstrumentNumber(rs2.getString("instrument_number"));
				paymentDtl.setMemberId(rs2.getString("member_id"));
				paymentDtl.setModeOfPayment(rs2.getString("mode_of_payment"));
				paymentDtl.setPaymenthdr(paymentHdr);
				paymentDtl.setProcessInstanceId(rs2.getString("process_instance_id"));
				paymentDtl.setTransDate(rs2.getDate("trans_date"));
				paymentDtl.setTaskId(rs2.getString("task_id"));
				paymentDtl.setRemark(rs2.getString("remark"));

				paymentDtlRepository.saveAndFlush(paymentDtl);

			}
			
			rs2.close();
			pst2.close() ;
			
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}

	}

	@Override
	public String itemBatchDtlFromDerbyToMysql(CompanyMst companyMst, String branchCode) {
		String sqlstr2 = " select  * from item_batch_dtl ";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();

				itemBatchDtl.setBarcode(rs2.getString("barcode"));
				itemBatchDtl.setBatch(rs2.getString("batch"));
				itemBatchDtl.setBranchCode(branchCode);
				itemBatchDtl.setCompanyMst(companyMst);
				itemBatchDtl.setExpDate(rs2.getDate("exp_date"));
				itemBatchDtl.setId(rs2.getString("id"));
				itemBatchDtl.setItemId(rs2.getString("item_id"));
				itemBatchDtl.setMrp(rs2.getDouble("mrp"));
				itemBatchDtl.setParticulars(rs2.getString("particulars"));

				itemBatchDtl.setQtyIn(rs2.getDouble("qty_in"));
				itemBatchDtl.setQtyOut(rs2.getDouble("qty_out"));
				itemBatchDtl.setSourceDtlId(rs2.getString("source_dtl_id"));
				itemBatchDtl.setSourceParentId(rs2.getString("source_parent_id"));
				itemBatchDtl.setSourceVoucherDate(rs2.getDate("source_voucher_date"));
				itemBatchDtl.setSourceVoucherNumber(rs2.getString("source_voucher_number"));
				itemBatchDtl.setStore(rs2.getString("store"));

				itemBatchDtl.setVoucherDate(rs2.getDate("voucher_date"));
				itemBatchDtl.setVoucherNumber(rs2.getString("voucher_number"));
				itemBatchDtlRepository.save(itemBatchDtl);

			}
			
			rs2.close();
			pst2.close() ;
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return null;
	}

	@Override
	public void kitDefinitionMstFromDerbyToMysql(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from kit_definition_mst";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				KitDefinitionMst kitDefinitionMst = new KitDefinitionMst();

				kitDefinitionMst.setBarcode(rs2.getString("barcode"));
				kitDefinitionMst.setBranchCode(rs2.getString("branch_code"));
				kitDefinitionMst.setCompanyMst(companyMst);
				kitDefinitionMst.setFinalSave(rs2.getString("final_save"));
				kitDefinitionMst.setId(rs2.getString("id"));
				kitDefinitionMst.setItemCode(rs2.getString("item_code"));
				kitDefinitionMst.setItemId(rs2.getString("item_id"));
				kitDefinitionMst.setKitName(rs2.getString("kit_name"));
				kitDefinitionMst.setMinimumQty(rs2.getDouble("minimum_qty"));
				kitDefinitionMst.setProductionCost(rs2.getDouble("production_cost"));
				kitDefinitionMst.setTaxRate(rs2.getDouble("tax_rate"));
				kitDefinitionMst.setUnitId(rs2.getString("unit_id"));
				kitDefinitionMst.setUnitName(rs2.getString("unit_name"));
				kitDefinitionMst.setOldId(rs2.getString("id"));

				kitDefinitionMst = kitDefinitionMstRepository.saveAndFlush(kitDefinitionMst);
				kitDefinitionDtlFromDerbyToMysql(companyMst, branchCode, kitDefinitionMst);

			}
			
			
			rs2.close();
			pst2.close() ;
			
			
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return;
	}

	@Override
	public void menuWindowMstTransfermysql(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from menu_window_mst";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				MenuWindowMst menuWindowMst = new MenuWindowMst();

				menuWindowMst.setBranchCode(rs2.getString("branch_code"));
				menuWindowMst.setCompanyMst(companyMst);
				menuWindowMst.setId(rs2.getString("id"));
				menuWindowMst.setOldId(rs2.getString("id"));
				menuWindowMst.setMenuName(rs2.getString("menu_name"));
				menuWindowMst.setWindowName(rs2.getString("window_name"));

				menuWindowMstRepository.save(menuWindowMst);

			}
			
			rs2.close();
			pst2.close() ;
			
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return;

	}

	@Override
	public void processMstTransfermysql(CompanyMst companyMst, String branchCode) {

		String sqlstr2 = " select  * from menu_window_mst";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				ProcessMst processMst = new ProcessMst();

				processMst.setBranchCode(rs2.getString("branch_code"));
				processMst.setCompanyMst(companyMst);
				processMst.setId(rs2.getString("id"));
				processMst.setProcessDescription(rs2.getString("process_description"));
				processMst.setProcessName(rs2.getString("process_name"));
				processMst.setProcessType(rs2.getString("process_type"));
				processMst.setOldId(rs2.getString("id"));

				processMstRepository.save(processMst);

			}
			
			rs2.close();
			pst2.close() ;
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return;

	}

	public void kitDefinitionDtlFromDerbyToMysql(CompanyMst companyMst, String branchCode,
			KitDefinitionMst kitDefinitionMst) {

		String sqlstr2 = " select  * from kit_defenition_dtl where kit_defenitionmst = '" + kitDefinitionMst.getId()
				+ "'";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				KitDefenitionDtl kitDefenitionDtl = new KitDefenitionDtl();

				kitDefenitionDtl.setBarcode(rs2.getString("barcode"));
				kitDefenitionDtl.setId(rs2.getString("id"));
				kitDefenitionDtl.setCompanyMst(companyMst);
				kitDefenitionDtl.setCustomisedUnitId(rs2.getString("customised_unit_id"));
				kitDefenitionDtl.setItemId(rs2.getString("item_id"));
				kitDefenitionDtl.setItemName(rs2.getString("item_name"));
				kitDefenitionDtl.setKitDefenitionmst(kitDefinitionMst);
				kitDefenitionDtl.setMltiUnitQty(rs2.getDouble("mlti_unit_qty"));
				kitDefenitionDtl.setQty(rs2.getDouble("qty"));
				kitDefenitionDtl.setUnitId(rs2.getString("unit_id"));
				kitDefenitionDtl.setUnitName(rs2.getString("unit_name"));

				kitDefenitionDtlRepository.save(kitDefenitionDtl);

			}
			
			rs2.close();
			pst2.close() ;
			
			
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return;

	}

	@Override
	public void soTransfermysql(CompanyMst companyMst, String branchcode) {

		String sqlstr2 = " select * from sales_order_trans_hdr where order_status='Orderd'";

		try {
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				SalesOrderTransHdr salesOrderTransHdr = new SalesOrderTransHdr();

				// ---------------------------------------------

				AccountHeads customerMst = saveAccountHeads(rs2.getString("id"), companyMst);

				LocalCustomerMst localCustomerMst = saveLocalCustomerMst(rs2.getString("local_customer_id"),
						companyMst);

				// ------------------------------

				salesOrderTransHdr.setBranchCode(rs2.getString("branch_code"));
				salesOrderTransHdr.setCancelationReason(rs2.getString("cancelation_reason"));
				salesOrderTransHdr.setCardamount(rs2.getDouble("cardamount"));
				salesOrderTransHdr.setCardNo(rs2.getString("card_no"));
				salesOrderTransHdr.setCardType(rs2.getString("card_type"));
				salesOrderTransHdr.setCashPay(rs2.getDouble("cash_pay"));
				salesOrderTransHdr.setChangeAmount(rs2.getDouble("change_amount"));
				salesOrderTransHdr.setCompanyMst(companyMst);
				salesOrderTransHdr.setConvertedToInvoice(rs2.getString("converted_to_invoice"));
				salesOrderTransHdr.setConvertedToInvoiceDate(rs2.getDate("converted_to_invoice_date"));
				salesOrderTransHdr.setCreditOrCash(rs2.getString("credit_or_cash"));
//				salesOrderTransHdr.setCustomerId(customerMst);
				salesOrderTransHdr.setDeliveryBoyId(rs2.getString("delivery_boy_id"));
				salesOrderTransHdr.setDeliveryMode(rs2.getString("delivery_mode"));
				salesOrderTransHdr.setId(rs2.getString("id"));
				salesOrderTransHdr.setInvoiceAmount(rs2.getDouble("invoice_amount"));
				salesOrderTransHdr.setInvoiceDiscount(rs2.getDouble("invoice_discount"));
				salesOrderTransHdr.setInvoicePrintType(rs2.getString("invoice_print_type"));
				salesOrderTransHdr.setItemDiscount(rs2.getDouble("item_discount"));
				salesOrderTransHdr.setLocalCustomerId(localCustomerMst);
				salesOrderTransHdr.setMachineId(rs2.getString("machine_id"));
				salesOrderTransHdr.setOldId(rs2.getString("id"));
				salesOrderTransHdr.setOrdedBy(rs2.getString("orded_by"));
				salesOrderTransHdr.setOrderAdvice(rs2.getString("order_advice"));
				salesOrderTransHdr.setOrderDue(rs2.getDate("order_due"));
				salesOrderTransHdr.setOrderDueDay(rs2.getString("order_due_day"));
				salesOrderTransHdr.setOrderDueTime(rs2.getString("order_due_time"));
				salesOrderTransHdr.setOrderMessageToPrint(rs2.getString("order_message_to_print"));
				salesOrderTransHdr.setOrderStatus(rs2.getString("order_status"));
				salesOrderTransHdr.setOrderTimeMode(rs2.getString("order_time_mode"));
				salesOrderTransHdr.setPaidAmount(rs2.getDouble("paid_amount"));
				salesOrderTransHdr.setPaymentMode(rs2.getString("payment_mode"));
				salesOrderTransHdr.setProcessInstanceId(rs2.getString("process_instance_id"));
				salesOrderTransHdr.setSaleOrderReceiptVoucherNumber(rs2.getString("sale_order_receipt_voucher_number"));
				salesOrderTransHdr.setSalesManId(rs2.getString("sales_man_id"));
				salesOrderTransHdr.setSalesMode(rs2.getString("sales_mode"));
				salesOrderTransHdr.setServingTableName(rs2.getString("serving_table_name"));
				salesOrderTransHdr.setSodexoAmount(rs2.getDouble("sodexo_amount"));
				salesOrderTransHdr.setStatusDate(rs2.getDate("status_date"));
				salesOrderTransHdr.setTargetDepartment(rs2.getString("target_department"));
				salesOrderTransHdr.setTaskId(rs2.getString("task_id"));
				salesOrderTransHdr.setUserId(rs2.getString("user_id"));
				salesOrderTransHdr.setVoucherDate(rs2.getDate("voucher_date"));
				salesOrderTransHdr.setVoucherNumber(rs2.getString("voucher_number"));
				
				salesOrderTransHdr = salesOrderTransHdrRepository.saveAndFlush(salesOrderTransHdr);
				
				saveSaleOrderDtlList(salesOrderTransHdr);
				
			}
			
			rs2.close();
			pst2.close() ;
			
			
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return;

	}

	private AccountHeads saveAccountHeads(String string, CompanyMst companyMst) {
		// TODO Auto-generated method stub
		return null;
	}

	private void saveSaleOrderDtlList(SalesOrderTransHdr salesOrderTransHdr) {
		// TODO Auto-generated method stub
		
	}

	private LocalCustomerMst saveLocalCustomerMst(String localcaustomerId, CompanyMst companyMst) {

		LocalCustomerMst localCustomerMst = new LocalCustomerMst();

		localCustomerMst = localCustomerMstRepository.findById(localcaustomerId).get();
		
		if(null == localCustomerMst)
		{
			String sqlstr2 = " select  * from local_customer_mst  ";

			 
			try {
				PreparedStatement	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

				ResultSet rs2 = pst2.executeQuery();

				while (rs2.next()) {
					
					localCustomerMst = localCustomerMstRepository.findByLocalcustomerName(rs2.getString("localcustomer_name"));
					
					if(null == localCustomerMst)
					{
						localCustomerMst = new LocalCustomerMst();

						localCustomerMst.setId(rs2.getString("id"));
						localCustomerMst.setOldId(rs2.getString("id"));
						localCustomerMst.setLocalcustomerName(rs2.getString("localcustomer_name"));
						localCustomerMst.setAddress(rs2.getString("address"));
						localCustomerMst.setPhoneNo(rs2.getString("phone_no"));
						localCustomerMst.setPhoneNO2(rs2.getString("phoneno2"));
						localCustomerMst.setCustomerId(rs2.getString("customer_id"));
						localCustomerMst.setAddressLine1(rs2.getString("address_line1"));
						localCustomerMst.setAddressLine2(rs2.getString("address_line2"));
						localCustomerMst.setLandMark(rs2.getString("land_mark"));
						localCustomerMst.setCompanyMstId(rs2.getString("company_mst_id"));
						localCustomerMst.setDateOfBirth(rs2.getDate("date_of_birth"));
						localCustomerMst.setWeddingDate(rs2.getDate("wedding_date"));

						localCustomerMst = localCustomerMstRepository.save(localCustomerMst);
					}
					
					rs2.close();
					pst2.close() ;
					
					return localCustomerMst;
				
				}
				
			
				rs2.close();
				
				pst2.close();
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			}
		}

		return localCustomerMst;
	}

	private AccountHeads saveCustomerMst(String customerId, CompanyMst companyMst) {

		AccountHeads customerMst = accountHeadsRepository.findById(customerId).get();

		if (null == customerMst) {
			String sqlstr2 = " select * from account_heads  where id='" + customerId + "'";

		 
			try {
				PreparedStatement	pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

				ResultSet rs2 = pst2.executeQuery();

				while (rs2.next()) {

					AccountHeads customerMstOpt = accountHeadsRepository
							.findByAccountNameAndCompanyMst( rs2.getString("customer_name"),companyMst);

					customerMst = customerMstOpt;

					if (null == customerMst) {
						customerMst = new AccountHeads();

						customerMst.setId(rs2.getString("id"));
						customerMst.setOldId(rs2.getString("id"));
						customerMst.setAccountName(rs2.getString("account_name"));
						customerMst.setPartyAddress1(rs2.getString("party_address1"));
						customerMst.setCustomerContact(rs2.getString("customer_contact"));
						customerMst.setPartyMail(rs2.getString("party_mail"));
						customerMst.setPartyGst(rs2.getString("party_gst"));
						customerMst.setCustomerState(rs2.getString("customer_state"));
						customerMst.setCustomerPlace(rs2.getString("customer_place"));
						customerMst.setPriceTypeId(rs2.getString("price_type_id"));
						customerMst.setCustomerCountry(rs2.getString("customer_country"));
						customerMst.setCustomerPin(rs2.getString("customer_pin"));
						customerMst.setBankName(rs2.getString("bank_name"));
						customerMst.setBankAccountName(rs2.getString("bank_account_name"));
						customerMst.setBankIfsc(rs2.getString("bank_ifsc"));
						customerMst.setCustomerGstType(rs2.getString("customer_gst_type"));
						customerMst.setDiscountProperty(rs2.getString("discount_property"));
						customerMst.setTaxInvoiceFormat(rs2.getString("tax_invoice_format"));
						customerMst.setCustomerDiscount(rs2.getDouble("customer_discount"));
						customerMst.setCustomerRank(rs2.getInt("customer_rank"));
						customerMst.setCustomerGroup(rs2.getString("customer_group"));
						customerMst.setCurrencyId(rs2.getString("currency_id"));
						customerMst.setCreditPeriod(rs2.getInt("credit_period"));

						customerMst.setDrugLicenseNumber(rs2.getString("drug_license_number"));
						customerMst.setCustomerType(rs2.getString("customer_type"));

						customerMst.setCompanyMst(companyMst);
						customerMst = accountHeadsRepository.save(customerMst);

					}
					
					
					rs2.close();
					
					pst2.close();
					
					return customerMst;

				}
				
				rs2.close();
				
				pst2.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			}

//			return "customerMst";

		}
		return customerMst;

	}
	
	
////===============================stockMigration=========////////////	
	
	
	
//	@Override
//	public String itemBatchDtlStockMigration(CompanyMst companyMst, String branchCode) {
//		String sqlstr2 = " select  * from item_batch_dtl ";
//
//		try {
//			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
//
//			ResultSet rs2 = pst2.executeQuery();
//
//			while (rs2.next()) {
//
//				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
//
//				itemBatchDtl.setBarcode(rs2.getString("barcode"));
//				itemBatchDtl.setBatch(rs2.getString("batch"));
//				itemBatchDtl.setBranchCode(branchCode);
//				itemBatchDtl.setCompanyMst(companyMst);
//				itemBatchDtl.setExpDate(rs2.getDate("exp_date"));
//				itemBatchDtl.setId(rs2.getString("id"));
//				itemBatchDtl.setItemId(rs2.getString("item_id"));
//				itemBatchDtl.setMrp(rs2.getDouble("mrp"));
//				itemBatchDtl.setParticulars(rs2.getString("OPENNING STOCK"));
//
//				itemBatchDtl.setQtyIn(rs2.getDouble("qty_in"));
//				itemBatchDtl.setQtyOut(rs2.getDouble("qty_out"));
//				itemBatchDtl.setSourceDtlId(rs2.getString("source_dtl_id"));
//				itemBatchDtl.setSourceParentId(rs2.getString("source_parent_id"));
//				itemBatchDtl.setSourceVoucherDate(rs2.getDate("source_voucher_date"));
//				itemBatchDtl.setSourceVoucherNumber(rs2.getString("source_voucher_number"));
//				itemBatchDtl.setStore(rs2.getString("store"));
//
//				itemBatchDtl.setVoucherDate(rs2.getDate("voucher_date"));
//				itemBatchDtl.setVoucherNumber(rs2.getString("voucher_number"));
//				itemBatchDtlRepository.save(itemBatchDtl);
//
//			}
//		} catch (Exception e) {
//			System.out.println(e.toString());
//		}
//
//		return null;
//	}

	//=============================Stock Migratiom to OPENING STOCKS============================///	
	
	    @Override
		public String openingStockDtlStockMigration(CompanyMst companyMst, String branchCode) {
			String sqlstr2 = " select  * from item_batch_dtl ";

			try {
				PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

				ResultSet rs2 = pst2.executeQuery();

				while (rs2.next()) {

					OpeningStockDtl openingStockDtl = new OpeningStockDtl();

					openingStockDtl.setBarcode(rs2.getString("barcode"));
					openingStockDtl.setBatch(rs2.getString("batch"));
					openingStockDtl.setBranchCode(branchCode);
					openingStockDtl.setCompanyMst(companyMst);
					openingStockDtl.setExpDate(rs2.getDate("exp_date"));
					openingStockDtl.setId(rs2.getString("id"));
					openingStockDtl.setItemId(rs2.getString("item_id"));
					openingStockDtl.setMrp(rs2.getDouble("mrp"));
					openingStockDtl.setParticulars(rs2.getString("OPENNING STOCK"));

					openingStockDtl.setQtyIn(rs2.getDouble("qty_in"));
					openingStockDtl.setQtyOut(rs2.getDouble("qty_out"));
					
					openingStockDtl.setStore(rs2.getString("store"));

					//openingStockDtl.setVoucherDate(rs2.getDate("voucher_date"));
					//openingStockDtl.setVoucherNumber(rs2.getString("voucher_number"));
					openingStockDtlRepository.save(openingStockDtl);

				}
				
				rs2.close();
				pst2.close();
			} catch (Exception e) {
				System.out.println(e.toString());
			}

			return "Completed successfully";
		}
	/*
	 * @Override public String menuTransfermysql(CompanyMst companyMst, String
	 * branchCode) {
	 * 
	 * 
	 * 
	 * String sqlstr2 = " select  * from menu_mst";
	 * 
	 * try { PreparedStatement pst2 =
	 * RestserverApplication.localCn.prepareStatement(sqlstr2);
	 * 
	 * ResultSet rs2 = pst2.executeQuery();
	 * 
	 * while (rs2.next()) {
	 * 
	 * 
	 * MenuMst menuMst = new MenuMst();
	 * 
	 * menuMst.setBranchCode(branchCode); menuMst.setCompanyMst(companyMst);
	 * menuMst.setId(rs2.getString("id"));
	 * menuMst.setMenuId(rs2.getString("menu_id"));
	 * menuMst.setOldId(rs2.getString("old_id"));
	 * menuMst.setParentId(rs2.getString("parent_id"));
	 * menuMst.setProcessInstanceId(rs2.getString("process_instance_id"));
	 * menuMst.setTaskId(rs2.getString("task_id"));
	 * 
	 * 
	 * 
	 * 
	 * 
	 * menuMstRepository.save(menuMst);
	 * 
	 * 
	 * } } catch (Exception e) { System.out.println(e.toString()); }
	 * 
	 * return "OK";
	 * 
	 * }
	 */

}
