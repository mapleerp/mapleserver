package com.maple.restserver.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyConversionMst;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CurrencyConversionMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;

@Service
@Transactional
public class CurrencyExchangeServiceImpl implements CurrencyExchnageService {

	@Autowired
	CurrencyConversionMstRepository currencyConversionMstRepository;

	@Autowired
	CurrencyMstRepository currencyMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Override
	public Double getCurrencyExchange(String fromCurrencyName, String toCurrencyName, Double inAmount,
			String companyMstId) {
		
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companyMstId);
		CompanyMst companyMst = CompanyMstOpt.get();
		
		if(null == companyMst)
		{
			return null;
		}
		Double convertedRate = 0.0;

		// Fetch From currency in conversion table  match FromCurrency
		
		CurrencyMst fromCurrecny = currencyMstRepository.findByCurrencyName(fromCurrencyName);
		CurrencyMst toCurrecny = currencyMstRepository.findByCurrencyName(toCurrencyName);
		CurrencyConversionMst currencyConversionMst = currencyConversionMstRepository
				.findByToCurrencyIdAndFromCurrencyId(toCurrecny.getId(),fromCurrecny.getId());
		
		if(null!=currencyConversionMst) {
			convertedRate = inAmount  * currencyConversionMst.getConversionRate();
		
			return convertedRate;
		}
			
		// Try if From and To are reversed in the request
		
		CurrencyConversionMst currencyConversionMstReverse = currencyConversionMstRepository
		.findByToCurrencyIdAndFromCurrencyId(fromCurrecny.getId(),toCurrecny.getId());
			
		if(null!=currencyConversionMstReverse) {
			convertedRate = inAmount  / currencyConversionMstReverse.getConversionRate();
		
			return convertedRate;
		}
		
	 return null;
	}
}
