package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.MachineResourceMst;
import com.maple.restserver.entity.MixCategoryMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.entity.MixCatItemLinkMst;
import com.maple.restserver.report.entity.SaleOrderItemQty;
import com.maple.restserver.report.entity.SaleOrderProductDtls;
@Service
public interface MachineResourceService {

	List<MachineResourceMst>machineDetails();
	
	List<MixCategoryMst> retriveResourceCategory();
	
	
	List<MixCatItemLinkMst>retrieveResourceCatItemLinkMst();
	
//	ProductionMst  productMachineRatio(String companymstid,java.util.Date fromDate,Double machineCapacity,String branchcode);

//	List<SaleOrderItemQty>fetchSaleOrderItemQty(String branchCode, String companymstid,java.util.Date fromDate );

//    String productionPrePlaningToProduction();
    
//    List<ProductionDtl>fetchProductionPrePlaningToProduction();

    public List<SaleOrderProductDtls> fetchSaleOrderProductQty(String branchCode, String companymstid, 
    		java.util.Date fromDate,java.util.Date tDate);

	void preplanningBatchInitializeStage2(String branchcode, String companymstid, Date fromDate,Date tdate ) throws ResourceNotFoundException;

	void deleteProductionPrePlanning(Date fromDate,Date tdate);
    
}
