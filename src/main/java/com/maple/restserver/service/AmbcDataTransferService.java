package com.maple.restserver.service;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;


@Service

public interface AmbcDataTransferService {


public String consumptionDtlTransferamdc(CompanyMst companyMst, String branchCode) ; 
public String countryMstTransferamdc(CompanyMst companyMst, String branchCode);
public String currencyConversionTransferambc(CompanyMst companyMst, String branchCode);
public String patientDtlTransferambc(CompanyMst companyMst, String branchCode);
public String physicalStockDeltaMstTransferambc(CompanyMst companyMst, String branchCode);
public String physicalStockMstTransferambc(CompanyMst companyMst, String branchCode);



public String customerMstTransferambc(CompanyMst companyMst, String branchCode);
public String supplierMstTransferamdc(CompanyMst companyMst, String branchCode);
public String   unitMstTransferamdc(CompanyMst companyMst, String branchCode) ;
public String  userMstTransferamdc(CompanyMst companyMst, String branchCode);

}
