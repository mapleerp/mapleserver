package com.maple.restserver.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.entity.LastBestDate;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.LastBestDateRepository;

@Service
public class LastBestDateServicePurchase extends LastBestDateService {

	@Autowired
	LastBestDateRepository lastBestDateRepository;
	@Autowired
	DayEndClosureRepository dayEndClosureRepository;

	@Override
	public LastBestDate findLastBestDate() {

		System.out.println(
				"Entered into LastBestDateServicePurchase.findLastBestDate() method****************************");

		LastBestDate lastbestdate = lastBestDateRepository.findByActivityType("PURCHASE");

		if (null == lastbestdate.getLastSuccessDate()) {
			return null;
		}

		List<DayEndClosureHdr> dayenddate = dayEndClosureRepository.getMaxOfDayEnd(super.getCompanymstid(),
				super.getBranchcode());

		if (dayenddate.size() > 0) {
			Date purchasedayend = dayenddate.get(0).getProcessDate();
			lastbestdate.setDayEndDate(purchasedayend);

		} else {

			lastbestdate.setDayEndDate(lastbestdate.getLastSuccessDate());
		}

		Date date = new Date(lastbestdate.getLastSuccessDate().getTime() + (1000 * 60 * 60 * 24));
		lastbestdate.setLastSuccessDate(date);

		int dateComparison = lastbestdate.getDayEndDate().compareTo(lastbestdate.getLastSuccessDate());

		if (dateComparison < 0) {
			lastbestdate = null;
		}
		System.out.println("The result of LastBestDateServicePurchase.findLastBestDate() method ====> " + lastBestDate);
		return lastbestdate;

	}

}
