package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.report.entity.BatchExpiryItemReport;
import com.maple.restserver.report.entity.BatchItemWiseSalesReport;

public interface BatchExpiryItemService {

	
	public List<BatchExpiryItemReport> itemWiseSortExpiry(String companymstid,String branchcode,Date fDate,List<String>itemList);

}
