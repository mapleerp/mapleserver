package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.JournalHdr;

@Service
public interface JournalReportService {

	List<JournalHdr>getJournalHdr(CompanyMst companyMst,Date fromDate,Date toDate,String branchcode);

	
	List<JournalDtl>getJournalDtl(String journalhdrid);
	
	List<JournalDtl>getJournalJasperReportr(CompanyMst companyMst,Date fromDate,Date toDate,String branchcode);
}
