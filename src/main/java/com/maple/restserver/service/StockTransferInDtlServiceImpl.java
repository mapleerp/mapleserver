package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.StockTransferInDtl;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.StockTransferInReport;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.repository.StockTransferInDtlRepository;
import com.maple.restserver.repository.StockTransferOutDtlRepository;

@Service
@Transactional
@Component
public class StockTransferInDtlServiceImpl implements StockTransferInDtlService {

	@Autowired
	StockTransferInDtlRepository stockTransferInDtlRepository;

	@Override
	public List<StockTransferInReport> getTransferInDetailReport(CompanyMst companyMst, String branchcode, Date fdate,
			Date tdate) {
		List<StockTransferInReport> stockTransferInDetailList = new ArrayList<StockTransferInReport>();

		List<Object> objList = stockTransferInDtlRepository.getTransferInDetailReport(companyMst, branchcode, fdate,
				tdate);

		for (int i = 0; i < objList.size(); i++) {
			Object[] obj = (Object[]) objList.get(i);

			StockTransferInReport stockTransferInReport = new StockTransferInReport();

			stockTransferInReport.setVoucherNumber((String) obj[0]);
			stockTransferInReport.setInvoiceDate((Date) obj[1]);
			stockTransferInReport.setFromBranch((String) obj[2]);
			stockTransferInReport.setItemName((String) obj[3]);
			stockTransferInReport.setCategory((String) obj[4]);
			stockTransferInReport.setItemCode((String) obj[5]);
			stockTransferInReport.setBatch((String) obj[6]);
			stockTransferInReport.setExpiryDate((Date) obj[7]);
			stockTransferInReport.setQty((Double) obj[8]);
			stockTransferInReport.setUnitName((String) obj[9]);
			stockTransferInReport.setRate((Double) obj[10]);
			stockTransferInReport.setAmount((Double) obj[11]);

			stockTransferInDetailList.add(stockTransferInReport);
		}

		return stockTransferInDetailList;
	}

	@Override
	public List<StockTransferInReport> getTransferInDetailReportByFromBranch(CompanyMst companyMst, String branchcode,
			String frombranchcode, Date fdate, Date tdate) {

		List<StockTransferInReport> stockTransferInDetailList = new ArrayList<StockTransferInReport>();

		List<Object> objList = stockTransferInDtlRepository.getTransferInDetailReportByFromBranch(companyMst,
				branchcode, frombranchcode, fdate, tdate);

		for (int i = 0; i < objList.size(); i++) {
			Object[] obj = (Object[]) objList.get(i);

			StockTransferInReport stockTransferInReport = new StockTransferInReport();

			stockTransferInReport.setVoucherNumber((String) obj[0]);
			stockTransferInReport.setInvoiceDate((Date) obj[1]);
			stockTransferInReport.setFromBranch((String) obj[2]);
			stockTransferInReport.setItemName((String) obj[3]);
			stockTransferInReport.setCategory((String) obj[4]);
			stockTransferInReport.setItemCode((String) obj[5]);
			stockTransferInReport.setBatch((String) obj[6]);
			stockTransferInReport.setExpiryDate((Date) obj[7]);
			stockTransferInReport.setQty((Double) obj[8]);
			stockTransferInReport.setUnitName((String) obj[9]);
			stockTransferInReport.setRate((Double) obj[10]);
			stockTransferInReport.setAmount((Double) obj[11]);

			stockTransferInDetailList.add(stockTransferInReport);
		}

		return stockTransferInDetailList;
	}

	@Override
	public List<StockTransferInReport> getTransferInDetailReportByFromBranch(CompanyMst companyMst, String branchcode,
			String frombranch, String[] array, Date fdate, Date tdate) {
		List<StockTransferInReport> stockTransferInDetailList = new ArrayList<StockTransferInReport>();

		String categoryIds = "";
		for (int i = 0; i < array.length; i++) {
			categoryIds = array[i];

			List<Object> objList = stockTransferInDtlRepository.getTransferInDetailReportByFromBranchAndCategory(
					companyMst, branchcode, frombranch, categoryIds, fdate, tdate);

			for (int j = 0; j < objList.size(); j++) {
				Object[] obj = (Object[]) objList.get(j);

				StockTransferInReport stockTransferInReport = new StockTransferInReport();

				stockTransferInReport.setVoucherNumber((String) obj[0]);
				stockTransferInReport.setInvoiceDate((Date) obj[1]);
				stockTransferInReport.setFromBranch((String) obj[2]);
				stockTransferInReport.setItemName((String) obj[3]);
				stockTransferInReport.setCategory((String) obj[4]);
				stockTransferInReport.setItemCode((String) obj[5]);
				stockTransferInReport.setBatch((String) obj[6]);
				stockTransferInReport.setExpiryDate((Date) obj[7]);
				stockTransferInReport.setQty((Double) obj[8]);
				stockTransferInReport.setUnitName((String) obj[9]);
				stockTransferInReport.setRate((Double) obj[10]);
				stockTransferInReport.setAmount((Double) obj[11]);

				stockTransferInDetailList.add(stockTransferInReport);
			}

		}

		return stockTransferInDetailList;
	}

	@Override
	public List<StockTransferInReport> getTransferInDetailReportByCategory(CompanyMst companyMst, String branchcode,
			String[] array, Date fdate, Date tdate) {
		List<StockTransferInReport> stockTransferInDetailList = new ArrayList<StockTransferInReport>();

		String categoryIds = "";
		for (int i = 0; i < array.length; i++) {
			categoryIds = array[i];

			List<Object> objList = stockTransferInDtlRepository.getTransferInDetailReportByCategory(
					companyMst, branchcode, categoryIds, fdate, tdate);

			for (int j = 0; j < objList.size(); j++) {
				Object[] obj = (Object[]) objList.get(j);

				StockTransferInReport stockTransferInReport = new StockTransferInReport();

				stockTransferInReport.setVoucherNumber((String) obj[0]);
				stockTransferInReport.setInvoiceDate((Date) obj[1]);
				stockTransferInReport.setFromBranch((String) obj[2]);
				stockTransferInReport.setItemName((String) obj[3]);
				stockTransferInReport.setCategory((String) obj[4]);
				stockTransferInReport.setItemCode((String) obj[5]);
				stockTransferInReport.setBatch((String) obj[6]);
				stockTransferInReport.setExpiryDate((Date) obj[7]);
				stockTransferInReport.setQty((Double) obj[8]);
				stockTransferInReport.setUnitName((String) obj[9]);
				stockTransferInReport.setRate((Double) obj[10]);
				stockTransferInReport.setAmount((Double) obj[11]);

				stockTransferInDetailList.add(stockTransferInReport);
			}

		}

		return stockTransferInDetailList;
	}

}
