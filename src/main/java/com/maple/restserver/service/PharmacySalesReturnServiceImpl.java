package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyMst;

import com.maple.restserver.report.entity.PharmacySalesReturnReport;
import com.maple.restserver.report.entity.ShortExpiryReport;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;

import com.maple.restserver.repository.SalesReturnHdrRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
public class PharmacySalesReturnServiceImpl implements PharmacySalesReturnService {

	@Autowired
	CategoryMstRepository categoryMstRepository;
	@Autowired
	SalesReturnHdrRepository salesReturnHdrRepository;

	@Autowired
	CurrencyMstRepository currencyMstRepository;


	@Override
	public List<PharmacySalesReturnReport> getPharmacySalesReturn(CompanyMst companyMst, java.sql.Date fdate,
			java.sql.Date tdate, String branchCode, String[] catList) {

		List<PharmacySalesReturnReport> pharmacySalesReturnReportList = new ArrayList<PharmacySalesReturnReport>();

		for (String categoryName : catList) {
			CategoryMst categoryMst = categoryMstRepository.findByCategoryNameAndCompanyMst(categoryName, companyMst);

			List<Object> obj = salesReturnHdrRepository.getPharmacySalesReturn(fdate, tdate, branchCode,
					categoryMst.getId());
			
			for (int i = 0; i < obj.size(); i++) {
				Object[] objAray = (Object[]) obj.get(i);
				PharmacySalesReturnReport pharmacySalesReturnReport = new PharmacySalesReturnReport();
//				String customerId = (String) objAray[0];
//				Optional<CustomerMst> customerMstOpt = customerMstRepository.findById(customerId);
//				CustomerMst customerMst = customerMstOpt.get();
//
//				pharmacySalesReturnReport.setCustomerName(customerMst.getCustomerName());
				pharmacySalesReturnReport.setInvoiceDate(SystemSetting.UtilDateToString((Date) objAray[0]));
//				if (null != customerMst.getCurrencyId()) {
//					Optional<CurrencyMst> currencyMstOpt = currencyMstRepository.findById(customerMst.getCurrencyId());
//					pharmacySalesReturnReport.setCurrency(currencyMstOpt.get().getCurrencyName());
//				}
				pharmacySalesReturnReport.setSalesVoucherNumber((String) objAray[1]);
				pharmacySalesReturnReport.setVoucherNumber((String) objAray[2]);
				pharmacySalesReturnReport.setCustomerName((String) objAray[3]);
				pharmacySalesReturnReport.setItemName((String) objAray[4]);
				pharmacySalesReturnReport.setItemCode((String) objAray[5]);
				pharmacySalesReturnReport.setGroupName((String) objAray[6]);
				pharmacySalesReturnReport.setBatchCode((String) objAray[7]);
				pharmacySalesReturnReport.setExpiryDate((String) objAray[8].toString());
				pharmacySalesReturnReport.setQuantity((Double) objAray[9]);
				pharmacySalesReturnReport.setRate((Double) objAray[10]);
				pharmacySalesReturnReport.setValue((Double) objAray[11]);
				pharmacySalesReturnReport.setUserName((String) objAray[12]);
				pharmacySalesReturnReport.setSalesMan((String) objAray[13]);
				pharmacySalesReturnReport.setAmountBeforeGst((Double) objAray[10]);
				pharmacySalesReturnReport.setGst((Double) objAray[14]);
				pharmacySalesReturnReport.setAmount((Double) objAray[15]);
				
				

				pharmacySalesReturnReportList.add(pharmacySalesReturnReport);
			}
		}
		return pharmacySalesReturnReportList;
	}

	
	
	
	@Override
	public List<PharmacySalesReturnReport> getPharmacySalesReturnWithoutCategory(CompanyMst companyMst, java.sql.Date fdate,
			java.sql.Date tdate, String branchCode) {

		List<PharmacySalesReturnReport> pharmacySalesReturnReportList = new ArrayList<PharmacySalesReturnReport>();


			List<Object> obj = salesReturnHdrRepository.getPharmacySalesReturnWithoutCategory(fdate, tdate, branchCode);
			
			for (int i = 0; i < obj.size(); i++) {
				Object[] objAray = (Object[]) obj.get(i);
				PharmacySalesReturnReport pharmacySalesReturnReport = new PharmacySalesReturnReport();
//				String customerId = (String) objAray[0];
//				Optional<CustomerMst> customerMstOpt = customerMstRepository.findById(customerId);
//				CustomerMst customerMst = customerMstOpt.get();
//
//				pharmacySalesReturnReport.setCustomerName(customerMst.getCustomerName());
				pharmacySalesReturnReport.setInvoiceDate(SystemSetting.UtilDateToString((Date) objAray[0]));
//				if (null != customerMst.getCurrencyId()) {
//					Optional<CurrencyMst> currencyMstOpt = currencyMstRepository.findById(customerMst.getCurrencyId());
//					pharmacySalesReturnReport.setCurrency(currencyMstOpt.get().getCurrencyName());
//				}
				pharmacySalesReturnReport.setSalesVoucherNumber((String) objAray[1]);
				pharmacySalesReturnReport.setVoucherNumber((String) objAray[2]);
				pharmacySalesReturnReport.setCustomerName((String) objAray[3]);
				pharmacySalesReturnReport.setItemName((String) objAray[4]);
				pharmacySalesReturnReport.setItemCode((String) objAray[5]);
				pharmacySalesReturnReport.setGroupName((String) objAray[6]);
				pharmacySalesReturnReport.setBatchCode((String) objAray[7]);
				pharmacySalesReturnReport.setExpiryDate((String) objAray[8].toString());
				pharmacySalesReturnReport.setQuantity((Double) objAray[9]);
				pharmacySalesReturnReport.setRate((Double) objAray[10]);
				pharmacySalesReturnReport.setValue((Double) objAray[11]);
				pharmacySalesReturnReport.setUserName((String) objAray[12]);
				pharmacySalesReturnReport.setSalesMan((String) objAray[13]);
				pharmacySalesReturnReport.setAmountBeforeGst((Double) objAray[10]);
				pharmacySalesReturnReport.setGst((Double) objAray[14]);
				pharmacySalesReturnReport.setAmount((Double) objAray[15]);
				
				

				pharmacySalesReturnReportList.add(pharmacySalesReturnReport);
			}
		return pharmacySalesReturnReportList;
	}

	
}
