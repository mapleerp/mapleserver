package com.maple.restserver.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

@Service
@Transactional
public interface LocalCustomerMstService {

	String initializeLocalCustomer(String companymstid);
	

}
