package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.BranchSaleReport;
import com.maple.restserver.report.entity.ItemSaleReport;
import com.maple.restserver.report.entity.SaleOrderinoiceReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.TaxSummaryMst;

@Service
@Transactional
public interface SalesInvoiceService {

	public List<SalesInvoiceReport> getSalesInvoice(String companymstid, String voucherNumber, Date voucherDate);

	public List<SalesInvoiceReport> getAllSalesInvoice(String companymstid);

	List<TaxSummaryMst> getSalesInvoiceTax(String companymstid, String voucherNumber, Date date);

	List<SalesInvoiceReport> getTaxinvoiceCustomer(String companymstid, String voucherNumber, Date date);

	List<TaxSummaryMst> getSumOfTaxAmounts(String companymstid, String voucherNumber, Date date);

	/*
	 * 
	 * select bm.branch_name, bm.branch_address1, bm.branch_address2, bm.place,
	 * bm.teln_no, bm.gstno bm.state_name, bm.email, bm.bank_name,
	 * bm.account_number, bm.bank_branch, bm.ifsc,
	 * sh.voucher_date,sh.voucher_number, cm.customer_name,cm.custopemr_place,
	 * cm.customer_state, cm.gstno, im.item_name,im.tax_rate, im.hsn, d.rate, d.qty,
	 * u.unit_name,d.discount, ((d.rate*d.qty*d.tax_rate/100) + (d.rate*d.qty*)) as
	 * amount , ( d.rate*d.qty*d.tax_rate/200 ) as sgst_amount , (
	 * d.rate*d.qty*d.tax_rate/200 ) as cgst_amount from branch_mst bm,
	 * sales_trans_hdr sh, sales_dtl d, customer_mst cm, unit_mst u where
	 * sh.branch_code = bm.branchcode and sh.customer_id = cm.id and sh.id =
	 * d.sales_trans_hdr_id and d.item_id = im.id and d.unit_id = u.id and
	 * sh.voucher_nuber = :vnumber and sh.voucher_date = :vdate
	 */
	public SaleOrderinoiceReport getSaleOrderInvoiceReport(String voucherNumber, Date date);

	List<ItemSaleReport> getItemSaleReport(String itemid, Date date);

	public Double getCessAmount(String vouchernumber, Date date);

	public Double getNonTaxableAmount(String vouchernumber, Date date);

	public List<BranchSaleReport> getBranchSaleHdrReportSummary(String companymstid, String branchcode, Date sdate,
			Date edate);

	public List<BranchSaleReport> getTopSellingProducts(String companymstid, Date udate);
}
