package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.report.entity.ReorderReport;



@Service

@Component
public interface IntentService {
	List<ReorderReport> IntentDtlReport(String companymstid, String branchcode, Date fDate);
	List<IntentHdr> getAllIntent();
	Map<String, Object> getGeneratedIntent(String  companymstid,String branchCode);
	public  List<IntentHdr> getIntentHdrs(String pdfFileName);

}
