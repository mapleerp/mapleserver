package com.maple.restserver.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.IntentItemBranchMst;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PurchaseSchemeMst;
import com.maple.restserver.entity.ReorderMst;
import com.maple.restserver.report.entity.AccountHeadsReport;
import com.maple.restserver.report.entity.ReorderReport;
import com.maple.restserver.report.entity.StockReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.IntentDtlRepository;
import com.maple.restserver.repository.IntentHdrRepository;
import com.maple.restserver.repository.IntentItemBranchRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.PurchaseSchemeMstRepository;
import com.maple.restserver.repository.ReorderMstRepository;
import com.maple.restserver.resource.IntentHdrResource;
 
import com.maple.restserver.utils.SystemSetting;

 
@Service
@Transactional
@Component
public class IntentServiceImpl implements IntentService {
	@Autowired
	StockReportService stockReportService;
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	@Autowired
	PurchaseSchemeMstRepository purchaseSchemeMstRepo;
	
	
	@Autowired
	IntentItemBranchRepository intentItemBranchRepo;
	
	@Autowired
	BranchMstRepository branchMstRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	IntentHdrRepository intentHdrRepository;
	
	@Autowired
	StockServiceImpl stockServiceImpl;
	
	@Autowired
	IntentDtlRepository intentDtlRepository;
	
	@Autowired
	ItemMstRepository itemMstRepository;
	
 
	@Override
	public List<IntentHdr> getAllIntent() {
		
		return null;
	}

	@Override
	public Map<String, Object> getGeneratedIntent(String companymstid,String branchCode) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		ArrayList<IntentHdr> intentHdrList = new ArrayList<IntentHdr>();
		ArrayList<IntentDtl> intentDtlList = new ArrayList<IntentDtl>();
		 IntentHdr intentHdr = new IntentHdr();
			BranchMst getBranch = branchMstRepo.getMyBranch();
		List<ReorderMst> itemBatchMst = stockServiceImpl.getAllStockLessThanMinQty(companymstid,branchCode);
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		 if(null != itemBatchMst)
		 {
			 
		
			intentHdr.setCompanyMst(companyMst);
			intentHdr.setFromBranch(getBranch.getBranchCode());
			 intentHdr.setVoucherDate(SystemSetting.getSystemDate());
			 intentHdr.setBranchCode(getBranch.getBranchCode());
			 intentHdr.setToBranch(getBranch.getBranchCode());
			 intentHdr = intentHdrRepository.save(intentHdr);
			 intentHdrList.add(intentHdr);
		 }
		
		for(ReorderMst item : itemBatchMst)
		{
			double sysQty=0.0;
			List<StockReport>stkreport=stockReportService.getItemWiseStockReport(getBranch.getBranchCode(), companymstid ,SystemSetting.getSystemDate(),item.getItemId());
			for(int i =0;i<stkreport.size();i++)
			{
				sysQty = sysQty +stkreport.get(i).getQty(); 
			}
			IntentDtl  intentDtl = new IntentDtl();
			
			Optional<ItemMst> itemMst =  itemMstRepository.findById(item.getItemId());
			
//			Integer time = itemMst.get().getReorderWaitingPeriod();
//			time = time*86400;
			//get the intent with item id time period
			IntentItemBranchMst intentItemBranchMst =  intentItemBranchRepo.findByCompanyMstAndItemId(companyMst, item.getItemId());
			if(null != intentItemBranchMst)
			{
				intentDtl.setToBranch(intentItemBranchMst.getBranchCode());
			}
			intentDtl.setItemId(item.getItemId());
			intentDtl.setUnitId(itemMst.get().getUnitId());
			intentDtl.setQty(item.getMaxQty()-sysQty);
			intentDtl.setIntentHdr(intentHdr);
			
			intentDtlRepository.save(intentDtl);
			intentDtlList.add(intentDtl);
			
		}
		
		
		 
		 map.put("itentDtlList", intentDtlList);
		 map.put("intentHdrList", intentHdrList);
		
		return map;
	}

	@Override
	public List<IntentHdr> getIntentHdrs(String pdfFileName) 
	{
		//List<IntentHdr> intentHdrList =   new ArrayList();
		 
		 
		 List<IntentHdr> intent =   intentHdrRepository.findAll();
		
//		 for(IntentHdr i :intent )
//		 {
//			
//			 IntentHdr intentHdr= new IntentHdr();
//			 intentHdr.setBranchCode(i.getBranchCode());
//			 intentHdr.setCompanyId(i.getCompanyId());
//			 intentHdr.setFromBranch(i.getFromBranch());
//			intentHdr.setIntentDate(i.getIntentDate());
//			 intentHdr.setToBranch(i.getToBranch());
//			 intentHdr.setVoucherNumber(i.getVoucherNumber());
//			 intentHdr.setVoucherType(i.getVoucherType());
//			 intentHdrList.add(intentHdr);
//		 }
		
		 return intent;
		
	}
	 

	@Override
	public List<ReorderReport> IntentDtlReport(String companymstid, String branchcode, 
			Date fDate) {
		
		List<ReorderReport> reorderReportList = new ArrayList<>();
		List<Object> intentObj = intentHdrRepository.IntentReportByDate(companymstid,
				branchcode,fDate);
		
		for(int i=0; i<intentObj.size(); i++)
		{
			
			 Object[] objAray = (Object[]) intentObj.get(i);
			 ReorderReport reorderReport = new ReorderReport();


			List<Object> supplierList = intentHdrRepository.IntentSupplierReport(companymstid,(String) objAray[14]);
			
			
			for(int j=0; j<supplierList.size(); i++)
			{
				 //------------------vsersion 3.28
				 Object[] subArray = (Object[]) supplierList.get(i);

				 reorderReport.setSupplierAddress((String) subArray[1]);
				 reorderReport.setSupplierName((String) subArray[0]);
				 reorderReport.setSupplierPhn((String) subArray[2]);
				 //------------------vsersion 3.28 end
			}
			 
			 
			 reorderReport.setBranchEmail((String) objAray[5]);
			 reorderReport.setBranchAddress((String) objAray[1]);
			 reorderReport.setBranchCode((String) objAray[3]);
			 reorderReport.setBranchName((String) objAray[2]);
			 reorderReport.setBranchPhone((String) objAray[4]);
//			 reorderReport.setBranchWebsite(branchWebsite);
			 reorderReport.setCompanyName((String) objAray[0]);
			

			 
			 Date date = (Date) objAray[7];
			 
			 reorderReport.setDate(SystemSetting.UtilDateToString(date));
			 reorderReport.setVoucherNumber((String) objAray[6]);
			 
			 reorderReport.setItemName((String) objAray[8]);
			 reorderReport.setItemQty((Double) objAray[10]);
			 reorderReport.setUnitName((String) objAray[9]);
			 CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
			 AccountHeads accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMst(reorderReport.getSupplierName(),companyMst);
			 ItemMst itemMst = itemMstRepository.findByItemName((String) objAray[8]);
			 List<PurchaseSchemeMst> getpurchaseScheme = purchaseSchemeMstRepo.findBySupplierIdAndItemIdWithDate(accountHeads.getId(),itemMst.getId(),date);
			 if(getpurchaseScheme.size()>0)
			 {
				 reorderReport.setMinimumQty(getpurchaseScheme.get(0).getMinimumQty());
				 reorderReport.setOfferDescription(getpurchaseScheme.get(0).getOfferDescription());
			 }
//			 reorderReport.setMinimumQty((Double) objAray[14]);
//			 reorderReport.setOfferDescription((String) objAray[15]);
			 reorderReportList.add(reorderReport);
			 
		}
		
		
		System.out.println(reorderReportList.size()+"list size izzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
		return reorderReportList;
	}
	
}
