package com.maple.restserver.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import com.maple.javapos.print.POSThermalPrintABS;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.BatchPriceDefinition;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyMst;

import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LocalCustomerMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.ParamValueConfig;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.SalesBillModelClass;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesDtlSummary;
import com.maple.restserver.entity.SalesManMst;
import com.maple.restserver.entity.SalesMessageEntity;
import com.maple.restserver.entity.SalesOrderDtl;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SalesTransHdrSummary;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.entity.TaxDtl;
import com.maple.restserver.entity.TaxMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.report.entity.ItemDtlInWeb;
import com.maple.restserver.report.entity.KOTItems;
import com.maple.restserver.report.entity.PharmacyDayEndReport;
import com.maple.restserver.report.entity.SalesInWeb;
import com.maple.restserver.report.entity.SalesModeWiseBillReport;
import com.maple.restserver.report.entity.SalesTaxSplitReport;
import com.maple.restserver.report.entity.TableWaiterReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.AccountReceivableRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;

import com.maple.restserver.repository.IntentInHdrRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LocalCustomerRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.ParamValueConfigRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesDetailsSummaryRepository;
import com.maple.restserver.repository.SalesManMstRepository;
import com.maple.restserver.repository.SalesOrderDtlRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.SalesTransHdrSummaryRepository;
import com.maple.restserver.repository.StatementOfAccountReportRepository;
import com.maple.restserver.repository.StockTransferOutHdrRepository;
import com.maple.restserver.repository.TaxDtlRepository;
import com.maple.restserver.repository.TaxMstRepository;
import com.maple.restserver.repository.UnitMstRepository;

import com.maple.restserver.resource.SalesTransHdrResource;
import com.maple.restserver.resource.TaxMstResource;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.resourcejavapos.print.ImperialKitchenPrint2;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.service.accounting.task.SalesAccounting;
import com.maple.restserver.service.accounting.task.SalesAccountingFC;
import com.maple.restserver.service.task.SalesDayEndReporting;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component
public class SalesTransHdrImpl implements SalesTransHdrService {

	private static final Logger logger = LoggerFactory.getLogger(MysqlDataTransferServiceImpl.class);

	// Version 1.5

	@Autowired
	SalesOrderDtlRepository salesOrderDtlRepo;
	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepo;

	@Value("${POS_SALES_PREFIX}")
	private String POS_SALES_PREFIX;

	@Value("${ONLINE_SALES_PREFIX}")
	private String ONLINE_SALES_PREFIX;

	// Version 1.5 ENDS

	@Autowired
	ParamValueConfigRepository paramValueConfigRepo;

	

	@Autowired
	BranchMstRepository branchMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	ImperialKitchenPrint2 printingSupport;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;

	@Autowired
	StockTransferOutHdrRepository stocktransferOutHdrRepo;

	@Autowired
	IntentInHdrRepository intentInHdrRepository;

	@Autowired
	SalesDetailsRepository salesDetailsRepository;

	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	private SalesTransHdrSummaryRepository salesTransHdrSummaryRepository;

	@Autowired
	private SalesDetailsSummaryRepository salesDetailsSummaryRepository;

	@Autowired
	ItemMstRepository itemMstRepository;

	@Autowired
	UnitMstRepository unitMstRepository;

	@Autowired
	TaxMstRepository taxMstRepository;

	@Autowired
	TaxMstResource taxMstResource;

	@Autowired
	MultiUnitMstRepository multyMstRepository;

	

	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepo;

	@Autowired
	BatchPriceDefinitionService batchPriceDefinitionService;

	@Autowired
	private PriceDefinitionRepository priceDefinitionRepo;
	
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	
	@Autowired
	SalesTransHdrResource salesTransHdrResource;

	@Autowired
	SalesReceiptsRepository salesReceiptsRepo;
	
	@Autowired
	private AccountReceivableRepository accountReceivableRepository;
	
	@Autowired
	AccountReceivable accountReceivable;
	
	@Autowired
	SalesAccounting salesAccounting;
	
	@Autowired
	CurrencyMstRepository currencyMstRepo;
	

	@Autowired
	SalesAccountingFC salesAccountingfc;
	
	@Autowired
	SalesDayEndReporting salesDayEndReporting;
	
	@Autowired
	LocalCustomerRepository localCustomerRepository;
	
	@Autowired
	BranchMstRepository branchMstRepo;
	
	@Autowired
	SalesManMstRepository salesManMstRepository;
	
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	
	@Override
	public List<SalesTransHdr> getSalesTransHdr(String company_mst_id) {

		List<SalesTransHdr> salesTransHdrList = new ArrayList();

		List<SalesTransHdr> salesTransHdr = salesTransHdrRepository.findAll();

		return salesTransHdr;
	}

	@Override
	public List<Object> getVoucherNumber(String company_mst_id, Date vDate, String vType) {

		if (vType.equals("SALESTYPE")) {

			return salesTransHdrRepository.getsalesVoucherNumber(company_mst_id, vDate);

		}
		if (vType.equals("PURCHASETYPE")) {

			return purchaseHdrRepository.getpurchaseVoucherNumber(company_mst_id, vDate, vType);

		}
		if (vType.equals("STKTROUTTYPE")) {

			return stocktransferOutHdrRepo.getStockVoucherNumber(company_mst_id, vDate);

		}
		if (vType.equals("INTENT REQUESTS")) {

			return intentInHdrRepository.getIntentInVoucherNumber(company_mst_id, vDate);

		} else {
			return null;
		}

	}

	@Override
	public List<SalesTransHdr> findByVoucherNumberAndVoucherDate(String company_mst_id, String vouchernumber,
			Date vdate) {

		return salesTransHdrRepository.findByVoucherNumberAndVoucherDate(vouchernumber, vdate);
	}

	@Override
	public List<SalesTransHdr> printKotByTablenameAndSalesman(String salesman, String tablename) {

		System.out.println("Received KOT   Print Request for " + salesman + " AND " + tablename);

		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.printKotByTablenameAndSalesman(salesman,
				tablename);
		SalesTransHdr salesTransHdr = salesTransHdrList.get(0);

		try {

			/*
			 * Print split if category is different and only one printer is available
			 */
			ParamValueConfig paramvalueConfig = paramValueConfigRepo.findByParamAndCompanyMst("KOTPRINTSPLIT",
					salesTransHdr.getCompanyMst());
			if (null != paramvalueConfig) {
				if (paramvalueConfig.getValue().equalsIgnoreCase("YES")) {

					printingSupport.PrintCategorySplitKot(salesTransHdr.getId());

					// printingSupport.PrintInvoiceThermalPrinterCategorySplitForOnePrinter(salesTransHdr.getId())
					// ;
				} else {
					printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId());
				}
			} else {
				printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId());
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return salesTransHdrList;
	}

	@Override
	public List<SalesTransHdr> printKotInvoiceByTablenameAndSalesman(String salesman, String tablename) {

		System.out.println("Received KOT Invoice Print Request for " + salesman + " AND " + tablename);

		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.printKotByTablenameAndSalesman(salesman,
				tablename);
		SalesTransHdr salesTransHdr = salesTransHdrList.get(0);

		try {
			printingSupport.PrintPerformaInvoiceThermalPrinter(salesTransHdr.getId());

		} catch (SQLException e) {

			logger.error(e.getMessage());
		}

		return salesTransHdrList;
	}

	@Override
	public List<TableWaiterReport> retrivePalcedTable(String companymstid, String branchcode,
			String customiseSalesMode) {
		List<TableWaiterReport> tWreportList = new ArrayList();
		;
		List<Object> obj = salesTransHdrRepository.retrivePalcedTable(companymstid, branchcode, customiseSalesMode);
		for (int i = 0; i < obj.size(); i++) {
			TableWaiterReport tWreport = new TableWaiterReport();

			Object[] objAray = (Object[]) obj.get(i);
			List<SalesDtl> salesDtl = salesDetailsRepository.findBySalesTransHdrId((String) objAray[0]);

			if (salesDtl.size() > 0) {
				tWreport.setTableName((String) objAray[1]);
				tWreport.setCustomiseSalesMode((String) objAray[3]);
				tWreport.setWaiterName((String) objAray[2]);
				tWreportList.add(tWreport);
			}
		}
		return tWreportList;

	}

	@Override
	public List<SalesModeWiseBillReport> findNumericVoucherNoBySalesModeAndDate(Date date, String companymstid) {

		List<SalesModeWiseBillReport> salesModeWiseBillReportList = new ArrayList<>();
		List<String> salesModeList = salesTransHdrRepository.findAllSalesMode(companymstid);

		System.out.println(salesModeList.size() + "sales mode list size isssssssssssssssssssssss");

		for (String salesmode : salesModeList) {
			String salesMode = salesmode;
			if (null == salesMode) {
				continue;
			}

			List<Object> obj = salesTransHdrRepository.findNumericVoucherNoBySalesModeAndDate(
					ClientSystemSetting.UtilDateToSQLDate(date), companymstid, salesMode);

			int intCount = salesTransHdrRepository.findNoOfVoucherNo(date, companymstid, salesMode);

			for (int i = 0; i < obj.size(); i++) {
				Object[] objAray = (Object[]) obj.get(i);
				SalesModeWiseBillReport salesModeWiseBillReport = new SalesModeWiseBillReport();
				salesModeWiseBillReport.setBranchCode((String) objAray[3]);
				if (null != objAray[2]) {
					BigInteger lstNo = (BigInteger) objAray[2];
					salesModeWiseBillReport.setMaxVoucherNumericNo(lstNo.longValue());

				}

				if (null != objAray[1]) {
					BigInteger frstNo = (BigInteger) objAray[1];
					salesModeWiseBillReport.setMinVoucherNumericNo(frstNo.longValue());

				}
				salesModeWiseBillReport.setSalesMode((String) objAray[0]);
				salesModeWiseBillReport.setCount(intCount);

				salesModeWiseBillReportList.add(salesModeWiseBillReport);

			}
		}
		return salesModeWiseBillReportList;
	}

	@Override
	public List<SalesTransHdr> printKotBySalesTransHdr(String salesTransHdrId) {

		System.out.println("Received KOT   Print Request for  sales transhdrID " + salesTransHdrId);

		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.printKotBySalesTransHdr(salesTransHdrId);
		SalesTransHdr salesTransHdr = salesTransHdrList.get(0);

		try {
			printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return salesTransHdrList;
	}

//	@Override
//	public List<SalesDtl> GenerateSalesInvoice(String companymstid, String customer, String salesman) {
//		
//		
//		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
//		if(!companyOpt.isPresent())
//		{
//			return null;
//		}
//		
//		Optional<CustomerMst> customerMstOpt = customerMstRepository.findByCompanyMstAndCustomerName(companyOpt.get(), customer);
//		if(!customerMstOpt.isPresent())
//		{
//			return null;
//		}
//		CustomerMst customerMst = customerMstOpt.get();
//		
//		
//		SalesTransHdr salesTransHdr = salesTransHdrRepository.
//				findByCompanyMstSalesManIdAndCustomerId(companyOpt.get(),salesman,customerMst);
//		
//		
//		List<SalesDtl> salesDtlList = salesDetailsRepository.findBySalesTransHdrId(salesTransHdr.getId());
//		if(salesDtlList.size() == 0)
//		{
//			return null;
//		}
//		
//		SummarySalesDtl summary = salessummary(salesTransHdr.getId());
//		
//		if (null == customerMst.getCustomerGst() || customerMst.getCustomerGst().length() < 13) {
//			salesTransHdr.setSalesMode("B2C");
//			salesTransHdr.setIsBranchSales("N");
//		} else {
//			salesTransHdr.setSalesMode("B2B");
//			salesTransHdr.setIsBranchSales("N");
//
//		}
//		
//		Optional<BranchMst> branchOpt = branchMstRepository.findById(customerMst.getId());
//		BranchMst branchMst = branchOpt.get();
//		if(null != branchMst)
//		{
//			salesTransHdr.setSalesMode("BRANCH_SALES");
//			salesTransHdr.setIsBranchSales("Y");
//		}
//		
//		salesTransHdr.setCustomerMst(customerMst);
//		salesTransHdr.setCustomerId(customerMst.getId());
//		salesTransHdr.setInvoiceAmount(summary.getTotalAmount());
//		return null;
//	}
//	
//	private SummarySalesDtl salessummary(String salesTransHdrId) {
//		List<Object> objList = salesDetailsRepository.findSumSalesDtl(salesTransHdrId);
//
//		Object[] objAray = (Object[]) objList.get(0);
//
//		SummarySalesDtl summary = new SummarySalesDtl();
//		summary.setTotalQty((Double) objAray[0]);
//		summary.setTotalAmount((Double) objAray[1]);
//		summary.setTotalTax((Double) objAray[2]);
//		summary.setTotalCessAmt((Double) objAray[3]);
//
//		return summary;
//
//	}

	// --------------------------version 1.5

	@Override
	public List<SalesModeWiseBillReport> findBillseriesByVoucherDate(Date date, String companymstid) {

		List<SalesModeWiseBillReport> salesModeWiseBillReportList = new ArrayList<>();
//				List<String> salesModeList = salesTransHdrRepository.findAllSalesMode(companymstid);

		List<String> voucherType = salesTransHdrRepository.findAllVoucherTypes(companymstid);

		// ...................version 1.9
//				if(null != ONLINE_SALES_PREFIX)
//				{
//					voucherType.add(ONLINE_SALES_PREFIX);
//				}
		// ...................version 1.9 end

		if (null != POS_SALES_PREFIX) {
			voucherType.add(POS_SALES_PREFIX);
		}

		for (String salesmode : voucherType) {
			String salesMode = salesmode;
			if (null == salesMode) {
				continue;
			}

//					List<Object> obj = salesTransHdrRepository.findNumericVoucherNoBySalesModeAndDate(SystemSetting.UtilDateToSQLDate(date),companymstid,salesMode);
			System.out.print(date + "date isssssssssssssssssssssssssssssssssssssssssss");
			List<Object> obj = salesTransHdrRepository.findNumericVoucherNoByBillSeriesAndDate(
					ClientSystemSetting.UtilDateToSQLDate(date), companymstid, "%" + salesMode + "%");

			// ----------------version 1.9

			int count = salesTransHdrRepository.findCountOfBillSeries(ClientSystemSetting.UtilDateToSQLDate(date),
					companymstid, "%" + salesMode + "%");

			// ----------------version 1.9 end

			for (int i = 0; i < obj.size(); i++) {

				Object[] objAray = (Object[]) obj.get(i);
				SalesModeWiseBillReport salesModeWiseBillReport = new SalesModeWiseBillReport();
				salesModeWiseBillReport.setBranchCode((String) objAray[2]);
				if (null != objAray[1]) {
					BigInteger lstNo = (BigInteger) objAray[1];
					salesModeWiseBillReport.setMaxVoucherNumericNo(lstNo.longValue());

				}

				if (null != objAray[0]) {
					BigInteger frstNo = (BigInteger) objAray[0];
					salesModeWiseBillReport.setMinVoucherNumericNo(frstNo.longValue());

				}
//						 salesModeWiseBillReport.setSalesMode((String)objAray[0]);

				List<String> salesModeList = salesTransHdrRepository.findBillSeriesSalesMode(
						ClientSystemSetting.UtilDateToSQLDate(date), companymstid, "%" + salesMode + "%");

				if (salesModeList.get(0).equalsIgnoreCase("B2B") || salesModeList.get(0).equalsIgnoreCase("B2C")) {
					salesModeWiseBillReport.setSalesMode("WholeSale");
				} else {
					salesModeWiseBillReport.setSalesMode("POS");

				}

				salesModeWiseBillReport.setCount(count);

				salesModeWiseBillReportList.add(salesModeWiseBillReport);

			}
		}
		return salesModeWiseBillReportList;
	}

	// ---------------version 4.11
	@Override
	public List<SalesTaxSplitReport> SaleTaxSplit(Date edate, CompanyMst companyMst, String branchCode) {
		List<SalesTaxSplitReport> taxSplit = new ArrayList<SalesTaxSplitReport>();

		List<Object> obj = salesTransHdrRepository.SaleTaxSplit(edate, companyMst, branchCode);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);

			SalesTaxSplitReport salesTaxSplitReport = new SalesTaxSplitReport();
			salesTaxSplitReport.setTaxableValue((Double) objAray[0]);
			salesTaxSplitReport.setTaxAmount((Double) objAray[1]);
			salesTaxSplitReport.setTaxRate((Double) objAray[2]);

			taxSplit.add(salesTaxSplitReport);
		}

		return taxSplit;
	}
	// ---------------version 4.11 end

	@Override
	public String saveSalesSummary(Date date, CompanyMst companyMst, String branchcode) {

		String savedStatus = saveSalesHdrAndDetailsSummary(date, companyMst, branchcode);
		return savedStatus;
	}

	private String saveSalesHdrAndDetailsSummary(Date date, CompanyMst companyMst, String branchcode) {

		Double countOfSalesHdr = salesTransHdrRepository.getCountOfSalesHdrByDate(date);

		if (countOfSalesHdr == 0) {
			return "Sales Hdr record not found";
		}

		SalesTransHdrSummary salesTransHdrSummary = SaveSalesTransHdrSummary(date, companyMst, branchcode);

		if (null == salesTransHdrSummary) {
			return "failed - creating sales_summary_hdr";
		}

		String savedSalesDtlStatus = saveSalesDtlSummary(salesTransHdrSummary, date);

		return savedSalesDtlStatus;
	}

	private String saveSalesDtlSummary(SalesTransHdrSummary salesTransHdrSummary, Date date) {

		List<Object> objList = salesDetailsRepository.salesDtlByItemAndDate(date, salesTransHdrSummary.getCompanyMst(),
				salesTransHdrSummary.getBranchCode());

		if (objList.size() == 0) {
			return "empty records of sales details";
		}

		for (int i = 0; i < objList.size(); i++) {
			Object[] obj = (Object[]) objList.get(i);

			SalesDtlSummary salesDtlSummary = new SalesDtlSummary();

			salesDtlSummary.setItemId((String) obj[0]);
			salesDtlSummary.setItemName((String) obj[1]);
			salesDtlSummary.setBarcode((String) obj[2]);
			salesDtlSummary.setAmount((Double) obj[3]);
			salesDtlSummary.setCgstAmount((Double) obj[4]);
			salesDtlSummary.setStandardPrice((Double) obj[5]);
			salesDtlSummary.setQty((Double) obj[6]);
			salesDtlSummary.setRate((Double) obj[7]);
			salesDtlSummary.setAddCessRate((Double) obj[8]);
			salesDtlSummary.setMrp((Double) obj[9]);
			salesDtlSummary.setCgstTaxRate((Double) obj[10]);
			salesDtlSummary.setAddCessAmount((Double) obj[11]);
			salesDtlSummary.setCessAmount((Double) obj[12]);
			salesDtlSummary.setSgstAmount((Double) obj[13]);
			salesDtlSummary.setTaxRate((Double) obj[14]);
			salesDtlSummary.setIgstAmount((Double) obj[15]);
			salesDtlSummary.setIgstTaxRate((Double) obj[16]);
			salesDtlSummary.setCessRate((Double) obj[17]);

			salesDtlSummary = salesDetailsSummaryRepository.save(salesDtlSummary);
		}

		return "Succcess";
	}

	private SalesTransHdrSummary SaveSalesTransHdrSummary(Date date, CompanyMst companyMst, String branchcode) {

		SalesTransHdrSummary salesTransHdrSummary = new SalesTransHdrSummary();

		List<AccountHeads> customerMst = (List<AccountHeads>) accountHeadsRepository.findByAccountNameAndCompanyMst("POS", companyMst);

		List<Object> objectList = salesTransHdrRepository.findSalesTransHdrSummaryByDate(date, companyMst, branchcode);

		for (int i = 0; i < objectList.size(); i++) {
			Object[] objAray = (Object[]) objectList.get(i);

			salesTransHdrSummary.setPaidAmount((Double) objAray[0]);
			salesTransHdrSummary.setCardamount((Double) objAray[1]);
			salesTransHdrSummary.setCashPay((Double) objAray[2]);
			salesTransHdrSummary.setInvoiceAmount((Double) objAray[3]);
			salesTransHdrSummary.setChangeAmount((Double) objAray[4]);

		}

		salesTransHdrSummary.setVoucherDate(date);
		salesTransHdrSummary.setCompanyMst(companyMst);
		salesTransHdrSummary.setBranchCode(branchcode);
		salesTransHdrSummary.setIsBranchSales("N");
		salesTransHdrSummary.setSalesMode("POS");
		salesTransHdrSummary.setAccountHeads(customerMst.get(0));
		salesTransHdrSummary.setCustomerId(customerMst.get(0).getId());

		String vNo = null;

		String finYear = SystemSetting.getFinancialYear();

		VoucherNumber voucherNo = voucherService.generateInvoice(SystemSetting.getFinancialYear() + POS_SALES_PREFIX,
				companyMst.getId());

		vNo = voucherNo.getCode();

		salesTransHdrSummary.setVoucherNumber(vNo);

		salesTransHdrSummary = salesTransHdrSummaryRepository.save(salesTransHdrSummary);

		return salesTransHdrSummary;
	}

	// --------------------------------Print Invoice -----Call API to print KOT.
	// passing Table Name and KOT number -------
	@Override
	public List<SalesTransHdr> printKotInvoiceByTableNameAndKotNumber(String kotnumber, String tablename, Date vdate) {

		System.out.println("Received KOT Invoice Print Request for " + kotnumber + " AND " + tablename);

		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository
				.printKotInvoiceByTableNameAndKotNumber(kotnumber, tablename, vdate);

		if (salesTransHdrList.size() == 0) {
			return null;
		}
		SalesTransHdr salesTransHdr = salesTransHdrList.get(0);

		try {
			printingSupport.PrintPerformaInvoiceThermalPrinter(salesTransHdr.getId());

		} catch (SQLException e) {

			logger.error(e.getMessage());
		}

		return salesTransHdrList;
	}

	// --------------------------------Print Performa Invoice -----Call API to print
	// KOT. passing Table Name and KOT number -------
	@Override
	public List<SalesTransHdr> printPerformaInvoiceByTableNameAndKotNumber(String kotnumber, String tablename,
			Date vdate) {

		System.out.println("Received KOT Invoice Print Request for " + kotnumber + " AND " + tablename);

		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository
				.printPerformaInvoiceByTableNameAndKotNumber(kotnumber, tablename, vdate);

		if (salesTransHdrList.size() == 0) {
			return null;
		}
		SalesTransHdr salesTransHdr = salesTransHdrList.get(0);

		try {
			printingSupport.PrintPerformaInvoiceThermalPrinter(salesTransHdr.getId());

		} catch (SQLException e) {

			logger.error(e.getMessage());
		}

		return salesTransHdrList;
	}

	@Override
	public List<SalesTransHdr> printKotBySalesmanAndKotNumber(String salesman, String tablename, String kotnumber,
			Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SalesTransHdr> printRunningKot(String salesman, String tablename, String kotnumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SalesTransHdr saveSalesTransHdrFromSalesOrderId(String orderid) {

		SalesOrderTransHdr salesOrderTransHdr = salesOrderTransHdrRepo.findById(orderid).get();

		SalesTransHdr salesTransHdr = new SalesTransHdr();

//		salesTransHdr.setAmountTendered(sale);
		if (null != salesOrderTransHdr.getBranchCode()) {
			salesTransHdr.setBranchCode(salesOrderTransHdr.getBranchCode());
		}
		if (null != salesOrderTransHdr.getCompanyMst()) {
			salesTransHdr.setCompanyMst(salesOrderTransHdr.getCompanyMst());
		}
		if (null != salesOrderTransHdr.getChangeAmount()) {
			salesTransHdr.setChangeAmount(salesOrderTransHdr.getChangeAmount());
		}
		if (null != salesOrderTransHdr.getCashPay()) {
			salesTransHdr.setCashPay(salesOrderTransHdr.getCashPay());
		}
		if (null != salesOrderTransHdr.getCardType()) {
			salesTransHdr.setCardType(salesOrderTransHdr.getCardType());
		}
		if (null != salesOrderTransHdr.getCardamount()) {
			salesTransHdr.setCardamount(salesOrderTransHdr.getCardamount());
		}
		// salesTransHdr.setBranchSaleCustomer(salesOrderTransHdr.getb);
		if (null != salesOrderTransHdr.getCardNo()) {
			salesTransHdr.setCardNo(salesOrderTransHdr.getCardNo());
		}
		if (null != salesOrderTransHdr.getCreditOrCash()) {
			salesTransHdr.setCreditOrCash(salesOrderTransHdr.getCreditOrCash());
		}
//		salesTransHdr.set
		// salesTransHdr.setCreditPeriod(salesOrderTransHdr.getcr);
		// salesTransHdr.setCurrencyConversionRate(salesOrderTransHdr.getCu);
		// salesTransHdr.setCurrencyId(salesOrderTransHdr.getCurrencyId());
		// salesTransHdr.setCurrencyType(salesOrderTransHdr.getCurrencyType());
		// salesTransHdr.setCustomerId(salesOrderTransHdr.getCustomerId());
		if (null != salesOrderTransHdr.getCustomiseSalesMode()) {
			salesTransHdr.setCustomiseSalesMode(salesOrderTransHdr.getCustomiseSalesMode());
		}
		if (null != salesOrderTransHdr.getDeliveryBoyId()) {
			salesTransHdr.setDeliveryBoyId(salesOrderTransHdr.getDeliveryBoyId());
		}
		// salesTransHdr.setDiscount(salesOrderTransHdr.getDis);
		// salesTransHdr.setDoctorId(salesOrderTransHdr.getD);
		// salesTransHdr.setEditedStatus(salesOrderTransHdr.get);
		// salesTransHdr.setFbKey(salesOrderTransHdr.getF);
		// salesTransHdr.setFcInvoiceAmount(salesOrderTransHdr.getF);
		if (null != salesOrderTransHdr.getInvoiceAmount()) {

			salesTransHdr.setInvoiceAmount(salesOrderTransHdr.getInvoiceAmount());
		}
		if (null != salesOrderTransHdr.getInvoiceDiscount()) {
			salesTransHdr.setInvoiceDiscount(salesOrderTransHdr.getInvoiceDiscount());
		}

		// salesTransHdr.setInvoiceNumberPrefix(salesOrderTransHdr.getIn);
		if (null != salesOrderTransHdr.getInvoicePrintType()) {
			salesTransHdr.setInvoicePrintType(salesOrderTransHdr.getInvoicePrintType());
		}
		// salesTransHdr.setIsBranchSales(salesOrderTransHdr.getIs);
		if (null != salesOrderTransHdr.getItemDiscount()) {
			salesTransHdr.setItemDiscount(salesOrderTransHdr.getItemDiscount());
		}
		// salesTransHdr.setKotNumber(salesOrderTransHdr.getKo);
		if (null != salesOrderTransHdr.getLocalCustomerId()) {
			salesTransHdr.setLocalCustomerMst(salesOrderTransHdr.getLocalCustomerId());
		}
		if (null != salesOrderTransHdr.getMachineId()) {
			salesTransHdr.setMachineId(salesOrderTransHdr.getMachineId());
		}
		// salesTransHdr.setNumericVoucherNumber(salesOrderTransHdr.getNu);
		if (null != salesOrderTransHdr.getPaidAmount()) {
			salesTransHdr.setPaidAmount(salesOrderTransHdr.getPaidAmount());
		}
		// salesTransHdr.setPatientMst(salesOrderTransHdr.getPa);
		// salesTransHdr.setPaytmAmount(salesOrderTransHdr.getPa);
		if (null != salesOrderTransHdr.getPerformaInvoicePrinted()) {
			salesTransHdr.setPerformaInvoicePrinted(salesOrderTransHdr.getPerformaInvoicePrinted());
		}
		// salesTransHdr.setPoNumber(salesOrderTransHdr.getpo);
		// salesTransHdr.setProcessInstanceId(salesOrderTransHdr.getprocessIn);
		// salesTransHdr.setSaleOrderHrdId(salesOrderTransHdr.getSa);
		if (null != salesOrderTransHdr.getSalesManId()) {
			salesTransHdr.setSalesManId(salesOrderTransHdr.getSalesManId());
		}
		if (null != salesOrderTransHdr.getSalesMode()) {
			salesTransHdr.setSalesMode(salesOrderTransHdr.getSalesMode());
		}
		if (null != salesOrderTransHdr.getSaleOrderReceiptVoucherNumber()) {
			salesTransHdr.setSalesReceiptsVoucherNumber(salesOrderTransHdr.getSaleOrderReceiptVoucherNumber());
		}
		if (null != salesOrderTransHdr.getServingTableName()) {
			salesTransHdr.setServingTableName(salesOrderTransHdr.getServingTableName());
		}
		if (null != salesOrderTransHdr.getSodexoAmount()) {
			salesTransHdr.setSodexoAmount(salesOrderTransHdr.getSodexoAmount());
		}
		// salesTransHdr.setSourceBranchCode(salesOrderTransHdr.getSo);
		// salesTransHdr.setSourceIP(salesOrderTransHdr.getSo);
		// salesTransHdr.setSourcePort(salesOrderTransHdr.getSo);
		// salesTransHdr.setTakeOrderNumber(salesOrderTransHdr.getTa);
		// salesTransHdr.setUpdatedTime(salesOrderTransHdr.getUp);
		if (null != salesOrderTransHdr.getUserId()) {
			salesTransHdr.setUserId(salesOrderTransHdr.getUserId());
		}

		// salesTransHdr.setVoucherType(salesOrderTransHdr.getVo);

		salesTransHdr.setSaleOrderHrdId(salesOrderTransHdr.getId());

		if (null != salesOrderTransHdr.getAccountHeads()) {
			salesTransHdr.setAccountHeads(salesOrderTransHdr.getAccountHeads());
		}

		if (null != salesOrderTransHdr.getLocalCustomerId()) {
			salesTransHdr.setLocalCustomerMst(salesOrderTransHdr.getLocalCustomerId());
		}

		salesTransHdr = salesTransHdrRepository.save(salesTransHdr);

		List<SalesOrderDtl> salesOrdrDtlList = salesOrderDtlRepo.findBySalesOrderTransHdrId(orderid);
		for (int i = 0; i < salesOrdrDtlList.size(); i++) {
			SalesDtl salesDtl = new SalesDtl();

			if (null != salesOrdrDtlList.get(i).getAmount()) {
				salesDtl.setAmount(salesOrdrDtlList.get(i).getAmount());
			}
			if (null != salesOrdrDtlList.get(i).getAddCessAmount()) {
				salesDtl.setAddCessAmount(salesOrdrDtlList.get(i).getAddCessAmount());
			}
			if (null != salesOrdrDtlList.get(i).getAddCessRate()) {
				salesDtl.setAddCessRate(salesOrdrDtlList.get(i).getAddCessRate());
			}
			if (null != salesOrdrDtlList.get(i).getBarode()) {
				salesDtl.setBarcode(salesOrdrDtlList.get(i).getBarode());
			}
			if (null != salesOrdrDtlList.get(i).getBatch()) {
				salesDtl.setBatch(salesOrdrDtlList.get(i).getBatch());
			}
			if (null != salesOrdrDtlList.get(i).getCessAmount()) {
				salesDtl.setCessAmount(salesOrdrDtlList.get(i).getCessAmount());
			}
			if (null != salesOrdrDtlList.get(i).getCessRate()) {
				salesDtl.setCessRate(salesOrdrDtlList.get(i).getCessRate());
			}
			if (null != salesOrdrDtlList.get(i).getCgstAmount()) {
				salesDtl.setCgstAmount(salesOrdrDtlList.get(i).getCgstAmount());
			}
			if (null != salesOrdrDtlList.get(i).getCgstTaxRate()) {
				salesDtl.setCgstTaxRate(salesOrdrDtlList.get(i).getCgstTaxRate());
			}
			if (null != salesOrdrDtlList.get(i).getCompanyMst()) {
				salesDtl.setCompanyMst(salesOrdrDtlList.get(i).getCompanyMst());
			}

			// salesDtl.setCostPrice(salesOrdrDtlList.get(i).getCo);
			if (null != salesOrdrDtlList.get(i).getDiscount()) {
				salesDtl.setDiscount(salesOrdrDtlList.get(i).getDiscount());
			}
			if (null != salesOrdrDtlList.get(i).getExpiryDate()) {
				java.sql.Date expDate = SystemSetting.UtilDateToSQLDate(salesOrdrDtlList.get(i).getExpiryDate());

				salesDtl.setExpiryDate(expDate);
			}
			// salesDtl.setFbKey(salesOrdrDtlList.get(i).getFb);
			// salesDtl.setFcAmount(salesOrdrDtlList.get(i).getF);
			// salesDtl.setFcCessAmount(salesOrdrDtlList.get(i).getfcCessAmount);
			// salesDtl.setFcCgst(salesOrdrDtlList.get(i).getfcCgst);
			// salesDtl.setFcDiscount(salesOrdrDtlList.get(i).getfcDiscount);
			// salesDtl.setFcIgstAmount(salesOrdrDtlList.get(i).getfcIgstAmount);
			// salesDtl.setFcMrp(salesOrdrDtlList.get(i).getfcMrp);
			// salesDtl.setFcRate(salesOrdrDtlList.get(i).getfcRate);
			// salesDtl.setFcSgst(salesOrdrDtlList.get(i).getfcSgst);
			// salesDtl.setFcStandardPrice(salesOrdrDtlList.get(i).getfcStandardPrice);
			// salesDtl.setFcTaxAmount(salesOrdrDtlList.get(i).getfcTaxAmount);
			// salesDtl.setFcTaxRate(salesOrdrDtlList.get(i).getfcTaxRate);
			if (null != salesOrdrDtlList.get(i).getIgstAmount()) {
				salesDtl.setIgstAmount(salesOrdrDtlList.get(i).getIgstAmount());
			}
			if (null != salesOrdrDtlList.get(i).getIgstTaxRate()) {
				salesDtl.setIgstTaxRate(salesOrdrDtlList.get(i).getIgstTaxRate());
			}
			if (null != salesOrdrDtlList.get(i).getItemId()) {
				salesDtl.setItemId(salesOrdrDtlList.get(i).getItemId());
			}
			if (null != salesOrdrDtlList.get(i).getItemName()) {
				salesDtl.setItemName(salesOrdrDtlList.get(i).getItemName());
			}
			if (null != salesOrdrDtlList.get(i).getItemTaxaxId()) {
				salesDtl.setItemTaxaxId(salesOrdrDtlList.get(i).getItemTaxaxId());
			}
			// salesDtl.setKotDescription(salesOrdrDtlList.get(i).getK);
			// salesDtl.setListPrice(salesOrdrDtlList.get(i).getLi);
			if (null != salesOrdrDtlList.get(i).getMrp()) {
				salesDtl.setMrp(salesOrdrDtlList.get(i).getMrp());
			}
			// salesDtl.setOfferReferenceId(salesOrdrDtlList.get(i).getO);
			// salesDtl.setPrintKotStatus(salesOrdrDtlList.get(i).getP);
			if (null != salesOrdrDtlList.get(i).getQty()) {
				salesDtl.setQty(salesOrdrDtlList.get(i).getQty());
			}
			if (null != salesOrdrDtlList.get(i).getRateBeforeDiscount()) {
				salesDtl.setRateBeforeDiscount(salesOrdrDtlList.get(i).getRateBeforeDiscount());
			}
			// salesDtl.setReturnedQty(salesOrdrDtlList.get(i).getR);

			salesDtl.setSalesTransHdr(salesTransHdr);
			// salesDtl.setSchemeId(salesOrdrDtlList.get(i).getS);
			if (null != salesOrdrDtlList.get(i).getSgstAmount()) {
				salesDtl.setSgstAmount(salesOrdrDtlList.get(i).getSgstAmount());
			}
			if (null != salesOrdrDtlList.get(i).getSgstTaxRate()) {
				salesDtl.setSgstTaxRate(salesOrdrDtlList.get(i).getSgstTaxRate());
			}
			if (null != salesOrdrDtlList.get(i).getStandardPrice()) {
				salesDtl.setStandardPrice(salesOrdrDtlList.get(i).getStandardPrice());
			}
			// salesDtl.setStatus(salesOrdrDtlList.get(i).getSt);
			if (null != salesOrdrDtlList.get(i).getTaxRate()) {
				salesDtl.setTaxRate(salesOrdrDtlList.get(i).getTaxRate());
			}
			if (null != salesOrdrDtlList.get(i).getUnitId()) {
				salesDtl.setUnitId(salesOrdrDtlList.get(i).getUnitId());
			}
			if (null != salesOrdrDtlList.get(i).getUnitName()) {
				salesDtl.setUnitName(salesOrdrDtlList.get(i).getUnitName());
			}

			if (null != salesOrdrDtlList.get(i).getRate()) {
				salesDtl.setRate(salesOrdrDtlList.get(i).getRate());
			}

			salesDetailsRepository.save(salesDtl);
		}

		if (null != salesTransHdr) {
			salesOrderTransHdr.setOrderStatus("Realized");
			salesOrderTransHdrRepo.save(salesOrderTransHdr);
		}

		return salesTransHdr;
	}

	@Override
	public List<PharmacyDayEndReport> getPharmacyDayEndreport(Date fudate, String branchCash, CompanyMst companyMst) {

		List<PharmacyDayEndReport> getPharmacyDayEndreportList = new ArrayList<PharmacyDayEndReport>();

		List<Object> obj = salesTransHdrRepository.getPharmacyDayEndReport(fudate);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);

			PharmacyDayEndReport pharmacyDayEndReport = new PharmacyDayEndReport();

			pharmacyDayEndReport.setVoucherNumber((String) objAray[0]);
			pharmacyDayEndReport.setCredit((Double) objAray[3]);
			pharmacyDayEndReport.setCustomer((String) objAray[4]);

//			pharmacyDayEndReport.setInsurancePolicy((String) objAray[5]);

			pharmacyDayEndReport.setInvoiceAmount((Double) objAray[2]);
			pharmacyDayEndReport.setVoucherType((String) objAray[1]);

			getPharmacyDayEndreportList.add(pharmacyDayEndReport);
		}

		return getPharmacyDayEndreportList;
	}

	@Override
	public Double getPharmacyCashSales(Date fudate, String branchCash, CompanyMst companyMst) {
		Double cashSales = salesTransHdrRepository.getCashSales(fudate, branchCash, companyMst);
//		if(null==cashSales)
//		{
//			cashSales=00.0;
//		}

		return cashSales;
	}

	@Override
	public Double getPharmacyCashReceived(Date fudate, String branchCash, CompanyMst companyMst) {
		Double cashReceived = salesTransHdrRepository.getcashReceived(fudate, branchCash, companyMst);
//		if(null==cashReceived)
//		{
//			cashReceived=00.0;
//		}

		return cashReceived;
	}

	@Override
	public Double getPharmacyCashPaid(Date fudate, String branchCash, CompanyMst companyMst) {
		Double cashPaid = salesTransHdrRepository.getCashPaid(fudate, branchCash, companyMst);
//		if(null==cashPaid)
//		{
//			cashPaid=00.0;
//		}

		return cashPaid;

	}

	@Override
	public Double getPharmacyCashBalance(Date fudate, String branchCash, CompanyMst companyMst) {
		Double cashBalance = salesTransHdrRepository.getOpeningBalance(fudate, branchCash, companyMst);
//		if(null==cashBalance)
//		{
//			cashBalance=00.0;
//		}

		return cashBalance;
	}

	@Override
	public Double getPharmacyClosingCashBalance(Date fudate, String branchCash, CompanyMst companyMst) {
		Double ClosingCashBalance = salesTransHdrRepository.getclosingBalance(fudate, branchCash, companyMst);
//		if(null==ClosingCashBalance)
//		{
//			ClosingCashBalance=00.0;
//		}

		return ClosingCashBalance;
	}

	@Override
	public String getAccountIdByAccountName(String branchCash) {
		String acountName = salesTransHdrRepository.AccountIdByAccountName(branchCash);

		return acountName;

	}

	@Override
	public String updatePostedToServerStatus(List<String> hdrIds) {

		try {

			// =============MAP-104=======commented for api based
			// publishing=================
//			salesTransHdrRepository.updatePostedToServerStatus(hdrIds);

		} catch (Exception e) {
			e.printStackTrace();
			return "FAILED";
		}
		return "SUCCESS";
	}

	// =============MAP-131-api-for-fetch-kot-details===============anandu==================
	@Override
	public List<KOTItems> getKotDetail() {
		List<KOTItems> kotItemsList = new ArrayList<KOTItems>();
		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.getKotDetail();
		for (SalesTransHdr salesTransHdr : salesTransHdrList) {
			KOTItems kOTItems = new KOTItems();

			kOTItems.setWaiter(salesTransHdr.getSalesManId());
			kOTItems.setTables(salesTransHdr.getServingTableName());
			kOTItems.setSalesTransHdr(salesTransHdr);

			if (null != salesTransHdr) {
				List<SalesDtl> salesDtlList = salesDetailsRepository.findBySalesTransHdrId(salesTransHdr.getId());
				kOTItems.setArrayofsalesdtl((ArrayList<SalesDtl>) salesDtlList);
			}

			kotItemsList.add(kOTItems);
		}
		return kotItemsList;

	}

	// ===================MAP-203-api-for-savesalesweb=====================anandu==================
	@Override
	public SalesInWeb saveSalesInWeb(@Valid SalesInWeb salesInWeb, String companymstid) {
		if (null != salesInWeb) {
			ArrayList<ItemDtlInWeb> itemDtlInWebArray = salesInWeb.getItemDtlInWeb();
			AccountHeads cust = salesInWeb.getAccountHeads();

			SalesTransHdr salesTransHdr = null;

			for (ItemDtlInWeb itemDtlInWeb : itemDtlInWebArray) {

				if (null == salesTransHdr) {
					salesTransHdr = createSalesTransHdr(cust, salesInWeb,companymstid);
				}

				createSalesDtl(salesTransHdr, itemDtlInWeb, salesInWeb);
			}

			List<SalesDtl> salesDtl = salesDetailsRepository.findBySalesTransHdr(salesTransHdr);

			if (salesDtl.size() > 0) {
				VoucherNumber voucherNo = voucherService.generateInvoice("WS", salesTransHdr.getCompanyMst().getId());

				salesTransHdr.setVoucherNumber(voucherNo.getCode());

				salesTransHdr = salesTransHdrRepository.saveAndFlush(salesTransHdr);
				salesInWeb.setVoucherNumber(salesTransHdr.getVoucherNumber());

			}
			
	//==========MAP-235-item-batch-dtl-and-hdr-updation============================anandu==============19-10-2021====		
		
			salesTransHdrResource.SalesStockUpdate(salesTransHdr.getVoucherNumber(), salesTransHdr.getVoucherDate(), salesTransHdr, MapleConstants.Store);
	
			
			//.................SalesReceiptSavings..///

			SalesReceipts salesReceipts = new SalesReceipts();
			salesReceipts.setReceiptMode("CREDIT");
			salesReceipts.setReceiptAmount(salesInWeb.getInvoiceAmount());
			salesReceipts.setSalesTransHdr(salesTransHdr);
			salesReceipts.setAccountId(salesTransHdr.getCustomerId());
			salesReceipts.setBranchCode(salesTransHdr.getBranchCode());
		      Optional<CompanyMst> com=companyMstRepository.findById(companymstid);
				salesReceipts.setCompanyMst(com.get());
				salesReceiptsRepo.save(salesReceipts);
				
				//====================================================
				
				
				List<AccountReceivable> accountReceivableList = accountReceivableRepository
						.findByCompanyMstAndSalesTransHdr(salesTransHdr.getCompanyMst(), salesTransHdr);
				

				if (accountReceivableList.size() > 0) {
					accountReceivable = accountReceivableList.get(0);
					if (null != accountReceivable) {
						accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
						java.sql.Date ldate = SystemSetting.UtilDateToSQLDate(salesTransHdr.getVoucherDate());
						accountReceivable.setVoucherDate(ldate);
						accountReceivable = accountReceivableRepository.saveAndFlush(accountReceivable);

					}
				}else {
					accountReceivable = new AccountReceivable();
					
					accountReceivable.setAccountId(cust.getId());
					accountReceivable.setAccountHeads(cust);
		
						accountReceivable.setDueAmount(salesInWeb.getInvoiceAmount());
//						accountReceivable.setDueAmount(salesInWeb.getInvoiceAmount());
//						accountReceivable.setBalanceAmount(salesInWeb.getInvoiceAmount());
						
					LocalDate due = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
					LocalDate dueDate = due.plusDays(cust.getCreditPeriod());
					accountReceivable.setDueDate(java.sql.Date.valueOf(dueDate));
					accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
					accountReceivable.setSalesTransHdr(salesTransHdr);
					accountReceivable.setRemark("Wholesale");
					LocalDate due1 = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
					accountReceivable.setVoucherDate(java.sql.Date.valueOf(due1));
					accountReceivable.setPaidAmount(0.0);
					accountReceivable.setCompanyMst(salesTransHdr.getCompanyMst());
					accountReceivableRepository.save(accountReceivable);
					
				}
				/*
				 * Manage Stock
				 * 
				 */

				try {
					salesAccounting.execute(salesTransHdr.getVoucherNumber(), salesTransHdr.getVoucherDate(),
							salesTransHdr.getId(), salesTransHdr.getCompanyMst());
					if (null != salesTransHdr.getCompanyMst().getCurrencyName()
							&& !salesTransHdr.getCompanyMst().getCurrencyName().equalsIgnoreCase(null)) {
						CurrencyMst comCurrencyop = currencyMstRepo
								.findByCurrencyName(salesTransHdr.getCompanyMst().getCurrencyName());
						if (null != salesTransHdr.getAccountHeads().getCurrencyId()) {
							Optional<CurrencyMst> custCurrencyOp = currencyMstRepo
									.findById(salesTransHdr.getAccountHeads().getCurrencyId());
							if (custCurrencyOp.isPresent()) {
								if (null != comCurrencyop) {
									if (!comCurrencyop.getId().equalsIgnoreCase(custCurrencyOp.get().getId())) {
										salesAccountingfc.execute(salesTransHdr.getVoucherNumber(),
												salesTransHdr.getVoucherDate(), salesTransHdr.getId(),
												salesTransHdr.getCompanyMst());
									}
								}
							}
						}
					}

				} catch (PartialAccountingException e) {

					logger.error(e.getMessage());
					return null;
				}

				salesDayEndReporting.execute(salesTransHdr.getVoucherNumber(), salesTransHdr.getVoucherDate(),
						salesTransHdr.getCompanyMst(), salesTransHdr.getId());


		
		}
		

		return salesInWeb;
	}

	private void createSalesDtl(SalesTransHdr salesTransHdr, ItemDtlInWeb itemDtlInWeb, SalesInWeb salesInWeb) {

		ItemMst itemMstOpt = itemMstRepository.findByItemName(itemDtlInWeb.getItemName());
		ItemMst itemMst = itemMstOpt;

		if (null == itemMst) {
			return;
		}

		SalesDtl salesDtl = new SalesDtl();
		salesDtl.setDiscount(0.0);
		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setItemName(itemMst.getItemName());
		salesDtl.setBarcode(itemMst.getBarCode());
		salesDtl.setBatch(MapleConstants.Nobatch);
		salesDtl.setQty(itemDtlInWeb.getQty());
		salesDtl.setMrp(itemDtlInWeb.getStandardPrice());

		salesDtl.setBarcode(itemMst.getBarCode());
		salesDtl.setStandardPrice(itemMst.getStandardPrice());

		salesDtl.setCompanyMst(salesTransHdr.getCompanyMst());
		
		salesDtl.setListPrice(itemMst.getStandardPrice());

		Double mrpRateIncludingTax = 0.0;

		mrpRateIncludingTax = itemDtlInWeb.getStandardPrice();

		if (null == salesTransHdr.getAccountHeads().getCustomerDiscount()) {
			salesTransHdr.getAccountHeads().setCustomerDiscount(0.0);
		}

		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double discount = 0.0;
			discount = mrpRateIncludingTax
					- (mrpRateIncludingTax * salesTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
			salesDtl.setListPrice(discount);
		}

		salesDtl.setItemId(itemMst.getId());

//		if(null != salesDtl.setBatchCode())
//
//		{
//			
//			List<ItemBatchExpiryDtl> itemBatchExpiryDtlList = Rest
//
//		ResponseEntity<List<ItemBatchExpiryDtl>> batchExpiryDtlResp = RestCaller
//				.getItemBatchExpByItemAndBatch(salesDtl.getItemId(), salesDtl.getBatch());
//
//		List<ItemBatchExpiryDtl> itemBatchExpiryDtlList = batchExpiryDtlResp
//				.getBody();if(itemBatchExpiryDtlList.size()>0&&null!=itemBatchExpiryDtlList)
//		{
//			ItemBatchExpiryDtl itemBatchExpiryDtl = itemBatchExpiryDtlList.get(0);
//
//			if (null != itemBatchExpiryDtl) {
//				String expirydate = SystemSetting.UtilDateToString(itemBatchExpiryDtl.getExpiryDate(), "yyyy-MM-dd");
//				salesDtl.setExpiryDate(java.sql.Date.valueOf(expirydate));
//			}
//		}}

//		Optional<UnitMst> unitMstResp = unitMstRepository.findById(itemMst.getUnitId());
//		UnitMst unitMst = unitMstResp.get();
//
//		salesDtl.setUnitId(unitMst.getId());
		salesDtl.setUnitId(itemMst.getUnitId());
		salesDtl.setUnitName(itemMst.getUnitId());

		salesDtl = TaxCalculation(salesDtl, salesTransHdr, itemDtlInWeb, itemMst, salesInWeb);

	}

	private SalesDtl TaxCalculation(SalesDtl salesDtl, SalesTransHdr salesTransHdr, ItemDtlInWeb itemDtlInWeb,
			ItemMst itemMst, SalesInWeb salesInWeb) {

		Double mrpRateIncludingTax = salesDtl.getStandardPrice();
		Double taxRate = itemMst.getTaxRate();

		List<TaxMst> getTaxMst = taxMstRepository.getTaxByItemId(salesDtl.getItemId());
		if (getTaxMst.size() > 0) {

			salesDtl = TaxCalculationByTaxMst(salesTransHdr, salesDtl, getTaxMst);

			Double rateBeforeTax = (100 * mrpRateIncludingTax)
					/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate()
							+ salesDtl.getSgstTaxRate() + salesDtl.getCgstTaxRate());
			salesDtl.setRate(rateBeforeTax);
			BigDecimal igstAmount = taxMstResource.taxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
			salesDtl.setIgstAmount(igstAmount.doubleValue() * salesDtl.getQty());

			BigDecimal SgstAmount = taxMstResource.taxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
			salesDtl.setSgstAmount(SgstAmount.doubleValue() * salesDtl.getQty());

			BigDecimal CgstAmount = taxMstResource.taxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
			salesDtl.setCgstAmount(CgstAmount.doubleValue() * salesDtl.getQty());

			BigDecimal cessAmount = taxMstResource.taxCalculator(salesDtl.getCessRate(), rateBeforeTax);
			salesDtl.setCessAmount(cessAmount.doubleValue() * salesDtl.getQty());

			BigDecimal addcessAmount = taxMstResource.taxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
			salesDtl.setAddCessAmount(addcessAmount.doubleValue() * salesDtl.getQty());
		}

		else {

			salesDtl = TaxCalculationWithoutTaxMst(salesDtl, salesTransHdr, itemMst, salesInWeb);
		}

		String companyState = salesTransHdr.getCompanyMst().getState();
		String customerState = "KERALA";
		try {
			customerState = salesTransHdr.getAccountHeads().getCustomerState();
		} catch (Exception e) {
			logger.info(e.toString());

		}

		if (null == customerState) {
			customerState = "KERALA";
		}

		if (null == companyState) {
			companyState = "KERALA";
		}
		String gstType ="";
		if(customerState.equalsIgnoreCase("KERALA"))
		{
			gstType="GST";
		}
		else
		{
			gstType="IGST";	
		}
		if (gstType.equalsIgnoreCase("GST")) {
			salesDtl.setSgstTaxRate(taxRate / 2);

			salesDtl.setCgstTaxRate(taxRate / 2);

			Double cgstAmt = 0.0, sgstAmt = 0.0;
			cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
			BigDecimal bdcgstAmt = new BigDecimal(cgstAmt);
			bdcgstAmt = bdcgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setCgstAmount(bdcgstAmt.doubleValue());
			sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
			BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
			bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

			salesDtl.setIgstTaxRate(0.0);
			salesDtl.setIgstAmount(0.0);

		} else {
			salesDtl.setSgstTaxRate(0.0);

			salesDtl.setCgstTaxRate(0.0);

			salesDtl.setCgstAmount(0.0);

			salesDtl.setSgstAmount(0.0);

			salesDtl.setIgstTaxRate(taxRate);
			salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		}

		BigDecimal settoamount = new BigDecimal(itemDtlInWeb.getQty() * salesDtl.getRate());

		double includingTax = (settoamount.doubleValue() * salesDtl.getTaxRate()) / 100;
		double amount = settoamount.doubleValue() + includingTax + salesDtl.getCessAmount();
		BigDecimal setamount = new BigDecimal(amount);
		setamount = setamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		salesDtl.setAmount(setamount.doubleValue());
//		}
//		else
//		{
//			BigDecimal settoamount = new BigDecimal(
//					Double.parseDouble(txtQty.getText()) *salesDtl.getMrp() );
//			settoamount = settoamount.setScale(2, BigDecimal.ROUND_CEILING);
//			salesDtl.setAmount(settoamount.doubleValue());
//		}
//		if (getUnit.getBody().getId().equalsIgnoreCase(item.getUnitId())) {
//			salesDtl.setStandardPrice(item.getStandardPrice());
//		} else {
		//
//			ResponseEntity<MultiUnitMst> multiUnitMstResp = RestCaller.getMultiUnitbyprimaryunit(item.getId(),
//					getUnit.getBody().getId());
//			MultiUnitMst multiUnitMst = multiUnitMstResp.getBody();
//			salesDtl.setStandardPrice(multiUnitMst.getPrice());
		//
//		}
		salesDtl.setAddCessRate(0.0);

		PriceDefenitionMst priceDefenitionMst1 = priceDefinitionMstRepo
				.findByCompanyMstAndPriceLevelName(salesTransHdr.getCompanyMst(), "MRP");
		if (null != priceDefenitionMst1) {
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			/*
			 * fetch for batchprice definition
			 */

			BatchPriceDefinition batchPriceDefinition = batchPriceDefinitionService
					.priceDefinitionByItemIdAndUnitAndBatch(salesDtl.getItemId(), priceDefenitionMst1.getId(),
							salesDtl.getUnitId(), salesDtl.getBatch(), salesInWeb.getLoginDate());

			if (null != batchPriceDefinition) {
				salesDtl.setMrp(batchPriceDefinition.getAmount());
			} else {

				PriceDefinition priceDefinition = priceDefinitionRepo.findByItemIdAndStartDateCostprice(
						salesDtl.getItemId(), priceDefenitionMst1.getId(), salesInWeb.getLoginDate(),
						salesDtl.getUnitId());

				if (null != priceDefinition) {
					salesDtl.setMrp(priceDefinition.getAmount());
				}
			}
		}

		PriceDefenitionMst priceDefenitionMst = priceDefinitionMstRepo
				.findByCompanyMstAndPriceLevelName(salesTransHdr.getCompanyMst(), "COST PRICE");

		if (null != priceDefenitionMst) {
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");

			BatchPriceDefinition batchPriceDefinition = batchPriceDefinitionService
					.priceDefinitionByItemIdAndUnitAndBatch(salesDtl.getItemId(), priceDefenitionMst.getId(),
							salesDtl.getUnitId(), salesDtl.getBatch(), salesInWeb.getLoginDate());

			if (null != batchPriceDefinition) {
				salesDtl.setCostPrice(batchPriceDefinition.getAmount());
			} else {

				PriceDefinition priceDefinition = priceDefinitionRepo.findByItemIdAndStartDateCostprice(
						salesDtl.getItemId(), priceDefenitionMst.getId(), salesInWeb.getLoginDate(),
						salesDtl.getUnitId());
				if (null != priceDefinition) {
					salesDtl.setCostPrice(priceDefinition.getAmount());
				}
			}
		}

		// -------------------new version 1.4

//		if (!txtWarranty.getText().trim().isEmpty()) {
//			salesDtl.setWarrantySerial(txtWarranty.getText());
//		}

		// -------------------new version 1.4 end

		// ===============added by anandu for the purpose of storewise stock
		// updation=====29-07-2021====
//		if (!txtStore.getText().trim().isEmpty()) {
//			salesDtl.setStore(txtStore.getText());
//		}
		// ==========end=================

		SalesDtl respentity = salesDetailsRepository.saveAndFlush(salesDtl);
		return respentity;

	}

	// ========================================================

	private SalesDtl TaxCalculationWithoutTaxMst(SalesDtl salesDtl, SalesTransHdr salesTransHdr, ItemMst item,
			SalesInWeb salesInWeb) {

		Double taxRate = 00.0;
		Double mrpRateIncludingTax = salesDtl.getMrp();
		// verificed

		if (salesDtl.getUnitId().equalsIgnoreCase(item.getUnitId())) {
			salesDtl.setStandardPrice(item.getStandardPrice());
		} else {

			MultiUnitMst multiUnitMst = multyMstRepository.findByCompanyMstIdAndItemIdAndUnit1(
					salesTransHdr.getCompanyMst().getId(), salesDtl.getItemId(), salesDtl.getUnitId());

			salesDtl.setStandardPrice(multiUnitMst.getPrice());

		}
		if (null != item.getTaxRate()) {
			salesDtl.setTaxRate(item.getTaxRate());
			taxRate = item.getTaxRate();

		} else {
			salesDtl.setTaxRate(0.0);
		}
		Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);

		// if Discount

		// Calculate discount on base price
		if (null != salesTransHdr.getAccountHeads().getDiscountProperty()) {

			if (salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF BASE PRICE")) {
				salesDtl = calcDiscountOnBasePrice(salesTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate,
						salesDtl);

			}
			if (salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF MRP")) {
//			salesDtl.setTaxRate(0.0);
				salesDtl = calcDiscountOnMRP(salesTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate,
						salesDtl);
			}
			if (salesTransHdr.getAccountHeads().getDiscountProperty()
					.equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX")) {
				salesDtl = ambrossiaDiscount(salesTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate,
						salesDtl);
			}
		} else {

			double cessAmount = 0.0;
			double cessRate = 0.0;
			salesDtl.setRate(rateBeforeTax);
			if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
				if (item.getCess() > 0) {
					cessRate = item.getCess();
					rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

					salesDtl.setRate(rateBeforeTax);
					cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;
				} else {
					cessAmount = 0.0;
					cessRate = 0.0;
				}
				salesDtl.setRate(rateBeforeTax);
				salesDtl.setCessRate(cessRate);
				salesDtl.setCessAmount(cessAmount);
				salesDtl.setRateBeforeDiscount(rateBeforeTax);
			}

			// salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
			double sgstTaxRate = taxRate / 2;
			double cgstTaxRate = taxRate / 2;
			salesDtl.setCgstTaxRate(cgstTaxRate);

			salesDtl.setSgstTaxRate(sgstTaxRate);
			String companyState = salesTransHdr.getCompanyMst().getState();
			String customerState = "KERALA";
			try {
				customerState = salesTransHdr.getAccountHeads().getCustomerState();
			} catch (Exception e) {
				logger.info(e.toString());

			}

			if (null == customerState) {
				customerState = "KERALA";
			}

			if (null == companyState) {
				companyState = "KERALA";
			}

			if (customerState.equalsIgnoreCase(companyState)) {
				salesDtl.setSgstTaxRate(taxRate / 2);

				salesDtl.setCgstTaxRate(taxRate / 2);

				Double cgstAmt = 0.0, sgstAmt = 0.0;
				cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
				BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
				bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				salesDtl.setCgstAmount(bdCgstAmt.doubleValue());
				sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
				BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
				bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

				salesDtl.setIgstTaxRate(0.0);
				salesDtl.setIgstAmount(0.0);

			} else {
				salesDtl.setSgstTaxRate(0.0);

				salesDtl.setCgstTaxRate(0.0);

				salesDtl.setCgstAmount(0.0);

				salesDtl.setSgstAmount(0.0);

				salesDtl.setIgstTaxRate(taxRate);
				salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

			}
		}

		return salesDtl;

	}

	private SalesDtl ambrossiaDiscount(SalesTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, Double taxRate, SalesDtl salesDtl) {

		System.out.print("mrpRateIncludingTax" + mrpRateIncludingTax);

		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			double discountAmount = (mrpRateIncludingTax * salesTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
			BigDecimal BrateAfterDiscount = new BigDecimal(mrpRateIncludingTax - discountAmount);

			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

//		 

			System.out.println("BrateAfterDiscount*****************" + BrateAfterDiscount);
			System.out.println("discountAmount*****************" + discountAmount);
//		
			salesDtl.setDiscount(discountAmount);

			salesDtl.setRateBeforeDiscount(rateBeforeTax);

			double newRate = BrateAfterDiscount.doubleValue();
			BigDecimal BnewRate = new BigDecimal(newRate);
			BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal BrateBeforeTax = new BigDecimal((newRate * 100) / (100 + taxRate));

			BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			salesDtl.setRate(BrateBeforeTax.doubleValue());

			// salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
			// salesDtl.setStandardPrice(BrateBeforeTax.doubleValue());
		} else {
			salesDtl.setRate(rateBeforeTax);
			salesDtl.setRateBeforeDiscount(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				double discountAmount = (mrpRateIncludingTax * salesTransHdr.getAccountHeads().getCustomerDiscount())
						/ 100;
				BigDecimal BrateAfterDiscount = new BigDecimal(mrpRateIncludingTax - discountAmount);

				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

//			
				salesDtl.setDiscount(discountAmount);

				double newRate = BrateAfterDiscount.doubleValue();
				BigDecimal BnewRate = new BigDecimal(newRate);
				BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				BigDecimal BrateBeforeTax = new BigDecimal((newRate * 100) / (100 + taxRate + cessRate));

				BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				salesDtl.setRate(BrateBeforeTax.doubleValue());

				// salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
				// salesDtl.setStandardPrice(BrateBeforeTax.doubleValue());

				System.out.println("rateBeforeTax---------" + BrateBeforeTax);

//				if (salesTransHdr.getCustomerMst().getCustomerDiscount() > 0) {
//					Double rateAfterDiscount = (100 * rateBeforeTax)
//							/ (100 + salesTransHdr.getCustomerMst().getCustomerDiscount());
//					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
//					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//					
//					
//					salesDtl.setRate(BrateAfterDiscount.doubleValue());
//					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
//					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//					// salesDtl.setMrp(rateBefrTax.doubleValue());
//					salesDtl.setStandardPrice(rateBefrTax.doubleValue());
//				} else {
//					salesDtl.setRate(rateBeforeTax);
//				}

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
		salesDtl.setRateBeforeDiscount(rateBeforeTax);

		return salesDtl;
	}

	private SalesDtl calcDiscountOnMRP(SalesTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate, SalesDtl salesDtl) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * mrpRateIncludingTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);

			BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BnewrateBeforeTax.doubleValue());
			salesDtl.setRateBeforeDiscount(rateBeforeTax);

		} else {
			salesDtl.setRate(rateBeforeTax);
			salesDtl.setRateBeforeDiscount(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * mrpRateIncludingTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

					BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(mrpRateIncludingTax);
					// salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesDtl.setRate(rateBeforeTax);
					salesDtl.setRateBeforeDiscount(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
		salesDtl.setRateBeforeDiscount(rateBeforeTax);

		return salesDtl;

	}

	private SalesDtl calcDiscountOnBasePrice(SalesTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate, SalesDtl salesDtl) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * rateBeforeTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BrateAfterDiscount.doubleValue());
			BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRateBeforeDiscount(rateBeforeTax);
			// salesDtl.setMrp(rateBefrTax.doubleValue());
			// salesDtl.setStandardPrice(rateBefrTax.doubleValue());
		} else {
			salesDtl.setRate(rateBeforeTax);
			salesDtl.setRateBeforeDiscount(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(rateBefrTax.doubleValue());
					// salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesDtl.setRate(rateBeforeTax);
					salesDtl.setRateBeforeDiscount(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
		salesDtl.setRateBeforeDiscount(rateBeforeTax);

		return salesDtl;

	}

	private SalesDtl TaxCalculationByTaxMst(SalesTransHdr salesTransHdr, SalesDtl salesDtl, List<TaxMst> getTaxMst) {

		for (TaxMst taxMst : getTaxMst) {

			String companyState = salesTransHdr.getCompanyMst().getState();
			String customerState = "KERALA";
			try {
				customerState = salesTransHdr.getAccountHeads().getCustomerState();

			} catch (Exception e) {
				logger.info(e.toString());

			}

			if (null == customerState) {
				customerState = "KERALA";
			}

			if (null == companyState) {
				companyState = "KERALA";
			}

			if (customerState.equalsIgnoreCase(companyState)) {
				if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
					salesDtl.setCgstTaxRate(taxMst.getTaxRate());
//					BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setCgstAmount(CgstAmount.doubleValue());
				}
				if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
					salesDtl.setSgstTaxRate(taxMst.getTaxRate());
//					BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setSgstAmount(SgstAmount.doubleValue());
				}
				salesDtl.setIgstTaxRate(0.0);
				salesDtl.setIgstAmount(0.0);

				TaxMst taxMst1 = taxMstRepository.getTaxByItemIdAndTaxId(salesDtl.getItemId(), "IGST");
				if (null != taxMst1) {

					salesDtl.setTaxRate(taxMst1.getTaxRate());
				}

			} else {
				if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
					salesDtl.setCgstTaxRate(0.0);
					salesDtl.setCgstAmount(0.0);
					salesDtl.setSgstTaxRate(0.0);
					salesDtl.setSgstAmount(0.0);

					salesDtl.setTaxRate(taxMst.getTaxRate());
					salesDtl.setIgstTaxRate(taxMst.getTaxRate());
//						BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setIgstAmount(igstAmount.doubleValue());
				}
			}
			if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
				if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
					salesDtl.setCessRate(taxMst.getTaxRate());
//					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setCessAmount(cessAmount.doubleValue());
				}
			}
			if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
				salesDtl.setAddCessRate(taxMst.getTaxRate());
//				BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(),
//						Double.valueOf(txtRate.getText()));
//				salesDtl.setAddCessAmount(cessAmount.doubleValue());
			}

		}

		return salesDtl;
	}

	private SalesTransHdr createSalesTransHdr(AccountHeads cust, SalesInWeb salesInWeb, String companymstid) {

		SalesTransHdr salesTransHdr = new SalesTransHdr();
		salesTransHdr.setInvoiceAmount(0.0);
		salesTransHdr.getId();
		Optional<CompanyMst> companyMst=companyMstRepository.findById(companymstid);
		salesTransHdr.setCompanyMst(companyMst.get());
		salesTransHdr.setVoucherType("Wholesale");
		salesTransHdr.setCustomerId(cust.getId());

		Optional<AccountHeads> customerMstOpt = accountHeadsRepository.findById(cust.getId());

		AccountHeads customerMst = customerMstOpt.get();

		if (null == customerMst) {
			return null;
		}
		salesTransHdr.setAccountHeads(customerMst);

		if (null != customerMst.getCustomerDiscount()) {
			salesTransHdr.setDiscount((customerMst.getCustomerDiscount().toString()));
		} else {
			salesTransHdr.setDiscount("0");
		}

		if (null == customerMst.getPartyGst() || customerMst.getPartyGst().length() < 13) {
			salesTransHdr.setSalesMode("B2C");
		} else {
			salesTransHdr.setSalesMode("B2B");
		}

		salesTransHdr.setCreditOrCash("CREDIT");
		salesTransHdr.setCustomiseSalesMode("WHOLESALEWINDOW");
//		salesTransHdr.setUserId();
		salesTransHdr.setBranchCode(salesInWeb.getBranch());
//		Date date = SystemSetting.applicationDate;
		salesTransHdr.setVoucherDate(salesInWeb.getLoginDate());

		boolean customerIsBranch = false;

		Optional<BranchMst> branchMstOpt = branchMstRepository.findById(customerMst.getId());
		if (branchMstOpt.isPresent()) {
			customerIsBranch = true;
		}

		if (customerIsBranch) {
			salesTransHdr.setIsBranchSales("Y");
		} else {
			salesTransHdr.setIsBranchSales("N");
		}

//		if (!txtCreditPeriod.getText().trim().isEmpty()) {
//			salesTransHdr.setCreditPeriod(Integer.parseInt(txtCreditPeriod.getText()));
//		}

//		if (null != cmbSalesMan.getSelectionModel().getSelectedItem()) {
//			ResponseEntity<List<SalesManMst>> salesManMstResp = RestCaller
//					.getSalesManMstByName(cmbSalesMan.getSelectionModel().getSelectedItem().toString());
//
//			List<SalesManMst> salesManList = salesManMstResp.getBody();
//
//			if (salesManList.size() > 0) {
//				if (null != salesManList.get(0)) {
//					if (null != salesManList.get(0).getId()) {
//						salesTransHdr.setSalesManId(salesManList.get(0).getId());
//
//					} else {
//						notifyMessage(3, "Invalid salesman...!");
//						return;
//					}
//
//				} else {
//					notifyMessage(3, "Invalid salesman...!");
//					return;
//				}
//
//			} else {
//				notifyMessage(3, "Invalid salesman...!");
//				return;
//
//			}
//		}

//		ResponseEntity<SalesTransHdr> getsales = RestCaller
//				.getNullSalesTransHdrByCustomer(salesTransHdr.getCustomerId(), sdate, "WHOLESALEWINDOW");
//		if (null != getsales.getBody()) {
//			salesTransHdr = getsales.getBody();
//		} else {
		SalesTransHdr respentity = salesTransHdrRepository.saveAndFlush(salesTransHdr);
		logger.info("Whole Sale save Sales trans Hdr completed!!");
//			salesTransHdr = respentity.getBody();
//		}

		return respentity;
	}

	
	
	/*
	 * used to set the values to new final entity "SalesBillModelClass" for POS and wholesale final submit
	 */
	@Override
	public SalesBillModelClass setValuesOnSalesBillModalClass(SalesTransHdr salesTransHdr) {
		
		SalesBillModelClass salesBillModelClass=new SalesBillModelClass();
		
		salesBillModelClass.setCompanyMst(salesTransHdr.getCompanyMst());
		
		salesBillModelClass.setSalesTransHdr(salesTransHdr);
		
		Double totalSalesAmount=0.0;
		Double taxAmount = 0.0;
		
		List<SalesDtl> salesDtlList = salesDetailsRepository.findBySalesTransHdr(salesTransHdr);
		if(salesDtlList.size()>0) {
			for(SalesDtl salesdtl : salesDtlList) {
				
				totalSalesAmount = totalSalesAmount + salesdtl.getAmount();
				
				if(null != salesdtl.getRate() && null != salesdtl.getQty() && null != salesdtl.getTaxRate())
				{
					Double tax = (salesdtl.getRate()*salesdtl.getQty()*salesdtl.getTaxRate())/100;
					taxAmount = taxAmount + tax;
				}
			}
		}
		
		salesBillModelClass.setTaxAmount(taxAmount);
		
		salesBillModelClass.setTotalAmount(totalSalesAmount);
		
		Double balanceAmount = 0.0;
		if(null == salesTransHdr.getAmountTendered()) {
			balanceAmount = 0.0;
		}else {
		 balanceAmount = salesTransHdr.getAmountTendered() - totalSalesAmount;
		}
		salesBillModelClass.setBalanceAmount(balanceAmount);
		
		ArrayList<SalesDtl> salesDtlArray = new ArrayList<SalesDtl>();
		salesDtlArray.addAll(salesDtlList);
		salesBillModelClass.setArrayOfSalesDtl(salesDtlArray);
		
		List<SalesReceipts> salesReceiptsList = salesReceiptsRepo.findBySalesTransHdr(salesTransHdr);
		ArrayList<SalesReceipts> salesReceiptsArray = new ArrayList<SalesReceipts>();
		salesReceiptsArray.addAll(salesReceiptsList);
		salesBillModelClass.setArrayOfSalesReceipts(salesReceiptsArray);
		
		BranchMst branch = branchMstRepository.findByBranchName(salesTransHdr.getBranchCode());
		salesBillModelClass.setBranchMst(branch);
		
		return salesBillModelClass;
		
		
	}
//............SalesTransHdrCreation For Online sales..//
	@Override
	public SalesTransHdr createSalesTransHdr(CompanyMst companyMst, String myBranch,String useId,String salesType, 
			String customerId, String salesmode, String localCustomerId, String salesMan) {
		
		boolean customerIsBranch = false;
		
		SalesTransHdr salesTransHdr=new SalesTransHdr();
	    Date date=new Date();
	    String newsalesmode=salesmode+"_"+myBranch;
	   Optional<AccountHeads> customerMst= accountHeadsRepository.findByAccountNameAndCompanyMstIdAndBranchCode(newsalesmode, companyMst.getId(),
				"%" + companyMst.getId() + myBranch + "%");
	   
		//salesTransHdr.setUpdatedTime(SystemSetting.systemDate);
		salesTransHdr.setAccountHeads(customerMst.get());
		salesTransHdr.setCompanyMst(companyMst);
		salesTransHdr.setInvoiceAmount(0.0);
		salesTransHdr.setUserId(useId);
		salesTransHdr.setBranchCode(myBranch);
		salesTransHdr.getId();
		salesTransHdr.setCustomerId(customerMst.get().getId());
		salesTransHdr.setSalesMode(newsalesmode);
		if (salesmode.equalsIgnoreCase("POS"))
		{
		salesTransHdr.setVoucherType("SALESTYPE");
		salesTransHdr.setCreditOrCash("CASH");
		salesTransHdr.setIsBranchSales("N");
		//salesTransHdr.setVoucherDate(SystemSetting.getApplicationDate());
		
		salesTransHdr=salesTransHdrRepository.save(salesTransHdr);
		}
		else 
		{
			if (null != customerMst.get().getCustomerDiscount()) {
				salesTransHdr.setDiscount((customerMst.get().getCustomerDiscount().toString()));
			} else {
				salesTransHdr.setDiscount("0");
			}


			if (localCustomerId.isEmpty()) {
				Optional<LocalCustomerMst> LocalCustomerMstResp = localCustomerRepository.findById(localCustomerId);
				LocalCustomerMst localCustomerMst = LocalCustomerMstResp.get();
				if (null != localCustomerMst) {
					salesTransHdr.setLocalCustomerMst(localCustomerMst);
				}
	
			}


			if (null == customerMst.get().getPartyGst() || customerMst.get().getPartyGst().length() < 13) {
				salesTransHdr.setSalesMode("B2C");
			} else {
				salesTransHdr.setSalesMode("B2B");
			}


			salesTransHdr.setCreditOrCash("CREDIT");
			salesTransHdr.setCustomiseSalesMode("WHOLESALEWINDOW");


			Date date1 = new Date();
			salesTransHdr.setVoucherDate(date1);
			
			
			Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companyMst.getId());
			BranchMst branchMstResp = branchMstRepo.findByBranchNameAndCompanyMst(customerMst.get().getAccountName(),companyMstOpt);

			if (null != branchMstResp) {
				customerIsBranch = true;
			}
			if (customerIsBranch) {
				salesTransHdr.setIsBranchSales("Y");
			} else {
				salesTransHdr.setIsBranchSales("N");
			}


			String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");

			if (null != customerMst.get().getCreditPeriod()) {
				salesTransHdr.setCreditPeriod(customerMst.get().getCreditPeriod());
			}

			if ( salesMan.isEmpty()) {
				List<SalesManMst> salesManList = salesManMstRepository.findBySalesManName(salesMan)	;
				if (salesManList.size() > 0) {
					if (null != salesManList.get(0)) {
						if (null != salesManList.get(0).getId()) {
							salesTransHdr.setSalesManId(salesManList.get(0).getId());
	
						} else {
							return null ;
						}
	
					} else {
						return null;
					}
	
				} else {
					return null;
	
				}
			}

//			ResponseEntity<SalesTransHdr> getsales = RestCaller
//					.getNullSalesTransHdrByCustomer(salesTransHdr.getCustomerId(), sdate, "WHOLESALEWINDOW");
//			salesTransHdrRepository.getSalesTransHdrBycustomerAndVoucherNull(salesTransHdr.getCustomerId(), sdate, companyMst,
//					"WHOLESALEWINDOW");
//			if (null != getsales.getBody()) {
//				salesTransHdr = getsales.getBody();
//			} else {
				 salesTransHdr = salesTransHdrRepository.save(salesTransHdr);
				logger.info("Whole Sale save Sales trans Hdr completed!!");
				
//			}
		}
		return salesTransHdr;
	}


	


}
