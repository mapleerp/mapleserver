package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.InventoryReorderStatusReport;

@Service

@Component
public interface InventoryReorderstatusReportService {

	List<InventoryReorderStatusReport> getInventoryReorderStatusReport(String startDate,String endDate,String branchCode,String companymstid);
	
	
}
