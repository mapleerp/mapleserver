package com.maple.restserver.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MachineResourceMst;
import com.maple.restserver.entity.MixCatItemLinkMst;
import com.maple.restserver.entity.MixCategoryMst;
import com.maple.restserver.entity.ProductionPreplanningDtl;
import com.maple.restserver.entity.ProductionPreplanningMst;
import com.maple.restserver.entity.SaleOrderProductionPlaningLinkMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.RemainingEntity;
import com.maple.restserver.report.entity.SaleOrderProductDtls;
import com.maple.restserver.report.entity.StockReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.MachineResourceMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.ProductionDetailRepository;
import com.maple.restserver.repository.ProductionDtlDtlRepository;
import com.maple.restserver.repository.ProductionMstRepository;
import com.maple.restserver.repository.ProductionPreplanningDtlRepository;
import com.maple.restserver.repository.ProductionPreplanningRepository;
import com.maple.restserver.repository.ResourceCatItemLinkRepository;
import com.maple.restserver.repository.SaleOrderProductionPlaningLinkMstRepository;
import com.maple.restserver.repository.SubCategoryMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
public class MachineResourceServiceImpl implements MachineResourceService {

	List<Object> objcts = new ArrayList<Object>();
	@Autowired
	ProductionPreplanningDtlRepository productionPreplanningDtlRepo;
	@Autowired
	ProductionPreplanningRepository productionPreplanningMstRepo;
	@Autowired
	SubCategoryMstRepository mixCategoryMstRepository;

	@Autowired
	MultiUnitConversionServiceImpl multiUnitConversionServiceImpl;

	@Autowired
	ResourceCatItemLinkRepository resourceCatItemLinkRepository;
	@Autowired
	MachineResourceMstRepository machineResourceMstRepository;

	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepository;

	@Autowired
	ProductionDetailRepository productionDetailRepository;

	@Autowired
	ProductionDtlDtlRepository productionDtlDtlRepository;
	@Autowired
	ProductionMstRepository productionMstRepository;
	@Autowired
	MultiUnitMstRepository multiUnitMstRepository;
	@Autowired
	ItemBatchMstRepository itemBatchMstRepository;
	
	@Autowired
	ProductionPreplanningDtlRepository productionPreplanningDtlRepository;

	@Autowired
	SaleOrderProductionPlaningLinkMstRepository saleOrderProductionPlaningLinkMstRepository;
	@Autowired
	KitDefenitionDtlRepository kitDefenitionDtlRepository;
	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	VoucherNumberService voucherNumberService;
	boolean recurssionOff = false;

	// REGY - This has to be moved to function and rename to more meaningful.
	boolean machineStatus = true;

	private String resourceCatId;

	@Override
	public List<MachineResourceMst> machineDetails() {
		List<MachineResourceMst> MachineResourceMstList = new ArrayList<MachineResourceMst>();
		List<Object> obj = machineResourceMstRepository.findMachineDeltails();

		for (int i = 0; i < obj.size(); i++) {
			MachineResourceMst machineResourceMst = new MachineResourceMst();
			Object[] objAray = (Object[]) obj.get(i);
			machineResourceMst.setMachineResourceName((String) objAray[0]);
			machineResourceMst.setCapcity((Double) objAray[1]);
			machineResourceMst.setUnitId((String) objAray[2]);
			MachineResourceMstList.add(machineResourceMst);
		}
		return MachineResourceMstList;
	}

	@Override
	public List<MixCategoryMst> retriveResourceCategory() {
		List<MixCategoryMst> subCategoryMstList = new ArrayList<MixCategoryMst>();
		List<Object> obj = mixCategoryMstRepository.fetchResourceCategory();

		for (int i = 0; i < obj.size(); i++) {
			MixCategoryMst resourceCateMst = new MixCategoryMst();
			Object[] objAray = (Object[]) obj.get(i);
			resourceCateMst.setMixName((String) objAray[0]);
			resourceCateMst.setMechineId((String) objAray[1]);
			subCategoryMstList.add(resourceCateMst);
		}
		return subCategoryMstList;
	}

	@Override
	public List<MixCatItemLinkMst> retrieveResourceCatItemLinkMst() {

		List<MixCatItemLinkMst> resourceCatItemLinkMstList = new ArrayList<MixCatItemLinkMst>();
		List<Object> obj = resourceCatItemLinkRepository.retrieveResourceCatItemLinkMst();

		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			MixCatItemLinkMst resourceCatItemLinkMst = new MixCatItemLinkMst();
			resourceCatItemLinkMst.setItemId((String) objAray[0]);
			resourceCatItemLinkMst.setResourceCatId((String) objAray[1]);
			resourceCatItemLinkMstList.add(resourceCatItemLinkMst);
		}
		return resourceCatItemLinkMstList;
	}

	/************************************
	 * SATHYAJITH*************************************************************** 
	 * 1.
	 * Function to return SO Qty, Stock Qty, Total rawmaterial Weight(RMW) , ItemId,
	 * Mix Name Order by Mix Name As FNPrePlanning(SO Date)
	 * 
	 * FNPrePlanning(SO Date){
	 * 
	 * Fetch All Item from SO order by mix name (Join SQ and Mix Table)
	 * 
	 * 
	 * Get list of Rawmaterials for the item loop through RW and get wt of each
	 * Convert wt into machine unit if no conversion method found, Throw error ,
	 * display error message and exit function. Get sum of wt Find Current Stock of
	 * Item.
	 * 
	 * Return SO Qty, Current Stock, Total rawmaterial Weight, ItemId, Mix Name
	 * 
	 * 
	 * }
	 * 
	 *************************************** SAARI************************************************************
	 * 
	 * 2. Loop through each record of point 1 (FNPrePlanning) 
	 * Declare a Var PrevMix
	 * before starting the loop 
	 * RequiredQty = SO Qty - Stock Qty 
	 * If Req.Qty < = 0
	 * Then continue End If Current Mix = Record.Mix Find machineCapacity from mix
	 * Name
	 * 
	 * if (PrevMix != CurrentMix){ Initial batch = null 
	 * MachineRemaining = MachineCapacity 
	 * RemainingEnitity = StartBatchFunction(ReqQty,ItemId,MachineCapacity, RMW, Initialbatch) end if
	 * PrevMix=CurrentMix if RemainingEnitity.MachineRemaining =0 then Initial batch
	 * = null MachineRemaining = MachineCapacity
	 * 
	 * end if
	 * 
	 * While RemainingEnitity.RMW>0{
	 *  RemainingEnitity =
	 * StartBatchFunction(ReqQty,ItemId,MachineRemaining, RMW, Initialbatch) if
	 * RemainingEnitity.MachineRemaining =0 then MachineRemaining = MachineCapacity
	 * Initial batch = null end if 
	 * }
	 * 
	 * 
	 ***************************************** SOORYA***********************************************************
	 * 
	 * 3. StartBatchFunction(ReqQty,ItemId,MachineRemainingCapacty, RMW,
	 * Initialbatch ){
	 * 
	 * 
	 * 
	 * 
	 * if RMW <= MachineRemainingCapacty then { if(null==Initialbatch){
	 * 
	 * batchNumber= voucher Gen.Batch BatchNumber = BatchNumber + Date }
	 * 
	 * Save to pre planning Save to sale-order-preplanning-link table
	 * 
	 * ReqQty = 0 MachineRemainingCapacty = MachineRemainingCapacty - RMW Return
	 * (ReqQty,ItemId,MachineRemainingCapacty, RMW, batch) } else{ 1. find unit
	 * weight of product in machine unit. =>Function to Find
	 * MachineUnitValueOfSingleItem(ItemId, MachineUnit)
	 * 
	 * 2. Find max QTY (NOS) that can fit into MachineRemainingCapacty =>
	 * getMaximumQtyOfProductForMachineRemainingCapacity(double
	 * machineRemainingCapacity, double unitQtyOfItemInMacUnit)
	 * 
	 * 3. Find Weight of max QTY (In machine unit) =>
	 * getWeightOfRawMaterialForItemQty(qty, itemid, machineunit)
	 * 
	 * 
	 * if(null==Initialbatch){
	 * 
	 * batchNumber= voucher Gen.Batch BatchNumber = BatchNumber + Date } Save to pre
	 * planning Save to sale-order-preplanning-link table
	 * 
	 * RMW = RMW - Weight of max QTY ReqQty = ReqQty - max QTY (NOS)
	 * MachineRemainingCapacty = 0 Return (ReqQty,ItemId,MachineRemainingCapacty,
	 * RMW,batch) end if
	 * 
	 * }
	 * 
	 * 
	 ****************************************** SATHYAJITH*************************************************************
	 * Function to Find weight (Machine Unit) of a single item
	 *
	 * getMachineUnitValueOfSingleItem(ItemId, MachineUnit) Get raw material of item
	 * ID convert each raw material qty into machine unit If could not convert ,
	 * throw error Sum up all qty and return Return double value
	 *
	 * 
	 ************************************************* SATHYAJITH****************************************************************
	 * Function to find Maximum Qty of product that can fit in machine capacity
	 *
	 * getMaximumQtyOfProductForMachineRemainingCapacity(double
	 * machineRemainingCapacity, double unitQtyOfItemInMacUnit)
	 *
	 * BigDecimal bdMaxQty = new
	 * BigDecimal(machineRemainingCapacity/unitQtyOfItemInMacUnit) bdMaxQty
	 * =bdMaxQty.setScale(0, BigDecimal.ROUND_DOWN); return bdMaxQty
	 *
	 *
	 * 
	 ************************************************** SATHYAJITH***************************************************************
	 * Function to find the weight of Item Quantity
	 * 
	 * getWeightOfRawMaterialForItemQty(ItemId, Qty, MachineUnit){
	 * 
	 * double singeleWeight = getMachineUnitValueOfSingleItem(ItemId, MachineUnit)
	 * 
	 * return singeleWeight * Qty
	 * 
	 * }
	 * 
	 * 
	 ******************************************************************************************************************/

	
	
	@Transactional
	@Override
	public void preplanningBatchInitializeStage2(String branchcode, String companymstid, Date fromDate,Date toDate)throws  ResourceNotFoundException{
		
		List<SaleOrderProductDtls> saleOrderProductDtlList = fetchSaleOrderProductQty(branchcode, companymstid,
				fromDate,toDate);


		System.out.print(saleOrderProductDtlList.size()+"size of sale order list isssssssssssssssssss");
		
		System.out.print(toDate+"to date inside service %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		System.out.print(saleOrderProductDtlList.size()
				+ "saleOrderProductDtlList issssssssssssssssssssssssssssssssssssssssssssssssss");
		ProductionPreplanningMst productionPreplanningMst = new ProductionPreplanningMst();
		productionPreplanningMst.setBranchCode(branchcode);
		CompanyMst companyMst = companyMstRepository.findById(companymstid).get();
		productionPreplanningMst.setCompanyMst(companyMst);
		productionPreplanningMst.setVoucherDate(fromDate);
		VoucherNumber vno = voucherNumberService.generateInvoice("PRDPRE", companymstid);
		productionPreplanningMst.setVoucherNumber(vno.getCode());
		productionPreplanningMst = productionPreplanningMstRepo.save(productionPreplanningMst);
		String prevMix = "";

		RemainingEntity remainingEntity = new RemainingEntity();
		
	//	Double machineRemaining = 0.0;

		for (int i = 0; i < saleOrderProductDtlList.size(); i++) {
			
			SaleOrderProductionPlaningLinkMst saleOrderProductionPlaningLinkMst= new SaleOrderProductionPlaningLinkMst();
			saleOrderProductionPlaningLinkMst.setProductionMstId(productionPreplanningMst.getId());
			saleOrderProductionPlaningLinkMst.setSaleOrderHdrId(saleOrderProductDtlList.get(i).getSaleOrderTranHdrId());
			saleOrderProductionPlaningLinkMst.setVoucherDate(fromDate);
			saleOrderProductionPlaningLinkMst.setCompanyMst(companyMst);
			saleOrderProductionPlaningLinkMstRepository.save(saleOrderProductionPlaningLinkMst);
			
			remainingEntity.setRMW(saleOrderProductDtlList.get(i).getTotalRawMaterialWeight());

			Double reqQty = saleOrderProductDtlList.get(i).getSaleOrderQty()
					- saleOrderProductDtlList.get(i).getCurrentStock();
			remainingEntity.setReqQty(reqQty);

			if (reqQty <= 0) {
				continue;
			}
			String currentMix = saleOrderProductDtlList.get(i).getMixName();
			Double machineCapacity = mixCategoryMstRepository.findMachineCapacity(currentMix);
			String batch = null;
			String machineUnitId = mixCategoryMstRepository.findMachineUnitId(currentMix);

			if (!prevMix.equalsIgnoreCase(currentMix)) {

				remainingEntity = StartBatchFunction(remainingEntity.getReqQty(),
						saleOrderProductDtlList.get(i).getItemId(), machineCapacity, remainingEntity.getRMW(), batch,
						companymstid, machineUnitId, productionPreplanningMst);
			}
			
			
			prevMix = currentMix;
			
			
			System.out.print(remainingEntity.getBatch() + "voucher number ");
			batch = remainingEntity.getBatch();
			
			

			if (remainingEntity.getMachineRemainingCapacity() == 0) {
				remainingEntity.setMachineRemainingCapacity(machineCapacity)  ;
				batch = null;
			}
			
		
		
		while (remainingEntity.getMachineRemainingCapacity() > 0 && remainingEntity.getReqQty() > 0) {
			
			
			System.out.print("inside while loop ");
				remainingEntity = StartBatchFunction(remainingEntity.getReqQty(),
						saleOrderProductDtlList.get(i).getItemId(), remainingEntity.getMachineRemainingCapacity(), remainingEntity.getRMW(), batch,
						companymstid, machineUnitId, productionPreplanningMst);
				if (remainingEntity.getMachineRemainingCapacity() == 0) {
					remainingEntity.setMachineRemainingCapacity(machineCapacity);
					batch = null;

				}
			}

		}
	}

	public RemainingEntity StartBatchFunction(Double reqQty, String itemId, Double machineRemainingCapacity, Double RMW,
			String initialBatch, String companymstid, String machineUnitId,
			ProductionPreplanningMst productionPreplanningMst) {
		if (RMW <= machineRemainingCapacity) {

			System.out.print("inside RMW <= machineRemainingCapacity ");
			if (null == initialBatch) {

				String vDate = ClientSystemSetting.UtilDateToString(SystemSetting.getSystemDate());
				VoucherNumber batchNum = voucherNumberService.generateInvoice(vDate, companymstid);

				System.out.print("generate batch number " + batchNum.getCode());
				String batchNumber = batchNum.getCode();

				String vdate = SystemSetting.UtilDateToString(SystemSetting.getSystemDate());
				batchNumber = batchNumber + vdate;
				initialBatch = batchNumber;
				System.out.print(initialBatch + "inital batch number issssssssssssssssssssssssssssssssssssssssssssss");
			}

			ProductionPreplanningDtl productionPreplanningDtl = new ProductionPreplanningDtl();
			productionPreplanningDtl.setBatch(initialBatch);
			productionPreplanningDtl.setItemId(itemId);
			productionPreplanningDtl.setProductionPreplanningMst(productionPreplanningMst);
			productionPreplanningDtl.setQty(reqQty);
			productionPreplanningDtl.setStatus("PREPLANNING");
			productionPreplanningDtlRepo.save(productionPreplanningDtl);
			RemainingEntity remainingEntity = new RemainingEntity();
			remainingEntity.setBatch(initialBatch);
			System.out.print(
					remainingEntity.getBatch() + "last batch number ");
			remainingEntity.setItemId(itemId);

			machineRemainingCapacity = machineRemainingCapacity - RMW;

			remainingEntity.setMachineRemainingCapacity(machineRemainingCapacity);
			
			System.out.print(remainingEntity.getMachineRemainingCapacity()+"machine remaining qty in entity ");

			RMW = 0.0;
			reqQty = 0.0;
			System.out.print("**************");
			remainingEntity.setReqQty(reqQty);
			System.out.print(remainingEntity.getReqQty()+" :require qty ");
			remainingEntity.setRMW(RMW);

			return remainingEntity;
		}else {

			System.out.print(
					"inside else ");
			Double weightOfItemSingleUnit = findWeightOfSingleItemInMachineUnit(companymstid, itemId, machineUnitId);
			Double maxQty = getMaximumQtyOfProductForMachineRemainingCapacity(machineRemainingCapacity,
					weightOfItemSingleUnit);
			Double maxQtyInMachineUnit = getWeightOfRawMaterialForItemQty(maxQty, itemId, machineUnitId, companymstid);
			if (null == initialBatch) {
				String vDate = ClientSystemSetting.UtilDateToString(SystemSetting.getSystemDate());
				VoucherNumber batchNum = voucherNumberService.generateInvoice(vDate, companymstid);

				System.out.print("generate batch number " + batchNum.getCode());
				String batchNumber = batchNum.getCode();

				String vdate = SystemSetting.UtilDateToString(SystemSetting.getSystemDate());
				batchNumber = batchNumber + vdate;
				initialBatch = batchNumber;
			}

			ProductionPreplanningDtl productionPreplanningDtl = new ProductionPreplanningDtl();
			productionPreplanningDtl.setBatch(initialBatch);
			productionPreplanningDtl.setItemId(itemId);
			productionPreplanningDtl.setProductionPreplanningMst(productionPreplanningMst);
			productionPreplanningDtl.setQty(maxQty);
			productionPreplanningDtl.setStatus("PREPLANNING");
			productionPreplanningDtlRepo.save(productionPreplanningDtl);

			System.out.print(
					"inside else ssss");
			RMW = RMW - maxQtyInMachineUnit;
			reqQty = reqQty - maxQty;
			machineRemainingCapacity = 0.0;
			RemainingEntity remainingEntity = new RemainingEntity();
			remainingEntity.setBatch(initialBatch);

			remainingEntity.setItemId(itemId);
			remainingEntity.setMachineRemainingCapacity(machineRemainingCapacity);
			remainingEntity.setReqQty(reqQty);
			System.out.print(remainingEntity.getReqQty()
					+ "required qty");
			remainingEntity.setRMW(RMW);
			return remainingEntity;
		}

	}

	private Double getWeightOfRawMaterialForItemQty(Double maxQty, String itemId, String machineUnitId,
			String companymstid) {

		Double singeleWeight = findWeightOfSingleItemInMachineUnit(companymstid, itemId, machineUnitId);
		return singeleWeight * maxQty;
	}

	private Double getMaximumQtyOfProductForMachineRemainingCapacity(Double machineRemainingCapacity,
			Double weightOfItemSingleUnit) {

		BigDecimal bdMaxQty = new BigDecimal(machineRemainingCapacity / weightOfItemSingleUnit);
		bdMaxQty = bdMaxQty.setScale(0, BigDecimal.ROUND_DOWN);
		return bdMaxQty.doubleValue();

	}

	
	public List<SaleOrderProductDtls> fetchSaleOrderProductQty(String branchCode, String companymstid,
			java.util.Date fromDate,java.util.Date tDate) throws ResourceNotFoundException {
		
		System.out.print(tDate+"todate ");

		List<SaleOrderProductDtls> saleOrderProductDtlList = new ArrayList<SaleOrderProductDtls>();

		Map<String, Double> map = new HashMap<String, Double>();
		objcts = machineResourceMstRepository.fetchSaleOrderItemQty(companymstid,fromDate,tDate);
		
	
		Integer countOfSaleOrder = machineResourceMstRepository.fetchCountOfSaleOrder(companymstid, fromDate, tDate);

		System.out.print(countOfSaleOrder + "count of sale ofder ");

		Integer countOfSaleOrderWithMixCategory = machineResourceMstRepository
				.fetchCountOfSaleOrderMixCategory(companymstid,fromDate,tDate);
		// ,fromDate

		System.out.print(countOfSaleOrderWithMixCategory + "countOfSaleOrderWithMixCategory");
		if (countOfSaleOrder == countOfSaleOrderWithMixCategory) {

			for (int i = 0; i < objcts.size(); i++) {

				Object[] objAray = (Object[]) objcts.get(i);

				String productId = (String) objAray[0];
				Double productQty = (Double) objAray[1];
				 String saleOrderTranshdrId=((String) objAray[4]);
				Double itemCurrentStock = 0.0;
				System.out.print(
						fromDate + "from date " + "product id   " + productId);
				Double itemStock = itemBatchMstRepository.getItemWiseStock(fromDate, productId);
				List<Object> obj = itemBatchMstRepository.getItemWiseStockReport(branchCode, companymstid, fromDate,
						productId);
				System.out.print(obj.size() + "list size ");
				StockReport stockReport = new StockReport();
				for (int a = 0; a < obj.size(); a++)

				{
					Object[] objctAray = (Object[]) obj.get(a);

					stockReport.setItemName((String) objctAray[0]);
					stockReport.setQty((Double) objctAray[1]);

					System.out
							.println(stockReport.getQty() + "stock report qty ");
				}
				itemCurrentStock = stockReport.getQty();

				System.out.print(itemCurrentStock + "item current stock ");

				List<MixCatItemLinkMst> ResourceCatItemLinkMstList = resourceCatItemLinkRepository
						.findByItemId(productId);

				Optional<MixCategoryMst> mixCategoryMstOpt = mixCategoryMstRepository
						.findById(ResourceCatItemLinkMstList.get(0).getResourceCatId());

				Optional<MachineResourceMst> MachineResourceMstOpt = machineResourceMstRepository
						.findById(mixCategoryMstOpt.get().getMechineId()); 
				if(null==MachineResourceMstOpt) {

					  throw new ResourceNotFoundException("Please Add Machine Resource detail");
				}
				String resourceSubCatId = ResourceCatItemLinkMstList.get(0).getResourceCatId();
				String machineUnitId = MachineResourceMstOpt.get().getUnitId();
				Double recipeQty = 0.0;
				
				map = recipeConvertToMachineUnit(companymstid, productId, machineUnitId, productQty);

				Double qty = map.get(productId);

				SaleOrderProductDtls saleOrderProductDtls = new SaleOrderProductDtls();

				saleOrderProductDtls.setItemId(productId);

				// saleOrderItemQty.setQtyInMachUnit(map.get("RecipeQtyInMachineUnit"));
				saleOrderProductDtls.setTotalRawMaterialWeight(map.get("RecipeQtyInMachineUnit"));

				if (null == itemCurrentStock) {
					saleOrderProductDtls.setCurrentStock(0.0);
				} else {
					saleOrderProductDtls.setCurrentStock(itemCurrentStock);
				}
			
			
				saleOrderProductDtls.setSaleOrderQty(productQty);
				saleOrderProductDtls.setMixName(mixCategoryMstOpt.get().getMixName());
				saleOrderProductDtls.setSaleOrderTranHdrId(saleOrderTranshdrId);
				Double qtyOfItemForSingleUnit = map.get(productId);

				saleOrderProductDtlList.add(saleOrderProductDtls);

			}

			return saleOrderProductDtlList;
		} else {
			
			  throw new ResourceNotFoundException("Please ensure MixCataegory is added Properly ");

		}

	}

	// -----------------------------------fetch item,qty from saleorder
	// ends-----------------------------------------------

	// -----------------------------------------------recipe conversion to machine
	// unit starts----------------------------------------

	public Map<String, Double> recipeConvertToMachineUnit(String companyMstId, String productId, String machineUnitId,
			Double productQty) throws ResourceNotFoundException {

		Map<String, Double> map = new HashMap<String, Double>();
		Double recipeQtyInMachUnit = 0.0;
		Double recipeSingleQtyInMachUnit = 0.0;
		Double minumQty = 0.0;
		Double rawMaterialForAprod = 0.0;
		List<Object> obj = kitDefinitionMstRepository.fetchKitDtls(productId);

		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);

			minumQty = (Double) objAray[0];
			System.out.print(minumQty + "minimum qty isssssssssssssssssssssssssssssssss");

			// String productUnitId=(String)objAray[1];

			String rawItemId = (String) objAray[1];
			String rawMateUnitId = (String) objAray[2];

			Double rawmatQty = (Double) objAray[3];

			
			double rawMateToMachine = multiUnitConversionServiceImpl.getConvertionQty(companyMstId, rawItemId,
					rawMateUnitId, machineUnitId, rawmatQty);
			
		 
			 

			rawMaterialForAprod = rawMateToMachine + rawMaterialForAprod;

		}
		recipeSingleQtyInMachUnit = rawMaterialForAprod / minumQty;

		recipeQtyInMachUnit = recipeSingleQtyInMachUnit * productQty;

		System.out.print(recipeSingleQtyInMachUnit + "sor single unit");

		System.out.print(recipeQtyInMachUnit + "recipe in machine unit ");
		map.put("RecipeQtyInMachineUnit", recipeQtyInMachUnit);

		map.put(productId, recipeSingleQtyInMachUnit);
		Double qty = map.get(productId);

		return map;

	}

	public Double findWeightOfSingleItemInMachineUnit(String companyMstId, String productId, String machineUnitId) {

		Double recipeSingleQtyInMachUnit = 0.0;
		Double minumQty = 0.0;
		Double rawMaterialForAprod = 0.0;
		List<Object> obj = kitDefinitionMstRepository.fetchKitDtls(productId);

		for (int i = 0; i < obj.size(); i++) {
			try {

				Object[] objAray = (Object[]) obj.get(i);

				minumQty = (Double) objAray[0];

				// String productUnitId=(String)objAray[1];

				String rawItemId = (String) objAray[1];
				String rawMateUnitId = (String) objAray[2];

				Double rawmatQty = (Double) objAray[3];

				double rawMateToMachine = multiUnitConversionServiceImpl.getConvertionQty(companyMstId, rawItemId,
						rawMateUnitId, machineUnitId, rawmatQty);

				rawMaterialForAprod = rawMateToMachine + rawMaterialForAprod;

			} catch (Exception e) {
				System.out.print(e.getMessage());
			}
		}
		recipeSingleQtyInMachUnit = rawMaterialForAprod / minumQty;

		return recipeSingleQtyInMachUnit;
	}


	@Transactional
	@Override
	public void deleteProductionPrePlanning(Date fromDate, Date tdate) {
	
		
		saleOrderProductionPlaningLinkMstRepository.deleteSaleOrderProductionPlaningLinkMst(fromDate,tdate);
		
		productionPreplanningMstRepo.deleteProductionPrePlanning(fromDate,tdate);

	}
	



	
	
	
}
