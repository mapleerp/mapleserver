package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.MonthlySalesDtl;
import com.maple.restserver.entity.MonthlySalesTransHdr;
import com.maple.restserver.repository.MonthlySalesDtlRepository;
import com.maple.restserver.repository.MonthlySalesTransHdrRepository;

@Service
@Transactional
public class MonthlySalesReportServiceImpl implements MonthlySalesReportService {

	@Autowired
	MonthlySalesDtlRepository monthlySalesDtlRepository;
	@Autowired
	MonthlySalesTransHdrRepository monthlySalesTransHdrRepository;
	@Override
	public List<MonthlySalesTransHdr> getMonthlySalesReporthdr(String branchcode, Date fromdate, Date todate,
			String companymstid) {
		
		List<MonthlySalesTransHdr> monthlySalesTransHdrList=new ArrayList<MonthlySalesTransHdr>();
		monthlySalesTransHdrList =monthlySalesTransHdrRepository.getMonthlySalesReporthdr(branchcode,
				fromdate,todate,companymstid);
		
		return monthlySalesTransHdrList;
	}
	@Override
	public List<MonthlySalesDtl> findByMonthlySalesTransHdr(String vouchernumber) {
		
MonthlySalesTransHdr  monthlySalesTransHdr=monthlySalesTransHdrRepository.findByVoucherNumber(vouchernumber);
		

List<MonthlySalesDtl> monthlySalesDtlList=monthlySalesDtlRepository.
findByMonthlySalesTransHdr(monthlySalesTransHdr);
		
		
		return monthlySalesDtlList;
	}

	
	
}
