package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyMst;

import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.report.entity.B2bSaleReport;
import com.maple.restserver.report.entity.BranchWiseProfitReport;
import com.maple.restserver.report.entity.ConsumptionReport;
import com.maple.restserver.report.entity.DailySalesReportDtl;
import com.maple.restserver.report.entity.DailySalesReturnReportDtl;
import com.maple.restserver.report.entity.HsnWiseSalesDtlReport;
import com.maple.restserver.report.entity.ItemWiseDtlReport;
import com.maple.restserver.report.entity.PharmacyGroupWiseSalesReport;
import com.maple.restserver.report.entity.PharmacySalesDetailReport;
import com.maple.restserver.report.entity.PharmacySalesReturnReport;
import com.maple.restserver.report.entity.ReceiptModeWiseReort;
import com.maple.restserver.report.entity.SaleOrderReceiptReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.SalesTaxSplitReport;
import com.maple.restserver.report.entity.SettledAmountDtlReport;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;

@Service
@Transactional
@Component
public class SaleTransHdrReportServiceImpl implements SalesTransHdrReportService{

	
	@Autowired
	CategoryMstRepository categoryMstRepository;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@Override
	public List<DailySalesReportDtl> getDailySalesReportdtl(String branchcode, Date date,Date tdate, String companymstid) {
		
	List< DailySalesReportDtl> dailySalsReportList =   new ArrayList();
	 
	 List<Object> obj =   salesTransHdrRepository.getDailySalesDtl(  branchcode,date,tdate,companymstid);
	 for(int i=0;i<obj.size();i++)
	 {
		 Object[] objAray = (Object[]) obj.get(i);
		 DailySalesReportDtl report=new DailySalesReportDtl();
		 report.setVoucherNumber((String) objAray[0]);
		 report.setCustomerName((String) objAray[1]);
		 report.setCustomerType((String) objAray[2]);
		 report.setAmount((Double) objAray[3]);
		 report.setReceiptMode((String) objAray[4]);
		 report.setVoucherDate((Date) objAray[5]);
		 dailySalsReportList.add(report);
	 }
	 
	return dailySalsReportList;
	}

	@Override
	public List<ItemWiseDtlReport> getDailyItemWiseSalesReportdtl(String branchcode, String date,
			String companymstid) {
		
		
		List<Object> obj = 	salesTransHdrRepository.getDailyItemWiseSalesReportdtl(   branchcode,    date,   companymstid);
		List< ItemWiseDtlReport> dailySalsReportList =   new ArrayList();

		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ItemWiseDtlReport report=new ItemWiseDtlReport();
			 report.setVoucherDate((Date) objAray[0]);
			 report.setItemName((String) objAray[1]);
			 report.setQty((Double) objAray[2]);
			 report.setValue((Double) objAray[3]);
			 report.setCompanyName((String) objAray[4]);
			 report.setBranchName((String) objAray[5]);
			 report.setBranchAddress((String) objAray[6]);
			 report.setBranchPhoneNo((String) objAray[7]);
			 report.setBranchState((String) objAray[8]);
			 report.setBranchEmail((String) objAray[9]);
			 report.setBranchWebsite((String) objAray[10]);
			 dailySalsReportList.add(report);
		
		 }
		 
		
		return dailySalsReportList;
	}
	
	@Override
	public List<ItemWiseDtlReport> getDailyItemWiseMonthlySalesReportdtl(String branchcode, String fromDate,String toDate,String companymstid) {
		
		
		List<Object> obj = 	salesTransHdrRepository.getDailyItemWiseMonthlySalesReportdtl(   branchcode,    fromDate,toDate,   companymstid);
		
		List< ItemWiseDtlReport> dailySalsReportList =   new ArrayList();
		
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ItemWiseDtlReport report=new ItemWiseDtlReport();
			 report.setVoucherDate((Date) objAray[0]);
			 report.setItemName((String) objAray[1]);
			 report.setQty((Double) objAray[2]);
			 report.setBranchName((String) objAray[3]);
			 report.setBranchWebsite((String) objAray[4]);
			 report.setBranchEmail((String) objAray[5]);
			 report.setCompanyName((String) objAray[6]);
			 dailySalsReportList.add(report);
		
		 }
		 
		return dailySalsReportList;
	}

	@Override
	public List<ItemWiseDtlReport> getItemWiseSalesReprtBetweenDates(String branchcode, String reportdate,
			String todate, String companymstid, String itemid) {
		
List<Object> obj = 	salesTransHdrRepository.getitemWiseSalesReportDateBetweenDates(   branchcode,    reportdate, todate,   companymstid,itemid);
		
		List< ItemWiseDtlReport> dailySalsReportList =   new ArrayList();
	
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ItemWiseDtlReport report=new ItemWiseDtlReport();
			 report.setVoucherDate((Date) objAray[0]);
			 report.setItemName((String) objAray[1]);
			 report.setQty((Double) objAray[2]);
			 report.setValue((Double) objAray[3]);
			 dailySalsReportList.add(report);
		 }
		return dailySalsReportList;
	}

	@Override
	public List<ItemWiseDtlReport> getCategoryWiseSalesReprtBetweenDates(String branchcode, Date reportdate,
			Date todate, String companymstid, String categoryid) {
		
   List<Object> obj=salesTransHdrRepository.getcategoryWiseSalesReportDateBetweenDates( branchcode,    reportdate, todate,   companymstid,categoryid);
		
		List<ItemWiseDtlReport> dailySalsReportList =new ArrayList();
		
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ItemWiseDtlReport report=new ItemWiseDtlReport();
			 report.setVoucherDate((Date) objAray[0]);
			 report.setItemName((String) objAray[1]);
			 report.setQty((Double) objAray[2]);
			 report.setBranchName((String) objAray[3]);
			 report.setBranchWebsite((String) objAray[4]);
			 report.setBranchEmail((String) objAray[5]);
			 report.setCompanyName((String) objAray[6]);
			 report.setValue((Double) objAray[7]);
			 report.setBranchAddress((String) objAray[8]);
			 report.setBranchPhoneNo((String) objAray[9]);
			 report.setBranchState((String) objAray[10]);
			 dailySalsReportList.add(report);
		 }
		 
		return dailySalsReportList;
	}

	@Override
	public List<B2bSaleReport> getB2BSalesReport(Date date,Date toDate, String branchCode,CompanyMst companymst) {
		
	
List<Object> obj = 	salesTransHdrRepository.getB2BSalesReport(date, toDate  ,branchCode, companymst);
List< B2bSaleReport> b2bReportList =   new ArrayList();
System.out.print(obj.size()+"obj list size issssssssssssssssssssssssssssssssssssssssssssssssssssssss");

         for(int i=0;i<obj.size();i++)
		 {
        	 
        	 
       
			 Object[] objAray = (Object[]) obj.get(i);
			 B2bSaleReport b2bReport=new B2bSaleReport();
			 b2bReport.setCustomerName((String) objAray[0]);
			 b2bReport.setCustomerGst((String) objAray[1]);
			 //b2bReport.setSalesMan((String) objAray[2]);
			 b2bReport.setVoucherNo((String) objAray[2]);
			 b2bReport.setPaytm((Double) objAray[3]);
			 b2bReport.setCardPayment((Double) objAray[4]);
			
			 b2bReport.setAdvance((Double) objAray[6]);
			 //b2bReport.setCash((Double) objAray[6]);
			 b2bReport.setCredit((Double) objAray[7]);
		     b2bReport.setNetTotal((Double) objAray[8]);
		     b2bReport.setBranchCode((String) objAray[9]);
		     b2bReport.setBranchName((String) objAray[10]);
		     b2bReport.setBranchAddress((String) objAray[11]);
		     b2bReport.setBranchPhone((String) objAray[12]);
		   
		 //    b2bReport.setSalesMode((String) objAray[13]);
		     b2bReport.setCompanyName((String) objAray[13]);
		     b2bReport.setVoucherDate((Date) objAray[14]);
		    
			 b2bReportList.add(b2bReport);
		 }
         
		return b2bReportList;
}
	@Override
	public List<SalesTransHdr> findByVoucherDate(Date date) {
		return salesTransHdrRepository.findByVoucherDate(date);
	}

	@Override
	public List<DailySalesReportDtl> getCustomerDailySalesReportdtl(String branchcode, Date fromdate, Date todate,
			String companymstid, String customerid) {
		
		List< DailySalesReportDtl> dailySalsReportList =   new ArrayList();
		 
		 
	 List<Object> obj =   salesTransHdrRepository.getCustomerDailySalesDtl(  branchcode,fromdate,todate,companymstid,customerid);
	 for(int i=0;i<obj.size();i++)
	 {
		 Object[] objAray = (Object[]) obj.get(i);
		 DailySalesReportDtl report=new DailySalesReportDtl();
		 report.setVoucherNumber((String) objAray[0]);
		 report.setCustomerName((String) objAray[1]);
		 report.setCustomerType((String) objAray[2]);
		 report.setAmount((Double) objAray[3]);
		 report.setVoucherDate((Date) objAray[4]);
		 dailySalsReportList.add(report);
	 }
	return dailySalsReportList;
	
	}

	@Override
	public List<BranchWiseProfitReport> getBranchWiseProfit(Date sdate, Date edate, CompanyMst companyMst) {
		 
		List<BranchWiseProfitReport> branchProfitList = new ArrayList<>();
		List<Object> obj = salesTransHdrRepository.getBranchWiseProfit(sdate,edate,companyMst);
		
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 BranchWiseProfitReport branchProfit = new BranchWiseProfitReport();
			 branchProfit.setBranchName((String) objAray[0]);
			 branchProfit.setProfit((Double) objAray[1]);
			 
			 branchProfitList.add(branchProfit);
			 
		 }
		
		return branchProfitList;
	}

	@Override
	public List<BranchWiseProfitReport> getProfitByBranch(Date sdate, Date edate, CompanyMst companyMst,
			String branchCode) {
		List<BranchWiseProfitReport> branchProfitList = new ArrayList<>();
		List<Object> obj = salesTransHdrRepository.getProfitByBranch(sdate,edate,companyMst,branchCode);
		
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 BranchWiseProfitReport branchProfit = new BranchWiseProfitReport();
			 branchProfit.setItemName((String) objAray[0]);
			 branchProfit.setProfit((Double) objAray[1]);
			 
			 branchProfitList.add(branchProfit);
			 
		 }
		
		return branchProfitList;
	}

	@Override
	public List<DailySalesReturnReportDtl> getDailySalesReturnReportdtl(String branchcode, Date date, Date tdate,
			String companymstid) {
		List< DailySalesReturnReportDtl> dailySalsReportList =   new ArrayList();
		 
		 List<Object> obj =   salesTransHdrRepository.getDailySalesReturnDtl(  branchcode,date,tdate,companymstid);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 DailySalesReturnReportDtl report=new DailySalesReturnReportDtl();
			 report.setVoucherNumber((String) objAray[0]);
			 report.setCustomerName((String) objAray[1]);
			 report.setCustomerType((String) objAray[2]);
			 report.setAmount((Double) objAray[3]);
			 report.setVoucherDate((Date) objAray[4]);
			 dailySalsReportList.add(report);
		 }
		 
		return dailySalsReportList;
	}

	@Override
	public List<SalesInvoiceReport> getSalesAnalysisReport(Date sdate, Date edate, String companymstid,String branchCode) {
		
		List<SalesInvoiceReport> salesInvoiceReportList = new ArrayList<SalesInvoiceReport>();
		
		List<Object> objList = salesTransHdrRepository.getSalesAnalysisReport(sdate,edate,companymstid,branchCode);
				
		for(int i=0; i<objList.size(); i++)
		{
			Object[] objArray = (Object[]) objList.get(i);
			
			SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
			
			salesInvoiceReport.setId((String) objArray[0]);
			salesInvoiceReport.setItemName((String) objArray[4]);
			salesInvoiceReport.setAmount((Double) objArray[5]);
			salesInvoiceReport.setQty((Double) objArray[3]);
			salesInvoiceReport.setVoucherDate((Date) objArray[2]);
			salesInvoiceReport.setVoucherNumber((String) objArray[1]);
			
			salesInvoiceReportList.add(salesInvoiceReport);
			
		}
				
		return salesInvoiceReportList;
	}

	//version 3.1
	@Override
	public List<SalesInvoiceReport> getSalesDtlReport(Date sdate, Date edate, CompanyMst companyMst,
			String branchCode) {
	
		
	List<SalesInvoiceReport> salesInvoiceReportList = new ArrayList<SalesInvoiceReport>();
		
		List<Object> objList = salesTransHdrRepository.getSalesDtlReport(sdate,edate,companyMst,branchCode);
				
		for(int i=0; i<objList.size(); i++)
		{
			Object[] objArray = (Object[]) objList.get(i);
		
			SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
			salesInvoiceReport.setVoucherNumber((String) objArray[0]);
			salesInvoiceReport.setVoucherDate((Date) objArray[1]);
			salesInvoiceReport.setCustomerName((String) objArray[2]);
			salesInvoiceReport.setItemName((String) objArray[3]);
			salesInvoiceReport.setQty((Double) objArray[4]);
			salesInvoiceReport.setMrp((Double) objArray[5]);
			salesInvoiceReport.setRate((Double) objArray[6]);
			salesInvoiceReport.setUnitName((String) objArray[7]);
			salesInvoiceReport.setTaxRate((Double) objArray[8]);
			salesInvoiceReport.setAmount((Double) objArray[4]*(Double) objArray[6]);
			salesInvoiceReportList.add(salesInvoiceReport);
			
		}
				
		return salesInvoiceReportList;
		
		
	}
	//version 3.1 end

	@Override
	public List<ReceiptModeWiseReort> getReceiptModeWiseReort(String branchcode, Date fromdate, 
			String companymstid) {
	
		
	List<ReceiptModeWiseReort> receiptModeWiseReportList= new ArrayList();
		
	String cashMode=branchcode+"-CASH";
		List<Object> obj = 	salesTransHdrRepository.getReceiptModeWiseWhithSoReport(    fromdate, branchcode,  companymstid,cashMode);
		
		
		
		 for(int i=0;i<obj.size();i++)
		 {
		
			 Object[] objAray = (Object[]) obj.get(i);
			 ReceiptModeWiseReort report=new ReceiptModeWiseReort();
			 report.setVoucherNumber((String) objAray[0]);
			 report.setReceiptAmount((Double) objAray[1]);
		     report.setReceiptMode((String) objAray[2]);
			 report.setVoucherDate(String.valueOf((Date) objAray[3]));
			 receiptModeWiseReportList.add(report);
		
		 }
		 
		
		return receiptModeWiseReportList;

	}

	@Override
	public Double getSaleOrderSettledAmount(String branchcode, Date fromdate, String companymstid) {
		Double settledAmount=salesTransHdrRepository.getSettlementReport(branchcode, fromdate);
		return settledAmount;
	}

	@Override
	public Double getPreviousAdvanceAmount(String branchcode, Date fromdate, String companymstid, String receiptmode) {
		Double previousAdvance=salesTransHdrRepository.getPreviousAdvance(branchcode,  fromdate,  receiptmode);
		return previousAdvance;
	}

	@Override
	public List<SettledAmountDtlReport> getSettledAmountDtlReport(String branchcode, Date fromdate,
			String companymstid) {
		
		List<SettledAmountDtlReport> settledAmountDtlReportList= new ArrayList();
		List<Object> obj = 	salesTransHdrRepository.getSettlementReportVoucherNo(branchcode,    fromdate);
		
		
		
		 for(int i=0;i<obj.size();i++)
		 {
		
			 Object[] objAray = (Object[]) obj.get(i);
			 SettledAmountDtlReport report=new SettledAmountDtlReport();
			 report.setVoucherNumber((String) objAray[0]);
			 report.setAmount((Double) objAray[1]);
			 report.setVoucherDate((Date) objAray[2]);
			 settledAmountDtlReportList.add(report);
		
		 }
		 
		
		return settledAmountDtlReportList;

	}

	@Override
	public List<SettledAmountDtlReport> getSettledAmountDtlCashWiseReport(String branchcode, Date fromdate,
			String companymstid) {
		
		List<SettledAmountDtlReport> settledAmountDtlReportList= new ArrayList();
		List<Object> obj = 	salesTransHdrRepository.getSettledAmountDtlCashWiseReport(branchcode,    fromdate);
		
		
		
		 for(int i=0;i<obj.size();i++)
		 {
		
			 Object[] objAray = (Object[]) obj.get(i);
			 SettledAmountDtlReport report=new SettledAmountDtlReport();
			 report.setVoucherNumber((String) objAray[0]);
			 report.setAmount((Double) objAray[1]);
			 report.setVoucherDate((Date) objAray[2]);
			 settledAmountDtlReportList.add(report);
		
		 }
		 
		
		return settledAmountDtlReportList;
	}

	@Override
	public Double getSaleOrderSettledCashAmount(String branchcode, Date fromdate, String companymstid) {
		Double settledCasAmount=salesTransHdrRepository.getSettlementCashReport(branchcode, fromdate);
		return settledCasAmount;
	}

	@Override
	public List<ReceiptModeWiseReort> getReceiptModeCashWiseReort(String branchcode, Date fromdate,
			String companymstid) {
		
	List<ReceiptModeWiseReort> receiptModeWiseReportList= new ArrayList();
		
	String receiptMode=branchcode+"-CASH";
		List<Object> obj = 	salesTransHdrRepository.getReceiptModeCashWiseReort(branchcode,    fromdate,   companymstid,receiptMode);
		
		
		
		 for(int i=0;i<obj.size();i++)
		 {
		
			 Object[] objAray = (Object[]) obj.get(i);
			 ReceiptModeWiseReort report=new ReceiptModeWiseReort();
			 report.setVoucherNumber((String) objAray[0]);
			 report.setReceiptAmount((Double) objAray[1]);
		     report.setReceiptMode((String) objAray[2]);
			 report.setVoucherDate(String.valueOf((Date) objAray[3]));
			 receiptModeWiseReportList.add(report);
		
		 }
		 
		
		return receiptModeWiseReportList;

	}

	@Override
	public List<DailySalesReportDtl> getDailySalesReportdtl(String branchcode, Date date, String companymstid) {
		
		List< DailySalesReportDtl> dailySalsReportList =   new ArrayList();
		 
		 List<Object> obj =   salesTransHdrRepository.getDailySalesReport(  branchcode,date,companymstid);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 DailySalesReportDtl report=new DailySalesReportDtl();
			 report.setVoucherNumber((String) objAray[0]);
			 report.setCustomerName((String) objAray[1]);
			 report.setCustomerType((String) objAray[2]);
			 report.setAmount((Double) objAray[3]);
			 report.setVoucherDate((Date) objAray[4]);
			 dailySalsReportList.add(report);
		 }
		 
		return dailySalsReportList;
		}

	@Override
	public Double getPreviousAdvanceAmountCard(String branchcode, Date fromdate, String companymstid,
			String receiptmode) {
		Double settledCasAmount=salesTransHdrRepository.getPreviousAdvanceCard(branchcode, fromdate,receiptmode);

		return settledCasAmount;
	}
	
	@Override
	public Double getPreviousAdvanceAmountCardRealized(String branchcode, Date fromdate, String companymstid,
			String receiptmode) {
		Double settledCasAmount=salesTransHdrRepository.getPreviousAdvanceCardRealized(branchcode, fromdate,receiptmode);

		return settledCasAmount;
	}
	
	
	

	@Override
	public List<SaleOrderReceiptReport> getPreviousAdvanceAmountDtl(String branchcode, Date fromdate,
			String companymstid, String receiptmode) {
		List<SaleOrderReceiptReport> saleOrderReceiptList = new ArrayList<SaleOrderReceiptReport>();
		List<Object> obj = salesTransHdrRepository.getPreviousAdvanceDtl(branchcode,fromdate,receiptmode);
		for(int i =0 ;i <obj.size();i++)
		{
			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderReceiptReport saleOrderReceiptReport= new SaleOrderReceiptReport();
			saleOrderReceiptReport.setReceiptAmount((Double) objAray[1]);
			saleOrderReceiptReport.setVoucherNumber((String) objAray[0]);
			saleOrderReceiptReport.setCustomerName((String) objAray[2]);
			saleOrderReceiptList.add(saleOrderReceiptReport);
			
			
		}
		return saleOrderReceiptList;
	}
	
	//---------------version 4.11
	@Override
	public List<SalesTaxSplitReport> SaleTaxSplit(Date edate, CompanyMst companyMst, String branchCode) {
		List<SalesTaxSplitReport> taxSplit = new ArrayList<SalesTaxSplitReport>();
		
		List<Object> obj = salesTransHdrRepository.SaleTaxSplit(edate, companyMst, branchCode);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);

			 SalesTaxSplitReport salesTaxSplitReport = new SalesTaxSplitReport();
			 salesTaxSplitReport.setTaxableValue((Double) objAray[0]);
			 salesTaxSplitReport.setTaxAmount((Double) objAray[1]);
			 salesTaxSplitReport.setTaxRate((Double) objAray[2]);
			 
			 taxSplit.add(salesTaxSplitReport);
		 }
		
		return taxSplit;
	}
	//---------------version 4.11 end

	@Override
	public List<PharmacyGroupWiseSalesReport> getPharmacyGroupWiseSalesReport(CompanyMst companyMst, Date fdate,
			Date tdate, String branchCode, String[] array) {
		
		
		List<PharmacyGroupWiseSalesReport>  pharmacyGroupWiseSalesReportList=new ArrayList<PharmacyGroupWiseSalesReport>();
		
		for(String categoryName:array) {
			CategoryMst categoryMst=categoryMstRepository.findByCategoryNameAndCompanyMst(categoryName,companyMst);

		List<Object> obj=salesTransHdrRepository.getPharmacyGroupWiseSalesReport(companyMst,fdate ,tdate, branchCode, categoryMst.getId());
		//PharmacyGroupWiseSalesReport pharmacyGroupWiseSalesReport=new PharmacyGroupWiseSalesReport();
		for(int i=0;i<obj.size();i++) {
			Object[] objAray =(Object[]) obj.get(i);
			PharmacyGroupWiseSalesReport pharmacyGroupWiseSalesReport=new PharmacyGroupWiseSalesReport();
			pharmacyGroupWiseSalesReport.setItemName((String) objAray[0]);
			pharmacyGroupWiseSalesReport.setBatchCode((String) objAray[1]);
			pharmacyGroupWiseSalesReport.setQty((Double) objAray[2]);
			pharmacyGroupWiseSalesReport.setAmount((Double) objAray[3]);
		    pharmacyGroupWiseSalesReport.setTax((Double) objAray[4]);
		    
		pharmacyGroupWiseSalesReportList.add(pharmacyGroupWiseSalesReport);
		}
		}
		return pharmacyGroupWiseSalesReportList;
		
	}

	@Override
	public List<PharmacySalesDetailReport> getPharmacyGroupWiseSalesDtlReport(CompanyMst companyMst, Date fdate,
			Date tdate, String branchCode, String[] array) {
		

		List<PharmacySalesDetailReport>  pharmacySalesDetailReportList=new ArrayList<PharmacySalesDetailReport>();
		
		for(String categoryName:array) {
			CategoryMst categoryMst=categoryMstRepository.findByCategoryNameAndCompanyMst(categoryName,companyMst);

		List<Object> obj=salesTransHdrRepository.getPharmacyGroupWiseSalesDtlReport(companyMst,fdate ,tdate, branchCode, categoryMst.getId());
		//PharmacySalesDetailReport pharmacySalesDetailReport=new PharmacySalesDetailReport();
		for(int i=0;i<obj.size();i++) {
			Object[] objAray =(Object[]) obj.get(i);
			PharmacySalesDetailReport pharmacySalesDetailReport=new PharmacySalesDetailReport();
			pharmacySalesDetailReport.setItemName((String) objAray[0]);
			pharmacySalesDetailReport.setGroupName((String) objAray[1]);
			pharmacySalesDetailReport.setVoucherNumber((String) objAray[2]);


			pharmacySalesDetailReport.setInvoiceDate((String) objAray[3].toString());
			pharmacySalesDetailReport.setBatchCode((String) objAray[4]);
			pharmacySalesDetailReport.setTax((Double) objAray[5]);

			pharmacySalesDetailReport.setInvoiceAmount((Double) objAray[6]);
			pharmacySalesDetailReport.setAmount((Double) objAray[7]);
			pharmacySalesDetailReport.setCustomerName((String) objAray[8]);
			pharmacySalesDetailReport.setQty((Double) objAray[9]);
	
		    
		    pharmacySalesDetailReportList.add(pharmacySalesDetailReport);
		}
		}
		return pharmacySalesDetailReportList;
	}
	public List<DailySalesReportDtl> getSalesDtlDailyReport(String branchcode, Date fromdate, Date todate,
			String companymstid) {
		List<DailySalesReportDtl> dailysalesReportList = new ArrayList();
		List<Object> obj = salesTransHdrRepository.salesDtlDaily(branchcode,fromdate,todate,companymstid);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);

			 DailySalesReportDtl dailySalesReportDtl = new DailySalesReportDtl();
			 dailySalesReportDtl.setBatch((String) objAray[5]);
			 dailySalesReportDtl.setCategoryName((String) objAray[4]);
			 dailySalesReportDtl.setCustomerName((String) objAray[2]);
			 dailySalesReportDtl.setExpiryDate((Date) objAray[6]);
			 dailySalesReportDtl.setItemName((String) objAray[3]);
			 dailySalesReportDtl.setQty((Double) objAray[7]);
			 dailySalesReportDtl.setRate((Double) objAray[9]);
			 dailySalesReportDtl.setTaxRate((Double) objAray[8]);
			 dailySalesReportDtl.setvDate((Date) objAray[0]);
			 dailySalesReportDtl.setVoucherNumber((String) objAray[1]);
			 dailySalesReportDtl.setSaleValue( dailySalesReportDtl.getQty()* dailySalesReportDtl.getRate());
			 dailySalesReportDtl.setUserId((String) objAray[10]);
			 dailysalesReportList.add(dailySalesReportDtl);
			 
		 }
		return dailysalesReportList;
	}

	@Override
	public List<ReceiptModeWiseReort> getReceiptModeWisSummaryeReort(String branchcode, Date fromdate,
			String companymstid) {

		List<ReceiptModeWiseReort> receiptModeWiseReportList =new ArrayList<ReceiptModeWiseReort>();
		
		String cashMode=branchcode+"-CASH";
		List<Object> obj = 	salesTransHdrRepository.getReceiptModeRepportWithSo(fromdate, branchcode,  companymstid,cashMode);
		
		
		
		 for(int i=0;i<obj.size();i++)
		 {
		
			 Object[] objAray = (Object[]) obj.get(i);
			 ReceiptModeWiseReort report=new ReceiptModeWiseReort();
		
		     report.setReceiptMode((String) objAray[0]);
		     report.setReceiptAmount((Double) objAray[1]);
			 receiptModeWiseReportList.add(report);
		
		 }
		 
		
		return receiptModeWiseReportList;
		
		

	}



	@Override
	public List<DailySalesReportDtl> getUserWiseSummarySalesReportDtl(String branchcode, String userid, Date sdate, Date edate,
			String companymstid) {
		List< DailySalesReportDtl> dailySalsReportList =   new ArrayList();
		 
		 List<Object> obj =   salesTransHdrRepository.getUserWiseSummarySalesReportDtl( branchcode,userid,sdate,edate,companymstid);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 DailySalesReportDtl report=new DailySalesReportDtl();
			 report.setVoucherNumber((String) objAray[0]);
			 report.setCustomerName((String) objAray[1]);
			 report.setCustomerType((String) objAray[2]);
			 report.setAmount((Double) objAray[3]);
			 report.setReceiptMode((String) objAray[4]);
			 report.setVoucherDate((Date) objAray[5]);
			 dailySalsReportList.add(report);
		 }
		 
		return dailySalsReportList;
	}

	@Override
	public List<DailySalesReportDtl> getUserWiseSalesReport(String branchcode, String userid, Date sdate, Date edate,
			String companymstid) {
		List< DailySalesReportDtl> dailySalsReportList =   new ArrayList();
		 
		 List<Object> obj =   salesTransHdrRepository.getUserWiseSalesReport( branchcode,userid,sdate,edate,companymstid);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 DailySalesReportDtl dailySalesReportDtl =new DailySalesReportDtl();
			 
			 dailySalesReportDtl.setItemName((String) objAray[0]);
			 dailySalesReportDtl.setQty((Double) objAray[1]);
			 dailySalesReportDtl.setAmount((Double) objAray[2]);
			 dailySalesReportDtl.setVoucherDate((Date) objAray[3]);
			 dailySalesReportDtl.setVoucherNumber((String) objAray[4]);
			 dailySalesReportDtl.setCustomerName((String) objAray[5]);
		
			 dailySalsReportList.add(dailySalesReportDtl);
		 }
		 
		return dailySalsReportList;
	}

	@Override
	public List<BranchWiseProfitReport> getCustomerWiseProfit(Date sdate, Date edate, CompanyMst companyMst,
			String branchCode) {
		
		List<BranchWiseProfitReport> branchProfitList = new ArrayList<>();
		List<Object> obj = salesTransHdrRepository.getCustomerWiseProfit(sdate,edate,companyMst,branchCode);
		
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 BranchWiseProfitReport branchProfit = new BranchWiseProfitReport();
			 branchProfit.setCustomerName((String) objAray[0]);
			 branchProfit.setProfit((Double) objAray[1]);
			 
			 branchProfitList.add(branchProfit);
			 
		 }
		
		return branchProfitList;
	}

	@Override
	public List<HsnWiseSalesDtlReport> getHsnWiseSalesDtlReportWithoutGroup(Date fromDate, Date toDate,
			String branchcode, CompanyMst companyMst) {
		List<HsnWiseSalesDtlReport> hsnWiseSalesDtlReportList = new ArrayList();
		List<Object> obj = salesTransHdrRepository.getHsnSalesDtlReportWithoutGroup( fromDate, toDate,
			 branchcode,  companyMst);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 HsnWiseSalesDtlReport hsnWiseSalesDtlReport = new HsnWiseSalesDtlReport();
			 hsnWiseSalesDtlReport.setCessRate((Double)objAray[9]);
			 hsnWiseSalesDtlReport.setCgstAmount((Double)objAray[6]);
			 hsnWiseSalesDtlReport.setSgstAmount((Double)objAray[7]);
			 hsnWiseSalesDtlReport.setHsnCode((String)objAray[1]);
			 hsnWiseSalesDtlReport.setItemName((String)objAray[0]);
			 hsnWiseSalesDtlReport.setQty((Double)objAray[3]);
			 hsnWiseSalesDtlReport.setRate((Double)objAray[4]);
			 hsnWiseSalesDtlReport.setTaxRate((Double)objAray[8]);
			 hsnWiseSalesDtlReport.setUnitName((String)objAray[2]);
			 hsnWiseSalesDtlReport.setValue((Double)objAray[5]);
			 hsnWiseSalesDtlReportList.add(hsnWiseSalesDtlReport);
		 }
		
		return hsnWiseSalesDtlReportList;
	}

	@Override
	public List<HsnWiseSalesDtlReport> getHsnWiseSalesReportWithGoup(Date fromDate, Date toDate, String[] array,
			String branchcode, CompanyMst companyMst) {
		List<HsnWiseSalesDtlReport> hsnWiseSalesDtlReportList = new ArrayList();
		List<Object> obj = salesTransHdrRepository.getHsnSalesDtlReportWithGroup( fromDate, toDate, array,
			 branchcode,  companyMst);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 HsnWiseSalesDtlReport hsnWiseSalesDtlReport = new HsnWiseSalesDtlReport();
			 hsnWiseSalesDtlReport.setCessRate((Double)objAray[9]);
			 hsnWiseSalesDtlReport.setCgstAmount((Double)objAray[6]);
			 hsnWiseSalesDtlReport.setSgstAmount((Double)objAray[7]);
			 hsnWiseSalesDtlReport.setHsnCode((String)objAray[1]);
			 hsnWiseSalesDtlReport.setItemName((String)objAray[0]);
			 hsnWiseSalesDtlReport.setQty((Double)objAray[3]);
			 hsnWiseSalesDtlReport.setRate((Double)objAray[4]);
			 hsnWiseSalesDtlReport.setTaxRate((Double)objAray[8]);
			 hsnWiseSalesDtlReport.setUnitName((String)objAray[2]);
			 hsnWiseSalesDtlReport.setValue((Double)objAray[5]);
			 hsnWiseSalesDtlReportList.add(hsnWiseSalesDtlReport);
		 }
		
		return hsnWiseSalesDtlReportList;
	}

	@Override
	public List<B2bSaleReport> getB2BSalesReportDtl(Date date, Date tdate, String branchCode, CompanyMst companyMst) {
		
		
List<Object> obj = 	salesTransHdrRepository.getB2BSalesReportdtl(date, tdate  ,branchCode, companyMst);
List< B2bSaleReport> b2bReportList =   new ArrayList();
System.out.print(obj.size()+"obj list size issssssssssssssssssssssssssssssssssssssssssssssssssssssss");

         for(int i=0;i<obj.size();i++)
		 {
        	 
        	 
       
			 Object[] objAray = (Object[]) obj.get(i);
			 B2bSaleReport b2bReport=new B2bSaleReport();
			 b2bReport.setCustomerName((String) objAray[0]);
			 b2bReport.setCustomerGst((String) objAray[1]);
			 //b2bReport.setSalesMan((String) objAray[2]);
			 b2bReport.setVoucherNo((String) objAray[2]);
		     b2bReport.setVoucherDate((Date) objAray[3]);
		     b2bReport.setNetTotal((Double) objAray[4]);
		     b2bReport.setTaxRate((Double) objAray[5]);
		    b2bReport.setTaxableValue((Double) objAray[6]);
			 b2bReportList.add(b2bReport);
		 }
         
		return b2bReportList;
	}
	
	@Override
	public List<B2bSaleReport> getNewB2CSalesReport(Date date, Date tdate, String branchCode, CompanyMst companyMst) {

		List<Object> obj = salesTransHdrRepository.getNewB2CSalesReportBetweenDate(date, tdate, branchCode, companyMst);
		List<B2bSaleReport> b2cReportList = new ArrayList();

		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			B2bSaleReport b2bReport = new B2bSaleReport();
			
			b2bReport.setTaxRate((Double) objAray[1]);
			b2bReport.setSalesMode((String) objAray[0]);
			b2bReport.setCessAmount((Double) objAray[4]);
			b2bReport.setCgstAmount((Double) objAray[3]);
			b2bReport.setSgstAmount((Double) objAray[5]);
			b2bReport.setTaxAmount((Double) objAray[8]);
			b2bReport.setIgstAmount((Double) objAray[6]);
			b2bReport.setAmount((Double) objAray[7]);
			b2bReport.setTaxableAmount((Double) objAray[2]);
			
			b2cReportList.add(b2bReport);
			
			
		}

		return b2cReportList;
//		return null;

	}

	@Override
	public List<SalesInvoiceReport> getWholesSalesInvoiceReport(Date date, Date tdate, String branchCode,
			CompanyMst companyMst) {
		List<Object> obj = salesTransHdrRepository.getWholesSalesInvoiceReport(date, tdate, branchCode, companyMst);
		List<SalesInvoiceReport> wholesaleReportList = new ArrayList();
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
			salesInvoiceReport.setVoucherDate((Date)objAray[2]);
			salesInvoiceReport.setVoucherNumber((String) objAray[3]);
			salesInvoiceReport.setCustomerName((String) objAray[1]);
			salesInvoiceReport.setAmount((Double) objAray[0]);
			salesInvoiceReport.setId((String) objAray[4]);
			wholesaleReportList.add(salesInvoiceReport);
		}
		return wholesaleReportList;
	}
}
