package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.DailySalesSummary;
import com.maple.restserver.report.entity.DailySalesReport;
import com.maple.restserver.report.entity.DailySalesSummaryReport;
import com.maple.restserver.report.entity.DayEndPettyCashPaymentAndReceipt;
import com.maple.restserver.report.entity.ReceiptModeReport;

@Component
public interface DailySaleReportService {
public DailySalesSummary getDailySaleSummary(Date date,String branchCode);
public DailySalesSummaryReport getOpeningPettyCash(Date date,String branchCode);
public DailySalesSummaryReport  advanceRecivedFromOrders(Date date,String branchCode);
public DailySalesSummaryReport getDailySalesDtls(Date date,String branchCode);
public DailySalesSummaryReport dailyCreditSalesDtl(Date date,String branchCode);
public List<DailySalesReport> dailySalesReport(Date date,String companymstid);
public List<DailySalesSummary>  allbranchdailySalesSummaryReport(Date date,String companymstid);

public List<ReceiptModeReport>receiptModeWiseReportSalesAndSoConverted(Date date,String companymstid,String branchcode);
public List<ReceiptModeReport> soAdvanceReceivedToday(Date date,String companymstid,String branchcode);

public List<ReceiptModeReport>dayEndReceiptModewiseReport(Date date,String companymstid,String branchcode);

public DayEndPettyCashPaymentAndReceipt dayEndPettyCashReceiptAndPettyCashPayment(Date date,String companymstid,String branchcode,String accountid);
public List<ReceiptModeReport> dayEndReceptModeWisePreviousSalesReport(Date date, String companymstid,
		String branchcode);

}
