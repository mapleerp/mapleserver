package com.maple.restserver.service;

import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.StockReport;
public interface BatchStockReportService {
	List<StockReport> getStockByBranchCodeAndDateAndCompanyMstIdAndBatch(String branchcode,String companymstid, Date fromdate, String categoryid);

}
