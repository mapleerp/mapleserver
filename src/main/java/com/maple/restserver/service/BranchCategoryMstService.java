package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.BranchCategoryMst;
import com.maple.restserver.entity.CompanyMst;

@Service
public interface BranchCategoryMstService {

	public  List<BranchCategoryMst> retrieveBranchCategory(CompanyMst companyMst);
}
