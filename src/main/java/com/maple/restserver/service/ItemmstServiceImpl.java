package com.maple.restserver.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.report.entity.BatchwisePerformancereport;
import com.maple.restserver.report.entity.ItemCorrectionReport;
import com.maple.restserver.report.entity.ItemExport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlDailyRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.SalesDetailsRepository;

 
@Service
@Transactional
@Component
public class ItemmstServiceImpl implements ItemmstService{
	
	@Autowired
	PurchaseDtlRepository purchaseDtlRepo;
	@Autowired
	SalesDetailsRepository salesDtlRepo;
	@Autowired
	ItemMstRepository itemMstRepository;

	@Autowired
	KitDefenitionDtlRepository kitDefDtlRepo;
	@Autowired
	ItemBatchDtlDailyRepository itemBatchDtlDailyRepo;
	
	@Autowired
	KitDefinitionMstRepository kitDefMstRepo;
 
	@Autowired
	ItemBatchMstRepository itemBatchMstRepo;
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	
	@Autowired
	SaveAndPublishServiceImpl saveAndPublishServiceImpl;
	
	
	
	@Value("${mybranch}")
	private String mybranch;
	
	
	@Override
	public List<ItemMst> getItems(String pdfFileName) {
 List<ItemMst> itemmstList =   new ArrayList();
		 
		 
		 List<ItemMst> itemmsts =   itemMstRepository.findAll();
		
	
		 return itemmsts;
	}


	@Override
	public ItemMst saveItemMst(ItemMst item) {
		  item = itemMstRepository.saveAndFlush(item);
		  
		  
	 
		
		return item;
	}


	@Override
	public List<ItemMst> findAll() {
		 
		return itemMstRepository.findAllByOrderByItemName();
	}


	@Override
 
	public   List<ItemMst>   findByBarCode(String barcode) {
 
		return itemMstRepository.findByBarCode(barcode);
	}


	@Override
	public ItemMst findByItemName(String itemname) {
		// TODO Auto-generated method stub
		return itemMstRepository.findByItemName(itemname);
	}


	@Override
	public Optional<ItemMst> findById(String id) {
		// TODO Auto-generated method stub
		return itemMstRepository.findById(id);
	}


	@Override
	public List<ItemMst> FindByComapnyMstAndCategoryMst(CompanyMst companyid, String categoryid) {
		return itemMstRepository.findByCompanyMstAndCategoryId(companyid,categoryid);
		 
	}


	@Transactional
	@Override
	public void updateItemBarcode(ItemMst itemmst, String barCode) {
		

		String itemid = itemmst.getId();
		itemBatchDtlRepo.updateBarcode(itemid, barCode);
		itemBatchMstRepo.updateBarcode(itemid, barCode);
		itemBatchDtlDailyRepo.updateBarcode(itemid, barCode);
		
		
		kitDefMstRepo.updateBarcode(itemid, barCode);
		kitDefDtlRepo.updateBarcode(itemid, barCode);
		salesDtlRepo.updateBarcode(itemid, barCode);
		purchaseDtlRepo.updateBarcode(itemid, barCode);
	}


	@Override
	public  List<ItemExport>  exportItem() {
	
		List<ItemExport> itemExportList=new ArrayList<ItemExport>();
		List<Object> obj=itemMstRepository.exportItemdetails();
		
	//	@Query(nativeQuery = true,value="select i.item_name, i.standard_price,i.item_code,tax_rate ,c.category_name,u.unit_name from item_mst i category_mst c ,unit_mst  where c.id=i.category_id and u.id=i.unit_id")
		  for(int i=0;i<obj.size();i++)
			 {
			Object[] objAray = (Object[]) obj.get(i);
	
			ItemExport report=new ItemExport();
			report.setItemName((String) objAray[0]);
			
				String stdPrice=String.valueOf((Double) objAray[1]);
		    report.setStandardPrice(stdPrice);
			report.setItemCode((String) objAray[2]);
			String taxRate=String.valueOf((Double) objAray[3]);
			report.setTaxRate(taxRate);
			report.setCategoryName((String) objAray[4]);
			report.setUnitName((String) objAray[5]);
			String stringSgst=String.valueOf((Double) objAray[6]);
			report.setSgst(stringSgst);
			report.setId((String) objAray[7]);
			report.setBarCode((String) objAray[9]);
			report.setHsnCode((String) objAray[8]);
			itemExportList.add(report);
			 }
		return itemExportList;
	}


	@Override
	public List<ItemCorrectionReport> getItemListWithCountofSaleStockPurchase(String companyid) {
		
		List<ItemCorrectionReport> itemCorrectionReportList = new ArrayList<ItemCorrectionReport>();
		
		
		List<Object> objList = itemMstRepository.getItemListWithCountofSaleStockPurchase(companyid);
		
		for(int i = 0; i < objList.size(); i++)
		{
			Object[] objAray = (Object[]) objList.get(i);
			
			ItemCorrectionReport itemCorrectionReport = new ItemCorrectionReport();
			
			itemCorrectionReport.setItemId((String) objAray[1]);
			itemCorrectionReport.setItemName((String) objAray[0]);
			
			
			BigDecimal purchaseCount = (BigDecimal) objAray[3];
			itemCorrectionReport.setPurchaseCount(purchaseCount.doubleValue());
			
			BigDecimal salesCount = (BigDecimal) objAray[2];
			itemCorrectionReport.setSalesCount(salesCount.doubleValue());
			
			BigDecimal stockCount = (BigDecimal) objAray[4];
			itemCorrectionReport.setStockCount(stockCount.doubleValue());
			
			itemCorrectionReportList.add(itemCorrectionReport);
			
		}
		
		return itemCorrectionReportList;
	}


	@Override
	public String publishItemIdAndPriceToServer(String itemid, Double standardPrice, String companymstid,String destination) {
		Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		CompanyMst companyMst = companyMstOpt.get();
		
		
		Optional<ItemMst> itemMstOpt =   itemMstRepository.findById(itemid);
		ItemMst	itemMst=itemMstOpt.get();
		
		
		
		saveAndPublishServiceImpl.publishObjectToServer(itemMst, 
				        mybranch,
						KafkaMapleEventType.ITEMMST, 
						destination,itemMst.getId());
		
		return MapleConstants.SUCCESS;
	}


	
	
}
