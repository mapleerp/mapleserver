package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.SaleOrderDetailsReport;
import com.maple.restserver.report.entity.SaleOrderDueReport;
import com.maple.restserver.report.entity.SaleOrderReceiptReport;
import com.maple.restserver.report.entity.SaleOrderReport;
import com.maple.restserver.report.entity.SaleOrderTax;
import com.maple.restserver.report.entity.SaleOrderinoiceReport;
import com.maple.restserver.report.entity.SalesOrderAdvanceBalanceReport;
import com.maple.restserver.repository.SaleOrderReceiptRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component
public class SaleOrderReportServiceImpl implements SaleOrderReportService {

	@Autowired
	SaleOrderReceiptRepository saleorderReceiptRepo;
	@Autowired
	SalesOrderTransHdrRepository saleorderTransHdrRepository;

	@Override
	public List<SaleOrderDetailsReport> getSaleOrderPendingReport(Date date, String branchCode) {

		List<SaleOrderDetailsReport> saleOrderReportList = new ArrayList();

		List<Object> obj = saleorderTransHdrRepository.getSaleOrderPendingReport(date, branchCode);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderDetailsReport saleOrderReport = new SaleOrderDetailsReport();
			saleOrderReport.setCustomerName((String) objAray[0]);
			saleOrderReport.setCustomerPhoneNo((String) objAray[1]);
			saleOrderReport.setVoucherNo((String) objAray[2]);
			saleOrderReport.setConvertedToInvoice((String) objAray[3]);
			saleOrderReport.setPaidAmount((Double) objAray[6]);
			saleOrderReport.setLocalCustomerName((String) objAray[7]);
			saleOrderReport.setLocalCustomerPhoneNo((String) objAray[8]);
			saleOrderReportList.add(saleOrderReport);
		}

		return saleOrderReportList;

	}

	@Override
	public List<SaleOrderDetailsReport> getSaleOrderRealizedrReport(Date date, String branchCode) {

		List<SaleOrderDetailsReport> saleOrderReportList = new ArrayList();

		List<Object> obj = saleorderTransHdrRepository.getSaleOrderRealizedReport(date, branchCode);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderDetailsReport saleOrderReport = new SaleOrderDetailsReport();
			saleOrderReport.setCustomerName((String) objAray[0]);
			saleOrderReport.setCustomerPhoneNo((String) objAray[1]);
			saleOrderReport.setVoucherNo((String) objAray[2]);
			saleOrderReport.setConvertedToInvoice((String) objAray[3]);
			saleOrderReport.setPaidAmount((Double) objAray[4]);
			saleOrderReportList.add(saleOrderReport);
		}

		return saleOrderReportList;

	}

	@Override
	public List<SaleOrderinoiceReport> getSaleOrderInvoiceReport(Date date, String vouchernumber, String branchCode) {
		List<SaleOrderinoiceReport> saleOrderInvoiceReportList = new ArrayList();

		List<Object> obj = saleorderTransHdrRepository.getSaleOrderInvoiceReport(date, vouchernumber, branchCode);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderinoiceReport saleOrderReport = new SaleOrderinoiceReport();
			saleOrderReport.setSlNo(i + 1);
			saleOrderReport.setCompanyName((String) objAray[0]);
			saleOrderReport.setCompanyAddress((String) objAray[1]);
			saleOrderReport.setCompanyPhoneNo((String) objAray[2]);
			saleOrderReport.setCompanyPlace((String) objAray[3]);
			saleOrderReport.setCompanyGst((String) objAray[4]);
			saleOrderReport.setOrderNo((String) objAray[5]);
			saleOrderReport.setOrderDate((Date) objAray[6]);
			saleOrderReport.setDeliveryMode((String) objAray[7]);
			saleOrderReport.setCustomerAddress((String) objAray[9]);
			saleOrderReport.setCustomerPhoneNo((String) objAray[10]);
			
			String customerName = (String) objAray[8];
			customerName = customerName.replaceAll((String) objAray[10], "");
			
			saleOrderReport.setCustomerName(customerName);

			saleOrderReport.setItemName((String) objAray[11]);
			saleOrderReport.setRate((Double) objAray[12]);
			saleOrderReport.setGst((Double) objAray[13]);
			saleOrderReport.setUnitName((String) objAray[14]);
			saleOrderReport.setAmount((Double) objAray[15]);
			saleOrderReport.setGstCustomerName((String) objAray[16]);
			saleOrderReport.setOrderDueTime((String) objAray[17]);
			saleOrderReport.setOrderTimeMode((String) objAray[18]);
			saleOrderReport.setOrderTakerName((String) objAray[19]);
			saleOrderReport.setQty((Double) objAray[20]);
			saleOrderReport.setOrderBy((String) objAray[21]);
			saleOrderReport.setTotal((Double) objAray[22]);
			saleOrderReport.setBranchWebsite((String) objAray[23]);
			saleOrderReport.setBranchEmail((String) objAray[24]);
			saleOrderReport.setOrderMsg((String) objAray[25]);
			saleOrderReport.setCustomerPhoneNo2((String) objAray[26]);
			saleOrderInvoiceReportList.add(saleOrderReport);
		}

		return saleOrderInvoiceReportList;
	}

	@Override
	public List<SaleOrderDueReport> getSaleOrderDueReport(Date date, String vouchernumber, String branchcode) {

		List<SaleOrderDueReport> saleOrderDueReportList = new ArrayList();

		List<Object> obj = saleorderTransHdrRepository.getSaleOrderDueReport(date, vouchernumber, branchcode);

		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderDueReport saleOrderDueReport = new SaleOrderDueReport();

			saleOrderDueReport.setDueDate((Date) objAray[0]);
			saleOrderDueReport.setAdvance((Double) objAray[1]);
			saleOrderDueReport.setPaymentMode((String) objAray[2]);
			saleOrderDueReport.setBalance((Double) objAray[3]);
			saleOrderDueReport.setDay((String) objAray[4]);
			saleOrderDueReport.setOrderTimeMode((String) objAray[5]);
			String timeMode = saleOrderDueReport.getOrderTimeMode();
			saleOrderDueReport.setTime((String) objAray[6] + timeMode);
			saleOrderDueReport.setOtName((String) objAray[7]);
			saleOrderDueReport.setMessageToPrint((String) objAray[8]);
			saleOrderDueReportList.add(saleOrderDueReport);
		}

		return saleOrderDueReportList;
	}

	@Override
	public List<SaleOrderTax> getSaleOrderTax(String vouchernumber, Date date, String branchCode) {

		List<SaleOrderTax> saleOrderTaxList = new ArrayList();

		List<Object> obj = saleorderTransHdrRepository.getTaxSaleOrderTaxSummary(vouchernumber, date, branchCode);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderTax saleOrderTaxReport = new SaleOrderTax();

			saleOrderTaxReport.setSgst((Double) objAray[0]);
			saleOrderTaxReport.setCgst((Double) objAray[1]);
			Double totalTax = saleorderTransHdrRepository.toalTax(date, vouchernumber, branchCode);
			saleOrderTaxReport.setTotal(totalTax);
			saleOrderTaxList.add(saleOrderTaxReport);
		}

		return saleOrderTaxList;
	}

	@Override
	public List<SalesOrderAdvanceBalanceReport> getSaleOrderBalanceAdvanceReportByDate(Date date, String branchCode) {
		List<SalesOrderAdvanceBalanceReport> saleOrderAdvanceBalList = new ArrayList();

		List<Object> obj = saleorderTransHdrRepository.getSaleOrderBalanceAdvanceReportByDate(date, branchCode);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			SalesOrderAdvanceBalanceReport saleOrderAdvBal = new SalesOrderAdvanceBalanceReport();
			saleOrderAdvBal.setVoucherDate((Date) objAray[0]);
			saleOrderAdvBal.setVoucherNumber((String) objAray[1]);
			saleOrderAdvBal.setCustomerName((String) objAray[2]);
			saleOrderAdvBal.setOrderInvoiceTotal((Double) objAray[3]);
			saleOrderAdvBal.setAdvance((Double) objAray[4]);
			saleOrderAdvBal.setBalance((Double) objAray[5]);
			// -------------version 4.8
			saleOrderAdvBal.setNetTotal((Double) objAray[3]);
			// ------------version 4.8 end
			saleOrderAdvBal.setBranchName((String) objAray[6]);
			saleOrderAdvBal.setBranchAddress1((String) objAray[7]);
			saleOrderAdvBal.setBranchAddress2((String) objAray[8]);
			saleOrderAdvBal.setBranchGst((String) objAray[9]);
			saleOrderAdvBal.setBranchPhNo((String) objAray[10]);
			saleOrderAdvBal.setSodexo((Double) objAray[11]);
			saleOrderAdvBal.setCompanyName((String) objAray[12]);
			saleOrderAdvanceBalList.add(saleOrderAdvBal);

		}

		return saleOrderAdvanceBalList;
	}

	@Override
	public List<SaleOrderReport> getHomeDelivaryReportByDate(Date date, String branchCode) {

		List<SaleOrderReport> saleOrderHomeList = new ArrayList();

		List<Object> obj = saleorderTransHdrRepository.getHomeDelivaryReportByDate(date, branchCode);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderReport saleOrderHome = new SaleOrderReport();
			saleOrderHome.setSlNo(i + 1);
			saleOrderHome.setDueDate((Date) objAray[0]);
			saleOrderHome.setCustomerName((String) objAray[1]);
			saleOrderHome.setCustomerAddress((String) objAray[2]);
			saleOrderHome.setCustomerPhone((String) objAray[3]);
			saleOrderHome.setDueTime((String) objAray[4]);
			saleOrderHome.setAdvanceAmount((Double) objAray[5]);
			saleOrderHome.setInvoiceToatal((Double) objAray[6]);
			saleOrderHome.setBalance((Double) objAray[7]);
			saleOrderHome.setItemName((String) objAray[8]);
			saleOrderHome.setCompanyName((String) objAray[9]);
			saleOrderHome.setBranchName((String) objAray[10]);
			saleOrderHome.setBranchEmail((String) objAray[11]);
			saleOrderHome.setBranchWebsite((String) objAray[12]);
			saleOrderHome.setBranchAddress((String) objAray[13]);
			saleOrderHome.setBranchPhone((String) objAray[14]);
			saleOrderHome.setBranchState((String) objAray[15]);
			saleOrderHome.setVoucherNo((String) objAray[16]);
			saleOrderHomeList.add(saleOrderHome);

		}

		return saleOrderHomeList;
	}

	@Override
	public List<SaleOrderReport> getSaleOrderReportByDeliverBoy(Date date, String deliveryboyid, String branchCode) {

		List<SaleOrderReport> saleOrderAdvanceBalList = new ArrayList();

		List<Object> obj = saleorderTransHdrRepository.getSaleOrderReportByDeliverBoy(date, deliveryboyid, branchCode);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderReport saleOrderDelivery = new SaleOrderReport();
			saleOrderDelivery.setSlNo(i + 1);
			saleOrderDelivery.setDeliveryBoy((String) objAray[0]);
			saleOrderDelivery.setOrderBy((String) objAray[1]);
			saleOrderDelivery.setDueDate((Date) objAray[2]);
			saleOrderDelivery.setVoucherNo((String) objAray[3]);
			saleOrderDelivery.setCustomerName((String) objAray[4]);
			saleOrderDelivery.setInvoiceToatal((Double) objAray[5]);
			saleOrderDelivery.setBalance((Double) objAray[6]);
			saleOrderDelivery.setCompanyName((String) objAray[7]);
			saleOrderDelivery.setBranchName((String) objAray[8]);
			saleOrderDelivery.setBranchEmail((String) objAray[9]);
			saleOrderDelivery.setBranchWebsite((String) objAray[10]);
			saleOrderDelivery.setBranchAddress((String) objAray[11]);
			saleOrderDelivery.setBranchPhone((String) objAray[12]);
			saleOrderDelivery.setBranchState((String) objAray[13]);

			saleOrderAdvanceBalList.add(saleOrderDelivery);

		}

		return saleOrderAdvanceBalList;
	}

	@Override
	public List<SaleOrderReport> getSaleOrderReportByBranchCodeAndDate(Date date, String branchCode) {

		List<SaleOrderReport> saleOrderOrdersList = new ArrayList();

		List<Object> obj = saleorderTransHdrRepository.getSaleOrderItemDetailReport(date, branchCode);
		for (int i = 0; i < obj.size(); i++) {

			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderReport saleOrderOrder = new SaleOrderReport();
			saleOrderOrder.setSlNo(i + 1);
			saleOrderOrder.setVoucherNo((String) objAray[0]);
			saleOrderOrder.setCustomerName((String) objAray[1]);
			saleOrderOrder.setCustomerPhone((String) objAray[2]);
			saleOrderOrder.setDeliveryType((String) objAray[3]);
			saleOrderOrder.setDueTime((String) objAray[4]);
//		 saleOrderOrder.setItemName((String) objAray[5]);
			saleOrderOrder.setOrderMsgToPrint((String) objAray[5]);
			saleOrderOrder.setOrderAdvice((String) objAray[6]);
//		 saleOrderOrder.setQty((Double) objAray[8]);
			saleOrderOrder.setBalance((Double) objAray[7]);
			saleOrderOrder.setCompanyName((String) objAray[8]);
			saleOrderOrder.setBranchName((String) objAray[9]);
			saleOrderOrder.setBranchEmail((String) objAray[10]);
			saleOrderOrder.setBranchWebsite((String) objAray[11]);
			saleOrderOrder.setBranchAddress((String) objAray[12]);
			saleOrderOrder.setBranchPhone((String) objAray[13]);
			saleOrderOrder.setBranchState((String) objAray[14]);
			saleOrderOrder.setDueDate((Date) objAray[15]);
			saleOrderOrder.setInvoiceAmount((Double) objAray[16]);
			saleOrderOrder.setPaidAmount((Double) objAray[17]);
			saleOrderOrdersList.add(saleOrderOrder);

		}

		return saleOrderOrdersList;
	}

	@Override
	public List<SaleOrderDetailsReport> getPendinSaleOrderReport(Date date, Date tdate, String branchcode) {
		List<SaleOrderDetailsReport> saleOrderReportList = new ArrayList();
		List<Object> obj = saleorderTransHdrRepository.getPendinSaleOrderReport(date, tdate, branchcode);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderDetailsReport saleOrderReport = new SaleOrderDetailsReport();
			saleOrderReport.setCustomerName((String) objAray[0]);

			saleOrderReport.setVoucherNo((String) objAray[1]);
			saleOrderReport.setOrderDueTime((String) objAray[3]);
			saleOrderReport.setTotalAmount((Double) objAray[4]);
			saleOrderReport.setItemName((String) objAray[5]);
			saleOrderReport.setQty((Double) objAray[6]);
			saleOrderReport.setRate((Double) objAray[7]);
			saleOrderReport.setMrp((Double) objAray[8]);
			saleOrderReport.setAmount((Double) objAray[9]);
			saleOrderReport.setUnitName((String) objAray[10]);
			saleOrderReport.setDueDate((Date) objAray[2]);
			saleOrderReport.setDeliveryMode((String) objAray[11]);

			saleOrderReportList.add(saleOrderReport);
		}

		return saleOrderReportList;
	}

	@Override
	public List<SaleOrderReport> findBySalesOrderTransHdrVoucherDate(Date date, String companymstid) {

		List<SaleOrderReport> saleOrderReportList = new ArrayList();
		List<Object> obj = saleorderTransHdrRepository.getSaleOrderTakeAwayReport(date, companymstid);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
		
			
			SaleOrderReport saleOrderReport = new SaleOrderReport();
			
			saleOrderReport.setCustomerName((String) objAray[0]);
			saleOrderReport.setCustomerAddress((String) objAray[1]);
			saleOrderReport.setCustomerPhone((String) objAray[2]);
			saleOrderReport.setAdvanceAmount((Double) objAray[3]);
			saleOrderReport.setInvoiceToatal((Double) objAray[4]);
			
			
			if(null != (Double) objAray[3] && null != (Double) objAray[4])
			{
				Double balance = (Double) objAray[4]-(Double) objAray[3] ;
				saleOrderReport.setBalance(balance);

			}

			saleOrderReport.setVoucherNumber((String) objAray[5]);
			saleOrderReport.setItemName((String) objAray[6]);
			saleOrderReport.setQty((Double) objAray[7]);

			saleOrderReportList.add(saleOrderReport);
		}

		return saleOrderReportList;
	}
	@Override
	public List<SaleOrderReceiptReport> getSaleOrderReceiptByDateAndMode(Date date, String mode) {
		List<SaleOrderReceiptReport> saleOrderReceiptList = new ArrayList<SaleOrderReceiptReport>();
		List<Object> obj = saleorderReceiptRepo.getSaleOrderReceiptByDatendMode(date, mode);
		for(int i =0 ;i <obj.size();i++)
		{
			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderReceiptReport saleOrderReceiptReport= new SaleOrderReceiptReport();
			saleOrderReceiptReport.setReceiptAmount((Double) objAray[1]);
			saleOrderReceiptReport.setVoucherNumber((String) objAray[0]);
			saleOrderReceiptReport.setCustomerName((String) objAray[2]);
			saleOrderReceiptList.add(saleOrderReceiptReport);
			
			
		}
		return saleOrderReceiptList;
			
	}
	@Override
	public List<SaleOrderReport> getDeliveryBoyPendingReport(Date date, String companymstid,String deliveryboyid,
			String branchcode) {
		
		List<SaleOrderReport> saleOrderReportList = new ArrayList<SaleOrderReport>();
		
		List<Object> objList = saleorderTransHdrRepository.getDeliveryBoyPendingReport(date,companymstid,deliveryboyid,branchcode);

		System.out.println(objList.size());
		for (int i = 0; i < objList.size(); i++) {
			Object[] objAray = (Object[]) objList.get(i);
			
			SaleOrderReport saleOrderReport = new SaleOrderReport();
			
			saleOrderReport.setCustomerName((String) objAray[0]);
			saleOrderReport.setCustomerAddress((String) objAray[1]);
			saleOrderReport.setCustomerPhone((String) objAray[2]);
			saleOrderReport.setAccount((String) objAray[9]);

			
			saleOrderReport.setVoucherNo((String) objAray[3]);
//			saleOrderReport.setOrderNo((String) objAray[5]);
			saleOrderReport.setDueDate((Date) objAray[6]);
			saleOrderReport.setDueTime((String) objAray[7]+(String) objAray[8]);
			saleOrderReport.setInvoiceAmount((Double) objAray[4]);
			
			saleOrderReportList.add(saleOrderReport);
			
		}
		
		return saleOrderReportList;
	}

	@Override
	public List<SaleOrderReceiptReport> getSaleOrderReceiptByDateAndCard(Date vdate) {
		List<SaleOrderReceiptReport> saleOrderReceiptList = new ArrayList<SaleOrderReceiptReport>();
		List<Object> obj = saleorderReceiptRepo.getSaleOrderReceiptByDatendCard(vdate);
		for(int i =0 ;i <obj.size();i++)
		{
			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderReceiptReport saleOrderReceiptReport= new SaleOrderReceiptReport();
			saleOrderReceiptReport.setReceiptAmount((Double) objAray[1]);
			saleOrderReceiptReport.setVoucherNumber((String) objAray[0]);
			saleOrderReceiptReport.setCustomerName((String) objAray[2]);
			saleOrderReceiptList.add(saleOrderReceiptReport);
			
			
		}
		return saleOrderReceiptList;
	}

	@Override
	public List<SaleOrderReceiptReport> getSaleOrderReceiptPreviousCard(Date vdate) {
		List<SaleOrderReceiptReport> saleOrderReceiptList = new ArrayList<SaleOrderReceiptReport>();
		List<Object> obj = saleorderReceiptRepo.getSaleOrderReceiptPreviousCard(vdate);
		for(int i =0 ;i <obj.size();i++)
		{
			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderReceiptReport saleOrderReceiptReport= new SaleOrderReceiptReport();
			saleOrderReceiptReport.setReceiptAmount((Double) objAray[1]);
			saleOrderReceiptReport.setVoucherNumber((String) objAray[0]);
			saleOrderReceiptReport.setCustomerName((String) objAray[2]);
			saleOrderReceiptList.add(saleOrderReceiptReport);
			
			
		}
		return saleOrderReceiptList;
	}

	@Override
	public List<SaleOrderReport> getDailyCakeSettingReport(Date date, String categoryid, 
			String branchCode,String companymstid) {
		
		List<SaleOrderReport> dailyCakeSettingReport = new ArrayList<SaleOrderReport>();

		List<Object> objList = saleorderTransHdrRepository.DailyCakeSettingReport(date, categoryid, branchCode,
				companymstid);
		
		for(int i=0; i<objList.size(); i++)
		{
			SaleOrderReport saleOrderReport = new SaleOrderReport();
			Object[] objAray = (Object[]) objList.get(i);
			
			saleOrderReport.setCustomerName((String) objAray[0]);
			saleOrderReport.setCustomerPhone((String)objAray[1]);
			saleOrderReport.setVoucherNo((String)objAray[2]);
			saleOrderReport.setItemName((String)objAray[7]);
			saleOrderReport.setQty((Double) objAray[9]);
			saleOrderReport.setUnit((String)objAray[8]);
			String sdate = SystemSetting.UtilDateToString((Date) objAray[5]);
			saleOrderReport.setDueTime(sdate+(String)objAray[6]);
			saleOrderReport.setDescription((String)objAray[4]);
			saleOrderReport.setIngrediant((String)objAray[3]);
			
			dailyCakeSettingReport.add(saleOrderReport);
			

		}
		return dailyCakeSettingReport;
	}

	//------------------version 4.9

		@Override
		public List<SaleOrderReport> getSaleOrderHomeDeliveryReport(Date date, String branchCode, String companymstid) {
			List<SaleOrderReport> saleOrderReportList = new ArrayList<SaleOrderReport>();

			List<Object> objList = saleorderTransHdrRepository.SaleOrderHomeDeliveryReport(date, branchCode,
					companymstid);
			
			for(int i=0; i<objList.size(); i++)
			{
				SaleOrderReport saleOrderReport = new SaleOrderReport();
				Object[] objAray = (Object[]) objList.get(i);
				
				saleOrderReport.setCustomerName((String) objAray[0]);
				saleOrderReport.setCustomerPhone((String)objAray[1]);
				saleOrderReport.setVoucherNo((String)objAray[3]);
				String sdate = SystemSetting.UtilDateToString((Date) objAray[5]);
				saleOrderReport.setDueTime(sdate+(String)objAray[6]);
				saleOrderReport.setInvoiceAmount((Double)objAray[7]);
				saleOrderReport.setBalance((Double)objAray[9]);
				saleOrderReport.setAdvanceAmount((Double)objAray[8]);
				saleOrderReport.setCustomerAddress((String)objAray[2]);
				saleOrderReport.setDeliveryType((String)objAray[4]);
				saleOrderReport.setItemName((String)objAray[10]);
				saleOrderReportList.add(saleOrderReport);
				

			}
			
					return saleOrderReportList;
		}
		
		//------------------version 4.9 end
		//---------------version 4.10

		@Override
		public List<SaleOrderReport> getDailyOrderSummuryReport(Date date, String branchCode, String companymstid) {
			
			List<SaleOrderReport> saleOrderSummarytList = new ArrayList<SaleOrderReport>();

			List<Object> objList = saleorderTransHdrRepository.getDailyOrderSummuryReport(date, branchCode,
					companymstid);
			
			for(int i=0; i<objList.size(); i++)
			{
				SaleOrderReport saleOrderReport = new SaleOrderReport();
				Object[] objAray = (Object[]) objList.get(i);
				
				saleOrderReport.setItemName((String)objAray[0]);
				saleOrderReport.setQty((Double)objAray[1]);
				saleOrderReport.setUnit((String)objAray[2]);
				saleOrderSummarytList.add(saleOrderReport);
				

			}
			
			return saleOrderSummarytList;
		}

		@Override
		public List<SaleOrderReport> getSaleOrderConvertedReport(Date date, String branchCode, String companymstid) {
			List<SaleOrderReport> saleOrderSummarytList = new ArrayList<SaleOrderReport>();
			List<Object> objList = saleorderTransHdrRepository.getSaleOrderConvertedReport(date,branchCode);
			
			
			for(int i=0; i<objList.size(); i++)
			{
				SaleOrderReport saleOrderReport = new SaleOrderReport();
				Object[] objAray = (Object[]) objList.get(i);
				saleOrderReport.setCustomerName((String)objAray[2]);
				saleOrderReport.setInvoiceAmount((Double)objAray[1]);
				saleOrderReport.setVoucherDate((Date)objAray[0]);
				saleOrderReport.setPaymentMode((String)objAray[3]);
				saleOrderReport.setPaidAmount((Double)objAray[4]);
				saleOrderSummarytList.add(saleOrderReport);
			}
			return saleOrderSummarytList;
		}
		
		//---------------version 4.10 end

}
