package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.kafka.common.protocol.types.Field.Str;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.repository.UserMstRepository;

@Service
@Transactional
@Component
public class UnitServiceImpl  implements UnitMstService{
	
	@Autowired
	UnitMstRepository unitMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;

	@Override
	public List<Object> getUnitMst(String unitName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAllUnitMst(String pdfFileName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String initializeUnitMst(String branchcode, String companymstid) {
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		UnitMst unitMst = new UnitMst();
		
		unitMst =unitMstRepository.findByUnitNameAndCompanyMstId("NOS",companymstid);
		if (null == unitMst) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("NUMBERS");
			unitMsts.setUnitName("NOS");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU001" + companymstid);
			unitMsts.setCompanyMst(companyMst);
			unitMstRepository.saveAndFlush(unitMsts);

		}
		unitMst =unitMstRepository.findByUnitNameAndCompanyMstId("PKT",companymstid);

		if (null == unitMst) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("PACKETS");
			unitMsts.setUnitName("PKT");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU002" +companymstid);
			unitMsts.setCompanyMst(companyMst);

			unitMstRepository.saveAndFlush(unitMsts);
		}

		unitMst =unitMstRepository.findByUnitNameAndCompanyMstId("KGS",companymstid);

		if (null == unitMst) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("KILOGRAMS");
			unitMsts.setUnitName("KGS");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU003" + companymstid);
			unitMsts.setCompanyMst(companyMst);

			unitMstRepository.saveAndFlush(unitMsts);
		}

		unitMst =unitMstRepository.findByUnitNameAndCompanyMstId("GMS",companymstid);

		if (null == unitMst) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("GRAMS");
			unitMsts.setUnitName("GMS");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU004" + companymstid);
			unitMsts.setCompanyMst(companyMst);

			unitMstRepository.saveAndFlush(unitMsts);

		}

		unitMst =unitMstRepository.findByUnitNameAndCompanyMstId("ML",companymstid);

		if (null == unitMst) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("MILLILITRE");
			unitMsts.setUnitName("ML");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU005" + companymstid);
			unitMsts.setCompanyMst(companyMst);

			unitMstRepository.saveAndFlush(unitMsts);

		}

		unitMst =unitMstRepository.findByUnitNameAndCompanyMstId("LTRS",companymstid);

		if (null == unitMst) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("LITRE");
			unitMsts.setUnitName("LTRS");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU006" + companymstid);
			unitMsts.setCompanyMst(companyMst);

			unitMstRepository.saveAndFlush(unitMsts);

		}
		unitMst =unitMstRepository.findByUnitNameAndCompanyMstId("BOX",companymstid);

		if (null == unitMst) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("BOX");
			unitMsts.setUnitName("BOX");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU007" + companymstid);
			unitMsts.setCompanyMst(companyMst);

			unitMstRepository.saveAndFlush(unitMsts);

		}
		unitMst =unitMstRepository.findByUnitNameAndCompanyMstId("TIN",companymstid);

		if (null == unitMst) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("TIN");
			unitMsts.setUnitName("TIN");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU008" + companymstid);
			unitMsts.setCompanyMst(companyMst);

			unitMstRepository.saveAndFlush(unitMsts);

		}
		unitMst =unitMstRepository.findByUnitNameAndCompanyMstId("BTL",companymstid);

		if (null == unitMst) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("BOTTLE");
			unitMsts.setUnitName("BTL");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU009" +companymstid);
			unitMsts.setCompanyMst(companyMst);

			unitMstRepository.saveAndFlush(unitMsts);

		}
		
		unitMst =unitMstRepository.findByUnitNameAndCompanyMstId("PCS",companymstid);
		if (null == unitMst) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("PCS");
			unitMsts.setUnitName("PCS");
			unitMsts.setUnitPrecision(1.0);
			unitMsts.setId("SYU010" + companymstid);
			unitMsts.setCompanyMst(companyMst);

			unitMstRepository.saveAndFlush(unitMsts);

		}
		return "Success";
	}
	
	
}
