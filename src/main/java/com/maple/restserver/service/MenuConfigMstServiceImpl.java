package com.maple.restserver.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MenuConfigMst;
import com.maple.restserver.entity.MenuMst;
import com.maple.restserver.repository.MenuConfigMstRepository;
import com.maple.restserver.repository.MenuMstRepository;

@Service
public class MenuConfigMstServiceImpl implements MenuConfigMstService {

	@Autowired
	MenuConfigMstRepository menuConfigMstRepository;

	@Autowired
	MenuMstRepository menuMstRepository;

	@Override
	public String applicationDomain(CompanyMst companyMst, String applicationDomainType) {
		
	
		
/*		if(applicationDomainType.equalsIgnoreCase("HARDWARE")) {

			List<MenuConfigMst> menuConfigPurchaseMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("PURCHASE",
					companyMst);
			if (menuConfigPurchaseMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPurchaseMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigPurchaseMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}

			
			List<MenuConfigMst> menuConfigAddSupplier = menuConfigMstRepository.findByMenuNameAndCompanyMst("ADD SUPPLIER",
					companyMst);
			if (menuConfigAddSupplier.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAddSupplier.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigPurchaseMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}

				List<MenuConfigMst> menuConfigPhysicalStock = menuConfigMstRepository.findByMenuNameAndCompanyMst("PHYSICAL STOCK",
						companyMst);
				if (menuConfigPhysicalStock.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPhysicalStock.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPhysicalStock.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMstRepository.save(menuMst);

					}

				}
				

			

			List<MenuConfigMst> menuConfigDayEndMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("DAY END CLOSURE",
					companyMst);
			if (menuConfigDayEndMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDayEndMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigDayEndMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigCustMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("CUSTOMER",
					companyMst);
			if (menuConfigCustMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigCustMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigCustMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}
			List<MenuConfigMst> menuConfigAccountMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("ACCOUNTS",
					companyMst);
			if (menuConfigAccountMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAccountMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigAccountMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigWholeSaleMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("WHOLESALE",
					companyMst);
			if (menuConfigWholeSaleMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigWholeSaleMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigWholeSaleMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigPosMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("POS WINDOW",
					companyMst);
			if (menuConfigPosMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPosMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigPosMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigReportsMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("REPORTS",
					companyMst);
			if (menuConfigReportsMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReportsMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigReportsMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDailySalesReportMst = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("DAILY SALES REPORT", companyMst);
				if (menuConfigDailySalesReportMst.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDailySalesReportMst.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDailySalesReportMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			}

			
		}if(applicationDomainType.equals("ORGANIZATION")) {
			
			

			List<MenuConfigMst> menuConfigOrgMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("ORGANIZATION",
					companyMst);
			if (menuConfigOrgMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigOrgMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigOrgMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}

			
	


		}
		*/
		if(applicationDomainType.equals("PHARMACY")){
			

			List<MenuConfigMst> menuConfigMstPharmacySalesReturnReport = menuConfigMstRepository.findByMenuNameAndCompanyMst("PHARMACY SALES RETURN REPORT",
					companyMst);
			if (menuConfigMstPharmacySalesReturnReport.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstPharmacySalesReturnReport.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstPharmacySalesReturnReport.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			

			List<MenuConfigMst> menuConfigMstPharmacySaleOrder = menuConfigMstRepository.findByMenuNameAndCompanyMst("PHARMACY SALEORDER",
					companyMst);
			if (menuConfigMstPharmacySaleOrder.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstPharmacySaleOrder.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstPharmacySaleOrder.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			
			
			List<MenuConfigMst> menuConfigMstCustomerPharmacy = menuConfigMstRepository.findByMenuNameAndCompanyMst("CUSTOMER PHARMACY",
					companyMst);
			if (menuConfigMstCustomerPharmacy.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstCustomerPharmacy.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstCustomerPharmacy.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			
			
			List<MenuConfigMst> menuConfigMstWholeSalePharmacy = menuConfigMstRepository.findByMenuNameAndCompanyMst("WHOLESALE PHARMACY",
					companyMst);
			if (menuConfigMstWholeSalePharmacy.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstWholeSalePharmacy.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstWholeSalePharmacy.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			
			
			
			
			List<MenuConfigMst> menuConfigMstPharmacyPurchaseReport = menuConfigMstRepository.findByMenuNameAndCompanyMst("PHARMACY PURCHASE REPORT",
					companyMst);
			if (menuConfigMstPharmacyPurchaseReport.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstPharmacyPurchaseReport.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstPharmacyPurchaseReport.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			
			
			List<MenuConfigMst> menuConfigMstPharmacyItemOrCategoryMovenment = menuConfigMstRepository.findByMenuNameAndCompanyMst("PHARMACY ITEM OR CATEGORY MOVEMENT",
					companyMst);
			if (menuConfigMstPharmacyItemOrCategoryMovenment.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstPharmacyPurchaseReport.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstPharmacyItemOrCategoryMovenment.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			
			
			

			List<MenuConfigMst> menuConfigMstSalesReturnPharmacy = menuConfigMstRepository.findByMenuNameAndCompanyMst(("SALES RETURN PHARMACY"),
					companyMst);
			if (menuConfigMstSalesReturnPharmacy.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstSalesReturnPharmacy.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstSalesReturnPharmacy.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			List<MenuConfigMst> menuConfigMstAccountHead = menuConfigMstRepository.findByMenuNameAndCompanyMst(("ACCOUNT HEADS"),
					companyMst);
			if (menuConfigMstAccountHead.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstAccountHead.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstAccountHead.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigMstAccountingReports = menuConfigMstRepository.findByMenuNameAndCompanyMst(("ACCOUNTING REPORTS"),
					companyMst);
			if (menuConfigMstAccountingReports.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstAccountingReports.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstAccountingReports.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			List<MenuConfigMst> menuConfigMstStatementsOfAccount = menuConfigMstRepository.findByMenuNameAndCompanyMst(("STATEMENTS OF ACCOUNT"),
					companyMst);
			if (menuConfigMstStatementsOfAccount.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstStatementsOfAccount.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstStatementsOfAccount.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			List<MenuConfigMst> menuConfigMstAccountClosingBalance = menuConfigMstRepository.findByMenuNameAndCompanyMst(("ACCOUNT CLOSING BALANCE"),
					companyMst);
			if (menuConfigMstAccountClosingBalance.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstAccountClosingBalance.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstAccountClosingBalance.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			List<MenuConfigMst> menuConfigMstAccountMerge = menuConfigMstRepository.findByMenuNameAndCompanyMst(("ACCOUNT MERGE"),
					companyMst);
			if (menuConfigMstAccountMerge.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstAccountMerge.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstAccountMerge.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			List<MenuConfigMst> menuConfigMstOwnAccountSettilement = menuConfigMstRepository.findByMenuNameAndCompanyMst(("OWN ACCOUNT SETTLEMENT"),
					companyMst);
			if (menuConfigMstOwnAccountSettilement.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstOwnAccountSettilement.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstOwnAccountSettilement.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			List<MenuConfigMst> menuConfigMstAccountSlNoUpdation = menuConfigMstRepository.findByMenuNameAndCompanyMst(("ACCOUNT SL.NO UPDATION"),
					companyMst);
			if (menuConfigMstAccountSlNoUpdation.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstAccountSlNoUpdation.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstAccountSlNoUpdation.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			List<MenuConfigMst> menuConfigMstAddAccountHeads = menuConfigMstRepository.findByMenuNameAndCompanyMst(("ADD ACCOUNT HEADS"),
					companyMst);
			if (menuConfigMstAddAccountHeads.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstAddAccountHeads.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstAddAccountHeads.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			List<MenuConfigMst> menuConfigMstMonthlyAccountReceiptReport = menuConfigMstRepository.findByMenuNameAndCompanyMst(("MONTHLY ACCOUNT RECEIPT REPORT"),
					companyMst);
			if (menuConfigMstMonthlyAccountReceiptReport.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstMonthlyAccountReceiptReport.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstMonthlyAccountReceiptReport.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			List<MenuConfigMst> menuConfigMstMonthlyAccountPaymentReport = menuConfigMstRepository.findByMenuNameAndCompanyMst(("MONTHLY ACCOUNT PAYMENT REPORT"),
					companyMst);
			if (menuConfigMstMonthlyAccountPaymentReport.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstMonthlyAccountPaymentReport.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstMonthlyAccountPaymentReport.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigMstBatchWiseInventoryReport = menuConfigMstRepository.findByMenuNameAndCompanyMst(("BATCHWISE INVENTORY REPORT"),
					companyMst);
			if (menuConfigMstBatchWiseInventoryReport.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstBatchWiseInventoryReport.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstBatchWiseInventoryReport.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}	
			List<MenuConfigMst> menuConfigMstBatchPriceDefinition = menuConfigMstRepository.findByMenuNameAndCompanyMst(("BATCH PRICE DEFINITION"),
					companyMst);
			if (menuConfigMstBatchPriceDefinition.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstBatchPriceDefinition.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstBatchPriceDefinition.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}	
			List<MenuConfigMst> menuConfigMstCategorywiseStockMovementReport = menuConfigMstRepository.findByMenuNameAndCompanyMst(("CATEGORYWISE STOCK MOVEMENT REPORT"),
					companyMst);
			if (menuConfigMstCategorywiseStockMovementReport.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstCategorywiseStockMovementReport.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstCategorywiseStockMovementReport.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}	
			List<MenuConfigMst> menuConfigMstPharmacyIteamOrCategoryMovement = menuConfigMstRepository.findByMenuNameAndCompanyMst(("PHARMACY ITEM OR CATEGORY MOVEMENT"),
					companyMst);
			if (menuConfigMstPharmacyIteamOrCategoryMovement.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstPharmacyIteamOrCategoryMovement.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstPharmacyIteamOrCategoryMovement.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}	
			
			List<MenuConfigMst> menuConfigMstMultyCategoryUpdation = menuConfigMstRepository.findByMenuNameAndCompanyMst(("MULTY CATEGORY UPDATION"),
					companyMst);
			if (menuConfigMstMultyCategoryUpdation.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstMultyCategoryUpdation.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstMultyCategoryUpdation.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}	
			List<MenuConfigMst> menuConfigMstCategoryManagment = menuConfigMstRepository.findByMenuNameAndCompanyMst(("CATEGORY MANAGEMENT"),
					companyMst);
			if (menuConfigMstCategoryManagment.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstCategoryManagment.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstCategoryManagment.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			List<MenuConfigMst> menuConfigMstConsumption = menuConfigMstRepository.findByMenuNameAndCompanyMst(("CONSUMPTION"),
					companyMst);
			if (menuConfigMstConsumption.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstConsumption.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstConsumption.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}	
			List<MenuConfigMst> menuConfigMstInhouseConsumptionReport = menuConfigMstRepository.findByMenuNameAndCompanyMst(("INHOUSE CONSUMPTION REPORT"),
					companyMst);
			if (menuConfigMstInhouseConsumptionReport.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstInhouseConsumptionReport.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstInhouseConsumptionReport.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}	
			List<MenuConfigMst> menuConfigMstInhouseConsumptionReason = menuConfigMstRepository.findByMenuNameAndCompanyMst(("CONSUMPTION REASON"),
					companyMst);
			if (menuConfigMstInhouseConsumptionReason.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstInhouseConsumptionReason.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstInhouseConsumptionReason.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}	
			List<MenuConfigMst> menuConfigMstReasonWiseConsumptionReport = menuConfigMstRepository.findByMenuNameAndCompanyMst(("REASON WISE CONSUMPTION REPORT"),
					companyMst);
			if (menuConfigMstReasonWiseConsumptionReport.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstReasonWiseConsumptionReport.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstReasonWiseConsumptionReport.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}	
			
		}
		
		
		
		if(applicationDomainType.equals("HARDWARE")) {
			
			

			
			

			List<MenuConfigMst> menuConfigMstPos = menuConfigMstRepository.findByMenuNameAndCompanyMst("POS WINDOW",
					companyMst);
			if (menuConfigMstPos.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstPos.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstPos.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}
			

			List<MenuConfigMst> menuConfigReportsMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("REPORTS",
					companyMst);
			if (menuConfigReportsMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReportsMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigReportsMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}
			
			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStatementOfAcc = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("STATEMENTS OF ACCOUNT", companyMst);
				if (menuConfigStatementOfAcc.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStatementOfAcc.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStatementOfAcc.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigAllCustBillWiseBalanceReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ALL CUSTOMER BILL WISE BALANCE REPORT", companyMst);
				if (menuConfigAllCustBillWiseBalanceReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigAllCustBillWiseBalanceReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigAllCustBillWiseBalanceReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigCustBillWiseBalanceReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("CUSTOMER BILL WISE BALANCE REPORT", companyMst);
				if (menuConfigCustBillWiseBalanceReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCustBillWiseBalanceReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCustBillWiseBalanceReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierLedgerReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("CUSTOMER BILL WISE BALANCE REPORT", companyMst);
				if (menuConfigSupplierLedgerReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierLedgerReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierLedgerReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierLedgerReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SUPPLIER LEDGER REPORT", companyMst);
				if (menuConfigSupplierLedgerReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierLedgerReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierLedgerReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
		
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigReceiptInvoiceDtlReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RECEIPT INVOICE DETAIL REPORT", companyMst);
				if (menuConfigReceiptInvoiceDtlReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigReceiptInvoiceDtlReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReceiptInvoiceDtlReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigPaymentInvoiceDtlReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PAYMENT INVOICE DETAIL REPORT", companyMst);
				if (menuConfigPaymentInvoiceDtlReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPaymentInvoiceDtlReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPaymentInvoiceDtlReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigTrialBalanceReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("TRIAL BALANCE", companyMst);
				if (menuConfigTrialBalanceReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigTrialBalanceReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigTrialBalanceReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigBalanceSheeet = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("BALANCE SHEET", companyMst);
				if (menuConfigBalanceSheeet.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigBalanceSheeet.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigBalanceSheeet.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierDueReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SUPPLIER DUE REPORT", companyMst);
				if (menuConfigSupplierDueReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierDueReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierDueReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigCardReconcileReportReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("CARD RECONCILE REPORT", companyMst);
				if (menuConfigCardReconcileReportReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCardReconcileReportReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCardReconcileReportReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSundaryDebtorBalance = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SUNDRY DEBTOR BALANCE", companyMst);
				if (menuConfigSundaryDebtorBalance.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSundaryDebtorBalance.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSundaryDebtorBalance.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigTaxReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("TAX REPORT", companyMst);
				if (menuConfigTaxReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigTaxReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigTaxReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
		
		
		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDailySalesReportMst = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("DAILY SALES REPORT", companyMst);
				if (menuConfigDailySalesReportMst.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDailySalesReportMst.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDailySalesReportMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDayBookReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("DAY BOOK REPORT", companyMst);
				if (menuConfigDayBookReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDayBookReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDayBookReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigReceiptModeWiseReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RECEIPT MODE WISE REPORT", companyMst);
				if (menuConfigReceiptModeWiseReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigReceiptModeWiseReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReceiptModeWiseReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigHsnCodeSaleReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("HSN CODE SALE REPORT", companyMst);
				if (menuConfigHsnCodeSaleReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigHsnCodeSaleReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigHsnCodeSaleReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigItemOrCategorySaleReportReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ITEM OR CATEGORY SALE REPORT", companyMst);
				if (menuConfigItemOrCategorySaleReportReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigItemOrCategorySaleReportReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemOrCategorySaleReportReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigItemOrCategorySaleReportReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ITEM OR CATEGORY SALE REPORT", companyMst);
				if (menuConfigItemOrCategorySaleReportReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigItemOrCategorySaleReportReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemOrCategorySaleReportReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDailySalesReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("DAILY SALES REPORT", companyMst);
				if (menuConfigDailySalesReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDailySalesReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDailySalesReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSalesModewiseSummaryReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SALES MODEWISE SUMMARY REPORT", companyMst);
				if (menuConfigSalesModewiseSummaryReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSalesModewiseSummaryReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSalesModewiseSummaryReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigB2cSalesReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("B2C SALES REPORT", companyMst);
				if (menuConfigB2cSalesReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigB2cSalesReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigB2cSalesReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigB2bSalesReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("B2B SALES REPORT", companyMst);
				if (menuConfigB2bSalesReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigB2bSalesReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigB2bSalesReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDamageEntryReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("DAMAGE ENTRY REPORT", companyMst);
				if (menuConfigDamageEntryReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDamageEntryReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDamageEntryReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigBranchWiseProfit = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("BRANCH WISE PROFIT", companyMst);
				if (menuConfigBranchWiseProfit.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigBranchWiseProfit.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigBranchWiseProfit.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigPendingIntentReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PENDING INTENT REPORT", companyMst);
				if (menuConfigPendingIntentReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPendingIntentReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPendingIntentReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigProductCoversionReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PRODUCT CONVERSION REPORT", companyMst);
				if (menuConfigProductCoversionReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigProductCoversionReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProductCoversionReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigCustomerBillWiseDueDays= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"CUSTOMER BILLWISE BY DUE DAYS", companyMst);
				if (menuConfigCustomerBillWiseDueDays.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCustomerBillWiseDueDays.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCustomerBillWiseDueDays.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
		
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigCustomerSalesReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"CUSTOMER SALES REPORT", companyMst);
				if (menuConfigCustomerSalesReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCustomerSalesReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCustomerSalesReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSalesReturnReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SALES RETURN REPORT", companyMst);
				if (menuConfigSalesReturnReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSalesReturnReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSalesReturnReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
		
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStockTransferOutReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"STOCK TRANSFER OUT REPORT", companyMst);
				if (menuConfigStockTransferOutReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStockTransferOutReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockTransferOutReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStockTransferInReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"STOCK TRANSFER IN REPORT", companyMst);
				if (menuConfigStockTransferInReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStockTransferInReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockTransferInReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStockTransferDetailDeletedReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"STOCK TRANSFER DETAIL DELETED REPORT", companyMst);
				if (menuConfigStockTransferDetailDeletedReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStockTransferDetailDeletedReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockTransferDetailDeletedReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigVoucherReprints= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"VOUCHER REPRINTS", companyMst);
				if (menuConfigVoucherReprints.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigVoucherReprints.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigVoucherReprints.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigReceiptReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"RECEIPT REPORT", companyMst);
				if (menuConfigReceiptReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigReceiptReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReceiptReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigPaymentReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PAYMENT REPORTS", companyMst);
				if (menuConfigPaymentReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPaymentReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPaymentReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigPurchaseReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PURCHASE REPORT", companyMst);
				if (menuConfigPurchaseReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPurchaseReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPurchaseReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigHsnCodeWisePurchaseReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"HSNCODE WISE PURCHASE REPORT", companyMst);
				if (menuConfigHsnCodeWisePurchaseReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigHsnCodeWisePurchaseReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigHsnCodeWisePurchaseReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierMonthlySummary= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SUPPLIER MONTHLY SUMMARY", companyMst);
				if (menuConfigSupplierMonthlySummary.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierMonthlySummary.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierMonthlySummary.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierBillWiseDueDays= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SUPPLIER BILLWISE BY DUE DAYS", companyMst);
				if (menuConfigSupplierBillWiseDueDays.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierBillWiseDueDays.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierBillWiseDueDays.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigProductionPlaningReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PRODUCTION PLANNING REPORT", companyMst);
				if (menuConfigProductionPlaningReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigProductionPlaningReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProductionPlaningReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigRawMaterialRequiredReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"RAW MATERIAL REQUIRED REPORT", companyMst);
				if (menuConfigRawMaterialRequiredReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigRawMaterialRequiredReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigRawMaterialRequiredReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigRawMaterialIssueReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"RAW MATERIAL ISSUE REPORT", companyMst);
				if (menuConfigRawMaterialIssueReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigRawMaterialIssueReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigRawMaterialIssueReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigActualProductionReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ACTUAL PRODUCTION REPORT", companyMst);
				if (menuConfigActualProductionReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigActualProductionReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigActualProductionReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigActualProductionAndPlannningReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ACTUAL PRODUCTION AND PLANING SUMMARY", companyMst);
				if (menuConfigActualProductionAndPlannningReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigActualProductionAndPlannningReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigActualProductionAndPlannningReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigActualProductionAndPlannningReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ACTUAL PRODUCTION AND PLANING SUMMARY", companyMst);
				if (menuConfigActualProductionAndPlannningReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigActualProductionAndPlannningReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigActualProductionAndPlannningReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigConsumptionReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"CONSUMPTION REPORT", companyMst);
				if (menuConfigConsumptionReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigConsumptionReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigConsumptionReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigReasonWiseConsumptionReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"REASON WISE CONSUMPTION REPORT", companyMst);
				if (menuConfigReasonWiseConsumptionReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigReasonWiseConsumptionReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReasonWiseConsumptionReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigShortExpiryReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SHORT EXPIRY REPORT", companyMst);
				if (menuConfigShortExpiryReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigShortExpiryReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigShortExpiryReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigCategoryWiseShortExpiryReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"CATEGORYWISE SHORT EXPIRY REPORT", companyMst);
				if (menuConfigCategoryWiseShortExpiryReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCategoryWiseShortExpiryReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCategoryWiseShortExpiryReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigItemWiseShortExpiryReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ITEMWISE SHORT EXPIRY REPORT", companyMst);
				if (menuConfigItemWiseShortExpiryReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigItemWiseShortExpiryReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemWiseShortExpiryReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigItemExceptionReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ITEM EXCEPTION REPORT", companyMst);
				if (menuConfigItemExceptionReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigItemExceptionReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemExceptionReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigKitReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"KIT REPORT", companyMst);
				if (menuConfigKitReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigKitReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigKitReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
	
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSaleOrderReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"SALEORDER REPORT", companyMst);
				if (menuConfigSaleOrderReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSaleOrderReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSaleOrderReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSaleOrderReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"SALEORDER BALANCE ADVANCE REPORT", companyMst);
				if (menuConfigSaleOrderReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSaleOrderReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSaleOrderReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDelivaryBoySaleOrderReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"DELIVERYBOY SALEORDER REPORT", companyMst);
				if (menuConfigDelivaryBoySaleOrderReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDelivaryBoySaleOrderReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDelivaryBoySaleOrderReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
		
		
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDelivaryBoySaleOrderReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"DELIVERYBOY SALEORDER REPORT", companyMst);
				if (menuConfigDelivaryBoySaleOrderReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDelivaryBoySaleOrderReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDelivaryBoySaleOrderReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigHomeDelivaryTakeawayReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"HOMEDELIVERY OR TAKE AWAY REPORT", companyMst);
				if (menuConfigHomeDelivaryTakeawayReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigHomeDelivaryTakeawayReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigHomeDelivaryTakeawayReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSaleOrderPendingReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"SALEORDER PENDING REPORT", companyMst);
				if (menuConfigSaleOrderPendingReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSaleOrderPendingReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSaleOrderPendingReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDelivaryBoyPendingReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"DELIVERY BOY PENDING REPORT", companyMst);
				if (menuConfigDelivaryBoyPendingReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDelivaryBoyPendingReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDelivaryBoyPendingReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDelivaryBoyPendingReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"DELIVERY BOY PENDING REPORT", companyMst);
				if (menuConfigDelivaryBoyPendingReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDelivaryBoyPendingReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDelivaryBoyPendingReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDailyCakeSettingReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"DAILY CAKE SETTING", companyMst);
				if (menuConfigDailyCakeSettingReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDailyCakeSettingReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDailyCakeSettingReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigHomeDelivaryReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"HOMEDELIVERY REPORT", companyMst);
				if (menuConfigHomeDelivaryReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigHomeDelivaryReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigHomeDelivaryReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDailyOrderReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"DAILY ORDER REPORT", companyMst);
				if (menuConfigDailyOrderReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDailyOrderReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDailyOrderReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStockReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("STOCK REPORT", companyMst);
				if (menuConfigStockReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStockReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigItemStockReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ITEM STOCK REPORT", companyMst);
				if (menuConfigItemStockReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigItemStockReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemStockReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				
				
			}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigItemWiseStockMovementReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ITEMWISE STOCK MOVEMENT REPORT", companyMst);
				if (menuConfigItemWiseStockMovementReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigItemWiseStockMovementReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemWiseStockMovementReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigCategoryWiseStockMovementReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("CATEGORYWISE STOCK MOVEMENT REPORT", companyMst);
				if (menuConfigCategoryWiseStockMovementReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCategoryWiseStockMovementReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCategoryWiseStockMovementReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			
			
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigInventoryReorderStatusReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("INVENTORY REORDER STATUS REPORT", companyMst);
				if (menuConfigInventoryReorderStatusReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigInventoryReorderStatusReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigInventoryReorderStatusReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			
			
		}

		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigFastMovingItemReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("FAST MOVING ITEMS REPORT", companyMst);
				if (menuConfigFastMovingItemReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigFastMovingItemReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigFastMovingItemReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

				if (menuConfigReportsMst.size() > 0) {
					List<MenuConfigMst> menuConfigItemLocationReport= menuConfigMstRepository
							.findByMenuNameAndCompanyMst( 
									"ITEM LOCATION REPORT", companyMst);
					if (menuConfigItemLocationReport.size() > 0) {
						MenuMst menuMst = menuMstRepository
								.findByMenuIdAndCompanyMst(menuConfigFastMovingItemReport.get(0).getId(), companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigFastMovingItemReport.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigReportsMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
				
				
				
				
				
						
				
				
			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStockValueReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("STOCK VALUE REPORT", companyMst);
				if (menuConfigStockValueReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStockValueReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockValueReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				
				
			}
			
			
			
			
			
			
			List<MenuConfigMst> menuConfigAccountMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("ACCOUNTS",
					companyMst);
			if (menuConfigAccountMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAccountMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigAccountMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

				

			}	
			
			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigJournal = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("JOURNAL", companyMst);
				if (menuConfigJournal.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigJournal.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigJournal.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			}
			
			
			
			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierPayments = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SUPPLIER PAYMENTS", companyMst);
				if (menuConfigSupplierPayments.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierPayments.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierPayments.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			}
		
				
					


	
	



	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPettyCashPayments = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PETTY-CASH PAYMENTS", companyMst);
		if (menuConfigPettyCashPayments.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPettyCashPayments.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPettyCashPayments.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

	}
	
	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPettyCashReceipts = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PETTY-CASH RECIEPTS", companyMst);
		if (menuConfigPettyCashReceipts.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPettyCashReceipts.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPettyCashReceipts.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

	}

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPdcReconcile = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PDC RECONCILE", companyMst);
		if (menuConfigPdcReconcile.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPdcReconcile.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPdcReconcile.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
	}
	

	

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPaymentEdit = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PAYMENT EDIT", companyMst);
		if (menuConfigPaymentEdit.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPaymentEdit.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPaymentEdit.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

	}
	

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigReceiptEdit = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("RECEIPT EDIT", companyMst);
		if (menuConfigReceiptEdit.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigReceiptEdit.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigReceiptEdit.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
	}
	

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPdcReceipt = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PDC RECEIPT", companyMst);
		if (menuConfigPdcReceipt.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPdcReceipt.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPdcReceipt.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
	}
	

	
	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPdcPayment = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"PDC PAYMENT", companyMst);
		if (menuConfigPdcPayment.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPdcPayment.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPdcPayment.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

	}

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigJournalEdit = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"JOURNAL EDIT", companyMst);
		if (menuConfigJournalEdit.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigJournalEdit.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigJournalEdit.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		
		}
	}
	
	



	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigOwnAccountSettlement = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"OWN ACCOUNT SETTLEMENT", companyMst);
		if (menuConfigOwnAccountSettlement.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigOwnAccountSettlement.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigOwnAccountSettlement.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

	}

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigSupplierPayment = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"SUPPLIER PAYMENTS", companyMst);
		if (menuConfigSupplierPayment.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigSupplierPayment.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigSupplierPayment.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}}

	
	
	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigReceiptWindow = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"RECEIPT WINDOW", companyMst);
		if (menuConfigReceiptWindow.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigReceiptWindow.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigReceiptWindow.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		
		}
	}

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPayment = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"PAYMENT", companyMst);
		if (menuConfigPayment.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPayment.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPayment.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}
		}
		

	}

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigCustomerBilWiseAdjustment = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"CUSTOMER BILLWISE ADJUSTMENT", companyMst);
		if (menuConfigCustomerBilWiseAdjustment.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigCustomerBilWiseAdjustment.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigCustomerBilWiseAdjustment.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

	
	}
	
	List<MenuConfigMst> menuConfigAdvancedFeaturesMst = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("ADVANCED FEATURES", companyMst);
	if (menuConfigAdvancedFeaturesMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAdvancedFeaturesMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigAdvancedFeaturesMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}
	
	List<MenuConfigMst> menuConfigItemMaster = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("ITEM MASTER", companyMst);
	if (menuConfigAdvancedFeaturesMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemMaster.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigItemMaster.get(0).getId());
			menuMst.setCompanyMst(companyMst);
			menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
			menuMstRepository.save(menuMst);

		}

	}
	List<MenuConfigMst> menuConfigItemEdit = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("ITEM EDIT", companyMst);
	if (menuConfigAdvancedFeaturesMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemEdit.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigItemEdit.get(0).getId());
			menuMst.setCompanyMst(companyMst);
			menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
			menuMstRepository.save(menuMst);

		}

	}
	

	List<MenuConfigMst> menuConfigCategoryManagement = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("CATEGORY MANAGEMENT", companyMst);
	if (menuConfigAdvancedFeaturesMst.size() > 0) {
		List<MenuConfigMst> menuConfigTaxCreation = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("TAX CREATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigTaxCreation.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigTaxCreation.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		

		List<MenuConfigMst> menuConfigItemWiseTaxCreation = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("ITEMWISE TAX CREATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemWiseTaxCreation.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigItemWiseTaxCreation.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		

		List<MenuConfigMst> menuConfigPublishMessage = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PUBLISH MESSAGE", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPublishMessage.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPublishMessage.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

		

		List<MenuConfigMst> menuConfigKItDefinition = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("KIT DEFINITION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKItDefinition.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigKItDefinition.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigKotManager = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("KOT MANAGER", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKotManager.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigKotManager.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigStoreCreation = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("STORE CREATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigStoreCreation.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigStoreCreation.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigDayEnd= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("DAY END", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDayEnd.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigDayEnd.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigSiteCreation= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("SITE CREATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSiteCreation.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigSiteCreation.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}


		List<MenuConfigMst> menuConfigFinanceCreation= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("FINANCE CREATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigFinanceCreation.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigFinanceCreation.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigBarcodePrinting= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("BARCODE PRINTING", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBarcodePrinting.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigBarcodePrinting.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		
		List<MenuConfigMst> menuConfigBarcodeConfiguration= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("BARCODE CONFIGURATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBarcodeConfiguration.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigBarcodeConfiguration.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

		
		List<MenuConfigMst> menuConfigItemMerge= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("ITEM MERGE", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemMerge.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigItemMerge.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}



		List<MenuConfigMst> menuConfigSaleOrderStatus= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("SALE ORDER STATUS", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSaleOrderStatus.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigSaleOrderStatus.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigReOrder= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("ReORDER", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReOrder.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigReOrder.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		
		

		List<MenuConfigMst> menuConfigIteBranch= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("ITEM BRANCH", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigIteBranch.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigIteBranch.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}


		List<MenuConfigMst> menuConfigMultyUnit= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("MULTY UNIT", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMultyUnit.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigMultyUnit.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}


		List<MenuConfigMst> menuConfigProductConversionConfiguration= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PRODUCT CONVERSION CONFIGURATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProductConversionConfiguration.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigProductConversionConfiguration.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

		
		List<MenuConfigMst> menuConfigNutritionFacts= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("NUTRITION FACTS", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigNutritionFacts.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigNutritionFacts.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		
		
		List<MenuConfigMst> menuConfigNutritionPrint= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("NUTRITION PRINT", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigNutritionPrint.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigNutritionPrint.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigBarcodeNutritionCombained= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("BARCODE NUTRRITION COMBINED", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBarcodeNutritionCombained.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigBarcodeNutritionCombained.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}


		List<MenuConfigMst> menuConfigScheme= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("SCHEME", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigScheme.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigScheme.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}


		}
			List<MenuConfigMst> menuConfigSchemeDetails= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("SCHEME DETAILS", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSchemeDetails.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigSchemeDetails.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigSaleOrderEdits= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("SALES ORDER EDIT", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSaleOrderEdits.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigSaleOrderEdits.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigAddSupplier= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("ADD SUPPLIER", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAddSupplier.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigAddSupplier.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			
			List<MenuConfigMst> menuConfigBatchPriceDefinition= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("BATCH PRICE DEFINITION", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBatchPriceDefinition.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigAddSupplier.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}


			}

				
				List<MenuConfigMst> menuConfigPriceDefinition= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PRICE DEFINITION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPriceDefinition.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPriceDefinition.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigVoucherTyoe= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("VOUCHER TYPE", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigVoucherTyoe.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigVoucherTyoe.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigReceiptMode= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RECEIPT MODE", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReceiptMode.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReceiptMode.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
		
				List<MenuConfigMst> menuConfigKitchenCategoryDtl= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("KITCHEN CATEGORY DTL", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKitchenCategoryDtl.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigKitchenCategoryDtl.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
		
				

				List<MenuConfigMst> menuConfigKotCategoryPrinter= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("KOT CATEGORY PRINTER", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKotCategoryPrinter.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigKotCategoryPrinter.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigPrinter= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PRINTER", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPrinter.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPrinter.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigChangeassword= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("CHANGE PASSWORD", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigChangeassword.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigChangeassword.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigGroupPermission= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("GROUP PERMISSION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigGroupPermission.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigGroupPermission.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				

				List<MenuConfigMst> menuConfigGroupCreation= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("GROUP CREATION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigGroupCreation.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigGroupCreation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				

				List<MenuConfigMst> menuConfigProcessPermission= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PROCESS PERMISSION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProcessPermission.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProcessPermission.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				

				List<MenuConfigMst> menuConfigProcessCreation= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PROCESS CREATION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProcessCreation.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProcessCreation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				List<MenuConfigMst> menuConfigMachineResource= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("MACHINE RESOUCE", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMachineResource.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigMachineResource.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigResourceCategory= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RESOUCE CATEGORY", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigResourceCategory.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigResourceCategory.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				
				
				List<MenuConfigMst> menuConfigUserRegistration= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("USER REGISTRATION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigUserRegistration.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigUserRegistration.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigAddAccountHeads= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ADD ACCOUNT HEADS", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAddAccountHeads.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigAddAccountHeads.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
		
				
				List<MenuConfigMst> menuConfigBranchCreation= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"BRANCH CREATION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBranchCreation.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigBranchCreation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigFinancialYear= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"BRANCH CREATION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigFinancialYear.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigFinancialYear.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigCompanyCreation= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"COMPANY CREATION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigCompanyCreation.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCompanyCreation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigDBinitialize= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"DB INITIALIZE", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDBinitialize.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDBinitialize.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigPhysicalStock= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PHYSICAL STOCK", companyMst);
				if (menuConfigPhysicalStock.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPhysicalStock.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPhysicalStock.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				List<MenuConfigMst> menuConfigPurchaseOrder= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PURCHASE ORDER", companyMst);
				if (menuConfigPhysicalStock.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPurchaseOrder.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPurchaseOrder.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigPurchaseOrderManagement= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PURCHASE ORDER MANAGEMENT", companyMst);
				if (menuConfigPurchaseOrderManagement.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPurchaseOrderManagement.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPurchaseOrderManagement.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigOtherBranchSale= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"OTHER BRANCH SALE", companyMst);
				if (menuConfigPurchaseOrderManagement.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigOtherBranchSale.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigOtherBranchSale.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigDamageEntry= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"DAMAGE ENTRY", companyMst);
				if (menuConfigDamageEntry.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDamageEntry.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDamageEntry.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigUserRegisteredStock= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"USER REGISTERED STOCK", companyMst);
				if (menuConfigUserRegisteredStock.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigUserRegisteredStock.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigUserRegisteredStock.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				List<MenuConfigMst> menuConfigStoreChange= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"STORE CHANGE", companyMst);
				if (menuConfigStoreChange.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigStoreChange.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStoreChange.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				

				List<MenuConfigMst> menuConfigItemLocation= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ITEM LOCATION", companyMst);
				if (menuConfigItemLocation.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemLocation.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemLocation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigSalesReturn= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SALES RETURN", companyMst);
				if (menuConfigSalesReturn.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSalesReturn.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSalesReturn.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigSalesFromOrder= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SALES FROM ORDER", companyMst);
				if (menuConfigSalesFromOrder.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSalesFromOrder.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSalesFromOrder.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				List<MenuConfigMst> menuConfigStockVerification= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"STOCK VERIFICATION", companyMst);
				if (menuConfigStockVerification.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigStockVerification.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockVerification.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigPurchaseScheme= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PURCHASE SCHEME", companyMst);
				if (menuConfigStockVerification.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPurchaseScheme.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPurchaseScheme.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				

				List<MenuConfigMst> menuConfigIntent= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"INTENT", companyMst);
				if (menuConfigIntent.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigIntent.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigIntent.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigIntentToStockTransfer= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"INTENT TO STOCK TRANSFER", companyMst);
				if (menuConfigIntentToStockTransfer.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigIntentToStockTransfer.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigIntentToStockTransfer.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigTaskList= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"TASK LIST", companyMst);
				if (menuConfigTaskList.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigTaskList.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigTaskList.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigSessionEnd= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SESSION END", companyMst);
				if (menuConfigSessionEnd.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSessionEnd.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSessionEnd.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				

				List<MenuConfigMst> menuConfigProductConversion= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PRODUCT CONVERSION", companyMst);
				if (menuConfigProductConversion.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProductConversion.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProductConversion.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigLanguageMst= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"LANGUAGE MST", companyMst);
				if (menuConfigLanguageMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigLanguageMst.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigLanguageMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigItemInLanguage= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ITEN IN LANGUAGE", companyMst);
				if (menuConfigItemInLanguage.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemInLanguage.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemInLanguage.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigBarGraph= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"BAR GRAPH", companyMst);
				if (menuConfigBarGraph.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBarGraph.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigBarGraph.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				


				List<MenuConfigMst> menuConfigProductionValueGraph= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PRODUCTION VALUE GRAPH", companyMst);
				if (menuConfigProductionValueGraph.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProductionValueGraph.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProductionValueGraph.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigSetAnImage= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SET AN IMAGE", companyMst);
				if (menuConfigSetAnImage.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSetAnImage.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSetAnImage.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				List<MenuConfigMst> menuConfigTallyIntegration= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"TALLY INTEGRATION", companyMst);
				if (menuConfigTallyIntegration.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigTallyIntegration.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigTallyIntegration.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigStockCorrection= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"STOCK CORRECTION", companyMst);
				if (menuConfigStockCorrection.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigStockCorrection.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockCorrection.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigProductionQtyGraph= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PRODUCTION QTY GRAPH", companyMst);
				if (menuConfigProductionQtyGraph.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProductionQtyGraph.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProductionQtyGraph.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigReetry= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"RETRY", companyMst);
				if (menuConfigReetry.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReetry.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReetry.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				

				List<MenuConfigMst> menuConfigConsumptionReason= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"CONSUMPTION REASON", companyMst);
				if (menuConfigConsumptionReason.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigConsumptionReason.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigConsumptionReason.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigVoucherDateEdit= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"VOUCHER DATE EDIT", companyMst);
				if (menuConfigVoucherDateEdit.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigVoucherDateEdit.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigVoucherDateEdit.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				List<MenuConfigMst> menuConfigSubBranchSale= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SUB BRANCH SALE", companyMst);
				if (menuConfigVoucherDateEdit.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSubBranchSale.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSubBranchSale.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			
				
			
			
				List<MenuConfigMst> menuConfigExecuteSql= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"EXECUTE SQL", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigExecuteSql.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigExecuteSql.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
		}
	
	 
	List<MenuConfigMst> menuConfigKotMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("KOT POS",
			companyMst);
	if (menuConfigKotMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKotMst.get(0).getId(), companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigKotMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigWholeSaleMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("WHOLESALE",
			companyMst);
	if (menuConfigWholeSaleMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigWholeSaleMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigWholeSaleMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigPosMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("POS WINDOW",
			companyMst);
	if (menuConfigPosMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPosMst.get(0).getId(), companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigPosMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigTakeOrderMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("TAKE ORDER",
			companyMst);
	if (menuConfigTakeOrderMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigTakeOrderMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigTakeOrderMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigOnlineSalesMst = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("ONLINE SALES", companyMst);
	if (menuConfigOnlineSalesMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigOnlineSalesMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigOnlineSalesMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigProductionMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("PRODUCTION",
			companyMst);
	if (menuConfigProductionMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProductionMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigProductionMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	if (menuConfigProductionMst.size() > 0) {
		List<MenuConfigMst> menuConfigProductionPrePlanningMst = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PRODUCTION PRE PLANNING", companyMst);

		if (menuConfigProductionPrePlanningMst.size() > 0) {

			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigProductionPrePlanningMst.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigProductionPrePlanningMst.get(0).getId());
				menuMst.setParentId(menuConfigProductionMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);

				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigProductionPlanningMst = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PRODUCTION PLANNING", companyMst);

		if (menuConfigProductionPlanningMst.size() > 0) {

			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigProductionPlanningMst.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigProductionPlanningMst.get(0).getId());
				menuMst.setParentId(menuConfigProductionMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);

				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigActualProductionMst = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("ACTUAL PRODUCTION", companyMst);

		if (menuConfigActualProductionMst.size() > 0) {

			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigActualProductionMst.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigActualProductionMst.get(0).getId());
				menuMst.setParentId(menuConfigProductionMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);

				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigRawMaterialIssueMst = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("RAW MATERIAL ISSUE", companyMst);

		if (menuConfigRawMaterialIssueMst.size() > 0) {

			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigRawMaterialIssueMst.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigRawMaterialIssueMst.get(0).getId());
				menuMst.setParentId(menuConfigProductionMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);

				menuMstRepository.save(menuMst);

			}

		}
		
		
		List<MenuConfigMst> menuConfigRawMaterialReturnMst = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("RAW MATERIAL RETURN", companyMst);

		if (menuConfigRawMaterialReturnMst.size() > 0) {

			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigRawMaterialReturnMst.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigRawMaterialReturnMst.get(0).getId());
				menuMst.setParentId(menuConfigProductionMst.get(0).getId());
				menuMst.setCompanyMst(companyMst);

				menuMstRepository.save(menuMst);

			}

		}

	}

	List<MenuConfigMst> menuConfigSOMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("SALEORDER",
			companyMst);
	if (menuConfigSOMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSOMst.get(0).getId(), companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigSOMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigPurchaseMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("PURCHASE",
			companyMst);
	if (menuConfigPurchaseMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPurchaseMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigPurchaseMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigCustMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("CUSTOMER",
			companyMst);
	if (menuConfigCustMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigCustMst.get(0).getId(), companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigCustMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigAcceptStockMst = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("ACCEPT STOCK", companyMst);
	if (menuConfigAcceptStockMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAcceptStockMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigAcceptStockMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigSTMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("STOCK TRANSFER",
			companyMst);
	if (menuConfigSTMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSTMst.get(0).getId(), companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigSTMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigDayEndMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("DAY END CLOSURE",
			companyMst);
	if (menuConfigDayEndMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDayEndMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigDayEndMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigInvoiceEditMst = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("INVOICE EDIT", companyMst);
	if (menuConfigInvoiceEditMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigInvoiceEditMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigInvoiceEditMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigSOEditMst = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("SALES ORDER EDIT", companyMst);
	if (menuConfigSOEditMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSOEditMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigSOEditMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}


	
			
			
		}
		
		
	
		
		
	
		}
		
if(applicationDomainType.equals("ORGANIZATION")) {
			
			

			List<MenuConfigMst> menuConfigOrgMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("ORGANIZATION",
					companyMst);
			if (menuConfigOrgMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigOrgMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigOrgMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMstRepository.save(menuMst);

				}

			}
			
			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigIncomeAndExpenseReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("INCOME AND EXPENCE REPORT", companyMst);
				if (menuConfigIncomeAndExpenseReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigIncomeAndExpenseReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigIncomeAndExpenseReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}

			
	
			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigPayments = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PAYMENTS", companyMst);
				if (menuConfigPayments.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPayments.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPayments.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}


			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigReceipts = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RECEIPTS", companyMst);
				if (menuConfigReceipts.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigReceipts.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReceipts.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}


			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigReceipts = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RECEIPTS", companyMst);
				if (menuConfigReceipts.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigReceipts.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReceipts.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigSubgroupmember = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SUB GROUP MEMBER", companyMst);
				if (menuConfigSubgroupmember.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSubgroupmember.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSubgroupmember.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			

			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigSubgroupmember = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SUB GROUP MEMBER", companyMst);
				if (menuConfigSubgroupmember.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSubgroupmember.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSubgroupmember.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigMeetingMst = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("MEETING MST", companyMst);
				if (menuConfigMeetingMst.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigMeetingMst.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigMeetingMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigMeetingMenber = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("MEETING MEMBER", companyMst);
				if (menuConfigMeetingMenber.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigMeetingMenber.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigMeetingMenber.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigPasterMst = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PASTER MST", companyMst);
				if (menuConfigPasterMst.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPasterMst.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPasterMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			
			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigDueAmountReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("DUE AMOUNT REPORT", companyMst);
				if (menuConfigDueAmountReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDueAmountReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDueAmountReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			

			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigFamilyCreation = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("FAMILY CREATION", companyMst);
				if (menuConfigFamilyCreation.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigFamilyCreation.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigFamilyCreation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigMemberCreation = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("MEMBER CREATION", companyMst);
				if (menuConfigMemberCreation.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigMemberCreation.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigMemberCreation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigEventCreation = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("EVENT CRTEATION", companyMst);
				if (menuConfigEventCreation.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigEventCreation.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigEventCreation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigMeetingMember = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("MEETING MEMBER", companyMst);
				if (menuConfigMeetingMember.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigMeetingMember.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigMeetingMember.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			
			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigMonthlyAccountReceiptReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("MONTHLY ACCOUNT RECEIPT REPORT", companyMst);
				if (menuConfigMonthlyAccountReceiptReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigMonthlyAccountReceiptReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigMonthlyAccountReceiptReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			

			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigMonthlyAccountPaymentReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("MONTHLY ACCOUNT PAYMENT REPORT", companyMst);
				if (menuConfigMonthlyAccountPaymentReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigMonthlyAccountPaymentReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigMonthlyAccountPaymentReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigMemberwiseReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("	MEMBERWISE RECEIPT REPORT", companyMst);
				if (menuConfigMemberwiseReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigMemberwiseReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigMemberwiseReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
		

			if (menuConfigOrgMst.size() > 0) {
				List<MenuConfigMst> menuConfigMemberwiseReceiptReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ALL MEMBERWISE RECEIPT REPORTT", companyMst);
				if (menuConfigMemberwiseReceiptReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigMemberwiseReceiptReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigMemberwiseReceiptReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigOrgMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			
		}
		
		if(applicationDomainType.equals("BAKERY")) {
			
			

			List<MenuConfigMst> menuConfigMstPos = menuConfigMstRepository.findByMenuNameAndCompanyMst("POS WINDOW",
					companyMst);
			if (menuConfigMstPos.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstPos.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstPos.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}
			

			List<MenuConfigMst> menuConfigReportsMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("REPORTS",
					companyMst);
			if (menuConfigReportsMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReportsMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigReportsMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}
			
			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStatementOfAcc = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("STATEMENTS OF ACCOUNT", companyMst);
				if (menuConfigStatementOfAcc.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStatementOfAcc.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStatementOfAcc.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
				
				if (menuConfigReportsMst.size() > 0) {
					List<MenuConfigMst> menuConfigSalesProfitReport = menuConfigMstRepository
							.findByMenuNameAndCompanyMst("SALES PROFIT REPORT", companyMst);
					if (menuConfigSalesProfitReport.size() > 0) {
						MenuMst menuMst = menuMstRepository
								.findByMenuIdAndCompanyMst(menuConfigSalesProfitReport.get(0).getId(), companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigSalesProfitReport.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigReportsMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}
					}
				}
				
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigAllCustBillWiseBalanceReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ALL CUSTOMER BILL WISE BALANCE REPORT", companyMst);
				if (menuConfigAllCustBillWiseBalanceReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigAllCustBillWiseBalanceReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigAllCustBillWiseBalanceReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigCustBillWiseBalanceReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("CUSTOMER BILL WISE BALANCE REPORT", companyMst);
				if (menuConfigCustBillWiseBalanceReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCustBillWiseBalanceReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCustBillWiseBalanceReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierLedgerReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("CUSTOMER BILL WISE BALANCE REPORT", companyMst);
				if (menuConfigSupplierLedgerReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierLedgerReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierLedgerReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierLedgerReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SUPPLIER LEDGER REPORT", companyMst);
				if (menuConfigSupplierLedgerReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierLedgerReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierLedgerReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
		
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigReceiptInvoiceDtlReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RECEIPT INVOICE DETAIL REPORT", companyMst);
				if (menuConfigReceiptInvoiceDtlReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigReceiptInvoiceDtlReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReceiptInvoiceDtlReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigPaymentInvoiceDtlReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PAYMENT INVOICE DETAIL REPORT", companyMst);
				if (menuConfigPaymentInvoiceDtlReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPaymentInvoiceDtlReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPaymentInvoiceDtlReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigTrialBalanceReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("TRIAL BALANCE", companyMst);
				if (menuConfigTrialBalanceReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigTrialBalanceReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigTrialBalanceReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigBalanceSheeet = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("BALANCE SHEET", companyMst);
				if (menuConfigBalanceSheeet.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigBalanceSheeet.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigBalanceSheeet.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierDueReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SUPPLIER DUE REPORT", companyMst);
				if (menuConfigSupplierDueReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierDueReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierDueReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigCardReconcileReportReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("CARD RECONCILE REPORT", companyMst);
				if (menuConfigCardReconcileReportReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCardReconcileReportReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCardReconcileReportReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSundaryDebtorBalance = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SUNDRY DEBTOR BALANCE", companyMst);
				if (menuConfigSundaryDebtorBalance.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSundaryDebtorBalance.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSundaryDebtorBalance.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigTaxReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("TAX REPORT", companyMst);
				if (menuConfigTaxReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigTaxReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigTaxReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			}
		
		
		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDailySalesReportMst = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("DAILY SALES REPORT", companyMst);
				if (menuConfigDailySalesReportMst.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDailySalesReportMst.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDailySalesReportMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDayBookReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("DAY BOOK REPORT", companyMst);
				if (menuConfigDayBookReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDayBookReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDayBookReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigReceiptModeWiseReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RECEIPT MODE WISE REPORT", companyMst);
				if (menuConfigReceiptModeWiseReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigReceiptModeWiseReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReceiptModeWiseReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigHsnCodeSaleReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("HSN CODE SALE REPORT", companyMst);
				if (menuConfigHsnCodeSaleReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigHsnCodeSaleReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigHsnCodeSaleReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigItemOrCategorySaleReportReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ITEM OR CATEGORY SALE REPORT", companyMst);
				if (menuConfigItemOrCategorySaleReportReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigItemOrCategorySaleReportReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemOrCategorySaleReportReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigItemOrCategorySaleReportReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ITEM OR CATEGORY SALE REPORT", companyMst);
				if (menuConfigItemOrCategorySaleReportReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigItemOrCategorySaleReportReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemOrCategorySaleReportReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDailySalesReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("DAILY SALES REPORT", companyMst);
				if (menuConfigDailySalesReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDailySalesReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDailySalesReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSalesModewiseSummaryReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SALES MODEWISE SUMMARY REPORT", companyMst);
				if (menuConfigSalesModewiseSummaryReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSalesModewiseSummaryReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSalesModewiseSummaryReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigB2cSalesReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("B2C SALES REPORT", companyMst);
				if (menuConfigB2cSalesReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigB2cSalesReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigB2cSalesReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigB2bSalesReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("B2B SALES REPORT", companyMst);
				if (menuConfigB2bSalesReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigB2bSalesReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigB2bSalesReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDamageEntryReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("DAMAGE ENTRY REPORT", companyMst);
				if (menuConfigDamageEntryReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDamageEntryReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDamageEntryReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigBranchWiseProfit = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("BRANCH WISE PROFIT", companyMst);
				if (menuConfigBranchWiseProfit.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigBranchWiseProfit.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigBranchWiseProfit.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigPendingIntentReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PENDING INTENT REPORT", companyMst);
				if (menuConfigPendingIntentReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPendingIntentReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPendingIntentReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigProductCoversionReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PRODUCT CONVERSION REPORT", companyMst);
				if (menuConfigProductCoversionReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigProductCoversionReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProductCoversionReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigCustomerBillWiseDueDays= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"CUSTOMER BILLWISE BY DUE DAYS", companyMst);
				if (menuConfigCustomerBillWiseDueDays.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCustomerBillWiseDueDays.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCustomerBillWiseDueDays.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
		
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigCustomerSalesReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"CUSTOMER SALES REPORT", companyMst);
				if (menuConfigCustomerSalesReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCustomerSalesReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCustomerSalesReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSalesReturnReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SALES RETURN REPORT", companyMst);
				if (menuConfigSalesReturnReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSalesReturnReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSalesReturnReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
		
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStockTransferOutReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"STOCK TRANSFER OUT REPORT", companyMst);
				if (menuConfigStockTransferOutReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStockTransferOutReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockTransferOutReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStockTransferInReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"STOCK TRANSFER IN REPORT", companyMst);
				if (menuConfigStockTransferInReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStockTransferInReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockTransferInReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStockTransferDetailDeletedReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"STOCK TRANSFER DETAIL DELETED REPORT", companyMst);
				if (menuConfigStockTransferDetailDeletedReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStockTransferDetailDeletedReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockTransferDetailDeletedReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigVoucherReprints= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"VOUCHER REPRINTS", companyMst);
				if (menuConfigVoucherReprints.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigVoucherReprints.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigVoucherReprints.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigReceiptReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"RECEIPT REPORT", companyMst);
				if (menuConfigReceiptReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigReceiptReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReceiptReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigPaymentReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PAYMENT REPORTS", companyMst);
				if (menuConfigPaymentReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPaymentReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPaymentReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigPaymentReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PAYMENT REPORT", companyMst);
				if (menuConfigPaymentReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPaymentReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPaymentReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigPurchaseReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PURCHASE REPORT", companyMst);
				if (menuConfigPurchaseReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPurchaseReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPurchaseReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigHsnCodeWisePurchaseReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"HSNCODE WISE PURCHASE REPORT", companyMst);
				if (menuConfigHsnCodeWisePurchaseReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigHsnCodeWisePurchaseReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigHsnCodeWisePurchaseReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierMonthlySummary= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SUPPLIER MONTHLY SUMMARY", companyMst);
				if (menuConfigSupplierMonthlySummary.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierMonthlySummary.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierMonthlySummary.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierBillWiseDueDays= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SUPPLIER BILLWISE BY DUE DAYS", companyMst);
				if (menuConfigSupplierBillWiseDueDays.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierBillWiseDueDays.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierBillWiseDueDays.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigShortExpiryReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SHORT EXPIRY REPORT", companyMst);
				if (menuConfigShortExpiryReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigShortExpiryReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigShortExpiryReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigCategoryWiseShortExpiryReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"CATEGORYWISE SHORT EXPIRY REPORT", companyMst);
				if (menuConfigCategoryWiseShortExpiryReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCategoryWiseShortExpiryReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCategoryWiseShortExpiryReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigItemWiseShortExpiryReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ITEMWISE SHORT EXPIRY REPORT", companyMst);
				if (menuConfigItemWiseShortExpiryReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigItemWiseShortExpiryReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemWiseShortExpiryReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigItemExceptionReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ITEM EXCEPTION REPORT", companyMst);
				if (menuConfigItemExceptionReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigItemExceptionReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemExceptionReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigKitReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"KIT REPORT", companyMst);
				if (menuConfigKitReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigKitReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigKitReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
	
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSaleOrderReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"SALEORDER REPORT", companyMst);
				if (menuConfigSaleOrderReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSaleOrderReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSaleOrderReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSaleOrderReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"SALEORDER BALANCE ADVANCE REPORT", companyMst);
				if (menuConfigSaleOrderReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSaleOrderReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSaleOrderReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDelivaryBoySaleOrderReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"DELIVERYBOY SALEORDER REPORT", companyMst);
				if (menuConfigDelivaryBoySaleOrderReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDelivaryBoySaleOrderReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDelivaryBoySaleOrderReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
		
		
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDelivaryBoySaleOrderReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"DELIVERYBOY SALEORDER REPORT", companyMst);
				if (menuConfigDelivaryBoySaleOrderReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDelivaryBoySaleOrderReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDelivaryBoySaleOrderReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigHomeDelivaryTakeawayReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"HOMEDELIVERY OR TAKE AWAY REPORT", companyMst);
				if (menuConfigHomeDelivaryTakeawayReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigHomeDelivaryTakeawayReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigHomeDelivaryTakeawayReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigSaleOrderPendingReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"SALEORDER PENDING REPORT", companyMst);
				if (menuConfigSaleOrderPendingReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSaleOrderPendingReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSaleOrderPendingReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDelivaryBoyPendingReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"DELIVERY BOY PENDING REPORT", companyMst);
				if (menuConfigDelivaryBoyPendingReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDelivaryBoyPendingReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDelivaryBoyPendingReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDelivaryBoyPendingReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"DELIVERY BOY PENDING REPORT", companyMst);
				if (menuConfigDelivaryBoyPendingReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDelivaryBoyPendingReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDelivaryBoyPendingReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDailyCakeSettingReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"DAILY CAKE SETTING", companyMst);
				if (menuConfigDailyCakeSettingReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDailyCakeSettingReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDailyCakeSettingReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigHomeDelivaryReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"HOMEDELIVERY REPORT", companyMst);
				if (menuConfigHomeDelivaryReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigHomeDelivaryReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigHomeDelivaryReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDailyOrderReport = menuConfigMstRepository
						.findByMenuNameAndCompanyMst( 
								"DAILY ORDER REPORT", companyMst);
				if (menuConfigDailyOrderReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDailyOrderReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDailyOrderReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStockReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("STOCK REPORT", companyMst);
				if (menuConfigStockReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStockReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigItemStockReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ITEM STOCK REPORT", companyMst);
				if (menuConfigItemStockReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigItemStockReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemStockReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				
				
			}
			}
			
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigItemWiseStockMovementReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ITEMWISE STOCK MOVEMENT REPORT", companyMst);
				if (menuConfigItemWiseStockMovementReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigItemWiseStockMovementReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemWiseStockMovementReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigCategoryWiseStockMovementReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("CATEGORYWISE STOCK MOVEMENT REPORT", companyMst);
				if (menuConfigCategoryWiseStockMovementReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCategoryWiseStockMovementReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCategoryWiseStockMovementReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			
			
			}

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigInventoryReorderStatusReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("INVENTORY REORDER STATUS REPORT", companyMst);
				if (menuConfigInventoryReorderStatusReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigInventoryReorderStatusReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigInventoryReorderStatusReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			
			
		}

		
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigFastMovingItemReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("FAST MOVING ITEMS REPORT", companyMst);
				if (menuConfigFastMovingItemReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigFastMovingItemReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigFastMovingItemReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

				if (menuConfigReportsMst.size() > 0) {
					List<MenuConfigMst> menuConfigItemLocationReport= menuConfigMstRepository
							.findByMenuNameAndCompanyMst( 
									"ITEM LOCATION REPORT", companyMst);
					if (menuConfigItemLocationReport.size() > 0) {
						MenuMst menuMst = menuMstRepository
								.findByMenuIdAndCompanyMst(menuConfigFastMovingItemReport.get(0).getId(), companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigFastMovingItemReport.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigReportsMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
				
				
				
				
				
						
				
				
			}
			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStockValueReport= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("STOCK VALUE REPORT", companyMst);
				if (menuConfigStockValueReport.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStockValueReport.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockValueReport.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				
				
			}
			
			
			
			
			
			
			List<MenuConfigMst> menuConfigAccountMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("ACCOUNTS",
					companyMst);
			if (menuConfigAccountMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAccountMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigAccountMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

				

			}	
			
			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigJournal = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("JOURNAL", companyMst);
				if (menuConfigJournal.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigJournal.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigJournal.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			}
			
			
			
			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierPayments = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SUPPLIER PAYMENTS", companyMst);
				if (menuConfigSupplierPayments.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierPayments.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierPayments.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			}
		
				
					


	
	



	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPettyCashPayments = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PETTY-CASH PAYMENTS", companyMst);
		if (menuConfigPettyCashPayments.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPettyCashPayments.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPettyCashPayments.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

	}
	
	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPettyCashReceipts = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PETTY-CASH RECIEPTS", companyMst);
		if (menuConfigPettyCashReceipts.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPettyCashReceipts.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPettyCashReceipts.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

	}

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPdcReconcile = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PDC RECONCILE", companyMst);
		if (menuConfigPdcReconcile.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPdcReconcile.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPdcReconcile.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
	}
	

	

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPaymentEdit = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PAYMENT EDIT", companyMst);
		if (menuConfigPaymentEdit.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPaymentEdit.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPaymentEdit.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

	}
	

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigReceiptEdit = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("RECEIPT EDIT", companyMst);
		if (menuConfigReceiptEdit.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigReceiptEdit.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigReceiptEdit.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
	}
	

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPdcReceipt = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PDC RECEIPT", companyMst);
		if (menuConfigPdcReceipt.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPdcReceipt.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPdcReceipt.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
	}
	

	
	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPdcPayment = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"PDC PAYMENT", companyMst);
		if (menuConfigPdcPayment.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPdcPayment.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPdcPayment.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

	}

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigJournalEdit = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"JOURNAL EDIT", companyMst);
		if (menuConfigJournalEdit.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigJournalEdit.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigJournalEdit.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		
		}
	}
	
	



	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigOwnAccountSettlement = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"OWN ACCOUNT SETTLEMENT", companyMst);
		if (menuConfigOwnAccountSettlement.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigOwnAccountSettlement.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigOwnAccountSettlement.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

	}

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigSupplierPayment = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"SUPPLIER PAYMENTS", companyMst);
		if (menuConfigSupplierPayment.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigSupplierPayment.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigSupplierPayment.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}}

	
	
	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigReceiptWindow = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"RECEIPT WINDOW", companyMst);
		if (menuConfigReceiptWindow.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigReceiptWindow.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigReceiptWindow.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		
		}
	}

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigPayment = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"PAYMENT", companyMst);
		if (menuConfigPayment.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigPayment.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPayment.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}
		}
		

	}

	if (menuConfigAccountMst.size() > 0) {
		List<MenuConfigMst> menuConfigCustomerBilWiseAdjustment = menuConfigMstRepository
				.findByMenuNameAndCompanyMst(
									"CUSTOMER BILLWISE ADJUSTMENT", companyMst);
		if (menuConfigCustomerBilWiseAdjustment.size() > 0) {
			MenuMst menuMst = menuMstRepository
					.findByMenuIdAndCompanyMst(menuConfigCustomerBilWiseAdjustment.get(0).getId(), companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigCustomerBilWiseAdjustment.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAccountMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

	
	}
	
	List<MenuConfigMst> menuConfigAdvancedFeaturesMst = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("ADVANCED FEATURES", companyMst);
	if (menuConfigAdvancedFeaturesMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAdvancedFeaturesMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigAdvancedFeaturesMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}
	
	List<MenuConfigMst> menuConfigItemMaster = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("ITEM MASTER", companyMst);
	if (menuConfigAdvancedFeaturesMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemMaster.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigItemMaster.get(0).getId());
			menuMst.setCompanyMst(companyMst);
			menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
			menuMstRepository.save(menuMst);

		}

	}
	List<MenuConfigMst> menuConfigItemEdit = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("ITEM EDIT", companyMst);
	if (menuConfigAdvancedFeaturesMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemEdit.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigItemEdit.get(0).getId());
			menuMst.setCompanyMst(companyMst);
			menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
			menuMstRepository.save(menuMst);

		}

	}
	

	List<MenuConfigMst> menuConfigCategoryManagement = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("CATEGORY MANAGEMENT", companyMst);
	if (menuConfigAdvancedFeaturesMst.size() > 0) {
		List<MenuConfigMst> menuConfigTaxCreation = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("TAX CREATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigTaxCreation.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigTaxCreation.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		

		List<MenuConfigMst> menuConfigItemWiseTaxCreation = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("ITEMWISE TAX CREATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemWiseTaxCreation.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigItemWiseTaxCreation.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		

		List<MenuConfigMst> menuConfigPublishMessage = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PUBLISH MESSAGE", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPublishMessage.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigPublishMessage.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

		

		List<MenuConfigMst> menuConfigKItDefinition = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("KIT DEFINITION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKItDefinition.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigKItDefinition.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigKotManager = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("KOT MANAGER", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKotManager.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigKotManager.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigStoreCreation = menuConfigMstRepository
				.findByMenuNameAndCompanyMst("STORE CREATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigStoreCreation.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigStoreCreation.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigDayEnd= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("DAY END", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDayEnd.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigDayEnd.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

		List<MenuConfigMst> menuConfigSiteCreation= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("SITE CREATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSiteCreation.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigSiteCreation.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}


		List<MenuConfigMst> menuConfigFinanceCreation= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("FINANCE CREATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigFinanceCreation.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigFinanceCreation.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigBarcodePrinting= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("BARCODE PRINTING", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBarcodePrinting.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigBarcodePrinting.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		
		List<MenuConfigMst> menuConfigBarcodeConfiguration= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("BARCODE CONFIGURATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBarcodeConfiguration.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigBarcodeConfiguration.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

		
		List<MenuConfigMst> menuConfigItemMerge= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("ITEM MERGE", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemMerge.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigItemMerge.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}



		List<MenuConfigMst> menuConfigSaleOrderStatus= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("SALE ORDER STATUS", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSaleOrderStatus.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigSaleOrderStatus.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigReOrder= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("ReORDER", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReOrder.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigReOrder.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		
		

		List<MenuConfigMst> menuConfigIteBranch= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("ITEM BRANCH", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigIteBranch.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigIteBranch.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}


		List<MenuConfigMst> menuConfigMultyUnit= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("MULTY UNIT", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMultyUnit.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigMultyUnit.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}


		List<MenuConfigMst> menuConfigProductConversionConfiguration= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("PRODUCT CONVERSION CONFIGURATION", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProductConversionConfiguration.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigProductConversionConfiguration.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}

		
		List<MenuConfigMst> menuConfigNutritionFacts= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("NUTRITION FACTS", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigNutritionFacts.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigNutritionFacts.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		
		
		List<MenuConfigMst> menuConfigNutritionPrint= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("NUTRITION PRINT", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigNutritionPrint.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigNutritionPrint.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}
		
		List<MenuConfigMst> menuConfigBarcodeNutritionCombained= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("BARCODE NUTRRITION COMBINED", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBarcodeNutritionCombained.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigBarcodeNutritionCombained.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}

		}


		List<MenuConfigMst> menuConfigScheme= menuConfigMstRepository
				.findByMenuNameAndCompanyMst("SCHEME", companyMst);
		if (menuConfigAdvancedFeaturesMst.size() > 0) {
			MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigScheme.get(0).getId(),
					companyMst);
			if (null == menuMst) {
				menuMst = new MenuMst();
				menuMst.setMenuId(menuConfigScheme.get(0).getId());
				menuMst.setCompanyMst(companyMst);
				menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
				menuMstRepository.save(menuMst);

			}


		}
			List<MenuConfigMst> menuConfigSchemeDetails= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("SCHEME DETAILS", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSchemeDetails.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigSchemeDetails.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigSaleOrderEdits= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("SALES ORDER EDIT", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSaleOrderEdits.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigSaleOrderEdits.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigAddSupplier= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("ADD SUPPLIER", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAddSupplier.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigAddSupplier.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			
			List<MenuConfigMst> menuConfigBatchPriceDefinition= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("BATCH PRICE DEFINITION", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBatchPriceDefinition.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigAddSupplier.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}


			}

				
				List<MenuConfigMst> menuConfigPriceDefinition= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PRICE DEFINITION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPriceDefinition.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPriceDefinition.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigVoucherTyoe= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("VOUCHER TYPE", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigVoucherTyoe.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigVoucherTyoe.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigReceiptMode= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RECEIPT MODE", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReceiptMode.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReceiptMode.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
		
				List<MenuConfigMst> menuConfigKitchenCategoryDtl= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("KITCHEN CATEGORY DTL", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKitchenCategoryDtl.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigKitchenCategoryDtl.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
		
				

				List<MenuConfigMst> menuConfigKotCategoryPrinter= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("KOT CATEGORY PRINTER", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKotCategoryPrinter.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigKotCategoryPrinter.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigPrinter= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PRINTER", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPrinter.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPrinter.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigChangeassword= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("CHANGE PASSWORD", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigChangeassword.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigChangeassword.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigGroupPermission= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("GROUP PERMISSION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigGroupPermission.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigGroupPermission.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				

				List<MenuConfigMst> menuConfigGroupCreation= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("GROUP CREATION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigGroupCreation.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigGroupCreation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				

				List<MenuConfigMst> menuConfigProcessPermission= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PROCESS PERMISSION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProcessPermission.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProcessPermission.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				

				List<MenuConfigMst> menuConfigProcessCreation= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PROCESS CREATION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProcessCreation.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProcessCreation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				List<MenuConfigMst> menuConfigMachineResource= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("MACHINE RESOUCE", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMachineResource.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigMachineResource.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigResourceCategory= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RESOUCE CATEGORY", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigResourceCategory.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigResourceCategory.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				
				
				List<MenuConfigMst> menuConfigUserRegistration= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("USER REGISTRATION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigUserRegistration.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigUserRegistration.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigAddAccountHeads= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ADD ACCOUNT HEADS", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAddAccountHeads.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigAddAccountHeads.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
		
				
				List<MenuConfigMst> menuConfigBranchCreation= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"BRANCH CREATION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBranchCreation.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigBranchCreation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigFinancialYear= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"BRANCH CREATION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigFinancialYear.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigFinancialYear.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigCompanyCreation= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"COMPANY CREATION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigCompanyCreation.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCompanyCreation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigDBinitialize= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"DB INITIALIZE", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDBinitialize.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDBinitialize.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigPhysicalStock= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PHYSICAL STOCK", companyMst);
				if (menuConfigPhysicalStock.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPhysicalStock.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPhysicalStock.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				List<MenuConfigMst> menuConfigPurchaseOrder= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PURCHASE ORDER", companyMst);
				if (menuConfigPhysicalStock.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPurchaseOrder.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPurchaseOrder.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigPurchaseOrderManagement= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PURCHASE ORDER MANAGEMENT", companyMst);
				if (menuConfigPurchaseOrderManagement.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPurchaseOrderManagement.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPurchaseOrderManagement.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigOtherBranchSale= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"OTHER BRANCH SALE", companyMst);
				if (menuConfigPurchaseOrderManagement.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigOtherBranchSale.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigOtherBranchSale.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigDamageEntry= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"DAMAGE ENTRY", companyMst);
				if (menuConfigDamageEntry.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDamageEntry.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDamageEntry.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigUserRegisteredStock= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"USER REGISTERED STOCK", companyMst);
				if (menuConfigUserRegisteredStock.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigUserRegisteredStock.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigUserRegisteredStock.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				List<MenuConfigMst> menuConfigStoreChange= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"STORE CHANGE", companyMst);
				if (menuConfigStoreChange.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigStoreChange.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStoreChange.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				

				List<MenuConfigMst> menuConfigItemLocation= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ITEM LOCATION", companyMst);
				if (menuConfigItemLocation.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemLocation.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemLocation.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigSalesReturn= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SALES RETURN", companyMst);
				if (menuConfigSalesReturn.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSalesReturn.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSalesReturn.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigSalesFromOrder= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SALES FROM ORDER", companyMst);
				if (menuConfigSalesFromOrder.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSalesFromOrder.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSalesFromOrder.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				List<MenuConfigMst> menuConfigStockVerification= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"STOCK VERIFICATION", companyMst);
				if (menuConfigStockVerification.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigStockVerification.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockVerification.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigPurchaseScheme= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PURCHASE SCHEME", companyMst);
				if (menuConfigStockVerification.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPurchaseScheme.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPurchaseScheme.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				

				List<MenuConfigMst> menuConfigIntent= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"INTENT", companyMst);
				if (menuConfigIntent.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigIntent.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigIntent.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigIntentToStockTransfer= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"INTENT TO STOCK TRANSFER", companyMst);
				if (menuConfigIntentToStockTransfer.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigIntentToStockTransfer.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigIntentToStockTransfer.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigTaskList= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"TASK LIST", companyMst);
				if (menuConfigTaskList.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigTaskList.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigTaskList.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigSessionEnd= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SESSION END", companyMst);
				if (menuConfigSessionEnd.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSessionEnd.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSessionEnd.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				

				List<MenuConfigMst> menuConfigProductConversion= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PRODUCT CONVERSION", companyMst);
				if (menuConfigProductConversion.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProductConversion.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProductConversion.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigLanguageMst= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"LANGUAGE MST", companyMst);
				if (menuConfigLanguageMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigLanguageMst.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigLanguageMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigItemInLanguage= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"ITEN IN LANGUAGE", companyMst);
				if (menuConfigItemInLanguage.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemInLanguage.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigItemInLanguage.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				

				List<MenuConfigMst> menuConfigBarGraph= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"BAR GRAPH", companyMst);
				if (menuConfigBarGraph.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBarGraph.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigBarGraph.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				


				List<MenuConfigMst> menuConfigProductionValueGraph= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PRODUCTION VALUE GRAPH", companyMst);
				if (menuConfigProductionValueGraph.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProductionValueGraph.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProductionValueGraph.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigSetAnImage= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SET AN IMAGE", companyMst);
				if (menuConfigSetAnImage.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSetAnImage.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSetAnImage.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				List<MenuConfigMst> menuConfigTallyIntegration= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"TALLY INTEGRATION", companyMst);
				if (menuConfigTallyIntegration.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigTallyIntegration.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigTallyIntegration.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigStockCorrection= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"STOCK CORRECTION", companyMst);
				if (menuConfigStockCorrection.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigStockCorrection.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStockCorrection.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigProductionQtyGraph= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"PRODUCTION QTY GRAPH", companyMst);
				if (menuConfigProductionQtyGraph.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProductionQtyGraph.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProductionQtyGraph.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigReetry= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"RETRY", companyMst);
				if (menuConfigReetry.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReetry.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReetry.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				

				List<MenuConfigMst> menuConfigConsumptionReason= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"CONSUMPTION REASON", companyMst);
				if (menuConfigConsumptionReason.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigConsumptionReason.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigConsumptionReason.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigVoucherDateEdit= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"VOUCHER DATE EDIT", companyMst);
				if (menuConfigVoucherDateEdit.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigVoucherDateEdit.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigVoucherDateEdit.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				
				List<MenuConfigMst> menuConfigSubBranchSale= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"SUB BRANCH SALE", companyMst);
				if (menuConfigVoucherDateEdit.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSubBranchSale.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSubBranchSale.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
			
				
			
			
				List<MenuConfigMst> menuConfigExecuteSql= menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
								"EXECUTE SQL", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigExecuteSql.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigExecuteSql.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
		}
	
	 
	List<MenuConfigMst> menuConfigKotMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("KOT POS",
			companyMst);
	if (menuConfigKotMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKotMst.get(0).getId(), companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigKotMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigWholeSaleMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("WHOLESALE",
			companyMst);
	if (menuConfigWholeSaleMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigWholeSaleMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigWholeSaleMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigPosMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("POS WINDOW",
			companyMst);
	if (menuConfigPosMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPosMst.get(0).getId(), companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigPosMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigTakeOrderMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("TAKE ORDER",
			companyMst);
	if (menuConfigTakeOrderMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigTakeOrderMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigTakeOrderMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigOnlineSalesMst = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("ONLINE SALES", companyMst);
	if (menuConfigOnlineSalesMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigOnlineSalesMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigOnlineSalesMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}


	List<MenuConfigMst> menuConfigSOMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("SALEORDER",
			companyMst);
	if (menuConfigSOMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSOMst.get(0).getId(), companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigSOMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigPurchaseMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("PURCHASE",
			companyMst);
	if (menuConfigPurchaseMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPurchaseMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigPurchaseMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigCustMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("CUSTOMER",
			companyMst);
	if (menuConfigCustMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigCustMst.get(0).getId(), companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigCustMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigAcceptStockMst = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("ACCEPT STOCK", companyMst);
	if (menuConfigAcceptStockMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAcceptStockMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigAcceptStockMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigSTMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("STOCK TRANSFER",
			companyMst);
	if (menuConfigSTMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSTMst.get(0).getId(), companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigSTMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigDayEndMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("DAY END CLOSURE",
			companyMst);
	if (menuConfigDayEndMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDayEndMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigDayEndMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigInvoiceEditMst = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("INVOICE EDIT", companyMst);
	if (menuConfigInvoiceEditMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigInvoiceEditMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigInvoiceEditMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}

	List<MenuConfigMst> menuConfigSOEditMst = menuConfigMstRepository
			.findByMenuNameAndCompanyMst("SALES ORDER EDIT", companyMst);
	if (menuConfigSOEditMst.size() > 0) {
		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSOEditMst.get(0).getId(),
				companyMst);
		if (null == menuMst) {
			menuMst = new MenuMst();
			menuMst.setMenuId(menuConfigSOEditMst.get(0).getId());
			menuMst.setCompanyMst(companyMst);

			menuMstRepository.save(menuMst);

		}

	}


	
			
			
		}
		
		
	
		
		
	}
		
	return "sucesses";
	}
}
		
/*		
if(applicationDomainType.equals("BAKERY")) {

			

			List<MenuConfigMst> menuConfigMstPos = menuConfigMstRepository.findByMenuNameAndCompanyMst("POS WINDOW",
					companyMst);
			if (menuConfigMstPos.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMstPos.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMstPos.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}
		
			List<MenuConfigMst> menuConfigReportsMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("REPORTS",
					companyMst);
			if (menuConfigReportsMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReportsMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigReportsMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}
		
		

			if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigDailySalesReportMst = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("DAILY SALES REPORT", companyMst);
				if (menuConfigDailySalesReportMst.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigDailySalesReportMst.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigDailySalesReportMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				if (menuConfigReportsMst.size() > 0) {
				List<MenuConfigMst> menuConfigStatementOfAcc = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("STATEMENTS OF ACCOUNT", companyMst);
				if (menuConfigStatementOfAcc.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigStatementOfAcc.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigStatementOfAcc.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigReportsMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}
				}

			
				if (menuConfigReportsMst.size() > 0) {
					List<MenuConfigMst> menuConfigStockReport= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("STOCK REPORT", companyMst);
					if (menuConfigStatementOfAcc.size() > 0) {
						MenuMst menuMst = menuMstRepository
								.findByMenuIdAndCompanyMst(menuConfigStockReport.get(0).getId(), companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigStockReport.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigReportsMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}

					if (menuConfigReportsMst.size() > 0) {
						List<MenuConfigMst> menuConfigItemStockReport= menuConfigMstRepository
								.findByMenuNameAndCompanyMst("ITEM STOCK REPORT", companyMst);
						if (menuConfigStatementOfAcc.size() > 0) {
							MenuMst menuMst = menuMstRepository
									.findByMenuIdAndCompanyMst(menuConfigItemStockReport.get(0).getId(), companyMst);
							if (null == menuMst) {
								menuMst = new MenuMst();
								menuMst.setMenuId(menuConfigItemStockReport.get(0).getId());
								menuMst.setCompanyMst(companyMst);
								menuMst.setParentId(menuConfigReportsMst.get(0).getId());
								menuMstRepository.save(menuMst);

							}

						
						
					}
					if (menuConfigReportsMst.size() > 0) {
						List<MenuConfigMst> menuConfigItemWiseStockMovementReport= menuConfigMstRepository
								.findByMenuNameAndCompanyMst("ITEMWISE STOCK MOVEMENT REPORT", companyMst);
						if (menuConfigStatementOfAcc.size() > 0) {
							MenuMst menuMst = menuMstRepository
									.findByMenuIdAndCompanyMst(menuConfigItemWiseStockMovementReport.get(0).getId(), companyMst);
							if (null == menuMst) {
								menuMst = new MenuMst();
								menuMst.setMenuId(menuConfigItemWiseStockMovementReport.get(0).getId());
								menuMst.setCompanyMst(companyMst);
								menuMst.setParentId(menuConfigReportsMst.get(0).getId());
								menuMstRepository.save(menuMst);

							}

						}
						
					

					if (menuConfigReportsMst.size() > 0) {
						List<MenuConfigMst> menuConfigCategoryWiseStockMovementReport= menuConfigMstRepository
								.findByMenuNameAndCompanyMst("CATEGORYWISE STOCK MOVEMENT REPORT", companyMst);
						if (menuConfigStatementOfAcc.size() > 0) {
							MenuMst menuMst = menuMstRepository
									.findByMenuIdAndCompanyMst(menuConfigCategoryWiseStockMovementReport.get(0).getId(), companyMst);
							if (null == menuMst) {
								menuMst = new MenuMst();
								menuMst.setMenuId(menuConfigCategoryWiseStockMovementReport.get(0).getId());
								menuMst.setCompanyMst(companyMst);
								menuMst.setParentId(menuConfigReportsMst.get(0).getId());
								menuMstRepository.save(menuMst);

							}

						}
						
					
		

					if (menuConfigReportsMst.size() > 0) {
						List<MenuConfigMst> menuConfigInventoryReorderStatusReport= menuConfigMstRepository
								.findByMenuNameAndCompanyMst("INVENTORY REORDER STATUS REPORT", companyMst);
						if (menuConfigStatementOfAcc.size() > 0) {
							MenuMst menuMst = menuMstRepository
									.findByMenuIdAndCompanyMst(menuConfigInventoryReorderStatusReport.get(0).getId(), companyMst);
							if (null == menuMst) {
								menuMst = new MenuMst();
								menuMst.setMenuId(menuConfigInventoryReorderStatusReport.get(0).getId());
								menuMst.setCompanyMst(companyMst);
								menuMst.setParentId(menuConfigReportsMst.get(0).getId());
								menuMstRepository.save(menuMst);

							}

						}
					

					if (menuConfigReportsMst.size() > 0) {
						List<MenuConfigMst> menuConfigStockValueReport= menuConfigMstRepository
								.findByMenuNameAndCompanyMst("STOCK VALUE REPORT", companyMst);
						if (menuConfigStatementOfAcc.size() > 0) {
							MenuMst menuMst = menuMstRepository
									.findByMenuIdAndCompanyMst(menuConfigStockValueReport.get(0).getId(), companyMst);
							if (null == menuMst) {
								menuMst = new MenuMst();
								menuMst.setMenuId(menuConfigStockValueReport.get(0).getId());
								menuMst.setCompanyMst(companyMst);
								menuMst.setParentId(menuConfigReportsMst.get(0).getId());
								menuMstRepository.save(menuMst);

							}

						}


}
				
					
					List<MenuConfigMst> menuConfigAccountMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("ACCOUNTS",
							companyMst);
					if (menuConfigAccountMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAccountMst.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigAccountMst.get(0).getId());
							menuMst.setCompanyMst(companyMst);

							menuMstRepository.save(menuMst);

						}

						

					}	
					
		
		

				
				if (menuConfigAccountMst.size() > 0) {
					List<MenuConfigMst> menuConfigJournal = menuConfigMstRepository
							.findByMenuNameAndCompanyMst("JOURNAL", companyMst);
					if (menuConfigJournal.size() > 0) {
						MenuMst menuMst = menuMstRepository
								.findByMenuIdAndCompanyMst(menuConfigJournal.get(0).getId(), companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigJournal.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAccountMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}

			
		
				
					
					
					
					if (menuConfigAccountMst.size() > 0) {
						List<MenuConfigMst> menuConfigSupplierPayments = menuConfigMstRepository
								.findByMenuNameAndCompanyMst("SUPPLIER PAYMENTS", companyMst);
						if (menuConfigSupplierPayments.size() > 0) {
							MenuMst menuMst = menuMstRepository
									.findByMenuIdAndCompanyMst(menuConfigSupplierPayments.get(0).getId(), companyMst);
							if (null == menuMst) {
								menuMst = new MenuMst();
								menuMst.setMenuId(menuConfigSupplierPayments.get(0).getId());
								menuMst.setCompanyMst(companyMst);
								menuMst.setParentId(menuConfigAccountMst.get(0).getId());
								menuMstRepository.save(menuMst);

							}

						}

					
				
						
							

		
			
			
		


			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigPettyCashPayments = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PETTY-CASH PAYMENTS", companyMst);
				if (menuConfigPettyCashPayments.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPettyCashPayments.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPettyCashPayments.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			
			
			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigPettyCashReceipts = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PETTY-CASH RECIEPTS", companyMst);
				if (menuConfigPettyCashReceipts.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPettyCashReceipts.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPettyCashReceipts.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			

			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigPdcReconcile = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PDC RECONCILE", companyMst);
				if (menuConfigPdcReconcile.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPdcReconcile.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPdcReconcile.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			
		
			

			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigPaymentEdit = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PAYMENT EDIT", companyMst);
				if (menuConfigPaymentEdit.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPaymentEdit.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPaymentEdit.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			
			

			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigReceiptEdit = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RECEIPT EDIT", companyMst);
				if (menuConfigReceiptEdit.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigReceiptEdit.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReceiptEdit.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			

			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigPdcReceipt = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PDC RECEIPT", companyMst);
				if (menuConfigPdcReceipt.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPdcReceipt.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPdcReceipt.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			
		
			
			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigPdcPayment = menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
											"PDC PAYMENT", companyMst);
				if (menuConfigPdcPayment.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPdcPayment.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPdcPayment.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			

			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigJournalEdit = menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
											"JOURNAL EDIT", companyMst);
				if (menuConfigJournalEdit.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigJournalEdit.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigJournalEdit.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				

			}
			
			
		
		

			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigOwnAccountSettlement = menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
											"OWN ACCOUNT SETTLEMENT", companyMst);
				if (menuConfigOwnAccountSettlement.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigOwnAccountSettlement.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigOwnAccountSettlement.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			}

			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigSupplierPayment = menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
											"SUPPLIER PAYMENTS", companyMst);
				if (menuConfigSupplierPayment.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigSupplierPayment.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSupplierPayment.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			
			
			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigReceiptWindow = menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
											"RECEIPT WINDOW", companyMst);
				if (menuConfigReceiptWindow.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigReceiptWindow.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigReceiptWindow.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				

			}

			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigPayment = menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
											"PAYMENT", companyMst);
				if (menuConfigPayment.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigPayment.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigPayment.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				

			}

			if (menuConfigAccountMst.size() > 0) {
				List<MenuConfigMst> menuConfigCustomerBilWiseAdjustment = menuConfigMstRepository
						.findByMenuNameAndCompanyMst(
											"CUSTOMER BILLWISE ADJUSTMENT", companyMst);
				if (menuConfigCustomerBilWiseAdjustment.size() > 0) {
					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigCustomerBilWiseAdjustment.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigCustomerBilWiseAdjustment.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAccountMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}

			
			}
		
		

			List<MenuConfigMst> menuConfigAdvancedFeaturesMst = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("ADVANCED FEATURES", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAdvancedFeaturesMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigItemMaster = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("ITEM MASTER", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemMaster.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigItemMaster.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigItemEdit = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("ITEM EDIT", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemEdit.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigItemEdit.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			

			List<MenuConfigMst> menuConfigCategoryManagement = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("CATEGORY MANAGEMENT", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigCategoryManagement.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigCategoryManagement.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigTaxCreation = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("TAX CREATION", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigTaxCreation.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigTaxCreation.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			

			List<MenuConfigMst> menuConfigItemWiseTaxCreation = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("ITEMWISE TAX CREATION", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemWiseTaxCreation.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigItemWiseTaxCreation.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			

			List<MenuConfigMst> menuConfigPublishMessage = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("PUBLISH MESSAGE", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPublishMessage.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigPublishMessage.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}

			

			List<MenuConfigMst> menuConfigKItDefinition = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("KIT DEFINITION", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKItDefinition.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigKItDefinition.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigKotManager = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("KOT MANAGER", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKotManager.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigKotManager.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigStoreCreation = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("STORE CREATION", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigStoreCreation.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigStoreCreation.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigDayEnd= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("DAY END", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDayEnd.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigDayEnd.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigSiteCreation= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("SITE CREATION", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSiteCreation.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigSiteCreation.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}


			List<MenuConfigMst> menuConfigFinanceCreation= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("FINANCE CREATION", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigFinanceCreation.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigFinanceCreation.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigBarcodePrinting= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("BARCODE PRINTING", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBarcodePrinting.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigBarcodePrinting.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			
			List<MenuConfigMst> menuConfigBarcodeConfiguration= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("BARCODE CONFIGURATION", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBarcodeConfiguration.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigBarcodeConfiguration.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}

			
			List<MenuConfigMst> menuConfigItemMerge= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("ITEM MERGE", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigItemMerge.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigItemMerge.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}



			List<MenuConfigMst> menuConfigSaleOrderStatus= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("SALE ORDER STATUS", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSaleOrderStatus.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigSaleOrderStatus.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigReOrder= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("ReORDER", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReOrder.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigReOrder.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}

			

			List<MenuConfigMst> menuConfigIteBranch= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("ITEM BRANCH", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigIteBranch.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigIteBranch.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}


			List<MenuConfigMst> menuConfigMultyUnit= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("MULTY UNIT", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMultyUnit.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigMultyUnit.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}


			List<MenuConfigMst> menuConfigProductConversionConfiguration= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("PRODUCT CONVERSION CONFIGURATION", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProductConversionConfiguration.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigProductConversionConfiguration.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}

			
			List<MenuConfigMst> menuConfigNutritionFacts= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("NUTRITION FACTS", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigNutritionFacts.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigNutritionFacts.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			
			
			List<MenuConfigMst> menuConfigNutritionPrint= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("NUTRITION PRINT", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigNutritionPrint.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigNutritionPrint.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}
			
			List<MenuConfigMst> menuConfigBarcodeNutritionCombained= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("BARCODE NUTRRITION COMBINED", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBarcodeNutritionCombained.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigBarcodeNutritionCombained.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}

			}


			List<MenuConfigMst> menuConfigScheme= menuConfigMstRepository
					.findByMenuNameAndCompanyMst("SCHEME", companyMst);
			if (menuConfigAdvancedFeaturesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigScheme.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigScheme.get(0).getId());
					menuMst.setCompanyMst(companyMst);
					menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
					menuMstRepository.save(menuMst);

				}


			}
				List<MenuConfigMst> menuConfigSchemeDetails= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SCHEME DETAILS", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSchemeDetails.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSchemeDetails.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigSaleOrderEdits= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("SALES ORDER EDIT", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSaleOrderEdits.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigSaleOrderEdits.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigAddSupplier= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ADD SUPPLIER", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAddSupplier.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigAddSupplier.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigBatchPriceDefinition= menuConfigMstRepository
						.findByMenuNameAndCompanyMst("BATCH PRICE DEFINITION", companyMst);
				if (menuConfigAdvancedFeaturesMst.size() > 0) {
					MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBatchPriceDefinition.get(0).getId(),
							companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigAddSupplier.get(0).getId());
						menuMst.setCompanyMst(companyMst);
						menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
						menuMstRepository.save(menuMst);

					}


				}

					
					List<MenuConfigMst> menuConfigPriceDefinition= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("PRICE DEFINITION", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPriceDefinition.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigPriceDefinition.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					

					List<MenuConfigMst> menuConfigVoucherTyoe= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("VOUCHER TYPE", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigVoucherTyoe.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigVoucherTyoe.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					

					List<MenuConfigMst> menuConfigReceiptMode= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("RECEIPT MODE", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigReceiptMode.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigReceiptMode.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
			
					List<MenuConfigMst> menuConfigKitchenCategoryDtl= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("KITCHEN CATEGORY DTL", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKitchenCategoryDtl.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigKitchenCategoryDtl.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
			
					

					List<MenuConfigMst> menuConfigKotCategoryPrinter= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("KOT CATEGORY PRINTER", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKotCategoryPrinter.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigKotCategoryPrinter.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					
					List<MenuConfigMst> menuConfigPrinter= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("PRINTER", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPrinter.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigPrinter.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					List<MenuConfigMst> menuConfigChangeassword= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("CHANGE PASSWORD", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigChangeassword.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigChangeassword.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					
					List<MenuConfigMst> menuConfigGroupPermission= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("GROUP PERMISSION", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigGroupPermission.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigGroupPermission.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					

					List<MenuConfigMst> menuConfigGroupCreation= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("GROUP CREATION", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigGroupCreation.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigGroupCreation.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					

					List<MenuConfigMst> menuConfigProcessPermission= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("PROCESS PERMISSION", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProcessPermission.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigProcessPermission.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					

					List<MenuConfigMst> menuConfigProcessCreation= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("PROCESS CREATION", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProcessCreation.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigProcessCreation.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					
					
					List<MenuConfigMst> menuConfigMachineResource= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("MACHINE RESOUCE", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigMachineResource.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigMachineResource.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					
					List<MenuConfigMst> menuConfigResourceCategory= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("RESOUCE CATEGORY", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigResourceCategory.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigResourceCategory.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					
					
					
					
					List<MenuConfigMst> menuConfigUserRegistration= menuConfigMstRepository
							.findByMenuNameAndCompanyMst("USER REGISTRATION", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigUserRegistration.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigUserRegistration.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					List<MenuConfigMst> menuConfigAddAccountHeads= menuConfigMstRepository
							.findByMenuNameAndCompanyMst(
									"ADD ACCOUNT HEADS", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAddAccountHeads.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigAddAccountHeads.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
			
					
					List<MenuConfigMst> menuConfigBranchCreation= menuConfigMstRepository
							.findByMenuNameAndCompanyMst(
									"BRANCH CREATION", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigBranchCreation.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigBranchCreation.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					List<MenuConfigMst> menuConfigFinancialYear= menuConfigMstRepository
							.findByMenuNameAndCompanyMst(
									"BRANCH CREATION", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigFinancialYear.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigFinancialYear.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					

					List<MenuConfigMst> menuConfigCompanyCreation= menuConfigMstRepository
							.findByMenuNameAndCompanyMst(
									"COMPANY CREATION", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigCompanyCreation.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigCompanyCreation.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					
					List<MenuConfigMst> menuConfigDBinitialize= menuConfigMstRepository
							.findByMenuNameAndCompanyMst(
									"DB INITIALIZE", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDBinitialize.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigDBinitialize.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
					
					
					List<MenuConfigMst> menuConfigExecuteSql= menuConfigMstRepository
							.findByMenuNameAndCompanyMst(
									"EXECUTE SQL", companyMst);
					if (menuConfigAdvancedFeaturesMst.size() > 0) {
						MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigExecuteSql.get(0).getId(),
								companyMst);
						if (null == menuMst) {
							menuMst = new MenuMst();
							menuMst.setMenuId(menuConfigExecuteSql.get(0).getId());
							menuMst.setCompanyMst(companyMst);
							menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
							menuMstRepository.save(menuMst);

						}

					}
			}
				
			
			 * List<MenuConfigMst> menuConfigResourceCategoryLink= menuConfigMstRepository
			 * .findByMenuNameAndCompanyMst("RESOURCE CATEFGORY LINK", companyMst); if
			 * (menuConfigAdvancedFeaturesMst.size() > 0) { MenuMst menuMst =
			 * menuMstRepository.findByMenuIdAndCompanyMst(menuConfigResourceCategoryLink.
			 * get(0).getId(), companyMst); if (null == menuMst) { menuMst = new MenuMst();
			 * menuMst.setMenuId(menuConfigResourceCategoryLink.get(0).getId());
			 * menuMst.setCompanyMst(companyMst);
			 * menuMst.setParentId(menuConfigAdvancedFeaturesMst.get(0).getId());
			 * menuMstRepository.save(menuMst);
			 * 
			 * }
			 * 
			 * }
			 
			List<MenuConfigMst> menuConfigKotMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("KOT POS",
					companyMst);
			if (menuConfigKotMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigKotMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigKotMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigWholeSaleMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("WHOLESALE",
					companyMst);
			if (menuConfigWholeSaleMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigWholeSaleMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigWholeSaleMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigPosMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("POS WINDOW",
					companyMst);
			if (menuConfigPosMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPosMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigPosMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigTakeOrderMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("TAKE ORDER",
					companyMst);
			if (menuConfigTakeOrderMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigTakeOrderMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigTakeOrderMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigOnlineSalesMst = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("ONLINE SALES", companyMst);
			if (menuConfigOnlineSalesMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigOnlineSalesMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigOnlineSalesMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigProductionMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("PRODUCTION",
					companyMst);
			if (menuConfigProductionMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigProductionMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigProductionMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			if (menuConfigProductionMst.size() > 0) {
				List<MenuConfigMst> menuConfigProductionPrePlanningMst = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PRODUCTION PRE PLANNING", companyMst);

				if (menuConfigProductionPrePlanningMst.size() > 0) {

					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigProductionPrePlanningMst.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProductionPrePlanningMst.get(0).getId());
						menuMst.setParentId(menuConfigProductionMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);

						menuMstRepository.save(menuMst);

					}

				}

				List<MenuConfigMst> menuConfigProductionPlanningMst = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("PRODUCTION PLANNING", companyMst);

				if (menuConfigProductionPlanningMst.size() > 0) {

					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigProductionPlanningMst.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigProductionPlanningMst.get(0).getId());
						menuMst.setParentId(menuConfigProductionMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);

						menuMstRepository.save(menuMst);

					}

				}

				List<MenuConfigMst> menuConfigActualProductionMst = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("ACTUAL PRODUCTION", companyMst);

				if (menuConfigActualProductionMst.size() > 0) {

					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigActualProductionMst.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigActualProductionMst.get(0).getId());
						menuMst.setParentId(menuConfigProductionMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);

						menuMstRepository.save(menuMst);

					}

				}
				
				List<MenuConfigMst> menuConfigRawMaterialIssueMst = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RAW MATERIAL ISSUE", companyMst);

				if (menuConfigRawMaterialIssueMst.size() > 0) {

					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigRawMaterialIssueMst.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigRawMaterialIssueMst.get(0).getId());
						menuMst.setParentId(menuConfigProductionMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);

						menuMstRepository.save(menuMst);

					}

				}
				
				
				List<MenuConfigMst> menuConfigRawMaterialReturnMst = menuConfigMstRepository
						.findByMenuNameAndCompanyMst("RAW MATERIAL RETURN", companyMst);

				if (menuConfigRawMaterialReturnMst.size() > 0) {

					MenuMst menuMst = menuMstRepository
							.findByMenuIdAndCompanyMst(menuConfigRawMaterialReturnMst.get(0).getId(), companyMst);
					if (null == menuMst) {
						menuMst = new MenuMst();
						menuMst.setMenuId(menuConfigRawMaterialReturnMst.get(0).getId());
						menuMst.setParentId(menuConfigProductionMst.get(0).getId());
						menuMst.setCompanyMst(companyMst);

						menuMstRepository.save(menuMst);

					}

				}

			}

			List<MenuConfigMst> menuConfigSOMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("SALEORDER",
					companyMst);
			if (menuConfigSOMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSOMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigSOMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigPurchaseMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("PURCHASE",
					companyMst);
			if (menuConfigPurchaseMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigPurchaseMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigPurchaseMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigCustMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("CUSTOMER",
					companyMst);
			if (menuConfigCustMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigCustMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigCustMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigAcceptStockMst = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("ACCEPT STOCK", companyMst);
			if (menuConfigAcceptStockMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigAcceptStockMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigAcceptStockMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigSTMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("STOCK TRANSFER",
					companyMst);
			if (menuConfigSTMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSTMst.get(0).getId(), companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigSTMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigDayEndMst = menuConfigMstRepository.findByMenuNameAndCompanyMst("DAY END CLOSURE",
					companyMst);
			if (menuConfigDayEndMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigDayEndMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigDayEndMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigInvoiceEditMst = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("INVOICE EDIT", companyMst);
			if (menuConfigInvoiceEditMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigInvoiceEditMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigInvoiceEditMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}

			List<MenuConfigMst> menuConfigSOEditMst = menuConfigMstRepository
					.findByMenuNameAndCompanyMst("SALES ORDER EDIT", companyMst);
			if (menuConfigSOEditMst.size() > 0) {
				MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(menuConfigSOEditMst.get(0).getId(),
						companyMst);
				if (null == menuMst) {
					menuMst = new MenuMst();
					menuMst.setMenuId(menuConfigSOEditMst.get(0).getId());
					menuMst.setCompanyMst(companyMst);

					menuMstRepository.save(menuMst);

				}

			}
			return "Success";
		
			}
		
	}
	*/
	
