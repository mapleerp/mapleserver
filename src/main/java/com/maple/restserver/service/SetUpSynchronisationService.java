package com.maple.restserver.service;

import org.springframework.stereotype.Service;

@Service
public interface SetUpSynchronisationService {
	
	public String getCloudCount();
	
	public String getAllDateWithDayEndDate(String companymstid,String branchcode );
	
    public String verificationAndsynchronization(String companymstid,String branchcode );
}
