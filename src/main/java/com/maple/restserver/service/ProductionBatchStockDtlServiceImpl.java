package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.ProductionBatchStockDtl;
import com.maple.restserver.repository.ProductionBatchStockDtlRepository;

@Service
@Transactional
@Component
public class ProductionBatchStockDtlServiceImpl implements ProductionBatchStockDtlService {

	@Autowired
	ProductionBatchStockDtlRepository productionBatchStockDtlRepository;
	
	@Override
	public 	List<ProductionBatchStockDtl>getProductionBatchStockDtl(String companymstid, String itemid) {
		
		List<String>rowMaterials=productionBatchStockDtlRepository.
				findRowMaterialsByItemId(companymstid,itemid);
		List<ProductionBatchStockDtl> dtlList=new ArrayList<ProductionBatchStockDtl>();
	
		for(String itemId:rowMaterials) {
		
			List<Object>obj=productionBatchStockDtlRepository.rowMaterialStockAvailable(companymstid,itemId);
		List<String>batchList=productionBatchStockDtlRepository.getbatchList(itemId);
		System.out.print(batchList.size()+"batch llist size issssssssssssssssssssssssssssss");
			if(batchList.size()>1) {
			
			 for(int i=0;i<obj.size();i++)
			 {
				 ProductionBatchStockDtl dtl=new ProductionBatchStockDtl();
			Object[] objAray = (Object[]) obj.get(i);
			dtl.setItemId((String) objAray[0]);
			dtl.setQty((Double) objAray[1]);
			dtl.setBatch((String) objAray[2]);
			dtl.setExpiryDate((Date) objAray[3]);
			dtl.setAllocatedQty(0.0);

			dtlList.add(dtl);
			}
		}}
		
		return dtlList;
	}

	@Override
	public Integer getProductionRowMaterialCount(String companymstid, String itemid) {

		List<String>rowMaterials=productionBatchStockDtlRepository.
				findRowMaterialsByItemId(companymstid,itemid);
		List<ProductionBatchStockDtl> dtlList=new ArrayList<ProductionBatchStockDtl>();
		Integer Count=0;
		for(String itemId:rowMaterials) {
			
			List<Object>obj=productionBatchStockDtlRepository.rowMaterialStockAvailable(companymstid,itemId);
			List<String>batchList=productionBatchStockDtlRepository.getbatchList(itemId);
		
				if(batchList.size()>1) {
					Count++;
					
					
					
				}
				
		}
		return Count;
	}

}
