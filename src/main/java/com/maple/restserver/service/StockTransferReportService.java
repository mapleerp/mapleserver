package com.maple.restserver.service;


import java.util.Date;
import java.util.List;

import com.maple.restserver.report.entity.StockTransferOutReport;

public interface StockTransferReportService {

	
	public  List<Object> getStockTransfer(String intentNumber);
	
	
	public   List<StockTransferOutReport>getStockTransferByVoucerAndDate(String vouchernumber,Date date,String companymstid );
	
	
	public   List<StockTransferOutReport>getStockTransferByHdrId(String hdrid );
	
	
	public List<StockTransferOutReport> getStockTransferOutDtlBetweenDate(Date fudate, Date tudate, String branchcode,
			String companymstid);
	
	
	public List<StockTransferOutReport> getStockTransferOutDtlBetweenDateAndToBranch(Date fudate, Date tudate,
			String branchcode, String companymstid, String tobranch);
	
	
	public List<StockTransferOutReport> getStockTransferOutDtlBetweenDateAndCategoryList(Date fudate, Date tudate,
			String branchcode, String companymstid, String[] array);
	
	
	public List<StockTransferOutReport> getStockTransferOutDtlBetweenDateAndCategoryListAndTobranch(Date fudate,
			Date tudate, String branchcode, String companymstid, String[] array, String tobranch);
	
	
	public List<StockTransferOutReport> getStockTransferOutSummaryBetweenDate(Date fdate, Date tdate);

	
	
	
	
	
}
