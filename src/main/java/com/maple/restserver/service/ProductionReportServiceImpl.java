package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.ProductionDtlsReport;
import com.maple.restserver.report.entity.SaleOrderDetailsReport;
import com.maple.restserver.repository.ProductionMstRepository;

@Service
@Transactional
@Component
public class ProductionReportServiceImpl implements ProductionReportService {
	
	@Autowired
	ProductionMstRepository productionMstRepository;

	@Override
	public List<ProductionDtlsReport> getProductionDtlReport(Date date, String branchcode, String vouchernumber) {
		List<ProductionDtlsReport> productionReportList= new ArrayList();
		
		 List<Object> obj =   productionMstRepository.getProductionDtlReport(date, branchcode,vouchernumber);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ProductionDtlsReport productionReport =new ProductionDtlsReport();
			 productionReport.setItemName((String) objAray[0]);
			 productionReport.setQty((Double) objAray[1]);
			 productionReport.setUnitName((String) objAray[2]);
	}

		 return productionReportList;
		 
	}

	@Override
	public List<ProductionDtlsReport> fetchProductionDtlReport(String branchcode,Date date) {
		List<ProductionDtlsReport> productionReportList= new ArrayList();
		
		 List<Object> obj =   productionMstRepository.fetchProductionDtlReport(branchcode,date);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ProductionDtlsReport productionReport =new ProductionDtlsReport();
			 productionReport.setItemName((String) objAray[0]);

			 productionReport.setQty((Double) objAray[1]);
			 productionReport.setUnitName((String) objAray[2]);
			 productionReportList.add(productionReport);
			 

	}

		 return productionReportList;
		 
	}

	@Override
	public List<ProductionDtlsReport> fetchSaleOrderItem(String branchcode, Date date, String resCatId) {
	  
		
		List<ProductionDtlsReport> productionReportList= new ArrayList();
		
		 List<Object> obj =   productionMstRepository.fetchSaleOrderItem(branchcode,date,resCatId);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ProductionDtlsReport productionReport =new ProductionDtlsReport();
			 productionReport.setItemName((String) objAray[0]);
			 productionReport.setUnitName((String) objAray[1]);
			 productionReport.setQty((Double) objAray[2]);
			 productionReportList.add(productionReport);
			 

	}

		 return productionReportList;
		
	}
}
