package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.report.entity.DayEndClosureReport;
import com.maple.restserver.report.entity.SalesReport;

@Service
@Transactional
public interface DayEndClosureService {

	public DayEndClosureHdr getDayEndDetails(Date date,String companymstid);
	
	
	public String retrieveDayEndStatus(Date date,String branchCode);
	
	
	public  List<SalesReport> getDayEndReport(Date date,String companymstid);


	public DayEndClosureHdr getDayEndTotalSalesDetails(Date date, String companymstid);


	public void dailyStockSummaryToCloud(String mybranch, String companyMstId);
	
//	public List<DayEndClosureReport> retrieveDayEndClosureReport(String companymstid,Date voucherdate);


//	public List<DayEndClosureHdr> findByProcessDateAndCompanyMst(Date voucherdate, String companymstid);
}
