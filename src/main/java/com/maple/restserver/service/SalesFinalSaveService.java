package com.maple.restserver.service;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesFinalSave;

@Service
@Transactional
public interface SalesFinalSaveService {

	void finalSaveForSales(@Valid SalesFinalSave salesFinalSave, CompanyMst companyMst, String store, String logdate);
	
	

}
