package com.maple.restserver.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.repository.SalesTransHdrRepository;
@Component
@Service
public interface WeeklyTransactionsService {


	void WeeklyTransactions(Date fdate,Date tdate);
	
}
