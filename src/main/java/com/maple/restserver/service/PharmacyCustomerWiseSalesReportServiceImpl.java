package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.PharmacyNewCustomerWiseSaleReport;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CategoryMst;

import com.maple.restserver.report.entity.GSTOutputDetailReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.repository.AccountHeadsRepository;

import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;

@Service
public class PharmacyCustomerWiseSalesReportServiceImpl implements PharmacyCustomerWiseSalesReportService {
	@Autowired
	SalesDetailsRepository salesDetailsRepository;
	
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	
	@Override
	public List<PharmacyNewCustomerWiseSaleReport> findAMDCCustomerWiseSaleReport(Date fdate, Date tdate, String[] array) {
		
		List<PharmacyNewCustomerWiseSaleReport> amdcCustomerWiseSaleReportList = new ArrayList<PharmacyNewCustomerWiseSaleReport>();
		
		for(String customerName:array) {
			AccountHeads customerMst=accountHeadsRepository.findByAccountName(customerName);
		List<Object> obj = salesDetailsRepository.findAMDCCustomerWiseSaleReport(fdate,tdate,customerMst.getId());
		for(int i=0;i<obj.size();i++)
		 {
			
			 
			
			 Object[] objAray = (Object[]) obj.get(i);
			 PharmacyNewCustomerWiseSaleReport customerWiseSaleReport = new PharmacyNewCustomerWiseSaleReport();
			 customerWiseSaleReport.setAmount((Double) objAray[4]);
			 customerWiseSaleReport.setBatchcode((String) objAray[5]);
			 customerWiseSaleReport.setItemName((String) objAray[2]);
			 customerWiseSaleReport.setVoucherNumber((String) objAray[0]);
			 customerWiseSaleReport.setInvoiceDate((Date) objAray[1]);
			 customerWiseSaleReport.setQuantity((Double) objAray[3]);
			 customerWiseSaleReport.setTax((Double) objAray[6]);
			 amdcCustomerWiseSaleReportList.add(customerWiseSaleReport);
		 }
		}
		return amdcCustomerWiseSaleReportList;
		}
	
}
