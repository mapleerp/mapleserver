package com.maple.restserver.service;

import java.io.IOException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.BulkTransactionParameter;
import com.maple.restserver.jms.send.KafkaMapleEvent;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.utils.EventBusFactory;


@Service
public class CallForResendBulkTransactionServiceImpl implements CallForResendBulkTransactionService {

	private final Logger logger = LoggerFactory.getLogger(CallForResendBulkTransactionService.class);
	Integer partitionKey = 1;
	EventBus eventBus = EventBusFactory.getEventBus();
	
	private String file_sufix = ".dat";
	private String name_differentiator = "$";
	
	@Override
	public void publishforbulktransaction(String branchCode, Date fDate, Date tDate, String vouchertype) {
		
		
		BulkTransactionParameter bulkTransactionParameter= new BulkTransactionParameter();
		
		
		bulkTransactionParameter.setBranchCode(branchCode);
		bulkTransactionParameter.setfDate(fDate);
		bulkTransactionParameter.settDate(tDate);
		bulkTransactionParameter.setVouchertype(vouchertype);
		
		String destination = branchCode;

		

		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper();
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(bulkTransactionParameter);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.BULKTRANSACTIONPARAMETER);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(branchCode);
		kafkaMapleEvent.setDestination(destination);
		
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				KafkaMapleEventType.BULKTRANSACTIONPARAMETER + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);
		
		eventBus.post(kafkaMapleEvent);
		
	}

}
