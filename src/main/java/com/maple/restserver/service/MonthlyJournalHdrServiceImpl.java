package com.maple.restserver.service;



import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MonthlyJournalDtl;
import com.maple.restserver.entity.MonthlyJournalHdr;
import com.maple.restserver.entity.ReceiptModeMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.MonthlyJournalDtlRepository;
import com.maple.restserver.repository.MonthlyJournalHdrRepository;
import com.maple.restserver.repository.ReciptModeMstRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.service.accounting.task.JournalAccounting;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.utils.SystemSetting;

@Service

@Component
public class MonthlyJournalHdrServiceImpl implements MonthlyJournalHdrService{
	private static final Logger logger = LoggerFactory.getLogger(MonthlyJournalHdrServiceImpl.class);

	@Autowired
	MonthlyJournalHdrRepository  monthlyJournalHdrRepository;
	
	
	@Autowired
	MonthlyJournalDtlRepository monthlyJournalDtlRepository;
	
	@Autowired
	JournalAccounting journalAccounting;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	
	@Autowired
	ReciptModeMstRepository reciptModeMstRepository;
@Transactional
	@Override
	public void copyToMonthlyJournalHdr(Date fromDate, Date toDate,String branchCode, CompanyMst companyMst) {
		
		
		String strFromDate=SystemSetting.UtilDateToString(fromDate);
		String strToDate=SystemSetting.UtilDateToString(toDate);
		String hdrVoucherNumber=strFromDate+"-"+strToDate;
		MonthlyJournalHdr monthlyJournalHdr=new MonthlyJournalHdr();
		monthlyJournalHdr.setVoucherNumber(hdrVoucherNumber);
		monthlyJournalHdr.setBranchCode(branchCode);
		monthlyJournalHdr.setCompanyMst(companyMst);
		java.sql.Date sqlVoucherDate=SystemSetting.UtilDateToSQLDate(toDate);
		monthlyJournalHdr.setVoucherDate(sqlVoucherDate);
	    java.sql.Date sqlSystemDate=SystemSetting.UtilDateToSQLDate(SystemSetting.getSystemDate());
		monthlyJournalHdr.setTransDate(sqlSystemDate);
		monthlyJournalHdrRepository.saveAndFlush(monthlyJournalHdr);
		MonthlyJournalHdr	monthlyJournalHdrs=monthlyJournalHdrRepository.findByVoucherNumber(hdrVoucherNumber);
		System.out.print(monthlyJournalHdrs+"trans hdr izzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
	MonthlyJournalDtl monthlyJournalDtl=new MonthlyJournalDtl();
	Double sumOfAsasableAmount=salesTransHdrRepository.salesAsasableAmount(fromDate,toDate);
	AccountHeads accountHeads= accountHeadsRepository.findByAccountName("SALES ACCOUNT");
	monthlyJournalDtl.setAccountId(accountHeads.getId());
	monthlyJournalDtl.setCreditAmount(sumOfAsasableAmount);
	monthlyJournalDtl.setDebitAmount(0.0);
	monthlyJournalDtl.setMonthlyJournalHdr(monthlyJournalHdrs);
	monthlyJournalDtl.setRemarks("monthly journal assable amount ");
	
	System.out.println(monthlyJournalHdrs+"*************************************************************************************************************************************************8");
	
	monthlyJournalDtlRepository.save(monthlyJournalDtl);
	
	
	Double sumOfSgstAmnt=salesTransHdrRepository.sumOfSGSTamount(fromDate,toDate);
	MonthlyJournalDtl monthlyJournalDtlSgst=new MonthlyJournalDtl();
	AccountHeads accountHeadsSGST= accountHeadsRepository.findByAccountName("SGST ACCOUNT");
	monthlyJournalDtlSgst.setAccountId(accountHeadsSGST.getId());
	monthlyJournalDtlSgst.setCreditAmount(sumOfSgstAmnt);
	monthlyJournalDtlSgst.setDebitAmount(0.0);
	monthlyJournalDtlSgst.setMonthlyJournalHdr(monthlyJournalHdrs);
	monthlyJournalDtlSgst.setRemarks("sgst amount to monthly journal");
	monthlyJournalDtlRepository.save(monthlyJournalDtlSgst);
	
	
	Double sumOfCgstAmnt=salesTransHdrRepository.sumOfCGSTamount(fromDate,toDate);
	MonthlyJournalDtl monthlyJournalDtlCgst=new MonthlyJournalDtl();
	AccountHeads accountHeadsCGST= accountHeadsRepository.findByAccountName("CGST ACCOUNT");
	monthlyJournalDtlCgst.setAccountId(accountHeadsCGST.getId());
	monthlyJournalDtlCgst.setCreditAmount(sumOfCgstAmnt);
	monthlyJournalDtlCgst.setDebitAmount(0.0);
	monthlyJournalDtlCgst.setMonthlyJournalHdr(monthlyJournalHdrs);
	monthlyJournalDtlCgst.setRemarks("Cgst amount to monthly journal");
	monthlyJournalDtlRepository.save(monthlyJournalDtlCgst);
	Double sumOfIgstAmnt=salesTransHdrRepository.sumOfIGSTamount(fromDate,toDate);
	
	MonthlyJournalDtl monthlyJournalDtlIgst=new MonthlyJournalDtl();
	AccountHeads accountHeadsIGST=accountHeadsRepository.findByAccountName("IGST ACCOUNT");
	if(null!=accountHeadsIGST) {
	monthlyJournalDtlIgst.setAccountId(accountHeadsIGST.getId());
	monthlyJournalDtlIgst.setCreditAmount(sumOfIgstAmnt);
	monthlyJournalDtlIgst.setDebitAmount(0.0);
	monthlyJournalDtlIgst.setMonthlyJournalHdr(monthlyJournalHdrs);
	monthlyJournalDtlIgst.setRemarks("Igst amount to monthly journal");
	monthlyJournalDtlRepository.save(monthlyJournalDtlIgst);
	}	
	Double sumOfCessAmnt=salesTransHdrRepository.sumOfCESSamount(fromDate,toDate);
	MonthlyJournalDtl monthlyJournalDtlCessAmnt=new MonthlyJournalDtl();
	AccountHeads accountHeadsCESS=accountHeadsRepository.findByAccountName("CESS ACCOUNT");
	if(null!=accountHeadsCESS) {
	monthlyJournalDtlCessAmnt.setAccountId(accountHeadsCESS.getId());
	monthlyJournalDtlCessAmnt.setCreditAmount(sumOfCessAmnt);
	monthlyJournalDtlCessAmnt.setMonthlyJournalHdr(monthlyJournalHdrs);
	monthlyJournalDtlCessAmnt.setDebitAmount(0.0);
	monthlyJournalDtlCessAmnt.setRemarks("cess amount to monthly journal");
	
	System.out.println("inside the cess !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	monthlyJournalDtlRepository.save(monthlyJournalDtlCessAmnt);
	}
	List<ReceiptModeMst> receiptModeMstList=reciptModeMstRepository.findAll();
	
	System.out.print(receiptModeMstList.size()+"receipt mode size issssssssssssssssssssss000000000000000000000000000000000000000000000000000000000");
	
for (int i=0;i<receiptModeMstList.size();i++) {
	if(receiptModeMstList.get(i).getReceiptMode().equalsIgnoreCase("CASH")) {
	String receiptMode=branchCode+"-CASH";
	Double receiptModeCash=0.0;

    receiptModeCash=salesTransHdrRepository.sumOfCashAmount(fromDate,toDate,receiptMode);
	MonthlyJournalDtl monthlyJournalDtlCash=new MonthlyJournalDtl();
	AccountHeads accountHeadsCASH=accountHeadsRepository.findByAccountName(receiptMode);
	monthlyJournalDtlCash.setAccountId(accountHeadsCASH.getId());
	monthlyJournalDtlCash.setCreditAmount(0.0);
	monthlyJournalDtlCash.setDebitAmount(receiptModeCash);
	monthlyJournalDtlCash.setMonthlyJournalHdr(monthlyJournalHdrs);
	monthlyJournalDtlCash.setRemarks(receiptMode+" amount to monthly journal");
	monthlyJournalDtlRepository.save(monthlyJournalDtlCash);
	

	}else {
		Double receiptModeAmount=0.0;
		 receiptModeAmount=salesTransHdrRepository.sumOfCashAmount(fromDate,toDate,receiptModeMstList.get(i).getReceiptMode());
		MonthlyJournalDtl monthlyJournalDtlCash=new MonthlyJournalDtl();
		AccountHeads accountHeadsCASH=accountHeadsRepository.findByAccountName(receiptModeMstList.get(i).getReceiptMode());
		monthlyJournalDtlCash.setAccountId(accountHeadsCASH.getId());
		monthlyJournalDtlCash.setCreditAmount(0.0);
		monthlyJournalDtlCash.setDebitAmount(receiptModeAmount);
		monthlyJournalDtlCash.setMonthlyJournalHdr(monthlyJournalHdrs);
		System.out.println(monthlyJournalHdrs+"monthly journal hdr isssssssssssssssss");
		monthlyJournalDtlCash.setRemarks(receiptModeMstList.get(i).getReceiptMode()+" amount to monthly journal");
		monthlyJournalDtlRepository.save(monthlyJournalDtlCash);

	}


List<Object>  creditAmount =salesTransHdrRepository.sumOfCreditAmount(fromDate,toDate);

for(int j=0;j<creditAmount.size();j++)
{
	 Object[] objAray = (Object[]) creditAmount.get(j);

MonthlyJournalDtl monthlyJournalDtlCREDIT=new MonthlyJournalDtl();

monthlyJournalDtlCREDIT.setAccountId((String) objAray[0]);
monthlyJournalDtlCREDIT.setCreditAmount(0.0);
monthlyJournalDtlCREDIT.setDebitAmount((Double) objAray[1]);
monthlyJournalDtlCREDIT.setMonthlyJournalHdr(monthlyJournalHdrs);
monthlyJournalDtlCREDIT.setRemarks("Credit monthly journal hdr isssssssssssss");
monthlyJournalDtlRepository.save(monthlyJournalDtlCREDIT);

}
Double sumOfDebitAmnt=0.0;
Double sumOfcreditAmount=0.0;
AccountHeads accountHeadsROUNDOFF=accountHeadsRepository.findByAccountName("ROUNDOFF");
sumOfDebitAmnt=monthlyJournalDtlRepository.sumOfDebitAmount(hdrVoucherNumber);
  sumOfcreditAmount=monthlyJournalDtlRepository.sumOfCreditAmount(hdrVoucherNumber);
 MonthlyJournalDtl monthlyJournalDtlROUNOFFT=new MonthlyJournalDtl();
 if(sumOfDebitAmnt>sumOfcreditAmount) {
	 Double amount=sumOfDebitAmnt-sumOfcreditAmount;
	 
	 monthlyJournalDtlROUNOFFT.setAccountId(accountHeadsROUNDOFF.getId());
	 monthlyJournalDtlROUNOFFT.setCreditAmount(amount);
	 monthlyJournalDtlROUNOFFT.setDebitAmount(0.0);
	 monthlyJournalDtlROUNOFFT.setMonthlyJournalHdr(monthlyJournalHdrs);
	 monthlyJournalDtlROUNOFFT.setRemarks("monthlyJournalDtlROUNDOFF CREDIT");
	 monthlyJournalDtlRepository.save(monthlyJournalDtlROUNOFFT);
 }
 if(sumOfcreditAmount>sumOfDebitAmnt) {
	 Double amount=sumOfcreditAmount-sumOfDebitAmnt;
	 monthlyJournalDtlROUNOFFT.setAccountId(accountHeadsROUNDOFF.getId());
	 monthlyJournalDtlROUNOFFT.setCreditAmount(0.0);
	 monthlyJournalDtlROUNOFFT.setDebitAmount(amount);
	 monthlyJournalDtlROUNOFFT.setMonthlyJournalHdr(monthlyJournalHdrs);
	 monthlyJournalDtlROUNOFFT.setRemarks("monthlyJournalDtlROUNDOFF DEBIT");
	 monthlyJournalDtlRepository.save(monthlyJournalDtlROUNOFFT);
 }
 
 
	}
	
try {
	journalAccounting.execute(monthlyJournalHdrs.getVoucherNumber(), monthlyJournalHdrs.getVoucherDate(), monthlyJournalHdrs.getId(), monthlyJournalHdrs.getCompanyMst());
} catch (PartialAccountingException e) {
	// TODO Auto-generated catch block
	logger.error(e.getMessage());
}



}

}
