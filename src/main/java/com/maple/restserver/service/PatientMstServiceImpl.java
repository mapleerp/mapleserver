package com.maple.restserver.service;

import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.PatientMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;


@Service
@Transactional
public class PatientMstServiceImpl implements PatientMstService{

	
	@Autowired
	CurrencyMstRepository currencyMstRepo;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	AccountHeadsRepository accountHeadsRepo;
	
	
	@Override
	public String cusAndAccountFromPatient(@Valid PatientMst patientMst, CompanyMst companyMst) {
		
	
		AccountHeads customermst = new AccountHeads();
		AccountHeads custByNameOP= accountHeadsRepo.findByAccountNameAndCompanyMst(patientMst.getPatientName(),companyMst );
		if(null==custByNameOP)
		{
			customermst.setCompanyMst(companyMst);
			if(null != companyMst.getCurrencyName())
			{
			CurrencyMst currencyMst = currencyMstRepo.findByCurrencyName(companyMst.getCurrencyName());
			customermst.setCurrencyId(currencyMst.getId());
			}
			customermst.setPartyAddress1(patientMst.getAddress1());
			customermst.setCustomerContact(patientMst.getPhoneNumber());
			customermst.setCustomerCountry(patientMst.getNationality());
			customermst.setAccountName(patientMst.getPatientName());
			customermst.setId(patientMst.getId());
			customermst.setCustomerRank(0);
			customermst = accountHeadsRepo.save(customermst);
		}
		
//		Optional<AccountHeads> accountHeadsop=accountHeadsRepo.findById(patientMst.getId());
//		if(!accountHeadsop.isPresent())
//		{
//			customermst.setCompanyMst(companyMst);
//			if(null != companyMst.getCurrencyName())
//			{
//			CurrencyMst currencyMst = currencyMstRepo.findByCurrencyName(companyMst.getCurrencyName());
//			customermst.setCurrencyId(currencyMst.getId());
//			}
//			AccountHeads accountHeads = new AccountHeads();
//			accountHeads.setId(patientMst.getId());
//			accountHeads.setAccountName(patientMst.getPatientName());
//			accountHeads.setCompanyMst(companyMst);
//			if(null != companyMst.getCurrencyName())
//			{
//			accountHeads.setCurrencyId(customermst.getCurrencyId());
//			}
//		}
//			
		return "Success";
	}

}
