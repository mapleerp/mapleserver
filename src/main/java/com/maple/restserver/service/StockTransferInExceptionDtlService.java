package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.StockTransferInExceptionReportDtl;

@Service
public interface StockTransferInExceptionDtlService {

	
	public List<StockTransferInExceptionReportDtl> stockTransferInExceptionReort(String voucherNumber,Date voucherDate);
}
