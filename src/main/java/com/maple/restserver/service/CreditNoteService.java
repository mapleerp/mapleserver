package com.maple.restserver.service;

import java.util.List;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CreditNote;

public interface CreditNoteService {

	List<CreditNote> getCreditNoteItems(CompanyMst companyMst);

}
