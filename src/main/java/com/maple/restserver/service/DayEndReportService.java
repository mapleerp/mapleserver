package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.report.entity.DayEndProcessing;
import com.maple.restserver.report.entity.DayEndReceiptMode;
import com.maple.restserver.report.entity.DayEndReport;
import com.maple.restserver.report.entity.DayEndWebReport;
import com.maple.restserver.report.entity.PharmacyDayClosingReport;
import com.maple.restserver.report.entity.VoucherNumberDtlReport;

public interface DayEndReportService {
	Double getCreditSaleAdjustment(Date date, String companymstid, String branchcode);

	Double getOtherCashSale(Date date, String companymstid, String branchcode);
	
	
	DayEndReport getDayEndReport(String branch,Date  date,String companymstid);
	
	VoucherNumberDtlReport	getVoucherNumberDetailReport(String branch, Date date,String companymstid);

	List<DayEndProcessing> getDayEndReceiptMode(String companymstid, String branchcode, Date date);
	
	Double getPaymentbyaccountId(Date date, String companymstid, String branchcode , String accountid);

	VoucherNumberDtlReport getPosVoucherNumberSummary(String branch, Date date, String companymstid,
			String salesprefix);

	List<PharmacyDayClosingReport> findPharmacyDayClosingReport(Date date);
	
	List<DayEndWebReport> getDayEndWebReportForWithTotalSalesHC(Date date, String companymstid);

}
