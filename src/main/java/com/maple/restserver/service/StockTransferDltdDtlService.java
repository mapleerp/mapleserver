package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.StockTransferOutDltdDtl;

public interface StockTransferDltdDtlService {

	List<StockTransferOutDltdDtl> findByCompanyMstAndDate(String companymstid, Date edate);

}
