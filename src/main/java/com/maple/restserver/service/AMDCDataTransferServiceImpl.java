package com.maple.restserver.service;




import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.maple.restserver.RestserverApplication;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ConsumptionDtl;
import com.maple.restserver.entity.DepartmentMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.his.entity.DoctorMst;
import com.maple.restserver.his.entity.NewDoctorMst;
import com.maple.restserver.mobileEntity.PatientDtl;
import com.maple.restserver.mobileRepository.PatientDtlRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.DepartmentMstRepository;
import com.maple.restserver.repository.DoctorMstRepository;
import com.maple.restserver.repository.InsuranceMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.repository.UserMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.SystemSetting;

//@Service
//
public class AMDCDataTransferServiceImpl implements AMDCDataTransferService {

	
	
	@Autowired
	UserMstRepository userMstRepository;
	  @Autowired AccountClassRepository accountClassRepository;
	  
	  @Autowired
	  
	  AccountHeadsRepository accountHeadsRepository;
	  
	  @Autowired
	  CategoryMstRepository categoryMstRepository;
	  
	  @Autowired
	  VoucherNumberService voucherNumberService;
	  
	  @Autowired
	  UnitMstRepository unitMstRepository;
	  
	  
	  @Autowired
	  PatientDtlRepository patientDtlRepository;
	  @Autowired
	  ItemMstRepository itemMstRepository;
	  
	  @Autowired 
	  DepartmentMstRepository departmentMstRepository;
	  
	  @Autowired
	  InsuranceMstRepository insuranceMstRepository;
	  
	  
	  @Autowired
	  DoctorMstRepository doctorMstRepository;
	  
	  
	  @Autowired
	  PriceDefinitionMstRepository  priceDefinitionMstRepository;
	  
	  @Autowired
	  ItemBatchDtlRepository itemBatchDtlRepository;
	  @Autowired
	  
	 ItemBatchMstRepository itemBatchMstRepository;
	  
	  @Autowired
	  BranchMstRepository branchMstRepository;
	  @Autowired
	  SalesTransHdrRepository  salesTransHdrRepository;
	  @Autowired
	  SalesDetailsRepository salesDetailsRepository;
	  @Autowired
	  PurchaseHdrRepository purchaseHdrRepository;
	  
	  @Override
	  public String accountClassTransferamdc(CompanyMst companyMst, String
	  branchCode) throws UnknownHostException, Exception {
	  
	  
	  
	  
	  
	  String sqlstr2 = " select  * from account_class";
	  
	  PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
	  
	  ResultSet rs2 = pst2.executeQuery();
	  
	  while (rs2.next()) { AccountClass accountClass = new AccountClass();
	  accountClass.setId(rs2.getString("id"));
	  
	  Double totalDebit=Double.valueOf(rs2.getString("total_debit"));
	  accountClass.setTotalDebit(BigDecimal.valueOf(totalDebit));
	  
	  Double totalCredit=Double.valueOf(rs2.getString("total_credit"));
	  accountClass.setTotalCredit(BigDecimal.valueOf(totalCredit));
	  
	  accountClass.setMachineId(rs2.getString("machine_id"));
	  accountClass.setFinancialYear(rs2.getString("financial_year"));
	  
	 
	  accountClass.setTransDate(rs2.getDate("trans_date"));
	  
	  accountClass.setVoucherType(rs2.getString("voucher_type"));
	  accountClass.setSourceVoucherNumber(rs2.getString("source_voucher_number"));
	  accountClass.setSourceParentId(rs2.getString("source_parent_id"));
	  accountClass.setCompanyMst(companyMst);
	  
	  accountClass =accountClassRepository.save(accountClass);
	  
	  } 
	  rs2.close();
	  pst2.close();
	  return "OK";
	  
	  }
	  
	  
	  @Override
	  public String accountHeadsTransferamdc(CompanyMst companyMst, String
	  branchCode) throws UnknownHostException, Exception {
	  
	  
	  
	  
	  
	  
	  String sqlstr2 = " select  * from account_heads";
	  
	  PreparedStatement pst2 =
			  RestserverApplication.localCn.prepareStatement(sqlstr2);
	  
	  ResultSet rs2 = pst2.executeQuery();
	  
	  while (rs2.next()) { AccountHeads accountHeads = new AccountHeads();
	  accountHeads.setId(rs2.getString("id"));
	  accountHeads.setAccountName(rs2.getString("account_name"));
	  accountHeads.setRootParentId(rs2.getString("root_account"));
	  accountHeads.setParentId(rs2.getString("parent_account_code"));
	  accountHeads.setCompanyMst(companyMst);
	  
	  accountHeads =accountHeadsRepository.save(accountHeads);
	  
	  } 
	  
	  rs2.close();
	  pst2.close();
	  return "OK";
	  
	  }
	  
	  
	  @Override public String depoartMentTransferamdc(CompanyMst companyMst, String
	  branchCode) throws UnknownHostException, Exception {
	  
	  
	  
	
	  
	  
	  String sqlstr2 = " select  * from department_mst";
	  
	  PreparedStatement pst2 =
	  RestserverApplication.localCn.prepareStatement(sqlstr2);
	  
	  ResultSet rs2 = pst2.executeQuery();
	  
	  while (rs2.next()) { DepartmentMst departmentMst = new DepartmentMst();
	  departmentMst.setId(rs2.getString("id"));
	  departmentMst.setDepartmentName(rs2.getString("department_name"));
	  
	  
	  departmentMst =departmentMstRepository.save(departmentMst);
	  
	  } 
	  
	  rs2.close();
	  pst2.close();
	  
	  return "OK";
	  
	  }
	  
	  

	  
	  
	  @Override 
	  public String doctorMstTransferamdc(CompanyMst companyMst, String
	  branchCode) throws UnknownHostException, Exception {
	  
	  
	  
	 
	  
	  
	  String sqlstr2 = " select  * from doctor_mst";
	  
	  PreparedStatement pst2 =
	  RestserverApplication.localCn.prepareStatement(sqlstr2);
	  
	  ResultSet rs2 = pst2.executeQuery();
	  
	  while (rs2.next())
	  
	  { 
		  DoctorMst DoctorMst = new DoctorMst();
	//  doctorMst.setId(rs2.getString("id"));
	  DoctorMst.setId(Integer.valueOf(rs2.getString("id")));
	 
	//  doctorMst.setDepartmentName(rs2.getString("department_name"));
	  
	  
	  DoctorMst =doctorMstRepository.save(DoctorMst);
	  
	  } 
	  
	  rs2.close();
	  pst2.close();
	  return "OK";
	  
	  }
	 

	  
	  
	  
	  
	  @Override
	  public String priceLevelMstTransferamdc(CompanyMst companyMst, String
	  branchCode) throws UnknownHostException, Exception {
	  
	  
	  
	 
	  
	  
	  String sqlstr2 = " select  * from price_level_mst";
	  
	  PreparedStatement pst2 =
	  RestserverApplication.localCn.prepareStatement(sqlstr2);
	  
	  ResultSet rs2 = pst2.executeQuery();
	  
	  while (rs2.next())
	  
	  { 
		  PriceDefenitionMst priceDefenitionMst = new PriceDefenitionMst();
	//  doctorMst.setId(rs2.getString("id"));
		  
		  priceDefenitionMst.setId(rs2.getString("id"));
		  priceDefenitionMst.setOldId(rs2.getString("id"));
	 
		  priceDefenitionMst.setCompanyMst(companyMst);
		  priceDefenitionMst.setPriceLevelName(rs2.getString("id"));

	//  doctorMst.setDepartmentName(rs2.getString("department_name"));
	  
	  
		  priceDefenitionMst =priceDefinitionMstRepository.save(priceDefenitionMst);
	  
	  } 
	  
	  rs2.close();
	  pst2.close();
	  return "OK";
	  
	  }
	 
	  

		@Override
		public String doSalesTransHdrTransfer(CompanyMst companyMst, String branchcode)
				throws UnknownHostException, Exception {

			String sqlstr2 = " select h.record_id, h.user_id , h.company_id ,h. branch_id ,h.godown_id, h.machine_id ,h.shift_id ,h.invoice_date , h.customer_id , "
					+ "  h.customer_type_id , h.invoice_type_id , h.posted_to_tally , h.tally_voucher_id ,h.summary  , h.str_invoice_date , h.is_summarised, h.doctor_id, h.salesman_id , h.delivery_address ,h.financial_year, h.finance_copmany_id ,h.delivery_vehicle ,"
					+ "v.invoice_amount,v.voucher_number ,v. net_invoice_amount,v.invoice_discount ,v.voucher_type,d.record_id,d.item_id,d.item_name,d.tax_rate ,d.qty,d.rate,d.unit_id,d.manufacturing_date,d.expiry_date,d.item_serial_no,d.batch_code,d.fc_rate,d.fc_discount,d.fc_tax_rate,d.barcode,d.hsn_code "
					+ " from sales_trans_hdr h ,sales_voucher_hdr v,xpos_sales_dtl d  where h.record_id=v.parent_id and d.parent_id=h.record_id ";

			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				String record_id = rs2.getString("h.record_id");
				String user_id = rs2.getString("h.user_id");
				String company_id = rs2.getString("h.godown_id");
				String machine_id = rs2.getString("h.machine_id");
				String branch_id = rs2.getString("h.branch_id");
				String shift_id = rs2.getString("h.shift_id");
				String invoice_date = rs2.getString("h.invoice_date");
				String customer_id = rs2.getString("h.customer_id");
				String customer_type_id = rs2.getString("h.customer_type_id");
				String invoice_type_id = rs2.getString("h.invoice_type_id");
				String posted_to_tally = rs2.getString("h.posted_to_tally");
				String tally_voucher_id = rs2.getString("h.tally_voucher_id");
				String summary = rs2.getString("h.summary");
				String str_invoice_date = rs2.getString("h.str_invoice_date");
				String is_summarised = rs2.getString("h.is_summarised");
				String doctor_id = rs2.getString("h.doctor_id");
				String salesman_id = rs2.getString("h.salesman_id");
				// String tally_reply=rs2.getString("h.tally_reply");
				String delivery_address = rs2.getString("h.delivery_address");
				String financial_year = rs2.getString("h.financial_year");
				String finance_copmany_id = rs2.getString("h.finance_copmany_id");
				String delivery_vehicle = rs2.getString("h.delivery_vehicle");
				String voucher_number = rs2.getString("v.voucher_number");
				float invoice_amount = rs2.getFloat("v.invoice_amount");
				float invoice_discount = rs2.getFloat("v.invoice_discount");

				String dRecordId = rs2.getString("d.record_id");
				String item_id = rs2.getString("d.item_id");
				String item_name = rs2.getString("d.item_name");
				float tax_rate = rs2.getFloat("d.tax_rate");
				float qty = rs2.getFloat("d.qty");
				float rate = rs2.getFloat("d.rate");
				String unit_id = rs2.getString("d.unit_id");
				String manufacturing_date = rs2.getString("d.manufacturing_date");
				String expiry_date = rs2.getString("d.expiry_date");
				String item_serial_no = rs2.getString("d. item_serial_no");

				String batch_code = rs2.getString("d.batch_code");
				float fc_rate = rs2.getFloat("d.fc_rate");
				float fc_discount = rs2.getFloat("d.fc_discount");
				float fc_tax_rate = rs2.getFloat("d.fc_tax_rate");
				String hsn_code = rs2.getString("d.hsn_code");
				String barcode = rs2.getString("d.barcode");
				// Double cost=rs2.getDouble("d.cost");

				SalesTransHdr salesTransHdr = new SalesTransHdr();
				Optional<BranchMst> branchMstOpt = branchMstRepository.findById(branch_id);
				if (branchMstOpt.isPresent()) {
					salesTransHdr.setBranchCode(branchMstOpt.get().getBranchCode());
				}
				salesTransHdr.setCompanyMst(companyMst);
				Optional<AccountHeads> customerMstOpt = accountHeadsRepository.findById(customer_id);
				if (customerMstOpt.isPresent()) {
					salesTransHdr.setAccountHeads(customerMstOpt.get());
				}
				salesTransHdr.setId(record_id);
				salesTransHdr.setUserId(user_id);
				salesTransHdr.setMachineId(machine_id);

				java.util.Date voucherDate = SystemSetting.StringToUtilDate(invoice_date, "yyyy-MM-dd");

				salesTransHdr.setVoucherDate(voucherDate);
				salesTransHdr.setSalesManId(salesman_id);
				salesTransHdr.setVoucherNumber(voucher_number);
				salesTransHdr.setInvoiceAmount(Double.valueOf(invoice_amount));

				salesTransHdr.setInvoiceDiscount(Double.valueOf(invoice_discount));

				salesTransHdrRepository.save(salesTransHdr);

				SalesDtl salesDtl = new SalesDtl();
				salesDtl.setId(dRecordId);
				salesDtl.setBatch(batch_code);
				salesDtl.setAmount(Double.valueOf(rate * qty));
//			salesDtl.setCessAmount(cessAmount);
//			salesDtl.setIgstAmount(igstAmount);
				salesDtl.setItemId(item_id);
				salesDtl.setItemName(item_name);
				salesDtl.setBarcode(barcode);
				java.sql.Date expDate = SystemSetting.StringToSqlDate(expiry_date, "yyyy-MM-dd");
				salesDtl.setExpiryDate(expDate);
				salesDtl.setDiscount(Double.valueOf(fc_discount));
				// salesDtl.setIgstTaxRate(igstTaxRate);
				// salesDtl.setCompanyMst(companyMst);
				// salesDtl.setFcCgst(fcCgst);
				// salesDtl.setFcIgstRate(fcIgstRate);
				// salesDtl.setFcMrp(cost);
				// salesDtl.setFcStandardPrice(fcStandardPrice);
				salesDtl.setFcRate(Double.valueOf(fc_rate));
				salesDtl.setFcDiscount(Double.valueOf(fc_discount));
				salesDtl.setFcTaxRate(Double.valueOf(fc_tax_rate));
				salesDtl.setItemTaxaxId("NOT IN USE");
				salesDtl.setOfferReferenceId("NOT IN USE");
				salesDtl.setUnitId(unit_id);
				Optional<SalesTransHdr> salesTransHdrOpt = salesTransHdrRepository.findById(record_id);
				if (salesTransHdrOpt.isPresent()) {
					salesDtl.setSalesTransHdr(salesTransHdrOpt.get());
				}
				salesDetailsRepository.save(salesDtl);

			}

			 rs2.close();
			  pst2.close();
			  
			  return "OK";
		}

		@Override
		public String doPurchaseTransfer(CompanyMst companyMst, String branchcode) throws UnknownHostException, Exception {

			/*
			 * String sqlstr2
			 * =" select h.LOGGED_USER,h.TR_ENTRY_DATE, h.invoice_date,h.net_invoice_total,h.COMPANY_ID,"
			 * +
			 * "h.branch_id,h.godown_id,h.record_id,h.machine_id,h.NARRATION,h.currency_name,h.conversion_rate,h.final_saved,h.purchase_typ,h.po_number,h.grn_number,h.is_grn_loaded,d.record_id,d.parent_id,d.item_name,d.item_id,d.batch,d.bin_no,d.expiry_date,d.manufacturing_date,d.qty,d.purchase_rate,d.unit_id,d.tax_rate,d.sale_rate,d.whole_sales_rate,d.updated_to_server,d.barcode,d.fc_rate,d.net_cost,d.purchase_date_time,d.item_code,d.effective_purchase_cost,d.mrp,d.previous_mrp,d.machine_id,d.tax_amount,d.exp_allocation,d.isfreeqty from purchase_hdr h,purchase_dtl d where h.record_id=d.parent_id"
			 * ;
			 */

			String sqlstr2 = "select * from purchase_hdr";
			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();
			while (rs2.next()) {
				int logged_user = rs2.getInt("LOGGED_USER");
				Date tr_entry_date = rs2.getDate("TR_ENTRY_DATE");
				// Date invoice_date=rs2.getDate("invoice_number");
				String invoice_number = rs2.getString("invoice_number");
				String voucher_number = rs2.getString("voucher_number");
				int instance_id = rs2.getInt("instance_id");
				int supplier_id = rs2.getInt("supplier_id");
				int updated_to_server = rs2.getInt("updated_to_server");
				int posted_to_tally = rs2.getInt("posted_to_tally");
				String tally_voucher_id = rs2.getString("tally_voucher_id");
				float net_invoice_total = rs2.getFloat("net_invoice_total");
				int COMPANY_ID = rs2.getInt("COMPANY_ID");
				int branch_id = rs2.getInt("branch_id");
				int godown_id = rs2.getInt("godown_id");
				int record_id = rs2.getInt("record_id");
				String machine_id = rs2.getString("machine_id");
				String NARRATION = rs2.getString("NARRATION");
				String currency_name = rs2.getString("currency_name");
				Double conversion_rate = rs2.getDouble("conversion_rate");
				int final_saved = rs2.getInt("final_saved");
				String purchase_typ = rs2.getString("purchase_typ");
				String po_number = rs2.getString("po_number");
				String grn_number = rs2.getString("grn_number");
				int is_grn_loaded = rs2.getInt("is_grn_loaded");

				PurchaseHdr purchaseHdr = new PurchaseHdr();

				purchaseHdr.setCompanyMst(companyMst);

				Optional<BranchMst> branchMstOpt = branchMstRepository.findById(String.valueOf(branch_id));
				if (branchMstOpt.isPresent()) {
					purchaseHdr.setBranchCode(branchMstOpt.get().getBranchCode());
				}
				// purchaseHdr.setFcInvoiceTotal(fcInvoiceTotal);

				purchaseHdr.setVoucherNumber(voucher_number);
				purchaseHdr.setMachineId(machine_id);
				purchaseHdr.setNarration(NARRATION);
				purchaseHdr.setSupplierId(String.valueOf(supplier_id));
				purchaseHdr.setTansactionEntryDate(tr_entry_date);
				purchaseHdr.setUserId(String.valueOf(logged_user));
				// purchaseHdr.setVoucherDate(invoice_date);
				// purchaseHdr.setPurchaseType(purchaseType);
				String recordId = String.valueOf(record_id);
				purchaseHdr.setId(recordId);
				purchaseHdr.setpONum(po_number);
				// purchaseHdr.setCurrency(currency);
				purchaseHdrRepository.save(purchaseHdr);

				System.out.print(purchaseHdr.getId() + "purchase hdr id isssssssssssssssssssssssssssssss");
			}

			 rs2.close();
			  pst2.close();
			  
			  return "OK";
		}

		@Override
		public String doItemBatchDtlTransfer(CompanyMst companyMst, String branchcode) throws SQLException {

			String sqlstr2 = " select item_id,barcode,batch_code,item_code,sale_qty,purchase_qty,unit_id,posted_to_server,qty_modification_id,machine_id,branch_code,record_id,description,source_record_id,trans_date,purchase_rate,selling_rate,profit,expiry_date,godown_code,tax_rate ,from_branch_id,to_branch_id,trans_in_record,from_branch_name,to_branch_name,party_name,voucher_number,voucher_date from item_batch_dtl ";

			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();
			while (rs2.next()) {

				int item_id = rs2.getInt("item_id");
				String barcode = rs2.getString("barcode");
				String batch_code = rs2.getString("batch_code");
				String item_code = rs2.getString("item_code");
				Double sale_qty = rs2.getDouble("sale_qty");
				Double purchase_qty = rs2.getDouble("purchase_qty");
				int unit_id = rs2.getInt("unit_id");
				int posted_to_server = rs2.getInt("posted_to_server");
				String qty_modification_id = rs2.getString("qty_modification_id");
				String machine_id = rs2.getString("machine_id");
				String branch_code = rs2.getString("branch_code");
				int record_id = rs2.getInt("record_id");

				String description = rs2.getString("description");
				String source_record_id = rs2.getString("source_record_id");
				Date trans_date = rs2.getDate("trans_date");
				Double purchase_rate = rs2.getDouble("purchase_rate");
				Double selling_rate = rs2.getDouble("selling_rate");
				Double profit = rs2.getDouble("profit");
				Date expiry_date = rs2.getDate("expiry_date");
				String godown_code = rs2.getString("godown_code");
				Double tax_rate = rs2.getDouble("tax_rate");
				int from_branch_id = rs2.getInt("from_branch_id");
				int to_branch_id = rs2.getInt("to_branch_id");
				String trans_in_record = rs2.getString("trans_in_record");
				String from_branch_name = rs2.getString("from_branch_name");
				String to_branch_name = rs2.getString("to_branch_name");
				// int srl_no=rs2.getInt("srl_no");
				String party_name = rs2.getString("party_name");
				String voucher_number = rs2.getString("voucher_number");
				Date voucher_date = rs2.getDate("voucher_date");
//			String from_branch_code=rs2.getString("from_branch_code");
//			String to_branch_code=rs2.getString("to_branch_code");

				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
				itemBatchDtl.setId(String.valueOf(record_id));
				itemBatchDtl.setBarcode("batch_code");
				itemBatchDtl.setItemId(String.valueOf(item_id));
				// itemBatchDtl.setMrp(mrp);
				itemBatchDtl.setVoucherDate(voucher_date);
				itemBatchDtl.setVoucherNumber(voucher_number);
				itemBatchDtl.setExpDate(expiry_date);
				itemBatchDtl.setBatch(batch_code);
				itemBatchDtl.setQtyOut(sale_qty);
				itemBatchDtl.setQtyIn(purchase_qty);
				itemBatchDtl.setCompanyMst(companyMst);
				itemBatchDtl.setStore("NO VALU PRESENT");

				itemBatchDtlRepository.save(itemBatchDtl);

				System.out.print("out side the item batch dtl save");

			}
			
			
			 rs2.close();
			  pst2.close();
			  return null;
		}
			

		private double getAccountBalance(String customer_id) {

			return 0;
		}

		
		

		@Override
		public String transferCustomer(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {
//		Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

			String sqlstr2 = "select * from account_heads ";

			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {
				String customer_name = rs2.getString("account_name");
				String customer_id = rs2.getString("id");
				String tin_no = rs2.getString("party_gst");
				// String customer_state = rs2.getString("customer_state");
				// String customer_state = "KERALA" ; //rs2.getString("customer_state");
				String customer_contry = "INDIA";
				String price_level_id = rs2.getString("price_level_id");
				String telephone_no = rs2.getString("customer_contact");
				String address1 = rs2.getString("party_address1");
				String mobileno = rs2.getString("customer_contact");
				String customerState = "KERALA";// rs2.getString("customer_state");
				double account_balance = getAccountBalance(customer_id);

				AccountHeads customerMst = new AccountHeads();
				customerMst.setCompanyMst(companyMst);
				customerMst.setCreditPeriod(0);
				customerMst.setPartyAddress1(address1);
				customerMst.setCustomerContact(mobileno);
				customerMst.setPartyGst(tin_no);
				customerMst.setAccountName(customer_name);
				customerMst.setPartyMail("-");
				customerMst.setCustomerState(address1);
				customerMst.setCustomerState("KERALA");
				customerMst.setCustomerRank(0);
				customerMst.setCustomerCountry("INDIA");
				customerMst.setId(customer_id);
				customerMst.setPriceTypeId(price_level_id);

				AccountHeads savedCustomer = accountHeadsRepository.save(customerMst);

				AccountHeads parentAccountHead = accountHeadsRepository
						.findByAccountNameAndCompanyMstId(savedCustomer.getAccountName(), companyMst.getId());
				if (null == parentAccountHead || null == parentAccountHead.getId()) {

					AccountHeads accountHeads = new AccountHeads();
					accountHeads.setAccountName(savedCustomer.getAccountName());
					accountHeads.setGroupOnly("N");
					accountHeads.setId(savedCustomer.getId());
					accountHeads.setCompanyMst(savedCustomer.getCompanyMst());
					accountHeadsRepository.save(accountHeads);

				}

				/*
				 * Map<String, Object> variables1 = new HashMap<String, Object>();
				 * variables1.put("voucherNumber", savedCustomer.getId());
				 * variables1.put("voucherDate",SystemSetting.getSystemDate());
				 * variables1.put("inet", 0); variables1.put("id", savedCustomer.getId());
				 * variables1.put("companyid", savedCustomer.getCompanyMst());
				 * variables1.put("REST",1);
				 * 
				 * 
				 * variables1.put("WF", "forwardAccountHeads"); eventBus.post(variables1);
				 * 
				 * 
				 * Map<String, Object> variables = new HashMap<String, Object>();
				 * variables.put("voucherNumber", savedCustomer.getId());
				 * variables.put("voucherDate",SystemSetting.getSystemDate());
				 * variables.put("inet", 0); variables.put("id", savedCustomer.getId());
				 * variables.put("companyid", savedCustomer.getCompanyMst());
				 * variables.put("REST",1);
				 * 
				 * variables.put("WF", "forwardCustomer"); eventBus.post(variables);
				 * 
				 */

			}
			 rs2.close();
			  pst2.close();
			  
			  return "OK";
		}


public String getCategoryNameFromId(int catId, Connection Conn) throws SQLException {

	String sqlstr2 = " SELECT  group_name from inv_category_mst where record_id =   " + catId;

	PreparedStatement pst2 = Conn.prepareStatement(sqlstr2);

	String groupName = "";
	ResultSet rs2 = pst2.executeQuery();

	while (rs2.next()) {
		groupName = rs2.getString("group_name");
	}

	 rs2.close();
	  pst2.close();
	  
	return groupName;

}
	  


public int isKitItem(int itemid, Connection Conn) throws SQLException {

	boolean result = false;

	String sqlstr2 = " SELECT  count(*)  from item_group_dtl where item_id  =   " + itemid + " AND group_id = 400 ";

	PreparedStatement pst2 = Conn.prepareStatement(sqlstr2);

	int count = 0;
	ResultSet rs2 = pst2.executeQuery();

	while (rs2.next()) {
		count = rs2.getInt(1);
	}

	 rs2.close();
	  pst2.close();
	  return count;

}
	  
@Override
public String doItemTransfer(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {

//RestserverApplication.RestserverApplication.localCn = getDbConn("SOURCEDB");

	// RestserverApplication.localCn = RestserverApplication.getDbConn("SOURCEDB");

	String sqlstr2 = " select  record_id , item_name , unit_id , tax_rate, standard_price , barcode, item_group_id, category_id, "
			+ " item_code, cess_rate,    hsn_code , item_code from item_mst   ";

	PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
//barCode_Line1,barCode_Line2 , best_Before,
	ResultSet rs2 = pst2.executeQuery();

	while (rs2.next()) {

		String unitname = "";
		String itemName = rs2.getString("item_name");

		itemName = itemName.replaceAll("'", "");
		itemName = itemName.replaceAll("&", "");

		itemName = itemName.replaceAll("/", "");
		itemName = itemName.replaceAll("%", "");
		itemName = itemName.replaceAll("\"", "");
		// itemName = itemName.replaceAll(" ", "-");

		int itemId = rs2.getInt("record_id");
		double taxRate = rs2.getDouble("tax_rate");
		String barcode = rs2.getString("barcode");
		double standardPrice = rs2.getDouble("standard_price");

		int item_group_id = rs2.getInt("item_group_id");
		int category_id = rs2.getInt("category_id");
		int unit_id = rs2.getInt("unit_id");
		// String barCodeLine1 = rs2.getString("barCode_Line1");
		// String barCodeLine2 = rs2.getString("barCode_Line2");
		// int bestBefore = rs2.getInt("best_Before");
		String hsn_code = rs2.getString("hsn_code");
		String item_code = rs2.getString("item_code");

		Integer rank = 0;
		String serviceOrGoods = "Goods";
		String sqlstr3 = " SELECT  group_name FROM inv_category_mst where  record_id  in (" + item_group_id + ", "
				+ category_id + ")";
		PreparedStatement pst3 = RestserverApplication.localCn.prepareStatement(sqlstr3);

		ResultSet rs3 = pst3.executeQuery();

		while (rs3.next()) {

			String groupName = rs3.getString("group_name");

			CategoryMst categoryMst = categoryMstRepository.findByCompanyMstIdAndCategoryName(companyMst.getId(),
					groupName);

			if (null == categoryMst) {
				categoryMst = new CategoryMst();

				categoryMst.setCategoryName(groupName);
				categoryMst.setCompanyMst(companyMst);
				VoucherNumber vNo = voucherNumberService.generateInvoice("CAT", companyMst.getId());
				categoryMst.setId(vNo.getCode());

				categoryMstRepository.saveAndFlush(categoryMst);
			}

		}
		rs3.close();
		pst3.close();

		String sqlstr4 = " SELECT  unit_name, unit_description FROM unit_mst  where  record_id  in (" + unit_id
				+ ")";
		PreparedStatement pst4 = RestserverApplication.localCn.prepareStatement(sqlstr4);

		ResultSet rs4 = pst4.executeQuery();

		while (rs4.next()) {

			unitname = rs4.getString("unit_name");
			String unitDescription = rs4.getString("unit_description");

			UnitMst unitMst = unitMstRepository.findByUnitNameAndCompanyMstId(unitname, companyMst.getId());

			if (null == unitMst) {
				unitMst = new UnitMst();

				VoucherNumber vNo = voucherNumberService.generateInvoice("UNIT", companyMst.getId());
				unitMst.setId(vNo.getCode());
				unitMst.setUnitName(unitname);
				unitMst.setUnitDescription(unitDescription);
				unitMst.setCompanyMst(companyMst);
				unitMstRepository.saveAndFlush(unitMst);
			}

		}
		rs4.close();
		pst4.close();
		
		ItemMst itemMst = itemMstRepository.findByItemName(itemName);

		if (null == itemMst) {

			String categoryName = getCategoryNameFromId(category_id, RestserverApplication.localCn);
			CategoryMst categoryMst = categoryMstRepository.findByCompanyMstIdAndCategoryName(companyMst.getId(),
					categoryName);

			String groupName = getCategoryNameFromId(item_group_id, RestserverApplication.localCn);
			CategoryMst categoryMst2 = categoryMstRepository.findByCompanyMstIdAndCategoryName(companyMst.getId(),
					groupName);

			UnitMst unitMst = unitMstRepository.findByUnitNameAndCompanyMstId(unitname, companyMst.getId());

			int isKit = isKitItem(itemId, RestserverApplication.localCn);

			itemMst = new ItemMst();
			itemMst.setBarCode(barcode);
			// itemMst.setBarCodeLine1(barCodeLine1);
			// itemMst.setBarCodeLine2(barCodeLine2);
			// itemMst.setBestBefore(bestBefore);
			itemMst.setBranchCode(branchCode);
			itemMst.setRank(rank);
			itemMst.setServiceOrGoods(serviceOrGoods);
			if (null != categoryMst && null != categoryMst.getId()) {
				itemMst.setCategoryId(categoryMst.getId());
			}
			itemMst.setCess(0.0);
			itemMst.setCgst(taxRate / 2);
			itemMst.setCompanyMst(companyMst);
			itemMst.setHsnCode(hsn_code);

			VoucherNumber vNo = voucherNumberService.generateInvoice("ITEM", companyMst.getId());

			itemMst.setId(vNo.getCode());

			itemMst.setIsDeleted("N");

			itemMst.setIsKit(isKit);
			itemMst.setItemCode(item_code);
			if (null != categoryMst2 && null != categoryMst2.getId()) {
				itemMst.setItemGroupId(categoryMst2.getId());
			}
			itemMst.setItemName(itemName);
			itemMst.setBestBefore(0);
			itemMst.setStandardPrice(standardPrice);
			itemMst.setSgst(taxRate / 2);
			itemMst.setTaxRate(taxRate);

			if (null == unitMst) {
				unitMst = unitMstRepository.findByUnitNameAndCompanyMstId("NOS", companyMst.getId());

			}
			itemMst.setUnitId(unitMst.getId());

			itemMst = itemMstRepository.saveAndFlush(itemMst);

		}

	}
	
	 
	 rs2.close();
	  pst2.close();
	  
	return "OK";
}


@Override
public String transferAccountHeads(CompanyMst companyMst, String branchCode)
		throws UnknownHostException, Exception {

	// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

	String sqlstr2 = "select * from account_heads ";

	PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

	ResultSet rs2 = pst2.executeQuery();

	while (rs2.next()) {
		String account_id = rs2.getString("account_id");
		String account_name = rs2.getString("account_name");
		String parent_account_id = rs2.getString("parent_account_id");

		String group_only = rs2.getString("group_only");
		String primary_group = rs2.getString("primary_group");

		// Find if account exist.
		// If not exist , add account

		AccountHeads parentAccountHead = accountHeadsRepository.findByAccountNameAndCompanyMstId(account_name,
				companyMst.getId());
		if (null == parentAccountHead || null == parentAccountHead.getId()) {

			AccountHeads accountHeads = new AccountHeads();
			accountHeads.setAccountName(account_name);
			accountHeads.setGroupOnly(group_only);
			accountHeads.setParentId(parent_account_id);
			accountHeads.setId(account_id);

			accountHeads.setCompanyMst(companyMst);
			accountHeadsRepository.save(accountHeads);

			/*
			 * Map<String, Object> variables1 = new HashMap<String, Object>();
			 * variables1.put("voucherNumber", parent_account_id);
			 * variables1.put("voucherDate",SystemSetting.getSystemDate());
			 * variables1.put("inet", 0); variables1.put("id", parent_account_id);
			 * variables1.put("companyid", companyMst); variables1.put("REST",1);
			 * 
			 * 
			 * variables1.put("WF", "forwardAccountHeads"); eventBus.post(variables1);
			 * 
			 */

		}

	}
	rs2.close();
	
	pst2.close();
	return "OK";
}



@Override
public String invCategoryMstTransferAmdc(CompanyMst companyMst, String branchCode)throws UnknownHostException, Exception {

	/*
	* INV_CATEGORY_MST
	 */
			String sqlstr2 = " select  * from inv_category_mst  ";

			PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

			ResultSet rs2 = pst2.executeQuery();

			while (rs2.next()) {

				CategoryMst categoryMst = new CategoryMst();

				categoryMst.setId(String.valueOf(rs2.getInt("record_id")));
				categoryMst.setParentId(String.valueOf(rs2.getInt("parent_id")));
				categoryMst.setCategoryName(rs2.getString("group_name"));
				categoryMst.setOldId(String.valueOf(rs2.getInt("record_id")));
			

				categoryMst.setCompanyMst(companyMst);
				categoryMst = categoryMstRepository.save(categoryMst);
				
				
				
			}
			
			rs2.close();
			
			pst2.close();
		return "OK";
}
@Override
public String patientDtlTransferambc(CompanyMst companyMst, String branchCode)throws UnknownHostException, Exception  {


	/*
	 * patientDtl
	 */

	String sqlstr2 = " select  * from patient_dtl  ";

	PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

	ResultSet rs2 = pst2.executeQuery();

	while (rs2.next()) {

		PatientDtl patientDtl = new PatientDtl();

		patientDtl.setInsuranceCompany(rs2.getString("insurance_company"));
		patientDtl.setInsCard(rs2.getString("ins_card"));
		patientDtl.setInsCardExpiryDate(rs2.getDate("ins_card_expiry_date"));
		patientDtl.setDateOfBirth(rs2.getDate("date_of_birth"));
		patientDtl.setGender(rs2.getString("gender"));
		patientDtl.setNationality(rs2.getString("nationality"));
		patientDtl.setNationalId(rs2.getString("national_id"));
		patientDtl.setPolicyType(rs2.getString("policy_type"));
		patientDtl.setPercentDiscount (rs2.getDouble("percent_discount "));
		patientDtl.setHospitalId(rs2.getString("hospital_id"));
		patientDtl.setCustomerId(rs2.getString("customer_id"));
		patientDtl.setPatientId(rs2.getString("patient_id"));
		patientDtl.setPostedToServer(rs2.getString("posted_to_server"));
		
		
		patientDtl.setCompanyMst(companyMst);
	

		patientDtl = patientDtlRepository.save(patientDtl);

	}
	rs2.close();
	
	pst2.close();
	return "OK";

}

@Override
public String doDataTransferUserMst(CompanyMst companyMst) throws UnknownHostException, Exception {

	String sqlstr2 = "select USERID,CHAR_USERID,PASSWORD,USERNAME,manager_id,task_list_id,enabled,branch_code,COMPANY_CODE,invoice_form from user_mst ";

	PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

	ResultSet rs2 = pst2.executeQuery();
	while (rs2.next()) {

		String USERNAME = rs2.getString("USERNAME");
		String PASSWORD = rs2.getString("PASSWORD");
		String branch_code = rs2.getString("branch_code");
		Double USERID = rs2.getDouble("USERID");

		UserMst userMst = new UserMst();
		userMst.setCompanyMst(companyMst);
		userMst.setBranchCode(branch_code);
		userMst.setPassword(PASSWORD);
		userMst.setUserName(USERNAME);
		userMst.setId(String.valueOf(USERID));
		userMstRepository.save(userMst);

	}
	rs2.close();
	
	pst2.close();
	return null;
}

@Override
public String doDataTransferUnitMst(CompanyMst companyMst, String branch) throws UnknownHostException, Exception {

	String sqlstr2 = "select * from unit_mst ";

	PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

	ResultSet rs2 = pst2.executeQuery();
	while (rs2.next()) {

		int record_id = rs2.getInt("record_id");
		String unit_name = rs2.getString("unit_name");
		String unit_description = rs2.getString("unit_description");
		float unit_precision = rs2.getFloat("unit_precision");
		String machine_id = rs2.getString("machine_id");
		// int tally_posted=rs2.getInt("tally_posted");

		UnitMst unitMst = new UnitMst();
		unitMst.setId(String.valueOf(record_id));
		unitMst.setUnitName(unit_name);
		unitMst.setUnitPrecision(Double.valueOf(unit_precision));
		unitMst.setCompanyMst(companyMst);
		unitMst.setUnitDescription(unit_description);
		unitMstRepository.save(unitMst);
	}
	
	rs2.close();
	
	pst2.close();
	return "OK";
}


@Override
public String branchMstTransferAmdc(CompanyMst companyMst, String branchCode)throws UnknownHostException, Exception  {
	

	String sqlstr2 = "select branch_code ,branch_name,COMPANY_CODE,id,branch_email,branch_pin,branch_address3,branch_address2,branch_address1,record_id,godown_created from branch_mst";

	PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

	ResultSet rs2 = pst2.executeQuery();
	while (rs2.next()) {

		String branch_code = rs2.getString("branch_code");
		String branch_name = rs2.getString("branch_name");
		String COMPANY_CODE = rs2.getString("COMPANY_CODE");
		int id = rs2.getInt("id");
		String branch_email = rs2.getString("branch_email");
		String branch_pin = rs2.getString("branch_pin");
		String branch_address3 = rs2.getString("branch_address3");
		String branch_address2 = rs2.getString("branch_address2");
		String branch_address1 = rs2.getString("branch_address1");
		int record_id = rs2.getInt("record_id");
		int godown_created = rs2.getInt("godown_created");
		// String branch_state=rs2.getString("branch_state");
		// String branch_country=rs2.getString("branch_country");
		// String branch_tel=rs2.getString("branch_tel");
		// String branch_gst=rs2.getString("branch_gst");

		BranchMst branchMst = new BranchMst();
		branchMst.setBranchCode(branch_code);
		branchMst.setId(String.valueOf(id));
		branchMst.setBranchName(branch_name);
//	branchMst.setBranchGst(branch_gst);

		branchMst.setBranchAddress1(branch_address1);
		branchMst.setCompanyMst(companyMst);
		branchMst.setBranchAddress2(branch_address2);
		branchMst.setBranchEmail(branch_email);
//	branchMst.setBranchTelNo(branch_tel);
		branchMstRepository.save(branchMst);

	}
	rs2.close();
	
	pst2.close();
	
	return null;

}



@Override
public String consumptionDtlTransferamdc(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception   {


	/*
	 * ConsumptionDtl
	 */

	String sqlstr2 = " select  * from consumption_dtl  ";

	PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

	ResultSet rs2 = pst2.executeQuery();

	while (rs2.next()) {

		ConsumptionDtl consumptionDtl = new ConsumptionDtl();
/*
		consumptionDtl.setCnParentId(rs2.getString("cn_parent_id"));
		consumptionDtl.setCnDate(rs2.getDate("cn_date"));
		consumptionDtl.setCnVoucher(rs2.getString("cn_voucher"));
		consumptionDtl.setCnDepartment(rs2.getString("cn_department"));
		consumptionDtl.setCnValue(rs2.getDouble("cn_value"));
		consumptionDtl.setRecordId(rs2.getString("record_id"));
		consumptionDtl.setMachineId(rs2.getString("machine_id"));
		consumptionDtl.setItemId(rs2.getInt("item_id"));
		consumptionDtl.setQty(rs2.getDouble("qty"));
		consumptionDtl.setRate(rs2.getDouble("rate"));
		consumptionDtl.setTaxRate(rs2.getDouble("tax_rate"));
		consumptionDtl.setAmount(rs2.getDouble("amount"));
		consumptionDtl.setUnitId(rs2.getInt("unit_id"));
		consumptionDtl.setItemSerialNo(rs2.getInt("item_serial_no"));
		consumptionDtl.setBatchCode(rs2.getString("batch_code"));
		consumptionDtl.setManufacturingDate(rs2.getDate("manufacturing_date"));
		consumptionDtl.setExpiryDate(rs2.getDate("expiry_date"));
		consumptionDtl.setTransferRate (rs2.getDouble("transfer_rate "));
		consumptionDtl.setGodownCode(rs2.getString("godown_code"));
		consumptionDtl.setVoucherType(rs2.getString("voucher_type"));
		consumptionDtl.setOrgQty(rs2.getDouble("org_qty"));
		
		
		
		
		consumptionDtl.setCompanyMst(companyMst);
		consumptionDtl.setRank(0);

		consumptionDtl = consumptionDtlRepository.save(consumptionDtl);
*/
		
	}
	rs2.close();
	
	pst2.close();
	return "OK";

}

@Override
public String consumptionHdrTransferamdc(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception   {


	/*
	 * ConsumptionHdr
	 */

	String sqlstr2 = " select  * from consumption_hdr  ";

	PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

	ResultSet rs2 = pst2.executeQuery();

	while (rs2.next()) {
/*
		ConsumptionHdr consumptionHdr = new ConsumptionHdr();

		consumptionHdr.setTrnDate(rs2.getDate("trn_date"));
		consumptionHdr.setCnDate(rs2.getDate("cn_date"));
		consumptionHdr.setCnVoucher(rs2.getString("cn_voucher"));
		consumptionHdr.setCnDepartment(rs2.getString("cn_department"));
		consumptionHdr.setCnValue(rs2.getDouble("cn_value"));
		consumptionHdr.setRecordId(rs2.getString("record_id"));
		consumptionHdr.setMachineId(rs2.getString("machine_id"));
		consumptionHdr.setBranchId(rs2.getInt("branch_id"));
		consumptionHdr.setCompanyId(rs2.getInt("company_id"));
		consumptionHdr.setPostedToServer(rs2.getInt("posted_to_server"));
		consumptionHdr.setPostedToTally(rs2.getInt("posted_to_tally"));
		consumptionHdr.setTallyVoucherId(rs2.getString("tally_voucher_id"));
		consumptionHdr.setUserid(rs2.getInt("userid"));
		consumptionHdr.setFinalSaveStatus(rs2.getString("final_save_status"));
		consumptionHdr.setVoucherType(rs2.getString("voucher_type"));
		consumptionHdr.setRemark(rs2.getString("remark"));
		
		
		
		
		
		consumptionHdr.setCompanyMst(companyMst);
		consumptionHdr.setRank(0);

		consumptionHdr = consumptionHdrRepository.save(consumptionHdr);
*/
		
	}
	rs2.close();
	
	pst2.close();
	return "OK";

}





}
