package com.maple.restserver.service;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.WebsiteClass;

@Service
public interface WebsiteModelService {

	WebsiteClass getWebsiteValues(CompanyMst companyMst);

}
