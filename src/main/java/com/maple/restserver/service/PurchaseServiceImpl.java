package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AccountPayable;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.PurchaseHdrMessageEntity;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.AccountPayableRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;

@Component
@Service
public class PurchaseServiceImpl implements PurchaseService{

	
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;
	@Autowired
	UnitMstRepository unitMstRepo;
	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	@Autowired
	ItemMstRepository itemMstRepository;
	@Autowired
	CategoryMstRepository categoryMstRepo;
	@Autowired
	AccountPayableRepository accountPayableRepo;
	@Autowired
	PurchaseDtlRepository purchaseDtlRepo;

	@Autowired
	PurchaseHdrRepository purchaseHdrRepo;

	@Override
	public PurchaseHdrMessageEntity  createPurchaseMsgEntity(String voucherNumber,Date voucherDate) {
	
		
		List<PurchaseHdr> hdrIdS = purchaseHdrRepo.findByVoucherNumberAndVoucherDate(voucherNumber,
				voucherDate);
		

		String id = hdrIdS.get(0).getId();
		ArrayList<PurchaseHdrMessageEntity> purchaseMsgEntityArray =new ArrayList<PurchaseHdrMessageEntity>();
		PurchaseHdrMessageEntity purchaseMessageEntity = new PurchaseHdrMessageEntity();
		ArrayList<ItemMst> itemList = new ArrayList<ItemMst>();
		ArrayList<CategoryMst> catList = new ArrayList<CategoryMst>();
		ArrayList<UnitMst> unitList = new ArrayList<UnitMst>();
		purchaseMessageEntity.setPurchaseHdr(hdrIdS.get(0));
		List<PurchaseDtl> purchaseDtl = purchaseDtlRepo.findByPurchaseHdrId(id);
		purchaseMessageEntity.getPurchaseDtlList().addAll(purchaseDtl);
		AccountPayable accountPayable = accountPayableRepo
				.findByCompanyMstAndPurchaseHdr(hdrIdS.get(0).getCompanyMst(), hdrIdS.get(0));
		purchaseMessageEntity.getAccountPayablelist().add(accountPayable);
		AccountHeads accountHeads = accountHeadsRepository.findById(hdrIdS.get(0).getSupplierId()).get();
		purchaseMessageEntity.getAccountHeadsList().add(accountHeads);
		try {
		for (int i = 0; i < purchaseDtl.size(); i++) {
			ItemMst itemmst = itemMstRepository.findById(purchaseDtl.get(i).getItemId()).get();
			itemList.add(itemmst);
			CategoryMst catmst = categoryMstRepo.findById(itemmst.getCategoryId()).get();
			catList.add(catmst);
			UnitMst unitmst = unitMstRepo.findById(itemmst.getUnitId()).get();
			unitList.add(unitmst);
		}
		purchaseMessageEntity.getItemList().addAll(itemList);
		purchaseMessageEntity.getCategoryList().addAll(catList);
		purchaseMessageEntity.setItemCount(purchaseDtl.size());
		purchaseMessageEntity.getUnitMstList().addAll(unitList);
//	purchaseMsgEntityArray.add(purchaseMessageEntity);
	}  
		
		catch (Exception e) {
			// TODO: handle exception
		}
		return purchaseMessageEntity;
	}

	@Override
	public String updatePurchasePostedToServerStatus(List<String> hdrIds) {

		try {
	//=============MAP-104=======commented for api based publishing=================		
//			purchaseHdrRepo.updatePurchasePostedToServerStatus(hdrIds);

		} catch (Exception e) {
			e.printStackTrace();
			return "FAILED";
		}
		return "SUCCESS";
	}
		
	
	
}
