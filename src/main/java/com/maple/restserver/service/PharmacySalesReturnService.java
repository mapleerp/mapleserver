package com.maple.restserver.service;

import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.PharmacySalesReturnReport;

@Service
public interface PharmacySalesReturnService {

	
	List<PharmacySalesReturnReport>getPharmacySalesReturn(CompanyMst companyMst, java.sql.Date fdate,java.sql.Date tdate,String branchCode,String[]array);

	List<PharmacySalesReturnReport> getPharmacySalesReturnWithoutCategory(CompanyMst companyMst, Date fdate, Date tdate,
			String branchCode);
}
