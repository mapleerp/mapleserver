package com.maple.restserver.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.entity.LastBestDate;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.LastBestDateRepository;

@Service
public class LastBestDateServiceSales extends LastBestDateService {

	@Autowired
	LastBestDateRepository lastBestDateRepository;

	@Autowired
	DayEndClosureRepository dayEndClosureRepository;

	@Override
	public LastBestDate findLastBestDate() {
		
		System.out.println("Entered into LastBestDateServiceSales.findLastBestDate() method****************************");

		lastBestDate = lastBestDateRepository.findByActivityType("SALES");
		
		if (null == lastBestDate.getLastSuccessDate()) {
			return null;
		}
		
		System.out.println(super.getCompanymstid());
		System.out.println(super.getBranchcode());


		List<DayEndClosureHdr> dayenddate = dayEndClosureRepository.getMaxOfDayEnd(super.getCompanymstid(),
				super.getBranchcode());

		
		if (dayenddate.size() > 0) {
			Date saledayend = dayenddate.get(0).getProcessDate();
			lastBestDate.setDayEndDate(saledayend);

		} else {

			lastBestDate.setDayEndDate(lastBestDate.getLastSuccessDate());
		}
		
		
		Date date = new Date(lastBestDate.getLastSuccessDate().getTime()+(1000*60*60*24));
		lastBestDate.setLastSuccessDate(date);
		
		
	
		
		int dateComparison =  lastBestDate.getLastSuccessDate().compareTo(lastBestDate.getDayEndDate());
		
		if(dateComparison > 0 )
		{
			lastBestDate = null;
		}

		System.out.println("The result of LastBestDateServiceSales.findLastBestDate() method ====> " + lastBestDate);
		return lastBestDate;

	}

}
