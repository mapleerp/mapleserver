package com.maple.restserver.service;

import java.sql.Date;
import java.util.List;

import com.maple.restserver.entity.StockTransferOutHdr;


public interface StockTransferOutHdrService {

	public  List<Object> getStockTransferOutHdr(String intentNumber,Date voucherDate);
	public   List<StockTransferOutHdr>getAllStockTransferOutHdr();
	
	
	String updateStockTransferOutHdrPostedToServerStatus(List<String> hdrIds);
	
	
}
