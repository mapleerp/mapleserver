package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReceiptModeMst;
import com.maple.restserver.report.entity.ReceiptModeReport;
@Service
public interface ReceiptModeMstService {


	List<ReceiptModeReport> findTotalCardAmount(CompanyMst companyMst, String branchcode, Date sdate);
	
	
	String initializeNewReceiptMode(String companymstid,String branchcode);

	

}
