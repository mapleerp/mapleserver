package com.maple.restserver.service;

import java.sql.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.report.entity.TransactionCountModel;
import com.maple.restserver.repository.ActualProductionHdrRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.ProductionMstRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;

@Service
@Transactional
@Component
public class TransactionCountServiceImpl implements TransactionCountService{

	@Autowired
	PaymentHdrRepository paymentHdrRepo;
	
	@Autowired
	ReceiptHdrRepository receiptHdrRepo;
	@Autowired
	AccountClassRepository accountClassRepo;
	
	@Autowired
	CreditClassRepository creditClassRepo;
	
	@Autowired
	DebitClassRepository debitClassRepo;
	
	@Autowired
	LedgerClassRepository ledgerClassRepo;
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepo;
	
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;
	
	@Autowired
	ProductionMstRepository productionMstRepository;
	@Autowired
	ActualProductionHdrRepository actualProductionHdrRepository;
	@Override
	public TransactionCountModel getTransactionCount(String branchCode, Date sqlDate, String companymstid) {
       int  purchaseHdrCount=0;
		int purchaseDtlCount=0;
		int productionMstCount=0;
		int productionDtlCount=0;
		int actualProductionHdrCount=0;
		int actualProductionDtlCount=0;
		int itemBatchDtlCount = 0;
		int creditClassCount =0;
		int debitClassCount =0;
		int ledgerClassCount =0;
		int accountClassCount =0;
		int paymentCount =0;
		int receiptCount =0;
		TransactionCountModel TransactionCountModel=  new TransactionCountModel();
		
		paymentCount = paymentHdrRepo.countOfPaymentHdr(branchCode,sqlDate,companymstid);
		TransactionCountModel.setPaymentCount(paymentCount);
		
		receiptCount = receiptHdrRepo.countOfReceiptHdr(branchCode,sqlDate,companymstid);
		TransactionCountModel.setReceiptCount(receiptCount);
		
		ledgerClassCount = ledgerClassRepo.countOfLedgerClass(branchCode, sqlDate, companymstid);
		TransactionCountModel.setLedgerClassCount(ledgerClassCount);
		
		
		debitClassCount = debitClassRepo.countOfDebitClassByDate(branchCode, sqlDate, companymstid);
		TransactionCountModel.setDebitClassCount(debitClassCount);
		
		accountClassCount = accountClassRepo.countOfAccountClassByDate(branchCode, sqlDate, companymstid);
		TransactionCountModel.setAccountClassCount(accountClassCount);
		
		creditClassCount = creditClassRepo.countOfCreditClass(branchCode, sqlDate, companymstid);
		TransactionCountModel.setCreditClassCount(creditClassCount);
		
		
		itemBatchDtlCount = itemBatchDtlRepo.ItemBatchDtlCount(branchCode, sqlDate, companymstid);
		TransactionCountModel.setItemBatchDtlCount(itemBatchDtlCount);
		purchaseHdrCount= purchaseHdrRepository.countOfPurchaseHdr(branchCode, sqlDate, companymstid);
		TransactionCountModel.setPurchaseHdrCount(purchaseHdrCount);
		
		
		purchaseDtlCount=purchaseHdrRepository.countOfPurchaseDtl(branchCode, sqlDate, companymstid);
		TransactionCountModel.setPurchaseDtlCount(purchaseDtlCount);
		
		
		
		
		
		
		productionMstCount=productionMstRepository.productionMstCount(branchCode, sqlDate, companymstid);
		TransactionCountModel.setProductionMstCount(productionMstCount);
		
		
		productionDtlCount=productionMstRepository.productionDtlCount(branchCode, sqlDate, companymstid);
		TransactionCountModel.setProductionDtlCount(productionDtlCount);
		
		
		
		actualProductionHdrCount=actualProductionHdrRepository.
				actualproductionHdrCount(branchCode, sqlDate, companymstid);
		TransactionCountModel.setActualProductionHdrCount(actualProductionHdrCount);
			
		actualProductionDtlCount=actualProductionHdrRepository.
				actualproductionDtlCount(branchCode, sqlDate, companymstid);
		TransactionCountModel.setActualProductionDtlCount(actualProductionDtlCount);
		
		
		return TransactionCountModel;
	}

}
