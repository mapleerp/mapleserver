package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.report.entity.IntentInReport;

@Component
public interface IntentInReportService {

	
	List<IntentInReport> getIntentInReport(String voucherNumber,Date date);
	
	List<Object> getPendingIntent(Date fdate,Date tdate);

	List<IntentInReport> getItemWiseIntentSummary(String branchcode, String companymstid, Date fdate);
}
