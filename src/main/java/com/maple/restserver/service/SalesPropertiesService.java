package com.maple.restserver.service;

import java.util.List;

import com.maple.restserver.entity.SalesPropertyTransHdr;

public interface SalesPropertiesService {

	List<SalesPropertyTransHdr> findBySalesTransHdrId(String hdrid);

}
