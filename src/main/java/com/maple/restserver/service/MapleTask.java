package com.maple.restserver.service;

import java.util.HashMap;

public interface MapleTask {

	void execute(HashMap hashmap);
}
