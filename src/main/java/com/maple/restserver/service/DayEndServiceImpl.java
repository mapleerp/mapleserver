package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DailyCashFromOrdersDtl;
import com.maple.restserver.entity.DailySalesSummary;
import com.maple.restserver.report.entity.CompanyDailySaleSummary;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.DailyCashFromOrdersDtlRepository;
import com.maple.restserver.repository.DailySalesSummaryRepository;

 @Component
@Service
public class DayEndServiceImpl implements DayEndService{

	@Autowired
	DailySalesSummaryRepository  dailySalesSummaryRepo;
	
	@Autowired
	DailyCashFromOrdersDtlRepository  dailyCashFromOrdersDtlRepo;
	
	
	@Autowired
	private BranchMstRepository brnchMstRepo;
	@Override
	public String getOpeningBillNumber(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getClosingBillNumber(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTotalBillNumbers(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getOpeningPettyCash(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getCashReceived(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getExpensePaid(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getClosingBalance(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getCashSalesOrder(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getOnlineSalesOrder(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDebitCardSalesOrder(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getTotalOrders(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getB2BSalesTotal(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getUberSalesTotal(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getSwiggySalesTotal(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getZomatoSalesTotal(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getFoodPandaSalesTotal(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getCashSalesTotal(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getCreditSalesTotal(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getCreditCardSalesTotal(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getSodexoSalesTotal(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getPaytmSalesTotal(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getPreviousTotal(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getTotalSales(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getTotalSalesOrder(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getTotalAtDrawer(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getOpeningCashAtDrawer(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getCashVariance(String branchCode, Date reportDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DailySalesSummary getDailySalesSumary(String branchCode, Date reportDate) {
		DailySalesSummary dailySalesSummary = dailySalesSummaryRepo.findByRecordDateAndBranchCode(branchCode,reportDate);
		return dailySalesSummary;
	}

	@Override
	public List<DailyCashFromOrdersDtl> getDailyAdvanceFromOrders(String branchCode, Date reportDate) {
		 
		List<DailyCashFromOrdersDtl> dailySalesSummary = dailyCashFromOrdersDtlRepo.findAll();
		
		
		return dailySalesSummary;
	}
	
	

	@Override
	public 	List<DailySalesSummary> getDailySalesSumaryAll() {
		List<DailySalesSummary> dailySalesSummary = dailySalesSummaryRepo.findAll();
		return null;
	}

	@Override
	public List<CompanyDailySaleSummary> getCompanyDailySalesSumary(Date date,CompanyMst companyMst) {

		List<CompanyDailySaleSummary> reportList=new ArrayList<CompanyDailySaleSummary>();
		List<BranchMst> branchList=new ArrayList<BranchMst>();
		branchList=	brnchMstRepo.findByCompanyMst(companyMst);
		for(BranchMst branch :branchList) {
			CompanyDailySaleSummary report= new CompanyDailySaleSummary();
		/*	List<Object> obj =dailySalesSummaryRepo.getCompanyDailySalesSumary( date,companyMst,branch.getBranchCode());
			for(int i=0;i<obj.size();i++)
			 {
			Object[] objAray = (Object[]) obj.get(i);
			report.setBranchName((String) objAray[0]);
			report.setCard((Double) objAray[1]);
			report.setCard2((Double) objAray[2]);
			report.setTotal((Double) objAray[3]);
			reportList.add(report);
			 */
		//	}		
		}
		return reportList;
	}
	
	

}
