package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.LocalPurchaseSummaryReport;

@Component
public interface LocalPurchaseSummaryDtlReportService {

	List<LocalPurchaseSummaryReport> getLocalPurchaseSummaryDtlReport(Date fdate, Date tdate);

}
