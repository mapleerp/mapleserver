package com.maple.restserver.service;



import java.net.UnknownHostException;
import java.sql.SQLException;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
 

@Service

public interface AMDCDataTransferService {
  

//public String consumptionDtlTransferAmdc(CompanyMst companyMst, String branchCode) ; 
//public String countryMstTransferAmdc(CompanyMst companyMst, String branchCode);
//public String currencyConversionTransferambc(CompanyMst companyMst, String branchCode);
//public String physicalStockDeltaMstTransferambc(CompanyMst companyMst, String branchCode);
//public String physicalStockMstTransferambc(CompanyMst companyMst, String branchCode);



////public String customerMstTransferambc(CompanyMst companyMst, String branchCode);
//public String supplierMstTransferAmdc(CompanyMst companyMst, String branchCode);
//public String   unitMstTransferAmdc(CompanyMst companyMst, String branchCode) ;
// String  userMstTransferAmdc(CompanyMst companyMst, String branchCode);

//Sibi.....
//public String consumptionHdrTransferAmdc(CompanyMst companyMst, String branchCode);
public String branchMstTransferAmdc(CompanyMst companyMst, String branchCode)throws UnknownHostException, Exception ;
//public String intentDtlTransferAmdc(CompanyMst companyMst, String branchCode);
////public String intentHdrTransferAmdc(CompanyMst companyMst, String branchCode);
//public String intentInDtlTransferAmdc(CompanyMst companyMst, String branchCode);
//public String intentInHdrTransferAmdc(CompanyMst companyMst, String branchCode);
//public String itemUnitMstTransferAmdc(CompanyMst companyMst, String branchCode);
//public String itemPriceMstTransferAmdc(CompanyMst companyMst, String branchCode);
//public String itemMstTransferAmdc(CompanyMst companyMst, String branchCode);
//public String itemGroupDtl2TransferAmdc(CompanyMst companyMst, String branchCode);
//public String itemBatchMstTransferAmdc(CompanyMst companyMst, String branchCode);
//public String itemBatchDtl2TransferAmdc(CompanyMst companyMst, String branchCode);
//public String itemBatchDtlTransferAmdc(CompanyMst companyMst, String branchCode);
//public String itemBarcodeMstTransferAmdc(CompanyMst companyMst, String branchCode);
//public String invEnqTransferAmdc(CompanyMst companyMst, String branchCode);

//public String invoiceVoucherPrintMstTransferAmdc(CompanyMst companyMst, String branchCode);
//public String invoiceNumberSeqTransferAmdc(CompanyMst companyMst, String branchCode);
//public String insuranceMstTransferAmdc(CompanyMst companyMst, String branchCode);


public String accountClassTransferamdc(CompanyMst companyMst, String
		  branchCode)throws UnknownHostException, Exception;


public String accountHeadsTransferamdc(CompanyMst companyMst, String
branchCode) throws UnknownHostException, Exception;


 public String depoartMentTransferamdc(CompanyMst companyMst, String
		  branchCode) throws UnknownHostException, Exception;

 
 public String doctorMstTransferamdc(CompanyMst companyMst, String
		  branchCode) throws UnknownHostException, Exception;
 
 
 public String priceLevelMstTransferamdc(CompanyMst companyMst, String
 branchCode) throws UnknownHostException, Exception;
 
 
 public String doSalesTransHdrTransfer(CompanyMst companyMst, String branchcode)
			throws UnknownHostException, Exception;
 
	public String
	doPurchaseTransfer(CompanyMst companyMst, String branchcode) throws UnknownHostException,
	Exception;
	
	
	
	public String doItemBatchDtlTransfer(CompanyMst companyMst, String branchcode) 
			throws SQLException;


	public String doItemTransfer(CompanyMst companyMst, String branchCode) throws UnknownHostException,
	Exception;
	
	public String transferCustomer(CompanyMst companyMst, String branchCode) throws UnknownHostException,
	Exception;
	
	
	public String transferAccountHeads(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception;

	
	public String patientDtlTransferambc(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	public String invCategoryMstTransferAmdc(CompanyMst companyMst, String branchCode)throws UnknownHostException, Exception;

	public String doDataTransferUserMst(CompanyMst companyMst ) throws UnknownHostException, Exception; 

	
	public String doDataTransferUnitMst(CompanyMst companyMst, String branch) throws UnknownHostException, Exception ;
	public String consumptionDtlTransferamdc(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception  ;
	public String consumptionHdrTransferamdc(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

}
