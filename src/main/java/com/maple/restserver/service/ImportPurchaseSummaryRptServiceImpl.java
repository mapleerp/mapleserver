package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.ImportPurchaseSummaryReport;
import com.maple.restserver.repository.PurchaseHdrRepository;


@Service
@Transactional
public class ImportPurchaseSummaryRptServiceImpl implements ImportPurchaseSummaryRptService {
	
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;
	
@Override	
public List<ImportPurchaseSummaryReport> getImportpurchaseReport(Date fdate, Date tdate) {
		
		List<ImportPurchaseSummaryReport> importPurchaseSummaryList = new ArrayList<ImportPurchaseSummaryReport>();
		List<Object> obj = purchaseHdrRepository.getImportpurchaseReport(fdate,  tdate);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			ImportPurchaseSummaryReport importPurchaseSummaryReport = new ImportPurchaseSummaryReport();
			importPurchaseSummaryReport.setInvoiceDate((Date) objAray[0]);
			importPurchaseSummaryReport.setInvoiceNumber((String)objAray[1]);
			importPurchaseSummaryReport.setSupplierInvNo((String)objAray[3]);
			importPurchaseSummaryReport.setSupplierName((String)objAray[2]);
			importPurchaseSummaryReport.setPurchaseOrder((String)objAray[5]);
			importPurchaseSummaryReport.setItemTotal(((Double)objAray[6]));
			importPurchaseSummaryReport.setItemTotalFc(((Double)objAray[7]));
			importPurchaseSummaryReport.setExpense((Double)objAray[8]);
			importPurchaseSummaryReport.setFcExpense((Double)objAray[9]);
			importPurchaseSummaryReport.setNotIncludedExp((Double)objAray[11]);
			importPurchaseSummaryReport.setImportDuty(((Double)objAray[12]));
			importPurchaseSummaryReport.setGrandTotal((Double)objAray[13]);
			importPurchaseSummaryReport.setIncludedExp((Double)objAray[10]);
			
			
			
		
			importPurchaseSummaryList.add(importPurchaseSummaryReport);
		 }

		return importPurchaseSummaryList;

}
}



