package com.maple.restserver.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


import org.springframework.stereotype.Component;

@Service
@Transactional
public interface SubscriptionValidityCheck   {

	boolean validateSubscription();
	boolean updateSubscription(String userId, String password, String companyMstId, String branchcode) throws Exception;
	String  getKey(String supscriptionDate) throws Exception;
	boolean  validateDate(String supscriptionDate, String keyString) throws Exception;
	
}
