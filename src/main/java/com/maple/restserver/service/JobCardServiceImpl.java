package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ActiveJobCardReport;
import com.maple.restserver.repository.JobCardHdrRepository;

 
@Service
@Transactional
public class JobCardServiceImpl implements JobCardService{
	
	@Autowired
	JobCardHdrRepository jobCardHdrRepo;

	@Override
	public List<ActiveJobCardReport> findAllActiveJobCardReport(CompanyMst companyMst, Date sdate, Date edate) {

		List<ActiveJobCardReport> activeJpbCardList = new ArrayList<>();
		List<Object> objList = jobCardHdrRepo.findAllActiveJobCardReport(companyMst,sdate,edate);
		
		for(int i=0; i<objList.size(); i++)
		{
			Object[] obj = (Object[]) objList.get(i);
			ActiveJobCardReport activeJobCardReport = new ActiveJobCardReport();
			activeJobCardReport.setComplaints((String) obj[2]);
			activeJobCardReport.setCustomer((String) obj[0]);
			activeJobCardReport.setItemName((String) obj[1]);
			activeJobCardReport.setJobCardHdrId((String) obj[6]);
			activeJobCardReport.setObservation((String) obj[3]);
			activeJobCardReport.setServiceInNo((String) obj[4]);
			activeJobCardReport.setVoucherDate((Date) obj[5]);
			activeJpbCardList.add(activeJobCardReport);
		}
		return activeJpbCardList;
	}

}
