package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.HsnWiseReport;

public interface HsnWiseReportService {

	
	List<HsnWiseReport>getHsnWiseReport(Date sdate,Date edate,CompanyMst companyMst,String branchCode);
}
