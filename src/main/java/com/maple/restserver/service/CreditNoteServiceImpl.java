package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CreditNote;
import com.maple.restserver.repository.CreditNoteRepository;

public class CreditNoteServiceImpl implements CreditNoteService {@Autowired
	CreditNoteRepository creditNoteRepository;
	
	
	
	@Override
	public List<CreditNote> getCreditNoteItems(CompanyMst companyMst) {
		List<CreditNote> creditNoteList = new ArrayList<CreditNote>();
		List<Object> obj = creditNoteRepository.findAllCreditNote(companyMst);
		 System.out.print(obj.size()+"object size isssssssssssssssssssssssss");
		for(int i = 0;i<obj.size();i++)
			
			
		{
			 Object[] objAray = (Object[]) obj.get(i);

		
			 CreditNote creditNote=new CreditNote();
			 creditNote.setUserId((String) objAray[0]);
			
			 creditNote.setDebitAccount((String) objAray[1]);
			 creditNote.setCreditAccount((String) objAray[2]);
			 creditNote.setAmount((Double) objAray[3]);
			 creditNote.setVoucherNumber((String) objAray[4]);
			 creditNote.setRemark((String) objAray[5]);
			 creditNote.setVoucherDate((java.util.Date) objAray[6]);
			 creditNote.setBranch((String) objAray[7]);
			 creditNote.setId((String) objAray[8]);
			
			 creditNoteList.add(creditNote);
		
		}
		return creditNoteList;
	}
}
