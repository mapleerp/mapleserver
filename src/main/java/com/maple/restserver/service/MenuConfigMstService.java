package com.maple.restserver.service;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MenuConfigMst;

@Service
public interface MenuConfigMstService {

	
	String applicationDomain(CompanyMst companyMst,String applicationDomainType);
}
