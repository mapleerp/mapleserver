package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ServiceItemMst;
import com.maple.restserver.report.entity.ServiceInReport;
import com.maple.restserver.report.entity.StockValueReports;

public interface ServiceInService {

	List<ServiceInReport> findByCompanyMstAndBetweenDate(CompanyMst companyMst, Date fromDate, Date toDate);

	List<StockValueReports> findByServiceOrItemProfit(CompanyMst companyMst, Date sdate, Date edate, String type);

}
