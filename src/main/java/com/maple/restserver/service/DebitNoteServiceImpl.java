package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DebitNote;
import com.maple.restserver.repository.DebitNoteRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
public class DebitNoteServiceImpl implements DebitNoteService  { 

	@Autowired
	DebitNoteRepository debitNoteRepository;
	
	
	
	@Override
	public List<DebitNote> getDebitNoteItems(CompanyMst companyMst) {
		List<DebitNote> debitNoteList = new ArrayList<DebitNote>();
		List<Object> obj = debitNoteRepository.findAllDebitNote(companyMst);
		 System.out.print(obj.size()+"object size isssssssssssssssssssssssss");
		for(int i = 0;i<obj.size();i++)
			
			
		{
			 Object[] objAray = (Object[]) obj.get(i);

		
			DebitNote debitNote=new DebitNote();
			debitNote.setUserId((String) objAray[0]);
			
			debitNote.setDebitAccount((String) objAray[1]);
			debitNote.setCreditAccount((String) objAray[2]);
			debitNote.setAmount((Double) objAray[3]);
			debitNote.setVoucherNumber((String) objAray[4]);
			debitNote.setRemark((String) objAray[5]);
		//	String date =SystemSetting.UtilDateToString((java.util.Date) objAray[5]);
			debitNote.setVoucherDate((java.util.Date) objAray[6]);
			debitNote.setBranch((String) objAray[7]);
			debitNote.setId((String) objAray[8]);
			
			debitNoteList.add(debitNote);
		
		}
		return debitNoteList;
	}
}
