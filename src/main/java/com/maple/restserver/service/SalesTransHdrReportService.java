package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.report.entity.B2bSaleReport;
import com.maple.restserver.report.entity.BranchWiseProfitReport;
import com.maple.restserver.report.entity.DailySalesReportDtl;
import com.maple.restserver.report.entity.DailySalesReturnReportDtl;
import com.maple.restserver.report.entity.HsnWiseSalesDtlReport;
import com.maple.restserver.report.entity.ItemWiseDtlReport;
import com.maple.restserver.report.entity.PharmacyGroupWiseSalesReport;
import com.maple.restserver.report.entity.PharmacySalesDetailReport;
import com.maple.restserver.report.entity.ReceiptModeWiseReort;
import com.maple.restserver.report.entity.SaleOrderReceiptReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.SalesTaxSplitReport;
import com.maple.restserver.report.entity.SettledAmountDtlReport;

@Component
public interface SalesTransHdrReportService {

	List<DailySalesReportDtl> getDailySalesReportdtl(String branchcode, Date date, Date tdate, String companymstid);

	List<DailySalesReportDtl> getDailySalesReportdtl(String branchcode, Date date, String companymstid);

	List<DailySalesReturnReportDtl> getDailySalesReturnReportdtl(String branchcode, Date date, Date tdate,
			String companymstid);

	List<ItemWiseDtlReport> getDailyItemWiseSalesReportdtl(String branchcode, String date, String companymstid);

	List<ItemWiseDtlReport> getDailyItemWiseMonthlySalesReportdtl(String branchcode, String fromDate, String toDate,
			String companymstid);

	List<ItemWiseDtlReport> getItemWiseSalesReprtBetweenDates(String branchcode, String reportdate, String todate,
			String companymstid, String itemid);

	List<ItemWiseDtlReport> getCategoryWiseSalesReprtBetweenDates(String branchcode, Date reportdate, Date todate,
			String companymstid, String categoryid);

	List<B2bSaleReport> getB2BSalesReport(Date date, Date tDate, String branchCode, CompanyMst companymst);

	List<SalesTransHdr> findByVoucherDate(Date date);

	List<DailySalesReportDtl> getCustomerDailySalesReportdtl(String branchcode, Date fromdate, Date todate,
			String companymstid, String customerid);

	List<BranchWiseProfitReport> getBranchWiseProfit(Date sdate, Date edate, CompanyMst companyMst);

	List<BranchWiseProfitReport> getProfitByBranch(Date sdate, Date edate, CompanyMst companyMst, String branchCode);

	List<SalesInvoiceReport> getSalesAnalysisReport(Date sdate, Date edate, String companymstid,String branchCode);

	// version 3.1
	List<SalesInvoiceReport> getSalesDtlReport(Date sdate, Date edate, CompanyMst companyMst, String branchCode);

	List<ReceiptModeWiseReort> getReceiptModeWiseReort(String branchcode, Date fromdate, String companymstid);

	List<ReceiptModeWiseReort> getReceiptModeCashWiseReort(String branchcode, Date fromdate, String companymstid);

//version 3.1 end
	Double getSaleOrderSettledAmount(String branchcode, Date fromdate, String companymstid);

	Double getSaleOrderSettledCashAmount(String branchcode, Date fromdate, String companymstid);

	Double getPreviousAdvanceAmount(String branchcode, Date fromdate, String companymstid, String receiptmode);

	List<SettledAmountDtlReport> getSettledAmountDtlReport(String branchcode, Date fromdate, String companymstid);

	List<SettledAmountDtlReport> getSettledAmountDtlCashWiseReport(String branchcode, Date fromdate,
			String companymstid);

	Double getPreviousAdvanceAmountCard(String branchcode, Date fromdate, String companymstid, String receiptmode);
	
	Double getPreviousAdvanceAmountCardRealized(String branchcode, Date fromdate, String companymstid, String receiptmode);
	
	

	List<SaleOrderReceiptReport> getPreviousAdvanceAmountDtl(String branchcode, Date fromdate, String companymstid,
			String receiptmode);

//----------------versio 4.11
	List<SalesTaxSplitReport> SaleTaxSplit(Date edate, CompanyMst companyMst, String branchCode);
//----------------versio 4.11 end

	List<PharmacyGroupWiseSalesReport> getPharmacyGroupWiseSalesReport(CompanyMst companyMst, Date fdate, Date tdate,
			String branchCode, String[] array);

	List<PharmacySalesDetailReport> getPharmacyGroupWiseSalesDtlReport(CompanyMst companyMst, Date fdate, Date tdate,
			String branchCode, String[] array);

	List<DailySalesReportDtl> getSalesDtlDailyReport(String branchcode, Date fromdate, Date todate,
			String companymstid);

	List<ReceiptModeWiseReort> getReceiptModeWisSummaryeReort(String branchcode, Date fromdate, String companymstid);

	List<DailySalesReportDtl> getUserWiseSummarySalesReportDtl(String branchcode, String userid, Date sdate, Date edate,
			String companymstid);

	List<DailySalesReportDtl> getUserWiseSalesReport(String branchcode, String userid, Date sdate, Date edate,
			String companymstid);

	List<BranchWiseProfitReport> getCustomerWiseProfit(Date sdate, Date edate, CompanyMst companyMst,
			String branchCode);

	List<HsnWiseSalesDtlReport> getHsnWiseSalesDtlReportWithoutGroup(Date fromDate, Date toDate, String branchcode,
			CompanyMst companyMst);

	List<HsnWiseSalesDtlReport> getHsnWiseSalesReportWithGoup(Date fromDate, Date toDate, String[] array,
			String branchcode, CompanyMst companyMst);

	List<B2bSaleReport> getB2BSalesReportDtl(Date date, Date tdate, String branchCode, CompanyMst companyMst);

	List<B2bSaleReport> getNewB2CSalesReport(Date date, Date tdate, String branchCode, CompanyMst companyMst);

	List<SalesInvoiceReport> getWholesSalesInvoiceReport(Date date, Date tdate, String branchCode,
			CompanyMst companyMst);

}
