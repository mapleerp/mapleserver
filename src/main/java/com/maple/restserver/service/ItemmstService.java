package com.maple.restserver.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.report.entity.ItemCorrectionReport;
import com.maple.restserver.report.entity.ItemExport;

@Service
@Transactional

public interface ItemmstService {
	public  List<ItemMst> getItems(String pdfFileName);
	
	public ItemMst saveItemMst(ItemMst item);
	
	public  List<ItemMst>findAll();
	
 
	public   List<ItemMst>  findByBarCode(String barcode);
 
	public  ItemMst findByItemName(String itemname);
	public  Optional<ItemMst> findById(String id);
	 

	//List<ItemMst> FindByComapnyMstAndCategoryMst(String companyid, String categoryid);

	List<ItemMst> FindByComapnyMstAndCategoryMst(CompanyMst companyid, String categoryid);

	void updateItemBarcode(ItemMst itemmst, String barCode);
	
	public  List<ItemExport>   exportItem();

	public List<ItemCorrectionReport> getItemListWithCountofSaleStockPurchase(String companyid);

	public String publishItemIdAndPriceToServer(String itemid, Double standardPrice, String companymstid, String destination);
	
}
