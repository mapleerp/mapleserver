package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.report.entity.PharmacyItemWiseSalesReport;
import com.maple.restserver.report.entity.PurchaseHdrReport;
import com.maple.restserver.report.entity.PurchaseReport;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.utils.SystemSetting;

 
@Service
@Transactional
public class PurchaseDtlServiceImpl implements PurchaseDtlService{

	@Autowired
	PurchaseDtlRepository purchaseDtlRepository;
	
	
	@Override
	public List<PurchaseDtl> updatePurchaseDtlDisplaySerial(String purchaseHdr) {

		List<PurchaseDtl> purchaseDtlList = purchaseDtlRepository
				.findByPurchaseHdrIdOrderBySerial(purchaseHdr);
		
		
		int DisplaySrlNo=0;
		for (PurchaseDtl purchaseDtl : purchaseDtlList) {
			DisplaySrlNo++;
			purchaseDtl.setDisplaySerial(DisplaySrlNo);
			purchaseDtlRepository.save(purchaseDtl);
			
			
		}

		return purchaseDtlList;
	}


	@Override
	public List<PurchaseReport> getPurchaseConsolidatedReport(Date fromDate, Date toDate, String branch) {
		 List<PurchaseReport> purchaseReportList=new ArrayList<PurchaseReport>();
	
			List<Object> obj = purchaseDtlRepository.getPurchaseConsolidatedReport(fromDate, toDate,branch);
			for (int i = 0; i < obj.size(); i++) {
				Object[] objAray = (Object[]) obj.get(i);
				PurchaseReport purchaseReport = new PurchaseReport();
            
				purchaseReport.setVoucherDate((Date) objAray[0]);
				purchaseReport.setItemName((String) objAray[1]);
//				purchaseReport.setBranchCode((String) objAray[2]);
				purchaseReport.setQty((Double) objAray[3]);
				purchaseReport.setSupplierName((String) objAray[4]);
				purchaseReport.setAmountTotal((Double) objAray[5]);
				purchaseReport.setBatch((String) objAray[6]);
				
				purchaseReportList.add(purchaseReport);
             
			}
			
		
		return purchaseReportList;
	}

}
