package com.maple.restserver.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.maple.maple.util.MapleConstants;
import com.maple.maple.util.TaxManager;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BatchPriceDefinition;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KotTableDtl;
import com.maple.restserver.entity.KotTableHdr;
import com.maple.restserver.entity.ParamValueConfig;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.TableOccupiedMst;
import com.maple.restserver.entity.TaxMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.BatchPriceDefinitionRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KotTableDtlRepository;
import com.maple.restserver.repository.KotTableHdrRepository;
import com.maple.restserver.repository.ParamValueConfigRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.TableOccupiedMstRepository;
import com.maple.restserver.repository.TaxMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.SystemSetting;
@Service
@Transactional
public class KotServiceImpl implements KotService{@Autowired
	KotTableHdrRepository kotTableHdrRepository;
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	@Autowired
	VoucherNumberRepository voucherNumberRepository;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Autowired
	TableOccupiedMstRepository tableOccupiedMstRepository;
	@Autowired
	VoucherNumberService voucherService;
	@Autowired
	ItemMstRepository itemMstRepository;
	@Autowired
	KotTableDtlRepository kotTableDtlRepository;
	@Autowired
	ParamValueConfigRepository paramValueConfigRepository;
	@Autowired
	TaxMstRepository taxMstRepository;
	@Autowired
	SalesDetailsRepository salesDetailsRepository;
	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepository;
	@Autowired
	BatchPriceDefinitionRepository batchPriceDefinitionRepository;
	@Autowired
	PriceDefinitionRepository priceDefinitionRepository;
	
	@Value("${mybranch}")
	private String mybranch;
	
	@Override
	public KotTableHdr saveKotPosHdr(KotTableHdr kotTableHdr) {
		
		KotTableHdr kotHdr = new KotTableHdr();
		kotHdr = kotTableHdrRepository.save(kotTableHdr);
	
		return kotHdr;
	}

	

	@Override
	public KotTableDtl saveKotPosDtl(KotTableDtl kotTableDtlList) {
		KotTableDtl kotTableDtl = new KotTableDtl();
		if(null != kotTableDtlList) {
	
				kotTableDtl = kotTableDtlRepository.save(kotTableDtlList);
				
		}
		return kotTableDtl;
	}
	
	@Override
	public KotTableHdr printKOT(KotTableHdr kotTableHdr) {
		
		Date date = new Date();
		
		String financialYear = SystemSetting.getFinancialYear();
//		String pNo = RestCaller.getVoucherNumber(financialYear + "PVN");
//
//		pNo = SystemSetting.systemBranch + pNo;
//		purchasehdr.setVoucherNumber(pNo);
		
		
		String kotDate = SystemSetting.UtilDateToString(date);
		
		kotDate=kotDate+"-";
		 // id =id+companymstid;
		  
	    final VoucherNumber voucherNo = voucherService.generateInvoice(kotDate,kotTableHdr.getCompanyMst().getId());

	     String vcNo = voucherNo.getCode();
	    
		
	//	VoucherNumber voucher = voucherService.generateInvoice(financialYear + "KOT", "COM");
		List<KotTableDtl> kotDtlList = kotTableDtlRepository.findByKotTableHdr(kotTableHdr);
		
		
		if(kotDtlList.size()>0) {
			
			for(int i=0;i<kotDtlList.size();i++) {
				
				kotDtlList.get(i).setKotNumber(vcNo);
				  kotTableDtlRepository.save(kotDtlList.get(i));
			}
		}
		return kotTableHdr;
	}
	
	@Override
	public KotTableHdr finalSaveKot(KotTableHdr kottableHdrs) {
		SalesTransHdr salesTransHdr = createSalesTransHdr(kottableHdrs);
		List<KotTableDtl> kotDtlList = kotTableDtlRepository.findByKotTableHdr(kottableHdrs);
		if(kotDtlList.size()>0) {
			
			for(int i=0;i<kotDtlList.size();i++) {
				saveSalesDtl(kotDtlList.get(i),salesTransHdr);
			}
		}
		kottableHdrs = updateKot(kottableHdrs);
		return kottableHdrs;
	}

	
	
	private KotTableHdr updateKot(KotTableHdr kottableHdrs) {
		kottableHdrs.setBilledStatus("YES");
		kottableHdrs.setTableStatus("COMPLETED");
		kottableHdrs = kotTableHdrRepository.save(kottableHdrs);
		return kottableHdrs;
	}



	@Override
	public List<KotTableDtl> findByKotHdr(KotTableHdr kotTableHdr) {
		List<Object> kotdtl=kotTableDtlRepository.findByKotHdr(kotTableHdr);
		List<KotTableDtl> kotTableDtlList = new ArrayList<KotTableDtl>();
		for (int i = 0; i < kotdtl.size(); i++) {
			Object[] objAray = (Object[]) kotdtl.get(i);
			KotTableDtl kotTableDtl = new KotTableDtl();
			
			kotTableDtl.setItemId((String) objAray[2]);
			kotTableDtl.setQty((Double) objAray[0]);
			kotTableDtl.setRate((Double) objAray[1]);
			kotTableDtl.setId((String) objAray[3]);
			kotTableDtl.setKotNumber((String) objAray[4]);
		Optional<KotTableHdr> kothdr = kotTableHdrRepository.findById((String) objAray[5]);
			kotTableDtl.setKotTableHdr(kothdr.get());
			
			
			kotTableDtlList.add(kotTableDtl);
			

		}
				
		
		
		return kotTableDtlList;
	}
	
	
	private SalesTransHdr createSalesTransHdr(KotTableHdr kotHdr) {
		SalesTransHdr salesTransHdr = new SalesTransHdr();

		
		Optional<AccountHeads> accountHeadsResponse = accountHeadsRepository.findByAccountNameAndBranchCodeAndCompanyMstId(
				"KOT","%" + kotHdr.getCompanyMst().getCompanyName() + mybranch + "%" ,kotHdr.getCompanyMst().getId());
				AccountHeads accountHeads = accountHeadsResponse.get();

		if (null == accountHeads) {
			 accountHeads = accountHeadsRepository.findByAccountName("KOT");
			
		}

		salesTransHdr.setAccountHeads(accountHeads);
//		salesTransHdr.setUserId(SystemSetting.getUserId());
		salesTransHdr.setSalesMode("KOT");
		salesTransHdr.setInvoiceAmount(0.0);
		salesTransHdr.setBranchCode(mybranch);
		Date date = new Date();
		salesTransHdr.setVoucherDate(date);
		salesTransHdr.setCustomerId(accountHeads.getId());
		salesTransHdr.setCreditOrCash("CASH");
		salesTransHdr.setIsBranchSales("N");
		salesTransHdr.setSalesManId(kotHdr.getWaiterName());

		salesTransHdr.setKotNumber(kotHdr.getId());

		salesTransHdr.setServingTableName(kotHdr.getTableName());
		salesTransHdr.setCustomiseSalesMode("NONE");
		salesTransHdr.setCompanyMst(kotHdr.getCompanyMst());
		 salesTransHdr = salesTransHdrRepository.save(salesTransHdr);
//		saveTableOccupiedMst(kotHdr.getTableName(),salesTransHdr);
		return salesTransHdr;
		
	}
	
	private void saveSalesDtl(KotTableDtl kotTableDtl, SalesTransHdr salesTransHdr) {
		SalesDtl salesDtl = new SalesDtl();
		salesDtl.setSalesTransHdr(salesTransHdr);
		Optional<ItemMst> item = itemMstRepository.findById(kotTableDtl.getItemId());
		salesDtl.setItemName(item.get().getItemName());
//		salesDtl.setBarcode(txtBarcode.getText().length() == 0 ? "" : txtBarcode.getText());

//		String batch = txtBatch.getText().length() == 0 ? "NOBATCH" : txtBatch.getText();
		salesDtl.setBatch(MapleConstants.Nobatch);


//		if (!txtDescription.getText().trim().isEmpty()) {
//			salesDtl.setKotDescription(txtDescription.getText());
//		}


		salesDtl.setQty(kotTableDtl.getQty());

		Double mrpRateIncludingTax = kotTableDtl.getRate();
		
		salesDtl.setDiscount(0.0);
		salesDtl.setIgstAmount(0.0);
		salesDtl.setIgstTaxRate(0.0);
		salesDtl.setUnitId(item.get().getUnitId());
		salesDtl.setMrp(mrpRateIncludingTax);
		Double taxRate = 00.0;
		
		salesDtl.setBarcode(item.get().getBarCode());
		salesDtl.setStandardPrice(item.get().getStandardPrice());

		if (null != item.get().getTaxRate()) {
			salesDtl.setTaxRate(item.get().getTaxRate());
			taxRate = item.get().getTaxRate();

		} else {
			salesDtl.setTaxRate(0.0);

		}
		/*
		 * Calculate Rate Before Tax
		 */

		salesDtl.setItemId(item.get().getId());
//		if (null == cmbUnit.getValue()) {
//			ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit);
//			salesDtl.setUnitId(getUnit.getBody().getId());
//			salesDtl.setUnitName(getUnit.getBody().getUnitName());
//		} else {
//			ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit);
//			salesDtl.setUnitId(getUnit.getBody().getId());
//			salesDtl.setUnitName(getUnit.getBody().getUnitName());
//		}

		List<TaxMst> getTaxMst = taxMstRepository.getTaxByItemId(kotTableDtl.getItemId());
		
		
		if (getTaxMst.size() > 0) {

			salesDtl = calculationWithTaxMst(getTaxMst, salesDtl, mrpRateIncludingTax);

		} else {

			salesDtl = calculationWithOutTaxMst(salesDtl, mrpRateIncludingTax, item.get(), taxRate);

		}

		salesDtl.setAmount(kotTableDtl.getQty() * mrpRateIncludingTax);

		salesDtl = setCostPrice(salesDtl);

		
			salesDtl.setStore(MapleConstants.Store);
		
			salesDtl.setCompanyMst(kotTableDtl.getCompanyMst());
			salesDetailsRepository.save(salesDtl);

	}

	private SalesDtl setCostPrice(SalesDtl salesDtl2) {

		PriceDefenitionMst priceDefenitionMst = priceDefinitionMstRepository
				.findByCompanyMstAndPriceLevelName(salesDtl2.getCompanyMst(), "COST PRICE");
		if (null != priceDefenitionMst) {
			Date sdate = new Date(); 

			List<BatchPriceDefinition> batchpriceDef = batchPriceDefinitionRepository.findByItemIdAndUnitId(
					salesDtl2.getItemId(), priceDefenitionMst.getId(), salesDtl2.getUnitId(), salesDtl2.getBatch(), sdate);
			if (null != batchpriceDef) {
				salesDtl2.setCostPrice(batchpriceDef.get(0).getAmount());
			} else {
				PriceDefinition priceDefenition = priceDefinitionRepository.findByItemIdAndStartDateCostprice(
						salesDtl2.getItemId(), priceDefenitionMst.getId(),sdate, salesDtl2.getUnitId());

				if (null != priceDefenition) {
					salesDtl2.setCostPrice(priceDefenition.getAmount());
				}
			}
		}

		return salesDtl2;

		
	}

//	private void saveTableOccupiedMst(String servingTable, SalesTransHdr salesTransHdr) {
//		TableOccupiedMst tableOccupiedMst = new TableOccupiedMst();
//		List<TableOccupiedMst> tableList = tableOccupiedMstRepository.findByTableId(servingTable);
//		int count = tableList.size();
//		if(count > 0) {
//			tableOccupiedMst = tableList.get(count-1);
//		}
//		if (null != tableOccupiedMst) {
////			tableOccupiedMst.setWaiterId(salesman);
//			java.util.Date date = new java.util.Date();
//			long timeoccupied = date.getTime();
//			Timestamp ts = new Timestamp(timeoccupied);
//			tableOccupiedMst.setOrderedTime(ts);
//			tableOccupiedMst.setSalesTransHdr(salesTransHdr);
//			updateTableOccupiedMst(tableOccupiedMst);
//			
//		} else if (null == tableOccupiedMst) {
//			tableOccupiedMst.setTableId(servingTable);
//			java.util.Date date = new java.util.Date();
//			long timeoccupied = date.getTime();
//			Timestamp ts = new Timestamp(timeoccupied);
//			tableOccupiedMst.setOccupiedTime(ts);
////			tableOccupiedMst.setWaiterId(salesman);
//			tableOccupiedMst.setSalesTransHdr(salesTransHdr);
//			tableOccupiedMst.setOrderedTime(ts);
//			tableOccupiedMstRepository.save(tableOccupiedMst);
//			
//		
//		}
//	}

//	private void updateTableOccupiedMst(TableOccupiedMst tableOccupiedMst) {
//		TableOccupiedMst tableOccupied = tableOccupiedMstRepository.findById(tableOccupiedMst.getId()).get();
//		tableOccupied.setWaiterId(tableOccupiedMst.getWaiterId());
//		tableOccupied.setOrderedTime(tableOccupiedMst.getOrderedTime());
//		tableOccupied.setSalesTransHdr(tableOccupiedMst.getSalesTransHdr());
//		tableOccupied.setBillingTime(tableOccupiedMst.getBillingTime());
//		tableOccupied.setInvoiceAmount(tableOccupiedMst.getInvoiceAmount());
//		
//		 tableOccupiedMstRepository.save(tableOccupied);
//		
//	}

	
	private SalesDtl calculationWithTaxMst(List<TaxMst> getTaxMst, SalesDtl salesDtl,
			Double mrpRateIncludingTax) {

		for (TaxMst taxMst : getTaxMst) {
			if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
				salesDtl.setIgstTaxRate(taxMst.getTaxRate());
				salesDtl.setTaxRate(taxMst.getTaxRate());
			}
			if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
				salesDtl.setCgstTaxRate(taxMst.getTaxRate());
			}
			if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
				salesDtl.setSgstTaxRate(taxMst.getTaxRate());
			}
			if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
				salesDtl.setCessRate(taxMst.getTaxRate());
			}
			if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
				salesDtl.setAddCessRate(taxMst.getTaxRate());
			}

		}
		Double rateBeforeTax = (100 * mrpRateIncludingTax)
				/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate());
		salesDtl.setRate(rateBeforeTax);
		BigDecimal igstAmount = taxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
		salesDtl.setIgstAmount(igstAmount.doubleValue());
		BigDecimal CgstAmount = taxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
		salesDtl.setCgstAmount(CgstAmount.doubleValue());
		BigDecimal SgstAmount = taxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
		salesDtl.setSgstAmount(SgstAmount.doubleValue());
		BigDecimal cessAmount = taxCalculator(salesDtl.getCessRate(), rateBeforeTax);
		salesDtl.setCessAmount(cessAmount.doubleValue());
		BigDecimal adcessAmount = taxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
		salesDtl.setAddCessAmount(adcessAmount.doubleValue());

		return salesDtl;

	}
	
	
	private SalesDtl calculationWithOutTaxMst(SalesDtl salesDtl, Double mrpRateIncludingTax, ItemMst item,
			Double taxRate) {

		if (null != item.getTaxRate()) {
			salesDtl.setTaxRate(item.getTaxRate());
			taxRate = item.getTaxRate();
		} else {
			salesDtl.setTaxRate(0.0);
		}
		Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
		salesDtl.setRate(rateBeforeTax);

		double cessRate = item.getCess();
		double cessAmount = 0.0;
		if (cessRate > 0) {

			rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());
			salesDtl.setRate(rateBeforeTax);

			cessAmount = salesDtl.getQty() * salesDtl.getRate() * cessRate / 100;

		}

		salesDtl.setCessRate(cessRate);

		salesDtl.setCessAmount(cessAmount);

		salesDtl.setSgstTaxRate(taxRate / 2);

		salesDtl.setCgstTaxRate(taxRate / 2);

		salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		salesDtl.setAddCessRate(0.0);
		
		return salesDtl;
	}
	
	
	public BigDecimal taxCalculator(Double taxpercent, Double mrp) {
		
		String taxId = "ASV";
		HashMap hm = new HashMap();
		TaxManager tm = new TaxManager();
		hm.put("TAXPERCENT", taxpercent);
		hm.put("MRP", mrp);

		return tm.getTax(taxId, hm);
		
	}



	

}