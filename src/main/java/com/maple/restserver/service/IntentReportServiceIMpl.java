package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.IntentVoucherReport;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.repository.IntentHdrRepository;
import com.maple.restserver.repository.IntentInHdrRepository;

@Service
@Transactional
@Component
public class IntentReportServiceIMpl implements IntentReportService {

	@Autowired
	IntentHdrRepository intentHdrRepo;
	
	
	@Override
	public List<IntentVoucherReport> getIntentVoucherReport(String voucherNumber, Date date, String companymstid,String branchcode ) {
		List<IntentVoucherReport> intentReportList =   new ArrayList();
		 
		 
		 List<Object> obj = intentHdrRepo.getIntentVoucherReport( voucherNumber, date, companymstid,branchcode );
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 IntentVoucherReport intentReport=new IntentVoucherReport();
			
			 intentReport.setCompanyName((String) objAray[0]);
			 intentReport.setCompanyaddress1((String) objAray[1]);	
			 intentReport.setCompanyaddress2((String) objAray[2]);
			 intentReport.setVoucherNumber((String) objAray[3]);	
			 intentReport.setVoucherDate((Date) objAray[4]);
			 intentReport.setItemId((String) objAray[5]);
			 intentReport.setUnitName((String) objAray[6]);
			 intentReport.setQty((Double) objAray[7]);   
			 intentReport.setIntentDate((Date) objAray[8]);   
			 intentReport.setToBranch((String) objAray[9]);

			 intentReportList.add(intentReport);
		
		 }
		 
		
		 
		return intentReportList;
		 
	}

}
