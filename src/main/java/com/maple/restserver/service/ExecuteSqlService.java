package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.report.entity.ReorderReport;



@Service

@Component
public interface ExecuteSqlService {
	void insertSqlCommand(String id, String  Sql);
	void executeSqlCommand();
	List<Map<String,Object>> executeSql(String param, BranchMst branch);

}
