package com.maple.restserver.service;

import com.maple.restserver.entity.KotCategoryMst;

public interface CategoryMstService {

	KotCategoryMst getPrinterByCategory(String categoryId);
	
}
