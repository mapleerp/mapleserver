package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProcessMst;
import com.maple.restserver.entity.ProcessPermissionMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.report.entity.ProcessPermissionWebEntity;
import com.maple.restserver.repository.ProcessMstRepository;
import com.maple.restserver.repository.ProcessPermissionRepository;
import com.maple.restserver.repository.UserMstRepository;

 
@Service
@Transactional
public class ProcessPermissionServiceImpl implements ProcessPermissionService{
	
	@Autowired
	UserMstRepository userMstRepository;
	
	@Autowired
	ProcessMstRepository processMstRepository;

	@Autowired
	ProcessPermissionRepository processpermissionRepo;

	@Override
	public List<ProcessPermissionWebEntity> findByCompanyMst(CompanyMst companyMst) {
		
		List<ProcessPermissionWebEntity> processPermissionList = new ArrayList<>();
				
				
		List<ProcessPermissionMst> processList = processpermissionRepo.findByCompanyMst(companyMst);
		
		for(ProcessPermissionMst process : processList) {
			Optional<UserMst> userMstOpt = userMstRepository.findById(process.getUserId());
			if(!userMstOpt.isPresent())
			{
				return null;
			}
			UserMst userMst = userMstOpt.get();

			
			Optional<ProcessMst> processMstOpt = processMstRepository.findById(process.getProcessId());
			if(processMstOpt.isPresent()) 
			{
				return null;
			}
			ProcessMst processMst = processMstOpt.get();

			ProcessPermissionWebEntity processPermissionWebEntity = new ProcessPermissionWebEntity();
			processPermissionWebEntity.setUserName(userMst.getUserName());
			processPermissionWebEntity.setProcessName(processMst.getProcessName());
			
			processPermissionList.add(processPermissionWebEntity);
		}
		
		return processPermissionList;
	}

}
