package com.maple.restserver.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public interface ItemMergeService {
	
void mergeitem(String fromItemId,String toItemId,String fromItemBarCode,String toItemName);

}
