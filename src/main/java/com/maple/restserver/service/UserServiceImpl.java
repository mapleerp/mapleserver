package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.repository.UserMstRepository;

@Service
@Transactional
@Component
public class UserServiceImpl  implements UserService{
@Autowired
UserMstRepository userMstRepo;


@Autowired
SubscriptionValidityCheck subscriptionValidityCheck;


	@Override
	public ArrayList<String> getUserRoles(String userid, String comapnymstid) {
		
		ArrayList<String> userRoles =userMstRepo.getUserRoles(userid,comapnymstid);
		
	
//	 if(!subscriptionValidityCheck.validateSubscription()) {
//		 //userRoles.remove("POS SALES");
//		 //userRoles.remove("WHOLESALES");
//		 userRoles.remove("RETAIL");
//		 //userRoles.remove("KOT POS");
//		
//	 }
	
	 		 
		return userMstRepo.getUserRoles(userid,comapnymstid);
	}

}
