package com.maple.restserver.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.TallyRetryMst;
import com.maple.restserver.repository.TallyRetryMstRepository;
@Component
public class TallyRetryMstServiceImpl implements TallyRetryMstService{

	@Autowired
	TallyRetryMstRepository tallyretryMstRepo;
	@Override
	public TallyRetryMst saveTallyRetryMst(CompanyMst companyMst,String voucherType,String voucherNumber,Date voucherDate) {
		

		TallyRetryMst tallyRetryMst = new TallyRetryMst();
		tallyRetryMst.setCompanyMst(companyMst);
		tallyRetryMst.setVoucherNumber(voucherNumber);
		tallyRetryMst.setVoucherType(voucherType);
		tallyRetryMst.setIntegrationStatus("FAILED");
		tallyRetryMst.setVoucherDate(voucherDate);
		tallyRetryMst.setRetryNumber(1);
		tallyRetryMst.setLastRetryDate(voucherDate);
		tallyRetryMst = tallyretryMstRepo.save(tallyRetryMst);
		return tallyRetryMst;
		
	}

}
