package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.PoliceReport;

@Service
public interface PoliceReportService {

	List<PoliceReport> findPoliceReport(CompanyMst companyMst,Date fromDate, Date toDate,String[]array);

}
