package com.maple.restserver.service;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Service
@Transactional
public interface ResendBulkTransactionToCloudService {

	public void resendBulkTransactionToCloud(Date fromDate, Date toDate,
			String vouchertype);

}
