package com.maple.restserver.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.SummaryStatementOfAccountReport;
import com.maple.restserver.report.entity.DamageEntryReport;
import com.maple.restserver.repository.StatementOfAccountReportRepository;

@Service
@Transactional
@Component

public class StatementOfAccountReportServiceImpl implements  StatementOfAccountReportService{

	@Autowired
	StatementOfAccountReportRepository statementOfAccountReportRepository;
	
	
	@Override
	public List<SummaryStatementOfAccountReport> StatementOfAccountReport(String companymstid, java.util.Date startDate,
			java.util.Date endDate, String acconuntId) {

		List<SummaryStatementOfAccountReport> statementOfAccountList= new ArrayList<SummaryStatementOfAccountReport>();
		List<Object> obj =statementOfAccountReportRepository.statementOfAccountReport(companymstid,startDate,endDate,acconuntId);
		Double openingBalance=statementOfAccountReportRepository.openingBalance(companymstid, startDate,  acconuntId);
		Double closingBalance=statementOfAccountReportRepository.ClosingBalance(companymstid,  endDate, acconuntId);
		for(int i=0;i<obj.size();i++)
		 {
			
		Object[] objAray = (Object[]) obj.get(i);
		SummaryStatementOfAccountReport sSummary=new SummaryStatementOfAccountReport();
		sSummary.setTransDate((Date) objAray[0]);
		sSummary.setAccountName((String) objAray[1]);
		sSummary.setDebitAmount((Double) objAray[2]);
		sSummary.setCreditAmount((Double) objAray[3]);
		sSummary.setRemark((String) objAray[4]);
		sSummary.setOpeningBalance(openingBalance);
		sSummary.setClosingBalance(closingBalance);
		statementOfAccountList.add(sSummary);
		 }

		return statementOfAccountList;
	}
		
	

	
}
