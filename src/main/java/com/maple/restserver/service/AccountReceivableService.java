package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.CustomerBalanceReport;

@Component
public interface AccountReceivableService {
	List<CustomerBalanceReport> findByCustomerBalanceById(CompanyMst companyMst,String  id,Date dateFrom,Date dateTo) ;

	
	List< AccountReceivable> findByCompanyMstAndAccountId(CompanyMst companyMst,String id);


	List<CustomerBalanceReport> findByAllCustomerBalance(CompanyMst companyMst, Date dateFrom, Date dateTo);


	Double billwiseAdjustment(Date udate, CompanyMst companyMst, String receipthdr, Double receivedamount,
			String accountid);
}
