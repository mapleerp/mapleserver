package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.WholeSaleDetailReport;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.utils.SystemSetting;
@Service
public class WholeSaleDetailReportServiceImpl implements WholeSaleDetailReportService{

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	
	@Override                            
	public List<WholeSaleDetailReport> findWholeSaleDetailReport(Date fromDate, Date toDate) {

		List<WholeSaleDetailReport> wholeSaleDetailReportList = new ArrayList<WholeSaleDetailReport>();
		List<Object> obj = salesTransHdrRepository.findWholeSaleDetailReport(fromDate,toDate);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			WholeSaleDetailReport wholeSaleDetailReport = new WholeSaleDetailReport();
			
			wholeSaleDetailReport.setInvoiceDate(SystemSetting.UtilDateToString((Date) objAray[0], "yyyy-MM-dd"));
			wholeSaleDetailReport.setVoucherNumber((String) objAray[1]);
			wholeSaleDetailReport.setCustomerName((String) objAray[2]);
			wholeSaleDetailReport.setItemName((String) objAray[4]);
			wholeSaleDetailReport.setGroupName((String) objAray[3]);
			wholeSaleDetailReport.setBatchCode((String) objAray[6]);
			wholeSaleDetailReport.setItemCode((String) objAray[5]);
			if(null!=objAray[7]) {
			wholeSaleDetailReport.setExpiryDate(SystemSetting.UtilDateToString((Date) objAray[7], "yyyy-MM-dd"));
			}
			wholeSaleDetailReport.setQuantity((Double) objAray[8]);
			wholeSaleDetailReport.setRate((Double) objAray[9]);
			wholeSaleDetailReport.setTaxRate((Double) objAray[10]);
			wholeSaleDetailReport.setSaleValue((Double) objAray[15]);
			wholeSaleDetailReport.setCost((Double) objAray[11]);
			wholeSaleDetailReport.setCostValue((Double) objAray[16]);
			wholeSaleDetailReport.setSalesMan((String) objAray[14]);
			//wholeSaleDetailReport.setCustomerPO((String) objAray[]);
			wholeSaleDetailReport.setTinNumber((String) objAray[18]);
			wholeSaleDetailReport.setUserName((String) objAray[13]);
			wholeSaleDetailReport.setBeforeGST((Double) objAray[9]);
			wholeSaleDetailReport.setGst((Double) objAray[17]);
			wholeSaleDetailReport.setTotalInvoiceAmount((Double) objAray[12]);
			
		//====================================================//	
			wholeSaleDetailReport.setCustomerPO("ABC");
//			wholeSaleDetailReport.setTinNumber("123");
		//===================================================//	
			
			wholeSaleDetailReportList.add(wholeSaleDetailReport);
			
		 }

		return wholeSaleDetailReportList;
	
	}
	
	

}
