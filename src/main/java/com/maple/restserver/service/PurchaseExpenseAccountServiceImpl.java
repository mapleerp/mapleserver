package com.maple.restserver.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AdditionalExpense;
import com.maple.restserver.entity.PurchaseAdditionalExpenseDtl;
import com.maple.restserver.entity.PurchaseAdditionalExpenseHdr;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.AdditionalExpenseRepository;
import com.maple.restserver.repository.PurchaseAdditionalExpenseDtlRepository;
import com.maple.restserver.repository.PurchaseAdditionalExpenseHdrRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
public class PurchaseExpenseAccountServiceImpl implements PurchaseExpenseAccountService {

	@Autowired
	private AccountHeadsRepository accountHeadsRepo;

	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;

	@Autowired
	PurchaseDtlRepository purchaseDtlRepository;

	@Autowired
	PurchaseAdditionalExpenseHdrRepository purchaseAdditionalExpenseHdrRepository;

	@Autowired
	PurchaseAdditionalExpenseDtlRepository purchaseAdditionalExpenseDtlRepository;

	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	@Autowired
	private AccountClassRepository accountClassRepo;

	@Autowired
	private CreditClassRepository creditClassRepo;

	@Autowired
	private DebitClassRepository debitClassRepo;

	@Autowired
	private LedgerClassRepository ledgerClassRepo;

	@Autowired
	AdditionalExpenseRepository additionalExpenseRepository;

	@Override
	public void doImportExpenseAccounting(PurchaseHdr prucahsehdr) throws PartialAccountingException {
		// from purchase hdr, get purchase dtl
		// using purchasedtl get expense hdr
		// from exp hdr get exp dtl

		BigDecimal zero = new BigDecimal("0");

		List<PurchaseDtl> purchaseDtlList = purchaseDtlRepository.findByPurchaseHdrId(prucahsehdr.getId());

		for (PurchaseDtl purchaseDtl : purchaseDtlList) {
			PurchaseAdditionalExpenseHdr purchaseAdditionalExpenseHdr = purchaseAdditionalExpenseHdrRepository
					.findByPurchaseDtl(purchaseDtl);

			if (null == purchaseAdditionalExpenseHdr) {
				return;
			}
			List<PurchaseAdditionalExpenseDtl> purchaseAdditionalExpenseDtlList = purchaseAdditionalExpenseDtlRepository
					.findByPurchaseAdditionalExpenseHdrId(purchaseAdditionalExpenseHdr.getId());

			for (PurchaseAdditionalExpenseDtl purchaseDtlExp : purchaseAdditionalExpenseDtlList) {

				String accountName = purchaseDtlExp.getExpense();
				double amount = purchaseDtlExp.getAmount();
				// find account Id from Account Name.

				AccountHeads creditAccountHeads = accountHeadsRepository.findByAccountName(accountName);

				if (null == creditAccountHeads) {
					throw new PartialAccountingException("Account nount found" + accountName);

				}

				AccountClass accountClass = new AccountClass();
				BigDecimal totalCredit = new BigDecimal(amount);
				totalCredit = totalCredit.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				accountClass.setSourceParentId(prucahsehdr.getId());
				accountClass.setTransDate(prucahsehdr.getVoucherDate());
				accountClass.setSourceVoucherNumber(prucahsehdr.getVoucherNumber());
				accountClass.setVoucherType("PURCHASE");
				accountClass.setTotalDebit(totalCredit);
				accountClass.setBrachCode(prucahsehdr.getBranchCode());

				accountClass.setTotalCredit(totalCredit);

				accountClass.setCompanyMst(prucahsehdr.getCompanyMst());
				accountClass.setFinancialYear(SystemSetting.getFinancialYear());

				accountClass = accountClassRepo.save(accountClass);

				DebitClass debitclass = new DebitClass();

				AccountHeads debitAccountHeads = accountHeadsRepository.findByAccountName("PURCHASE ACCOUNTS");

				debitclass.setAccountId(debitAccountHeads.getId());
				debitclass.setAccountClass(accountClass);

				debitclass.setDrAmount(totalCredit);

				debitclass.setRemark(prucahsehdr.getVoucherNumber());
				debitclass.setCompanyMst(prucahsehdr.getCompanyMst());

				debitclass = debitClassRepo.save(debitclass);

				CreditClass creditclass = new CreditClass();

				creditclass.setAccountId(creditAccountHeads.getId());
				creditclass.setAccountClass(accountClass);
				creditclass.setCrAmount(totalCredit);

				creditclass.setRemark(prucahsehdr.getVoucherNumber());

				creditclass.setCompanyMst(prucahsehdr.getCompanyMst());
				creditclass.setTransDate(prucahsehdr.getVoucherDate());

				creditclass.setSourceVoucherNumber(prucahsehdr.getVoucherNumber());
				creditclass = creditClassRepo.saveAndFlush(creditclass);

				LedgerClass ledgerClassDr = new LedgerClass();
				ledgerClassDr.setAccountClass(accountClass);
				ledgerClassDr.setAccountId(debitclass.getAccountId());
				ledgerClassDr.setDebitAmount(debitclass.getDrAmount());
				ledgerClassDr.setCreditAmount(zero);
				ledgerClassDr.setCompanyMst(prucahsehdr.getCompanyMst());
				ledgerClassDr.setSourceVoucherId(prucahsehdr.getVoucherNumber());
				ledgerClassDr.setTransDate(prucahsehdr.getVoucherDate());
				ledgerClassDr.setBranchCode(prucahsehdr.getBranchCode());

				ledgerClassDr.setBranchCode(accountClass.getBrachCode());
				ledgerClassDr.setRemark("Purchase expense " + accountClass.getSourceVoucherNumber());

				// ledgerClassRepo.save(ledgerClassDr);

				ledgerClassRepo.save(ledgerClassDr);

				LedgerClass ledgerClassCr = new LedgerClass();
				ledgerClassCr.setAccountClass(accountClass);
				ledgerClassCr.setAccountId(creditclass.getAccountId());
				ledgerClassCr.setDebitAmount(zero);
				ledgerClassCr.setCreditAmount(creditclass.getCrAmount());
				ledgerClassCr.setCompanyMst(prucahsehdr.getCompanyMst());
				ledgerClassCr.setSourceVoucherId(prucahsehdr.getVoucherNumber());
				ledgerClassCr.setTransDate(prucahsehdr.getVoucherDate());
				ledgerClassCr.setBranchCode(prucahsehdr.getBranchCode());

				ledgerClassCr.setBranchCode(accountClass.getBrachCode());
				ledgerClassCr.setRemark("Purchase expense " + accountClass.getSourceVoucherNumber());

				// ledgerClassRepo.save(ledgerClassDr);

				ledgerClassRepo.save(ledgerClassCr);

			}

		}

	}

	@Override
	public void doAdditionalExpenseAccounting(PurchaseHdr prucahsehdr) throws PartialAccountingException {

		// from purchase hdr, get purchase dtl
		// using purchasedtl get expense hdr
		// from exp hdr get exp dtl

		BigDecimal zero = new BigDecimal("0");

		List<AdditionalExpense> additionalExpenseList = additionalExpenseRepository
				.findByPurchaseHdrId(prucahsehdr.getId());

		for (AdditionalExpense additionalExpense : additionalExpenseList) {

			if (additionalExpense.getExpenseHead().equalsIgnoreCase("INCLUDED IN BILL")) {
				includedExpense(additionalExpense, prucahsehdr);
			} else {
				notIncludedExpense(additionalExpense, prucahsehdr);
			}

		}

	}

	private void includedExpense(AdditionalExpense additionalExpense, PurchaseHdr prucahsehdr) {

		BigDecimal zero = new BigDecimal("0");
		double amount = additionalExpense.getAmount();

		// find account Id from Account Name.

		String creditAccountId = prucahsehdr.getSupplierId(); // additionalExpense.getAccountId();

		AccountClass accountClass = new AccountClass();
		BigDecimal totalCredit = new BigDecimal(amount);
		totalCredit = totalCredit.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		accountClass.setSourceParentId(prucahsehdr.getId());
		accountClass.setTransDate(prucahsehdr.getVoucherDate());
		accountClass.setSourceVoucherNumber(prucahsehdr.getVoucherNumber());
		accountClass.setVoucherType("PURCHASE");
		accountClass.setTotalDebit(totalCredit);
		accountClass.setBrachCode(prucahsehdr.getBranchCode());

		accountClass.setTotalCredit(totalCredit);

		accountClass.setCompanyMst(prucahsehdr.getCompanyMst());
		accountClass.setFinancialYear(SystemSetting.getFinancialYear());

		accountClass = accountClassRepo.save(accountClass);

		DebitClass debitclass = new DebitClass();

		// AccountHeads debitAccountHeads =
		// accountHeadsRepository.findByAccountName("PURCHASE ACCOUNT");

		String debitAccountId = additionalExpense.getAccountId();

		debitclass.setAccountId(debitAccountId);
		debitclass.setAccountClass(accountClass);

		debitclass.setDrAmount(totalCredit);

		debitclass.setRemark(prucahsehdr.getVoucherNumber());
		debitclass.setCompanyMst(prucahsehdr.getCompanyMst());

		debitclass = debitClassRepo.save(debitclass);

		CreditClass creditclass = new CreditClass();

		creditclass.setAccountId(creditAccountId);
		creditclass.setAccountClass(accountClass);
		creditclass.setCrAmount(totalCredit);

		creditclass.setRemark(prucahsehdr.getVoucherNumber());

		creditclass.setCompanyMst(prucahsehdr.getCompanyMst());
		creditclass.setTransDate(prucahsehdr.getVoucherDate());

		creditclass.setSourceVoucherNumber(prucahsehdr.getVoucherNumber());
		creditclass = creditClassRepo.saveAndFlush(creditclass);

		LedgerClass ledgerClassDr = new LedgerClass();
		ledgerClassDr.setAccountClass(accountClass);
		ledgerClassDr.setAccountId(debitclass.getAccountId());
		ledgerClassDr.setDebitAmount(debitclass.getDrAmount());
		ledgerClassDr.setCreditAmount(zero);
		ledgerClassDr.setCompanyMst(prucahsehdr.getCompanyMst());
		ledgerClassDr.setSourceVoucherId(prucahsehdr.getVoucherNumber());
		ledgerClassDr.setTransDate(prucahsehdr.getVoucherDate());
		ledgerClassDr.setBranchCode(prucahsehdr.getBranchCode());

		ledgerClassDr.setBranchCode(accountClass.getBrachCode());
		ledgerClassDr.setRemark("Purchase expense " + accountClass.getSourceVoucherNumber());

		// ledgerClassRepo.save(ledgerClassDr);

		ledgerClassRepo.save(ledgerClassDr);

		LedgerClass ledgerClassCr = new LedgerClass();
		ledgerClassCr.setAccountClass(accountClass);
		ledgerClassCr.setAccountId(creditclass.getAccountId());
		ledgerClassCr.setDebitAmount(zero);
		ledgerClassCr.setCreditAmount(creditclass.getCrAmount());
		ledgerClassCr.setCompanyMst(prucahsehdr.getCompanyMst());
		ledgerClassCr.setSourceVoucherId(prucahsehdr.getVoucherNumber());
		ledgerClassCr.setTransDate(prucahsehdr.getVoucherDate());
		ledgerClassCr.setBranchCode(prucahsehdr.getBranchCode());

		ledgerClassCr.setBranchCode(accountClass.getBrachCode());
		ledgerClassCr.setRemark("Purchase expense " + accountClass.getSourceVoucherNumber());

		// ledgerClassRepo.save(ledgerClassDr);

		ledgerClassRepo.save(ledgerClassCr);

	}

	private void notIncludedExpense(AdditionalExpense additionalExpense, PurchaseHdr prucahsehdr) {

		BigDecimal zero = new BigDecimal("0");
		double amount = additionalExpense.getAmount();

		// find account Id from Account Name.

		AccountHeads creditAccount = accountHeadsRepo.findByAccountNameAndCompanyMstId("PURCHASE ACCOUNTS",
				prucahsehdr.getCompanyMst().getId());

		String creditAccountId = creditAccount.getId();

		AccountClass accountClass = new AccountClass();
		BigDecimal totalCredit = new BigDecimal(amount);
		totalCredit = totalCredit.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		accountClass.setSourceParentId(prucahsehdr.getId());
		accountClass.setTransDate(prucahsehdr.getVoucherDate());
		accountClass.setSourceVoucherNumber(prucahsehdr.getVoucherNumber());
		accountClass.setVoucherType("PURCHASE");
		accountClass.setTotalDebit(totalCredit);
		accountClass.setBrachCode(prucahsehdr.getBranchCode());

		accountClass.setTotalCredit(totalCredit);

		accountClass.setCompanyMst(prucahsehdr.getCompanyMst());
		accountClass.setFinancialYear(SystemSetting.getFinancialYear());

		accountClass = accountClassRepo.save(accountClass);

		DebitClass debitclass = new DebitClass();

		// AccountHeads debitAccountHeads =
		// accountHeadsRepository.findByAccountName("PURCHASE ACCOUNT");

		String debitAccountId = additionalExpense.getAccountId();

		debitclass.setAccountId(debitAccountId);
		debitclass.setAccountClass(accountClass);

		debitclass.setDrAmount(totalCredit);

		debitclass.setRemark(prucahsehdr.getVoucherNumber());
		debitclass.setCompanyMst(prucahsehdr.getCompanyMst());

		debitclass = debitClassRepo.save(debitclass);

		CreditClass creditclass = new CreditClass();

		creditclass.setAccountId(creditAccountId);
		creditclass.setAccountClass(accountClass);
		creditclass.setCrAmount(totalCredit);

		creditclass.setRemark(prucahsehdr.getVoucherNumber());

		creditclass.setCompanyMst(prucahsehdr.getCompanyMst());
		creditclass.setTransDate(prucahsehdr.getVoucherDate());

		creditclass.setSourceVoucherNumber(prucahsehdr.getVoucherNumber());
		creditclass = creditClassRepo.saveAndFlush(creditclass);

		LedgerClass ledgerClassDr = new LedgerClass();
		ledgerClassDr.setAccountClass(accountClass);
		ledgerClassDr.setAccountId(debitclass.getAccountId());
		ledgerClassDr.setDebitAmount(debitclass.getDrAmount());
		ledgerClassDr.setCreditAmount(zero);
		ledgerClassDr.setCompanyMst(prucahsehdr.getCompanyMst());
		ledgerClassDr.setSourceVoucherId(prucahsehdr.getVoucherNumber());
		ledgerClassDr.setTransDate(prucahsehdr.getVoucherDate());
		ledgerClassDr.setBranchCode(prucahsehdr.getBranchCode());

		ledgerClassDr.setBranchCode(accountClass.getBrachCode());
		ledgerClassDr.setRemark("Purchase expense " + accountClass.getSourceVoucherNumber());

		// ledgerClassRepo.save(ledgerClassDr);

		ledgerClassRepo.save(ledgerClassDr);

		LedgerClass ledgerClassCr = new LedgerClass();
		ledgerClassCr.setAccountClass(accountClass);
		ledgerClassCr.setAccountId(creditclass.getAccountId());
		ledgerClassCr.setDebitAmount(zero);
		ledgerClassCr.setCreditAmount(creditclass.getCrAmount());
		ledgerClassCr.setCompanyMst(prucahsehdr.getCompanyMst());
		ledgerClassCr.setSourceVoucherId(prucahsehdr.getVoucherNumber());
		ledgerClassCr.setTransDate(prucahsehdr.getVoucherDate());
		ledgerClassCr.setBranchCode(prucahsehdr.getBranchCode());

		ledgerClassCr.setBranchCode(accountClass.getBrachCode());
		ledgerClassCr.setRemark("Purchase expense " + accountClass.getSourceVoucherNumber());

		// ledgerClassRepo.save(ledgerClassDr);

		ledgerClassRepo.save(ledgerClassCr);

	}

}
