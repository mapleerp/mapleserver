package com.maple.restserver.service.task;

 
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductConversionDtl;
import com.maple.restserver.entity.ProductConversionMst;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProductConversionDtlRepository;
import com.maple.restserver.repository.ProductConversionMstRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;



@Transactional
@Service
public class ProductConversionStock  {
	 private static final Logger logger = LoggerFactory.getLogger(ProductConversionStock.class);
	  //---------------version 5.5 surya
	  

	  @Autowired
	  ItemBatchDtlService itemBatchDtlService ;
	  //--------------version 5.5 surya end
	
	 @Autowired
		private ItemBatchMstRepository itemBatchMstRepo;
		
	 @Autowired
		private SalesTransHdrRepository salesTransHdrRepo;
	 
	 @Autowired
		private SalesDetailsRepository salesDetailsRepo;
	 
	 @Autowired
		private ItemBatchDtlRepository  itemBatchDtlRepository;
	 
	  @Autowired
	  private VoucherNumberService voucherService;
	  
	  @Autowired
	  ProductConversionDtlRepository productConversionDtlRepository;
	  
	  @Autowired
	  ProductConversionMstRepository productConversionMstRepository ;
	 
	  @Autowired
	  ItemMstRepository itemMstRepository ;

EventBus eventBus = EventBusFactory.getEventBus();

@Autowired
LmsQueueMstRepository lmsQueueMstRepository;
	  
 
	public void execute( String voucherNumber  ,Date voucherDate,CompanyMst companyMst, String productConversionMstId )   {
		 	
		Optional<ProductConversionMst> productConversionMstOpt = productConversionMstRepository.findById(productConversionMstId);
		
		List<ProductConversionDtl> productConversionDtlList = productConversionDtlRepository.findByProductConversionMstId(productConversionMstOpt.get().getId());

		Iterator iter =productConversionDtlList.iterator();
		while (iter.hasNext()) {
			ProductConversionDtl productConversionDtl = (ProductConversionDtl) iter.next();
			
			Optional<ItemMst> itemoptFrom = itemMstRepository.findById(productConversionDtl.getFromItem());
			Optional<ItemMst> itemoptTo = itemMstRepository.findById(productConversionDtl.getToItem());
			
			// find if batch mast has an entry
			List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo.findByItemIdAndBatchAndBarcode(itemoptFrom.get().getId(),
					MapleConstants.Nobatch,itemoptFrom.get().getBarCode() ) ;
			
		   if(itembatchmst.size()==1) {
				ItemBatchMst itemBatchMst =itembatchmst.get(0);
				itemBatchMst.setMrp(itemoptFrom.get().getStandardPrice());
				itemBatchMst.setQty(itemBatchMst.getQty()  -   productConversionDtl.getFromQty()  );
				itemBatchMstRepo.saveAndFlush(itemBatchMst);
			}else {
				ItemBatchMst itemBatchMst = new ItemBatchMst();
				itemBatchMst.setBarcode(itemoptFrom.get().getBarCode() );
				itemBatchMst.setBatch(MapleConstants.Nobatch);
				itemBatchMst.setMrp(itemoptFrom.get().getStandardPrice());
				itemBatchMst.setQty( -1 * productConversionDtl.getFromQty());
				itemBatchMst.setItemId(itemoptFrom.get().getId());
				itemBatchMst.setCompanyMst(companyMst);
				itemBatchMstRepo.saveAndFlush(itemBatchMst);
			}
			
			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
			final VoucherNumber voucherNo = voucherService.generateInvoice("STV",companyMst.getId());
			itemBatchDtl.setBarcode(itemoptFrom.get().getBarCode());
			itemBatchDtl.setBatch(MapleConstants.Nobatch);
			itemBatchDtl.setItemId(itemoptFrom.get().getId());
			itemBatchDtl.setMrp(itemoptFrom.get().getStandardPrice());
			itemBatchDtl.setQtyIn(0.0);
			itemBatchDtl.setQtyOut( productConversionDtl.getFromQty()  );
			itemBatchDtl.setVoucherNumber(voucherNo.getCode());
			itemBatchDtl.setVoucherDate(productConversionDtl.getProductConversionMst().getConversionDate());
			itemBatchDtl.setSourceVoucherNumber(productConversionDtl.getProductConversionMst().getVoucherNo());
			itemBatchDtl.setCompanyMst(companyMst);
			itemBatchDtlRepository.saveAndFlush(itemBatchDtl)	;
			// To Product 

			// find if batch mast has an entry
			List<ItemBatchMst> itembatchmstTo = (List<ItemBatchMst>) itemBatchMstRepo.findByItemIdAndBatchAndBarcode(itemoptTo.get().getId(),
					MapleConstants.Nobatch,itemoptTo.get().getBarCode() ) ;
			
		   if(itembatchmstTo.size()==1) {
				ItemBatchMst itemBatchMstTo =itembatchmstTo.get(0);
				itemBatchMstTo.setMrp(itemoptTo.get().getStandardPrice());
				itemBatchMstTo.setQty(itemBatchMstTo.getQty()  +   productConversionDtl.getToQty()  );
				itemBatchMstRepo.saveAndFlush(itemBatchMstTo);
			}else {
				ItemBatchMst itemBatchMst = new ItemBatchMst();
				itemBatchMst.setBarcode(itemoptTo.get().getBarCode() );
				itemBatchMst.setBatch(MapleConstants.Nobatch);
				itemBatchMst.setMrp(itemoptTo.get().getStandardPrice());
				itemBatchMst.setQty(productConversionDtl.getToQty() );
				itemBatchMst.setItemId(itemoptTo.get().getId());
				itemBatchMst.setCompanyMst(companyMst);
				itemBatchMstRepo.saveAndFlush(itemBatchMst);
			}
			ItemBatchDtl itemBatchDtlTo = new ItemBatchDtl();
			itemBatchDtlTo.setBarcode(itemoptTo.get().getBarCode());
			itemBatchDtlTo.setBatch(MapleConstants.Nobatch);
			itemBatchDtlTo.setItemId(itemoptTo.get().getId());
			itemBatchDtlTo.setMrp(itemoptTo.get().getStandardPrice());
			itemBatchDtlTo.setQtyIn( productConversionDtl.getToQty() );
			itemBatchDtlTo.setQtyOut( 0.0  );
			itemBatchDtlTo.setVoucherNumber(voucherNo.getCode());
			itemBatchDtlTo.setVoucherDate(productConversionDtl.getProductConversionMst().getConversionDate());
			itemBatchDtl.setSourceVoucherNumber(productConversionDtl.getProductConversionMst().getVoucherNo());
			itemBatchDtl.setCompanyMst(companyMst);
			itemBatchDtlRepository.saveAndFlush(itemBatchDtl)	;
		}
	}
	
	public void executebatchwise( String voucherNumber  ,Date voucherDate,CompanyMst companyMst, String productConversionMstId )   {
	 	
		Optional<ProductConversionMst> productConversionMstOpt = productConversionMstRepository.findById(productConversionMstId);
		
		List<ProductConversionDtl> productConversionDtlList = productConversionDtlRepository.findByProductConversionMstId(productConversionMstOpt.get().getId());
	
		
		Iterator iter = productConversionDtlList.iterator();
		while (iter.hasNext()) {
			ProductConversionDtl productConversionDtl = (ProductConversionDtl) iter.next();
			
			if(null != productConversionDtl.getFromItem())
			{
			//	if(!productConversionDtl.getBatchCode().equalsIgnoreCase("NOBATCH"))
			//	{
			Optional<ItemMst> itemoptFrom = itemMstRepository.findById(productConversionDtl.getFromItem());
			Optional<ItemMst> itemoptTo = itemMstRepository.findById(productConversionDtl.getToItem());
			// find if batch mast has an entry
			
			
			
			
			//=======apply a common function for item_batch_mst updation and 
			// ======== item_batch_dtl insertion ====== by anandu === 25-06-2021 
			
			List<ProductConversionDtl> productConversionDtlOpt=productConversionDtlRepository.findByIdAndBatchCode(
					  itemoptFrom.get().getId(),productConversionDtl.getBatchCode());
			
			Double qty = -1 * productConversionDtl.getFromQty();
			Date ExpiryDate = null;
			String processInstanceId = null;
			String taskId = null;
			
			 
			itemBatchDtlService.itemBatchMstUpdation(itemoptFrom.get().getBarCode(),productConversionDtlOpt.get(0).getBatchCode(),productConversionDtlOpt.get(0).getProductConversionMst().getBranchCode(),
					ExpiryDate,itemoptFrom.get().getId(),itemoptFrom.get().getStandardPrice(),processInstanceId,qty,taskId,companyMst,"MAIN");
			
			
			String barcode = null;
			String batchCode = null;
			String itemId = null;
			String branchCode = null;
			String sourceVoucherNo = null;
			String particulars = null;
			String sourceParentId = null;
			String sourceDtlId = null;
			
			Double standardPrice = 0.0;
			Double qtyIn = 0.0;
			Double qtyOut = 0.0;
			Date invoiceVoucherDate = null;


			barcode = itemoptFrom.get().getBarCode();
			batchCode = productConversionDtl.getBatchCode();
			itemId = itemoptFrom.get().getId();
			standardPrice = itemoptFrom.get().getStandardPrice();
			qtyIn = 0.0;
			qtyOut = productConversionDtl.getFromQty();
			branchCode = productConversionDtl.getProductConversionMst().getBranchCode();
			invoiceVoucherDate = productConversionDtl.getProductConversionMst().getConversionDate();
			sourceVoucherNo = productConversionDtl.getProductConversionMst().getVoucherNo();

			particulars = "PRODUCT CONVERSION";
			sourceParentId = productConversionMstId;
			sourceDtlId = productConversionDtl.getId();
			final VoucherNumber voucherNo = voucherService.generateInvoice("STV",companyMst.getId());

						
			itemBatchDtlService.itemBatchDtlInsertionNew(barcode,batchCode,branchCode,
					ExpiryDate,itemId,standardPrice,particulars,processInstanceId,
					qtyIn,qtyOut,sourceDtlId,sourceParentId,invoiceVoucherDate,sourceVoucherNo,"MAIN",taskId,
					invoiceVoucherDate,voucherNo.getCode(),companyMst);
			
			
//						// To Product 
			
			itemBatchDtlService.itemBatchMstUpdation(itemoptTo.get().getBarCode(),productConversionDtl.getBatchCode(),productConversionDtlOpt.get(0).getProductConversionMst().getBranchCode(),
					ExpiryDate,itemoptTo.get().getId(),itemoptTo.get().getStandardPrice(),processInstanceId,productConversionDtl.getToQty(),taskId,companyMst,"MAIN");
			
			
		   barcode = itemoptTo.get().getBarCode();
					itemId = itemoptTo.get().getId();
					standardPrice = itemoptTo.get().getStandardPrice();
					qtyIn = productConversionDtl.getToQty();
					qtyOut = 0.0;

					String unitId = itemoptTo.get().getUnitId();
					particulars = "PRODUCT CONVERSION";
					sourceParentId = productConversionMstId;
					
					
					itemBatchDtlService.itemBatchDtlInsertionNew(barcode,batchCode,branchCode,
							ExpiryDate,itemId,standardPrice,particulars,processInstanceId,
							qtyIn,qtyOut,sourceDtlId,sourceParentId,invoiceVoucherDate,sourceVoucherNo,"MAIN",taskId,
							invoiceVoucherDate,voucherNo.getCode(),companyMst);
					
					//================= end ======================== 25-06-2021 ===========
				   
			
		}
		}
		}
		

}
