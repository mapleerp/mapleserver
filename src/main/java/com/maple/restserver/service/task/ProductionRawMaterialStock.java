package com.maple.restserver.service.task;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductionBatchStockDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.ActualProductionDtlRepository;
import com.maple.restserver.repository.ActualProductionHdrRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProductionBatchStockDtlRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Transactional
@Service
public class ProductionRawMaterialStock {
	private static final Logger logger = LoggerFactory.getLogger(ProductionRawMaterialStock.class);
	@Autowired
	ProductionFinishedGoodsStock productionFinishedGoodsStock;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	private ItemBatchMstRepository itemBatchMstRepo;
	@Autowired
	CategoryMstRepository categoryMstRepos;
	@Autowired
	private SalesTransHdrRepository salesTransHdrRepo;
	EventBus eventBus = EventBusFactory.getEventBus();
	@Autowired
	private SalesDetailsRepository salesDetailsRepo;

	@Autowired
	ProductionBatchStockDtlRepository productionBatchStockDtlRepository;
	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	ActualProductionHdrRepository actualProductionHdrRepository;

	@Autowired
	ActualProductionDtlRepository actualProductionDtlRepository;

	@Autowired
	ItemMstRepository itemMstRepository;

	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepository;

	@Autowired
	KitDefenitionDtlRepository kitDefinitionDtlRepository;

	@Autowired
	ItemBatchDtlService itemBatchDtlService;

	public void execute(String voucherNumber, Date voucherDate, CompanyMst companyMst, String actualProductionHdrId) {

//		Optional<ActualProductionHdr> actualProductionHdrOpt = actualProductionHdrRepository
//				.findById(actualProductionHdrId);
//
//		List<ActualProductionDtl> actualProductionDtlList = actualProductionDtlRepository
//				.findByActualProductionHdrId(actualProductionHdrOpt.get().getId());
//
//		Iterator iter = actualProductionDtlList.iterator();
//		while (iter.hasNext()) {
//			ActualProductionDtl actualProductionDtl = (ActualProductionDtl) iter.next();
//
//			Double qty = actualProductionDtl.getActualQty();
//			String itemId = actualProductionDtl.getItemId();
//
//			/*
//			 * Find Kit Header
//			 */
//
//			KitDefinitionMst kitDefinitionMst = null;
//			List<KitDefinitionMst> kitDefinitionMstList = kitDefinitionMstRepository.findByItemId(itemId);
//			
//			if(kitDefinitionMstList.size()>0)
//			{
//				kitDefinitionMst = kitDefinitionMstList.get(0);
//			}
//			Double defenitionQty = kitDefinitionMst.getMinimumQty();
//
//			/*
//			 * Find Kit Dtl - Raw Materil
//			 */
//
//			List<KitDefenitionDtl> kitDefenitionDtlList = kitDefinitionDtlRepository
//					.findBykitDefenitionmstId(kitDefinitionMst.getId());
//
//			Iterator iterRM = kitDefenitionDtlList.iterator();
//
//			while (iterRM.hasNext()) {
//
//				KitDefenitionDtl kitDefenitionDtl = (KitDefenitionDtl) iterRM.next();
//
//				String rawMaterialId = kitDefenitionDtl.getItemId();
//				Double rawMaterialUnitQty = kitDefenitionDtl.getQty();
//
//				Double requiredRawMaterial = rawMaterialUnitQty * qty / defenitionQty;
//
//				Optional<ItemMst> itemopt = itemMstRepository.findById(rawMaterialId);
//
//				ItemMst rmItem = itemopt.get();
//
//				// find if batch mast has an entry
//				List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
//						.findByItemIdAndBatchAndBarcode(rawMaterialId, "NOBATCH", rmItem.getBarCode());
//
//				if (itembatchmst.size() == 1) {
//					ItemBatchMst itemBatchMst = itembatchmst.get(0);
//					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
//					itemBatchMst.setQty(itemBatchMst.getQty() - requiredRawMaterial);
//
//					itemBatchMst.setBranchCode(actualProductionHdrOpt.get().getBranchCode());
//
//					itemBatchMstRepo.saveAndFlush(itemBatchMst);
//				} else {
//					ItemBatchMst itemBatchMst = new ItemBatchMst();
//					itemBatchMst.setBarcode(itemopt.get().getBarCode());
//					itemBatchMst.setBatch("NOBATCH");
//					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
//					itemBatchMst.setQty(-1 * requiredRawMaterial);
//					itemBatchMst.setItemId(rawMaterialId);
//					itemBatchMst.setBranchCode(actualProductionHdrOpt.get().getBranchCode());
//
//					itemBatchMst.setCompanyMst(companyMst);
//					itemBatchMstRepo.saveAndFlush(itemBatchMst);
//
//				}
//				
//				itemBatchDtlService.ProductionRawMaterialStock(rawMaterialId,itemopt.get(),requiredRawMaterial
//						,actualProductionDtl,actualProductionHdrOpt.get(),companyMst,entr);
//
////				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
////				final VoucherNumber voucherNo = voucherService.generateInvoice("STV", companyMst.getId());
////
////				itemBatchDtl.setBarcode(itemopt.get().getBarCode());
////				itemBatchDtl.setBatch("NOBATCH");
////				itemBatchDtl.setItemId(rawMaterialId);
////				itemBatchDtl.setMrp(itemopt.get().getStandardPrice());
////				itemBatchDtl.setQtyIn(0.0);
////				itemBatchDtl.setQtyOut(requiredRawMaterial);
////				itemBatchDtl.setVoucherNumber(voucherNo.getCode());
////				itemBatchDtl.setVoucherDate(actualProductionDtl.getActualProductionHdr().getVoucherDate());
////				itemBatchDtl.setItemId(rawMaterialId);
////
////				itemBatchDtl.setSourceVoucherNumber(actualProductionDtl.getActualProductionHdr().getVoucherNumber());
////
////				itemBatchDtl.setBranchCode(actualProductionHdrOpt.get().getBranchCode());
////				itemBatchDtl.setCompanyMst(companyMst);
////
////				itemBatchDtlRepository.saveAndFlush(itemBatchDtl);
//
//			}
//
//		}

	}

	public ArrayList checkRawMaterial(String actualProductionHdrId) {

		ArrayList itemLis = new ArrayList();
		Optional<ActualProductionHdr> actualProductionHdrOpt = actualProductionHdrRepository
				.findById(actualProductionHdrId);

		List<ActualProductionDtl> actualProductionDtlList = actualProductionDtlRepository
				.findByActualProductionHdrId(actualProductionHdrOpt.get().getId());

		Iterator iter = actualProductionDtlList.iterator();
		while (iter.hasNext()) {
			double reqQty = 0.0;
			ActualProductionDtl actualProductionDtl = (ActualProductionDtl) iter.next();

			Double qty = actualProductionDtl.getProductionDtl().getQty();
			String itemId = actualProductionDtl.getItemId();

			KitDefinitionMst kitDefinitionMst = null;
			List<KitDefinitionMst> kitDefinitionMstList = kitDefinitionMstRepository.findByItemId(itemId);

			if (kitDefinitionMstList.size() > 0) {
				kitDefinitionMst = kitDefinitionMstList.get(0);
			}
			Double defenitionQty = kitDefinitionMst.getMinimumQty();

			/*
			 * Find Kit Dtl - Raw Materil
			 */

			List<KitDefenitionDtl> kitDefenitionDtlList = kitDefinitionDtlRepository
					.findBykitDefenitionmstId(kitDefinitionMst.getId());

			Iterator iterRM = kitDefenitionDtlList.iterator();

			while (iterRM.hasNext()) {

				KitDefenitionDtl kitDefenitionDtl = (KitDefenitionDtl) iterRM.next();

				String rawMaterialId = kitDefenitionDtl.getItemId();
				Double rawMaterialUnitQty = kitDefenitionDtl.getQty();

				Double requiredRawMaterial = rawMaterialUnitQty * qty / defenitionQty;

				Optional<ItemMst> itemopt = itemMstRepository.findById(rawMaterialId);

				ItemMst rmItem = itemopt.get();
				// Map<String, Double> map = new HashMap<String, Double>();
//				

				JSONArray map = itemBatchDtlService.getBatchWiseQty(rawMaterialId, requiredRawMaterial);
//				
				if (map.isEmpty()) {
					itemLis.add(rmItem.getItemName());

				}

				else {
					double sysQty = 0.0;

					Iterator<Object> objectIterator = map.iterator();

					while (objectIterator.hasNext()) {
						JSONObject object = (JSONObject) objectIterator.next();
						String batch = (String) object.get("batch");
						double qtyB = (double) object.get("qty");
						java.sql.Date expDate = (java.sql.Date) object.get("exp");
						sysQty = sysQty + qtyB;
					}
					reqQty = reqQty + requiredRawMaterial;
					if (requiredRawMaterial != sysQty) {

						itemLis.add(rmItem.getItemName());

					}

				}
			}
		}
		for (int i = 0; i < itemLis.size(); i++) {
			for (int j = i; j < itemLis.size(); j++) {
				if (itemLis.get(i).toString().equalsIgnoreCase(itemLis.get(j).toString())) {
					itemLis.remove(j);
				}
			}
		}
		return itemLis;
	}

	public void executebatchwiserowmaterialupdate(String voucherNumber, Date voucherDate, CompanyMst companyMst,
			String actualProductionHdrId) throws ResourceNotFoundException {

		Optional<ActualProductionHdr> actualProductionHdrOpt = actualProductionHdrRepository
				.findById(actualProductionHdrId);

		List<ActualProductionDtl> actualProductionDtlList = actualProductionDtlRepository
				.findByActualProductionHdrId(actualProductionHdrOpt.get().getId());

		Iterator iter = actualProductionDtlList.iterator();
		while (iter.hasNext()) {
			
			ActualProductionDtl actualProductionDtl = (ActualProductionDtl) iter.next();

			Double qty = actualProductionDtl.getProductionDtl().getQty();
			String itemId = actualProductionDtl.getItemId();
			Double actualQty = actualProductionDtl.getActualQty();

			KitDefinitionMst kitDefinitionMst = null;
			List<KitDefinitionMst> kitDefinitionMstList = kitDefinitionMstRepository.findByItemId(itemId);

			if (kitDefinitionMstList.size() > 0) {
				kitDefinitionMst = kitDefinitionMstList.get(0);
			}
			Double defenitionQty = kitDefinitionMst.getMinimumQty();
			Double requiredRawMaterial = 0.0;

			List<KitDefenitionDtl> kitDefenitionDtlList = kitDefinitionDtlRepository
					.findBykitDefenitionmstId(kitDefinitionMst.getId());

			Iterator iterRM = kitDefenitionDtlList.iterator();

			while (iterRM.hasNext()) {

				KitDefenitionDtl kitDefenitionDtl = (KitDefenitionDtl) iterRM.next();

				String rawMaterialId = kitDefenitionDtl.getItemId();
				Double rawMaterialUnitQty = kitDefenitionDtl.getQty();

				Optional<ItemMst> itemopt = itemMstRepository.findById(rawMaterialId);
				System.out.println(itemopt.get());
				ItemMst rmItem = itemopt.get();

				if (null == rmItem.getCategoryId()) {
					requiredRawMaterial = rawMaterialUnitQty * qty / defenitionQty;
				} else {
					Optional<CategoryMst> categoryMstopt = categoryMstRepos.findById(rmItem.getCategoryId());
					if (categoryMstopt.isPresent()) {
						CategoryMst categoryMst = categoryMstopt.get();
						if (categoryMst.getCategoryName().equalsIgnoreCase("PACKING MATERIALS")) {
							requiredRawMaterial = rawMaterialUnitQty * actualQty / defenitionQty;
						} else {
							requiredRawMaterial = rawMaterialUnitQty * qty / defenitionQty;
						}
					} else {
						requiredRawMaterial = rawMaterialUnitQty * qty / defenitionQty;
					}
				}

				BigDecimal bReqMat = new BigDecimal(requiredRawMaterial);
				bReqMat = bReqMat.setScale(6, BigDecimal.ROUND_HALF_DOWN);
				System.out.println(bReqMat.toPlainString());
				requiredRawMaterial = bReqMat.doubleValue();
				// Map<String, Double> map = new HashMap<String, Double>();
//					
				// map = (HashMap<String, Double>)
				// itemBatchDtlService.getBatchWiseQty(rawMaterialId,
				// requiredRawMaterial);

				JSONArray map = itemBatchDtlService.getBatchWiseQtyWithStore(rawMaterialId, requiredRawMaterial,actualProductionDtl.getStore());

				if (map.isEmpty()) {

					System.out.println(map);
					throw new ResourceNotFoundException();
				}

				else {
					double sysQty = 0.0;

					Iterator<Object> objectIterator = map.iterator();
					while (objectIterator.hasNext()) {
						JSONObject object = (JSONObject) objectIterator.next();
						String batch = (String) object.get("batch");
						double qtyB = (double) object.get("qty");

						String store = (String) object.get("store");

						java.sql.Date expDate = SystemSetting.StringToSqlDate("2050-01-01", "yyyy-MM-dd");
						if (null != object.get("exp")) {

							expDate = (java.sql.Date) object.get("exp");
						}

						BigDecimal bentrQty = new BigDecimal(qtyB);
						bentrQty = bentrQty.setScale(6, BigDecimal.ROUND_HALF_DOWN);
						sysQty = sysQty + bentrQty.doubleValue();
						// }
						BigDecimal bsysQty = new BigDecimal(sysQty);
						bsysQty = bsysQty.setScale(6, BigDecimal.ROUND_HALF_DOWN);
						System.out.println(bsysQty.toPlainString());
						sysQty = bsysQty.doubleValue();
						if (requiredRawMaterial > sysQty) {
							logger.info("1" + itemopt.get().getItemName());
							System.out.println("Code here");
//					actualProductionHdrRepository.deleteById(actualProductionHdrId);
							// actualProductionDtlRepository.deleteById(actualProductionDtl.getId());
							// throw new ResourceNotFoundException();
						}

//					objectIterator = map.iterator();
//					while (objectIterator.hasNext()) {
//						JSONObject object = (JSONObject) objectIterator.next();
//						String batch = (String) object.get("batch");
//						double qtyB = (double) object.get("qty");
						String particular = itemopt.get().getItemName() + actualProductionDtl.getActualQty()
								+ "PRODUCTION";

						itemBatchDtlService.itemBatchDtlInsertionNew(rmItem.getBarCode(), batch,
								actualProductionHdrOpt.get().getBranchCode(), expDate, rawMaterialId,
								itemopt.get().getStandardPrice(), particular, null, 0.0, requiredRawMaterial,
								rawMaterialId, actualProductionHdrOpt.get().getId(), voucherDate, voucherNumber, store,
								null, voucherDate, actualProductionHdrOpt.get().getVoucherNumber(), companyMst);

						requiredRawMaterial = requiredRawMaterial * -1;

						itemBatchDtlService.itemBatchMstUpdation(rmItem.getBarCode(), batch,
								actualProductionHdrOpt.get().getBranchCode(), expDate, rawMaterialId,
								itemopt.get().getStandardPrice(), null, requiredRawMaterial, null, companyMst, store);

//						List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
//								.findByItemIdAndBatchAndBarcodeAndStore(rawMaterialId, batch, rmItem.getBarCode(),store);
//
//						if (itembatchmst.size() == 1) {
//							ItemBatchMst itemBatchMst = itembatchmst.get(0);
//							itemBatchMst.setMrp(itemopt.get().getStandardPrice());
//
//							itemBatchMst.setQty(itemBatchMst.getQty() - requiredRawMaterial);
//
//							itemBatchMst.setBranchCode(actualProductionHdrOpt.get().getBranchCode());
//
//							itemBatchMstRepo.save(itemBatchMst);
//						} else {
//							ItemBatchMst itemBatchMst = new ItemBatchMst();
//							itemBatchMst.setBarcode(itemopt.get().getBarCode());
//							itemBatchMst.setBatch(batch);
//							itemBatchMst.setMrp(itemopt.get().getStandardPrice());
//							itemBatchMst.setQty(-1 * requiredRawMaterial);
//							itemBatchMst.setItemId(rawMaterialId);
//							itemBatchMst.setBranchCode(actualProductionHdrOpt.get().getBranchCode());
//							itemBatchMst.setCompanyMst(companyMst);
//							itemBatchMstRepo.save(itemBatchMst);
//
//						}
//						itemBatchDtlService.ProductionRawMaterialStock(rawMaterialId, itemopt.get(), requiredRawMaterial,
//								actualProductionDtl, actualProductionHdrOpt.get(), companyMst, batch);

					}
				}
			}
		}

	}

}
