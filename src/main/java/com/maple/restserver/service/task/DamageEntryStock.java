package com.maple.restserver.service.task;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.DamageHdr;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductConversionDtl;
import com.maple.restserver.entity.ProductConversionMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.repository.ActualProductionDtlRepository;
import com.maple.restserver.repository.ActualProductionHdrRepository;
import com.maple.restserver.repository.DamageDtlRepository;
import com.maple.restserver.repository.DamageHdrRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProductConversionDtlRepository;
import com.maple.restserver.repository.ProductConversionMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Transactional
@Service
public class DamageEntryStock {
	private static final Logger logger = LoggerFactory.getLogger(DamageEntryStock.class);
	// ----------------version 5.0 surya
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	// ----------------version 5.0 surya end
	@Autowired
	private ItemBatchMstRepository itemBatchMstRepo;

	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	DamageHdrRepository damageHdrRepository;

	@Autowired
	DamageDtlRepository damageDtlRepository;

	@Autowired
	ItemMstRepository itemMstRepository;

	public void execute(String voucherNumber, Date voucherDate, String damageHdrId, CompanyMst companyMst,
			String branchCode) {

		Optional<DamageHdr> damageHdrOpt = damageHdrRepository.findById(damageHdrId);

		List<DamageDtl> damageDtlList = damageDtlRepository.findByDamageHdrIdAndCompanyMstId(damageHdrOpt.get().getId(),
				companyMst.getId());

		Iterator iter = damageDtlList.iterator();
		while (iter.hasNext()) {
			DamageDtl damageDtl = (DamageDtl) iter.next();

			Optional<ItemMst> itemopt = itemMstRepository.findById(damageDtl.getItemId());

			// find if batch mast has an entry
			List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
					.findByItemIdAndBatchAndBarcode(itemopt.get().getId(), MapleConstants.Nobatch, itemopt.get().getBarCode());

			if (itembatchmst.size() == 1) {
				ItemBatchMst itemBatchMst = itembatchmst.get(0);
				itemBatchMst.setMrp(itemopt.get().getStandardPrice());
				itemBatchMst.setQty(itemBatchMst.getQty() - damageDtl.getQty());
				itemBatchMstRepo.saveAndFlush(itemBatchMst);
			} else {
				ItemBatchMst itemBatchMst = new ItemBatchMst();
				itemBatchMst.setBarcode(itemopt.get().getBarCode());
				itemBatchMst.setBatch(MapleConstants.Nobatch);
				itemBatchMst.setMrp(itemopt.get().getStandardPrice());
				itemBatchMst.setQty(-1 * damageDtl.getQty());
				itemBatchMst.setItemId(itemopt.get().getId());
				itemBatchMst.setCompanyMst(companyMst);
				itemBatchMstRepo.saveAndFlush(itemBatchMst);

			}

			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
			final VoucherNumber voucherNo = voucherService.generateInvoice("STV", companyMst.getId());
			itemBatchDtl.setBarcode(itemopt.get().getBarCode());
			itemBatchDtl.setBatch(MapleConstants.Nobatch);

			itemBatchDtl.setItemId(itemopt.get().getId());
			itemBatchDtl.setMrp(itemopt.get().getStandardPrice());
			itemBatchDtl.setQtyIn(0.0);
			itemBatchDtl.setQtyOut(damageDtl.getQty());
			itemBatchDtl.setVoucherNumber(voucherNo.getCode());
			itemBatchDtl.setVoucherDate(damageDtl.getDamageHdr().getVoucherDate());
			itemBatchDtl.setBranchCode(branchCode);
			itemBatchDtl.setSourceVoucherNumber(damageDtl.getDamageHdr().getVoucheNumber());
			itemBatchDtl.setCompanyMst(companyMst);
			itemBatchDtl.setParticulars("DAMAGE ENTRY");
			itemBatchDtl = itemBatchDtlRepository.saveAndFlush(itemBatchDtl);

		}

	}

	public void executebatchwise(String voucherNumber, Date voucherDate, String damageHdrId, CompanyMst companyMst,
			String branchCode) {

		Optional<DamageHdr> damageHdrOpt = damageHdrRepository.findById(damageHdrId);

		List<DamageDtl> damageDtlList = damageDtlRepository.findByDamageHdrIdAndCompanyMstId(damageHdrOpt.get().getId(),
				companyMst.getId());

		Iterator iter = damageDtlList.iterator();
		while (iter.hasNext()) {
			DamageDtl damageDtl = (DamageDtl) iter.next();

			Optional<ItemMst> itemopt = itemMstRepository.findById(damageDtl.getItemId());
			// ==================common function for stock updation in itembatchmst and
			// itembatchdtl
			// ==========================by anandu===============25-06-2021==========
			Double qty = -1 * damageDtl.getQty();
			String processInstanceId = null;
			String taskId = null;
			Date expiry = null;

			itemBatchDtlService.itemBatchMstUpdation(itemopt.get().getBarCode(), damageDtlList.get(0).getBatchCode(),
					branchCode, expiry, itemopt.get().getId(), itemopt.get().getStandardPrice(), processInstanceId, qty,
					taskId, companyMst, damageDtl.getStore());

			final VoucherNumber voucherNo = voucherService.generateInvoice("STV", companyMst.getId());
			Double QtyIn = 0.0;

			itemBatchDtlService.itemBatchDtlInsertionNew(itemopt.get().getBarCode(),
					damageDtlList.get(0).getBatchCode(), branchCode, expiry, itemopt.get().getId(),
					itemopt.get().getStandardPrice(), "DAMAGE ENTRY", processInstanceId, QtyIn, damageDtl.getQty(),
					damageDtl.getId(), damageHdrId, voucherDate, voucherNumber, damageDtl.getStore(), taskId,
					voucherDate, voucherNo.getCode(), companyMst);
			// ----------------version 5.0 surya end

		}

	}

}