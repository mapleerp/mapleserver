package com.maple.restserver.service.task;

 
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;

 
@Component
public class KitStockUpdate    {
	 private static final Logger logger = LoggerFactory.getLogger(KitStockUpdate.class);

	 @Autowired
		private ItemBatchMstRepository itemBatchMstRepo;
		
	 @Autowired
		private PurchaseHdrRepository purchaseHdrRepo;
	 
	 @Autowired
		private PurchaseDtlRepository purchaseDtlRepo;
	 
	 
	 @Autowired
		private ItemBatchDtlRepository  itemBatchDtlRepository;
	 
	  @Autowired
	  private VoucherNumberService voucherService;
	 
	 
	public void execute(  String voucherNumber, Date voucherDate, String purchaseId)  {
	 	
		Optional<PurchaseHdr> purchaseHdr = purchaseHdrRepo.findById(purchaseId);
		
		//List<PurchaseHdr> puchaseList = purchaseHdrRepo.findByVoucherNumberAndVoucherDate(  voucherNumber,   voucherDate);
		//PurchaseHdr puchaseHdr = puchaseList.get(0);
		
		List<PurchaseDtl> puchaseDtlList = purchaseDtlRepo.findByPurchaseHdrId(purchaseHdr.get().getId());
		
		Iterator iter = puchaseDtlList.iterator();
		while (iter.hasNext()) {
			PurchaseDtl purchaseDtl = (PurchaseDtl) iter.next();
			
			
			// find if batch mast has an entry
			List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo.findByItemIdAndBatchAndBarcode(purchaseDtl.getItemId(),
					purchaseDtl.getBatch(),purchaseDtl.getBarcode() ) ;
			
		   if(itembatchmst.size()==1) {
				ItemBatchMst itemBatchMst =itembatchmst.get(0);
				itemBatchMst.setMrp(purchaseDtl.getMrp());
				itemBatchMst.setQty(purchaseDtl.getQty() + itemBatchMst.getQty());
				
				itemBatchMstRepo.saveAndFlush(itemBatchMst);
			}else {
				ItemBatchMst itemBatchMst = new ItemBatchMst();
				itemBatchMst.setBarcode(purchaseDtl.getBarcode());
				itemBatchMst.setBatch(purchaseDtl.getBatch());
				itemBatchMst.setMrp(purchaseDtl.getMrp());
				itemBatchMst.setQty(purchaseDtl.getQty());
				itemBatchMst.setItemId(purchaseDtl.getItemId());
				itemBatchMstRepo.saveAndFlush(itemBatchMst);
				
			}
			
			
			
			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
			 final VoucherNumber voucherNo = voucherService.generateInvoice("STV",purchaseHdr.get().getCompanyMst().getId());
			
			itemBatchDtl.setBarcode(purchaseDtl.getBarcode());
			itemBatchDtl.setBatch(purchaseDtl.getBatch());
			itemBatchDtl.setItemId(purchaseDtl.getItemId());
			itemBatchDtl.setMrp(purchaseDtl.getMrp());
			itemBatchDtl.setQtyIn(purchaseDtl.getQty());
			itemBatchDtl.setQtyOut(0.0);
			itemBatchDtl.setVoucherNumber(voucherNo.getCode());
			itemBatchDtl.setVoucherDate(purchaseHdr.get().getVoucherDate());
			itemBatchDtl.setItemId(purchaseDtl.getItemId());
			
			itemBatchDtl.setSourceVoucherNumber(purchaseHdr.get().getVoucherNumber());
			itemBatchDtl.setSourceVoucherDate(purchaseHdr.get().getVoucherDate());
			itemBatchDtl.setStore("MAIN");
			itemBatchDtl.setSourceParentId(purchaseId);
			itemBatchDtl.setSourceDtlId(purchaseDtl.getId());
			
			itemBatchDtlRepository.saveAndFlush(itemBatchDtl)	;
			
			
		}
		
		
		
	}

}
