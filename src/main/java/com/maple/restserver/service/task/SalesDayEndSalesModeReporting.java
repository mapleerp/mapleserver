package com.maple.restserver.service.task;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DailyCreditSales;
import com.maple.restserver.entity.DailySalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.repository.DailyCreditSalesRepository;
import com.maple.restserver.repository.DailySalesDtlRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;



@Transactional
@Service
public class SalesDayEndSalesModeReporting{
	@Autowired
	private SalesTransHdrRepository salesTransHdrRepo;


	@Autowired
	private DailyCreditSalesRepository dailyCreditSalesRepository;

	@Autowired
	private DailySalesDtlRepository dailySalesDtlRepository;
	

	public void execute(String voucherNumber,Date voucherDate,String salesTransHdrId,CompanyMst companyMst){
		
		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepo.findByVoucherNumberAndVoucherDate(voucherNumber,
				sqlDate);
		Iterator itr = salesTransHdrList.iterator();
 
		while (itr.hasNext()) {
			
			
			SalesTransHdr salesTransHdr = (SalesTransHdr) itr.next();
			DailyCreditSales dailyCreditSales = dailyCreditSalesRepository.findByReportDate(voucherDate);
			
			if(null==dailyCreditSales) {
				dailyCreditSales = new DailyCreditSales();
				dailyCreditSales.setCompanyMst(companyMst);
				
		 	} 
			
			if(salesTransHdr.getSalesMode().equalsIgnoreCase("UBER SALES")) {
				dailyCreditSales.setUberSales(salesTransHdr.getInvoiceAmount());
			}
			 
			if(salesTransHdr.getSalesMode().equalsIgnoreCase("SWIGGY")) {
				dailyCreditSales.setsWiggySales(salesTransHdr.getInvoiceAmount());
			}
				  
			if(salesTransHdr.getSalesMode().equalsIgnoreCase("ZOMATO")) {
				dailyCreditSales.setsWiggySales(salesTransHdr.getInvoiceAmount());
			}
			if(salesTransHdr.getSalesMode().equalsIgnoreCase("FOOD PANDA")) {
				dailyCreditSales.setFoodPandaSales(salesTransHdr.getInvoiceAmount());
			}
			if(salesTransHdr.getSalesMode().equalsIgnoreCase("ONLINE")) {
				dailyCreditSales.setOnlineSales(salesTransHdr.getInvoiceAmount());
			}
			 
			dailyCreditSalesRepository.save(dailyCreditSales);
			
			
			DailySalesDtl dailySalesDtl = dailySalesDtlRepository.findByReportDate(voucherDate);
			
			if(null==dailySalesDtl) {
				dailySalesDtl = new DailySalesDtl();
				dailySalesDtl.setCompanyMst(companyMst);
		 	} 
			
			if(null!=salesTransHdr.getCashPay() && salesTransHdr.getCashPay() >0) {
				dailySalesDtl.setCashSales(salesTransHdr.getCashPay());
				dailySalesDtlRepository.save(dailySalesDtl);
			}
			
			if(null!=salesTransHdr.getCardamount() && salesTransHdr.getCardamount()>0) {
				dailySalesDtl.setCreditCardSales(dailySalesDtl.getCreditCardSales());
				dailySalesDtlRepository.save(dailySalesDtl);
			}
			
			 

			if(null!=salesTransHdr.getCreditAmount() && salesTransHdr.getCreditAmount()>0) {
				dailySalesDtl.setCreditSales(salesTransHdr.getCreditAmount()); 
				dailySalesDtlRepository.save(dailySalesDtl);
			}
			if(null!=salesTransHdr.getSodexoAmount() && salesTransHdr.getSodexoAmount() > 0 ) {
				dailySalesDtl.setSodexoSales(salesTransHdr.getSodexoAmount()); 
				dailySalesDtlRepository.save(dailySalesDtl);
			
			}
			
			if(null!=salesTransHdr.getPaytmAmount() && salesTransHdr.getPaytmAmount()>0) {
				dailySalesDtl.setPaytmSales(salesTransHdr.getPaytmAmount());
				dailySalesDtlRepository.save(dailySalesDtl);
			}
		
		
		}
	}

}
