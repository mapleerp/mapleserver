package com.maple.restserver.service.task;

 


import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.IntentInDtl;
import com.maple.restserver.entity.IntentInHdr;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PurchaseOrderDtl;
import com.maple.restserver.entity.PurchaseOrderHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.IntentDtlRepository;
import com.maple.restserver.repository.IntentHdrRepository;
import com.maple.restserver.repository.IntentInDtlRepository;
import com.maple.restserver.repository.IntentInHdrRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PurchaseOrderDtlRepository;
import com.maple.restserver.repository.PurchaseOrderHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.MapleTask;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;



@Component
public class PoPreProcess    implements MapleTask{
	 private static final Logger logger = LoggerFactory.getLogger(PoPreProcess.class);

	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	 
	  @Autowired
	  private VoucherNumberService voucherService;
	 
	 
	  @Autowired
	  PurchaseOrderHdrRepository  purchaseOrderHdrRepository;

		 
	  
	  @Autowired
	  PurchaseOrderDtlRepository  purchaseOrderDtlRepository;

		 
	  @Autowired
	  IntentInHdrRepository   intentInHdrRepository ;

	  
	  
		 
	  @Autowired
	  IntentInDtlRepository  intentInDtlRepository;
	  

		 
	  @Autowired
	  CompanyMstRepository  companyMstRepository;


	  EventBus eventBus = EventBusFactory.getEventBus();
	  
	@Override
	public void execute(HashMap hashmap) {
		
		System.out.println("Inside Execute");
		
		String company = (String) hashmap.get("company");
		String branchcode = (String) hashmap.get("branchcode");
		String taskid = (String) hashmap.get("taskid");
		String processInstanceId = (String) hashmap.get("processinstanceid");
		
		String voucherNumber = (String) hashmap.get("voucherNumber");
		String voucherDate = (String) hashmap.get("voucherDate");
		
		 	PurchaseOrderHdr purchaseOrderHdr = new PurchaseOrderHdr();
		 	CompanyMst companyMst = companyMstRepository.findById(company).get();
		 	
		purchaseOrderHdr.setCompanyMst(companyMst);
		
		purchaseOrderHdr.setBranchCode(branchcode);
		
		purchaseOrderHdr.setTaskId(taskid);
		
		purchaseOrderHdr.setProcessInstanceId(processInstanceId);
		
		purchaseOrderHdr = purchaseOrderHdrRepository.save(purchaseOrderHdr);
	 	
		/*
		 * Fetch records from Intent Dtl and Hdr based on voucher number and voucher date 
		 */
		
		 //Get HDR Id of next Processs and set in the flow
		
		Date sqlDate = SystemSetting.StringToSqlDate(voucherDate, "dd/MM/yyyy");
		
		List<IntentInHdr> intentInHdrList = intentInHdrRepository.findByInVoucherNumberAndInVoucherDate(voucherNumber, sqlDate);
		
		if(intentInHdrList.size()>0) {
		 IntentInHdr intentInHdr = intentInHdrList.get(0);
		 
		 // Now Fetch Intent Dtl
		 List<IntentInDtl> intentDtlList  = intentInDtlRepository.findByIntentInHdrId(intentInHdr.getId());
		 
		 // Now loop through each record and save to PO
		 
		 for   (IntentInDtl intentDtl : intentDtlList) {
		 
			 
			 PurchaseOrderDtl purchaseOrderDtl = new PurchaseOrderDtl();
			 
			 purchaseOrderDtl.setQty(intentDtl.getQty());
			 purchaseOrderDtl.setPurchaseOrderHdr(purchaseOrderHdr);
			 purchaseOrderDtl.setItemId( intentDtl.getItemId());
			 purchaseOrderDtl.setItemPropertyAsJson(intentDtl.getItemPropertyAsJson());
			 purchaseOrderDtl.setCompanyMst(companyMst);
			 purchaseOrderDtlRepository.save(purchaseOrderDtl);
			 
		}
		 
		 
		 
			System.out.println("Inside Map");
			
			
			Map<String, Object> variables = new HashMap<String, Object>();
			variables.put("voucherNumber", purchaseOrderHdr.getId());
			variables.put("voucherDate",ClientSystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", purchaseOrderHdr.getId());		 
			variables.put("branchcode", purchaseOrderHdr.getBranchCode());
			variables.put("companyid", purchaseOrderHdr.getCompanyMst());
		 			variables.put("REST",1);	
		 			
		 			
		 			variables.put("WF", "forwardPurchaseOrder");
		 			
		 			
		 			String workflow = (String) variables.get("WF");
		 			  voucherNumber = (String) variables.get("voucherNumber");
		 			String sourceID = (String) variables.get("id");


		 			LmsQueueMst lmsQueueMst = new LmsQueueMst();

		 			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		 			
		 			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		 			
		 			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		 			java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
		 			lmsQueueMst.setVoucherDate(sqlVDate);
		 			
		 			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		 			lmsQueueMst.setVoucherType(workflow);
		 			lmsQueueMst.setPostedToServer("NO");
		 			lmsQueueMst.setJobClass("forwardPurchaseOrder");
		 			lmsQueueMst.setCronJob(true);
		 			lmsQueueMst.setJobName(workflow + sourceID);
		 			lmsQueueMst.setJobGroup(workflow);
		 			lmsQueueMst.setRepeatTime(60000L);
		 			lmsQueueMst.setSourceObjectId(sourceID);
		 			
		 			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		 			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		 			variables.put("lmsqid", lmsQueueMst.getId());
		 			eventBus.post(variables);

		 			 
					System.out.println("Inside eventBus posted");
			
		 
		}
 		//execution.setVariable("businessProcessId", purchaseOrderHdr.getId());		
		//Send Message Back to Server saying the PO Header Id. This is done as part of PO SAVE. 
	 
		
	}

}
