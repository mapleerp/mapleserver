package com.maple.restserver.service.task;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchExpiryDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductionBatchStockDtl;
import com.maple.restserver.repository.ActualProductionDtlRepository;
import com.maple.restserver.repository.ActualProductionHdrRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchExpiryDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProductionBatchStockDtlRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Transactional
@Service
@Component
public class ProductionFinishedGoodsStock {
	private static final Logger logger = LoggerFactory.getLogger(ProductionFinishedGoodsStock.class);
	@Autowired
	private ItemBatchExpiryDtlRepository itemBatchExpiryDtlRepo;
	@Autowired
	private ItemBatchMstRepository itemBatchMstRepo;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	// ----------------version 5.0 surya
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	// ----------------version 5.0 surya end
	@Autowired
	private SalesTransHdrRepository salesTransHdrRepo;
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	private SalesDetailsRepository salesDetailsRepo;

	@Autowired
	ProductionBatchStockDtlRepository productionBatchStockDtlRepository;
	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	ActualProductionHdrRepository actualProductionHdrRepository;

	@Autowired
	ActualProductionDtlRepository actualProductionDtlRepository;

	@Autowired
	ItemMstRepository itemMstRepository;

	public void execute(String voucherNumber, Date voucherDate, CompanyMst companyMst, String actualProductionHdrId) {

		Optional<ActualProductionHdr> actualProductionHdrOpt = actualProductionHdrRepository
				.findById(actualProductionHdrId);

		List<ActualProductionDtl> actualProductionDtlList = actualProductionDtlRepository
				.findByActualProductionHdrId(actualProductionHdrOpt.get().getId());

		Iterator iter = actualProductionDtlList.iterator();
		while (iter.hasNext()) {
			ActualProductionDtl actualProductionDtl = (ActualProductionDtl) iter.next();

			Optional<ItemMst> itemopt = itemMstRepository.findById(actualProductionDtl.getItemId());

			List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo.findByItemIdAndBatchAndBarcode(
					actualProductionDtl.getItemId(), MapleConstants.Nobatch, itemopt.get().getBarCode());

			if (itembatchmst.size() == 1) {
				ItemBatchMst itemBatchMst = itembatchmst.get(0);
				itemBatchMst.setMrp(itemopt.get().getStandardPrice());
				itemBatchMst.setQty(itemBatchMst.getQty() + actualProductionDtl.getActualQty());

				itemBatchMstRepo.saveAndFlush(itemBatchMst);
			} else {
				ItemBatchMst itemBatchMst = new ItemBatchMst();
				itemBatchMst.setBarcode(itemopt.get().getBarCode());
				itemBatchMst.setBatch(MapleConstants.Nobatch);
				itemBatchMst.setMrp(itemopt.get().getStandardPrice());
				itemBatchMst.setQty(actualProductionDtl.getActualQty());
				itemBatchMst.setItemId(actualProductionDtl.getItemId());
				itemBatchMst.setBranchCode(actualProductionHdrOpt.get().getBranchCode());

				itemBatchMst.setCompanyMst(companyMst);
				itemBatchMstRepo.saveAndFlush(itemBatchMst);

			}

//----------------version 5.0 surya 

			itemBatchDtlService.updateProductionStock(itemopt.get(), actualProductionDtl, companyMst,
					actualProductionHdrId, actualProductionHdrOpt.get());
			// ----------------version 5.0 surya end

		}

	}

	public void executebatchwisestockupdate(String voucherNumber, Date voucherDate, CompanyMst companyMst,
			String actualProductionHdrId) {

		Optional<ActualProductionHdr> actualProductionHdrOpt = actualProductionHdrRepository
				.findById(actualProductionHdrId);

		List<ActualProductionDtl> actualProductionDtlList = actualProductionDtlRepository
				.findByActualProductionHdrId(actualProductionHdrOpt.get().getId());

		Iterator iter = actualProductionDtlList.iterator();
		while (iter.hasNext()) {
			ActualProductionDtl actualProductionDtl = (ActualProductionDtl) iter.next();

			Optional<ItemMst> itemopt = itemMstRepository.findById(actualProductionDtl.getItemId());

			List<ItemBatchExpiryDtl> items = itemBatchExpiryDtlRepo.findByCompanyMstAndItemIdAndBatch(
					actualProductionDtl.getCompanyMst(), actualProductionDtl.getItemId(),
					actualProductionDtl.getBatch());

			Date Expiry = null;
			if (!items.isEmpty()) {
				Expiry = items.get(0).getExpiryDate();
			}

			String batch = null;

			if (actualProductionDtl.getBatch() != null) {
				batch = actualProductionDtl.getBatch();
			} else {
				batch = MapleConstants.Nobatch;
			}

			String store = MapleConstants.Store;
			
			
			
			if(null != actualProductionDtl.getStore())
			{
				store = actualProductionDtl.getStore();
			}

			String processInstanceId = null;
			String taskId = null;
			Double QtyIn = 0.0;

			itemBatchDtlService.itemBatchMstUpdation(itemopt.get().getBarCode(), batch,
					actualProductionHdrOpt.get().getBranchCode(), Expiry, actualProductionDtl.getItemId(),
					itemopt.get().getStandardPrice(), processInstanceId, actualProductionDtl.getActualQty(), taskId,
					companyMst, store);
//----------------version 5.0 surya 

//		   itemBatchDtlService.updateProductionStock(itemopt.get(),actualProductionDtl,companyMst,actualProductionHdrId,actualProductionHdrOpt.get());
			final VoucherNumber voucherNo = voucherService.generateInvoice("STV", companyMst.getId());

			itemBatchDtlService.itemBatchDtlInsertionNew(itemopt.get().getBarCode(), batch,
					actualProductionHdrOpt.get().getBranchCode(), Expiry, actualProductionDtl.getItemId(),
					itemopt.get().getStandardPrice(), "ACTUAL PRODUCTION STOCK UPDATE", processInstanceId, actualProductionDtl.getActualQty(),
					0.0, actualProductionDtl.getId(), actualProductionHdrId, voucherDate,
					voucherNumber, store, taskId, voucherDate, voucherNo.getCode(), companyMst);

		}

	}

}
