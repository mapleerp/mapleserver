package com.maple.restserver.service.task;


import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DailySalesSummary;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.repository.DailySalesSummaryRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;



@Transactional
@Service
public class SalesDayEndReporting {
	@Autowired
	private SalesTransHdrRepository salesTransHdrRepo;


	@Autowired
	private DailySalesSummaryRepository dailySalesSummaryRepo;

	
	
	public void execute(String voucherNumber,Date voucherDate,CompanyMst companyMst,String salesTransHdrId){
		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepo.findByVoucherNumberAndVoucherDate(voucherNumber,
				sqlDate);
		Iterator itr = salesTransHdrList.iterator();
 
		while (itr.hasNext()) {
			
			
			SalesTransHdr salesTransHdr = (SalesTransHdr) itr.next();
			
			DailySalesSummary dailySalesSummary = dailySalesSummaryRepo.findByRecordDate(voucherDate);
			if(null==dailySalesSummary || null==dailySalesSummary.getId()) {
				// This is the firstInvoice for the date.
				  dailySalesSummary = new DailySalesSummary();
				  dailySalesSummary.setOpeningBillNo(salesTransHdr.getVoucherNumber());
				  dailySalesSummary.setTotalBillNo(1);
				  dailySalesSummary.setClosingBillNo(salesTransHdr.getVoucherNumber());
				  
				  dailySalesSummary.setCompanyMst(companyMst);
				  
				  if(null!=salesTransHdr.getCashPay() && salesTransHdr.getCashPay() > 0) {
					  dailySalesSummary.setCashSale(salesTransHdr.getCashPay());
				  }else {
					  dailySalesSummary.setCashReceived(0.0);
				  }
				  
				  if(null!=salesTransHdr.getCardamount() && salesTransHdr.getCardamount() > 0) {
					  dailySalesSummary.setCardSale(salesTransHdr.getCardamount());
				  }else {
					  dailySalesSummary.setCardSale(0.0);
				  }
				  
				  if(null!=salesTransHdr.getSodexoAmount() && salesTransHdr.getSodexoAmount() > 0) {
					  dailySalesSummary.setSodexo(salesTransHdr.getSodexoAmount());
				  }else {
					  dailySalesSummary.setSodexo(0.0); 
				  }
				  
				  if(null!=salesTransHdr.getPaytmAmount() && salesTransHdr.getPaytmAmount() > 0) {
					  dailySalesSummary.setPaytm(salesTransHdr.getPaytmAmount());
				  }else {
					  dailySalesSummary.setPaytm(0.0); 
				  }
				  
				  
				  dailySalesSummary.setRecordDate(voucherDate);
				  dailySalesSummary.setBranchCode(salesTransHdr.getBranchCode());
				  
				  
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("CREDIT")){
					  dailySalesSummary.setCredit(salesTransHdr.getInvoiceAmount());
				  }else {
					  dailySalesSummary.setCredit(0.0);
				  }
				  
				  
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("B2B")){
					  dailySalesSummary.setB2BSales(salesTransHdr.getInvoiceAmount());
				  }else {
					  dailySalesSummary.setB2BSales(0.0);
				  }
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("UBER SALES")){
					  dailySalesSummary.setUberSale(salesTransHdr.getInvoiceAmount());
				  }else {
					  dailySalesSummary.setUberSale(0.0);
				  }
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("SWIGGY")){
					  dailySalesSummary.setSwiggy(salesTransHdr.getInvoiceAmount());
					  
				  }else {
					  dailySalesSummary.setSwiggy(0.0);
				  }
				  
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("ZOMATO")){
					  dailySalesSummary.setZomoto(salesTransHdr.getInvoiceAmount());
					  
				  }else {
					  dailySalesSummary.setZomoto(0.0);
				  }
				  
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("FOOD PANDA")){
					  dailySalesSummary.setFoodPanda(salesTransHdr.getInvoiceAmount());
					  
				  }else {
					  dailySalesSummary.setFoodPanda(0.0);
				  }
				  
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("ONLINE")){
					  dailySalesSummary.setOnlineSalesOrder(salesTransHdr.getInvoiceAmount());
					  
				  }else {
					  dailySalesSummary.setOnlineSalesOrder(0.0);
				  }
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("POS")){
					  dailySalesSummary.setCashReceived(salesTransHdr.getInvoiceAmount());
					  
				  }else {
					  dailySalesSummary.setCashReceived(0.0);
				  }
				  
				  
				  
				  dailySalesSummaryRepo.save(dailySalesSummary);
				  
				  
				
			}else {
				
				if(null==dailySalesSummary.getOpeningBillNo()) {
					// this is a scenario where sales Order was saved first before any invoice.
					
					
					  dailySalesSummary.setOpeningBillNo(salesTransHdr.getVoucherNumber());
					  dailySalesSummary.setTotalBillNo(1);
					  dailySalesSummary.setClosingBillNo(salesTransHdr.getVoucherNumber());
					  
					  dailySalesSummary.setCompanyMst(companyMst);
					  
					  if(null==salesTransHdr.getCashPay()  ) {
						 
						  dailySalesSummary.setCashReceived(0.0);
					  }
					  
					  if(null==salesTransHdr.getCardamount()  ) {
					 
						  dailySalesSummary.setCardSale(0.0);
					  }
					  
					  if(null==salesTransHdr.getSodexoAmount() ) {
					   
						  dailySalesSummary.setSodexo(0.0); 
					  }
					  
					  if(null==salesTransHdr.getPaytmAmount()  ) {
					 
						  dailySalesSummary.setPaytm(0.0); 
					  }
					  
					  
					  dailySalesSummary.setRecordDate(voucherDate);
					  dailySalesSummary.setBranchCode(salesTransHdr.getBranchCode());
								
				}
				  
				  
				  dailySalesSummary.setTotalBillNo(dailySalesSummary.getTotalBillNo()+1);
				  dailySalesSummary.setClosingBillNo(salesTransHdr.getVoucherNumber());
				  
				 
				  if(null!=salesTransHdr.getCashPay() && salesTransHdr.getCashPay() > 0) {
					  dailySalesSummary.setCashSale(dailySalesSummary.getCashSale() == null ?  salesTransHdr.getCashPay() : dailySalesSummary.getCashSale() + salesTransHdr.getCashPay());
				  } 
				  if(null!=salesTransHdr.getCardamount() && salesTransHdr.getCardamount() > 0) {
					  dailySalesSummary.setCardSale( dailySalesSummary.getCardSale() == null ? salesTransHdr.getCardamount() :  dailySalesSummary.getCardSale()  + salesTransHdr.getCardamount());
				  } 
				  
				  if(null!=salesTransHdr.getSodexoAmount() && salesTransHdr.getSodexoAmount() > 0) {
					  dailySalesSummary.setSodexo( dailySalesSummary.getSodexo() == null ? salesTransHdr.getSodexoAmount() : dailySalesSummary.getSodexo() + salesTransHdr.getSodexoAmount());
				  } 
				  
				  if(null!=salesTransHdr.getPaytmAmount() && salesTransHdr.getPaytmAmount() > 0) {
					  dailySalesSummary.setPaytm( dailySalesSummary.getPaytm() == null ? salesTransHdr.getPaytmAmount() : dailySalesSummary.getPaytm()  + salesTransHdr.getPaytmAmount());
				  } 
				  
				
				  
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("CREDIT")){
					  dailySalesSummary.setCredit( dailySalesSummary.getCredit()  == null ?  salesTransHdr.getInvoiceAmount() : dailySalesSummary.getCredit() + salesTransHdr.getInvoiceAmount());
				  } 
				  
				  
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("B2B")){
					  dailySalesSummary.setB2BSales(dailySalesSummary.getB2BSales()  == null ?  salesTransHdr.getInvoiceAmount() : dailySalesSummary.getB2BSales()  + salesTransHdr.getInvoiceAmount());
				  } 
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("UBER SALES")){
					  dailySalesSummary.setUberSale( dailySalesSummary.getUberSale()  == null ? salesTransHdr.getInvoiceAmount() : dailySalesSummary.getUberSale() + salesTransHdr.getInvoiceAmount());
				  } 
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("SWIGGY")){
					  dailySalesSummary.setSwiggy(dailySalesSummary.getSwiggy()  == null ? salesTransHdr.getInvoiceAmount() : dailySalesSummary.getSwiggy()   + salesTransHdr.getInvoiceAmount());
					  
				  } 
				  
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("ZOMATO")){
					  dailySalesSummary.setZomoto( dailySalesSummary.getZomoto()  == null ? salesTransHdr.getInvoiceAmount() :  dailySalesSummary.getZomoto()  + salesTransHdr.getInvoiceAmount());
					  
				  } 
				  
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("FOOD PANDA")){
					  dailySalesSummary.setFoodPanda(dailySalesSummary.getFoodPanda()  == null ? salesTransHdr.getInvoiceAmount() : dailySalesSummary.getFoodPanda()  + salesTransHdr.getInvoiceAmount());
					  
				  } 
				  
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("ONLINE")){
					  dailySalesSummary.setOnlineSalesOrder( dailySalesSummary.getOnlineSalesOrder()  == null ?  salesTransHdr.getInvoiceAmount()  : dailySalesSummary.getOnlineSalesOrder()  + salesTransHdr.getInvoiceAmount());
					  
				  } 
				  
				  if(salesTransHdr.getSalesMode().equalsIgnoreCase("POS")){
					  
					  dailySalesSummary.setCashReceived( dailySalesSummary.getCashReceived()  == null ?  salesTransHdr.getInvoiceAmount()  : dailySalesSummary.getCashReceived()  + salesTransHdr.getInvoiceAmount());
  
				  }
				  
				  
				  dailySalesSummaryRepo.save(dailySalesSummary);
			}
			
		}
		
	}

}
