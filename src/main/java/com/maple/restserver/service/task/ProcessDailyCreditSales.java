package com.maple.restserver.service.task;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.DailyCreditSales;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.repository.DailyCreditSalesRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;




@Transactional
@Service
public class ProcessDailyCreditSales     {

	
	@Autowired
	private SalesTransHdrRepository salesTransHdrRepo;

	
	@Autowired
	private SalesDetailsRepository salesDtlRepo;
	
	@Autowired
	DailyCreditSalesRepository dailyCreditSalesRepository;
	
	
	
	 
	public void execute( String voucherNumber ,Date voucherDate   )   {
	 	
		
		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());
		
		
		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepo.findByVoucherNumberAndVoucherDate(voucherNumber,
				sqlDate);
		
		Iterator itr = salesTransHdrList.iterator();

		while (itr.hasNext()) {
			SalesTransHdr salesTransHdr = (SalesTransHdr) itr.next();
			/*
			 * 	saletype.getItems().add("UBER SALES");
		saletype.getItems().add("SWIGGY");
		saletype.getItems().add("ZOMATO");
		saletype.getItems().add("FOOD PANDA");
		saletype.getItems().add("ONLINE");
			 */
			
			DailyCreditSales creditSales =dailyCreditSalesRepository.findByReportDate(sqlDate);
			
			if(null==creditSales) {
				creditSales = new DailyCreditSales();
				creditSales.setUberSales(0.0);
				creditSales.setFoodPandaSales(0.0);
				creditSales.setB2BSales(0.0);
				creditSales.setsWiggySales(0.0);
				creditSales.setZomatoSales(0.0);
				creditSales.setReportDate(sqlDate);
			}
			if(salesTransHdr.getSalesMode().equalsIgnoreCase("UBER SALES")) {
				BigDecimal totalDebit = salesDtlRepo.findSumDrAmountSalesDtl(salesTransHdr.getId());
				creditSales.setUberSales(creditSales.getUberSales()+totalDebit.doubleValue());
				
				dailyCreditSalesRepository.save(creditSales);
				
			}
			if(salesTransHdr.getSalesMode().equalsIgnoreCase("SWIGGY")) {
				BigDecimal totalDebit = salesDtlRepo.findSumDrAmountSalesDtl(salesTransHdr.getId());
				creditSales.setsWiggySales(creditSales.getsWiggySales()+totalDebit.doubleValue());
				
				dailyCreditSalesRepository.save(creditSales);
				
			}
			if(salesTransHdr.getSalesMode().equalsIgnoreCase("ZOMATO")) {
				BigDecimal totalDebit = salesDtlRepo.findSumDrAmountSalesDtl(salesTransHdr.getId());
				creditSales.setZomatoSales(creditSales.getZomatoSales()+totalDebit.doubleValue());
				
				dailyCreditSalesRepository.save(creditSales);
				
			}
			if(salesTransHdr.getSalesMode().equalsIgnoreCase("FOOD PANDA")) {
				BigDecimal totalDebit = salesDtlRepo.findSumDrAmountSalesDtl(salesTransHdr.getId());
				creditSales.setFoodPandaSales(creditSales.getFoodPandaSales()+totalDebit.doubleValue());
				
				dailyCreditSalesRepository.save(creditSales);
				
			}
			
		}
		
	}

}
