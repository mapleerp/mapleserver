package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.report.entity.PaymentVoucher;

@Service
@Transactional
public interface PaymentHdrService {
	
	public  List<Object> getDailyPayment(String voucherNumber, Date voucherDate);
	public List<PaymentVoucher> getPaymentHdr( );
	


}
