package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.report.entity.PharmacyPhysicalStockVarianceReport;

@Service
public interface PhysicalStockService {
	
 
	ArrayList<ItemMst> updatePhyisicalStock(String barcode, String itemName, double qty, double mrp, String batchCode,
			Date expiryDate,String companymstid,String branchCode , String voucherNumber , Date VoucherDate, String store );

	List<PharmacyPhysicalStockVarianceReport> findPhysicalStockVarianceReport(Date fromDate, Date toDate);
	
	 
}
