package com.maple.restserver.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.LastBestDateRepository;

@Service
@Transactional
public class SetUpSynchronisationServiceImpl implements SetUpSynchronisationService {
	
	
	@Autowired
	DayEndClosureRepository dayEndClosureRepository;
	
	@Autowired
	LastBestDateRepository lastBestDateRepository;
	
	@Autowired

	ExternalApi externalApi;

	@Autowired

	CompanyMstRepository companyMstRepo;
	
	@Autowired

	BranchMstRepository branchMstRepository;
	
	
	@Override
	public String verificationAndsynchronization(String companymstid,String branchcode ) {
	
		
		System.out.println("Starting  Verification.................");

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);

		if (!companyMstOpt.isPresent()) {
			return "failed";
		}

		CompanyMst companyMst = companyMstOpt.get();

		List<BranchMst> branchMst = branchMstRepository.findByBranchCode(branchcode);
         
		
		getAllDateWithDayEndDate(companymstid, branchcode);
		
		
		
		
		
		return "Verification And Synchronisation successfully completed............";
	}
	
	
	
	
	
	
	@Override 
	public String getCloudCount() {
		return null;
		
	}
	
	@Override
	public String getAllDateWithDayEndDate(String companymstid,String branchcode ) {
		
		
		Date salesBestDate = null;

		Date purchaseBestDate = null;

		Date stockBestDate = null;

		List<DayEndClosureHdr> sysdate = dayEndClosureRepository.getMaxOfDayEnd(companymstid, branchcode);

		Date saleDate = lastBestDateRepository.findBestDateByActivityType("SALES");

		if (saleDate != null) {

			salesBestDate = new Date(saleDate.getTime() + (1000 * 60 * 60 * 24));

		} else {

			salesBestDate = sysdate.get(0).getProcessDate();

		}

		Date purchaseDate = lastBestDateRepository.findBestDateByActivityType("PURCHASE");

		if (purchaseDate != null) {

			purchaseBestDate = purchaseDate;

			purchaseBestDate = new Date(purchaseBestDate.getTime() + (1000 * 60 * 60 * 24));

		} else {

			purchaseBestDate = sysdate.get(0).getProcessDate();

		}

		Date stockDate = lastBestDateRepository.findBestDateByActivityType("STOCKTRANSFER");

		if (stockDate != null) {

			stockBestDate = stockDate;

			stockBestDate = new Date(stockBestDate.getTime() + (1000 * 60 * 60 * 24));

		} else {

			stockBestDate = sysdate.get(0).getProcessDate();

		}	
		
		
		
		
		
		
		
		return "Date function successfully saved!";
		
	}

}
