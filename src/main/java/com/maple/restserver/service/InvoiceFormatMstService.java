package com.maple.restserver.service;

import java.util.List;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.InvoiceFormatMst;

public interface InvoiceFormatMstService {


	List<InvoiceFormatMst> findByCompanyMstAndPriceTypeIdAndGstAndDiscount(CompanyMst companyMst, String pricetypeid,
			String gst, String discount, String batch);

}
