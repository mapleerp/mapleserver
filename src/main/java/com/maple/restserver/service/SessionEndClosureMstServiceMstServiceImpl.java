package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.entity.SessionEndClosureMst;
import com.maple.restserver.repository.SessionEndClosureMstRepository;

@Component
@Service
public class SessionEndClosureMstServiceMstServiceImpl implements SessionEndClosureMstService {

	
	@Autowired
	SessionEndClosureMstRepository sessionendclosuremstrepo;
	
	@Override
	public SessionEndClosureMst retrieveSessionEndClosure(Date date, String userId) {

		
		
		  SessionEndClosureMst sesssionEndClosureHdr =
		  sessionendclosuremstrepo.getSessionEndClosureMst(date,userId);
		  if(sesssionEndClosureHdr!=null) {
		  
		  
		  List<Object>
		  objList=sessionendclosuremstrepo.getSessionEndDetails(date,userId);
		  
		  Object[] objAray = (Object[]) objList.get(0);
		  
		  
		  sesssionEndClosureHdr.setCardSale((Double) objAray[0]);
		  
		  sesssionEndClosureHdr.setCashSale((Double) objAray[1]);
		  sesssionEndClosureHdr.setCardSale2((Double) objAray[2]);
		  
		  } 
		  
		  
		  return sesssionEndClosureHdr;
		 
		
		
		

	}

}
