package com.maple.restserver.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.StockReport;
import com.maple.restserver.repository.ItemBatchMstRepository;

@Service
@Transactional
public class BatchStockReportServiceImpl implements BatchStockReportService{

	@Autowired
	ItemBatchMstRepository itemBatchMstRepository;
	@Override
	public List<StockReport> getStockByBranchCodeAndDateAndCompanyMstIdAndBatch(String branchcode, String companymstid,
			Date fromdate, String categoryid) {
		   List< StockReport> stockReportList =   new ArrayList();
			

			 List<Object> obj =   itemBatchMstRepository.findByBranchCodeAndDateAndCompanyMstIdAndBatch(branchcode, companymstid, fromdate, categoryid);
		 
			 for(int i=0;i<obj.size();i++)
			 {
				 Object[] objAray = (Object[]) obj.get(i);
				 StockReport stockReport=new StockReport();
				 stockReport.setItemName((String) objAray[0]);
				 stockReport.setQty((Double) objAray[1]);
				 stockReport.setStandardPrice((Double) objAray[2]);
				 stockReport.setBranchName((String) objAray[3]);
				 stockReport.setCategory((String) objAray[4]);
//				 stockReport.setBatch((String) objAray[5]);
				 stockReportList.add(stockReport);
		
			 }
			 
			return stockReportList;
			 
		}
	

}
