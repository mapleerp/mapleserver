package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ReorderMst;
import com.maple.restserver.entity.StockMovementView;
import com.maple.restserver.report.entity.StockReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.ReorderMstRepository;
import com.maple.restserver.utils.SystemSetting;


@Service
@Transactional



@Component
public class StockServiceImpl implements StockService {

	@Autowired
	ItemBatchMstRepository itemBatchMstRepo;

	
	@Autowired
	StockReportService stockReportService;
	
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	ItemMstRepository itemMstRepo;
	
	
	@Autowired
	private ReorderMstRepository reordermstRepo;
	
	@Autowired
	private CompanyMstRepository companyMstRepository;

	

	@Override
	public List<ItemBatchMst> getAllStock() {

		return itemBatchMstRepo.findAll();
	}

	@Override
	public List<ReorderMst> getAllStockLessThanMinQty(String companymstid,String branchCode) {
 
		 
		//itemBatchMstRepo.findAll();
		
		// Loop through each item
		
		// get min qty and max qty of each item usiong repo reordermstRepo
		// if stock qty from itemBatchMstRepo is less than min qty from reordermstRepo then 
		// insert to result array
		
		
		//return itemBatchMstRepo.findAll();
		Date today = SystemSetting.getSystemDate();
		java.sql.Date sqDate = SystemSetting.UtilDateToSQLDate(today);

		ArrayList<ReorderMst> itemBatchMstNew = new ArrayList<ReorderMst>();
		
		List<StockReport> stkReport = stockReportService.getDailyStockReport(branchCode, companymstid, sqDate);
		//List<ItemBatchMst> itemBatchMsts = itemBatchMstRepo.findByCompanyMstId(companymstid);
		
		for (StockReport item : stkReport) {
			
			System.out.println(item.getItemName());
			ItemMst itemMst = itemMstRepo.findByItemName(item.getItemName());
			CompanyMst companyMst = companyMstRepository.findById(companymstid).get();
			List<ReorderMst> reorderMst = reordermstRepo.findSearchbydate(itemMst.getId(),sqDate,companyMst);
			for (ReorderMst reorder : reorderMst) {

				if (item.getQty() < reorder.getMinQty()) {
					itemBatchMstNew.add(reorder);

				}
			}

		}
		
		boolean flag = true;

		List<ReorderMst> reorderMstAllList = reordermstRepo.findAll();
		for(ReorderMst reorder :reorderMstAllList)
		{
			for (StockReport item : stkReport) {
				ItemMst itemMst = itemMstRepo.findByItemName(item.getItemName());
				if(reorder.getItemId().equalsIgnoreCase(itemMst.getId()))
				{
					flag =true;
					break;
				}
				else
				{
					flag =false;
				}
			}
			if(!flag)
			{
				itemBatchMstNew.add(reorder);
			}
		}
			
		return itemBatchMstNew;
	}

	@Override
	public Double getStockOfItem(String companymstid, String itemId) {
		Double qty = itemBatchDtlRepository.getItemStock(itemId);
		if(qty == null)
			qty = 0.0;
		return qty;
	}
	@Override
	public List<StockMovementView> getAllItemMovement(String companymstid, java.sql.Date startDate, java.sql.Date EndDate, String branchcode) {
		 
		
		ArrayList<StockMovementView> listStockMovementView = new ArrayList();
		
		ArrayList<Object> listObject = itemBatchDtlRepository.findByCompanyMstAndVoucherDateBetWeenDate(startDate,EndDate, branchcode);
		 for(int i=0;i<listObject.size();i++)
		 {
			 Object[] objAray = (Object[]) listObject.get(i);
			 //sum(d.qtyIn), sum(d.qtyOut) , i.item_name, b.branch_code
			 
			 StockMovementView stockMovementView =new StockMovementView();
			
			
			 stockMovementView.setQtyIn((Double) objAray[0]);
			 stockMovementView.setQtyOut((Double) objAray[1]);
	
			 
			 stockMovementView.setItemName((String) objAray[2]);
			 stockMovementView.setBranchCode((String) objAray[3]);
			 
			 
			 
			 listStockMovementView.add(stockMovementView);
		
		 }
		 return listStockMovementView;
	}
}
