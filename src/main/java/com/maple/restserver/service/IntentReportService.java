package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.IntentVoucherReport;

@Service

@Component
public interface IntentReportService {

	
public	 List<IntentVoucherReport> 	getIntentVoucherReport(String voucherNumber, Date date,String companymstid,String branchcode );
}
