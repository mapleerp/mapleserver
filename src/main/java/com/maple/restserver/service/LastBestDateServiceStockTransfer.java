package com.maple.restserver.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.entity.LastBestDate;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.LastBestDateRepository;

@Service
public class LastBestDateServiceStockTransfer extends LastBestDateService {

//=======================MAP-82-count-verification-and-retry-of-bulk-publishing-in-stock-transfer==========anandu=====	

	@Autowired
	LastBestDateRepository lastBestDateRepository;
	@Autowired
	DayEndClosureRepository dayEndClosureRepository;

	@Override
	public LastBestDate findLastBestDate() {

		LastBestDate lastbestdate = lastBestDateRepository.findByActivityType("STOCKTRANSFER");

		List<DayEndClosureHdr> dayenddate = dayEndClosureRepository.getMaxOfDayEnd(super.getCompanymstid(),
				super.getBranchcode());

		Date stockTransferDayEnd = dayenddate.get(0).getProcessDate();
		lastbestdate.setDayEndDate(stockTransferDayEnd);
		
		Date date = new Date(lastbestdate.getLastSuccessDate().getTime()+(1000*60*60*24));
		lastbestdate.setLastSuccessDate(date);

		return lastbestdate;

	}

}
