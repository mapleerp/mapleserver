package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.SalesAndStockConsolidatedReport;

@Service
@Transactional
public interface SalesAndStockConsolidatedReportService {

	List<SalesAndStockConsolidatedReport> getSalesAndStockConsolidatedReport(Date fudate, Date tudate,
			String frombranch, String companymstid, String[] itemArray);

}
