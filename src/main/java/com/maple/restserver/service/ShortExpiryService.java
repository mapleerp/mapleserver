package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ShortExpiryReport;

@Component
public interface ShortExpiryService {

	
	List<ShortExpiryReport>getShortExpiry(CompanyMst companyMst,String branchcode, java.sql.Date currentDate);
	
	List<ShortExpiryReport>getCatogarywiseShortExpiry(CompanyMst companyMst,String branchcode,String [] catogarylist);

List<ShortExpiryReport>getItemWiseShortExpiry(CompanyMst companyMst,String branchcode,String []  itemlist);
}
