package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.RawMaterialIssueHdr;
import com.maple.restserver.report.entity.RawMaterialIssueReport;

public interface RawMaterialIssueService {
	List<RawMaterialIssueReport> findByRawMaterialVoucher(String voucherNumber, Date vDate,String companyid);

	List<RawMaterialIssueReport> rawMaterialIssueReportbetweenDate(String companymstid, String branchcode, Date fDate,
			Date tDate);
	
}
