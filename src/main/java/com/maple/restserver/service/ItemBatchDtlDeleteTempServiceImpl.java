package com.maple.restserver.service;

import java.sql.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtlDaily;
import com.maple.restserver.entity.ItemBatchDtlDeleteTemp;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.ItemBatchDtlDailyRepository;
import com.maple.restserver.repository.ItemBatchDtlDeleteTempRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;

@Service
@Transactional
@Component
public class ItemBatchDtlDeleteTempServiceImpl implements ItemBatchDtlDeleteTempService {

	@Autowired
	BranchMstRepository branchMstRepository;
	@Autowired
	ItemBatchDtlDeleteTempRepository itemBatchDtlDeleteTempRepo;
	
	@Autowired
	ItemBatchDtlDailyRepository itemBatchDtlDailyRepository;
	
	@Autowired
	ItemBatchMstRepository itemBatchMstRepository;
	@Override
	public void updateItemBatchDtlDeleteTempFromItemBatchDtl(CompanyMst companyMst, Date curDate) {


		
		itemBatchDtlDeleteTempRepo.updateDailyStockFromDtlStep0();
		BranchMst branchMst = branchMstRepository.getMyBranch();
		itemBatchDtlDailyRepository.updateDailyStockFromDtlStep1(branchMst.getBranchCode());

		// itemBatchDtlRepo.

		// itemBatchMstRepo.updateStockFromDtlStep2withDate(curDate) ;
		List<Object> listObject = itemBatchMstRepository.getClosingStockAsOnDate(curDate);
		System.out.println("----------itemBatchMstRepository size----------"+listObject.size());
		boolean flag = false;
		for (int i = 0; i < listObject.size(); i++) {

			Object[] objAray = (Object[]) listObject.get(i);
			ItemBatchDtlDeleteTemp itemBatchDtlDeleteTemp = new ItemBatchDtlDeleteTemp();
			itemBatchDtlDeleteTemp.setVoucherDate(curDate);
			itemBatchDtlDeleteTemp.setBarcode(objAray[0] == null ? "" : (String) objAray[0]);

			itemBatchDtlDeleteTemp.setBatch(objAray[1] == null ? "" : (String) objAray[1]);
			itemBatchDtlDeleteTemp.setBranchCode(objAray[2] == null ? "" : (String) objAray[2]);

			// itemBatchMst.setExpDate(objAray[3] == null ? null : (Date) objAray[3]);
			itemBatchDtlDeleteTemp.setId(objAray[3] == null ? "" : (String) objAray[3]);
			itemBatchDtlDeleteTemp.setItemId(objAray[4] == null ? "" : (String) objAray[4]);
			itemBatchDtlDeleteTemp.setQtyOut(0.0);

			itemBatchDtlDeleteTemp.setQtyIn(objAray[5] == null ? 0 : (double) objAray[5]);
			itemBatchDtlDeleteTemp.setCompanyMst(companyMst);
			
			itemBatchDtlDeleteTemp.setStore("MAIN");

			itemBatchDtlDeleteTempRepo.save(itemBatchDtlDeleteTemp);

		}

		// Update item batch mst with kit
//		updateItemBatchDtlDailyWithKit();
		itemBatchDtlDeleteTempRepo.updateDailyStockFromDtlStep3();

		//itemBatchDtlDailyRepository.correctNegativStock();

		// itemBatchMstRepo.updateStockFromDtlStep4();

		return;
	
		
	}

}
