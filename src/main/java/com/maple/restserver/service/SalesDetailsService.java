package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.report.entity.HsnCodeSaleReport;
import com.maple.restserver.report.entity.InsuranceCompanySalesSummaryReport;
import com.maple.restserver.report.entity.RetailSalesDetailReport;
import com.maple.restserver.report.entity.RetailSalesSummaryReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.SalesReport;

@Service
@Transactional
public interface SalesDetailsService {

	Double getSalesDetailItemQty(String companyMstid ,String salesTransHdrID, String itemId, String batch);

	List<SalesDtl> findBySalesTransHdrAndCategoryIdd(String salestranshdrId, String categoryid);

	List<HsnCodeSaleReport> getHSNCodeWiseSalesDtl(Date fdate,Date tdate,CompanyMst companyMst ,String branchCode);

	List<SalesInvoiceReport> getCustomerWiseSalesReportWithCategoryList(Date fudate, Date tudate, CompanyMst companyMst,
			String branchcode, String[] array, String customername);

	List<SalesInvoiceReport> getCustomerWiseSalesReport(Date fudate, Date tudate, CompanyMst companyMst,
			String branchcode, String customername);

	
	List<InsuranceCompanySalesSummaryReport> getSalesSummaryWithInsurance(Date fudate, Date tudate, String branchcode);

	List<InsuranceCompanySalesSummaryReport> getSalesSummaryWithInsuranceName(Date fudate, Date tudate,
			String branchcode, String[] array);

	void resendBranchSales(String id);

	List<RetailSalesDetailReport> getRetailSalesDetailReport(Date fromDate, Date toDate);

	List<RetailSalesSummaryReport> getRetailSalesSummaryReport(Date fromDate, Date toDate);

	SalesDtl saveSalesDtlForOnlinePos(SalesDtl salesDtlRequest,String branch);
	
}
