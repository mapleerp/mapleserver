package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReceiptModeMst;
import com.maple.restserver.report.entity.CardReconcileReport;
import com.maple.restserver.report.entity.PurchaseReport;
import com.maple.restserver.report.entity.ReceiptModeReport;
import com.maple.restserver.report.entity.SaleOrderReceiptReport;
import com.maple.restserver.repository.SalesReceiptsRepository;
@Service
@Transactional
@Component
public class SalesReceiptServiceImpl implements SalesReceiptService {
	
	@Autowired
	SalesReceiptsRepository salesReceiptsRepo;

	public List<CardReconcileReport> getSalesReceiptBetweenDate(Date fudate, Date tudate,String receiptMode,CompanyMst companyMst,String branchCode ) {
		List< CardReconcileReport> cardReconcileList =   new ArrayList();
		 List<Object> obj = salesReceiptsRepo.getSalesReceiptBetweenDate( fudate, tudate,receiptMode,companyMst,branchCode);  
			 for(int i=0;i<obj.size();i++)
			 {
				 Object[] objAray = (Object[]) obj.get(i);
				 CardReconcileReport cardReconcileReport = new CardReconcileReport();
				 cardReconcileReport.setVoucherDate((Date) objAray[3]);
				 cardReconcileReport.setVoucherNumber((String)objAray[0]);
				 cardReconcileReport.setReceiptMode((String)objAray[1]);
				 cardReconcileReport.setReceiptAmount((Double)objAray[2]);
				 cardReconcileList.add(cardReconcileReport);

			 }
		return cardReconcileList;
	}
	@Override
	public List<SaleOrderReceiptReport> getSalesReceiptByModeAndDate(String branchCode, Date vdate, String mode) {
		List<SaleOrderReceiptReport> salesReceiptList = new ArrayList<SaleOrderReceiptReport>();
		List<Object> obj = salesReceiptsRepo.getSalesReceiptByModeAndDate(branchCode, vdate, mode);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			SaleOrderReceiptReport salesReceipt = new SaleOrderReceiptReport();
			salesReceipt.setReceiptAmount((Double)objAray[1]);
			salesReceipt.setVoucherNumber((String)objAray[0]);
			salesReceipt.setCustomerName((String)objAray[2]);
			salesReceiptList.add(salesReceipt);
		 }
		 return salesReceiptList;
	}
	public List<ReceiptModeReport> getReceiptModeBetweenDate(Date fudate, Date tudate, String compid, String branchcode) {
		List<ReceiptModeReport> receiptModeReportList = new ArrayList<ReceiptModeReport>();
		List<Object> obj = salesReceiptsRepo.getReceiptModeBetweenDate(fudate, tudate, compid,branchcode);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ReceiptModeReport salesReceipt = new ReceiptModeReport();
			 salesReceipt.setAmount((Double)objAray[1]);
			salesReceipt.setReceiptMode((String)objAray[0]);
			receiptModeReportList.add(salesReceipt);
		 }
		 return receiptModeReportList;
	}
	public List<SaleOrderReceiptReport> getSalesReceiptByAccIDAndDate(String branchcode, Date fudate, String accid) {
		List<SaleOrderReceiptReport> salesReceiptList = new ArrayList<SaleOrderReceiptReport>();
		List<Object> obj = salesReceiptsRepo.getSalesReceiptByAccIdAndDate(branchcode, fudate, accid);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			SaleOrderReceiptReport salesReceipt = new SaleOrderReceiptReport();
			salesReceipt.setReceiptAmount((Double)objAray[1]);
			salesReceipt.setVoucherNumber((String)objAray[0]);
			salesReceipt.setCustomerName((String)objAray[2]);
			salesReceiptList.add(salesReceipt);
		 }
		 return salesReceiptList;		
	}
	public List<SaleOrderReceiptReport> getSalesReceiptByModeCard(String branchcode, Date fudate, String mode) {
		List<SaleOrderReceiptReport> salesReceiptList = new ArrayList<SaleOrderReceiptReport>();
		List<Object> obj = salesReceiptsRepo.getSalesReceiptByCard(branchcode, fudate, mode);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			SaleOrderReceiptReport salesReceipt = new SaleOrderReceiptReport();
			salesReceipt.setReceiptAmount((Double)objAray[1]);
			salesReceipt.setVoucherNumber((String)objAray[0]);
			salesReceipt.setCustomerName((String)objAray[2]);
			salesReceiptList.add(salesReceipt);
		 }
		 return salesReceiptList;
	}
	public List<ReceiptModeReport> UserWiseSalesReceiptsSummary(Date fudate, Date tudate, String companymstid,
			String branchcode, String userid) {
		List<ReceiptModeReport> receiptModeReportList = new ArrayList<ReceiptModeReport>();
		List<Object> obj = salesReceiptsRepo.UserWiseSalesReceiptsSummary(fudate, tudate, companymstid,branchcode,userid);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ReceiptModeReport salesReceipt = new ReceiptModeReport();
			 salesReceipt.setAmount((Double)objAray[1]);
			salesReceipt.setReceiptMode((String)objAray[0]);
			receiptModeReportList.add(salesReceipt);
		 }
		 return receiptModeReportList;
	}
	
	
	@Override
	public Double getCardSalesAndSoReceiptsAmount(java.sql.Date date, String companymstid, String branchcode) {
		
		
		Double totalAmount = 0.0;

		Double totalSales = salesReceiptsRepo.getCardTotalSalesReceipt(date,companymstid,branchcode);
		
		if(null == totalSales)
		{
			totalSales = 0.0;
		}
		
		Double totalSo = salesReceiptsRepo.getCardTotalSOReceipt(date,companymstid,branchcode);
		
		if(null == totalSo)
		{
			totalSo = 0.0;
		}

		Double totalPreviousSale = salesReceiptsRepo.getCardTotalPreviousSalesReceipt(date,companymstid,branchcode);

		if(null == totalPreviousSale)
		{
			totalPreviousSale = 0.0;
		}
		
		totalAmount = totalSales+totalSo-totalPreviousSale;
		
		return totalAmount;

	}

	
	
}
