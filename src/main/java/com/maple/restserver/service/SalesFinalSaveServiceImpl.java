package com.maple.restserver.service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.entity.DayBook;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesFinalSave;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.repository.AccountReceivableRepository;
import com.maple.restserver.repository.CurrencyMstRepository;
import com.maple.restserver.repository.DayBookRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.ItemStockPopUpRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.resource.SalesTransHdrResource;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.service.accounting.task.SalesAccounting;
import com.maple.restserver.service.accounting.task.SalesAccountingFC;
import com.maple.restserver.service.task.SalesDayEndReporting;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
public class SalesFinalSaveServiceImpl implements SalesFinalSaveService{
	@Autowired
	DayBookRepository dayBookRepository;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepo;
	@Autowired
	AccountReceivableRepository accountReceivableRepository;
	@Autowired
	 SalesReceiptsRepository salesReceiptsRepo;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	@Value("${voucherNoYearPrefix}")
	private String voucherNoYearPrefix;
	@Value("${POS_SALES_PREFIX}")
	private String POS_SALES_PREFIX;
	@Autowired
	private VoucherNumberService voucherService;
	@Autowired
	SchemeService schemeService;
	@Autowired
	AccountReceivable accountReceivable;
	@Autowired
	SalesDayEndReporting salesDayEndReporting;
	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;
	@Autowired
	private SalesDetailsRepository salesDetailsRepo;
	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepo;
	@Autowired
	private ItemStockPopUpRepository itemStockPopUpRepository;
	@Autowired
	private ItemMstRepository itemMstRepository;
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	@Autowired
	MultiUnitMstRepository multiUnitMstRepository;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	private ItemBatchMstRepository itemBatchMstRepo;
	@Autowired
	KitDefenitionDtlRepository kitDefenitionDtlRepo;
	@Autowired
	SalesAccounting salesAccounting;
	@Autowired
	SalesAccountingFC salesAccountingfc;
	@Autowired
	CurrencyMstRepository currencyMstRepo;
	
	private static final Logger logger = LoggerFactory.getLogger(SalesTransHdrResource.class);
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	boolean recurssionOff = false;
	
	Pageable topFifty = PageRequest.of(0, 50);
	
	@Value("${ENABLEACCOUNTING}")
	private String ENABLEACCOUNTING;

	@Override
	public void finalSaveForSales(@Valid SalesFinalSave salesFinalSave, CompanyMst companyMst, String store, String logdate) {
		SalesReceipts salesReceipts = new SalesReceipts();
		AccountReceivable accountReceivable = new AccountReceivable();
		SalesTransHdr salesTransHdrFromUrl = new SalesTransHdr();
		DayBook dayBook = new DayBook();
		

		//save sales receipts
		if(null != salesFinalSave.getSalesReceipts()) {
					
		 salesReceipts = salesFinalSave.getSalesReceipts();
		 salesReceipts.setCompanyMst(companyMst);
		 salesReceipts = salesReceiptsRepo.save(salesReceipts);
		}
		
		//save account receivable
		if(null != salesFinalSave.getAccountReceivable()) {
			
			accountReceivable = salesFinalSave.getAccountReceivable();
			accountReceivable.setCompanyMst(companyMst);
			accountReceivable = accountReceivableRepository.save(accountReceivable);
		}
		
		//update sales trans hdr
		if(null != salesFinalSave.getSalesTransHdr()) {
			salesTransHdrFromUrl = salesFinalSave.getSalesTransHdr();
			
			if(null==store||store.length()==0) {
				store = MapleConstants.Store;
							
			}
			Date logDate = SystemSetting.StringToUtilDate(logdate, "dd-MM-yyyy");
			Optional<SalesTransHdr> salestranshdridOptional =  salesTransHdrRepo.findById(salesTransHdrFromUrl.getId())  ;
			/*
			 * Check for valid record and return if no record found
			 */
			SalesTransHdr salestranshdr = salestranshdridOptional.get();		
			salesTransHdrFromUrl.setCompanyMst(salestranshdr.getCompanyMst());
			     /*
				 * Check and set vouhcer number
				 */
				if (null == salesTransHdrFromUrl.getVoucherNumber()) {
				
					setVoucherNumber(salesTransHdrFromUrl);
				}
				/*
				 * Set Object fields
				 */
				setObjectFields(salestranshdr,salesTransHdrFromUrl);
				salestranshdr = saveAndPublishService.saveSalesTransHdr(salesTransHdrFromUrl,salesTransHdrFromUrl.getBranchCode());
				if(null!=salestranshdr)
				{
					/*
					 * publishing entities
					 */
					commonPublishForSales(salestranshdr);
						
				}
//				Boolean offer = schemeService.CheckOfferWhileFinalSave(salesTransHdrFromUrl, logDate);
			 	/*
				 * mange Sales receipts and account receivable
				 */
				manageSalesReceipts(salestranshdr,salesTransHdrFromUrl);
				
				/*
				 * Manage Accounting
				 */
				if (ENABLEACCOUNTING.equalsIgnoreCase("true")) {
					logger.info(".........enable-accounting is true...........");
					/*
					 * Manage Accounting
					 */
					manageAccounting(salestranshdr);
				}
				salesDayEndReporting.execute(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(),
						salestranshdr.getCompanyMst(), salestranshdr.getId());
				/*
				 * Parameter store is introduced on Dec 20, 2020 by Regy
				 */
				/*
				 * Manage Stock
				 */
				SalesStockUpdate(salesTransHdrFromUrl.getVoucherNumber(), salesTransHdrFromUrl.getVoucherDate(),
						salestranshdr, store);
				
				
				
		
		}
		
		//save day book
		if(null != salesFinalSave.getDayBook()) {
			 dayBook = salesFinalSave.getDayBook();
			dayBook.setBranchCode(salesTransHdrFromUrl.getBranchCode());
			dayBook.setDrAmount(salesTransHdrFromUrl.getInvoiceAmount());
			dayBook.setNarration(dayBook.getDrAccountName() + salesTransHdrFromUrl.getVoucherNumber());
			dayBook.setSourceVoucheNumber(salesTransHdrFromUrl.getVoucherNumber());
			dayBook.setSourceVoucherType("SALES");
			dayBook.setCrAccountName("SALES ACCOUNT");
			dayBook.setCrAmount(salesTransHdrFromUrl.getInvoiceAmount());

			LocalDate rdate = SystemSetting.utilToLocaDate(salesTransHdrFromUrl.getVoucherDate());
			dayBook.setSourceVoucherDate(java.sql.Date.valueOf(rdate));
			dayBook.setCompanyMst(companyMst);
			dayBook = dayBookRepository.save(dayBook);
			/*
			 * publish day book
			 */
//			if(null != dayBook) {
//				saveAndPublishService.publishObjectToKafkaEvent(dayBook,
//						salesTransHdrFromUrl.getBranchCode(),
//						KafkaMapleEventType.DAYBOOK, KafkaMapleEventType.SERVER, 
//						dayBook.getId());
//			}
		}
		
		
	}
	
	
	private void setVoucherNumber(@Valid SalesTransHdr salesTransHdrRequest) {

		if (null != salesTransHdrRequest.getInvoiceNumberPrefix()) {
			String voucherNoPreFix = null;
			voucherNoPreFix = SystemSetting.getFinancialYear() + salesTransHdrRequest.getInvoiceNumberPrefix();
			String vcNo = "";

			VoucherNumber voucherNo = voucherService.generateInvoice(voucherNoPreFix,
					salesTransHdrRequest.getCompanyMst().getId());
			
			String invoice[] = voucherNo.getCode().split(voucherNoPreFix);

			salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));
			salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));


			if (voucherNoYearPrefix.equalsIgnoreCase("NO")) {
				vcNo = voucherNo.getCode().substring(9);
			} else {
				vcNo = voucherNo.getCode();
			}
			
			


			if (salesTransHdrRequest.getInvoiceNumberPrefix().equalsIgnoreCase("dd-MM")) {

				vcNo = vcNo.replace(SystemSetting.getFinancialYear(), "");

				vcNo = vcNo.replace(salesTransHdrRequest.getInvoiceNumberPrefix(), "");
				vcNo = vcNo.replace(salesTransHdrRequest.getCompanyMst().getId(), "");

				System.out.println("vcNo =" + vcNo);

				vcNo = SystemSetting.SqlDateTostring(salesTransHdrRequest.getVoucherDate(), "dd-MM") + "-"
						+ vcNo;
				System.out.println("vcNo =" + vcNo);
			} else if (salesTransHdrRequest.getInvoiceNumberPrefix().equalsIgnoreCase("MM-dd")) {
				vcNo = vcNo.replace(SystemSetting.getFinancialYear(), "");

				vcNo = vcNo.replace(salesTransHdrRequest.getInvoiceNumberPrefix(), "");
				vcNo = vcNo.replace(salesTransHdrRequest.getCompanyMst().getId(), "");

				System.out.println("vcNo =" + vcNo);

				vcNo = SystemSetting.SqlDateTostring(salesTransHdrRequest.getVoucherDate(), "MM-dd") + "-"
						+ vcNo;
				System.out.println("vcNo =" + vcNo);
			}
			salesTransHdrRequest.setVoucherNumber(vcNo);
		} else {
			String voucherNoPreFixPos = null;
			String vNo = null;

			String finYear = SystemSetting.getFinancialYear();

			VoucherNumber voucherNo = voucherService.generateInvoice(
					SystemSetting.getFinancialYear() + POS_SALES_PREFIX, salesTransHdrRequest.getCompanyMst().getId());

			if (voucherNoYearPrefix.equalsIgnoreCase("NO")) {
				vNo = voucherNo.getCode().substring(9);
			} else {
				vNo = voucherNo.getCode();
			}
			salesTransHdrRequest.setVoucherNumber(vNo);

			String invoice[] = salesTransHdrRequest.getVoucherNumber().split(POS_SALES_PREFIX);
			salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));

		}

		
	}


	private void setObjectFields(SalesTransHdr salestranshdr,SalesTransHdr salesTransHdrRequest) {
		if (null != salesTransHdrRequest.getEditedStatus()) {
			salestranshdr.setEditedStatus(salesTransHdrRequest.getEditedStatus());
		}
		salestranshdr.setLocalCustomerMst(salesTransHdrRequest.getLocalCustomerMst());

		salestranshdr.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
		// salestranshdr.setNumericVoucherNumber(salesTransHdrRequest.getNumericVoucherNumber());
		salestranshdr.setCustomerId(salesTransHdrRequest.getCustomerId());
		salestranshdr.setCardamount(salesTransHdrRequest.getCardamount());
		salestranshdr.setCardNo(salesTransHdrRequest.getCardNo());
		salestranshdr.setCashPay(salesTransHdrRequest.getCashPay());
		salestranshdr.setCreditOrCash(salesTransHdrRequest.getCreditOrCash());
		salestranshdr.setDeliveryBoyId(salesTransHdrRequest.getDeliveryBoyId());
		salestranshdr.setInvoiceAmount(salesTransHdrRequest.getInvoiceAmount());
		salestranshdr.setIsBranchSales(salesTransHdrRequest.getBranchCode());
		salestranshdr.setMachineId(salesTransHdrRequest.getMachineId());
		salestranshdr.setPaidAmount(salesTransHdrRequest.getPaidAmount());
		salestranshdr.setSalesManId(salesTransHdrRequest.getSalesManId());
		salestranshdr.setSalesMode(salesTransHdrRequest.getSalesMode());
		salestranshdr.setCurrencyId(salesTransHdrRequest.getCurrencyId());
		salestranshdr.setServingTableName(salesTransHdrRequest.getServingTableName());
		// salestranshdr.setBranchCode(salesTransHdrRequest.getBranchCode());

		salestranshdr.setAccountHeads(salesTransHdrRequest.getAccountHeads());

		Calendar calendar = Calendar.getInstance();
		java.util.Date currentDate = calendar.getTime();
		java.sql.Date date = new java.sql.Date(currentDate.getTime());

		String strDate = SystemSetting.SqlDateTostring(date);

		date = SystemSetting.StringToSqlDate(strDate, "dd/MM/yyyy");

		if (null == salesTransHdrRequest.getVoucherDate()) {
			salesTransHdrRequest.setVoucherDate(date);
		}

		// salesTransHdrRequest.setVoucherDate(salesTransHdrRequest.getVoucherDate());
		salesTransHdrRequest.setCompanyMst(salestranshdr.getCompanyMst());

		salesTransHdrRequest.setSourceIP(salestranshdr.getSourceIP());
		salesTransHdrRequest.setSourcePort(salestranshdr.getSourcePort());

		if (null != salestranshdr.getBranchCode()) {
			salesTransHdrRequest.setBranchCode(salestranshdr.getBranchCode());
		}

		if (null != salesTransHdrRequest.getInvoiceNumberPrefix()) {
			String invoice[] = salesTransHdrRequest.getVoucherNumber()
					.split(salesTransHdrRequest.getInvoiceNumberPrefix());
			if (invoice.length < 1) {
				salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));
			}
		}

		salestranshdr.setInvoicePrintType(salesTransHdrRequest.getInvoicePrintType());

		salestranshdr.setFcInvoiceAmount(salesTransHdrRequest.getFcInvoiceAmount());
		salestranshdr.setCurrencyType(salesTransHdrRequest.getCurrencyType());
		salestranshdr.setDoctorId(salesTransHdrRequest.getDoctorId());
		salestranshdr.setCurrencyConversionRate(salesTransHdrRequest.getCurrencyConversionRate());

		salestranshdr.setPoNumber(salesTransHdrRequest.getPoNumber());
		
		
		
	}
	
	
	public void commonPublishForSales(SalesTransHdr salestranshdr) {
		
		/*
		 * publish sales trans hdr and dtl
		 */
		saveAndPublishService.salesFinalSaveToKafkaEvent(salestranshdr, 
				salestranshdr.getBranchCode(), KafkaMapleEventType.SERVER);
		
		/*
		 * publish sales receipts
		 */
		List<SalesReceipts> salesReceiptsList = salesReceiptsRepo.findBySalesTransHdr(salestranshdr);
		if(salesReceiptsList.size()>0) {
			for (SalesReceipts salesReceipts:salesReceiptsList)
			{
				saveAndPublishService.publishObjectToKafkaEvent(salesReceipts,  salestranshdr.getBranchCode(), 
				KafkaMapleEventType.SALESRECEIPTS, KafkaMapleEventType.SERVER, salesReceipts.getId());
			}
		}
		/*
		 * publish account receivable
		 */
//		List<AccountReceivable> accountReceivableList = accountReceivableRepository.findByCompanyMstAndSalesTransHdr(salestranshdr.getCompanyMst(), salestranshdr);
//		if(accountReceivableList.size()>0) {
//			for (AccountReceivable accountReceivable:accountReceivableList)
//			{
//				saveAndPublishService.publishObjectToKafkaEvent(accountReceivable, 
//						salestranshdr.getBranchCode(), 
//						KafkaMapleEventType.ACCOUNTRECEIVABLE, 
//						KafkaMapleEventType.SERVER, accountReceivable.getId());
//			}
//		}
		
		
	}
	
	
private void manageSalesReceipts(SalesTransHdr salestranshdr, @Valid SalesTransHdr salesTransHdrRequest) {
		

		List<SalesReceipts> salesReceiptsList = salesReceiptsRepo.findByCompanyMstIdAndSalesTransHdrId(
				salestranshdr.getCompanyMst().getId(),
				salestranshdr.getId());

		Iterator iter = salesReceiptsList.iterator();
		while (iter.hasNext()) {
			SalesReceipts salesReceipts = (SalesReceipts) iter.next();
			salesReceipts.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			salesReceipts.setCompanyMst(salesTransHdrRequest.getCompanyMst());
			salesReceiptsRepo.save(salesReceipts);

		}

		List<AccountReceivable> accountReceivableList = accountReceivableRepository
				.findByCompanyMstAndSalesTransHdr(salestranshdr.getCompanyMst(), salestranshdr);

		if (accountReceivableList.size() > 0) {
			accountReceivable = accountReceivableList.get(0);
			if (null != accountReceivable) {
				accountReceivable.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
				java.sql.Date ldate = SystemSetting.UtilDateToSQLDate(salesTransHdrRequest.getVoucherDate());
				accountReceivable.setVoucherDate(ldate);
				accountReceivable = accountReceivableRepository.save(accountReceivable);

			}
		}
		
	}
	
	
	private void manageAccounting(SalesTransHdr salestranshdr) {
		try {
			salesAccounting.execute(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(),
					salestranshdr.getId(), salestranshdr.getCompanyMst());
			if (null != salestranshdr.getCompanyMst().getCurrencyName()
					&& !salestranshdr.getCompanyMst().getCurrencyName().equalsIgnoreCase(null)) {
				CurrencyMst comCurrencyop = currencyMstRepo
						.findByCurrencyName(salestranshdr.getCompanyMst().getCurrencyName());
				if (null != salestranshdr.getAccountHeads().getCurrencyId()) {
					Optional<CurrencyMst> custCurrencyOp = currencyMstRepo
							.findById(salestranshdr.getAccountHeads().getCurrencyId());
					if (custCurrencyOp.isPresent()) {
						if (null != comCurrencyop) {
							if (!comCurrencyop.getId().equalsIgnoreCase(custCurrencyOp.get().getId())) {
								salesAccountingfc.execute(salestranshdr.getVoucherNumber(),
										salestranshdr.getVoucherDate(), salestranshdr.getId(),
										salestranshdr.getCompanyMst());
							}
						}
					}
				}
			}

		} catch (PartialAccountingException e) {

			logger.error(e.getMessage());
			 
		}

		
	}
	
	
	
	
	
	
	public void SalesStockUpdate(String voucherNumber, Date voucherDate, SalesTransHdr salesTransHdr, String STORE) {

		List<ItemBatchDtl> itemBatchDtlList = itemBatchDtlRepository.findBySourceParentId(salesTransHdr.getId());
		if (itemBatchDtlList.size() > 0) {

			itemBatchDtlRepository.deleteBySourceParentId(salesTransHdr.getId());
		}

		List<SalesDtl> salesDtlList = salesDetailsRepo.findBySalesTransHdrId(salesTransHdr.getId());

		Iterator iter = salesDtlList.iterator();
		while (iter.hasNext()) {

			SalesDtl salesDtl = (SalesDtl) iter.next();
			// Check stock from itembatchDtl

//			
			KitDefinitionMst kitDefinitionMst = null;
			List<KitDefinitionMst> kitDefinitionMstList = kitDefinitionMstRepo.findByItemId(salesDtl.getItemId());
			if (kitDefinitionMstList.size() > 0) {
				kitDefinitionMst = kitDefinitionMstList.get(0);
			}

			if (null != kitDefinitionMst) {

				List<Object> searchItemStockByName = itemStockPopUpRepository
						.findSearchbyItemName(salesDtl.getItemName(), salesDtl.getBatch(), topFifty);
				Double chkQty = 0.0;
				for (int i = 0; i < searchItemStockByName.size(); i++) {

					Object[] objArray = (Object[]) searchItemStockByName.get(i);
					chkQty = (Double) objArray[4];

				}
				if (chkQty < 1 || chkQty < salesDtl.getQty()) {
					Double diffQty = null;
					if (chkQty < salesDtl.getQty()) {
						diffQty = salesDtl.getQty() - chkQty;
					} else {
						diffQty = salesDtl.getQty();
					}
					runTimeProduction(salesDtl, diffQty);
					executebatchwiserowmaterialupdate(salesDtl.getSalesTransHdr().getVoucherNumber(),
							salesDtl.getSalesTransHdr().getVoucherDate(), salesDtl.getCompanyMst(), salesDtl);

				}
			}
//			

			Optional<ItemMst> itemOpt = itemMstRepository.findById(salesDtl.getItemId());
			if (!itemOpt.isPresent()) {
				return;
			}

			ItemMst item = itemOpt.get();
			if (null != item.getServiceOrGoods()) {
				if (!item.getServiceOrGoods().equalsIgnoreCase("SERVICE ITEM")) {
					// ----this code execute for service item
//					List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
//							.findByItemIdAndBatchAndBarcode(salesDtl.getItemId(), salesDtl.getBatch(),
//									salesDtl.getBarcode());
					double conversionQty = 0.0;
					Double qty = 0.0;
					if (!item.getUnitId().equalsIgnoreCase(salesDtl.getUnitId())) {
						conversionQty = getConvertionQty(salesTransHdr.getCompanyMst().getId(), item.getId(),
								salesDtl.getUnitId(), item.getUnitId(), salesDtl.getQty());

						qty = -1 * conversionQty;
					} else {
						qty = -1 * salesDtl.getQty();
					}

					String processInstanceId = null;
					String taskId = null;

					String store = null;
					if (null != salesDtl.getStore()) {
						store = salesDtl.getStore();
					} else {
						store = "MAIN";
					}

					itemBatchDtlService.itemBatchMstUpdation(salesDtl.getBarcode(), salesDtl.getBatch(),
							salesTransHdr.getBranchCode(), salesDtl.getExpiryDate(), salesDtl.getItemId(),
							salesDtl.getMrp(), processInstanceId, qty, taskId, salesTransHdr.getCompanyMst(), store);

					String particular = "SALES " + salesTransHdr.getAccountHeads().getAccountName();
					Double QtyIn = 0.0;
					final VoucherNumber voucherNo = voucherService.generateInvoice("STV",
							salesTransHdr.getCompanyMst().getId());

					itemBatchDtlService.itemBatchDtlInsertionNew(salesDtl.getBarcode(), salesDtl.getBatch(),
							salesTransHdr.getBranchCode(), salesDtl.getExpiryDate(), salesDtl.getItemId(),
							salesDtl.getMrp(), particular, processInstanceId, QtyIn, salesDtl.getQty(),
							salesDtl.getId(), salesTransHdr.getId(), voucherDate, voucherNumber, store, taskId,
							voucherDate, voucherNo.getCode(), salesTransHdr.getCompanyMst());
				}
			}

		}

	}
	
	
	private void runTimeProduction(SalesDtl salesDtl, Double diffQty) {
		KitDefinitionMst kitDefinitionMst = null;
		List<KitDefinitionMst> kitDefinitionMstList = kitDefinitionMstRepo.findByItemId(salesDtl.getItemId());
		if (kitDefinitionMstList.size() > 0) {
			kitDefinitionMst = kitDefinitionMstList.get(0);
		}
		Optional<ItemMst> itemopt = itemMstRepository.findById(salesDtl.getItemId());
		{
			if (null != kitDefinitionMst) {
				List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo.findByItemIdAndBatchAndBarcode(
						salesDtl.getItemId(), salesDtl.getBatch(), salesDtl.getBarcode());

				if (itembatchmst.size() == 1) {
					ItemBatchMst itemBatchMst = itembatchmst.get(0);
					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
					itemBatchMst.setQty(itemBatchMst.getQty() + diffQty);
					itemBatchMstRepo.save(itemBatchMst);

				} else {
					ItemBatchMst itemBatchMst = new ItemBatchMst();
					itemBatchMst.setBarcode(itemopt.get().getBarCode());
					if (salesDtl.getBatch() != null) {
						itemBatchMst.setBatch(salesDtl.getBatch());
					} else {
						itemBatchMst.setBatch(MapleConstants.Nobatch);
					}

					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
					itemBatchMst.setQty(diffQty);
					itemBatchMst.setItemId(salesDtl.getItemId());
					itemBatchMst.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());
					itemBatchMst.setCompanyMst(salesDtl.getSalesTransHdr().getCompanyMst());
					itemBatchMstRepo.save(itemBatchMst);

				}

				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
				final VoucherNumber voucherNo = voucherService.generateInvoice("STV",
						salesDtl.getSalesTransHdr().getCompanyMst().getId());
				itemBatchDtl.setBarcode(itemopt.get().getBarCode());
				if (salesDtl.getBatch() != null) {
					itemBatchDtl.setBatch(salesDtl.getBatch());
				} else {
					itemBatchDtl.setBatch(MapleConstants.Nobatch);
				}
				itemBatchDtl.setItemId(salesDtl.getItemId());
				itemBatchDtl.setMrp(itemopt.get().getStandardPrice());
				itemBatchDtl.setQtyIn(diffQty);
				itemBatchDtl.setQtyOut(0.0);
				itemBatchDtl.setStore("MAIN");
				itemBatchDtl.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());
				itemBatchDtl.setVoucherNumber(voucherNo.getCode());
				itemBatchDtl.setVoucherDate(salesDtl.getSalesTransHdr().getVoucherDate());
				itemBatchDtl.setItemId(salesDtl.getItemId());
				itemBatchDtl.setSourceVoucherNumber(salesDtl.getSalesTransHdr().getVoucherNumber());
				itemBatchDtl.setParticulars("SALES PRODUCTION");
				itemBatchDtl.setStore("MAIN");
				itemBatchDtl.setSourceParentId(salesDtl.getSalesTransHdr().getId());
				itemBatchDtl.setSourceDtlId(salesDtl.getId());
				itemBatchDtl.setCompanyMst(salesDtl.getSalesTransHdr().getCompanyMst());
				itemBatchDtl = itemBatchDtlRepository.save(itemBatchDtl);

			}

		}
	}
	
	
	public void executebatchwiserowmaterialupdate(String voucherNumber, Date voucherDate, CompanyMst companyMst,
			SalesDtl salesDtl) {

//			
//			/*
//			 * Find Kit Header
//			 */

		KitDefinitionMst kitDefinitionMst = null;
		List<KitDefinitionMst> kitDefinitionMstList = kitDefinitionMstRepo.findByItemId(salesDtl.getItemId());
		if (kitDefinitionMstList.size() > 0) {
			kitDefinitionMst = kitDefinitionMstList.get(0);
		}
		Double defenitionQty = kitDefinitionMst.getMinimumQty();

		/*
		 * Find Kit Dtl - Raw Materil
		 */

		List<KitDefenitionDtl> kitDefenitionDtlList = kitDefenitionDtlRepo
				.findBykitDefenitionmstId(kitDefinitionMst.getId());

		Iterator iterRM = kitDefenitionDtlList.iterator();

		while (iterRM.hasNext()) {

			KitDefenitionDtl kitDefenitionDtl = (KitDefenitionDtl) iterRM.next();

			String rawMaterialId = kitDefenitionDtl.getItemId();
			Double rawMaterialUnitQty = kitDefenitionDtl.getQty();
			if (null != rawMaterialId) {
				Double requiredRawMaterial = rawMaterialUnitQty * salesDtl.getQty() / defenitionQty;

				System.out.println(rawMaterialId);

				Optional<ItemMst> itemopt = itemMstRepository.findById(rawMaterialId);

				if (!itemopt.isPresent()) {
					System.out.println(rawMaterialId + " Not Present");
					continue;
				}

				ItemMst rmItem = itemopt.get();

				List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
						.findByItemIdAndBatchAndBarcode(rawMaterialId, MapleConstants.Nobatch, rmItem.getBarCode());

				if (itembatchmst.size() == 1) {
					ItemBatchMst itemBatchMst = itembatchmst.get(0);
					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
					itemBatchMst.setQty(itemBatchMst.getQty() - requiredRawMaterial);
					itemBatchMst.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());
					itemBatchMstRepo.save(itemBatchMst);
				} else {
					ItemBatchMst itemBatchMst = new ItemBatchMst();
					itemBatchMst.setBarcode(itemopt.get().getBarCode());
					itemBatchMst.setBatch(MapleConstants.Nobatch);
					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
					itemBatchMst.setQty(-1 * requiredRawMaterial);
					itemBatchMst.setItemId(rawMaterialId);
					itemBatchMst.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());
					itemBatchMst.setCompanyMst(companyMst);
					itemBatchMstRepo.save(itemBatchMst);

				}

				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
				final VoucherNumber voucherNo = voucherService.generateInvoice("STV", companyMst.getId());
				itemBatchDtl.setBarcode(itemopt.get().getBarCode());
				itemBatchDtl.setBatch(MapleConstants.Nobatch);
				itemBatchDtl.setItemId(rawMaterialId);
				itemBatchDtl.setMrp(itemopt.get().getStandardPrice());
				itemBatchDtl.setQtyIn(0.0);
				itemBatchDtl.setParticulars("SALES KOT-" + salesDtl.getItemName());
				itemBatchDtl.setStore("MAIN");
				itemBatchDtl.setQtyOut(requiredRawMaterial);
				itemBatchDtl.setVoucherNumber(voucherNo.getCode());
				itemBatchDtl.setVoucherDate(salesDtl.getSalesTransHdr().getVoucherDate());
				itemBatchDtl.setItemId(rawMaterialId);
				itemBatchDtl.setSourceVoucherNumber(salesDtl.getSalesTransHdr().getVoucherNumber());
				itemBatchDtl.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());
				itemBatchDtl.setCompanyMst(companyMst);
				itemBatchDtl.setSourceParentId(salesDtl.getSalesTransHdr().getId());
				itemBatchDtl.setSourceDtlId(salesDtl.getId());
				itemBatchDtl = itemBatchDtlRepository.save(itemBatchDtl);

			}

		}
	}
	

	
	private double getConvertionQty(String companyMstId, String itemId, String sourceUnit, String targetUnit,
			double sourceQty) {
		if (recurssionOff) {
			return sourceQty;
		}
		MultiUnitMst multiUnitMstList = multiUnitMstRepository.findByCompanyMstIdAndItemIdAndUnit1(companyMstId, itemId,
				sourceUnit);

		while (!multiUnitMstList.getUnit2().equalsIgnoreCase(targetUnit)) {

			if (recurssionOff) {
				break;
			}
			sourceUnit = multiUnitMstList.getUnit2();

			sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();

			getConvertionQty(companyMstId, itemId, sourceUnit, targetUnit, sourceQty);

		}
		sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();
		recurssionOff = true;
		// sourceQty = sourceQty *
		// multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
		return sourceQty;
	}

}
