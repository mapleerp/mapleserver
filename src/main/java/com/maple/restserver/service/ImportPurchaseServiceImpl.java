package com.maple.restserver.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CurrencyConversionMst;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.entity.PurchaseAdditionalExpenseDtl;
import com.maple.restserver.entity.PurchaseAdditionalExpenseHdr;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.report.entity.IntentInReport;
import com.maple.restserver.report.entity.SaleOrderTax;
import com.maple.restserver.repository.CurrencyConversionMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;
import com.maple.restserver.repository.IntentInDtlRepository;
import com.maple.restserver.repository.IntentInHdrRepository;
import com.maple.restserver.repository.PurchaseAdditionalExpenseDtlRepository;
import com.maple.restserver.repository.PurchaseAdditionalExpenseHdrRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;

 

@Service
@Transactional
@Component
public class ImportPurchaseServiceImpl implements ImportPurchaseService {

	@Autowired
	PurchaseDtlRepository purchaseDtlRepository;
	
	@Autowired
	PurchaseAdditionalExpenseHdrRepository purchaseAdditionalExpenseHdrRepository;
	
	@Autowired
	PurchaseAdditionalExpenseDtlRepository purchaseAdditionalExpenseDtlRepository;
	
 
	@Autowired
	CurrencyMstRepository currencyMstRepo;
	
	
	@Autowired
	CurrencyConversionMstRepository currencyConversionMstRepo;
	
	@Override
	public void updateNetCost(PurchaseHdr purchaseHdr) {
		 /*
		  * 

	    	 
	    	ResponseEntity<List<PurchaseDtl>> purchaseDtlList = RestCaller.getPurchaseDtl(purchasehdr);
	    	--for(int i=0;i<purchaseDtlList.getBody().size();i++)
	    	{
	    		PurchaseDtl purchase = purchaseDtlList.getBody().get(i);
	    		Double netCost =0.0;
	    		ResponseEntity<PurchaseAdditionalExpenseHdr> purchaseAdditionalExpenseHdr = RestCaller.getPurchaseAdditionalExpenseHdrByPurchaseDtl(purchaseDtlList.getBody().get(i).getId());
	    		if(null != purchaseAdditionalExpenseHdr.getBody())
	    		{
	    			ResponseEntity<List<PurchaseAdditionalExpenseDtl>> purchaseAddExpDtl = RestCaller.getpurchaseAdditionalExpenseDtlByHdrIdByCalculated(purchaseAdditionalExpenseHdr.getBody().getId());
	    			if(purchaseAddExpDtl.getBody().size()>0)
	    			{
	    				--for(int j =0;j<purchaseAddExpDtl.getBody().size();j++)
	    				{
	    					netCost = netCost + purchaseAddExpDtl.getBody().get(j).getAmount();
		    				PurchaseAdditionalExpenseDtl purchaseAddExpDtl1 = purchaseAddExpDtl.getBody().get(j);
		    				purchaseAddExpDtl1.setCalculated("YES");
		    				RestCaller.updatePurchaseAdditionalExpenseDtl(purchaseAddExpDtl1);
	    				}
	    				
	    			}
	    		}
	    		purchase.setNetCost(netCost+purchase.getNetCost());
	    		if(null != cmbCurrency.getSelectionModel().getSelectedItem())
	    		{
	    			Double fcNetCost = RestCaller.getFCRate(purchase.getNetCost(),cmbCurrency.getSelectionModel().getSelectedItem());
	    			purchase.setFcNetCost(fcNetCost);
	    		}
	    		RestCaller.updatepurchaseNetCost(purchase);
	    	}
	    	ResponseEntity<List<PurchaseDtl>> purchaseDtlList1rep = RestCaller.getPurchaseDtl(purchasehdr);
	    	
	     
	    	
	    
		  */
		
		// Get List of Purchase Dtl usinig HDR.
		
		List<PurchaseDtl> purchaseDtlList = purchaseDtlRepository.findByPurchaseHdrId(purchaseHdr.getId());
		for (PurchaseDtl purchaseDtl : purchaseDtlList) {
			
			PurchaseAdditionalExpenseHdr purchaseAdditionalExpenseHdr = purchaseAdditionalExpenseHdrRepository.findByPurchaseDtl(purchaseDtl);
		
			
			//List<PurchaseAdditionalExpenseDtl> purchaseAddExpDtl =purchaseAdditionalExpenseDtlRepository.findByPurchaseAdditionalExpenseHdrIdNotCalculated(purchaseAdditionalExpenseHdr);
			List<PurchaseAdditionalExpenseDtl> purchaseAddExpDtl =purchaseAdditionalExpenseDtlRepository.findByPurchaseAdditionalExpenseHdrIdNotCalculated(purchaseAdditionalExpenseHdr.getId());
			
			double netCost =0.0;
			for (PurchaseAdditionalExpenseDtl purchaseDtl2 : purchaseAddExpDtl) {
				
				netCost = netCost + purchaseDtl2.getAmount();
				 
				purchaseDtl2.setCalculated("YES");
				
				purchaseAdditionalExpenseDtlRepository.save(purchaseDtl2);
			}
			
			double netfinalNetCost  = netCost + purchaseDtl.getNetCost();
			
			purchaseDtl.setNetCost(netfinalNetCost);
			
			
			
			 	
			
			
			Double fcrate=0.0;
			CurrencyMst currencyMstByid = currencyMstRepo.findByCurrencyName(purchaseHdr.getCurrency());
			CurrencyConversionMst currencyConversionMst = currencyConversionMstRepo.findByToCurrencyId(currencyMstByid.getId());
			 
			Double fcRate = netfinalNetCost/currencyConversionMst.getConversionRate();
			BigDecimal bdfcRate = new BigDecimal(fcRate);
			bdfcRate = bdfcRate.setScale(2,BigDecimal.ROUND_HALF_EVEN);
			fcrate =  bdfcRate.doubleValue();
			purchaseDtl.setFcNetCost(fcrate);
			
			purchaseDtlRepository.save(purchaseDtl);
			
			
			 
			
		}
		
			   
		
	} 
	
}
