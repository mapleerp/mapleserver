package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.BranchStockReport;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.ItemStockRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
public class BranchStockReportServiceImpl implements BranchStockReportService {
	@Autowired
	ItemStockRepository itemStockRepository;
	
	@Autowired
	CategoryMstRepository categoryMstRepository;
	
	@Override
	public List<BranchStockReport> findBranchStockReport(Date date) {

		List<BranchStockReport> branchStockReportList = new ArrayList<BranchStockReport>();
		
		List<Object> obj = itemStockRepository.findBranchStockReport(date);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			BranchStockReport branchStockReport = new BranchStockReport();
			
			branchStockReport.setGroupName((String) objAray[0].toString());
			branchStockReport.setItemName((String) objAray[1]);
			branchStockReport.setBatchCode((String) objAray[2]);
			branchStockReport.setItemCode((String) objAray[3]);
		
			if(null!=objAray[4]) {
				branchStockReport.setExpiryDate((String) objAray[4].toString());
			}
			branchStockReport.setQuantity((Double) objAray[5]);
			branchStockReport.setRate((Double) objAray[6]);
			Double qty = (Double) objAray[5];
			Double rate = (Double) objAray[6];
			Double amount = qty * rate;
			branchStockReport.setAmount(amount);
			
		
			branchStockReportList.add(branchStockReport);
			
		 }
	
        return branchStockReportList;
	
	}
	@Override
	public List<BranchStockReport> findPharmacyStockReportBranchWise(CompanyMst companyMst, Date date,
			String branchCode) {
		
             List<BranchStockReport> branchStockReportList = new ArrayList<BranchStockReport>();
		
		      List<Object> obj = itemStockRepository.findPharmacyStockBranchWiseReport(companyMst,date,branchCode);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			BranchStockReport branchStockReport = new BranchStockReport();
			
			branchStockReport.setGroupName((String) objAray[0].toString());
			branchStockReport.setItemName((String) objAray[1]);
			branchStockReport.setBatchCode((String) objAray[2]);
			branchStockReport.setItemCode((String) objAray[3]);
		
			if(null!=objAray[4]) {
				branchStockReport.setExpiryDate((String) objAray[4].toString());
			}
			branchStockReport.setQuantity((Double) objAray[5]);
			branchStockReport.setRate((Double) objAray[6]);
			Double qty = (Double) objAray[5];
			Double rate = (Double) objAray[6];
			Double amount = qty * rate;
			branchStockReport.setAmount(amount);
			
		
			branchStockReportList.add(branchStockReport);
			
		 }
	
        return branchStockReportList;
	
	}
	@Override
	public List<BranchStockReport> findPharmacyStockReportBranchWiseWithCategory(CompanyMst companyMst, Date date,
			String branchCode, String[] array) {
		

        List<BranchStockReport> branchStockReportList = new ArrayList<BranchStockReport>();
        
    	for(String categoryName:array) {
			CategoryMst categoryMst=categoryMstRepository.findByCategoryNameAndCompanyMst(categoryName,companyMst);
	
	      List<Object> obj = itemStockRepository.findPharmacyStockBranchAndItemWiseReport(companyMst,date,branchCode,categoryMst.getId());
	 for(int i=0;i<obj.size();i++)
	 {
		Object[] objAray = (Object[]) obj.get(i);
		BranchStockReport branchStockReport = new BranchStockReport();
		
		branchStockReport.setGroupName((String) objAray[0].toString());
		branchStockReport.setItemName((String) objAray[1]);
		branchStockReport.setBatchCode((String) objAray[2]);
		branchStockReport.setItemCode((String) objAray[3]);
	
		if(null!=objAray[4]) {
			//branchStockReport.setExpiryDate((String) objAray[4].toString());
			branchStockReport.setExpiryDate(SystemSetting.UtilDateToString((Date) objAray[4], "yyyy-MM-dd"));
			
		}
		branchStockReport.setQuantity((Double) objAray[5]);
		branchStockReport.setRate((Double) objAray[6]);
		Double qty = (Double) objAray[5];
		Double rate = (Double) objAray[6];
		Double amount = qty * rate;
		branchStockReport.setAmount(amount);
		
	
		branchStockReportList.add(branchStockReport);
		
	 }
     }
	
	 return branchStockReportList;	
	
	
	}
}

