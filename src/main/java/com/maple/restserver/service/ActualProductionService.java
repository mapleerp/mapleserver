package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.ActualProductionReport;
import com.maple.restserver.report.entity.FinishedProduct;



@Component
public interface ActualProductionService {
	List<ActualProductionReport> actualProductionReport(String companymstid,String branchcode,Date fdate,Date tdate);

	List<ActualProductionReport> actualProductionPrintReport(String companymstid, String branchcode, Date fdate,
			Date tdate);
	
	
	List<FinishedProduct>actualproductionbetweendate(String companymstid, String branchcode, Date fdate,
			Date tdate);
	
}
