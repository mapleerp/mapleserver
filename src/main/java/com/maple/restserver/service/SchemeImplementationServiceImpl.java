package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SchEligiAttrListDef;
import com.maple.restserver.entity.SchEligibilityAttribInst;
import com.maple.restserver.entity.SchEligibilityDef;
import com.maple.restserver.entity.SchOfferAttrInst;
import com.maple.restserver.entity.SchOfferAttrListDef;
import com.maple.restserver.entity.SchOfferDef;
import com.maple.restserver.entity.SchSelectAttrListDef;
import com.maple.restserver.entity.SchSelectDef;
import com.maple.restserver.entity.SchSelectionAttribInst;
import com.maple.restserver.entity.SchemeCategory;
import com.maple.restserver.entity.SchemeInstance;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.SchemeMessage;
import com.maple.restserver.report.entity.SaleOrderDetailsReport;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.SchEligiAttrListDefRepository;
import com.maple.restserver.repository.SchEligibilityAttribInstRepository;
import com.maple.restserver.repository.SchEligibilityDefRepository;
import com.maple.restserver.repository.SchOfferAttrInstRepository;
import com.maple.restserver.repository.SchOfferAttrListDefRepository;
import com.maple.restserver.repository.SchOfferDefRepository;
import com.maple.restserver.repository.SchSelectAttrListDefReoository;
import com.maple.restserver.repository.SchSelectDefRepository;
import com.maple.restserver.repository.SchSelectionAttribInstRepository;

import com.maple.restserver.repository.SchemeInstanceRepository;
import com.maple.restserver.repository.UnitMstRepository;

@Service
public class SchemeImplementationServiceImpl implements SchemeImplementationService {

	@Autowired
	SchSelectionAttribInstRepository schSelectionAttribInstRepository;
	@Autowired
	SchemeInstanceRepository schemeInstanceRepository;
	
	
	@Autowired
	ItemMstRepository itemMstRepository;
	@Autowired
	SchEligibilityAttribInstRepository schEligibilityAttribInstRepository;
	@Autowired
	CategoryMstRepository categoryMstRepository;
	@Autowired
	SalesDetailsService salesDetailsService;
	@Autowired
	MultiUnitConversionServiceImpl multiUnitConversionServiceImpl;
	@Autowired
	SalesDetailsRepository salesdtlRepo;
	@Autowired
	SchOfferAttrInstRepository schOfferAttrInstRepository;
	@Autowired
	UnitMstRepository unitMstRepository;
	
	@Autowired
	SchOfferDefRepository schOfferDefRepository;
	
	@Autowired
	SchOfferAttrListDefRepository schOfferAttrListDefRepository;
	
	@Autowired
	SchEligibilityDefRepository  schEligibilityDefRepository;
	
	@Autowired
	SchSelectDefRepository schSelectDefRepository;
	
	
	@Autowired
	SchEligiAttrListDefRepository schEligiAttrListDefRepository;
	
	@Autowired
	SchSelectAttrListDefReoository schSelectAttrListDefReoository;
	
	@Autowired
	SaveAndPublishServiceImpl saveAndPublishServiceImpl;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	

	@Override
	public String offerCheckingAndOfferApply(SalesDtl salesDtl, CompanyMst companyMst, Date loginDate) {

		/*
		 * check whether there is any active offer
		 */
		List<SchemeInstance> schemeInstanceList = activeSchemeCheck(salesDtl, companyMst);

		if (schemeInstanceList.size() == 0) {
			return null;
		}

		for (SchemeInstance scheme : schemeInstanceList) {
			/*
			 * validity checking
			 */
			Boolean validityPeriod = CheckValidityDate(scheme, companyMst, salesDtl, loginDate);

			if (!validityPeriod) {
				return null;
			}
			/*
			 * Offer
			 */
			String offer = scheme.getOfferId();
			String eligibility = scheme.getEligibilityId();

			switch (eligibility) {
			case MapleConstants.FOR_A_SPECIFIC_QTY_OF_CATEGORY: {
				
				
				//CATEGORY
				//QTY
				//sch_eligibility_attrib_inst
				String elgCategory = schemeInstanceRepository.getAttriValueByEligiAttriName("CATEGORY",eligibility,scheme.getId());
				String elgQty = schemeInstanceRepository.getAttriValueByEligiAttriName("QTY",eligibility,scheme.getId());
				CategoryMst categoryMst = categoryMstRepository.findByCategoryNameAndCompanyMst(elgCategory, companyMst);
				// id from category mst 
				if (null==categoryMst)
				{
					 return MapleConstants.FAILED;
				}
				String catId =categoryMst.getId();
				
				switch (offer) {
					case MapleConstants.AMOUNT_DISCOUNT:{
						// get this attribute
						//DISCOUNT
						String discountWiseAttriValue = schemeInstanceRepository.getAttributeValueByOfferAttriName("DISCOUNT",offer,scheme.getId());
						
					}
					case MapleConstants.FREE_QUANTITY:{
						// ITEMNAME
						//QTY
						//BATCH_CODE
						//MULTIPLE ALLOWED
						// select attribute_value from sch_offer_attr_inst where attribute_name = 'ITEMNAME';
						String itemWiseAttriValue = schemeInstanceRepository.getAttributeValueByOfferAttriName("ITEMNAME",offer,scheme.getId());
						ItemMst itemMst = itemMstRepository.findByItemName(itemWiseAttriValue);
						//Get Item ID
						String itemId=itemMst.getId();
						
						String qtyWiseAttriValue = schemeInstanceRepository.getAttributeValueByOfferAttriName("QTY",offer,scheme.getId());
						String batchCodeWiseAttriValue = schemeInstanceRepository.getAttributeValueByOfferAttriName("BATCH_CODE",offer,scheme.getId());
						String multiAllowedWiseAttriValue = schemeInstanceRepository.getAttributeValueByOfferAttriName("MULTIPLE ALLOWED",offer,scheme.getId());
					
					
						List<Object> schemeCategoryList=schemeInstanceRepository.
								getSchemeCategory(salesDtl.getSalesTransHdr().getId());
						
	
						
						for(int i = 0; i < schemeCategoryList.size(); i++) {
							Object[] objAray = (Object[]) schemeCategoryList.get(i);
							SchemeCategory schemeCategory = new SchemeCategory();
							schemeCategory.setCategoryId((String) objAray[1]);
							schemeCategory.setQty((Double)objAray[0]);
							
							if(schemeCategory.getCategoryId().equalsIgnoreCase(catId)) {
								double salesqty =schemeCategory.getQty();
								double elgQtyDouble = Double.parseDouble(elgQty);
								if(salesqty>=elgQtyDouble) {
									// Multiple YES
									if(multiAllowedWiseAttriValue.equalsIgnoreCase("YES")) {
										
										
										
									// Get sales dtl item and qty bases of item id 
										SalesDtl salesDtlOfferItem = salesdtlRepo.findByItemIdAndSalesTransHdrAndOfferReferenceId(
												itemId, salesDtl.getSalesTransHdr(),"YES");
										
										String offerItemAddedItemName ="";
										Double offerItemAlreadyAddedQty =0.0;
										
										if(salesDtlOfferItem==null)
										{
											offerItemAddedItemName=itemMst.getItemName();
											offerItemAlreadyAddedQty=0.0;
										}
										else {
											offerItemAddedItemName = salesDtlOfferItem.getItemName();
											offerItemAlreadyAddedQty = salesDtlOfferItem.getQty();
										}
										int offerToProveQty = (int) (salesqty/elgQtyDouble);
										
										double  offerToadd = offerToProveQty - offerItemAlreadyAddedQty;
										//Now add offerToadd of Otfeer Item to sals
										buildOffer(offerItemAddedItemName,offerToadd,salesDtl.getCompanyMst(),salesDtl.getSalesTransHdr());
										
										
									}else {
										
									}
									
								}
								
							}
						}
					
					}
					case MapleConstants.PERCENTAGE_DISCOUNT:{
					// PERCENTAGE DISCOUNT
						String PercentageDiscountWiseAttriValue = schemeInstanceRepository.getAttributeValueByOfferAttriName("PERCENTAGE DISCOUNT",offer,scheme.getId());
					}
				}

				
				
				
			}
				break;
	case MapleConstants.FOR_TOTAL_INVOICE_AMOUNT: {
				
				
				//CATEGORY
				//QTY
				//sch_eligibility_attrib_inst
				String  elgAmount = schemeInstanceRepository.getAttriValueByEligiAttriName("AMOUNT",eligibility,scheme.getId());
				 
				//Find Invoice Amount
				double invoiceAmount = salesTransHdrRepository.getInvoiceAmountByIdAndCompanyMst( salesDtl.getSalesTransHdr().getId(),
						companyMst.getId());
				
				if(invoiceAmount<Double.parseDouble(elgAmount))
					return "ok";
				
				
				switch (offer) {
					case MapleConstants.AMOUNT_DISCOUNT:{
						// get this attribute
						//DISCOUNT
						String discountWiseAttriValue = schemeInstanceRepository.getAttributeValueByOfferAttriName("DISCOUNT",offer,scheme.getId());
						
					}
					case MapleConstants.FREE_QUANTITY:{
						 
						String itemWiseAttriValue = schemeInstanceRepository.getAttributeValueByOfferAttriName("ITEMNAME",offer,scheme.getId());
						ItemMst itemMst = itemMstRepository.findByItemName(itemWiseAttriValue);
					 
						String itemId=itemMst.getId();
						
						String qtyWiseAttriValue = schemeInstanceRepository.getAttributeValueByOfferAttriName("QTY",offer,scheme.getId());
						String batchCodeWiseAttriValue = schemeInstanceRepository.getAttributeValueByOfferAttriName("BATCH_CODE",offer,scheme.getId());
						String multiAllowedWiseAttriValue = schemeInstanceRepository.getAttributeValueByOfferAttriName("MULTIPLE ALLOWED",offer,scheme.getId());
					
						
					 					
										
										
									// Get sales dtl item and qty bases of item id 
										SalesDtl salesDtlOfferItem = salesdtlRepo.findByItemIdAndSalesTransHdrAndOfferReferenceId(
												itemId, salesDtl.getSalesTransHdr(),"YES");
										
										String offerItemAddedItemName ="";
										Double offerItemAlreadyAddedQty =0.0;
										
										if(salesDtlOfferItem==null)
										{
											offerItemAddedItemName=itemMst.getItemName();
											offerItemAlreadyAddedQty=0.0;
										}
										else {
											if(multiAllowedWiseAttriValue.equalsIgnoreCase("NO")) {
												return "ok";
											}
											offerItemAddedItemName = salesDtlOfferItem.getItemName();
											offerItemAlreadyAddedQty = salesDtlOfferItem.getQty();
										}
										int offerToProveQty = Integer.parseInt(qtyWiseAttriValue);
										
									  
										buildOffer(offerItemAddedItemName,offerToProveQty,salesDtl.getCompanyMst(),salesDtl.getSalesTransHdr());
						 
					}
					case MapleConstants.PERCENTAGE_DISCOUNT:{
					// PERCENTAGE DISCOUNT
						String PercentageDiscountWiseAttriValue = schemeInstanceRepository.getAttributeValueByOfferAttriName("PERCENTAGE DISCOUNT",offer,scheme.getId());
					}
				}

				
				
				
			}

			case MapleConstants.FOR_A_SPECIFIC_QTY_OF_ITEM_NAME: {

			}
				break;

			}

			
			
			
			
			
			
		
		}
		return null;

	}

	private void buildOffer(String offerItemAddedItemName, 
			double offerToadd, CompanyMst companyMst, SalesTransHdr salesTransHdr) {

		ItemMst itemMst = itemMstRepository.findByItemName(offerItemAddedItemName);
		
		
		SalesDtl salesDtl = new SalesDtl();

		
		salesDtl.setAddCessAmount(0.0);
		salesDtl.setAddCessRate(0.0);
		salesDtl.setAmount(0.0);
		salesDtl.setBarcode(itemMst.getBarCode());
		salesDtl.setBatch(MapleConstants.Nobatch);
		salesDtl.setCessRate(0.0);
		salesDtl.setCessAmount(0.0);
		salesDtl.setCgstAmount(0.0);
		salesDtl.setCgstTaxRate(0.0);
		salesDtl.setCompanyMst(companyMst);
		salesDtl.setCostPrice(0.0);
		salesDtl.setDiscount(0.0);
		salesDtl.setExpiryDate(null);
		salesDtl.setIgstAmount(0.0);
		salesDtl.setIgstTaxRate(0.0);
		salesDtl.setItemId(itemMst.getId());
		salesDtl.setItemName(itemMst.getItemName());
		salesDtl.setOfferReferenceId("YES");
		salesDtl.setMrp(0.0);
		salesDtl.setPrintKotStatus("N");
		salesDtl.setQty(offerToadd);
		salesDtl.setRate(0.0);
		salesDtl.setRateBeforeDiscount(0.0);
		salesDtl.setReturnedQty(0.0);
		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setSgstAmount(0.0);
		salesDtl.setSgstTaxRate(0.0);
		salesDtl.setStandardPrice(itemMst.getStandardPrice());
		salesDtl.setTaxRate(itemMst.getTaxRate());
		salesDtl.setUnitId(itemMst.getUnitId());
		
		salesDetailsService.saveSalesDtlForOnlinePos(salesDtl,salesTransHdr.getBranchCode());
		
	}



	private Boolean CheckValidityDate(SchemeInstance scheme, CompanyMst companyMst, SalesDtl addedItem,
			Date loginDate) {

		List<SchSelectionAttribInst> schSlectionAttrbInstList = schSelectionAttribInstRepository
				.findByCompanyMstAndSchemeId(companyMst, scheme.getId());

		for (SchSelectionAttribInst select : schSlectionAttrbInstList) {

			if (select.getAttributeType().equalsIgnoreCase("DATE")) {

				if (select.getAttributeName().equalsIgnoreCase("START DATE")) {
					Boolean validStartDate = validStartDate(select, loginDate);
					if (!validStartDate) {
						return false;
					}
				}

				if (select.getAttributeName().equalsIgnoreCase("END DATE")) {
					Boolean validEndDate = validEndDate(select, loginDate);
					if (!validEndDate) {
						return false;
					}
				}

			}

		}

		return true;
	}

	private List<SchemeInstance> activeSchemeCheck(SalesDtl addedItem, CompanyMst companyMst) {

		String salesMode = null;

		SalesTransHdr salesTransHdr = addedItem.getSalesTransHdr();
		if (null != salesTransHdr) {
			if (null == salesTransHdr.getSalesMode()) {
				return null;
			}
		}

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("POS")) {

			salesMode = "Retail";

		}
		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2B")
				|| salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			salesMode = "WholeSale";
		}

		if (null == salesMode) {
			return null;
		}

		List<SchemeInstance> schemeInstanceList = schemeInstanceRepository
				.findByCompanyMstAndSchemeWholesaleRetailAndIsActive(companyMst.getCompanyName(), salesMode, "YES");
		return schemeInstanceList;
	}

	private Boolean validStartDate(SchSelectionAttribInst select, Date nowDate) {
		java.util.Date startDate = ClientSystemSetting.StringToUtilDate(select.getAttributeValue(), "dd-MM-yyyy");
		Integer intDate = nowDate.compareTo(startDate);

		if (intDate >= 0) {
			return true;
		}

		return false;
	}

	private Boolean validEndDate(SchSelectionAttribInst select, Date nowDate) {
		java.util.Date endDate = ClientSystemSetting.StringToUtilDate(select.getAttributeValue(), "dd-MM-yyyy");
		Integer intDate = endDate.compareTo(nowDate);

		if (intDate >= 0) {
			return true;
		}
		return false;
	}
	
	
/*
 * scheme publishing to all branches.........17-03-2022.............
 */
	@Override
	public String publishSchemeToAllBranches(String schemename,String companymstid) {
		
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
			CompanyMst companyMst = companyMstOpt.get();
		
		SchemeInstance schemeInstance = new SchemeInstance();
		schemeInstance =schemeInstanceRepository.findBySchemeNameAndCompanyMstId(schemename, companymstid);
		
		
		Optional<SchSelectDef> schSelectDef = schSelectDefRepository.findById(schemeInstance.getSelectionId());
		 List<SchSelectionAttribInst> schSelectionAttribInst = 
				 schSelectionAttribInstRepository.findBySelectionId(schSelectDef.get().getId());
		 for(SchSelectionAttribInst schSelectionAttIns : schSelectionAttribInst) {
			 if(schSelectionAttIns.getAttributeValue().isEmpty()) {
				 return MapleConstants.FAILED;
			 }
		 }
		 List<SchSelectAttrListDef> schSelectAttrListDef = 
				 schSelectAttrListDefReoository.findBySelectionIdAndCompanyMst(schSelectDef.get().getId(), companyMst);
		
		 
		 Optional<SchOfferDef> schOfferDef = schOfferDefRepository.findById(schemeInstance.getOfferId());
		 List<SchOfferAttrInst> schOfferAttrInst = schOfferAttrInstRepository.findByOfferId(schOfferDef.get().getId());
		 for(SchOfferAttrInst schOfrAtrInst : schOfferAttrInst) {
			 if(schOfrAtrInst.getAttributeValue().isEmpty()) {
				 return MapleConstants.FAILED;
			 }
		 }
		 List<SchOfferAttrListDef> schOfferAttrListDef = 
				 schOfferAttrListDefRepository.findByOfferIdAndCompanyMst(schOfferDef.get().getId(), companyMst);
		 
		 
		 Optional<SchEligibilityDef> schEligibilityDef = schEligibilityDefRepository.findById(schemeInstance.getEligibilityId());
		 List<SchEligiAttrListDef> schEligiAttrListDef = 
				 schEligiAttrListDefRepository.findByEligibilityIdAndCompanyMst(schEligibilityDef.get().getId(), companyMst);
		 List<SchEligibilityAttribInst> schEligibilityAttribInst = 
				 schEligibilityAttribInstRepository.findByEligibilityId(schEligibilityDef.get().getId());
		 for(SchEligibilityAttribInst schEligAttriInst : schEligibilityAttribInst) {
			 if(schEligAttriInst.getAttributeValue().isEmpty()) {
				 return MapleConstants.FAILED;
			 }
		 }
		 
		 
		 SchemeMessage schemeMessage = new SchemeMessage();
		 schemeMessage.setSchemeInstance(schemeInstance);
		 schemeMessage.setSchSelectDef(schSelectDef.get());
		 schemeMessage.getSchSelectionAttribInst().addAll(schSelectionAttribInst);
		 schemeMessage.getSchSelectAttrListDef().addAll(schSelectAttrListDef);
		 schemeMessage.setSchOfferDef(schOfferDef.get());
		 schemeMessage.getSchOfferAttrInst().addAll(schOfferAttrInst);
		 schemeMessage.getSchOfferAttrListDef().addAll(schOfferAttrListDef);
		 schemeMessage.setSchEligibilityDef(schEligibilityDef.get());
		 schemeMessage.getSchEligibilityAttribInst().addAll(schEligibilityAttribInst);
		 schemeMessage.getSchEligiAttrListDef().addAll(schEligiAttrListDef);
		 
		 saveAndPublishServiceImpl.publishObjectToKafkaEvent(schemeMessage, 
				 schemeInstance.getBranchCode(),
						KafkaMapleEventType.SCHEMEMESSAGE, 
						KafkaMapleEventType.ALL,schemeInstance.getId());
		 
		
		
		
		return MapleConstants.SUCCESS;
	}



//	@Override
//	public void finalSchemePublish(String id, CompanyMst companyMst, String mybranch) {
//		
//		
//		SchemeInstance schemeInstance =new SchemeInstance();
//		
//		schemeInstance =schemeInstanceRepository.findByIdAndCompanyMst(id, companyMst);
//		
//		
//	}








}
