package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.PurchaseReport;

@Service
@Transactional
public interface PurchaseOrderReportService {

	
	 List<PurchaseReport> retrieveReportPurchaseOrder(String vouchernumber,String branchcode,Date date);
}
