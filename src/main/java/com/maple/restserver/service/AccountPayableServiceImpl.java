package com.maple.restserver.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AccountPayable;
import com.maple.restserver.entity.OwnAccount;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PaymentInvoiceDtl;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.AccountPayableRepository;
import com.maple.restserver.repository.OwnAccountRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.PaymentInvoiceDtlRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
public class AccountPayableServiceImpl implements AccountPayableService{

	@Autowired
	VoucherNumberService voucherNumberService;
	
	@Autowired
	OwnAccountRepository ownAccountRepo;
	@Autowired
	PaymentInvoiceDtlRepository paymentInvDtlRepo;
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	@Autowired
	PaymentHdrRepository paymentHdrRepo;
	@Autowired
	AccountPayableRepository accountPayableRepo;
	@Override
	public Double supplierBillAdjustment(Date rdate, String accountid,Double paidamount,String paymentHdr) {
		
		List<AccountPayable> accountPayableList = accountPayableRepo.getAccountPayableBySupId(accountid);
		for(int i=0;i<accountPayableList.size();i++)
		{
			if(paidamount>0)
			{
			AccountPayable accPayable = accountPayableList.get(i);
			Double balanceAmount = accPayable.getDueAmount()-accPayable.getPaidAmount();
			BigDecimal bdBalanceAmount = new BigDecimal(balanceAmount);
			bdBalanceAmount = bdBalanceAmount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
			balanceAmount= bdBalanceAmount.doubleValue();
			if(balanceAmount>=paidamount)
			{
				accPayable.setPaidAmount(accPayable.getPaidAmount()+paidamount);
				savePaymentInvoiceDtl(paidamount, accPayable, accountid, rdate, paymentHdr);
//			if(accPayable.getDueAmount()>=paidamount)
//			{
//				
				
//				if((accPayable.getPaidAmount()+paidamount)<accPayable.getDueAmount())
//				{
//				BigDecimal bdPaidAmount = new BigDecimal(accPayable.getPaidAmount()+paidamount);
//				bdPaidAmount = bdPaidAmount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
//				accPayable.setPaidAmount(bdPaidAmount.doubleValue());
//				
////				BigDecimal bdDueAmount = new BigDecimal(accPayable.getDueAmount()-paidamount);
////				bdDueAmount = bdDueAmount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
////				accPayable.setDueAmount(bdDueAmount.doubleValue());
//				
//				savePaymentInvoiceDtl(paidamount, accPayable, accountid, rdate, paymentHdr);
//				paidamount = 0.0;
//				}
//				else
//				{
//					accPayable.setPaidAmount(accPayable.getDueAmount());
//					
////					BigDecimal bdDueAmount = new BigDecimal(accPayable.getDueAmount()-paidamount);
////					bdDueAmount = bdDueAmount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
////					accPayable.setDueAmount(bdDueAmount.doubleValue());
//					
//					savePaymentInvoiceDtl(paidamount, accPayable, accountid, rdate, paymentHdr);
//					paidamount = 0.0;
//				}
				paidamount=0.0;
			}
			else 
			{
				paidamount= paidamount-(accPayable.getDueAmount()-accPayable.getPaidAmount());
				accPayable.setPaidAmount(accPayable.getDueAmount());
				savePaymentInvoiceDtl(balanceAmount, accPayable, accountid, rdate, paymentHdr);
				BigDecimal bdPaidamount = new BigDecimal(paidamount);
				bdPaidamount = bdPaidamount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
				paidamount=bdPaidamount.doubleValue();
			}
//			{
//				
//				if(accPayable.getPaidAmount()+paidamount>accPayable.getDueAmount())
//				{
//					paidamount= paidamount-(accPayable.getDueAmount()-accPayable.getPaidAmount());
//					accPayable.setPaidAmount(accPayable.getDueAmount());
//					BigDecimal bpaidamount = new BigDecimal(paidamount);
//					bpaidamount = bpaidamount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
//					paidamount = bpaidamount.doubleValue();
//					savePaymentInvoiceDtl(accPayable.getDueAmount(), accPayable, accountid, rdate, paymentHdr);
////				
//				}
//				else
//				{
//				BigDecimal bdPaidAmount = new BigDecimal(accPayable.getPaidAmount()+paidamount);
//				bdPaidAmount = bdPaidAmount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
//				accPayable.setPaidAmount(bdPaidAmount.doubleValue());
//				paidamount = paidamount-accPayable.getDueAmount();
//				BigDecimal bpaidamount = new BigDecimal(paidamount);
//				bpaidamount = bpaidamount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
//				paidamount = bpaidamount.doubleValue();
//				savePaymentInvoiceDtl(accPayable.getDueAmount(), accPayable, accountid, rdate, paymentHdr);
////				accPayable.setDueAmount(0.0);
//				}
//
//				
//
//			}
			accPayable = accountPayableRepo.save(accPayable);
		
			}

			else
			{
				break;
			}
		}
		if(paidamount > 0)
		{
			saveToOwnAccount(rdate, paymentHdr, accountid, paidamount);
		}
		return paidamount;
	}

	private void savePaymentInvoiceDtl(Double paidamount,AccountPayable accPayable,String accountid,Date rdate,String paymentHdr)
	{
		PaymentInvoiceDtl paymentInvDtl= new PaymentInvoiceDtl();
		paymentInvDtl.setCompanyMst(accPayable.getCompanyMst());
		paymentInvDtl.setInvoiceNumber(accPayable.getVoucherNumber());
		paymentInvDtl.setPaidAmount(paidamount);
		Optional<PaymentHdr> paymentHdrOp  = paymentHdrRepo.findById(paymentHdr);
		paymentInvDtl.setPaymentHdr(paymentHdrOp.get());
		AccountHeads accountHeads = accountHeadsRepository.findById(accountid).get();
		paymentInvDtl.setAccountHeads(accountHeads);
		paymentInvDtl.setVoucherDate(rdate);
		paymentInvDtlRepo.save(paymentInvDtl);
	}
	private void saveToOwnAccount(Date vdate,String paymentHdr,String accountId,Double paidamount)
	{
		Optional<PaymentHdr> paymentHdrOp  = paymentHdrRepo.findById(paymentHdr);

		OwnAccount ownAccount = new OwnAccount();
		ownAccount.setAccountId(accountId);
		ownAccount.setAccountType("SUPPLIER");
		ownAccount.setCompanyMst(paymentHdrOp.get().getCompanyMst());
		ownAccount.setCreditAmount(0.0);
		ownAccount.setDebitAmount(paidamount);
		ownAccount.setOwnAccountStatus("PENDING");
		ownAccount.setPaymenthdr(paymentHdrOp.get());
		ownAccount.setRealizedAmount(0.0);
		ownAccount.setVoucherDate(SystemSetting.UtilDateToSQLDate(vdate));
		VoucherNumber vno = voucherNumberService.generateInvoice("OWNACC",paymentHdrOp.get().getCompanyMst().getId());
		ownAccount.setVoucherNo(vno.getCode());
		ownAccountRepo.save(ownAccount);
	}
}
