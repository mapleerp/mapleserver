package com.maple.restserver.service;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;

@Service
public interface InstallationModelClassService {

	void InstallationModelFileCreation(CompanyMst companyMst);

	void FileInstallation(CompanyMst companyMst);
	
	

}
