package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.ReceiptInvoiceDtl;

@Component
public interface ReceiptInvoiceDtlService {

	
	 List<ReceiptInvoiceDtl> findByCustomerMstIdAndOwnAccount(String  customerMst);
}
