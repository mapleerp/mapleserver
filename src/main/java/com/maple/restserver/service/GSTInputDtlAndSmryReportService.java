package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.GSTInputDtlAndSmryReport;



@Service
public interface GSTInputDtlAndSmryReportService {

	
	List<GSTInputDtlAndSmryReport> findGstInputDtlAndSmry(Date fdate, Date tdate);
}
