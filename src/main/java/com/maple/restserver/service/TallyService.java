package com.maple.restserver.service;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.SalesTransHdr;

@Component

public interface TallyService {

	
	void monthlySalesSummaryToTally(Date fromDate,Date toDate);

	String salesToTallyByVoucherNoAndVoucherDate(String voucherno, java.sql.Date date);

	boolean salesToTallyBySalesTranshdr(SalesTransHdr salesTransHdr);

	boolean purchaseToTallyByPurchaseHdr(PurchaseHdr purchaseHdr);

	boolean receiptToTallyByReceiptHdr(ReceiptHdr receiptHdr);

	boolean PaymentsToTallyByPaymentHdr(PaymentHdr paymentHdr);

	boolean JournalToTallyJournalHdr(JournalHdr journalHdr);
}
