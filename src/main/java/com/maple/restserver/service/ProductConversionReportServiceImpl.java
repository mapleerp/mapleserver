package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.ProductConversionReport;
import com.maple.restserver.repository.ProductConversionDtlRepository;
import com.maple.restserver.repository.ProductConversionMstRepository;

@Service
@Transactional
@Component

public class ProductConversionReportServiceImpl implements ProductConversionReportService {
	

	@Autowired
	ProductConversionMstRepository productConversionmstrepository;
	
	@Autowired
	ProductConversionDtlRepository productconversiondtlrepository;

	@Override
	public List<ProductConversionReport>fetchProductConversionReport ( String companymstid, String branchcode, Date fDate, Date TDate) {
	
		List<ProductConversionReport> productionReportList =   new ArrayList();
		
		
		 List<Object> obj = productConversionmstrepository.getproductconversionreport(companymstid,branchcode,fDate,TDate); 
		 
				
		 
		   for(int i=0;i<obj.size();i++)
			 {
			  Object[] objAray = (Object[])obj.get(i);
			  ProductConversionReport report=new   ProductConversionReport();
			  report.setFromItemName((String) objAray[0]);
			  report.setFromItemQty((Double) objAray[2]);
			  report.setFromItemStdPrice((Double) objAray[1]);
			  report.setFromAmount(report.getFromItemQty()*report.getFromItemStdPrice());
			  report.setToItemName((String) objAray[5]);
			  report.setToItemStdPrice((Double) objAray[6]);
			  report.setToItemQty((Double) objAray[7]);
			  report.setToAmount(report.getToItemQty()*report.getToItemStdPrice());
		      report.setDate((Date) objAray[8]);
			  productionReportList.add(report);
			 }
			 
			 return productionReportList;
		
	}

}
