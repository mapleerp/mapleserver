package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.InvoiceEditEnableRepository;
import com.maple.restserver.repository.TenantyMstRepository;

@Service
public class InvoiceEditEnableServiceImpl implements InvoiceEditEnableService{

	@Autowired
	InvoiceEditEnableRepository invoiceEditEnableRepository;
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	TenantyMstRepository tenantyMstRepository;
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	TenantyMstService tenantyMstService;

	@Override
	public List<Object> findPaymentByCompanyMstIdAndVoucherDate(String companymstid, String branchcode, Date fdate,
			String userId) {
		
		
		List<Object> objList = new ArrayList<Object>();
	
		
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

		boolean tenantyValue = tenantyMstService.checkTenantyMst(companyMst.get());

		if (tenantyValue) {
			objList = invoiceEditEnableRepository.getAllPaymentVoucherNumbers(companyMst.get());
		} else {
			objList =  invoiceEditEnableRepository.findPaymentByCompanyMstIdAndVoucherDate(companymstid, branchcode, fdate,
					userId);
		}

		return objList;
		
		
	}
	
}
