package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.ProductionBatchStockDtl;



@Service
public interface ProductionBatchStockDtlService {

	List<ProductionBatchStockDtl>getProductionBatchStockDtl(String companymstid, String itemid);
	Integer getProductionRowMaterialCount(String companymstid,String itemid);
}
