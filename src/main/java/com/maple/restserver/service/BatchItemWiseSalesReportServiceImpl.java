package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.report.entity.BatchItemWiseSalesReport;
import com.maple.restserver.report.entity.BatchWiseCustomerSalesReport;
import com.maple.restserver.repository.SalesTransHdrRepository;

@Service
@Transactional
public class BatchItemWiseSalesReportServiceImpl implements BatchItemWiseSalesReportService{

	
	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Override
	public List<BatchItemWiseSalesReport> itemWiseSalesReport(String companymstid, String branchcode, Date fDate,
			Date TDate, List<String> itemList) {
		List<BatchItemWiseSalesReport> reportList=new ArrayList<BatchItemWiseSalesReport>();
		for(String itemId:itemList) {
			
			List<Object>objList=salesTransHdrRepository.itemwisesalesreport( companymstid,  branchcode,fDate,TDate,itemId);
			 
			  for(int i=0;i<objList.size();i++)
				 {
				Object[] objAray = (Object[]) objList.get(i);
				BatchItemWiseSalesReport report=new BatchItemWiseSalesReport();
				
				  report.setCustomerName((String) objAray[4]);
				  report.setCustomerName((String) objAray[1]);
				  report.setMrp((Double) objAray[3]);
				  report.setQty((Double) objAray[2]);
				  report.setVoucherDate((java.sql.Date)objAray[0]);
				  reportList.add(report);
				 }
		  }
			
		return reportList;
	}
}
