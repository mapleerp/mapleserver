package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.StockTransferInDtl;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.report.entity.StockTransferInReport;
import com.maple.restserver.report.entity.StockTransferOutReport;

public interface StockTransferInDtlService {

	List<StockTransferInReport> getTransferInDetailReport(CompanyMst companyMst, String branchcode, Date fdate,
			Date tdate);

	List<StockTransferInReport> getTransferInDetailReportByFromBranch(CompanyMst companyMst, String branchcode,
			String frombranchcode, Date fdate, Date tdate);

	List<StockTransferInReport> getTransferInDetailReportByFromBranch(CompanyMst companyMst, String branchcode,
			String frombranch, String[] array , Date fdate, Date tdate);

	List<StockTransferInReport> getTransferInDetailReportByCategory(CompanyMst companyMst, String branchcode,
			String[] array, Date fdate, Date tdate);

	


}
