package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.TaxReport;

@Service
public interface TaxReportService {

	List<TaxReport> getTaxReport (Date sdate, Date edate,
			CompanyMst companyMst,String branchCode);
	
	List<TaxReport> getPurchaseTaxReport (Date sdate, Date edate,
			CompanyMst companyMst,String branchCode);
	
}
