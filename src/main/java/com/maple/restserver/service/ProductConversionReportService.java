package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.report.entity.ProductConversionReport;

public interface ProductConversionReportService {
 List<ProductConversionReport> fetchProductConversionReport (String companymstid,String branchcode,Date fDate,Date DaTDate);
 

}
