package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.LastBestDate;


public interface DataSynchronisationService {

	
	public int getLocalCount();
	
	public void countComparAndRetry();
	
	public String retry();
	
	public LastBestDate getLastBestDate();
	
}
