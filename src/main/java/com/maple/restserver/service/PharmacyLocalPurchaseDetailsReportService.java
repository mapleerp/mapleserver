package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.PharmacyLocalPurchaseDetailsReport;

@Service

@Component
public interface PharmacyLocalPurchaseDetailsReportService {

	List<PharmacyLocalPurchaseDetailsReport> getPharmacyLocalPurchaseDetailsReport(Date fromDate, Date toDate);

}
