package com.maple.restserver.service;

import java.sql.Date;
import java.util.ArrayList;
 
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AccountPayable;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LastBestDate;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.PurchaseHdrMessageEntity;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesMessageEntity;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.AccountPayableRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LastBestDateRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.utils.SystemSetting;

@Component
@Primary
public class PurchaseDataSync implements DataSynchronisationService {

	String branchCode;
	String companyMstId;
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	BranchMstRepository branchMstRepository;
 
	
	@Autowired
	AccountPayableRepository accountPayableRepo;
	
	@Autowired
	DayEndClosureRepository dayEndClosureRepository;
	
	@Autowired
	PurchaseDtlRepository purchaseDtlRepo;
	
	
	@Autowired
	PurchaseService purchaseService;
	
	
	@Autowired
	CategoryMstRepository categoryRepo;
	
	
	@Autowired
	PurchaseHdrRepository purchaseHdrRepo;
	@Autowired
	UnitMstRepository unitMstRepo;
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	@Autowired
	ExternalApi externalApi;
	
	@Autowired
	ItemMstRepository itemMstRepo;
	
	@Autowired
	LastBestDateRepository lastBestDateRepository;
	
	@Autowired
	LastBestDateServicePurchase lastBestDateServicePurchase;
	

	private int  purchaseCount = 0;
	private LastBestDate lastBestDate =null ;
	
	private CloudDataCount cloudDataCount;
	
	
	public CloudDataCount getCloudDataCount() {
		return cloudDataCount;
	}

	public void setCloudDataCount(CloudDataCount cloudDataCount) {
		this.cloudDataCount = cloudDataCount;
	}
 
	@Override
	public int getLocalCount() {
		
		System.out.println("Entered into PurchaseDataSync.getLocalCount() method****************************");
		
		if(null == lastBestDate)
		{
			return 0;
		}
	
		purchaseCount = purchaseHdrRepo.getCountofPurchaseForDateWindow(
				companyMstId,
				lastBestDate.getLastSuccessDate(),
				lastBestDate.getLastSuccessDate(),branchCode);
		
		
		return purchaseCount;
	}

	@Override
	public void countComparAndRetry() {
		
		System.out.println("Entered into PurchaseDataSync.countComparAndRetry() method****************************");
		
		boolean canRetry=false;
		
		 
		Double serverDataCountInDouble =cloudDataCount.getServerCount("PURCHASE");
		int serverDataCount = serverDataCountInDouble.intValue();
		
		int  localDataCount = getLocalCount();
		

		System.out.println("The Local Server count of purchase are ====> " + localDataCount);
		System.out.println("The Cloud Server count of purchase are ====> " + serverDataCount);
		
		canRetry = serverDataCount < localDataCount;
		
		if(canRetry) {
			retry();
			
		}else {
			updateLastBestDate();
			
		}
		
		 
	}

  
	
	@Override
	public String retry() {
		
		int pageNo = 0;
		int pageSize = 50;
		
		System.out.println("Entered into PurchaseDataSync.retry() method****************************");
		System.out.println("Building Purchase MsgEntity Starting");
		
		String date = SystemSetting.UtilDateToString(lastBestDate.getLastSuccessDate(), "yyyy-MM-dd");

		
		String hdrIdsFromCloud = externalApi.PurchaseHdrIdsFromCloud(date);
		
		if(null == hdrIdsFromCloud)
		{
			hdrIdsFromCloud ="";
		}
		
		while (true) {

			Pageable topFifty = PageRequest.of(pageNo, pageSize);


		List<PurchaseHdr> hdrIdS = purchaseHdrRepo.findPurchaseHdrBetweenVoucherDate
				(lastBestDate.getLastSuccessDate(), lastBestDate.getDayEndDate(),topFifty,hdrIdsFromCloud);

		if(hdrIdS.size() == 0)
		{
			break;
		}
		
		ArrayList<PurchaseHdrMessageEntity> purchaseMessageEntityArray = new ArrayList<PurchaseHdrMessageEntity>();

		Iterator iter = hdrIdS.iterator();

		Integer recordCount = 0, i = 0;

		while (iter.hasNext()) {

			i = i + 1;

			recordCount++;

			ArrayList<ItemMst> itemList = new ArrayList<ItemMst>();

			ArrayList<CategoryMst> catList = new ArrayList<CategoryMst>();

			ArrayList<UnitMst> unitList = new ArrayList<UnitMst>();

			PurchaseHdr transHdr = (PurchaseHdr) iter.next();

			PurchaseHdrMessageEntity purchaseMessageEntity = new PurchaseHdrMessageEntity();

			purchaseMessageEntity.setPurchaseHdr(transHdr);

			List<PurchaseDtl> purchaseDtl = purchaseDtlRepo.findByPurchaseHdrId(transHdr.getId());

			purchaseMessageEntity.getPurchaseDtlList().addAll(purchaseDtl);

			for (PurchaseDtl pur : purchaseDtl) {

				ItemMst itemmst = itemMstRepo.findById(pur.getItemId()).get();

				itemList.add(itemmst);

				CategoryMst catmst = categoryRepo.findById(itemmst.getCategoryId()).get();

				catList.add(catmst);

				UnitMst unitmst = unitMstRepo.findById(itemmst.getUnitId()).get();

				unitList.add(unitmst);

			}

			purchaseMessageEntity.getItemList().addAll(itemList);

			purchaseMessageEntity.getCategoryList().addAll(catList);

			purchaseMessageEntity.getUnitMstList().addAll(unitList);

			purchaseMessageEntity.setItemCount(purchaseDtl.size());

			AccountPayable accPayable = accountPayableRepo.findByCompanyMstAndPurchaseHdr(transHdr.getCompanyMst(),

					transHdr);

			purchaseMessageEntity.getAccountPayablelist().add(accPayable);

			Optional<AccountHeads> sup = accountHeadsRepository.findById(transHdr.getSupplierId());

			if (sup.isPresent()) {

				purchaseMessageEntity.getAccountHeadsList().add(sup.get());

			}

			purchaseMessageEntityArray.add(purchaseMessageEntity);

			System.out.println("Building Purchase Msg Entity recordCount" + recordCount);

			if (i >= 500) {

				List<String> message = externalApi.BulkPurchaseToCloud(purchaseMessageEntityArray, lastBestDate.getCompanyMst().getId());

				i = 0;

				purchaseMessageEntityArray.clear();

			}

		}

		System.out.println("Send purchase");

		List<String> message = externalApi.BulkPurchaseToCloud(purchaseMessageEntityArray, lastBestDate.getCompanyMst().getId());
//		if(!message.isEmpty()) {
//
//			purchaseService.updatePurchasePostedToServerStatus(message);
//
//		}

		pageNo++;
		
		System.out.println("The result of externalApi.BulkPurchaseToCloud() in PurchaseDataSync.retry() ====> " + message);

		}
		return "OK";
	}
	
	
	private void updateLastBestDate() {
		
		System.out.println("Entered into PurchaseDataSync.updateLastBestDate() method****************************");
	 
//		lastBestDate.setLastSuccessDate(lastBestDate.getDayEndDate());

		System.out.println("The result of PurchaseDataSync.updateLastBestDate() ====> " + lastBestDate);
		if(null == lastBestDate)
		{
			return;
		}
			 
			lastBestDateRepository.save(lastBestDate);

		 

	}
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCompanyMstId() {
		return companyMstId;
	}

	public void setCompanyMstId(String companyMstId) {
		this.companyMstId = companyMstId;
	}
 

 
	@Override
	public LastBestDate getLastBestDate() {
		
		System.out.println("Entered into purchaseDataSync.getLastBestDate() method****************************");
		
		lastBestDateServicePurchase.setBranchcode(branchCode);
		lastBestDateServicePurchase.setCompanymstid(companyMstId);
		lastBestDate = lastBestDateServicePurchase.findLastBestDate();

		System.out.println("The result of purchaseDataSync.getLastBestDate() method ====> " + lastBestDate);
		return lastBestDate;
	}
	 
}
