package com.maple.restserver.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.KotCategoryInventoryMst;

@Service
@Transactional
public interface KotCategoryInventoryMstService {


	List<KotCategoryInventoryMst> findByParentItem(String companymstid);

	List<KotCategoryInventoryMst> findByCompanyMstIdAndParentItem(String companymstid, String parentitem);
}
