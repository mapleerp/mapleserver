package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.report.entity.BatchwiseSalesTransactionReport;
import com.maple.restserver.repository.SalesTransHdrRepository;

@Service
@Transactional
public class BatchwiseDailySalesTransactionReportServiceImpl implements BatchwiseDailySalesTransactionReportService  {
@Autowired
SalesTransHdrRepository salesTransHdrRepository;
	
	
	@Override
	public List<BatchwiseSalesTransactionReport> batchwiseSalesTransactionReport(String companymstid, String branchcode,
			Date fDate, Date TDate, List<String> groupList) {
		  List<BatchwiseSalesTransactionReport> reportList=new ArrayList<BatchwiseSalesTransactionReport>();
	  for(String catId:groupList) {
		  List<Object>objList=salesTransHdrRepository.batchwiseSalesTransactionReport( companymstid,  branchcode,fDate,TDate,catId);
	 
		  for(int i=0;i<objList.size();i++)
			 {
			Object[] objAray = (Object[]) objList.get(i);
			BatchwiseSalesTransactionReport report=new BatchwiseSalesTransactionReport();
			report.setVouchernumber((String) objAray[0]);
			report.setInvoiceAmount((Double) objAray[1]);
			report.setInvoiceDiscount((Double) objAray[2]);
			report.setCustomername((String) objAray[3]);
			report.setTinnumber((String) objAray[4]);
			report.setSalesman((String) objAray[5]);
			reportList.add(report);
			 }
	  }
		
		return reportList;
	}

}
