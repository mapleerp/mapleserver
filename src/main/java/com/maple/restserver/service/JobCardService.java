package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JobCardHdr;
import com.maple.restserver.report.entity.ActiveJobCardReport;

public interface JobCardService {

	List<ActiveJobCardReport> findAllActiveJobCardReport(CompanyMst companyMst, Date sdate, Date edate);



}
