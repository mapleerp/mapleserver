package com.maple.restserver.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.OwnAccount;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentInvoiceDtl;
import com.maple.restserver.entity.ReceiptInvoiceDtl;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;

import com.maple.restserver.repository.DayBookRepository;
import com.maple.restserver.repository.OwnAccountRepository;
import com.maple.restserver.repository.PaymentInvoiceDtlRepository;
import com.maple.restserver.repository.ReceiptDtlRepository;
import com.maple.restserver.repository.ReceiptInvoiceDtlRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;

@Service
@Transactional
public class PaymentEditServiceImpl implements PaymentEditService{

	
	@Autowired
	DayBookRepository dayBookRepo;
	
	@Autowired
	private ReceiptDtlRepository receiptDtlRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	VoucherNumberService voucherNumberService;
	
	@Autowired
	OwnAccountRepository ownAccountRepo;
	
	@Autowired
	ReceiptInvoiceDtlRepository receiptInvoiceDtlRepo;
	
	@Autowired
	PaymentInvoiceDtlRepository paymentInvoiceDtlRepository;
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	@Override
	public void deletePaymentAndOwnAccount(String companymstid, String paymentHdrId, PaymentDtl paymentDtls) {
		
		
		
		
		 List<OwnAccount> getOwn = ownAccountRepo.findByCompanyMstIdAndReceiptHdrId(companymstid, paymentHdrId);
			
			if(getOwn.size()>0)
	    	{
				ownAccountRepo.deleteByPaymenthdrId(paymentHdrId);
				Optional<AccountHeads> custSaved = accountHeadsRepository.findById(paymentDtls.getAccountId());
				if(null != custSaved.get())
				{
					OwnAccount ownAccount = new OwnAccount();
					ownAccount.setAccountId(paymentDtls.getAccountId());
					ownAccount.setAccountType("CUSTOMER");
					ownAccount.setCreditAmount(0.0);
					ownAccount.setDebitAmount(paymentDtls.getAmount());
					ownAccount.setOwnAccountStatus("PENDING");
					ownAccount.setRealizedAmount(0.0);
					String sdtae = ClientSystemSetting.UtilDateToString(paymentDtls.getTransDate());
					System.out.println(sdtae);
					ownAccount.setVoucherDate(ClientSystemSetting.StringToSqlDate(sdtae,"yyyy-MM-dd"));
					ownAccount.setVoucherNo(voucherNumberService.generateInvoice("OWNACC",custSaved.get().getCompanyMst().getId()).getCode());
					ownAccount.setPaymenthdr(paymentDtls.getPaymenthdr());
					Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
					ownAccount.setCompanyMst(companymst.get());
					ownAccountRepo.save(ownAccount);
				
				}
				
	    	}
			List<PaymentInvoiceDtl> getPayInv = paymentInvoiceDtlRepository.findByPaymentHdr(paymentDtls.getPaymenthdr());
			if(getPayInv.size()>0)
			{
				paymentInvoiceDtlRepository.deleteByPaymentHdrId(paymentHdrId);
				 List<OwnAccount> getOwn1 = ownAccountRepo.findByCompanyMstIdAndReceiptHdrId(companymstid, paymentHdrId);
				 if(getOwn1.size()==0)
				 {
						Optional<AccountHeads> custSaved = accountHeadsRepository.findById(paymentDtls.getAccountId());
						if(null != custSaved.get())
						{
							OwnAccount ownAccount = new OwnAccount();
							ownAccount.setAccountId(paymentDtls.getAccountId());
							ownAccount.setAccountType("CUSTOMER");
							ownAccount.setCreditAmount(0.0);
							ownAccount.setDebitAmount(paymentDtls.getAmount());
							ownAccount.setOwnAccountStatus("PENDING");
							ownAccount.setRealizedAmount(0.0);
							String sdtae = ClientSystemSetting.UtilDateToString(paymentDtls.getTransDate());
							System.out.println(sdtae);
							ownAccount.setVoucherNo(voucherNumberService.generateInvoice("OWNACC",custSaved.get().getCompanyMst().getId()).getCode());
							ownAccount.setVoucherDate(ClientSystemSetting.StringToSqlDate(sdtae,"yyyy-MM-dd"));						
							ownAccount.setPaymenthdr(paymentDtls.getPaymenthdr());
							Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
							ownAccount.setCompanyMst(companymst.get());
							ownAccountRepo.save(ownAccount);
			    		
						}
				 }
			}

	}

}
