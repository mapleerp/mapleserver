package com.maple.restserver.service;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReceiptModeMst;
import com.maple.restserver.report.entity.DamageEntryReport;
import com.maple.restserver.report.entity.SaleModeWiseSummaryReport;
import com.maple.restserver.report.entity.WriteOffDetailsAndSummaryReport;
import com.maple.restserver.repository.DamageDtlRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component
public class DamageEntryReportServiceImpl implements DamageEntryReportServive {
	@Autowired
	DamageDtlRepository damageDtlRepository;

	

	@Override
	public List<DamageEntryReport> getDamageEntryReport(CompanyMst companymst, java.util.Date fromdate,
			java.util.Date todate, String branchCode) {
		List<DamageEntryReport> damageEntryList= new ArrayList<DamageEntryReport>();
		List<Object> obj =damageDtlRepository.getDamageEntryReport(companymst,fromdate,todate,branchCode);
		
		for(int i=0;i<obj.size();i++)
		 {
			
		Object[] objAray = (Object[]) obj.get(i);
		DamageEntryReport dSummary=new DamageEntryReport();
		dSummary.setCompanyName((String) objAray[0]);
		dSummary.setBranchName((String) objAray[1]);
		dSummary.setBranchCode((String) objAray[2]);
		dSummary.setItemName((String) objAray[3]);
		dSummary.setBatchCode((String) objAray[4]);
		dSummary.setQty((Double) objAray[5]);
		dSummary.setFromDate((fromdate));
		dSummary.setToDate((todate));
		dSummary.setVoucherDate((Date)objAray[6]);
		dSummary.setNarration((String) objAray[7]);
		damageEntryList.add(dSummary);
	}

		return damageEntryList;
		
	}
	
	
	@Override
	public List<WriteOffDetailsAndSummaryReport> getWriteOffDetailAndSummary(CompanyMst companyMst, Date fDate, Date tDate,
			String branchCode, String reason) {
		List<WriteOffDetailsAndSummaryReport> writeOffDetailsAndSummaryReportList= new ArrayList<WriteOffDetailsAndSummaryReport>();
		List<Object> obj =damageDtlRepository.getWriteOffDetailsAndSummaryReport(companyMst,fDate,tDate,branchCode,reason);
		
		for(int i=0;i<obj.size();i++)
		 {
			
		Object[] objAray = (Object[]) obj.get(i);
		WriteOffDetailsAndSummaryReport writeOffDetailsAndSummaryReport=new WriteOffDetailsAndSummaryReport();
		writeOffDetailsAndSummaryReport.setItemName((String) objAray[0]);
		writeOffDetailsAndSummaryReport.setItemCode((String) objAray[1]);
		writeOffDetailsAndSummaryReport.setItemGroup((String) objAray[6]);
		writeOffDetailsAndSummaryReport.setVoucherNumber((String) objAray[9]);
		writeOffDetailsAndSummaryReport.setDate(SystemSetting.UtilDateToString((Date) objAray[8], "yyyy-MM-dd"));
		writeOffDetailsAndSummaryReport.setAmount((Double) objAray[5]);
		writeOffDetailsAndSummaryReport.setBatch((String) objAray[2]);
		writeOffDetailsAndSummaryReport.setDepartment("Null");
		writeOffDetailsAndSummaryReport.setExpiryDate(SystemSetting.UtilDateToString((Date) objAray[7], "yyyy-MM-dd"));
		writeOffDetailsAndSummaryReport.setQuantity((Double) objAray[4]);
		writeOffDetailsAndSummaryReport.setRate((Double) objAray[3]);
		writeOffDetailsAndSummaryReport.setUser("Null");
		writeOffDetailsAndSummaryReport.setRemarks((String) objAray[10]);
		writeOffDetailsAndSummaryReportList.add(writeOffDetailsAndSummaryReport);
	}

		return writeOffDetailsAndSummaryReportList;
		
	}
		
	}
	

