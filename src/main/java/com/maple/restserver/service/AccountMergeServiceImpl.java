package com.maple.restserver.service;


import java.util.Date;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.accounting.entity.AccountMergeMst;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.DeletedAccountClass;
import com.maple.restserver.repository.AccountHeadsRepository;


import com.maple.restserver.repository.AccountPayableRepository;
import com.maple.restserver.repository.AccountReceivableRepository;

import com.maple.restserver.repository.AccountMergeRepository;


import com.maple.restserver.repository.AccountMergeRepository;

import com.maple.restserver.repository.AccountPayableRepository;
import com.maple.restserver.repository.AccountReceivableRepository;


import com.maple.restserver.repository.DeletedAccountClassRepository;
import com.maple.restserver.repository.OtherBranchSalesTransHdrRepository;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentInvoiceDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.PurchaseOrderHdrRepository;
import com.maple.restserver.repository.PurchaseSchemeMstRepository;
import com.maple.restserver.repository.ReceiptDtlRepository;
import com.maple.restserver.repository.ReceiptInvoiceDtlRepository;
import com.maple.restserver.repository.SaleOrderReceiptRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesReturnHdrRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
public class AccountMergeServiceImpl implements AccountMergeService {


	@Autowired
	SaleOrderReceiptRepository saleOrderReceiptRepository;

	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;
	
	@Autowired
	ReceiptDtlRepository receiptDtlRepository;
	@Autowired 
	PaymentDtlRepository paymentDtlRepository;
	
	@Autowired
	ReceiptInvoiceDtlRepository receiptInvoiceDtlRepository;
	
	@Autowired
	PaymentInvoiceDtlRepository paymentInvoiceDtlRepository;
	
	@Autowired
	SalesReturnHdrRepository salesReturnHdrRepository;
	
	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepository;
	
	@Autowired
	PurchaseOrderHdrRepository purchaseOrderHdrRepository;
	
	@Autowired
	OtherBranchSalesTransHdrRepository  otherBranchSalesTransHdrRepository;
	
	@Autowired
	PurchaseSchemeMstRepository purchaseSchemeMstRepository;
	
	@Autowired
	DebitClassRepository debitClassRepository;
	
	@Autowired
	LedgerClassRepository  ledgerClassRepository;
	@Autowired
	SalesReceiptsRepository  salesReceiptsRepository;
	
	@Autowired
    CreditClassRepository creditClassRepository;
	@Autowired
	DeletedAccountClassRepository  deletedAccountClassRepository;
	
	@Autowired
	AccountReceivableRepository accountReceivableRepository;
	
	@Autowired
	AccountPayableRepository accountPayableRepository;
	
	
	/*
	 * @Autowired SupplierRepository supplierRepository;
	 * 
	 * @Autowired CustomerMstRepository customerMstRepository;
	 */
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	
	@Autowired
	AccountMergeRepository accountMergeRepository;
	
	
	@Transactional
	@Override
	public String accountMerge(String fromAccountId, String toAccountId) {
		
		AccountMergeMst accountMergeMst =new AccountMergeMst();
		Optional<AccountHeads> accountHeads =Optional.empty();
		
		
		salesTransHdrRepository.accountMerge(fromAccountId,  toAccountId);
		
		
		purchaseHdrRepository.accountMerge(fromAccountId,  toAccountId);
		
		
		receiptDtlRepository.accountMerge(fromAccountId,  toAccountId);
		paymentDtlRepository.accountMerge(fromAccountId,  toAccountId);
		
		receiptInvoiceDtlRepository.accountMerge(fromAccountId,  toAccountId);
		
		paymentInvoiceDtlRepository.accountMerge(fromAccountId,  toAccountId);
		
		salesReturnHdrRepository.accountMerge(fromAccountId,  toAccountId);
		
		salesOrderTransHdrRepository.accountMerge(fromAccountId,  toAccountId);
		
		purchaseOrderHdrRepository.accountMerge(fromAccountId,  toAccountId);
		
		otherBranchSalesTransHdrRepository.accountMerge(fromAccountId,  toAccountId);
		
		purchaseSchemeMstRepository.accountMerge(fromAccountId,  toAccountId);
	
		salesReceiptsRepository.accountMerge(fromAccountId,  toAccountId);
		
		
		creditClassRepository.accountMerge(fromAccountId, toAccountId);
		
		debitClassRepository.accountMerge(fromAccountId, toAccountId);
		
		ledgerClassRepository.accountMerge(fromAccountId, toAccountId);
		
		accountReceivableRepository.accountMerge(fromAccountId, toAccountId);
		
		accountPayableRepository.accountMerge(fromAccountId, toAccountId);
		
		DeleteAccountAndSaveToDeletedAccountClass(fromAccountId);
		

		accountHeads =accountHeadsRepository.findById(fromAccountId);
		accountMergeMst.setFromAccountName(accountHeads.get().getAccountName());
		accountHeads =accountHeadsRepository.findById(toAccountId);
		accountMergeMst.setToAccountName(accountHeads.get().getAccountName());
		accountMergeMst.setFromAccountId(fromAccountId);
		accountMergeMst.setToAccountId(toAccountId);
		accountMergeMst.setCompanyMst(accountHeads.get().getCompanyMst());
		
		Date updateDate= new Date();
		
        accountMergeMst.setUpdatedDate(updateDate);
		
        accountMergeRepository.save(accountMergeMst);


		
		return "sucesses";
	}
	@Transactional
	@Override
	public void DeleteAccountAndSaveToDeletedAccountClass(String accountId) {
	
		
		/*
		 * Optional<com.maple.restserver.entity.Supplier> SupplierOpt=
		 * supplierRepository.findById(accountId); if(SupplierOpt.isPresent()) {
		 * DeletedAccountClass deletedAccountClassSupplier=new DeletedAccountClass();
		 * deletedAccountClassSupplier.setAccountId(SupplierOpt.get().getId());
		 * deletedAccountClassSupplier.setAccountName(SupplierOpt.get().getSupplierName(
		 * )); deletedAccountClassSupplier.setAccountType("Supplier");
		 * deletedAccountClassSupplier.setCompanyMst(SupplierOpt.get().getCompanyMst());
		 * deletedAccountClassRepository.save(deletedAccountClassSupplier);
		 * deletedAccountClassRepository.deleteMergedSupplier(accountId); }
		 * 
		 * Optional<CustomerMst>
		 * customerMstOpt=customerMstRepository.findById(accountId);
		 * 
		 * if(customerMstOpt.isPresent()) {
		 * 
		 * DeletedAccountClass deletedAccountClassCustomer=new DeletedAccountClass();
		 * deletedAccountClassCustomer.setAccountId(customerMstOpt.get().getId());
		 * deletedAccountClassCustomer.setAccountName(customerMstOpt.get().
		 * getCustomerName());
		 * deletedAccountClassCustomer.setCompanyMst(customerMstOpt.get().getCompanyMst(
		 * )); deletedAccountClassCustomer.setAccountType("customer");
		 * deletedAccountClassRepository.save(deletedAccountClassCustomer);
		 * deletedAccountClassRepository.deleteMergedCustomer(accountId);
		 * 
		 * }
		 */
	Optional<AccountHeads> accountHeadsOpt=accountHeadsRepository.findById(accountId);
	
	   if(accountHeadsOpt.isPresent()) {
		DeletedAccountClass deletedAccountClassAccountHeads=new DeletedAccountClass();
		deletedAccountClassAccountHeads.setAccountId(accountHeadsOpt.get().getId());
		deletedAccountClassAccountHeads.setCompanyMst(accountHeadsOpt.get().getCompanyMst());
		deletedAccountClassAccountHeads.setAccountType("Account Heads");
		deletedAccountClassAccountHeads.setAccountName(accountHeadsOpt.get().getAccountName());
		deletedAccountClassRepository.save(deletedAccountClassAccountHeads);
		deletedAccountClassRepository.deleteMergedAccountHeads(accountId);
	}
	
	}

	
	
}
