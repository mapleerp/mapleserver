package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.report.entity.BatchWiseCustomerSalesReport;

public interface BatchWiseCustomerSalesReportService {

	public List<BatchWiseCustomerSalesReport> customerwisesalesreport(String companymstid,String branchcode,Date fDate,Date TDate,List<String>itemList);
	
}
