package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.SalesProfitHdrReport;

@Service
public interface SalesProfitReportService {

	
	List<SalesProfitHdrReport>getSalesProfitHdrReport(String branchcode,java.util.Date  fromdate,java.util.Date todate,String companymstid );



}
