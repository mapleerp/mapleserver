package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.RawMaterialIssueReport;
import com.maple.restserver.report.entity.RawMaterialReturnReport;
@Service
@Transactional
public interface RawMaterialReturnService {

	List<RawMaterialReturnReport> findByRawMaterialVoucher(String vocuhernumber, Date date, String companymstid);

}
