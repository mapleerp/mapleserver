package com.maple.restserver.service;

import java.security.MessageDigest;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.SubscriptionMst;
import com.maple.restserver.repository.SubscriptionMstRepository;
import com.maple.restserver.utils.SystemSetting;


@Service
@Transactional
@Component
public class SubscriptionValidityCheckImpl  implements SubscriptionValidityCheck{

	@Autowired
	SubscriptionMstRepository subscriptionMstRepository;
	
	
	@Autowired
	ExternalApi externalApi;
	
	
 
	@Override
	public boolean validateSubscription() {
		
		 
		 boolean validSubscription = false;
		 
		 /*
		  * Verify Subscription
		  * 1. Get subscriptionObject   from table.
		  * 2. If null call server url to get subscription date
		  *     Save subscription in Table
		  * 3. validate subsction date
		  * 
		  */ 
		
		List<SubscriptionMst> subscriptionList =   subscriptionMstRepository.findAll();
		
		if(null==subscriptionList || subscriptionList.size()==0) {
			
			return validSubscription;
		}
		SubscriptionMst subscriptionMst = subscriptionList.get(0);
		
	 
		String subscriptionEndString = SystemSetting.SqlDateTostring(subscriptionMst.getSubscriptionEnds());
		
		try {
			validSubscription =  validateDate(subscriptionEndString , subscriptionMst.getSubscriptionKey());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return validSubscription;
	}

	@Override
	public boolean updateSubscription(String userId, String password,String companyMstId, String branchcode) throws Exception {
		
		/*
		 * 
		 try {
				updateSubscription("test","test","C003", "C003");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 */
		 
		String subscriptionDate = externalApi.geSubscriptionDate(userId,password,companyMstId, branchcode);
		
		SubscriptionMst 	subscriptionMst = new 	SubscriptionMst();
		
		Date subscriptionEndDate = SystemSetting.StringToSqlDate(subscriptionDate, "dd/MM/yyyy");
		
		
		
		String subscriptionKey =  getKey(subscriptionDate);
		
		subscriptionMst.setSubscriptionEnds(subscriptionEndDate);
		subscriptionMst.setSubscriptionKey(subscriptionKey);
		
		subscriptionMstRepository.save(subscriptionMst);
		
		
		return true;
	}

	@Override
	public String getKey(String supscriptionDate) throws Exception {
		 //Reading data from user
		   
	      
		  
	      //Creating the MessageDigest object  
	      MessageDigest md = MessageDigest.getInstance("SHA-256");

	      //Passing data to the created MessageDigest Object
	      md.update(supscriptionDate.getBytes());
	      
	      //Compute the message digest
	      byte[] digest = md.digest();      
	      //System.out.println(digest);  
	     
	      //Converting the byte array in to HexString format
	      StringBuffer hexString = new StringBuffer();
	      
	      for (int i = 0;i<digest.length;i++) {
	         hexString.append(Integer.toHexString(0xFF & digest[i]));
	      }
	     // System.out.println("Hex format : " + hexString.toString());     
	   return hexString.toString();
	   }
	
	

	@Override
	public boolean  validateDate(String supscriptionDate, String keyString) throws Exception{
		 //Reading data from user
	   
	      
		  
	      //Creating the MessageDigest object  
	      MessageDigest md = MessageDigest.getInstance("SHA-256");

	      //Passing data to the created MessageDigest Object
	      md.update(supscriptionDate.getBytes());
	      
	      //Compute the message digest
	      byte[] digest = md.digest();      
	      //System.out.println(digest);  
	     
	      //Converting the byte array in to HexString format
	      StringBuffer hexString = new StringBuffer();
	      
	      for (int i = 0;i<digest.length;i++) {
	         hexString.append(Integer.toHexString(0xFF & digest[i]));
	      }
	     // System.out.println("Hex format : " + hexString.toString());    
	      
	     
	   return keyString.contentEquals(hexString.toString()) ;
	   }

}
