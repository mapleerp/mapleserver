package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.KotTableDtl;
import com.maple.restserver.entity.KotTableHdr;
@Service
@Component
public interface KotService {

	KotTableDtl saveKotPosDtl(KotTableDtl kotTableDtlList);

	KotTableHdr saveKotPosHdr(KotTableHdr kotTableHdr);

	KotTableHdr printKOT(KotTableHdr kotTableHdr);

	KotTableHdr finalSaveKot(KotTableHdr kottableHdrs);
	List<KotTableDtl> findByKotHdr(KotTableHdr kotTableHdr);

}
