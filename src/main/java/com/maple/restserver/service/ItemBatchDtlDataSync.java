package com.maple.restserver.service;

import java.sql.Date;
import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.maple.restserver.cloud.api.ExternalApi;

import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.LastBestDate;

import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.LastBestDateRepository;
import com.maple.restserver.utils.SystemSetting;

@Component
public class ItemBatchDtlDataSync implements DataSynchronisationService {

	String branchCode;
	String companyMstId;

	private int ItemBatchDtlCount = 0;
	private LastBestDate lastBestDate = null;

	private CloudDataCount cloudDataCount;

	public CloudDataCount getCloudDataCount() {
		return cloudDataCount;
	}

	public void setCloudDataCount(CloudDataCount cloudDataCount) {
		this.cloudDataCount = cloudDataCount;
	}

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	BranchMstRepository branchMstRepository;

	@Autowired
	ItemBatchDtlService itemBatchDtlService;

	@Autowired
	DayEndClosureRepository dayEndClosureRepository;

	@Autowired
	ExternalApi externalApi;

	@Autowired
	LastBestDateRepository lastBestDateRepository;

	@Autowired
	LastBestDateServiceItemBatchDtl lastBestDateServiceItemBatchDtl;

	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepo;

	@Override
	public int getLocalCount() {

		System.out.println("Entered into ItemBatchDtlDataSync.getLocalCount() method****************************");
		
		if(null == lastBestDate)
		{
			return 0;
		}

		ItemBatchDtlCount = itemBatchDtlRepo.getCountOfStockFromItemBatchDtl(companyMstId,
				lastBestDate.getLastSuccessDate(), lastBestDate.getLastSuccessDate(), branchCode);

		return ItemBatchDtlCount;
	}

	@Override
	public void countComparAndRetry() {

		System.out
				.println("Entered into ItemBatchDtlDataSync.countComparAndRetry() method****************************");

		boolean canRetry = false;

		int localDataCount = getLocalCount();
		Double serverDataCountInDouble = cloudDataCount.getServerCount("STOCK");
		int serverDataCount = serverDataCountInDouble.intValue();

		System.out.println("The Local Server count of item batch dtl are ====> " + localDataCount);
		System.out.println("The Cloud Server count of item batch dtl are ====> " + serverDataCount);

		canRetry = serverDataCount < localDataCount;

		if (canRetry) {
			retry();

		} else {
			updateLastBestDate();

		}

	}

	@Override
	public String retry() {

		int pageNo = 0;
		int pageSize = 50;

		System.out.println("Entered into ItemBatchDtlDataSync.retry() method****************************");

		System.out.println("Building ItemBatchDtl Starting");

		String date = SystemSetting.UtilDateToString(lastBestDate.getLastSuccessDate(), "yyyy-MM-dd");

		String hdrIdsFromCloud = externalApi.ItemBatchDtlIdsFromCloud(date);

		if (null == hdrIdsFromCloud) {
			hdrIdsFromCloud = "";
		}

		int recordCount = 0;

		while (true) {

			Pageable topFifty = PageRequest.of(pageNo, pageSize);

			List<ItemBatchDtl> ItemBatchDtlarray = new ArrayList();

			List<ItemBatchDtl> hdrIdS = itemBatchDtlRepo.getItemBatchDtlBtwnDate(lastBestDate.getLastSuccessDate(),
					lastBestDate.getDayEndDate(), topFifty,hdrIdsFromCloud);

			if (hdrIdS.size() == 0) {
				break;
			}

			System.out.println("Building ItemBatchDtl recordCount" + recordCount);

			ArrayList<ItemBatchDtl> itemNewList = new ArrayList<ItemBatchDtl>();
			for (int i = 0; i < hdrIdS.size(); i++) {

				itemNewList.add(hdrIdS.get(i));

			}

			ResponseEntity<String> message = externalApi.bulkitemBatchDtlToCloud(itemNewList,
					lastBestDate.getCompanyMst().getId());
			itemNewList.clear();

			System.out.println(
					"The result of externalApi.bulkitemBatchDtlToCloud() in ItemBatchDtlDataSync.retry() ====> "
							+ message);

//			if (!message.isEmpty()) {
//				itemBatchDtlService.updatePostedToServerStatus(message);
//			}

			System.out.println("Send ItemBatchDtl Array");

			pageNo++;

		}

		return "OK";

	}

	private void updateLastBestDate() {

		System.out.println("The result of ItemBatchDtlDataSync.updateLastBestDate() ====> " + lastBestDate);
		
		if(null == lastBestDate)
		{
			return;
		}

		lastBestDateRepository.save(lastBestDate);


	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCompanyMstId() {
		return companyMstId;
	}

	public void setCompanyMstId(String companyMstId) {
		this.companyMstId = companyMstId;
	}

	@Override
	public LastBestDate getLastBestDate() {

		System.out.println("Entered into ItemBatchDtlDataSync.getLastBestDate() method****************************");

		lastBestDateServiceItemBatchDtl.setBranchcode(branchCode);
		lastBestDateServiceItemBatchDtl.setCompanymstid(companyMstId);

		lastBestDate = lastBestDateServiceItemBatchDtl.findLastBestDate();
		System.out.println("The result of ItemBatchDtlDataSync.getLastBestDate() method ====> " + lastBestDate);
		return lastBestDate;
	}

}
