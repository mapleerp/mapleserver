package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.FinancialYearMst;

@Component
public interface FinancialYearService {

	String formattingYear(String financialYear);

}
