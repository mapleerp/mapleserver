package com.maple.restserver.service.internetcheck;

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.maple.restserver.service.InternetCheckService;
import com.maple.restserver.service.TallyServiceImpl;
@Component
public class InternetCheck {
	private static final Logger logger = LoggerFactory.getLogger(InternetCheck.class);
	
	 public boolean hasInternet()  {
		   try {
		   if(InternetCheckService.hasInternet(new URL("http://www.mapleerp.com"))) {
			   return true;
		        
			}else {
				return true;
				
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
			return false;
		}
		
		   
	   }

}
