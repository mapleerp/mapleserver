package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Controller;

import com.maple.restserver.entity.BatchPriceDefinition;
import com.maple.restserver.report.entity.ItemNotInBatchPriceReport;

@Controller
public interface BatchPriceDefinitionService {

	List<ItemNotInBatchPriceReport> getItemsNotInBatchPriceDefinition();
	
	 BatchPriceDefinition priceDefinitionByItemIdAndUnitAndBatch(String itemid, String priceId, String unitId,
				String batch, Date udate);

}
