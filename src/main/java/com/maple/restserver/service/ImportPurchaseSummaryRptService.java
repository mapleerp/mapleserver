package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.report.entity.ImportPurchaseSummaryReport;

public interface ImportPurchaseSummaryRptService {

	List<ImportPurchaseSummaryReport> getImportpurchaseReport(Date fromDate, Date toDate);

}
