package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.CardReconcileReport;
import com.maple.restserver.report.entity.SaleOrderReceiptReport;

@Service
@Transactional
@Component
public interface SalesReceiptService {
	public List<CardReconcileReport> getSalesReceiptBetweenDate(Date fudate, Date tudate,String receiptMode,CompanyMst companyMst,String branchCode);

	List<SaleOrderReceiptReport> getSalesReceiptByModeAndDate(String branchCode, Date vdate, String mode);

	public Double getCardSalesAndSoReceiptsAmount(java.sql.Date date, String companymstid, String branchcode);
}
