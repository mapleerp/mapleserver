package com.maple.restserver.service;

import java.util.List;

import com.maple.restserver.entity.UnitMst;

public interface UnitMstService {
	
	public  List<Object> getUnitMst(String  unitName);
	public  String getAllUnitMst(String pdfFileName);
	public String initializeUnitMst(String branchcode, String companymstid);

}
