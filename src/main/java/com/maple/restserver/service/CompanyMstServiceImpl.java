package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Component
public class CompanyMstServiceImpl implements CompanyMstService {

	@Override
	public ArrayList<String> getStateByCountry(String country) {
		ArrayList<String> strList = new ArrayList();
		if(country.equalsIgnoreCase("INDIA"))
		{
			strList.add("KERALA");
			strList.add("ANDHRA PRADESH");
	    	strList.add("ARUNACHAL PRADESH");
	    	strList.add("ASSAM");
	    	strList.add("BIHAR");
	    	strList.add("CHHATTISGARH");
			strList.add("GOA");
			strList.add("GUJARAT");
			strList.add("HARYANA");
			strList.add("HIMACHAL PRADESH");
			strList.add("JAMMU AND  KASHMIR");
			strList.add("JHARKHAND");
			strList.add("KARNATAKA");
			strList.add("MADHYA PRADESH");
			strList.add("MAHARASHTRA");
			strList.add("MANIPUR");
			strList.add("MEGHALAYA");
			strList.add("MIZORAM");
			strList.add("NAGALAND");
			strList.add("ODISHA");
			strList.add("PUNJAB");
			strList.add("RAJASTHAN");
			strList.add("TAMIL NADU");
			strList.add("TELANGANA");
			strList.add("TRIPURA");
			strList.add("UTTAR PRADESH");
			strList.add("UTTARAKHAND");
			strList.add("WEST BENGAL");
		}
		else if(country.equalsIgnoreCase("MALDIVES"))
		{
			strList.add("MAIN LAND");
		}
		return strList;
	}

}
