package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndReportStore;
@Component
public interface DayEndReportStoreService {

	List<DayEndReportStore> findDayEndReportByDate(CompanyMst companyMst, Date date);

	//-------------version 6.7 surya 

	List<DayEndReportStore> findDayEndReportByDateAndCard(CompanyMst companyMst, Date date);
	//-------------version 6.7 surya end


}
