package com.maple.restserver.service;

import java.net.UnknownHostException;
import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;


//com.maple.restserver.service.DataTransferService
@Service
public interface DataTransferService {
	
	String doDataTransferUserMst(CompanyMst companyMst) throws UnknownHostException, Exception;
	String doItemTransfer(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;


	String doItemTransferExTax(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferKit(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferStock(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferMinStock(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String pushstocktoServer(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferCustomer(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferSupplier(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferAccountHeads(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	 

	String transferPurchase(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferSales(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;
	

	String transferOtherBranchSales(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferReceipts(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferPayments(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferJournal(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String salesDataCorrectionFromItemBatchDtl(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception;

	String removeItemBatchDuplicate(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String updateItemBatchMstStockFromDtl(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception;

	String updateItemBatchMstStockBranch(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception;

	String updateUnitFromPurchaseDtl(CompanyMst companyMst, String branchcode);

	String updateUnitFromPriceDefenition(CompanyMst companyMst, String branchcode);

	String updateBillSeries(CompanyMst companyMst, String branchCode, String prefix, String newprefix);

	String updateUDateCorrection(CompanyMst companyMst, String branchcode, int startVoucher, int endVoucher,
			java.sql.Date oldDate, java.sql.Date newDate, String vouchersuffix);

//	String updateSupplierIdAsAccountId();

	String removeDuplicateVOucherinSalesTransHdr(Date vdate);

	String transferMemberMst(CompanyMst companyMst) throws UnknownHostException, Exception;

	String transferOrganisation(CompanyMst companyMst) throws UnknownHostException, Exception;

	String transferOrgAccountHeads(CompanyMst companyMst) throws UnknownHostException, Exception;

	String transferFamilyMst(CompanyMst companyMst) throws UnknownHostException, Exception;

	String doSalesTransHdrTransfer(CompanyMst companyMst, String branchcode) throws UnknownHostException, Exception;

	String doPurchaseTransfer(CompanyMst companyMst, String branchcode) throws UnknownHostException, Exception;

	String doItemBatchDtlTransfer(CompanyMst companyMst, String branchcode) throws UnknownHostException, Exception;

	String transferCompanyMst(CompanyMst companyMst) throws UnknownHostException, Exception;

	String doDataTransferBranch(CompanyMst companyMst) throws UnknownHostException, Exception;

	String doDataTransferUnitMst(CompanyMst companyMst, String branch) throws UnknownHostException, Exception;

	String doPurchaseDtl(CompanyMst companyMst, String branch) throws UnknownHostException, Exception;

	String transferStockFromFile(CompanyMst companyMst, String fileName) throws UnknownHostException, Exception;

	String transferItemFromFileCarPalace(CompanyMst companyMst, String fileName, String branchcode)
			throws UnknownHostException, Exception;

	 	
}
