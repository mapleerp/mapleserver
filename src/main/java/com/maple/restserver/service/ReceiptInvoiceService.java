package com.maple.restserver.service;


import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.OrgReceiptInvoice;
import com.maple.restserver.report.entity.ReceiptInvoice;

 
@Service
public interface ReceiptInvoiceService {

 
 
	public   List<ReceiptInvoice>getReceiptByVoucherAndDate(String vouchernumber,Date  date,String companymstid );
	public   List<OrgReceiptInvoice>getReceiptByVoucherAndDateAndmember( String voucherNumber, Date date,String companymstid,String memberId);
	 
	//version 3.1
	List<ReceiptInvoice>getReceiptInvoiceReport(Date sdate,Date edate,CompanyMst companyMst,String branchCode);
//version 3.1 end

}
