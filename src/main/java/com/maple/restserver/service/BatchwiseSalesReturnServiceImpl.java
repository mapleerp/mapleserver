package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.BatchWiseSalesReturnReport;
import com.maple.restserver.report.entity.BatchwisePerformancereport;
import com.maple.restserver.repository.SalesReturnHdrRepository;

@Service
@Transactional
public class BatchwiseSalesReturnServiceImpl implements BatchwiseSalesReturnService{

	@Autowired
	SalesReturnHdrRepository salesReturnHdrRepository;

	@Override
	public List<BatchWiseSalesReturnReport> batchwiseSalesReturnreport(String companymstid, String branchcode,
			Date fDate, Date TDate, List<String> itemgroup) {
		List<BatchWiseSalesReturnReport>reportList=new ArrayList<BatchWiseSalesReturnReport>();

		for(String groupId:itemgroup) {
			 List<Object>objList=salesReturnHdrRepository.batchwiseSalesReturnreport( companymstid,  branchcode,fDate,TDate,groupId);
			 
			  for(int i=0;i<objList.size();i++)
				 {
				Object[] objAray = (Object[]) objList.get(i);
				BatchWiseSalesReturnReport report=new BatchWiseSalesReturnReport();
				report.setItemcode((String) objAray[0]);
				
				reportList.add(report);
				 }
		  }
		
		
		
		return reportList;
	}
	
	
	
}
