package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.BatchWiseSalesReturnReport;

public interface  BatchwiseSalesReturnService {

	
	
	public List<BatchWiseSalesReturnReport>	batchwiseSalesReturnreport(String companymstid,String branchcode,Date fDate,Date TDate,List<String>itemgroup);
}
