package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component

public interface CompanyMstService {

	ArrayList<String> getStateByCountry(String country);

}
