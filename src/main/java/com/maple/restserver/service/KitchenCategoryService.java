package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;

@Service

@Component
public interface KitchenCategoryService {

	List<String> getKitchenItems(CompanyMst companyMst);

}
