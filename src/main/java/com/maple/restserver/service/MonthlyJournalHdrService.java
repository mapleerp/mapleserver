package com.maple.restserver.service;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;

@Service

public interface MonthlyJournalHdrService {

void  copyToMonthlyJournalHdr(Date fromDate,Date toDate,String branchCode,CompanyMst companyMst);
	
	
}
