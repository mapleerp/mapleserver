package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 
import com.maple.restserver.entity.BatchPriceDefinition;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.report.entity.ActualProductionReport;
import com.maple.restserver.report.entity.ItemNotInBatchPriceReport;
import com.maple.restserver.repository.BatchPriceDefinitionRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.UnitMstRepository;

@Service
@Transactional
public class BatchPriceDefinitionServiceImpl implements BatchPriceDefinitionService{

	@Autowired
	UnitMstRepository unitMstRepo;
	@Autowired
	BatchPriceDefinitionRepository batchPriceDefinitionRepo;
	
	@Autowired
	ItemMstRepository itemmstRepo;
	@Override
	public List<ItemNotInBatchPriceReport> getItemsNotInBatchPriceDefinition() {
		
		List<ItemNotInBatchPriceReport> itemList = new ArrayList();
//		List<Object> obItemList = batchPriceDefinitionRepo.getItem();
//		List<Object> obBatchList = batchPriceDefinitionRepo.getBatch();

		List<Object> obj = batchPriceDefinitionRepo.getItemsNotInBatchPrice();
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ItemNotInBatchPriceReport itemNotInBatchPriceReport =new ItemNotInBatchPriceReport();
			 System.out.println("ITEM NAME"+(String) objAray[0]);
			 Optional<ItemMst> itemmstop =  itemmstRepo.findById((String) objAray[0]);
			 if(itemmstop.isPresent())
			 {
			 ItemMst itemmst =  itemmstRepo.findById((String) objAray[0]).get();
			 Optional<UnitMst> unitMstOp = unitMstRepo.findById(itemmst.getUnitId());
			 if(unitMstOp.isPresent())
			 {
				 UnitMst unitMst = unitMstOp.get();
			 itemNotInBatchPriceReport.setBatch((String) objAray[1]);
			 itemNotInBatchPriceReport.setItemName(itemmst.getItemName());
			 itemNotInBatchPriceReport.setUnitName(unitMst.getUnitName());
			 itemList.add(itemNotInBatchPriceReport);
			 }
			 }
		 }
		return itemList;
	}

	@Override
	public BatchPriceDefinition priceDefinitionByItemIdAndUnitAndBatch(String itemid, String priceId, String unitId,
			String batch, Date udate) {
		
		List<BatchPriceDefinition> batchList = batchPriceDefinitionRepo.findByItemIdAndUnitId(itemid, priceId, unitId,batch,udate);
		if(batchList.size()>0)
		{
			return batchList.get(0);
		}
		else
		{
			List<BatchPriceDefinition> batchListEndDate = batchPriceDefinitionRepo.
					findByItemIdAndUnitIdEndDateNull(itemid, priceId, unitId,batch,udate);
			if(batchListEndDate.size()>0)
			{
				return batchListEndDate.get(0);
			}
		}
		return null;
		
	}

}
