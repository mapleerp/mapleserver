package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.SubBranchSalesDtl;

public interface SubBranchSalesDtlService {

	List<SubBranchSalesDtl> getSpneserDtlBySpenserIdAndDate(String branchcode, String companymstid, Date fudate,
			Date ftdate);

}
