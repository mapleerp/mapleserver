package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.ProfitAndLossReport;
import com.maple.restserver.report.entity.StockSummaryDtlReport;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ProfitLossRepository;
import com.sun.org.apache.bcel.internal.generic.RETURN;

@Service
@Transactional
@Component
public class ProfitAndLossServiceImpl implements ProfitAndLossService {

	
	@Autowired
	ProfitLossRepository profitLossRepository;

	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;

	@Override
	public List<ProfitAndLossReport> getBranchwiseTotalProfit(String companymstid, Date profitDate) {

		ArrayList<ProfitAndLossReport> profitAndLossReportList = new ArrayList<ProfitAndLossReport>();

		List<Object> object = itemBatchDtlRepository.getBranchwiseTotalProfit(companymstid, profitDate);

		for (int k = 0; k < object.size(); k++) {

			Object[] objctArray = (Object[]) object.get(k);

			ProfitAndLossReport profitAndLossReport = new ProfitAndLossReport();

			profitAndLossReport.setBranchName((String) objctArray[0]);
			profitAndLossReport.setTotalProfit((Double) objctArray[1]);

			profitAndLossReportList.add(profitAndLossReport);

		}
		return profitAndLossReportList;

	}

}
