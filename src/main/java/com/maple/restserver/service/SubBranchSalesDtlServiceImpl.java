package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.SubBranchSalesDtl;
import com.maple.restserver.repository.SubBranchSalesDtlRepository;
import com.maple.restserver.repository.SubBranchSalesHdrRepository;

 
@Service
@Transactional
public class SubBranchSalesDtlServiceImpl implements SubBranchSalesDtlService{
	
	@Autowired
	SubBranchSalesDtlRepository spencerSalesDtlRepository;
	
	@Autowired
	SubBranchSalesHdrRepository subBranchSalesHdrRepository;

	@Override
	public List<SubBranchSalesDtl> getSpneserDtlBySpenserIdAndDate(String branchcode, String companymstid, Date fudate,
			Date ftdate) {

		List<SubBranchSalesDtl> salesDtlList = new ArrayList<SubBranchSalesDtl>();
		
		List<Object> objList = spencerSalesDtlRepository.getSpneserDtlBySpenserIdAndDate(branchcode,companymstid,fudate,ftdate);
		for(int i=0; i<objList.size(); i++)
		{
			Object[] objAray = (Object[]) objList.get(i);
			
			SubBranchSalesDtl spencerSalesDtl = new SubBranchSalesDtl();
			
			spencerSalesDtl.setQty((Double) objAray[0]);
			spencerSalesDtl.setItemId((String) objAray[1]);
			spencerSalesDtl.setUnitId((String) objAray[3]);
			spencerSalesDtl.setBatch((String) objAray[2]);

			salesDtlList.add(spencerSalesDtl);
		}
		
		subBranchSalesHdrRepository.updateSubBranchSalesHdr(branchcode,companymstid,fudate,ftdate);
		return salesDtlList;
	}

}
