package com.maple.restserver.service;

import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.kafka.common.protocol.types.Field.Str;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ConsumptionDtl;
import com.maple.restserver.entity.ConsumptionHdr;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchDtlDaily;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.MonthlySalesTransHdr;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.OpeningStockDtl;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.StockTransferInDtl;
import com.maple.restserver.entity.StockTransferInHdr;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.entity.StoreChangeMst;
import com.maple.restserver.entity.StoreMst;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.report.entity.CategoryWiseStockMovement;
import com.maple.restserver.report.entity.IntentVoucherReport;
import com.maple.restserver.report.entity.PharmacyGroupWiseSalesReport;
import com.maple.restserver.report.entity.PharmacyItemWiseSalesReport;
import com.maple.restserver.report.entity.PoliceReport;
import com.maple.restserver.report.entity.PurchaseReport;
import com.maple.restserver.report.entity.StockMigrationReport;
import com.maple.restserver.report.entity.StockReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlDailyRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchExpiryDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.StockTransferOutDtlRepository;
import com.maple.restserver.repository.StoreMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component
public class ItemBatchDtlServiceimpl implements ItemBatchDtlService {
	private static final Logger logger = LoggerFactory.getLogger(ItemBatchDtlService.class);

	@Value("${mybranch}")
	private String mybranch;
	
	
	@Value("${spring.datasource.url}")
	private String dbType;

	@Autowired
	StoreMstRepository storeMstRepo;

	@Autowired
	ItemMstRepository itemMstRepo;

	@Autowired
	BranchMstRepository branchMstRepo;
	
	@Autowired
	CategoryMstRepository categoryMstRepository;

	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepo;

	@Autowired
	ItemBatchMstRepository itemBatchMstRepo;

	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepo;
	// ----------------version 5.0 surya

	boolean recurssionOff = false;

	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	MultiUnitMstRepository multiUnitMstRepository;

	@Autowired
	ItemBatchDtlDailyRepository itemBatchDtlDailyRepository;
	
	@Autowired
	ItemMstRepository itemMstRepository;

	@Autowired
	private AccountHeadsRepository accountHeadsRepository;

	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	StockTransferOutDtlRepository stockTransferOutDtlRepo;

	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;
	@Autowired
	SalesDetailsRepository salesDetailsRepository;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	

	// ----------------version 5.0 surya end
	@Override
	public List<CategoryWiseStockMovement> getCategoryWiseStockMovement(String branchcode, String companymstid,
			java.sql.Date fdate, java.sql.Date tdate) {
		List<CategoryWiseStockMovement> categoryStockMvmntList = new ArrayList();
		List<Object> obj= new ArrayList();
		if (dbType.contains("mysql")) {
			obj = itemBatchDtlRepo.getCategoryStockMovement(branchcode, companymstid, fdate, tdate);
		}
		else if (dbType.contains("derby")) {
			obj = itemBatchDtlRepo.getCategoryStockMovementDerby(branchcode, companymstid, fdate, tdate);
		}
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			CategoryWiseStockMovement categoryWiseStockMovement = new CategoryWiseStockMovement();
			categoryWiseStockMovement.setCategory((String) objAray[0]);
			categoryWiseStockMovement.setInWardQty((Double) objAray[1]);
			categoryWiseStockMovement.setOutWardQty((Double) objAray[2]);
			categoryWiseStockMovement.setVoucherDate((Date) objAray[3]);
			List<Object> objOpeningStk = itemBatchDtlRepo.CategoryWiseOPeningStock(branchcode, companymstid, fdate);
			if (null == objOpeningStk) {
				for (int j = 0; j < objOpeningStk.size(); j++) {
					Object[] objctAray = (Object[]) objOpeningStk.get(j);
					if (categoryWiseStockMovement.getCategory().equalsIgnoreCase((String) objctAray[0])) {

						categoryWiseStockMovement.setOpeningStock((Double) objctAray[1]);
					}

					List<Object> objClosingStk = itemBatchDtlRepo.CategoryWiseClosingStock(branchcode, companymstid,
							fdate, tdate);
					for (int k = 0; k < objClosingStk.size(); k++) {
						Object[] objctArray = (Object[]) objClosingStk.get(k);
						if (categoryWiseStockMovement.getCategory().equalsIgnoreCase((String) objctArray[0])) {
							if (null == categoryWiseStockMovement.getOpeningStock()) {
								categoryWiseStockMovement.setClosingStock((Double) objctArray[1] + 0.0);
							} else {
								categoryWiseStockMovement.setClosingStock(
										(Double) objctArray[1] + categoryWiseStockMovement.getOpeningStock());
							}
						}
					}
				}
			} else {

				List<Object> objClosingStk = itemBatchDtlRepo.CategoryWiseClosingStock(branchcode, companymstid, fdate,
						tdate);
				for (int k = 0; k < objClosingStk.size(); k++) {
					Object[] objctArray = (Object[]) objClosingStk.get(k);
					if (categoryWiseStockMovement.getCategory().equalsIgnoreCase((String) objctArray[0])) {

						categoryWiseStockMovement.setClosingStock((Double) objctArray[1] + 0.0);

					}
				}
			}
			categoryStockMvmntList.add(categoryWiseStockMovement);
		}
		return categoryStockMvmntList;
	}

	@Override
	public List<CategoryWiseStockMovement> getCategoryWiseStockMovementReport(String branchcode, String companymstid,
			java.sql.Date fdate, java.sql.Date tdate, String categoryid) {

		List<CategoryWiseStockMovement> categoryStockMvmntList = new ArrayList();
		List<Object> obj = itemBatchDtlRepo.getItemStockMovement(branchcode, companymstid, fdate, tdate, categoryid);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			CategoryWiseStockMovement categoryWiseStockMovement = new CategoryWiseStockMovement();
			categoryWiseStockMovement.setCategory((String) objAray[0]);
			categoryWiseStockMovement.setInWardQty((Double) objAray[1]);
			categoryWiseStockMovement.setOutWardQty((Double) objAray[2]);
			categoryWiseStockMovement.setVoucherDate((Date) objAray[3]);
			List<Object> objOpeningStk = itemBatchDtlRepo.ItemWiseOPeningStockReport(branchcode, companymstid, fdate,
					categoryid);
			if (null == objOpeningStk) {
				for (int j = 0; j < objOpeningStk.size(); j++) {
					Object[] objctAray = (Object[]) objOpeningStk.get(j);
					if (categoryWiseStockMovement.getCategory().equalsIgnoreCase((String) objctAray[0])) {
						categoryWiseStockMovement.setOpeningStock((Double) objctAray[1]);
					}

					List<Object> objClosingStk = itemBatchDtlRepo.ItemWiseClosingStockReport(branchcode, companymstid,
							fdate, tdate, categoryid);
					for (int k = 0; k < objClosingStk.size(); k++) {

						Object[] objctArray = (Object[]) objClosingStk.get(k);
						if (categoryWiseStockMovement.getCategory().equalsIgnoreCase((String) objctArray[0])) {
							if (null == categoryWiseStockMovement.getOpeningStock()) {
								categoryWiseStockMovement.setClosingStock((Double) objctArray[1] + 0.0);
							} else {
								categoryWiseStockMovement.setClosingStock(
										(Double) objctArray[1] + categoryWiseStockMovement.getOpeningStock());
							}
						}

					}
				}
			} else {
				List<Object> objClosingStk = itemBatchDtlRepo.ItemWiseClosingStockReport(branchcode, companymstid,
						fdate, tdate, categoryid);
				for (int k = 0; k < objClosingStk.size(); k++) {

					Object[] objctArray = (Object[]) objClosingStk.get(k);
					if (categoryWiseStockMovement.getCategory().equalsIgnoreCase((String) objctArray[0])) {

						categoryWiseStockMovement.setClosingStock((Double) objctArray[1] + 0.0);

					}

				}

			}

			categoryStockMvmntList.add(categoryWiseStockMovement);

		}
		return categoryStockMvmntList;
	}

	@Override
	public String updateItemBatchMstStockFromDtl(CompanyMst companyMst, java.sql.Date curDate)
			throws UnknownHostException, Exception {

		itemBatchMstRepo.updateStockFromDtlStep0();

		// BranchMst branchMst =branchMstRepo.findByBranchName(mybranch);

		// itemBatchMstRepo.updateStockFromDtlStep1(branchMst.getBranchCode());

		// itemBatchDtlRepo.

		// itemBatchMstRepo.updateStockFromDtlStep2withDate(curDate) ;
		List<Object> listObject = itemBatchMstRepo.getClosingStockAsOnDate(curDate);
		boolean flag = false;
		for (int i = 0; i < listObject.size(); i++) {

			Object[] objAray = (Object[]) listObject.get(i);
			ItemBatchMst itemBatchMst = new ItemBatchMst();

			itemBatchMst.setBarcode(objAray[0] == null ? "" : (String) objAray[0]);

			itemBatchMst.setBatch(objAray[1] == null ? "" : (String) objAray[1]);
			itemBatchMst.setBranchCode(objAray[2] == null ? "" : (String) objAray[2]);

//			 itemBatchMst.setExpDate(objAray[6] == null ? null : (Date) objAray[6]);

			String id = UUID.randomUUID().toString();
			itemBatchMst.setId(id);
			// itemBatchMst.setId(objAray[3] == null ? "" : (String) objAray[3]);
			itemBatchMst.setItemId(objAray[3] == null ? "" : (String) objAray[3]);

			itemBatchMst.setQty(objAray[4] == null ? 0 : (double) objAray[4]);
			itemBatchMst.setCompanyMst(companyMst);
			itemBatchMst.setStore(objAray[5] == null ? MapleConstants.Store : (String) objAray[5]);

			itemBatchMstRepo.saveAndFlush(itemBatchMst);

		}

		itemBatchMstRepo.updateExpiryDateFromItemBatchExpiryDtl();

		// Update item batch mst with kit
		try {
			updateItemBatchMstWitKit();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		itemBatchMstRepo.updateStockFromDtlStep3();

		itemBatchMstRepo.correctNegativStock();

		// itemBatchMstRepo.updateStockFromDtlStep4();

		return null;
	}

	private void updateItemBatchMstWitKit() {
		List<ItemBatchMst> itemBatchMstList = itemBatchMstRepo.findAll();

		List<KitDefinitionMst> kitList = kitDefinitionMstRepo.findAll();
		boolean flag = false;
		for (int i = 0; i < kitList.size(); i++) {
			for (int j = 0; j < itemBatchMstList.size(); j++) {
				flag = false;
				if (kitList.get(i).getItemId().equalsIgnoreCase(itemBatchMstList.get(j).getItemId())) {
					flag = true;
					break;
				}

			}
			if (!flag) {
				List<KitDefinitionMst> kitDefMstList = kitDefinitionMstRepo.findByItemId(kitList.get(i).getItemId());
				ItemBatchMst itemBatchMst = new ItemBatchMst();
				itemBatchMst.setBarcode(kitDefMstList.get(0).getBarcode());
				itemBatchMst.setBatch(MapleConstants.Nobatch);
				itemBatchMst.setBranchCode(kitDefMstList.get(0).getBarcode());
				itemBatchMst.setCompanyMst(kitDefMstList.get(0).getCompanyMst());
				itemBatchMst.setItemId(kitDefMstList.get(0).getItemId());
				Optional<ItemMst> itemMstOpt = itemMstRepo.findById(kitDefMstList.get(0).getItemId());
				if (itemMstOpt.isPresent()) {
					itemBatchMst.setMrp(itemMstOpt.get().getStandardPrice());
				} else {
					itemBatchMst.setMrp(0.0);
				}
				itemBatchMst.setQty(0.0);
				itemBatchMst = itemBatchMstRepo.save(itemBatchMst);
				// itemBatchMstRepo.updateItemBatchMstWithKit(kitList.get(i).getItemId()) ;
			}
		}
	}

	// ----------------version 5.0 surya
	@Override
	public void SalesStockUpdation(ItemMst item, SalesDtl salesDtl, SalesTransHdr salesTransHdr,
			ItemBatchMst itemBatchMst) {

		double conversionQty = 0.0;

		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		final VoucherNumber voucherNo = voucherService.generateInvoice("STV", salesTransHdr.getCompanyMst().getId());
		if (!item.getUnitId().equalsIgnoreCase(salesDtl.getUnitId())) {

			String itemId = item.getId();
			String sourceUnit = salesDtl.getUnitId();
			String targetUnit = item.getUnitId();

			recurssionOff = false;
			conversionQty = getConvertionQty(salesTransHdr.getCompanyMst().getId(), item.getId(), sourceUnit,
					targetUnit, salesDtl.getQty());
			itemBatchDtl.setQtyOut(conversionQty);
		} else {
			itemBatchDtl.setQtyOut(salesDtl.getQty());
		}
		itemBatchDtl.setBarcode(salesDtl.getBarcode());
		itemBatchDtl.setBatch(salesDtl.getBatch());
		itemBatchDtl.setItemId(salesDtl.getItemId());
		itemBatchDtl.setMrp(salesDtl.getMrp());
		itemBatchDtl.setQtyIn(0.0);
		itemBatchDtl.setExpDate(itemBatchMst.getExpDate());
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(salesTransHdr.getVoucherDate());
		itemBatchDtl.setItemId(salesDtl.getItemId());
		itemBatchDtl.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());
		itemBatchDtl.setSourceVoucherDate(salesTransHdr.getVoucherDate());
		itemBatchDtl.setCompanyMst(salesTransHdr.getCompanyMst());
		itemBatchDtl.setBranchCode(salesTransHdr.getBranchCode());

		// List<ItemBatchDtl> itemBatchDtlList = itemBatchDtlRepository
		itemBatchDtl.setSourceParentId(salesTransHdr.getId());
		itemBatchDtl.setSourceDtlId(salesDtl.getId());
		itemBatchDtl.setParticulars("SALES " + salesTransHdr.getAccountHeads().getAccountName());
		itemBatchDtl.setStore(MapleConstants.Store);
		itemBatchDtl = itemBatchDtlRepo.save(itemBatchDtl);
		//eventBus.post(itemBatchDtl);
//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("voucherNumber", itemBatchDtl.getId());
//		variables.put("id", itemBatchDtl.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("REST", 1);
//		variables.put("companyid", itemBatchDtl.getCompanyMst());
//		variables.put("branchcode", itemBatchDtl.getBranchCode());
//
//		variables.put("WF", "forwardItemBatchDtl");
//
//		String workflow = (String) variables.get("WF");
//		String voucherNumber1 = (String) variables.get("voucherNumber");
//		String sourceID = (String) variables.get("id");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//		lmsQueueMst.setVoucherDate(sqlVDate);
//
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardItemBatchDtl");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow + sourceID);
//		lmsQueueMst.setJobGroup(workflow);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID);
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//		eventBus.post(variables);
		ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();

		itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
		itemBatchDtlDaily.setBatch(itemBatchDtl.getBatch());
		itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
		itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
		itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
		itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
		itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
		itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
		itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
		itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
		itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
		itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());
		itemBatchDtlDaily.setStore(MapleConstants.Store);
		itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
		itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());

		itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);
	}

	@Override
	public void SalesStockUpdationMobileStock(ItemMst item, String batch, SalesDtl salesDtl,
			SalesTransHdr salesTransHdr, ItemBatchMst itemBatchMst, String storeName) {

		double conversionQty = 0.0;

		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		final VoucherNumber voucherNo = voucherService.generateInvoice("STV", salesTransHdr.getCompanyMst().getId());
		if (!item.getUnitId().equalsIgnoreCase(salesDtl.getUnitId())) {

			String itemId = item.getId();
			String sourceUnit = salesDtl.getUnitId();
			String targetUnit = item.getUnitId();

			recurssionOff = false;
			conversionQty = getConvertionQty(salesTransHdr.getCompanyMst().getId(), item.getId(), sourceUnit,
					targetUnit, salesDtl.getQty());
			itemBatchDtl.setQtyOut(conversionQty);
		} else {
			itemBatchDtl.setQtyOut(salesDtl.getQty());
		}
		itemBatchDtl.setBarcode(salesDtl.getBarcode());
		itemBatchDtl.setBatch(batch);
		itemBatchDtl.setItemId(salesDtl.getItemId());
		itemBatchDtl.setMrp(salesDtl.getMrp());
		itemBatchDtl.setQtyIn(0.0);
		itemBatchDtl.setExpDate(itemBatchMst.getExpDate());
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(salesTransHdr.getVoucherDate());
		itemBatchDtl.setItemId(salesDtl.getItemId());
		itemBatchDtl.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());
		itemBatchDtl.setSourceVoucherDate(salesTransHdr.getVoucherDate());
		itemBatchDtl.setCompanyMst(salesTransHdr.getCompanyMst());
		itemBatchDtl.setBranchCode(salesTransHdr.getBranchCode());

		// List<ItemBatchDtl> itemBatchDtlList = itemBatchDtlRepository
		itemBatchDtl.setSourceParentId(salesTransHdr.getId());
		itemBatchDtl.setSourceDtlId(salesDtl.getId());
		itemBatchDtl.setParticulars("SALES " + salesTransHdr.getAccountHeads().getAccountName());
		itemBatchDtl.setStore(storeName);
		itemBatchDtl = itemBatchDtlRepo.save(itemBatchDtl);
		//eventBus.post(itemBatchDtl);
//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("voucherNumber", itemBatchDtl.getId());
//		variables.put("id", itemBatchDtl.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("REST", 1);
//		variables.put("companyid", itemBatchDtl.getCompanyMst());
//		variables.put("branchcode", itemBatchDtl.getBranchCode());
//
//		variables.put("WF", "forwardItemBatchDtl");
//
//		String workflow = (String) variables.get("WF");
//		String voucherNumber1 = (String) variables.get("voucherNumber");
//		String sourceID = (String) variables.get("id");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//		lmsQueueMst.setVoucherDate(sqlVDate);
//
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardItemBatchDtl");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow + sourceID);
//		lmsQueueMst.setJobGroup(workflow);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID);
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//		eventBus.post(variables);
		ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();

		itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
		itemBatchDtlDaily.setBatch(batch);
		itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
		itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
		itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
		itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
		itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
		itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
		itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
		itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
		itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
		itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());
		itemBatchDtlDaily.setStore(storeName);
		itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
		itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());

		itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);
	}

	private double getConvertionQty(String companyMstId, String itemId, String sourceUnit, String targetUnit,
			double sourceQty) {
		if (recurssionOff) {
			return sourceQty;
		}
		MultiUnitMst multiUnitMstList = multiUnitMstRepository.findByCompanyMstIdAndItemIdAndUnit1(companyMstId, itemId,
				sourceUnit);

		while (!multiUnitMstList.getUnit2().equalsIgnoreCase(targetUnit)) {

			if (recurssionOff) {
				break;
			}
			sourceUnit = multiUnitMstList.getUnit2();

			sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();

			getConvertionQty(companyMstId, itemId, sourceUnit, targetUnit, sourceQty);

		}
		sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();
		recurssionOff = true;
		// sourceQty = sourceQty *
		// multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
		return sourceQty;
	}

	@Override
	public void updatePurchaseStock(PurchaseDtl purchaseDtl, PurchaseHdr purchaseHdr, ItemMst item) {

		double conversionQty = 0.0;
		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		final VoucherNumber voucherNo = voucherService.generateInvoice("STV", purchaseHdr.getCompanyMst().getId());

		if (!item.getUnitId().equalsIgnoreCase(purchaseDtl.getUnitId())) {

			String itemId = item.getId();
			String sourceUnit = purchaseDtl.getUnitId();
			String targetUnit = item.getUnitId();
			recurssionOff = false;

			conversionQty = getConvertionQty(purchaseHdr.getCompanyMst().getId(), item.getId(), sourceUnit, targetUnit,
					purchaseDtl.getQty());
			itemBatchDtl.setQtyIn(conversionQty);
		} else {
			itemBatchDtl.setQtyIn(purchaseDtl.getQty());
		}

		itemBatchDtl.setBarcode(purchaseDtl.getBarcode());
		itemBatchDtl.setBatch(purchaseDtl.getBatch());
		itemBatchDtl.setItemId(purchaseDtl.getItemId());
		itemBatchDtl.setMrp(purchaseDtl.getPurchseRate());

		itemBatchDtl.setQtyOut(0.0);
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(purchaseHdr.getVoucherDate());
		itemBatchDtl.setItemId(purchaseDtl.getItemId());
		itemBatchDtl.setExpDate(purchaseDtl.getExpiryDate());

		itemBatchDtl.setSourceVoucherNumber(purchaseHdr.getVoucherNumber());

		itemBatchDtl.setSourceVoucherDate(purchaseHdr.getVoucherDate());
		itemBatchDtl.setBranchCode(purchaseHdr.getBranchCode());

		AccountHeads accountHeads = accountHeadsRepository.findById(purchaseHdr.getSupplierId()).get();

		itemBatchDtl.setParticulars(
				"Purchase  from " + accountHeads.getAccountName() + "by " + purchaseHdr.getVoucherNumber());

		itemBatchDtl.setCompanyMst(purchaseHdr.getCompanyMst());
		itemBatchDtl.setStore(MapleConstants.Store);
		itemBatchDtl.setSourceParentId(purchaseHdr.getId());
		itemBatchDtl.setSourceDtlId(purchaseDtl.getId());
		itemBatchDtl = itemBatchDtlRepo.save(itemBatchDtl);
		//eventBus.post(itemBatchDtl);
		
		
		ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();

		itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
		itemBatchDtlDaily.setBatch(itemBatchDtl.getBatch());
		itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
		itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
		itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
		itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
		itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
		itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
		itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
		itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
		itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
		itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());
		itemBatchDtlDaily.setStore(MapleConstants.Store);
		itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
		itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());

		itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);

//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("voucherNumber", itemBatchDtl.getId());
//		variables.put("id", itemBatchDtl.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("REST", 1);
//		variables.put("companyid", itemBatchDtl.getCompanyMst());
//		variables.put("branchcode", itemBatchDtl.getBranchCode());
//
//		variables.put("WF", "forwardItemBatchDtl");
//
//		String workflow = (String) variables.get("WF");
//		String voucherNumber1 = (String) variables.get("voucherNumber");
//		String sourceID = (String) variables.get("id");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//		lmsQueueMst.setVoucherDate(sqlVDate);
//
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardItemBatchDtl");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow + sourceID);
//		lmsQueueMst.setJobGroup(workflow);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID);
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//		eventBus.post(variables);

	}

	@Override
	public void updatePhysicalStock(CompanyMst companyMst, ItemMst item, String batchCode, double mrp, Date expiryDate,
			Double systemQty, double qty, String voucherNumber, Date VoucherDate, String branchCode) {
		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		itemBatchDtl.setCompanyMst(companyMst);
		itemBatchDtl.setBarcode(item.getBarCode());
		itemBatchDtl.setBatch(batchCode);
		itemBatchDtl.setItemId(item.getId());
		itemBatchDtl.setBranchCode(branchCode);
		itemBatchDtl.setParticulars("Physical Stock Entry");
		itemBatchDtl.setMrp(mrp);
		itemBatchDtl.setExpDate(expiryDate);

		double delta = systemQty - qty;

		System.out.println("delta Qty = " + delta);

		if (delta < -0.01) {
			delta = delta * -1;
			itemBatchDtl.setQtyIn(delta);
			itemBatchDtl.setQtyOut(0.0);

			itemBatchDtl.setVoucherDate(VoucherDate);

			VoucherNumber voucherNo = voucherService.generateInvoice(SystemSetting.VOUCHER_NAME_GENERATOR_ID,
					companyMst.getId());

			itemBatchDtl.setVoucherNumber(voucherNo.getCode());

			itemBatchDtl.setSourceVoucherNumber(voucherNumber);
			itemBatchDtl.setSourceVoucherDate(VoucherDate);
			itemBatchDtl.setBranchCode(branchCode);
			itemBatchDtl.setStore(MapleConstants.Store);
			itemBatchDtl = itemBatchDtlRepo.save(itemBatchDtl);
			// -------------version 5.4 surya

			ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();

			itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
			itemBatchDtlDaily.setBatch(itemBatchDtl.getBatch());
			itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
			itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
			itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
			itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
			itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
			itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
			itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
			itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
			itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
			itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());
			itemBatchDtlDaily.setStore(MapleConstants.Store);
			itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
			itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());

			itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);

			// ---------------version 5.4 surya end

//	resultArray.add(item);
			//eventBus.post(itemBatchDtl);
//			Map<String, Object> variables = new HashMap<String, Object>();
//
//			variables.put("voucherNumber", itemBatchDtl.getId());
//			variables.put("id", itemBatchDtl.getId());
//			variables.put("voucherDate", SystemSetting.getSystemDate());
//			variables.put("inet", 0);
//			variables.put("REST", 1);
//			variables.put("companyid", itemBatchDtl.getCompanyMst());
//			variables.put("branchcode", itemBatchDtl.getBranchCode());
//
//			variables.put("WF", "forwardItemBatchDtl");
//
//			String workflow = (String) variables.get("WF");
//			String voucherNumber1 = (String) variables.get("voucherNumber");
//			String sourceID = (String) variables.get("id");
//
//			LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//			// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//			java.util.Date uDate = (Date) variables.get("voucherDate");
//			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//			lmsQueueMst.setVoucherDate(sqlVDate);
//
//			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//			lmsQueueMst.setVoucherType(workflow);
//			lmsQueueMst.setPostedToServer("NO");
//			lmsQueueMst.setJobClass("forwardItemBatchDtl");
//			lmsQueueMst.setCronJob(true);
//			lmsQueueMst.setJobName(workflow + sourceID);
//			lmsQueueMst.setJobGroup(workflow);
//			lmsQueueMst.setRepeatTime(60000L);
//			lmsQueueMst.setSourceObjectId(sourceID);
//
//			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//			lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//			variables.put("lmsqid", lmsQueueMst.getId());
//			eventBus.post(variables);

		} else if (delta > 0.01) {

			// delta = delta * -1 ;
			itemBatchDtl.setQtyOut(delta);
			itemBatchDtl.setQtyIn(0.0);

			itemBatchDtl.setVoucherDate(VoucherDate);

			VoucherNumber voucherNo = voucherService.generateInvoice(SystemSetting.VOUCHER_NAME_GENERATOR_ID,
					companyMst.getId());

			itemBatchDtl.setVoucherNumber(voucherNo.getCode());

			itemBatchDtl.setSourceVoucherNumber(voucherNumber);
			itemBatchDtl.setSourceVoucherDate(VoucherDate);
			itemBatchDtl.setBranchCode(branchCode);
			itemBatchDtl.setStore(MapleConstants.Store);
			itemBatchDtl = itemBatchDtlRepo.save(itemBatchDtl);
			eventBus.post(itemBatchDtl);
			ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();

			itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
			itemBatchDtlDaily.setBatch(itemBatchDtl.getBatch());
			itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
			itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
			itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
			itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
			itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
			itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
			itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
			itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
			itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
			itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());
			itemBatchDtlDaily.setStore(MapleConstants.Store);
			itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
			itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());

			itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);

//	resultArray.add(item);

//			Map<String, Object> variables = new HashMap<String, Object>();
//
//			variables.put("voucherNumber", itemBatchDtl.getId());
//			variables.put("id", itemBatchDtl.getId());
//			variables.put("voucherDate", SystemSetting.getSystemDate());
//			variables.put("inet", 0);
//			variables.put("REST", 1);
//			variables.put("companyid", itemBatchDtl.getCompanyMst());
//			variables.put("branchcode", itemBatchDtl.getBranchCode());
//
//			variables.put("WF", "forwardItemBatchDtl");
//
//			String workflow = (String) variables.get("WF");
//			String voucherNumber1 = (String) variables.get("voucherNumber");
//			String sourceID = (String) variables.get("id");
//
//			LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//			// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//			java.util.Date uDate = (Date) variables.get("voucherDate");
//			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//			lmsQueueMst.setVoucherDate(sqlVDate);
//
//			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//			lmsQueueMst.setVoucherType(workflow);
//			lmsQueueMst.setPostedToServer("NO");
//			lmsQueueMst.setJobClass("forwardItemBatchDtl");
//			lmsQueueMst.setCronJob(true);
//			lmsQueueMst.setJobName(workflow + sourceID);
//			lmsQueueMst.setJobGroup(workflow);
//			lmsQueueMst.setRepeatTime(60000L);
//			lmsQueueMst.setSourceObjectId(sourceID);
//
//			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//			lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//			variables.put("lmsqid", lmsQueueMst.getId());
//			eventBus.post(variables);

		} else if (delta == 0) {
			itemBatchDtl.setQtyOut(0.0);
			itemBatchDtl.setQtyIn(0.0);

		}

	}

	@Override
	public void updateDamageStock(ItemMst itemMst, DamageDtl damageDtl, String branchCode, CompanyMst companyMst,
			String damageHdrId) {

		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		final VoucherNumber voucherNo = voucherService.generateInvoice("STV", companyMst.getId());
		itemBatchDtl.setBarcode(itemMst.getBarCode());
		itemBatchDtl.setBatch(damageDtl.getBatchCode());
		itemBatchDtl.setItemId(itemMst.getId());
		itemBatchDtl.setMrp(itemMst.getStandardPrice());
		itemBatchDtl.setQtyIn(0.0);
		itemBatchDtl.setStore(MapleConstants.Store);
		itemBatchDtl.setQtyOut(damageDtl.getQty());
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(damageDtl.getDamageHdr().getVoucherDate());
		itemBatchDtl.setBranchCode(branchCode);
		itemBatchDtl.setSourceVoucherNumber(damageDtl.getDamageHdr().getVoucheNumber());
		itemBatchDtl.setCompanyMst(companyMst);
		itemBatchDtl.setSourceParentId(damageHdrId);
		itemBatchDtl.setSourceDtlId(damageDtl.getId());
		itemBatchDtl.setStore(MapleConstants.Store);
		itemBatchDtl.setParticulars("DAMAGE ENTRY");
		itemBatchDtlRepo.save(itemBatchDtl);
		eventBus.post(itemBatchDtl);
		ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();

		itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
		itemBatchDtlDaily.setBatch(itemBatchDtl.getBatch());
		itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
		itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
		itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
		itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
		itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
		itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
		itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
		itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
		itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
		itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());
		itemBatchDtlDaily.setStore(MapleConstants.Store);
		itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
		itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());

		itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);

//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("voucherNumber", itemBatchDtl.getId());
//		variables.put("id", itemBatchDtl.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("REST", 1);
//		variables.put("companyid", itemBatchDtl.getCompanyMst());
//		variables.put("branchcode", itemBatchDtl.getBranchCode());
//
//		variables.put("WF", "forwardItemBatchDtl");
//
//		String workflow = (String) variables.get("WF");
//		String voucherNumber1 = (String) variables.get("voucherNumber");
//		String sourceID = (String) variables.get("id");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//		lmsQueueMst.setVoucherDate(sqlVDate);
//
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardItemBatchDtl");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow + sourceID);
//		lmsQueueMst.setJobGroup(workflow);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID);
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//		eventBus.post(variables);
	}

	@Override
	public void updateProductionStock(ItemMst itemMst, ActualProductionDtl actualProductionDtl, CompanyMst companyMst,
			String actualProductionHdrId, ActualProductionHdr actualProductionHdr) {
		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		final VoucherNumber voucherNo = voucherService.generateInvoice("STV", companyMst.getId());
		itemBatchDtl.setBarcode(itemMst.getBarCode());
		if (actualProductionDtl.getBatch() != null) {
			itemBatchDtl.setBatch(actualProductionDtl.getBatch());
		} else {
			itemBatchDtl.setBatch(MapleConstants.Nobatch);
		}
		itemBatchDtl.setItemId(actualProductionDtl.getItemId());
		itemBatchDtl.setMrp(itemMst.getStandardPrice());
		itemBatchDtl.setQtyIn(actualProductionDtl.getActualQty());
		itemBatchDtl.setQtyOut(0.0);
		itemBatchDtl.setBranchCode(actualProductionHdr.getBranchCode());
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(actualProductionDtl.getActualProductionHdr().getVoucherDate());
		itemBatchDtl.setItemId(actualProductionDtl.getItemId());
		itemBatchDtl.setSourceVoucherNumber(actualProductionDtl.getActualProductionHdr().getVoucherNumber());
		itemBatchDtl.setParticulars("PRODUCTION");
		itemBatchDtl.setCompanyMst(companyMst);
		itemBatchDtl.setStore(MapleConstants.Store);
		itemBatchDtl.setSourceParentId(actualProductionHdrId);
		itemBatchDtl.setSourceDtlId(actualProductionDtl.getId());
		itemBatchDtlRepo.save(itemBatchDtl);
		//eventBus.post(itemBatchDtl);
		ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();

		itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
		itemBatchDtlDaily.setBatch(itemBatchDtl.getBatch());
		itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
		itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
		itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
		itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
		itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
		itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
		itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
		itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
		itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
		itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());
		itemBatchDtlDaily.setStore(MapleConstants.Store);
		itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
		itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());

		itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);
//
//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("voucherNumber", itemBatchDtl.getId());
//		variables.put("id", itemBatchDtl.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("REST", 1);
//		variables.put("companyid", itemBatchDtl.getCompanyMst());
//		variables.put("branchcode", itemBatchDtl.getBranchCode());
//
//		variables.put("WF", "forwardItemBatchDtl");
//
//		String workflow = (String) variables.get("WF");
//		String voucherNumber1 = (String) variables.get("voucherNumber");
//		String sourceID = (String) variables.get("id");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//		lmsQueueMst.setVoucherDate(sqlVDate);
//
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardItemBatchDtl");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow + sourceID);
//		lmsQueueMst.setJobGroup(workflow);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID);
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//		eventBus.post(variables);
	}

	@Override
	public void updateStockTransferStock(StockTransferOutDtl stockTransferOutDtl,
			StockTransferOutHdr stockTransferOutHdr) {
		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		final VoucherNumber voucherNo = voucherService.generateInvoice("STV",
				stockTransferOutHdr.getCompanyMst().getId());

		itemBatchDtl.setBarcode(stockTransferOutDtl.getBarcode());
		itemBatchDtl.setBatch(stockTransferOutDtl.getBatch());
		itemBatchDtl.setItemId(stockTransferOutDtl.getItemId().getId());
		itemBatchDtl.setMrp(stockTransferOutDtl.getMrp());
		itemBatchDtl.setQtyIn(0.0);
		itemBatchDtl.setQtyOut(stockTransferOutDtl.getQty());
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(stockTransferOutHdr.getVoucherDate());
		itemBatchDtl.setItemId(stockTransferOutDtl.getItemId().getId());

		itemBatchDtl.setSourceVoucherNumber(stockTransferOutHdr.getVoucherNumber());
		itemBatchDtl.setSourceVoucherDate(stockTransferOutHdr.getVoucherDate());

		itemBatchDtl.setParticulars("STOCK TRANSFER TO " + stockTransferOutHdr.getToBranch());
		itemBatchDtl.setBranchCode(stockTransferOutHdr.getFromBranch());
		itemBatchDtl.setCompanyMst(stockTransferOutHdr.getCompanyMst());
		itemBatchDtl.setSourceParentId(stockTransferOutHdr.getId());
		itemBatchDtl.setSourceDtlId(stockTransferOutDtl.getId());
		if (null != stockTransferOutDtl.getStoreName()) {
			itemBatchDtl.setStore(stockTransferOutDtl.getStoreName());
		} else {
			itemBatchDtl.setStore(MapleConstants.Store);
		}
		itemBatchDtl = itemBatchDtlRepo.save(itemBatchDtl);
		//eventBus.post(itemBatchDtl);

		ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();

		itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
		itemBatchDtlDaily.setBatch(itemBatchDtl.getBatch());
		itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
		itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
		itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
		itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
		itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
		itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
		itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
		itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
		itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
		itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());
		if (null != stockTransferOutDtl.getStoreName()) {
			itemBatchDtlDaily.setStore(stockTransferOutDtl.getStoreName());
		} else {
			itemBatchDtlDaily.setStore(MapleConstants.Store);
		}
		itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
		itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());

		itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);

//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("voucherNumber", itemBatchDtl.getId());
//		variables.put("id", itemBatchDtl.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("REST", 1);
//		variables.put("branchcode", itemBatchDtl.getBranchCode());
//		variables.put("companyid", itemBatchDtl.getCompanyMst());
//
//		variables.put("WF", "forwardItemBatchDtl");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		String workflow = (String) variables.get("WF");
//		String sourceID = itemBatchDtl.getId();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//		lmsQueueMst.setVoucherDate(sqlVDate);
//
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardItemBatchDtl");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow + sourceID);
//		lmsQueueMst.setJobGroup(workflow);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID);
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//
//		eventBus.post(variables);
	}

	@Override
	public void updateAcceptStock(String barcode, String batchCode, String itemId, double mrp, double qty,
			Date transDate, String branchCode, String voucherNumber, Date transDate2, CompanyMst compasnyMst,
			@Valid StockTransferInHdr stockTransferInHdr, StockTransferInDtl stockTransferInDtl) {

		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		final VoucherNumber voucherNo = voucherService.generateInvoice("STV", compasnyMst.getId());

		itemBatchDtl.setBarcode(barcode);
		itemBatchDtl.setBatch(batchCode);
		itemBatchDtl.setItemId(itemId);
		itemBatchDtl.setMrp(mrp);
		itemBatchDtl.setQtyIn(qty);
		itemBatchDtl.setQtyOut(0.0);
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(transDate);

		itemBatchDtl.setBranchCode(branchCode);
		itemBatchDtl.setSourceVoucherNumber(voucherNumber);
		itemBatchDtl.setSourceVoucherDate(transDate);

		itemBatchDtl.setCompanyMst(compasnyMst);
		itemBatchDtl.setParticulars("STOCK TRANSFER IN FROM " + stockTransferInHdr.getFromBranch());
		itemBatchDtl.setStore(MapleConstants.Store);
		itemBatchDtl.setSourceParentId(stockTransferInHdr.getId());
		itemBatchDtl.setSourceDtlId(stockTransferInDtl.getId());
		itemBatchDtl = itemBatchDtlRepo.save(itemBatchDtl);

		ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();
		itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
		itemBatchDtlDaily.setBatch(itemBatchDtl.getBatch());
		itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
		itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
		itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
		itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
		itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
		itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
		itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
		itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
		itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
		itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());
		itemBatchDtlDaily.setStore(MapleConstants.Store);
		itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
		itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());
		itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);
		//eventBus.post(itemBatchDtl);
//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("voucherNumber", itemBatchDtl.getId());
//		variables.put("id", itemBatchDtl.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("REST", 1);
//		variables.put("companyid", itemBatchDtl.getCompanyMst());
//		variables.put("branchcode", itemBatchDtl.getBranchCode());
//
//		variables.put("WF", "forwardItemBatchDtl");
//
//		String workflow = (String) variables.get("WF");
//		String voucherNumber1 = (String) variables.get("voucherNumber");
//		String sourceID = (String) variables.get("id");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//		lmsQueueMst.setVoucherDate(sqlVDate);
//
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardItemBatchDtl");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow + sourceID);
//		lmsQueueMst.setJobGroup(workflow);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID);
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//		eventBus.post(variables);
	}
	// -------------------version 5.5 surya

	@Override
	public void updateItemBatchDtlAndItemBatchDtlDaily(String barcode, String batchCode, String itemId,
			Double standardPrice, Double qtyIn, Double qtyOut, String branchCode, Date invoiceVoucherDate,
			String sourceVoucherNo, String particulars, String sourceParentId, String sourceDtlId,
			CompanyMst companyMst, VoucherNumber voucherNo) {

		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		itemBatchDtl.setBarcode(barcode);
		itemBatchDtl.setBatch(batchCode);
		itemBatchDtl.setItemId(itemId);
		itemBatchDtl.setMrp(standardPrice);
		itemBatchDtl.setQtyIn(qtyIn);
		itemBatchDtl.setQtyOut(qtyOut);
		itemBatchDtl.setBranchCode(branchCode);
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(invoiceVoucherDate);
		itemBatchDtl.setSourceVoucherNumber(sourceVoucherNo);
		itemBatchDtl.setCompanyMst(companyMst);
		itemBatchDtl.setParticulars("PRODUCT CONVERSION");
		itemBatchDtl.setStore(MapleConstants.Store);
		itemBatchDtl.setSourceParentId(sourceParentId);
		itemBatchDtl.setSourceDtlId(sourceDtlId);
		itemBatchDtlRepo.save(itemBatchDtl);

		ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();
		itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
		itemBatchDtlDaily.setBatch(itemBatchDtl.getBatch());
		itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
		itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
		itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
		itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
		itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
		itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
		itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
		itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
		itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
		itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());
		itemBatchDtlDaily.setStore(MapleConstants.Store);
		itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
		itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());
		itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);
		//eventBus.post(itemBatchDtl);
//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("voucherNumber", itemBatchDtl.getId());
//		variables.put("id", itemBatchDtl.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("REST", 1);
//		variables.put("companyid", itemBatchDtl.getCompanyMst());
//		variables.put("branchcode", itemBatchDtl.getBranchCode());
//
//		variables.put("WF", "forwardItemBatchDtl");
//
//		String workflow = (String) variables.get("WF");
//		String voucherNumber1 = (String) variables.get("voucherNumber");
//		String sourceID = (String) variables.get("id");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//		lmsQueueMst.setVoucherDate(sqlVDate);
//
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardItemBatchDtl");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow + sourceID);
//		lmsQueueMst.setJobGroup(workflow);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID);
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//		eventBus.post(variables);

	}

	@Override
	public void updateConsumptionStock(ItemMst item, ConsumptionDtl consumptionDtl, ConsumptionHdr consumptionHdr) {

		Double conversionQty = 0.0;

		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		final VoucherNumber voucherNo = voucherService.generateInvoice("CONSUMPTION",
				consumptionHdr.getCompanyMst().getId());

		if (!item.getUnitId().equalsIgnoreCase(consumptionDtl.getUnitId())) {

			String itemId = item.getId();
			String sourceUnit = consumptionDtl.getUnitId();
			String targetUnit = item.getUnitId();
			recurssionOff = false;

			conversionQty = getConvertionQty(consumptionHdr.getCompanyMst().getId(), item.getId(), sourceUnit,
					targetUnit, consumptionDtl.getQty());
			itemBatchDtl.setQtyOut(conversionQty);
		} else {
			itemBatchDtl.setQtyOut(consumptionDtl.getQty());
		}

		itemBatchDtl.setBarcode(item.getBarCode());
		itemBatchDtl.setBatch(consumptionDtl.getBatch());
		itemBatchDtl.setItemId(consumptionDtl.getItemId());
		itemBatchDtl.setMrp(consumptionDtl.getMrp());

		itemBatchDtl.setQtyIn(0.0);
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(consumptionHdr.getVoucherDate());
		itemBatchDtl.setItemId(consumptionDtl.getItemId());

		itemBatchDtl.setSourceVoucherNumber(consumptionHdr.getVoucherNumber());

		itemBatchDtl.setSourceVoucherDate(consumptionHdr.getVoucherDate());
		itemBatchDtl.setBranchCode(consumptionHdr.getBranchCode());

		itemBatchDtl.setParticulars("CONSUMPTION");

		itemBatchDtl.setCompanyMst(consumptionHdr.getCompanyMst());
		itemBatchDtl.setStore(MapleConstants.Store);
		itemBatchDtl.setSourceParentId(consumptionHdr.getId());
		itemBatchDtl.setSourceDtlId(consumptionDtl.getId());
		itemBatchDtl = itemBatchDtlRepo.save(itemBatchDtl);

		ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();
		itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
		itemBatchDtlDaily.setBatch(itemBatchDtl.getBatch());
		itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
		itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
		itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
		itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
		itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
		itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
		itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
		itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
		itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
		itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());
		itemBatchDtlDaily.setStore(MapleConstants.Store);
		itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
		itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());
		itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);
		//eventBus.post(itemBatchDtl);
//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("voucherNumber", itemBatchDtl.getId());
//		variables.put("id", itemBatchDtl.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("REST", 1);
//		variables.put("companyid", consumptionHdr.getCompanyMst());
//		variables.put("WF", "forwardItemBatchDtl");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(uDate);
//
//		uDate = SystemSetting.SqlDateToUtilDate(sqlDate);
//		lmsQueueMst.setVoucherDate(sqlDate);
//
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType("forwardItemBatchDtl");
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardItemBatchDtl");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName("forwardItemBatchDtl" + itemBatchDtl.getId());
//		lmsQueueMst.setJobGroup("forwardItemBatchDtl");
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(itemBatchDtl.getId());
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//
//		eventBus.post(variables);

	}

	@Override
	public void ProductionRawMaterialStock(String rawMaterialId, ItemMst itemMst, Double requiredRawMaterial,
			ActualProductionDtl actualProductionDtl, ActualProductionHdr actualProductionHdr, CompanyMst companyMst,
			String batch) {

		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		final VoucherNumber voucherNo = voucherService.generateInvoice("STV", companyMst.getId());

		itemBatchDtl.setBarcode(itemMst.getBarCode());
		itemBatchDtl.setBatch(batch);
		itemBatchDtl.setItemId(rawMaterialId);
		itemBatchDtl.setMrp(itemMst.getStandardPrice());
		itemBatchDtl.setQtyIn(0.0);
		itemBatchDtl.setQtyOut(requiredRawMaterial);
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(actualProductionDtl.getActualProductionHdr().getVoucherDate());
		itemBatchDtl.setItemId(rawMaterialId);
		ItemMst itemmst = itemMstRepo.findById(actualProductionDtl.getItemId()).get();
		itemBatchDtl.setParticulars(itemmst.getItemName() + actualProductionDtl.getActualQty() + "PRODUCTION");

		itemBatchDtl.setStore(MapleConstants.Store);
		itemBatchDtl.setSourceVoucherNumber(actualProductionDtl.getActualProductionHdr().getVoucherNumber());

		itemBatchDtl.setBranchCode(actualProductionHdr.getBranchCode());
		itemBatchDtl.setCompanyMst(companyMst);

		logger.info("2222222222222" + itemmst.getItemName());
		itemBatchDtl = itemBatchDtlRepo.save(itemBatchDtl);
		eventBus.post(itemBatchDtl);
		ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();
		itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
		itemBatchDtlDaily.setBatch(itemBatchDtl.getBatch());
		itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
		itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
		itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
		itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
		itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
		itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
		itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
		itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
		itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
		itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());
		itemBatchDtlDaily.setStore(MapleConstants.Store);
		itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
		itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());
		itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);
//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("voucherNumber", itemBatchDtl.getId());
//		variables.put("id", itemBatchDtl.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("REST", 1);
//		variables.put("companyid", itemBatchDtl.getCompanyMst());
//		variables.put("WF", "forwardItemBatchDtl");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(uDate);
//
//		uDate = SystemSetting.SqlDateToUtilDate(sqlDate);
//		lmsQueueMst.setVoucherDate(sqlDate);
//
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType("forwardItemBatchDtl");
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardItemBatchDtl");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName("forwardItemBatchDtl" + itemBatchDtl.getId());
//		lmsQueueMst.setJobGroup("forwardItemBatchDtl");
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(itemBatchDtl.getId());
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//
//		eventBus.post(variables);

	}
	// -------------------version 5.5 surya end

	@Override
	public List<StockReport> getFastMovingItem(Date fuDate, Date tuDate, String companymstid, String branchcode,
			String reportName) {

		ArrayList<StockReport> stockList = new ArrayList<StockReport>();
		List<Object> ob = itemBatchDtlRepo.getFastMovingItems(fuDate, tuDate, companymstid, branchcode, reportName);
		for (int i = 0; i < ob.size(); i++) {

			Object[] objAray = (Object[]) ob.get(i);
			StockReport stockReport = new StockReport();
			stockReport.setItemName((String) objAray[0]);
			stockReport.setQty((Double) objAray[1]);
			stockList.add(stockReport);
		}

		return stockList;
	}

	// ------------------version 2.2.1

	@Override
	public void ItemBatchDtlUpdation(String itemId, String sourceParentId, String sourceDtlId, Double qtyIn,
			Double qtyOut, Double mrp, String unitId, ItemBatchMst itemBatchMst, Date voucherDate, String voucherNumber,
			String particular, CompanyMst companyMst, String batch, String branchCode, String STORE) {

		Double qty = 0.0;

		if (qtyIn > 0.0) {
			qty = qtyIn;
		}
		if (qtyOut > 0.0) {
			qty = qtyOut;
		}

		Optional<ItemMst> itemMstOpt = itemMstRepo.findById(itemId);
		ItemMst item = itemMstOpt.get();

		double conversionQty = 0.0;

		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		final VoucherNumber voucherNo = voucherService.generateInvoice("STV", companyMst.getId());
		if (!item.getUnitId().equalsIgnoreCase(unitId)) {

			String targetUnit = item.getUnitId();

			recurssionOff = false;
			conversionQty = getConvertionQty(companyMst.getId(), itemId, unitId, targetUnit, qty);

			if (qtyIn > 0.0) {
				itemBatchDtl.setQtyIn(conversionQty);
				itemBatchDtl.setQtyIn(0.0);

			}
			if (qtyOut > 0.0) {
				itemBatchDtl.setQtyOut(conversionQty);
				itemBatchDtl.setQtyIn(0.0);

			}
		} else {
			itemBatchDtl.setQtyOut(qtyOut);
			itemBatchDtl.setQtyIn(qtyIn);

		}
		itemBatchDtl.setBarcode(item.getBarCode());
		itemBatchDtl.setBatch(batch);
		itemBatchDtl.setItemId(itemId);
		itemBatchDtl.setMrp(mrp);
		itemBatchDtl.setExpDate(itemBatchMst.getExpDate());
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(voucherDate);
		itemBatchDtl.setSourceVoucherNumber(voucherNumber);
		itemBatchDtl.setSourceVoucherDate(voucherDate);
		itemBatchDtl.setCompanyMst(companyMst);
		itemBatchDtl.setBranchCode(branchCode);

		// List<ItemBatchDtl> itemBatchDtlList = itemBatchDtlRepository
		itemBatchDtl.setSourceParentId(sourceParentId);
		itemBatchDtl.setSourceDtlId(sourceDtlId);
		itemBatchDtl.setParticulars(particular);
		itemBatchDtl.setStore(STORE);
		itemBatchDtl = itemBatchDtlRepo.save(itemBatchDtl);
		//eventBus.post(itemBatchDtl);
//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("voucherNumber", itemBatchDtl.getId());
//		variables.put("id", itemBatchDtl.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("REST", 1);
//		variables.put("companyid", itemBatchDtl.getCompanyMst());
//		variables.put("branchcode", itemBatchDtl.getBranchCode());
//
//		variables.put("WF", "forwardItemBatchDtl");
//
//		String workflow = (String) variables.get("WF");
//		String voucherNumber1 = (String) variables.get("voucherNumber");
//		String sourceID = (String) variables.get("id");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//		lmsQueueMst.setVoucherDate(sqlVDate);
//
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardItemBatchDtl");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow + sourceID);
//		lmsQueueMst.setJobGroup(workflow);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID);
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//		eventBus.post(variables);
		ItemBatchDtlDaily itemBatchDtlDaily = new ItemBatchDtlDaily();

		itemBatchDtlDaily.setBarcode(itemBatchDtl.getBarcode());
		itemBatchDtlDaily.setBatch(itemBatchDtl.getBatch());
		itemBatchDtlDaily.setBranchCode(itemBatchDtl.getBranchCode());
		itemBatchDtlDaily.setCompanyMst(itemBatchDtl.getCompanyMst());
		itemBatchDtlDaily.setExpDate(itemBatchDtl.getExpDate());
		itemBatchDtlDaily.setItemId(itemBatchDtl.getItemId());
		itemBatchDtlDaily.setMrp(itemBatchDtl.getMrp());
		itemBatchDtlDaily.setParticulars(itemBatchDtl.getParticulars());
		itemBatchDtlDaily.setQtyIn(itemBatchDtl.getQtyIn());
		itemBatchDtlDaily.setQtyOut(itemBatchDtl.getQtyOut());
		itemBatchDtlDaily.setSourceDtlId(itemBatchDtl.getSourceDtlId());
		itemBatchDtlDaily.setSourceParentId(itemBatchDtl.getSourceParentId());

		// itemBatchDtlDaily.setStore("MAIN");
		itemBatchDtlDaily.setStore(STORE);

		itemBatchDtlDaily.setVoucherDate(itemBatchDtl.getVoucherDate());
		itemBatchDtlDaily.setVoucherNumber(itemBatchDtl.getVoucherNumber());

		itemBatchDtlDaily = itemBatchDtlDailyRepository.save(itemBatchDtlDaily);

	}

	// ------------------version 2.2.1 end
	@Override
	public List<StockReport> fastMovingItemsBySalesMode(Date fuDate, Date tuDate, String companymstid,
			String branchcode, String salesmode, String reportname) {
		ArrayList<StockReport> stockList = new ArrayList<StockReport>();
		List<Object> ob = itemBatchDtlRepo.fastMovingItemsBySalesMode(fuDate, tuDate, companymstid, branchcode,
				salesmode, reportname);

		for (int i = 0; i < ob.size(); i++) {

			Object[] objAray = (Object[]) ob.get(i);
			StockReport stockReport = new StockReport();
			stockReport.setItemName((String) objAray[0]);
			stockReport.setQty((Double) objAray[1]);
			stockList.add(stockReport);
		}

		return stockList;
	}

	@Override
	public List<StockReport> fastMovingItemsBySalesModeAndCategory(Date fuDate, Date tuDate, String companymstid,
			String branchcode, String salesmode, String category, String reportname) {

		ArrayList<StockReport> stockList = new ArrayList<StockReport>();
		List<Object> ob = itemBatchDtlRepo.fastMovingItemsBySalesModeAndCategory(fuDate, tuDate, companymstid,
				branchcode, salesmode, category);

		for (int i = 0; i < ob.size(); i++) {

			Object[] objAray = (Object[]) ob.get(i);
			StockReport stockReport = new StockReport();
			stockReport.setItemName((String) objAray[0]);
			stockReport.setQty((Double) objAray[1]);
			stockList.add(stockReport);
		}

		return stockList;

	}

	@Override
	public Map<String, Double> getBatchWiseQtyFromMobileStore(String itemName, double reqQty, String storeName) {
		Map<String, Double> map = new HashMap<String, Double>();
		// Check for total qty
		Double totalQty = itemBatchDtlRepo.getTotalQty(itemName);
		if (null == totalQty) {
			map.put(MapleConstants.Nobatch, reqQty);

			return map;
		} else if (reqQty > totalQty) {

			map.put(MapleConstants.Nobatch, reqQty);

			return map;
		}
		// ItemMst items = itemMstRepository.findByItemName(itemName);
		List<Object> itemBatchList = itemBatchDtlRepo.findByItemIdOrderByExpDateByStore(itemName, storeName);
		for (int i = 0; i < itemBatchList.size(); i++) {
			Object[] objAray = (Object[]) itemBatchList.get(i);
			double stkQty = ((Double) objAray[0]);
			BigDecimal bSQty = new BigDecimal(stkQty);
			bSQty = bSQty.setScale(6, BigDecimal.ROUND_HALF_DOWN);
			stkQty = bSQty.doubleValue();
			String batch = ((String) objAray[1]);
			if (reqQty > 0) {
				if (reqQty > stkQty) {
					if (stkQty > 0) {
						map.put(batch, stkQty);
						reqQty = reqQty - stkQty;
					}
				} else {
					map.put(batch, reqQty);
					reqQty = reqQty - reqQty;
					break;
				}
			}

		}
		return map;
	}

	@Override
	public void itemBatchDtlInsertion(java.util.Date tdate, CompanyMst companyMst) {

		List<ItemBatchDtl> itemBatchDtlList = itemBatchDtlRepo.findByCompanyMst(companyMst);

		for (ItemBatchDtl item : itemBatchDtlList) {

			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();

			itemBatchDtl.setBarcode(item.getBarcode());
			itemBatchDtl.setBatch(item.getBatch());
			itemBatchDtl.setBranchCode(item.getBranchCode());
			itemBatchDtl.setCompanyMst(companyMst);
			itemBatchDtl.setExpDate(item.getExpDate());
			itemBatchDtl.setItemId(item.getItemId());
			itemBatchDtl.setMrp(item.getMrp());
			itemBatchDtl.setParticulars(item.getParticulars());
			itemBatchDtl.setQtyIn(item.getQtyIn());
			itemBatchDtl.setQtyOut(item.getQtyOut());
			itemBatchDtl.setSourceDtlId(item.getSourceDtlId());
			itemBatchDtl.setSourceParentId(item.getSourceParentId());
			itemBatchDtl.setSourceVoucherNumber(item.getSourceVoucherNumber());
			itemBatchDtl.setStore(MapleConstants.Store);
			itemBatchDtl.setVoucherDate(tdate);
			itemBatchDtl.setVoucherNumber(item.getVoucherNumber());

			//itemBatchDtlRepo.save(itemBatchDtl);
			eventBus.post(itemBatchDtl);
		}
	}

	@Override
	public List<CategoryWiseStockMovement> getItemWiseStockMovementReport(String branchcode, String companymstid,
			java.sql.Date fdate, java.sql.Date tdate) {

		List<CategoryWiseStockMovement> categoryStockMvmntList = new ArrayList();
		List<Object> obj = itemBatchDtlRepo.getItemStockMovement(branchcode, companymstid, fdate, tdate);

		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			CategoryWiseStockMovement categoryWiseStockMovement = new CategoryWiseStockMovement();
			categoryWiseStockMovement.setCategory((String) objAray[0]);
			categoryWiseStockMovement.setInWardQty((Double) objAray[1]);
			categoryWiseStockMovement.setOutWardQty((Double) objAray[2]);
//			categoryWiseStockMovement.setVoucherDate((Date) objAray[3]);
			List<Object> objOpeningStk = itemBatchDtlRepo.ItemWiseOPeningStockReport(branchcode, companymstid, fdate);

			if (null == objOpeningStk) {
				for (int j = 0; j < objOpeningStk.size(); j++) {
					Object[] objctAray = (Object[]) objOpeningStk.get(j);
					if (categoryWiseStockMovement.getCategory().equalsIgnoreCase((String) objctAray[0])) {
						categoryWiseStockMovement.setOpeningStock((Double) objctAray[1]);
					}

					List<Object> objClosingStk = itemBatchDtlRepo.ItemWiseClosingStockReport(branchcode, companymstid,
							fdate, tdate);
					for (int k = 0; k < objClosingStk.size(); k++) {

						Object[] objctArray = (Object[]) objClosingStk.get(k);
						if (categoryWiseStockMovement.getCategory().equalsIgnoreCase((String) objctArray[0])) {
							if (null == categoryWiseStockMovement.getOpeningStock()) {
								categoryWiseStockMovement.setClosingStock((Double) objctArray[1] + 0.0);
							} else {
								categoryWiseStockMovement.setClosingStock(
										(Double) objctArray[1] + categoryWiseStockMovement.getOpeningStock());
							}
						}

					}
				}
			} else {
				List<Object> objClosingStk = itemBatchDtlRepo.ItemWiseClosingStockReport(branchcode, companymstid,
						fdate, tdate);
				for (int k = 0; k < objClosingStk.size(); k++) {

					Object[] objctArray = (Object[]) objClosingStk.get(k);
					if (categoryWiseStockMovement.getCategory().equalsIgnoreCase((String) objctArray[0])) {

						categoryWiseStockMovement.setClosingStock((Double) objctArray[1] + 0.0);

					}

				}

			}

			categoryStockMvmntList.add(categoryWiseStockMovement);

		}
		return categoryStockMvmntList;

	}

	public JSONArray getBatchWiseQty(String itemName, double reqQty) {
//		Map<String, Double> map = new HashMap<String, Double>();
		// Check for total qty
		Double totalQty = itemBatchDtlRepo.getTotalQty(itemName);
		if (null == totalQty) {
			return null;
		} else if (reqQty > totalQty) {
			return null;
		}
		// ItemMst items = itemMstRepository.findByItemName(itemName);
		List<Object> itemBatchList = itemBatchDtlRepo.findByItemIdOrderByExpDate(itemName);

		JSONArray array = new JSONArray();

		for (int i = 0; i < itemBatchList.size(); i++) {
			Object[] objAray = (Object[]) itemBatchList.get(i);

			double stkQty = ((Double) objAray[0]);
			BigDecimal bSQty = new BigDecimal(stkQty);
			bSQty = bSQty.setScale(6, BigDecimal.ROUND_HALF_DOWN);
			stkQty = bSQty.doubleValue();
			String batch = ((String) objAray[1]);
			java.sql.Date expDate = null;
			if (null != objAray[2]) {
				expDate = SystemSetting.UtilDateToSQLDate((Date) objAray[2]);
//						 (( java.sql.Date) objAray[2]);

			} else {
				expDate = SystemSetting.StringToSqlDate("2050-01-01", "yyyy-MM-dd");
			}
//			 expDate =  (( java.sql.Date) objAray[2]);

			if (reqQty > 0) {
				if (reqQty > stkQty) {
					if (stkQty > 0) {

						JSONObject obj = new JSONObject();
						obj.put("batch", batch);
						obj.put("qty", stkQty);
						obj.put("exp", expDate);
						array.put(obj);

						// map.put(batch, stkQty);
						// reqQty = reqQty - stkQty;

					} else {

					}
				} else {

					JSONObject obj = new JSONObject();
					obj.put("batch", batch);
					obj.put("qty", stkQty);
					obj.put("exp", expDate);
					array.put(obj);

					// map.put(batch, reqQty);
					// reqQty = reqQty - reqQty;

					break;
				}
			}

		}
		return array;
	}



	@Override
	public List<StockMigrationReport> findAllItemBatchDtl(CompanyMst companyMst) {

		List<StockMigrationReport> stockMigrationReportList = new ArrayList();
		List<Object> obj = itemBatchDtlRepo.findAllItemBatchDtl(companyMst);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			StockMigrationReport stockMigrationReport = new StockMigrationReport();
			stockMigrationReport.setId((String) objAray[0]);
			stockMigrationReport.setBarCode((String) objAray[1]);
			stockMigrationReport.setBatch((String) objAray[2]);
			stockMigrationReport.setItemId((String) objAray[18]);
			stockMigrationReport.setItemName((String) objAray[19]);
			stockMigrationReport.setRate((Double) objAray[20]);
			stockMigrationReport.setQtyIn((Double) objAray[8]);
			stockMigrationReport.setQtyOut((Double) objAray[9]);

			stockMigrationReportList.add(stockMigrationReport);
		}

		return stockMigrationReportList;

	}

	@Override
	public void saveItemBatchDtlFromOpeningStock(OpeningStockDtl openingStockDtl) {

		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		itemBatchDtl.setBarcode(openingStockDtl.getBarcode());
		itemBatchDtl.setBatch(openingStockDtl.getBatch());
		itemBatchDtl.setBranchCode(openingStockDtl.getBranchCode());
		itemBatchDtl.setCompanyMst(openingStockDtl.getCompanyMst());
		itemBatchDtl.setExpDate(openingStockDtl.getExpDate());
		itemBatchDtl.setItemId(openingStockDtl.getItemId());
		itemBatchDtl.setMrp(openingStockDtl.getMrp());
		itemBatchDtl.setParticulars(openingStockDtl.getParticulars());
		itemBatchDtl.setQtyIn(openingStockDtl.getQtyIn());
		itemBatchDtl.setQtyOut(0.0);
		itemBatchDtl.setSourceParentId(openingStockDtl.getId());
		itemBatchDtl.setSourceVoucherDate(openingStockDtl.getVoucherDate());
		itemBatchDtl.setStore(openingStockDtl.getStore());
		itemBatchDtl.setVoucherNumber(openingStockDtl.getVoucherNumber());
		itemBatchDtlRepo.save(itemBatchDtl);
	}

	// ================stock verification in item batch dtl ==========anandu
	// ====18-06-2021========
	@Override
	public String stockVerificationForStkTransfer(String companymstid, String hdrId) {
		String stockList = null;
		List<StockTransferOutDtl> stkList = stockTransferOutDtlRepo.findByStockTransferOutHdrId(hdrId);
		for (StockTransferOutDtl stockTransferOutDtl : stkList) {
			String storeName = null;
			if (null != stockTransferOutDtl.getStoreName()) {
				storeName = stockTransferOutDtl.getStoreName();
			} else {
				storeName = MapleConstants.Store;
			}
			
	//=============MAP-146-add round off/ cast method in stock verification based on mysql or derby========anandu=======
			Double qty =0.0;
			if (dbType.contains("mysql")) {
			 qty = itemBatchDtlRepository.findQtyByItemMySql(stockTransferOutDtl.getItemId().getId(),
					stockTransferOutDtl.getBarcode(), stockTransferOutDtl.getBatch(), storeName);
			}else if(dbType.contains("derby")) {
			 qty = itemBatchDtlRepository.findQtyByItemDerby(stockTransferOutDtl.getItemId().getId(),
						stockTransferOutDtl.getBarcode(), stockTransferOutDtl.getBatch(), storeName);
			}
			
			
			if (null == qty) {
				if (null == stockList) {
					stockList = "NOT IN STOCK";
				}
				ItemMst itemMst = itemMstRepo.findById(stockTransferOutDtl.getItemId().getId()).get();
				stockList = stockList + itemMst.getItemName() + "(" + stockTransferOutDtl.getQty() + "),";

			} else if (stockTransferOutDtl.getQty() > qty) {
				if (null == stockList) {
					stockList = "NOT IN STOCK";
				}
				Double balance = stockTransferOutDtl.getQty() - qty;
				ItemMst itemMst = itemMstRepo.findById(stockTransferOutDtl.getItemId().getId()).get();
				stockList = stockList + ", " + itemMst.getItemName() + "(" + balance + ")";
			}
		}

		return stockList;
	}

	// ===================end =====================18-06-2021=============

	@Override
	public List<PharmacyItemWiseSalesReport> findPharmacyItemWiseSalesReport(Date fromDate, Date toDate) {

		List<PharmacyItemWiseSalesReport> PharmacyItemWiseSalesReportList = new ArrayList<PharmacyItemWiseSalesReport>();
		List<Object> obj = salesDetailsRepository.findPharmacyItemWiseSalesReport(fromDate, toDate);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			PharmacyItemWiseSalesReport itemWiseSalesReport = new PharmacyItemWiseSalesReport();
			itemWiseSalesReport.setInvoiceDate(SystemSetting.UtilDateToString((Date) objAray[0], "yyyy-MM-dd"));
			itemWiseSalesReport.setItemName((String) objAray[1]);
			itemWiseSalesReport.setCustomerName((String) objAray[3]);
			itemWiseSalesReport.setQty((Double) objAray[2]);
			itemWiseSalesReport.setUnitPrice((Double) objAray[4]);
			itemWiseSalesReport.setBatch((String) objAray[5]);
			

			PharmacyItemWiseSalesReportList.add(itemWiseSalesReport);

		}

		return PharmacyItemWiseSalesReportList;
	}

	// ===================end =====================18-06-2021=============

	// ======================common function for item batch dtl
	// insertion============19-06-2021=========

	@Override
	public void itemBatchDtlInsertionNew(String barcode, String batch, String branchCode, Date expiryDate,
			String itemId, Double mrp, String particulars, String processInstanceId, Double qtyIn, Double qtyOut,
			String sourceDetailId, String sourceParentId, Date sourceVoucherDate, String sourceVoucherNumber,
			String store, String taskId, Date voucherDate, String voucherNumber, CompanyMst companyMst) {

		logger.info("Inside itemBatchDtlInsertionNew");
		
		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		itemBatchDtl.setBarcode(barcode);
		itemBatchDtl.setBatch(batch);
		itemBatchDtl.setBranchCode(branchCode);
		itemBatchDtl.setExpDate(expiryDate);
		itemBatchDtl.setItemId(itemId);
		itemBatchDtl.setMrp(mrp);
		itemBatchDtl.setParticulars(particulars);
		itemBatchDtl.setProcessInstanceId(processInstanceId);
		itemBatchDtl.setQtyIn(qtyIn);
		itemBatchDtl.setQtyOut(qtyOut);
		itemBatchDtl.setSourceDtlId(sourceDetailId);
		itemBatchDtl.setSourceParentId(sourceParentId);
		itemBatchDtl.setSourceVoucherDate(sourceVoucherDate);
		itemBatchDtl.setSourceVoucherNumber(sourceVoucherNumber);
		itemBatchDtl.setStore(store);
		itemBatchDtl.setTaskId(taskId);
		itemBatchDtl.setVoucherDate(voucherDate);
		itemBatchDtl.setVoucherNumber(voucherNumber);
		itemBatchDtl.setCompanyMst(companyMst);

		itemBatchDtl = itemBatchDtlRepo.saveAndFlush(itemBatchDtl);

		
		//===========item batch dtl publishing============================
		
//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("voucherNumber", itemBatchDtl.getId());
//		variables.put("id", itemBatchDtl.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("REST", 1);
//		variables.put("branchcode", itemBatchDtl.getBranchCode());
//		variables.put("companyid", itemBatchDtl.getCompanyMst());
//
//		variables.put("WF", "forwardItemBatchDtl");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		String workflow = (String) variables.get("WF");
//		String sourceID = itemBatchDtl.getId();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//		lmsQueueMst.setVoucherDate(sqlVDate);
//
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardItemBatchDtl");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow + sourceID);
//		lmsQueueMst.setJobGroup(workflow);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID);
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		//lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//logger.info("saving to event bus");
		//eventBus.post(variables);
		
		saveAndPublishService.publishObjectToKafkaEvent(itemBatchDtl,
				branchCode, KafkaMapleEventType.ITEMBATCHDTL,
				KafkaMapleEventType.SERVER,
				itemBatchDtl.getId());
	}

	// ======================common function for item batch mst
	// updation============19-06-2021=========

	@Override
	public void itemBatchMstUpdation(String barcode, String batch, String branchCode, Date expiryDate, String itemId,
			Double mrp, String processInstanceId, Double qty, String taskId, CompanyMst companyMst, String store) {

		List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
				.findByItemIdAndBatchAndBarcodeAndStore(itemId, batch, barcode, store);

		if (itembatchmst.size() == 1) {
			ItemBatchMst itemBatchMst = itembatchmst.get(0);
			itemBatchMst.setMrp(mrp);
			itemBatchMst.setQty(qty + itemBatchMst.getQty());
			itemBatchMst.setCompanyMst(companyMst);
			itemBatchMst.setBranchCode(branchCode);
			itemBatchMst.setStore(store);

			itemBatchMstRepo.saveAndFlush(itemBatchMst);

		} else {
			ItemBatchMst itemBatchMst = new ItemBatchMst();
			itemBatchMst.setBarcode(barcode);
			itemBatchMst.setBatch(batch);
			itemBatchMst.setBranchCode(branchCode);
			itemBatchMst.setExpDate(expiryDate);
			itemBatchMst.setItemId(itemId);
			itemBatchMst.setMrp(mrp);
			itemBatchMst.setProcessInstanceId(processInstanceId);
			itemBatchMst.setQty(qty);
			itemBatchMst.setTaskId(taskId);
			itemBatchMst.setCompanyMst(companyMst);
			itemBatchMst.setStore(store);

			itemBatchMst = itemBatchMstRepo.saveAndFlush(itemBatchMst);
		}
	}

	
	

	public JSONArray getBatchWiseQtyWithStore(String itemName, Double reqQty, String storeName) {

		Double totalQty = itemBatchDtlRepo.getTotalQty(itemName);
		if (null == totalQty) {
			return null;
		} else if (reqQty > totalQty) {
			return null;
		}

		List<Object> itemBatchList = itemBatchDtlRepo.findByItemIdOrderByExpDateWithStore(itemName,storeName);

		JSONArray array = new JSONArray();

		for (int i = 0; i < itemBatchList.size(); i++) {
			Object[] objAray = (Object[]) itemBatchList.get(i);

			double stkQty = ((Double) objAray[0]);
			
			BigDecimal bSQty = new BigDecimal(stkQty);
			bSQty = bSQty.setScale(6, BigDecimal.ROUND_HALF_DOWN);
			stkQty = bSQty.doubleValue();
			
			
			String batch = ((String) objAray[1]);
			
			java.sql.Date expDate = null;
			if (null != objAray[2]) {
				expDate = SystemSetting.UtilDateToSQLDate((Date) objAray[2]);

			} else {
				expDate = SystemSetting.StringToSqlDate("2050-01-01", "yyyy-MM-dd");
			}
			
			String store = ((String) objAray[4]);

			if (reqQty > 0) {
				if (reqQty > stkQty) {
					if (stkQty > 0) {

						JSONObject obj = new JSONObject();
						obj.put("batch", batch);
						obj.put("qty", stkQty);
						obj.put("exp", expDate);
						obj.put("store", store);

						array.put(obj);

					} else {

					}
				} else {

					JSONObject obj = new JSONObject();
					obj.put("batch", batch);
					obj.put("qty", stkQty);
					obj.put("exp", expDate);
					obj.put("store", store);

					array.put(obj);

					break;
				}
			}

		}
		return array;
	}


	@Override
	public List<PharmacyItemWiseSalesReport> findPharmacyItemWiseSalesReportByItemName(Date fromDate, Date toDate,
			String[] array) {
		
     List<PharmacyItemWiseSalesReport>  pharmacyItemWiseSalesReportList=new ArrayList<PharmacyItemWiseSalesReport>();
		
		for(String itemName:array) {
			 //List<PharmacyItemWiseSalesReport>  pharmacyItemWiseSalesReportList=new ArrayList<PharmacyItemWiseSalesReport>();
			
			ItemMst itemMst=itemMstRepository.findByItemName(itemName);
			
//			List<Object> obj = salesDetailsRepository.findPharmacyItemWiseSalesReportByItemName(fromDate, toDate,itemMst.getId());
			List<Object> obj =new ArrayList<Object>();
			if (dbType.contains("mysql")) {
			 obj = salesDetailsRepository.findPharmacyItemWiseSalesReportByItemName(fromDate, toDate,itemMst.getId());
			}
			else if (dbType.contains("derby")) {
			obj = salesDetailsRepository.findPharmacyItemWiseSalesReportByItemNameDerby(fromDate, toDate,itemMst.getId());
			}

			for (int i = 0; i < obj.size(); i++) {
				Object[] objAray = (Object[]) obj.get(i);
				PharmacyItemWiseSalesReport itemWiseSalesReport = new PharmacyItemWiseSalesReport();
                itemWiseSalesReport.setInvoiceDate(SystemSetting.UtilDateToString((Date) objAray[0], "yyyy-MM-dd"));
				itemWiseSalesReport.setItemName((String) objAray[1]);
				itemWiseSalesReport.setCustomerName((String) objAray[3]);
				itemWiseSalesReport.setQty((Double) objAray[2]);
				itemWiseSalesReport.setUnitPrice((Double) objAray[4]);
				itemWiseSalesReport.setBatch((String) objAray[5]);
				
                pharmacyItemWiseSalesReportList.add(itemWiseSalesReport);
                
			}
			
		}
		return pharmacyItemWiseSalesReportList;
	}

	@Override
	public List<CategoryWiseStockMovement> getStockMovementbyCategoryWise(String branchcode,CompanyMst companyMst, Date fdate, Date tdate,
			String[] array) {
		
		

		List<CategoryWiseStockMovement> categoryStockMvmntList = new ArrayList<CategoryWiseStockMovement>();
		
		for(String categoryName:array) {
			CategoryMst categoryMst=categoryMstRepository.findByCategoryNameAndCompanyMst(categoryName,companyMst);

		
			List<Object> obj = itemBatchDtlRepo.getCategoryWiseStockMovement(branchcode, companyMst, fdate, tdate,categoryMst.getId());
			
			
			for (int i = 0; i < obj.size(); i++) {
				Object[] objAray = (Object[]) obj.get(i);
				CategoryWiseStockMovement categoryWiseStockMovement = new CategoryWiseStockMovement();
				categoryWiseStockMovement.setCategory((String) objAray[2]);
				categoryWiseStockMovement.setInWardQty((Double) objAray[0]);
				categoryWiseStockMovement.setOutWardQty((Double) objAray[1]);
				categoryWiseStockMovement.setVoucherDate((Date) objAray[3]);
				List<Object> objOpeningStk = itemBatchDtlRepo.getCategoryWiseOPeningStock(branchcode, companyMst, fdate,categoryMst.getId());
				if (null == objOpeningStk) {
					for (int j = 0; j < objOpeningStk.size(); j++) {
						Object[] objctAray = (Object[]) objOpeningStk.get(j);
						if (categoryWiseStockMovement.getCategory().equalsIgnoreCase((String) objctAray[0])) {

							categoryWiseStockMovement.setOpeningStock((Double) objctAray[1]);
						}

						List<Object> objClosingStk = itemBatchDtlRepo.getCategoryWiseClosingStock(branchcode, companyMst,
								fdate, tdate,categoryMst.getId());
						for (int k = 0; k < objClosingStk.size(); k++) {
							Object[] objctArray = (Object[]) objClosingStk.get(k);
							if (categoryWiseStockMovement.getCategory().equalsIgnoreCase((String) objctArray[0])) {
								if (null == categoryWiseStockMovement.getOpeningStock()) {
									categoryWiseStockMovement.setClosingStock((Double) objctArray[1] + 0.0);
								} else {
									categoryWiseStockMovement.setClosingStock(
											(Double) objctArray[1] + categoryWiseStockMovement.getOpeningStock());
								}
							}
						}
					}
				} else {

					List<Object> objClosingStk = itemBatchDtlRepo.getCategoryWiseClosingStock(branchcode, companyMst, fdate,
							tdate,categoryMst.getId());
					for (int k = 0; k < objClosingStk.size(); k++) {
						Object[] objctArray = (Object[]) objClosingStk.get(k);
						if (categoryWiseStockMovement.getCategory().equalsIgnoreCase((String) objctArray[0])) {

							categoryWiseStockMovement.setClosingStock((Double) objctArray[1] + 0.0);

						}
					}
				}
				categoryStockMvmntList.add(categoryWiseStockMovement);
			}
			//return categoryStockMvmntList;
		
	}
		return categoryStockMvmntList;
	}

	@Override
	public Double findItemStockDetailsByStore(String itemId, String batch, String companymstid, String storeFrom) {
		
		Double itemStockDetailsByStore=itemBatchDtlRepo.getItemStockDetailsByStore(itemId,batch,companymstid,storeFrom);
		return itemStockDetailsByStore;
	}

	@Override
	public String SalesToBatchDtl(String companymstid, String branchcode,Date date) {
		
		
		List<Object> salesToBatchListObj = salesDetailsRepository.SalesToBatchDtl(companymstid,branchcode,date);
		
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		
		for(int i=0; i < salesToBatchListObj.size(); i++)
		{
			Object[] objArray = (Object[]) salesToBatchListObj.get(i);
			
			Date voucherDate = (Date) objArray[0];
			String voucherNumber = (String) objArray[1];
			String hdrId = (String) objArray[2];
			String dtlId = (String) objArray[3];
			String itemId = (String) objArray[4];
			String barcode = (String) objArray[5];
			String batch = (String) objArray[6];
			Double mrp = (Double) objArray[7];
			Double qty = (Double) objArray[8];
			String customerName = (String) objArray[9];


			
			
			 String particular = "SALES " + customerName;
				Double QtyIn = 0.0;
				final VoucherNumber voucherNo = voucherService.generateInvoice("STV", companyMst.getId());
				
				itemBatchDtlInsertionNew(barcode,batch,branchcode,
						null,itemId,mrp,particular,null,
						0.0,qty,dtlId,hdrId,voucherDate,voucherNumber,"MAIN",null,
						voucherDate,voucherNo.getCode(),companyMst);
		}
		
		
		return "success";

		
	}

	@Override
	public List<PharmacyItemWiseSalesReport> findItemWiseSalesReportByItemName(Date fromDate, Date toDate, String branchcode) {
			
	     List<PharmacyItemWiseSalesReport>  pharmacyItemWiseSalesReportList=new ArrayList<PharmacyItemWiseSalesReport>();
			
			
				
				
				
				List<Object> obj = salesDetailsRepository.findItemWiseSalesReportByItemNameWithBranchcode(fromDate, toDate,branchcode);
				for (int i = 0; i < obj.size(); i++) {
					Object[] objAray = (Object[]) obj.get(i);
					PharmacyItemWiseSalesReport itemWiseSalesReport = new PharmacyItemWiseSalesReport();
	               
					itemWiseSalesReport.setInvoiceDate(SystemSetting.UtilDateToString((Date) objAray[0], "yyyy-MM-dd"));
					itemWiseSalesReport.setItemName((String) objAray[1]);
					itemWiseSalesReport.setBranchCode((String) objAray[2]);
					itemWiseSalesReport.setQty((Double) objAray[3]);
					itemWiseSalesReport.setCustomerName((String) objAray[4]);
					itemWiseSalesReport.setUnitPrice((Double) objAray[5]);
					itemWiseSalesReport.setBatch((String) objAray[6]);
					
	                pharmacyItemWiseSalesReportList.add(itemWiseSalesReport);
	                
				}
				
			
			return pharmacyItemWiseSalesReportList;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
}

	

	


