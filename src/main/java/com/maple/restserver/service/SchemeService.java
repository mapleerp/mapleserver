package com.maple.restserver.service;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.SalesTransHdr;

@Service
public interface SchemeService {

	public boolean CheckOfferWhileFinalSave( SalesTransHdr salestranshdr, Date logDate);
}
