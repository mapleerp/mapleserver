package com.maple.restserver.service;

import org.springframework.stereotype.Service;


@Service
public interface MultiUnitConversionService {
	
	public double getConvertionQty(String companyMstId, String itemId, String
			  sourceUnit, String targetUnit, double sourceQty) ;
	

}
