package com.maple.restserver.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

//import org.jvnet.hk2.annotations.Service;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.report.entity.AccountHeadsReport;

@Service
@Transactional
public interface AccountHeadsService {

	public  List<AccountHeads> getAccountHeads(String pdfFileName);
	public  List<AccountHeads>	findByBankAccount(String  companymstid);
	
	/*
	 * This will initialize account heads
	 */
	public   String 	addAccountHeadInitialize(String  companymstid, String branchcode);
	
	
	
	public   String	addAccountHeadSalesReturn(String  companymstid);
	public String  addAccountHeadAsset(String companymstid);
	public String  addAccountHeadLiability(String companymstid);
	
	public String addAccountHeadIncome(String companymstid);
	
	public String  addAccountHeadExpance(String companymstid);
	
	public String addAccountHeadCurrentAssets(String  companymstid);
	
	public String  addAccountHeadCash( String  companymstid);
	
	public String addAccountHeadBankAccount(String companymstid);
	public String addAccountHeadSodexo(String companymstid);
	public String addAccountHeadSalesAccount(String companymstid);
	public String addAccountHeadPurchaseAccount(String companymstid);
	public String addAccountHeadDutiesandtaxes(String companymstid);
	public String addAccountHeadCgstAccount(String companymstid);
	
	public String addAccountHeadsgstAccount(String companymstid);
	public String addAccountHeadCessAccount( String companymstid);
	
	
	public String addAccountHeadKeralaFloodCess(String companymstid);

	
	public String addAccountHeadPettyCash(String companymstid,String branchCode);
	
	public String addAccountHeadroundoff(String companymstid,String branchcode);
	
	
	public String addProcessPermissionReports(String companymstid);
	
	public String addProcessPermissionKotPos(String companymstid);
	public String  unitCreation(String companymstid);
	
	public String addProcessDamageEntry(String companymstid);
	public String addProcessWholeSale(String companymstid);
	public String addProcessPosWindow(String companymstid);
	
	
	public String addProcessOnlineSales(String companymstid);
	
	
    public String addProcessReorder(String companymstid);
    
    
    public String addProcessSaleOrder(String companymstid);
    
    
    public String addProcessItemMaster(String companymstid);
    
    public String addProcessAccountHeads(String companymstid);
    
    public String addProcessProductConversion(String companymstid);
    
    public String addProcessReceiptWindow(String companymstid);
    
    
    public String addProcessDayEndClossure(String companymstid);
    
    
    public String addProcessPurchase(String companymstid);
    
    
    public String addProcessCustomer(String companymstid);
    
    public String addProcessAcceptStock(String companymstid);
    
    public String addProcessAddSupplier(String companymstid);
    
    
    public String addProcessPayment(String companymstid);
    
    public String addProcessTaskList(String companymstid);
    
    
    public String addProcessMultyUnit(String companymstid);
    
    public String addProcessStockTransfer(String companymstid);
    
    public String addProcessIntentent(String companymstid);
    public String   addProcessProductionPlanning(String companymstid);
    
    
    public String addProcessJournal(String companymstid);
    
    public String addProcessSessionEnd(String companymstid);
    public String addProcessPhysicalStock(String companymstid);
    
    public String addProcessTaxCreation(String companymstid);
    
    public String addProcessKitDefinition(String companymstid);
    
    public String  addProcessPriceDefinition(String companymstid);
    
    public String addProcessKotManger(String companymstid);
    
    public String addProcessUserRegistration(String companymstid);
    
    public String addProcessProcessCreation(String companymstid);
    
    public String addProcessChangePasswordf(String companymstid);
    
    public String addProcessBranchCreation(String ompanymstid);
    
    public String addProcessCompanyCreation(String companymstid);
    public String   addProcessVoucherReprint(String companymstid);
    
    public String addProcessScheme(String companymstid);
    
    public String addProcessSchemeDetails(String companymstid);
    
    public String addProcessPermission(String companymstid);
    
    
   public String  addProcessPermissionGroupPermission(String companymstid);
   
   public String addProcessPermissionGroupCreation(String companymstid);
   
   public String addProcessPermissionCustomer(String companymstid);
	
   public String addProcessPermissionSalesReport(String companymstid);
   
   public String  addProcessPermissionMopnthalySummaryReport(String companymstid);
   
   
   public String addProcessPermissionSaleOrderReport(String companymstid);
   
   
   public String addProcessPermissionAccountBalance(String companymstid);
   
   
   public String addProcessPermissionMonthlyStockReport(String companymstid);
   
   public String addProcessPermissionsSalesReport(String companymstid);
   
   public String addProcessPermissionsAccounts(String companymstid);
   
   
   public String addProcessPermissionsSupplierPayment(String companymstid);
   
   public String addProcessPermissionsOwnAccountSettlement(String companymstid);
   
   public String addProcessPermissionsActualProduction(String companymstid);
   
   public String addProcessPermissionsSalesReturn(String companymstid);
   
   public String addProcessPermissionSalesFromOrder(String companymstid);
   
  public String  addProcessPermissionSaleorderEdit(String companymstid);
  
  public String  addProcessPermissionConfigurations(String companymstid);
  
  public String addProcessPermissionReceiptMode(String companymstid);
  
  public String addProcessPermissionVouchertype(String companymstid);
  
  public String addProcessPermissions(String companymstid);
  
public List<AccountHeads> findByAssetAccount(String companymstid);


String addAccountHeadBranchCash(String companymstid, String branchCode);

/*
 * new service for managing accountheads with supplier and customer
 */
public void accountHeadsManagement(String objectId,String objectName,String objectType,String companyMst);


public List<AccountHeads> searchLikeAccountName(String string, String companymstid, Pageable topFifty);
public List<AccountHeads> searchLikeSupplierByNamefromAccountHeads(String string, CompanyMst companyMst);


/*
 * fetch account by account name. If it is not present, then create a new account and sent back.
 */
public AccountHeads getAccountByAccountNameWithStatus(String selectedItem, Boolean accountCreateStatus,
		CompanyMst companyMst);


}
