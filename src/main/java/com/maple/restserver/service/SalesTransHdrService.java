package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesBillModelClass;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SalesTransHdrSummary;
import com.maple.restserver.report.entity.KOTItems;
import com.maple.restserver.report.entity.PharmacyDayEndReport;
import com.maple.restserver.report.entity.SalesInWeb;
import com.maple.restserver.report.entity.SalesModeWiseBillReport;
import com.maple.restserver.report.entity.SalesTaxSplitReport;
import com.maple.restserver.report.entity.TableWaiterReport;


@Service
@Transactional
public interface SalesTransHdrService {
	
	//----------------versio 4.11
	List<SalesTaxSplitReport> SaleTaxSplit(Date edate, CompanyMst companyMst, String branchCode);
	//----------------versio 4.11 end
	
	public List<SalesTransHdr> getSalesTransHdr(String company_mst_id);
	
	
	public List<Object> getVoucherNumber(String company_mst_id , Date vDate ,String vType );
	
	List<SalesTransHdr> findByVoucherNumberAndVoucherDate(String company_mst_id , String vouchernumber,Date vdate);

	List<SalesTransHdr> printKotByTablenameAndSalesman(String salesman,String tablename);

	List<SalesTransHdr> printKotBySalesTransHdr(String salesTransHdrId);
	
	
	List<SalesTransHdr> printKotInvoiceByTablenameAndSalesman(String salesman,String tablename);
	List<TableWaiterReport> 	retrivePalcedTable(String companymstid,String branchcode,String customiseSalesMode);


	public List<SalesModeWiseBillReport> findNumericVoucherNoBySalesModeAndDate(Date date,String companymstid);


//	public List<SalesDtl> GenerateSalesInvoice(String companymstid, String customer, String salesman);

	public List<SalesModeWiseBillReport> findBillseriesByVoucherDate(Date date,String companymstid);


	String saveSalesSummary(Date date, CompanyMst companyMst, String branchcode);

	List<SalesTransHdr> printKotBySalesmanAndKotNumber(String salesman, String tablename, String kotnumber, Date date);

	List<SalesTransHdr> printRunningKot(String salesman, String tablename, String kotnumber);

	


	
	//------------Print Invoice -----Call API to print KOT. passing Table Name and KOT number -------

	List<SalesTransHdr> printKotInvoiceByTableNameAndKotNumber(String kotnumber, String tablename,Date vdate);
	
	//------------Print  performa Invoice -----Call API to print KOT. passing Table Name and KOT number -------

	List<SalesTransHdr> printPerformaInvoiceByTableNameAndKotNumber(String kotnumber, String tablename, Date vdate);

	SalesTransHdr saveSalesTransHdrFromSalesOrderId(String orderid);

	

	List<PharmacyDayEndReport> getPharmacyDayEndreport(Date fudate, String branchCash, CompanyMst companyMst);

	Double getPharmacyCashSales(Date fudate, String branchCash, CompanyMst companyMst);

	Double getPharmacyCashReceived(Date fudate, String branchCash, CompanyMst companyMst);

	Double getPharmacyCashPaid(Date fudate, String branchCash, CompanyMst companyMst);

	Double getPharmacyCashBalance(Date fudate, String branchCash, CompanyMst companyMst);

	Double getPharmacyClosingCashBalance(Date fudate, String branchCash, CompanyMst companyMst);

	String getAccountIdByAccountName(String branchCash);
	
	String updatePostedToServerStatus(List<String> hdrIds);

	//=============MAP-131-api-for-fetch-kot-details===============anandu==================
	List<KOTItems> getKotDetail();
	
	
//===========MAP-203-api-for-savesalesinweb===================anandu============
	SalesInWeb saveSalesInWeb(@Valid SalesInWeb salesInWeb, String companymstid);

	SalesBillModelClass setValuesOnSalesBillModalClass(SalesTransHdr salesTransHdr);
	
	
	//============SalesTransHdr CReation For Online sales...//
	
	SalesTransHdr createSalesTransHdr(CompanyMst companyMst, String myBranch,
			String useId, String salestype, String customerid, String salesmode,
			String localCustomerId,String salesMan);

	

}
