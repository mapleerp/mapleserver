package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.ActualProductionReport;
import com.maple.restserver.report.entity.FinishedProduct;
import com.maple.restserver.repository.ActualProductionHdrRepository;


@Service
@Transactional
@Component
public class ActualProductionServiceImpl implements ActualProductionService {
	@Autowired
	ActualProductionHdrRepository actualProductionHdrRepository; 
		
	@Override
	public List<ActualProductionReport> actualProductionReport(String companymstid, 
			String branchcode, Date fdate, Date tdate) {
		List<ActualProductionReport> actualproductionReportList= new ArrayList();
		
		 List<Object> obj =   actualProductionHdrRepository.actualProductionReport(companymstid,branchcode,fdate,tdate);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ActualProductionReport actualproductionReport =new ActualProductionReport();
			 
				
		     
			 actualproductionReport.setActualQty((Double) objAray[0]);
			 actualproductionReport.setUnitName((String) objAray[1]);
			 actualproductionReport.setVoucherDate((Date) objAray[2]);
			 actualproductionReport.setItemName((String) objAray[3]);
			 actualproductionReport.setId((String) objAray[4]);
			 actualproductionReport.setBatch((String) objAray[5]);
			 actualproductionReportList.add(actualproductionReport);
			 

	}

		 return actualproductionReportList;
	
	}
	@Override
	public List<ActualProductionReport> actualProductionPrintReport(String companymstid, 
			String branchcode, Date fdate, Date tdate) {
		List<ActualProductionReport> actualproductionReportList= new ArrayList();
		
		 List<Object> obj =   actualProductionHdrRepository.actualProductionPrintReport(companymstid,branchcode,fdate,tdate);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ActualProductionReport actualproductionReport =new ActualProductionReport();
			 
				
		     
			 actualproductionReport.setActualQty((Double) objAray[0]);
			 actualproductionReport.setUnitName((String) objAray[1]);
			 actualproductionReport.setVoucherDate((Date) objAray[2]);
			 actualproductionReport.setItemName((String) objAray[3]);
			 actualproductionReportList.add(actualproductionReport);
			 

	}

		 return actualproductionReportList;
	
	}
	@Override
	public List<FinishedProduct> actualproductionbetweendate(String companymstid, String branchcode, Date fdate,
			Date tdate) {
		List<FinishedProduct> fineshedProductList= new ArrayList();
		
		 List<Object> obj =   actualProductionHdrRepository.actualproductionbetweendate(companymstid,branchcode,fdate,tdate);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
	  FinishedProduct fineshedProductReport =new FinishedProduct();
	   fineshedProductReport.setActualQty((Double) objAray[0]);
	   fineshedProductReport.setPlaningQty((Double) objAray[1]);
	   fineshedProductReport.setCostPrice((Double) objAray[2]);
	   fineshedProductReport.setProduct((String) objAray[3]);
	   fineshedProductReport.setBatch((String) objAray[4]);
	   fineshedProductReport.setValue((Double) objAray[0]*(Double) objAray[2]);
	   fineshedProductReport.setVoucherDate((Date) objAray[5]);
	   fineshedProductList.add(fineshedProductReport);
			 

	}

		 return fineshedProductList;
	}

}
