package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.maple.restserver.report.entity.PharmacyLocalPurchaseDetailsReport;
import com.maple.restserver.repository.PurchaseHdrRepository;

@Service
@Transactional
public class PharmacyLocalPurchaseDetailsReportServiceImpl implements PharmacyLocalPurchaseDetailsReportService {

	
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;
	
	@Override
	public List<PharmacyLocalPurchaseDetailsReport> getPharmacyLocalPurchaseDetailsReport(Date fromDate, Date toDate) {
		
		List<PharmacyLocalPurchaseDetailsReport>PharmacyLocalPurchaseDetailsReportList = new ArrayList<PharmacyLocalPurchaseDetailsReport>();
		List<Object> obj = purchaseHdrRepository.getPharmacyLocalPurchaseDetailsReport(fromDate,toDate);
		 for(int i=0;i<obj.size();i++)
		 {
			 
			Object[] objAray = (Object[]) obj.get(i);
			PharmacyLocalPurchaseDetailsReport pharmacyLocalPurchaseDetailsReport = new PharmacyLocalPurchaseDetailsReport();
			
		
			
		    pharmacyLocalPurchaseDetailsReport.setAmount((Double)objAray[17]);
			pharmacyLocalPurchaseDetailsReport.setBatch((String)objAray[14]);
			pharmacyLocalPurchaseDetailsReport.setDescription((String)objAray[11]);
			pharmacyLocalPurchaseDetailsReport.setExpiryDate((Date)objAray[15]);
			//pharmacyLocalPurchaseDetailsReport.setGroup((String)objAray[13]);
			pharmacyLocalPurchaseDetailsReport.setGroup((String)objAray[5]);
			pharmacyLocalPurchaseDetailsReport.setGst((Double)objAray[9]);
			pharmacyLocalPurchaseDetailsReport.setInvoiceDate((Date)objAray[7]);
			pharmacyLocalPurchaseDetailsReport.setItemCode((String)objAray[12]);
			pharmacyLocalPurchaseDetailsReport.setNetCost((Double)objAray[3]);
			pharmacyLocalPurchaseDetailsReport.setPoNumber((String) objAray[8]);
			pharmacyLocalPurchaseDetailsReport.setPurchaseRate((Double)objAray[9]);
			pharmacyLocalPurchaseDetailsReport.setQty((Double)objAray[16]);
			pharmacyLocalPurchaseDetailsReport.setSupplier((String) objAray[4]);
			pharmacyLocalPurchaseDetailsReport.setSupplierInvoice((String) objAray[6]);
			pharmacyLocalPurchaseDetailsReport.setTransEntryDate((Date)objAray[2]);
			pharmacyLocalPurchaseDetailsReport.setUser((String) objAray[18]);
			pharmacyLocalPurchaseDetailsReport.setVoucherNumber((String) objAray[1]);
			
			
			PharmacyLocalPurchaseDetailsReportList.add(pharmacyLocalPurchaseDetailsReport);
		 }
		 return PharmacyLocalPurchaseDetailsReportList;
	}
}
