package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.report.entity.DailyPaymentSummaryReport;
import com.maple.restserver.report.entity.OrgPaymentVoucher;
import com.maple.restserver.report.entity.PaymentReports;
import com.maple.restserver.report.entity.PaymentVoucher;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.utils.SystemSetting;

 
@Service
@Transactional
@Component
public class PaymentReportServiceImpl implements PaymentReportService {
	@Autowired
	PaymentHdrRepository paymentHdrRepo;
	
	@Autowired
	PaymentDtlRepository paymentDtlRepository;
	
	@Override
	public List<Object> getDailyPayment(String voucherNumber, Date voucherDate) 
	{
	
		 List<Object> obj =  paymentHdrRepo.getDailyPayment(voucherNumber, voucherDate);
			
			return obj;
	}


	@Override
	public List<PaymentVoucher> getPayments() {
		 
		 List<PaymentVoucher> paymentReportList =   new ArrayList();
		 
		 
		 List<Object> obj =   paymentHdrRepo.getAllDailyPayments();
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 PaymentVoucher paymentVoucher = new PaymentVoucher();
		 	 paymentVoucher.setBranchName((String)objAray[0]);
			 paymentVoucher.setVoucherNO((String)objAray[1]);
			 paymentVoucher.setVoucherDate((Date)objAray[2]);
		    paymentVoucher.setAccountName((String)objAray[3]);
		    paymentVoucher.setAmount(( Double)objAray[4]);
			 paymentVoucher.setRemark((String)objAray[5]);
			

			 paymentReportList.add(paymentVoucher);	
		 }
		return paymentReportList;
	}


	@Override
	public List<PaymentVoucher> getPaymentReport(String vouchernumber,Date date,String branchCode) {
			
		List<PaymentVoucher> paymentRepoList= new ArrayList();
		
		 List<Object> obj =   paymentHdrRepo.getPaymentReport(vouchernumber,date,branchCode);
		 
		 for(int i=0;i<obj.size();i++)
		 {

			
			 Object[] objAray = (Object[]) obj.get(i);
			 PaymentVoucher paymentReport =new PaymentVoucher();
			
			  paymentReport.setBranchName((String)objAray[0]);
			  paymentReport.setBranchEmail((String)objAray[1]);
			  paymentReport.setBranchWebsite((String)objAray[2]);
			  paymentReport.setVoucherNO((String)objAray[3]);
			  paymentReport.setVoucherDate((Date)objAray[4]);
			  paymentReport.setRemark((String)objAray[5]);
			  paymentReport.setMemberId((String)objAray[6]);
			  paymentReport.setAmount((Double)objAray[7]);
			  paymentReport.setInstrumentNumber((String)objAray[8]);
			  paymentReport.setInstrumentDate((Date)objAray[9]);
			  paymentReport.setModeOfPayment((String)objAray[10]);
			  paymentReport.setAccountName((String)objAray[11]);
			  
			  paymentReport.setCompanyName((String)objAray[12]);
			  paymentReport.setTotalAmount((Double)objAray[13]);
			
			  paymentRepoList.add(paymentReport);
			  System.out.print("inside payment service issssssssssssssssssssssssssssssssssssssssssssssssss");
			 System.out.println("hgadjhgadjgajd"+paymentReport);
	}

		 return paymentRepoList;
	}


	@Override
	public List<PaymentDtl> findByAccountIdAndInstrumentDate(String accountid, Date reportdate) {
		
		

		List<PaymentDtl> paymentList= new ArrayList();
		
		 List<Object> obj =   paymentDtlRepository.findByAccountIdAndInstrumentDate(accountid,reportdate);
		 for(int i=0;i<obj.size();i++)
		 {
			 
			 Object[] objAray = (Object[]) obj.get(i);
			 PaymentDtl paymentdtl =new PaymentDtl();
			 paymentdtl.setAmount((Double) objAray[0]);
			 paymentdtl.setRemark((String) objAray[1]);
				
			 paymentList.add(paymentdtl);
	}

		 return paymentList;
	}
	
	
	@Override
	public List<OrgPaymentVoucher> getOrgPaymentReport(String vouchernumber,Date date,String branchCode,String memberid) {
			
		List<OrgPaymentVoucher> orgpaymentRepoList= new ArrayList();
		
		 List<Object> obj =   paymentHdrRepo.getOrgPaymentReport(vouchernumber,date,branchCode,memberid);
		 
		 for(int i=0;i<obj.size();i++)
		 {
			 
			 Object[] objAray = (Object[]) obj.get(i);
			 OrgPaymentVoucher paymentReport =new OrgPaymentVoucher();
			
			  paymentReport.setBranchName((String)objAray[0]);
			  paymentReport.setBranchEmail((String)objAray[1]);
			  paymentReport.setBranchWebsite((String)objAray[2]);
			  paymentReport.setVoucherNO((String)objAray[3]);
			  paymentReport.setVoucherDate((Date)objAray[4]);
			  paymentReport.setRemark((String)objAray[5]);
			  paymentReport.setMemberId((String)objAray[6]);
			  paymentReport.setAmount((Double)objAray[7]);
			  paymentReport.setInstrumentNumber((String)objAray[8]);
			  paymentReport.setInstrumentDate((Date)objAray[9]);
			  paymentReport.setModeOfPayment((String)objAray[10]);
			  paymentReport.setFamilyName((String)objAray[11]);
			  paymentReport.setMemberName((String)objAray[12]);
			  paymentReport.setAccountName((String)objAray[13]);
			  
			  paymentReport.setCompanyName((String)objAray[14]);
			  paymentReport.setTotalAmount((Double)objAray[7]);
			
			  orgpaymentRepoList.add(paymentReport);
			 System.out.println("hgadjhgadjgajd"+paymentReport);
	}

		 return orgpaymentRepoList;
	}


	public List<PaymentVoucher> getPaymentReportBetweenDate(Date reportFromdate, Date reportTodate,
			String branchCode,String companymstid) {
		
		
		List<PaymentVoucher> paymentRepoList= new ArrayList();
		
		 List<Object> obj =   paymentHdrRepo.getPaymentReportBetweenDate(reportFromdate,reportTodate,branchCode,companymstid);
		 
		 for(int i=0;i<obj.size();i++)
		 {
			 
			 Object[] objAray = (Object[]) obj.get(i);
			 PaymentVoucher paymentReport =new PaymentVoucher();
			 paymentReport.setVoucherNO((String)objAray[0]);
			 paymentReport.setVoucherDate((Date)objAray[1]);
			 paymentReport.setAmount((Double)objAray[2]);
			 paymentReport.setAccountName((String)objAray[3]);
			 paymentReport.setId((String)objAray[4]);
			 paymentRepoList.add(paymentReport);
		 }
		 return paymentRepoList;
	
	}


	public List<PaymentVoucher> getPaymentReportBetweenDateByAccount(String accountid, Date fdate, Date tdate,
			String branchCode, String companymstid) {
		List<PaymentVoucher> paymentRepoList= new ArrayList();
		
		 List<Object> obj =   paymentHdrRepo.getPaymentReportBetweenDateByAccount(accountid,fdate,tdate,branchCode,companymstid);
		 
		 for(int i=0;i<obj.size();i++)
		 {
			 
			 Object[] objAray = (Object[]) obj.get(i);
			 PaymentVoucher paymentReport =new PaymentVoucher();
			 paymentReport.setVoucherNO((String)objAray[0]);
			 paymentReport.setVoucherDate((Date)objAray[1]);
			 paymentReport.setAmount((Double)objAray[2]);
			 paymentReport.setAccountName((String)objAray[3]);
			 paymentReport.setId((String)objAray[4]);
			 paymentRepoList.add(paymentReport);
		
		 }
		 return paymentRepoList;
	}


	
	
	@Override
	public List<DailyPaymentSummaryReport> getDailyPaymentSummaryReport(
			String companymstid, Date date) {

		List<DailyPaymentSummaryReport> paymentSummuryList = new ArrayList<>();
		List<String> paymentModeList = paymentHdrRepo.findAllPaymentMode(companymstid);
		
		for(String paymentMode : paymentModeList)
		{
			
			List<Object> objList = paymentHdrRepo.findSumOfPaymentBySalesModeAndDate(paymentMode,date,companymstid);
			
			for(int i=0;i<objList.size();i++)
			{
				Object[] objAray = (Object[]) objList.get(i);
				DailyPaymentSummaryReport dailyPaymentSummaryReport = new DailyPaymentSummaryReport();
				dailyPaymentSummaryReport.setCreditAccount((String) objAray[1]);
				dailyPaymentSummaryReport.setPaymentMode(paymentMode);
				dailyPaymentSummaryReport.setTotalCash((Double) objAray[0]);
				
				paymentSummuryList.add(dailyPaymentSummaryReport);
			}
		}
		
		return paymentSummuryList;
	}

//version 3.1 
	@Override
	public List<PaymentVoucher> getPaymentDtlReport(Date sdate, Date edate, CompanyMst companyMst, String branchCode) {
		
		
		List<PaymentVoucher> paymentRepoList= new ArrayList();
		
		 List<Object> obj =   paymentHdrRepo.getPaymentDtlReport(sdate,edate,companyMst,branchCode);
		 
		 for(int i=0;i<obj.size();i++)
		 {

			 Object[] objAray = (Object[]) obj.get(i);
			 PaymentVoucher paymentReport =new PaymentVoucher();
			 paymentReport.setVoucherNO((String)objAray[0]);
			 paymentReport.setAmount((Double)objAray[1]);
			 paymentReport.setInstrumentDate((Date)objAray[2]);
			 paymentReport.setInstrumentNumber((String)objAray[3]);
			 paymentReport.setModeOfPayment((String)objAray[4]);
			 paymentReport.setAccountName((String)objAray[5]);
			 paymentReport.setVoucherDate((Date)objAray[6]);
			 paymentRepoList.add(paymentReport);
		
		 }
		 return paymentRepoList;

	}

//version 3.1
	//---------------version 4.14

		@Override
		public DailyPaymentSummaryReport getDailyPaymentSummaryReportByRecieptMode(String companymstid, Date date,
				String paymentMode) {
			Double obj = paymentHdrRepo.findSumOfPaymentBySalesModeAndDateByPaymentMode(paymentMode,date,companymstid);
			
				DailyPaymentSummaryReport dailyPaymentSummaryReport = new DailyPaymentSummaryReport();
				dailyPaymentSummaryReport.setTotalCash(obj);
				
				return dailyPaymentSummaryReport;
		}


		@Override
		public List<PaymentHdr> fetchPaymnetReportHdr(CompanyMst companyMst, String branchcode, Date fromDate,
				Date toDate) {
			 List<PaymentHdr> paymentHdrList=new ArrayList<PaymentHdr>();
			List<Object> objList=paymentHdrRepo.getPaymetHdr(companyMst,branchcode,fromDate,toDate);
			
			for(int i=0;i<objList.size();i++) {
				Object[] objAray = (Object[]) objList.get(i);
				PaymentHdr paymentHdr=new PaymentHdr();	
				paymentHdr.setId((String) objAray[0]);
				paymentHdr.setVoucherDate((Date) objAray[1]);
				paymentHdr.setVoucherNumber((String) objAray[2]);
				paymentHdr.setVoucherType((String) objAray[3]);
				paymentHdrList.add(paymentHdr);
				
			}
			
			return paymentHdrList;
		}


		@Override
		public List<PaymentReports> exportPaymentReport(CompanyMst companyMst, Date fromDate, Date toDate,
				String branchcode) {
		

			String voucherNumber;
			String voucherDate;;
			String voucherType;
			String creditAccountId;
			String accountId;
			String instrumentNumber;
			String bankAccountNumber;
			String modeOfPayment;
			Double amount;
			
			List<PaymentReports> paymentReportList=new ArrayList<PaymentReports>();
			
			 
			

			
			List<Object> obj=paymentHdrRepo.exportPaymentReport( companyMst,  fromDate,  toDate,
					 branchcode);
			 for(int i=0;i<obj.size();i++)
			 {

				 PaymentReports paymentReports =new PaymentReports();
				
				 
				 Object[] objAray = (Object[]) obj.get(i);
				 paymentReports.setVoucherNumber((String)objAray[0]);
				 String vDate=SystemSetting.UtilDateToString((Date)objAray[1]);
				 paymentReports.setVoucherDate(vDate);
				 paymentReports.setVoucherType((String)objAray[2]);
				 paymentReports.setCreditAccountId((String)objAray[3]);
				 paymentReports.setAccountId((String)objAray[4]);
				 paymentReports.setInstrumentNumber((String)objAray[5]);
				 paymentReports.setBankAccountNumber((String)objAray[6]);
				 paymentReports.setModeOfPayment((String)objAray[7]);
				 paymentReports.setAmount((Double)objAray[8]);
				 paymentReportList.add(paymentReports);
			 }
		
			return paymentReportList;
		}


		@Override
		public List<PaymentReports> paymentReport(CompanyMst companyMst, Date fromDate, String branchcode) {
		   
			
			List<PaymentReports> paymentReportList=new ArrayList<PaymentReports>();
			
			 
			

			
			List<Object> obj=paymentHdrRepo.paymentReport( companyMst,  fromDate, 
					 branchcode);
			 for(int i=0;i<obj.size();i++)
			 {

				 PaymentReports paymentReports =new PaymentReports();
				
				 
				 Object[] objAray = (Object[]) obj.get(i);
				 
				 paymentReports.setAccountId((String)objAray[0]);
				 paymentReports.setAmount((Double)objAray[1]);
				 paymentReportList.add(paymentReports);
			 }
		
			return paymentReportList;
			
			
			
			
		}


		@Override
		public List<PaymentReports> pettCashpaymentReport(CompanyMst companyMst, Date fromDate, String branchcode) {
		
			String modeofpayment = branchcode+"-PETTY CASH";
			List<PaymentReports> paymentReportList=new ArrayList<PaymentReports>();
			List<Object> obj=paymentHdrRepo.pettycashPayment( companyMst,  fromDate, 
					 branchcode,modeofpayment);
			 for(int i=0;i<obj.size();i++)
			 {

				 PaymentReports paymentReports =new PaymentReports();
				
				 
				 Object[] objAray = (Object[]) obj.get(i);
				 
				 paymentReports.setAccountId((String)objAray[0]);
				 paymentReports.setAmount((Double)objAray[1]);
				 paymentReportList.add(paymentReports);
			 }
		
			return paymentReportList;
			
		}


		@Override
		public List<PaymentDtl> fetchBypaymenthdrId(String paymenthdrId) {
			
			List<PaymentDtl> paymentDtlReportList=new ArrayList<PaymentDtl>();
			List<Object> obj=paymentDtlRepository.fetchBypaymenthdrId( paymenthdrId);
			
			 for(int i=0;i<obj.size();i++)
			 {

				 PaymentDtl paymentDtl =new PaymentDtl();
				
				 
				 Object[] objAray = (Object[]) obj.get(i);
				 
				 paymentDtl.setAccountId((String)objAray[0]);
				 paymentDtl.setRemark((String)objAray[1]);
				 paymentDtl.setAmount((Double)objAray[2]);
				 paymentDtl.setInstrumentNumber((String)objAray[3]);
				 paymentDtl.setBankAccountNumber((String)objAray[4]);
				 paymentDtl.setModeOfPayment((String)objAray[5]);
			
				 paymentDtlReportList.add(paymentDtl);
			 }
		
			return paymentDtlReportList;
		
		}


		@Override
		public List<PaymentReports> paymentReportReportPrinting(CompanyMst companyMst, Date fromDate, Date toDate,
				String branchcode) {
		
			
			List<PaymentReports> paymentReportList=new ArrayList<PaymentReports>();
			
			List<Object> obj=paymentHdrRepo.paymentReportReportPrinting( companyMst,  fromDate,  toDate,
					 branchcode);
			 for(int i=0;i<obj.size();i++)
				 
			 {
				 PaymentReports paymentReports =new PaymentReports();
				 Object[] objAray = (Object[]) obj.get(i);
				 paymentReports.setVoucherNumber((String)objAray[0]);
				 String vDate=SystemSetting.UtilDateToString((Date)objAray[1]);
				 paymentReports.setVoucherDate(vDate);
				 paymentReports.setVoucherType((String)objAray[2]);
				 paymentReports.setCreditAccountId((String)objAray[3]);
				 paymentReports.setAccountId((String)objAray[4]);
				 paymentReports.setInstrumentNumber((String)objAray[5]);
				 paymentReports.setBankAccountNumber((String)objAray[6]);
				 paymentReports.setModeOfPayment((String)objAray[7]);
				 paymentReports.setAmount((Double)objAray[8]);
				 paymentReportList.add(paymentReports);
			 }
		
			return paymentReportList;

		}

		//---------------version 4.14 end

}
	
	
	
	

