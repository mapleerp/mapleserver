package com.maple.restserver.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.ItemBatchDtlDailyRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
@Service
@Transactional
@Component
public class ItemBatchMstServiceImpl implements ItemBatchMstService {

	
	
	@Autowired
	ItemMstRepository itemMstRepo;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepo;
	@Autowired
	KitDefinitionMstRepository kitDefinitiomMstRepo;

	@Autowired
	ItemBatchDtlDailyRepository itemBatchDtlDailyRepository;

	@Autowired
	BranchMstRepository branchMstRepository;

	@Autowired
	ItemBatchMstRepository itemBatchMstRepository;

	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepo;

	@Autowired
	SalesDetailsRepository salesDetailsRepo;

	
	
	
	
	
	
	
	
	
	
	@Override
	public String stockVerification(String companymstid, String salestranshdrid) {

		String stockList = MapleConstants.STOCKOK;
//		String msg ="";
		SalesTransHdr salesTransHdr = null;
		Optional<SalesTransHdr> salesTransHdrOpt = salesTransHdrRepo.findById(salestranshdrid);
		if (salesTransHdrOpt.isPresent()) {
			salesTransHdr = salesTransHdrOpt.get();
//			msg = salesTransHdr.getSalesMode();

		}
		List<SalesDtl> salesDtlList = salesDetailsRepo.findBySalesTransHdrId(salestranshdrid);
		for (SalesDtl salesDtl : salesDtlList) {
//			if(msg.equalsIgnoreCase("KOT") || msg.equalsIgnoreCase("POS"))
//			{
			List<KitDefinitionMst> kitDefinitionMstList = kitDefinitiomMstRepo.findByItemId(salesDtl.getItemId());
			if (kitDefinitionMstList.size() > 0) {
				break;
			}
//			}

			Double qty = itemBatchMstRepository.findQtyByItemAndBatch(salesDtl.getItemId(), 
					salesDtl.getBatch(),
					MapleConstants.Store);

			if (null == qty) {
				if (stockList.equalsIgnoreCase(MapleConstants.STOCKOK)) {
					stockList = MapleConstants.NOTINSTOCK;
				}
				stockList = stockList + salesDtl.getItemName() + "(" + salesDtl.getQty() + "),";

			} else if (salesDtl.getQty() > qty) {
				if (stockList.equalsIgnoreCase(MapleConstants.STOCKOK)) {
					stockList = MapleConstants.NOTINSTOCK;
				}
				Double balance = salesDtl.getQty() - qty;
				stockList = stockList + ", " + salesDtl.getItemName() + "(" + balance + ")";
			}
		}

		return stockList;
	}
}
