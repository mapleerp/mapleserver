package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SupplierPriceMst;
import com.maple.restserver.repository.SupplierPriceMstRepository;

@Service
@Transactional
public class SupplierPriceMstImpl implements SupplierPriceMstService {

	@Autowired
	SupplierPriceMstRepository supplierPriceMstRepository;

	@Override
	public List<SupplierPriceMst> findAllSupplierPriceMst(CompanyMst companyMst) {

		List<SupplierPriceMst> supplierPriceMstList = new ArrayList<SupplierPriceMst>();
		
		String company =  companyMst.getId();
		List<Object> list = supplierPriceMstRepository.findallSupplierPriceMst(company);
		for (int i = 0; i < list.size(); i++) {

			Object[] objAray = (Object[]) list.get(i);
			
			
			SupplierPriceMst report = new SupplierPriceMst();
			report.setSupplierId((String) objAray[0]);
			report.setItemId((String) objAray[1]);
			if(null!= objAray[2]) {
				report.setdP((Double) objAray[2]);
			}else {
				report.setdP(0.0);
			}
			
			 if(null!=objAray[3]) {
				 report.setUpdatedDate((java.sql.Date) objAray[3]);
			 }
			
			report.setId((String) objAray[4]);
			
			
			supplierPriceMstList.add(report);

		}

		return supplierPriceMstList;
	}
}
