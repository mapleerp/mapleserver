package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.report.entity.ReorderReport;
import com.maple.restserver.service.accounting.task.PartialAccountingException;



@Service

@Component
public interface SalesFromOrderService {
	void convertSaleOrderToSales(SalesOrderTransHdr salesOrderTransHdr,String userID) throws PartialAccountingException;

}
