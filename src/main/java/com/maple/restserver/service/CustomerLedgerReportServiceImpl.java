package com.maple.restserver.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.customerLedgerReport;
import com.maple.restserver.repository.SalesTransHdrRepository;


@Service
@Transactional
@Component
public class CustomerLedgerReportServiceImpl implements CustomerLedgerReportService {

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
//not userd
	@Override
	public List<customerLedgerReport> getCustomerLedgerReport(CompanyMst companymst, String customerid,
			String startdate, String enddate, String branchcode) {
		
		return null;
	}

}
