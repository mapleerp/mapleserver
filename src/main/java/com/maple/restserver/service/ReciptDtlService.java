package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.report.entity.DailyReceiptsSummaryReport;
import com.maple.restserver.report.entity.ReceiptInvoice;
import com.maple.restserver.report.entity.ReceiptReport;

@Component
public interface ReciptDtlService {
	List<ReceiptDtl> reciptDtlServiceImpl(String accountid,Date reportdate);
	List<ReceiptInvoice> reciptDtlBetweenDate(String accountid, Date tdate, Date fdate);
	List<ReceiptInvoice> reciptDtlBetweenDate(Date tdate, Date fdate);
	//------------------version 5.1 surya 
		List<DailyReceiptsSummaryReport> getDailyReciptSummaryByDateAndAccount(String companymstid, Date date, String branchcode);
		//------------------version 5.1 surya end
}
