package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.AMDCDailySalesSummaryReport;
import com.maple.restserver.report.entity.PharmacyDailySalesTransactionReport;
import com.maple.restserver.report.entity.PoliceReport;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.utils.SystemSetting;
@Service
public class AMDCDailySalesSummaryReportServiceImpl implements AMDCDailySalesSummaryReportService{
	
	@Value("${spring.datasource.url}")
	private String dbType;
	
	@Autowired
	SalesReceiptsRepository salesReceiptsRepository;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Override
	public List<AMDCDailySalesSummaryReport> findAMDCDailySalesSummaryReport(Date cdate) {
		
		List<AMDCDailySalesSummaryReport> amdcDailySalesSummaryReportList = new ArrayList<AMDCDailySalesSummaryReport>();
		List<Object> obj=new ArrayList<Object>();
		if (dbType.contains("mysql")) {
			obj = salesReceiptsRepository.findamdcDailySalesSummary(cdate);
		}
		else if (dbType.contains("derby")) {
			obj = salesReceiptsRepository.findamdcDailySalesSummaryDerby(cdate);
		}
		
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			AMDCDailySalesSummaryReport amdcDailySalesSummaryReport = new AMDCDailySalesSummaryReport();
			
			amdcDailySalesSummaryReport.setBranch((String) objAray[0]);
			amdcDailySalesSummaryReport.setTotalCashSales((Double) objAray[2]);
			amdcDailySalesSummaryReport.setTotalCardSales((Double) objAray[3]);
			amdcDailySalesSummaryReport.setTotalCreditSales((Double) objAray[1]);
			
			amdcDailySalesSummaryReport.setTotalBankSales((Double) objAray[5]);
			amdcDailySalesSummaryReport.setTotalInsuranceSales((Double) objAray[4]);
			amdcDailySalesSummaryReport.setGrandTotal((Double) objAray[6]);
			
			//amdcDailySalesSummaryReport.setTotalBankSales(0.0);
			//amdcDailySalesSummaryReport.setTotalInsuranceSales(0.0);
			//amdcDailySalesSummaryReport.setGrandTotal(0.0);
			
			amdcDailySalesSummaryReportList.add(amdcDailySalesSummaryReport);
			
		 }

		return amdcDailySalesSummaryReportList;
	
	}
	
	
	
	
	@Override
	public List<PharmacyDailySalesTransactionReport> findPharmacyDailySalesTransactionReport(Date fromDate, Date toDate) {

		List<PharmacyDailySalesTransactionReport> DailySalesTransactionReportList = new ArrayList<PharmacyDailySalesTransactionReport>();
		List<Object> obj = salesTransHdrRepository.findPharmacyDailySalesTransactionReport(fromDate, toDate);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			PharmacyDailySalesTransactionReport dailySalesTransactionReport = new PharmacyDailySalesTransactionReport();
			dailySalesTransactionReport.setDate(SystemSetting.UtilDateToString((Date) objAray[0], "yyyy-MM-dd"));
			dailySalesTransactionReport.setVoucherNumber((String) objAray[1]);
			dailySalesTransactionReport.setCustomerName((String) objAray[2]);
			dailySalesTransactionReport.setSalesMan((String) objAray[6]);
			dailySalesTransactionReport.setInvoiceAmount((Double) objAray[3]);
			dailySalesTransactionReport.setInvoiceDiscount((Double) objAray[5]);
			dailySalesTransactionReport.setTinNumber((String) objAray[4]);
			
			DailySalesTransactionReportList.add(dailySalesTransactionReport);

		}

		return DailySalesTransactionReportList;
	}

	
}
