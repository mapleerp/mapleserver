package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.SupplierBillwiseDueDayReport;
import com.maple.restserver.report.entity.SupplierMonthlySummary;
import com.maple.restserver.report.entity.supplierLedger;

@Component
public interface SupplierLedgerReportService {
	List<supplierLedger> 	getSupplierLedgerReport(String companymstid,String supplierid, String date,String branchcode);
	List<SupplierMonthlySummary> getMonthlySupplierLedgerReport( String companymstid,String supplierid,Date fromdate,Date todate,String branchcode);
	List<SupplierBillwiseDueDayReport> getSupplierDueDayReport(String companymstid, String supplierid, Date fromDate,
			Date toDate);
}
