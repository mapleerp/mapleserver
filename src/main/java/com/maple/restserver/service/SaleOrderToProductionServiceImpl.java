package com.maple.restserver.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.entity.SaleOrderProductionLinkMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ProductionDetailRepository;
import com.maple.restserver.repository.ProductionMstRepository;
import com.maple.restserver.repository.SaleOrderProductionLinkMstRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
public class SaleOrderToProductionServiceImpl implements SaleOrderToProductionService {

	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	ProductionMstRepository productionMstRepository;
	
	@Autowired
	ProductionDetailRepository productionDetailRepository;
	@Autowired
	private VoucherNumberService voucherService;
	
	@Autowired
	SaleOrderProductionLinkMstRepository saleOrderProductionLinkMstRepository;
	
	@Transactional
	@Override
	public String convertSaleOrderToProduction(String companymstid,String branchCode) {
	
		
		
		 Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		List<Object> obj=salesOrderTransHdrRepository.pendingSaleOrder();
		
		
		ProductionMst productionMst=new ProductionMst();
		productionMst.setBranchCode(branchCode);
		productionMst.setCompanyMst(comapnyMstOpt.get());
		VoucherNumber voucherNo = voucherService.generateInvoice(branchCode, companymstid);
		productionMst.setVoucherNumber(voucherNo.getCode());
		productionMst.setVoucherDate(SystemSetting.getSystemDate());
		productionMstRepository.saveAndFlush(productionMst);
	
		ProductionMst productionMsts=productionMstRepository.findByVoucherNumber(voucherNo.getCode());
		 for(int i=0;i<obj.size();i++)
		 {
		Object[] objAray = (Object[]) obj.get(i);
		ProductionDtl  productionDtl=new ProductionDtl();
		productionDtl.setItemId((String) objAray[1]);
		java.util.Date date=SystemSetting.getSystemDate();
		String vDate=SystemSetting.UtilDateToString(date);
		productionDtl.setQty((Double) objAray[2]);
		productionDtl.setProductionMst(productionMsts);
		productionDtl.setBatch(vDate+(String) objAray[3]);
		productionDtl.setStatus("N");
		productionDetailRepository.save(productionDtl);
		SaleOrderProductionLinkMst saleOrderProductionLinkMst=new SaleOrderProductionLinkMst();
		saleOrderProductionLinkMst.setProductionPrePlaningHdrId(productionMsts.getId());
		saleOrderProductionLinkMst.setCompanyMst(comapnyMstOpt.get());
		saleOrderProductionLinkMst.setSaleOrderHdrId((String) objAray[0]);
		saleOrderProductionLinkMst.setVoucherDate(date);
		saleOrderProductionLinkMstRepository.save(saleOrderProductionLinkMst);
		}
		
		 
		 return "Sale Order converted to production";
	}
		
	
}
