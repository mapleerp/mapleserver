package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.MonthlySalesDtl;
import com.maple.restserver.entity.MonthlySalesTransHdr;

@Service
public interface MonthlySalesReportService {

List<MonthlySalesTransHdr> getMonthlySalesReporthdr(String branchcode, java.util.Date fromdate,java.util.Date todate,String companymstid );

List<MonthlySalesDtl>findByMonthlySalesTransHdr(String vouchernumber);
}
