package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.kafka.common.protocol.types.Field.Str;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionDtlDtl;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.report.entity.ProductionDtlsReport;
import com.maple.restserver.report.entity.ReqRawMaterial;
import com.maple.restserver.repository.ActualProductionDtlRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.ProductionDetailRepository;
import com.maple.restserver.repository.ProductionDtlDtlRepository;
import com.maple.restserver.repository.ProductionMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component

public class ProductionServiceImpl implements ProductionService {

	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	private ItemBatchMstRepository itemBatchMstRepo;

	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	ItemMstRepository itemMstRepo;

	@Autowired
	ProductionDtlDtlRepository productionDtlDtlRepo;

	@Autowired
	ProductionMstRepository productionMstRepository;

	@Autowired
	ActualProductionDtlRepository actualProductionDtlRepository;

	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepo;

	@Autowired
	KitDefenitionDtlRepository kItdefinitionDetailRepo;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	ProductionDetailRepository productionDetailRepo;

	@Override
	public String itemIsKitByName(String itemName) {
		String category = itemMstRepo.getCategoryByItemName(itemName);
		String isKit = "N";
		if (category.equalsIgnoreCase("KIT")) {
			isKit = "Y";
		} else {
			isKit = "N";
		}
		return isKit;
	}

	@Override
	public String itemIsKitById(String itemId) {
		// TODO Auto-generated method stub
		String category = itemMstRepo.getCategoryByItemId(itemId);
		String isKit = "N";
		if (category.equalsIgnoreCase("KIT")) {
			isKit = "Y";
		} else {
			isKit = "N";
		}
		return isKit;
	}

	@Override
	public Double makeProductionByProductionDtlId(String productionDtlId, String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		Optional<ProductionDtl> productionDtlList = productionDetailRepo.findById(productionDtlId);
		ProductionDtl productionDtl = productionDtlList.get();

		ProductionMst productionMst = productionDtl.getProductionMst();
		// KitDefinitionMst kitDefinitionMst =
		// kitDefinitionMstRepo.findByItemId(productionDtl.getItemId());

		String dtlId = productionDtl.getId();
		String kitbatch = productionDtl.getBatch();

		String finishedProductId = productionDtl.getItemId();

		Optional<ItemMst> finishedItemO = itemMstRepo.findById(finishedProductId);

		ItemMst finishedItem = finishedItemO.get();

		double productionQuantity = productionDtl.getQty();

		List<ProductionDtlDtl> rawMaterialsList = productionDtlDtlRepo.findByproductionDtlId(dtlId);
		System.out.println("dtl id =" + dtlId);

		Iterator iter = rawMaterialsList.iterator();
		double amount = 0.0;

		final VoucherNumber voucherNo = voucherService.generateInvoice("PROD", companymstid);

		while (iter.hasNext()) {
			ProductionDtlDtl productionDtlDtl = (ProductionDtlDtl) iter.next();
			// Get qty and Rate
			Double qty = productionDtlDtl.getQty();

			String rawMaterialName = productionDtlDtl.getItemName();

			System.out.println("Item Name = " + rawMaterialName);

			ItemMst item = itemMstRepo.findByItemName(rawMaterialName);

			System.out.println("Item Id  = " + item.getItemName());
			// double rate = item.getStandardPrice();

			// amount = amount + (rate * qty);

			/*
			 * Raw Material Out
			 */
			List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
					.findItemBatchMstByItemIdAndQty(item.getId(), qty);
			ItemBatchMst itemBatchMst = null;
			if (itembatchmst.size() >= 1) {

				System.out.println("FOUND ITEM RAW MATERIAL" + rawMaterialName);

				itemBatchMst = itembatchmst.get(0);

				System.out.println("FOUND ITEM RAW MATERIAL" + rawMaterialName);
				System.out.println("itemBatchMst.ITEMID" + itemBatchMst.getId());

				itemBatchMst.setQty(itemBatchMst.getQty() - qty);

				itemBatchMst = itemBatchMstRepo.save(itemBatchMst);

			} else {

				System.out.println(" NOT FOUND ITEM RAW MATERIAL ADDING " + item.getItemName());
				itemBatchMst = new ItemBatchMst();

				itemBatchMst.setBarcode(item.getBarCode());
				itemBatchMst.setBatch(MapleConstants.Nobatch);
				itemBatchMst.setMrp(item.getStandardPrice());
				itemBatchMst.setQty(qty * -1);
				itemBatchMst.setItemId(item.getId());
				itemBatchMstRepo.save(itemBatchMst);

			}

			System.out.println("  RAW MATERIAL ADDING DTL  ");
			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();

			itemBatchDtl.setBarcode(item.getBarCode());
			itemBatchDtl.setBatch(itemBatchMst.getBatch());
			itemBatchDtl.setItemId(itemBatchMst.getItemId());
			itemBatchDtl.setMrp(item.getStandardPrice());
			itemBatchDtl.setQtyIn(0.0);
			itemBatchDtl.setQtyOut(qty);
			itemBatchDtl.setVoucherNumber(voucherNo.getCode());
			itemBatchDtl.setVoucherDate(productionMst.getVoucherDate());

			itemBatchDtl.setSourceVoucherNumber(productionMst.getVoucherNumber());
			itemBatchDtl.setSourceVoucherDate(productionMst.getVoucherDate());

			itemBatchDtlRepository.save(itemBatchDtl);

		}

		/*
		 * Finished Product In
		 */

		System.out.println("kitbatch" + kitbatch);

		List<ItemBatchMst> itembatchmstFinishedProduct = (List<ItemBatchMst>) itemBatchMstRepo
				.findByItemIdAndBatch(finishedProductId, kitbatch);

		ItemBatchMst itemBatchMst2 = null;

		if (itembatchmstFinishedProduct.size() >= 1) {

			System.out.println("FOUND FINISHED BATCH");
			itemBatchMst2 = itembatchmstFinishedProduct.get(0);

			itemBatchMst2.setQty(itemBatchMst2.getQty() + productionQuantity);

			itemBatchMstRepo.save(itemBatchMst2);
		} else {
			{
				itemBatchMst2 = new ItemBatchMst();
				itemBatchMst2.setBarcode(finishedItem.getBarCode());
				itemBatchMst2.setBatch(productionDtl.getBatch());
				itemBatchMst2.setMrp(finishedItem.getStandardPrice());
				itemBatchMst2.setQty(productionQuantity);
				itemBatchMst2.setItemId(finishedProductId);
				itemBatchMstRepo.save(itemBatchMst2);

			}

		}
		ItemBatchDtl itemBatchDtlFinishedProduct = new ItemBatchDtl();
		// final VoucherNumber voucherNo = voucherService.generateInvoice("PROD");

		itemBatchDtlFinishedProduct.setBarcode(finishedItem.getBarCode());
		itemBatchDtlFinishedProduct.setBatch(productionDtl.getBatch());
		itemBatchDtlFinishedProduct.setItemId(finishedProductId);
		itemBatchDtlFinishedProduct.setMrp(finishedItem.getStandardPrice());
		itemBatchDtlFinishedProduct.setQtyOut(0.0);
		itemBatchDtlFinishedProduct.setQtyIn(productionQuantity);
		itemBatchDtlFinishedProduct.setVoucherNumber(voucherNo.getCode());
		itemBatchDtlFinishedProduct.setVoucherDate(productionMst.getVoucherDate());

		itemBatchDtlFinishedProduct.setSourceVoucherNumber(productionMst.getVoucherNumber());
		itemBatchDtlFinishedProduct.setSourceVoucherDate(productionMst.getVoucherDate());

		itemBatchDtlRepository.saveAndFlush(itemBatchDtlFinishedProduct);

		return amount;

	}

	@Override
	public Double getProductionCostByProductionDetailId(String detailId) {
		Optional<ProductionDtl> productionDtl = productionDetailRepo.findById(detailId);

		String kitId = productionDtl.get().getItemId();

		List<ProductionDtlDtl> rawMaterialsList = productionDtlDtlRepo.findByproductionDtlId(kitId);

		Iterator iter = rawMaterialsList.iterator();
		double amount = 0.0;
		while (iter.hasNext()) {
			ProductionDtlDtl productionDtlDtl = (ProductionDtlDtl) iter.next();
			// Get qty and Rate
			double qty = productionDtlDtl.getQty();

			String rawmaterilId = productionDtlDtl.getRawMaterialItemId();

			Optional<ItemMst> itemMst = itemMstRepo.findById(rawmaterilId);

			double rate = itemMst.get().getStandardPrice();

			amount = amount + (rate * qty);

		}

		return amount;
	}

	@Override
	public Double makeProductionByKitId(String itemId, double Productionqty) {

		/*
		 * Find definition.
		 * 
		 */
		Optional<KitDefinitionMst> kitDefinitionMstList = kitDefinitionMstRepo.findById(itemId);
		if (!kitDefinitionMstList.isPresent()) {
			return 0.0;
		}
		KitDefinitionMst kitDefinitionMst = kitDefinitionMstList.get();
		double kitDefqty = kitDefinitionMst.getMinimumQty();

		// Fetch Kit Details

		List<KitDefenitionDtl> kitDefenitionDtlList = kItdefinitionDetailRepo
				.findBykitDefenitionmstId(kitDefinitionMst.getId());

		Iterator iter = kitDefenitionDtlList.iterator();
		while (iter.hasNext()) {
			KitDefenitionDtl kitDefenitionDtl = (KitDefenitionDtl) iter.next();
			String rawItemId = kitDefenitionDtl.getItemId();
			double rawItemQty = kitDefenitionDtl.getQty();

			double unitQty = rawItemQty / kitDefqty;

			// Required RawMaterial Qty

			double requiredQty = Productionqty * unitQty;

			// Get list of batch of Raw Material

			List<ItemBatchMst> itemBatchMstList = itemBatchMstRepo.findByItemId(rawItemId);
			Iterator iterB = itemBatchMstList.iterator();
			while (iterB.hasNext() && requiredQty > 0) {
				ItemBatchMst itemrawB = (ItemBatchMst) iterB.next();
				double itembQ = itemrawB.getQty();
				double balQty = itembQ - requiredQty;

				if (balQty >= 0) {
					requiredQty = 0;

					// We had enough qty
					// Update item batch mst - reqQty

					// Get Item batch
					Optional<ItemBatchMst> itemBatchMstOpt = itemBatchMstRepo.findById(itemrawB.getId());
					ItemBatchMst itemBatchMstQ = itemBatchMstOpt.get();
					itemBatchMstQ.setQty(itemBatchMstQ.getQty() - requiredQty);
					itemBatchMstRepo.save(itemBatchMstQ);

					ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
					itemBatchDtl.setItemId(itemBatchMstQ.getItemId());
					itemBatchDtl.setBatch(itemBatchMstQ.getBatch());

					itemBatchDtl.setBarcode(itemBatchMstQ.getBarcode());
					itemBatchDtl.setExpDate(itemBatchMstQ.getExpDate());
					itemBatchDtl.setMrp(itemBatchMstQ.getMrp());
					itemBatchDtl.setQtyOut(requiredQty);
					itemBatchDtl.setQtyIn(0.0);

					itemBatchDtl.setSourceVoucherDate(SystemSetting.systemDate);

					final VoucherNumber voucherNo = voucherService.generateInvoice("PROD",
							kitDefinitionMst.getCompanyMst().getId());

					itemBatchDtl.setSourceVoucherNumber(voucherNo.getCode());
					itemBatchDtl.setVoucherDate(SystemSetting.systemDate);
					itemBatchDtl.setVoucherNumber(voucherNo.getCode());

				} else {
					requiredQty = balQty * -1;

					// Update item batch mst to zero

					Optional<ItemBatchMst> itemBatchMstOpt = itemBatchMstRepo.findById(itemrawB.getId());
					ItemBatchMst itemBatchMstQ = itemBatchMstOpt.get();
					itemBatchMstQ.setQty(0.0);
					itemBatchMstRepo.save(itemBatchMstQ);

					ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
					itemBatchDtl.setItemId(itemBatchMstQ.getItemId());
					itemBatchDtl.setBatch(itemBatchMstQ.getBatch());

					itemBatchDtl.setBarcode(itemBatchMstQ.getBarcode());
					itemBatchDtl.setExpDate(itemBatchMstQ.getExpDate());
					itemBatchDtl.setMrp(itemBatchMstQ.getMrp());
					itemBatchDtl.setQtyOut(itembQ);
					itemBatchDtl.setQtyIn(0.0);

					itemBatchDtl.setSourceVoucherDate(SystemSetting.systemDate);

					final VoucherNumber voucherNo = voucherService.generateInvoice("PROD",
							kitDefinitionMst.getCompanyMst().getId());

					itemBatchDtl.setSourceVoucherNumber(voucherNo.getCode());
					itemBatchDtl.setVoucherDate(SystemSetting.systemDate);
					itemBatchDtl.setVoucherNumber(voucherNo.getCode());

				}
			}
		}

		return null;
	}

	@Override
	public List<ProductionDtlDtl> getRawMaterialsByDate(Date date) {
		List<ProductionDtlDtl> rawMaterialReportList = new ArrayList();

		List<Object> objarray = productionDtlDtlRepo.getRawmaterialByVoucherDate(date);
		// i.item_name, u.unit_name, sum(dd.qty )
		for (int i = 0; i < objarray.size(); i++) {
			Object[] objAray = (Object[]) objarray.get(i);

			ProductionDtlDtl rawMaterialReport = new ProductionDtlDtl();
			rawMaterialReport.setItemName((String) objAray[0]);

			rawMaterialReport.setUnitId((String) objAray[1]);
			rawMaterialReport.setQty((Double) objAray[2]);

			rawMaterialReportList.add(rawMaterialReport);

		}
		return rawMaterialReportList;
	}

	@Override
	public List<ProductionDtlsReport> getRawMaterialsSumByDate(Date date) {
		List<ProductionDtlsReport> rawMaterialReportList = new ArrayList();

		List<Object> objarray = productionDtlDtlRepo.getRawMaterialsSumByDate(date);
		// i.item_name, u.unit_name, sum(dd.qty )
		for (int i = 0; i < objarray.size(); i++) {
			Object[] objAray = (Object[]) objarray.get(i);

			ProductionDtlsReport rawMaterialReport = new ProductionDtlsReport();
			rawMaterialReport.setItemName((String) objAray[0]);

			rawMaterialReport.setUnitName((String) objAray[1]);
			rawMaterialReport.setQty((Double) objAray[2]);

			rawMaterialReportList.add(rawMaterialReport);

		}
		return rawMaterialReportList;
	}

	@Override
	public List<ProductionDtlsReport> getRawMaterialsSumByBetweenDate(Date fdate, Date tDate) {
		List<ProductionDtlsReport> rawMaterialReportList = new ArrayList();

		List<Object> objarray = productionDtlDtlRepo.getRawMaterialsSumByBetweenDate(fdate, tDate);
		// i.item_name, u.unit_name, sum(dd.qty )
		for (int i = 0; i < objarray.size(); i++) {
			Object[] objAray = (Object[]) objarray.get(i);

			ProductionDtlsReport rawMaterialReport = new ProductionDtlsReport();
			rawMaterialReport.setItemName((String) objAray[0]);

			rawMaterialReport.setUnitName((String) objAray[1]);
			rawMaterialReport.setQty((Double) objAray[2]);
			rawMaterialReport.setVoucherDate((Date) objAray[3]);
			rawMaterialReportList.add(rawMaterialReport);

		}
		return rawMaterialReportList;
	}

	@Override
	public List<ProductionDtl> getKitByDate(Date date) {

		List<ProductionDtl> productionDtlList = new ArrayList();

		List<Object> objarray = productionDetailRepo.ProductionKitDtlByDate(date);
		// i.item_name, u.unit_name, sum(dd.qty )
		for (int i = 0; i < objarray.size(); i++) {
			Object[] objAray = (Object[]) objarray.get(i);

			ProductionDtl productionDtl = new ProductionDtl();

			productionDtl.setId((String) objAray[0]);

			productionDtl.setBatch((String) objAray[1]);

			productionDtl.setItemId((String) objAray[2]);

			productionDtl.setQty((Double) objAray[3]);

			Optional<ProductionMst> productionMstOpt = productionMstRepository.findById((String) objAray[4]);

			ProductionMst productionMst = productionMstOpt.get();

			productionDtl.setProductionMst(productionMst);

			productionDtl.setStatus((String) objAray[5]);

			productionDtl.setStore((String) objAray[6]);

			productionDtlList.add(productionDtl);

		}
		return productionDtlList;
	}

	@Override
	public List<ReqRawMaterial> getRawMaterialQty(String acthdrid) {
		List<ReqRawMaterial> rowMaterialStockList = new ArrayList<ReqRawMaterial>();
		List<ActualProductionDtl> actualProductionDtlList = actualProductionDtlRepository
				.findByActualProductionHdrId(acthdrid);
		List<Object> obj = actualProductionDtlRepository.getRawMaterialQty(acthdrid);
		Double requiredStock = 0.0;
		Double reqStock = 0.0;
		List<Object> objects = new ArrayList<Object>();
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			System.out.print(obj.size() + "obj list size issssssssssssssssssssssssssssssss");
			String itemId = (String) objAray[0];
			requiredStock = (Double) objAray[1];
			String store = (String) objAray[2];

			objects = actualProductionDtlRepository.cheackRowMaterialStockByStore(itemId, requiredStock,store);

			
			System.out.println(objects.size() + "2 list size isssssssssssssssssssssssssssssss");
			for (int j = 0; j < objects.size(); j++) {
				Object[] objArays = (Object[]) objects.get(j);
				// String itemIds=(String)objArays[0];
				String itemIds = (String) objAray[0];
				Double qty = (Double) objArays[1];
				ReqRawMaterial rowMaterialStock = new ReqRawMaterial();
				rowMaterialStock.setItemName((String) objArays[0]);
				rowMaterialStock.setCurrentStock((Double) objArays[1]);
				rowMaterialStock.setUnit((String) objArays[2]);
				reqStock = actualProductionDtlRepository.requiredItemStock(acthdrid, itemIds);
				System.out.print(itemIds + "item id izzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");

				rowMaterialStock.setRequiredStock(reqStock);

				System.out.print(reqStock + "required stock izzzzzzzzzzzzzzzzzzzzz");
				System.out.print((Double) objArays[1] + "current stock");
				rowMaterialStock.setReqQty(reqStock - qty);

				rowMaterialStockList.add(rowMaterialStock);

			}
		}

		return rowMaterialStockList;
	}

	@Override
	public List<ProductionDtl> getPlanedProduction() {

		List<ProductionDtl> ProductionDtlList = new ArrayList<ProductionDtl>();
		List<Object> obj = productionMstRepository.getPlanedProduction();
		System.out.print(obj.size() + "production dtl list size izzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");

		for (int i = 0; i < obj.size(); i++) {
			ProductionDtl productionDtl = new ProductionDtl();
			Object[] objArays = (Object[]) obj.get(i);
			productionDtl.setItemId((String) objArays[0]);
			productionDtl.setBatch((String) objArays[1]);
			productionDtl.setQty((Double) objArays[2]);
			productionDtl.setId((String) objArays[3]);
			ProductionDtlList.add(productionDtl);

		}
		return ProductionDtlList;
	}
	
	
	@Override
	public List<ProductionDtl> getKitByDateByStore(Date date,String store) {

		List<ProductionDtl> productionDtlList = new ArrayList();

		List<Object> objarray = productionDetailRepo.ProductionKitDtlByDateAndStore(date,store);
		// i.item_name, u.unit_name, sum(dd.qty )
		for (int i = 0; i < objarray.size(); i++) {
			Object[] objAray = (Object[]) objarray.get(i);

			ProductionDtl productionDtl = new ProductionDtl();

			productionDtl.setId((String) objAray[0]);

			productionDtl.setBatch((String) objAray[1]);

			productionDtl.setItemId((String) objAray[2]);

			productionDtl.setQty((Double) objAray[3]);

			Optional<ProductionMst> productionMstOpt = productionMstRepository.findById((String) objAray[4]);

			ProductionMst productionMst = productionMstOpt.get();

			productionDtl.setProductionMst(productionMst);

			productionDtl.setStatus((String) objAray[5]);

			productionDtl.setStore((String) objAray[6]);

			productionDtlList.add(productionDtl);

		}
		return productionDtlList;
	}

}
