package com.maple.restserver.service;

import java.net.UnknownHostException;
import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.json.JSONArray;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ConsumptionDtl;
import com.maple.restserver.entity.ConsumptionHdr;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.OpeningStockDtl;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.StockTransferInDtl;
import com.maple.restserver.entity.StockTransferInHdr;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.report.entity.CategoryWiseStockMovement;
import com.maple.restserver.report.entity.PharmacyItemWiseSalesReport;
import com.maple.restserver.report.entity.StockMigrationReport;
import com.maple.restserver.report.entity.StockReport;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;

@Service

@Component
public interface ItemBatchDtlService {

	
	




	List<CategoryWiseStockMovement> getCategoryWiseStockMovement(String branchcode, String companymstid, Date fdate,
			Date tdate);

	String updateItemBatchMstStockFromDtl(CompanyMst companyMst, java.sql.Date curDate) throws UnknownHostException, Exception  ; 

	List<CategoryWiseStockMovement>getCategoryWiseStockMovementReport(String branchcode,String  companymstid ,Date fdate,Date tdate,String categoryid);
	//----------------version 5.0 surya 

		void SalesStockUpdation(ItemMst item, SalesDtl salesDtl, SalesTransHdr salesTransHdr, ItemBatchMst itemBatchMst);

		void updatePurchaseStock(PurchaseDtl purchaseDtl, PurchaseHdr purchaseHdr,ItemMst item);

		void updatePhysicalStock(CompanyMst companyMst, ItemMst item, String batchCode, double mrp,
				java.util.Date expiryDate, Double systemQty, double qty, String voucherNumber, java.util.Date voucherDate,String branchCode);

		void updateProductionStock(ItemMst itemMst, ActualProductionDtl actualProductionDtl, CompanyMst companyMst,
				String actualProductionHdrId, ActualProductionHdr actualProductionHdr);

		void updateDamageStock(ItemMst itemMst, DamageDtl damageDtl, String branchCode, 
				CompanyMst companyMst, String damageHdrId);

		void updateStockTransferStock(StockTransferOutDtl stockTransferOutDtl, StockTransferOutHdr stockTransferOutHdr);

		void updateAcceptStock(String barcode, String batchCode, String itemId, double mrp, double qty,
				java.util.Date transDate, String branchCode, String voucherNumber, java.util.Date transDate2,
				CompanyMst compasnyMst, @Valid StockTransferInHdr stockTransferInHdr,
				StockTransferInDtl stockTransferInDtl);


		//----------------version 5.5 surya

		void updateItemBatchDtlAndItemBatchDtlDaily(String barcode, String batchCode, String itemId, Double standardPrice,
				Double qtyIn, Double qtyOut, String branchCode, java.util.Date invoiceVoucherDate, String sourceVoucherNo,
				String particulars, String sourceParentId, String sourceDtlId, CompanyMst companyMst,VoucherNumber voucherNo);
		//----------------version 5.5 surya end
		//----------------version 5.0 surya end
		//----------------version 6.7 surya 

		void updateConsumptionStock(ItemMst item, ConsumptionDtl consumptionDtl, ConsumptionHdr consumptionHdr);
		//----------------version 6.7 surya end

		void ProductionRawMaterialStock(String rawMaterialId, ItemMst itemMst, Double requiredRawMaterial,
				ActualProductionDtl actualProductionDtl, ActualProductionHdr actualProductionHdr,
				CompanyMst companyMst,String batch);

		List<StockReport> getFastMovingItem(java.util.Date fuDate, java.util.Date tuDate, String companymstid,
				String branchcode,String reportName);

		
		void ItemBatchDtlUpdation(String id, String id2, String id3, Double d, Double qty, Double mrp, String unitId,
				ItemBatchMst itemBatchMst, java.util.Date voucherDate, String voucherNumber, 
				String particular,CompanyMst companyMst,String batch,String branchCode, String STORE);

		List<StockReport> fastMovingItemsBySalesMode(java.util.Date fuDate, java.util.Date tuDate, String companymstid,
				String branchcode, String salesmode, String reportname);

		List<StockReport> fastMovingItemsBySalesModeAndCategory(java.util.Date fuDate, java.util.Date tuDate, String companymstid,
				String branchcode, String salesmode, String category, String reportname);

		Map<String, Double> getBatchWiseQtyFromMobileStore(String itemName, double reqQty,String storeName);

		void SalesStockUpdationMobileStock(ItemMst item, String batch, SalesDtl salesDtl, SalesTransHdr salesTransHdr,
				ItemBatchMst itemBatchMst,String storeName);

	 


		void itemBatchDtlInsertion(java.util.Date tuDate, CompanyMst companyMst);

		// -------------------new version 2.1 surya 

		List<CategoryWiseStockMovement> getItemWiseStockMovementReport(String branchcode, String companymstid,
				Date fdate, Date tdate);

		// -------------------new version 2.1 surya end


		JSONArray getBatchWiseQty(String itemName,double reqQty);

		List<StockMigrationReport> findAllItemBatchDtl(CompanyMst companyMst);

		void saveItemBatchDtlFromOpeningStock(OpeningStockDtl openingStockDtl);

		String stockVerificationForStkTransfer(String companymstid, String stkhdrid);


		List<PharmacyItemWiseSalesReport> findPharmacyItemWiseSalesReport(java.util.Date fromDate,
				java.util.Date toDate);
		

		
		//==================common for item Batch Dtl Insertion=========19-06-2021==========
		void itemBatchDtlInsertionNew(String barcode, String batch, String branchCode, java.util.Date expiryDate,
				String itemId, Double mrp, String particulars, String processInstanceId, Double qtyIn, Double qtyOut,
				String sourceDetailId, String sourceParentId, java.util.Date sourceVoucherDate,
				String sourceVoucherNumber, String store, String taskId, java.util.Date voucherDate,
				String voucherNumber, CompanyMst companyMst);

		//==================common for item Batch Mst Updation=========19-06-2021==========
		void itemBatchMstUpdation(String barcode, String batch, String branchCode, java.util.Date expiryDate,
				String itemId, Double mrp, String processInstanceId, Double qty, String taskId, CompanyMst companyMst,
				String store);


		List<PharmacyItemWiseSalesReport> findPharmacyItemWiseSalesReportByItemName(java.util.Date fromDate,
				java.util.Date toDate, String[] array);

		JSONArray getBatchWiseQtyWithStore(String rawMaterialId, Double requiredRawMaterial,String store);

		List<CategoryWiseStockMovement> getStockMovementbyCategoryWise(String branchcode, CompanyMst companyMst,
				java.util.Date fdate, java.util.Date tdate, String[] array);

		Double findItemStockDetailsByStore(String itemId, String batch, String companymstid, String storeFrom);

		String SalesToBatchDtl(String companymstid, String branchcode,java.util.Date date);

		List<PharmacyItemWiseSalesReport> findItemWiseSalesReportByItemName(java.util.Date fromDate,
				java.util.Date toDate, String branchcode);

	
	

	


		
		

		


}
