package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.SaleOrderinoiceReport;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;

@Transactional
@Component
@Service
public class SalesOrderInvoiceReportServiceImpl {
	
	@Autowired
	SalesOrderTransHdrRepository saleorderTransHdrRepository;

	public List<SaleOrderinoiceReport> getSalesOrderInvoice(String companymstid, String voucherNumber, Date date) {
	
		List<SaleOrderinoiceReport> saleOrderinoiceReportList = new ArrayList();

		List<Object> obj = saleorderTransHdrRepository.getSaleOrderinoiceReport(companymstid,voucherNumber,date);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			SaleOrderinoiceReport saleOrderinoiceReport = new SaleOrderinoiceReport();
			saleOrderinoiceReport.setItemCode((String) objAray[12].toString());
			saleOrderinoiceReport.setItemName((String) objAray[2]);
			if(null != objAray[13]) {
			saleOrderinoiceReport.setExpiryDate((String) objAray[13].toString());
			}
			if(null != objAray[12]) {
			saleOrderinoiceReport.setBatch((String) objAray[11].toString());
			}
			saleOrderinoiceReport.setQty((Double) objAray[5]);
			saleOrderinoiceReport.setRate((Double) objAray[4]);
			saleOrderinoiceReport.setTaxRate((Double) objAray[3]);
			saleOrderinoiceReport.setAmount((Double) objAray[9]);
			if(null != objAray[8]) {
			saleOrderinoiceReport.setGst((Double) objAray[8]);
			}
			if(null != objAray[0]) {
			saleOrderinoiceReport.setVoucherDate((String) objAray[0].toString());
			}
			if(null != objAray[1]) {
			saleOrderinoiceReport.setVoucherNumber((String) objAray[1]);
			}
			saleOrderinoiceReportList.add(saleOrderinoiceReport);
		}

		return saleOrderinoiceReportList;
		
	}
	

}
