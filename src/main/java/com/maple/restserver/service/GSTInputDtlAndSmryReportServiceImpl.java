package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.GSTInputDtlAndSmryReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.utils.SystemSetting;
@Service
public class GSTInputDtlAndSmryReportServiceImpl implements GSTInputDtlAndSmryReportService{

	
	@Autowired
	AccountHeadsRepository accountHeadsRepo;
	
	@Override
	public List<GSTInputDtlAndSmryReport> findGstInputDtlAndSmry(Date fdate, Date tdate) {
		
		List<GSTInputDtlAndSmryReport> gstInputDtlAndSmryReportList = new ArrayList<GSTInputDtlAndSmryReport>();
		List<Object> obj = accountHeadsRepo.findGstInputDtlAndSmry(fdate,tdate);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			GSTInputDtlAndSmryReport gstInputDtlAndSmryReport = new GSTInputDtlAndSmryReport();
			
			gstInputDtlAndSmryReport.setInvoiceDate(SystemSetting.UtilDateToString((Date) objAray[0], "yyyy-MM-dd"));
			gstInputDtlAndSmryReport.setInvoiceNumber((String) objAray[1]);
			gstInputDtlAndSmryReport.setSupplierName((String) objAray[2]);
			gstInputDtlAndSmryReport.setSupplierGst((String) objAray[3]);
			gstInputDtlAndSmryReport.setTotalPurchaseExcludingGst((Double) objAray[5]);
			Double totalPurchaseExcludingGst = (Double) objAray[5];
			
			Double GstAmount = (Double) objAray[6];
			Double totalPurchase = totalPurchaseExcludingGst + GstAmount;
			
			gstInputDtlAndSmryReport.setGstAmount((Double) objAray[6]);
			gstInputDtlAndSmryReport.setTotalPurchase(totalPurchase);
			gstInputDtlAndSmryReport.setBranch((String) objAray[8]);
			gstInputDtlAndSmryReport.setGstPercent((String) objAray[7].toString());
			gstInputDtlAndSmryReport.setExcemptedPurchase((Double) objAray[9]);
			gstInputDtlAndSmryReportList.add(gstInputDtlAndSmryReport);
			
		 }

		return gstInputDtlAndSmryReportList;
	
	}
}
