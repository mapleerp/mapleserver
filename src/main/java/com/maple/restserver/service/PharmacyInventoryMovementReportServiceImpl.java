package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.report.entity.PharmacyItemMovementAnalisysReport;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
public class PharmacyInventoryMovementReportServiceImpl implements PharmacyInventoryMovementReportService{

	@Autowired
	ItemBatchMstRepository itemBatchMstRepository;
	
	@Autowired
	CategoryMstRepository categoryMstRepository;
	
	@Autowired
	ItemMstRepository itemMstRepository;
 
	@Override
	public List<PharmacyItemMovementAnalisysReport> getPharmacyInventoryMovementReport(CompanyMst companyMst,
			Date fdate, Date tdate, String branchCode, String[] array) {
	


		List<PharmacyItemMovementAnalisysReport>  pharmacyItemMovementAnalisysReportList=new ArrayList<PharmacyItemMovementAnalisysReport>();
		
		for(String categoryName:array) {
			CategoryMst categoryMst=categoryMstRepository.findByCategoryNameAndCompanyMst(categoryName,companyMst);

	//	List<Object> obj=itemBatchMstRepository.getPharmacyInventoryMovementReport(fdate ,tdate, branchCode, categoryMst.getId());
		PharmacyItemMovementAnalisysReport pharmacyItemMovementAnalisysReport=new PharmacyItemMovementAnalisysReport();
			/*
			 * for(int i=0;i<obj.size();i++) { Object[] objAray =(Object[]) obj.get(i);
			 * 
			 * String voucherDate=SystemSetting.UtilDateToString((Date) objAray[0]);
			 * pharmacyItemMovementAnalisysReport.setTrEntryDate(voucherDate);
			 * pharmacyItemMovementAnalisysReport.setGroupName((String) objAray[1]);
			 * pharmacyItemMovementAnalisysReport.setItemCode((String) objAray[2]);
			 * pharmacyItemMovementAnalisysReport.setBatchCode((String) objAray[3]);
			 * pharmacyItemMovementAnalisysReport.setDescription((String) objAray[4]);
			 * pharmacyItemMovementAnalisysReport.setUnitName((String) objAray[5]);
			 * pharmacyItemMovementAnalisysReport.setVoucherNumber((String) objAray[6]);
			 * pharmacyItemMovementAnalisysReport.setInwardQty((Double) objAray[7]);
			 * pharmacyItemMovementAnalisysReport.setOutwardQty((Double) objAray[8]);
			 * pharmacyItemMovementAnalisysReport.setCost((Double) objAray[9]);
			 * pharmacyItemMovementAnalisysReport.setItemName((String) objAray[10]);
			 * pharmacyItemMovementAnalisysReport.setPartyName((String) objAray[2]);
			 * pharmacyItemMovementAnalisysReport.setBalance((Double) objAray[2]);
			 * pharmacyItemMovementAnalisysReport.setOpeningBalance((Double) objAray[2]);
			 * pharmacyItemMovementAnalisysReport.setValue((Double) objAray[2]);
			 * 
			 * pharmacyItemMovementAnalisysReportList.add(pharmacyItemMovementAnalisysReport
			 * ); }
			 */
		}
		return pharmacyItemMovementAnalisysReportList;
		
		

	}

	@Override
	public List<PharmacyItemMovementAnalisysReport> getPharmacyInventoryMovementReportByCatItem(CompanyMst companyMst,
			Date fdate, Date tdate, String branchCode, String[] categoryArray, String[] itemArray,String batch) {

		List<PharmacyItemMovementAnalisysReport>  pharmacyItemMovementAnalisysReportList=new ArrayList<PharmacyItemMovementAnalisysReport>();
		

		

	
		if(!categoryArray.toString().equalsIgnoreCase("NO-CATEGORY")&&!itemArray.toString().equalsIgnoreCase("NO-ITEM")&&!batch.toString().equalsIgnoreCase("NO-BATCH-SELECTED"));{

			List<Object> obj=itemBatchMstRepository.getPharmacyInventoryMovementReportByCatItemBatch(fdate ,tdate, branchCode, itemArray,categoryArray,batch);
			for(int i=0;i<obj.size();i++) {
				
				PharmacyItemMovementAnalisysReport pharmacyItemMovementAnalisysReport=new PharmacyItemMovementAnalisysReport();
				Object[] objAray =(Object[]) obj.get(i);
				
				String voucherDate=SystemSetting.UtilDateToString((Date) objAray[0]);
				pharmacyItemMovementAnalisysReport.setTrEntryDate(voucherDate);
				pharmacyItemMovementAnalisysReport.setGroupName((String) objAray[1]);
				pharmacyItemMovementAnalisysReport.setItemCode((String) objAray[2]);
				pharmacyItemMovementAnalisysReport.setBatchCode((String) objAray[3]);
				pharmacyItemMovementAnalisysReport.setDescription((String) objAray[4]);
				pharmacyItemMovementAnalisysReport.setUnitName((String) objAray[5]);
				pharmacyItemMovementAnalisysReport.setVoucherNumber((String) objAray[6]);
				pharmacyItemMovementAnalisysReport.setInwardQty((Double) objAray[7]);
				pharmacyItemMovementAnalisysReport.setOutwardQty((Double) objAray[8]);
				pharmacyItemMovementAnalisysReport.setCost((Double) objAray[9]);
				pharmacyItemMovementAnalisysReport.setItemName((String) objAray[10]);
			//	pharmacyItemMovementAnalisysReport.setPartyName((String) objAray[2]);
		
				ItemMst itemMst=itemMstRepository.findByItemName((String) objAray[10]);
				Double openingBalance=0.0;
				if(null!=itemMst) {
					openingBalance=	itemBatchMstRepository.getOpeningStockBatchWise(fdate, branchCode, itemMst.getId(),batch);
				}
				pharmacyItemMovementAnalisysReport.setOpeningBalance(openingBalance);
				Double currentStock=itemBatchMstRepository.getCurrentStockBatchWise(fdate, tdate, branchCode, itemMst.getId(),batch);
				Double balanceStock=currentStock+openingBalance;
				pharmacyItemMovementAnalisysReport.setBalance(balanceStock);
				pharmacyItemMovementAnalisysReport.setValue(balanceStock*(Double) objAray[9]);
			    pharmacyItemMovementAnalisysReportList.add(pharmacyItemMovementAnalisysReport);
			
			}
		}
		if(!categoryArray.toString().equalsIgnoreCase("NO-CATEGORY")&&!itemArray.toString().equalsIgnoreCase("NO-ITEM")&&batch.toString().equalsIgnoreCase("NO-BATCH-SELECTED")) {
		List<Object> obj=itemBatchMstRepository.getPharmacyInventoryMovementReportByCatItem(fdate ,tdate, branchCode, itemArray,categoryArray);
		
		for(int i=0;i<obj.size();i++) {
			Object[] objAray =(Object[]) obj.get(i);
			PharmacyItemMovementAnalisysReport pharmacyItemMovementAnalisysReport=new PharmacyItemMovementAnalisysReport();
			String voucherDate=SystemSetting.UtilDateToString((Date) objAray[0]);
			pharmacyItemMovementAnalisysReport.setTrEntryDate(voucherDate);
			pharmacyItemMovementAnalisysReport.setGroupName((String) objAray[1]);
			pharmacyItemMovementAnalisysReport.setItemCode((String) objAray[2]);
			pharmacyItemMovementAnalisysReport.setBatchCode((String) objAray[3]);
			pharmacyItemMovementAnalisysReport.setDescription((String) objAray[4]);
			pharmacyItemMovementAnalisysReport.setUnitName((String) objAray[5]);
			pharmacyItemMovementAnalisysReport.setVoucherNumber((String) objAray[6]);
			pharmacyItemMovementAnalisysReport.setInwardQty((Double) objAray[7]);
			pharmacyItemMovementAnalisysReport.setOutwardQty((Double) objAray[8]);
			pharmacyItemMovementAnalisysReport.setCost((Double) objAray[9]);
			pharmacyItemMovementAnalisysReport.setItemName((String) objAray[10]);
			pharmacyItemMovementAnalisysReport.setPartyName((String) objAray[2]);
	
			ItemMst itemMst=itemMstRepository.findByItemName((String) objAray[10]);
			Double openingBalance=0.0;
			if(null!=itemMst) {
				openingBalance=	itemBatchMstRepository.getOpeningStock(fdate, branchCode, itemMst.getId());
			}
			if(null == openingBalance) {
				openingBalance = 0.0;
			}
			pharmacyItemMovementAnalisysReport.setOpeningBalance(openingBalance);
			Double currentStock=itemBatchMstRepository.getCurrentStock(fdate, tdate, branchCode, itemMst.getId());
			if(null == currentStock) {
				currentStock = 0.0;
			}
			Double balanceStock=currentStock+openingBalance;
			pharmacyItemMovementAnalisysReport.setBalance(balanceStock);
			pharmacyItemMovementAnalisysReport.setValue(balanceStock*(Double) objAray[9]);
		    pharmacyItemMovementAnalisysReportList.add(pharmacyItemMovementAnalisysReport);
		
		}
		
		}
		if(!categoryArray.toString().equalsIgnoreCase("NO-CATEGORY")&&itemArray.toString().equalsIgnoreCase("NO-ITEM")&&batch.toString().equalsIgnoreCase("NO-BATCH-SELECTED")){
			List<Object> obj=itemBatchMstRepository.getPharmacyInventoryMovementReportByCategory(fdate ,tdate, branchCode,categoryArray);
			for(int i=0;i<obj.size();i++) {
				Object[] objAray =(Object[]) obj.get(i);
				PharmacyItemMovementAnalisysReport pharmacyItemMovementAnalisysReport=new PharmacyItemMovementAnalisysReport();
				String voucherDate=SystemSetting.UtilDateToString((Date) objAray[0]);
				pharmacyItemMovementAnalisysReport.setTrEntryDate(voucherDate);
				pharmacyItemMovementAnalisysReport.setGroupName((String) objAray[1]);
				pharmacyItemMovementAnalisysReport.setItemCode((String) objAray[2]);
				pharmacyItemMovementAnalisysReport.setBatchCode((String) objAray[3]);
				pharmacyItemMovementAnalisysReport.setDescription((String) objAray[4]);
				pharmacyItemMovementAnalisysReport.setUnitName((String) objAray[5]);
				pharmacyItemMovementAnalisysReport.setVoucherNumber((String) objAray[6]);
				pharmacyItemMovementAnalisysReport.setInwardQty((Double) objAray[7]);
				pharmacyItemMovementAnalisysReport.setOutwardQty((Double) objAray[8]);
				pharmacyItemMovementAnalisysReport.setCost((Double) objAray[9]);
				pharmacyItemMovementAnalisysReport.setItemName((String) objAray[10]);
				pharmacyItemMovementAnalisysReport.setPartyName((String) objAray[2]);
		
				ItemMst itemMst=itemMstRepository.findByItemName((String) objAray[10]);
				Double openingBalance=0.0;
				if(null!=itemMst) {
					
					
					openingBalance=	itemBatchMstRepository.getOpeningStockByCategory(fdate, branchCode, itemMst.getCategoryId());
				}
				pharmacyItemMovementAnalisysReport.setOpeningBalance(openingBalance);
				Double currentStock=itemBatchMstRepository.getCurrentStockByCategory(fdate, tdate, branchCode, itemMst.getCategoryId());
				Double balanceStock=currentStock+openingBalance;
				pharmacyItemMovementAnalisysReport.setBalance(balanceStock);
				pharmacyItemMovementAnalisysReport.setValue(balanceStock*(Double) objAray[9]);
			    pharmacyItemMovementAnalisysReportList.add(pharmacyItemMovementAnalisysReport);
			
			}
				
			
		
		}
		return pharmacyItemMovementAnalisysReportList;
	}
	
	
	
	
	
	
	
}
