package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.PurchasePriceDefinitionMst;

@Service
public interface PurchasePriceDefinitionService {

	
	List<PurchasePriceDefinitionMst> retrievePurchasePriceDefinitionMstByHdrId(String companymstid, String hdrid);
	
	String getPurchasePriceDefinitionMstByHdrIdFromLocalDB(String companymstid, String hdrid);

}
