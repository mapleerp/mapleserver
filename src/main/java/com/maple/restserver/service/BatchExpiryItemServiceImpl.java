package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.report.entity.BatchExpiryItemReport;
import com.maple.restserver.report.entity.BatchItemWiseSalesReport;
import com.maple.restserver.repository.ItemBatchMstRepository;

@Service
@Transactional
@Component
public class BatchExpiryItemServiceImpl implements BatchExpiryItemService {

	
	@Autowired
	ItemBatchMstRepository itemBatchMstRepo;
	@Override
	public List<BatchExpiryItemReport> itemWiseSortExpiry(String companymstid, String branchcode, Date fDate,
			List<String> itemList) {
		List<BatchExpiryItemReport> reportList=new ArrayList<BatchExpiryItemReport>();
		for(String itemId:itemList) {
			
			List<Object>objList=itemBatchMstRepo.findByShortExpiryItem( companymstid,  branchcode,itemId);
			 
			  for(int i=0;i<objList.size();i++)
				 {
				Object[] objAray = (Object[]) objList.get(i);
				BatchExpiryItemReport report=new BatchExpiryItemReport();
				
				  report.setAmount((Double) objAray[5] * (Double) objAray[6]);
				  report.setBatch((String) objAray[3]);
				  report.setCategory((String) objAray[0]);
				  report.setExpiryDate((java.sql.Date)objAray[4]);
				  report.setItemCode((String) objAray[2]);
				  report.setItemName((String) objAray[1]);
				  report.setQty((Double) objAray[5]);
				  report.setRate((Double) objAray[6]);
				  //report.setAge();
				//  report.setAge(age);
				  reportList.add(report);
				 }
		  }
			
		return reportList;
	}

}
