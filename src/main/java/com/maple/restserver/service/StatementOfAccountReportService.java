package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.SummaryStatementOfAccountReport;

@Component
public interface StatementOfAccountReportService {
List<SummaryStatementOfAccountReport>	StatementOfAccountReport(String companymstid,Date startDate,Date endDate,String acconuntId);
}
