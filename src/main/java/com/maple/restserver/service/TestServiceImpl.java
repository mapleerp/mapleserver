package com.maple.restserver.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.repository.ItemMstRepository;

@Service
@Transactional
@Component
public class TestServiceImpl  implements TestService{

	@Autowired
	ItemMstRepository itemMstRepository;
	@Override
	public ItemMst fetchRandomItem(int x) {
		
		
      ItemMst itemMst=itemMstRepository.fetchRandomItem(x);
		
		
		
		return itemMst;
	}

	
	
	
}
