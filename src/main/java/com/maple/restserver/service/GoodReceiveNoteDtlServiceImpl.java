package com.maple.restserver.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.GoodReceiveNoteDtl;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.repository.GoodReceiveNoteDtlRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;

@Service
@Transactional
public class GoodReceiveNoteDtlServiceImpl  implements GoodReceiveNoteDtlService{
	@Autowired
	GoodReceiveNoteDtlRepository goodReceiveNoteDtlRepository;
	
	
	@Override
	public List<GoodReceiveNoteDtl> updateGoodReceiveNoteDtlDisplaySerial(String goodReceiveNoteHdr) {

		List<GoodReceiveNoteDtl> goodReceiveNoteDtlList = goodReceiveNoteDtlRepository
				.findByGoodReceiveNoteHdrIdOrderBySerial(goodReceiveNoteHdr);
		
		
		int DisplaySrlNo=0;
		for (GoodReceiveNoteDtl goodReceiveNoteDtl : goodReceiveNoteDtlList) {
			DisplaySrlNo++;
			goodReceiveNoteDtl.setDisplaySerial(DisplaySrlNo);
			goodReceiveNoteDtlRepository.save(goodReceiveNoteDtl);
			
			
		}

		return goodReceiveNoteDtlList;
	}
}
