package com.maple.restserver.service;


import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.KitValidationReport;

@Service
@Transactional
public interface kitdefinionservice {
	
	public List<KitDefinitionMst> getKitDefinitionMst(String pdfFileName);

	public List<KitDefenitionDtl> findKitDefMstId(String kitdefinitionMstId);

public KitValidationReport findRawMaterialForSingleItemInMachineUnit(String companyMstId,String itemId)throws  ResourceNotFoundException;
 

public void runTimeProduction(String itemId,String batch,String barcode,CompanyMst companymst,
			String dtlid,String hdrid,String voucherNumber,Date voucherDate,String branchCode, Double diffQty);


public void executebatchwiserowmaterialupdate(String voucherNumber, Date voucherDate, CompanyMst companyMst,
			String itemName,String itemId,String batch,Double qty,String branchCode,String parentId,
			String dtlId);
}
