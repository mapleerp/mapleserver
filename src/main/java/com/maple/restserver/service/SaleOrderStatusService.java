package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.SalesOrderDtl;


@Component
public interface SaleOrderStatusService {

	public  List<SalesOrderDtl>FindBySaleOrderTransHdrVoucherDate(String companymstid , String voucherNumber, Date voucherDate);
	
	
}
