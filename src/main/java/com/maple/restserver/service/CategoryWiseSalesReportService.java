package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.CategoryWiseSalesReport;

@Service
public interface CategoryWiseSalesReportService {

	
	List<CategoryWiseSalesReport>getCategoryWiseSalesReport(Date fromDate,Date toDate,String branchCode,CompanyMst companyMst);
}
