package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.RawMaterialIssueReport;
import com.maple.restserver.report.entity.RawMaterialReturnReport;
import com.maple.restserver.repository.RawMaterialReturnHdrRepository;

@Service
@Transactional
@Component
public class RawMaterialReturnServiceImpl implements RawMaterialReturnService  {
	
	@Autowired
	RawMaterialReturnHdrRepository rawMaterialReturnHdrRepo;
	
	
	@Override
	public List<RawMaterialReturnReport> findByRawMaterialVoucher(String voucherNumber, Date vDate,String companyid)
	{
		List< RawMaterialReturnReport> rawMaterialReturnReportList =   new ArrayList();
		List<Object> obj = rawMaterialReturnHdrRepo.rawMaterialRetunrByVoucherNoAndDate(voucherNumber,  vDate, companyid);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 RawMaterialReturnReport rawMaterialReturn = new RawMaterialReturnReport();
			 rawMaterialReturn.setItemName((String) objAray[2]);
			 rawMaterialReturn.setUnitName((String) objAray[3]);
			 rawMaterialReturn.setBatch((String) objAray[4]);
			 rawMaterialReturn.setQty((Double)objAray[5]);
			 rawMaterialReturn.setVoucherNumber((String) objAray[0]);
			 rawMaterialReturn.setVoucherDate((Date) objAray[1]);
			rawMaterialReturnReportList.add(rawMaterialReturn);
		 }
			 
		 
		return rawMaterialReturnReportList;
		
	}

}
