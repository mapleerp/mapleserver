package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.report.entity.BatchWiseCustomerSalesReport;
import com.maple.restserver.report.entity.BatchwiseSalesTransactionReport;
import com.maple.restserver.repository.SalesTransHdrRepository;

@Service
@Transactional
public class BatchWiseCustomerSalesReportServiceImpl implements BatchWiseCustomerSalesReportService  {

	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Override
	public List<BatchWiseCustomerSalesReport> customerwisesalesreport(String companymstid, String branchcode,
			Date fDate, Date TDate, List<String> itemList) {
		
		List<BatchWiseCustomerSalesReport> reportList=new ArrayList<BatchWiseCustomerSalesReport>();
		for(String itemId:itemList) {
			
			List<Object>objList=salesTransHdrRepository.customerwisesalesreport( companymstid,  branchcode,fDate,TDate,itemId);
			 
			  for(int i=0;i<objList.size();i++)
				 {
				Object[] objAray = (Object[]) objList.get(i);
				BatchWiseCustomerSalesReport report=new BatchWiseCustomerSalesReport();
				
				  report.setVouchernumber((String) objAray[0]); 
				  report.setInvoicedate((String) objAray[1]);
				  report.setItemname((String) objAray[2]); 
				  report.setBatchcode((String) objAray[3]);
				  report.setQty((Double) objAray[4]);
				  report.setAmount((Double) objAray[5]);
				  report.setCustomername((String)objAray[6]);
				  reportList.add(report);
				 }
		  }
			
		return reportList;
	}

	
	
	
	
	
}
