package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitchenCategoryDtl;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KitchenCategoryDtlRepository;

@Service
@Transactional
@Component
public class KitchenCategoryServiceImpl implements KitchenCategoryService {

	@Autowired
	KitchenCategoryDtlRepository kitchenCategoryRepo;
	
	@Override
	public List<String> getKitchenItems(CompanyMst companyMst) {
		ArrayList<String> itemList = new ArrayList<String>();
		List<KitchenCategoryDtl> kitchenCategoryList = kitchenCategoryRepo.findByCompanyMst(companyMst);
		for(int i = 0;i<kitchenCategoryList.size();i++)
		{
			List<String> itemMstList = kitchenCategoryRepo.getItemMstByKitchenCategory(kitchenCategoryList.get(i).getCategoryId());
			System.out.println(itemMstList);
			for(int j=0;j<itemMstList.size();j++)
			{
				
				itemList.add(itemMstList.get(j).toString());
			}
		}
		return itemList;
	}

}
