package com.maple.restserver.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.DayBook;
import com.maple.restserver.entity.OwnAccount;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptInvoiceDtl;
import com.maple.restserver.report.entity.DailyPaymentSummaryReport;
import com.maple.restserver.report.entity.DailyReceiptsSummaryReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;

import com.maple.restserver.repository.DayBookRepository;
import com.maple.restserver.repository.OwnAccountRepository;
import com.maple.restserver.repository.ReceiptDtlRepository;
import com.maple.restserver.repository.ReceiptInvoiceDtlRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;

@Service
@Transactional
@Component
public class ReceiptEditServiceImpl implements ReceiptEditService {

	@Autowired
	DayBookRepository dayBookRepo;
	
	@Autowired
	private ReceiptDtlRepository receiptDtlRepo;
	
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	VoucherNumberService voucherNumberService;
	
	@Autowired
	OwnAccountRepository ownAccountRepo;
	
	@Autowired
	ReceiptInvoiceDtlRepository receiptInvoiceDtlRepo;
	
	
	
	public void deleteReceiptAndOwnAccount(String cmpany,String receiptHdrId,ReceiptDtl receiptDtl)
	{
	 List<OwnAccount> getOwn = ownAccountRepo.findByCompanyMstIdAndReceiptHdrId(cmpany, receiptHdrId);
	
		if(getOwn.size()>0)
    	{
			ownAccountRepo.deleteByReceiptHdrId(receiptHdrId);
			Optional<AccountHeads> custSaved = accountHeadsRepository.findById(receiptDtl.getAccount());
			if(null != custSaved.get())
			{
				OwnAccount ownAccount = new OwnAccount();
				ownAccount.setAccountId(receiptDtl.getAccount());
				ownAccount.setAccountType("CUSTOMER");
				ownAccount.setCreditAmount(receiptDtl.getAmount());
				ownAccount.setDebitAmount(0.0);
				ownAccount.setOwnAccountStatus("PENDING");
				ownAccount.setRealizedAmount(0.0);
				String sdtae = ClientSystemSetting.UtilDateToString(receiptDtl.getTransDate());
				System.out.println(sdtae);
				ownAccount.setVoucherDate(ClientSystemSetting.StringToSqlDate(sdtae,"yyyy-MM-dd"));
				ownAccount.setVoucherNo(voucherNumberService.generateInvoice("OWNACC",custSaved.get().getCompanyMst().getId()).getCode());
				ownAccount.setReceiptHdr(receiptDtl.getReceiptHdr());
				Optional<CompanyMst> companymst = companyMstRepo.findById(cmpany);
				ownAccount.setCompanyMst(companymst.get());
				ownAccountRepo.save(ownAccount);
			
			}
    	}
		List<ReceiptInvoiceDtl> getReceiptInv = receiptInvoiceDtlRepo.findByReceiptHdr(receiptDtl.getReceiptHdr());
		if(getReceiptInv.size()>0)
		{
			receiptInvoiceDtlRepo.deleteByReceiptHdrId(receiptHdrId);
			 List<OwnAccount> getOwn1 = ownAccountRepo.findByCompanyMstIdAndReceiptHdrId(cmpany, receiptHdrId);
			 if(getOwn1.size()==0)
			 {
					Optional<AccountHeads> custSaved = accountHeadsRepository.findById(receiptDtl.getAccount());
					if(null != custSaved.get())
					{
						OwnAccount ownAccount = new OwnAccount();
						ownAccount.setAccountId(receiptDtl.getAccount());
						ownAccount.setAccountType("CUSTOMER");
						ownAccount.setCreditAmount(receiptDtl.getAmount());
						ownAccount.setDebitAmount(0.0);
						ownAccount.setOwnAccountStatus("PENDING");
						ownAccount.setRealizedAmount(0.0);
						String sdtae = ClientSystemSetting.UtilDateToString(receiptDtl.getTransDate());
						System.out.println(sdtae);
						ownAccount.setVoucherNo(voucherNumberService.generateInvoice("OWNACC",custSaved.get().getCompanyMst().getId()).getCode());
						ownAccount.setVoucherDate(ClientSystemSetting.StringToSqlDate(sdtae,"yyyy-MM-dd"));						
						ownAccount.setReceiptHdr(receiptDtl.getReceiptHdr());
						Optional<CompanyMst> companymst = companyMstRepo.findById(cmpany);
						ownAccount.setCompanyMst(companymst.get());
						ownAccountRepo.save(ownAccount);
		    		
					}
			 }
		}
		
		}

	@Override
	public List<DailyReceiptsSummaryReport> receiptSummaryReport(String companymstid, java.util.Date date) {
		
		List<DailyReceiptsSummaryReport> receiptsSummuryList = new ArrayList<>();
		List<String> receiptModeList = receiptDtlRepo.findAllReceiptMode(companymstid);
		System.out.println("receiptModeList======"+receiptModeList.size());
		
		for(String receiptMode : receiptModeList)
		{
			List<Object> objList = receiptDtlRepo.receiptSummaryReport(companymstid,date,receiptMode);
			System.out.println("objList======"+objList.size());

			for(int i=0; i<objList.size(); i++)
			{
				Object[] objArray = (Object[]) objList.get(i);
				
				DailyReceiptsSummaryReport dailyReceiptsSummaryReport = new DailyReceiptsSummaryReport();
				dailyReceiptsSummaryReport.setDebitAccount((String) objArray[1]);
				dailyReceiptsSummaryReport.setReceiptMode(receiptMode);
				dailyReceiptsSummaryReport.setTotalCash((Double) objArray[0]);
				
				receiptsSummuryList.add(dailyReceiptsSummaryReport);
			}
		
		}
		
		return receiptsSummuryList;
	}

	//-------------------version 4.14
		@Override
		public DailyReceiptsSummaryReport receiptSummaryReportByReceiptMode(String companymstid, java.util.Date date,
				String receiptmode) {
			Double obj = receiptDtlRepo.receiptSummaryReportByReceiptMode(companymstid,date,receiptmode);
			
				DailyReceiptsSummaryReport dailyReceiptsSummaryReport = new DailyReceiptsSummaryReport();
				dailyReceiptsSummaryReport.setTotalCash(obj);
				
				return dailyReceiptsSummaryReport;
				
		}
		//-------------------version 4.14 end


}
