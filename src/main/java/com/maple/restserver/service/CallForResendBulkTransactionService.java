package com.maple.restserver.service;

import java.util.Date;

import org.springframework.stereotype.Service;


@Service
public interface CallForResendBulkTransactionService {

	void publishforbulktransaction(String branchCode, Date fDate, Date tDate, String vouchertype);

}
