package com.maple.restserver.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LocalCustomerMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LocalCustomerRepository;

@Service
@Transactional
public class LocalCustomerMstServiceIml  implements LocalCustomerMstService{

	@Autowired
	LocalCustomerRepository localCustomerRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	

	@Override
	public String initializeLocalCustomer(String companymstid) {
          Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		
		if(!companyOpt.isPresent())
		{
			return "Failed";
		}
		
		LocalCustomerMst localCustomerMst = new LocalCustomerMst();
		
		List<LocalCustomerMst> localCustomerMstList = localCustomerRepository.findByLocalcustomerNameAndCompanyMst("LOCALCUSTOMER",companyOpt.get());
		if(localCustomerMstList.size()==0)
		{
			localCustomerMst = new LocalCustomerMst();
			
			 
			
			localCustomerMst.setLocalcustomerName("LOCALCUSTOMER");
			
			localCustomerMst.setCompanyMst(companyOpt.get());
			
			localCustomerMst = localCustomerRepository.save(localCustomerMst);
		}
		
		
		return "Success";
	}
	}


