package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.PharmacyBrachStockMarginReport;

@Service

@Component
public interface PharmacyBranchStockMarginRptService {

	List<PharmacyBrachStockMarginReport> getPharmacyBranchStockMarginReport(Date fromDate, Date toDate, String branchcode);

}
