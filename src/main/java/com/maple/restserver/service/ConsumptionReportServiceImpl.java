package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ConsumptionReasonMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.report.entity.ConsumptionHdrReport;
import com.maple.restserver.report.entity.ConsumptionReport;
import com.maple.restserver.report.entity.ConsumptionReportDtl;
import com.maple.restserver.report.entity.InsuranceWiseSalesReport;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.ConsumptionHdrRepository;
import com.maple.restserver.repository.ConsumptionReasonMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
@Component
@Transactional
public class ConsumptionReportServiceImpl implements ConsumptionReportService {

	@Autowired
	CategoryMstRepository categoryMstRepo;
	@Autowired
	ConsumptionReasonMstRepository consumptionReasonMstRepository;
	@Autowired
	ConsumptionHdrRepository consumptionHdrRepository;
	
	@Autowired
	ItemMstRepository itemMstRepository;
	
	@Override
	public List<ConsumptionReport> getConsumptionReport(String branchcode, Date fromdate, Date todate,
			String companymstid) {

		List<ConsumptionReport> ConsumptionReportList= new ArrayList();
		
		List<Object> obj = 	consumptionHdrRepository.getConsumptionReport(   branchcode,    fromdate,  todate,  companymstid);
		
		
		
		 for(int i=0;i<obj.size();i++)
		 {
		
			 Object[] objAray = (Object[]) obj.get(i);
			 ConsumptionReport report=new ConsumptionReport();
			 report.setItemName((String) objAray[0]);
			 report.setQty((Double) objAray[1]);
			 report.setMrp((Double) objAray[2]);
			 report.setReason((String) objAray[3]);
			 report.setUnit((String) objAray[4]);
			 report.setAmount((Double) objAray[5]);
		
			 ConsumptionReportList.add(report);
		
		 }
		 
		
		return ConsumptionReportList;
	}
	@Override
	public List<ConsumptionHdrReport> getConsumptionHdrReport(String branchcode, Date fromdate, Date todate,
			String companymstid, String reasonid) {
	List<ConsumptionHdrReport>consumptionHdrReportList=new ArrayList<ConsumptionHdrReport>();
		List<Object> obj=consumptionHdrRepository.getConsumptionHdrReport(branchcode,fromdate,todate,companymstid ,reasonid);

		 for(int i=0;i<obj.size();i++)
		 {
		
			 Object[] objAray = (Object[]) obj.get(i);
			 ConsumptionHdrReport report=new ConsumptionHdrReport();
			 report.setVoucherNumber((String) objAray[0]);
			 String voucherDate=SystemSetting.UtilDateToString((Date) objAray[1]);
			 report.setVoucherDate(voucherDate);
			 report.setReason((String) objAray[2]);
			 consumptionHdrReportList.add(report);
		
		 }
		return consumptionHdrReportList;
		
	
	}
	@Override
	public List<ConsumptionReportDtl> getConsumptionDtlReport( String reasonid, Date fromdate, Date todate) {
		List<ConsumptionReportDtl>consumptionReportDtlList=new ArrayList<ConsumptionReportDtl>();
		
		List<Object>obj=consumptionHdrRepository.getConsumptionDtlReport( reasonid,  fromdate, todate);

		 for(int i=0;i<obj.size();i++)
		 {
		
			 Object[] objAray = (Object[]) obj.get(i);
			 ConsumptionReportDtl report=new ConsumptionReportDtl();
			 report.setItemId((String) objAray[0]);
			 report.setBatch((String) objAray[1]);
			 report.setQty((Double) objAray[2]);
			 report.setMrp((Double) objAray[3]);
			 report.setAmount((Double) objAray[4]);
			 report.setUnitId((String) objAray[5]);
			 consumptionReportDtlList.add(report);
		
		 }
		
		return consumptionReportDtlList;
	}
	@Override
	public Double getConsumptionAmount(String branchcode, Date fromdate, Date todate, String companymstid
			) {
		
		Double consumptionAmount=0.0;
		consumptionAmount=consumptionHdrRepository.getConsumptionAmount(branchcode,fromdate,todate,companymstid );
		return consumptionAmount;
	}
	@Override
	public List<ConsumptionHdrReport> getConsumptionReason(String branchcode, Date fromdate, Date todate,
			String companymstid) {
		List<ConsumptionHdrReport> consumptionHdrReportList=new ArrayList<ConsumptionHdrReport>();
		List<String> reasonid=consumptionHdrRepository.getConsumptionReason( branchcode,  fromdate,  todate,
				 companymstid);
		for(int i=0;i<reasonid.size();i++) {
			ConsumptionHdrReport report =new ConsumptionHdrReport();
			Optional<ConsumptionReasonMst> consumptionReasonMstOpt = consumptionReasonMstRepository.findById(reasonid.get(i));
			report.setReason(consumptionReasonMstOpt.get().getReason());
			consumptionHdrReportList.add(report);
		}
		   
		
		
		return consumptionHdrReportList;
	}
	@Override
	public List<ConsumptionReport> getdepartmentWiseConsumption(String department, String[] category, String branchcode,
			Date strfromdate, Date strtodate,CompanyMst companyMst) {

		List<ConsumptionReport> consumptionReportList = new ArrayList();
		for(String categoryName:category) {
			CategoryMst categoryMst=categoryMstRepo.findByCategoryNameAndCompanyMst(categoryName,companyMst);
			List<Object> obj = consumptionHdrRepository.getDepertmentwiseConsupmtion(department,categoryMst.getId(),branchcode,
					strfromdate,strtodate,companyMst);

			 for(int i=0;i<obj.size();i++)
			 {
			
				 Object[] objAray = (Object[]) obj.get(i);
				 ConsumptionReport report=new ConsumptionReport();
				 report.setAmount((Double) objAray[4]);
				 report.setItemName((String) objAray[1]);
				 report.setMrp((Double) objAray[3]);
				 report.setQty((Double) objAray[2]);
				 report.setReason((String) objAray[0]);
				 report.setUnit((String) objAray[7]);
				 report.setVoucherDate((Date) objAray[6]);
				 report.setCategoryName((String) objAray[8]);
				 consumptionReportList.add(report);
			 }
		}
		return consumptionReportList;
	}
	@Override
	public List<ConsumptionReport> getItemWiseConsumption(String department, String[] array, String branchcode,
			Date fromdate, Date todate, CompanyMst companyMst) {
		List<ConsumptionReport> consumptionReportList = new ArrayList();
		for(String itemname:array) {
			Optional<ItemMst> itemMstOpt = itemMstRepository.findByItemNameAndCompanyMst(itemname,companyMst);
			ItemMst itemMst = itemMstOpt.get();
			List<Object> obj = consumptionHdrRepository.getItemWiseConsumption(department,itemMst.getId(),branchcode,
					fromdate,todate,companyMst);

			 for(int i=0;i<obj.size();i++)
			 {
			
				 Object[] objAray = (Object[]) obj.get(i);
				 ConsumptionReport report=new ConsumptionReport();
				 report.setAmount((Double) objAray[4]);
				 report.setItemName((String) objAray[1]);
				 report.setMrp((Double) objAray[3]);
				 report.setQty((Double) objAray[2]);
				 report.setReason((String) objAray[0]);
				 report.setUnit((String) objAray[7]);
				 report.setVoucherDate((Date) objAray[6]);
				 report.setCategoryName((String) objAray[8]);
				 consumptionReportList.add(report);
			 }
		}
		return consumptionReportList;
	}
	

	
	@Override
	public List<ConsumptionReport> getConsumptionreportsummary(String branchcode, Date fromdate, Date todate) {
		
		List<ConsumptionReport> getConsumptionreportList= new ArrayList<ConsumptionReport>();
		List<Object> obj = consumptionHdrRepository.getConsumptionreportsummary( branchcode,  fromdate,  todate);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			ConsumptionReport consumptionReport = new ConsumptionReport();
			
		  consumptionReport.setVoucherDate((Date) objAray[0]);
		  consumptionReport.setVoucherNumber((String) objAray[1]);
		  consumptionReport.setAmount((Double) objAray[2]);
		  consumptionReport.setUserName((String) objAray[3]);
		  consumptionReport.setDepartment((String) objAray[4]);
		  
		  getConsumptionreportList.add(consumptionReport);
		  
		
		
	}
		return getConsumptionreportList;

	
	
	}
	
	//==================consumptiom detail report with category===================
	@Override
	public List<ConsumptionReport> getConsumptionDetail(CompanyMst companyMst,Date fromdate, Date todate, String[] array,String username){
		
		List<ConsumptionReport> getConsumptionreportList= new ArrayList<ConsumptionReport>();
		
		for (String categoryName : array) {
			CategoryMst categoryMst = categoryMstRepo.findByCategoryNameAndCompanyMst(categoryName, companyMst);
			
		List<Object> obj = consumptionHdrRepository.getConsumptionDetail(fromdate,  todate,categoryMst.getId());
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			ConsumptionReport consumptionDetailReport = new ConsumptionReport();
			
			consumptionDetailReport.setDepartment((String) objAray[0]);
			consumptionDetailReport.setVoucherNumber((String) objAray[1]);
			consumptionDetailReport.setDate((String) objAray[2].toString());
//			consumptionDetailReport.setUserName((String) objAray[3]);
			consumptionDetailReport.setItemName((String) objAray[3]);
			consumptionDetailReport.setCategoryName((String) objAray[4]);
			consumptionDetailReport.setBatch((String) objAray[5]);
			consumptionDetailReport.setExpiry((String) objAray[6].toString());
			consumptionDetailReport.setQty((Double) objAray[7]);
			consumptionDetailReport.setRate((Double) objAray[8]);
			consumptionDetailReport.setAmount((Double) objAray[9]);
			
			consumptionDetailReport.setUserName(username);
		  
			getConsumptionreportList.add(consumptionDetailReport);
		  
		
		 	}
		}
		return getConsumptionreportList;

	
	
	}
	
	//==================consumptiom detail report with out category===================
	@Override
	public List<ConsumptionReport> getConsumptionDetailWithoutCategory(CompanyMst companyMst,Date fromdate, Date todate,String username){
		
		List<ConsumptionReport> getConsumptionreportList= new ArrayList<ConsumptionReport>();
		
		List<Object> obj = consumptionHdrRepository.getConsumptionDetailWithoutCategory(fromdate,  todate);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			ConsumptionReport consumptionDetailReport = new ConsumptionReport();
			
			consumptionDetailReport.setDepartment((String) objAray[0]);
			consumptionDetailReport.setVoucherNumber((String) objAray[1]);
			consumptionDetailReport.setDate((String) objAray[2].toString());
			consumptionDetailReport.setUserName(username);
			consumptionDetailReport.setItemName((String) objAray[3]);
			consumptionDetailReport.setCategoryName((String) objAray[4]);
			consumptionDetailReport.setBatch((String) objAray[5]);
			consumptionDetailReport.setExpiry((String) objAray[6].toString());
			consumptionDetailReport.setQty((Double) objAray[7]);
			consumptionDetailReport.setRate((Double) objAray[8]);
			consumptionDetailReport.setAmount((Double) objAray[9]);
			
		  
			getConsumptionreportList.add(consumptionDetailReport);
		  
		
		 	}
		return getConsumptionreportList;

	
	
	}
	
	
	//==================consumptiom summary report with category===================
	@Override
	public List<ConsumptionReport> getConsumptionSummary(CompanyMst companyMst, Date fromdate, Date todate, String[] array,String username){
		
		List<ConsumptionReport> getConsumptionreportList= new ArrayList<ConsumptionReport>();
		
		for (String categoryName : array) {
			CategoryMst categoryMst = categoryMstRepo.findByCategoryNameAndCompanyMst(categoryName, companyMst);
			
		List<Object> obj = consumptionHdrRepository.getConsumptionSummary(fromdate,  todate,categoryMst.getId());
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			ConsumptionReport consumptionSummaryReport = new ConsumptionReport();
			
			consumptionSummaryReport.setDepartment((String) objAray[3]);
			consumptionSummaryReport.setVoucherNumber((String) objAray[1]);
			consumptionSummaryReport.setDate((String) objAray[0].toString());
			consumptionSummaryReport.setUserName(username);
			consumptionSummaryReport.setAmount((Double) objAray[2]);
			
		  
			getConsumptionreportList.add(consumptionSummaryReport);
		  
		
		 	}
		}
		return getConsumptionreportList;

	
	
	}
	
	
	//==================consumptiom summary report with out category===================
	@Override
	public List<ConsumptionReport> getConsumptionSummaryWithoutCategory(CompanyMst companyMst, Date fromdate, Date todate, String username){
		
		List<ConsumptionReport> getConsumptionreportList= new ArrayList<ConsumptionReport>();
		
		List<Object> obj = consumptionHdrRepository.getConsumptionSummaryWithoutCategory(fromdate,  todate);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			ConsumptionReport consumptionSummaryReport = new ConsumptionReport();
			
			consumptionSummaryReport.setDepartment((String) objAray[3]);
			consumptionSummaryReport.setVoucherNumber((String) objAray[1]);
			consumptionSummaryReport.setDate((String) objAray[0].toString());
			consumptionSummaryReport.setUserName(username);
			consumptionSummaryReport.setAmount((Double) objAray[2]);
			
		  
			getConsumptionreportList.add(consumptionSummaryReport);
		  
		
		 	}
		
		return getConsumptionreportList;

	
	
	}
	
}
