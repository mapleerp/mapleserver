package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.report.entity.StockTransferOutReport;

public interface StockTransferOutDtlService {
	
	public  List<Object> getStockTransfer(String batch, Date voucherDate);
	public    List<StockTransferOutDtl> getAllStockTransfer( );
	//
	//Service to fetch item order by existing serial-data
	
	public    List<StockTransferOutDtl> updateStocktransferOutDisplaySerial(String stockTransferOutHdr );

}
