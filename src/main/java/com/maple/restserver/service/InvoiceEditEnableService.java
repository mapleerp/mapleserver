package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public interface InvoiceEditEnableService {

	List<Object>findPaymentByCompanyMstIdAndVoucherDate(String companymstid,String branchcode,Date fdate,String userid);
}
