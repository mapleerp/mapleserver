package com.maple.restserver.service;

import org.springframework.stereotype.Component;

@Component
public interface BranchMstService {
	public String findByBranchName(String branchName);
}
