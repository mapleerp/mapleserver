package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.TaxReport;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;

@Transactional
@Service
public class TaxReportServiceImpl implements TaxReportService {

	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;
	@Override
	public List<TaxReport> getTaxReport(Date sdate, Date edate, CompanyMst companyMst, String branchCode) {
	
		List<TaxReport> taxReportList=new ArrayList<TaxReport>();
		
		
	List<Object>salesObjList=salesTransHdrRepository.salesTaxReport( sdate, edate,  companyMst, branchCode);
	for(int i=0;i<salesObjList.size();i++)
	 {

	Object[] objAray = (Object[]) salesObjList.get(i);
	TaxReport taxReport=new TaxReport();
System.out.print(salesObjList.size()+"sales object list size issssssssssssss");
		taxReport.setSalesTaxRate((Double) objAray[0]);
		taxReport.setTotalSaleTaxAmount((Double) objAray[1]);
		taxReport.setSalesCgstAmount((Double) objAray[2]);
		taxReport.setSalesSgstAmount((Double) objAray[3]);
		taxReport.setSalesIgstAmount((Double) objAray[4]);
     	taxReport.setSalesTaxAssesableAmount((Double) objAray[5]);
     	taxReport.setSalesCessAmount((Double) objAray[6]);
     	taxReport.setNetSalesAmount((Double) objAray[5]+(Double) objAray[1]+(Double) objAray[6]);
     	taxReportList.add(taxReport);
	 }
	

		/*List<Object>purchaseObjectList=purchaseHdrRepository.purchaseTaxReport( sdate, edate,  companyMst, branchCode);
		for(int j=0;j<purchaseObjectList.size();j++) {
			
			Object[] objectAray = (Object[]) purchaseObjectList.get(j);
			taxReport.setPurchaseTaxRate((Double) objectAray[0]);
			
			taxReport.setTotalPurchaseTaxAmount((Double) objectAray[1]);
			taxReport.setPurchaseTaxAssesableAmount((Double) objectAray[2]);
		
			taxReport.setPurchaseCessAmount((Double) objectAray[3]);
			taxReportList.add(taxReport);*/
		//}
		

		
		/*
		 * taxReport.setFinalCgstAmount(finalCgstAmount);
		 * taxReport.setFinalSgstAmount(finalSgstAmount);
		 * taxReport.setFinalTaxRate(finalTaxRate);
		 * taxReport.setFinalTaxAssesableAmount(finalTaxAssesableAmount);
		 * taxReport.setFinalIgstAmount(finalIgstAmount); taxReportList.add(taxReport);
		 */
		return taxReportList;
	}
	@Override
	public List<TaxReport> getPurchaseTaxReport(Date sdate, Date edate, CompanyMst companyMst, String branchCode) {
		
		

		List<TaxReport> taxReportList=new ArrayList<TaxReport>();
		
	
		List<Object>purchaseObjectList=purchaseHdrRepository.purchaseTaxReport( sdate, edate,  companyMst, branchCode);
		for(int j=0;j<purchaseObjectList.size();j++) {
		Object[] objectAray = (Object[]) purchaseObjectList.get(j);
		TaxReport taxReport =new TaxReport();
		System.out.print(purchaseObjectList.size()+"purchase list size issssssssssssssss");
		taxReport.setPurchaseTaxRate((Double) objectAray[0]);
		
		taxReport.setTotalPurchaseTaxAmount((Double) objectAray[1]);
		
		taxReport.setPurchaseSgstAmount((Double) objectAray[1]/2);
		taxReport.setPurchaseCgstAmount((Double) objectAray[1]/2);
		taxReport.setPurchaseTaxAssesableAmount((Double) objectAray[2]);
		taxReport.setPurchaseCessAmount((Double) objectAray[3]);
		taxReport.setNetPurchaseAmount((Double) objectAray[1]+(Double) objectAray[2]+(Double) objectAray[3]);
		
		taxReportList.add(taxReport);
		}
		/*
		 * taxReport.setFinalCgstAmount(finalCgstAmount);
		 * taxReport.setFinalSgstAmount(finalSgstAmount);
		 * taxReport.setFinalTaxRate(finalTaxRate);
		 * taxReport.setFinalTaxAssesableAmount(finalTaxAssesableAmount);
		 * taxReport.setFinalIgstAmount(finalIgstAmount); taxReportList.add(taxReport);
		 */
		
		return taxReportList;
	}

	
	
}
