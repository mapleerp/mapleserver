package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.PharmacyBrachStockMarginReport;

import com.maple.restserver.repository.ItemBatchDtlRepository;


@Service
@Transactional
public class PharmacyBranchStockMarginRptServiceImpl implements PharmacyBranchStockMarginRptService {
	
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;
	
	@Override
	public List<PharmacyBrachStockMarginReport> getPharmacyBranchStockMarginReport(Date fromDate, Date toDate, String branchcode ) {
		List<PharmacyBrachStockMarginReport>PharmacyBrachStockMarginReportList = new ArrayList<PharmacyBrachStockMarginReport>();
		List<Object> obj = itemBatchDtlRepository.getPharmacyBranchStockMarginReport(fromDate, toDate, branchcode);
		 for(int i=0;i<obj.size();i++)
		 {
			 
			Object[] objAray = (Object[]) obj.get(i);
			PharmacyBrachStockMarginReport pharmacyBrachStockMarginReport = new PharmacyBrachStockMarginReport();
			
		
			
			pharmacyBrachStockMarginReport.setAmount((Double)objAray[7]);
			
			pharmacyBrachStockMarginReport.setBatchCode((String)objAray[3]);
			pharmacyBrachStockMarginReport.setExpiryDate((Date)objAray[4]);
			pharmacyBrachStockMarginReport.setGroupName((String)objAray[0]);
			pharmacyBrachStockMarginReport.setItemCode((String)objAray[1]);
			pharmacyBrachStockMarginReport.setMarginRate((Double)objAray[10]);
			//pharmacyBrachStockMarginReport.setMarginValue((Double(objAray[7]-objAray[9])));
			pharmacyBrachStockMarginReport.setPurchaseRate((Double)objAray[8]);
			pharmacyBrachStockMarginReport.setPurchaseValue((Double)objAray[9]);
			pharmacyBrachStockMarginReport.setQuantity((Double)objAray[5]);
			pharmacyBrachStockMarginReport.setItemName((String)objAray[2]);
			pharmacyBrachStockMarginReport.setRate((Double)objAray[6]);
			Double amount=(Double) objAray[7];
			Double purchaseValue=(Double) objAray[9];
			Double marginValue =amount-purchaseValue;
			pharmacyBrachStockMarginReport.setMarginValue((Double)marginValue);
			
			PharmacyBrachStockMarginReportList.add(pharmacyBrachStockMarginReport);
		 }
		 return PharmacyBrachStockMarginReportList;
		 
}
	}
