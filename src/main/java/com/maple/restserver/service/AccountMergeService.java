package com.maple.restserver.service;

import org.springframework.stereotype.Service;

@Service
public interface AccountMergeService {


	String accountMerge(String fromAccountId,String toAccountId);
	
	public void DeleteAccountAndSaveToDeletedAccountClass(String accountId);
}
