package com.maple.restserver.service;

import java.util.ArrayList;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.OrgReceiptInvoice;
import com.maple.restserver.report.entity.ReceiptInvoice;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.utils.SystemSetting;
@Service
@Transactional
@Component

public class ReceiptInvoiceServiceImpl implements ReceiptInvoiceService{
	@Autowired
	ReceiptHdrRepository receiptHdrRepo;
	@Override
	public List<ReceiptInvoice> getReceiptByVoucherAndDate(String vouchernumber, Date date, String companymstid) {
		List< ReceiptInvoice> receiptReportList =   new ArrayList();
		 
		 List<Object> obj = receiptHdrRepo.receiptsInvoiceByVoucherAndDate(vouchernumber,  date, companymstid);
 
			 for(int i=0;i<obj.size();i++)
			 {
				 Object[] objAray = (Object[]) obj.get(i);
				 ReceiptInvoice receiptInvoice = new ReceiptInvoice();
				 
				 
				 receiptInvoice.setBranchName((String) objAray[0]);
				 receiptInvoice.setBranchAddress1(( String) objAray[1]);
				 receiptInvoice.setBranchPlace(( String) objAray[2]);
				 receiptInvoice.setBranchGst((String) objAray[3]);
				 receiptInvoice.setBranchTelNo((String) objAray[4]);
				 receiptInvoice.setVoucherNumber((String) objAray[5]);
				 receiptInvoice.setRecievedAmount(( Double) objAray[6]);
				 receiptInvoice.setVoucherDate(( Date) objAray[7]);
				 receiptInvoice.setAccount((String) objAray[8]);
				 receiptInvoice.setRemark((String) objAray[9]);
				 receiptInvoice.setInstrumentDate(( Date) objAray[10]);
				 receiptInvoice.setModeOfpayment((String) objAray[11]);
				 receiptInvoice.setInstrumentNo((String) objAray[12]);
			     receiptReportList.add(receiptInvoice);
				 
			 
			 }
			 return receiptReportList;

	}
	@Override
	public List<OrgReceiptInvoice> getReceiptByVoucherAndDateAndmember(String voucherNumber, Date date,
			String companymstid, String memberId) {
		List< OrgReceiptInvoice> receiptReportLists =   new ArrayList();
		java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate( date);
		 List<Object> obj = receiptHdrRepo.getReceiptByVoucherAndDateAndMemberId(voucherNumber, sqlDate, companymstid, memberId);
		 
		 
			 for(int i=0;i<obj.size();i++)
			 {
				 
				 
				 Object[] objAray = (Object[]) obj.get(i);
				 OrgReceiptInvoice receiptInvoice = new OrgReceiptInvoice();
				 
				 receiptInvoice.setBranchName((String) objAray[0]);
				 receiptInvoice.setBranchAddress(( String) objAray[1]);
				//receiptInvoice.setBranchPlace(( String) objAray[2]);
				 receiptInvoice.setBranchGst((String) objAray[3]);
				 receiptInvoice.setBranchTelNo((String) objAray[4]);
				 receiptInvoice.setVoucherNumber((String) objAray[5]);
				 receiptInvoice.setRecievedAmount(( Double) objAray[6]);
				 receiptInvoice.setVoucherDate((Date) objAray[7]);
				 receiptInvoice.setAccount((String) objAray[8]);
				 receiptInvoice.setRemark((String) objAray[9]);
				receiptInvoice.setInstrumentDate(( Date) objAray[10]);
				 receiptInvoice.setModeOfpayment((String) objAray[11]);
				 receiptInvoice.setMemberName((String) objAray[12]);
				 receiptInvoice.setFamilyName((String) objAray[13]);
				 receiptInvoice.setInstrumentNumber((String) objAray[14]);
					
				 receiptReportLists.add(receiptInvoice);
				 
			 
			 }
			 return receiptReportLists;

	
		
	}
	
	//version 3.1
	@Override
	public List<ReceiptInvoice> getReceiptInvoiceReport(Date sdate, Date edate, CompanyMst companyMst,
			String branchCode) {
		List< ReceiptInvoice> receiptReportList =   new ArrayList();
		 
		 List<Object> obj = receiptHdrRepo.getReceiptInvoiceReport(sdate,  edate, companyMst,branchCode);

			 for(int i=0;i<obj.size();i++)
			 {
				 Object[] objAray = (Object[]) obj.get(i);
				 ReceiptInvoice receiptInvoice = new ReceiptInvoice();
			
				 receiptInvoice.setBranchAddress1(( String) objAray[1]);
				 receiptInvoice.setBranchPlace(( String) objAray[2]);
				 receiptInvoice.setBranchGst((String) objAray[3]);
				 receiptInvoice.setBranchTelNo((String) objAray[4]);
				 receiptInvoice.setVoucherNumber((String) objAray[0]);
				 receiptInvoice.setVoucherDate(( Date) objAray[1]);
				 receiptInvoice.setRecievedAmount(( Double) objAray[2]);
				 receiptInvoice.setInstrumentDate(( Date) objAray[3]);
				 receiptInvoice.setInstrumentNo((String) objAray[4]);
				 receiptInvoice.setModeOfpayment((String) objAray[5]);
				 receiptInvoice.setBranchName((String) objAray[6]);
			     receiptReportList.add(receiptInvoice);
				 
			 
			 }
			 return receiptReportList;
	}
//version 3.1 end
}
