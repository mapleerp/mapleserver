package com.maple.restserver.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.report.entity.ShortExpiryReport;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.ItemBatchExpiryDtlRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.utils.SystemSetting;

import ch.qos.logback.core.util.Duration;

@Service
@Transactional
@Component
public class ShortExpiryServiceImpl implements ShortExpiryService {

	@Autowired
	CategoryMstRepository categoryMstRepository;
	
	@Autowired
	ItemMstRepository itemMstRepository;
	@Autowired
	ItemBatchExpiryDtlRepository itemBatchExpiryDtlRepository;
	@Override
	public List<ShortExpiryReport> getShortExpiry(CompanyMst companyMst, String branchcode,java.sql.Date currentDate) {
		
		List<Object>obj=itemBatchExpiryDtlRepository.getShortExpiryNew(companyMst,branchcode);

		List<ShortExpiryReport> shortExpiryReportList=new ArrayList<ShortExpiryReport>();
		
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ShortExpiryReport shortExpiry = new ShortExpiryReport();
			 shortExpiry.setItemName((String)objAray[0]);
			 
			 shortExpiry.setItemCode((String)objAray[1]);
			 shortExpiry.setRate((Double)objAray[2]);
			 shortExpiry.setAmount((Double)objAray[3]);
			 shortExpiry.setCategoryName((String)objAray[4]);
			 
			 shortExpiry.setQty((Double)objAray[5]);
			 shortExpiry.setBatch((String)objAray[6]);
			 shortExpiry.setExpiryDate((Date)objAray[7]);
			 shortExpiry.setUpdatedDate((Date)objAray[8]);
			 
//			 long days = shortExpiry.getExpiryDate().getTime() - currentDate.getTime();
//			 shortExpiry.setDaysToExpiry(Long.toString(days));
//			 LocalDateTime expiryDate = LocalDate.parse(shortExpiry.getExpiryDate());
//			 shortExpiry.setDaysToExpiry("10");
			 
//			 	Date date1 = SystemSetting.SqlDateToUtilDate((java.sql.Date) shortExpiry.getExpiryDate());
//			 	LocalDate date1tolocal = SystemSetting.utilToLocaDate(date1);
//			 	
//			 	
//			 	Date date2 = SystemSetting.SqlDateToUtilDate((java.sql.Date) currentDate);
//			 	LocalDate date2tolocal =  SystemSetting.utilToLocaDate(date2);
//			 
//			 	Long daysBetween = java.time.Duration.between(date1, date2).toDays();
			 
			 Long expDateInLong = shortExpiry.getExpiryDate().getTime();
			 Long currentDateInLong = currentDate.getTime();
			 
			 Long daysInLong = expDateInLong - currentDateInLong;
			 
			 int days = (int) (daysInLong/86400000);
			 String dayDifference = Integer.toString(days);
			 shortExpiry.setDaysToExpiry(dayDifference);
			 
			 shortExpiryReportList.add(shortExpiry);	
		 }
		return shortExpiryReportList;
	}
	@Override
	public List<ShortExpiryReport> getCatogarywiseShortExpiry(CompanyMst companyMst, String branchcode,
			String[] catogarylist) {
		List<ShortExpiryReport> shortExpiryReportList=new ArrayList<ShortExpiryReport>();
		for(String categoryName:catogarylist) {
			CategoryMst categoryMst=categoryMstRepository.findByCategoryNameAndCompanyMst(categoryName,companyMst);
		 List<Object>obj=itemBatchExpiryDtlRepository.getCatogarywiseShortExpiry(companyMst,branchcode,categoryMst.getId());
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ShortExpiryReport shortExpiry = new ShortExpiryReport();
			 shortExpiry.setItemName((String)objAray[0]);
			 shortExpiry.setQty((Double)objAray[1]);
			 shortExpiry.setBatch((String)objAray[2]);
			 shortExpiry.setExpiryDate((Date)objAray[3]);
			 shortExpiry.setUpdatedDate((Date)objAray[4]);
			 shortExpiry.setCategoryName((String)objAray[5]);
			 shortExpiryReportList.add(shortExpiry);	
		 } 
		}
		return shortExpiryReportList;
		
	}
	@Override
	public List<ShortExpiryReport> getItemWiseShortExpiry(CompanyMst companyMst, String branchcode, String [] itemlist) {
		List<ShortExpiryReport> shortExpiryReportList=new ArrayList<ShortExpiryReport>();
		
		for(String itemname :itemlist ) {
		ItemMst itemMst=itemMstRepository.findByItemName(itemname);
		List<Object>obj=itemBatchExpiryDtlRepository.getItemWiseShortExpiry(companyMst,branchcode,itemMst.getId());
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 ShortExpiryReport shortExpiry = new ShortExpiryReport();
			 shortExpiry.setItemName((String)objAray[0]);
			 shortExpiry.setQty((Double)objAray[1]);
			 shortExpiry.setBatch((String)objAray[2]);
			 shortExpiry.setExpiryDate((Date)objAray[3]);
			 shortExpiry.setUpdatedDate((Date)objAray[4]);
			 shortExpiryReportList.add(shortExpiry);	
		 } }
		return shortExpiryReportList;
	}
	

}
