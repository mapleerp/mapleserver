package com.maple.restserver.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.maple.util.TaxManager;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.TaxMst;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.TaxMstRepository;

@Service
@Transactional
@Component
public class TaxMstServiceImpl implements TaxMstService {

	@Autowired
	TaxMstRepository taxMstRepository;

	@Autowired
	TaxMstService taxMstService;

	@Autowired
	ItemMstRepository itemMstRepository;

	@Override
	public BigDecimal taxCalculator(Double taxpercent, Double mrp) {
		String taxId = "ASV";
		HashMap hm = new HashMap();
		TaxManager tm = new TaxManager();
		hm.put("TAXPERCENT", taxpercent);
		hm.put("MRP", mrp);

		return tm.getTax(taxId, hm);
	}

	@Override
	public SalesDtl calculateAndSetTaxToSalesDtl(SalesDtl salesDtl) {


		Double taxRate = 00.0;
		ItemMst item = itemMstRepository.findByItemName(salesDtl.getItemName());

		List<TaxMst> getTaxMst = taxMstRepository.getTaxByItemId(salesDtl.getItemId());
		if (getTaxMst.size() > 0) {
			for (TaxMst taxMst : getTaxMst) {
				if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
					salesDtl.setIgstTaxRate(taxMst.getTaxRate());
					salesDtl.setTaxRate(taxMst.getTaxRate());
				}
				if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
					salesDtl.setCgstTaxRate(taxMst.getTaxRate());
				}
				if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
					salesDtl.setSgstTaxRate(taxMst.getTaxRate());
				}
				if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
					salesDtl.setCessRate(taxMst.getTaxRate());
				}
				if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
					salesDtl.setAddCessRate(taxMst.getTaxRate());
				}
			}

			Double rateBeforeTax = (100 * salesDtl.getMrp())
					/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate());
			salesDtl.setRate(rateBeforeTax);

			BigDecimal igstAmount = taxMstService.taxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
			salesDtl.setIgstAmount(igstAmount.doubleValue() * salesDtl.getQty());

			BigDecimal CgstAmount = taxMstService.taxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
			salesDtl.setCgstAmount(CgstAmount.doubleValue() * salesDtl.getQty());

			BigDecimal SgstAmount = taxMstService.taxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
			salesDtl.setSgstAmount(SgstAmount.doubleValue() * salesDtl.getQty());

			BigDecimal cessAmount = taxMstService.taxCalculator(salesDtl.getCessRate(), rateBeforeTax);
			salesDtl.setCessAmount(cessAmount.doubleValue() * salesDtl.getQty());

			BigDecimal adcessAmount = taxMstService.taxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
			salesDtl.setAddCessAmount(adcessAmount.doubleValue() * salesDtl.getQty());

		} else {
			taxRate = item.getTaxRate();
			salesDtl.setTaxRate(taxRate);
			Double rateBeforeTax = (100 * salesDtl.getMrp()) / (100 + taxRate);
			salesDtl.setRate(rateBeforeTax);

			double cessRate = item.getCess();
			double cessAmount = 0.0;
			if (cessRate > 0) {

				rateBeforeTax = (100 * salesDtl.getMrp()) / (100 + taxRate + item.getCess());
				salesDtl.setRate(rateBeforeTax);
				cessAmount = salesDtl.getQty() * salesDtl.getRate() * cessRate / 100;
			}

			salesDtl.setCessRate(cessRate);

			salesDtl.setCessAmount(cessAmount);

			salesDtl.setSgstTaxRate(taxRate / 2);

			salesDtl.setCgstTaxRate(taxRate / 2);

			salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

			salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);
		}

		salesDtl.setAmount(salesDtl.getQty() * salesDtl.getMrp());
		discountCalculation(salesDtl);
		return salesDtl;
	}

	private void discountCalculation(SalesDtl salesDtl) {
		if (null == salesDtl.getSalesTransHdr().getAccountHeads().getCustomerDiscount()) {
			salesDtl.getSalesTransHdr().getAccountHeads().setCustomerDiscount(0.0);
		}

		if (salesDtl.getSalesTransHdr().getAccountHeads().getCustomerDiscount() > 0) {
			Double discount = 0.0;
			discount = salesDtl.getMrp()
					- (salesDtl.getMrp() * salesDtl.getSalesTransHdr().getAccountHeads().getCustomerDiscount()) / 100;
			salesDtl.setListPrice(discount);
		}
		// if Discount

		// Calculate discount on base price

		Double rateBeforeTax = (100 * salesDtl.getMrp()) / (100 + salesDtl.getTaxRate());

		ItemMst itemMst = itemMstRepository.getById(salesDtl.getItemId());

		if (null != salesDtl.getSalesTransHdr().getAccountHeads().getDiscountProperty()) {

			if (salesDtl.getSalesTransHdr().getAccountHeads().getDiscountProperty()
					.equalsIgnoreCase("ON BASIS OF BASE PRICE")) {
				calcDiscountOnBasePrice(salesDtl, salesDtl.getSalesTransHdr(), rateBeforeTax, itemMst,
						salesDtl.getMrp(), salesDtl.getTaxRate());

			}
			if (salesDtl.getSalesTransHdr().getAccountHeads().getDiscountProperty()
					.equalsIgnoreCase("ON BASIS OF MRP")) {

				calcDiscountOnMRP(salesDtl, salesDtl.getSalesTransHdr(), rateBeforeTax, itemMst, salesDtl.getMrp(),
						salesDtl.getTaxRate());
			}
			if (salesDtl.getSalesTransHdr().getAccountHeads().getDiscountProperty()
					.equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX")) {
				ambrossiaDiscount(salesDtl, salesDtl.getSalesTransHdr(), rateBeforeTax, itemMst, salesDtl.getMrp(),
						salesDtl.getTaxRate());
			}
		} else {

			double cessAmount = 0.0;
			double cessRate = 0.0;
			salesDtl.setRate(rateBeforeTax);
			if (salesDtl.getSalesTransHdr().getSalesMode().equalsIgnoreCase("B2C")) {
				if (itemMst.getCess() > 0) {
					cessRate = itemMst.getCess();
					rateBeforeTax = (100 * salesDtl.getMrp()) / (100 + salesDtl.getTaxRate() + itemMst.getCess());

					salesDtl.setRate(rateBeforeTax);
					cessAmount = salesDtl.getQty() * salesDtl.getRate() * itemMst.getCess() / 100;
				} else {
					cessAmount = 0.0;
					cessRate = 0.0;
				}
				salesDtl.setRate(rateBeforeTax);
				salesDtl.setCessRate(cessRate);
				salesDtl.setCessAmount(cessAmount);
				salesDtl.setRateBeforeDiscount(rateBeforeTax);
			}

			double sgstTaxRate = salesDtl.getTaxRate() / 2;
			double cgstTaxRate = salesDtl.getTaxRate() / 2;
			salesDtl.setCgstTaxRate(cgstTaxRate);

			salesDtl.setSgstTaxRate(sgstTaxRate);
			String companyState = salesDtl.getSalesTransHdr().getCompanyMst().getState();
			String customerState = "KERALA";
			try {
				customerState = salesDtl.getSalesTransHdr().getAccountHeads().getCustomerState();
			} catch (Exception e) {
//							logger.info(e.toString());

			}

			if (null == customerState) {
				customerState = "KERALA";
			}

			if (null == companyState) {
				companyState = "KERALA";
			}

			if (customerState.equalsIgnoreCase(companyState)) {
				salesDtl.setSgstTaxRate(salesDtl.getTaxRate() / 2);

				salesDtl.setCgstTaxRate(salesDtl.getTaxRate() / 2);

				Double cgstAmt = 0.0, sgstAmt = 0.0;
				cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
				BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
				bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				salesDtl.setCgstAmount(bdCgstAmt.doubleValue());
				sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
				BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
				bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

				salesDtl.setIgstTaxRate(0.0);
				salesDtl.setIgstAmount(0.0);

			} else {
				salesDtl.setSgstTaxRate(0.0);

				salesDtl.setCgstTaxRate(0.0);

				salesDtl.setCgstAmount(0.0);

				salesDtl.setSgstAmount(0.0);

				salesDtl.setIgstTaxRate(salesDtl.getTaxRate());
				salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

			}
		}

	}

	private void ambrossiaDiscount(SalesDtl salesDtl, SalesTransHdr salesTransHdr, Double rateBeforeTax,
			ItemMst itemMst, Double mrp, Double taxRate) {

		System.out.print("mrpRateIncludingTax" + salesDtl.getMrp());

		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			double discountAmount = (salesDtl.getMrp() * salesTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
			BigDecimal BrateAfterDiscount = new BigDecimal(salesDtl.getMrp() - discountAmount);

			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			System.out.println("BrateAfterDiscount*****************" + BrateAfterDiscount);
			System.out.println("discountAmount*****************" + discountAmount);

			salesDtl.setDiscount(discountAmount);

			salesDtl.setRateBeforeDiscount(rateBeforeTax);

			double newRate = BrateAfterDiscount.doubleValue();
			BigDecimal BnewRate = new BigDecimal(newRate);
			BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal BrateBeforeTax = new BigDecimal((newRate * 100) / (100 + taxRate));

			BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			salesDtl.setRate(BrateBeforeTax.doubleValue());

		} else {
			salesDtl.setRate(rateBeforeTax);
			salesDtl.setRateBeforeDiscount(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (itemMst.getCess() > 0) {
				cessRate = itemMst.getCess();

				double discountAmount = (salesDtl.getMrp() * salesTransHdr.getAccountHeads().getCustomerDiscount())
						/ 100;
				BigDecimal BrateAfterDiscount = new BigDecimal(salesDtl.getMrp() - discountAmount);

				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				salesDtl.setDiscount(discountAmount);

				double newRate = BrateAfterDiscount.doubleValue();
				BigDecimal BnewRate = new BigDecimal(newRate);
				BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				BigDecimal BrateBeforeTax = new BigDecimal((newRate * 100) / (100 + taxRate + cessRate));

				BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				salesDtl.setRate(BrateBeforeTax.doubleValue());

				System.out.println("rateBeforeTax---------" + BrateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * itemMst.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
		salesDtl.setRateBeforeDiscount(rateBeforeTax);

	}

	private void calcDiscountOnMRP(SalesDtl salesDtl, SalesTransHdr salesTransHdr, Double rateBeforeTax,
			ItemMst itemMst, Double mrp, Double taxRate) {

		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * salesDtl.getMrp())
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);

			BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BnewrateBeforeTax.doubleValue());
			salesDtl.setRateBeforeDiscount(rateBeforeTax);

		} else {
			salesDtl.setRate(rateBeforeTax);
			salesDtl.setRateBeforeDiscount(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (itemMst.getCess() > 0) {
				cessRate = itemMst.getCess();

				rateBeforeTax = (100 * salesDtl.getMrp()) / (100 + taxRate + itemMst.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * salesDtl.getMrp())
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

					BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				} else {
					salesDtl.setRate(rateBeforeTax);
					salesDtl.setRateBeforeDiscount(rateBeforeTax);
				}

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * itemMst.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
		salesDtl.setRateBeforeDiscount(rateBeforeTax);

	}

	private void calcDiscountOnBasePrice(SalesDtl salesDtl, SalesTransHdr salesTransHdr, Double rateBeforeTax,
			ItemMst item, Double mrpRateIncludingTax, double taxRate) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * rateBeforeTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BrateAfterDiscount.doubleValue());
			BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRateBeforeDiscount(rateBeforeTax);

		} else {
			salesDtl.setRate(rateBeforeTax);
			salesDtl.setRateBeforeDiscount(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				} else {
					salesDtl.setRate(rateBeforeTax);
					salesDtl.setRateBeforeDiscount(rateBeforeTax);
				}

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
		salesDtl.setRateBeforeDiscount(rateBeforeTax);

	}

}
