package com.maple.restserver.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
@Service
public class MultiUnitConversionServiceImpl implements MultiUnitConversionService {

	boolean recurssionOff = false;
	@Autowired
	MultiUnitMstRepository multiUnitMstRepository;
	
	@Autowired
	ItemMstRepository  itemMstRepository;
	
	@Override
	public double getConvertionQty(String companyMstId, String itemId, String sourceUnit, String targetUnit,
			double sourceQty)throws ResourceNotFoundException {
		Optional<ItemMst> itemMstOpt=itemMstRepository.findById(itemId);
		
		if (recurssionOff) {
			return sourceQty;
		}
		
//		if(null==targetUnit) {
//			throw new ResourceNotFoundException("Multi unit conversion not found for item "+itemMstOpt.get().getItemName());
//		}
//		
//		
//		if(null==sourceUnit) {
//			throw new ResourceNotFoundException("Multi unit conversion not found for item "+itemMstOpt.get().getItemName());
//		}
		
		if(sourceUnit.equalsIgnoreCase(targetUnit)) {
			return  sourceQty;
			
		}
		System.out.println("ITEM IDDDDDDDDDDDDDDDDDDDDDD"+itemId);
		
		MultiUnitMst multiUnitMstList = multiUnitMstRepository.findByCompanyMstIdAndItemIdAndUnit1(companyMstId, itemId,
				sourceUnit);

		
		if(null==multiUnitMstList) {
			throw new ResourceNotFoundException("Multi unit conversion not found for item "+itemMstOpt.get().getItemName());
		}
		
		while (!multiUnitMstList.getUnit2().equalsIgnoreCase(targetUnit)) {

			if (recurssionOff) {
				break;
			}
			sourceUnit = multiUnitMstList.getUnit2();

			sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();

//			if(null==targetUnit) {
//				throw new ResourceNotFoundException("Multi unit conversion not found for item "+itemMstOpt.get().getItemName());
//			}
			
			getConvertionQty(companyMstId, itemId, sourceUnit, targetUnit, sourceQty);

		}
		sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();
		recurssionOff = true;
		// sourceQty = sourceQty *
		// multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
		return sourceQty;
	}
}
