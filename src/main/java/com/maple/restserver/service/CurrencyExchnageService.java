package com.maple.restserver.service;

public interface CurrencyExchnageService {

	public Double getCurrencyExchange(String fromCurrencyName , 
			String toCurrencyName, Double inQty, String companyMstId);
	
	
	
	
}
