package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.RawMaterialIssueHdr;
import com.maple.restserver.report.entity.RawMaterialIssueReport;
import com.maple.restserver.report.entity.ReceiptInvoice;
import com.maple.restserver.repository.RawMaterialReturnHdrRepository;

@Service
@Transactional
@Component
public class RawMaterialIssueImpl implements RawMaterialIssueService {

	@Autowired
	RawMaterialReturnHdrRepository rawMaterialReturnHdrRepo;
	
	@Override
	public List<RawMaterialIssueReport> findByRawMaterialVoucher(String voucherNumber, Date vDate, String companyid) {
		List< RawMaterialIssueReport> RawMaterialIssueReportList =   new ArrayList();
		List<Object> obj = rawMaterialReturnHdrRepo.rawMaterialByVoucherNoAndDate(voucherNumber,  vDate, companyid);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			RawMaterialIssueReport rawMaterialIssue = new RawMaterialIssueReport();
			rawMaterialIssue.setItemName((String) objAray[2]);
			rawMaterialIssue.setUnitName((String) objAray[3]);
			rawMaterialIssue.setBatch((String) objAray[4]);
			rawMaterialIssue.setQty((Double)objAray[5]);
			rawMaterialIssue.setVoucherNumber((String) objAray[0]);
			rawMaterialIssue.setVoucherDate((Date) objAray[1]);
			RawMaterialIssueReportList.add(rawMaterialIssue);
		 }
			 
		 
		return RawMaterialIssueReportList;
	}

	@Override
	public List<RawMaterialIssueReport> rawMaterialIssueReportbetweenDate(String companymstid, String branchcode,
			Date fDate, Date tDate) {
		List< RawMaterialIssueReport> RawMaterialIssueReportList =   new ArrayList();
		List<Object> obj = rawMaterialReturnHdrRepo.rawMaterialIssueBetweenDate(companymstid,  branchcode, fDate,tDate);
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 RawMaterialIssueReport rawMaterialIssue = new RawMaterialIssueReport();
			 rawMaterialIssue.setItemName((String) objAray[2]);
			 rawMaterialIssue.setUnitName((String) objAray[3]);
			rawMaterialIssue.setBatch((String) objAray[4]);
			rawMaterialIssue.setQty((Double)objAray[5]);
			rawMaterialIssue.setVoucherNumber((String) objAray[0]);
			rawMaterialIssue.setVoucherDate((Date) objAray[1]);
			RawMaterialIssueReportList.add(rawMaterialIssue);
		 }
		return RawMaterialIssueReportList;
	}

}
