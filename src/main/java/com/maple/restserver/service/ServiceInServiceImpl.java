package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ServiceItemMst;
import com.maple.restserver.report.entity.ServiceInReport;
import com.maple.restserver.report.entity.StockValueReports;
import com.maple.restserver.repository.ServiceInDtlRepository;

 
@Service
@Transactional
public class ServiceInServiceImpl implements ServiceInService{
	
	
	@Autowired
	ServiceInDtlRepository serviceInDtlRepo;

	@Override
	public List<ServiceInReport> findByCompanyMstAndBetweenDate(CompanyMst companyMst, Date fromDate, Date toDate) {
		List<ServiceInReport> serivceInList = new ArrayList<>();
		
		List<Object> objList = serviceInDtlRepo.findByCompanyMstAndBetweenDate(companyMst,fromDate,toDate);
		
		
		for(int i=0; i<objList.size();i++)
		{
			
			Object[] objAray = (Object[]) objList.get(i);
			ServiceInReport serviceInReport = new ServiceInReport();
			
			serviceInReport.setQty((Double) objAray[0]);
			serviceInReport.setModel((String) objAray[1]);
			serviceInReport.setItemName((String) objAray[2]);
			serviceInReport.setUnderWarranty((String) objAray[3]);
			serviceInReport.setObservation((String) objAray[4]);
			serviceInReport.setComplaints((String) objAray[5]);
			serviceInReport.setCustomer((String) objAray[6]);
			serviceInReport.setProductName((String) objAray[7]);
			serviceInReport.setBrandName((String) objAray[8]);
			serviceInReport.setVoucherDate((Date) objAray[9]);
			serviceInReport.setSerialId((String) objAray[10]);
			
			serivceInList.add(serviceInReport);
		}
		return serivceInList;
	}

	@Override
	public List<StockValueReports> findByServiceOrItemProfit(CompanyMst companyMst, Date sdate, Date edate, String type) {
		
		List<StockValueReports> profitList = new ArrayList<>();

		List<Object> objList = serviceInDtlRepo.findByServiceOrItemProfit(companyMst,sdate,edate,type);
	
		for(int i=0; i<objList.size(); i++)
		{
			Object[] objArray = (Object[]) objList.get(i);
			StockValueReports stockValueReports = new StockValueReports();
			stockValueReports.setClosingQty((Double) objArray[0]);
			stockValueReports.setClosingValue((Double) objArray[1]);
			stockValueReports.setItemName((String) objArray[2]);
			stockValueReports.setDicount((Double) objArray[5]);
			stockValueReports.setMrp((Double) objArray[4]);
			stockValueReports.setCostprice((Double) objArray[3]);
			
			profitList.add(stockValueReports);
		}
		return profitList;
	
	}

}
