package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.report.entity.PaymentVoucher;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.repository.PaymentHdrRepository;
 
@Service
@Transactional
@Component
public class PaymentHdrImpl implements PaymentHdrService{

	@Autowired
	PaymentHdrRepository paymentHdrRepo;
	
 
	
	@Override
	public List<Object> getDailyPayment(String voucherNumber, Date voucherDate) 
	{
	
		 List<Object> obj =  paymentHdrRepo.getDailyPayment(voucherNumber, voucherDate);
			
			return obj;
	}



	@Override
	public List<PaymentVoucher> getPaymentHdr() {
 
		 
		 List<PaymentVoucher> paymentReportList =   new ArrayList();
		 
		 
		 List<Object> obj =   paymentHdrRepo.getAllDailyPayments();
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 PaymentVoucher paymentVoucher = new PaymentVoucher();
		 	 paymentVoucher.setBranchName((String)objAray[0]);
			 paymentVoucher.setVoucherNO((String)objAray[1]);
			 paymentVoucher.setVoucherDate(( Date)objAray[2]);
		    paymentVoucher.setAccountName((String)objAray[3]);
		    paymentVoucher.setAmount(( Double)objAray[4]);
			 paymentVoucher.setRemark((String)objAray[5]);
			

			 paymentReportList.add(paymentVoucher);	
		 }
		return paymentReportList;
 
	}


	
	
	
	
	

}
