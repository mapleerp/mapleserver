package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.StockTransferMessageEntity;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.StockTransferOutDtlRepository;
import com.maple.restserver.repository.StockTransferOutHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;

@Component
public class StockTransferOutServiceImpl implements StockTransferOutService {
	
	@Autowired
	StockTransferOutHdrRepository stockTranOutHdrRepo;
	
	@Autowired
	StockTransferOutDtlRepository stockTransferOutDtlRepo;
	
	@Autowired
	UnitMstRepository unitMstRepo;
	
	@Autowired
	ItemMstRepository itemMstRepository;
	
	@Autowired
	CategoryMstRepository categoryMstRepository;
	
	@Autowired
	PriceDefinitionRepository priceDefinitionRepository;
	
	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepository;
	@Override
	public StockTransferMessageEntity createMessageEntity(String id) {
		Optional<StockTransferOutHdr> hdrOpt = stockTranOutHdrRepo.findById(id);
		
		ArrayList<ItemMst> itemList = new ArrayList();
		ArrayList<CategoryMst> catList = new ArrayList();
		ArrayList<UnitMst> unitList = new ArrayList();
		ArrayList<PriceDefinition> priceList = new ArrayList();
		ArrayList<PriceDefenitionMst> priceDefMstList = new ArrayList();
		
		StockTransferOutHdr stockTransferOutHdr = hdrOpt.get();
		
		StockTransferMessageEntity stockTransMessageEntity = new StockTransferMessageEntity();
		stockTransMessageEntity.setStockTransferOutHdr(stockTransferOutHdr);
		ArrayList<StockTransferOutDtl> stockDtl = stockTransferOutDtlRepo
				.findByStockTransferOutHdrIdAndCompanyMstId(stockTransferOutHdr.getId(),
						stockTransferOutHdr.getCompanyMst().getId());
		stockTransMessageEntity.getStockTransferOutDtlList().addAll(stockDtl);
		stockTransMessageEntity.setItemCount(stockDtl.size());
	

			for (StockTransferOutDtl stockTran : stockDtl) {
				UnitMst unitMst = unitMstRepo.findById(stockTran.getUnitId()).get();
				unitList.add(unitMst);
				Optional<ItemMst> item = itemMstRepository.findById(stockTran.getItemId().getId());
//				jmsTemplate.convertAndSend("server.itemmst", item.get());
				itemList.add(item.get());
				
				if(null!=item.get().getCategoryId()) {
					Optional<CategoryMst> category = categoryMstRepository.findById(item.get().getCategoryId());
	//				jmsTemplate.convertAndSend("server.category", category.get());
					catList.add(category.get());
				}

				List<PriceDefinition> priceDefList = priceDefinitionRepository.findByItemId(stockTran.getItemId().getId());

				for (PriceDefinition priceDef : priceDefList) {

					Optional<PriceDefenitionMst> priceDefMst = priceDefinitionMstRepository
							.findById(priceDef.getPriceId());
					
					if(priceDefMst.isPresent()) {
						PriceDefenitionMst priceDefenitionMst = priceDefMst.get();
						priceDefMstList.add(priceDefenitionMst);
						priceList.add(priceDef);
					}
					// jmsTemplate.convertAndSend("server.pricedefinitonmst", priceDefenitionMst);
//					jmsTemplate.convertAndSend("server.pricedefiniton", priceDef);

				}

			}
			stockTransMessageEntity.getUnitMstList().addAll(unitList);
			stockTransMessageEntity.getCategoryList().addAll(catList);
			stockTransMessageEntity.getItemmstList().addAll(itemList);
			stockTransMessageEntity.getPriceDefinitionList().addAll(priceList);
			stockTransMessageEntity.getPriceDefinitionMstList().addAll(priceDefMstList);
		
		return stockTransMessageEntity;
	}

	
	
}
