package com.maple.restserver.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.StockVerificationDtl;
import com.maple.restserver.report.entity.StockVerificationReport;
import com.maple.restserver.repository.StockVerificationDtlRepository;

@Service
@Transactional
@Component
public class StockVerificationServiceImpl implements StockVerificationService{
	
	@Autowired
	StockVerificationDtlRepository stockVerificationDtlRepo;
	
	public String getOtp(Integer len)
	{ 
		System.out.println("Generating OTP using random() : "); 
		System.out.print("You OTP is : "); 
	  String strOTP =""; 
	 String l = len.toString();
	 len = l.length();
    // Using numeric values 
    String numbers = "0123456789"; 

    // Using random method 
    Random rndm_method = new Random(); 

    char[] otp = new char[len]; 

    for (int i = 0; i < len; i++) 
    { 
        // Use of charAt() method : to get character value 
        // Use of nextInt() as it is scanning the value as int 
        otp[i] = 
         numbers.charAt(rndm_method.nextInt(numbers.length())); 
        System.out.print( otp[i]);
        strOTP = strOTP + otp[i];
    } 
    	return strOTP; 
	}

	@Override
	public List<StockVerificationReport> findByStockVerificationByBetweenDate(Date fromdate, Date todate,String companymstid) {
		
		List<StockVerificationReport> stockList = new ArrayList<>();
		
		List<Object> objectList = stockVerificationDtlRepo.findStockVerificationByDate(fromdate,todate,companymstid);
	
		for(int i=0; i < objectList.size(); i++)
		{
			Object[] objctAray = (Object[]) objectList.get(i);
			
			StockVerificationReport stockVerificationReport = new StockVerificationReport();
			
			stockVerificationReport.setBranchCode((String) objctAray[4]);
			stockVerificationReport.setItemName((String) objctAray[0]);
			stockVerificationReport.setQty((Double) objctAray[2]);
			stockVerificationReport.setSystemQty((Double) objctAray[3]);
			stockVerificationReport.setUserName((String) objctAray[1]);
			
			stockList.add(stockVerificationReport);
		}
	
		return stockList;
	}
	
	
}
