package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.BatchwisePerformancereport;
@Component
public interface BatchwisePerformanceReportService {

	
List<BatchwisePerformancereport>	batchwisePerformanceReport(String companymstid,String branchcode,Date fDate,Date TDate,List<String>itemgroup);
}
