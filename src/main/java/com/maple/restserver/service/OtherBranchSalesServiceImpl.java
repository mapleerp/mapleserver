package com.maple.restserver.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.OtherBranchSalesDtl;
import com.maple.restserver.entity.OtherBranchSalesMessageEntity;
import com.maple.restserver.entity.OtherBranchSalesTransHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.report.entity.TaxSummaryMst;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.OtherBranchSalesDtlRepository;
import com.maple.restserver.repository.OtherBranchSalesTransHdrRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
public class OtherBranchSalesServiceImpl implements OtherBranchSalesService {
	
	private static final Logger logger = LoggerFactory.getLogger(MysqlDataTransferServiceImpl.class);
	
	

	@Autowired
	CategoryMstRepository categoryMstRepo;
	@Autowired
	ItemMstRepository itemMstRepository;
	@Autowired
	UnitMstRepository unitMstRepo;
	@Autowired
	BranchMstRepository branchMstRepo;
	@Autowired
	ItemMstRepository itemMstRepo;
	@Autowired
	OtherBranchSalesDtlRepository otherBranchSalesDtlRepo;
	@Autowired
	OtherBranchSalesTransHdrRepository otherBranchSalesTransHdr;
	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	
	@Autowired
	ExternalApi externalApi;
	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Value("${serverorclient}")
	private String serverorclient;

	EventBus eventBus = EventBusFactory.getEventBus();


	@Override
	public List<SalesInvoiceReport> getSalesInvoice(String companymstid, String voucherNumber, Date voucherDate) {
		List<Object> obj = otherBranchSalesTransHdr.salesInvoice(companymstid, voucherNumber, voucherDate);
		List<SalesInvoiceReport> salesInvoiceReportList = new ArrayList();
		Double taxableAmount = 0.0;
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
			salesInvoiceReport.setBranchName((String) objAray[0]);
			// salesInvoiceReport.setBranchName("TEST BRANCH NAME");
			salesInvoiceReport.setBranchAddress1((String) objAray[1]);
			salesInvoiceReport.setBranchAddress2((String) objAray[2]);
			salesInvoiceReport.setBranchPlace((String) objAray[3]);
			salesInvoiceReport.setBranchTelNo((String) objAray[4]);
			salesInvoiceReport.setBranchGst((String) objAray[5]);
			salesInvoiceReport.setBranchState((String) objAray[6]);
			salesInvoiceReport.setBranchEmail((String) objAray[7]);
			salesInvoiceReport.setBankName((String) objAray[8]);
			salesInvoiceReport.setAccountNumber((String) objAray[9]);
			salesInvoiceReport.setBankBranch((String) objAray[10]);
			salesInvoiceReport.setIfsc((String) objAray[11]);
			Date date = SystemSetting.UtilDateToSQLDate((java.util.Date) objAray[12]);
			salesInvoiceReport.setVoucherDate(date);
			salesInvoiceReport.setVoucherNumber((String) objAray[13]);
			salesInvoiceReport.setCustomerName((String) objAray[14]);
			salesInvoiceReport.setCustomerPlace((String) objAray[15]);
			salesInvoiceReport.setCustomerState((String) objAray[16]);
			salesInvoiceReport.setCustomerGst((String) objAray[17]);
			salesInvoiceReport.setItemName((String) objAray[18]);
			if (null != objAray[40]) {
				salesInvoiceReport.setMrp((Double) objAray[40]);
			} else {
				salesInvoiceReport.setMrp(0.0);
			}

			if (null != objAray[19]) {

				salesInvoiceReport.setTaxRate((Double) objAray[19]);
			} else {
				salesInvoiceReport.setTaxRate(0.0);
			}

			if (null != objAray[20]) {
				salesInvoiceReport.setHsnCode((String) objAray[20]);
			} else {
				salesInvoiceReport.setHsnCode("");

			}

			if (null != objAray[21]) {
				salesInvoiceReport.setRate((Double) objAray[21]);
			} else {

			}
			if (null != objAray[22]) {
				salesInvoiceReport.setQty((Double) objAray[22]);
			} else {
				salesInvoiceReport.setQty(0.0);
			}

			if (null != objAray[23]) {
				salesInvoiceReport.setUnitName((String) objAray[23]);
			} else {

			}

			if (null != objAray[24]) {

				salesInvoiceReport.setDiscount((Double) objAray[24]);
			} else {
				salesInvoiceReport.setDiscount(0.0);
			}

			if (null != objAray[25]) {
				salesInvoiceReport.setAmount((Double) objAray[25]);
			} else {
				salesInvoiceReport.setAmount(0.0);
			}

			if (null != objAray[26]) {
				salesInvoiceReport.setSgstAmount(((Double) objAray[26]));
			} else {
				salesInvoiceReport.setSgstAmount(0.0);
			}

			if (null != objAray[27]) {
				salesInvoiceReport.setCgstAmount(((Double) objAray[27]));
			} else {
				salesInvoiceReport.setCgstAmount(0.0);
			}

			if (null != objAray[28]) {
				salesInvoiceReport.setIgstAmount(((Double) objAray[28]));
			} else {
				salesInvoiceReport.setIgstAmount(0.0);
			}

			if (null != objAray[29]) {
				salesInvoiceReport.setSgstRate((Double) objAray[29]);
			} else {
				salesInvoiceReport.setSgstRate(0.0);

			}

			if (null != objAray[30]) {
				salesInvoiceReport.setCgstRate((Double) objAray[30]);
			} else {
				salesInvoiceReport.setCgstRate(0.0);
			}

			if (null != objAray[31]) {
				salesInvoiceReport.setIgstRate((Double) objAray[31]);
			} else {
				salesInvoiceReport.setIgstRate(0.0);
			}

			if (null != objAray[32]) {
				salesInvoiceReport.setCessRate((Double) objAray[32]);
			} else {
				salesInvoiceReport.setCessRate(0.0);
			}

			if (null != objAray[33]) {
				salesInvoiceReport.setCessAmount((Double) objAray[33]);
			} else {
				salesInvoiceReport.setCessAmount(0.0);
			}
			if (null != objAray[34]) {
				salesInvoiceReport.setBranchWebsite((String) objAray[34]);
			} else {
				salesInvoiceReport.setBranchWebsite("");
			}
			salesInvoiceReport.setCustomerPhone((String) objAray[35]);

			if (null != objAray[36]) {
				salesInvoiceReport.setTaxableAmount((Double) objAray[36]);
			} else {
				salesInvoiceReport.setTaxableAmount(0.0);
			}

			if (null != objAray[37]) {
				salesInvoiceReport.setDiscountPercent((String) objAray[37]);
			} else {
				salesInvoiceReport.setDiscountPercent("");
			}

			if (null != objAray[38]) {
				salesInvoiceReport.setInvoiceDiscount(-1 * (Double) objAray[38]);
			} else {
				salesInvoiceReport.setInvoiceDiscount(0.0);
			}

			salesInvoiceReport.setSalesMode((String) objAray[39]);

			taxableAmount = otherBranchSalesTransHdr.getTaxableAmount(voucherNumber, voucherDate, companymstid);
			salesInvoiceReport.setTaxableAmount(taxableAmount);

			/*
			 * List<Object> object=SalesTransHdrRepo.getTaxInvoiceCustomer(companymstid,
			 * voucherNumber, voucherDate); for (int k = 0; k < object.size(); k++) {
			 * Object[] objctAray = (Object[]) object.get(k);
			 * salesInvoiceReport.setLocalCustomerName((String) objctAray[0]);
			 * salesInvoiceReport.setAddressLine1((String) objctAray[1]);
			 * salesInvoiceReport.setAddressLine2((String) objctAray[2]);
			 * salesInvoiceReport.setLocalCustomerPhone((String) objctAray[3]); }
			 */
			salesInvoiceReport.setBatch((String) objAray[41]);
			salesInvoiceReport.setStandrdPrice((Double) objAray[42]);
			salesInvoiceReportList.add(salesInvoiceReport);

		}

		return salesInvoiceReportList;
	}

	@Override
	public List<TaxSummaryMst> getSalesInvoiceTax(String companymstid, String voucherNumber, Date date) {

		List<TaxSummaryMst> taxSummaryList = new ArrayList();

		List<Object> obj = otherBranchSalesTransHdr.getTaxSummary(companymstid, voucherNumber, date);

		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			TaxSummaryMst taxSummary = new TaxSummaryMst();
			Double taxPercent = ((Double) objAray[0]);
			taxSummary.setTaxAmount((Double) objAray[1]);

			Double sgstpercent = taxPercent;
			Double taxAmount = ((Double) objAray[1]);

			taxSummary.setTaxPercentage("GST" + sgstpercent + "%");

			taxSummary.setSgstAmount((Double) objAray[2]);
			taxSummary.setCgstAmount((Double) objAray[3]);
			taxSummary.setIgstAmount((Double) objAray[4]);

			taxSummary.setSgstTaxRate(((Double) objAray[5]));
			taxSummary.setCgstTaxRate(((Double) objAray[6]).doubleValue());
			taxSummary.setIgstTaxRate(((Double) objAray[7]).doubleValue());

			taxSummary.setAmount(((Double) objAray[8]).doubleValue());
			taxSummary.setTaxRate(((Double) objAray[0]).doubleValue());

			taxSummaryList.add(taxSummary);

		}
		return taxSummaryList;

	}

	@Override
	public List<TaxSummaryMst> getSumOfTaxAmounts(String companymstid, String vouchernumber, Date date) {
		List<TaxSummaryMst> taxSumReportList = new ArrayList();

		List<Object> obj = otherBranchSalesTransHdr.getSumOfTaxAmounts(companymstid, vouchernumber, date);

		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			TaxSummaryMst tSummary = new TaxSummaryMst();
			tSummary.setSumOfsgstAmount(((Double) objAray[0]));
			tSummary.setSumOfcgstAmount(((Double) objAray[1]));
			tSummary.setSumOfigstAmount(((Double) objAray[2]));
			taxSumReportList.add(tSummary);

		}
		return taxSumReportList;
	}

	@Override
	public List<SalesInvoiceReport> getTaxinvoiceCustomer(String companymstid, String voucherNumber, Date date) {

		List<SalesInvoiceReport> salesInvoiceReportList = new ArrayList();
//		List<Object> object = otherBranchSalesTransHdr.getTaxInvoiceCustomer(companymstid, voucherNumber, date);
		List<Object> objects = otherBranchSalesTransHdr.getTaxInvoiceGstCustomer(companymstid, voucherNumber, date);

		SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
		for (int l = 0; l < objects.size(); l++) {
			Object[] objectArray = (Object[]) objects.get(l);
			salesInvoiceReport.setCustomerName((String) objectArray[0]);
			salesInvoiceReport.setCustomerAddress((String) objectArray[1]);
			salesInvoiceReport.setCustomerPlace((String) objectArray[2]);
			salesInvoiceReport.setCustomerPhone((String) objectArray[3]);
			salesInvoiceReport.setCustomerGst((String) objectArray[4]);

		}
//		for (int k = 0; k < object.size(); k++) {
//			Object[] objctAray = (Object[]) object.get(k);
//
//			salesInvoiceReport.setLocalCustomerName((String) objctAray[0]);
//			salesInvoiceReport.setLocalCustomerAddres((String) objctAray[1]);
//			salesInvoiceReport.setLocalCustomerPlace((String) objctAray[3]);
//			salesInvoiceReport.setLocalCustomerState((String) objctAray[4]);
//			salesInvoiceReport.setLocalCustomerPhone((String) objctAray[2]);
//		}

		salesInvoiceReportList.add(salesInvoiceReport);

		return salesInvoiceReportList;
	}

	@Override
	public Double getCessAmount(String vouchernumber, java.util.Date date) {

		return otherBranchSalesTransHdr.getCessAmount(vouchernumber, date);

	}

	@Override
	public Double getNonTaxableAmount(String vouchernumber, java.util.Date date) {
		
		Double nonTaxableAmount = otherBranchSalesTransHdr.getNonTaxableAmount(vouchernumber, date);

		return nonTaxableAmount;
	}

	@Override
	public List<StockTransferOutReport> getOtherBranchStockTransfer(String vouchernumber, java.util.Date date) {
		List<StockTransferOutReport> stockTransferList = new ArrayList<StockTransferOutReport>();
		
		java.sql.Date sqldate = SystemSetting.UtilDateToSQLDate(date);
		List<OtherBranchSalesTransHdr> otherBranchSalesTransHdrList = otherBranchSalesTransHdr.findByVoucherNumberAndVoucherDate(vouchernumber, sqldate);

		List<OtherBranchSalesDtl> otherBranchSalesDtlList = otherBranchSalesDtlRepo.findByOtherBranchSalesTransHdr(otherBranchSalesTransHdrList.get(0).getId());
		for(int i =0 ;i<otherBranchSalesDtlList.size();i++)
		{
			StockTransferOutReport stockTransferOutReport = new StockTransferOutReport();
			stockTransferOutReport.setAmount(otherBranchSalesDtlList.get(i).getAmount());
			stockTransferOutReport.setQty(otherBranchSalesDtlList.get(i).getQty());
			stockTransferOutReport.setRate(otherBranchSalesDtlList.get(i).getRate());
			Optional<ItemMst> items = itemMstRepo.findById(otherBranchSalesDtlList.get(i).getItemId());
			stockTransferOutReport.setHsnCode(items.get().getHsnCode());
			stockTransferOutReport.setItemId(items.get().getItemName());
			stockTransferOutReport.setSlNo(i+1);
			Optional<UnitMst> unitMstOp = unitMstRepo.findById(otherBranchSalesDtlList.get(i).getUnitId());
			stockTransferOutReport.setUnitId(unitMstOp.get().getUnitName());
			Optional<BranchMst> branchMstOP= branchMstRepo.findById(otherBranchSalesTransHdrList.get(0).getAccountHeads().getId());
			stockTransferOutReport.setToBranch(branchMstOP.get().getBranchName());
			stockTransferOutReport.setBranchPlace(branchMstOP.get().getBranchPlace());
			stockTransferOutReport.setVoucherDate(date);
			stockTransferOutReport.setVoucherNumber(vouchernumber);
			stockTransferList.add(stockTransferOutReport);
		}

		return stockTransferList;
	}

	@Override
	public void resendOtherBranchSales(String hdrid) {
		
		Optional<SalesTransHdr> salesTransHdr = salesTransHdrRepository.findById(hdrid);
		
		if(!salesTransHdr.isPresent())
		{
			return;
		}
		
		OtherBranchSalesMessageEntity salesMessageEntity = new OtherBranchSalesMessageEntity();

		OtherBranchSalesTransHdr otherbranchSales = otherBranchSalesTransHdr.
				findBySalesTransHdrIdAndCompanyMst(salesTransHdr.get().getId(), salesTransHdr.get().getCompanyMst());
		
		salesMessageEntity.setOtherBranchSalesTransHdr(otherbranchSales);
		List<OtherBranchSalesDtl> salesDtl = otherBranchSalesDtlRepo.findByOtherBranchSalesTransHdr(otherbranchSales.getId());

		List<ItemMst> itemList = new ArrayList();
		List<CategoryMst> catList = new ArrayList();
		salesMessageEntity.getSalesDtlList().addAll(salesDtl);

		for (OtherBranchSalesDtl otherBranchSalesDtl : salesDtl) {
			ItemMst itemmst = itemMstRepository.findById(otherBranchSalesDtl.getItemId()).get();
			itemList.add(itemmst);
			CategoryMst category = categoryMstRepo.findById(itemmst.getCategoryId()).get();
			catList.add(category);
		}
		salesMessageEntity.getItemList().addAll(itemList);
		salesMessageEntity.getCategoryList().addAll(catList);
		Optional<BranchMst> branchMstOpt = branchMstRepo.findById(otherbranchSales.getBranchSaleCustomer());
		salesMessageEntity.setBranchMst(branchMstOpt.get());
		try {
			// ----------version 4.19 Surya
			
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

			System.out.println("Convert otherbranch sales");
			
			externalApi.sendOtherbranchSalesMessage(salesMessageEntity, salesTransHdr.get().getCompanyMst().getId());
			
			
//			jmsTemplate.convertAndSend("server.otherbranchsales", salesMessageEntity);

		} catch (Exception e) {
			
			LmsQueueMst lmsQueueMstSaved =  lmsQueueMstRepository.findBySourceObjectId(otherbranchSales.getId());
			
			System.out.println("Failed");
			logger.error(e.getMessage());

			
			if(null != lmsQueueMstSaved)
			{
				lmsQueueMstSaved.setPostedToServer("NO");

				lmsQueueMstSaved = lmsQueueMstRepository.saveAndFlush(lmsQueueMstSaved);

			} else {
				
		

		
			Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("companyid", otherbranchSales.getCompanyMst());

			variables.put("voucherNumber", otherbranchSales.getVoucherNumber());
			variables.put("voucherDate", otherbranchSales.getVoucherDate());
			variables.put("id", otherbranchSales.getId());

			variables.put("branchcode", otherbranchSales.getBranchCode());

			if (serverorclient.equalsIgnoreCase("REST")) {
				variables.put("REST", 1);
			} else {
				variables.put("REST", 0);
			}

			// variables.put("voucherDate", purchase.getVoucherDate());
			// variables.put("id",purchase.getId());
			variables.put("inet", 0);

			variables.put("WF", "forwardOtherBranchSales");

			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");

		

			System.out.println("LMS QUEUE insertion");

			eventBus.post(variables);		
			
			}
			
		}


	}

}
