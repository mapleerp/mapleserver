package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReceiptModeMst;
import com.maple.restserver.report.entity.ReceiptModeReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.repository.ReciptModeMstRepository;
import com.maple.restserver.repository.SaleOrderReceiptRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;

@Service
@Transactional
public class ReceiptModeMstServiceImpl implements ReceiptModeMstService {

	@Autowired
	SalesReceiptsRepository salesReceiptsRepository;

	@Autowired
	SaleOrderReceiptRepository saleOrderReceiptRepository;

	@Autowired
	ReceiptHdrRepository receiptHdrRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	ReciptModeMstRepository reciptModeMstRepository;

	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	@Autowired
	private VoucherNumberService voucherService;

	@Override
	public List<ReceiptModeReport> findTotalCardAmount(CompanyMst companyMst, String branchcode, Date sdate) {

		List<ReceiptModeReport> receiptModeReportList = new ArrayList<ReceiptModeReport>();

		List<Object> totalSalesReceipts = salesReceiptsRepository.totalSalesCardAmount(companyMst, branchcode, sdate);

		if (totalSalesReceipts.size() > 0) {
			for (int i = 0; i < totalSalesReceipts.size(); i++) {
				Object[] objAray = (Object[]) totalSalesReceipts.get(i);

				if (receiptModeReportList.size() == 0) {
					ReceiptModeReport receiptModeReport = new ReceiptModeReport();
					receiptModeReport.setAmount((Double) objAray[0]);
					receiptModeReport.setReceiptMode((String) objAray[1]);

					receiptModeReportList.add(receiptModeReport);
				} else {

					boolean receiptModeCheck = false;
					for (ReceiptModeReport receiptModeReport : receiptModeReportList) {
						if (receiptModeReport.getReceiptMode().equalsIgnoreCase((String) objAray[1])) {
							receiptModeReport.setAmount(receiptModeReport.getAmount() + (Double) objAray[0]);
							receiptModeReport.setReceiptMode((String) objAray[1]);

							receiptModeCheck = true;
							break;
						}
					}

					if (!receiptModeCheck) {
						ReceiptModeReport receiptModeReport = new ReceiptModeReport();
						receiptModeReport.setAmount((Double) objAray[0]);
						receiptModeReport.setReceiptMode((String) objAray[1]);

						receiptModeReportList.add(receiptModeReport);
					}
				}
			}
		}

		List<Object> totalSaleOrder = saleOrderReceiptRepository.totalSalesOrderCardAmount(companyMst, branchcode,
				sdate);

		if (totalSaleOrder.size() > 0) {
			for (int i = 0; i < totalSaleOrder.size(); i++) {
				Object[] objAray = (Object[]) totalSaleOrder.get(i);

				if (receiptModeReportList.size() == 0) {
					ReceiptModeReport receiptModeReport = new ReceiptModeReport();
					receiptModeReport.setSaleOrderAmount((Double) objAray[0]);
					receiptModeReport.setReceiptMode((String) objAray[1]);

					receiptModeReportList.add(receiptModeReport);
				} else {

					boolean receiptModeCheck = false;
					for (ReceiptModeReport receiptModeReport : receiptModeReportList) {
						if (receiptModeReport.getReceiptMode().equalsIgnoreCase((String) objAray[1])) {
							receiptModeReport.setSaleOrderAmount((Double) objAray[0]);
							receiptModeReport.setReceiptMode((String) objAray[1]);

							receiptModeCheck = true;
							break;
						}
					}

					if (!receiptModeCheck) {
						ReceiptModeReport receiptModeReport = new ReceiptModeReport();
						receiptModeReport.setSaleOrderAmount((Double) objAray[0]);
						receiptModeReport.setReceiptMode((String) objAray[1]);

						receiptModeReportList.add(receiptModeReport);
					}
				}
			}
		}

		List<Object> totalReceipts = receiptHdrRepository.totalReceiptCardAmount(companyMst, branchcode, sdate);

		if (totalReceipts.size() > 0) {
			for (int i = 0; i < totalReceipts.size(); i++) {
				Object[] objAray = (Object[]) totalReceipts.get(i);

				if (receiptModeReportList.size() == 0) {
					ReceiptModeReport receiptModeReport = new ReceiptModeReport();
					receiptModeReport.setAmount((Double) objAray[0]);
					receiptModeReport.setReceiptMode((String) objAray[1]);

					receiptModeReportList.add(receiptModeReport);
				} else {

					boolean receiptModeCheck = false;
					for (ReceiptModeReport receiptModeReport : receiptModeReportList) {
						if (receiptModeReport.getReceiptMode().equalsIgnoreCase((String) objAray[1])) {
							receiptModeReport.setAmount(receiptModeReport.getAmount() + (Double) objAray[0]);
							receiptModeReport.setReceiptMode((String) objAray[1]);

							receiptModeCheck = true;
							break;
						}
					}

					if (!receiptModeCheck) {
						ReceiptModeReport receiptModeReport = new ReceiptModeReport();
						receiptModeReport.setAmount((Double) objAray[0]);
						receiptModeReport.setReceiptMode((String) objAray[1]);

						receiptModeReportList.add(receiptModeReport);
					}
				}
			}
		}

		/*
		 * Now less the Receipts realized today and received before.
		 * cardPreviousReceiptRealized
		 */

		List<Object> totalPrvReceipts = salesReceiptsRepository.cardPreviousReceiptRealized(companyMst, branchcode,
				sdate);

		if (totalPrvReceipts.size() > 0) {
			for (int i = 0; i < totalPrvReceipts.size(); i++) {
				Object[] objAray = (Object[]) totalPrvReceipts.get(i);

				if (receiptModeReportList.size() == 0) {
					ReceiptModeReport receiptModeReport = new ReceiptModeReport();
					receiptModeReport.setPrevSaleOrderAmount((Double) objAray[0]);
					receiptModeReport.setReceiptMode((String) objAray[1]);

					// receiptModeReportList.add(receiptModeReport);
				} else {

					boolean receiptModeCheck = false;
					for (ReceiptModeReport receiptModeReport : receiptModeReportList) {
						if (receiptModeReport.getReceiptMode().equalsIgnoreCase((String) objAray[1])) {
							receiptModeReport.setPrevSaleOrderAmount((Double) objAray[0]);
							receiptModeReport.setReceiptMode((String) objAray[1]);

							receiptModeCheck = true;
							break;
						}
					}

					if (!receiptModeCheck) {
						ReceiptModeReport receiptModeReport = new ReceiptModeReport();
						receiptModeReport.setPrevSaleOrderAmount((Double) objAray[0]);
						receiptModeReport.setReceiptMode((String) objAray[1]);

						// receiptModeReportList.add(receiptModeReport);
					}
				}
			}
		}

		// -----------------prev so bt the receipt added by today

		List<Object> totalPrvSoAndTodaysReceipts = salesReceiptsRepository
				.cardPreviousSoRealizedWithTodayCard(companyMst, branchcode, sdate);

		if (totalPrvSoAndTodaysReceipts.size() > 0) {
			for (int i = 0; i < totalPrvSoAndTodaysReceipts.size(); i++) {
				Object[] objAray = (Object[]) totalPrvSoAndTodaysReceipts.get(i);

				if (receiptModeReportList.size() == 0) {
					ReceiptModeReport receiptModeReport = new ReceiptModeReport();
					receiptModeReport.setAmount((Double) objAray[0]);
					receiptModeReport.setReceiptMode((String) objAray[1]);

					receiptModeReportList.add(receiptModeReport);
				} else {

					boolean receiptModeCheck = false;
					for (ReceiptModeReport receiptModeReport : receiptModeReportList) {
						if (receiptModeReport.getReceiptMode().equalsIgnoreCase((String) objAray[1])) {
							receiptModeReport.setAmount(receiptModeReport.getAmount() + (Double) objAray[0]);
							receiptModeReport.setReceiptMode((String) objAray[1]);

							receiptModeCheck = true;
							break;
						}
					}

					if (!receiptModeCheck) {
						ReceiptModeReport receiptModeReport = new ReceiptModeReport();
						receiptModeReport.setAmount((Double) objAray[0]);
						receiptModeReport.setReceiptMode((String) objAray[1]);

						receiptModeReportList.add(receiptModeReport);
					}
				}
			}
		}

		return receiptModeReportList;
	}

	/////////////////////////// new service for
	/////////////////////////// credit,insurance,discount///////////////////////////////////
	@Override
	public String initializeNewReceiptMode(String companymstid,String branchcode) {

		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return "Failed";
		}
//		
		 

		ReceiptModeMst receiptModeMst = reciptModeMstRepository.findByCompanyMstAndReceiptMode(companyOpt.get(), "CREDIT");
		if (null == receiptModeMst) {

			receiptModeMst = new ReceiptModeMst();

			VoucherNumber voucherNo = voucherService.generateInvoice(branchcode, "RM");

			String vcNo = voucherNo.getCode();
			receiptModeMst.setId(vcNo);

			receiptModeMst.setReceiptMode("CREDIT");
//			profitAndLossConfigHdr.setBranchCode(branchcode);
			receiptModeMst.setCompanyMst(companyOpt.get());
//			
			reciptModeMstRepository.save(receiptModeMst);

			AccountHeads accountHeads = new AccountHeads();
			accountHeads.setAccountName("CREDIT");
			accountHeads.setGroupOnly("N");
			accountHeads.setCompanyMst(companyOpt.get());
			accountHeads.setId(vcNo);

			accountHeadsRepository.save(accountHeads);
		}

		receiptModeMst = reciptModeMstRepository.findByCompanyMstAndReceiptMode(companyOpt.get(), "DISCOUNT ALLOWED");
		if (null == receiptModeMst) {
			receiptModeMst = new ReceiptModeMst();

		

			VoucherNumber voucherNo = voucherService.generateInvoice(branchcode, "RM");

			String vcNo = voucherNo.getCode();
			receiptModeMst.setId(vcNo);

			receiptModeMst.setReceiptMode("DISCOUNT ALLOWED");
//			profitAndLossConfigHdr.setBranchCode(branchcode);
			receiptModeMst.setCompanyMst(companyOpt.get());
//			
			reciptModeMstRepository.save(receiptModeMst);

			AccountHeads accountHeads = new AccountHeads();
			accountHeads.setAccountName("DISCOUNT ALLOWED");
			accountHeads.setGroupOnly("N");
			accountHeads.setId(vcNo);
			accountHeads.setCompanyMst(companyOpt.get());


			accountHeadsRepository.save(accountHeads);
		}

		receiptModeMst = reciptModeMstRepository.findByCompanyMstAndReceiptMode(companyOpt.get(), "INSURANCE");
		if (null == receiptModeMst) {
			
			VoucherNumber voucherNo = voucherService.generateInvoice(branchcode, "RM");

			String vcNo = voucherNo.getCode();
			receiptModeMst = new ReceiptModeMst();

			receiptModeMst.setId(vcNo);

			receiptModeMst.setReceiptMode("INSURANCE");
//			profitAndLossConfigHdr.setBranchCode(branchcode);
			receiptModeMst.setCompanyMst(companyOpt.get());
//			
			reciptModeMstRepository.save(receiptModeMst);

			AccountHeads accountHeads = new AccountHeads();
			accountHeads.setAccountName("INSURANCE");
			accountHeads.setGroupOnly("N");
			accountHeads.setId(vcNo);
			accountHeads.setCompanyMst(companyOpt.get());


			accountHeadsRepository.save(accountHeads);
		}

		return "Success";
	}

}
