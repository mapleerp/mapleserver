package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DailyCashFromOrdersDtl;
import com.maple.restserver.entity.DailySalesSummary;
import com.maple.restserver.report.entity.CompanyDailySaleSummary;

@Component
public interface DayEndService {
	
	
	
	DailySalesSummary getDailySalesSumary(String branchCode ,Date reportDate);
	
	List<DailySalesSummary> getDailySalesSumaryAll() ;
	
	List<CompanyDailySaleSummary>  getCompanyDailySalesSumary(Date date,CompanyMst companyMst);
	List<DailyCashFromOrdersDtl> getDailyAdvanceFromOrders(String branchCode ,Date reportDate);
 
	String getOpeningBillNumber(String branchCode ,Date reportDate);
	
	String getClosingBillNumber(String branchCode, Date reportDate);
	
	
	String getTotalBillNumbers(String branchCode, Date reportDate);
	
	Double getOpeningPettyCash(String branchCode, Date reportDate);
	
	Double getCashReceived(String branchCode, Date reportDate);
	
	Double getExpensePaid(String branchCode, Date reportDate);
	
	Double getClosingBalance(String branchCode, Date reportDate);
	
	
	Double getCashSalesOrder(String branchCode, Date reportDate);
	Double getOnlineSalesOrder(String branchCode, Date reportDate);
	Double getDebitCardSalesOrder(String branchCode, Date reportDate);
	
	//ArrayList<String, Double> getCashReceivedFromOrderList(String branchCode, Date reportDate);
	Double getTotalOrders (String branchCode, Date reportDate);
	Double getB2BSalesTotal (String branchCode, Date reportDate);
	Double getUberSalesTotal (String branchCode, Date reportDate);
	Double getSwiggySalesTotal (String branchCode, Date reportDate);
	Double getZomatoSalesTotal (String branchCode, Date reportDate);
	Double getFoodPandaSalesTotal (String branchCode, Date reportDate);
	
	
	Double getCashSalesTotal (String branchCode, Date reportDate);
	Double getCreditSalesTotal (String branchCode, Date reportDate);
	Double getCreditCardSalesTotal (String branchCode, Date reportDate);
	Double getSodexoSalesTotal (String branchCode, Date reportDate);
	Double getPaytmSalesTotal (String branchCode, Date reportDate);
	Double getPreviousTotal (String branchCode, Date reportDate);
	
	Double getTotalSales (String branchCode, Date reportDate);
	Double getTotalSalesOrder (String branchCode, Date reportDate);
	//ArrayList<String, Double> getB2BSalesList(String branchCode, Date reportDate);
	//ArrayList<String, Double> getPettyExpensesList(String branchCode, Date reportDate);
	Double getTotalAtDrawer (String branchCode, Date reportDate);
	Double getOpeningCashAtDrawer (String branchCode, Date reportDate);
	Double getCashVariance (String branchCode, Date reportDate);
	
	
	
}
