package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.RawMaterialIssueReport;
import com.maple.restserver.report.entity.ServiceInvoice;
import com.maple.restserver.repository.ServiceInDtlRepository;

@Service
@Transactional
@Component
public class ServiceInvoiceServiceImpl implements ServiceInvoiceService {

	@Autowired
	ServiceInDtlRepository serviceInDtlRepo;

	@Override
	public List<ServiceInvoice> getserviceInvoice(CompanyMst companymstid, String branchcode, Date fDate,String voucherNumber) {
			List< ServiceInvoice> ServiceInvoiceList =   new ArrayList();
			List<Object> obj = serviceInDtlRepo.generateServiceInvoice(companymstid,branchcode,fDate,voucherNumber);
			 for(int i=0;i<obj.size();i++)
			 {
				Object[] objAray = (Object[]) obj.get(i);
				ServiceInvoice serviceInvoice = new ServiceInvoice();
				serviceInvoice.setItemName((String)objAray[0]);
				serviceInvoice.setBrandName((String)objAray[3]);
				serviceInvoice.setComplaints((String)objAray[4]);
				serviceInvoice.setCustomerAddress((String)objAray[8]);
				serviceInvoice.setCustomerName((String)objAray[7]);
				serviceInvoice.setModel((String)objAray[1]);
				serviceInvoice.setObservation((String)objAray[6]);
				serviceInvoice.setProductName((String)objAray[2]);
				serviceInvoice.setQty((Double)objAray[11]);
				serviceInvoice.setSerialId((String)objAray[12]);
				serviceInvoice.setVoucherDate((java.util.Date)objAray[10]);
				serviceInvoice.setVoucherNumber((String)objAray[9]);
				if(((String)objAray[5]).equalsIgnoreCase("Y"))
				{
				serviceInvoice.setWarranty("YES");
				}
				else
				{
					serviceInvoice.setWarranty("NO");
				}
				ServiceInvoiceList.add(serviceInvoice);
			 }
				 
			 
			return ServiceInvoiceList;
		}
	}

