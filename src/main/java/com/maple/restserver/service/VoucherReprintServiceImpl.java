package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.SaleOrderDetailsReport;
import com.maple.restserver.report.entity.VoucherReprintDtl;
import com.maple.restserver.repository.SalesTransHdrRepository;

@Service
@Transactional
@Component
public class VoucherReprintServiceImpl implements VoucherReprintService {

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@Override
	public List<VoucherReprintDtl> getVoucherReprint(String companymstid, String vouchernumber, String branchcode) {
		List<VoucherReprintDtl> voucherReprintList = new ArrayList();

		List<Object> obj = salesTransHdrRepository.getVoucherReprint(companymstid, vouchernumber, branchcode);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			VoucherReprintDtl reprint = new VoucherReprintDtl();
			reprint.setBranchName((String) objAray[0]);
			reprint.setItemName((String) objAray[1]);
			reprint.setQty((Double) objAray[2]);
			reprint.setTaxRate((Double) objAray[3]);
			reprint.setMrp((Double) objAray[4]);
			reprint.setUnit((String) objAray[5]);
			reprint.setAmount((Double) objAray[6]);
			reprint.setCustomerName((String) objAray[7]);
			voucherReprintList.add(reprint);
		}

		return voucherReprintList;

	}

	@Override
	public List<VoucherReprintDtl> getSalesReturnVoucherReprint(String companymstid, String vouchernumber,
			String branchcode) {
		List<VoucherReprintDtl> voucherReprintList = new ArrayList();

		List<Object> obj = salesTransHdrRepository.getSalesReturnVoucherReprint(companymstid, vouchernumber,
				branchcode);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			VoucherReprintDtl reprint = new VoucherReprintDtl();
			reprint.setBranchName((String) objAray[0]);
			reprint.setItemName((String) objAray[1]);
			reprint.setQty((Double) objAray[2]);
			reprint.setTaxRate((Double) objAray[3]);
			reprint.setMrp((Double) objAray[4]);
			reprint.setUnit((String) objAray[5]);
			reprint.setAmount((Double) objAray[6]);
			reprint.setCustomerName((String) objAray[7]);
			voucherReprintList.add(reprint);
		}

		return voucherReprintList;
	}

	@Override
	public List<VoucherReprintDtl> getVoucherReprint(String companymstid, String vouchernumber, String branchcode,
			Date date) {
		

		List<VoucherReprintDtl> voucherReprintList = new ArrayList();

		List<Object> obj = salesTransHdrRepository.getVoucherReprint(companymstid, vouchernumber, branchcode,date);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			VoucherReprintDtl reprint = new VoucherReprintDtl();
			reprint.setBranchName((String) objAray[0]);
			reprint.setItemName((String) objAray[1]);
			reprint.setQty((Double) objAray[2]);
			reprint.setTaxRate((Double) objAray[3]);
			reprint.setMrp((Double) objAray[4]);
			reprint.setUnit((String) objAray[5]);
			reprint.setAmount((Double) objAray[6]);
			reprint.setCustomerName((String) objAray[7]);
			voucherReprintList.add(reprint);
		}

		return voucherReprintList;

	
	}
}
