package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.PurchaseHdrMessageEntity;
 

public interface PurchaseService {

	
	PurchaseHdrMessageEntity createPurchaseMsgEntity(String voucherNumber,	Date voucherDate);
	
	String updatePurchasePostedToServerStatus(List<String> hdrIds);

 

}
