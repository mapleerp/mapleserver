package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.maple.util.MapleConstants;
import com.maple.restserver.accounting.repository.OpeningBalanceMstRepository;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.StockTransferInHdr;
import com.maple.restserver.report.entity.SalesAndStockConsolidatedReport;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.StockTransferInHdrRepository;
import com.maple.restserver.repository.StockTransferOutHdrRepository;

@Service
@Transactional
public class SalesAndStockConsolidatedReportServiceImpl implements SalesAndStockConsolidatedReportService{

	@Autowired
	BranchMstRepository branchMstRepository;
	@Autowired
	OpeningBalanceMstRepository openingBalanceMstRepository;
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	SalesTransHdrService salesTransHdrService;
	@Autowired
	StockTransferInHdrRepository stockTransferInHdrRepository;
	@Autowired
	ItemMstRepository itemMstRepository;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Autowired
	StockTransferOutHdrRepository stockTransferOutHdrRepository;
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;
	
	@Override
	public List<SalesAndStockConsolidatedReport> getSalesAndStockConsolidatedReport(Date fudate, Date tudate,
			String frombranch, String companymstid, String[] itemArray) {
		
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		List<SalesAndStockConsolidatedReport> salesAndStockConsolidatedReportList = new ArrayList<SalesAndStockConsolidatedReport>();
		List<BranchMst> branches = branchMstRepository.findAll();
		
		for(int i=0; i<branches.size(); i++) {
			
			String branchCode = branches.get(i).getBranchCode();
			String branchCash = branchCode + "-CASH";
			
			for(int j=0;j<itemArray.length;j++) {
				Double openingBalance = 0.0;
				Double stockIn = 0.0;
				Double saledQuantity = 0.0;
				Double stockOut = 0.0;
				Double balanceQuantity = 0.0;
				Double physicalStockIn = 0.0;
				Double physicalStockOut = 0.0;
				Double totalStockIn = 0.0;
				Double totalStockOut = 0.0;
				
				SalesAndStockConsolidatedReport salesAndStockConsolidatedReport = new SalesAndStockConsolidatedReport();
				
				salesAndStockConsolidatedReport.setItemName(itemArray[j]);
				
				
				
				String stockInParticular = "%" + MapleConstants.STOCKTRANSFERINPARTICULARS + "%";
				String stockOutParticular = "%" + MapleConstants.STOCKTRANSFEROUTPARTICULARS + "%";
				String salesParticular = "%" + MapleConstants.SALESPARTICULARS + "%";
				String physicalStockParticular = "%" + MapleConstants.PHYSICALSTOCKPARTICULAR + "%";
				
				ItemMst itemMst = itemMstRepository.findByItemName(itemArray[j]);
				String itemId = itemMst.getId();
				
				 openingBalance= itemBatchDtlRepository.getOpeningBalanceQty(fudate,branchCode,itemId);
				 if(null==openingBalance)
					 openingBalance=0.0;
				 
				 stockIn= itemBatchDtlRepository.getStockInQty(fudate,tudate,branchCode,itemId,stockInParticular);
				 if(null==stockIn)
					 stockIn=0.0;
				 
				 saledQuantity= itemBatchDtlRepository.getSaledQuantity(fudate,tudate,branchCode,itemId,salesParticular);
				 if(null==saledQuantity)
					 saledQuantity=0.0;
				 
				 stockOut= itemBatchDtlRepository.getStockOutQty(fudate,tudate,branchCode,itemId,stockOutParticular);
				 if(null==stockOut)
					 stockOut=0.0;
				
				 
				 physicalStockIn = itemBatchDtlRepository.getPhysicalStockIn(fudate,tudate,branchCode,itemId,physicalStockParticular);
				 if(null == physicalStockIn)
					 physicalStockIn=0.0;
				 
				 physicalStockOut = itemBatchDtlRepository.getPhysicalStockOut(fudate,tudate,branchCode,itemId,physicalStockParticular);
				 if(null == physicalStockOut)
					 physicalStockOut=0.0;
				 
				 totalStockIn = stockIn + physicalStockIn;
				 totalStockOut = stockOut + physicalStockOut;
//				 openingBalance = salesTransHdrService.getPharmacyCashBalance(fudate, branchCash, companyMst);
//				
//
//				ItemMst itemMst = itemMstRepository.findByItemName(itemArray[j]);
//				String itemId = itemMst.getId();
//				 stockIn = stockTransferInHdrRepository.getStockInQtyByItemName(fudate,tudate,branchCode,itemId);
//				
//				 if(null==stockIn)
//					 stockIn=0.0;
//				 
//				 
//				
//				 saledQuantity = salesTransHdrRepository.getSalesQtyByItemId(fudate,tudate,itemId);
//				
//				 if(null==saledQuantity)
//					 saledQuantity=0.0;
//				 
//				 
//				
//				 stockOut = stockTransferOutHdrRepository.getStockOutQtyByItemName(fudate,tudate,branchCode,itemId);
//				
//				 if(null==stockOut)
//					 stockOut=0.0;
//				 
//				 
//				
//			 
				 
				 
				 
				 balanceQuantity = (openingBalance + totalStockIn) - (totalStockOut + saledQuantity);
		 
				salesAndStockConsolidatedReport.setOpeningBalance(openingBalance);
				salesAndStockConsolidatedReport.setStockIn(totalStockIn);
				salesAndStockConsolidatedReport.setSales(saledQuantity);
				salesAndStockConsolidatedReport.setStockOut(totalStockOut);
				salesAndStockConsolidatedReport.setBalanceQty(balanceQuantity);
				salesAndStockConsolidatedReport.setBranchName(branches.get(i).getBranchName());
				
				salesAndStockConsolidatedReportList.add(salesAndStockConsolidatedReport);
			}
		}
		return salesAndStockConsolidatedReportList;
	}

}
