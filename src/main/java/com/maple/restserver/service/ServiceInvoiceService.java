package com.maple.restserver.service;

import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ServiceInvoice;

@Service
@Transactional
public interface ServiceInvoiceService {

	
  List<ServiceInvoice>	getserviceInvoice(CompanyMst companymstid,String branchcode,java.util.Date fDate,String voucherNumber);
}
