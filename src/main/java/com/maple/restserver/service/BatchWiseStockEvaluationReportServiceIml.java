package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.BatchWiseStockEvaluationReport;
import com.maple.restserver.repository.ItemBatchMstRepository;

@Service
@Transactional

public  class BatchWiseStockEvaluationReportServiceIml implements BatchWiseStockEvaluationReportService {


	@Autowired
	ItemBatchMstRepository itemBatchMstRepository;
	
	@Override
	public List<BatchWiseStockEvaluationReport> getStockByCompanyMstIdAndDate(String itemid, String batch,
			Date startdate, Date enddate) {
		List<BatchWiseStockEvaluationReport> branchWiseStockEvaluationReportList = new ArrayList<BatchWiseStockEvaluationReport>();
	

		 List<Object> obj =   itemBatchMstRepository.findByCompanyMstIdAndDate(itemid,batch,startdate,enddate);
	 
	
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 BatchWiseStockEvaluationReport batchWiseStockEvaluationReport=new BatchWiseStockEvaluationReport();
			 batchWiseStockEvaluationReport.setItemName((String) objAray[0]);
			 batchWiseStockEvaluationReport.setQty((Double) objAray[1]);
			 batchWiseStockEvaluationReport.setPrice((Double) objAray[2]);
			 batchWiseStockEvaluationReport.setBranchName((String) objAray[3]);
			 batchWiseStockEvaluationReport.setBatch((String) objAray[4]);
			 branchWiseStockEvaluationReportList.add(batchWiseStockEvaluationReport);
	
		 }
		 
		return branchWiseStockEvaluationReportList;
		 
	}

	
	
	
	
}
