package com.maple.restserver.service.accounting.task;




import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.AccountClassfc;
import com.maple.restserver.accounting.entity.CreditClassfc;
import com.maple.restserver.accounting.entity.DebitClassfc;
import com.maple.restserver.accounting.entity.LedgerClassfc;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayBook;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.accounting.repository.AccountClassfcRepository;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassfcRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DayBookRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassfcRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassfcRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.accounting.service.AccountingService;
import com.maple.restserver.accounting.service.AccountingServiceFC;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.resource.PaymentHdrResource;
import com.maple.restserver.service.InternetCheckService;
import com.maple.restserver.utils.EventBusFactory;

@Transactional
@Service
public class PaymentAccountingFC  {

	
	private static final Logger logger = LoggerFactory.getLogger(PaymentHdrResource.class);
	EventBus eventBus = EventBusFactory.getEventBus();

	
	 
	@Autowired
	private CompanyMstRepository  companyMstRepository;
	
	
	
	@Autowired
	private PaymentHdrRepository paymentHdrRepo;
	
	@Autowired
	private PaymentDtlRepository paymentDtlRepo;
	 
	@Autowired
	private AccountClassfcRepository accountClassRepo;
	
	
	@Autowired
	private AccountHeadsRepository accountHeadsRepo;
	
	@Autowired
	private CreditClassfcRepository creditClassRepo;

	
	@Autowired
	private DebitClassfcRepository debitClassRepo;

	@Autowired
	private LedgerClassfcRepository ledgerClassRepo;
	
	
	@Autowired
	private DayBookRepository dayBookRepository;
	
	
	
	 
	 @Autowired
	 AccountingServiceFC accountingService;
	
	
	@Value("${camunda.host}")
	 String host;
	
	
	public PaymentAccountingFC(){
		
	}
	@Transactional(rollbackFor = PartialAccountingException.class)
	 
	 
		public void execute(String voucherNumber, Date voucherDate, String paymentHdrId, CompanyMst comnpanyMst ) {
			// TODO Auto-generated method stub
		 

		/*
		 * Deletion to be called separately for  Editing 
		 */
 
	 //accountClassRepo.deleteBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);


	
	    		        	 
	    		 	    	 
	    		 			  java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());
	    		         
	    		 			 Optional<PaymentHdr> paymentHdrOpt = paymentHdrRepo.findById(paymentHdrId);
	    		 			PaymentHdr paymentHdr = paymentHdrOpt.get();
	    	  
		
    		        		
    		        		 AccountClassfc accountClass = new AccountClassfc();
    		        		 Double totalCredit = paymentDtlRepo.findSumDebit(voucherNumber,sqlDate);
    		        		 accountClass.setSourceParentId(paymentHdr.getId());
    		        		 accountClass.setSourceVoucherNumber(paymentHdr.getVoucherNumber());
    		        		 accountClass.setVoucherType("PAYMENT");
    		        		 
    		        		 BigDecimal bdTC = new BigDecimal(totalCredit);
    		        		 bdTC = bdTC.setScale(2,BigDecimal.ROUND_HALF_EVEN);
   		        		  
    		        		 accountClass.setTotalCredit(bdTC);
    		        		 accountClass.setTotalDebit(new BigDecimal(0.0));
    		        		
    		        		 accountClass.setBrachCode(paymentHdr.getBranchCode());
    		        		 accountClass.setCompanyMst(paymentHdr.getCompanyMst());
    		        		 accountClass.setTransDate(voucherDate);
    		        		 accountClass =  accountClassRepo.save(accountClass);
    		        		
    		        		 
    		        		 logger.info("Saving Account Class " + accountClass.getId());
    		        		 List<PaymentDtl> paymentDtlList = paymentDtlRepo.findBypaymenthdrId(  paymentHdr.getId());
    		        		 
    		        		 
    		        		 Iterator itrInner = paymentDtlList.iterator();
        		        	 
        		        	 while (itrInner.hasNext()) {
        		        		 PaymentDtl paymentDtl = (PaymentDtl)itrInner.next();
        		        	
        		        		 /*
        		        		  * Get mod of Pay. Fetch Account Id from Account heads from Mode Of Pay
        		        		  * 
        		        		  */
        		        		// String modeOfPay = paymentDtl.getModeOfPayment();
        		        		 Optional<AccountHeads> accountHeadOpts = accountHeadsRepo.findById(paymentDtl.getCreditAccountId()); ;
        		        		/* if(paymentDtl.getModeOfPayment().equalsIgnoreCase("CASH")){
        		        			   accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId(paymentDtl.getModeOfPayment(),comnpanyMst.getId());
        		        			    
        		        			   
        		        		 }else {
        		        			 accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId(paymentDtl.getBankAccountNumber(),comnpanyMst.getId());
        		        			  
        		        		 }
*/
        		        		 AccountHeads accountHeads = accountHeadOpts.get();
        		        		 
        		        		 
        		        		 
        		        		 CreditClassfc creditclass = new CreditClassfc();
        		        		 
        		        		 creditclass.setAccountId(accountHeads.getId());
        		        		 creditclass.setAccountClassfc(accountClass);
        		        		 
        		        		 BigDecimal bdPA = new BigDecimal(paymentDtl.getAmount());
        		        		 bdPA = bdPA.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        		        		 
        		        		 creditclass.setCrAmount(bdPA);
        		        		 creditclass.setRemark(paymentDtl.getRemark());
        		        		 
        		        		 creditclass.setCompanyMst(paymentHdr.getCompanyMst());
        		        		 
        		        		 creditclass = creditClassRepo.save(creditclass);
        		        		 
        		        		 
        		        		 LedgerClassfc  ledgerClassCr = new LedgerClassfc();
        		        		 ledgerClassCr.setAccountClassfc(accountClass);
        		        		 ledgerClassCr.setAccountId(creditclass.getAccountId());
        		        		 ledgerClassCr.setCreditAmount(creditclass.getCrAmount());
        		        		 
        		        		 ledgerClassCr.setDebitAmount(new BigDecimal(0.0));
        		        		 
        		        		 ledgerClassCr.setTransDate(paymentHdr.getVoucherDate());
        		        		 
        		        			ledgerClassCr.setBranchCode(accountClass.getBrachCode());
        							ledgerClassCr.setRemark(paymentDtl.getRemark()
        									+accountClass.getSourceVoucherNumber()); 
        		        		
        		        		 ledgerClassCr.setCompanyMst(paymentHdr.getCompanyMst());
        		        		 
        		        		 //ledgerClassRepo.save(ledgerClassCr);
        		        		 ledgerClassCr = accountingService.saveLedger(ledgerClassCr);
        		        		  
        		        	 
        		        		 
        		        		 
        		        		 DebitClassfc debitClass= new DebitClassfc();
        		        		 
        		        		 Optional<AccountHeads> accountHeadsDebit = accountHeadsRepo.findById(paymentDtl.getAccountId());
        		         
        		        		 
        		        		 
        		        		 
        		        		 
        		        		 debitClass.setAccountId(accountHeadsDebit.get().getId());
        		        		 debitClass.setAccountClassfc(accountClass);
        		        		 
//        		        		 BigDecimal bdPA = new BigDecimal(paymentDtl.getAmount());
//        		        		 bdPA = bdPA.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        		        		 
        		        		 debitClass.setDrAmount(bdPA);
        		        		 debitClass.setRemark(paymentDtl.getRemark());
        		        		
        		        		 debitClass.setCompanyMst(paymentHdr.getCompanyMst());
        		        		 
        		        		 debitClass = debitClassRepo.save(debitClass);
        		        		 
        		        		 
	        		        		 LedgerClassfc  ledgerClassDr = new LedgerClassfc();
	        		        		 ledgerClassDr.setAccountClassfc(accountClass);
	        		        		 ledgerClassDr.setAccountId(debitClass.getAccountId());
	        		        		 ledgerClassDr.setDebitAmount(debitClass.getDrAmount());
	        		        		 ledgerClassDr.setTransDate(paymentHdr.getVoucherDate());
	        		        		 ledgerClassDr.setCreditAmount(new BigDecimal(0.0));
	        		        		 
	        		        		 ledgerClassDr.setCompanyMst(paymentHdr.getCompanyMst());
	        		        		 
	        		        		 
	        		        		 ledgerClassDr.setBranchCode(accountClass.getBrachCode());
	        		        		 
	        		        		 
	        		        		 
	        		        		 ledgerClassDr.setRemark(accountHeads.getAccountName() + " "
	        		        				 +paymentDtl.getRemark()+accountClass.getSourceVoucherNumber() +"Payment"); 
	        							
	        							
	        		        		 //ledgerClassDr = ledgerClassRepo.save(ledgerClassDr);
	        		        		 
	        		        		 accountingService.saveLedger(ledgerClassDr);
	        		        		 
	        		        		 ledgerClassCr.setRemark(accountHeadsDebit.get().getAccountName()+ " "+
	        		        				 ledgerClassCr.getRemark()+"Payment");
	        		        		 
	        		        		 accountingService.saveLedger(ledgerClassCr);
	        		        		 
	        		        		 
	        		        		 /*
	        		        		  * Now Insert into DayBook
	        		        		  */
        		        		  
	        		        		 
			
	        		    	        
        		        		  
        		        		 
        		        	 }
        		        	 
        		        	 eventBus.post(accountClass);
    		        		 
		/*
		 * List<AccountClassfc> accountClassToForwardList = accountClassRepo
		 * .findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);
		 * 
		 * if (accountClassToForwardList.size() > 0) {
		 * 
		 * 
		 * Iterator iterForward = accountClassToForwardList.iterator(); while
		 * (iterForward.hasNext()) { AccountClass accountClassForward = (AccountClass)
		 * iterForward.next();
		 * 
		 * 
		 * Map<String, Object> variables = new HashMap<String, Object>();
		 * 
		 * 
		 * 
		 * variables.put("voucherNumber", accountClassForward.getSourceVoucherNumber());
		 * variables.put("voucherDate", accountClassForward.getTransDate());
		 * variables.put("inet", 0); variables.put("id", accountClassForward.getId());
		 * variables.put("isbranchsales", "NO"); variables.put("companyid",
		 * accountClassForward.getCompanyMst()); variables.put("REST", 0);
		 * 
		 * 
		 * 
		 * variables.put("WF", "forwardAccountingFC");
		 * 
		 * 
		 * 
		 * 
		 * eventBus.post(variables);
		 * 
		 * 
		 * }
		 * 
		 * }
		 */
    		        	 
 
        
		
	 
	 }
	
}
