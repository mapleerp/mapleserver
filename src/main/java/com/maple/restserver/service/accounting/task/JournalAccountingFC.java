package com.maple.restserver.service.accounting.task;




import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.accounting.entity.AccountClassfc;
import com.maple.restserver.accounting.entity.CreditClassfc;
import com.maple.restserver.accounting.entity.DebitClassfc;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.entity.LedgerClassfc;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
 
import com.maple.restserver.entity.DayBook;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.accounting.repository.AccountClassfcRepository;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassfcRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassfcRepository;
import com.maple.restserver.repository.DayBookRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassfcRepository;
import com.maple.restserver.repository.JournalDtlRepository;
import com.maple.restserver.repository.JournalHdrRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.accounting.service.AccountingService;
import com.maple.restserver.accounting.service.AccountingServiceFC;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.resource.PaymentHdrResource;
import com.maple.restserver.service.InternetCheckService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;

@Transactional
@Service
public class JournalAccountingFC  {

	private static final Logger logger = LoggerFactory.getLogger(PaymentHdrResource.class);
	 
	@Autowired
	private CompanyMstRepository  companyMstRepository;
	
	
	
	 

	
	
 
	@Autowired
	private AccountClassfcRepository accountClassRepo;

	@Autowired
	private AccountHeadsRepository accountHeadsRepo;

	@Autowired
	private CreditClassfcRepository creditClassRepo;

	@Autowired
	private DebitClassfcRepository debitClassRepo;

	@Autowired
	private LedgerClassfcRepository ledgerClassRepo;
	
	
	@Autowired
	private SalesReceiptsRepository salesReceiptsRepo;
	
 
	
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	private JournalHdrRepository journalHdrRepository;
	
	@Autowired
	private JournalDtlRepository journalDtlRepository;
	
	
	
	@Autowired
	private DayBookRepository dayBookRepository;
	
	 @Autowired
	 AccountingServiceFC accountingService;
	 @Autowired
		SaveAndPublishService saveAndPublishService;
	
	public JournalAccountingFC() {

	}

	@Transactional(rollbackFor = PartialAccountingException.class)
 	public void execute(String voucherNumber  ,  Date voucherDate , String journalHdrId ,CompanyMst comnpanyMst )   {

		
		

		/*
		 * Deletion to be called separately for  Editing 
		 */
 
	 //accountClassRepo.deleteBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);


	
		
		
		BigDecimal zero = new BigDecimal("0");
		
		 
		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		List<JournalHdr> journalHdrList = journalHdrRepository.findByVoucherNumberAndVoucherDate(voucherNumber,
				sqlDate);
		Iterator itr = journalHdrList.iterator();

	  	 System.out.println("Company ID "+comnpanyMst.getId());
    	 

		while (itr.hasNext()) {
			
			
			JournalHdr journalHdr = (JournalHdr) itr.next();
			
			
			AccountClassfc accountClass = new AccountClassfc();
			BigDecimal totalCredit = new BigDecimal(journalDtlRepository.findSumCreditAmount(journalHdr.getId()));
		
			BigDecimal totalDebit = new BigDecimal(journalDtlRepository.findSumDebitAmount(journalHdr.getId()));
			
			
			accountClass.setSourceParentId(journalHdr.getId());
			accountClass.setSourceVoucherNumber(journalHdr.getVoucherNumber());
			accountClass.setVoucherType("JOURNAL");
			accountClass.setTotalDebit(totalDebit);
			 accountClass.setBrachCode(journalHdr.getBranchCode());
			accountClass.setTotalCredit(totalCredit);
			
			accountClass.setCompanyMst(journalHdr.getCompanyMst());
			accountClass.setTransDate(voucherDate);
//			accountClass = accountClassRepo.save(accountClass);
			accountClass =saveAndPublishService.saveAccountClassfc(accountClass, accountClass.getBrachCode());
			
			List<JournalDtl> journalDtlList = journalDtlRepository.findByJournalHdrId(journalHdr.getId()) ;
			
			Set<DebitClassfc> debitclassList = new HashSet<DebitClassfc>();
			Iterator rcpIter = journalDtlList.iterator();

			while (rcpIter.hasNext()) {
				
				JournalDtl journalDtl = (JournalDtl) rcpIter.next();
				

				logger.info("Saving Journal Class " + accountClass.getId());
				
			 
				
				// Debiting to Customer/Cash
				AccountHeads accountHeads ;
				
				 accountHeads = accountHeadsRepo.findByIdAndCompanyMstId(journalDtl.getAccountHead(),comnpanyMst.getId());
				 
				 
				 

	       				
			 			
			 			
				if(journalDtl.getDebitAmount() > 0 ) {
				
					
					DebitClassfc debitclass = new DebitClassfc();
	
					debitclass.setAccountId(accountHeads.getId());
					debitclass.setAccountClassfc(accountClass);
					debitclass.setDrAmount(new BigDecimal(journalDtl.getDebitAmount()));
					debitclass.setRemark(journalHdr.getVoucherNumber());
					debitclass.setCompanyMst(journalHdr.getCompanyMst());
					
					
					debitclass = debitClassRepo.save(debitclass);
	
					LedgerClassfc ledgerClassDr = new LedgerClassfc();
					ledgerClassDr.setAccountClassfc(accountClass);
					ledgerClassDr.setAccountId(debitclass.getAccountId());
					ledgerClassDr.setDebitAmount(debitclass.getDrAmount());
					ledgerClassDr.setCreditAmount(zero);
					ledgerClassDr.setCompanyMst(journalHdr.getCompanyMst());
					
				
					
					ledgerClassDr.setTransDate(journalHdr.getVoucherDate());
					ledgerClassDr.setBranchCode(accountClass.getBrachCode());
					ledgerClassDr.setRemark("Journal "+ accountClass.getSourceVoucherNumber());
					
//					ledgerClassRepo.save(ledgerClassDr);
					saveAndPublishService.saveLedgerClassfc(ledgerClassDr, ledgerClassDr.getBranchCode());
					
					accountingService.saveLedger(ledgerClassDr);

					/*
					 * Noew Insert Day Book
					 */
					
					/*
					 * DayBook dayBook = new DayBook();
					 * dayBook.setBranchCode(journalHdr.getBranchCode());
					 * dayBook.setCompanyMst(journalHdr.getCompanyMst());
					 * 
					 * 
					 * dayBook.setDrAccountName(accountHeads.getAccountName());
					 * dayBook.setDrAmount(debitclass.getDrAmount().doubleValue());
					 * dayBook.setSourceVoucheNumber(journalHdr.getVoucherNumber());
					 * dayBook.setSourceVoucherDate(journalHdr.getVoucherDate());
					 * dayBook.setSourceVoucherType("JOURNAL");
					 * 
					 * dayBookRepository.save(dayBook);
					 */
 				  
				}
			 
				
				
				 
				if(journalDtl.getCreditAmount() > 0 ) {
				
					
					CreditClassfc creditclass = new CreditClassfc();
	
					creditclass.setAccountId(accountHeads.getId());
					creditclass.setAccountClassfc(accountClass);
					creditclass.setCrAmount(new BigDecimal(journalDtl.getCreditAmount()));
					creditclass.setRemark(journalHdr.getVoucherNumber());
					
					creditclass.setCompanyMst(journalHdr.getCompanyMst());
					
//					creditclass = creditClassRepo.save(creditclass);
					creditclass=saveAndPublishService.saveCreditClassfc(creditclass, creditclass.getBranchCode());
					

					LedgerClassfc ledgerClassCr = new LedgerClassfc();
					ledgerClassCr.setAccountClassfc(accountClass);
					ledgerClassCr.setAccountId(creditclass.getAccountId());
					ledgerClassCr.setCreditAmount(creditclass.getCrAmount());
					ledgerClassCr.setDebitAmount(zero);
					
					ledgerClassCr.setTransDate(journalHdr.getVoucherDate());
					ledgerClassCr.setCompanyMst(journalHdr.getCompanyMst());

					ledgerClassCr.setBranchCode(accountClass.getBrachCode());
					ledgerClassCr.setRemark("Journal"+accountClass.getSourceVoucherNumber());
					
					
//					ledgerClassRepo.save(ledgerClassCr);
					saveAndPublishService.saveLedgerClassfc(ledgerClassCr, ledgerClassCr.getBranchCode());
					
					 accountingService.saveLedger(ledgerClassCr);
					/*
					 * Noew Insert Day Book
					 */
					/*
					 * DayBook dayBook = new DayBook();
					 * dayBook.setBranchCode(journalHdr.getBranchCode());
					 * dayBook.setCompanyMst(journalHdr.getCompanyMst());
					 * 
					 * dayBook.setDrAccountName(accountHeads.getAccountName());
					 * dayBook.setDrAmount(creditclass.getCrAmount().doubleValue());
					 * dayBook.setSourceVoucheNumber(journalHdr.getVoucherNumber());
					 * dayBook.setSourceVoucherDate(journalHdr.getVoucherDate());
					 * dayBook.setSourceVoucherType("JOURNAL");
					 * 
					 * dayBookRepository.save(dayBook);
					 */
 				  
				}
				
		 
	   	       
	   	        
   	        
				
				
				
				
 			}
			
			eventBus.post(accountClass);
			
			
			/*
			 * List<AccountClassfc> accountClassToForwardList = accountClassRepo
			 * .findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);
			 * 
			 * if (accountClassToForwardList.size() > 0) {
			 * 
			 * Iterator iterForward = accountClassToForwardList.iterator(); while
			 * (iterForward.hasNext()) { AccountClassfc accountClassForward =
			 * (AccountClassfc) iterForward.next();
			 * 
			 * Map<String, Object> variables = new HashMap<String, Object>();
			 * 
			 * variables.put("voucherNumber", accountClassForward.getSourceVoucherNumber());
			 * variables.put("voucherDate", accountClassForward.getTransDate());
			 * variables.put("inet", 0); variables.put("id", accountClassForward.getId());
			 * variables.put("isbranchsales", "NO"); variables.put("companyid",
			 * accountClassForward.getCompanyMst()); variables.put("REST", 0);
			 * 
			 * variables.put("WF", "forwardAccountingFC");
			 * 
			 * 
			 * eventBus.post(variables); } }
			 */
		}

	}

}
