package com.maple.restserver.service.accounting.task;

public class PartialAccountingException extends Exception {
	
		  public PartialAccountingException(String message) {
		      super(message);
		  }
}
