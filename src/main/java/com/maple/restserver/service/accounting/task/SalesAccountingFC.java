package com.maple.restserver.service.accounting.task;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.client.ExternalTaskClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.RestserverApplication;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.AccountClassfc;
import com.maple.restserver.accounting.entity.CreditClassfc;
import com.maple.restserver.accounting.entity.DebitClassfc;
import com.maple.restserver.accounting.entity.LedgerClassfc;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.DayBook;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.message.entity.AccountMessage;
import com.maple.restserver.accounting.repository.AccountClassfcRepository;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassfcRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassfcRepository;
import com.maple.restserver.repository.DayBookRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassfcRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.accounting.service.AccountingService;
import com.maple.restserver.accounting.service.AccountingServiceFC;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.resource.PaymentHdrResource;
import com.maple.restserver.service.InternetCheckService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Transactional
@Service
public class SalesAccountingFC {

	private static final Logger logger = LoggerFactory.getLogger(SalesAccountingFC.class);

	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	private CompanyMstRepository companyMstRepository;

	@Autowired
	private SalesTransHdrRepository salesTransHdrRepo;


	@Autowired
	private SalesDetailsRepository salesDtlRepo;

	@Autowired
	private AccountClassfcRepository accountClassFCRepo;

	@Autowired
	private AccountHeadsRepository accountHeadsRepo;

	@Autowired
	private CreditClassfcRepository creditClassFCRepo;

	@Autowired
	private DebitClassfcRepository debitClassFCRepo;

	@Autowired
	private LedgerClassfcRepository ledgerClassFCRepo;

	@Autowired
	private SalesReceiptsRepository salesReceiptsRepo;

	@Autowired
	private DayBookRepository dayBookRepository;

	@Autowired
	AccountingServiceFC accountingService;

	@Autowired
	RestserverApplication restserverApplication;

	public SalesAccountingFC() {

	}

	@Transactional(rollbackFor = PartialAccountingException.class)
	public void execute(String voucherNumber, Date voucherDate, String salesTransHdrId, CompanyMst companyMst)
			throws PartialAccountingException {

		int i = 0;

		BigDecimal zero = new BigDecimal("0");

		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		/*
		 * Deletion to be called separately for invoice Edit
		 */
		// accountClassFCRepo.deleteBySourceVoucherNumberAndTransDate(voucherNumber,
		// voucherDate);

		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepo.findByVoucherNumberAndVoucherDate(voucherNumber,
				sqlDate);
		Iterator itr = salesTransHdrList.iterator();
		while (itr.hasNext()) {

			SalesTransHdr salesTransHdr = (SalesTransHdr) itr.next();
			AccountClassfc accountClass = new AccountClassfc();

			BigDecimal totalCredit = salesDtlRepo.findSumCrAmountSalesDtl(salesTransHdr.getId());

			if (null == totalCredit) {
				continue;
			}
			totalCredit = totalCredit.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal totalDebit = salesDtlRepo.findSumDrAmountSalesDtl(salesTransHdr.getId());
			if (null == totalDebit) {
				continue;
			}

			totalDebit = totalDebit.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			accountClass.setSourceParentId(salesTransHdr.getId());
			accountClass.setTransDate(salesTransHdr.getVoucherDate());
			accountClass.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());
			accountClass.setVoucherType("SALES");
			accountClass.setTotalDebit(totalDebit);
			accountClass.setBrachCode(salesTransHdr.getBranchCode());

			accountClass.setTotalCredit(totalCredit);

			accountClass.setCompanyMst(salesTransHdr.getCompanyMst());

			accountClass = accountClassFCRepo.save(accountClass);

			List<SalesReceipts> salesReceiptsList = salesReceiptsRepo.findBySalesTransHdr(salesTransHdr);

			Set<DebitClassfc> debitclassList = new HashSet<DebitClassfc>();
			Iterator rcpIter = salesReceiptsList.iterator();

			while (rcpIter.hasNext()) {

				SalesReceipts aSalesReceipts = (SalesReceipts) rcpIter.next();

				logger.info("Saving Account Class " + accountClass.getId());

				// Debiting to Customer/Cash
				AccountHeads accountHeads;
				if (aSalesReceipts.getReceiptMode().equalsIgnoreCase("CREDIT")
						|| aSalesReceipts.getReceiptMode().equalsIgnoreCase("BRANCH_SALES")) {
					AccountHeads accountHeads2 = salesTransHdr.getAccountHeads();

					logger.debug("************ACCOUNT HEADS**********"
							+ salesTransHdr.getAccountHeads().getAccountName() + " -     " + companyMst.getId());
					accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId(
							salesTransHdr.getAccountHeads().getAccountName(), companyMst.getId());

				} else {

					logger.debug("Customer ID " + salesTransHdr.getAccountHeads().getId() + " NAME ="
							+ salesTransHdr.getAccountHeads().getAccountName());
					logger.debug("************ACCOUNT HEADS**********" + aSalesReceipts.getReceiptMode());

					accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId(aSalesReceipts.getReceiptMode(),
							companyMst.getId());

				}

				DebitClassfc debitclass = new DebitClassfc();

				// logger.debug("Debit Account" + accountHeads.getAccountName());

				if (null == accountHeads) {
					accountHeads = new AccountHeads();
					accountHeads.setId(salesTransHdr.getAccountHeads().getId());
					accountHeads.setAccountName(salesTransHdr.getAccountHeads().getAccountName());
					accountHeads.setGroupOnly("N");
					accountHeads.setTaxId("0");
					accountHeads.setParentId("0");
					accountHeads.setCompanyMst(salesTransHdr.getCompanyMst());
					accountHeads = accountHeadsRepo.save(accountHeads);
				}
				debitclass.setAccountId(accountHeads.getId());
				debitclass.setAccountClassfc(accountClass);

				BigDecimal ReceiptAmount = new BigDecimal(aSalesReceipts.getReceiptAmount());
				ReceiptAmount = ReceiptAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				debitclass.setDrAmount(ReceiptAmount);

				debitclass.setRemark(salesTransHdr.getVoucherNumber());
				debitclass.setCompanyMst(salesTransHdr.getCompanyMst());

				debitclass = debitClassFCRepo.save(debitclass);

				LedgerClassfc ledgerClassDr = new LedgerClassfc();
				ledgerClassDr.setAccountClassfc(accountClass);
				ledgerClassDr.setAccountId(debitclass.getAccountId());
				ledgerClassDr.setDebitAmount(debitclass.getDrAmount());
				ledgerClassDr.setCreditAmount(zero);
				ledgerClassDr.setCompanyMst(salesTransHdr.getCompanyMst());
				ledgerClassDr.setSourceVoucherId(salesTransHdr.getVoucherNumber());
				ledgerClassDr.setTransDate(salesTransHdr.getVoucherDate());
				ledgerClassDr.setBranchCode(salesTransHdr.getBranchCode());

				ledgerClassDr.setBranchCode(accountClass.getBrachCode());
				ledgerClassDr.setRemark("Sales " + accountClass.getSourceVoucherNumber());

				// ledgerClassRepo.save(ledgerClassDr);

				accountingService.saveLedger(ledgerClassDr);

			}

			/*
			 * Find Total Acessible Value will be credited to Sales Account
			 * 
			 */
			List<SalesDtl> salesDtlList = salesDtlRepo.findBySalesTransHdrId(salesTransHdr.getId());
			Iterator iter = salesDtlList.iterator();

			while (iter.hasNext()) {
				SalesDtl salesDtl = (SalesDtl) iter.next();

				Double AcessibleValue = salesDtlRepo.findSumAcessibleAmountItemWise(salesTransHdr.getId(),
						salesDtl.getItemId(), salesDtl.getId());
				AccountHeads accountHeads = null;
				accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId("SALES ACCOUNT", companyMst.getId());

				CreditClassfc creditclass = new CreditClassfc();

				creditclass.setAccountId(accountHeads.getId());
				creditclass.setAccountClassfc(accountClass);

				BigDecimal bdAcessibleValue = new BigDecimal(AcessibleValue);
				bdAcessibleValue = bdAcessibleValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				creditclass.setCrAmount(bdAcessibleValue);
				creditclass.setRemark(salesTransHdr.getVoucherNumber());

				creditclass.setCompanyMst(salesTransHdr.getCompanyMst());

				creditclass.setItemId(salesDtl.getItemId());
				creditclass.setRate(salesDtl.getRate());
				creditclass.setQty(salesDtl.getQty());

				creditclass.setTransDate(voucherDate);

				creditclass.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());

				creditclass = creditClassFCRepo.save(creditclass);

				LedgerClassfc ledgerClassCr = new LedgerClassfc();
				ledgerClassCr.setAccountClassfc(accountClass);
				ledgerClassCr.setAccountId(creditclass.getAccountId());
				ledgerClassCr.setCreditAmount(creditclass.getCrAmount());
				ledgerClassCr.setDebitAmount(zero);

				ledgerClassCr.setTransDate(salesTransHdr.getVoucherDate());
				ledgerClassCr.setCompanyMst(salesTransHdr.getCompanyMst());

				ledgerClassCr.setBranchCode(accountClass.getBrachCode());
				ledgerClassCr.setRemark("Sales " + accountClass.getSourceVoucherNumber() + salesDtl.getItemName());

				ledgerClassCr.setSourceVoucherId(salesTransHdr.getVoucherNumber());

				// ledgerClassRepo.saveAndFlush(ledgerClassCr);
				accountingService.saveLedger(ledgerClassCr);

				// Account Taxwise

				Double objList = salesDtlRepo.findTotalTaxSumSalesDtlItemWise(salesTransHdr.getId(),
						salesDtl.getItemId(), salesDtl.getId());

				double taxAmount = objList;

				if (taxAmount > 0) {
					BigDecimal cgstAmount = new BigDecimal(taxAmount / 2);
					cgstAmount = cgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					AccountHeads cgstAccountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId("CGST ACCOUNT",
							companyMst.getId());

					if (null == cgstAccountHeads) {

						throw new PartialAccountingException("CGST ACCOUNT Not found");

					}

					// find existing CGST Recors
					List<CreditClassfc> creditclassCgstList = creditClassFCRepo
							.findByAccountClassfcAndAccountId(accountClass, cgstAccountHeads.getId());
					CreditClassfc creditclassCgst = null;
					if (creditclassCgstList.size() > 0) {
						creditclassCgst = creditclassCgstList.get(0);

						creditclassCgst.setAccountId(cgstAccountHeads.getId());
						creditclassCgst.setAccountClassfc(accountClass);
						creditclassCgst.setCrAmount(creditclassCgst.getCrAmount().add(cgstAmount));

						creditclassCgst.setRemark(salesTransHdr.getVoucherNumber());

						creditclassCgst.setCompanyMst(salesTransHdr.getCompanyMst());
						creditclassCgst.setTransDate(voucherDate);

						creditclassCgst.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());
					} else {
						creditclassCgst = new CreditClassfc();

						creditclassCgst.setAccountId(cgstAccountHeads.getId());
						creditclassCgst.setAccountClassfc(accountClass);
						creditclassCgst.setCrAmount(cgstAmount);

						creditclassCgst.setRemark(salesTransHdr.getVoucherNumber());

						creditclassCgst.setCompanyMst(salesTransHdr.getCompanyMst());
						creditclassCgst.setTransDate(voucherDate);

						creditclassCgst.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());
					}

					creditclassCgst = creditClassFCRepo.saveAndFlush(creditclassCgst);

					LedgerClassfc ledgerClassCgstCr = null;

					List<LedgerClassfc> ledgerClassCgstCrList = ledgerClassFCRepo
							.findByAccountClassfcIdAndAccountId(accountClass.getId(), creditclassCgst.getAccountId());

					if (ledgerClassCgstCrList.size() > 0) {
						ledgerClassCgstCr = ledgerClassCgstCrList.get(0);
						// ledgerClassCgstCr.setAccountClassFC(accountClass);
						// ledgerClassCgstCr.setAccountId(creditclassCgst.getAccountId());
						ledgerClassCgstCr.setCreditAmount(ledgerClassCgstCr.getCreditAmount().add(cgstAmount));
						// ledgerClassCgstCr.setDebitAmount(zero);

						// ledgerClassCgstCr.setTransDate(salesTransHdr.getVoucherDate());

						// ledgerClassCgstCr.setCompanyMst(salesTransHdr.getCompanyMst());

						// ledgerClassCgstCr.setBranchCode(accountClass.getBrachCode());
						// ledgerClassCgstCr.setRemark(accountClass.getSourceVoucherNumber());
						// ledgerClassCgstCr.setSourceVoucherId(salesTransHdr.getVoucherNumber());
					} else {
						ledgerClassCgstCr = new LedgerClassfc();
						ledgerClassCgstCr.setAccountClassfc(accountClass);
						ledgerClassCgstCr.setAccountId(creditclassCgst.getAccountId());
						ledgerClassCgstCr.setCreditAmount(creditclassCgst.getCrAmount());
						ledgerClassCgstCr.setDebitAmount(zero);

						ledgerClassCgstCr.setTransDate(salesTransHdr.getVoucherDate());

						ledgerClassCgstCr.setCompanyMst(salesTransHdr.getCompanyMst());

						ledgerClassCgstCr.setBranchCode(accountClass.getBrachCode());
						ledgerClassCgstCr.setRemark(accountClass.getSourceVoucherNumber());
						ledgerClassCgstCr.setSourceVoucherId(salesTransHdr.getVoucherNumber());
					}

					// ledgerClassRepo.saveAndFlush(ledgerClassCgstCr);
					accountingService.saveLedger(ledgerClassCgstCr);

					// ------------------ SGST
					AccountHeads sgstAccountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId("SGST ACCOUNT",
							companyMst.getId());

					if (null == sgstAccountHeads) {
						throw new PartialAccountingException("SGST ACCOUNT Not found");
					}

					List<CreditClassfc> creditclasSGgstList = creditClassFCRepo
							.findByAccountClassfcAndAccountId(accountClass, sgstAccountHeads.getId());
					CreditClassfc creditclasSGgst = null;
					if (creditclasSGgstList.size() > 0) {
						creditclasSGgst = creditclasSGgstList.get(0);

						// creditclasSGgst.setAccountId(sgstAccountHeads.getId());
						// creditclasSGgst.setAccountClassFC(accountClass);
						creditclasSGgst.setCrAmount(creditclasSGgst.getCrAmount().add(cgstAmount));

						// creditclasSGgst.setRemark(salesTransHdr.getVoucherNumber());

						// creditclasSGgst.setCompanyMst(salesTransHdr.getCompanyMst());

						// creditclasSGgst.setTransDate(voucherDate);
						// creditclasSGgst.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());
					} else {
						creditclasSGgst = new CreditClassfc();
						creditclasSGgst.setAccountId(sgstAccountHeads.getId());
						creditclasSGgst.setAccountClassfc(accountClass);
						creditclasSGgst.setCrAmount(cgstAmount);

						creditclasSGgst.setRemark(salesTransHdr.getVoucherNumber());

						creditclasSGgst.setCompanyMst(salesTransHdr.getCompanyMst());

						creditclasSGgst.setTransDate(voucherDate);
						creditclasSGgst.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());

					}
					creditclasSGgst = creditClassFCRepo.saveAndFlush(creditclasSGgst);

					List<LedgerClassfc> ledgerClassSgstCrList = ledgerClassFCRepo
							.findByAccountClassfcIdAndAccountId(accountClass.getId(), creditclasSGgst.getAccountId());
					LedgerClassfc ledgerClassSgstCr = null;

					if (ledgerClassSgstCrList.size() > 0) {
						ledgerClassSgstCr = ledgerClassSgstCrList.get(0);

						// ledgerClassSgstCr.setAccountClassFC(accountClass);
						// ledgerClassSgstCr.setAccountId(creditclasSGgst.getAccountId());
						ledgerClassSgstCr.setCreditAmount(ledgerClassSgstCr.getCreditAmount().add(cgstAmount));
						// ledgerClassSgstCr.setDebitAmount(zero);

						// ledgerClassSgstCr.setTransDate(salesTransHdr.getVoucherDate());

						// ledgerClassSgstCr.setCompanyMst(salesTransHdr.getCompanyMst());

						// ledgerClassSgstCr.setBranchCode(accountClass.getBrachCode());
						// ledgerClassSgstCr.setRemark(accountClass.getSourceVoucherNumber());

						// ledgerClassSgstCr.setSourceVoucherId(salesTransHdr.getVoucherNumber());

					} else {
						ledgerClassSgstCr = new LedgerClassfc();
						ledgerClassSgstCr.setAccountClassfc(accountClass);
						ledgerClassSgstCr.setAccountId(creditclasSGgst.getAccountId());
						ledgerClassSgstCr.setCreditAmount(creditclasSGgst.getCrAmount());
						ledgerClassSgstCr.setDebitAmount(zero);

						ledgerClassSgstCr.setTransDate(salesTransHdr.getVoucherDate());

						ledgerClassSgstCr.setCompanyMst(salesTransHdr.getCompanyMst());

						ledgerClassSgstCr.setBranchCode(accountClass.getBrachCode());
						ledgerClassSgstCr.setRemark(accountClass.getSourceVoucherNumber());

						ledgerClassSgstCr.setSourceVoucherId(salesTransHdr.getVoucherNumber());
					}

					// ledgerClassRepo.saveAndFlush(ledgerClassSgstCr);

					accountingService.saveLedger(ledgerClassSgstCr);
				}

				// If Cess Item ?

				if (!salesTransHdr.getSalesMode().equalsIgnoreCase("B2B")) {
					BigDecimal CessAmount = new BigDecimal(salesDtlRepo.findSumCessAmountItemWise(salesTransHdr.getId(),
							salesDtl.getItemId(), salesDtl.getId()));

					CessAmount = CessAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					if (CessAmount.doubleValue() > 0) {

						AccountHeads floodCessaccountHeads = null;
						floodCessaccountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId("KERALA FLOOD CESS",
								companyMst.getId());

						List<CreditClassfc> creditclasKFCList = creditClassFCRepo
								.findByAccountClassfcAndAccountId(accountClass, floodCessaccountHeads.getId());
						CreditClassfc creditclasKFC = null;

						if (creditclasKFCList.size() > 0) {
							creditclasKFC = creditclasKFCList.get(0);

							creditclasKFC.setAccountId(floodCessaccountHeads.getId());
							creditclasKFC.setAccountClassfc(accountClass);
							creditclasKFC.setCrAmount(creditclasKFC.getCrAmount().add(CessAmount));

							creditclasKFC.setRemark(salesTransHdr.getVoucherNumber());

							creditclasKFC.setCompanyMst(salesTransHdr.getCompanyMst());

							creditclasKFC.setTransDate(voucherDate);

							creditclasKFC.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());

						} else {
							creditclasKFC = new CreditClassfc();

							creditclasKFC.setAccountId(floodCessaccountHeads.getId());
							creditclasKFC.setAccountClassfc(accountClass);
							creditclasKFC.setCrAmount(CessAmount);

							creditclasKFC.setRemark(salesTransHdr.getVoucherNumber());

							creditclasKFC.setCompanyMst(salesTransHdr.getCompanyMst());

							creditclasKFC.setTransDate(voucherDate);

							creditclasKFC.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());

						}

						creditclasKFC = creditClassFCRepo.saveAndFlush(creditclasKFC);

						LedgerClassfc ledgerClassKFC = null;
						List<LedgerClassfc> ledgerClassKFCList = ledgerClassFCRepo
								.findByAccountClassfcIdAndAccountId(accountClass.getId(), creditclasKFC.getAccountId());
						LedgerClassfc ledgerClassSgstCr = null;

						if (ledgerClassKFCList.size() > 0) {
							ledgerClassKFC = ledgerClassKFCList.get(0);
							// ledgerClassKFC.setAccountClassFC(accountClass);
							// ledgerClassKFC.setAccountId(creditclasKFC.getAccountId());
							ledgerClassKFC.setCreditAmount(ledgerClassKFC.getCreditAmount().add(CessAmount));
							// ledgerClassKFC.setDebitAmount(zero);

							// ledgerClassKFC.setTransDate(salesTransHdr.getVoucherDate());

							// ledgerClassKFC.setCompanyMst(salesTransHdr.getCompanyMst());
							// ledgerClassKFC.setBranchCode(accountClass.getBrachCode());
							// ledgerClassKFC.setRemark(accountClass.getSourceVoucherNumber());

							// ledgerClassKFC.setSourceVoucherId(salesTransHdr.getVoucherNumber());

						} else {

							ledgerClassKFC = new LedgerClassfc();
							ledgerClassKFC.setAccountClassfc(accountClass);
							ledgerClassKFC.setAccountId(creditclasKFC.getAccountId());
							ledgerClassKFC.setCreditAmount(creditclasKFC.getCrAmount());
							ledgerClassKFC.setDebitAmount(zero);

							ledgerClassKFC.setTransDate(salesTransHdr.getVoucherDate());

							ledgerClassKFC.setCompanyMst(salesTransHdr.getCompanyMst());
							ledgerClassKFC.setBranchCode(accountClass.getBrachCode());
							ledgerClassKFC.setRemark(accountClass.getSourceVoucherNumber());

							ledgerClassKFC.setSourceVoucherId(salesTransHdr.getVoucherNumber());

						}

						// ledgerClassRepo.saveAndFlush(ledgerClassKFC);
						accountingService.saveLedger(ledgerClassKFC);
					}
				}

			}

			/*
			 * Set Round Off
			 * 
			 */

			List<AccountClassfc> accountClassList = accountClassFCRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iterRe = accountClassList.iterator();
				while (iterRe.hasNext()) {
					AccountClass accountClassRe = (AccountClass) iterRe.next();

					List<CreditClassfc> creditClassList = creditClassFCRepo
							.findByAccountClassfcId(accountClass.getId());

					List<DebitClassfc> debitClassList = debitClassFCRepo.findByAccountClassfcId(accountClass.getId());

					double debittotal = 0.0;
					double credittotal = 0.0;
					Iterator drIter = debitClassList.iterator();
					while (drIter.hasNext()) {
						DebitClassfc debitClass = (DebitClassfc) drIter.next();
						debittotal = debittotal + debitClass.getDrAmount().doubleValue();

					}

					Iterator crIter = creditClassList.iterator();
					while (crIter.hasNext()) {
						CreditClassfc creditClass = (CreditClassfc) crIter.next();
						credittotal = credittotal + creditClass.getCrAmount().doubleValue();

					}

					if (credittotal < debittotal) {
						double roundOff = credittotal - debittotal;

						BigDecimal bdRoundOff = new BigDecimal(roundOff);
						bdRoundOff = bdRoundOff.setScale(2, BigDecimal.ROUND_HALF_EVEN);

						AccountHeads roundOffAccountHeads = accountHeadsRepo
								.findByAccountNameAndCompanyMstId("ROUNDOFF", companyMst.getId());

						if (null == roundOffAccountHeads) {
							throw new PartialAccountingException("SGST ACCOUNT Not found");
						}

						DebitClassfc debitClassRoundOff = new DebitClassfc();

						debitClassRoundOff.setAccountId(roundOffAccountHeads.getId());
						debitClassRoundOff.setAccountClassfc(accountClass);
						debitClassRoundOff.setDrAmount(bdRoundOff);

						debitClassRoundOff.setRemark(salesTransHdr.getVoucherNumber());

						debitClassRoundOff.setCompanyMst(salesTransHdr.getCompanyMst());

						debitClassRoundOff.setTransDate(voucherDate);

						debitClassRoundOff.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());

						debitClassRoundOff = debitClassFCRepo.saveAndFlush(debitClassRoundOff);

					} else if (debittotal < credittotal) {

						double roundOff = debittotal - credittotal;

						BigDecimal bdRoundOff = new BigDecimal(roundOff);
						bdRoundOff = bdRoundOff.setScale(2, BigDecimal.ROUND_HALF_EVEN);

						AccountHeads pareAccount = accountHeadsRepo.findByAccountNameAndCompanyMstId("CASH",
								companyMst.getId());
						AccountHeads roundOffAccountHeads = accountHeadsRepo
								.findByAccountNameAndCompanyMstId("ROUNDOFF", companyMst.getId());
						if (null == roundOffAccountHeads || null == roundOffAccountHeads.getId()) {
							roundOffAccountHeads = new AccountHeads();
							roundOffAccountHeads.setAccountName("ROUNDOFF");
							roundOffAccountHeads.setCompanyMst(salesTransHdr.getCompanyMst());
							roundOffAccountHeads.setGroupOnly("N");
							roundOffAccountHeads.setMachineId("MAC");
							roundOffAccountHeads.setTaxId("");
							roundOffAccountHeads.setParentId(pareAccount.getId());
							roundOffAccountHeads.setVoucherType("");
							roundOffAccountHeads.setId("	SYSROUND001");
							accountHeadsRepo.save(roundOffAccountHeads);

						}

						CreditClassfc creditclasRoundOff = new CreditClassfc();

						creditclasRoundOff.setAccountId(roundOffAccountHeads.getId());
						creditclasRoundOff.setAccountClassfc(accountClass);
						creditclasRoundOff.setCrAmount(bdRoundOff);

						creditclasRoundOff.setRemark(salesTransHdr.getVoucherNumber());

						creditclasRoundOff.setCompanyMst(salesTransHdr.getCompanyMst());

						creditclasRoundOff.setTransDate(voucherDate);

						creditclasRoundOff.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());

						creditclasRoundOff = creditClassFCRepo.saveAndFlush(creditclasRoundOff);

					}

				}
			}

			eventBus.post(accountClass);
			
			/*
			 * Forward accounting to Server
			 * 
			 */

			/*
			 * List<AccountClassfc> accountClassToForwardList = accountClassFCRepo
			 * .findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);
			 * 
			 * if (accountClassToForwardList.size() > 0) {
			 * 
			 * Iterator iterForward = accountClassToForwardList.iterator(); while
			 * (iterForward.hasNext()) { AccountClass accountClassForward = (AccountClass)
			 * iterForward.next();
			 * 
			 * 
			 * restserverApplication.insertIntoStockLmsQueue(accountClassForward.
			 * getSourceVoucherNumber(), accountClassForward.getTransDate(),
			 * accountClassForward.getId(), accountClassForward.getCompanyMst(),
			 * accountClassForward.getBrachCode(), "forwardAccounting","NO",
			 * accountClassForward.getCompanyMst());
			 * 
			 * 
			 * try {
			 * 
			 * AccountMessage accountMessage = restserverApplication
			 * .getAccountMessageEntity(accountClassForward); eventBus.post(accountMessage);
			 * 
			 * } catch (Exception e) {
			 * 
			 * restserverApplication.insertIntoStockLmsQueue(accountClassForward.
			 * getSourceVoucherNumber(), accountClassForward.getTransDate(),
			 * accountClassForward.getId(), accountClassForward.getCompanyMst(),
			 * accountClassForward.getBrachCode(), "forwardAccounting","NO"); }
			 * 
			 * 
			 * }
			 * 
			 * }
			 */

		}

	}

}
