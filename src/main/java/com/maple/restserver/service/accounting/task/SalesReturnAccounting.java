package com.maple.restserver.service.accounting.task;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.client.ExternalTaskClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.DayBook;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesReturnDtl;
import com.maple.restserver.entity.SalesReturnHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.message.entity.AccountMessage;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.repository.DayBookRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.accounting.service.AccountingService;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesRetunDtlRepository;
import com.maple.restserver.repository.SalesReturnHdrRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.resource.PaymentHdrResource;
import com.maple.restserver.service.AccountHeadsService;
import com.maple.restserver.service.InternetCheckService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Transactional
@Service
public class SalesReturnAccounting {
	
	

	private static final Logger logger = LoggerFactory.getLogger(SalesReturnAccounting.class);

	EventBus eventBus = EventBusFactory.getEventBus();
	
	@Autowired
	private CompanyMstRepository companyMstRepository;

	@Autowired
	private SalesReturnHdrRepository salesReturnHdrRepository;


	@Autowired
	private SalesRetunDtlRepository salesRetunDtlRepository;

	@Autowired
	private AccountClassRepository accountClassRepo;

	@Autowired
	private AccountHeadsRepository accountHeadsRepo;

	@Autowired
	private CreditClassRepository creditClassRepo;

	@Autowired
	private DebitClassRepository debitClassRepo;

	@Autowired
	private LedgerClassRepository ledgerClassRepo;

 

	@Autowired
	private DayBookRepository dayBookRepository;
	
 

	 @Autowired
	 AccountingService accountingService;
	 
	 @Autowired
	 AccountHeadsService  accountHeadsService;
	 
	public SalesReturnAccounting() {

	}
 
 
	
	@Transactional(rollbackFor = PartialAccountingException.class)
	public void execute(String voucherNumber, Date voucherDate, String salesTransHdrId, CompanyMst companyMst) 
			throws PartialAccountingException{

		int i=0;
		 
		BigDecimal zero = new BigDecimal("0");

		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());
		
		/*
		 * Clear Previous transaction if any
		 */

		
 

 
		/*
		 * Deletion to be called separately for  Editing 
		 */
 
	 //accountClassRepo.deleteBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

		List<AccountClass> accList = accountClassRepo.findBySourceParentId(salesTransHdrId);
		if(accList.size()>1)
		{
			throw new PartialAccountingException(salesTransHdrId);
		}
		
		else if(accList.size()==1)
		{
			accountClassRepo.deleteBySourceParentId(salesTransHdrId);
		}
	
	
	 
	 
		List<SalesReturnHdr> salesTransHdrList = salesReturnHdrRepository.findByVoucherNumberAndVoucherDate(voucherNumber,
				sqlDate);
		Iterator itr = salesTransHdrList.iterator();
		AccountClass accountClass = new AccountClass();
		// Only One record is expected
		while (itr.hasNext()) {

			SalesReturnHdr salesReturnHdr = (SalesReturnHdr) itr.next();
			
			BigDecimal totalCredit = salesRetunDtlRepository.findSumCrAmountSalesDtl(salesReturnHdr.getId());

			if(null==totalCredit) {
				continue;
			}
			totalCredit = totalCredit.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		 
			
		 
			accountClass.setSourceParentId(salesReturnHdr.getId());
			accountClass.setTransDate(salesReturnHdr.getVoucherDate());
			accountClass.setSourceVoucherNumber(salesReturnHdr.getVoucherNumber());
			accountClass.setVoucherType("SALES RETURN");
			accountClass.setTotalDebit(totalCredit);
			accountClass.setBrachCode(salesReturnHdr.getBranchCode());

			accountClass.setTotalCredit(totalCredit);

			accountClass.setCompanyMst(salesReturnHdr.getCompanyMst());

			accountClass = accountClassRepo.save(accountClass);


			/*
			 * 1. Total sales amount credited to CUSTOMER
			 * 2. Tax amount debited to CGST/SGST Tax account
			 * 3. Assessable amount debited to SALES RETURN
			 */
			
			
			
			
			/*
			 * Account CREDIT SECTION
			 */
			
			CreditClass creditclassCustomer = new CreditClass();
				

			creditclassCustomer.setAccountId(salesReturnHdr.getAccountHeads().getId());
			
			creditclassCustomer.setAccountClass(accountClass);
			creditclassCustomer.setCrAmount(totalCredit);

			creditclassCustomer.setRemark(salesReturnHdr.getVoucherNumber());

			creditclassCustomer.setCompanyMst(salesReturnHdr.getCompanyMst());
			creditclassCustomer.setTransDate(voucherDate);

			creditclassCustomer.setSourceVoucherNumber(salesReturnHdr.getVoucherNumber());
			creditclassCustomer = creditClassRepo.save(creditclassCustomer);
			
			LedgerClass ledgerClassCr = new LedgerClass();
			ledgerClassCr.setAccountClass(accountClass);
			ledgerClassCr.setAccountId(creditclassCustomer.getAccountId());
			ledgerClassCr.setDebitAmount(zero);
			ledgerClassCr.setCreditAmount(creditclassCustomer.getCrAmount());
			
			ledgerClassCr.setTransDate(salesReturnHdr.getVoucherDate());
			ledgerClassCr.setCompanyMst(salesReturnHdr.getCompanyMst());

			ledgerClassCr.setBranchCode(accountClass.getBrachCode());
			ledgerClassCr.setRemark("Sales Return " + accountClass.getSourceVoucherNumber() );

			ledgerClassCr.setSourceVoucherId(salesReturnHdr.getVoucherNumber());

			ledgerClassCr.setRemarkAccountName(ledgerClassCr.getRemark());
			
			
			//ledgerClassRepo.saveAndFlush(ledgerClassCr);
			
			accountingService.saveLedger(ledgerClassCr);

			
				
			
			List<SalesReturnDtl> salesDtlList = salesRetunDtlRepository.findBySalesReturnHdrId(salesReturnHdr.getId());
			Iterator iter = salesDtlList.iterator();

			while (iter.hasNext()) {
				SalesReturnDtl salesReturnDtl = (SalesReturnDtl) iter.next();

				Double AcessibleValue = salesRetunDtlRepository.findSumAcessibleAmountItemWise(salesReturnHdr.getId(),
						salesReturnDtl.getItemId());
				AccountHeads accountHeads = null;
				accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId("SALES RETURN", companyMst.getId());
				
				if(null==accountHeads) {
					 accountHeadsService.addAccountHeadSalesReturn(companyMst.getId());
					 accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId("SALES RETURN", companyMst.getId());

				}
				

				DebitClass debitClass = new DebitClass();

				debitClass.setAccountId(accountHeads.getId());
				debitClass.setAccountClass(accountClass);

				BigDecimal bdAcessibleValue = new BigDecimal(AcessibleValue);
				bdAcessibleValue = bdAcessibleValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				debitClass.setDrAmount(bdAcessibleValue);
				debitClass.setRemark(salesReturnHdr.getVoucherNumber());

				debitClass.setCompanyMst(salesReturnHdr.getCompanyMst());

				debitClass.setItemId(salesReturnDtl.getItemId());
				debitClass.setRate(salesReturnDtl.getRate());
				debitClass.setQty(salesReturnDtl.getQty());

				debitClass.setTransDate(voucherDate);

				debitClass.setSourceVoucherNumber(salesReturnHdr.getVoucherNumber());

				debitClass = debitClassRepo.save(debitClass);

				LedgerClass ledgerClassDr = new LedgerClass();
				ledgerClassDr.setAccountClass(accountClass);
				ledgerClassDr.setAccountId(debitClass.getAccountId());
				ledgerClassDr.setDebitAmount(debitClass.getDrAmount());
				ledgerClassDr.setCreditAmount(zero);
				
				ledgerClassDr.setTransDate(salesReturnHdr.getVoucherDate());
				ledgerClassDr.setCompanyMst(salesReturnHdr.getCompanyMst());

				ledgerClassDr.setBranchCode(accountClass.getBrachCode());
				ledgerClassDr.setRemark("Sales Return " + accountClass.getSourceVoucherNumber() + salesReturnDtl.getItemName());

				ledgerClassDr.setSourceVoucherId(salesReturnHdr.getVoucherNumber());

				ledgerClassDr.setRemarkAccountName(ledgerClassDr.getRemark());
				
				
				//ledgerClassRepo.saveAndFlush(ledgerClassDr);
				accountingService.saveLedger(ledgerClassDr);

				// Account Taxwise

				Double objList = salesRetunDtlRepository.findTotalTaxSumSalesDtlItemWise(salesReturnHdr.getId(), salesReturnDtl.getItemId());

				double taxAmount = objList;
				
				if(taxAmount > 0 ) {
				BigDecimal cgstAmount = new BigDecimal(taxAmount / 2);
				cgstAmount = cgstAmount.setScale(2, BigDecimal.ROUND_CEILING);

				AccountHeads accountHeadsCgst = accountHeadsRepo.findByAccountNameAndCompanyMstId("CGST ACCOUNT", companyMst.getId());
				
				//find existing CGST Recors
				List<DebitClass>debitclassCgstList = debitClassRepo.findByAccountClassAndAccountId(accountClass,accountHeadsCgst.getId());
				DebitClass  debitclassCgst = null;
				if(debitclassCgstList.size()>0) {
					debitclassCgst = debitclassCgstList.get(0);

					debitclassCgst.setAccountId(accountHeadsCgst.getId());
					debitclassCgst.setAccountClass(accountClass);
					debitclassCgst.setDrAmount(debitclassCgst.getDrAmount().add(cgstAmount));

					debitclassCgst.setRemark(salesReturnHdr.getVoucherNumber());

					debitclassCgst.setCompanyMst(salesReturnHdr.getCompanyMst());
					debitclassCgst.setTransDate(voucherDate);

					debitclassCgst.setSourceVoucherNumber(salesReturnHdr.getVoucherNumber());
				}
				else {
					   debitclassCgst = new DebitClass();
					

					   debitclassCgst.setAccountId(accountHeadsCgst.getId());
					   debitclassCgst.setAccountClass(accountClass);
					   debitclassCgst.setDrAmount(cgstAmount);

					   debitclassCgst.setRemark(salesReturnHdr.getVoucherNumber());

					   debitclassCgst.setCompanyMst(salesReturnHdr.getCompanyMst());
					   debitclassCgst.setTransDate(voucherDate);

					   debitclassCgst.setSourceVoucherNumber(salesReturnHdr.getVoucherNumber());
				}

				debitclassCgst = debitClassRepo.saveAndFlush(debitclassCgst);

				LedgerClass ledgerClassCgstDr = null;
				
				List<LedgerClass>ledgerClassCgstDrList = ledgerClassRepo.findByAccountClassIdAndAccountId(accountClass.getId(), debitclassCgst.getAccountId());
				
				if(ledgerClassCgstDrList.size()>0) {
					ledgerClassCgstDr = ledgerClassCgstDrList.get(0);
					ledgerClassCgstDr.setAccountClass(accountClass);
					ledgerClassCgstDr.setAccountId(debitclassCgst.getAccountId());
					ledgerClassCgstDr.setCreditAmount(zero);
					//ledgerClassCgstDr.setCreditAmount(ledgerClassCgstDr.getCreditAmount().add(debitclassCgst.getDrAmount()));
					ledgerClassCgstDr.setDebitAmount(ledgerClassCgstDr.getDebitAmount().add(debitclassCgst.getDrAmount()));
					//ledgerClassCgstDr.setDebitAmount(zero);

					ledgerClassCgstDr.setTransDate(salesReturnHdr.getVoucherDate());

					ledgerClassCgstDr.setCompanyMst(salesReturnHdr.getCompanyMst());

					ledgerClassCgstDr.setBranchCode(accountClass.getBrachCode());
					ledgerClassCgstDr.setRemark(accountClass.getSourceVoucherNumber());
					ledgerClassCgstDr.setSourceVoucherId(salesReturnHdr.getVoucherNumber());
				}else {
					ledgerClassCgstDr =new LedgerClass();
					ledgerClassCgstDr.setAccountClass(accountClass);
					ledgerClassCgstDr.setAccountId(debitclassCgst.getAccountId());
					//ledgerClassCgstDr.setCreditAmount(debitclassCgst.getDrAmount());
					//ledgerClassCgstDr.setDebitAmount(zero);

					ledgerClassCgstDr.setCreditAmount(zero);
					ledgerClassCgstDr.setDebitAmount(debitclassCgst.getDrAmount());

					
					ledgerClassCgstDr.setTransDate(salesReturnHdr.getVoucherDate());

					ledgerClassCgstDr.setCompanyMst(salesReturnHdr.getCompanyMst());

					ledgerClassCgstDr.setBranchCode(accountClass.getBrachCode());
					ledgerClassCgstDr.setRemark(accountClass.getSourceVoucherNumber());
					ledgerClassCgstDr.setSourceVoucherId(salesReturnHdr.getVoucherNumber());
				}
				
				ledgerClassCgstDr.setRemarkAccountName(ledgerClassCgstDr.getRemark());

				//ledgerClassRepo.saveAndFlush(ledgerClassCgstDr);
				accountingService.saveLedger(ledgerClassCgstDr);

				// ------------------ SGST
				AccountHeads accountHeadsSgst = accountHeadsRepo.findByAccountNameAndCompanyMstId("SGST ACCOUNT", companyMst.getId());
			
				
				List<DebitClass>debitclasSGgstList = debitClassRepo.findByAccountClassAndAccountId(accountClass,accountHeadsSgst.getId());
				DebitClass  debitclasSGgst = null;
				if(debitclasSGgstList.size()>0) {
					debitclasSGgst = debitclasSGgstList.get(0);

				

					debitclasSGgst.setAccountId(accountHeadsSgst.getId());
					debitclasSGgst.setAccountClass(accountClass);
					debitclasSGgst.setDrAmount(debitclasSGgst.getDrAmount().add(cgstAmount));

					debitclasSGgst.setRemark(salesReturnHdr.getVoucherNumber());

					debitclasSGgst.setCompanyMst(salesReturnHdr.getCompanyMst());

					debitclasSGgst.setTransDate(voucherDate);
					debitclasSGgst.setSourceVoucherNumber(salesReturnHdr.getVoucherNumber());
				}else {
					debitclasSGgst = new DebitClass();
					debitclasSGgst.setAccountId(accountHeadsSgst.getId());
					  debitclasSGgst.setAccountClass(accountClass);
					  debitclasSGgst.setDrAmount(cgstAmount);

					  debitclasSGgst.setRemark(salesReturnHdr.getVoucherNumber());

					  debitclasSGgst.setCompanyMst(salesReturnHdr.getCompanyMst());

					  debitclasSGgst.setTransDate(voucherDate);
					  debitclasSGgst.setSourceVoucherNumber(salesReturnHdr.getVoucherNumber());

				}
				debitclasSGgst = debitClassRepo.saveAndFlush(debitclasSGgst);

				
	List<LedgerClass>ledgerClassSgstDrList = ledgerClassRepo.findByAccountClassIdAndAccountId(accountClass.getId(), debitclasSGgst.getAccountId());
	LedgerClass ledgerClassSgstDr = null;
	
				if(ledgerClassSgstDrList.size()>0) {
					ledgerClassSgstDr = ledgerClassSgstDrList.get(0);
					
					ledgerClassSgstDr.setAccountClass(accountClass);
					ledgerClassSgstDr.setAccountId(debitclasSGgst.getAccountId());
					ledgerClassSgstDr.setCreditAmount(zero);
					ledgerClassSgstDr.setDebitAmount(ledgerClassSgstDr.getDebitAmount().add(debitclasSGgst.getDrAmount()));

					ledgerClassSgstDr.setTransDate(salesReturnHdr.getVoucherDate());

					ledgerClassSgstDr.setCompanyMst(salesReturnHdr.getCompanyMst());

					ledgerClassSgstDr.setBranchCode(accountClass.getBrachCode());
					ledgerClassSgstDr.setRemark(accountClass.getSourceVoucherNumber());

					ledgerClassSgstDr.setSourceVoucherId(salesReturnHdr.getVoucherNumber());
	
					
					
				}else {
					ledgerClassSgstDr = new LedgerClass();
					ledgerClassSgstDr.setAccountClass(accountClass);
					ledgerClassSgstDr.setAccountId(debitclasSGgst.getAccountId());
					ledgerClassSgstDr.setCreditAmount(zero);
					ledgerClassSgstDr.setDebitAmount(debitclasSGgst.getDrAmount() );

					ledgerClassSgstDr.setTransDate(salesReturnHdr.getVoucherDate());

					ledgerClassSgstDr.setCompanyMst(salesReturnHdr.getCompanyMst());

					ledgerClassSgstDr.setBranchCode(accountClass.getBrachCode());
					ledgerClassSgstDr.setRemark(accountClass.getSourceVoucherNumber());

					ledgerClassSgstDr.setSourceVoucherId(salesReturnHdr.getVoucherNumber());
				}
				

				//ledgerClassRepo.saveAndFlush(ledgerClassSgstDr);
				
				ledgerClassSgstDr.setRemarkAccountName(ledgerClassSgstDr.getRemark());
				
				accountingService.saveLedger(ledgerClassSgstDr);
				}

				// If Cess Item ?

				if (!salesReturnHdr.getSalesMode().equalsIgnoreCase("B2B")) {
					BigDecimal CessAmount = new BigDecimal(salesRetunDtlRepository.findSumCessAmountItemWise(salesReturnHdr.getId(), salesReturnDtl.getItemId()));

					CessAmount = CessAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					if (CessAmount.doubleValue() > 0) {

						AccountHeads floodCessaccountHeads = null;
						floodCessaccountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId("KERALA FLOOD CESS",
								companyMst.getId());
						
						
						//				
						//List<DebitClass>debitclasSGgstList = debitClassRepo.findByAccountClassAndAccountId(accountClass,accountHeadsSgst.getId());
						List<DebitClass>debitclasKFCList = debitClassRepo.findByAccountClassAndAccountId(accountClass, floodCessaccountHeads.getId());
						DebitClass  debitclasKFC = null;
						 
						if(debitclasKFCList.size()>0) {
							debitclasKFC = debitclasKFCList.get(0);
							

							debitclasKFC.setAccountId(floodCessaccountHeads.getId());
							debitclasKFC.setAccountClass(accountClass);
							debitclasKFC.setDrAmount(debitclasKFC.getDrAmount().add(CessAmount));

							debitclasKFC.setRemark(salesReturnHdr.getVoucherNumber());

							debitclasKFC.setCompanyMst(salesReturnHdr.getCompanyMst());

							debitclasKFC.setTransDate(voucherDate);

							debitclasKFC.setSourceVoucherNumber(salesReturnHdr.getVoucherNumber());

						}else {
							
							debitclasKFC = new DebitClass();
							debitclasKFC.setAccountId(floodCessaccountHeads.getId());
							debitclasKFC.setAccountClass(accountClass);
							debitclasKFC.setDrAmount(CessAmount);

							debitclasKFC.setRemark(salesReturnHdr.getVoucherNumber());

							debitclasKFC.setCompanyMst(salesReturnHdr.getCompanyMst());

							debitclasKFC.setTransDate(voucherDate);

							debitclasKFC.setSourceVoucherNumber(salesReturnHdr.getVoucherNumber());

						}



			
						debitclasKFC = debitClassRepo.saveAndFlush(debitclasKFC);

						LedgerClass ledgerClassKFC = null;
						List<LedgerClass>ledgerClassKFCList = ledgerClassRepo.findByAccountClassIdAndAccountId(accountClass.getId(), debitclasKFC.getAccountId());
						LedgerClass ledgerClassSgstCr = null;
						
									if(ledgerClassKFCList.size()>0) {
										ledgerClassKFC = ledgerClassKFCList.get(0);
										ledgerClassKFC.setAccountClass(accountClass);
										ledgerClassKFC.setAccountId(debitclasKFC.getAccountId());
										ledgerClassKFC.setDebitAmount(ledgerClassKFC.getCreditAmount().add(debitclasKFC.getDrAmount()));
										ledgerClassKFC.setCreditAmount(zero);
										
										ledgerClassKFC.setTransDate(salesReturnHdr.getVoucherDate());

										ledgerClassKFC.setCompanyMst(salesReturnHdr.getCompanyMst());
										ledgerClassKFC.setBranchCode(accountClass.getBrachCode());
										ledgerClassKFC.setRemark(accountClass.getSourceVoucherNumber());

										ledgerClassKFC.setSourceVoucherId(salesReturnHdr.getVoucherNumber());

									}else {
										
										ledgerClassKFC = new LedgerClass();
										ledgerClassKFC.setAccountClass(accountClass);
										ledgerClassKFC.setAccountId(debitclasKFC.getAccountId());
										ledgerClassKFC.setDebitAmount(debitclasKFC.getDrAmount());
										ledgerClassKFC.setCreditAmount(zero);
										
										ledgerClassKFC.setTransDate(salesReturnHdr.getVoucherDate());

										ledgerClassKFC.setCompanyMst(salesReturnHdr.getCompanyMst());
										ledgerClassKFC.setBranchCode(accountClass.getBrachCode());
										ledgerClassKFC.setRemark(accountClass.getSourceVoucherNumber());

										ledgerClassKFC.setSourceVoucherId(salesReturnHdr.getVoucherNumber());

									}
									ledgerClassKFC.setRemarkAccountName(ledgerClassKFC.getRemark());
					
						//ledgerClassRepo.saveAndFlush(ledgerClassKFC);
						
						accountingService.saveLedger(ledgerClassKFC);
					}
				}

			}

			/*
			 * Set Round Off
			 * 
			 */

			List<AccountClass> accountClassList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iterRe = accountClassList.iterator();
				while (iterRe.hasNext()) {
					AccountClass accountClassRe = (AccountClass) iterRe.next();

					List<CreditClass> creditClassList = creditClassRepo.findByAccountClassId(accountClass.getId());

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClassId(accountClass.getId());

					double debittotal =0.0;
					double credittotal =0.0;
					Iterator drIter = debitClassList.iterator();
					while (drIter.hasNext()) {
						DebitClass debitClass= (DebitClass) drIter.next();
						debittotal = debittotal + debitClass.getDrAmount().doubleValue();
						
						
					}
					
					Iterator crIter = creditClassList.iterator();
					while (crIter.hasNext()) {
						CreditClass creditClass= (CreditClass) crIter.next();
						credittotal = credittotal + creditClass.getCrAmount().doubleValue();
						
						
					}
					
 						
					}
					  
				}
			}

			/*
			 * Forward accounting to Server
			 * 
			 */
		
		eventBus.post(accountClass);
		
			
			List<AccountClass> accountClassToForwardList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassToForwardList.size() > 0) {
				 

				Iterator iterForward = accountClassToForwardList.iterator();
				while (iterForward.hasNext()) {
					AccountClass accountClassForward = (AccountClass) iterForward.next();
					
					
					Map<String, Object> variables = new HashMap<String, Object>();

					 

					variables.put("voucherNumber", accountClassForward.getSourceVoucherNumber());
					variables.put("voucherDate", accountClassForward.getTransDate());
					variables.put("inet", 0);
					variables.put("id", accountClassForward.getId());
					variables.put("isbranchsales", "NO");
					variables.put("companyid", accountClassForward.getCompanyMst());
					variables.put("REST", 0);
					 
					 

					variables.put("WF", "forwardAccounting");
					
					
					
					LmsQueueMst lmsQueueMst = new LmsQueueMst();

				 
					eventBus.post(variables);
					
					
				}

			}
			
			
			
			
		}

	}


