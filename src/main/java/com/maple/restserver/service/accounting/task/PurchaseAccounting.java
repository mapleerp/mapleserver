package com.maple.restserver.service.accounting.task;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.RestserverApplication;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayBook;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SummaryPurchaseDtl;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.message.entity.AccountMessage;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DayBookRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.accounting.service.AccountingService;
import com.maple.restserver.accounting.service.InventoryLedgerMstService;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.resource.PaymentHdrResource;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.InternetCheckService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Transactional
@Service
public class PurchaseAccounting {

	private static final Logger logger = LoggerFactory.getLogger(PaymentHdrResource.class);
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	private CompanyMstRepository companyMstRepository;

	@Autowired
	private PurchaseHdrRepository purchaseHdrRepo;

	@Autowired
	private PurchaseDtlRepository purchaseDtlRepo;

	@Autowired
	private AccountClassRepository accountClassRepo;

	@Autowired
	private AccountHeadsRepository accountHeadsRepo;

	@Autowired
	private CreditClassRepository creditClassRepo;

	@Autowired
	private DebitClassRepository debitClassRepo;

	@Autowired
	private LedgerClassRepository ledgerClassRepo;

	@Autowired
	private DayBookRepository dayBookRepository;

	@Autowired
	RestserverApplication restserverApplication;

	@Autowired
	AccountingService accountingService;

	@Autowired
	InventoryLedgerMstService inventoryledgerMstService;

	@Autowired
	SaveAndPublishService saveAndPublishService;

	public PurchaseAccounting() {

	}

	@Transactional(rollbackFor = PartialAccountingException.class)

	public void execute(String voucherNumber, Date voucherDate, String purchaseHdrId, CompanyMst comnpanyMst)
			throws PartialAccountingException {

		BigDecimal zero = new BigDecimal("0");

		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		/*
		 * Deletion to be called separately for Editing
		 */

		// accountClassRepo.deleteBySourceVoucherNumberAndTransDate(voucherNumber,
		// voucherDate);
		List<AccountClass> accList = accountClassRepo.findBySourceParentId(purchaseHdrId);
		if (accList.size() > 1) {
			throw new PartialAccountingException(purchaseHdrId);
		}

		else if (accList.size() == 1) {
			accountClassRepo.deleteBySourceParentId(purchaseHdrId);
		}
		AccountClass accountClass = new AccountClass();
		List<PurchaseHdr> purchaseHdrList = purchaseHdrRepo.findByVoucherNumberAndVoucherDate(voucherNumber,
				voucherDate);
		Iterator itr = purchaseHdrList.iterator();
		// Only One record is expected
		while (itr.hasNext()) {
			PurchaseHdr purchaseHdr = (PurchaseHdr) itr.next();

			List<Object> objList = purchaseDtlRepo.findSumPurchaseDetailTaxwise(purchaseHdr.getId());

			for (int i = 0; i < objList.size(); i++) {
				Object[] objAray = (Object[]) objList.get(i);

				SummaryPurchaseDtl summary = new SummaryPurchaseDtl();
				summary.setTotalAmount((Double) objAray[0]);
				summary.setTotalQty((Double) objAray[1]);
				summary.setTotalDiscount((Double) objAray[2]);
				summary.setTotalTax((Double) objAray[3]);
				summary.setTotalCessAmt((Double) objAray[4]);
				summary.setTaxRate((Double) objAray[5]);

				accountClass.setSourceParentId(purchaseHdr.getId());
				accountClass.setSourceVoucherNumber(purchaseHdr.getVoucherNumber());
				accountClass.setVoucherType("PURCHASE");

				BigDecimal totalCredit = new BigDecimal(summary.getTotalAmount());
				totalCredit = totalCredit.setScale(5, BigDecimal.ROUND_HALF_EVEN);

				BigDecimal totalDebit = new BigDecimal(purchaseHdr.getInvoiceTotal());
				totalDebit = totalDebit.setScale(5, BigDecimal.ROUND_HALF_EVEN);

				accountClass.setTotalCredit(totalCredit);
				accountClass.setTotalDebit(totalDebit);
				accountClass.setTransDate(purchaseHdr.getVoucherDate());

				accountClass.setBrachCode(purchaseHdr.getBranchCode());
				accountClass.setCompanyMst(purchaseHdr.getCompanyMst());

				// accountClass = accountClassRepo.save(accountClass);

				accountClass = saveAndPublishService.saveAccountClass(accountClass, purchaseHdr.getBranchCode());

				logger.info("Saving Account Class " + accountClass.getId());

				/*
				 * Find Total Acessible Value will be Debited to Purchase Account
				 * 
				 */

				String taxPercent = summary.getTaxRate() + "";

				String purcchaseAccount = "PURCHASE ACCOUNTS";

				AccountHeads accountHeads = null;
				accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId(purcchaseAccount, comnpanyMst.getId());
				if (null == accountHeads || null == accountHeads.getId()) {
					// Create Purchase Account

					AccountHeads pareAccount = accountHeadsRepo.findByAccountNameAndCompanyMstId("PURCHASE ACCOUNTS",
							comnpanyMst.getId());

					accountHeads = new AccountHeads();
					accountHeads.setAccountName(purcchaseAccount);
					accountHeads.setCompanyMst(comnpanyMst);
					accountHeads.setGroupOnly("N");
					accountHeads.setMachineId("MAC");
					accountHeads.setTaxId("");
					accountHeads.setParentId(pareAccount.getId());
					accountHeads.setVoucherType("");

					VoucherNumber voucherNo = voucherService.generateInvoice("ACCOUNTID", comnpanyMst.getId());
					String accountId = purchaseHdr.getBranchCode() + voucherNo.getCode();

					accountHeads.setId(accountId);
					// accountHeadsRepo.save(accountHeads);

					saveAndPublishService.saveAccountHeads(accountHeads, purchaseHdr.getBranchCode());

				}

			

				// Tax CGST
				accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId("CGST ACCOUNT", comnpanyMst.getId());

				DebitClass debitClassCgst = new DebitClass();

				debitClassCgst.setAccountId(accountHeads.getId());
				debitClassCgst.setAccountClass(accountClass);

				BigDecimal DrAmount = new BigDecimal(summary.getTotalTax() / 2);
				DrAmount = DrAmount.setScale(10, BigDecimal.ROUND_HALF_EVEN);

				debitClassCgst.setDrAmount(DrAmount);

				debitClassCgst.setRemark(purchaseHdr.getVoucherNumber());

				debitClassCgst.setCompanyMst(purchaseHdr.getCompanyMst());
				debitClassCgst = debitClassRepo.save(debitClassCgst);

				LedgerClass ledgerClassCgst = new LedgerClass();
				ledgerClassCgst.setAccountClass(accountClass);
				ledgerClassCgst.setAccountId(debitClassCgst.getAccountId());
				ledgerClassCgst.setCreditAmount(zero);
				ledgerClassCgst.setDebitAmount(debitClassCgst.getDrAmount());
				ledgerClassCgst.setTransDate(purchaseHdr.getVoucherDate());
				ledgerClassCgst.setSourceVoucherId(purchaseHdr.getVoucherNumber());
				ledgerClassCgst.setSourceParentId(purchaseHdr.getVoucherNumber());

				ledgerClassCgst.setCompanyMst(purchaseHdr.getCompanyMst());

				ledgerClassCgst.setBranchCode(accountClass.getBrachCode());
				ledgerClassCgst.setRemark("Purchase - CGST " + accountClass.getSourceVoucherNumber());

				ledgerClassCgst.setRemarkAccountName("PURCHASE " + accountClass.getSourceVoucherNumber());
				// ledgerClassRepo.save(ledgerClassCgst);

				// accountingService.saveLedger(ledgerClassCgst);

				saveAndPublishService.saveLedgerClass(ledgerClassCgst, purchaseHdr.getBranchCode());

				// SGST Taxwise

				accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId("SGST ACCOUNT", comnpanyMst.getId());

				DebitClass debitClassSgst = new DebitClass();

				debitClassSgst.setAccountId(accountHeads.getId());
				debitClassSgst.setAccountClass(accountClass);

				BigDecimal SgDrAmount = new BigDecimal(summary.getTotalTax() / 2);
				SgDrAmount = SgDrAmount.setScale(10, BigDecimal.ROUND_HALF_EVEN);

				debitClassSgst.setDrAmount(SgDrAmount);
				debitClassSgst.setRemark(purchaseHdr.getVoucherNumber());

				debitClassSgst.setCompanyMst(purchaseHdr.getCompanyMst());
				// debitClassSgst = debitClassRepo.save(debitClassSgst);

				debitClassSgst = saveAndPublishService.saveDebitClass(debitClassSgst, purchaseHdr.getBranchCode());

				LedgerClass ledgerClassSgst = new LedgerClass();
				ledgerClassSgst.setAccountClass(accountClass);
				ledgerClassSgst.setAccountId(debitClassSgst.getAccountId());
				ledgerClassSgst.setCreditAmount(zero);
				ledgerClassSgst.setDebitAmount(debitClassSgst.getDrAmount());
				ledgerClassSgst.setTransDate(purchaseHdr.getVoucherDate());

				ledgerClassSgst.setSourceVoucherId(purchaseHdr.getVoucherNumber());
				ledgerClassSgst.setSourceParentId(purchaseHdr.getVoucherNumber());

				ledgerClassSgst.setCompanyMst(purchaseHdr.getCompanyMst());

				ledgerClassSgst.setBranchCode(accountClass.getBrachCode());
				ledgerClassSgst.setRemark("Purchase - SGST " + accountClass.getSourceVoucherNumber());

				ledgerClassSgst.setRemarkAccountName("PURCHASE " + accountClass.getSourceVoucherNumber());

				// ledgerClassRepo.save(ledgerClassSgst);

				// accountingService.saveLedger(ledgerClassSgst);

				saveAndPublishService.saveLedgerClass(ledgerClassSgst, purchaseHdr.getBranchCode());

				// ------------------ SGST
				// Get Supplier Name
				Optional<AccountHeads> accountHeadsList = accountHeadsRepo.findById(purchaseHdr.getSupplierId());
				AccountHeads accountHeads2 = accountHeadsList.get();

				accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId(accountHeads2.getAccountName(),
						comnpanyMst.getId());
				CreditClass creditclas = new CreditClass();

				creditclas.setAccountId(accountHeads.getId());
				creditclas.setAccountClass(accountClass);

				BigDecimal CrAmount = new BigDecimal(summary.getTotalAmount() + summary.getTotalTax());
				CrAmount = CrAmount.setScale(10, BigDecimal.ROUND_HALF_EVEN);

				creditclas.setCrAmount(CrAmount);

				creditclas.setRemark(purchaseHdr.getVoucherNumber());

				creditclas.setCompanyMst(purchaseHdr.getCompanyMst());
				// creditclas = creditClassRepo.save(creditclas);

				creditclas = saveAndPublishService.saveCreditClass(creditclas, purchaseHdr.getBranchCode());

				LedgerClass ledgerClassSupCr = new LedgerClass();
				ledgerClassSupCr.setAccountClass(accountClass);
				ledgerClassSupCr.setAccountId(creditclas.getAccountId());
				ledgerClassSupCr.setCreditAmount(creditclas.getCrAmount());
				ledgerClassSupCr.setDebitAmount(zero);

				ledgerClassSupCr.setSourceVoucherId(purchaseHdr.getVoucherNumber());
				ledgerClassSupCr.setSourceParentId(purchaseHdr.getVoucherNumber());

				ledgerClassSupCr.setTransDate(purchaseHdr.getVoucherDate());

				ledgerClassSupCr.setCompanyMst(purchaseHdr.getCompanyMst());

				ledgerClassSupCr.setBranchCode(accountClass.getBrachCode());
				ledgerClassSupCr.setRemark("Purchase " + accountClass.getSourceVoucherNumber());

				ledgerClassSupCr.setRemarkAccountName("PURCHASE " + accountClass.getSourceVoucherNumber());

				// ledgerClassRepo.save(ledgerClassSupCr);
				// accountingService.saveLedger(ledgerClassSupCr);

				saveAndPublishService.saveLedgerClass(ledgerClassSupCr, purchaseHdr.getBranchCode());

			}
			

			String purcchaseAccount = "PURCHASE ACCOUNTS";

			AccountHeads accountHeads = null;
			accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId(purcchaseAccount, comnpanyMst.getId());
			
			List<PurchaseDtl> purchaseDtlList = purchaseDtlRepo.findByPurchaseHdrId(purchaseHdr.getId());
			Iterator iter = purchaseDtlList.iterator();

			while (iter.hasNext()) {
				PurchaseDtl purchaseDtl = (PurchaseDtl) iter.next();

				Double AcessibleValue = purchaseDtlRepo.findSumAcessibleAmountItemWise(purchaseHdr.getId(),
						purchaseDtl.getItemId(), purchaseDtl.getId());

				DebitClass debitClass = new DebitClass();

				debitClass.setAccountId(accountHeads.getId());
				debitClass.setAccountClass(accountClass);

				BigDecimal bdAcessibleValue = new BigDecimal(AcessibleValue);
				bdAcessibleValue = bdAcessibleValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				debitClass.setDrAmount(bdAcessibleValue);
				debitClass.setRemark(purchaseHdr.getVoucherNumber());

				debitClass.setCompanyMst(purchaseHdr.getCompanyMst());

				debitClass.setItemId(purchaseDtl.getItemId());
				debitClass.setRate(purchaseDtl.getPurchseRate());
				debitClass.setQty(purchaseDtl.getQty());

				debitClass.setTransDate(voucherDate);

				debitClass.setSourceVoucherNumber(purchaseHdr.getVoucherNumber());
				debitClass.setRemark(purchaseHdr.getVoucherNumber());

				debitClass.setCompanyMst(purchaseHdr.getCompanyMst());

				// debitClass = debitClassRepo.save(debitClass);
				debitClass = saveAndPublishService.saveDebitClass(debitClass, purchaseHdr.getBranchCode());

				LedgerClass ledgerClassDr = new LedgerClass();
				ledgerClassDr.setAccountClass(accountClass);
				ledgerClassDr.setAccountId(debitClass.getAccountId());
				ledgerClassDr.setDebitAmount(debitClass.getDrAmount());
				ledgerClassDr.setCreditAmount(zero);

				ledgerClassDr.setTransDate(purchaseHdr.getVoucherDate());
				ledgerClassDr.setCompanyMst(purchaseHdr.getCompanyMst());

				ledgerClassDr.setBranchCode(accountClass.getBrachCode());
				ledgerClassDr.setRemark("Purchase" + accountClass.getSourceVoucherNumber());

				ledgerClassDr.setRemarkAccountName(ledgerClassDr.getRemark());

				ledgerClassDr.setSourceVoucherId(purchaseHdr.getVoucherNumber());
				ledgerClassDr.setSourceParentId(purchaseHdr.getVoucherNumber());

				// ledgerClassRepo.save(ledgerClassDr);

				// accountingService.saveLedger(ledgerClassDr);

				saveAndPublishService.saveLedgerClass(ledgerClassDr, purchaseHdr.getBranchCode());

			}
			
			
			
			

			// Account discount
			// Get dicont account Head
			AccountHeads discountAccountHead = accountHeadsRepo.findByAccountNameAndCompanyMstId("DISCOUNT RECEIVED",
					comnpanyMst.getId());
			Double discountAmount = purchaseHdr.getDiscount();

			// if(null == discountAmount || discountAccountHead== null)
			// {
			// return;
			// }

			Optional<AccountHeads> accountHeadsList = accountHeadsRepo.findById(purchaseHdr.getSupplierId());
			AccountHeads accountHeads3 = accountHeadsList.get();

			AccountHeads supplierAccountHeads = accountHeadsRepo
					.findByAccountNameAndCompanyMstId(accountHeads3.getAccountName(), comnpanyMst.getId());

			CreditClass creditclas = new CreditClass();

			creditclas.setAccountId(discountAccountHead.getId());
			creditclas.setAccountClass(accountClass);

			BigDecimal CrAmount = new BigDecimal(discountAmount);
			CrAmount = CrAmount.setScale(10, BigDecimal.ROUND_HALF_EVEN);

			creditclas.setCrAmount(CrAmount);

			creditclas.setRemark(purchaseHdr.getVoucherNumber());

			creditclas.setCompanyMst(purchaseHdr.getCompanyMst());
			// creditclas = creditClassRepo.save(creditclas);
			creditclas = saveAndPublishService.saveCreditClass(creditclas, accountClass.getBrachCode());

			LedgerClass ledgerClassSupCr = new LedgerClass();
			ledgerClassSupCr.setAccountClass(accountClass);
			ledgerClassSupCr.setAccountId(creditclas.getAccountId());
			ledgerClassSupCr.setCreditAmount(creditclas.getCrAmount());
			ledgerClassSupCr.setDebitAmount(zero);

			ledgerClassSupCr.setSourceVoucherId(purchaseHdr.getVoucherNumber());
			ledgerClassSupCr.setSourceParentId(purchaseHdr.getVoucherNumber());

			ledgerClassSupCr.setTransDate(purchaseHdr.getVoucherDate());

			ledgerClassSupCr.setCompanyMst(purchaseHdr.getCompanyMst());

			ledgerClassSupCr.setBranchCode(accountClass.getBrachCode());
			ledgerClassSupCr.setRemark("Purchase " + accountClass.getSourceVoucherNumber());

			ledgerClassSupCr.setRemarkAccountName("PURCHASE " + accountClass.getSourceVoucherNumber());

			// ledgerClassRepo.save(ledgerClassSupCr);
			// accountingService.saveLedger(ledgerClassSupCr);
			saveAndPublishService.saveLedgerClass(ledgerClassSupCr, purchaseHdr.getBranchCode());

			// Debit discount amount to supplier
			DebitClass debitClass = new DebitClass();

			debitClass.setAccountId(supplierAccountHeads.getId());
			debitClass.setAccountClass(accountClass);

			BigDecimal drAmount = new BigDecimal(discountAmount);
			drAmount = drAmount.setScale(5, BigDecimal.ROUND_HALF_EVEN);

			debitClass.setDrAmount(drAmount);

			debitClass.setRemark(purchaseHdr.getVoucherNumber());

			debitClass.setCompanyMst(purchaseHdr.getCompanyMst());
			// debitClass = debitClassRepo.save(debitClass);

			debitClass = saveAndPublishService.saveDebitClass(debitClass, purchaseHdr.getBranchCode());

			LedgerClass ledgerClassDr = new LedgerClass();
			ledgerClassDr.setAccountClass(accountClass);
			ledgerClassDr.setAccountId(debitClass.getAccountId());
			ledgerClassDr.setDebitAmount(debitClass.getDrAmount());
			ledgerClassDr.setCreditAmount(zero);

			ledgerClassDr.setTransDate(purchaseHdr.getVoucherDate());
			ledgerClassDr.setCompanyMst(purchaseHdr.getCompanyMst());

			ledgerClassDr.setBranchCode(accountClass.getBrachCode());
			ledgerClassDr.setRemark("Purchase" + accountClass.getSourceVoucherNumber());

			ledgerClassDr.setRemarkAccountName(ledgerClassDr.getRemark());

			ledgerClassDr.setSourceVoucherId(purchaseHdr.getVoucherNumber());
			ledgerClassDr.setSourceParentId(purchaseHdr.getVoucherNumber());

			// ledgerClassRepo.save(ledgerClassDr);

			// accountingService.saveLedger(ledgerClassDr);

			saveAndPublishService.saveLedgerClass(ledgerClassDr, accountClass.getBrachCode());

		}

		eventBus.post(accountClass);

		/*
		 * inventory Accounting
		 * 
		 */

		inventoryledgerMstService.purchaseinventoryAccounting(voucherNumber, voucherDate, purchaseHdrId, comnpanyMst,
				accountClass);

		/*
		 * List<AccountClass> accountClassToForwardList = accountClassRepo
		 * .findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);
		 * 
		 * if (accountClassToForwardList.size() > 0) {
		 * 
		 * 
		 * Iterator iterForward = accountClassToForwardList.iterator(); while
		 * (iterForward.hasNext()) { AccountClass accountClassForward = (AccountClass)
		 * iterForward.next();
		 * 
		 * 
		 * try {
		 * 
		 * AccountMessage accountMessage =
		 * lmsQueueJobClassServiceImpl.getAccountMessageEntity(accountClassForward);
		 * eventBus.post(accountMessage);
		 * 
		 * } catch (Exception e) {
		 * 
		 * restserverApplication.insertIntoStockLmsQueue(accountClassForward.
		 * getSourceVoucherNumber(), accountClassForward.getTransDate(),
		 * accountClassForward.getId(), accountClassForward.getCompanyMst(),
		 * accountClassForward.getBrachCode(), "forwardAccounting","No",
		 * accountClassForward.getCompanyMst()); }
		 * 
		 * 
		 * }
		 * 
		 * }
		 */

	}

	public void removePurchaseAccounting(String voucherNumber, Date voucherDate, String purchaseHdrId,
			CompanyMst comnpanyMst, PurchaseHdr purchaseHdr) {

		BigDecimal zero = new BigDecimal("0");

		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		List<AccountClass> accountClassPreviousList = accountClassRepo
				.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

		if (accountClassPreviousList.size() > 0) {
			logger.info("Found previous records.. Deleting");

			Iterator iter = accountClassPreviousList.iterator();
			while (iter.hasNext()) {
				AccountClass accountClass = (AccountClass) iter.next();
				logger.info("Found previous records.. Deleting" + accountClass.getSourceVoucherNumber()
						+ accountClass.getBrachCode());
				ledgerClassRepo.deleteByAccountClass(accountClass);
				creditClassRepo.deleteByAccountClass(accountClass);
				debitClassRepo.deleteByaccountClass(accountClass);

				accountClassRepo.delete(accountClass);
				ledgerClassRepo.flush();
				accountClassRepo.flush();
				debitClassRepo.flush();
				creditClassRepo.flush();
			}

		}

		// @DATA CORRECTION
		// find by voucher number alone

		List<AccountClass> accountClassList = accountClassRepo.findBySourceVoucherNumber(voucherNumber);

		if (accountClassList.size() > 0) {
			logger.info("Found previous records.. Deleting");

			Iterator iter = accountClassList.iterator();
			while (iter.hasNext()) {
				AccountClass accountClass = (AccountClass) iter.next();
				logger.info("Found previous records.. Deleting" + accountClass.getSourceVoucherNumber()
						+ accountClass.getBrachCode());
				ledgerClassRepo.deleteByAccountClass(accountClass);
				creditClassRepo.deleteByAccountClass(accountClass);
				debitClassRepo.deleteByaccountClass(accountClass);

				accountClassRepo.delete(accountClass);
				ledgerClassRepo.flush();
				accountClassRepo.flush();
				debitClassRepo.flush();
				creditClassRepo.flush();
			}

		}

		dayBookRepository.deleteByVoucheNumberAndVoucherDate(purchaseHdr.getVoucherNumber(),
				purchaseHdr.getVoucherDate());

	}
}
