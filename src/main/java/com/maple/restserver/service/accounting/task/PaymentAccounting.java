package com.maple.restserver.service.accounting.task;




import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.RestserverApplication;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayBook;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.message.entity.AccountMessage;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DayBookRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.accounting.service.AccountingService;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.resource.PaymentHdrResource;
import com.maple.restserver.service.InternetCheckService;
import com.maple.restserver.service.SaveAndPublishServiceImpl;
import com.maple.restserver.utils.EventBusFactory;

@Transactional
@Service
public class PaymentAccounting  {

	
	private static final Logger logger = LoggerFactory.getLogger(PaymentHdrResource.class);
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	RestserverApplication restserverApplication;
	
	
	 
	@Autowired
	private CompanyMstRepository  companyMstRepository;
	
	
	
	@Autowired
	private PaymentHdrRepository paymentHdrRepo;
	
	@Autowired
	private PaymentDtlRepository paymentDtlRepo;
	 
	@Autowired
	private AccountClassRepository accountClassRepo;
	
	
	@Autowired
	private AccountHeadsRepository accountHeadsRepo;
	
	@Autowired
	private CreditClassRepository creditClassRepo;

	
	@Autowired
	private DebitClassRepository debitClassRepo;

	@Autowired
	private LedgerClassRepository ledgerClassRepo;
	
	
	@Autowired
	private DayBookRepository dayBookRepository;
	
	@Autowired
	SaveAndPublishServiceImpl saveAndPublishServiceImpl;
 

	 
	 @Autowired
	 AccountingService accountingService;
	
	
	@Value("${camunda.host}")
	 String host;
	
	
	public PaymentAccounting(){
		
	}
	@Transactional(rollbackFor = PartialAccountingException.class)
	 
	 
		public void execute(String voucherNumber, Date voucherDate, String paymentHdrId, CompanyMst comnpanyMst ) throws PartialAccountingException{
			// TODO Auto-generated method stub
		 

		/*
		 * Deletion to be called separately for  Editing 
		 */
 
	 //accountClassRepo.deleteBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);
		List<AccountClass> accList = accountClassRepo.findBySourceParentId(paymentHdrId);
		if(accList.size()>1)
		{
			throw new PartialAccountingException(paymentHdrId);
		}
		
		else if(accList.size()==1)
		{
			accountClassRepo.deleteBySourceParentId(paymentHdrId);
		}
		

	
	    		        	 
	    		 	    	 
	    		 			  java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());
	    		         
	    		 			 Optional<PaymentHdr> paymentHdrOpt = paymentHdrRepo.findById(paymentHdrId);
	    		 			PaymentHdr paymentHdr = paymentHdrOpt.get();
	    	  
		
    		        		
    		        		 AccountClass accountClass = new AccountClass();
    		        		 Double totalCredit = paymentDtlRepo.findSumDebit(voucherNumber,sqlDate);
    		        		 accountClass.setSourceParentId(paymentHdr.getId());
    		        		 accountClass.setSourceVoucherNumber(paymentHdr.getVoucherNumber());
    		        		 accountClass.setVoucherType("PAYMENT");
    		        		 
    		        		 BigDecimal bdTC = new BigDecimal(totalCredit);
    		        		 bdTC = bdTC.setScale(2,BigDecimal.ROUND_HALF_EVEN);
   		        		  
    		        		 accountClass.setTotalCredit(bdTC);
    		        		 accountClass.setTotalDebit(new BigDecimal(0.0));
    		        		
    		        		 accountClass.setBrachCode(paymentHdr.getBranchCode());
    		        		 accountClass.setCompanyMst(paymentHdr.getCompanyMst());
    		        		 accountClass.setTransDate(voucherDate);
//    		        		 accountClass =  accountClassRepo.save(accountClass);
    		        		 accountClass = saveAndPublishServiceImpl.saveAccountClass(accountClass, paymentHdr.getBranchCode());
    		        		 
    		        		 logger.info("Saving Account Class " + accountClass.getId());
    		        		 List<PaymentDtl> paymentDtlList = paymentDtlRepo.findBypaymenthdrId(  paymentHdr.getId());
    		        		 
    		        		 
    		        		 Iterator itrInner = paymentDtlList.iterator();
        		        	 
        		        	 while (itrInner.hasNext()) {
        		        		 PaymentDtl paymentDtl = (PaymentDtl)itrInner.next();
        		        	
        		        		 /*
        		        		  * Get mod of Pay. Fetch Account Id from Account heads from Mode Of Pay
        		        		  * 
        		        		  */
        		        		// String modeOfPay = paymentDtl.getModeOfPayment();
        		        		 Optional<AccountHeads> accountHeadOpts = accountHeadsRepo.findById(paymentDtl.getCreditAccountId()); ;
        		        		/* if(paymentDtl.getModeOfPayment().equalsIgnoreCase("CASH")){
        		        			   accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId(paymentDtl.getModeOfPayment(),comnpanyMst.getId());
        		        			    
        		        			   
        		        		 }else {
        		        			 accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId(paymentDtl.getBankAccountNumber(),comnpanyMst.getId());
        		        			  
        		        		 }
*/
        		        		 AccountHeads accountHeads = accountHeadOpts.get();
        		        		 
        		        		 
        		        		 
        		        		 CreditClass creditclass = new CreditClass();
        		        		 
        		        		 creditclass.setAccountId(accountHeads.getId());
        		        		 creditclass.setAccountClass(accountClass);
        		        		 
        		        		 BigDecimal bdPA = new BigDecimal(paymentDtl.getAmount());
        		        		 bdPA = bdPA.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        		        		 
        		        		 creditclass.setCrAmount(bdPA);
        		        		 creditclass.setRemark(paymentDtl.getRemark());
        		        		 
        		        		 creditclass.setCompanyMst(paymentHdr.getCompanyMst());
        		        		 
//        		        		 creditclass = creditClassRepo.save(creditclass);
        		        		 creditclass = saveAndPublishServiceImpl.saveCreditClass(creditclass, paymentHdr.getBranchCode());
        		        		 
        		        		 LedgerClass  ledgerClassCr = new LedgerClass();
        		        		 ledgerClassCr.setAccountClass(accountClass);
        		        		 ledgerClassCr.setAccountId(creditclass.getAccountId());
        		        		 ledgerClassCr.setCreditAmount(creditclass.getCrAmount());
        		        		 
        		        		 ledgerClassCr.setDebitAmount(new BigDecimal(0.0));
        		        		 
        		        		 ledgerClassCr.setTransDate(paymentHdr.getVoucherDate());
        		        		 
        		        			ledgerClassCr.setBranchCode(accountClass.getBrachCode());
        							ledgerClassCr.setRemark(paymentDtl.getRemark()
        									+accountClass.getSourceVoucherNumber()); 
        		        		
        		        		 ledgerClassCr.setCompanyMst(paymentHdr.getCompanyMst());
        		        		 
        		        		 
        		        		 ledgerClassCr.setRemarkAccountName(ledgerClassCr.getRemark());
        		        		 
        		        		 
        		        		 //ledgerClassRepo.save(ledgerClassCr);
        		        		 ledgerClassCr = accountingService.saveLedger(ledgerClassCr);
        		        		  
        		        	 
        		        		 
        		        		 
        		        		 DebitClass debitClass= new DebitClass();
        		        		 
        		        		 Optional<AccountHeads> accountHeadsDebit = accountHeadsRepo.findById(paymentDtl.getAccountId());
        		         
        		        		 
        		        		 
        		        		 
        		        		 
        		        		 debitClass.setAccountId(accountHeadsDebit.get().getId());
        		        		 debitClass.setAccountClass(accountClass);
        		        		 
//        		        		 BigDecimal bdPA = new BigDecimal(paymentDtl.getAmount());
//        		        		 bdPA = bdPA.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        		        		 
        		        		 debitClass.setDrAmount(bdPA);
        		        		 debitClass.setRemark(paymentDtl.getRemark());
        		        		
        		        		 debitClass.setCompanyMst(paymentHdr.getCompanyMst());
        		        		 
//        		        		 debitClass = debitClassRepo.save(debitClass);
        		        		 debitClass = saveAndPublishServiceImpl.saveDebitClass(debitClass, paymentHdr.getBranchCode());
        		        		 
	        		        		 LedgerClass  ledgerClassDr = new LedgerClass();
	        		        		 ledgerClassDr.setAccountClass(accountClass);
	        		        		 ledgerClassDr.setAccountId(debitClass.getAccountId());
	        		        		 ledgerClassDr.setDebitAmount(debitClass.getDrAmount());
	        		        		 ledgerClassDr.setTransDate(paymentHdr.getVoucherDate());
	        		        		 ledgerClassDr.setCreditAmount(new BigDecimal(0.0));
	        		        		 
	        		        		 ledgerClassDr.setCompanyMst(paymentHdr.getCompanyMst());
	        		        		 
	        		        		 
	        		        		 ledgerClassDr.setBranchCode(accountClass.getBrachCode());
	        		        		 
	        		        		 
	        		        		 
	        		        		 ledgerClassDr.setRemark(accountHeads.getAccountName() + " "
	        		        				 +paymentDtl.getRemark()+accountClass.getSourceVoucherNumber() +"Payment"); 
	        							
	        							
	        		        		 
	        		        		 ledgerClassDr.setRemarkAccountName(ledgerClassDr.getRemark());
	        		        		 //ledgerClassDr = ledgerClassRepo.save(ledgerClassDr);
	        		        		 
	        		        		 accountingService.saveLedger(ledgerClassDr);
	        		        		 
	        		        		 ledgerClassCr.setRemark(accountHeadsDebit.get().getAccountName()+ " "+
	        		        				 ledgerClassCr.getRemark()+"Payment");
	        		        		 
	        		        		 accountingService.saveLedger(ledgerClassCr);
	        		        		 
	        		        		 
	        		        		 /*
	        		        		  * Now Insert into DayBook
	        		        		  */
        		        		  
	        		        		 
			
	        		    	        
        		        		  
        		        		 
        		        	 }
        		        	 
        		        	 eventBus.post(accountClass);
    		        		 
		/*
		 * List<AccountClass> accountClassToForwardList = accountClassRepo
		 * .findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);
		 * 
		 * if (accountClassToForwardList.size() > 0) {
		 * 
		 * 
		 * Iterator iterForward = accountClassToForwardList.iterator(); while
		 * (iterForward.hasNext()) { AccountClass accountClassForward = (AccountClass)
		 * iterForward.next();
		 * 
		 * try {
		 * 
		 * AccountMessage accountMessage =
		 * lmsQueueJobClassServiceImpl.getAccountMessageEntity(accountClassForward);
		 * eventBus.post(accountMessage);
		 * 
		 * } catch (Exception e) {
		 * 
		 * restserverApplication.insertIntoStockLmsQueue(accountClassForward.
		 * getSourceVoucherNumber(), accountClassForward.getTransDate(),
		 * accountClassForward.getId(), accountClassForward.getCompanyMst(),
		 * accountClassForward.getBrachCode(), "forwardAccounting","NO",
		 * accountClassForward.getCompanyMst()); }
		 * 
		 * 
		 * }
		 * 
		 * }
		 */
    		        	 
 
        
		
	 
	 }
	
}
