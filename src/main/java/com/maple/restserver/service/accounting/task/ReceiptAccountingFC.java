package com.maple.restserver.service.accounting.task;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.accounting.entity.AccountClassfc;
import com.maple.restserver.accounting.entity.CreditClassfc;
import com.maple.restserver.accounting.entity.DebitClassfc;
import com.maple.restserver.accounting.entity.LedgerClassfc;
import com.maple.restserver.accounting.entity.LedgerClassfc;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayBook;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.accounting.repository.AccountClassfcRepository;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassfcRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DayBookRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassfcRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassfcRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.accounting.service.AccountingService;
import com.maple.restserver.accounting.service.AccountingServiceFC;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.ReceiptDtlRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.resource.PaymentHdrResource;
import com.maple.restserver.service.InternetCheckService;
import com.maple.restserver.utils.EventBusFactory;

@Transactional
@Service
public class ReceiptAccountingFC {

	private static final Logger logger = LoggerFactory.getLogger(PaymentHdrResource.class);

 

	@Autowired
	private CompanyMstRepository companyMstRepository;

	@Autowired
	private ReceiptHdrRepository receiptHdrRepo;

	@Autowired
	private ReceiptDtlRepository receiptDtlRepo;

	@Autowired
	private AccountClassfcRepository accountClassRepo;

	@Autowired
	private AccountHeadsRepository accountHeadsRepo;

	@Autowired
	private CreditClassfcRepository creditClassRepo;

	@Autowired
	private DebitClassfcRepository debitClassRepo;

	@Autowired
	private LedgerClassfcRepository ledgerClassRepo;

	@Autowired
	private DayBookRepository dayBookRepository;

	@Autowired
	AccountingServiceFC accountingService;
	EventBus eventBus = EventBusFactory.getEventBus();

	public ReceiptAccountingFC() {

	}

	@Transactional(rollbackFor = PartialAccountingException.class)

	public void execute(String voucherNumber, Date voucherDate, String receiptHdrId, CompanyMst comnpanyMst) {

		
		/*
		 * Deletion to be called separately for  Editing 
		 */
		
		//accountClassRepo.deleteBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		Optional<ReceiptHdr> receiptHdrOpt = receiptHdrRepo.findById(receiptHdrId);
		ReceiptHdr receiptHdr = receiptHdrOpt.get();

		AccountClassfc accountClass = new AccountClassfc();

		Double totalCredit = receiptDtlRepo.getSumOfReceiptDtlAmount(receiptHdrId);

		accountClass.setSourceParentId(receiptHdr.getId());
		accountClass.setSourceVoucherNumber(receiptHdr.getVoucherNumber());
		accountClass.setVoucherType("RECEIPT");

		if (null == totalCredit) {
			return;
		}

		BigDecimal bdC = new BigDecimal(totalCredit);
		bdC = bdC.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		accountClass.setTotalCredit(bdC);

		accountClass.setCompanyMst(receiptHdr.getCompanyMst());

		BigDecimal bd = new BigDecimal(totalCredit);
		bd = bd.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		accountClass.setTotalDebit(bd);
		accountClass.setTransDate(voucherDate);
		accountClass.setBrachCode(receiptHdr.getBranchCode());

		accountClass = accountClassRepo.save(accountClass);

		List<ReceiptDtl> receiptDtlList = receiptDtlRepo.findByReceiptHdrId(receiptHdrId);

		Iterator itr = receiptDtlList.iterator();

		while (itr.hasNext()) {

			ReceiptDtl receiptDtl = (ReceiptDtl) itr.next();

			// String modeOfPay = paymentDtl.getModeOfPayment();

			// Debit Account is read from receipt table itself

			// AccountHeads accountHeads = null ;

			CreditClassfc creditclass = new CreditClassfc();

			creditclass.setAccountId(receiptDtl.getAccount());
			creditclass.setAccountClassfc(accountClass);

			BigDecimal bdCr = new BigDecimal(receiptDtl.getAmount());
			bdCr = new BigDecimal(receiptDtl.getAmount()).setScale(2, BigDecimal.ROUND_HALF_EVEN);

			creditclass.setCrAmount(bdCr);

			creditclass.setRemark(receiptDtl.getRemark());

			creditclass.setCompanyMst(receiptHdr.getCompanyMst());

			creditclass = creditClassRepo.save(creditclass);

			LedgerClassfc ledgerClassCr = new LedgerClassfc();
			ledgerClassCr.setAccountClassfc(accountClass);
			ledgerClassCr.setAccountId(creditclass.getAccountId());
//        		        		 
//        		        		 BigDecimal bd = new BigDecimal(receiptDtl.getAmount());
//        		        		 bd = new BigDecimal(receiptDtl.getAmount()).setScale(2,BigDecimal.ROUND_HALF_EVEN);

			ledgerClassCr.setCreditAmount(creditclass.getCrAmount());

			ledgerClassCr.setDebitAmount(new BigDecimal(0.0));
			ledgerClassCr.setTransDate(receiptHdr.getVoucherDate());

			ledgerClassCr.setCompanyMst(receiptHdr.getCompanyMst());

			ledgerClassCr.setBranchCode(accountClass.getBrachCode());
			// ledgerClassCr.setRemark("Receipt "+accountClass.getSourceVoucherNumber());

			ledgerClassCr.setRemark(receiptDtl.getRemark() + accountClass.getSourceVoucherNumber());

			// ledgerClassRepo.save(ledgerClassCr);

			ledgerClassCr = accountingService.saveLedger(ledgerClassCr);

			DebitClassfc debitClass = new DebitClassfc();

			Optional<AccountHeads> accountHeadsDebit = accountHeadsRepo.findById(receiptDtl.getDebitAccountId());
			Optional<AccountHeads> accountHeadsCredit = accountHeadsRepo.findById(creditclass.getAccountId());

			debitClass.setAccountId(accountHeadsDebit.get().getId());
			debitClass.setAccountClassfc(accountClass);

			BigDecimal bdDr = new BigDecimal(receiptDtl.getAmount());
			bdDr = bdDr.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			debitClass.setDrAmount(bdDr);
			debitClass.setRemark(receiptDtl.getRemark());

			debitClass.setCompanyMst(receiptHdr.getCompanyMst());

			debitClass = debitClassRepo.save(debitClass);

			LedgerClassfc ledgerClassDr = new LedgerClassfc();
			ledgerClassDr.setAccountClassfc(accountClass);
			ledgerClassDr.setAccountId(debitClass.getAccountId());

			ledgerClassDr.setCreditAmount(new BigDecimal(0.0));
			ledgerClassDr.setDebitAmount(debitClass.getDrAmount());
			ledgerClassDr.setTransDate(receiptHdr.getVoucherDate());

			ledgerClassDr.setCompanyMst(receiptHdr.getCompanyMst());

			ledgerClassDr.setBranchCode(accountClass.getBrachCode());

			// ledgerClassDr.setRemark("Receipt "+accountClass.getSourceVoucherNumber());

			ledgerClassDr.setRemark(accountHeadsCredit.get().getAccountName() + " " + receiptDtl.getRemark()
					+ accountClass.getSourceVoucherNumber() + " Receipt ");

			// ledgerClassDr = ledgerClassRepo.save(ledgerClassDr);
			accountingService.saveLedger(ledgerClassDr);

			ledgerClassCr
					.setRemark(accountHeadsDebit.get().getAccountName() + " " + ledgerClassCr.getRemark() + " Receipt");

			accountingService.saveLedger(ledgerClassCr);

			/*
			 * Now Insert into DayBook
			 */

//>>>>>>> cf177e370f566397609d55983265c0598c35bc2f
			/*
			 * DayBook dayBook = new DayBook();
			 * dayBook.setBranchCode(receiptHdr.getBranchCode());
			 * dayBook.setCompanyMst(receiptHdr.getCompanyMst());
			 * 
			 * 
			 * 
			 * dayBook.setDrAmount(receiptDtl.getAmount());
			 * dayBook.setNarration(receiptDtl.getRemark());
			 * 
			 * 
			 * Optional<AccountHeads> accountHeadsCredit =
			 * accountHeadsRepo.findById(receiptDtl.getAccount());
			 * 
			 * dayBook.setCrAccountName(accountHeadsCredit.get().getAccountName());
			 * dayBook.setCrAmount(receiptDtl.getAmount());
			 * 
			 * 
			 * dayBook.setDrAccountName(accountHeadsDebit.get().getAccountName());
			 * dayBook.setDrAmount(receiptDtl.getAmount());
			 * 
			 * dayBook.setSourceVoucheNumber(receiptHdr.getVoucherNumber());
			 * dayBook.setSourceVoucherDate(receiptHdr.getTransdate());
			 * dayBook.setSourceVoucherType("RECEIPTS");
			 * 
			 * dayBookRepository.save(dayBook);
			 */

		}
		
		
		eventBus.post(accountClass);
		
		/*
		 * List<AccountClassfc> accountClassToForwardList = accountClassRepo
		 * .findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);
		 * 
		 * if (accountClassToForwardList.size() > 0) {
		 * 
		 * Iterator iterForward = accountClassToForwardList.iterator(); while
		 * (iterForward.hasNext()) { AccountClassfc accountClassForward =
		 * (AccountClassfc) iterForward.next();
		 * 
		 * Map<String, Object> variables = new HashMap<String, Object>();
		 * 
		 * variables.put("voucherNumber", accountClassForward.getSourceVoucherNumber());
		 * variables.put("voucherDate", accountClassForward.getTransDate());
		 * variables.put("inet", 0); variables.put("id", accountClassForward.getId());
		 * variables.put("isbranchsales", "NO"); variables.put("companyid",
		 * accountClassForward.getCompanyMst()); variables.put("REST", 0);
		 * 
		 * variables.put("WF", "forwardAccountingFC");
		 * 
		 * 
		 * eventBus.post(variables);
		 * 
		 * }
		 * 
		 * }
		 */

	}

}
