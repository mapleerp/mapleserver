package com.maple.restserver.service;

import java.sql.Date;
import java.util.List;

import com.maple.restserver.entity.StockVerificationDtl;
import com.maple.restserver.report.entity.StockVerificationReport;

public interface StockVerificationService {

	List<StockVerificationReport> findByStockVerificationByBetweenDate(Date fromdate, Date todate,String companymstid);

}
