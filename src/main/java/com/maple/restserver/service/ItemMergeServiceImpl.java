package com.maple.restserver.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.ItemMergeMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.repository.ItemMergeMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
@Service
@Transactional
@Component
public class ItemMergeServiceImpl implements ItemMergeService {

	
	@Autowired
	ItemMergeMstRepository itemMergeMstRepository;
	
	@Autowired
	ItemMstRepository itemMstRepo;
	


	@Override
	public void mergeitem(String fromItemId, String toItemId, String fromItemBarCode, String toItemName) {

		
		itemMergeMstRepository.updateActulProductionDtl(fromItemId, toItemId);
		itemMergeMstRepository.updateDamageDtl(fromItemId, toItemId);
		itemMergeMstRepository.updateItemBatchDtl(fromItemId, toItemId, fromItemBarCode);
		
		itemMergeMstRepository.updatekitdefinitionmst(fromItemId, toItemId, fromItemBarCode);
//		itemMergeMstRepository.updatekitdefinitondtl(fromItemId, toItemId, fromItemBarCode);
		itemMergeMstRepository.updatekitdefinitondtl(fromItemId, toItemId, fromItemBarCode, toItemName);
		itemMergeMstRepository.updatepurchasedtl(fromItemId, toItemId, fromItemBarCode, toItemName);
		itemMergeMstRepository.updaterowmaterialissue(fromItemId, toItemId);
		itemMergeMstRepository.updaterowmaterialreturn(fromItemId, toItemId);
   //	itemMergeMstRepository.updateItemBatchMst(fromItemId, toItemId, fromItemBarCode);
		itemMergeMstRepository.updatesalesdtlresult(fromItemId, toItemId, toItemName);
		itemMergeMstRepository.updatesaleorderdtlresult(fromItemId, toItemId, fromItemBarCode, toItemName);
		itemMergeMstRepository.updateStockTransferOutDtl(fromItemId, toItemId, fromItemBarCode);
		
		
		//  futureupdations  stocktransferindtl, production palanning
	    itemMergeMstRepository.updatephysicalstockdtl(fromItemId, toItemId, fromItemBarCode);
		
	
		
		
	

	//	 ItemMst itemMst = itemMstRepo.findById(fromItemId).get();
		
//		 String itemName = itemMst.getItemName()+"-DELETED";
//		 itemMergeMstRepository.deleteItem(fromItemId,itemName);


			
		
		
	}

}
