package com.maple.restserver.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.KotCategoryInventoryMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.KotCategoryInventoryMstRepository;

@Service
@Transactional
@Component
public class KotCategoryInventoryMstServiceImpl implements KotCategoryInventoryMstService {

	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	KotCategoryInventoryMstRepository kotCategoryInventoryMstRepository;
	
	@Override
	public List<KotCategoryInventoryMst> findByParentItem(String companymstid){
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
       List <KotCategoryInventoryMst> categoryList=kotCategoryInventoryMstRepository.findAllCategoryByParentItemNull(CompanyMstOpt.get().getId());
		
		List<Object> items=kotCategoryInventoryMstRepository.findAllItemByParentItemNull(CompanyMstOpt.get().getId());
		List<KotCategoryInventoryMst> getItemsFromKotCategoryInventoryMstList = new ArrayList<KotCategoryInventoryMst>();
		for (int i = 0; i < items.size(); i++) {
			Object[] objAray = (Object[]) items.get(i);
			KotCategoryInventoryMst kotCategoryInventoryMst = new KotCategoryInventoryMst();
			
			kotCategoryInventoryMst.setId((String) objAray[0]);
			kotCategoryInventoryMst.setChildItem((String) objAray[1]);
			kotCategoryInventoryMst.setLeaf((String) objAray[2]);
			kotCategoryInventoryMst.setParentItem((String) objAray[3]);
			getItemsFromKotCategoryInventoryMstList.add(kotCategoryInventoryMst);
			

		}
				
		getItemsFromKotCategoryInventoryMstList.addAll(categoryList);
		
		
		return getItemsFromKotCategoryInventoryMstList;
	}		
	
	
	@Override
	public List<KotCategoryInventoryMst> findByCompanyMstIdAndParentItem(String companymstid, String parentitem){
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		List <KotCategoryInventoryMst> categoryList=kotCategoryInventoryMstRepository.findCategoryByParentItemAndCompanyMstId(CompanyMstOpt.get().getId(),parentitem);
		
		List<Object> items=kotCategoryInventoryMstRepository.findItemByParentItemAndCompanyMstId(CompanyMstOpt.get().getId(),parentitem);
		List<KotCategoryInventoryMst> getItemsFromKotCategoryInventoryMstList = new ArrayList<KotCategoryInventoryMst>();
		for (int i = 0; i < items.size(); i++) {
			Object[] objAray = (Object[]) items.get(i);
			KotCategoryInventoryMst kotCategoryInventoryMst = new KotCategoryInventoryMst();
			
			kotCategoryInventoryMst.setId((String) objAray[0]);
			kotCategoryInventoryMst.setChildItem((String) objAray[1]);
			kotCategoryInventoryMst.setLeaf((String) objAray[2]);
			kotCategoryInventoryMst.setParentItem((String) objAray[3]);
			getItemsFromKotCategoryInventoryMstList.add(kotCategoryInventoryMst);
			

		}
				
		getItemsFromKotCategoryInventoryMstList.addAll(categoryList);
		
		
		return getItemsFromKotCategoryInventoryMstList;
	}
}
