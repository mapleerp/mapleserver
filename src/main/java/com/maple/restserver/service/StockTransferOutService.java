package com.maple.restserver.service;

import com.maple.restserver.entity.StockTransferMessageEntity;

public interface StockTransferOutService {

	StockTransferMessageEntity createMessageEntity(String id);

}
