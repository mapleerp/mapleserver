package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.accounting.service.AccountingServiceImpl;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.entity.DayEndReportStore;
import com.maple.restserver.report.entity.AccountBalanceReport;
import com.maple.restserver.report.entity.ReceiptModeReport;
import com.maple.restserver.report.entity.VoucherNumberDtlReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.DayEndReportStoreRepository;
import com.maple.restserver.repository.SaleOrderReceiptRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component
public class DayEndReportStoreServiceImpl implements DayEndReportStoreService {

	@Autowired
	SalesReceiptsRepository salesReceiptsRepo;

	@Autowired
	private DayEndReportStoreRepository dayEndReportStoreRepository;

	@Autowired
	private BranchMstRepository branchMstRepository;

	@Autowired
	private AccountHeadsRepository accountHeadsRepository;

	@Autowired
	DayEndReportServiceImpl dayEndReportServiceImpl;

	@Autowired
	private AccountingServiceImpl accountingServiceImpl;

	@Autowired
	DayEndClosureRepository dayEndClosureRepo;

	@Autowired
	DailySaleSummaryService dailySaleSummaryServiceImpl;

	@Autowired
	SaleTransHdrReportServiceImpl saleTransHdrReportServiceImpl;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@Autowired
	SaleOrderReceiptRepository saleOrderReceiptRepo;

	public List<DayEndReportStore> findDayEndReportByDate(CompanyMst companyMst, Date date) {
		List<DayEndReportStore> dayEndReportStoreList = new ArrayList<DayEndReportStore>();
		dayEndReportStoreList = dayEndReportStoreRepository.findByCompanyMstAndDate(companyMst, date);

		if (null == dayEndReportStoreList || dayEndReportStoreList.size() == 0) {
			List<BranchMst> branchList = branchMstRepository.findByCompanyMst(companyMst);
			for (BranchMst branch : branchList) {
				DayEndReportStore dayEndReportStore = getDayEndReportByBranchCode(companyMst, date,
						branch.getBranchCode());
				dayEndReportStoreList.add(dayEndReportStore);
			}

		}

		return dayEndReportStoreList;

	}

	private DayEndReportStore getDayEndReportByBranchCode(CompanyMst companyMst, Date date, String branchCode) {

		DayEndReportStore dayEndReportStore = new DayEndReportStore();
		
		Optional<BranchMst> branchMst = branchMstRepository.findByBranchCodeAndCompanyMstId(branchCode, companyMst.getId());
		dayEndReportStore.setBranchName(branchMst.get().getBranchName());
		// ----------bill series
		VoucherNumberDtlReport voucherNumberDtlReport = dayEndReportServiceImpl.getVoucherNumberDetailReport(branchCode,
				date, companyMst.getId());

		if (null != voucherNumberDtlReport) {
			if (null != voucherNumberDtlReport.getPosBillCount()) {
				dayEndReportStore.setPosBillCount(voucherNumberDtlReport.getPosBillCount() + "");

			}
			if (null != voucherNumberDtlReport.getPosEndingBillNumber()) {
				dayEndReportStore.setPosMaxBillNo(voucherNumberDtlReport.getPosEndingBillNumber() + "");
			}
			if (null != voucherNumberDtlReport.getPosEndingBillNumber()) {
				dayEndReportStore.setPosMinBillNo(voucherNumberDtlReport.getPosEndingBillNumber() + "");
			}

			if (null != voucherNumberDtlReport.getWholeSaleBillCount()) {
				dayEndReportStore.setWholeSaleBillCount(voucherNumberDtlReport.getWholeSaleBillCount() + "");
			}
			if (null != voucherNumberDtlReport.getWholeSaleEndingBillNumber()) {

				dayEndReportStore.setWholeSaleMaxBillNo(voucherNumberDtlReport.getWholeSaleEndingBillNumber() + "");
			}
			if (null != voucherNumberDtlReport.getWholeSaleStartingBillNumber()) {
				dayEndReportStore.setWholeSaleMinBillNo(voucherNumberDtlReport.getWholeSaleStartingBillNumber() + "");
			}
		}

		// --------petty Cash
		AccountHeads accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId(branchCode + "-PETTY CASH",
				companyMst.getId());
		if (null != accountHeads) {
			List<AccountBalanceReport> accountBalanceReport = accountingServiceImpl
					.getAccountStatement(companyMst.getId(), accountHeads.getId(), date, date, branchCode);
			if (accountBalanceReport.size() > 0) {
				
				dayEndReportStore.setPettyCash(accountBalanceReport.get(0).getClosingBalance() + "");
			}
		}
		// -------physical cash
		List<DayEndClosureHdr> dayEndClosureHdrList = dayEndClosureRepo
				.findByProcessDateAndCompanyMstAndBranchCode(date, companyMst, branchCode);
		
		if(dayEndClosureHdrList.size() >0)
		{

		dayEndReportStore.setPhysicalCash(dayEndClosureHdrList.get(0).getPhysicalCash() + "");
		
		}

		// -----------sales receipts
		List<ReceiptModeReport> yesBankSalesReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), "YES BANK");
		
		if(yesBankSalesReceipt.size() > 0)
		{
	
		dayEndReportStore.setYesBankReceipt(yesBankSalesReceipt.get(0).getAmount() + "");
		
		}

		List<ReceiptModeReport> paytmReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), "PAYTM");
		if(paytmReceipt.size() > 0)
		{
	
		dayEndReportStore.setPaytmReceipt(paytmReceipt.get(0).getAmount() + "");
		}

		List<ReceiptModeReport> ebsReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), "EBS");
		if(ebsReceipt.size() > 0)
		{
		dayEndReportStore.setEbsReceipt(ebsReceipt.get(0).getAmount() + "");
		}

		List<ReceiptModeReport> gpayReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), "GPAY");
		if(gpayReceipt.size() > 0)
		{
		dayEndReportStore.setGpayReceipt(gpayReceipt.get(0).getAmount() + "");
		}

//		 List<ReceiptModeReport> swiggyReceipt = dailySaleSummaryServiceImpl.
//				 getSalesReceiptsByMode(branchCode, SystemSetting.UtilDateToSQLDate(date), companyMst.getId(),"SWIGGY");
//		 dayEndReportStore.setSwiggyReceipt(gpayReceipt.get(0).getAmount()+"");
//		 
//		 List<ReceiptModeReport> zomatoReceipt = dailySaleSummaryServiceImpl.
//				 getSalesReceiptsByMode(branchCode, SystemSetting.UtilDateToSQLDate(date), companyMst.getId(),"ZOMATO");
//		 dayEndReportStore.setZomatoReceipt(zomatoReceipt.get(0).getAmount()+"");
//		 
		List<ReceiptModeReport> creditReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), "CREDIT");
		if(creditReceipt.size() > 0)
		{
		dayEndReportStore.setCreditReceipt(creditReceipt.get(0).getAmount() + "");
		}

		List<ReceiptModeReport> neftReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), "NEFT");
		if(neftReceipt.size() > 0)
		{
		dayEndReportStore.setNeftSales(neftReceipt.get(0).getAmount() + "");
		}

		// --------previous cash sale order
		Double prvCashSaleOrder = saleTransHdrReportServiceImpl.getPreviousAdvanceAmount(branchCode, date,
				companyMst.getId(), "CASH");
		if (null == prvCashSaleOrder) {
			prvCashSaleOrder = 0.0;
		}
		dayEndReportStore.setPreviousCashSaleOrder(prvCashSaleOrder + "");

		// ------------todays cash sales

		List<ReceiptModeReport> cashReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), branchCode + "-CASH");

		Double cash = 0.0;
		if (cashReceipt.size() > 0) {
			cash = cashReceipt.get(0).getAmount();
		}

		Double cashAdjustment = dayEndReportServiceImpl.getCreditSaleAdjustment(date, companyMst.getId(), branchCode);
		if (null == cashAdjustment) {
			cashAdjustment = 0.0;
		}

		Double todaysCashSales = cash + cashAdjustment - prvCashSaleOrder;

		dayEndReportStore.setCash(todaysCashSales + "");

		// ----------card sales
		Double cardsales = salesReceiptsRepo.sumOfCardAmountBranch(date, companyMst.getId(), "CASH",branchCode);
		if (null == cardsales) {
			cardsales = 0.0;
		}

		Double settledCasAmount = salesTransHdrRepository.getPreviousAdvanceCard(branchCode, date, "CASH");
		if (null == settledCasAmount) {
			settledCasAmount = 0.0;
		}
		cardsales = cardsales + settledCasAmount;
		dayEndReportStore.setCardSale(cardsales + "");

		// -----------card sale order receipt

		Double cardso = saleOrderReceiptRepo.getTotalCardSOBarnch("CASH", companyMst.getId(),
				SystemSetting.UtilDateToSQLDate(date),branchCode);
		if (null != cardso) {
			dayEndReportStore.setCardSaleOrder(cardso + "");
		}

		// ------------- swiggy sales

		AccountHeads swiggyAcc = accountHeadsRepository.findByAccountNameAndCompanyMstId("SWIGGY", companyMst.getId());

		List<ReceiptModeReport> swiggyResp = dailySaleSummaryServiceImpl.getSalesReceiptsByModeByAccountId(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), swiggyAcc.getId());

		if (swiggyResp.size() > 0) {
			dayEndReportStore.setSwiggyReceipt(swiggyResp.get(0).getAmount().toString());
		}

		AccountHeads zomatoAcc = accountHeadsRepository.findByAccountNameAndCompanyMstId("ZOMATO", companyMst.getId());

		List<ReceiptModeReport> zomatoResp = dailySaleSummaryServiceImpl.getSalesReceiptsByModeByAccountId(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), zomatoAcc.getId());

		if (zomatoResp.size() > 0) {
			dayEndReportStore.setZomatoReceipt(zomatoResp.get(0).getAmount().toString());
		}

		return dayEndReportStore;

	}

	//-------------version 6.7 surya 

	@Override
	public List<DayEndReportStore> findDayEndReportByDateAndCard(CompanyMst companyMst, Date date) {

		List<DayEndReportStore> dayEndReportStoreList = new ArrayList<DayEndReportStore>();
		dayEndReportStoreList = dayEndReportStoreRepository.findByCompanyMstAndDate(companyMst, date);
		
		for(int i=0;i < dayEndReportStoreList.size(); i++)
		{
			Double card2 = dailySaleSummaryServiceImpl.getCard2SalesSummary(dayEndReportStoreList.get(i).getBranchCode(),date, companyMst.getId());
			
			dayEndReportStoreList.get(i).setCard2(card2);
		}

		if (null == dayEndReportStoreList || dayEndReportStoreList.size() == 0) {
			List<BranchMst> branchList = branchMstRepository.findByCompanyMst(companyMst);
			for (BranchMst branch : branchList) {
				DayEndReportStore dayEndReportStore = getDayEndReportByBranchCodeAndCard(companyMst, date,
						branch.getBranchCode());
				dayEndReportStoreList.add(dayEndReportStore);
			}

		}

		return dayEndReportStoreList;

	
	}

	private DayEndReportStore getDayEndReportByBranchCodeAndCard(CompanyMst companyMst, Date date, String branchCode) {
		


		DayEndReportStore dayEndReportStore = new DayEndReportStore();
		
		Optional<BranchMst> branchMst = branchMstRepository.findByBranchCodeAndCompanyMstId(branchCode, companyMst.getId());
		dayEndReportStore.setBranchName(branchMst.get().getBranchName());
		// ----------bill series
		VoucherNumberDtlReport voucherNumberDtlReport = dayEndReportServiceImpl.getVoucherNumberDetailReport(branchCode,
				date, companyMst.getId());

		if (null != voucherNumberDtlReport) {
			if (null != voucherNumberDtlReport.getPosBillCount()) {
				dayEndReportStore.setPosBillCount(voucherNumberDtlReport.getPosBillCount() + "");

			}
			if (null != voucherNumberDtlReport.getPosEndingBillNumber()) {
				dayEndReportStore.setPosMaxBillNo(voucherNumberDtlReport.getPosEndingBillNumber() + "");
			}
			if (null != voucherNumberDtlReport.getPosEndingBillNumber()) {
				dayEndReportStore.setPosMinBillNo(voucherNumberDtlReport.getPosEndingBillNumber() + "");
			}

			if (null != voucherNumberDtlReport.getWholeSaleBillCount()) {
				dayEndReportStore.setWholeSaleBillCount(voucherNumberDtlReport.getWholeSaleBillCount() + "");
			}
			if (null != voucherNumberDtlReport.getWholeSaleEndingBillNumber()) {

				dayEndReportStore.setWholeSaleMaxBillNo(voucherNumberDtlReport.getWholeSaleEndingBillNumber() + "");
			}
			if (null != voucherNumberDtlReport.getWholeSaleStartingBillNumber()) {
				dayEndReportStore.setWholeSaleMinBillNo(voucherNumberDtlReport.getWholeSaleStartingBillNumber() + "");
			}
		}

		// --------petty Cash
		AccountHeads accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId(branchCode + "-PETTY CASH",
				companyMst.getId());
		if (null != accountHeads) {
			List<AccountBalanceReport> accountBalanceReport = accountingServiceImpl
					.getAccountStatement(companyMst.getId(), accountHeads.getId(), date, date, branchCode);
			if (accountBalanceReport.size() > 0) {
				
				dayEndReportStore.setPettyCash(accountBalanceReport.get(0).getClosingBalance() + "");
			}
		}
		// -------physical cash
		List<DayEndClosureHdr> dayEndClosureHdrList = dayEndClosureRepo
				.findByProcessDateAndCompanyMstAndBranchCode(date, companyMst, branchCode);
		
		if(dayEndClosureHdrList.size() >0)
		{

		dayEndReportStore.setPhysicalCash(dayEndClosureHdrList.get(0).getPhysicalCash() + "");
		
		}

		// -----------sales receipts
		List<ReceiptModeReport> yesBankSalesReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), "YES BANK");
		
		if(yesBankSalesReceipt.size() > 0)
		{
	
		dayEndReportStore.setYesBankReceipt(yesBankSalesReceipt.get(0).getAmount() + "");
		
		}

		List<ReceiptModeReport> paytmReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), "PAYTM");
		if(paytmReceipt.size() > 0)
		{
	
		dayEndReportStore.setPaytmReceipt(paytmReceipt.get(0).getAmount() + "");
		}

		List<ReceiptModeReport> ebsReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), "EBS");
		if(ebsReceipt.size() > 0)
		{
		dayEndReportStore.setEbsReceipt(ebsReceipt.get(0).getAmount() + "");
		}

		List<ReceiptModeReport> gpayReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), "GPAY");
		if(gpayReceipt.size() > 0)
		{
		dayEndReportStore.setGpayReceipt(gpayReceipt.get(0).getAmount() + "");
		}

//		 List<ReceiptModeReport> swiggyReceipt = dailySaleSummaryServiceImpl.
//				 getSalesReceiptsByMode(branchCode, SystemSetting.UtilDateToSQLDate(date), companyMst.getId(),"SWIGGY");
//		 dayEndReportStore.setSwiggyReceipt(gpayReceipt.get(0).getAmount()+"");
//		 
//		 List<ReceiptModeReport> zomatoReceipt = dailySaleSummaryServiceImpl.
//				 getSalesReceiptsByMode(branchCode, SystemSetting.UtilDateToSQLDate(date), companyMst.getId(),"ZOMATO");
//		 dayEndReportStore.setZomatoReceipt(zomatoReceipt.get(0).getAmount()+"");
//		 
		List<ReceiptModeReport> creditReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), "CREDIT");
		if(creditReceipt.size() > 0)
		{
		dayEndReportStore.setCreditReceipt(creditReceipt.get(0).getAmount() + "");
		}

		List<ReceiptModeReport> neftReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), "NEFT");
		if(neftReceipt.size() > 0)
		{
		dayEndReportStore.setNeftSales(neftReceipt.get(0).getAmount() + "");
		}

		// --------previous cash sale order
		Double prvCashSaleOrder = saleTransHdrReportServiceImpl.getPreviousAdvanceAmount(branchCode, date,
				companyMst.getId(), "CASH");
		if (null == prvCashSaleOrder) {
			prvCashSaleOrder = 0.0;
		}
		dayEndReportStore.setPreviousCashSaleOrder(prvCashSaleOrder + "");

		// ------------todays cash sales

		List<ReceiptModeReport> cashReceipt = dailySaleSummaryServiceImpl.getSalesReceiptsByMode(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), branchCode + "-CASH");

		Double cash = 0.0;
		if (cashReceipt.size() > 0) {
			cash = cashReceipt.get(0).getAmount();
		}

		Double cashAdjustment = dayEndReportServiceImpl.getCreditSaleAdjustment(date, companyMst.getId(), branchCode);
		if (null == cashAdjustment) {
			cashAdjustment = 0.0;
		}

		Double todaysCashSales = cash + cashAdjustment - prvCashSaleOrder;

		dayEndReportStore.setCash(todaysCashSales + "");

		// ----------card sales
		Double cardsales = salesReceiptsRepo.sumOfCardAmountBranch(date, companyMst.getId(), "CASH",branchCode);
		if (null == cardsales) {
			cardsales = 0.0;
		}

		Double settledCasAmount = salesTransHdrRepository.getPreviousAdvanceCard(branchCode, date, "CASH");
		if (null == settledCasAmount) {
			settledCasAmount = 0.0;
		}
		cardsales = cardsales + settledCasAmount;
		dayEndReportStore.setCardSale(cardsales + "");

		// -----------card sale order receipt

		Double cardso = saleOrderReceiptRepo.getTotalCardSOBarnch("CASH", companyMst.getId(),
				SystemSetting.UtilDateToSQLDate(date),branchCode);
		if (null != cardso) {
			dayEndReportStore.setCardSaleOrder(cardso + "");
		}

		// ------------- swiggy sales

		AccountHeads swiggyAcc = accountHeadsRepository.findByAccountNameAndCompanyMstId("SWIGGY", companyMst.getId());

		List<ReceiptModeReport> swiggyResp = dailySaleSummaryServiceImpl.getSalesReceiptsByModeByAccountId(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), swiggyAcc.getId());

		if (swiggyResp.size() > 0) {
			dayEndReportStore.setSwiggyReceipt(swiggyResp.get(0).getAmount().toString());
		}

		AccountHeads zomatoAcc = accountHeadsRepository.findByAccountNameAndCompanyMstId("ZOMATO", companyMst.getId());

		List<ReceiptModeReport> zomatoResp = dailySaleSummaryServiceImpl.getSalesReceiptsByModeByAccountId(branchCode,
				SystemSetting.UtilDateToSQLDate(date), companyMst.getId(), zomatoAcc.getId());

		if (zomatoResp.size() > 0) {
			dayEndReportStore.setZomatoReceipt(zomatoResp.get(0).getAmount().toString());
		}
		
		Double card2 = dailySaleSummaryServiceImpl.getCard2SalesSummary(branchCode,date, companyMst.getId());
		dayEndReportStore.setCard2(card2);

		return dayEndReportStore;

	

	}
	
	//-------------version 6.7 surya end


}
