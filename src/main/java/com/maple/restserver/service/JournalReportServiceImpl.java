package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.repository.JournalDtlRepository;
import com.maple.restserver.repository.JournalHdrRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
public class JournalReportServiceImpl implements JournalReportService{

	
	@Autowired
	JournalHdrRepository 	journalHdrRepository;
	
	@Autowired
	JournalDtlRepository journalDtlRepository;

	

	@Override
	public List<JournalHdr> getJournalHdr(CompanyMst companyMst, Date fromDate, Date toDate, String branchcode) {
	
		
		List<JournalHdr> journalHdrList=new ArrayList<JournalHdr>();
		List<Object> objectList =journalHdrRepository.journalHdrRepository(companyMst,fromDate,toDate,branchcode);
		for(int i=0;i<objectList.size();i++) {
			Object[] objAray = (Object[]) objectList.get(i);
			JournalHdr journalHdr=new JournalHdr();
			journalHdr.setId((String) objAray[0]);
			journalHdr.setVoucherDate((java.sql.Date) objAray[1]);
			journalHdr.setVoucherNumber((String) objAray[2]);
			journalHdr.setTransDate((java.sql.Date) objAray[3]);
			journalHdrList.add(journalHdr);
		}
		
		return journalHdrList;
	}



	@Override
	public List<JournalDtl> getJournalDtl(String journalhdrid) {
		
		List<JournalDtl> JournalDtlList=new ArrayList<JournalDtl>();
		List<Object> obj=journalDtlRepository.getJournalDtl(journalhdrid);
		for(int i=0;i<obj.size();i++) {
			Object[] objAray = (Object[]) obj.get(i);
			
			JournalDtl journalDtl=new JournalDtl();
			journalDtl.setAccountHead((String) objAray[0]);
			journalDtl.setRemarks((String) objAray[1]);
			journalDtl.setCreditAmount((Double) objAray[2]);
			journalDtl.setDebitAmount((Double) objAray[3]);
			JournalDtlList.add(journalDtl);
			
		}
		
		return JournalDtlList;
	}



	@Override
	public List<JournalDtl> getJournalJasperReportr(CompanyMst companyMst, Date fromDate, Date toDate,
			String branchcode) {
	
		
		
		List<JournalDtl> JournalDtlList=new ArrayList<JournalDtl>();
		List<Object> obj=journalDtlRepository.getJournalJasperReportr(companyMst,fromDate,toDate,branchcode);
		
		System.out.print(obj.size()+"object list size issssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		for(int i=0;i<obj.size();i++) {
			
			System.out.print(obj.size()+"object list size issssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
			Object[] objAray = (Object[]) obj.get(i);
			
			JournalDtl journalDtl=new JournalDtl();
			journalDtl.setAccountHead((String) objAray[0]);
			journalDtl.setRemarks((String) objAray[1]);
			journalDtl.setCreditAmount((Double) objAray[2]);
			journalDtl.setDebitAmount((Double) objAray[3]);
			JournalDtlList.add(journalDtl);
			
		}
		
		return JournalDtlList;
	
	}

}
