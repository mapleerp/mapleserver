package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.StockTransferOutDltdDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.repository.StockTransferOutDltdDtlRepository;
import com.maple.restserver.repository.StockTransferOutHdrRepository;

 
@Service
@Transactional
public class StockTransferDltdDtlServiceImpl implements StockTransferDltdDtlService{
	
	@Autowired
	private StockTransferOutDltdDtlRepository stockTransferOutDltdDtlRepository;
	
	@Autowired
	private StockTransferOutHdrRepository stockTransferOutHdrRepository;


	@Override
	public List<StockTransferOutDltdDtl> findByCompanyMstAndDate(String companymstid, Date edate) {
		
		List<StockTransferOutDltdDtl> dltdDtlList = new ArrayList<StockTransferOutDltdDtl>();
		
		List<String> objList = stockTransferOutDltdDtlRepository.findByCompanyMstAndDate(companymstid,edate);
		
		for (int i = 0; i < objList.size(); i++) {
			
			StockTransferOutDltdDtl stockTransferOutDltdDtl = new StockTransferOutDltdDtl();
			
//			stockTransferOutDltdDtl.setId((String) objAray[0]);
			
			StockTransferOutHdr stockTransferOutHdr = stockTransferOutHdrRepository.findByIdAndCompanyMstId(objList.get(i),companymstid);
			stockTransferOutDltdDtl.setStockTransferOutHdr(stockTransferOutHdr);
			
			dltdDtlList.add(stockTransferOutDltdDtl);
			
		}
		
		return dltdDtlList;
		
	}

}
