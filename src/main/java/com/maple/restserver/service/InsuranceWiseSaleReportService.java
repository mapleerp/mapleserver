package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.InsuranceWiseSalesReport;


@Component
public interface InsuranceWiseSaleReportService {

	List<InsuranceWiseSalesReport> getInsurancewisesalesreport(Date sdate, Date edate);
	
	

}
