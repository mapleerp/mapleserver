package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionPreplanningMst;
import com.maple.restserver.report.entity.ProductionPreplaninngDtlReport;


@Service
public interface ProductionPrePlaningService {

	
	ProductionPreplanningMst getProductionPreplanningMst(java.util.Date sdate, CompanyMst  companyMst ,String branchCode);
	
	
	
	List<ProductionPreplaninngDtlReport> getProductionPreplanningDtl(java.util.Date sdate, CompanyMst  companyMst ,String branchCode);
	
	void saveProductionPlaningDtlDtl(ProductionDtl productionDtl);
}
