package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.BranchStockReport;

@Service
public interface BranchStockReportService {

	List<BranchStockReport> findBranchStockReport(Date date);

	List<BranchStockReport> findPharmacyStockReportBranchWise(CompanyMst companyMst, Date date, String branchCode);

	List<BranchStockReport> findPharmacyStockReportBranchWiseWithCategory(CompanyMst companyMst, Date date,
			String branchCode, String[] array);

}
