package com.maple.restserver.service;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.Column;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.apache.derby.drda.NetworkServerControl;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.RestserverApplication;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AddKotWaiter;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.DamageHdr;
import com.maple.restserver.entity.DeliveryBoyMst;
import com.maple.restserver.entity.FinancialYearMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.KotCategoryMst;
import com.maple.restserver.entity.MenuConfigMst;
import com.maple.restserver.entity.OtherBranchSalesDtl;
import com.maple.restserver.entity.OtherBranchSalesTransHdr;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PriceBatchMst;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.ProcessPermissionMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionDtlDtl;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesOrderDtl;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;

import com.maple.restserver.entity.SchEligibilityDef;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.entity.StoreMst;

import com.maple.restserver.entity.SysDateMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.FinancialYearMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.JournalDtlRepository;
import com.maple.restserver.repository.JournalHdrRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.MenuConfigMstRepository;
import com.maple.restserver.repository.OtherBranchPurchaseDtlRepository;
import com.maple.restserver.repository.OtherBranchSalesDtlRepository;
import com.maple.restserver.repository.OtherBranchSalesTransHdrRepository;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.ProcessPermissionRepository;
import com.maple.restserver.repository.ProductionMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.ReceiptDtlRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.SchEligibilityAttribInstRepository;
import com.maple.restserver.repository.SchOfferAttrInstRepository;
import com.maple.restserver.repository.SchSelectAttrListDefReoository;
import com.maple.restserver.repository.SchSelectDefRepository;
import com.maple.restserver.repository.SchemeInstanceRepository;
import com.maple.restserver.repository.StockTransferOutDtlRepository;
import com.maple.restserver.repository.StockTransferOutHdrRepository;
import com.maple.restserver.repository.SysDateMstRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.repository.UserMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Service

public class DataTransferServiceImpl implements DataTransferService {
	public final static Logger logger = LoggerFactory.getLogger(DataTransferServiceImpl.class);

	//public static Connection localCn;
	// @Autowired
	// private RuntimeService runtimeService;

 
	
	
	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepository;
	
	
	@Autowired
	PriceDefinitionRepository priceDefinitionRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;

 

	@Autowired
	AccountClassRepository accountClassRepo;
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	UserMstRepository userMstRepository;
	@Autowired
	BranchMstRepository branchMstRepository;
	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	private ItemBatchMstRepository itemBatchMstRepo;

	

	@Autowired
	PriceDefinitionRepository priceDefinitionRepo;

	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	SchemeInstanceRepository schemeInstanceRepo;

	@Autowired
	SchEligibilityAttribInstRepository schEligibilityAttribInstRepo;

	@Autowired
	SchOfferAttrInstRepository schOfferAttrInstRepo;

	@Autowired
	SchSelectAttrListDefReoository schSelectAttrListDefReo;

	@Autowired
	SchSelectDefRepository schSelectDefRepo;

	@Autowired
	private CategoryMstRepository categoryMstRepository;

	@Autowired
	private UnitMstRepository unitMstRepository;

	@Autowired
	private ItemMstRepository itemMstRepository;

	@Autowired
	private KitDefinitionMstRepository kitDefinitionMstRepository;

	@Autowired
	private KitDefenitionDtlRepository kitDefenitionDtlRepository;

	/*
	 * @Autowired private CustomerMstRepository customerMstRepository;
	 */
	@Autowired
	private AccountHeadsRepository accountHeadsRepository;


	@Autowired
	private
	SalesTransHdrRepository salesTransHdrRepository;
@Autowired
OtherBranchSalesTransHdrRepository otherBranchSalesTransHdrRepository;

@Autowired
OtherBranchSalesDtlRepository  otherBranchSalesDtlRepository;
	@Autowired
	private SalesReceiptsRepository salesReceiptsRepository;
	@Autowired
	private SalesDetailsRepository salesDetailsRepository;
	@Autowired
	private PurchaseHdrRepository purchaseHdrRepository;
	@Autowired
	private PurchaseDtlRepository purchaseDtlRepository;

	@Autowired
	private ReceiptHdrRepository receiptHdrRepository;

	@Autowired
	private ReceiptDtlRepository receiptDtlRepository;

	@Autowired
	private JournalHdrRepository journalHdrRepository;

	@Autowired
	private JournalDtlRepository journalDtlRepository;

	@Autowired
	private PaymentHdrRepository paymentHdrRepository;

	@Autowired
	private PaymentDtlRepository paymentDtlRepository;

	@Autowired
	private VoucherNumberService voucherNumberService;
	
	
	@Autowired
	DebitClassRepository debitClassRepository;

	public String getCategoryNameFromId(int catId, Connection Conn) throws SQLException {

		String sqlstr2 = " SELECT  group_name from inv_category_mst where record_id =   " + catId;

		PreparedStatement pst2 = Conn.prepareStatement(sqlstr2);

		String groupName = "";
		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {
			groupName = rs2.getString("group_name");
		}
		rs2.close();
		pst2.close();
		return groupName;

	}

	public String getUnitNameFromId(int catId, Connection Conn) throws SQLException {

		String sqlstr2 = " SELECT  unit_name from unit_mst where record_id =   " + catId;

		PreparedStatement pst2 = Conn.prepareStatement(sqlstr2);

		String groupName = "";
		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {
			groupName = rs2.getString("unit_name");
		}
		
		rs2.close();
		
		pst2.close();

		return groupName;

	}

	public int isKitItem(int itemid, Connection Conn) throws SQLException {

		boolean result = false;

		String sqlstr2 = " SELECT  count(*)  from item_group_dtl where item_id  =   " + itemid + " AND group_id = 400 ";

		PreparedStatement pst2 = Conn.prepareStatement(sqlstr2);

		int count = 0;
		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {
			count = rs2.getInt(1);
		}
		rs2.close();
		pst2.close();
		return count;

	}

	public String getItemNameFromId(int itemid, Connection Conn) throws SQLException {

		String itemName = "";

		String sqlstr2 = " SELECT  item_name from item_mst  where record_id   =   " + itemid + "  ";

		PreparedStatement pst2 = Conn.prepareStatement(sqlstr2);

		int count = 0;
		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {
			itemName = rs2.getString(1);
		}

		itemName = itemName.replaceAll("'", "");
		
		rs2.close();
		pst2.close();
		return itemName;

	}

	@Override
	public String doItemTransfer(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {

//	RestserverApplication.RestserverApplication.localCn = getDbConn("SOURCEDB");

		// RestserverApplication.localCn = RestserverApplication.getDbConn("SOURCEDB");

		String sqlstr2 = " select  record_id , item_name , unit_id , tax_rate, standard_price , barcode, item_group_id, category_id, "
				+ " item_code, cess_rate,    hsn_code , item_code from item_mst   ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
//barCode_Line1,barCode_Line2 , best_Before,
		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			String unitname = "";
			String itemName = rs2.getString("item_name");

			itemName = itemName.replaceAll("'", "");
			itemName = itemName.replaceAll("&", "");

			itemName = itemName.replaceAll("/", "");
			itemName = itemName.replaceAll("%", "");
			itemName = itemName.replaceAll("\"", "");
			// itemName = itemName.replaceAll(" ", "-");

			int itemId = rs2.getInt("record_id");
			double taxRate = rs2.getDouble("tax_rate");
			String barcode = rs2.getString("barcode");
			double standardPrice = rs2.getDouble("standard_price");

			int item_group_id = rs2.getInt("item_group_id");
			int category_id = rs2.getInt("category_id");
			int unit_id = rs2.getInt("unit_id");
			// String barCodeLine1 = rs2.getString("barCode_Line1");
			// String barCodeLine2 = rs2.getString("barCode_Line2");
			// int bestBefore = rs2.getInt("best_Before");
			String hsn_code = rs2.getString("hsn_code");
			String item_code = rs2.getString("item_code");

			Integer rank = 0;
			String serviceOrGoods = "Goods";
			String sqlstr3 = " SELECT  group_name FROM inv_category_mst where  record_id  in (" + item_group_id + ", "
					+ category_id + ")";
			PreparedStatement pst3 = RestserverApplication.localCn.prepareStatement(sqlstr3);

			ResultSet rs3 = pst3.executeQuery();

			while (rs3.next()) {

				
				
				
				
				String groupName = rs3.getString("group_name");

				CategoryMst categoryMst = categoryMstRepository.findByCompanyMstIdAndCategoryName(companyMst.getId(),
						groupName);

				if (null == categoryMst) {
					categoryMst = new CategoryMst();

					categoryMst.setCategoryName(groupName);
					categoryMst.setCompanyMst(companyMst);
					VoucherNumber vNo = voucherNumberService.generateInvoice("CAT", branchCode);
					categoryMst.setId(vNo.getCode());

					categoryMstRepository.saveAndFlush(categoryMst);
				}

			}

			rs3.close();
			
			pst3.close();
			
			
			String sqlstr4 = " SELECT  unit_name, unit_description FROM unit_mst  where  record_id  in (" + unit_id
					+ ")";
			PreparedStatement pst4 = RestserverApplication.localCn.prepareStatement(sqlstr4);

			ResultSet rs4 = pst4.executeQuery();

			while (rs4.next()) {

				unitname = rs4.getString("unit_name");
				String unitDescription = rs4.getString("unit_description");

				UnitMst unitMst = unitMstRepository.findByUnitNameAndCompanyMstId(unitname, companyMst.getId());

				if (null == unitMst) {
					unitMst = new UnitMst();

					VoucherNumber vNo = voucherNumberService.generateInvoice("UNIT", branchCode);
					unitMst.setId(vNo.getCode());
					unitMst.setUnitName(unitname);
					unitMst.setUnitDescription(unitDescription);
					unitMst.setCompanyMst(companyMst);
					unitMstRepository.saveAndFlush(unitMst);
				}

			}
			
			rs4.close();
			pst4.close();

			ItemMst itemMst = itemMstRepository.findByItemName(itemName);

			if (null == itemMst) {

				String categoryName = getCategoryNameFromId(category_id, RestserverApplication.localCn);
				CategoryMst categoryMst = categoryMstRepository.findByCompanyMstIdAndCategoryName(companyMst.getId(),
						categoryName);

				String groupName = getCategoryNameFromId(item_group_id, RestserverApplication.localCn);
				CategoryMst categoryMst2 = categoryMstRepository.findByCompanyMstIdAndCategoryName(companyMst.getId(),
						groupName);

				UnitMst unitMst = unitMstRepository.findByUnitNameAndCompanyMstId(unitname, companyMst.getId());

				int isKit = isKitItem(itemId, RestserverApplication.localCn);

				itemMst = new ItemMst();
				itemMst.setBarCode(barcode);
				// itemMst.setBarCodeLine1(barCodeLine1);
				// itemMst.setBarCodeLine2(barCodeLine2);
				// itemMst.setBestBefore(bestBefore);
				itemMst.setBranchCode(branchCode);
				itemMst.setRank(rank);
				itemMst.setServiceOrGoods(serviceOrGoods);
				if (null != categoryMst && null != categoryMst.getId()) {
					itemMst.setCategoryId(categoryMst.getId());
				}
				itemMst.setCess(0.0);
				itemMst.setCgst(taxRate / 2);
				itemMst.setCompanyMst(companyMst);
				itemMst.setHsnCode(hsn_code);

				VoucherNumber vNo = voucherNumberService.generateInvoice(branchCode, companyMst.getId());

				itemMst.setId(vNo.getCode());

				itemMst.setIsDeleted("N");

				itemMst.setIsKit(isKit);
				itemMst.setItemCode(item_code);
				if (null != categoryMst2 && null != categoryMst2.getId()) {
					itemMst.setItemGroupId(categoryMst2.getId());
				}
				itemMst.setItemName(itemName);
				itemMst.setBestBefore(0);
				itemMst.setStandardPrice(standardPrice);
				itemMst.setSgst(taxRate / 2);
				itemMst.setTaxRate(taxRate);

				if (null == unitMst) {
					unitMst = unitMstRepository.findByUnitNameAndCompanyMstId("NOS", companyMst.getId());

				}
				itemMst.setUnitId(unitMst.getId());

				itemMst = itemMstRepository.save(itemMst);

				/*8Map<String, Object> variables = new HashMap<String, Object>();

				variables.put("companyid", companyMst);

				variables.put("voucherNumber", itemMst.getId());
				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("id", itemMst.getId());

				variables.put("REST", 1);

				// variables.put("voucherDate", purchase.getVoucherDate());
				// variables.put("id",purchase.getId());
				variables.put("inet", 0);

				variables.put("WF", "itemsave");
				eventBus.post(variables);*/

			}

		}
		
		rs2.close();
		pst2.close();
		return "OK";
	}

	@Override
	public String transferKit(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {

		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = " SELECT  record_id , item_name , unit_id , tax_rate, standard_price , barcode, item_group_id, category_id, "
				+ " item_code, cess_rate, barCode_Line1,  barCode_Line2 , best_Before, hsn_code , item_code from item_mst   ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			String unitname = "";
			String itemName = rs2.getString("item_name");

			itemName = itemName.replaceAll("'", "");
			int itemId = rs2.getInt("record_id");
			double taxRate = rs2.getDouble("tax_rate");
			String barcode = rs2.getString("barcode");
			double standardPrice = rs2.getDouble("standard_price");

			int item_group_id = rs2.getInt("item_group_id");
			int category_id = rs2.getInt("category_id");
			int unit_id = rs2.getInt("unit_id");
			String barCodeLine1 = rs2.getString("barCode_Line1");
			String barCodeLine2 = rs2.getString("barCode_Line2");
			int bestBefore = rs2.getInt("best_Before");
			String hsn_code = rs2.getString("hsn_code");
			String item_code = rs2.getString("item_code");

			ItemMst itemMst = itemMstRepository.findByItemName(itemName);

			int isKit = isKitItem(itemId, RestserverApplication.localCn);

			if (isKit == 1) {
				// Transfer Kit

				String sqlstr5 = " SELECT  kit_id, kit_name, kit_unit_id, kit_minimum_assembly_qty, kit_tax_rate "
						+ " FROM kit_def_hdr  where  kit_name = '" + itemName + "'";
				PreparedStatement pst5 = RestserverApplication.localCn.prepareStatement(sqlstr5);

				ResultSet rs5 = pst5.executeQuery();

				while (rs5.next()) {

					int kit_id = rs5.getInt("kit_id");

					int kit_unit_id = rs5.getInt("kit_unit_id");
					double kit_minimum_assembly_qty = rs5.getDouble("kit_minimum_assembly_qty");
					double kit_tax_rate = rs5.getDouble("kit_tax_rate");
					KitDefinitionMst kitDefinitionMst = null;
					List<KitDefinitionMst> kitDefinitionMstList = kitDefinitionMstRepository
							.findByItemId(itemMst.getId());
					if (kitDefinitionMstList.size() > 0) {
						kitDefinitionMst = kitDefinitionMstList.get(0);
					}

					if (null == kitDefinitionMst) {
						kitDefinitionMst = new KitDefinitionMst();
						kitDefinitionMst.setBarcode(itemMst.getBarCode());
						kitDefinitionMst.setCompanyMst(companyMst);
						kitDefinitionMst.setFinalSave("Y");

						VoucherNumber vNo = voucherNumberService.generateInvoice("KIT", companyMst.getId());

						kitDefinitionMst.setId(vNo.getCode());

						double productionCost = 0.0;

						kitDefinitionMst.setItemCode(itemMst.getItemCode());
						kitDefinitionMst.setItemId(itemMst.getId());
						kitDefinitionMst.setKitName(itemMst.getItemName());
						kitDefinitionMst.setMinimumQty(kit_minimum_assembly_qty);
						kitDefinitionMst.setProductionCost(productionCost);
						kitDefinitionMst.setTaxRate(taxRate);
						kitDefinitionMst.setUnitId(itemMst.getUnitId());
						kitDefinitionMst.setUnitName(unitname);
						kitDefinitionMst = kitDefinitionMstRepository.save(kitDefinitionMst);

					}

					String sqlstr6 = " SELECT record_id,  kit_id, item_id, qty, unit_id  "
							+ " FROM kit_def_dtl  where  kit_id = " + kit_id;
					PreparedStatement pst6 = RestserverApplication.localCn.prepareStatement(sqlstr6);

					ResultSet rs6 = pst6.executeQuery();

					while (rs6.next()) {

						int record_id = rs6.getInt("record_id");
						int item_id = rs6.getInt("item_id");
						double qty = rs6.getDouble("qty");

						qty = qty * kit_minimum_assembly_qty;

						int kit_dtl_unit_id = rs6.getInt("unit_id");

						String unitName = getUnitNameFromId(kit_dtl_unit_id, RestserverApplication.localCn);

						itemName = getItemNameFromId(item_id, RestserverApplication.localCn);
						itemName = itemName.replaceAll("'", "");

						itemMst = itemMstRepository.findByItemName(itemName);

						UnitMst unitMst = unitMstRepository.findByUnitNameAndCompanyMstId(unitName, companyMst.getId());

						KitDefenitionDtl kitDefenitionDtl = new KitDefenitionDtl();
						kitDefenitionDtl.setBarcode(barCodeLine2);
						kitDefenitionDtl.setCompanyMst(companyMst);

						VoucherNumber vNo = voucherNumberService.generateInvoice("KDTL", companyMst.getId());

						// kitDefenitionDtl.setId(vNo.getCode());
						kitDefenitionDtl.setItemId(itemMst.getId());

						kitDefenitionDtl.setItemName(itemName);
						kitDefenitionDtl.setKitDefenitionmst(kitDefinitionMst);
						kitDefenitionDtl.setQty(qty);
						kitDefenitionDtl.setUnitId(unitMst.getId());
						kitDefenitionDtl.setUnitName(unitName);

						kitDefenitionDtlRepository.save(kitDefenitionDtl);

					}
					
					rs6.close();
					pst6.close();

				}
				
				rs5.close();
				pst5.close();

			}
		}
		
		rs2.close();
		
		pst2.close();
		return "Kit Transferred";
	}

	@Override
	public String transferStock(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {
		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = " select  i.item_name ,sum(d.purchase_qty) - sum(d.sale_qty) as qty  from item_batch_dtl d , item_mst  i"
				+ " where d.item_id = i.record_id  group by  i.item_name  ";

		
		 
		
		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			String itemName = rs2.getString("item_name");
			itemName = itemName.replaceAll("'", "");

			double qty = rs2.getDouble("qty");

			ItemMst itemMst = itemMstRepository.findByItemName(itemName);
			if (null == itemMst) {
				continue;
			}

			ItemBatchMst itemBatchMst = new ItemBatchMst();

			if (null != itemMst.getBarCode()) {
				itemBatchMst.setBarcode(itemMst.getBarCode());
			}
			itemBatchMst.setBatch(MapleConstants.Nobatch);
			itemBatchMst.setMrp(itemMst.getStandardPrice());
			itemBatchMst.setQty(qty);
			itemBatchMst.setItemId(itemMst.getId());

			itemBatchMst.setCompanyMst(companyMst);

			itemBatchMst.setBranchCode(branchCode);
			itemBatchMstRepo.save(itemBatchMst);

			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
			VoucherNumber vNo = voucherNumberService.generateInvoice("OPN", companyMst.getId());

			itemBatchDtl.setBarcode(itemMst.getBarCode());
			itemBatchDtl.setBatch(MapleConstants.Nobatch);
			itemBatchDtl.setItemId(itemMst.getId());
			itemBatchDtl.setMrp(itemMst.getStandardPrice());
			itemBatchDtl.setQtyIn(qty);
			itemBatchDtl.setQtyOut(0.0);
			itemBatchDtl.setBranchCode(branchCode);
			itemBatchDtl.setParticulars("Opening Stock Transferred");
			itemBatchDtl.setVoucherNumber(vNo.getCode());
			itemBatchDtl.setVoucherDate(SystemSetting.systemDate);
			itemBatchDtl.setStore(MapleConstants.Store);

			itemBatchDtl.setCompanyMst(companyMst);

			itemBatchDtlRepository.save(itemBatchDtl);

		}

		rs2.close();
		pst2.close();
		
		return null;
	}

	@Override
	public String transferCustomer(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {
//	Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select * from customer_mst ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {
			String customer_name = rs2.getString("account_name");
			String customer_id = rs2.getString("id");
			String tin_no = rs2.getString("party_gst");
			// String customer_state = rs2.getString("customer_state");
			// String customer_state = "KERALA" ; //rs2.getString("customer_state");
			String customer_contry = "INDIA";
			String price_level_id = rs2.getString("price_level_id");
			String telephone_no = rs2.getString("customer_contact");
			String address1 = rs2.getString("party_address1");
			String mobileno = rs2.getString("customer_contact");
			String customerState = "KERALA";// rs2.getString("customer_state");
			double account_balance = getAccountBalance(customer_id);

			AccountHeads customerMst = new AccountHeads();
			customerMst.setCompanyMst(companyMst);
			customerMst.setCreditPeriod(0);
			customerMst.setPartyAddress1(address1);
			customerMst.setCustomerContact(mobileno);
			customerMst.setPartyGst(tin_no);
			customerMst.setAccountName(customer_name);
			customerMst.setPartyMail("-");
			customerMst.setCustomerState(address1);
			customerMst.setCustomerState("KERALA");
			customerMst.setCustomerRank(0);
			customerMst.setCustomerCountry("INDIA");
			customerMst.setId(customer_id);
			customerMst.setPriceTypeId(price_level_id);

			AccountHeads savedCustomer = accountHeadsRepository.save(customerMst);

			AccountHeads parentAccountHead = accountHeadsRepository
					.findByAccountNameAndCompanyMstId(savedCustomer.getAccountName(), companyMst.getId());
			if (null == parentAccountHead || null == parentAccountHead.getId()) {

				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setAccountName(savedCustomer.getAccountName());
				accountHeads.setGroupOnly("N");
				accountHeads.setId(savedCustomer.getId());
				accountHeads.setCompanyMst(savedCustomer.getCompanyMst());
				accountHeadsRepository.save(accountHeads);

			}

			/*
			 * Map<String, Object> variables1 = new HashMap<String, Object>();
			 * variables1.put("voucherNumber", savedCustomer.getId());
			 * variables1.put("voucherDate",SystemSetting.getSystemDate());
			 * variables1.put("inet", 0); variables1.put("id", savedCustomer.getId());
			 * variables1.put("companyid", savedCustomer.getCompanyMst());
			 * variables1.put("REST",1);
			 * 
			 * 
			 * variables1.put("WF", "forwardAccountHeads"); eventBus.post(variables1);
			 * 
			 * 
			 * Map<String, Object> variables = new HashMap<String, Object>();
			 * variables.put("voucherNumber", savedCustomer.getId());
			 * variables.put("voucherDate",SystemSetting.getSystemDate());
			 * variables.put("inet", 0); variables.put("id", savedCustomer.getId());
			 * variables.put("companyid", savedCustomer.getCompanyMst());
			 * variables.put("REST",1);
			 * 
			 * variables.put("WF", "forwardCustomer"); eventBus.post(variables);
			 * 
			 */

		}
		
		rs2.close();
		pst2.close();
		return null;
	}

	private double getAccountBalance(String customer_id) {

		return 0;
	}

	@Override
	public String transferSupplier(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {
		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select  supplier_name, tin_no, email_id, supplier_location, mobile_no , record_id, drug_licence_number from supplier_mst ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {
			String supplier_name = rs2.getString("supplier_name");
			String supplier_location = rs2.getString("supplier_location");
			// String address1 = rs2.getString("address1");

			String tin_no = rs2.getString("tin_no");
			String mobile_no = rs2.getString("mobile_no");

			String email_id = rs2.getString("email_id");
			String record_id = rs2.getString("record_id");

			AccountHeads accountHeads = new AccountHeads();
			accountHeads.setCreditPeriod(20);
			accountHeads.setPartyMail(email_id);
			accountHeads.setCustomerContact(mobile_no);
			accountHeads.setCustomerState("KERALA");

			accountHeads.setPartyGst(tin_no);
			accountHeads.setAccountName(supplier_name);

				accountHeads.setGroupOnly("N");
				accountHeads.setId(record_id);
				accountHeads.setCompanyMst(companyMst);

				accountHeadsRepository.save(accountHeads);

			
			/*
			 * Map<String, Object> variables1 = new HashMap<String, Object>();
			 * variables1.put("voucherNumber", supplier.getId());
			 * variables1.put("voucherDate",SystemSetting.getSystemDate());
			 * variables1.put("inet", 0); variables1.put("id", supplier.getId());
			 * variables1.put("companyid", supplier.getCompanyMst());
			 * variables1.put("REST",1);
			 * 
			 * 
			 * variables1.put("WF", "forwardAccountHeads"); eventBus.post(variables1);
			 */

			/*
			 * Map<String, Object> variables = new HashMap<String, Object>();
			 * variables.put("voucherNumber", supplier.getId());
			 * variables.put("voucherDate",SystemSetting.getSystemDate());
			 * variables.put("inet", 0); variables.put("id", supplier.getId());
			 * variables.put("companyid", supplier.getCompanyMst());
			 * variables.put("REST",1);
			 * 
			 * variables.put("WF", "forwardSupplier"); eventBus.post(variables);
			 */

			/*
			 * 
			 * ProcessInstanceWithVariables pVariablesInReturn =
			 * runtimeService.createProcessInstanceByKey("forwardCustomer")
			 * .setVariables(variables)
			 * 
			 * .executeWithVariablesInReturn();
			 */

		}
		rs2.close();
		pst2.close();
		return null;
	}

	@Override
	public String transferAccountHeads(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select * from account_heads ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {
			String account_id = rs2.getString("account_id");
			String account_name = rs2.getString("account_name");
			String parent_account_id = rs2.getString("parent_account_id");

			String group_only = rs2.getString("group_only");
			String primary_group = rs2.getString("primary_group");

			// Find if account exist.
			// If not exist , add account

			AccountHeads parentAccountHead = accountHeadsRepository.findByAccountNameAndCompanyMstId(account_name,
					companyMst.getId());
			if (null == parentAccountHead || null == parentAccountHead.getId()) {

				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setAccountName(account_name);
				accountHeads.setGroupOnly(group_only);
				accountHeads.setParentId(parent_account_id);
				accountHeads.setId(account_id);

				accountHeads.setCompanyMst(companyMst);
				accountHeadsRepository.save(accountHeads);

				/*
				 * Map<String, Object> variables1 = new HashMap<String, Object>();
				 * variables1.put("voucherNumber", parent_account_id);
				 * variables1.put("voucherDate",SystemSetting.getSystemDate());
				 * variables1.put("inet", 0); variables1.put("id", parent_account_id);
				 * variables1.put("companyid", companyMst); variables1.put("REST",1);
				 * 
				 * 
				 * variables1.put("WF", "forwardAccountHeads"); eventBus.post(variables1);
				 * 
				 */

			}

		}

		rs2.close();
		pst2.close();
		return null;
	}

	@Override
	public String transferSales(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {

		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select t.record_id as record_id , branch_id , t.customer_id, "
				+ " t.invoice_date, t.delivery_address," + " t.delivery_vehicle, "
				+ " h.voucher_number,net_invoice_amount, " + " c.customer_name  " + " from sales_trans_hdr t, "
				+ "sales_voucher_hdr h , " + " customer_mst c  where t.record_id = h.parent_id "
				+ " and t.customer_id = c.customer_id  ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {
			String customer_id = rs2.getString("customer_id");
			Date invoice_date = rs2.getDate("invoice_date");
			String delivery_address = rs2.getString("delivery_address");

			String delivery_vehicle = rs2.getString("delivery_vehicle");
			String record_id = rs2.getString("record_id");
			String voucher_number = rs2.getString("voucher_number");
			String customer_name = rs2.getString("customer_name");
			int branch_id = rs2.getInt("branch_id");
			float invoice_amount = rs2.getFloat("net_invoice_amount");
			AccountHeads customerMst = null;

			Optional<AccountHeads> customerOpt = accountHeadsRepository.findById(customer_id);
			if (customerOpt.isPresent()) {
				customerMst = customerOpt.get();
			} else {
				List<AccountHeads> customerMstList = (List<AccountHeads>) accountHeadsRepository.findByAccountName(customer_name);
				if (customerMstList.size() > 0) {
					customerMst = customerMstList.get(0);
				} else {
					continue;
				}
			}

			SalesTransHdr salesTransHdr = new SalesTransHdr();
			salesTransHdr.setAccountHeads(customerMst);

			// salesTransHdr.setAmountTendered(amountTendered);
			Optional<BranchMst> branchMstOpt = branchMstRepository.findById(String.valueOf(branch_id));
			if (branchMstOpt.isPresent()) {

				salesTransHdr.setBranchCode(branchMstOpt.get().getBranchCode());
			}
			// salesTransHdr.setCardamount(cardamount);
			// salesTransHdr.setCardNo(cardNo);
			// salesTransHdr.setCardType(cardType);
			// salesTransHdr.setCashPay(cashPay);
			salesTransHdr.setCompanyMst(companyMst);
			// salesTransHdr.setCreditAmount(creditAmount);
			salesTransHdr.setCreditOrCash("CREDIT");
			salesTransHdr.setCustomerId(customer_id);
			salesTransHdr.setInvoiceAmount(Double.valueOf(invoice_amount));
			// salesTransHdr.setId(id);
			// salesTransHdr.setInvoiceAmount(invoiceAmount);
			// salesTransHdr.setInvoiceNumberPrefix(invoiceNumberPrefix);
			salesTransHdr.setIsBranchSales("NO");
			// salesTransHdr.setLocalCustomerMst(customer_id);
			// salesTransHdr.setMachineId(machineId);
			// salesTransHdr.setPaidAmount(paidAmount);
			// salesTransHdr.setSalesMode(salesMode);
			// salesTransHdr.setVoucherType(voucherType);
			salesTransHdr.setVoucherNumber(voucher_number);
			salesTransHdr.setVoucherDate(invoice_date);
			// salesTransHdr.setUserId(userId);

			salesTransHdr = salesTransHdrRepository.save(salesTransHdr);

			String sqlstrDtl = "select i.item_name, i.unit_id,  "
					+ "d.item_id, d.tax_rate, d.qty, d.rate, d.purchase_rate , i.cess_rate "
					+ " from xpos_sales_dtl d , " + " sales_trans_hdr t, item_mst i   "
					+ " where t.record_id = d.parent_id  and" + " d.item_id = i.record_id " + " and  t.record_id = ? ";

			PreparedStatement pstDtl = RestserverApplication.localCn.prepareStatement(sqlstrDtl);
			pstDtl.setString(1, record_id);

			ResultSet rsDtl = pstDtl.executeQuery();

			while (rsDtl.next()) {

				String item_name = rsDtl.getString("item_name");
				Double tax_rate = rsDtl.getDouble("tax_rate");
				Double qty = rsDtl.getDouble("qty");
				Double rate = rsDtl.getDouble("rate");
				String unit_id = rsDtl.getString("unit_id");
				Double purchase_rate = rsDtl.getDouble("purchase_rate");
				String item_id = rsDtl.getString("item_id");

				Double cess_rate = rsDtl.getDouble("cess_rate");
				SalesDtl salesDtl = new SalesDtl();

				// salesDtl.setAddCessRate(addCessRate);
				// salesDtl.setAmount(amount);
				// salesDtl.setBarcode(barcode);
				// salesDtl.setBatch(batch);
				// salesDtl.setCessAmount(cessAmount);
				salesDtl.setCessRate(0.0);
				salesDtl.setCgstAmount(rate * qty * tax_rate / 200);
				salesDtl.setCgstTaxRate(tax_rate / 2);
				salesDtl.setCompanyMst(companyMst);
				salesDtl.setIgstAmount(0.0);
				salesDtl.setItemId(item_id);
				// salesDtl.setUnitName(unitName);
				salesDtl.setUnitId(unit_id);
				salesDtl.setTaxRate(tax_rate);
				salesDtl.setSgstTaxRate(tax_rate / 2);
				salesDtl.setSgstAmount(rate * qty * tax_rate / 200);
				salesDtl.setSalesTransHdr(salesTransHdr);
				salesDtl.setRate(rate);
				salesDtl.setQty(qty);
				salesDtl.setMrp(purchase_rate);
				salesDtl.setItemName(item_name);
				salesDtl.setAddCessRate(0.0);

				salesDtl.setAmount(qty * rate + (qty * rate * tax_rate / 100));

				salesDtl.setSalesTransHdr(salesTransHdr);

				salesDtl.setBatch(MapleConstants.Nobatch);

				salesDtl.setCessAmount(qty * rate * cess_rate / 100);
				salesDtl.setDiscount(0.0);
				salesDtl.setIgstTaxRate(tax_rate);
				salesDtl.setIgstAmount(0.0);

				salesDetailsRepository.saveAndFlush(salesDtl);

			}
			
			rsDtl.close();
			pstDtl.close();
			

			SalesReceipts salesReceipts = new SalesReceipts();
			salesReceipts.setAccountId(customer_id);
			salesReceipts.setBranchCode(branchCode);
			salesReceipts.setCompanyMst(companyMst);
			// salesReceipts.setId(id);
			// salesReceipts.setReceiptAmount(receiptAmount);
			// salesReceipts.setReceiptMode(receiptMode);
			salesReceipts.setSalesTransHdr(salesTransHdr);
			// salesReceipts.setUserId(userId);
			salesReceipts.setVoucherNumber(voucher_number);

			salesReceipts = salesReceiptsRepository.save(salesReceipts);

			salesTransHdr.setSalesReceiptsVoucherNumber(salesReceipts.getId());

			// --------

			/*
			 * 
			 * String SalesMode = salesTransHdr.getSalesMode();
			 * 
			 * Double invoiceAmount = salesTransHdr.getInvoiceAmount(); Double
			 * cashPaidAmount = salesTransHdr.getCashPay(); Double cardAmount =
			 * salesTransHdr.getCardamount(); Double sodexoAmount =
			 * salesTransHdr.getSodexoAmount(); Double creditAmount =
			 * salesTransHdr.getCreditAmount();
			 * 
			 * List<SalesReceipts> salesReceiptsList =
			 * salesTransHdr.findByCompanyMstIdAndSalesTransHdrId(companymstid,
			 * salesTransHdr.getId());
			 * 
			 * Iterator iter = salesReceiptsList.iterator(); while (iter.hasNext()) {
			 * SalesReceipts salesReceipts = (SalesReceipts) iter.next();
			 * salesReceipts.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			 * salesReceipts.setCompanyMst(salesTransHdrRequest.getCompanyMst());
			 * salesReceiptsRepo.saveAndFlush(salesReceipts);
			 * 
			 * }
			 * 
			 * AccountReceivable accountReceivable = accountReceivableRepository
			 * .findByCompanyMstAndSalesTransHdr(savedSalesTransHdr.getCompanyMst(),
			 * savedSalesTransHdr); if (null != accountReceivable) {
			 * accountReceivable.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			 * accountReceivable.setVoucherDate(salesTransHdrRequest.getVoucherDate());
			 * 
			 * accountReceivableRepository.saveAndFlush(accountReceivable); }
			 * 
			 * 
			 * 
			 * 
			 * 
			 * while(iter.hasNext()) { SalesReceipts salesReceipts = (SalesReceipts)
			 * iter.next();
			 * salesReceipts.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			 * salesReceipts.setCompanyMst(salesTransHdrRequest.getCompanyMst());
			 * salesReceiptsRepo.saveAndFlush(salesReceipts);
			 * 
			 * }
			 * 
			 * 
			 * 
			 * if(null!=accountReceivable) {
			 * accountReceivable.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			 * accountReceivable.setVoucherDate(salesTransHdrRequest.getVoucherDate());
			 * 
			 * accountReceivableRepository.saveAndFlush(accountReceivable); }
			 * 
			 * 
			 * 
			 * //Manage Stock
			 * 
			 * 
			 * 
			 * 
			 * salesAccounting.execute(salesTransHdrRequest.getVoucherNumber(),
			 * salesTransHdrRequest.getVoucherDate(), savedSalesTransHdr.getId(),
			 * savedSalesTransHdr.getCompanyMst());
			 * 
			 * 
			 * salesDayEndReporting.execute(salesTransHdrRequest.getVoucherNumber(),
			 * salesTransHdrRequest.getVoucherDate(), savedSalesTransHdr.getCompanyMst(),
			 * savedSalesTransHdr.getId());
			 * 
			 * 
			 * 
			 * SalesStockUpdate(salesTransHdrRequest.getVoucherNumber(),
			 * salesTransHdrRequest.getVoucherDate(), savedSalesTransHdr);
			 * 
			 * 
			 * 
			 * //Manage Accounting
			 * 
			 * 
			 * 
			 * 
			 * Map<String, Object> variables = new HashMap<String, Object>();
			 * 
			 * String creditOrCash = savedSalesTransHdr.getCreditOrCash(); if (null ==
			 * creditOrCash) { creditOrCash = "CASH"; }
			 * 
			 * if (null == savedSalesTransHdr.getIsBranchSales()) {
			 * savedSalesTransHdr.setIsBranchSales("N"); }
			 * 
			 * String isBranchSales = savedSalesTransHdr.getIsBranchSales(); int
			 * int_isBranchSales = 0; if (isBranchSales.equalsIgnoreCase("Y")) {
			 * int_isBranchSales = 1;
			 * 
			 * } else { int_isBranchSales = 0; }
			 * 
			 * variables.put("voucherNumber", savedSalesTransHdr.getVoucherNumber());
			 * variables.put("voucherDate", savedSalesTransHdr.getVoucherDate());
			 * variables.put("inet", 0); variables.put("id", savedSalesTransHdr.getId());
			 * variables.put("isbranchsales", int_isBranchSales); variables.put("companyid",
			 * savedSalesTransHdr.getCompanyMst());
			 * 
			 * if (serverorclient.equalsIgnoreCase("REST")) { variables.put("REST", 1); }
			 * else { variables.put("REST", 0); }
			 * 
			 * if (creditOrCash.equalsIgnoreCase("CREDIT") ||
			 * creditOrCash.equalsIgnoreCase("B2B")) { variables.put("CREDIT", 1); } else {
			 * variables.put("CREDIT", 0); }
			 * 
			 * variables.put("WF", "forwardSales"); eventBus.post(variables);
			 * 
			 */
			// --------

		}

		rs2.close();
		pst2.close();
		return null;
	}

	
	

	@Override
	public String transferOtherBranchSales(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {
		
		

		String sqlstr2 = "select t.record_id as record_id , branch_id , t.customer_id,  t.source_branch_code, t.branch_code, "
				+ " t.invoice_date, t.delivery_address," + " t.delivery_vehicle, "
				+ " h.voucher_number,net_invoice_amount, " + " c.customer_name  " + " from sales_trans_hdr t, "
				+ "sales_voucher_hdr h , " + " customer_mst c  where t.record_id = h.parent_id "
				+ " and t.customer_id = c.customer_id  ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {
			String customer_id = rs2.getString("customer_id");
			Date invoice_date = rs2.getDate("invoice_date");
			String delivery_address = rs2.getString("delivery_address");

			String delivery_vehicle = rs2.getString("delivery_vehicle");
			String record_id = rs2.getString("record_id");
			String voucher_number = rs2.getString("voucher_number");
			String customer_name = rs2.getString("customer_name");
			int branch_id = rs2.getInt("branch_id");
			float invoice_amount = rs2.getFloat("net_invoice_amount");
			String sourceBranchCode=rs2.getString("surce_branch_code");
			String branchcode=rs2.getString("t.branch_code");
			AccountHeads customerMst = null;

			Optional<AccountHeads> customerOpt = accountHeadsRepository.findById(customer_id);
			if (customerOpt.isPresent()) {
				customerMst = customerOpt.get();
			} else {
				List<AccountHeads> customerMstList = (List<AccountHeads>) accountHeadsRepository.findByAccountName(customer_name);
				if (customerMstList.size() > 0) {
					customerMst = customerMstList.get(0);
				} else {
					continue;
				}
			}

			OtherBranchSalesTransHdr   otherBranchSalesTransHdr= new  OtherBranchSalesTransHdr();
			otherBranchSalesTransHdr.setAccountHeads(customerMst);

			// salesTransHdr.setAmountTendered(amountTendered);
			Optional<BranchMst> branchMstOpt = branchMstRepository.findById(String.valueOf(branch_id));
			if (branchMstOpt.isPresent()) {

				otherBranchSalesTransHdr.setBranchCode(branchMstOpt.get().getBranchCode());
			}
			// salesTransHdr.setCardamount(cardamount);
			// salesTransHdr.setCardNo(cardNo);
			// salesTransHdr.setCardType(cardType);
			// salesTransHdr.setCashPay(cashPay);
			otherBranchSalesTransHdr.setCompanyMst(companyMst);
			// salesTransHdr.setCreditAmount(creditAmount);
			otherBranchSalesTransHdr.setCreditOrCash("CREDIT");
			otherBranchSalesTransHdr.setCustomerId(customer_id);
			otherBranchSalesTransHdr.setInvoiceAmount(Double.valueOf(invoice_amount));
			// salesTransHdr.setId(id);
			// salesTransHdr.setInvoiceAmount(invoiceAmount);
			// salesTransHdr.setInvoiceNumberPrefix(invoiceNumberPrefix);
			otherBranchSalesTransHdr.setIsBranchSales("YES");
			// salesTransHdr.setLocalCustomerMst(customer_id);
			// salesTransHdr.setMachineId(machineId);
			// salesTransHdr.setPaidAmount(paidAmount);
			// salesTransHdr.setSalesMode(salesMode);
			// salesTransHdr.setVoucherType(voucherType);
			otherBranchSalesTransHdr.setBranchCode(branchcode);
			otherBranchSalesTransHdr.setVoucherNumber(voucher_number);
			otherBranchSalesTransHdr.setSourceBranchCode(sourceBranchCode);
			otherBranchSalesTransHdr.setVoucherDate(invoice_date);
			// salesTransHdr.setUserId(userId);

			otherBranchSalesTransHdr = otherBranchSalesTransHdrRepository.save(otherBranchSalesTransHdr);

			String sqlstrDtl = "select i.item_name, i.unit_id,  "
					+ "d.item_id, d.tax_rate, d.qty, d.rate, d.purchase_rate , i.cess_rate "
					+ " from xpos_sales_dtl d , " + " sales_trans_hdr t, item_mst i   "
					+ " where t.record_id = d.parent_id  and" + " d.item_id = i.record_id " + " and  t.record_id = ? ";

			PreparedStatement pstDtl = RestserverApplication.localCn.prepareStatement(sqlstrDtl);
			pstDtl.setString(1, record_id);

			ResultSet rsDtl = pstDtl.executeQuery();

			while (rsDtl.next()) {

				String item_name = rsDtl.getString("item_name");
				Double tax_rate = rsDtl.getDouble("tax_rate");
				Double qty = rsDtl.getDouble("qty");
				Double rate = rsDtl.getDouble("rate");
				String unit_id = rsDtl.getString("unit_id");
				Double purchase_rate = rsDtl.getDouble("purchase_rate");
				String item_id = rsDtl.getString("item_id");

				Double cess_rate = rsDtl.getDouble("cess_rate");
				OtherBranchSalesDtl otherBranchSalesDtl = new OtherBranchSalesDtl();

				// salesDtl.setAddCessRate(addCessRate);
				// salesDtl.setAmount(amount);
				// salesDtl.setBarcode(barcode);
				// salesDtl.setBatch(batch);
				// salesDtl.setCessAmount(cessAmount);
				otherBranchSalesDtl.setCessRate(0.0);
				otherBranchSalesDtl.setCgstAmount(rate * qty * tax_rate / 200);
				otherBranchSalesDtl.setCgstTaxRate(tax_rate / 2);
				otherBranchSalesDtl.setCompanyMst(companyMst);
				otherBranchSalesDtl.setIgstAmount(0.0);
				otherBranchSalesDtl.setItemId(item_id);
				// salesDtl.setUnitName(unitName);
				otherBranchSalesDtl.setUnitId(unit_id);
				otherBranchSalesDtl.setTaxRate(tax_rate);
				otherBranchSalesDtl.setSgstTaxRate(tax_rate / 2);
				otherBranchSalesDtl.setSgstAmount(rate * qty * tax_rate / 200);
				otherBranchSalesDtl.setOtherBranchSalesTransHdr(otherBranchSalesTransHdr);;
				otherBranchSalesDtl.setRate(rate);
				otherBranchSalesDtl.setQty(qty);
				otherBranchSalesDtl.setMrp(purchase_rate);
				otherBranchSalesDtl.setItemName(item_name);
				otherBranchSalesDtl.setAddCessRate(0.0);

				otherBranchSalesDtl.setAmount(qty * rate + (qty * rate * tax_rate / 100));

				otherBranchSalesDtl.setOtherBranchSalesTransHdr(otherBranchSalesTransHdr);

				otherBranchSalesDtl.setBatch(MapleConstants.Nobatch);

				otherBranchSalesDtl.setCessAmount(qty * rate * cess_rate / 100);
				otherBranchSalesDtl.setDiscount(0.0);
				otherBranchSalesDtl.setIgstTaxRate(tax_rate);
				otherBranchSalesDtl.setIgstAmount(0.0);

				otherBranchSalesDtlRepository.saveAndFlush(otherBranchSalesDtl);

			}
			
			rsDtl.close();
			
			pstDtl.close();
			/*
			 * SalesReceipts salesReceipts = new SalesReceipts();
			 * salesReceipts.setAccountId(customer_id);
			 * salesReceipts.setBranchCode(branchCode);
			 * salesReceipts.setCompanyMst(companyMst); // salesReceipts.setId(id); //
			 * salesReceipts.setReceiptAmount(receiptAmount); //
			 * salesReceipts.setReceiptMode(receiptMode);
			 * salesReceipts.setSalesTransHdr(otherBranchSalesTransHdr); //
			 * salesReceipts.setUserId(userId);
			 * salesReceipts.setVoucherNumber(voucher_number);
			 * 
			 * salesReceipts = salesReceiptsRepository.save(salesReceipts);
			 * 
			 * otherBranchSalesTransHdr.setSalesReceiptsVoucherNumber(salesReceipts.getId())
			 * ;
			 */

			// --------

			/*
			 * 
			 * String SalesMode = salesTransHdr.getSalesMode();
			 * 
			 * Double invoiceAmount = salesTransHdr.getInvoiceAmount(); Double
			 * cashPaidAmount = salesTransHdr.getCashPay(); Double cardAmount =
			 * salesTransHdr.getCardamount(); Double sodexoAmount =
			 * salesTransHdr.getSodexoAmount(); Double creditAmount =
			 * salesTransHdr.getCreditAmount();
			 * 
			 * List<SalesReceipts> salesReceiptsList =
			 * salesTransHdr.findByCompanyMstIdAndSalesTransHdrId(companymstid,
			 * salesTransHdr.getId());
			 * 
			 * Iterator iter = salesReceiptsList.iterator(); while (iter.hasNext()) {
			 * SalesReceipts salesReceipts = (SalesReceipts) iter.next();
			 * salesReceipts.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			 * salesReceipts.setCompanyMst(salesTransHdrRequest.getCompanyMst());
			 * salesReceiptsRepo.saveAndFlush(salesReceipts);
			 * 
			 * }
			 * 
			 * AccountReceivable accountReceivable = accountReceivableRepository
			 * .findByCompanyMstAndSalesTransHdr(savedSalesTransHdr.getCompanyMst(),
			 * savedSalesTransHdr); if (null != accountReceivable) {
			 * accountReceivable.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			 * accountReceivable.setVoucherDate(salesTransHdrRequest.getVoucherDate());
			 * 
			 * accountReceivableRepository.saveAndFlush(accountReceivable); }
			 * 
			 * 
			 * 
			 * 
			 * 
			 * while(iter.hasNext()) { SalesReceipts salesReceipts = (SalesReceipts)
			 * iter.next();
			 * salesReceipts.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			 * salesReceipts.setCompanyMst(salesTransHdrRequest.getCompanyMst());
			 * salesReceiptsRepo.saveAndFlush(salesReceipts);
			 * 
			 * }
			 * 
			 * 
			 * 
			 * if(null!=accountReceivable) {
			 * accountReceivable.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			 * accountReceivable.setVoucherDate(salesTransHdrRequest.getVoucherDate());
			 * 
			 * accountReceivableRepository.saveAndFlush(accountReceivable); }
			 * 
			 * 
			 * 
			 * //Manage Stock
			 * 
			 * 
			 * 
			 * 
			 * salesAccounting.execute(salesTransHdrRequest.getVoucherNumber(),
			 * salesTransHdrRequest.getVoucherDate(), savedSalesTransHdr.getId(),
			 * savedSalesTransHdr.getCompanyMst());
			 * 
			 * 
			 * salesDayEndReporting.execute(salesTransHdrRequest.getVoucherNumber(),
			 * salesTransHdrRequest.getVoucherDate(), savedSalesTransHdr.getCompanyMst(),
			 * savedSalesTransHdr.getId());
			 * 
			 * 
			 * 
			 * SalesStockUpdate(salesTransHdrRequest.getVoucherNumber(),
			 * salesTransHdrRequest.getVoucherDate(), savedSalesTransHdr);
			 * 
			 * 
			 * 
			 * //Manage Accounting
			 * 
			 * 
			 * 
			 * 
			 * Map<String, Object> variables = new HashMap<String, Object>();
			 * 
			 * String creditOrCash = savedSalesTransHdr.getCreditOrCash(); if (null ==
			 * creditOrCash) { creditOrCash = "CASH"; }
			 * 
			 * if (null == savedSalesTransHdr.getIsBranchSales()) {
			 * savedSalesTransHdr.setIsBranchSales("N"); }
			 * 
			 * String isBranchSales = savedSalesTransHdr.getIsBranchSales(); int
			 * int_isBranchSales = 0; if (isBranchSales.equalsIgnoreCase("Y")) {
			 * int_isBranchSales = 1;
			 * 
			 * } else { int_isBranchSales = 0; }
			 * 
			 * variables.put("voucherNumber", savedSalesTransHdr.getVoucherNumber());
			 * variables.put("voucherDate", savedSalesTransHdr.getVoucherDate());
			 * variables.put("inet", 0); variables.put("id", savedSalesTransHdr.getId());
			 * variables.put("isbranchsales", int_isBranchSales); variables.put("companyid",
			 * savedSalesTransHdr.getCompanyMst());
			 * 
			 * if (serverorclient.equalsIgnoreCase("REST")) { variables.put("REST", 1); }
			 * else { variables.put("REST", 0); }
			 * 
			 * if (creditOrCash.equalsIgnoreCase("CREDIT") ||
			 * creditOrCash.equalsIgnoreCase("B2B")) { variables.put("CREDIT", 1); } else {
			 * variables.put("CREDIT", 0); }
			 * 
			 * variables.put("WF", "forwardSales"); eventBus.post(variables);
			 * 
			 */
			// --------

		}
		rs2.close();
		
		pst2.close();
		
		return null;
	}
	
	
	@Override
	public String transferPurchase(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {

		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select h.record_id ,branch_id, tr_entry_date, invoice_date,"
				+ " invoice_number, voucher_number, s.supplier_name, " + " net_invoice_total "
				+ " from purchase_hdr h, supplier_mst s where h.supplier_id  = s.record_id  ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {
			String record_id = rs2.getString("record_id");
			Date tr_entry_date = rs2.getDate("tr_entry_date");
			Date invoice_date = rs2.getDate("invoice_date");
			String invoice_number = rs2.getString("voucher_number");
			String voucher_number = rs2.getString("invoice_number");
			String supplier_name = rs2.getString("supplier_name");
			int branch_id = rs2.getInt("branch_id");

			AccountHeads accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMst(supplier_name, companyMst);

			Double net_invoice_total = rs2.getDouble("net_invoice_total");

			List<PurchaseHdr> purchaseHdrOpt = purchaseHdrRepository.findByVoucherNumberAndVoucherDate(voucher_number,
					tr_entry_date);
			if (purchaseHdrOpt.size() > 0) {
				continue;
			}

			PurchaseHdr purchaseHdr = new PurchaseHdr();
			purchaseHdr.setInvoiceTotal(net_invoice_total);
			purchaseHdr.setSupplierInvNo(invoice_number);
			purchaseHdr.setSupplierInvDate((java.sql.Date) invoice_date);
			purchaseHdr.setVoucherNumber(voucher_number);
			purchaseHdr.setSupplierId(accountHeads.getId());
			purchaseHdr.setCompanyMst(companyMst);
			purchaseHdr.setVoucherDate(tr_entry_date);

			Optional<BranchMst> branchMstOpt = branchMstRepository.findById(String.valueOf(branch_id));
			if (branchMstOpt.isPresent()) {
				purchaseHdr.setBranchCode(branchMstOpt.get().getBranchCode());
			}
			// purchaseHdr.setBranchCode(branchCode);

			purchaseHdr.setTansactionEntryDate(tr_entry_date);

			if (voucher_number.equalsIgnoreCase("INPROGRESS")) {
				purchaseHdr.setFinalSavedStatus("N");
			} else {
				purchaseHdr.setFinalSavedStatus("Y");
			}

			purchaseHdr = purchaseHdrRepository.save(purchaseHdr);

			String sqlstrDtl = "select i.item_name , d.qty, d.purchase_rate, " + "d. tax_rate, d.tax_amount  "
					+ " from purchasE_dtl d, item_mst i , purchase_hdr h " + " where h.record_id = d.parent_id and \n"
					+ "				d.item_id = i.record_id " + " and  h.record_id = ? ";

			PreparedStatement pstDtl = RestserverApplication.localCn.prepareStatement(sqlstrDtl);
			pstDtl.setString(1, record_id);

			ResultSet rsDtl = pstDtl.executeQuery();

			int itemSerial = 0;
			String itemid = "";
			String unitid = "";
			while (rsDtl.next()) {
				itemSerial++;

				String item_name = rsDtl.getString("item_name");

				item_name = item_name.replaceAll("'", "");
				item_name = item_name.replaceAll("&", "");

				item_name = item_name.replaceAll("/", "");
				item_name = item_name.replaceAll("%", "");
				item_name = item_name.replaceAll("\"", "");

				Double qty = rsDtl.getDouble("qty");
				Double purchase_rate = rsDtl.getDouble("purchase_rate");
				Double tax_rate = rsDtl.getDouble("tax_rate");
				Double tax_amount = rsDtl.getDouble("tax_amount");

				ItemMst item = itemMstRepository.findByItemName(item_name);

				if (null == item) {
					itemid = item_name;
					unitid = "0";
				} else {
					itemid = item.getId();
					unitid = item.getUnitId();

				}

				PurchaseDtl purchaseDtl = new PurchaseDtl();

				// purchaseDtl.setAmount(amount);
				// purchaseDtl.setBarcode(barcode);
				// purchaseDtl.setCessRate(cessRate);
				// purchaseDtl.setCessAmt(cessAmt);

				purchaseDtl.setTaxRate(tax_rate);
				purchaseDtl.setItemId(itemid);
				purchaseDtl.setTaxAmt(tax_amount);
				purchaseDtl.setQty(qty);
				purchaseDtl.setPurchseRate(purchase_rate);
				purchaseDtl.setPurchaseHdr(purchaseHdr);
				purchaseDtl.setUnitId(unitid);
				purchaseDtl.setItemSerial(itemSerial);

				purchaseDtl.setCessAmt(0.0);
				purchaseDtl.setCessRate(0.0);
				purchaseDtl.setPreviousMRP(0.0);

				purchaseDtl.setFreeQty(0);
				purchaseDtl.setAmount(purchase_rate * qty);

				purchaseDtl.setDiscount(0.0);

				purchaseDtl.setMrp(0.0);

				purchaseDtl.setItemName(item_name);
				purchaseDtlRepository.saveAndFlush(purchaseDtl);

			}
			rsDtl.close();
			pstDtl.close();

		}
		rs2.close();
		pst2.close();
		return null;
	}

	@Override
	public String transferReceipts(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {

		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select h.voucher_date,h.voucher_type,h.voucher_number,h.transdate,d.account,"
				+ "d.mode_ofpayment,d.amount,d.instdate,d.deposit_bank,d.bank_account_number,d.remark,"
				+ "d.debit_account_id,d.instnumber,d.invoice_number from receipt_hdr h,receipt_dtl d"
				+ "where h.id=d.receipt_hdr_id ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();
		while (rs2.next()) {

			String record_id = rs2.getString("record_id");
			Date voucher_date = rs2.getDate("voucher_date");
			String voucher_type = rs2.getString("voucher_type");
			String voucher_number = rs2.getString("voucher_number");
			Date transdate = rs2.getDate("transdate");

			String account = rs2.getString("account");
			String mode_ofpayment = rs2.getString("mode_ofpayment");
			Double amount = rs2.getDouble("amount");
			Date instdate = rs2.getDate("instdate");
			String deposit_bank = rs2.getString("deposit_bank");
			String bank_account_number = rs2.getString("bank_account_number");
			String remark = rs2.getString("remark");
			String debit_account_id = rs2.getString("debit_account_id");
			String instnumber = rs2.getString("instnumber");
			String invoice_number = rs2.getString("invoice_number");

			ReceiptHdr receiptHdr = new ReceiptHdr();
			receiptHdr.setVoucherNumber(voucher_number);
			receiptHdr.setTransdate(transdate);
			receiptHdr.setVoucherType(voucher_type);
			receiptHdr.setVoucherDate(voucher_date);
			receiptHdr = receiptHdrRepository.save(receiptHdr);

			ReceiptDtl receiptDtl = new ReceiptDtl();

			receiptDtl.setAccount(account);
			receiptDtl.setInstdate(instdate);
			receiptDtl.setAmount(amount);
			receiptDtl.setBankAccountNumber(bank_account_number);
			receiptDtl.setBranchCode(branchCode);
			receiptDtl.setDebitAccountId(debit_account_id);
			receiptDtl.setDepositBank(deposit_bank);
			receiptDtl.setInstnumber(instnumber);
			receiptDtl.setInvoiceNumber(invoice_number);
			receiptDtl.setInstrumentDate(instdate);
			receiptDtl.setModeOfpayment(mode_ofpayment);
			receiptDtl.setRemark(remark);
			// receiptDtl.setTransDate(transDate);
			receiptDtl.setReceiptHdr(receiptHdr);
			receiptDtlRepository.save(receiptDtl);

		}
		rs2.close();
		pst2.close();
		return null;
	}

	@Override
	public String transferPayments(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {

		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select h.voucher_date,h.voucher_type,h.voucher_number,h.user_id,"
				+ "d.account_id,d.mode_ofpayment,d.amount,d.instrument_date,d.instrument_number,"
				+ " d.credit_account_id,d.bank_account_id,d.remark from payment_hdr h,payment_dtl d"
				+ " where h.id=d.paymenthdr_id ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();
		while (rs2.next()) {

			String record_id = rs2.getString("record_id");
			Date voucher_date = rs2.getDate("voucher_date");
			String voucher_type = rs2.getString("voucher_type");
			String voucher_number = rs2.getString("voucher_number");
			String user_id = rs2.getString("user_id");

			String account_id = rs2.getString("account_id");
			String mode_ofpayment = rs2.getString("mode_ofpayment");
			Double amount = rs2.getDouble("amount");
			Date instrument_date = rs2.getDate("instrument_date");
			String instrument_number = rs2.getString("instrument_number");
			String credit_account_id = rs2.getString("credit_account_id");
			String bank_account_id = rs2.getString("bank_account_id");
			String remark = rs2.getString("remark");

			PaymentHdr paymentHdr = new PaymentHdr();
			paymentHdr.setTVoucherDate(voucher_date);
			paymentHdr.setVoucherNumber(voucher_number);
			paymentHdr.setVoucherType(voucher_type);
			paymentHdr.setUserId(user_id);
			paymentHdr = paymentHdrRepository.save(paymentHdr);

			PaymentDtl paymentDtl = new PaymentDtl();
			paymentDtl.setAccountId(account_id);
			paymentDtl.setAmount(amount);
			paymentDtl.setBankAccountNumber(bank_account_id);
			paymentDtl.setCreditAccountId(credit_account_id);
			paymentDtl.setInstrumentDate(instrument_date);
			paymentDtl.setInstrumentNumber(instrument_number);
			paymentDtl.setModeOfPayment(mode_ofpayment);
			paymentDtl.setRemark(remark);
			// paymentDtl.setTransDate(transDate);
			paymentDtl.setPaymenthdr(paymentHdr);
			paymentDtlRepository.save(paymentDtl);

		}
		rs2.close();
		pst2.close();
		return null;
	}

	@Override
	public String transferJournal(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {
		// Connection RestserverApplication.localCn = getDbConn("SOURCEDB");

		String sqlstr2 = "select h.voucher_date,h.transdate,h.voucher_number,d.account_head,d.debit_amount,"
				+ "d.credit_amount,d.remarks from journal_hdr h,journal_dtl where h.id=d.journal_hdr";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();
		while (rs2.next()) {

			Date voucher_date = rs2.getDate("voucher_date");
			Date transdate = rs2.getDate("trans_date");
			String voucher_number = rs2.getString("voucher_number");

			String account_head = rs2.getString("account_head");
			Double debit_amount = rs2.getDouble("debit_amount");
			Double credit_amount = rs2.getDouble("credit_amount");
			String remarks = rs2.getString("remarks");

			JournalHdr journalHdr = new JournalHdr();
			journalHdr.setVoucherNumber(voucher_number);
			journalHdr.setVoucherDate((java.sql.Date) voucher_date);
			journalHdr.setTransDate((java.sql.Date) transdate);
			journalHdr = journalHdrRepository.save(journalHdr);

			JournalDtl journalDtl = new JournalDtl();
			journalDtl.setAccountHead(account_head);
			journalDtl.setCreditAmount(credit_amount);
			journalDtl.setDebitAmount(debit_amount);
			journalDtl.setRemarks(remarks);
			journalDtl.setJournalHdr(journalHdr);
			journalDtlRepository.save(journalDtl);

		}
		rs2.close();
		pst2.close();
		return null;
	}

	@Override
	public String salesDataCorrectionFromItemBatchDtl(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		List<SalesTransHdr> salestransHdrList = salesTransHdrRepository.findAll();
		Iterator iter = salestransHdrList.iterator();
		while (iter.hasNext()) {
			SalesTransHdr salesTransHdr = (SalesTransHdr) iter.next();

			String sourceVoucher = salesTransHdr.getVoucherNumber();
			if (null == sourceVoucher) {
				continue;
			}

			System.out.println("Voucher " + sourceVoucher);

			List<ItemBatchDtl> itembatchList = itemBatchDtlRepository.getItemNotInSalesDtl(sourceVoucher);

			Iterator iterBatch = itembatchList.iterator();
			while (iterBatch.hasNext()) {
				ItemBatchDtl itemBatchDtl = (ItemBatchDtl) iterBatch.next();

				Optional<ItemMst> itemMstOpt = itemMstRepository.findById(itemBatchDtl.getItemId());
				ItemMst itemMst = itemMstOpt.get();
				SalesDtl salesDtl = new SalesDtl();

				Optional<UnitMst> unitMstOpt = unitMstRepository.findById(itemMst.getUnitId());
				UnitMst unitMst = unitMstOpt.get();

				Double cessRate = 0.0;
				Double cessAmount = 0.0;
				Double rate = 0.0;

				salesDtl.setAddCessRate(0.0);
				Double amount = itemBatchDtl.getQtyOut() * itemMst.getStandardPrice();
				salesDtl.setAmount(amount);
				salesDtl.setBarcode(itemMst.getBarCode());
				salesDtl.setBatch(MapleConstants.Nobatch);
				if (itemMst.getCess() > 0) {
					cessRate = itemMst.getCess();

					rate = (100 * itemMst.getStandardPrice()) / (100 + itemMst.getTaxRate() + itemMst.getCess());
					cessAmount = itemBatchDtl.getQtyOut() * rate * itemMst.getCess() / 100;

				} else {
					rate = (100 * itemMst.getStandardPrice()) / (100 + itemMst.getTaxRate());
				}
				salesDtl.setCessAmount(cessAmount);
				salesDtl.setCessRate(cessRate);

				Double cgstTaxRate = itemMst.getTaxRate() / 2;
				Double sgstTaxRate = itemMst.getTaxRate() / 2;
				Double cgstAmount = cgstTaxRate * itemBatchDtl.getQtyOut() * rate / 100;
				Double sgstAmount = cgstTaxRate * itemBatchDtl.getQtyOut() * rate / 100;

				salesDtl.setCgstAmount(cgstAmount);
				salesDtl.setCgstTaxRate(cgstTaxRate);
				salesDtl.setCompanyMst(companyMst);
				salesDtl.setSgstTaxRate(sgstTaxRate);
				salesDtl.setDiscount(0.0);
				salesDtl.setIgstAmount(0.0);
				salesDtl.setIgstTaxRate(0.0);
				salesDtl.setItemId(itemBatchDtl.getItemId());
				salesDtl.setItemName(itemMst.getItemName());
				salesDtl.setMrp(itemMst.getStandardPrice());
				salesDtl.setQty(itemBatchDtl.getQtyOut());

				salesDtl.setRate(rate);
				salesDtl.setReturnedQty(0.0);
				salesDtl.setSalesTransHdr(salesTransHdr);
				salesDtl.setSgstAmount(sgstAmount);
				salesDtl.setTaxRate(itemMst.getTaxRate());
				salesDtl.setUnitId(unitMst.getId());
				salesDtl.setUnitName(unitMst.getUnitName());

				salesDetailsRepository.saveAndFlush(salesDtl);

			}

		}

		return null;
	}

	@Override
	public String removeItemBatchDuplicate(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		List<SalesTransHdr> salestransHdrList = salesTransHdrRepository.findAll();
		Iterator iter = salestransHdrList.iterator();
		while (iter.hasNext()) {
			SalesTransHdr salesTransHdr = (SalesTransHdr) iter.next();

			String sourceVoucher = salesTransHdr.getVoucherNumber();
			if (null == sourceVoucher) {
				continue;
			}

			System.out.println("Voucher " + sourceVoucher);

			itemBatchDtlRepository.deleteBySourceVoucherNumber(sourceVoucher);

			List<SalesDtl> salesDtlList = salesDetailsRepository.findBySalesTransHdrId(salesTransHdr.getId());

			Iterator iterDtl = salesDtlList.iterator();
			while (iterDtl.hasNext()) {
				SalesDtl salesDtl = (SalesDtl) iterDtl.next();

				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
				final VoucherNumber voucherNo = voucherService.generateInvoice("STV",
						salesTransHdr.getCompanyMst().getId());
				itemBatchDtl.setBarcode(salesDtl.getBarcode());
				itemBatchDtl.setBatch(salesDtl.getBatch());
				itemBatchDtl.setItemId(salesDtl.getItemId());
				itemBatchDtl.setMrp(salesDtl.getMrp());
				itemBatchDtl.setQtyIn(0.0);
				itemBatchDtl.setQtyOut(salesDtl.getQty());
				itemBatchDtl.setVoucherNumber(voucherNo.getCode());
				itemBatchDtl.setVoucherDate(salesTransHdr.getVoucherDate());
				itemBatchDtl.setItemId(salesDtl.getItemId());
				itemBatchDtl.setSourceVoucherNumber(salesTransHdr.getVoucherNumber());
				itemBatchDtl.setSourceVoucherDate(salesTransHdr.getVoucherDate());
				itemBatchDtl.setCompanyMst(salesTransHdr.getCompanyMst());
				itemBatchDtl.setBranchCode(salesTransHdr.getBranchCode());

				itemBatchDtl.setParticulars("SALES " + salesTransHdr.getAccountHeads().getAccountName());

				itemBatchDtl = itemBatchDtlRepository.saveAndFlush(itemBatchDtl);

			}
		}

		return null;
	}

	@Override
	public String pushstocktoServer(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateItemBatchMstStockFromDtl(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {

		logger.info("updateStockFromDtlStep0");

		itemBatchMstRepo.updateStockFromDtlStep0();

		itemBatchMstRepo.updateStockFromDtlStep1(branchCode);

		logger.info("updateStockFromDtlStep2");

		itemBatchMstRepo.updateStockFromDtlStep2();

		logger.info("updateStockFromDtlStep3");

		itemBatchMstRepo.updateStockFromDtlStep3();

		logger.info("updateStockFromDtlStep4");

		itemBatchMstRepo.updateStockFromDtlStep4();

		return null;
	}

	@Override
	public String updateItemBatchMstStockBranch(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception {
		itemBatchMstRepo.updateStockFromDtlStep1(branchCode);
		return null;
	}

	@Override
	public String updateUnitFromPurchaseDtl(CompanyMst companyMst, String branchcode) {
		purchaseDtlRepository.updatePuchase_Dtl(branchcode);
		return null;
	}

	@Override
	public String updateUnitFromPriceDefenition(CompanyMst companyMst, String branchcode) {
		itemMstRepository.updateItemMst(branchcode);
		priceDefinitionRepo.updatePriceDefenition(branchcode);
		return null;
	}

	@Override
	public String transferMinStock(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {

		List<ItemMst> itemArray = itemMstRepository.findAll();

		System.out.println("Starting loop");
		Iterator iter = itemArray.iterator();

		while (iter.hasNext()) {
			ItemMst itemMst = (ItemMst) iter.next();

			System.out.println("Inside  loop" + itemMst.getItemName());

			double qty = 10000;

			if (null == itemMst) {
				continue;
			}

			ItemBatchMst itemBatchMst = new ItemBatchMst();

			if (null != itemMst.getBarCode()) {
				itemBatchMst.setBarcode(itemMst.getBarCode());
			}
			itemBatchMst.setBatch(MapleConstants.Nobatch);
			itemBatchMst.setMrp(itemMst.getStandardPrice());
			itemBatchMst.setQty(qty);

			itemBatchMst.setItemId(itemMst.getId());

			itemBatchMst.setCompanyMst(companyMst);

			itemBatchMst.setBranchCode(branchCode);
			itemBatchMstRepo.save(itemBatchMst);

			System.out.println("itemBatchMstRepo  save" + itemMst.getItemName());

			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
			VoucherNumber vNo = voucherNumberService.generateInvoice("OPN", companyMst.getId());

			itemBatchDtl.setBarcode(itemMst.getBarCode());
			itemBatchDtl.setBatch(MapleConstants.Nobatch);
			itemBatchDtl.setItemId(itemMst.getId());
			itemBatchDtl.setMrp(itemMst.getStandardPrice());
			itemBatchDtl.setQtyIn(qty);
			itemBatchDtl.setQtyOut(0.0);
			itemBatchDtl.setBranchCode(branchCode);
			itemBatchDtl.setParticulars("Opening Stock Transferred");
			itemBatchDtl.setVoucherNumber(vNo.getCode());
			itemBatchDtl.setVoucherDate(SystemSetting.systemDate);

			itemBatchDtl.setStore(MapleConstants.Store);
			itemBatchDtl.setCompanyMst(companyMst);

			itemBatchDtlRepository.save(itemBatchDtl);

			System.out.println("DTL   save" + itemMst.getItemName());

		}

		return null;

	}

	@Override
	@Transactional
	public String updateBillSeries(CompanyMst companyMst, String branchCode, String prefix, String newPrefix) {

		List salesTransHdrList = salesTransHdrRepository.findByVoucherNumberLikePrefix(prefix);
		for (int i = 0; i < salesTransHdrList.size(); i++) {
			String oldvoucher = salesTransHdrList.get(i).toString();
			String subStr = oldvoucher.substring(prefix.length(), oldvoucher.length());
			String newvoucher = newPrefix + subStr;
			salesTransHdrRepository.updateVoucherNumber(newvoucher, oldvoucher);
		}
		return "success";
	}

	@Transactional
	@Override
	public String updateUDateCorrection(CompanyMst companyMst, String branchcode, int startVoucher, int endVoucher,
			java.sql.Date oldDate, java.sql.Date newDate, String vouchersuffix) {

		salesTransHdrRepository.updateVoucherDate(startVoucher, endVoucher, oldDate, newDate);

//	salesTransHdrRepository.updateAccountClassVoucherDate( startVoucher,   endVoucher , oldDate, newDate);
//	salesTransHdrRepository.updateLedgerClassVoucherDate(startVoucher, endVoucher, oldDate, newDate);
//	salesTransHdrRepository.updateDebitClassVoucherDate(startVoucher, endVoucher, oldDate, newDate);
//	salesTransHdrRepository.updateCreditClassVoucherDate(startVoucher, endVoucher, oldDate, newDate);

		return null;
	}

//	@Transactional
//	@Override
//	public String updateSupplierIdAsAccountId() {
//
//		List<Object> supNameList = supplierRepository.getSupplierListNotInAccountHeads();
//		for (int i = 0; i < supNameList.size(); i++) {
//			Object[] objAray = (Object[]) supNameList.get(i);
//			String supName = (String) objAray[0];
//			String supId = (String) objAray[1];
//			AccountHeads accounHeads = accountHeadsRepository.findByAccountName(supName);
//			purchaseHdrRepository.updateSupplierId(supId, accounHeads.getId());
//			System.out.println("SUPPLIER NAME" + supName);
//			supplierRepository.updateSupplierId(supId, accounHeads.getId());
//			
//		}
//		return "Done";
//	}

	@Transactional
	@Override
	public String removeDuplicateVOucherinSalesTransHdr(Date vdate) {
		List<String> voucherList = salesTransHdrRepository.getDuplicateVoucher(vdate);
		for (int i = 0; i < voucherList.size(); i++) {
			List<String> transHdrIdList = salesTransHdrRepository.getSalesTransHdrIdbyVoucher(voucherList.get(i));
			itemBatchDtlRepository.deleteBySourceParentId(transHdrIdList.get(0));
			accountClassRepo.deleteBySourceParentId(transHdrIdList.get(0));
			Optional<SalesTransHdr> salesTransHdrOp = salesTransHdrRepository.findById(transHdrIdList.get(0));
			if (salesTransHdrOp.isPresent()) {
				salesReceiptsRepository.deleteBySalesTransHdr(salesTransHdrOp.get());
			}
			salesTransHdrRepository.deleteById(transHdrIdList.get(0));
		}

		return "Done";
	}

 
	@Override
	public String transferOrgAccountHeads(CompanyMst companyMst) throws UnknownHostException, Exception {
		String sqlstr2 = "select * from account_heads";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			String id = rs2.getString("account_id");
			String account_name = rs2.getString("account_name");
			String branch_code = rs2.getString("branch_code");
			String parent_account_id = rs2.getString("parent_account_id");
			AccountHeads accountHeads = new AccountHeads();
			accountHeads.setId(id);
			accountHeads.setAccountName(account_name);

			System.out.print(account_name + "account name issssssssssssssssssssssssssssssssssssss");
			accountHeads.setCompanyMst(companyMst);
			// accountHeads.setBranchCode(branch_code);
			accountHeads.setParentId(parent_account_id);
			accountHeads.setCompanyMst(companyMst);
			accountHeadsRepository.saveAndFlush(accountHeads);

		}
		rs2.close();
		pst2.close();
		return null;
	}

 

	@Override
	public String doSalesTransHdrTransfer(CompanyMst companyMst, String branchcode)
			throws UnknownHostException, Exception {

		String sqlstr2 = " select h.record_id, h.user_id , h.company_id ,h. branch_id ,h.godown_id, h.machine_id ,h.shift_id ,h.invoice_date , h.customer_id , "
				+ "  h.customer_type_id , h.invoice_type_id , h.posted_to_tally , h.tally_voucher_id ,h.summary  , h.str_invoice_date , h.is_summarised, h.doctor_id, h.salesman_id , h.delivery_address ,h.financial_year, h.finance_copmany_id ,h.delivery_vehicle ,"
				+ "v.invoice_amount,v.voucher_number ,v. net_invoice_amount,v.invoice_discount ,v.voucher_type,d.record_id,d.item_id,d.item_name,d.tax_rate ,d.qty,d.rate,d.unit_id,d.manufacturing_date,d.expiry_date,d.item_serial_no,d.batch_code,d.fc_rate,d.fc_discount,d.fc_tax_rate,d.barcode,d.hsn_code "
				+ " from sales_trans_hdr h ,sales_voucher_hdr v,xpos_sales_dtl d  where h.record_id=v.parent_id and d.parent_id=h.record_id ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			String record_id = rs2.getString("h.record_id");
			String user_id = rs2.getString("h.user_id");
			String company_id = rs2.getString("h.godown_id");
			String machine_id = rs2.getString("h.machine_id");
			String branch_id = rs2.getString("h.branch_id");
			String shift_id = rs2.getString("h.shift_id");
			String invoice_date = rs2.getString("h.invoice_date");
			String customer_id = rs2.getString("h.customer_id");
			String customer_type_id = rs2.getString("h.customer_type_id");
			String invoice_type_id = rs2.getString("h.invoice_type_id");
			String posted_to_tally = rs2.getString("h.posted_to_tally");
			String tally_voucher_id = rs2.getString("h.tally_voucher_id");
			String summary = rs2.getString("h.summary");
			String str_invoice_date = rs2.getString("h.str_invoice_date");
			String is_summarised = rs2.getString("h.is_summarised");
			String doctor_id = rs2.getString("h.doctor_id");
			String salesman_id = rs2.getString("h.salesman_id");
			// String tally_reply=rs2.getString("h.tally_reply");
			String delivery_address = rs2.getString("h.delivery_address");
			String financial_year = rs2.getString("h.financial_year");
			String finance_copmany_id = rs2.getString("h.finance_copmany_id");
			String delivery_vehicle = rs2.getString("h.delivery_vehicle");
			String voucher_number = rs2.getString("v.voucher_number");
			float invoice_amount = rs2.getFloat("v.invoice_amount");
			float invoice_discount = rs2.getFloat("v.invoice_discount");

			String dRecordId = rs2.getString("d.record_id");
			String item_id = rs2.getString("d.item_id");
			String item_name = rs2.getString("d.item_name");
			float tax_rate = rs2.getFloat("d.tax_rate");
			float qty = rs2.getFloat("d.qty");
			float rate = rs2.getFloat("d.rate");
			String unit_id = rs2.getString("d.unit_id");
			String manufacturing_date = rs2.getString("d.manufacturing_date");
			String expiry_date = rs2.getString("d.expiry_date");
			String item_serial_no = rs2.getString("d. item_serial_no");

			String batch_code = rs2.getString("d.batch_code");
			float fc_rate = rs2.getFloat("d.fc_rate");
			float fc_discount = rs2.getFloat("d.fc_discount");
			float fc_tax_rate = rs2.getFloat("d.fc_tax_rate");
			String hsn_code = rs2.getString("d.hsn_code");
			String barcode = rs2.getString("d.barcode");
			// Double cost=rs2.getDouble("d.cost");

			SalesTransHdr salesTransHdr = new SalesTransHdr();
			Optional<BranchMst> branchMstOpt = branchMstRepository.findById(branch_id);
			if (branchMstOpt.isPresent()) {
				salesTransHdr.setBranchCode(branchMstOpt.get().getBranchCode());
			}
			salesTransHdr.setCompanyMst(companyMst);
			Optional<AccountHeads> customerMstOpt = accountHeadsRepository.findById(customer_id);
			if (customerMstOpt.isPresent()) {
				salesTransHdr.setAccountHeads(customerMstOpt.get());
			}
			salesTransHdr.setId(record_id);
			salesTransHdr.setUserId(user_id);
			salesTransHdr.setMachineId(machine_id);

			java.util.Date voucherDate = SystemSetting.StringToUtilDate(invoice_date, "yyyy-MM-dd");

			salesTransHdr.setVoucherDate(voucherDate);
			salesTransHdr.setSalesManId(salesman_id);
			salesTransHdr.setVoucherNumber(voucher_number);
			salesTransHdr.setInvoiceAmount(Double.valueOf(invoice_amount));

			salesTransHdr.setInvoiceDiscount(Double.valueOf(invoice_discount));

			salesTransHdrRepository.save(salesTransHdr);

			SalesDtl salesDtl = new SalesDtl();
			salesDtl.setId(dRecordId);
			salesDtl.setBatch(batch_code);
			salesDtl.setAmount(Double.valueOf(rate * qty));
//		salesDtl.setCessAmount(cessAmount);
//		salesDtl.setIgstAmount(igstAmount);
			salesDtl.setItemId(item_id);
			salesDtl.setItemName(item_name);
			salesDtl.setBarcode(barcode);
			java.sql.Date expDate = SystemSetting.StringToSqlDate(expiry_date, "yyyy-MM-dd");
			salesDtl.setExpiryDate(expDate);
			salesDtl.setDiscount(Double.valueOf(fc_discount));
			// salesDtl.setIgstTaxRate(igstTaxRate);
			// salesDtl.setCompanyMst(companyMst);
			// salesDtl.setFcCgst(fcCgst);
			// salesDtl.setFcIgstRate(fcIgstRate);
			// salesDtl.setFcMrp(cost);
			// salesDtl.setFcStandardPrice(fcStandardPrice);
			salesDtl.setFcRate(Double.valueOf(fc_rate));
			salesDtl.setFcDiscount(Double.valueOf(fc_discount));
			salesDtl.setFcTaxRate(Double.valueOf(fc_tax_rate));
			salesDtl.setItemTaxaxId("NOT IN USE");
			salesDtl.setOfferReferenceId("NOT IN USE");
			salesDtl.setUnitId(unit_id);
			Optional<SalesTransHdr> salesTransHdrOpt = salesTransHdrRepository.findById(record_id);
			if (salesTransHdrOpt.isPresent()) {
				salesDtl.setSalesTransHdr(salesTransHdrOpt.get());
			}
			salesDetailsRepository.save(salesDtl);

		}	
		rs2.close();
		pst2.close();

		return null;
	}

	@Override
	public String doPurchaseTransfer(CompanyMst companyMst, String branchcode) throws UnknownHostException, Exception {

		/*
		 * String sqlstr2
		 * =" select h.LOGGED_USER,h.TR_ENTRY_DATE, h.invoice_date,h.net_invoice_total,h.COMPANY_ID,"
		 * +
		 * "h.branch_id,h.godown_id,h.record_id,h.machine_id,h.NARRATION,h.currency_name,h.conversion_rate,h.final_saved,h.purchase_typ,h.po_number,h.grn_number,h.is_grn_loaded,d.record_id,d.parent_id,d.item_name,d.item_id,d.batch,d.bin_no,d.expiry_date,d.manufacturing_date,d.qty,d.purchase_rate,d.unit_id,d.tax_rate,d.sale_rate,d.whole_sales_rate,d.updated_to_server,d.barcode,d.fc_rate,d.net_cost,d.purchase_date_time,d.item_code,d.effective_purchase_cost,d.mrp,d.previous_mrp,d.machine_id,d.tax_amount,d.exp_allocation,d.isfreeqty from purchase_hdr h,purchase_dtl d where h.record_id=d.parent_id"
		 * ;
		 */

		String sqlstr2 = "select * from purchase_hdr";
		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();
		while (rs2.next()) {
			int logged_user = rs2.getInt("LOGGED_USER");
			Date tr_entry_date = rs2.getDate("TR_ENTRY_DATE");
			// Date invoice_date=rs2.getDate("invoice_number");
			String invoice_number = rs2.getString("invoice_number");
			String voucher_number = rs2.getString("voucher_number");
			int instance_id = rs2.getInt("instance_id");
			int supplier_id = rs2.getInt("supplier_id");
			int updated_to_server = rs2.getInt("updated_to_server");
			int posted_to_tally = rs2.getInt("posted_to_tally");
			String tally_voucher_id = rs2.getString("tally_voucher_id");
			float net_invoice_total = rs2.getFloat("net_invoice_total");
			int COMPANY_ID = rs2.getInt("COMPANY_ID");
			int branch_id = rs2.getInt("branch_id");
			int godown_id = rs2.getInt("godown_id");
			int record_id = rs2.getInt("record_id");
			String machine_id = rs2.getString("machine_id");
			String NARRATION = rs2.getString("NARRATION");
			String currency_name = rs2.getString("currency_name");
			Double conversion_rate = rs2.getDouble("conversion_rate");
			int final_saved = rs2.getInt("final_saved");
			String purchase_typ = rs2.getString("purchase_typ");
			String po_number = rs2.getString("po_number");
			String grn_number = rs2.getString("grn_number");
			int is_grn_loaded = rs2.getInt("is_grn_loaded");

			PurchaseHdr purchaseHdr = new PurchaseHdr();

			purchaseHdr.setCompanyMst(companyMst);

			Optional<BranchMst> branchMstOpt = branchMstRepository.findById(String.valueOf(branch_id));
			if (branchMstOpt.isPresent()) {
				purchaseHdr.setBranchCode(branchMstOpt.get().getBranchCode());
			}
			// purchaseHdr.setFcInvoiceTotal(fcInvoiceTotal);

			purchaseHdr.setVoucherNumber(voucher_number);
			purchaseHdr.setMachineId(machine_id);
			purchaseHdr.setNarration(NARRATION);
			purchaseHdr.setSupplierId(String.valueOf(supplier_id));
			purchaseHdr.setTansactionEntryDate(tr_entry_date);
			purchaseHdr.setUserId(String.valueOf(logged_user));
			// purchaseHdr.setVoucherDate(invoice_date);
			// purchaseHdr.setPurchaseType(purchaseType);
			String recordId = String.valueOf(record_id);
			purchaseHdr.setId(recordId);
			purchaseHdr.setpONum(po_number);
			// purchaseHdr.setCurrency(currency);
			purchaseHdrRepository.save(purchaseHdr);

			System.out.print(purchaseHdr.getId() + "purchase hdr id isssssssssssssssssssssssssssssss");
		}

		rs2.close();
		pst2.close();
		return null;
	}

	@Override
	public String doItemBatchDtlTransfer(CompanyMst companyMst, String branchcode) throws SQLException {

		String sqlstr2 = " select item_id,barcode,batch_code,item_code,sale_qty,purchase_qty,unit_id,posted_to_server,qty_modification_id,machine_id,branch_code,record_id,description,source_record_id,trans_date,purchase_rate,selling_rate,profit,expiry_date,godown_code,tax_rate ,from_branch_id,to_branch_id,trans_in_record,from_branch_name,to_branch_name,party_name,voucher_number,voucher_date from item_batch_dtl ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();
		while (rs2.next()) {

			int item_id = rs2.getInt("item_id");
			String barcode = rs2.getString("barcode");
			String batch_code = rs2.getString("batch_code");
			String item_code = rs2.getString("item_code");
			Double sale_qty = rs2.getDouble("sale_qty");
			Double purchase_qty = rs2.getDouble("purchase_qty");
			int unit_id = rs2.getInt("unit_id");
			int posted_to_server = rs2.getInt("posted_to_server");
			String qty_modification_id = rs2.getString("qty_modification_id");
			String machine_id = rs2.getString("machine_id");
			String branch_code = rs2.getString("branch_code");
			int record_id = rs2.getInt("record_id");

			String description = rs2.getString("description");
			String source_record_id = rs2.getString("source_record_id");
			Date trans_date = rs2.getDate("trans_date");
			Double purchase_rate = rs2.getDouble("purchase_rate");
			Double selling_rate = rs2.getDouble("selling_rate");
			Double profit = rs2.getDouble("profit");
			Date expiry_date = rs2.getDate("expiry_date");
			String godown_code = rs2.getString("godown_code");
			Double tax_rate = rs2.getDouble("tax_rate");
			int from_branch_id = rs2.getInt("from_branch_id");
			int to_branch_id = rs2.getInt("to_branch_id");
			String trans_in_record = rs2.getString("trans_in_record");
			String from_branch_name = rs2.getString("from_branch_name");
			String to_branch_name = rs2.getString("to_branch_name");
			// int srl_no=rs2.getInt("srl_no");
			String party_name = rs2.getString("party_name");
			String voucher_number = rs2.getString("voucher_number");
			Date voucher_date = rs2.getDate("voucher_date");
//		String from_branch_code=rs2.getString("from_branch_code");
//		String to_branch_code=rs2.getString("to_branch_code");

			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
			itemBatchDtl.setId(String.valueOf(record_id));
			itemBatchDtl.setBarcode(barcode);
			itemBatchDtl.setItemId(String.valueOf(item_id));
			// itemBatchDtl.setMrp(mrp);
			itemBatchDtl.setVoucherDate(voucher_date);
			itemBatchDtl.setVoucherNumber(voucher_number);
			itemBatchDtl.setExpDate(expiry_date);
			itemBatchDtl.setBatch(batch_code);
			itemBatchDtl.setQtyOut(sale_qty);
			itemBatchDtl.setQtyIn(purchase_qty);
			itemBatchDtl.setCompanyMst(companyMst);
			itemBatchDtl.setStore("MAIN");

			itemBatchDtlRepository.save(itemBatchDtl);

			System.out.print("out side the item batch dtl save");

		}
		rs2.close();
		pst2.close();
		return null;
	}

	@Override
	public String transferCompanyMst(CompanyMst companyMst) throws UnknownHostException, Exception {

		String sqlstr2 = "select COMPANY_CODE,COMPANY_NAME,CST_NO,KGST_NO,TIN_NO,PHONE_NO,MOBILE_NO,PLACE,ADDRESS1,ADDRESS2,ADDRESS3,company_id from company_mst ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();
		while (rs2.next()) {

			int COMPANY_CODE = rs2.getInt("COMPANY_CODE");
			String COMPANY_NAME = rs2.getString("COMPANY_NAME");
			String CST_NO = rs2.getString("CST_NO");
			String KGST_NO = rs2.getString("KGST_NO");
			String TIN_NO = rs2.getString("TIN_NO");
			// String SUGAR_LICENCE=rs2.getString("SUGAR_LICENCE");
			String PHONE_NO = rs2.getString("PHONE_NO");
			String MOBILE_NO = rs2.getString("MOBILE_NO");
			String PLACE = rs2.getString("PLACE");
			String ADDRESS1 = rs2.getString("ADDRESS1");
			String ADDRESS2 = rs2.getString("ADDRESS2");
			String ADDRESS3 = rs2.getString("ADDRESS3");
			;
			// String ACTIVE=rs2.getString("ACTIVE");
			String company_id = rs2.getString("company_id");
			CompanyMst companyMsts = new CompanyMst();
			companyMsts.setId(String.valueOf(COMPANY_CODE));
			companyMsts.setCompanyName(COMPANY_NAME);
			companyMsts.setCompanyGst(TIN_NO);
			companyMsts.setCurrencyName("MVR");
			companyMsts.setState(PLACE);
			companyMstRepository.save(companyMsts);

		}
		rs2.close();
		pst2.close();

		return null;
	}

	@Override
	public String doDataTransferBranch(CompanyMst companyMst) throws UnknownHostException, Exception {

		String sqlstr2 = "select branch_code ,branch_name,COMPANY_CODE,id,branch_email,branch_pin,branch_address3,branch_address2,branch_address1,record_id,godown_created from branch_mst";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();
		while (rs2.next()) {

			String branch_code = rs2.getString("branch_code");
			String branch_name = rs2.getString("branch_name");
			String COMPANY_CODE = rs2.getString("COMPANY_CODE");
			int id = rs2.getInt("id");
			String branch_email = rs2.getString("branch_email");
			String branch_pin = rs2.getString("branch_pin");
			String branch_address3 = rs2.getString("branch_address3");
			String branch_address2 = rs2.getString("branch_address2");
			String branch_address1 = rs2.getString("branch_address1");
			int record_id = rs2.getInt("record_id");
			int godown_created = rs2.getInt("godown_created");
			// String branch_state=rs2.getString("branch_state");
			// String branch_country=rs2.getString("branch_country");
			// String branch_tel=rs2.getString("branch_tel");
			// String branch_gst=rs2.getString("branch_gst");

			BranchMst branchMst = new BranchMst();
			branchMst.setBranchCode(branch_code);
			branchMst.setId(String.valueOf(id));
			branchMst.setBranchName(branch_name);
//		branchMst.setBranchGst(branch_gst);

			branchMst.setBranchAddress1(branch_address1);
			branchMst.setCompanyMst(companyMst);
			branchMst.setBranchAddress2(branch_address2);
			branchMst.setBranchEmail(branch_email);
//		branchMst.setBranchTelNo(branch_tel);
			branchMstRepository.save(branchMst);

		}
		rs2.close();
		pst2.close();
		return null;
	}

	@Override
	public String doDataTransferUserMst(CompanyMst companyMst) throws UnknownHostException, Exception {

		String sqlstr2 = "select USERID,CHAR_USERID,PASSWORD,USERNAME,manager_id,task_list_id,enabled,branch_code,COMPANY_CODE,invoice_form from user_mst ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();
		while (rs2.next()) {

			String USERNAME = rs2.getString("USERNAME");
			String PASSWORD = rs2.getString("PASSWORD");
			String branch_code = rs2.getString("branch_code");
			Double USERID = rs2.getDouble("USERID");

			UserMst userMst = new UserMst();
			userMst.setCompanyMst(companyMst);
			userMst.setBranchCode(branch_code);
			userMst.setPassword(PASSWORD);
			userMst.setUserName(USERNAME);
			userMst.setId(String.valueOf(USERID));
			userMstRepository.save(userMst);

		}
		
		rs2.close();
		pst2.close();
		return null;
	}
	
	

	@Override
	public String doDataTransferUnitMst(CompanyMst companyMst, String branch) throws UnknownHostException, Exception {

		String sqlstr2 = "select * from unit_mst ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();
		while (rs2.next()) {

			int record_id = rs2.getInt("record_id");
			String unit_name = rs2.getString("unit_name");
			String unit_description = rs2.getString("unit_description");
			float unit_precision = rs2.getFloat("unit_precision");
			String machine_id = rs2.getString("machine_id");
			// int tally_posted=rs2.getInt("tally_posted");

			UnitMst unitMst = new UnitMst();
			unitMst.setId(String.valueOf(record_id));
			unitMst.setUnitName(unit_name);
			unitMst.setUnitPrecision(Double.valueOf(unit_precision));
			unitMst.setCompanyMst(companyMst);
			unitMst.setUnitDescription(unit_description);
			unitMstRepository.save(unitMst);
		}
		/*
		 * PurchaseDtl purchaseDtl=new PurchaseDtl();
		 * purchaseDtl.setId(String.valueOf(d_record_id)); purchaseDtl.setBatch(batch);
		 * purchaseDtl.setItemId(String.valueOf(d_item_id));
		 * 
		 * purchaseDtl.setItemName(item_name); purchaseDtl.setQty(Double.valueOf(qty));
		 * purchaseDtl.setMrp(previous_mrp); Optional<PurchaseHdr>
		 * purchaseHdrOpt=purchaseHdrRepository.findById(String.valueOf(record_id));
		 * if(purchaseHdrOpt.isPresent()) {
		 * purchaseDtl.setPurchaseHdr(purchaseHdrOpt.get()); }
		 * purchaseDtl.setFreeQty(isfreeqty); purchaseDtl.setMrp(mrp);
		 * purchaseDtl.setUnitId(String.valueOf(unit_id));
		 * purchaseDtl.setPreviousMRP(previous_mrp); purchaseDtl.setNetCost(net_cost);
		 * purchaseDtl.setBinNo(bin_no);
		 * purchaseDtl.setTaxRate(Double.valueOf(tax_rate));
		 * purchaseDtlRepository.save(purchaseDtl);
		 */
		rs2.close();
		pst2.close();
		return null;
	}

	@Override
	public String doPurchaseDtl(CompanyMst companyMst, String branch) throws UnknownHostException, Exception {

		String sqlstr2 = "select *from purchase_dtl";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);

		ResultSet rs2 = pst2.executeQuery();
		while (rs2.next()) {

			int d_record_id = rs2.getInt("RECORD_ID");
			int d_parent_id = rs2.getInt("parent_id");
			String item_name = rs2.getString("item_name");

			int d_item_id = rs2.getInt("item_id");
			String batch = rs2.getString("batch");

			String bin_no = rs2.getString("bin_no");
			Date expiry_date = rs2.getDate("expiry_date");
			Date manufacturing_date = rs2.getDate("manufacturing_date");

			float qty = rs2.getFloat("qty");
			float purchase_rate = rs2.getFloat("purchase_rate");
			int unit_id = rs2.getInt("unit_id");
			float tax_rate = rs2.getFloat("tax_rate");
			float sale_rate = rs2.getFloat("sale_rate");
			float whole_sales_rate = rs2.getFloat("whole_sales_rate");
			int d_updated_to_server = rs2.getInt("updated_to_server");
			String barcode = rs2.getString("barcode");
			Double fc_rate = rs2.getDouble("fc_rate");
			Double net_cost = rs2.getDouble("net_cost");
			String item_code = rs2.getString("item_code");
			Double effective_purchase_cost = rs2.getDouble("effective_purchase_cost");
			Double mrp = rs2.getDouble("mrp");
			Double previous_mrp = rs2.getDouble("previous_mrp");
			String d_machine_id = rs2.getString("machine_id");
			Double tax_amount = rs2.getDouble("tax_amount");
//	  Double exp_allocation
//	  =rs2.getDouble("exp_allocation "); 
			int isfreeqty = rs2.getInt("isfreeqty");

			PurchaseDtl purchaseDtl = new PurchaseDtl();
			purchaseDtl.setId(String.valueOf(d_record_id));
			purchaseDtl.setBatch(batch);
			purchaseDtl.setItemId(String.valueOf(d_item_id));
			purchaseDtl.setItemName(item_name);
			System.out.print(d_parent_id + "parent id issssssssssssssssssssssssssssssssssssssssssssssss");
			purchaseDtl.setQty(Double.valueOf(qty));
			purchaseDtl.setMrp(previous_mrp);
			Optional<PurchaseHdr>

			purchaseHdrOpt = purchaseHdrRepository.findById(String.valueOf(d_parent_id));
			if (purchaseHdrOpt.isPresent()) {
				purchaseDtl.setPurchaseHdr(purchaseHdrOpt.get());
			}
			purchaseDtl.setFreeQty(isfreeqty);
			purchaseDtl.setMrp(mrp);
			purchaseDtl.setUnitId(String.valueOf(unit_id));
			purchaseDtl.setPreviousMRP(previous_mrp);
			purchaseDtl.setNetCost(net_cost);
			purchaseDtl.setBinNo(bin_no);
			purchaseDtl.setTaxRate(Double.valueOf(tax_rate));
			purchaseDtlRepository.save(purchaseDtl);

		}
		rs2.close();
		pst2.close();
		return null;

	}

	@Override
	public String transferStockFromFile(CompanyMst companyMst, String fileName) throws UnknownHostException, Exception {

		try {

			FileInputStream fstream = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			ArrayList list = new ArrayList();
			while ((strLine = br.readLine()) != null) {
				list.add(strLine);
			}
			
			Iterator itr;
			for (itr = list.iterator(); itr.hasNext();) {
				
				
				
				String str = itr.next().toString();
				String[] splitSt = str.split(";");
				String itemId = "", branch = "", stockqty = "", itemname = "",store="";
				ItemMst itemMst;
				for (int i = 0; i < splitSt.length; i++) {

					try {
						itemId = splitSt[0];

						if (itemId.trim().length() == 0) {
							continue;
						}

						branch = splitSt[1];
						branch = branch.trim();
						stockqty = splitSt[2];

						itemname = splitSt[3];
						store=splitSt[4];
						itemname = itemname.trim();

						
					} catch (Exception e) {

					}
				}

				if (itemId.trim().length() == 0) {
					continue;
				}
				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
				

				itemBatchDtl.setItemId(itemId);
				Double dQty = Double.parseDouble(stockqty);

				itemBatchDtl.setQtyIn(dQty);
				itemBatchDtl.setBranchCode(branch);

				ItemMst item = itemMstRepository.findByItemName(itemname);

				if (null == item) {
					continue;
				}

				itemBatchDtl.setBarcode(item.getBarCode());

				 itemBatchDtl.setMrp(item.getStandardPrice());

				Date date1 = SystemSetting.StringToSqlDate("16/02/2021", "dd/MM/yyyy");

				Date systemDate=SystemSetting.getSystemDate();
				
				String sysDate=SystemSetting.UtilDateToString(systemDate);
				
				
				itemBatchDtl.setVoucherDate(date1);
				itemBatchDtl.setVoucherNumber("Opening");
				// itemBatchDtl.setExpDate(expiry_date);
				
				String sysDateForBatch=SystemSetting.UtilDateToString(systemDate);
				
				
				sysDateForBatch = sysDateForBatch.replaceAll("/", "-");
				
				itemBatchDtl.setBatch(MapleConstants.Nobatch);
				itemBatchDtl.setQtyOut(0.0);

				itemBatchDtl.setCompanyMst(companyMst);
				itemBatchDtl.setStore(store);

				itemBatchDtl.setItemId(item.getId());

		
				itemBatchDtl.setCompanyMst(companyMst);
				itemBatchDtlRepository.save(itemBatchDtl);

				// itemBatchDtlRepository.save(itemBatchDtl);

			}

			
			br.close();
		} catch (Exception e) {
			System.out.println("Error" + e.toString());
		}
	
		return null;
	}

	@Override
	public String doItemTransferExTax(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception {

//		RestserverApplication.RestserverApplication.localCn = getDbConn("SOURCEDB");

		//RestserverApplication.localCn ;// = RestserverApplication.getDbConn("SOURCEDB");

		String sqlstr2 = " select  record_id , item_name , unit_id , tax_rate, standard_price , barcode, item_group_id, category_id, "
				+ " item_code, cess_rate,    hsn_code , item_code from item_mst   ";

		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
		// barCode_Line1,barCode_Line2 , best_Before,
		ResultSet rs2 = pst2.executeQuery();

		while (rs2.next()) {

			String unitname = "";
			String itemName = rs2.getString("item_name");

			itemName = itemName.replaceAll("'", "");
			itemName = itemName.replaceAll("&", "");

			itemName = itemName.replaceAll("/", "");
			itemName = itemName.replaceAll("%", "");
			itemName = itemName.replaceAll("\"", "");
			// itemName = itemName.replaceAll(" ", "-");

			int itemId = rs2.getInt("record_id");
			double taxRate = rs2.getDouble("tax_rate");
			String barcode = rs2.getString("barcode");
			double standardPrice = rs2.getDouble("standard_price");

			standardPrice = standardPrice + (standardPrice * taxRate / 100);

			int item_group_id = rs2.getInt("item_group_id");
			int category_id = rs2.getInt("category_id");
			int unit_id = rs2.getInt("unit_id");
			// String barCodeLine1 = rs2.getString("barCode_Line1");
			// String barCodeLine2 = rs2.getString("barCode_Line2");
			// int bestBefore = rs2.getInt("best_Before");
			String hsn_code = rs2.getString("hsn_code");
			String item_code = rs2.getString("item_code");

			Integer rank = 0;
			String serviceOrGoods = "Goods";
			String sqlstr3 = " SELECT  group_name FROM inv_category_mst where  record_id  in (" + item_group_id + ", "
					+ category_id + ")";
			PreparedStatement pst3 = RestserverApplication.localCn.prepareStatement(sqlstr3);

			ResultSet rs3 = pst3.executeQuery();

			while (rs3.next()) {

				String groupName = rs3.getString("group_name");

				CategoryMst categoryMst = categoryMstRepository.findByCompanyMstIdAndCategoryName(companyMst.getId(),
						groupName);

				if (null == categoryMst) {
					categoryMst = new CategoryMst();

					categoryMst.setCategoryName(groupName);
					categoryMst.setCompanyMst(companyMst);
					VoucherNumber vNo = voucherNumberService.generateInvoice("CAT", branchCode);
					categoryMst.setId(vNo.getCode());

					categoryMstRepository.saveAndFlush(categoryMst);
				}

			}
			
			rs3.close();
			pst3.close();

			String sqlstr4 = " SELECT  unit_name, unit_description FROM unit_mst  where  record_id  in (" + unit_id
					+ ")";
			PreparedStatement pst4 = RestserverApplication.localCn.prepareStatement(sqlstr4);

			ResultSet rs4 = pst4.executeQuery();

			while (rs4.next()) {

				unitname = rs4.getString("unit_name");
				String unitDescription = rs4.getString("unit_description");

				UnitMst unitMst = unitMstRepository.findByUnitNameAndCompanyMstId(unitname, companyMst.getId());

				if (null == unitMst) {
					unitMst = new UnitMst();

					VoucherNumber vNo = voucherNumberService.generateInvoice("UNIT", companyMst.getId());
					unitMst.setId(vNo.getCode());
					unitMst.setUnitName(unitname);
					unitMst.setUnitDescription(unitDescription);
					unitMst.setCompanyMst(companyMst);
					unitMstRepository.saveAndFlush(unitMst);
				}

			}
			
			rs4.close();
			pst4.close();

			ItemMst itemMst = itemMstRepository.findByItemName(itemName);

			if (null == itemMst) {

				String categoryName = getCategoryNameFromId(category_id, RestserverApplication.localCn);
				CategoryMst categoryMst = categoryMstRepository.findByCompanyMstIdAndCategoryName(companyMst.getId(),
						categoryName);

				String groupName = getCategoryNameFromId(item_group_id, RestserverApplication.localCn);
				CategoryMst categoryMst2 = categoryMstRepository.findByCompanyMstIdAndCategoryName(companyMst.getId(),
						groupName);

				UnitMst unitMst = unitMstRepository.findByUnitNameAndCompanyMstId(unitname, companyMst.getId());

				int isKit = isKitItem(itemId, RestserverApplication.localCn);

				itemMst = new ItemMst();
				itemMst.setBarCode(barcode);
				// itemMst.setBarCodeLine1(barCodeLine1);
				// itemMst.setBarCodeLine2(barCodeLine2);
				// itemMst.setBestBefore(bestBefore);
				itemMst.setBranchCode(branchCode);
				itemMst.setRank(rank);
				itemMst.setServiceOrGoods(serviceOrGoods);
				if (null != categoryMst && null != categoryMst.getId()) {
					itemMst.setCategoryId(categoryMst.getId());
				}
				itemMst.setCess(0.0);
				itemMst.setCgst(taxRate / 2);
				itemMst.setCompanyMst(companyMst);
				itemMst.setHsnCode(hsn_code);

				VoucherNumber vNo = voucherNumberService.generateInvoice(branchCode, companyMst.getId());

				itemMst.setId(vNo.getCode());

				itemMst.setIsDeleted("N");

				itemMst.setIsKit(isKit);
				itemMst.setItemCode(item_code);
				if (null != categoryMst2 && null != categoryMst2.getId()) {
					itemMst.setItemGroupId(categoryMst2.getId());
				}
				itemMst.setItemName(itemName);
				itemMst.setBestBefore(0);
				
				standardPrice = SystemSetting.round(standardPrice, 2);
				
				itemMst.setStandardPrice(standardPrice);
				itemMst.setSgst(taxRate / 2);
				itemMst.setTaxRate(taxRate);

				if (null == unitMst) {
					unitMst = unitMstRepository.findByUnitNameAndCompanyMstId("NOS", companyMst.getId());

				}
				itemMst.setUnitId(unitMst.getId());

				itemMst = itemMstRepository.saveAndFlush(itemMst);

				Map<String, Object> variables = new HashMap<String, Object>();

				variables.put("companyid", companyMst);

				variables.put("voucherNumber", itemMst.getId());
				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("id", itemMst.getId());

				variables.put("REST", 1);

				// variables.put("voucherDate", purchase.getVoucherDate());
				// variables.put("id",purchase.getId());
				variables.put("inet", 0);

				variables.put("WF", "itemsave");
				//eventBus.post(variables);

				
				System.out.println("NEW ITEM "+itemName);
			}else {
				if(standardPrice - itemMst.getStandardPrice()  >0.5  ) {
					
					System.out.println("PRICE CORRECTION "+standardPrice + " --- "+  itemMst.getStandardPrice());
					
					itemMst.setStandardPrice(standardPrice);
					
					itemMst = itemMstRepository.save(itemMst);

					Map<String, Object> variables = new HashMap<String, Object>();

					variables.put("companyid", companyMst);

					variables.put("voucherNumber", itemMst.getId());
					variables.put("voucherDate", SystemSetting.getSystemDate());
					variables.put("id", itemMst.getId());

					variables.put("REST", 1);

					// variables.put("voucherDate", purchase.getVoucherDate());
					// variables.put("id",purchase.getId());
					variables.put("inet", 0);

					variables.put("WF", "itemsave");
					//eventBus.post(variables);
					
				}
			}

		}
		rs2.close();
		pst2.close();
		return "OK";

	}


	
//	@Override
//	public String menuConfigMstTransfermysql(CompanyMst companyMst, String branchCode)
//			throws UnknownHostException, Exception {
//
//		/*
//		 * MenuConfigMst
//		 */
//
//		String sqlstr2 = " select  * from menu_config_mst";
//
//		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
//
//		ResultSet rs2 = pst2.executeQuery();
//
//		while (rs2.next()) {
//
//			MenuConfigMst menuConfigMst = new MenuConfigMst();
//
//			menuConfigMst.setId(rs2.getString("id"));
//			menuConfigMst.setMenuName(rs2.getString("menu_name"));
//			menuConfigMst.setMenuFxml(rs2.getString("menu_fxml"));
//			menuConfigMst.setMenuDescription(rs2.getString("menu_description"));
//			menuConfigMst.setCustomiseSalesMode(rs2.getString("customise_sales_mode"));
//
//			menuConfigMst.setCompanyMst(companyMst);
//			// menuConfigMst.setRank(0);
//
//			menuConfigMst = menuConfigMstRepository.save(menuConfigMst);
//
//		}
//		return "OK";
//
//	}

	
	

	
	
	
//	@Override
//	public String kotCategoryMstTransfermysql(CompanyMst companyMst, String branchCode)
//			throws UnknownHostException, Exception {
//
//		/*
//		 * KotCategoryMst
//		 */
//
//		String sqlstr2 = " select  * from  kot_category_mst";
//
//		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
//
//		ResultSet rs2 = pst2.executeQuery();
//
//		while (rs2.next()) {
//
//			KotCategoryMst kotCategoryMst = new KotCategoryMst();
//
//			kotCategoryMst.setId(rs2.getString("id"));
//			kotCategoryMst.setCategoryId(rs2.getString("category_id"));
//			kotCategoryMst.setPrinterId(rs2.getString("printer_id"));
//
//			kotCategoryMst.setCompanyMst(companyMst);
//			
//
//			kotCategoryMst = kotCategoryMstRepository.save(kotCategoryMst);
//
//		}
//		return "OK";
//
//	}

//	@Override
//	public String kotCategoryMstTransfermysql(CompanyMst companyMst, String branchCode)
//			throws UnknownHostException, Exception {
//
//		/*
//		 * KotCategoryMst
//		 */
//
//		String sqlstr2 = " select  * from  kot_category_mst";
//
//		PreparedStatement pst2 = RestserverApplication.localCn.prepareStatement(sqlstr2);
//
//		ResultSet rs2 = pst2.executeQuery();
//
//		while (rs2.next()) {
//
//			KotCategoryMst kotCategoryMst = new KotCategoryMst();
//
//			kotCategoryMst.setId(rs2.getString("id"));
//			kotCategoryMst.setCategoryId(rs2.getString("category_id"));
//			kotCategoryMst.setPrinterId(rs2.getString("printer_id"));
//
//			kotCategoryMst.setCompanyMst(companyMst);
//			
//
//			kotCategoryMst = kotCategoryMstRepository.save(kotCategoryMst);
//
//		}
//		return "OK";
//
//	}




//			
//			salesOrderDtl.setCompanyMst(companyMst);
//		
//			
//			salesOrderDtl = salesOrderDtlRepository.save(salesOrderDtl);
			 





	@Override
	public String transferItemFromFileCarPalace(CompanyMst companyMst, String fileName, String branchcode)
			throws UnknownHostException, Exception {
		
		
		//COST_PATTOM,MRP_PATTOM,COST_GODOWN,MRP_GODOWN,COST_INDUSTRY,MRP_INDUSTRY
		
		 System.out.println("COST_PATTOM");
		PriceDefenitionMst priceDefenitionMst  = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyMst, "COST_PATTOM");
		if(null==priceDefenitionMst || null==priceDefenitionMst.getId()) {
			
			priceDefenitionMst = new PriceDefenitionMst();
			
			priceDefenitionMst.setCompanyMst(companyMst);
			priceDefenitionMst.setPriceLevelName("COST_PATTOM");
			priceDefenitionMst.setId("COST_PATTOM");
			priceDefenitionMst.setPriceCalculator("0");
			
			priceDefinitionMstRepository.save(priceDefenitionMst)	;
			
		}
		
	 

		 System.out.println("MRP_PATTOM");
		 priceDefenitionMst  = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyMst, "MRP_PATTOM");
		if(null==priceDefenitionMst || null==priceDefenitionMst.getId()) {
			
			priceDefenitionMst = new PriceDefenitionMst();
			
			priceDefenitionMst.setCompanyMst(companyMst);
			priceDefenitionMst.setPriceLevelName("MRP_PATTOM");
			priceDefenitionMst.setId("MRP_PATTOM");
			priceDefenitionMst.setPriceCalculator("0");
			
			priceDefinitionMstRepository.save(priceDefenitionMst)	;
			
		}
		
		 System.out.println("COST_GODOWN");
		 priceDefenitionMst  = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyMst, "COST_GODOWN");
			if(null==priceDefenitionMst || null==priceDefenitionMst.getId()) {
				
				priceDefenitionMst = new PriceDefenitionMst();
				
				priceDefenitionMst.setCompanyMst(companyMst);
				priceDefenitionMst.setPriceLevelName("COST_GODOWN");
				priceDefenitionMst.setId("COST_GODOWN");
				priceDefenitionMst.setPriceCalculator("0");
				
				priceDefinitionMstRepository.save(priceDefenitionMst)	;
				
			}
		
			 System.out.println("MRP_GODOWN");
			
			 priceDefenitionMst  = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyMst, "MRP_GODOWN");
				if(null==priceDefenitionMst || null==priceDefenitionMst.getId()) {
					
					priceDefenitionMst = new PriceDefenitionMst();
					
					priceDefenitionMst.setCompanyMst(companyMst);
					priceDefenitionMst.setPriceLevelName("MRP_GODOWN");
					priceDefenitionMst.setId("MRP_GODOWN");
					priceDefenitionMst.setPriceCalculator("0");
					
					priceDefinitionMstRepository.save(priceDefenitionMst)	;
					
				}
			
				
				 System.out.println("COST_INDUSTRY");
				priceDefenitionMst  = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyMst, "COST_INDUSTRY");
				if(null==priceDefenitionMst || null==priceDefenitionMst.getId()) {
					
					priceDefenitionMst = new PriceDefenitionMst();
					
					priceDefenitionMst.setCompanyMst(companyMst);
					priceDefenitionMst.setPriceLevelName("COST_INDUSTRY");
					priceDefenitionMst.setId("COST_INDUSTRY");
					priceDefenitionMst.setPriceCalculator("0");
					
					priceDefinitionMstRepository.save(priceDefenitionMst)	;
					
				}
			
				 System.out.println("MRP_INDUSTRY");

				priceDefenitionMst  = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyMst, "MRP_INDUSTRY");
				if(null==priceDefenitionMst || null==priceDefenitionMst.getId()) {
					
					priceDefenitionMst = new PriceDefenitionMst();
					
					priceDefenitionMst.setCompanyMst(companyMst);
					priceDefenitionMst.setPriceLevelName("MRP_INDUSTRY");
					priceDefenitionMst.setId("MRP_INDUSTRY");
					priceDefenitionMst.setPriceCalculator("0");
					
					priceDefinitionMstRepository.save(priceDefenitionMst)	;
					
				}
			
				
				
		try {

			FileInputStream fstream = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			ArrayList list = new ArrayList();
			while ((strLine = br.readLine()) != null) {
				list.add(strLine);
			}

			int rowCount = 0;
			
			Iterator itr;
			for (itr = list.iterator(); itr.hasNext();) {
				 System.out.println(rowCount);
				rowCount++;
				if(rowCount==1) {
					continue;
				}
				 System.out.println(rowCount);
				try {
					String str = itr.next().toString();
					String[] splitSt = str.split(",");
					//String itemcode = "", itemname = "", unit = "", hsn = "", barcode = "", category = "";
					
					String itemcode ="" ,
							itemname="",
							unit="",
							hsn="",
							category="",
							subcategory="";
					String barcode="",
							COST_PATTOM,MRP_PATTOM="",COST_GODOWN="",MRP_GODOWN="",COST_INDUSTRY="",MRP_INDUSTRY="";
					
					
					String cessrateStr = "";
					String taxrateStr = "";
					double cessrate = 0.0;
					double taxrate = 0.0;
					
					double dCOST_PATTOM = 0.0;
					double dMRP_PATTOM = 0.0;

					double dCOST_GODOWN = 0.0;
					double dMRP_GODOWN = 0.0;
					
					double dCOST_INDUSTRY = 0.0;
					double dMRP_INDUSTRY = 0.0;

					// for (int i = 0; i < splitSt.length; i++) {

					try {
						itemcode = splitSt[0];

						if (itemcode.trim().length() == 0) {
							continue;
						}

						itemname = splitSt[1];
						itemname = itemname.trim();

						unit = splitSt[2];
						unit = unit.trim();

						hsn = splitSt[3];
						hsn = hsn.trim();

						
						category = splitSt[4];
						category = category.trim();
						
						subcategory = splitSt[5];
						subcategory = subcategory.trim();
						
						barcode = splitSt[6];
						barcode = barcode.trim();

						
						COST_PATTOM = splitSt[7]; 
						COST_PATTOM = COST_PATTOM.trim();
						
						if(COST_PATTOM.length()==0) {
							COST_PATTOM="0";
						}
						
						
						MRP_PATTOM = splitSt[8]; 
						MRP_PATTOM = MRP_PATTOM.trim();
						
						
						if(MRP_PATTOM.length()==0) {
							MRP_PATTOM="0";
						}
						
						
						COST_GODOWN = splitSt[9]; 
						COST_GODOWN = COST_GODOWN.trim();
						
						if(COST_GODOWN.length()==0) {
							COST_GODOWN="0";
						}
						
						
						MRP_GODOWN = splitSt[10]; 
						MRP_GODOWN = MRP_GODOWN.trim();

						if(MRP_GODOWN.length()==0) {
							MRP_GODOWN="0";
						}
						
						
						COST_INDUSTRY = splitSt[11]; 
						COST_INDUSTRY = COST_INDUSTRY.trim();
						
						if(COST_INDUSTRY.length()==0) {
							COST_INDUSTRY="0";
						}
						
						MRP_INDUSTRY = splitSt[12]; 
						MRP_INDUSTRY = MRP_INDUSTRY.trim();
						if(MRP_INDUSTRY.length()==0) {
							MRP_INDUSTRY="0";
						}
						
						

						taxrateStr = "18";
						taxrateStr = taxrateStr.trim();

						cessrateStr = "1";
						cessrateStr = cessrateStr.trim();

						cessrate = Double.parseDouble(cessrateStr);
						taxrate = Double.parseDouble(taxrateStr);
						
						dCOST_PATTOM = Double.parseDouble(COST_PATTOM);
						dMRP_PATTOM = Double.parseDouble(MRP_PATTOM);
						
						dCOST_GODOWN = Double.parseDouble(COST_GODOWN);
						dMRP_GODOWN = Double.parseDouble(MRP_GODOWN);
						
					 
						dCOST_INDUSTRY = Double.parseDouble(COST_INDUSTRY);
						dMRP_INDUSTRY = Double.parseDouble(MRP_INDUSTRY);
		 
						

					} catch (Exception e) {
						logger.error(e.getMessage());
					}

					if (itemname.trim().length() == 0) {
						continue;
					}

					ItemMst item = itemMstRepository.findByItemName(itemname);

					if (null == item) {
						item = new ItemMst();

						VoucherNumber itemIdNo = voucherNumberService.generateInvoice("ITEM", companyMst.getId());

						item.setId(itemIdNo.getCode());
						item.setItemName(itemname);
						item.setBarCode(barcode);
						item.setHsnCode(hsn);
						item.setRank(0);

						/*
						 * Find Unit Id from unit name.
						 */

						UnitMst unitMst = unitMstRepository.findByUnitName(unit);

						if (null == unitMst) {
							unitMst = new UnitMst();

							VoucherNumber vNo = voucherNumberService.generateInvoice("UNIT", companyMst.getId());
							unitMst.setId(vNo.getCode());
							unitMst.setUnitName(unit);
							unitMst.setUnitDescription(unit);
							unitMst.setCompanyMst(companyMst);
							unitMst.setBranchCode(branchcode);
							unitMst = unitMstRepository.save(unitMst);
						}

						item.setUnitId(unitMst.getId());
						
						

						CategoryMst categoryMstParent = categoryMstRepository
								.findByCompanyMstIdAndCategoryName(companyMst.getId(), category);

						if (null == categoryMstParent) {
							categoryMstParent = new CategoryMst();

							VoucherNumber vNo = voucherNumberService.generateInvoice("CATEGORY", companyMst.getId());
							categoryMstParent.setId(vNo.getCode());
							categoryMstParent.setCategoryName(category);
							categoryMstParent.setParentId(null);
							categoryMstParent.setCompanyMst(companyMst);

							categoryMstParent.setBranchCode(branchcode);

							categoryMstParent = categoryMstRepository.save(categoryMstParent);
						}

						

						CategoryMst categoryMst = categoryMstRepository
								.findByCompanyMstIdAndCategoryName(companyMst.getId(), subcategory);

						if (null == categoryMst) {
							categoryMst = new CategoryMst();

							VoucherNumber vNo = voucherNumberService.generateInvoice("CATEGORY", companyMst.getId());
							categoryMst.setId(vNo.getCode());
							categoryMst.setCategoryName(subcategory);
							categoryMst.setParentId(categoryMstParent.getId());
							categoryMst.setCompanyMst(companyMst);

							categoryMst.setBranchCode(branchcode);

							categoryMst = categoryMstRepository.save(categoryMst);
						}

						item.setCategoryId(categoryMst.getId());

						item.setCess(cessrate);
						item.setTaxRate(taxrate);
						item.setIsKit(0);
						item.setReorderWaitingPeriod(0);
						item.setServiceOrGoods("GOODS");
						item.setCompanyMst(companyMst);
						item.setBestBefore(100000);
						item.setStandardPrice(dMRP_PATTOM);
						item = itemMstRepository.save(item);

						 priceDefenitionMst  = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyMst, "COST_PATTOM");
						
						 PriceDefinition priceDefinition = new PriceDefinition();
						 priceDefinition.setAmount(dCOST_PATTOM);
						 priceDefinition.setBranchCode(branchcode);
						 priceDefinition.setCompanyMst(companyMst);
						 
						 priceDefinition.setItemId(item.getId());
						 
						 priceDefinition.setStartDate(SystemSetting.UtilDateToSQLDate(SystemSetting.getSystemDate()));
						 priceDefinition.setUnitId(unitMst.getId());
						 priceDefinition.setPriceId(priceDefenitionMst.getId());
					
						 priceDefinitionRepository.save(priceDefinition);
						 
						 ////COST_PATTOM,MRP_PATTOM,COST_GODOWN,MRP_GODOWN,COST_INDUSTRY,MRP_INDUSTRY
						 
						 priceDefenitionMst  = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyMst, "MRP_PATTOM");
							
						  priceDefinition = new PriceDefinition();
						 priceDefinition.setAmount(dMRP_PATTOM);
						 priceDefinition.setBranchCode(branchcode);
						 priceDefinition.setCompanyMst(companyMst);
						 
						 priceDefinition.setItemId(item.getId());
						 
						 priceDefinition.setStartDate(SystemSetting.UtilDateToSQLDate(SystemSetting.getSystemDate()));
						 priceDefinition.setUnitId(unitMst.getId());
						 priceDefinition.setPriceId(priceDefenitionMst.getId());
					
						 priceDefinitionRepository.save(priceDefinition);
						 

						 ////COST_PATTOM,MRP_PATTOM,COST_GODOWN,MRP_GODOWN,COST_INDUSTRY,MRP_INDUSTRY
						
						 
						 priceDefenitionMst  = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyMst, "COST_GODOWN");
							
						  priceDefinition = new PriceDefinition();
						 priceDefinition.setAmount(dCOST_GODOWN);
						 priceDefinition.setBranchCode(branchcode);
						 priceDefinition.setCompanyMst(companyMst);
						 
						 priceDefinition.setItemId(item.getId());
						 
						 priceDefinition.setStartDate(SystemSetting.UtilDateToSQLDate(SystemSetting.getSystemDate()));
						 priceDefinition.setUnitId(unitMst.getId());
						 priceDefinition.setPriceId(priceDefenitionMst.getId());
					
						 priceDefinitionRepository.save(priceDefinition);
						 
						 priceDefenitionMst  = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyMst, "MRP_GODOWN");
							
						  priceDefinition = new PriceDefinition();
						 priceDefinition.setAmount(dMRP_GODOWN);
						 priceDefinition.setBranchCode(branchcode);
						 priceDefinition.setCompanyMst(companyMst);
						 
						 priceDefinition.setItemId(item.getId());
						 
						 priceDefinition.setStartDate(SystemSetting.UtilDateToSQLDate(SystemSetting.getSystemDate()));
						 priceDefinition.setUnitId(unitMst.getId());
						 priceDefinition.setPriceId(priceDefenitionMst.getId());
					
						 priceDefinitionRepository.save(priceDefinition);
						 
						 priceDefenitionMst  = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyMst, "COST_INDUSTRY");
							
						  priceDefinition = new PriceDefinition();
						 priceDefinition.setAmount(dCOST_INDUSTRY);
						 priceDefinition.setBranchCode(branchcode);
						 priceDefinition.setCompanyMst(companyMst);
						 
						 priceDefinition.setItemId(item.getId());
						 
						 priceDefinition.setStartDate(SystemSetting.UtilDateToSQLDate(SystemSetting.getSystemDate()));
						 priceDefinition.setUnitId(unitMst.getId());
						 priceDefinition.setPriceId(priceDefenitionMst.getId());
					
						 priceDefinitionRepository.save(priceDefinition);
						 
						 priceDefenitionMst  = priceDefinitionMstRepository.findByCompanyMstAndPriceLevelName(companyMst, "MRP_INDUSTRY");
							
						  priceDefinition = new PriceDefinition();
						 priceDefinition.setAmount(dMRP_INDUSTRY);
						 priceDefinition.setBranchCode(branchcode);
						 priceDefinition.setCompanyMst(companyMst);
						 
						 priceDefinition.setItemId(item.getId());
						 
						 priceDefinition.setStartDate(SystemSetting.UtilDateToSQLDate(SystemSetting.getSystemDate()));
						 priceDefinition.setUnitId(unitMst.getId());
						 priceDefinition.setPriceId(priceDefenitionMst.getId());
					
						 priceDefinitionRepository.save(priceDefinition);
						 
					}

				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			}
			
			br.close();

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
		
		
		
		
		 
	}

	public String transferItemFromFile(CompanyMst companyMst, String fileName, String branchcode)
			throws UnknownHostException, Exception {

		try {

			FileInputStream fstream = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			ArrayList list = new ArrayList();
			while ((strLine = br.readLine()) != null) {
				list.add(strLine);
			}

			Iterator itr;
			for (itr = list.iterator(); itr.hasNext();) {

				try {
					String str = itr.next().toString();
					String[] splitSt = str.split(",");
					String itemcode = "", itemname = "", unit = "", hsn = "", barcode = "", category = "";
					String cessrateStr = "";
					String taxrateStr = "";
					double cessrate = 0.0;
					double taxrate = 0.0;

					// for (int i = 0; i < splitSt.length; i++) {

					try {
						itemcode = splitSt[0];

						if (itemcode.trim().length() == 0) {
							continue;
						}

						itemname = splitSt[1];
						itemname = itemname.trim();

						unit = splitSt[2];
						unit = unit.trim();

						hsn = splitSt[3];
						hsn = hsn.trim();

						barcode = splitSt[4];
						barcode = barcode.trim();

						category = splitSt[5];
						category = category.trim();

						taxrateStr = splitSt[6];
						taxrateStr = taxrateStr.trim();

						cessrateStr = splitSt[7];
						cessrateStr = cessrateStr.trim();

						cessrate = Double.parseDouble(cessrateStr);
						taxrate = Double.parseDouble(taxrateStr);

					} catch (Exception e) {

					}

					if (itemname.trim().length() == 0) {
						continue;
					}

					ItemMst item = itemMstRepository.findByItemName(itemname);

					if (null == item) {
						item = new ItemMst();

						VoucherNumber itemIdNo = voucherNumberService.generateInvoice(branchcode, companyMst.getId());

						item.setId(itemIdNo.getCode());
						item.setItemName(itemname);
						item.setBarCode(barcode);
						item.setHsnCode(hsn);
						item.setRank(0);

						/*
						 * Find Unit Id from unit name.
						 */

						UnitMst unitMst = unitMstRepository.findByUnitName(unit);

						if (null == unitMst) {
							unitMst = new UnitMst();

							VoucherNumber vNo = voucherNumberService.generateInvoice("UNIT", companyMst.getId());
							unitMst.setId(vNo.getCode());
							unitMst.setUnitName(unit);
							unitMst.setUnitDescription(unit);
							unitMst.setCompanyMst(companyMst);
							unitMst.setBranchCode(branchcode);
							unitMst = unitMstRepository.save(unitMst);
						}

						item.setUnitId(unitMst.getId());

						CategoryMst categoryMst = categoryMstRepository
								.findByCompanyMstIdAndCategoryName(companyMst.getId(), category);

						if (null == categoryMst) {
							categoryMst = new CategoryMst();

							VoucherNumber vNo = voucherNumberService.generateInvoice("CATEGORY", companyMst.getId());
							categoryMst.setId(vNo.getCode());
							categoryMst.setCategoryName(category);
							categoryMst.setParentId(null);
							categoryMst.setCompanyMst(companyMst);

							categoryMst.setBranchCode(branchcode);

							categoryMst = categoryMstRepository.save(categoryMst);
						}

						item.setCategoryId(categoryMst.getId());

						item.setCess(cessrate);
						item.setTaxRate(taxrate);
						item.setIsKit(0);
						item.setReorderWaitingPeriod(0);
						item.setServiceOrGoods("GOODS");
						item.setCompanyMst(companyMst);
						item.setBestBefore(100000);
						item = itemMstRepository.save(item);

					}

				} catch (Exception e) {
				
					System.out.println(e.toString());
					continue;
				}
			}
			
			br.close();

		} catch (Exception e) {
			System.out.println("Error" + e.toString());
		}

		return null;
	}

	@Override
	public String transferMemberMst(CompanyMst companyMst) throws UnknownHostException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String transferOrganisation(CompanyMst companyMst) throws UnknownHostException, Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String transferFamilyMst(CompanyMst companyMst) throws UnknownHostException, Exception {
		// TODO Auto-generated method stub
		return null;
	}


}