package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionDtlDtl;
import com.maple.restserver.entity.ProductionPreplanningMst;
import com.maple.restserver.report.entity.ProductionPreplaninngDtlReport;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.ProductionDtlDtlRepository;
import com.maple.restserver.repository.ProductionPreplanningDtlRepository;
import com.maple.restserver.repository.ProductionPreplanningRepository;


@Service
@Transactional
public class ProductionPrePlaningServiceImpl implements ProductionPrePlaningService{


	@Autowired
	ProductionDtlDtlRepository productionDtlDtlRepository;
	@Autowired
	ProductionPreplanningRepository productionPreplanningRepository;
	
	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepository;
	@Autowired
	KitDefenitionDtlRepository kitDefenitionDtlRepository;
	
	@Autowired
	ProductionPreplanningDtlRepository productionPreplanningDtlRepository;
	
	@Autowired
	ItemMstRepository itemMstRepository;


	@Override
	public ProductionPreplanningMst getProductionPreplanningMst(Date sdate, CompanyMst companyMst, String branchCode) {

		
		return productionPreplanningDtlRepository.getProductionPreplanningMst( sdate,  companyMst,  branchCode);
		

	}


	@Override
	public List<ProductionPreplaninngDtlReport> getProductionPreplanningDtl(java.util.Date sdate, CompanyMst  companyMst ,String branchCode) {
		
		
		List<ProductionPreplaninngDtlReport> productionPrePlaningDtlReportList=new ArrayList<ProductionPreplaninngDtlReport>();
	
		List<Object> obj=productionPreplanningDtlRepository.getProductionPreplanningDtl( companyMst , branchCode);
	
		System.out.print(obj.size()+"object size issssssssssssssssssssssssssssssssssssss");
		for(int i=0;i<obj.size();i++) {
			Object[] objAray = (Object[]) obj.get(i);
			ProductionPreplaninngDtlReport productionPreplanningDtl=new ProductionPreplaninngDtlReport();
			productionPreplanningDtl.setItemName((String) objAray[0]);
			productionPreplanningDtl.setQty((Double) objAray[1]);
			productionPreplanningDtl.setBatch((String) objAray[2]);
			productionPreplanningDtl.setId((String) objAray[3]);
			productionPreplanningDtl.setStatus((String) objAray[4]);
			productionPreplanningDtl.setItemId((String) objAray[5]);
			productionPrePlaningDtlReportList.add(productionPreplanningDtl);
		}
		return productionPrePlaningDtlReportList;
	}


	@Override
	public void saveProductionPlaningDtlDtl(ProductionDtl productionDtl) {
		
		
	List<Object> obj=kitDefinitionMstRepository.fetchKitDtls(productionDtl.getItemId());
	Double minimumQty=0.0;
    String itemId="";
    String unitId="";
    Double rawMatirailQty=0.0;
	for(int i=0; i<obj.size();i++) {
		
		Object[] objctAray = (Object[]) obj.get(i);
       minimumQty=((Double) objctAray[0]);
       itemId=((String) objctAray[1]);
       unitId=((String) objctAray[2]);
       rawMatirailQty=((Double) objctAray[3]);
       Double prodRatio=productionDtl.getQty()/minimumQty;
       
	ProductionDtlDtl productionDtlDtl =new ProductionDtlDtl();
	productionDtlDtl.setQty(rawMatirailQty*prodRatio);
	productionDtlDtl.setProductionDtl(productionDtl);
	productionDtlDtl.setUnitId(unitId);
	Optional<ItemMst> itemMstOpt=itemMstRepository.findById(itemId);
	productionDtlDtl.setRawMaterialItemId(itemId);
	productionDtlDtl.setItemName(itemMstOpt.get().getItemName());
	productionDtlDtlRepository.save(productionDtlDtl);
	}

	}
	
	
	
}
