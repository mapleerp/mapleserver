package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.MachineResourceMst;
import com.maple.restserver.entity.MixCatItemLinkMst;
import com.maple.restserver.entity.MixCategoryMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.KitValidationReport;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
 
import com.maple.restserver.repository.MachineResourceMstRepository;
import com.maple.restserver.repository.ResourceCatItemLinkRepository;
import com.maple.restserver.repository.SubCategoryMstRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
 
@Service
@Transactional
@Component
public class KitDefinitionImpl implements kitdefinionservice {
	
	@Autowired
	KitDefenitionDtlRepository kitDefenitionDtlRepo;
	@Autowired
	ItemBatchMstRepository itemBatchMstRepo;
	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepository;
	@Autowired
	VoucherNumberService voucherService;
	@Autowired
	 KitDefinitionMstRepository kitdefinitionMstRepo;
	EventBus eventBus = EventBusFactory.getEventBus();
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepo;
	@Autowired
	MultiUnitConversionServiceImpl multiUnitConversionServiceImpl;
	
 
	@Autowired
	SubCategoryMstRepository mixCategoryMstRepository;
	@Autowired
	ResourceCatItemLinkRepository resourceCatItemLinkRepository;
	
	@Autowired
	UnitMstRepository unitMstRepository;
	@Autowired
	ItemMstRepository  itemMstRepository;

@Autowired
MachineResourceMstRepository machineResourceMstRepository;
	
	@Override
	public List<KitDefinitionMst> getKitDefinitionMst(String pdfFileName) {
		 
		 	 		
		 List<KitDefinitionMst> kitDefinitionMstList =   new ArrayList();
		 
		 
	 List<KitDefinitionMst> kitDefinitionMst =   kitDefinitionMstRepository.findAll();
	// kitdefinitionMstList.add(kitDefinitionMst);
//		
//			 for(KitDefinitionMst i :kitDefinitionMst )
//		 {
//			
//				 KitDefinitionMst kitDefinitionMstReport= new KitDefinitionMst();
//				 kitDefinitionMstReport.setBarcode(i.getBarcode());
//				 kitDefinitionMstReport.setItemCode(i.getItemCode());
//				 kitDefinitionMstReport.setItemId(i.getItemId());
//				 kitDefinitionMstReport.setKitName(i.getKitName());
//				 kitDefinitionMstReport.setMinimumQty(i.getMinimumQty());
//				 kitDefinitionMstReport.setProductionCost(i.getProductionCost());
//				 kitDefinitionMstReport.setTaxRate(i.getTaxRate());
//				 kitDefinitionMstReport.setUnitId(i.getUnitId());
//				 kitDefinitionMstReport.setUnitName(i.getUnitName());
//				
//				 kitdefinitionMstList.add(kitDefinitionMstReport);
//		 }
		 		 
		 /*
		  * Create SalesInoiceEntity
		  *  Entity and pass value from object to Entity
		  */
		 
		return kitDefinitionMst;
	}


	@Override
	public List<KitDefenitionDtl> findKitDefMstId(String kitdefinitionMstId) {
		 List<KitDefenitionDtl> kitDefinitionMstList =   new ArrayList();
		 
		
		 List<Object> kitList = kitdefinitionMstRepo.findBykitDefenitionmst( kitdefinitionMstId);
		 for(int i=0;i<kitList.size();i++)
		 {
			 Object[] objAray = (Object[]) kitList.get(i);
			 KitDefenitionDtl kitDefenitionDtl = new KitDefenitionDtl();
			 kitDefenitionDtl.setId((String)objAray[0]);
			 kitDefenitionDtl.setBarcode((String)objAray[1]);
			 kitDefenitionDtl.setItemId((String)objAray[2]);
			 kitDefenitionDtl.setItemName((String)objAray[3]);
			 kitDefenitionDtl.setQty((Double)objAray[4]);
			 kitDefenitionDtl.setUnitId((String)objAray[5]);
			 kitDefenitionDtl.setUnitName((String)objAray[6]);
			
			 kitDefinitionMstList.add(kitDefenitionDtl);
		 }
		 return kitDefinitionMstList;
	}


	@Override
	public KitValidationReport findRawMaterialForSingleItemInMachineUnit(String companyMstId,String itemId)throws  ResourceNotFoundException {

		System.out.print(itemId+"item id isssssssssssssssssssssssssssssssssssssssss");
		KitValidationReport kitValidationReport=new KitValidationReport();
		MixCatItemLinkMst mixCatItemLinkMst=resourceCatItemLinkRepository.fetchByItemId(itemId);
		Optional<ItemMst> itemMstOpt=itemMstRepository.findById(itemId);
		try {
		if(null==mixCatItemLinkMst) {
			throw new ResourceNotFoundException(itemMstOpt.get().getItemName()+" not present in Mix Category ");
		}
		System.out.print(mixCatItemLinkMst.getResourceCatId()+"mix category item link mst isssssssssssssssssssssssssssssssssssssssss");
		Optional<MixCategoryMst>  mixCategoryMstOpt=mixCategoryMstRepository.findById(mixCatItemLinkMst.getResourceCatId());
		Optional<MachineResourceMst> machineResourceMstOpt=machineResourceMstRepository.findById(mixCategoryMstOpt.get().getMechineId());
	   
		if(null==machineResourceMstOpt.get()) {
			throw new ResourceNotFoundException("Plz Configure Machine");
		}
		
		String machineUnitId=machineResourceMstOpt.get().getUnitId();

	    Optional<UnitMst> unitMstOpt=unitMstRepository.findById(machineResourceMstOpt.get().getUnitId());
		kitValidationReport.setMachineUnit(unitMstOpt.get().getUnitName());
	    System.out.print(machineUnitId+"machine unit id issssssssssssssssssssssssssssssssssssssssssssssss");
	
	    
	    
	    
			Double recipeQtyInMachUnit = 0.0;
			Double recipeSingleQtyInMachUnit = 0.0;
			Double minumQty = 0.0;
			Double rawMaterialForAprod = 0.0;
			List<Object> obj = kitDefinitionMstRepository.fetchKitDtls(itemId);

			if(obj.size()<=0) {
				throw new ResourceNotFoundException("Plz Configure Kit");
			}
			System.out.print(obj.size()+"object list size isssssssssssssssssssssssssss");
			for (int i = 0; i < obj.size(); i++) {

				Object[] objAray = (Object[]) obj.get(i);

				minumQty = (Double) objAray[0];
				System.out.print(minumQty + "minimum qty isssssssssssssssssssssssssssssssss");

				// String productUnitId=(String)objAray[1];

				String rawItemId = (String) objAray[1];
				String rawMateUnitId = (String) objAray[2];

				Double rawmatQty = (Double) objAray[3];

				System.out.print(rawmatQty+"raw material qty isssssssssssssssssssssssssssssssssssss");
				
				double rawMateToMachine = multiUnitConversionServiceImpl.getConvertionQty(companyMstId, rawItemId,
						rawMateUnitId, machineUnitId, rawmatQty);

				rawMaterialForAprod = rawMateToMachine + rawMaterialForAprod;
				System.out.print(rawMaterialForAprod+" material for production isssssssssssss%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");

			}
			System.out.print(rawMaterialForAprod+"raw material for production isssssssssssss%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
			recipeSingleQtyInMachUnit = rawMaterialForAprod / minumQty;

			recipeQtyInMachUnit = recipeSingleQtyInMachUnit ;
			
			System.out.print(recipeQtyInMachUnit+"receipe qty isssssssssssssssssssss");

		if(recipeQtyInMachUnit>=0) {
			kitValidationReport.setQtyInMachineUnit(recipeQtyInMachUnit);
		}
		}catch (Exception e) {
			kitValidationReport.setErrorMessage(e.getMessage());
		}
	return kitValidationReport;
}
	public void runTimeProduction(String itemId,String batch,String barcode,CompanyMst companymst,
			String dtlid,String hdrid,String voucherNumber,Date voucherDate,String branchCode, Double diffQty) {
		KitDefinitionMst kitDefinitionMst = null;
		List<KitDefinitionMst> kitDefinitionMstList = kitdefinitionMstRepo.findByItemId(itemId);
		if (kitDefinitionMstList.size() > 0) {
			kitDefinitionMst = kitDefinitionMstList.get(0);
		}
		Optional<ItemMst> itemopt = itemMstRepository.findById(itemId);
		{
			if (null != kitDefinitionMst) {
				List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo.findByItemIdAndBatchAndBarcode(
						itemId, batch,barcode);

				if (itembatchmst.size() == 1) {
					ItemBatchMst itemBatchMst = itembatchmst.get(0);
					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
					itemBatchMst.setQty(itemBatchMst.getQty() + diffQty);
					itemBatchMstRepo.saveAndFlush(itemBatchMst);

				} else {
					ItemBatchMst itemBatchMst = new ItemBatchMst();
					itemBatchMst.setBarcode(itemopt.get().getBarCode());
					if (batch != null) {
						itemBatchMst.setBatch(batch);
					} else {
						itemBatchMst.setBatch(MapleConstants.Nobatch);
					}

					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
					itemBatchMst.setQty(diffQty);
					itemBatchMst.setItemId(itemId);
					itemBatchMst.setBranchCode(branchCode);
					itemBatchMst.setCompanyMst(companymst);
					itemBatchMstRepo.save(itemBatchMst);

				}

				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
				final VoucherNumber voucherNo = voucherService.generateInvoice("STV",
						companymst.getId());
				itemBatchDtl.setBarcode(itemopt.get().getBarCode());
				if (batch != null) {
					itemBatchDtl.setBatch(batch);
				} else {
					itemBatchDtl.setBatch(MapleConstants.Nobatch);
				}
				itemBatchDtl.setItemId(itemId);
				itemBatchDtl.setMrp(itemopt.get().getStandardPrice());
				itemBatchDtl.setQtyIn(diffQty);
				itemBatchDtl.setQtyOut(0.0);
				itemBatchDtl.setStore(MapleConstants.Store);
				itemBatchDtl.setBranchCode(branchCode);
				itemBatchDtl.setVoucherNumber(voucherNo.getCode());
				itemBatchDtl.setVoucherDate(voucherDate);
				itemBatchDtl.setItemId(itemId);
				itemBatchDtl.setSourceVoucherNumber(voucherNumber);
				itemBatchDtl.setParticulars("SALES PRODUCTION");
				itemBatchDtl.setStore(MapleConstants.Store);
				itemBatchDtl.setSourceParentId(hdrid);
				itemBatchDtl.setSourceDtlId(dtlid);
				itemBatchDtl.setCompanyMst(companymst);
				itemBatchDtl = itemBatchDtlRepo.saveAndFlush(itemBatchDtl);
				Map<String, Object> variables = new HashMap<String, Object>();

				variables.put("voucherNumber", itemBatchDtl.getId());
				variables.put("id", itemBatchDtl.getId());
				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("inet", 0);
				variables.put("REST", 1);
				variables.put("companyid", itemBatchDtl.getCompanyMst());
				variables.put("WF", "forwardItemBatchDtl");

				 

				eventBus.post(variables);

			}

		}
	}
	public void executebatchwiserowmaterialupdate(String voucherNumber, Date voucherDate, CompanyMst companyMst,
			String itemName,String itemId,String batch,Double qty,String branchCode,String parentId,
			String dtlId) {

//		Optional<ActualProductionHdr> actualProductionHdrOpt = actualProductionHdrRepository.findById(actualProductionHdrId);
//		
//		
//		List<ActualProductionDtl> actualProductionDtlList = actualProductionDtlRepository.findByActualProductionHdrId(actualProductionHdrOpt.get().getId());
//		
//		Iterator iter = actualProductionDtlList.iterator();
//		while (iter.hasNext()) {
//			ActualProductionDtl actualProductionDtl = (ActualProductionDtl) iter.next();
//			
//			Double qty = actualProductionDtl.getActualQty() ;
//			String itemId = actualProductionDtl.getItemId();
//			
//			/*
//			 * Find Kit Header
//			 */

		KitDefinitionMst kitDefinitionMst = null;
		List<KitDefinitionMst> kitDefinitionMstList = kitdefinitionMstRepo.findByItemId(itemId);
		if (kitDefinitionMstList.size() > 0) {
			kitDefinitionMst = kitDefinitionMstList.get(0);
		}
		Double defenitionQty = kitDefinitionMst.getMinimumQty();

		/*
		 * Find Kit Dtl - Raw Materil
		 */

		List<KitDefenitionDtl> kitDefenitionDtlList = kitDefenitionDtlRepo
				.findBykitDefenitionmstId(kitDefinitionMst.getId());

		Iterator iterRM = kitDefenitionDtlList.iterator();

		while (iterRM.hasNext()) {

			KitDefenitionDtl kitDefenitionDtl = (KitDefenitionDtl) iterRM.next();

			String rawMaterialId = kitDefenitionDtl.getItemId();
			Double rawMaterialUnitQty = kitDefenitionDtl.getQty();
			if (null != rawMaterialId) {
				Double requiredRawMaterial = rawMaterialUnitQty * qty / defenitionQty;

				System.out.println(rawMaterialId);

				Optional<ItemMst> itemopt = itemMstRepository.findById(rawMaterialId);

				if (!itemopt.isPresent()) {
					System.out.println(rawMaterialId + " Not Present");
					continue;
				}

				ItemMst rmItem = itemopt.get();

				List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
						.findByItemIdAndBatchAndBarcode(rawMaterialId, MapleConstants.Nobatch, rmItem.getBarCode());

				if (itembatchmst.size() == 1) {
					ItemBatchMst itemBatchMst = itembatchmst.get(0);
					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
					itemBatchMst.setQty(itemBatchMst.getQty() - requiredRawMaterial);
					itemBatchMst.setBranchCode(branchCode);
					itemBatchMstRepo.saveAndFlush(itemBatchMst);
				} else {
					ItemBatchMst itemBatchMst = new ItemBatchMst();
					itemBatchMst.setBarcode(itemopt.get().getBarCode());
					itemBatchMst.setBatch(MapleConstants.Nobatch);
					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
					itemBatchMst.setQty(-1 * requiredRawMaterial);
					itemBatchMst.setItemId(rawMaterialId);
					itemBatchMst.setBranchCode(branchCode);
					itemBatchMst.setCompanyMst(companyMst);
					itemBatchMstRepo.saveAndFlush(itemBatchMst);

				}

				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
				final VoucherNumber voucherNo = voucherService.generateInvoice("STV", companyMst.getId());
				itemBatchDtl.setBarcode(itemopt.get().getBarCode());
				itemBatchDtl.setBatch(MapleConstants.Nobatch);
				itemBatchDtl.setItemId(rawMaterialId);
				itemBatchDtl.setMrp(itemopt.get().getStandardPrice());
				itemBatchDtl.setQtyIn(0.0);
				itemBatchDtl.setParticulars("SALES KOT-" + itemName);
				itemBatchDtl.setStore(MapleConstants.Store);
				itemBatchDtl.setQtyOut(requiredRawMaterial);
				itemBatchDtl.setVoucherNumber(voucherNo.getCode());
				itemBatchDtl.setVoucherDate(voucherDate);
				itemBatchDtl.setItemId(rawMaterialId);
				itemBatchDtl.setSourceVoucherNumber(voucherNumber);
				itemBatchDtl.setBranchCode(branchCode);
				itemBatchDtl.setCompanyMst(companyMst);
				itemBatchDtl.setSourceParentId(parentId);
				itemBatchDtl.setSourceDtlId(dtlId);
				itemBatchDtl = itemBatchDtlRepo.saveAndFlush(itemBatchDtl);
				Map<String, Object> variables = new HashMap<String, Object>();

				variables.put("voucherNumber", itemBatchDtl.getId());
				variables.put("id", itemBatchDtl.getId());
				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("inet", 0);
				variables.put("REST", 1);
				variables.put("companyid", itemBatchDtl.getCompanyMst());
				variables.put("WF", "forwardItemBatchDtl");

				 

				eventBus.post(variables);

			}

		}
	}
	}


	


