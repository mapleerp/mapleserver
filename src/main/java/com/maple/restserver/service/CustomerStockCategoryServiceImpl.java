package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CustomerStockCategory;
import com.maple.restserver.report.entity.CustomerCategorywiseStockReport;
import com.maple.restserver.report.entity.CustomerSotckCategoryReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CustomerStockCategoryRepository;

@Service
@Transactional
@Component
public class CustomerStockCategoryServiceImpl implements CustomerStockCategoryService {
	@Autowired
	CustomerStockCategoryRepository customerStockCategoryRepository;
	
	
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
//	CustomerMstRepository customerMstRepository;
	
	@Override
	public List<CustomerSotckCategoryReport> fetchCustomerNameAndCategoryName(String companyMstId) {
		List<CustomerSotckCategoryReport> repoList=new ArrayList<CustomerSotckCategoryReport>();
		
		Optional<CompanyMst> companyMst= companyMstRepository.findById(companyMstId);
		List<CustomerStockCategory> customerStockCategory=customerStockCategoryRepository.findByCompanyMst(companyMst.get());
		
		for(CustomerStockCategory reportDetail :customerStockCategory)
		
		 {
			
		
		CustomerSotckCategoryReport report= new CustomerSotckCategoryReport();
		
		report.setCategoryName(reportDetail.getCategoryMst().getCategoryName());
		report.setCustomerName(reportDetail.getAccountHeads().getAccountName());
		report.setStatus(reportDetail.getStatus());
		repoList.add(report);
		 }
		return repoList;
	}


	@Override
	public List<CustomerCategorywiseStockReport> fetchCustomerCategorywiseStockReport(String companyMstId,String customerphoneno) {
		
		
		List<CustomerCategorywiseStockReport> reportList=new ArrayList<>();
		AccountHeads customerMst=accountHeadsRepository.findByCustomerContact(customerphoneno);
		Optional<CompanyMst> companyMst= companyMstRepository.findById(companyMstId);
		List<CustomerStockCategory> customerStockCategory=customerStockCategoryRepository.findByAccountHeadsCustomerContact(customerphoneno);
		System.out.print(customerStockCategory.size()+"customer category table list size issssssssssss");
		for(CustomerStockCategory reportDetail :customerStockCategory)
		 {
				
			List<Object> obj=customerStockCategoryRepository.fetchCustomerCategorywiseStockReport(reportDetail.getCategoryMst().getId(),companyMst.get());
			System.out.print(obj.size()+"report list size is ");
			
			for(int i=0;i<obj.size();i++)
			 {
				CustomerCategorywiseStockReport reportDtl=new CustomerCategorywiseStockReport();
				
			Object[] objAray = (Object[]) obj.get(i);
			reportDtl.setItemName((String) objAray[0]);
			reportDtl.setCategoryName((String) objAray[1]);
			reportDtl.setQty((Double) objAray[2]);
			reportDtl.setStandardPrice((Double) objAray[3]);
			reportList.add(reportDtl);
			}
		 }
		return reportList;
	}


	@Override
	public List<CustomerCategorywiseStockReport> fetchCustomerCategorywiseStockReportByItem(String companymstid,
			String customerphoneno, String itemid) {
		
		List<CustomerCategorywiseStockReport> reportList=new ArrayList<>();
		AccountHeads customerMst=accountHeadsRepository.findByCustomerContact(customerphoneno);
		Optional<CompanyMst> companyMst= companyMstRepository.findById(companymstid);
		List<CustomerStockCategory> customerStockCategory=customerStockCategoryRepository.findByAccountHeadsCustomerContact(customerphoneno);
		System.out.print(customerStockCategory.size()+"customer category table list size issssssssssss");
		for(CustomerStockCategory reportDetail :customerStockCategory)
		 {
				
			List<Object> obj=customerStockCategoryRepository.findByCustomerMstCustomerContactByItem(reportDetail.getCategoryMst().getId(),companyMst.get().getId(),itemid);
			System.out.print(obj.size()+"report list size is ");
			
			for(int i=0;i<obj.size();i++)
			 {
				CustomerCategorywiseStockReport reportDtl=new CustomerCategorywiseStockReport();
				
			Object[] objAray = (Object[]) obj.get(i);
			reportDtl.setItemName((String) objAray[0]);
			reportDtl.setCategoryName((String) objAray[1]);
			reportDtl.setQty((Double) objAray[2]);
			reportDtl.setStandardPrice((Double) objAray[3]);
			reportList.add(reportDtl);
			}
		 }
		return reportList;
	}

	
}
