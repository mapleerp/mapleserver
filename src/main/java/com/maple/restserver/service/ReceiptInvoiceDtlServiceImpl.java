package com.maple.restserver.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.ReceiptInvoiceDtl;
import com.maple.restserver.repository.ReceiptInvoiceDtlRepository;


@Service
@Transactional
@Component


public class ReceiptInvoiceDtlServiceImpl implements ReceiptInvoiceDtlService {
	@Autowired
	ReceiptInvoiceDtlRepository receiptInvoiceDtlRepository;
	@Override
	public  List<ReceiptInvoiceDtl> findByCustomerMstIdAndOwnAccount(String customerMstId) {
		
		return receiptInvoiceDtlRepository.findByCustomerMstIdAndOwnAccount(customerMstId) ;
	}

}
