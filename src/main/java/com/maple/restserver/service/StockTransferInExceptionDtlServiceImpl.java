package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.StockSummaryCategoryWiseReport;
import com.maple.restserver.report.entity.StockTransferInExceptionReportDtl;
import com.maple.restserver.repository.StockTransferInExceptionDtlRepository;

@Service
public class StockTransferInExceptionDtlServiceImpl implements StockTransferInExceptionDtlService{

	
	@Autowired
	StockTransferInExceptionDtlRepository stockTransferInExceptionDtlRepository;

	@Override
	public List<StockTransferInExceptionReportDtl> stockTransferInExceptionReort(String voucherNumber,
			Date voucherDate) {
		List<StockTransferInExceptionReportDtl> stockTransferInExceptionReportDtls=new ArrayList();
		
		List<Object> obj=stockTransferInExceptionDtlRepository.stockTransferInExceptionReport
				(voucherNumber, voucherDate);
		for (int i = 0; i < obj.size(); i++) {
			
			Object[] objAray = (Object[]) obj.get(i);
			StockTransferInExceptionReportDtl sReport = new StockTransferInExceptionReportDtl();
			sReport.setItemName((String) objAray[0]);
			sReport.setBatch((String) objAray[1]);
			sReport.setQty(String.valueOf((Double) objAray[2]));
			sReport.setBarcode((String) objAray[3]);
			sReport.setRate(String.valueOf((Double) objAray[4]));
			sReport.setItemCode((String) objAray[5]);
			sReport.setReason((String) objAray[6]);
			sReport.setUnit((String) objAray[7]);
			sReport.setAmount(String.valueOf((Double) objAray[8]));
			stockTransferInExceptionReportDtls.add(sReport);

		}

	
		return stockTransferInExceptionReportDtls;
	}
	
	
	
	
	
}
