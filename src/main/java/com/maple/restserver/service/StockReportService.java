package com.maple.restserver.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.ItemStockEnquiryReport;
import com.maple.restserver.report.entity.StockReport;

@Component
public interface StockReportService {

	
	List<StockReport> getStockByBranchCodeAndDateAndCompanyMstId(String branchcode,String companymstid, Date fromdate, String categoryid);
	List<StockReport>  getDailyStockReport(String branchcode,String companymstid, Date fromdate);
	List<StockReport>  getItemWiseStockReport(String branchcode,String  companymstid ,java.util.Date date,String itemid);
	List<ItemStockEnquiryReport> getItemStockReport(String companymstid, String branchcode, String itemid, String vouchertype);
	List<StockReport> getDailyMobileStockReport(String branchcode, String companymstid, Date date);
	List<StockReport> getDailyMobileStockReportByCategory(String branchcode, String companymstid, Date date,
			String[] array);
	List<StockReport> getDailyMobileStockReportByItemName(String branchcode, String companymstid, Date date,
			String itemname);
	List<StockReport> getStoreWiseStockReport(String branchcode, String companymstid, String store);
	
	
	
	List<StockReport> getPriceTypeWiseStockSummaryReport(java.util.Date fdate, java.util.Date tdate, String pricetype,
			String branchcode);
}
