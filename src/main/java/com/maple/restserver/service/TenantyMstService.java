package com.maple.restserver.service;

import com.maple.restserver.entity.CompanyMst;

public interface TenantyMstService {

	boolean checkTenantyMst(CompanyMst companyMst);

}
