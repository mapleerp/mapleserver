package com.maple.restserver.service;


import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public interface UserService {
	ArrayList<String> getUserRoles(String username,String comapnymstid);
}
