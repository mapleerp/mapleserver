package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.ProfitAndLossReport;
import com.maple.restserver.report.entity.StockSummaryDtlReport;

@Service
public interface ProfitAndLossService {

	List<ProfitAndLossReport> getBranchwiseTotalProfit(String companymstid, Date profitDate);

	//List<ProfitAndLossReport> getProfitAndLoss(String companymstid, Date fromdate, Date todate, String branchCode);

}
