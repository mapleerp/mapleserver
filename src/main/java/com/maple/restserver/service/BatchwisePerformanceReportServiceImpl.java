package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.report.entity.BatchwisePerformancereport;
import com.maple.restserver.report.entity.BatchwiseSalesTransactionReport;
import com.maple.restserver.repository.SalesTransHdrRepository;

@Service
@Transactional
public class BatchwisePerformanceReportServiceImpl implements BatchwisePerformanceReportService  {

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Override
	public List<BatchwisePerformancereport> batchwisePerformanceReport(String companymstid, String branchcode,
			Date fDate, Date TDate, List<String> itemgroup) {

		  List<BatchwisePerformancereport> reportList=new ArrayList<BatchwisePerformancereport>();
	  for(String catId:itemgroup) {
		  List<Object>objList=salesTransHdrRepository.batchwisePerformanceReport( companymstid,  branchcode,fDate,TDate,catId);
	 
		  for(int i=0;i<objList.size();i++)
			 {
			Object[] objAray = (Object[]) objList.get(i);
			BatchwisePerformancereport report=new BatchwisePerformancereport();
			report.setVouchernumber((String) objAray[0]);
			report.setInvoicedate((String) objAray[1]);
			report.setCistomername((String) objAray[2]);
			report.setInvoiceamount((Double) objAray[3]);
			report.setItemname((String) objAray[4]);
			report.setGroupname((String) objAray[5]);
			report.setBatchcode((String) objAray[6]);
			report.setQty((Double) objAray[7]);
			report.setAmount((Double) objAray[8]);
			report.setTax((Double) objAray[9]);
			reportList.add(report);
			 }
	  }
		
		return reportList;
	
		
		
	
	}

}
