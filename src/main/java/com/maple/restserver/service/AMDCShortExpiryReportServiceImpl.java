package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.AMDCShortExpiryReport;
import com.maple.restserver.repository.ItemBatchExpiryDtlRepository;

@Service
public class AMDCShortExpiryReportServiceImpl implements AMDCShortExpiryReportService {
	@Autowired
	ItemBatchExpiryDtlRepository itemBatchExpiryDtlRepository;

	@Override
	public List<AMDCShortExpiryReport> findAMDCShortExpiryReport(Date date) {

		List<AMDCShortExpiryReport> AMDCShortExpiryReportList = new ArrayList<AMDCShortExpiryReport>();

//		List<Object> obj = itemBatchExpiryDtlRepository.findAMDCShortExpiryReport(date);
//		for (int i = 0; i < obj.size(); i++) {
//			Object[] objAray = (Object[]) obj.get(i);
//			AMDCShortExpiryReport aMDCShortExpiryReport = new AMDCShortExpiryReport();
//
//			aMDCShortExpiryReport.setGroupName((String) objAray[0].toString());
//			aMDCShortExpiryReport.setItemName((String) objAray[1]);
//			aMDCShortExpiryReport.setBatchCode((String) objAray[2]);
//			aMDCShortExpiryReport.setItemCode((String) objAray[3]);
//
//			if (null != objAray[5]) {
//				aMDCShortExpiryReport.setExpiryDate((String) objAray[4].toString());
//			}
//			aMDCShortExpiryReport.setQuantity((Double) objAray[5]);
//			aMDCShortExpiryReport.setRate((Double) objAray[6]);
//			aMDCShortExpiryReport.setAmount((Double) objAray[7]);
//			aMDCShortExpiryReport.setDaysToExpiry((String) objAray[8]);
//
//			AMDCShortExpiryReportList.add(aMDCShortExpiryReport);
//
//		}

		return AMDCShortExpiryReportList;

	}

}
