package com.maple.restserver.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;

@Service
@Transactional
public interface ItemPropertyInstanceService {

	Boolean checkForItemPropertyInstance(String itemid, String purchasedtl,CompanyMst companymst,String branchCode);

}
