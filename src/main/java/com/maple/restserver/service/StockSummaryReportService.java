package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.StockSummaryCategoryWiseReport;
import com.maple.restserver.report.entity.StockSummaryDtlReport;
import com.maple.restserver.report.entity.StockSummaryReport;
@Component
public interface StockSummaryReportService {
	List<StockSummaryCategoryWiseReport> getCategoryWiseStockSummaryReport(String companymstid,String categoryid,Date fromdate,Date todate,String branchCode);
	List<StockSummaryReport> 	getAllCategoryStockSummaryReport(String companymstid,Date fromdate,Date todate,String branchCode);
	 List<StockSummaryDtlReport>  getStockSummaryDtls(String companymstid,String itemid,Date fromdate,Date todate,String branchCode);
	 
	 List<StockSummaryDtlReport>  getAllStockSummaryValue(String companymstid,Date todate,String branchCode,String categoryid);
	List<StockSummaryDtlReport> getAllStockSummaryByCostPrice(String companymstid, Date tDate, String branchCode,
			String categoryid);
	List<StockSummaryDtlReport> getStockSummaryDtls(String companymstid, Date fDate, Date tDate, String branchCode);

}
