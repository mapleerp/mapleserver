package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.DailySalesReportDtl;
import com.maple.restserver.report.entity.SalesProfitHdrReport;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
public class SalesProfitServiceImpl implements SalesProfitReportService {

	@Value("${spring.datasource.url}")
	private String dbType;
	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@Override
	public List<SalesProfitHdrReport> getSalesProfitHdrReport(String branchcode, Date fromdate, Date todate,
			String companymstid) {
	
		
		List< SalesProfitHdrReport> salesProfitReportList =   new ArrayList();
		List<Object> obj=new ArrayList<Object>();
		if (dbType.contains("mysql")) {
			obj =   salesTransHdrRepository.getSalesProfitReportWithBatch( branchcode,fromdate,todate,companymstid);
		}
		else if (dbType.contains("derby")) {
			obj =   salesTransHdrRepository.getSalesProfitReportWithBatchDerby( branchcode,fromdate,todate,companymstid);
		}
		 //h.voucher_number,c.customer_name, h.sales_mode , h.invoice_amount,h.voucher_date
		System.out.print(obj.size()+"object list size isssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		 for(int i=0;i<obj.size();i++)
		 {
			
			 Object[] objAray = (Object[]) obj.get(i);
			 SalesProfitHdrReport report=new SalesProfitHdrReport();
			 
			
			 report.setVoucherNumber((String) objAray[0]);
			 report.setCustomer((String) objAray[1]);
			 report.setReceiptMode((String) objAray[2]);
			 report.setInvoiceAmount((Double) objAray[3]);
			 String voucherDate=SystemSetting.UtilDateToString((Date) objAray[4]);
			 report.setVoucherDate(voucherDate);
			 Double costPriceTotal=(Double) objAray[5];
			 if(null!=costPriceTotal) {
			 report.setProfit((Double) objAray[3]-costPriceTotal);
			 }
			 if(null != objAray[6]) {
			 report.setBatch((String) objAray[6]);
			 }
		
			 salesProfitReportList.add(report);
		 }
		 
		return salesProfitReportList;
		
		
	}
	
	
	
}
