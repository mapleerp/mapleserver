package com.maple.restserver.service;

import java.net.UnknownHostException;
import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;

@Service
public interface MysqlDataTransferService {
 
 

	String doItemTransfermysql(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;
	String transferAccountHeads(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;
	String transferStockFromFile(CompanyMst companyMst, String fileName) throws UnknownHostException, Exception;
	String transferItemFromFile(CompanyMst companyMst, String fileName, String branchcode)
			throws UnknownHostException, Exception;

	String processPermissionMstTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception;

	String sysDateMstTransfermysql(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;
	
	String stockTransferOutTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception;
	
	String stockTransferOutHdrTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception;

	String deliveryBoyMstTransfermysql(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;
	String addKotWaiterTransfermysql(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String financialYearMstTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception;
	
	

	String kotCategoryMstTransfermysql(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String unitMstTransfermysql(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String productionTransfermysql(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String damageTransfermysql(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String priceDefenitionTransfermysql(CompanyMst companyMst, String branchCode)
			throws UnknownHostException, Exception;

	

	String productionDtlTransfermysql(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	 
	String transferKotTable(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferGroupMst(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferGroupPermission(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferProductConversion(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;

	String transferCategoryMst(CompanyMst companyMst,String branchCode);

	String transferBranchMst(CompanyMst companyMst,String branchCode);

 
	

	 

	String transferMenuMst(CompanyMst companyMst,String branchCode);

	String transferStoreMst(CompanyMst companyMst,String branchCode);

	String transferUserMstMysql(CompanyMst companyMst,String branchCode);

	

	String transferPurchaseHdrMysql(CompanyMst companyMst,String branchCode);

	String transferSalesDltDtl(CompanyMst companyMst,String branchCode);

	

	String transferOtherBranchSalesTransHdr(CompanyMst companyMst,String branchCode);

	String transferPrinterMst(CompanyMst companyMst,String branchCode);
	
	
	
	String transferSchemeEligibilityMysql(CompanyMst companyMst,String branchCode);

	
	String transferSchemeEligibilityAttribDefMysql(CompanyMst companyMst,String branchCode);

	String transferSchemeEligibilityAttribInstMysql(CompanyMst companyMst,String branchCode);

	String localCustomerMstTransfermysql(CompanyMst companyMst,String branchCode);
	
	String salesManMstTransfermysql(CompanyMst companyMst,String branchCode);

 
	
	String siteMstTransfermysql(CompanyMst companyMst,String branchCode);

	String supplierTransfermysql(CompanyMst companyMst,String branchCode);
	
	String supplierPriceMstTransfermysql(CompanyMst companyMst,String branchCode);
	
	String schemeInstanceTransfermysql(CompanyMst companyMst,String branchCode);

	String productConversionConfigMstTransfermysql(CompanyMst companyMst,String branchCode);
	
	String multiUnitMstTransfermysql(CompanyMst companyMst,String branchCode);
	
	String salesOrderReceiptsTransfermysql(CompanyMst companyMst,String branchCode);
	
	String schSelectDefTransfermysql(CompanyMst companyMst,String branchCode);

	String salesTypeMstTransfermysql(CompanyMst companyMst,String branchCode);

	String salesPropertiesConfigMstTransfermysql(CompanyMst companyMst,String branchCode);
	
	String schSelectionAttribInstTransfermysql(CompanyMst companyMst,String branchCode);
 
	String schOfferDefTransfermysql(CompanyMst companyMst,String branchCode);
	String itemDeviceMstTransfermysql(CompanyMst companyMst, String branchCode);
	String kotItemMstTransfermysql(CompanyMst companyMst, String branchCode);
  

	String paramValueConfigTransfermysql(CompanyMst companyMst,String branchCode);

	
	String branchMstTransfermysql(CompanyMst companyMst,String branchCode);
	
	String orderTakerMstTransfermysql(CompanyMst companyMst,String branchCode);
	
	String invoiceFormatMstTransfermysql(CompanyMst companyMst,String branchCode);

	String printerMstTransfermysql(CompanyMst companyMst,String branchCode);
	String customerMstTransfermysql(CompanyMst companyMst,String branchCode);
	String receiptModeMstTransfermysql(CompanyMst companyMst,String branchCode);

	String menuConfigMstTransfermysql(CompanyMst companyMst,String branchCode);
	
	//String menuTransfermysql(CompanyMst companyMst,String branchCode);
	
	

	String vouchernumbermysql(CompanyMst companyMst,String branchCode);

	String sequencemysql(CompanyMst companyMst,String branchCode);


	String accountClassDataTransferFromDerbyToMysql(CompanyMst companyMst, String branchcode) throws UnknownHostException, Exception;
	 
	//String debitClassDataTransferFromDerbyToMysql(CompanyMst companyMst, String branchcode) throws UnknownHostException, Exception;
	//String creditClassDataTransferFromDerbyToMysql(CompanyMst companyMst, String branchcode) throws UnknownHostException, Exception;
	
	
	String paymentTransferToMysql(CompanyMst companyMst, String branchcode) throws UnknownHostException, Exception;
	 
	String salesOrderTransfermysql(CompanyMst companyMst, String branchCode) throws UnknownHostException, Exception;
	String transferSalesFromDerbyToMysql(CompanyMst companyMst,String branchCode);

	String  itemBatchDtlFromDerbyToMysql(CompanyMst companyMst,String branchCode);
	
	void kitDefinitionMstFromDerbyToMysql(CompanyMst companyMst, String branchCode);
	
	 
	
	
	void menuWindowMstTransfermysql(CompanyMst companyMst, String branchCode);
	void processMstTransfermysql(CompanyMst companyMst, String branchCode);
	
	
	void soTransfermysql(CompanyMst companyMst, String branchcode);
	//String itemBatchDtlStockMigration(CompanyMst companyMst, String branchCode);
	String openingStockDtlStockMigration(CompanyMst companyMst, String branchcode);
	
}
