package com.maple.restserver.service;

import java.util.List;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ProcessPermissionWebEntity;

public interface ProcessPermissionService {

	List<ProcessPermissionWebEntity> findByCompanyMst(CompanyMst companyMst);

}
