package com.maple.restserver.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.ResendBulkTransactionToCloud;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.jms.send.KafkaMapleEvent;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.utils.EventBusFactory;
@Service
@Transactional
public class ResendBulkTransactionToCloudServiceImpl implements ResendBulkTransactionToCloudService{
	private final Logger logger = LoggerFactory.getLogger(ResendBulkTransactionToCloudService.class);
	EventBus eventBus = EventBusFactory.getEventBus();
	Integer partitionKey = 1;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Autowired
	SalesDetailsRepository salesDetailsRepository;
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;
	@Autowired
	PurchaseDtlRepository purchaseDtlRepository;
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;
	@Autowired
	SalesReceiptsRepository salesReceiptsRepository;
	
	@Value("${mybranch}")
	private String mybranch;
	
	private String file_sufix = ".dat";
	private String name_differentiator = "$";
	
	@Override
	public void resendBulkTransactionToCloud(Date fromDate, Date toDate,
			String vouchertype) {
		
		String destination = MapleConstants.TOSERVER;

		
		ResendBulkTransactionToCloud resendBulkTransactionToCloud = new ResendBulkTransactionToCloud();
		ArrayList<SalesTransHdr> salesTransHdrArray = new ArrayList<SalesTransHdr>();
		ArrayList<SalesDtl> salesDtlArray = new ArrayList<SalesDtl>();
		ArrayList<SalesReceipts> salesReceiptsArray = new ArrayList<SalesReceipts>();
		ArrayList<PurchaseHdr> purchaseHdrArray = new ArrayList<PurchaseHdr>();
		ArrayList<PurchaseDtl> purchaseDtlArray = new ArrayList<PurchaseDtl>();
		ArrayList<ItemBatchDtl> itemBatchDtlArray = new ArrayList<ItemBatchDtl>();
		
		if(vouchertype.equalsIgnoreCase(MapleConstants.SALESTYPE)) {
			List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findAllSalesBetweenDate(fromDate, toDate);
			salesTransHdrArray.addAll(salesTransHdrList);
			for(int i=0;i<salesTransHdrList.size();i++) {
				List<SalesDtl> salesDtlList=salesDetailsRepository.findBySalesTransHdr(salesTransHdrList.get(i));
				salesDtlArray.addAll(salesDtlList);
				
				List<SalesReceipts> salesReceiptsList = salesReceiptsRepository.findBySalesTransHdr(salesTransHdrList.get(i));
				salesReceiptsArray.addAll(salesReceiptsList);
			}
			
			resendBulkTransactionToCloud.setSalesTransHdrList(salesTransHdrArray);
			resendBulkTransactionToCloud.setSalesDtlList(salesDtlArray);
			resendBulkTransactionToCloud.setSalesReceiptsList(salesReceiptsArray);
		}
		else if(vouchertype.equalsIgnoreCase(MapleConstants.PURCHASETYPE)) {
			List<PurchaseHdr> purchaseHdrList = purchaseHdrRepository.findAllPurchaseBetweenDate(fromDate, toDate);
			purchaseHdrArray.addAll(purchaseHdrList);
			for(int i=0;i<purchaseHdrList.size();i++) {
				List<PurchaseDtl> purchaseDtlList=purchaseDtlRepository.findByPurchaseHdrId(purchaseHdrList.get(i).getId());
				purchaseDtlArray.addAll(purchaseDtlList);
			}
			resendBulkTransactionToCloud.setPurchaseHdrList(purchaseHdrArray);
			resendBulkTransactionToCloud.setPurchaseDtlList(purchaseDtlArray);
		}
		else if(vouchertype.equalsIgnoreCase(MapleConstants.ITEMBATCHDTLTYPE)) {
			List<ItemBatchDtl> itemBatchDtlList = itemBatchDtlRepository.findAllItemBatchDtlBetweenDate(fromDate, toDate);
			itemBatchDtlArray.addAll(itemBatchDtlList);
			resendBulkTransactionToCloud.setItemBatchDtlList(itemBatchDtlArray);
		}
		
		
		String jsonStr = "";

		ObjectMapper Obj = new ObjectMapper()
				.registerModule(new ParameterNamesModule()).registerModule(new Jdk8Module())
				.registerModule(new JavaTimeModule());
		try {
			// Converting the Java object into a JSON string
			jsonStr = Obj.writeValueAsString(resendBulkTransactionToCloud);
			logger.info("Converted to JSON String: {} ", jsonStr);
		} catch (IOException e) {
			System.out.println(jsonStr);
			logger.info("Exception : {} ", e);
		}
		KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
		kafkaMapleEvent.setLibraryEventId(partitionKey);
		kafkaMapleEvent.setLibraryEventType(KafkaMapleEventType.RESENDBULKTRANSACTION);
		kafkaMapleEvent.setPayLoad(jsonStr);

		kafkaMapleEvent.setSource(mybranch);
		kafkaMapleEvent.setDestination(destination);
		
		String sourceFileName =  kafkaMapleEvent.getDestination() + name_differentiator + 
				KafkaMapleEventType.RESENDBULKTRANSACTION + file_sufix;
		kafkaMapleEvent.setSourceFile(sourceFileName);
		
		eventBus.post(kafkaMapleEvent);
		
		
	}

}
