package com.maple.restserver.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BranchMst;

import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.entity.MonthlyJournalHdr;
import com.maple.restserver.entity.MonthlySalesTransHdr;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.TallyFailedVoucherMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.BranchMstRepository;

import com.maple.restserver.repository.MonthlySalesDtlRepository;
import com.maple.restserver.repository.MonthlySalesReceiptsRepository;
import com.maple.restserver.repository.MonthlySalesTransHdrRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.TallyFailedVoucherMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.tally.integration.task.CPaymentsToTally;
import com.maple.restserver.tally.integration.task.CReceiptsToTally;
import com.maple.restserver.tally.integration.task.JournalIntegrationTally;
import com.maple.restserver.tally.integration.task.PurchaseIntegrationTally;
import com.maple.restserver.tally.integration.task.SaleIntegrationTally;
import com.maple.restserver.utils.SystemSetting;


@Service
@Transactional
@Component
public class TallyServiceImpl implements TallyService {
	private static final Logger logger = LoggerFactory.getLogger(TallyServiceImpl.class);
	
	
	@Autowired
	TallyFailedVoucherMstRepository tallyFailedVoucherMstRepository;
	@Value("${tallyServer}")
	private String tallyServer;
	@Autowired
	DebitClassRepository debitClassRepo;
	@Autowired
	AccountClassRepository accountClassRepo;
	@Autowired
	LedgerClassRepository ledgerClassRepo;
	@Autowired
	SaleIntegrationTally saleIntegrationTally;
	@Autowired
	CreditClassRepository creditClassRepo;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Autowired
	AccountHeadsRepository accountHeadsRepo;
	@Autowired
	MonthlySalesReceiptsRepository monthlySalesreceiptRepo;
	@Autowired
	MonthlySalesDtlRepository monthlySalesDtlRepo;

	@Autowired
	private VoucherNumberService voucherService;
	@Autowired
	MonthlySalesTransHdrRepository monthlySalesTransHdrRepo;


	@Autowired
	PurchaseIntegrationTally purchaseIntegrationTally;
	@Autowired
	BranchMstRepository branchMstRepo;


	@Autowired
	JournalIntegrationTally journalIntegrationTally;
	
	@Autowired
	CPaymentsToTally cPaymentsToTally;
	
	@Autowired
	CReceiptsToTally cReceiptsToTally;

	
	
	@Override
	public void monthlySalesSummaryToTally(Date fromDate,Date toDate) {
		BranchMst branchMst = branchMstRepo.getMyBranch();
		List<String> custIdList = monthlySalesTransHdrRepo.getDistinctCustId(fromDate,toDate);
		for(int i=0;i<custIdList.size();i++)
		{
			int month = toDate.getMonth();
			AccountHeads customerMstopt =accountHeadsRepo.findByIdAndCompanyMstId(custIdList.get(i),branchMst.getCompanyMst().getId());
			MonthlySalesTransHdr monthlySalesTransHdr = new MonthlySalesTransHdr();
			monthlySalesTransHdr.setBranchCode(branchMst.getBranchCode());
			monthlySalesTransHdr.setCustomerId(custIdList.get(i));
			monthlySalesTransHdr.setVoucherDate(toDate);
			monthlySalesTransHdr.setSalesMode("POS");
			Double invoiceAmount = monthlySalesTransHdrRepo.sumOfInvoiceAmount(custIdList.get(i),fromDate,toDate);
			monthlySalesTransHdr.setInvoiceAmount(invoiceAmount);
			monthlySalesTransHdr.setCompanyMst(branchMst.getCompanyMst());
			monthlySalesTransHdr.setAccountHeads(customerMstopt);
		
			monthlySalesTransHdr.setVoucherNumber(customerMstopt.getAccountName()+Integer.toString(month));
			monthlySalesTransHdr=monthlySalesTransHdrRepo.save(monthlySalesTransHdr);
			
			/*
			 * Update monthlysales Dtl with new Generated Id
			 */
			monthlySalesDtlRepo.updateMonthlySalesDtlWithId(monthlySalesTransHdr.getId(), custIdList.get(i), fromDate, toDate);
			monthlySalesreceiptRepo.updateMonthlySalesReceipts(monthlySalesTransHdr.getId(), custIdList.get(i), fromDate, toDate);
			AccountHeads accountHeads = accountHeadsRepo.findByAccountName(branchMst.getBranchCode()+"-CASH");
			
			monthlySalesreceiptRepo.updateMonthlySalesReceiptsVoucherNumber(monthlySalesTransHdr.getId(),monthlySalesTransHdr.getVoucherNumber(),branchMst.getBranchCode()+"-CASH",accountHeads.getId());
			
		}
		monthlySalesTransHdrRepo.deleteOrphanMonthlySalesTransHdr();
	}

	@Override
	public String salesToTallyByVoucherNoAndVoucherDate(String voucherno, java.sql.Date date) {
		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findByVoucherNumberAndVoucherDate(voucherno, date);
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findByVoucherNumberLike("%2019-2020RT002209%");
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findFailedVouchers();
		
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = salesTransHdrList.iterator();

		while (iterSales.hasNext()) {
			SalesTransHdr salesTransHdr = (SalesTransHdr) iterSales.next();

			if (null == salesTransHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = salesTransHdr.getVoucherNumber();
			Date voucherDate = salesTransHdr.getVoucherDate();
			
			
			
			System.out.println("VOUCHER NUBMER =  " + voucherNumber);
			System.out.println("VOUCHER DATE =  " + voucherDate);
			
			

			String sourvceId = salesTransHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = salesTransHdr.getId();

			  
			 

			String companyMst = salesTransHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = salesTransHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							boolean result = saleIntegrationTally.SaveCashSale(accountClass.getCompanyMst(),
									salesTransHdr.getBranchCode(), "GODOWN", salesTransHdr.getSalesMode(),
									voucherNumber, strVoucherDate, salesTransHdr.getAccountHeads(), debitClassList,
									creditClassList,tallyServer);

						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
							System.out.println(e.toString());
							System.out.println("FAILED VOUCHER =  " + voucherNumber);
							System.out.println("VOUCHER NUBMER =  " + voucherNumber);
							System.out.println("VOUCHER DATE =  " + voucherDate);
							TallyFailedVoucherMst tallyFailedVoucherMst = new TallyFailedVoucherMst();
							tallyFailedVoucherMst.setVoucherNumber(voucherNumber);
							
							tallyFailedVoucherMstRepository.save(tallyFailedVoucherMst);
							
							
							
						}
					}

				}
			}

		}

		return "Sales Posted to Tally";
		
	}
	@Override
	
	public boolean receiptToTallyByReceiptHdr(ReceiptHdr receiptHdr) {

		boolean result = false;

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = receiptHdr.getVoucherNumber();
			Date voucherDate = receiptHdr.getVoucherDate();

			String sourvceId = receiptHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = receiptHdr.getId();

			 
			 

			String companyMst = receiptHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = receiptHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					
					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							 
							
							
							
							 result =  cReceiptsToTally.SaveReceipts(accountClass.getCompanyMst(),
									 branchMst, "RECEIPT",
									 branchMst, 
										  voucherNumber,   strVoucherDate,
										  debitClassList,
										  creditClassList,tallyServer);
										
										
										
						} catch (    TransformerException | IOException | ParserConfigurationException | FactoryConfigurationError e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
						}
					}

				}
			}

		

		return result;
	}
	
	@Override
	public boolean JournalToTallyJournalHdr(JournalHdr journalHdr)
	{
		boolean result=false;

		BigDecimal zero = new BigDecimal("0");

		String voucherNumber = journalHdr.getVoucherNumber();
		Date voucherDate = journalHdr.getVoucherDate();
		
		
		
		System.out.println("VOUCHER NUBMER =  " + voucherNumber);
		System.out.println("VOUCHER DATE =  " + voucherDate);
		
		

		String sourvceId = journalHdr.getId();
		// String lmsqueueId = jobInfo.getId();

		String id = journalHdr.getId();

		  
		 

		String companyMst = journalHdr.getCompanyMst().getId();
		// (String) context.getJobDetail().getJobDataMap().get("companyid");

		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		String branchMst = journalHdr.getBranchCode();

		String goDownName = branchMst;

		String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

		System.out.println("Company ID " + companyMst);

		List<AccountClass> accountClassPreviousList = accountClassRepo
				.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

		if (accountClassPreviousList.size() > 0) {

			Iterator iter = accountClassPreviousList.iterator();
			while (iter.hasNext()) {
				AccountClass accountClass = (AccountClass) iter.next();

				List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
				List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

				List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

				if (creditClassList.size() > 0) {

					try {
						 result = journalIntegrationTally.SaveJournal(accountClass.getCompanyMst(),
								journalHdr.getBranchCode(), "GODOWN", "JNL",
								voucherNumber, strVoucherDate,  debitClassList,
								creditClassList,tallyServer);

					} catch (SQLException e) {
						// TODO Auto-generated catch block
						logger.error(e.getMessage());
						System.out.println(e.toString());
						System.out.println("FAILED VOUCHER =  " + voucherNumber);
						System.out.println("VOUCHER NUBMER =  " + voucherNumber);
						System.out.println("VOUCHER DATE =  " + voucherDate);
						TallyFailedVoucherMst tallyFailedVoucherMst = new TallyFailedVoucherMst();
						tallyFailedVoucherMst.setVoucherNumber(voucherNumber);
						
						tallyFailedVoucherMstRepository.save(tallyFailedVoucherMst);
						
						
						
					}
				}

			}
		}
		return result;
	}

	
@Override
	
	public boolean PaymentsToTallyByPaymentHdr(PaymentHdr paymentHdr) {

		boolean result = false;

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = paymentHdr.getVoucherNumber();
			Date voucherDate = paymentHdr.getVoucherDate();

			String sourvceId = paymentHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = paymentHdr.getId();

			 
			 

			String companyMst = paymentHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = paymentHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					
					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							 
							
							
							
							 result =  cPaymentsToTally.SavePayments(accountClass.getCompanyMst(),
									 branchMst, "PAYMENTS",
									 branchMst, 
										  voucherNumber,   strVoucherDate,
										  debitClassList,
										  creditClassList,tallyServer);
										
										
										
						} catch (    TransformerException | IOException | ParserConfigurationException | FactoryConfigurationError e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
						}
					}

				}
			}

		

		return result;
	}
	
	@Override 
	public boolean purchaseToTallyByPurchaseHdr(PurchaseHdr purchaseHdr)
	{

		boolean result = false;
			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = purchaseHdr.getVoucherNumber();
			Date voucherDate = purchaseHdr.getVoucherDate();

			String sourvceId = purchaseHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = purchaseHdr.getId();

			  

			String companyMst = purchaseHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = purchaseHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							 result = purchaseIntegrationTally.SavePurchase(purchaseHdr.getCompanyMst(),
									tallyServer, purchaseHdr.getBranchCode(), voucherNumber, strVoucherDate, 
									purchaseHdr.getSupplierInvDate(), purchaseHdr.getSupplierInvNo(), 
									purchaseHdr.getSupplierId(), debitClassList, creditClassList,
									"PURCHASE");
									
									 
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
						}
					}

				}
			}
			return result;
		}
	
	@Override
	public boolean salesToTallyBySalesTranshdr(SalesTransHdr salesTransHdr) {
		
			boolean result = false ;

			if (null == salesTransHdr.getVoucherNumber()) {
				return result;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = salesTransHdr.getVoucherNumber();
			Date voucherDate = salesTransHdr.getVoucherDate();
			
			
			
			System.out.println("VOUCHER NUBMER =  " + voucherNumber);
			System.out.println("VOUCHER DATE =  " + voucherDate);
			
			

			String sourvceId = salesTransHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = salesTransHdr.getId();

			  
			 

			String companyMst = salesTransHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = salesTransHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							 result = saleIntegrationTally.SaveCashSale(accountClass.getCompanyMst(),
									salesTransHdr.getBranchCode(), "GODOWN", salesTransHdr.getSalesMode(),
									voucherNumber, strVoucherDate, salesTransHdr.getAccountHeads(), debitClassList,
									creditClassList,tallyServer);

						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
							System.out.println(e.toString());
							System.out.println("FAILED VOUCHER =  " + voucherNumber);
							System.out.println("VOUCHER NUBMER =  " + voucherNumber);
							System.out.println("VOUCHER DATE =  " + voucherDate);
							TallyFailedVoucherMst tallyFailedVoucherMst = new TallyFailedVoucherMst();
							tallyFailedVoucherMst.setVoucherNumber(voucherNumber);
							
							tallyFailedVoucherMstRepository.save(tallyFailedVoucherMst);
							
							
							
						}
					}

				}
			}


		return result;
		
	}
	
}
