package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ItemDetailPopUp;
import com.maple.restserver.repository.ItemStockPopUpRepository;

@Service
@Transactional
@Component
public class ItemDetailPopUpServiceImpl implements ItemDetailPopUpService{

	@Autowired
	private ItemStockPopUpRepository itemStockPopUpRepository;
	
	@Override
	public List<ItemDetailPopUp> getItemDetailPopUp(String searchstring, Pageable topFifty, CompanyMst companyMst,
			Date sdate) {
		
		
		List<ItemDetailPopUp> itemDetailPopUpList = new ArrayList();
		 
		 List<Object> objByItemCode = itemStockPopUpRepository
				.findSearchByItemCodeWithBatch(searchstring.toLowerCase(), topFifty, companyMst, sdate);
		System.out.println("findSearchByItemCodeWithBatch");
		
		for (int i = 0; i < objByItemCode.size(); i++) {
			Object[] objAray = (Object[]) objByItemCode.get(i);
			ItemDetailPopUp itemDetailPopUp = new ItemDetailPopUp();
			itemDetailPopUp.setItemName((String) objAray[0]);
			itemDetailPopUp.setBarcode((String) objAray[1]);
			itemDetailPopUp.setUnitName((String) objAray[2]);
			itemDetailPopUp.setStandardPrice((Double) objAray[3]);
			itemDetailPopUp.setQty((Double) objAray[4]);
			itemDetailPopUp.setCess((Double) objAray[5]);
			itemDetailPopUp.setTaxRate((Double) objAray[6]);
			itemDetailPopUp.setItemId((String) objAray[7]);
			itemDetailPopUp.setUnitId((String) objAray[8]);
			itemDetailPopUp.setBatch((String) objAray[9]);
			itemDetailPopUp.setItemCode((String) objAray[10]);
			itemDetailPopUp.setExpDate((Date) objAray[11]);
			itemDetailPopUp.setItemPriceLock((String) objAray[12]);
			itemDetailPopUp.setStore((String) objAray[13]);
			
			itemDetailPopUpList.add(itemDetailPopUp);
		}
		
			
		
		if (objByItemCode.size() == 0) {
			objByItemCode = itemStockPopUpRepository.findSearchByBarcodeWithBatch(searchstring.toLowerCase(),
					topFifty, companyMst, sdate);
			
			System.out.println("findSearchByBarcodeWithBatch");
			
			for (int i = 0; i < objByItemCode.size(); i++) {
				Object[] objAray = (Object[]) objByItemCode.get(i);
				ItemDetailPopUp itemDetailPopUp = new ItemDetailPopUp();
				itemDetailPopUp.setItemName((String) objAray[0]);
				itemDetailPopUp.setBarcode((String) objAray[1]);
				itemDetailPopUp.setUnitName((String) objAray[2]);
				itemDetailPopUp.setStandardPrice((Double) objAray[3]);
				itemDetailPopUp.setQty((Double) objAray[4]);
				itemDetailPopUp.setCess((Double) objAray[5]);
				itemDetailPopUp.setTaxRate((Double) objAray[6]);
				itemDetailPopUp.setItemId((String) objAray[7]);
				itemDetailPopUp.setUnitId((String) objAray[8]);
				itemDetailPopUp.setBatch((String) objAray[9]);
				itemDetailPopUp.setItemCode((String) objAray[10]);
				itemDetailPopUp.setExpDate((Date) objAray[11]);
				itemDetailPopUp.setItemPriceLock((String) objAray[12]);
				itemDetailPopUp.setStore((String) objAray[13]);
				
				itemDetailPopUpList.add(itemDetailPopUp);
			}

		}

		if (objByItemCode.size() == 0) {
			objByItemCode = itemStockPopUpRepository.findSearchWithBatch("%" + searchstring.toLowerCase() + "%",
					topFifty, companyMst, sdate);
			System.out.println("findSearchWithBatch");
			
			for (int i = 0; i < objByItemCode.size(); i++) {
				Object[] objAray = (Object[]) objByItemCode.get(i);
				ItemDetailPopUp itemDetailPopUp = new ItemDetailPopUp();
				itemDetailPopUp.setItemName((String) objAray[0]);
				itemDetailPopUp.setBarcode((String) objAray[1]);
				itemDetailPopUp.setUnitName((String) objAray[2]);
				itemDetailPopUp.setStandardPrice((Double) objAray[3]);
				itemDetailPopUp.setQty((Double) objAray[4]);
				itemDetailPopUp.setCess((Double) objAray[5]);
				itemDetailPopUp.setTaxRate((Double) objAray[6]);
				itemDetailPopUp.setItemId((String) objAray[7]);
				itemDetailPopUp.setUnitId((String) objAray[8]);
				itemDetailPopUp.setBatch((String) objAray[9]);
				itemDetailPopUp.setItemCode((String) objAray[10]);
				itemDetailPopUp.setExpDate((Date) objAray[11]);
				itemDetailPopUp.setItemPriceLock((String) objAray[12]);
				itemDetailPopUp.setStore((String) objAray[13]);
				
				itemDetailPopUpList.add(itemDetailPopUp);
			}

		}
		
		return itemDetailPopUpList;
		
	}

}
