package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.report.entity.PoliceReport;
import com.maple.restserver.report.entity.WholeSaleDetailReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CategoryMstRepository;

import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
public class PoliceReportServiceImpl implements PoliceReportService {

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	
	@Autowired
	AccountHeadsRepository  accountHeadsRepository;

	@Override
	public List<PoliceReport> findPoliceReport(CompanyMst companyMst, Date fromDate, Date toDate,String[] custList) {

		List<PoliceReport> policeReportList = new ArrayList<PoliceReport>();
		
		for (String customerName : custList) {
		List<AccountHeads> customerMst = (List<AccountHeads>) accountHeadsRepository.findByAccountNameAndCompanyMst(customerName, companyMst);
		List<Object> obj = salesTransHdrRepository.findPoliceReport(fromDate, toDate,customerMst.get(0).getId());
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			PoliceReport policeReport = new PoliceReport();
			policeReport.setInvoiceDate(SystemSetting.UtilDateToString((Date) objAray[0], "yyyy-MM-dd"));
			policeReport.setVoucherNumber((String) objAray[1]);
			policeReport.setCustomerName((String) objAray[2]);
			policeReport.setMpsType((String) objAray[2]);
			policeReport.setInvoiceAmount((Double) objAray[3]);
			policeReport.setCashPaid((Double) objAray[4]);
			policeReport.setCardPaid((Double) objAray[5]);
			policeReport.setCreditAmount((Double) objAray[6]);
			policeReport.setPatientID((String) objAray[7]);

			policeReport.setInsuranceAmount((Double) objAray[8]);

			policeReportList.add(policeReport);

		}}

		return policeReportList;
	}
}
