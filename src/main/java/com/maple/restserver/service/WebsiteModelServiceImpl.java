package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.WebsiteClass;
import com.maple.restserver.entity.WebsiteFeatureMst;
import com.maple.restserver.entity.WebsiteMainMst;
import com.maple.restserver.entity.WebsiteServiceMst;
import com.maple.restserver.repository.WebsiteFeatureMstRepository;
import com.maple.restserver.repository.WebsiteServiceMstRepository;

@Service
public class WebsiteModelServiceImpl  implements WebsiteModelService{

	@Autowired
	WebsiteServiceMstRepository websiteServiceMstRepository;
	
	@Autowired
	WebsiteFeatureMstRepository websiteFeatureMstRepository;
	
	@Override
	public WebsiteClass getWebsiteValues(CompanyMst companyMst) {
		
		WebsiteClass websiteClass=new WebsiteClass();
		
		WebsiteMainMst websiteMainMst=new WebsiteMainMst();
		websiteMainMst.setId("1");
		websiteMainMst.setSearchbannertexth1("ERP Which Is Scalable  As Your Business Grows"); 
		websiteMainMst.setAboutimageurl("../assets/images/image/about.jpg");
		websiteMainMst.setAboutp("Maple ERP is suitable for any retail outlet chain which is managed centrally. Maple combine the power of retail POS with a powerful back-end system. While maintaining the online transaction between branches and back-end , Maple allows POS billing even during internet outage. Mobile application module of Maple allows to track the transaction online via mobile application and helps in distribution/Van sales. Retail outlets need simple user interface but complex processing and integration at back end data.");
		websiteMainMst.setNewsletterh2br("Let us help you");
		websiteMainMst.setNewsletterrh2("Have Question in mind?");
		websiteMainMst.setLocation("MapleERP Software Solutions (P) Ltd, Karickom,Kottarakkara, Kerala,India.");
		websiteMainMst.setWebsitephone1("0474-2450546");
		websiteMainMst.setWebsitephone2("(+91) 9048105544");
		websiteMainMst.setEmail("sales@erpmaple.com");
		websiteMainMst.setCopyrightp("Copyright @ 2021 MapleERP");
		websiteClass.setWebsiteMainMst(websiteMainMst);
		
		ArrayList<WebsiteFeatureMst> websiteFeatureMstList =new ArrayList();
		
		List<WebsiteFeatureMst> websiteFeature=websiteFeatureMstRepository.findAll();
		for(WebsiteFeatureMst websiteFeatureMst:websiteFeature) {
			websiteFeatureMstList.add(websiteFeatureMst);
		}
		websiteClass.setWebsiteFeatureMst(websiteFeatureMstList);
		
		
		
		
       ArrayList<WebsiteServiceMst> websiteServiceMstList =new ArrayList();
		
		List<WebsiteServiceMst> websiteService=websiteServiceMstRepository.findAll();
		for(WebsiteServiceMst WebsiteServiceMst:websiteService) {
			websiteServiceMstList.add(WebsiteServiceMst);
		}
		websiteClass.setWebsiteServiceMst(websiteServiceMstList);
		
				
		return websiteClass;
		
		
	}
	

}
