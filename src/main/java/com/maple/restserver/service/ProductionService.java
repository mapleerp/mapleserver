package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionDtlDtl;
import com.maple.restserver.report.entity.ProductionDtlsReport;
import com.maple.restserver.report.entity.ReqRawMaterial;

@Service
public interface ProductionService {

	List<ProductionDtlDtl> getRawMaterialsByDate(Date date);

	List<ProductionDtlsReport> getRawMaterialsSumByDate(Date date);

	List<ProductionDtl> getKitByDate(Date date);

	String itemIsKitByName(String itemName);

	String itemIsKitById(String itemId);

	Double getProductionCostByProductionDetailId(String detailId);

	// Double getProductionCostByKitName(String iteName);
	// Double getProductionCostByKitId(String itemId);

	Double makeProductionByProductionDtlId(String productionDtlId, String companymstid);

	Double makeProductionByKitId(String itemId, double qty);

	List<ProductionDtlsReport> getRawMaterialsSumByBetweenDate(Date fdate, Date tDate);

	List<ReqRawMaterial> getRawMaterialQty(String acthdrid);

	List<ProductionDtl> getPlanedProduction();
	
	List<ProductionDtl> getKitByDateByStore(Date date,String store);


}
