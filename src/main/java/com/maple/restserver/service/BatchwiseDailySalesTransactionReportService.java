package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.report.entity.BatchwiseSalesTransactionReport;


@Component

public interface BatchwiseDailySalesTransactionReportService {

	List<BatchwiseSalesTransactionReport> batchwiseSalesTransactionReport(String companymstid,String branchcode,Date fDate,Date TDate ,List<String> groupList);
	
}
