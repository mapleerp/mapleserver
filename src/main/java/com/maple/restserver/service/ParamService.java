package com.maple.restserver.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.ParamMst;
import com.maple.restserver.repository.ParamMstRepository;

@Component
public class ParamService {
	@Autowired
	private ParamMstRepository paramMstRepository;
	
	

public void deleteParam() {
	
}

public List<ParamMst>  getAllParam(String companymstid) {
	return paramMstRepository.findByCompanyMstId(companymstid);
}

public  List<ParamMst>  searchLikeParamByName(String searchString,String companymstid) {
	return paramMstRepository.findSearch(searchString,companymstid);
}


public  Optional<ParamMst>  getParam(String id,String companymstid) {
	return paramMstRepository.findByIdAndCompanyMstId(id,companymstid);
}

	
}
