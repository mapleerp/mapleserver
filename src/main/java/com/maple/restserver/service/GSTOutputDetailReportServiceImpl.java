package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.GSTOutputDetailReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.utils.SystemSetting;
@Service
public class GSTOutputDetailReportServiceImpl implements GSTOutputDetailReportService{
	@Autowired
	AccountHeadsRepository accountHeadsRepo;
	
	@Value("${spring.datasource.url}")
	private String dbType;
	
	@Override
	public List<GSTOutputDetailReport> findLedgerDetails(Date fdate, Date tdate) {
		
		List<GSTOutputDetailReport> gstOutputDetailReportList = new ArrayList<GSTOutputDetailReport>();
		List<Object> obj=new ArrayList<Object>();
		if (dbType.contains("mysql")) {
			obj = accountHeadsRepo.findLedgerDetails(fdate,tdate);
		}
		else if (dbType.contains("derby")) {
			obj = accountHeadsRepo.findLedgerDetailsDerby(fdate,tdate);
		}
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			GSTOutputDetailReport gstOutputDetailReport = new GSTOutputDetailReport();
			
			gstOutputDetailReport.setInvoiceDate(SystemSetting.UtilDateToString((Date) objAray[0], "yyyy-MM-dd"));
			gstOutputDetailReport.setInvoiceNumber((String) objAray[1]);
			gstOutputDetailReport.setCustomerName((String) objAray[2]);
			gstOutputDetailReport.setCustomerTin((String) objAray[3]);
			gstOutputDetailReport.setTotalSalesExcludingGst((Double) objAray[5]);
			gstOutputDetailReport.setGstAmount((Double) objAray[6]);
			
			Double totalPurchaseExcludingGst = (Double) objAray[5];
			Double GstAmount = (Double) objAray[6];
			Double totalPurchase = totalPurchaseExcludingGst + GstAmount;
			
			gstOutputDetailReport.setTotalSales(totalPurchase);
			gstOutputDetailReport.setBranch((String) objAray[7]);
			gstOutputDetailReport.setGstPercent((String) objAray[8].toString());
			gstOutputDetailReport.setExcemptedSales((Double) objAray[9]);
			gstOutputDetailReportList.add(gstOutputDetailReport);
			
		 }
//		GSTOutputDetailReport gstOutputDetailReport = new GSTOutputDetailReport();
//		gstOutputDetailReport.setInvoiceDate((String) "2021-05-01");
//		gstOutputDetailReport.setInvoiceNumber((String) "123456");
//		gstOutputDetailReport.setCustomerName((String) "abc");
//		gstOutputDetailReport.setTotalSalesExcludingGst((String) "1000");
//		gstOutputDetailReport.setGstAmount((String) "50");
//		gstOutputDetailReport.setTotalSales((String) "2000");
//		gstOutputDetailReport.setFromDate(fdate);
//		gstOutputDetailReport.setToDate(tdate);
//		
//		gstOutputDetailReportList.add(gstOutputDetailReport);
		
		
		

		return gstOutputDetailReportList;
	
	}
	
	
}
