package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PharmacyPurchaseReport;
import com.maple.restserver.report.entity.HsnCodeSaleReport;
import com.maple.restserver.report.entity.HsnWisePurchaseDtlReport;
import com.maple.restserver.report.entity.MonthlySupplierSummary;
import com.maple.restserver.report.entity.PharmacySalesDetailReport;
import com.maple.restserver.report.entity.PurchaseReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.StatementOfAccountReport;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
@Transactional
@Component
public class PurchaseReportServiceImpl implements PurchaseReportService {

	@Autowired
	PurchaseHdrRepository purchaseHdrRepo;

	@Autowired
	CategoryMstRepository categoryMstRepo;
 
	@Autowired
	CategoryMstRepository categoryMstRepository;
@Autowired
PurchaseDtlRepository purchaseDtlRepository;
	@Override
	public List<Object> getDailyPurchase(String voucherNo) {
		
		 //List<Object> obj =purchaseHdrRepo.getDailyPurchase( voucherNo);
		return  null;
	}



	@Override
	public List< PurchaseReport> getAllPurchase() 
	{
		List< PurchaseReport> purchaseReportList =   new ArrayList();
		 
	 List<Object> obj = purchaseHdrRepo.getAllPurchase();  
		 for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 PurchaseReport purchaseReport = new PurchaseReport();
	         purchaseReport.setSupplierName((String) objAray[0]);
			 purchaseReport.setVoucherNumber(( String) objAray[1]);
			 purchaseReport.setVoucherDate(( Date) objAray[2]);
			 purchaseReport.setItemName((String) objAray[3]);
			 purchaseReport.setQty(( Double) objAray[4]);
			 purchaseReport.setPurchseRate(( Double) objAray[5]);
			 purchaseReport.setTaxRate(( Double) objAray[6]);
			 purchaseReportList.add(purchaseReport);
			 
		 }
		 return purchaseReportList;

	}



	@Override
	public List<PurchaseReport> retrieveReportPurchase(String vouchernumber, String branchcode, Date date) {
		List< PurchaseReport> purchaseReportList =   new ArrayList();
		 List<Object> obj = purchaseHdrRepo.retrieveReportPurchase( vouchernumber, branchcode,  date);  
			 for(int i=0;i<obj.size();i++)
			 {
				 Object[] objAray = (Object[]) obj.get(i);
				 PurchaseReport purchaseReport = new PurchaseReport();
		         purchaseReport.setSupplierName((String) objAray[0]);
				 purchaseReport.setVoucherNumber(( String) objAray[1]);
				 purchaseReport.setItemName((String) objAray[2]);
				 purchaseReport.setQty(( Double) objAray[3]);
				 purchaseReport.setPurchseRate(( Double) objAray[4]);
				 purchaseReport.setTaxRate(( Double) objAray[5]);
				 purchaseReport.setVoucherDate(( Date) objAray[6]);
				 purchaseReport.setSupplierInvNo((String) objAray[7]);
				 purchaseReport.setUnit((String) objAray[8]);
				 purchaseReport.setAmount((Double) objAray[9]);
				 purchaseReport.setSupplierGst((String) objAray[10]);
				 Double totalAmount= purchaseHdrRepo.retrieveTotalAmount( vouchernumber, branchcode, date);
				 purchaseReport.setAmountTotal(totalAmount);
				 purchaseReportList.add(purchaseReport);
				 
			 }
			 return purchaseReportList;
	}



	@Override
	public List<PurchaseReport> retrieveTaxRate(String vouchernumber, String branchcode, Date date) {
		List< PurchaseReport> taxtList =   new ArrayList();
	
		 List<Object> obj = purchaseDtlRepository.findTaxwiseSumPurchaseDtl( vouchernumber, branchcode,  date);  
			 for(int i=0;i<obj.size();i++)
			 {
				 
				 
				 Object[] objAray = (Object[]) obj.get(i);
				 PurchaseReport taxReport = new PurchaseReport();
				 taxReport.setTaxAmount((Double) objAray[0]);;
				 taxReport.setTaxRate(( Double) objAray[1]);;
				 taxtList.add(taxReport);
			 }
			 return taxtList;
	}



	@Override
	public List<MonthlySupplierSummary> retrievePurchaseReport(String vouchernumber, String branchcode,
			String startDate, String endDate) {
		List< MonthlySupplierSummary> taxtList =   new ArrayList();
		
		 List<Object> obj = purchaseDtlRepository.retrievePurchaseReport( vouchernumber, branchcode,startDate,endDate);  
			 for(int i=0;i<obj.size();i++)
			 {
			/*
			 * @Query(nativeQuery = true, value
			 * ="select c.company_name,b.branch_name,b.branch_adress1,b.branch_adress2,b.branch_state,b.branch_gst,b.branch_email,b.branch_website,b.branch_tel_no,pd.debit_amount,pd.credit_amount,pd.closing_balance,startDate,endDate "
			 * +
			 * "s.supplier_name,s.company,s.phone_no,s.emailid i.item_name, from purchase_hdr h,purchase_dtl d,branch_mst b,item_mst i,payable_dtl pd,"
			 * +
			 * "company_mst c supplier s where h.id=d.purchase_hdr_id and s.id=h.supplier_id and h.company_mst=c.id and b.branch_code=h.branch_code and i.id=d.item_id and pd.supplier_mst_id=s.id and h.voucher_date between :startDate and:endDate and h.branch_code:branchcode and h.voucher_number=:vouchernumber "
			 * )
			 */
				 Object[] objAray = (Object[]) obj.get(i);
				 MonthlySupplierSummary  supplieReport = new MonthlySupplierSummary();
				
				
			 }
			 return taxtList;
	}



	@Override
	public List<HsnCodeSaleReport> getHSNCodeWiseSalesDtl(Date fdate, Date tdate, String branchCode) {
		List<HsnCodeSaleReport> hsnCodeReportList = new ArrayList<HsnCodeSaleReport>();
		List<Object> obj = purchaseDtlRepository.getHSNCodeWiseSalesDtl(fdate,  tdate, branchCode);
		 for(int i=0;i<obj.size();i++)
		 {
			Object[] objAray = (Object[]) obj.get(i);
			HsnCodeSaleReport hsnCodeSaleReport = new HsnCodeSaleReport();
			hsnCodeSaleReport.setHsnCode((String) objAray[0]);
			hsnCodeSaleReport.setQty((Double) objAray[1]);
			hsnCodeSaleReport.setCgst((Double) objAray[2]);
			hsnCodeSaleReport.setValue((Double) objAray[3]);
			hsnCodeSaleReport.setVoucherDate((Date) objAray[4]);
			hsnCodeReportList.add(hsnCodeSaleReport);
		 }

		return hsnCodeReportList;
	}


//version  3.1
	@Override
	public List<PurchaseReport> getPurchaseDtlReport(Date fromDate, Date toDate, String branchCode,
			CompanyMst companyMst) {
		
		List< PurchaseReport> purchaseReportList =   new ArrayList();
		
		 java.sql.Date	sqlfromDate=SystemSetting.UtilDateToSQLDate(fromDate);
		 java.sql.Date	sqltoDate=SystemSetting.UtilDateToSQLDate(toDate);
		 List<Object> obj = purchaseHdrRepo.getPurchaseDtlReport( sqlfromDate, sqltoDate,  branchCode,companyMst);  
		 for(int i=0;i<obj.size();i++)
		 {
			
			 Object[] objAray = (Object[]) obj.get(i);
			 PurchaseReport purchaseReport = new PurchaseReport();
	         purchaseReport.setSupplierName((String) objAray[2]);
			 purchaseReport.setVoucherNumber(( String) objAray[0]);
			 purchaseReport.setItemName((String) objAray[3]);
			 purchaseReport.setQty(( Double) objAray[4]);
			 purchaseReport.setPurchseRate(( Double) objAray[5]);
			 purchaseReport.setTaxRate(( Double) objAray[6]);
			 purchaseReport.setVoucherDate(( Date) objAray[1]);
			 purchaseReport.setUnit((String) objAray[7]);
			 purchaseReport.setAmount(( Double) objAray[5]*( Double) objAray[4]);
			 purchaseReportList.add(purchaseReport);
			 
		 }
		 return purchaseReportList;

	}
//	version 3.1 end 
	
	
	
	@Override
	public List<PurchaseReport> purchaseExportToExcel(CompanyMst companyMst, String branchcode, Date fromDate,
			Date todate) {
		List< PurchaseReport> purchaseReportList =   new ArrayList();
		
		 List<Object> obj=purchaseHdrRepo.purchaseExportToExcel(fromDate, todate, companyMst, branchcode);
		 for(int i=0;i<obj.size();i++)
		 {
			
			 
			
			 Object[] objAray = (Object[]) obj.get(i);
			 PurchaseReport purchaseReport = new PurchaseReport();
			 purchaseReport.setVoucherNumber(( String) objAray[0]);
			 purchaseReport.setVoucherDate(( Date) objAray[1]);
			 purchaseReport.setAmountTotal(( Double) objAray[2]);
			 
	         purchaseReport.setSupplierName((String) objAray[3]);
			 purchaseReport.setItemName((String) objAray[4]);
			 purchaseReport.setQty(( Double) objAray[5]);
			 purchaseReport.setUnit((String) objAray[6]);
			 purchaseReport.setTaxRate(( Double) objAray[7]);
			 purchaseReport.setAmount(( Double) objAray[8]);
			 purchaseReport.setPurchseRate((Double) objAray[9]);
			 purchaseReport.setTaxAmount((Double) objAray[10]);
			 purchaseReport.setSupplierGst((String) objAray[11]);
			 
			 purchaseReportList.add(purchaseReport);
			 
		 }
		
		return purchaseReportList;
	}



	@Override
	public List<PharmacyPurchaseReport> getPharmacyPurchaseReport(CompanyMst companyMst, Date fdate, Date tdate,
			String branchCode, String[] array) {
	
		

		List<PharmacyPurchaseReport>  pharmacyPurchaseReportList=new ArrayList<PharmacyPurchaseReport>();
		
		for(String categoryName:array) {
			CategoryMst categoryMst=categoryMstRepository.findByCategoryNameAndCompanyMst(categoryName,companyMst);

		List<Object> obj=purchaseHdrRepo.getPharmacyPurchaseReport(companyMst,fdate ,tdate, branchCode, categoryMst.getId());
		//PharmacyPurchaseReport pharmacyPurchaseReport=new PharmacyPurchaseReport();
		for(int i=0;i<obj.size();i++) {
			Object[] objAray =(Object[]) obj.get(i);
			PharmacyPurchaseReport pharmacyPurchaseReport=new PharmacyPurchaseReport();
		
				/*
				 * + "h.tansaction_entry_date ," + " s.supplier_name, " + "" +
				 * "h.voucher_date, " + "s.supgst," + "h.supplier_inv_date," +
				 * " h.invoice_total "
				 */
			String trEntryDate=SystemSetting.UtilDateToString((Date) objAray[0]);
			pharmacyPurchaseReport.setTrEntryDate(trEntryDate);
			pharmacyPurchaseReport.setSupplierName((String) objAray[1]);
			
			System.out.print((Date) objAray[2]+"voucher date isssssssssssssssssssssssssssssssssssssssssssssss");
			String ourVoucherDate=SystemSetting.UtilDateToString((Date) objAray[2]);
			pharmacyPurchaseReport.setOurVoucherDate(ourVoucherDate);
			String supplierVoucherDate=SystemSetting.UtilDateToString((java.util.Date) objAray[4]);
			pharmacyPurchaseReport.setSupplierVoucherDate(ourVoucherDate);
			pharmacyPurchaseReport.setTinNo((String) objAray[3]);
			
			pharmacyPurchaseReport.setNetInvoiceTotal((Double) objAray[5]);
			pharmacyPurchaseReportList.add(pharmacyPurchaseReport);
		}
		}
		return pharmacyPurchaseReportList;
		

	}
	public List<PurchaseReport> getSupplierWisePurchaseReport(Date fromDate, Date toDate, String[] array,
			String supname,CompanyMst companyMst) {
		List< PurchaseReport> purchaseReportList =   new ArrayList();
		for(String categoryName:array) {
			CategoryMst categoryMst=categoryMstRepo.findByCategoryNameAndCompanyMst(categoryName,companyMst);
		List<Object> obj = purchaseHdrRepo.getSupplierwisePurchaseReport(categoryMst.getId(),supname,fromDate,toDate);
		for(int i=0;i<obj.size();i++)
		 {
			
			 
			
			 Object[] objAray = (Object[]) obj.get(i);
			 PurchaseReport purchaseReport = new PurchaseReport();
			 purchaseReport.setAmount(( Double) objAray[6]);
			 purchaseReport.setItemName((String) objAray[3]);
			 purchaseReport.setPurchseRate(( Double) objAray[5]);
			 purchaseReport.setQty(( Double) objAray[4]);
			 purchaseReport.setSupplierInvNo((String) objAray[7]);
			 purchaseReport.setSupplierName((String) objAray[0]);
			 purchaseReport.setVoucherNumber((String) objAray[1]);
			 purchaseReport.setVoucherDate((Date) objAray[2]);
			 purchaseReportList.add(purchaseReport);
		}
		}
		return purchaseReportList;
	}



	@Override
	public List<PurchaseReport> getSupplierWisePurchaseReportWithDate(Date fromDate, Date toDate, String supname,
			CompanyMst companyMst) {
		List< PurchaseReport> purchaseReportList =   new ArrayList();
		List<Object> obj = purchaseHdrRepo.getSupplierwisePurchaseReportWithDate(supname,fromDate,toDate);
		for(int i=0;i<obj.size();i++)
		 {
			
			 
			
			 Object[] objAray = (Object[]) obj.get(i);
			 PurchaseReport purchaseReport = new PurchaseReport();
			 purchaseReport.setAmount(( Double) objAray[6]);
			 purchaseReport.setItemName((String) objAray[3]);
			 purchaseReport.setPurchseRate(( Double) objAray[5]);
			 purchaseReport.setQty(( Double) objAray[4]);
			 purchaseReport.setSupplierInvNo((String) objAray[7]);
			 purchaseReport.setSupplierName((String) objAray[0]);
			 purchaseReport.setVoucherNumber((String) objAray[1]);
			 purchaseReport.setVoucherDate((Date) objAray[2]);
			 purchaseReportList.add(purchaseReport);
		}
		
		return purchaseReportList;
		}
	
	
	@Override
	public List<HsnWisePurchaseDtlReport> getHsnWisePurchaseDtlReport(Date fromDate, Date toDate, String[] array,
			String branchcode, CompanyMst companyMst) {

		List<HsnWisePurchaseDtlReport> hsnPurchaseList = new ArrayList();
		List<Object> obj = purchaseHdrRepo.getHsnPurchaseDtlReport(fromDate,toDate,array,branchcode,companyMst);
		for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 HsnWisePurchaseDtlReport hsnWisePurchaseDtlReport = new HsnWisePurchaseDtlReport();
			 hsnWisePurchaseDtlReport.setHsnCode((String) objAray[1]);
			 hsnWisePurchaseDtlReport.setItemName((String) objAray[0]);
			 hsnWisePurchaseDtlReport.setQty((Double) objAray[3]);
			 hsnWisePurchaseDtlReport.setRate((Double) objAray[5]);
			 hsnWisePurchaseDtlReport.setTaxRate((Double) objAray[6]);
			 hsnWisePurchaseDtlReport.setUnitName((String) objAray[2]);
			 hsnWisePurchaseDtlReport.setValue((Double) objAray[4]);
			 hsnPurchaseList.add(hsnWisePurchaseDtlReport);
		 }

		return hsnPurchaseList;
	}



	@Override
	public List<HsnWisePurchaseDtlReport> getHsnWisePurchaseDtlReportWithoutGroup(Date fromDate, Date toDate,
			String branchcode, CompanyMst companyMst) {
		List<HsnWisePurchaseDtlReport> hsnPurchaseList = new ArrayList();
		List<Object> obj = purchaseHdrRepo.getHsnPurchaseDtlReportWithoutGroup(fromDate,toDate,branchcode,companyMst);
		for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 HsnWisePurchaseDtlReport hsnWisePurchaseDtlReport = new HsnWisePurchaseDtlReport();
			 hsnWisePurchaseDtlReport.setHsnCode((String) objAray[1]);
			 hsnWisePurchaseDtlReport.setItemName((String) objAray[0]);
			 hsnWisePurchaseDtlReport.setQty((Double) objAray[3]);
			 hsnWisePurchaseDtlReport.setRate((Double) objAray[5]);
			 hsnWisePurchaseDtlReport.setTaxRate((Double) objAray[6]);
			 hsnWisePurchaseDtlReport.setUnitName((String) objAray[2]);
			 hsnWisePurchaseDtlReport.setValue((Double) objAray[4]);
			 hsnPurchaseList.add(hsnWisePurchaseDtlReport);
		 }

		return hsnPurchaseList;
	}

//===============import purchase invoice detail===============//
	@Override
	public List<PurchaseReport> getImportPurchaseInvoiceDetail(String phdrid) {
		
		List<PurchaseReport> purchaseReportList = new ArrayList();
		List<Object> obj = purchaseHdrRepo.getImportPurchaseInvoiceDetail(phdrid);
		for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 PurchaseReport importPurchaseReport = new PurchaseReport();
			 importPurchaseReport.setSupplierName((String) objAray[0]);
			 importPurchaseReport.setSupplierAddress((String) objAray[1]);
			 importPurchaseReport.setSupplierEmail((String) objAray[2]);
			 importPurchaseReport.setSupplierGst((String) objAray[3]);
			 importPurchaseReport.setVoucherNumber((String) objAray[4]);
			 importPurchaseReport.setVoucherDate((Date) objAray[5]);
			 importPurchaseReport.setSupplierInvNo((String) objAray[6]);
			 importPurchaseReport.setIncoiceDate((String) objAray[7].toString());
			 importPurchaseReport.setItemCode((String) objAray[8]);
			 importPurchaseReport.setItemName((String) objAray[9]);
			 importPurchaseReport.setBatch((String) objAray[10]);
			 importPurchaseReport.setExpiry((Date) objAray[11]);
			 importPurchaseReport.setQty((Double) objAray[12]);
			 importPurchaseReport.setUnit((String) objAray[17]);
			 importPurchaseReport.setPurchseRate((Double) objAray[13]);
			 importPurchaseReport.setNetCost((Double) objAray[15]);
			 importPurchaseReport.setAmount((Double) objAray[14]);
			 importPurchaseReport.setTaxRate((Double) objAray[16]);
			 importPurchaseReport.setAmountTotal((Double) objAray[18]);
			 importPurchaseReport.setSupplierState((String) objAray[19]);
			 importPurchaseReport.setTaxAmount((Double) objAray[20]);

			 
			 purchaseReportList.add(importPurchaseReport);
		 }

		return purchaseReportList;
	}
	
	//==================end=====================//
	

	@Override
	public String autoGenerateBatch(String itemid) {
		Random random = new Random();
		Integer x = random.nextInt(100);
		String batch = itemid + x.toString();
		return batch;
	}
	
	
	@Override
	public List<PurchaseReport> getImportPurchaseInvoiceReportByHdrId(String phdrid) {
		List<PurchaseReport> purchaseReportList = new ArrayList();
		List<Object> obj = purchaseHdrRepo.getImportPurchaseInvoiceReportByHdrId(phdrid);
		for(int i=0;i<obj.size();i++)
		 {
			 Object[] objAray = (Object[]) obj.get(i);
			 PurchaseReport importPurchaseReport = new PurchaseReport();
			 importPurchaseReport.setSupplierName((String) objAray[0]);
			 importPurchaseReport.setSupplierAddress((String) objAray[1]);
			 importPurchaseReport.setSupplierEmail((String) objAray[2]);
			 importPurchaseReport.setSupplierGst((String) objAray[3]);
			 importPurchaseReport.setVoucherNumber((String) objAray[4]);
			 importPurchaseReport.setVoucherDate((Date) objAray[5]);
			 importPurchaseReport.setSupplierInvNo((String) objAray[6]);
			 importPurchaseReport.setIncoiceDate((String) objAray[7].toString());
			 importPurchaseReport.setItemCode((String) objAray[8]);
			 importPurchaseReport.setItemName((String) objAray[9]);
			 importPurchaseReport.setBatch((String) objAray[10]);
			 importPurchaseReport.setExpiry((Date) objAray[11]);
			 importPurchaseReport.setQty((Double) objAray[12]);
			 importPurchaseReport.setUnit((String) objAray[17]);
			 importPurchaseReport.setUnitPrice((Double) objAray[13]);
			 importPurchaseReport.setNetCost((Double) objAray[15]);
			 importPurchaseReport.setAmount((Double) objAray[14]);
			 importPurchaseReport.setTaxRate((Double) objAray[16]);
			 
			 importPurchaseReport.setAmountTotal((Double) objAray[18]);
			 importPurchaseReport.setSupplierState((String) objAray[19]);
			 importPurchaseReport.setTaxAmount((Double) objAray[20]);

			 
			 purchaseReportList.add(importPurchaseReport);
		 }

		return purchaseReportList;
	}
	



	@Override
	public Double getTotalAdditionalExpenseByHdrId(String phdrid) {
		Double obj = purchaseHdrRepo.getTotalAdditionalExpenseByHdrIdRepo(phdrid);
			 PurchaseReport totalExpense = new PurchaseReport();
			 totalExpense.setAdditionalExpense((Double) obj);

		return obj;
	}



	@Override
	public Double getTotalImportExpense(String phdrid) {
		Double obj = purchaseHdrRepo.getTotalImportExpenseRepo(phdrid);
		PurchaseReport totalImportExpense = new PurchaseReport();
		totalImportExpense.setImportDuty((Double) obj);
		return obj;
		
	}
}	

