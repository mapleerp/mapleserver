package com.maple.restserver.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.ItemBatchDtlReport;

@Component
public interface YearEndService {

	public  void  YearEnd(String companyMstId);
}
