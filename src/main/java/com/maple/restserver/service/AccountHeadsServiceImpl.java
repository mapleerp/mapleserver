package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

//import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.entity.ProcessMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.report.entity.SaleOrderinoiceReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;
import com.maple.restserver.repository.ProcessMstRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;

@Service
@Transactional
@Component
public class AccountHeadsServiceImpl implements AccountHeadsService {
	@Autowired
	ReceiptModeMstService receiptModeMstService;

	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	@Autowired
	UnitMstRepository unitMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	ProcessMstRepository processMstRepository;
	
	@Autowired
	CurrencyMstRepository currencyMstRepo;

	@Autowired
	VoucherNumberService voucherNumberService;
	
	
	
	@Override
	public List<AccountHeads> getAccountHeads(String pdfFileName) {

		// List<AccountHeadsReport> accountHeadsList = new ArrayList();

		List<AccountHeads> accountHeads = accountHeadsRepository.findAll();

//			 for(AccountHeads i :accountHeads )
//		 {
//			
//				 AccountHeadsReport accountHeadsReport= new AccountHeadsReport();
//				 accountHeadsReport.setAccountName(i.getAccountName());
//				 accountHeadsReport.setGroupOnly(i.getGroupOnly());
//				 accountHeadsReport.setMachineId(i.getMachineId());
//				 accountHeadsReport.setParentId(i.getParentId());
//				 accountHeadsReport.setVoucherType(i.getVoucherType());
//				 accountHeadsList.add(accountHeadsReport);
//		 }

		/*
		 * Create SalesInoiceEntity Entity and pass value from object to Entity
		 */

		return accountHeads;
	}

	@Override
	public List<AccountHeads> findByBankAccount(String companymstid) {

		List<AccountHeads> accontHeadsList = new ArrayList<>();
		List<String> string = accountHeadsRepository.findByBankAccount(companymstid);
		for (int i = 0; i < string.size(); i++) {

			AccountHeads heads = new AccountHeads();
			heads.setId(string.get(0));
			accontHeadsList = accountHeadsRepository.fetchAccountHeads(heads.getId());

		}

		return accontHeadsList;
	}

	@Override
	public String addAccountHeadSalesReturn(String companymstid) {

		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("SALES RETURN", companymstid);
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = CompanyMstOpt.get();
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(companyMst);
			accountHeads.setId("SY00100" + companymstid);
			accountHeads.setAccountName("SALES RETURN");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("N");
			accountHeadsRepository.save(accountHeads);
			return "saved";
		} else {
			return "already saved";
		}
	}

	@Override
	public String addAccountHeadInitialize(String companymstid, String branchcode) {

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);

		AccountHeads discountReceived = accountHeadsRepository.findByAccountNameAndCompanyMstId("DISCOUNT RECEIVED",
				CompanyMstOpt.get().getId());
		if (null == discountReceived) {

			discountReceived = new AccountHeads();
			discountReceived.setCompanyMst(CompanyMstOpt.get());
			discountReceived.setId("SY0053" + CompanyMstOpt.get().getId());
			discountReceived.setAccountName("DISCOUNT RECEIVED");
			discountReceived.setMachineId("MACH");
			discountReceived.setParentId("0");
			discountReceived.setTaxId("0");
			discountReceived.setGroupOnly("N");
			discountReceived = accountHeadsRepository.saveAndFlush(discountReceived);
		}

		AccountHeads assets = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS",
				CompanyMstOpt.get().getId());
		if(null == assets) {
		assets = new AccountHeads();
		assets.setCompanyMst(CompanyMstOpt.get());
		assets.setId("SY001" + CompanyMstOpt.get().getId());
		assets.setAccountName("ASSETS");
		assets.setMachineId("MACH");
		assets.setParentId("0");
		assets.setTaxId("0");
		assets.setGroupOnly("Y");
		assets = accountHeadsRepository.saveAndFlush(assets);
		}

		AccountHeads liability = accountHeadsRepository.findByAccountNameAndCompanyMstId("LIABLITY",
				CompanyMstOpt.get().getId());
		if(null == liability) {
		liability = new AccountHeads();
		liability.setCompanyMst(CompanyMstOpt.get());
		liability.setAccountName("LIABLITY");
		liability.setMachineId("MACH");
		liability.setParentId("0");
		liability.setTaxId("0");
		liability.setGroupOnly("Y");
		liability.setId("SY002" + companymstid);
		liability = accountHeadsRepository.saveAndFlush(liability);
		}
		
		
		AccountHeads income = accountHeadsRepository.findByAccountNameAndCompanyMstId("INCOME",
				CompanyMstOpt.get().getId());
		if(null == income) {
		income = new AccountHeads();
		income.setCompanyMst(CompanyMstOpt.get());
		income.setAccountName("INCOME");
		income.setMachineId("MACH");
		income.setParentId("0");
		income.setTaxId("0");
		income.setGroupOnly("Y");
		income.setId("SY003" + companymstid);
		income = accountHeadsRepository.saveAndFlush(income);
		}
		
		
		AccountHeads expance = accountHeadsRepository.findByAccountNameAndCompanyMstId("EXPENSE",
				CompanyMstOpt.get().getId());
		if(null == expance) {
		expance = new AccountHeads();
		expance.setCompanyMst(CompanyMstOpt.get());
		expance.setAccountName("EXPENSE");
		expance.setMachineId("MACH");
		expance.setParentId("0");
		expance.setTaxId("0");
		expance.setGroupOnly("Y");
		expance.setId("SY004" + companymstid);
		expance = accountHeadsRepository.saveAndFlush(expance);
		}
		

		AccountHeads currentasset = accountHeadsRepository.findByAccountNameAndCompanyMstId("CURRENT ASSETS",
				CompanyMstOpt.get().getId());
		if(null == currentasset) {
		currentasset = new AccountHeads();
		currentasset.setCompanyMst(CompanyMstOpt.get());
		currentasset.setAccountName("CURRENT ASSETS");
		currentasset.setMachineId("MACH");
		currentasset.setParentId("0");
		currentasset.setTaxId("0");
		currentasset.setGroupOnly("Y");
		currentasset.setId("SY005" + companymstid);
		currentasset = accountHeadsRepository.saveAndFlush(currentasset);
		}
		

		AccountHeads cash = accountHeadsRepository.findByAccountNameAndCompanyMstId("CASH",
				CompanyMstOpt.get().getId());
		if(null == cash) {
		cash = new AccountHeads();
		AccountHeads accountHeads = new AccountHeads();
		AccountHeads parentAccountHead = accountHeadsRepository.findByAccountNameAndCompanyMstId("CURRENT ASSETS",
				companymstid);
		cash.setAccountName("CASH");
		cash.setMachineId("MACH");
		cash.setParentId(parentAccountHead.getId());
		cash.setTaxId("0");
		cash.setCompanyMst(CompanyMstOpt.get());
		cash.setGroupOnly("Y");
		cash.setId("SY006" + companymstid);
		cash = accountHeadsRepository.saveAndFlush(cash);
		}

		/*
		 * AccountHeads bankaccount=new AccountHeads();
		 * bankaccount.setCompanyMst(CompanyMstOpt.get());
		 * bankaccount.setAccountName("BANK ACCOUNT"); bankaccount.setMachineId("MACH");
		 * bankaccount.setParentId("0"); bankaccount.setTaxId("0");
		 * bankaccount.setGroupOnly("Y"); bankaccount.setId("SY0024"+companymstid);
		 * accountHeadsRepository.saveAndFlush(bankaccount);
		 */

		AccountHeads sodexo = accountHeadsRepository.findByAccountNameAndCompanyMstId("SODEXO",
				CompanyMstOpt.get().getId());
		if(null == sodexo) {
		sodexo = new AccountHeads();
		sodexo.setCompanyMst(CompanyMstOpt.get());
		sodexo.setAccountName("SODEXO");
		sodexo.setMachineId("MACH");
		AccountHeads parentAccountHead = accountHeadsRepository.findByAccountNameAndCompanyMstId("CURRENT ASSETS", companymstid);
		sodexo.setParentId(parentAccountHead.getId());
		sodexo.setCompanyMst(CompanyMstOpt.get());
		sodexo.setTaxId("0");
		sodexo.setGroupOnly("N");
		sodexo.setId("SY007" + companymstid);
		sodexo = accountHeadsRepository.saveAndFlush(sodexo);
		}
		

		AccountHeads salesaccount = accountHeadsRepository.findByAccountNameAndCompanyMstId("SALES ACCOUNT",
				CompanyMstOpt.get().getId());
		if(null == salesaccount) {
		salesaccount = new AccountHeads();
		salesaccount.setCompanyMst(CompanyMstOpt.get());
		salesaccount.setAccountName("SALES ACCOUNT");
		salesaccount.setMachineId("MACH");
		salesaccount.setParentId("0");
		salesaccount.setTaxId("0");
		salesaccount.setGroupOnly("N");
		salesaccount.setId("SY008" + companymstid);
		salesaccount = accountHeadsRepository.saveAndFlush(salesaccount);
		}

		
		AccountHeads purchaseaccount = accountHeadsRepository.findByAccountNameAndCompanyMstId("PURCHASE ACCOUNTS",
				CompanyMstOpt.get().getId());
		if(null == purchaseaccount) {
		purchaseaccount = new AccountHeads();
		purchaseaccount.setCompanyMst(CompanyMstOpt.get());
		purchaseaccount.setAccountName("PURCHASE ACCOUNTS");
		purchaseaccount.setMachineId("MACH");
		purchaseaccount.setParentId("0");
		purchaseaccount.setTaxId("0");
		purchaseaccount.setGroupOnly("N");
		purchaseaccount.setId("SY009" + companymstid);
		purchaseaccount = accountHeadsRepository.saveAndFlush(purchaseaccount);
		}
		

		AccountHeads dutiesandtaxes = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
				CompanyMstOpt.get().getId());
		if(null == dutiesandtaxes) {
		dutiesandtaxes = new AccountHeads();
		dutiesandtaxes.setCompanyMst(CompanyMstOpt.get());
		dutiesandtaxes.setAccountName("DUTIES AND TAXES");
		dutiesandtaxes.setMachineId("MACH");
		dutiesandtaxes.setParentId("0");
		dutiesandtaxes.setTaxId("0");
		dutiesandtaxes.setGroupOnly("Y");
		dutiesandtaxes.setId("SY0010" + companymstid);
		dutiesandtaxes = accountHeadsRepository.save(dutiesandtaxes);
		}
		
		
		AccountHeads cessaccount = accountHeadsRepository.findByAccountNameAndCompanyMstId("ADD CESS ACCOUNT",
				CompanyMstOpt.get().getId());
		if(null == cessaccount) {
		cessaccount = new AccountHeads();
		cessaccount.setCompanyMst(CompanyMstOpt.get());
		AccountHeads parentAccounts = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
				companymstid);
		cessaccount.setAccountName("ADD CESS ACCOUNT");
		cessaccount.setMachineId("MACH");
		cessaccount.setParentId(parentAccounts.getParentId());
		cessaccount.setTaxId("0");
		cessaccount.setGroupOnly("N");
		cessaccount.setId("SY0011" + companymstid);
		cessaccount = accountHeadsRepository.saveAndFlush(cessaccount);
		}
		
		
		AccountHeads kfc = accountHeadsRepository.findByAccountNameAndCompanyMstId("KERALA FLOOD CESS",
				CompanyMstOpt.get().getId());
		if(null == kfc) {
		kfc = new AccountHeads();
		kfc.setCompanyMst(CompanyMstOpt.get());
		AccountHeads dutyandtaxes = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
				companymstid);
		kfc.setAccountName("KERALA FLOOD CESS");
		kfc.setMachineId("MACH");
		kfc.setParentId(dutyandtaxes.getParentId());
		kfc.setTaxId("0");
		kfc.setGroupOnly("N");
		kfc.setId("SY0012" + companymstid);
		kfc = accountHeadsRepository.saveAndFlush(kfc);
		}
		
		
		AccountHeads salesreturn = accountHeadsRepository.findByAccountNameAndCompanyMstId("SALES RETURN",
				CompanyMstOpt.get().getId());
		if(null == salesreturn) {
		salesreturn = new AccountHeads();
		salesreturn.setCompanyMst(CompanyMstOpt.get());
		salesreturn.setId("SY0013" + companymstid);
		salesreturn.setAccountName("SALES RETURN");
		salesreturn.setMachineId("MACH");
		salesreturn.setParentId("0");
		salesreturn.setTaxId("0");
		salesreturn.setGroupOnly("N");
		salesreturn = accountHeadsRepository.saveAndFlush(salesreturn);
		}
		

		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("INDIRECT EXPENSES", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("INDIRECT EXPENSES");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("EXPENSE", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0014" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("SUSPENSE ACCOUNT", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("SUSPENSE ACCOUNT");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0015" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		AccountHeads accountHeadsSC = accountHeadsRepository.findByAccountNameAndCompanyMstId("SUNDRY CREDITORS",
				companymstid);
		if (null == accountHeadsSC) {
			accountHeadsSC = new AccountHeads();
			accountHeadsSC.setCompanyMst(CompanyMstOpt.get());
			accountHeadsSC.setAccountName("SUNDRY CREDITORS");
			accountHeadsSC.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeadsSC.setParentId(accountCash.getId());

			}

			accountHeadsSC.setTaxId("0");
			accountHeadsSC.setGroupOnly("Y");
			accountHeadsSC.setId("SY0016" + companymstid);
			accountHeadsRepository.saveAndFlush(accountHeadsSC);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("SUNDRY DEBTORS", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("SUNDRY DEBTORS");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0017" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("UNSECURED LOAN", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("UNSECURED LOAN");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0018" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("STOCK IN HAND", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("STOCK IN HAND");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0019" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("SECURED LOAN", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("SECURED LOAN");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("LIABLITY",
					companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0020" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("RETAINED EARNINGS", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("RETAINED EARNINGS");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("LIABLITY",
					companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0021" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("RESERVE AND SURPLUS", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("RESERVE AND SURPLUS");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0022" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("MISCELLANEOUS EXPENSES", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("MISCELLANEOUS EXPENSES");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0023" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CGST ACCOUNT", companymstid);

		// Optional<CompanyMst> CompanyMstOpt =
		// companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("CGST ACCOUNT");
			accountHeads.setMachineId("MACH");

			AccountHeads bankAsset = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
					companymstid);

			if (null != bankAsset) {
				accountHeads.setParentId(bankAsset.getId());
			}
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0024" + companymstid);
			accountHeads = accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("SGST ACCOUNT", companymstid);

		// Optional<CompanyMst> CompanyMstOpt =
		// companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("SGST ACCOUNT");
			accountHeads.setMachineId("MACH");

			AccountHeads bankAsset = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
					companymstid);

			if (null != bankAsset) {
				accountHeads.setParentId(bankAsset.getId());
			}
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0025" + companymstid);
			accountHeads = accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CESS ACCOUNT", companymstid);

		// Optional<CompanyMst> CompanyMstOpt =
		// companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("CESS ACCOUNT");
			accountHeads.setMachineId("MACH");

			AccountHeads bankAsset = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
					companymstid);

			if (null != bankAsset) {
				accountHeads.setParentId(bankAsset.getId());
			}
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0026" + companymstid);
			accountHeads = accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("ADD CESS ACCOUNT", companymstid);

		// Optional<CompanyMst> CompanyMstOpt =
		// companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("ADD CESS ACCOUNT");
			accountHeads.setMachineId("MACH");

			AccountHeads bankAsset = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
					companymstid);

			if (null != bankAsset) {
				accountHeads.setParentId(bankAsset.getId());
			}
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0027" + companymstid);
			accountHeads = accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("KERALA FLOOD CESS", companymstid);

		// Optional<CompanyMst> CompanyMstOpt =
		// companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("KERALA FLOOD CESS");
			accountHeads.setMachineId("MACH");

			AccountHeads bankAsset = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
					companymstid);

			if (null != bankAsset) {
				accountHeads.setParentId(bankAsset.getId());
			}
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0028" + companymstid);
			accountHeads = accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("PROFIT", companymstid);

		// Optional<CompanyMst> CompanyMstOpt =
		// companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("PROFIT");
			accountHeads.setMachineId("MACH");

			AccountHeads bankAsset = accountHeadsRepository.findByAccountNameAndCompanyMstId("LIABLITY", companymstid);

			if (null != bankAsset) {
				accountHeads.setParentId(bankAsset.getId());
			}
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0029" + companymstid);
			accountHeads = accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("LOSS", companymstid);

		// Optional<CompanyMst> CompanyMstOpt =
		// companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("LOSS");
			accountHeads.setMachineId("MACH");

			AccountHeads bankAsset = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != bankAsset) {
				accountHeads.setParentId(bankAsset.getId());
			}
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0030" + companymstid);
			accountHeads = accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("OPENING BALANCE", companymstid);

		if (null == accountHeads) {
			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("OPENING BALANCE");
			accountHeads.setMachineId("MACH");

			AccountHeads bankAsset = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != bankAsset) {
				accountHeads.setParentId(bankAsset.getId());
			}
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0031" + companymstid);
			accountHeads = accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("BANK ACCOUNTS", companymstid);

		// Optional<CompanyMst> CompanyMstOpt =
		// companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("BANK ACCOUNTS");
			accountHeads.setMachineId("MACH");

			AccountHeads bankAsset = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != bankAsset) {
				accountHeads.setParentId(bankAsset.getId());
			}
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0032" + companymstid);
			accountHeads = accountHeadsRepository.save(accountHeads);
		}

		AccountHeads accountHeads1 = new AccountHeads();
		accountHeads1 = accountHeadsRepository.findByAccountNameAndCompanyMstId("BRANCH DIVISION", companymstid);

		if (null == accountHeads1) {
			accountHeads1 = new AccountHeads();

			accountHeads1.setCompanyMst(CompanyMstOpt.get());
			accountHeads1.setAccountName("BRANCH DIVISION");
			accountHeads1.setMachineId("MACH");

			AccountHeads accountHeadsAsset = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS",
					companymstid);

			if (null != accountHeadsAsset) {
				accountHeads1.setParentId(accountHeadsAsset.getId());
			}

			accountHeads1.setTaxId("0");
			accountHeads1.setGroupOnly("Y");
			accountHeads1.setId("SY0034" + companymstid);
			accountHeadsRepository.save(accountHeads1);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CAPITAL ACCOUNTS", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("CAPITAL ACCOUNTS");
			accountHeads.setMachineId("MACH");

			AccountHeads accountCapitalAccounts = accountHeadsRepository.findByAccountNameAndCompanyMstId("INCOME",
					companymstid);

			if (null != accountCapitalAccounts) {
				accountHeads.setParentId(accountCapitalAccounts.getId());
			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0035" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CASH", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("CASH");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {
				accountHeads.setParentId(accountCash.getId());
			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0036" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CASH IN HAND", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("CASH IN HAND");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");

			accountHeads.setId("SY0037" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CURRENT ASSESTS", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("CURRENT ASSESTS");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0038" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CURRENT LIABLITY", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("CURRENT LIABLITY");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("LIABLITY",
					companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0039" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("DEPOSITS", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("DEPOSITS");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0040" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("DIRECT EXPENSES", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("DIRECT EXPENSES");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("EXPENSE", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0041" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		/*
		 * accountHeads = new AccountHeads(); accountHeads =
		 * accountHeadsRepository.findByAccountNameAndCompanyMstId("DIRECT INCOME",
		 * companymstid); if (null == accountHeads) { accountHeads = new AccountHeads();
		 * accountHeads.setCompanyMst(CompanyMstOpt.get());
		 * accountHeads.setAccountName("DIRECT INCOME");
		 * accountHeads.setMachineId("MACH"); AccountHeads accountCash =
		 * accountHeadsRepository.findByAccountNameAndCompanyMstId("INCOME",
		 * companymstid);
		 * 
		 * if (null != accountCash) {
		 * 
		 * accountHeads.setParentId(accountCash.getId());
		 * 
		 * }
		 * 
		 * accountHeads.setTaxId("0"); accountHeads.setGroupOnly("Y");
		 * accountHeads.setId("SYBR008" + companymstid);
		 * accountHeadsRepository.save(accountHeads); }
		 */

		/*
		 * accountHeads = new AccountHeads(); accountHeads =
		 * accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
		 * companymstid); if (null == accountHeads) { accountHeads = new AccountHeads();
		 * accountHeads.setCompanyMst(CompanyMstOpt.get());
		 * accountHeads.setAccountName("DUTIES AND TAXES");
		 * accountHeads.setMachineId("MACH"); AccountHeads accountCash =
		 * accountHeadsRepository.findByAccountNameAndCompanyMstId("EXPENSE",
		 * companymstid);
		 * 
		 * if (null != accountCash) {
		 * 
		 * accountHeads.setParentId(accountCash.getId());
		 * 
		 * }
		 * 
		 * accountHeads.setTaxId("0"); accountHeads.setGroupOnly("Y");
		 * accountHeads.setId("SY017" + companymstid);
		 * accountHeadsRepository.save(accountHeads); }
		 */

		/*
		 * accountHeads = new AccountHeads(); accountHeads =
		 * accountHeadsRepository.findByAccountNameAndCompanyMstId("DIRECT EXPENSES",
		 * companymstid); if (null == accountHeads) { accountHeads = new AccountHeads();
		 * accountHeads.setCompanyMst(CompanyMstOpt.get());
		 * accountHeads.setAccountName("DIRECT EXPENSES");
		 * accountHeads.setMachineId("MACH"); AccountHeads accountCash =
		 * accountHeadsRepository.findByAccountNameAndCompanyMstId("EXPENSE",
		 * companymstid);
		 * 
		 * if (null != accountCash) {
		 * 
		 * accountHeads.setParentId(accountCash.getId());
		 * 
		 * }
		 * 
		 * accountHeads.setTaxId("0"); accountHeads.setGroupOnly("Y");
		 * accountHeads.setId("SY0024" + companymstid);
		 * accountHeadsRepository.save(accountHeads); }
		 */

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("DIRECT INCOME", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("DIRECT INCOME");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("INCOME", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0042" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}
		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("INDIRECT INCOME", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("INDIRECT INCOME");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("INCOME", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0043" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}
		/*
		 * accountHeads = new AccountHeads(); accountHeads =
		 * accountHeadsRepository.findByAccountNameAndCompanyMstId("INDIRECT EXPENSES",
		 * companymstid); if (null == accountHeads) { accountHeads = new AccountHeads();
		 * accountHeads.setCompanyMst(CompanyMstOpt.get());
		 * accountHeads.setAccountName("INDIRECT EXPENSES");
		 * accountHeads.setMachineId("MACH"); AccountHeads accountCash =
		 * accountHeadsRepository.findByAccountNameAndCompanyMstId("EXPENSE",
		 * companymstid);
		 * 
		 * if (null != accountCash) {
		 * 
		 * accountHeads.setParentId(accountCash.getId());
		 * 
		 * }
		 * 
		 * accountHeads.setTaxId("0"); accountHeads.setGroupOnly("Y");
		 * accountHeads.setId("SY0024" + companymstid);
		 * accountHeadsRepository.save(accountHeads); }
		 */
		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("INVERSTMENTS", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("INVERSTMENTS");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0044" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}
		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("LOAN AND ADVANCES", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("LOAN AND ADVANCES");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0045" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}
		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("LOAN", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("LOAN");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("LIABLITY",
					companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0046" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("HEAD ASSETS", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("HEAD ASSETS");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0047" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("PROVISIONS", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("PROVISIONS");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0048" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("IMPORT DUTY", companymstid);
		if (null == accountHeads) {
			accountHeads = new AccountHeads();
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("IMPORT DUTY");
			accountHeads.setMachineId("MACH");
			AccountHeads accountCash = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

			if (null != accountCash) {

				accountHeads.setParentId(accountCash.getId());

			}

			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0052" + companymstid);
			accountHeadsRepository.save(accountHeads);
		}

		addAccountHeadBranchCash(companymstid, branchcode);
		addAccountHeadroundoff(companymstid, branchcode);
		addAccountHeadPettyCash(companymstid, branchcode);

		receiptModeMstService.initializeNewReceiptMode(companymstid, branchcode);

		return "saved";
	}

	@Override
	public String addAccountHeadAsset(String companymstid) {

		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("ASSETS", companymstid);

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setId("SY001" + CompanyMstOpt.get().getId());
			accountHeads.setAccountName("ASSETS");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeadsRepository.save(accountHeads);
			return "Account heads saved";
		} else {
			return "account heads already added";
		}
	}

	@Override
	public String addAccountHeadLiability(String companymstid) {
		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("LIABLITY", companymstid);

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("LIABLITY");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY002" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "Account heads saved";
		} else {
			return "account heads already added";
		}

	}

	@Override
	public String addAccountHeadIncome(String companymstid) {

		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("INCOME", companymstid);

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("INCOME");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY003" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "Account heads saved";
		} else {
			return "account heads already added";
		}
	}

	@Override
	public String addAccountHeadExpance(String companymstid) {

		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("EXPENSE", companymstid);

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("EXPENSE");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY004" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "Accoount heads saved";
		} else {
			return "account heads already added";
		}
	}

	@Override
	public String addAccountHeadCurrentAssets(String companymstid) {
		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CURRENT ASSETS", companymstid);

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("CURRENT ASSETS");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY005" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "Accoount heads saved";
		} else {
			return "account heads already added";
		}
	}

	@Override
	public String addAccountHeadCash(String companymstid) {

		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CURRENT ASSETS", companymstid);

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null != accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads = new AccountHeads();
			accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CASH", companymstid);
			if (null == accountHeads) {
				accountHeads.setAccountName("CASH");
				accountHeads.setMachineId("MACH");
				accountHeads.setParentId(accountHeads.getId());
				accountHeads.setTaxId("0");
				accountHeads.setGroupOnly("Y");
				accountHeads.setId("SY006" + companymstid);
				accountHeadsRepository.save(accountHeads);
				return "Account heads added";

			} else {
				return "account heads already added";

			}

		}
		return "account heads already added";
	}

	@Override
	public String addAccountHeadBankAccount(String companymstid) {
		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("BANK ACCOUNTS", companymstid);
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("BANK ACCOUNT");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY0024" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "saved";
		} else {
			return "already saved";
		}
	}

	@Override
	public String addAccountHeadSodexo(String companymstid) {
		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("SODEXO", companymstid);
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("SODEXO");
			accountHeads.setMachineId("MACH");
			AccountHeads parentAccountHead = accountHeadsRepository.findByAccountNameAndCompanyMstId("CURRENT ASSETS",
					companymstid);
			accountHeads.setParentId(parentAccountHead.getId());
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("N");
			accountHeads.setId("SY011" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "Saved";
		} else {
			return "already Saved";
		}
	}

	@Override
	public String addAccountHeadSalesAccount(String companymstid) {
		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("SALES ACCOUNT", companymstid);
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("SALES ACCOUNT");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("N");
			accountHeads.setId("SY015" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "saved";
		} else {
			return "already saved";
		}
	}

	@Override
	public String addAccountHeadPurchaseAccount(String companymstid) {
		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("PURCHASE ACCOUNT", companymstid);
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("PURCHASE ACCOUNTS");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("N");
			accountHeads.setId("SY016" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "saved";
		} else {
			return "already saved";
		}
	}

	@Override
	public String addAccountHeadDutiesandtaxes(String companymstid) {
		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES", companymstid);
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("DUTIES AND TAXES");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId("0");
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("Y");
			accountHeads.setId("SY017" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "saved";
		} else {
			return "already saved";
		}
	}

	@Override
	public String addAccountHeadCgstAccount(String companymstid) {
		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CGST ACCOUNT", companymstid);
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			accountHeads.setAccountName("CGST ACCOUNT");
			accountHeads.setMachineId("MACH");
			AccountHeads parentAccountHead = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
					companymstid);
			accountHeads.setParentId(parentAccountHead.getId());
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("N");
			accountHeads.setId("SY018" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "saved";
		} else {
			return "already saved";
		}
	}

	@Override
	public String addAccountHeadsgstAccount(String companymstid) {
		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("SGST ACCOUNT", companymstid);
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			AccountHeads parentAccountHead = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
					companymstid);
			accountHeads = new AccountHeads();
			accountHeads.setAccountName("SGST ACCOUNT");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId(parentAccountHead.getParentId());
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("N");
			accountHeads.setParentId(parentAccountHead.getId());
			accountHeads.setId("SY019" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "saved";
		}

		else {
			return "already saved";
		}
	}

	@Override
	public String addAccountHeadCessAccount(String companymstid) {
		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("ADD CESS ACCOUNT", companymstid);
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			AccountHeads parentAccountHead = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
					companymstid);
			accountHeads.setAccountName("ADD CESS ACCOUNT");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId(parentAccountHead.getParentId());
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("N");
			accountHeads.setId("SY021" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "saved";
		} else {
			return "already saved";
		}
	}

	@Override
	public String addAccountHeadKeralaFloodCess(String companymstid) {
		AccountHeads accountHeads = new AccountHeads();
		accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("KERALA FLOOD CESS", companymstid);
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		if (null == accountHeads) {
			accountHeads.setCompanyMst(CompanyMstOpt.get());
			AccountHeads parentAccountHead = accountHeadsRepository.findByAccountNameAndCompanyMstId("DUTIES AND TAXES",
					companymstid);
			accountHeads = new AccountHeads();
			accountHeads.setAccountName("KERALA FLOOD CESS");
			accountHeads.setMachineId("MACH");
			accountHeads.setParentId(parentAccountHead.getParentId());
			accountHeads.setTaxId("0");
			accountHeads.setGroupOnly("N");
			accountHeads.setId("SY022" + companymstid);
			accountHeadsRepository.save(accountHeads);
			return "saved";
		} else {
			return "already saved";
		}
	}

	@Override
	public String unitCreation(String companymstid) {
		UnitMst nos = unitMstRepository.findByUnitNameAndCompanyMstId("NOS", companymstid);
		if (null == nos) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("NUMBERS");
			unitMsts.setUnitName("NOS");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU001" + companymstid);
			unitMstRepository.saveAndFlush(unitMsts);
		}
		UnitMst pkt = unitMstRepository.findByUnitNameAndCompanyMstId("PKT", companymstid);
		if (null == pkt) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("PACKETS");
			unitMsts.setUnitName("PKT");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU002" + companymstid);
			unitMstRepository.saveAndFlush(unitMsts);
		}
		UnitMst kgs = unitMstRepository.findByUnitNameAndCompanyMstId("KGS", companymstid);
		if (null == kgs) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("KILOGRAMS");
			unitMsts.setUnitName("KGS");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU003" + companymstid);
			unitMstRepository.saveAndFlush(unitMsts);
		}
		UnitMst gms = unitMstRepository.findByUnitNameAndCompanyMstId("GMS", companymstid);
		if (null == gms) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("GRAMS");
			unitMsts.setUnitName("GMS");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU004" + companymstid);
			unitMstRepository.saveAndFlush(unitMsts);
		}
		UnitMst ml = unitMstRepository.findByUnitNameAndCompanyMstId("ML", companymstid);
		if (null == ml) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("MILLILITRE");
			unitMsts.setUnitName("ML");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU005" + companymstid);
			unitMstRepository.saveAndFlush(unitMsts);
		}
		UnitMst ltrs = unitMstRepository.findByUnitNameAndCompanyMstId("LTRS", companymstid);
		if (null == ltrs) {
			UnitMst unitMsts = new UnitMst();
			unitMsts.setUnitDescription("LITRE");
			unitMsts.setUnitName("LTRS");
			unitMsts.setUnitPrecision(0.0);
			unitMsts.setId("SYU006" + companymstid);
			unitMstRepository.saveAndFlush(unitMsts);
		}
		return "saved";
	}

	@Override
	public String addAccountHeadPettyCash(String companymstid, String branchCode) {

		AccountHeads parentAccountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CASH", companymstid);

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		AccountHeads accountHeads = new AccountHeads();

		accountHeads.setAccountName(branchCode + "-PETTY CASH");
		accountHeads.setMachineId("MACH");
		accountHeads.setParentId(parentAccountHeads.getId());
		accountHeads.setTaxId("0");
		accountHeads.setCompanyMst(CompanyMstOpt.get());

		accountHeads.setGroupOnly("N");
		accountHeads.setId("SY0051" + CompanyMstOpt.get().getCompanyName() + branchCode);
		accountHeadsRepository.save(accountHeads);

		return "saved";
	}

	@Override
	public String addAccountHeadBranchCash(String companymstid, String branchCode) {

		AccountHeads parentAccountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CASH", companymstid);
		String AccountNAme = branchCode + "-CASH";

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		AccountHeads accountHeads = new AccountHeads();
		accountHeads.setCompanyMst(CompanyMstOpt.get());
		accountHeads.setAccountName(AccountNAme);
		accountHeads.setMachineId("MACH");
		accountHeads.setParentId(parentAccountHeads.getId());
		accountHeads.setTaxId("0");
		accountHeads.setGroupOnly("N");
		accountHeads.setId("SY0049" + CompanyMstOpt.get().getCompanyName() + branchCode);

		accountHeadsRepository.save(accountHeads);
		return "saved";
	}

	@Override
	public String addAccountHeadroundoff(String companymstid, String branchcode) {
		AccountHeads parentAccountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId("CASH", companymstid);

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		AccountHeads accountHeads = new AccountHeads();
		accountHeads.setAccountName("ROUNDOFF");
		accountHeads.setMachineId("MACH");
		accountHeads.setCompanyMst(CompanyMstOpt.get());

		accountHeads.setParentId(accountHeads.getId());
		accountHeads.setTaxId("0");
		accountHeads.setGroupOnly("N");
		accountHeads.setId("SY0050" + CompanyMstOpt.get().getCompanyName() + branchcode);
		accountHeadsRepository.save(accountHeads);

		return "saved";
	}

	@Override
	public String addProcessPermissionReports(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC001" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Reports and Configuration");
		processMst.setProcessName("REPORTS");
		processMstRepository.saveAndFlush(processMst);
		return "saved ";
	}

	@Override
	public String addProcessPermissionKotPos(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC002" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Kitchen Order POS");
		processMst.setProcessName("KOT POS");
		processMstRepository.saveAndFlush(processMst);
		return "saved ";

	}

	@Override
	public String addProcessDamageEntry(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC003" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Damage Entry");
		processMst.setProcessName("DAMAGE ENTRY");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessWholeSale(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC004" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Whole Sale Entry");
		processMst.setProcessName("WHOLESALE");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPosWindow(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC005" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("POS Window entry");
		processMst.setProcessName("POS WINDOW");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessOnlineSales(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC006" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Online Sales entry");
		processMst.setProcessName("ONLINE SALES");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessReorder(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC007" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Re Order Entry");
		processMst.setProcessName("ReORDER");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessSaleOrder(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC008" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Sale Order Entry");
		processMst.setProcessName("SALEORDER");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessItemMaster(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC009" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Item Creation Window");
		processMst.setProcessName("ITEM MASTER");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessAccountHeads(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0010" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Account Heds Creation and initialization");
		processMst.setProcessName("ACCOUNT HEADS");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessProductConversion(String companymstid) {

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0011" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Convert Product into another");
		processMst.setProcessName("PRODUCT CONVERSION");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessReceiptWindow(String companymstid) {

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0012" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Reciept Entry");
		processMst.setProcessName("RECEIPT WINDOW");
		processMstRepository.saveAndFlush(processMst);
		return "saved";

	}

	@Override
	public String addProcessDayEndClossure(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0013" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Day End Entry");
		processMst.setProcessName("DAY END CLOSURE");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPurchase(String companymstid) {

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0014" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Purchase window Entry");
		processMst.setProcessName("PURCHASE");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessCustomer(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0015" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Customer Entry");
		processMst.setProcessName("CUSTOMER");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessAcceptStock(String companymstid) {

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0016" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Accept Stock Entry");
		processMst.setProcessName("ACCEPT STOCK");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessAddSupplier(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0017" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("New Supplier Entry");
		processMst.setProcessName("ADD SUPPLIER");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPayment(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0018" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Payment Entry");
		processMst.setProcessName("PAYMENT");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessTaskList(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0019" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Task List");
		processMst.setProcessName("TASK LIST");
		processMstRepository.saveAndFlush(processMst);
		return "saved";

	}

	@Override
	public String addProcessMultyUnit(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0020" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Multi Unit Entry");
		processMst.setProcessName("MULTI UNIT");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessStockTransfer(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0021" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Stock Transfer Out");
		processMst.setProcessName("STOCK TRANSFER");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessIntentent(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0022" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Request Intent");
		processMst.setProcessName("INTENT");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessProductionPlanning(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0023" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Production Planning");
		processMst.setProcessName("PRODUCTION PLANNING");
		processMstRepository.saveAndFlush(processMst);
		return "saved";

	}

	@Override
	public String addProcessJournal(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0024" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Journal Entry Window");
		processMst.setProcessName("JOURNAL");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessSessionEnd(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0025" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Session End Entry Window");
		processMst.setProcessName("SESSION END");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPhysicalStock(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0026" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Physical Stock Entry Window");
		processMst.setProcessName("PHYSICAL STOCK");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessTaxCreation(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0027" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Tax Creation Entry Window");
		processMst.setProcessName("TAX CREATION");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessKitDefinition(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0028" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Kit details Entry Window");
		processMst.setProcessName("KIT DEFINITION");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPriceDefinition(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0029" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Price Definition details Entry Window");
		processMst.setProcessName("PRICE DEFINITION");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessKotManger(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0030" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Kitchen Order Table Entry");
		processMst.setProcessName("KOT MANAGER");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessUserRegistration(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0031" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("New User Creation");
		processMst.setProcessName("USER REGISTRATION");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessProcessCreation(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0032" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("New Process Creation");
		processMst.setProcessName("PROCESS CREATION");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessChangePasswordf(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0033" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("New Process Creation");
		processMst.setProcessName("CHANGE PASSWORD");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessBranchCreation(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0034" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("New Branch Entry");
		processMst.setProcessName("BRANCH CREATION");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessCompanyCreation(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst30 = new ProcessMst();
		processMst30.setId("PRC0035" + CompanyMstOpt.get().getCompanyName());
		processMst30.setProcessDescription("New Company Creation");
		processMst30.setProcessName("COMPANY CREATION");
		return "saved";
	}

	@Override
	public String addProcessVoucherReprint(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0036" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Print with Voucher Number and Date");
		processMst.setProcessName("VOUCHER REPRINT");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessScheme(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0037" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("New Scheme Entry");
		processMst.setProcessName("SCHEME");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessSchemeDetails(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0038" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("New Scheme Details Entry");
		processMst.setProcessName("SCHEME DETAILS");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermission(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0039" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Process Permission Entry");
		processMst.setProcessName("PROCESS PERMISSION");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionGroupPermission(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0040" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Process Permission Entry");
		processMst.setProcessName("GROUP PERMISSION");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionGroupCreation(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0041" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Group Creation Entry Window");
		processMst.setProcessName("GROUP CREATION");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionCustomer(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0042" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Customer Reports");
		processMst.setProcessName("Customer");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionSalesReport(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0043" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Sales Report Window");
		processMst.setProcessName("Sales Report");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionMopnthalySummaryReport(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0044" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Monthly Summary Report Window");
		processMst.setProcessName("MONTHLY SUMMARY");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionSaleOrderReport(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0045" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Sale Order Report Window");
		processMst.setProcessName("SALEORDER REPORT");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionAccountBalance(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0046" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Account Balance Report Window");
		processMst.setProcessName("ACCOUNT BALANCE");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionMonthlyStockReport(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0047" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Monthly Stock Report Window");
		processMst.setProcessName("MONTHLY STOCK REPORT");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionsSalesReport(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0048" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Sales Report Window");
		processMst.setProcessName("SALES REPORT");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionsAccounts(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0049" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Receipts and Payments Windows");
		processMst.setProcessName("ACCOUNTS");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionsSupplierPayment(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0050" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Supplier Payments Window");
		processMst.setProcessName("SUPPLIER PAYMENTS");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionsOwnAccountSettlement(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0051" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("For Settlement of Receipts and Payments against invoice");
		processMst.setProcessName("OWN ACCOUNT SETTLEMENT");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionsActualProduction(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0052" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("For the Actual production Entry");
		processMst.setProcessName("ACTUAL PRODUCTION");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionsSalesReturn(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0053" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("For Whole Sale Return");
		processMst.setProcessName("SALES RETURN");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionSalesFromOrder(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0054" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("For converting sale order to sales");
		processMst.setProcessName("SALES FROM ORDER");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionSaleorderEdit(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0055" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("For editing sale orders");
		processMst.setProcessName("SALES ORDER EDIT");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionConfigurations(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0056" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("For Configure Software");
		processMst.setProcessName("CONFIGURATIONS");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionReceiptMode(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0057" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("For Adding Payment Mode");
		processMst.setProcessName("RECEIPT MODE");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissionVouchertype(String companymstid) {
		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0058" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("For Adding Voucher Type");
		processMst.setProcessName("VOUCHER TYPE");
		processMstRepository.saveAndFlush(processMst);
		return "saved";
	}

	@Override
	public String addProcessPermissions(String companymstid) {

		Optional<CompanyMst> CompanyMstOpt = companyMstRepository.findById(companymstid);
		ProcessMst processMst1 = new ProcessMst();
		processMst1.setId("PRC001" + CompanyMstOpt.get().getCompanyName());
		processMst1.setProcessDescription("Reports and Configuration");
		processMst1.setProcessName("REPORTS");
		processMstRepository.saveAndFlush(processMst1);

		ProcessMst processMst2 = new ProcessMst();
		processMst2.setId("PRC002" + CompanyMstOpt.get().getCompanyName());
		processMst2.setProcessDescription("Kitchen Order POS");
		processMst2.setProcessName("KOT POS");
		processMstRepository.saveAndFlush(processMst2);

		ProcessMst processMst3 = new ProcessMst();
		processMst3.setId("PRC003" + CompanyMstOpt.get().getCompanyName());
		processMst3.setProcessDescription("Damage Entry");
		processMst3.setProcessName("DAMAGE ENTRY");
		processMstRepository.saveAndFlush(processMst3);

		ProcessMst processMst4 = new ProcessMst();
		processMst4.setId("PRC004" + CompanyMstOpt.get().getCompanyName());
		processMst4.setProcessDescription("Whole Sale Entry");
		processMst4.setProcessName("WHOLESALE");
		processMstRepository.saveAndFlush(processMst4);

		ProcessMst processMst5 = new ProcessMst();
		processMst5.setId("PRC005" + CompanyMstOpt.get().getCompanyName());
		processMst5.setProcessDescription("POS Window entry");
		processMst5.setProcessName("POS WINDOW");
		processMstRepository.saveAndFlush(processMst5);

		ProcessMst processMst6 = new ProcessMst();
		processMst6.setId("PRC006" + CompanyMstOpt.get().getCompanyName());
		processMst6.setProcessDescription("Online Sales entry");
		processMst6.setProcessName("ONLINE SALES");
		processMstRepository.saveAndFlush(processMst6);

		ProcessMst processMst7 = new ProcessMst();
		processMst7.setId("PRC007" + CompanyMstOpt.get().getCompanyName());
		processMst7.setProcessDescription("Re Order Entry");
		processMst7.setProcessName("ReORDER");
		processMstRepository.saveAndFlush(processMst7);

		ProcessMst processMst8 = new ProcessMst();
		processMst8.setId("PRC008" + CompanyMstOpt.get().getCompanyName());
		processMst8.setProcessDescription("Sale Order Entry");
		processMst8.setProcessName("SALEORDER");
		processMstRepository.saveAndFlush(processMst8);

		ProcessMst processMst9 = new ProcessMst();
		processMst9.setId("PRC009" + CompanyMstOpt.get().getCompanyName());
		processMst9.setProcessDescription("Item Creation Window");
		processMst9.setProcessName("ITEM MASTER");
		processMstRepository.saveAndFlush(processMst9);

		ProcessMst processMst10 = new ProcessMst();
		processMst10.setId("PRC0010" + CompanyMstOpt.get().getCompanyName());
		processMst10.setProcessDescription("Account Heds Creation and initialization");
		processMst10.setProcessName("ACCOUNT HEADS");
		processMstRepository.saveAndFlush(processMst10);

		ProcessMst processMst11 = new ProcessMst();
		processMst11.setId("PRC0011" + CompanyMstOpt.get().getCompanyName());
		processMst11.setProcessDescription("Convert Product into another");
		processMst11.setProcessName("PRODUCT CONVERSION");
		processMstRepository.saveAndFlush(processMst11);

		ProcessMst processMst12 = new ProcessMst();
		processMst12.setId("PRC0012" + CompanyMstOpt.get().getCompanyName());
		processMst12.setProcessDescription("Reciept Entry");
		processMst12.setProcessName("RECEIPT WINDOW");
		processMstRepository.saveAndFlush(processMst12);

		ProcessMst processMst13 = new ProcessMst();
		processMst13.setId("PRC0013" + CompanyMstOpt.get().getCompanyName());
		processMst13.setProcessDescription("Day End Entry");
		processMst13.setProcessName("DAY END CLOSURE");
		processMstRepository.saveAndFlush(processMst13);

		ProcessMst processMst14 = new ProcessMst();
		processMst14.setId("PRC0014" + CompanyMstOpt.get().getCompanyName());
		processMst14.setProcessDescription("Purchase window Entry");
		processMst14.setProcessName("PURCHASE");
		processMstRepository.saveAndFlush(processMst14);

		ProcessMst processMst15 = new ProcessMst();
		processMst15.setId("PRC0015" + CompanyMstOpt.get().getCompanyName());
		processMst15.setProcessDescription("Customer Entry");
		processMst15.setProcessName("CUSTOMER");
		processMstRepository.saveAndFlush(processMst15);

		ProcessMst processMst16 = new ProcessMst();
		processMst16.setId("PRC0016" + CompanyMstOpt.get().getCompanyName());
		processMst16.setProcessDescription("Accept Stock Entry");
		processMst16.setProcessName("ACCEPT STOCK");
		processMstRepository.saveAndFlush(processMst16);

		ProcessMst processMst17 = new ProcessMst();
		processMst17.setId("PRC0018" + CompanyMstOpt.get().getCompanyName());
		processMst17.setProcessDescription("Payment Entry");
		processMst17.setProcessName("PAYMENT");
		processMstRepository.saveAndFlush(processMst17);

		ProcessMst processMst19 = new ProcessMst();
		processMst19.setId("PRC0019" + CompanyMstOpt.get().getCompanyName());
		processMst19.setProcessDescription("Task List");
		processMst19.setProcessName("TASK LIST");
		processMstRepository.saveAndFlush(processMst19);

		ProcessMst processMst20 = new ProcessMst();
		processMst20.setId("PRC0020" + CompanyMstOpt.get().getCompanyName());
		processMst20.setProcessDescription("Multi Unit Entry");
		processMst20.setProcessName("MULTI UNIT");
		processMstRepository.saveAndFlush(processMst20);

		ProcessMst processMst21 = new ProcessMst();
		processMst21.setId("PRC0021" + CompanyMstOpt.get().getCompanyName());
		processMst21.setProcessDescription("Stock Transfer Out");
		processMst21.setProcessName("STOCK TRANSFER");
		processMstRepository.saveAndFlush(processMst21);

		ProcessMst processMst22 = new ProcessMst();
		processMst22.setId("PRC0022" + CompanyMstOpt.get().getCompanyName());
		processMst22.setProcessDescription("Request Intent");
		processMst22.setProcessName("INTENT");
		processMstRepository.saveAndFlush(processMst22);

		ProcessMst processMst23 = new ProcessMst();
		processMst23.setId("PRC0023" + CompanyMstOpt.get().getCompanyName());
		processMst23.setProcessDescription("Production Planning");
		processMst23.setProcessName("PRODUCTION PLANNING");
		processMstRepository.saveAndFlush(processMst23);

		ProcessMst processMst24 = new ProcessMst();
		processMst24.setId("PRC0024" + CompanyMstOpt.get().getCompanyName());
		processMst24.setProcessDescription("Journal Entry Window");
		processMst24.setProcessName("JOURNAL");
		processMstRepository.saveAndFlush(processMst24);

		ProcessMst processMst25 = new ProcessMst();
		processMst25.setId("PRC0025" + CompanyMstOpt.get().getCompanyName());
		processMst25.setProcessDescription("Session End Entry Window");
		processMst25.setProcessName("SESSION END");
		processMstRepository.saveAndFlush(processMst25);

		ProcessMst processMst26 = new ProcessMst();
		processMst26.setId("PRC0026" + CompanyMstOpt.get().getCompanyName());
		processMst26.setProcessDescription("Physical Stock Entry Window");
		processMst26.setProcessName("PHYSICAL STOCK");
		processMstRepository.saveAndFlush(processMst26);

		ProcessMst processMst27 = new ProcessMst();
		processMst27.setId("PRC0027" + CompanyMstOpt.get().getCompanyName());
		processMst27.setProcessDescription("Tax Creation Entry Window");
		processMst27.setProcessName("TAX CREATION");
		processMstRepository.saveAndFlush(processMst27);

		ProcessMst processMst28 = new ProcessMst();
		processMst28.setId("PRC0028" + CompanyMstOpt.get().getCompanyName());
		processMst28.setProcessDescription("Kit details Entry Window");
		processMst28.setProcessName("KIT DEFINITION");
		processMstRepository.saveAndFlush(processMst28);

		ProcessMst processMst29 = new ProcessMst();
		processMst29.setId("PRC0029" + CompanyMstOpt.get().getCompanyName());
		processMst29.setProcessDescription("Price Definition details Entry Window");
		processMst29.setProcessName("PRICE DEFINITION");
		processMstRepository.saveAndFlush(processMst29);

		ProcessMst processMst30 = new ProcessMst();
		processMst30.setId("PRC0030" + CompanyMstOpt.get().getCompanyName());
		processMst30.setProcessDescription("Kitchen Order Table Entry");
		processMst30.setProcessName("KOT MANAGER");
		processMstRepository.saveAndFlush(processMst30);

		ProcessMst processMst31 = new ProcessMst();
		processMst31.setId("PRC0031" + CompanyMstOpt.get().getCompanyName());
		processMst31.setProcessDescription("New User Creation");
		processMst31.setProcessName("USER REGISTRATION");
		processMstRepository.saveAndFlush(processMst31);

		ProcessMst processMst32 = new ProcessMst();
		processMst32.setId("PRC0032" + CompanyMstOpt.get().getCompanyName());
		processMst32.setProcessDescription("New Process Creation");
		processMst32.setProcessName("PROCESS CREATION");
		processMstRepository.saveAndFlush(processMst32);

		ProcessMst processMst33 = new ProcessMst();
		processMst33.setId("PRC0033" + CompanyMstOpt.get().getCompanyName());
		processMst33.setProcessDescription("New Process Creation");
		processMst33.setProcessName("CHANGE PASSWORD");
		processMstRepository.saveAndFlush(processMst33);

		ProcessMst processMst34 = new ProcessMst();
		processMst34.setId("PRC0034" + CompanyMstOpt.get().getCompanyName());
		processMst34.setProcessDescription("New Branch Entry");
		processMst34.setProcessName("BRANCH CREATION");
		processMstRepository.saveAndFlush(processMst34);

		ProcessMst processMst35 = new ProcessMst();
		processMst35.setId("PRC0035" + CompanyMstOpt.get().getCompanyName());
		processMst35.setProcessDescription("New Company Creation");
		processMst35.setProcessName("COMPANY CREATION");

		ProcessMst processMst36 = new ProcessMst();
		processMst36.setId("PRC0036" + CompanyMstOpt.get().getCompanyName());
		processMst36.setProcessDescription("Print with Voucher Number and Date");
		processMst36.setProcessName("VOUCHER REPRINT");
		processMstRepository.saveAndFlush(processMst36);

		ProcessMst processMst37 = new ProcessMst();
		processMst37.setId("PRC0037" + CompanyMstOpt.get().getCompanyName());
		processMst37.setProcessDescription("New Scheme Entry");
		processMst37.setProcessName("SCHEME");
		processMstRepository.saveAndFlush(processMst37);

		ProcessMst processMst38 = new ProcessMst();
		processMst38.setId("PRC0038" + CompanyMstOpt.get().getCompanyName());
		processMst38.setProcessDescription("New Scheme Details Entry");
		processMst38.setProcessName("SCHEME DETAILS");
		processMstRepository.saveAndFlush(processMst38);

		ProcessMst processMst39 = new ProcessMst();
		processMst39.setId("PRC0039" + CompanyMstOpt.get().getCompanyName());
		processMst39.setProcessDescription("Process Permission Entry");
		processMst39.setProcessName("PROCESS PERMISSION");
		processMstRepository.saveAndFlush(processMst39);

		ProcessMst processMst40 = new ProcessMst();
		processMst40.setId("PRC0040" + CompanyMstOpt.get().getCompanyName());
		processMst40.setProcessDescription("Process Permission Entry");
		processMst40.setProcessName("GROUP PERMISSION");
		processMstRepository.saveAndFlush(processMst40);

		ProcessMst processMst41 = new ProcessMst();
		processMst41.setId("PRC0041" + CompanyMstOpt.get().getCompanyName());
		processMst41.setProcessDescription("Group Creation Entry Window");
		processMst41.setProcessName("GROUP CREATION");
		processMstRepository.saveAndFlush(processMst41);

		ProcessMst processMst42 = new ProcessMst();
		processMst42.setId("PRC0042" + CompanyMstOpt.get().getCompanyName());
		processMst42.setProcessDescription("Customer Reports");
		processMst42.setProcessName("Customer");
		processMstRepository.saveAndFlush(processMst42);

		ProcessMst processMst43 = new ProcessMst();
		processMst43.setId("PRC0043" + CompanyMstOpt.get().getCompanyName());
		processMst43.setProcessDescription("Sales Report Window");
		processMst43.setProcessName("Sales Report");
		processMstRepository.saveAndFlush(processMst43);

		ProcessMst processMst44 = new ProcessMst();
		processMst44.setId("PRC0044" + CompanyMstOpt.get().getCompanyName());
		processMst44.setProcessDescription("Monthly Summary Report Window");
		processMst44.setProcessName("MONTHLY SUMMARY");
		processMstRepository.saveAndFlush(processMst44);

		ProcessMst processMst = new ProcessMst();
		processMst.setId("PRC0045" + CompanyMstOpt.get().getCompanyName());
		processMst.setProcessDescription("Sale Order Report Window");
		processMst.setProcessName("SALEORDER REPORT");
		processMstRepository.saveAndFlush(processMst);

		ProcessMst processMst46 = new ProcessMst();
		processMst46.setId("PRC0046" + CompanyMstOpt.get().getCompanyName());
		processMst46.setProcessDescription("Account Balance Report Window");
		processMst46.setProcessName("ACCOUNT BALANCE");
		processMstRepository.saveAndFlush(processMst46);

		ProcessMst processMst47 = new ProcessMst();
		processMst47.setId("PRC0047" + CompanyMstOpt.get().getCompanyName());
		processMst47.setProcessDescription("Monthly Stock Report Window");
		processMst47.setProcessName("MONTHLY STOCK REPORT");
		processMstRepository.saveAndFlush(processMst47);

		ProcessMst processMst48 = new ProcessMst();
		processMst48.setId("PRC0048" + CompanyMstOpt.get().getCompanyName());
		processMst48.setProcessDescription("Sales Report Window");
		processMst48.setProcessName("SALES REPORT");
		processMstRepository.saveAndFlush(processMst48);

		ProcessMst processMst49 = new ProcessMst();
		processMst49.setId("PRC0049" + CompanyMstOpt.get().getCompanyName());
		processMst49.setProcessDescription("Receipts and Payments Windows");
		processMst49.setProcessName("ACCOUNTS");
		processMstRepository.saveAndFlush(processMst49);

		ProcessMst processMst50 = new ProcessMst();
		processMst50.setId("PRC0050" + CompanyMstOpt.get().getCompanyName());
		processMst50.setProcessDescription("Supplier Payments Window");
		processMst50.setProcessName("SUPPLIER PAYMENTS");
		processMstRepository.saveAndFlush(processMst50);

		ProcessMst processMst51 = new ProcessMst();
		processMst51.setId("PRC0051" + CompanyMstOpt.get().getCompanyName());
		processMst51.setProcessDescription("For Settlement of Receipts and Payments against invoice");
		processMst51.setProcessName("OWN ACCOUNT SETTLEMENT");
		processMstRepository.saveAndFlush(processMst51);

		ProcessMst processMst52 = new ProcessMst();
		processMst52.setId("PRC0052" + CompanyMstOpt.get().getCompanyName());
		processMst52.setProcessDescription("For the Actual production Entry");
		processMst52.setProcessName("ACTUAL PRODUCTION");
		processMstRepository.saveAndFlush(processMst52);

		ProcessMst processMst53 = new ProcessMst();
		processMst53.setId("PRC0053" + CompanyMstOpt.get().getCompanyName());
		processMst53.setProcessDescription("For Whole Sale Return");
		processMst53.setProcessName("SALES RETURN");
		processMstRepository.saveAndFlush(processMst53);

		ProcessMst processMst54 = new ProcessMst();
		processMst54.setId("PRC0054" + CompanyMstOpt.get().getCompanyName());
		processMst54.setProcessDescription("For converting sale order to sales");
		processMst54.setProcessName("SALES FROM ORDER");
		processMstRepository.saveAndFlush(processMst54);

		ProcessMst processMst55 = new ProcessMst();
		processMst55.setId("PRC0055" + CompanyMstOpt.get().getCompanyName());
		processMst55.setProcessDescription("For editing sale orders");
		processMst55.setProcessName("SALES ORDER EDIT");
		processMstRepository.saveAndFlush(processMst55);

		ProcessMst processMst56 = new ProcessMst();
		processMst56.setId("PRC0056" + CompanyMstOpt.get().getCompanyName());
		processMst56.setProcessDescription("For Configure Software");
		processMst56.setProcessName("CONFIGURATIONS");
		processMstRepository.saveAndFlush(processMst56);

		ProcessMst processMst57 = new ProcessMst();
		processMst57.setId("PRC0057" + CompanyMstOpt.get().getCompanyName());
		processMst57.setProcessDescription("For Adding Payment Mode");
		processMst57.setProcessName("RECEIPT MODE");
		processMstRepository.saveAndFlush(processMst57);

		ProcessMst processMs58 = new ProcessMst();
		processMs58.setId("PRC0058" + CompanyMstOpt.get().getCompanyName());
		processMs58.setProcessDescription("For Adding Voucher Type");
		processMs58.setProcessName("VOUCHER TYPE");
		processMstRepository.saveAndFlush(processMs58);

		return "processes saved";
	}

	@Override
	public List<AccountHeads> findByAssetAccount(String companymstid) {

		List<AccountHeads> accontHeadsList = new ArrayList<>();
		List<String> string = accountHeadsRepository.findByAssetAccount(companymstid);
		for (int i = 0; i < string.size(); i++) {

			AccountHeads heads = new AccountHeads();
			heads.setId(string.get(0));
			accontHeadsList = accountHeadsRepository.fetchAccountHeads(heads.getId());

		}

		return accontHeadsList;

	}

	// .................SIBI.................2/5/2021............//
	
	
	
/*
 * new service for managing accountheads with supplier and customer
 */
	@Override
	public void accountHeadsManagement(String objectId,String objectName,String objectType,String companyMstId) {
		
		AccountHeads accountHeads = accountHeadsRepository.findByIdAndCompanyMstId(objectId, companyMstId);
		if(null != accountHeads) {
			if(accountHeads.getAccountName().equalsIgnoreCase(objectName)) {
				accountHeadsRepository.updateStatusInAccountHeads(accountHeads.getId(),objectType);
			}else {
				accountHeadsRepository.updateNameAndStatusInAccountHeads(accountHeads.getId(),objectName,objectType);
			}
		}else {
			createAccountHeads(objectId,objectName,objectType,companyMstId);
		}
	}

	
	
	private void createAccountHeads(String objectId, String objectName, String objectType, String companyMstId) {
		AccountHeads accountHeadsNew = new AccountHeads();
		accountHeadsNew.setId(objectId);
		accountHeadsNew.setAccountName(objectName);
		accountHeadsNew.setCustomerOrSupplier(objectType);
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companyMstId);
		accountHeadsNew.setCompanyMst(companyMst.get());
		
		if(null != companyMst.get().getCurrencyName())
		{
		CurrencyMst currencyMst = currencyMstRepo.findByCurrencyName(companyMst.get().getCurrencyName());
		accountHeadsNew.setCurrencyId(currencyMst.getId());
		}
		
		if(objectType.equalsIgnoreCase(MapleConstants.SUPPLIER)) {
			AccountHeads accountHeadsForRootId = accountHeadsRepository.findByAccountName( MapleConstants.LIABILITY);
			if(null != accountHeadsForRootId) {
				accountHeadsNew.setRootParentId(accountHeadsForRootId.getId());
			}
			AccountHeads accountHeadsForParentId = accountHeadsRepository.findByAccountName( MapleConstants.SUNDRYCREDITORS);
			if(null!=accountHeadsForParentId) {
				accountHeadsNew.setParentId(accountHeadsForParentId.getId());
			}
		}else if(objectType.equalsIgnoreCase(MapleConstants.CUSTOMER)) {
			AccountHeads accountHeadsForRootId = accountHeadsRepository.findByAccountName( MapleConstants.ASSET);
			if(null != accountHeadsForRootId) {
				accountHeadsNew.setRootParentId(accountHeadsForRootId.getId());
			}
			AccountHeads accountHeadsForParentId = accountHeadsRepository.findByAccountName( MapleConstants.SUNDRYDEBTORS);
			if(null!=accountHeadsForParentId) {
				accountHeadsNew.setParentId(accountHeadsForParentId.getId());
			}
		}
		
		accountHeadsNew.setGroupOnly(MapleConstants.NO);
		
		accountHeadsRepository.save(accountHeadsNew);
		
	}

	@Override
	public List<AccountHeads> searchLikeAccountName(String searchString, String companymstid, Pageable topFifty) {
		
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		return accountHeadsRepository.findSearch(searchString.toLowerCase(),companyMst.get(),topFifty);
	}

	@Override
	public List<AccountHeads> searchLikeSupplierByNamefromAccountHeads(String searchData, CompanyMst companyMst) {
		return accountHeadsRepository.findSearchByData(searchData,companyMst);
	}

	
	/*
	 * fetch account by account name. If it is not present, then create a new account and sent back.
	 */
	@Override
	public AccountHeads getAccountByAccountNameWithStatus(String selectedItem, Boolean accountCreateStatus,
			CompanyMst companyMst) {
		
 
		 
			AccountHeads   accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMstId(selectedItem, companyMst.getId());
			if(null == accountHeads && accountCreateStatus) {
				  accountHeads = new AccountHeads();
				  
				  VoucherNumber voucherNumber =  voucherNumberService.generateInvoice("ACCH", companyMst.getId());
				  String vcno =voucherNumber.getCode();
				 
				  
				  accountHeads.setId(vcno);
				accountHeads.setAccountName(selectedItem);
				accountHeads.setCustomerOrSupplier("CUSTOMER");
				accountHeads.setCompanyMst(companyMst);
				accountHeads = accountHeadsRepository.save(accountHeads);
			}
		 
		
		return accountHeads;
	}

}
