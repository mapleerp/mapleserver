package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.report.entity.DailyReceiptsSummaryReport;

public interface ReceiptEditService {

	List<DailyReceiptsSummaryReport> receiptSummaryReport(String companymstid, Date date);

	//--------------version 4.14

	DailyReceiptsSummaryReport receiptSummaryReportByReceiptMode(String companymstid, Date date,
			String receiptmode);
	//--------------version 4.14 end
}
