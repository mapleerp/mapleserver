package com.maple.restserver.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service

@Component
public interface ItemBatchMstService  {
	
	String stockVerification(String companymstid, String salestranshdrid);
}
