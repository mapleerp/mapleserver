package com.maple.restserver.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SchEligibilityAttribInst;
import com.maple.restserver.entity.SchOfferAttrInst;
import com.maple.restserver.entity.SchSelectionAttribInst;
import com.maple.restserver.entity.SchemeInstance;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SchEligibilityAttribInstRepository;
import com.maple.restserver.repository.SchOfferAttrInstRepository;
import com.maple.restserver.repository.SchSelectionAttribInstRepository;
import com.maple.restserver.repository.SchemeInstanceRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.utils.SystemSetting;

@Service
public class SchemeServiceImpl implements SchemeService {
	/*
	 * Date 27-08-2020
	 * 
	 * SchemeService is not in use ,which is written for simpliyfing the code
	 * ,currently schem is managed from front end ,i kept the code here for the
	 * further enhancement of the scheme module
	 * 
	 * 
	 */
	@Autowired
	SchemeInstanceRepository schemeInstanceRepository;
	@Autowired
	SchSelectionAttribInstRepository schSelectionAttribInstRepository;
	
	@Autowired
	SchEligibilityAttribInstRepository schEligibilityAttribInstRepository;
	@Autowired
	SalesDetailsRepository salesDetailsRepo;
	@Autowired
	SchOfferAttrInstRepository schOfferAttrInstRepository;
	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	ItemMstRepository itemMstRepository;
	@Autowired
	UnitMstRepository unitMstRepository;
	@Override
	public boolean CheckOfferWhileFinalSave(SalesTransHdr salesTransHdr, Date logDate) {
		
		
		

		String salesMode = null;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("POS")) {

			salesMode = "Retail";

		}
		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2B")
				|| salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			salesMode = "WholeSale";
		}
		if (null == salesMode) {
			return false;
		}
		
		List<SchemeInstance> schemeInstanceList = schemeInstanceRepository
				.findByCompanyMstAndSchemeWholesaleRetailAndIsActive(salesTransHdr.getCompanyMst().getId(), salesMode,
						"YES");

		if (schemeInstanceList.size() > 0) {

			for (SchemeInstance scheme : schemeInstanceList) {
				Boolean validityPeriod = CheckValidityPeriod(scheme, logDate, salesTransHdr);
				if (validityPeriod) {

					Boolean eligible = eligibilityCheck(scheme, salesTransHdr);
					if (eligible) {
						return true;
					}

				}
			}
		}
		return false;
		
		
		

	}

	private Boolean CheckValidityPeriod(SchemeInstance scheme, Date loginDate, SalesTransHdr salesTransHdr) {

		List<SchSelectionAttribInst> schSlectionAttrbInstList = schSelectionAttribInstRepository
				.findByCompanyMstAndSchemeId(salesTransHdr.getCompanyMst(), scheme.getId());

		for (SchSelectionAttribInst select : schSlectionAttrbInstList) {

			if (select.getAttributeType().equalsIgnoreCase("DATE")) {

				if (select.getAttributeName().equalsIgnoreCase("START DATE")) {
					Boolean validStartDate = validStartDate(select, loginDate);
					if (!validStartDate) {
						return false;
					}
				}

				if (select.getAttributeName().equalsIgnoreCase("END DATE")) {
					Boolean validEndDate = validEndDate(select, loginDate);
					if (!validEndDate) {
						return false;
					}
				}

			}

			if (select.getAttributeType().equalsIgnoreCase("DAY")) {

//				java.util.Date udate = SystemSetting.systemDate;
				java.util.Calendar calender = java.util.Calendar.getInstance();
				calender.setTime(loginDate);

				int day = calender.get(java.util.Calendar.DAY_OF_WEEK);
				String dayofweek = checkValidDay(day);

				if (!dayofweek.equalsIgnoreCase(select.getAttributeValue())) {
					return false;
				}

			}

			if (select.getAttributeType().equalsIgnoreCase("MONTH")) {

//				java.util.Date udate = SystemSetting.systemDate;
				java.util.Calendar calender = java.util.Calendar.getInstance();
				calender.setTime(loginDate);

				int month = loginDate.getMonth();
				String monthname = MonthName(month);

				if (!monthname.equalsIgnoreCase(select.getAttributeValue())) {
					return false;
				}

			}

		}

		return true;
	}
	
	
	private Boolean validStartDate(SchSelectionAttribInst select, Date nowDate) {
		System.out.println("Noooooooooooooooo" + nowDate);
		java.util.Date startDate = SystemSetting.StringToUtilDate(select.getAttributeValue(), "dd-MM-yyyy");
		Integer intDate = nowDate.compareTo(startDate);

		if (intDate >= 0) {
			return true;
		}

		return false;
	}
	
	private Boolean validEndDate(SchSelectionAttribInst select, Date nowDate) {
		System.out.println("Noooooooooooooooo" + nowDate);
		java.util.Date endDate = SystemSetting.StringToUtilDate(select.getAttributeValue(), "dd-MM-yyyy");
		Integer intDate = endDate.compareTo(nowDate);

		if (intDate >= 0) {
			return true;
		}
		return false;
	}
	
	private String checkValidDay(int day) {
		if (day == 1) {
			return "SUNDAY";
		}
		if (day == 2) {
			return "MONDAY";
		}
		if (day == 3) {
			return "TUESDAY";
		}
		if (day == 4) {
			return "WEDNESDAY";
		}
		if (day == 5) {
			return "THURSDAY";
		}
		if (day == 6) {
			return "FRIDAY";
		}
		if (day == 7) {
			return "SATURDAY";
		}

		return null;
	}
	
	private String MonthName(int month) {
		if (month == 0) {
			return "JANUARY";
		}
		if (month == 1) {
			return "FEBRUARY";
		}
		if (month == 2) {
			return "MARCH";
		}
		if (month == 3) {
			return "APRIL";
		}
		if (month == 4) {

			return "MAY";
		}
		if (month == 5) {
			return "JUNE";
		}
		if (month == 6) {
			return "JULY";
		}
		if (month == 7) {
			return "AUGUST";
		}
		if (month == 8) {
			return "SEPTEMBER";
		} else if (month == 9) {
			return "OCTOBER";
		}
		if (month == 10) {
			return "NOVEMBER";
		}
		if (month == 11) {
			return "DECEMBER";
		}

		return null;
	}
	private Boolean eligibilityCheck(SchemeInstance scheme, SalesTransHdr salesTransHdr) {

		CompanyMst companyMst = salesTransHdr.getCompanyMst();
		List<SchEligibilityAttribInst> schEligibilityAttrbInstList = schEligibilityAttribInstRepository
				.findByCompanyMstAndSchemeIdAndEligibilityId(companyMst, scheme.getId(), scheme.getEligibilityId());

		for (SchEligibilityAttribInst inst : schEligibilityAttrbInstList) {

			if (inst.getEligibilityId().equals("7")) {

				Boolean validInvoice = ChecknvoiceAmount(inst, scheme, salesTransHdr);
				if (!validInvoice) {
					return false;
				} else {
					return true;
				}

			}

		}

		return true;

	}

	private Boolean ChecknvoiceAmount(SchEligibilityAttribInst inst, SchemeInstance scheme,
			SalesTransHdr salesTransHdr) {
		Double eligibAmount = 0.0;

		SummarySalesDtl summary = salessummary(salesTransHdr.getId());

		SchEligibilityAttribInst eligib = schEligibilityAttribInstRepository
				.findByCompanyMstAndAttributeNameAndSchemeId(salesTransHdr.getCompanyMst(), "AMOUNT", scheme.getId());

		eligibAmount = Double.parseDouble(eligib.getAttributeValue());

		SalesDtl salesDtl = new SalesDtl();
		salesDtl.setAmount(summary.getTotalAmount());
		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setCompanyMst(salesTransHdr.getCompanyMst());

		if (eligibAmount <= summary.getTotalAmount()) {
			if (scheme.getOfferId().equals("2")) {

				AddOfferQtyCheck(scheme, salesTransHdr, salesDtl, eligibAmount, "ITEM_AND_QTY");
				return true;
			}

			if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

				AddAmountDiscount(scheme, salesTransHdr, eligibAmount, "ITEM_AND_QTY");
				return true;
			}

		}
		return false;
	}
	

	private SummarySalesDtl salessummary(String salesTransHdrId) {
		List<Object> objList = salesDetailsRepo.findSumSalesDtl(salesTransHdrId);

		Object[] objAray = (Object[]) objList.get(0);

		SummarySalesDtl summary = new SummarySalesDtl();
		summary.setTotalQty((Double) objAray[0]);
		summary.setTotalAmount((Double) objAray[1]);
		summary.setTotalTax((Double) objAray[2]);
		summary.setTotalCessAmt((Double) objAray[3]);

		return summary;

	}
	
	
	private void AddOfferQtyCheck(SchemeInstance scheme, SalesTransHdr salesTransHdr, SalesDtl salesDtl, Double eligQty,
			String eligibilityType) {

		String itemName = "";
		String batch = "";
		String mutipleAllowed = "";
		Double offerQty = 0.0;

		List<SchOfferAttrInst> offerInstList = schOfferAttrInstRepository
				.findByCompanyMstAndSchemeId(salesTransHdr.getCompanyMst(), scheme.getId());

		if (offerInstList.size() > 0) {
			for (SchOfferAttrInst offer : offerInstList) {
				if (offer.getAttributeName().equalsIgnoreCase("ITEMNAME")) {
					itemName = offer.getAttributeValue();
				}

				if (offer.getAttributeName().equalsIgnoreCase("QTY")) {
					offerQty = Double.parseDouble(offer.getAttributeValue());
				}

				if (offer.getAttributeName().equalsIgnoreCase("BATCH_CODE")) {
					batch = offer.getAttributeValue();
				}

				if (offer.getAttributeName().equalsIgnoreCase("MULTIPLE ALLOWED")) {
					mutipleAllowed = offer.getAttributeValue();
				}
			}
		}

		addOfferItem(itemName, offerQty, batch, mutipleAllowed, salesDtl, eligQty, eligibilityType, scheme);

	}


	private void AddAmountDiscount(SchemeInstance scheme, SalesTransHdr salesTransHdr, Double instQty, String string) {

		Double discount = 0.0;

		SummarySalesDtl summary = salessummary(salesTransHdr.getId());

		List<SchOfferAttrInst> offerInstList = schOfferAttrInstRepository
				.findByCompanyMstAndSchemeId(salesTransHdr.getCompanyMst(), scheme.getId());
		for (SchOfferAttrInst offer : offerInstList) {
			if (offer.getAttributeName().equalsIgnoreCase("DISCOUNT")) {
				discount = Double.parseDouble(offer.getAttributeValue());
			}
			if (offer.getAttributeName().equalsIgnoreCase("PERCENTAGE DISCOUNT")) {
				discount = Double.parseDouble(offer.getAttributeValue());
			}
		}

		if (scheme.getOfferId().equals("1")) {

			salesTransHdr.setInvoiceDiscount(discount);

			discount = (discount / summary.getTotalAmount()) * 100;
			String discountper = discount + "%";

			salesTransHdr.setDiscount(discountper);
		}
		if (scheme.getOfferId().equals("4")) {

			salesTransHdr.setDiscount(discount + "%");

			discount = summary.getTotalAmount() * discount / 100;
			salesTransHdr.setInvoiceDiscount(discount);

		}

		return;

	}
	
	private void addOfferItem(String itemName, Double offerQty, String batch, String mutipleAllowed,
			SalesDtl salesDtlref, Double eligQty, String eligibilityType, SchemeInstance scheme) {

		Optional<ItemMst> itemMstOpt = itemMstRepository.findByItemNameAndCompanyMst(itemName,
				salesDtlref.getCompanyMst());
		ItemMst itemMst = itemMstOpt.get();

		SalesTransHdr salesTransHdr = salesDtlref.getSalesTransHdr();

		int itemqty = 0;

		if (mutipleAllowed.equalsIgnoreCase("YES")) {

			SchEligibilityAttribInst schEligibilityAttribInstAmt = schEligibilityAttribInstRepository
					.findByCompanyMstAndAttributeNameAndSchemeId(salesDtlref.getCompanyMst(), "AMOUNT", scheme.getId());

			if (null != schEligibilityAttribInstAmt) {
				itemqty = (int) (salesDtlref.getAmount() / eligQty);

				itemqty = (int) (itemqty * offerQty);
			}

			System.out.println("--mutipleAllowed---Qty-------" + itemqty);
		}

		Optional<CompanyMst> companyMst = companyMstRepository.findById(salesDtlref.getCompanyMst().getId());

		SalesDtl salesDtl = new SalesDtl();
		salesDtl.setCompanyMst(companyMst.get());
		salesDtl.setItemId(itemMst.getId());
		salesDtl.setItemName(itemMst.getItemName());
		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setBarcode(itemMst.getBarCode());
		salesDtl.setBatch(batch);
		salesDtl.setSchemeId(scheme.getId());
		if (itemqty > 0) {
			salesDtl.setQty((double) itemqty);
		} else {
			salesDtl.setQty((double) offerQty);
		}
		salesDtl.setUnitId(itemMst.getUnitId());

		Optional<UnitMst> unitMstOpt = unitMstRepository.findById(itemMst.getUnitId());
		UnitMst unitMst = unitMstOpt.get();

		if (null != unitMst) {
			salesDtl.setUnitName(unitMst.getUnitName());
		}

		salesDtl.setMrp(0.0);
		salesDtl.setTaxRate(0.0);
		salesDtl.setRate(0.0);
		salesDtl.setCessAmount(0.0);
		salesDtl.setCessRate(0.0);
		salesDtl.setCgstAmount(0.0);
		salesDtl.setCgstTaxRate(0.0);
		salesDtl.setSgstAmount(0.0);
		salesDtl.setSgstTaxRate(0.0);
		salesDtl.setAmount(0.0);
		salesDtl.setAddCessRate(0.0);
		salesDtl.setDiscount(0.0);
		salesDtl.setIgstAmount(0.0);
		salesDtl.setIgstTaxRate(0.0);

		salesDtl = salesDetailsRepo.saveAndFlush(salesDtl);

	}
}
