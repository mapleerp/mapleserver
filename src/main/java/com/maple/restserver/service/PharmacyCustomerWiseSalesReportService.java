package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.PharmacyNewCustomerWiseSaleReport;

@Service
public interface PharmacyCustomerWiseSalesReportService {

	List<PharmacyNewCustomerWiseSaleReport> findAMDCCustomerWiseSaleReport(Date fdate, Date tdate, String[] array);

}
