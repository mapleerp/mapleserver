package com.maple.restserver.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MenuMst;


@Service
@Transactional
@Component
public interface MenuMstService {

	String InitializeCompanyMst(CompanyMst companyMst,String branchcode);

}
