package com.maple.restserver.service;

import org.springframework.stereotype.Service;

@Service
public interface SaleOrderToProductionService {

	public String convertSaleOrderToProduction(String companymstid,String branchCode);
}
