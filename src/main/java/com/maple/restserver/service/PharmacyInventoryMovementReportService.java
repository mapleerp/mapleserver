package com.maple.restserver.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.PharmacyItemMovementAnalisysReport;

@Service
public interface PharmacyInventoryMovementReportService {

	
	List<PharmacyItemMovementAnalisysReport>getPharmacyInventoryMovementReport(CompanyMst companyMst ,Date fdate,Date tdate,String branchCode,String []array); 
	
	
	List<PharmacyItemMovementAnalisysReport>getPharmacyInventoryMovementReportByCatItem(CompanyMst companyMst ,Date fdate,Date tdate,String branchCode,String [] itemArray,String []categoryArray,String batch);
}
