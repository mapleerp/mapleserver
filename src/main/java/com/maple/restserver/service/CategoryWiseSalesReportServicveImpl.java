package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.CategoryWiseSalesReport;
import com.maple.restserver.repository.SalesTransHdrRepository;

@Service
public class CategoryWiseSalesReportServicveImpl  implements CategoryWiseSalesReportService {

	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@Override
	public List<CategoryWiseSalesReport> getCategoryWiseSalesReport( Date fromDate, Date toDate,
			String branchCode, CompanyMst companyMst) {

		
		  List<CategoryWiseSalesReport> categoryWiseSalesReportList=new
		  ArrayList<CategoryWiseSalesReport>();
		  List<Object>
		  obj=salesTransHdrRepository.getCategoryWiseSalesReport( fromDate, toDate,
		  branchCode, companyMst); 
		  for(int i=0;i<obj.size();i++) { Object[] objAray =
		  (Object[]) obj.get(i);
		  
		System.out.print(obj.size()+"category list size issssssssssssssssssssssssss");  
		  CategoryWiseSalesReport categoryWiseSalesReport=new CategoryWiseSalesReport();
		  
	
		  categoryWiseSalesReport.setCategoryName((String) objAray[0]);
		  categoryWiseSalesReport.setCategorId((String) objAray[1]);
		  categoryWiseSalesReportList.add(categoryWiseSalesReport);
			
			  List<Object>objectList=salesTransHdrRepository.fetchSalesDtlsByCategory(
			  categoryWiseSalesReport.getCategorId(),fromDate,toDate,branchCode,companyMst)
			  ;
			  //
			  for(int j=0;j<objectList.size();j++)
			  { 
			  CategoryWiseSalesReport report=new CategoryWiseSalesReport();
			  Object[] objArray = (Object[]) objectList.get(j);
			  report.setItemName((String) objArray[0]);
			  report.setQty((Double)objArray[1]); 
			  report.setValue((Double) objArray[1]*(Double) objArray[2]);
			  report.setRate((Double) objArray[2]);
			  categoryWiseSalesReportList.add(report);
			  
			  }
			 
		  }
		 
		return categoryWiseSalesReportList;
	}
	
	
	
	
	
	
}
