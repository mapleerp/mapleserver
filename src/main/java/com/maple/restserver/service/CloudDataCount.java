package com.maple.restserver.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.LastBestDate;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@Component
public class CloudDataCount {

	@Autowired
	ExternalApi externalApi;

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	BranchMstRepository branchMstRepository;

	private String companymstid;

	public String getCompanymstid() {
		return companymstid;
	}

	public void setCompanymstid(String companymstid) {
		this.companymstid = companymstid;
	}

	public String getBranchcode() {
		return branchcode;
	}

	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}

	private String branchcode;
	private List<LastBestDate> lastBestDateList;

	public ArrayList<LastBestDate> getcloudDataCount(ArrayList<LastBestDate>  lbList) {
		
		System.out.println("Entered into CloudDataCountservice.getcloudDataCount() method****************************");

		lastBestDateList = externalApi.countVerificationTocloud(companymstid, branchcode, lbList);

		System.out.println("The result of CloudDataCountservice.getcloudDataCount() ====> " + lastBestDateList);
		return  (ArrayList<LastBestDate>) lastBestDateList ;
	}

	public Double getServerCount(String activtyType) {
		
		
		System.out.println("Entered into cloudDataCount.getServerCount() method****************************");
			
		Double serverDataCount = 0.0;
		
//		for (LastBestDate lastBestDate : lastBestDateList) {
//
//			if (lastBestDate.getActivityType().equalsIgnoreCase(activtyType)) {
//				serverDataCount = lastBestDate.getCount();
//				break;
//			}
//
//		}
		
		Iterator iter = lastBestDateList.iterator();
		while(iter.hasNext()) {
			HashMap hm = (HashMap) iter.next();
			
			System.out.println(hm);
			
			
			String abc = (String) hm.get("activityType");
			System.out.print(abc);
			
			if (abc.equalsIgnoreCase(activtyType)) {
				serverDataCount = (Double) hm.get("count");
				break;
			}
		}
		System.out.println("The result of getServerCount is:"+serverDataCount);
		return serverDataCount;

	}

}
