package com.maple.restserver.mobileEntity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MobileCustomerMst;

@Entity
public class MobileCartHdr implements Serializable {
	private static final long serialVersionUID = 1L;
    @Id
    @Column(length = 50)
    String id;
    @Column(length = 50)
    private String customerId;
    @Column(length = 50)
    private String voucherNumber;
    @Column(length = 50)
    private String voucherDate;
    @Column(length = 50)
    private String status;
    
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
    
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "mobileCustomerMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	MobileCustomerMst mobileCustomerMst;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public MobileCustomerMst getMobileCustomerMst() {
		return mobileCustomerMst;
	}

	public void setMobileCustomerMst(MobileCustomerMst mobileCustomerMst) {
		this.mobileCustomerMst = mobileCustomerMst;
	}
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "MobileCartHdr [id=" + id + ", customerId=" + customerId + ", voucherNumber=" + voucherNumber
				+ ", voucherDate=" + voucherDate + ", status=" + status + ", companyMst=" + companyMst
				+ ", mobileCustomerMst=" + mobileCustomerMst + "]";
	}

	
}
