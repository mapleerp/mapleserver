package com.maple.restserver.mobileEntity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;

@Entity
public class MobileCartDtl  implements Serializable {
	private static final long serialVersionUID = 1L;
    @Id
    @Column(length = 50)
    String id;
    @Column(length = 50)
    String itemId;
    @Column(length = 50)
    String qty;
    @Column(length = 50)
    String itemName;
    
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
  	@JoinColumn(name = "companyMst", nullable = false)
  	@OnDelete(action = OnDeleteAction.CASCADE)
  	CompanyMst companyMst;
    
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
  	@JoinColumn(name = "mobileCartHdr", nullable = false)
  	@OnDelete(action = OnDeleteAction.CASCADE)
    MobileCartHdr mobileCartHdr;

    
    
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public MobileCartHdr getMobileCartHdr() {
		return mobileCartHdr;
	}

	public void setMobileCartHdr(MobileCartHdr mobileCartHdr) {
		this.mobileCartHdr = mobileCartHdr;
	}

	@Override
	public String toString() {
		return "MobileCartDtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", itemName=" + itemName
				+ ", companyMst=" + companyMst + ", mobileCartHdr=" + mobileCartHdr + "]";
	}

	
    
}
