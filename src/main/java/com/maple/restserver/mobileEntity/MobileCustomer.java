package com.maple.restserver.mobileEntity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;

@Entity
public class MobileCustomer implements Serializable{
	private static final long serialVersionUID = 1L;
    @Id
    @Column(length = 50)
    String id;
    @Column(length = 20)
	private String mobNumber;
    @Column(length = 50)
	private String customerName;
    @Column(length = 50)
	private String locationLongitude;
    @Column(length = 50)
	private String locationLattitude;
    @Column(length = 10)
	private String otp;
    @Column(length = 20)
	String password;
    @Column(length = 50)
	String category;
    @Column(length = 50)
	String customerStatus;
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMobNumber() {
		return mobNumber;
	}

	public void setMobNumber(String mobNumber) {
		this.mobNumber = mobNumber;
	}

	public String getLocationLongitude() {
		return locationLongitude;
	}

	public void setLocationLongitude(String locationLongitude) {
		this.locationLongitude = locationLongitude;
	}

	public String getLocationLattitude() {
		return locationLattitude;
	}

	public void setLocationLattitude(String locationLattitude) {
		this.locationLattitude = locationLattitude;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCustomerStatus() {
		return customerStatus;
	}

	public void setCustomerStatus(String customerStatus) {
		this.customerStatus = customerStatus;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "MobileCustomer [id=" + id + ", mobNumber=" + mobNumber + ", customerName=" + customerName
				+ ", locationLongitude=" + locationLongitude + ", locationLattitude=" + locationLattitude + ", otp="
				+ otp + ", password=" + password + ", category=" + category + ", customerStatus=" + customerStatus
				+ ", companyMst=" + companyMst + "]";
	}


	
	
	
}
