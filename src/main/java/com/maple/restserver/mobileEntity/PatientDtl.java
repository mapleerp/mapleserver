package com.maple.restserver.mobileEntity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;
@Entity
public class PatientDtl implements Serializable {
	private static final long serialVersionUID = 1L;
    @Id
    @Column(length = 50)
    String id;
    @Column(length = 50)
    String locationLongitude;
    @Column(length = 50)
    String locationLatitude;
    @Column(length = 50)
    String deviceId;
    @Column(length = 50)
    String covidStatus;
    @Column(length = 50)
    String insuranceCompany;
    @Column(length = 50)
    String insCard;
    Date insCardExpiryDate;
    Date dateOfBirth;
    @Column(length = 50)
    String gender;
    @Column(length = 50)
    String nationality;
    @Column(length = 50)
    String  nationalId;
    @Column(length = 50)
    String policyType;
    Double percentDiscount;
    @Column(length = 50)
    String hospitalId;
    @Column(length = 50)
    String customerId;
    @Column(length = 50)
    String patientId;
    @Column(length = 50)
    String postedToServer;
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    
    
	CompanyMst companyMst;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLocationLongitude() {
		return locationLongitude;
	}
	public void setLocationLongitude(String locationLongitude) {
		this.locationLongitude = locationLongitude;
	}
	public String getLocationLatitude() {
		return locationLatitude;
	}
	public void setLocationLatitude(String locationLatitude) {
		this.locationLatitude = locationLatitude;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getCovidStatus() {
		return covidStatus;
	}
	public void setCovidStatus(String covidStatus) {
		this.covidStatus = covidStatus;
	}
	public String getInsuranceCompany() {
		return insuranceCompany;
	}
	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}
	public String getInsCard() {
		return insCard;
	}
	public void setInsCard(String insCard) {
		this.insCard = insCard;
	}
	
	
	
	public Date getInsCardExpiryDate() {
		return insCardExpiryDate;
	}
	public void setInsCardExpiryDate(Date insCardExpiryDate) {
		this.insCardExpiryDate = insCardExpiryDate;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getNationalId() {
		return nationalId;
	}
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public Double getPercentDiscount() {
		return percentDiscount;
	}
	public void setPercentDiscount(Double percentDiscount) {
		this.percentDiscount = percentDiscount;
	}
	public String getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(String hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getPostedToServer() {
		return postedToServer;
	}
	public void setPostedToServer(String postedToServer) {
		this.postedToServer = postedToServer;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	@Override
	public String toString() {
		return "PatientDtl [id=" + id + ", locationLongitude=" + locationLongitude + ", locationLatitude="
				+ locationLatitude + ", deviceId=" + deviceId + ", covidStatus=" + covidStatus + ", insuranceCompany="
				+ insuranceCompany + ", insCard=" + insCard + ", insCardExpiryDate=" + insCardExpiryDate
				+ ", dateOfBirth=" + dateOfBirth + ", gender=" + gender + ", nationality=" + nationality
				+ ", nationalId=" + nationalId + ", policyType=" + policyType + ", percentDiscount=" + percentDiscount
				+ ", hospitalId=" + hospitalId + ", customerId=" + customerId + ", patientId=" + patientId
				+ ", postedToServer=" + postedToServer + ", companyMst=" + companyMst + "]";
	}
	
	
    
    
	
	
    
    
}
