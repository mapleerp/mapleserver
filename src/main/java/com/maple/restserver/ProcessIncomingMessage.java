package com.maple.restserver;

import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Optional;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
 
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.AccountClassfc;
import com.maple.restserver.accounting.entity.AccountMergeMst;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.CreditClassfc;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.DebitClassfc;
import com.maple.restserver.accounting.entity.InventoryLedgerMst;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.entity.LedgerClassfc;
import com.maple.restserver.accounting.entity.LinkedAccounts;
import com.maple.restserver.accounting.entity.OpeningBalanceConfigMst;
import com.maple.restserver.accounting.entity.OpeningBalanceMst;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.AccountClassfcRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.CreditClassfcRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.DebitClassfcRepository;
import com.maple.restserver.accounting.repository.InventoryLedgerMstRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassfcRepository;
import com.maple.restserver.accounting.repository.LinkedAccountsRepository;
import com.maple.restserver.accounting.repository.OpeningBalanceConfigMstRepository;
import com.maple.restserver.accounting.repository.OpeningBalanceMstRepository;
import com.maple.restserver.entity.*;
import com.maple.restserver.jms.send.KafkaMapleEvent;
import com.maple.restserver.jms.send.KafkaMapleEventType;

import com.maple.restserver.message.entity.AcceptStockMessage;
import com.maple.restserver.message.entity.KitDefinitionHdrAndDtlMessageEntity;
import com.maple.restserver.message.entity.SchemeMessage;
import com.maple.restserver.repository.*;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.SystemSetting;

@Component
public class ProcessIncomingMessage {
	private final Logger logger = LoggerFactory.getLogger(ProcessIncomingMessage.class);
	@Autowired
	FileMover fileMover;
	@Autowired
	ItemMstRepository itemMstRepo;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Autowired
	UserMstRepository userMstRepo;
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	BranchMstRepository branchMstRepo;
	@Autowired
	PurchaseHdrRepository purchaseHdrRepo;
	/*
	 * @Autowired CustomerMstRepository customerMstRepo;
	 */
	@Autowired
	PurchaseDtlRepository purchaseDtlRepo;
	@Autowired
	UnitMstRepository unitMstRepo;
	
	@Autowired
	AccountClassRepository accountClassRepo;
	@Autowired
	AccountClassfcRepository accountClassfcRepo;
	@Autowired
	AccountMergeRepository accountMergeRepo;
	@Autowired
	CreditClassRepository creditClassRepo;
	@Autowired
	CreditClassfcRepository creditClassfcRepo;
	@Autowired
	DebitClassRepository debitClassRepo;
	@Autowired
	DebitClassfcRepository debitClassfcRepo;
	@Autowired
	InventoryLedgerMstRepository inventoryLedgerMstRepo;
	@Autowired
	LedgerClassRepository ledgerClassRepo;
	@Autowired
	LedgerClassfcRepository ledgerClassfcRepo;
	@Autowired
	LinkedAccountsRepository linkedAccountsRepo;
	@Autowired
	OpeningBalanceConfigMstRepository openingBalanceConfigMstRepository;
	@Autowired
	OpeningBalanceMstRepository openingBalanceMstRepo;
	@Autowired
	StoreMstRepository storeMstRepo;
	@Autowired
	SalesTypeMstRepository salesTypeMstRepo;
	@Autowired
	SchEligiAttrListDefRepository schEligiAttrListDefRepo;
	@Autowired
	SchEligibilityAttribInstRepository schEligibilityAttribInstRepo;
	@Autowired
	SchEligibilityDefRepository schEligibilityDefRepo;
	@Autowired
	SchemeInstanceRepository schemeInstanceRepo;
	@Autowired
	SchOfferAttrInstRepository schOfferAttrInstRepo;
	@Autowired
	SchOfferAttrListDefRepository schOfferAttrListDefRepo;
	@Autowired
	SchOfferDefRepository schOfferDefRepo;
	@Autowired
	SchSelectAttrListDefReoository schSelectAttrListDefReoo;
	@Autowired
	SchSelectDefRepository schSelectDefRepo;
	@Autowired
	SchSelectionAttribInstRepository schSelectionAttribInstRepo;
	@Autowired
	ServiceInHdrRepository serviceInHdrRepo;
	@Autowired
	ServiceInDtlRepository serviceInDtlRepo;
	@Autowired
	StoreChangeMstRepository storeChangeMstRepo;
	@Autowired
	StoreChangeDtlRepository storeChangeDtlRepo;
	@Autowired
	URSMstRepository uRSMstRepo;
	@Autowired
	VoucherNumberRepository voucherNumberRepo;
	@Autowired
	IntentDtlRepository intentDtlRepo;
	@Autowired
	IntentHdrRepository intentHdrRepo;
	@Autowired
	InvoiceEditEnableRepository invoiceEditEnableRepo;
	@Autowired
	OtherBranchSalesDtlRepository otherBranchSalesDtlRepo;
	@Autowired
	OtherBranchSalesTransHdrRepository otherBranchSalesTransHdrRepo;
	@Autowired
	OtherBranchPurchaseHdrRepository otherBranchPurchaseHdrRepo;
	@Autowired
	OtherBranchPurchaseDtlRepository otherBranchPurchaseDtlRepo;
	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepo;
	@Autowired
	SalesOrderDtlRepository salesOrderDtlRepo;
	@Autowired
	SalesDetailsRepository salesDetailsRepo;
	@Autowired
	SalesReceiptsRepository salesReceiptsRepo;
	@Autowired
	SalesDtlDeletedRepository salesDtlDeletedRepo;
	
	@Autowired
	CategoryMstRepository categoryMstRepo;
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepo;
	@Autowired
	ItemMergeMstRepository itemMergeMstRepo;
	@Autowired
	PaymentHdrRepository paymentHdrRepo;
	@Autowired
	PaymentDtlRepository paymentDtlRepo;
	@Autowired
	PurchaseSchemeMstRepository purchaseSchemeMstRepo;
	@Autowired
	ReceiptHdrRepository receiptHdrRepo;
	@Autowired
	ReceiptDtlRepository receiptDtlRepo;
	@Autowired
	ReciptModeMstRepository reciptModeMstRepo;
	@Autowired
	ActualProductionHdrRepository actualProductionHdrRepo;
	@Autowired

	ActualProductionDtlRepository actualProductionDtlRepo;
	@Autowired
	StockTransferOutHdrRepository stockTransferOutHdrRepo;
	@Autowired
	StockTransferOutDtlRepository stockTransferOutDtlRepo;
	@Autowired
	StockTransferInHdrRepository stockTransferInHdrRepo;
	@Autowired
	StockTransferInDtlRepository stockTransferInDtlRepo;
	@Autowired
	AccountHeadsRepository accountHeadsRepo;
	@Autowired
	AccountPayableRepository accountPayableRepo;
	@Autowired
	AccountReceivableRepository accountReceivableRepo;
	@Autowired
	AdditionalExpenseRepository additionalExpenseRepo;
	@Autowired
	BatchPriceDefinitionRepository batchPriceDefinitionRepo;
	@Autowired
	BrandMstRepository brandMstRepo;
	@Autowired
	ConsumptionDtlRepository consumptionDtlRepo;
	@Autowired
	ConsumptionReasonMstRepository consumptionReasonMstRepo;
	@Autowired
	DamageHdrRepository damageHdrRepo;
	@Autowired
	DamageDtlRepository damageDtlRepo;
	@Autowired
	DayBookRepository dayBookRepo;
	@Autowired
	DayEndClosureRepository dayEndClosureRepo;
	@Autowired
	DayEndClosureDtlRepository dayEndClosureDtlRepo;
	@Autowired
	DayEndReportStoreRepository dayEndReportStoreRepo;
	@Autowired
	DelivaryBoyMstRepository delivaryBoyMstRepo;
	@Autowired
	GoodReceiveNoteDtlRepository goodReceiveNoteDtlRepo;
	@Autowired
	GoodReceiveNoteHdrRepository goodReceiveNoteHdrRepo;
	@Autowired
	GroupMstRepository groupMstRepo;
	@Autowired
	GroupPermissionRepository groupPermissionRepo;
	@Autowired
	IntentInHdrRepository intentInHdrRepo;
	@Autowired
	IntentInDtlRepository intentInDtlRepo;
	@Autowired
	ItemBatchExpiryDtlRepository itemBatchExpiryDtlRepo;
	@Autowired
	SalesRetunDtlRepository salesRetunDtlRepo;
	@Autowired
	SalesReturnHdrRepository salesReturnHdrRepo;
	@Autowired
	ReorderMstRepository reorderMstRepo;
	@Autowired
	SaleOrderReceiptRepository saleOrderReceiptRepo;
	@Autowired
	ReceiptInvoiceDtlRepository receiptInvoiceDtlRepo;
	@Autowired
	RawMaterialReturnHdrRepository rawMaterialReturnHdrRepo;
	@Autowired
	RawMaterialReturnDtlRepository rawMaterialReturnDtlRepo;
	@Autowired
	RawMaterialIssueDtlRepository rawMaterialIssueDtlRepo;
	@Autowired
	RawMaterialMstRepository rawMaterialMstRepo;
	@Autowired
	PurchaseOrderDtlRepository purchaseOrderDtlRepo;
	@Autowired
	PurchaseOrderHdrRepository purchaseOrderHdrRepo;
	@Autowired
	ProcessMstRepository processMstRepo;
	@Autowired
	ProcessPermissionRepository processPermissionRepo;
	@Autowired
	ProductConfigurationMstRepository productConversionConfigMstRepo;
	@Autowired
	ProductConversionDtlRepository productConversionDtlRepo;
	@Autowired
	ProductMstRepository productMstRepo;
	@Autowired
	ProductConversionMstRepository productConversionMstRepo;
	@Autowired
	PriceDefinitionRepository priceDefinitionRepo;
	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepo;
	@Autowired
	OwnAccountSettlementDtlRepository ownAccountSettlementDtlRepo;
	@Autowired
	OwnAccountSettlementMstRepository ownAccountSettlementMstRepo;
	@Autowired
	PaymentInvoiceDtlRepository paymentInvoiceDtlRepo;
	@Autowired
	OwnAccountRepository ownAccountRepo;
	@Autowired
	OrderTakerMstRepository orderTakerMstRepo;
	@Autowired
	PDCPaymentRepository pDCPaymentRepo;
	@Autowired
	PDCReceiptRepository pDCReceiptRepo;
	@Autowired
	PDCReconcileHdrRepository pDCReconcileHdrRepo;
	@Autowired
	PDCReconcileDtlRepository pDCReconcileDtlRepo;
	@Autowired
	MultiUnitMstRepository multiUnitMstRepo;
	
	@Autowired
	LocationMstRepository locationMstRepo;
	@Autowired
	LocalCustomerRepository localCustomerRepo;
	@Autowired
	LocalCustomerDtlRepository localCustomerDtlRepo;
	@Autowired
	AddKotTableRepository addKotTableRepo;
	@Autowired
	AddKotWaiterRepository addKotWaiterRepo;
	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepo;
	@Autowired
	KitDefenitionDtlRepository kitDefenitionDtlRepo;
	@Autowired
	JournalHdrRepository journalHdrRepo;
	@Autowired
	JournalDtlRepository journalDtlRepo;
	@Autowired
	JobCardHdrRepository jobCardHdrRepo;
	@Autowired
	JobCardDtlRepository jobCardDtlRepo;
	
	@Autowired
	VoucherNumberService voucherNumberService;
	
	@Autowired
	SysDateMstRepository sysDateMstRepository;

	
	@Autowired
	MessaageToFileWriter mFileWriter;
	
	@Value("${mybranch}")
	private String mybranch;
	@Value("${mycompany}")
	private String mycompany;
	

	 
	public void processedReceivedMessage (String objectType , 
			String payLoad , String replySourceFileName, 
			String destination, String voucherNumber, String replyTargetFileName)   {


		ObjectMapper mapper = new ObjectMapper()
				.registerModule(new ParameterNamesModule()).registerModule(new Jdk8Module())
				.registerModule(new JavaTimeModule());
	 
	 
		KafkaMapleEventType kafkaMapleEventType = KafkaMapleEventType.valueOf(objectType);


		switch (kafkaMapleEventType) {
		case SALESTRANSHDR:

			try {
				SalesTransHdr salesTransHdr = mapper.readValue(payLoad, SalesTransHdr.class);

				logger.info("SalesTransHdr : {} ", salesTransHdr);

				salesTransHdrRepository.save(salesTransHdr);
				
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);

			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ITEMMST:
			try {
				ItemMst itemMst = mapper.readValue(payLoad, ItemMst.class);

				itemMstRepo.save(itemMst);

				logger.info("itemMst extracted from payload : {} ", itemMst);
				
				
				//String sourceFileToDelete =  destination + "$" + voucherNumber + MapleConstants.msgfilextention;;		
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case COMPANYMST:
			try {
				CompanyMst companyMst = mapper.readValue(payLoad, CompanyMst.class);

				companyMstRepo.save(companyMst);

				logger.info("companyMst extracted from payload : {} ", companyMst);
				
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case BRANCHMST:
			try {
				BranchMst branchMst = mapper.readValue(payLoad, BranchMst.class);

				branchMstRepo.save(branchMst);

				logger.info("branchMst extracted from payload : {} ", branchMst);
				
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case USERMST:
			try {
				UserMst userMst = mapper.readValue(payLoad, UserMst.class);

				userMstRepo.save(userMst);

				logger.info("userMst extracted from payload : {} ", userMst);
				
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		
		
		case PURCHASEHDR:
			try {
				PurchaseHdr purchaseHdr = mapper.readValue(payLoad, PurchaseHdr.class);

				purchaseHdrRepo.save(purchaseHdr);

				logger.info("purchaseHdr extracted from payload : {} ", purchaseHdr);
				
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
//		case CUSTOMERMST:
//			try {
//				CustomerMst customerMst = mapper.readValue(payLoad, CustomerMst.class);
//
//				customerMstRepo.save(customerMst);
//
//				logger.info("customerMst extracted from payload : {} ", customerMst);
//			} catch (IOException e) {
//
//				e.printStackTrace();
//			}
//
//			break;
//		case SUPPLIER:
//			try {
//				Supplier supplier = mapper.readValue(payLoad, Supplier.class);
//
//				supplierRepo.save(supplier);
//
//				logger.info("supplier extracted from payload : {} ", supplier);
//			} catch (IOException e) {
//
//				e.printStackTrace();
//			}
//
//			break;
		case PURCHASEDTL:
			try {
				PurchaseDtl purchaseDtl = mapper.readValue(payLoad, PurchaseDtl.class);

				purchaseDtlRepo.save(purchaseDtl);

				logger.info("purchaseDtl extracted from payload : {} ", purchaseDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ITEMBATCHDTL:
			try {
				ItemBatchDtl itemBatchDtl = mapper.readValue(payLoad, ItemBatchDtl.class);

				itemBatchDtlRepo.save(itemBatchDtl);

				logger.info("itemBatchDtl extracted from payload : {} ", itemBatchDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ITEMMERGEMST:
			try {
				ItemMergeMst itemMergeMst = mapper.readValue(payLoad, ItemMergeMst.class);

				itemMergeMstRepo.save(itemMergeMst);

				logger.info("itemMergeMst extracted from payload : {} ", itemMergeMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case CATEGORYMST:
			try {
				CategoryMst categoryMst = mapper.readValue(payLoad, CategoryMst.class);

				CategoryMst oldCategory = categoryMstRepo
						.findByCategoryNameAndCompanyMst(categoryMst.getCategoryName(), categoryMst.getCompanyMst());
				if(null == oldCategory) {
				categoryMstRepo.save(categoryMst);
				}

				logger.info("categoryMst extracted from payload : {} ", categoryMst);
				
				//String sourceFileToDelete =  destination + "$" + voucherNumber + MapleConstants.msgfilextention;;		
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case PAYMENTHDR:
			try {
				PaymentHdr paymentHdr = mapper.readValue(payLoad, PaymentHdr.class);

				paymentHdrRepo.save(paymentHdr);

				logger.info("paymentHdr extracted from payload : {} ", paymentHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case PAYMENTDTL:
			try {
				PaymentDtl paymentDtl = mapper.readValue(payLoad, PaymentDtl.class);

				paymentDtlRepo.save(paymentDtl);

				logger.info("paymentDtl extracted from payload : {} ", paymentDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ACTUALPRODUCTIONHDR:
			try {
				ActualProductionHdr actualProductionHdr = mapper.readValue(payLoad, ActualProductionHdr.class);

				actualProductionHdrRepo.save(actualProductionHdr);

				logger.info("actualProductionHdr extracted from payload : {} ", actualProductionHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ACTUALPRODUCTIONDTL:
			try {
				ActualProductionDtl actualProductionDtl = mapper.readValue(payLoad, ActualProductionDtl.class);

				actualProductionDtlRepo.save(actualProductionDtl);

				logger.info("actualProductionDtl extracted from payload : {} ", actualProductionDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case PURCHASESCHEMEMST:
			try {
				PurchaseSchemeMst purchaseSchemeMst = mapper.readValue(payLoad, PurchaseSchemeMst.class);

				purchaseSchemeMstRepo.save(purchaseSchemeMst);

				logger.info("purchaseSchemeMst extracted from payload : {} ", purchaseSchemeMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case RECEIPTHDR:
			try {
				ReceiptHdr receiptHdr = mapper.readValue(payLoad, ReceiptHdr.class);

				receiptHdrRepo.save(receiptHdr);

				logger.info("receiptHdr extracted from payload : {} ", receiptHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case RECEIPTDTL:
			try {
				ReceiptDtl receiptDtl = mapper.readValue(payLoad, ReceiptDtl.class);

				receiptDtlRepo.save(receiptDtl);

				logger.info("receiptDtl extracted from payload : {} ", receiptDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case RECEIPTMODEMST:
			try {
				ReceiptModeMst receiptModeMst = mapper.readValue(payLoad, ReceiptModeMst.class);

				reciptModeMstRepo.save(receiptModeMst);

				logger.info("receiptModeMst extracted from payload : {} ", receiptModeMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case STOREMST:
			try {
				StoreMst storeMst = mapper.readValue(payLoad, StoreMst.class);

				storeMstRepo.save(storeMst);

				logger.info("storeMst extracted from payload : {} ", storeMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case UNITMST:
			try {
				UnitMst unitMst = mapper.readValue(payLoad, UnitMst.class);

				unitMstRepo.save(unitMst);

				logger.info("unitMst extracted from payload : {} ", unitMst);
				
				//String sourceFileToDelete =  destination + "$" + voucherNumber + MapleConstants.msgfilextention;;		
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ACCOUNTCLASS:
			try {
				AccountClass accountClass = mapper.readValue(payLoad, AccountClass.class);

				accountClassRepo.save(accountClass);

				logger.info("accountClass extracted from payload : {} ", accountClass);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ACCOUNTCLASSFC:
			try {
				AccountClassfc accountClassfc = mapper.readValue(payLoad, AccountClassfc.class);

				accountClassfcRepo.save(accountClassfc);

				logger.info("accountClassfc extracted from payload : {} ", accountClassfc);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ACCOUNTMERGEMST:
			try {
				AccountMergeMst accountMergeMst = mapper.readValue(payLoad, AccountMergeMst.class);

				accountMergeRepo.save(accountMergeMst);

				logger.info("accountMergeMst extracted from payload : {} ", accountMergeMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case CREDITCLASS:
			try {
				CreditClass creditClass = mapper.readValue(payLoad, CreditClass.class);

				creditClassRepo.save(creditClass);

				logger.info("creditClass extracted from payload : {} ", creditClass);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case CREDITCLASSFC:
			try {
				CreditClassfc creditClassfc = mapper.readValue(payLoad, CreditClassfc.class);

				creditClassfcRepo.save(creditClassfc);

				logger.info("creditClassfc extracted from payload : {} ", creditClassfc);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case DEBITCLASS:
			try {
				DebitClass debitClass = mapper.readValue(payLoad, DebitClass.class);

				debitClassRepo.save(debitClass);

				logger.info("debitClass extracted from payload : {} ", debitClass);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case DEBITCLASSFC:
			try {
				DebitClassfc debitClassfc = mapper.readValue(payLoad, DebitClassfc.class);

				debitClassfcRepo.save(debitClassfc);

				logger.info("debitClassfc extracted from payload : {} ", debitClassfc);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case INVENTORYLEDGERMST:
			try {
				InventoryLedgerMst inventoryLedgerMst = mapper.readValue(payLoad, InventoryLedgerMst.class);

				inventoryLedgerMstRepo.save(inventoryLedgerMst);

				logger.info("inventoryLedgerMst extracted from payload : {} ", inventoryLedgerMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case LEDGERCLASS:
			try {
				LedgerClass ledgerClass = mapper.readValue(payLoad, LedgerClass.class);

				ledgerClassRepo.save(ledgerClass);

				logger.info("ledgerClass extracted from payload : {} ", ledgerClass);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case LEDGERCLASSFC:
			try {
				LedgerClassfc ledgerClassfc = mapper.readValue(payLoad, LedgerClassfc.class);

				ledgerClassfcRepo.save(ledgerClassfc);

				logger.info("ledgerClassfc extracted from payload : {} ", ledgerClassfc);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case LINKEDACCOUNTS:
			try {
				LinkedAccounts linkedAccounts = mapper.readValue(payLoad, LinkedAccounts.class);

				linkedAccountsRepo.save(linkedAccounts);

				logger.info("linkedAccounts extracted from payload : {} ", linkedAccounts);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case OPENINGBALANCECONFIGMST:
			try {
				OpeningBalanceConfigMst openingBalanceConfigMst = mapper.readValue(payLoad, OpeningBalanceConfigMst.class);

				openingBalanceConfigMstRepository.save(openingBalanceConfigMst);

				logger.info("openingBalanceConfigMst extracted from payload : {} ", openingBalanceConfigMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case OPENINGBALANCEMST:
			try {
				OpeningBalanceMst openingBalanceMst = mapper.readValue(payLoad, OpeningBalanceMst.class);

				openingBalanceMstRepo.save(openingBalanceMst);

				logger.info("openingBalanceMst extracted from payload : {} ", openingBalanceMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case STOCKTRANSFEROUTHDR:
			try {
				StockTransferOutHdr stockTransferOutHdr = mapper.readValue(payLoad, StockTransferOutHdr.class);

				stockTransferOutHdrRepo.save(stockTransferOutHdr);

				logger.info("stockTransferOutHdr extracted from payload : {} ", stockTransferOutHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case STOCKTRANSFEROUTDTL:
			try {
				StockTransferOutDtl stockTransferOutDtl = mapper.readValue(payLoad, StockTransferOutDtl.class);

				stockTransferOutDtlRepo.save(stockTransferOutDtl);

				logger.info("stockTransferOutDtl extracted from payload : {} ", stockTransferOutDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case STOCKTRANSFERINHDR:
			try {
				StockTransferInHdr stockTransferInHdr = mapper.readValue(payLoad, StockTransferInHdr.class);

				Optional<StockTransferInHdr> stockTransferInHdrInDbOpt = stockTransferInHdrRepo.findById(stockTransferInHdr.getId());
				
				if(stockTransferInHdrInDbOpt.isPresent()) {
					
					logger.info("Dupliczated  stockTransferInHdr : {} ", stockTransferInHdr);
					return;
					
				}
				stockTransferInHdrRepo.save(stockTransferInHdr);

				logger.info("stockTransferInHdr extracted from payload : {} ", stockTransferInHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case STOCKTRANSFERINDTL:
			try {
				StockTransferInDtl stockTransferInDtl = mapper.readValue(payLoad, StockTransferInDtl.class);

				stockTransferInDtlRepo.save(stockTransferInDtl);

				logger.info("stockTransferInDtl extracted from payload : {} ", stockTransferInDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;	
		
		case STOCKTRANSFEROUTDTLARRAY:
			try {
				
				
				
			  StockTransferMessageEntity stockTransferMessageEntity = mapper.readValue(payLoad, StockTransferMessageEntity.class);
 
			 // get out hdr and create inhdr .
			  // Save above inHdr
			  // Get array of out dtl
			  // loop thru array
			  //  create indtl and set value from outdtl
			  // set inhdr of in dtl
			  
			  // save indtl;
			  
			  
			  
			  
			  
			  StockTransferInHdr stockTransferInHdrs = new StockTransferInHdr();
		
			
				stockTransferInHdrs.setCompanyMst(stockTransferMessageEntity.getStockTransferOutHdr().getCompanyMst());
				stockTransferInHdrs.setFromBranch(stockTransferMessageEntity.getStockTransferOutHdr().getFromBranch());
				stockTransferInHdrs.setBranchCode(stockTransferMessageEntity.getStockTransferOutHdr().getToBranch());
				stockTransferInHdrs.setIntentNumber(stockTransferMessageEntity.getStockTransferOutHdr().getIntentNumber());
				stockTransferInHdrs.setInVoucherDate(stockTransferMessageEntity.getStockTransferOutHdr().getVoucherDate());
				
				String voucherNoPreFix = null;
				voucherNoPreFix = SystemSetting.getFinancialYear() + MapleConstants.StockTransferPrefix;
				

				VoucherNumber voucherNo = voucherNumberService.generateInvoice(voucherNoPreFix,
						stockTransferMessageEntity.getStockTransferOutHdr().getCompanyMst().getId());
				String vnoCode  = voucherNo.getCode().substring(9);
			 
				
				
				
			
				stockTransferInHdrs.setVoucherNumber(vnoCode);
				stockTransferInHdrs.setInVoucherNumber(stockTransferMessageEntity.getStockTransferOutHdr().getVoucherNumber());
				
				SysDateMst sysDateMst=sysDateMstRepository.findByCompanyMstAndBranchCode(stockTransferMessageEntity.getStockTransferOutHdr().getCompanyMst(),mybranch);
				
				stockTransferInHdrs.setVoucherDate(sysDateMst.getSystemDate());
				stockTransferInHdrs.setStatusAcceptStock(MapleConstants.AcceptStockStatusPending);
				stockTransferInHdrs.setStockTransferOutHdrId(stockTransferMessageEntity.getStockTransferOutHdr().getId());
	

				stockTransferInHdrs = stockTransferInHdrRepo.save(stockTransferInHdrs);

			  
			// ArrayList<StockTransferInDtl> stockTransferInDtlsArray = new ArrayList();
			
			for(StockTransferOutDtl  stockTransferOutDtl:stockTransferMessageEntity.getStockTransferOutDtlList())
			{
				 
				StockTransferInDtl stockTransferInDtls= new StockTransferInDtl();
				stockTransferInDtls.setAmount(stockTransferOutDtl.getAmount());
				stockTransferInDtls.setBarcode(stockTransferOutDtl.getBarcode());
				stockTransferInDtls.setBatch(stockTransferOutDtl.getBatch());
				stockTransferInDtls.setCompanyMst(stockTransferOutDtl.getCompanyMst());
				stockTransferInDtls.setExpiryDate(stockTransferOutDtl.getExpiryDate());
				stockTransferInDtls.setId(stockTransferOutDtl.getId());
				stockTransferInDtls.setItemCode(stockTransferOutDtl.getItemCode());
				
				Optional<ItemMst> itemmst = itemMstRepo.findById(stockTransferOutDtl.getItemId().getId());
				
				if(itemmst.isPresent()) {
				stockTransferInDtls.setItemId(stockTransferOutDtl.getItemId());
				}else {
					ItemMst itemMst = itemMstRepo.save(stockTransferOutDtl.getItemId());
					stockTransferInDtls.setItemId(itemMst);
				}
				stockTransferInDtls.setMrp(stockTransferOutDtl.getMrp());
				stockTransferInDtls.setQty(stockTransferOutDtl.getQty());
				stockTransferInDtls.setRate(stockTransferOutDtl.getRate());
				stockTransferInDtls.setTaxRate(stockTransferOutDtl.getTaxRate());
				stockTransferInDtls.setUnitId(stockTransferOutDtl.getUnitId());
				stockTransferInDtls.setStockTransferInHdr(stockTransferInHdrs);
			
				
				  stockTransferInDtlRepo.save(stockTransferInDtls);
			
			
			}
		
	
				
				logger.info("stockTransferMessageEntity extracted from payload : {} ", stockTransferMessageEntity);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;	
			
		case ACCOUNTHEADS:
			try {
				AccountHeads accountHeads = mapper.readValue(payLoad, AccountHeads.class);

				accountHeadsRepo.save(accountHeads);

				logger.info("accountHeads extracted from payload : {} ", accountHeads);
				
				//String sourceFileToDelete =  destination + "$" + voucherNumber + MapleConstants.msgfilextention;;		
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;	
		case ACCOUNTPAYABLE:
			try {
				AccountPayable accountPayable = mapper.readValue(payLoad, AccountPayable.class);

				accountPayableRepo.save(accountPayable);

				logger.info("accountPayable extracted from payload : {} ", accountPayable);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;	
		case ACCOUNTRECEIVABLE:
			try {
				AccountReceivable accountReceivable = mapper.readValue(payLoad, AccountReceivable.class);

				accountReceivableRepo.save(accountReceivable);

				logger.info("accountReceivable extracted from payload : {} ", accountReceivable);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ADDITIONALEXPENSE:
			try {
				AdditionalExpense additionalExpense = mapper.readValue(payLoad, AdditionalExpense.class);

				additionalExpenseRepo.save(additionalExpense);

				logger.info("additionalExpense extracted from payload : {} ", additionalExpense);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case BATCHPRICEDEFINITION:
			try {
				BatchPriceDefinition batchPriceDefinition = mapper.readValue(payLoad, BatchPriceDefinition.class);
				
				

				batchPriceDefinitionRepo.save(batchPriceDefinition);

				logger.info("batchPriceDefinition extracted from payload : {} ", batchPriceDefinition);
				
				//String sourceFileToDelete =  destination + "$" + voucherNumber + MapleConstants.msgfilextention;		
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case BRANDMST:
			try {
				BrandMst brandMst = mapper.readValue(payLoad, BrandMst.class);

				brandMstRepo.save(brandMst);

				logger.info("brandMst extracted from payload : {} ", brandMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case CONSUMPTIONDTL:
			try {
				ConsumptionDtl consumptionDtl = mapper.readValue(payLoad, ConsumptionDtl.class);

				consumptionDtlRepo.save(consumptionDtl);

				logger.info("consumptionDtl extracted from payload : {} ", consumptionDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case CONSUMPTIONREASONMST:
			try {
				ConsumptionReasonMst consumptionReasonMst = mapper.readValue(payLoad, ConsumptionReasonMst.class);

				consumptionReasonMstRepo.save(consumptionReasonMst);

				logger.info("consumptionReasonMst extracted from payload : {} ", consumptionReasonMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case DAMAGEHDR:
			try {
				DamageHdr damageHdr = mapper.readValue(payLoad, DamageHdr.class);

				damageHdrRepo.save(damageHdr);

				logger.info("damageHdr extracted from payload : {} ", damageHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case DAMAGEDTL:
			try {
				DamageDtl damageDtl = mapper.readValue(payLoad, DamageDtl.class);

				damageDtlRepo.save(damageDtl);

				logger.info("damageDtl extracted from payload : {} ", damageDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case DAYBOOK:
			try {
				DayBook dayBook = mapper.readValue(payLoad, DayBook.class);

				dayBookRepo.save(dayBook);

				logger.info("dayBook extracted from payload : {} ", dayBook);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case DAYENDCLOSUREHDR:
			try {
				DayEndClosureHdr dayEndClosureHdr = mapper.readValue(payLoad, DayEndClosureHdr.class);

				dayEndClosureRepo.save(dayEndClosureHdr);

				logger.info("dayEndClosureHdr extracted from payload : {} ", dayEndClosureHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case DAYENDCLOSUREDTL:
			try {
				DayEndClosureDtl dayEndClosureDtl = mapper.readValue(payLoad, DayEndClosureDtl.class);

				dayEndClosureDtlRepo.save(dayEndClosureDtl);

				logger.info("dayEndClosureDtl extracted from payload : {} ", dayEndClosureDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case DAYENDREPORTSTORE:
			try {
				DayEndReportStore dayEndReportStore = mapper.readValue(payLoad, DayEndReportStore.class);

				dayEndReportStoreRepo.save(dayEndReportStore);

				logger.info("dayEndReportStore extracted from payload : {} ", dayEndReportStore);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case DELIVERYBOYMST:
			try {
				DeliveryBoyMst deliveryBoyMst = mapper.readValue(payLoad, DeliveryBoyMst.class);

				delivaryBoyMstRepo.save(deliveryBoyMst);

				logger.info("deliveryBoyMst extracted from payload : {} ", deliveryBoyMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case GOODRECEIVENOTEDTL:
			try {
				GoodReceiveNoteDtl goodReceiveNoteDtl = mapper.readValue(payLoad, GoodReceiveNoteDtl.class);

				goodReceiveNoteDtlRepo.save(goodReceiveNoteDtl);

				logger.info("goodReceiveNoteDtl extracted from payload : {} ", goodReceiveNoteDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case GOODRECEIVENOTEHDR:
			try {
				GoodReceiveNoteHdr goodReceiveNoteHdr = mapper.readValue(payLoad, GoodReceiveNoteHdr.class);

				goodReceiveNoteHdrRepo.save(goodReceiveNoteHdr);

				logger.info("goodReceiveNoteHdr extracted from payload : {} ", goodReceiveNoteHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case GROUPMST:
			try {
				GroupMst groupMst = mapper.readValue(payLoad, GroupMst.class);

				groupMstRepo.save(groupMst);

				logger.info("groupMst extracted from payload : {} ", groupMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case GROUPPERMISSIONMST:
			try {
				GroupPermissionMst groupPermissionMst = mapper.readValue(payLoad, GroupPermissionMst.class);

				groupPermissionRepo.save(groupPermissionMst);

				logger.info("groupPermissionMst extracted from payload : {} ", groupPermissionMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case INTENTINHDR:
			try {
				IntentInHdr intentInHdr = mapper.readValue(payLoad, IntentInHdr.class);

				intentInHdrRepo.save(intentInHdr);

				logger.info("intentInHdr extracted from payload : {} ", intentInHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case INTENTINDTL:
			try {
				IntentInDtl intentInDtl = mapper.readValue(payLoad, IntentInDtl.class);

				intentInDtlRepo.save(intentInDtl);

				logger.info("intentInDtl extracted from payload : {} ", intentInDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ITEMBATCHEXPIRYDTL:
			try {
				ItemBatchExpiryDtl itemBatchExpiryDtl = mapper.readValue(payLoad, ItemBatchExpiryDtl.class);

				itemBatchExpiryDtlRepo.save(itemBatchExpiryDtl);

				logger.info("itemBatchExpiryDtl extracted from payload : {} ", itemBatchExpiryDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		
		case KITDEFINITIONMST:
			try {
				KitDefinitionMst kitDefinitionMst = mapper.readValue(payLoad, KitDefinitionMst.class);

				kitDefinitionMstRepo.save(kitDefinitionMst);

				logger.info("kitDefinitionMst extracted from payload : {} ", kitDefinitionMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case KITDEFENITIONDTL:
			try {
				KitDefenitionDtl kitDefenitionDtl = mapper.readValue(payLoad, KitDefenitionDtl.class);

				kitDefenitionDtlRepo.save(kitDefenitionDtl);

				logger.info("kitDefenitionDtl extracted from payload : {} ", kitDefenitionDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ADDKOTTABLE:
			try {
				AddKotTable addKotTable = mapper.readValue(payLoad, AddKotTable.class);

				addKotTableRepo.save(addKotTable);

				logger.info("addKotTable extracted from payload : {} ", addKotTable);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
			
		case ADDKOTWAITER:
			try {
				AddKotWaiter addKotWaiter = mapper.readValue(payLoad, AddKotWaiter.class);

				addKotWaiterRepo.save(addKotWaiter);

				logger.info("addKotWaiter extracted from payload : {} ", addKotWaiter);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case LOCALCUSTOMERMST:
			try {
				LocalCustomerMst localCustomerMst = mapper.readValue(payLoad, LocalCustomerMst.class);

				localCustomerRepo.save(localCustomerMst);

				logger.info("localCustomerMst extracted from payload : {} ", localCustomerMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
			
		case LOCALCUSTOMERDTL:
			try {
				LocalCustomerDtl localCustomerDtl = mapper.readValue(payLoad, LocalCustomerDtl.class);

				localCustomerDtlRepo.save(localCustomerDtl);

				logger.info("localCustomerDtl extracted from payload : {} ", localCustomerDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case LOCATIONMST:
			try {
				LocationMst locationMst = mapper.readValue(payLoad, LocationMst.class);

				locationMstRepo.save(locationMst);

				logger.info("locationMst extracted from payload : {} ", locationMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case MULTIUNITMST:
			try {
				MultiUnitMst multiUnitMst = mapper.readValue(payLoad, MultiUnitMst.class);

				multiUnitMstRepo.save(multiUnitMst);

				logger.info("multiUnitMst extracted from payload : {} ", multiUnitMst);
				
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
				
				
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ORDERTAKERMST:
			try {
				OrderTakerMst orderTakerMst = mapper.readValue(payLoad, OrderTakerMst.class);

				orderTakerMstRepo.save(orderTakerMst);

				logger.info("orderTakerMst extracted from payload : {} ", orderTakerMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
			
		case PDCPAYMENT:
			try {
				PDCPayment pDCPayment = mapper.readValue(payLoad, PDCPayment.class);

				pDCPaymentRepo.save(pDCPayment);

				logger.info("pDCPayment extracted from payload : {} ", pDCPayment);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
			
		case PDCRECEIPTS:
			try {
				PDCReceipts pDCReceipts = mapper.readValue(payLoad, PDCReceipts.class);

				pDCReceiptRepo.save(pDCReceipts);

				logger.info("pDCReceipts extracted from payload : {} ", pDCReceipts);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case PDCRECONCILEHDR:
			try {
				PDCReconcileHdr pDCReconcileHdr = mapper.readValue(payLoad, PDCReconcileHdr.class);

				pDCReconcileHdrRepo.save(pDCReconcileHdr);

				logger.info("pDCReconcileHdr extracted from payload : {} ", pDCReconcileHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case PDCRECONCILEDTL:
			try {
				PDCReconcileDtl pDCReconcileDtl = mapper.readValue(payLoad, PDCReconcileDtl.class);

				pDCReconcileDtlRepo.save(pDCReconcileDtl);

				logger.info("pDCReconcileDtl extracted from payload : {} ", pDCReconcileDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case PRICEDEFENITIONMST:
			try {
				PriceDefenitionMst priceDefenitionMst = mapper.readValue(payLoad, PriceDefenitionMst.class);

				priceDefinitionMstRepo.save(priceDefenitionMst);

				logger.info("priceDefenitionMst extracted from payload : {} ", priceDefenitionMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case PRICEDEFINITION:
			try {
				PriceDefinition priceDefinition = mapper.readValue(payLoad, PriceDefinition.class);
				
			 
				
				if (null != priceDefinition.getUnitId()) {
					priceDefinitionRepo.UpdatePriceDefinitionbyItemIdUnitId(priceDefinition.getItemId(),
							priceDefinition.getStartDate(), priceDefinition.getPriceId(), priceDefinition.getUnitId());
				} else {
					priceDefinitionRepo.UpdatePriceDefinitionbyItemId(priceDefinition.getItemId(),
							priceDefinition.getStartDate(), priceDefinition.getPriceId());

				}
				
				

				priceDefinitionRepo.save(priceDefinition);

				logger.info("priceDefinition extracted from payload : {} ", priceDefinition);
				
			//	String sourceFileToDelete =  destination + "$" + voucherNumber + MapleConstants.msgfilextention;;		
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case PROCESSMST:
			try {
				ProcessMst processMst = mapper.readValue(payLoad, ProcessMst.class);

				processMstRepo.save(processMst);

				logger.info("processMst extracted from payload : {} ", processMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
			
		case PROCESSPERMISSIONMST:
			try {
				ProcessPermissionMst processPermissionMst = mapper.readValue(payLoad, ProcessPermissionMst.class);

				processPermissionRepo.save(processPermissionMst);

				logger.info("processPermissionMst extracted from payload : {} ", processPermissionMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
			
		case PRODUCTCONVERSIONCONFIGMST:
			try {
				ProductConversionConfigMst productConversionConfigMst = mapper.readValue(payLoad, ProductConversionConfigMst.class);

				productConversionConfigMstRepo.save(productConversionConfigMst);

				logger.info("productConversionConfigMst extracted from payload : {} ", productConversionConfigMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
			
		case PRODUCTCONVERSIONDTL:
			try {
				ProductConversionDtl productConversionDtl = mapper.readValue(payLoad, ProductConversionDtl.class);

				productConversionDtlRepo.save(productConversionDtl);

				logger.info("productConversionDtl extracted from payload : {} ", productConversionDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
			
		case PRODUCTMST:
			try {
				ProductMst productMst = mapper.readValue(payLoad, ProductMst.class);

				productMstRepo.save(productMst);

				logger.info("productMst extracted from payload : {} ", productMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case PURCHASEORDERDTL:
			try {
				PurchaseOrderDtl purchaseOrderDtl = mapper.readValue(payLoad, PurchaseOrderDtl.class);

				purchaseOrderDtlRepo.save(purchaseOrderDtl);

				logger.info("purchaseOrderDtl extracted from payload : {} ", purchaseOrderDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;	
		case PURCHASEORDERHDR:
			try {
				PurchaseOrderHdr purchaseOrderHdr = mapper.readValue(payLoad, PurchaseOrderHdr.class);

				purchaseOrderHdrRepo.save(purchaseOrderHdr);

				logger.info("purchaseOrderHdr extracted from payload : {} ", purchaseOrderHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case RAWMATERIALISSUEDTL:
			try {
				RawMaterialIssueDtl rawMaterialIssueDtl = mapper.readValue(payLoad, RawMaterialIssueDtl.class);

				rawMaterialIssueDtlRepo.save(rawMaterialIssueDtl);

				logger.info("rawMaterialIssueDtl extracted from payload : {} ", rawMaterialIssueDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case RAWMATERIALISSUEHDR:
			try {
				RawMaterialIssueHdr rawMaterialIssueHdr = mapper.readValue(payLoad, RawMaterialIssueHdr.class);

				rawMaterialMstRepo.save(rawMaterialIssueHdr);

				logger.info("rawMaterialIssueHdr extracted from payload : {} ", rawMaterialIssueHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case RAWMATERIALRETURNHDR:
			try {
				RawMaterialReturnHdr rawMaterialReturnHdr = mapper.readValue(payLoad, RawMaterialReturnHdr.class);

				rawMaterialReturnHdrRepo.save(rawMaterialReturnHdr);

				logger.info("rawMaterialReturnHdr extracted from payload : {} ", rawMaterialReturnHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case RAWMATERIALRETURNDTL:
			try {
				RawMaterialReturnDtl rawMaterialReturnDtl = mapper.readValue(payLoad, RawMaterialReturnDtl.class);

				rawMaterialReturnDtlRepo.save(rawMaterialReturnDtl);

				logger.info("rawMaterialReturnDtl extracted from payload : {} ", rawMaterialReturnDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case RECEIPTINVOICEDTL:
			try {
				ReceiptInvoiceDtl receiptInvoiceDtl = mapper.readValue(payLoad, ReceiptInvoiceDtl.class);

				receiptInvoiceDtlRepo.save(receiptInvoiceDtl);

				logger.info("receiptInvoiceDtl extracted from payload : {} ", receiptInvoiceDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case REORDERMST:
			try {
				ReorderMst reorderMst = mapper.readValue(payLoad, ReorderMst.class);

				reorderMstRepo.save(reorderMst);

				logger.info("reorderMst extracted from payload : {} ", reorderMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SALEORDERRECEIPT:
			try {
				SaleOrderReceipt saleOrderReceipt = mapper.readValue(payLoad, SaleOrderReceipt.class);

				saleOrderReceiptRepo.save(saleOrderReceipt);

				logger.info("saleOrderReceipt extracted from payload : {} ", saleOrderReceipt);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SALESORDERTRANSHDR:
			try {
				SalesOrderTransHdr salesOrderTransHdr = mapper.readValue(payLoad, SalesOrderTransHdr.class);

				salesOrderTransHdrRepo.save(salesOrderTransHdr);

				logger.info("salesOrderTransHdr extracted from payload : {} ", salesOrderTransHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SALESORDERDTL:
			try {
				SalesOrderDtl salesOrderDtl = mapper.readValue(payLoad, SalesOrderDtl.class);

				salesOrderDtlRepo.save(salesOrderDtl);

				logger.info("salesOrderDtl extracted from payload : {} ", salesOrderDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SALESDTL:
			try {
				SalesDtl salesDtl = mapper.readValue(payLoad, SalesDtl.class);

				salesDetailsRepo.save(salesDtl);

				logger.info("salesDtl extracted from payload : {} ", salesDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SALESRECEIPTS:
			try {
				SalesReceipts salesReceipts = mapper.readValue(payLoad, SalesReceipts.class);

				salesReceiptsRepo.save(salesReceipts);

				logger.info("salesReceipts extracted from payload : {} ", salesReceipts);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SALESDTLDELETED:
			try {
				SalesDtlDeleted salesDtlDeleted = mapper.readValue(payLoad, SalesDtlDeleted.class);

				salesDtlDeletedRepo.save(salesDtlDeleted);

				logger.info("salesDtlDeleted extracted from payload : {} ", salesDtlDeleted);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SALESRETURNDTL:
			try {
				SalesReturnDtl salesReturnDtl = mapper.readValue(payLoad, SalesReturnDtl.class);

				salesRetunDtlRepo.save(salesReturnDtl);

				logger.info("salesReturnDtl extracted from payload : {} ", salesReturnDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SALESRETURNHDR:
			try {
				SalesReturnHdr salesReturnHdr = mapper.readValue(payLoad, SalesReturnHdr.class);

				salesReturnHdrRepo.save(salesReturnHdr);

				logger.info("salesReturnHdr extracted from payload : {} ", salesReturnHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SALESTYPEMST:
			try {
				SalesTypeMst salesTypeMst = mapper.readValue(payLoad, SalesTypeMst.class);

				salesTypeMstRepo.save(salesTypeMst);

				logger.info("salesTypeMst extracted from payload : {} ", salesTypeMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
			
		case SCHELIGIATTRLISTDEF:
			try {
				SchEligiAttrListDef schEligiAttrListDef = mapper.readValue(payLoad, SchEligiAttrListDef.class);

				schEligiAttrListDefRepo.save(schEligiAttrListDef);

				logger.info("schEligiAttrListDef extracted from payload : {} ", schEligiAttrListDef);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHELIGIBILITYATTRIBINST:
			try {
				SchEligibilityAttribInst schEligibilityAttribInst = mapper.readValue(payLoad, SchEligibilityAttribInst.class);

				schEligibilityAttribInstRepo.save(schEligibilityAttribInst);

				logger.info("schEligibilityAttribInst extracted from payload : {} ", schEligibilityAttribInst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHELIGIBILITYDEF:
			try {
				SchEligibilityDef schEligibilityDef = mapper.readValue(payLoad, SchEligibilityDef.class);

				schEligibilityDefRepo.save(schEligibilityDef);

				logger.info("schEligibilityDef extracted from payload : {} ", schEligibilityDef);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHEMEINSTANCE:
			try {
				SchemeInstance schemeInstance = mapper.readValue(payLoad, SchemeInstance.class);

				schemeInstanceRepo.save(schemeInstance);

				logger.info("schemeInstance extracted from payload : {} ", schemeInstance);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHOFFERATTRINST:
			try {
				SchOfferAttrInst schOfferAttrInst = mapper.readValue(payLoad, SchOfferAttrInst.class);

				schOfferAttrInstRepo.save(schOfferAttrInst);

				logger.info("schOfferAttrInst extracted from payload : {} ", schOfferAttrInst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHOFFERATTRLISTDEF:
			try {
				SchOfferAttrListDef schOfferAttrListDef = mapper.readValue(payLoad, SchOfferAttrListDef.class);

				schOfferAttrListDefRepo.save(schOfferAttrListDef);

				logger.info("schOfferAttrListDef extracted from payload : {} ", schOfferAttrListDef);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHOFFERDEF:
			try {
				SchOfferDef schOfferDef = mapper.readValue(payLoad, SchOfferDef.class);

				schOfferDefRepo.save(schOfferDef);

				logger.info("schOfferDef extracted from payload : {} ", schOfferDef);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHSELECTATTRLISTDEF:
			try {
				SchSelectAttrListDef schSelectAttrListDef = mapper.readValue(payLoad, SchSelectAttrListDef.class);

				schSelectAttrListDefReoo.save(schSelectAttrListDef);

				logger.info("schSelectAttrListDef extracted from payload : {} ", schSelectAttrListDef);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHSELECTDEF:
			try {
				SchSelectDef schSelectDef = mapper.readValue(payLoad, SchSelectDef.class);

				schSelectDefRepo.save(schSelectDef);

				logger.info("schSelectDef extracted from payload : {} ", schSelectDef);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHSELECTIONATTRIBINST:
			try {
				SchSelectionAttribInst schSelectionAttribInst = mapper.readValue(payLoad, SchSelectionAttribInst.class);

				schSelectionAttribInstRepo.save(schSelectionAttribInst);

				logger.info("schSelectionAttribInst extracted from payload : {} ", schSelectionAttribInst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SERVICEINHDR:
			try {
				ServiceInHdr serviceInHdr = mapper.readValue(payLoad, ServiceInHdr.class);

				serviceInHdrRepo.save(serviceInHdr);

				logger.info("serviceInHdr extracted from payload : {} ", serviceInHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SERVICEINDTL:
			try {
				ServiceInDtl serviceInDtl = mapper.readValue(payLoad, ServiceInDtl.class);

				serviceInDtlRepo.save(serviceInDtl);

				logger.info("serviceInDtl extracted from payload : {} ", serviceInDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case STORECHANGEMST:
			try {
				StoreChangeMst storeChangeMst = mapper.readValue(payLoad, StoreChangeMst.class);

				storeChangeMstRepo.save(storeChangeMst);

				logger.info("storeChangeMst extracted from payload : {} ", storeChangeMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case STORECHANGEDTL:
			try {
				StoreChangeDtl storeChangeDtl = mapper.readValue(payLoad, StoreChangeDtl.class);

				storeChangeDtlRepo.save(storeChangeDtl);

				logger.info("storeChangeDtl extracted from payload : {} ", storeChangeDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case URSMST:
			try {
				UrsMst ursMst = mapper.readValue(payLoad, UrsMst.class);

				uRSMstRepo.save(ursMst);

				logger.info("ursMst extracted from payload : {} ", ursMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		
		case INTENTDTL:
			try {
				IntentDtl intentDtl = mapper.readValue(payLoad, IntentDtl.class);

				intentDtlRepo.save(intentDtl);

				logger.info("intentDtl extracted from payload : {} ", intentDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case INTENTHDR:
			try {
				IntentHdr intentHdr = mapper.readValue(payLoad, IntentHdr.class);

				intentHdrRepo.save(intentHdr);

				logger.info("intentHdr extracted from payload : {} ", intentHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case INVOICEEDITENABLEMST:
			try {
				InvoiceEditEnableMst invoiceEditEnableMst = mapper.readValue(payLoad, InvoiceEditEnableMst.class);

				invoiceEditEnableRepo.save(invoiceEditEnableMst);

				logger.info("invoiceEditEnableMst extracted from payload : {} ", invoiceEditEnableMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case OTHERBRANCHSALESDTL:
			try {
				OtherBranchSalesDtl otherBranchSalesDtl = mapper.readValue(payLoad, OtherBranchSalesDtl.class);

				otherBranchSalesDtlRepo.save(otherBranchSalesDtl);

				logger.info("otherBranchSalesDtl extracted from payload : {} ", otherBranchSalesDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case OTHERBRANCHSALESTRANSHDR:
			try {
				OtherBranchSalesTransHdr otherBranchSalesTransHdr = mapper.readValue(payLoad, OtherBranchSalesTransHdr.class);

				otherBranchSalesTransHdrRepo.save(otherBranchSalesTransHdr);

				logger.info("otherBranchSalesTransHdr extracted from payload : {} ", otherBranchSalesTransHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case OTHERBRANCHPURCHASEHDR:
			try {
				OtherBranchPurchaseHdr otherBranchPurchaseHdr = mapper.readValue(payLoad, OtherBranchPurchaseHdr.class);

				otherBranchPurchaseHdrRepo.save(otherBranchPurchaseHdr);

				logger.info("otherBranchPurchaseHdr extracted from payload : {} ", otherBranchPurchaseHdr);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case OTHERBRANCHPURCHASEDTL:
			try {
				OtherBranchPurchaseDtl otherBranchPurchaseDtl = mapper.readValue(payLoad, OtherBranchPurchaseDtl.class);

				otherBranchPurchaseDtlRepo.save(otherBranchPurchaseDtl);

				logger.info("otherBranchPurchaseDtl extracted from payload : {} ", otherBranchPurchaseDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case OWNACCOUNT:
			try {
				OwnAccount ownAccount = mapper.readValue(payLoad, OwnAccount.class);

				ownAccountRepo.save(ownAccount);

				logger.info("ownAccount extracted from payload : {} ", ownAccount);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case OWNACCOUNTSETTLEMENTDTL:
			try {
				OwnAccountSettlementDtl ownAccountSettlementDtl = mapper.readValue(payLoad, OwnAccountSettlementDtl.class);

				ownAccountSettlementDtlRepo.save(ownAccountSettlementDtl);

				logger.info("ownAccountSettlementDtl extracted from payload : {} ", ownAccountSettlementDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case OWNACCOUNTSETTLEMENTMST:
			try {
				OwnAccountSettlementMst ownAccountSettlementMst = mapper.readValue(payLoad, OwnAccountSettlementMst.class);

				ownAccountSettlementMstRepo.save(ownAccountSettlementMst);

				logger.info("ownAccountSettlementMst extracted from payload : {} ", ownAccountSettlementMst);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case PAYMENTINVOICEDTL:
			try {
				PaymentInvoiceDtl paymentInvoiceDtl = mapper.readValue(payLoad, PaymentInvoiceDtl.class);

				paymentInvoiceDtlRepo.save(paymentInvoiceDtl);

				logger.info("paymentInvoiceDtl extracted from payload : {} ", paymentInvoiceDtl);
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case ACCEPTSTOCKMESSAGE:
			try {
				
				AcceptStockMessage acceptStockMessage = mapper.readValue(payLoad, AcceptStockMessage.class);
				Optional<StockTransferOutHdr> stocktransferouthdid = stockTransferOutHdrRepo.findById(acceptStockMessage.getStockTransferOutHdrId());
				if(!stocktransferouthdid.isPresent()) {
					  logger.info("Got Inavlid accept Message. Critical Error: {} ", payLoad);
					  logger.info("Got Inavlid accept Message. Critical Error: {} ", acceptStockMessage);
					return;
				}
				
				StockTransferOutHdr stockTransferOutHdr = stocktransferouthdid.get();
				stockTransferOutHdr.setStatus("COMPLETED");
				stockTransferOutHdrRepo.save(stockTransferOutHdr);
				
			   logger.info("paymentInvoiceDtl extracted from payload : {} ", acceptStockMessage);
			   
			   fileMover.moveFromOutProcessToOutProcessArchive(replyTargetFileName);
			   sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHEMEHDRDELETE:
			try {
				
				SchemeInstance schemeInstance = mapper.readValue(payLoad, SchemeInstance.class);
				schemeInstanceRepo.deleteById(schemeInstance.getId());
				
			   logger.info("schemeInstance extracted from payload : {} ", schemeInstance);
			   sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHSELECTIONATTRIBINSTDELETE:
			try {
				
				SchSelectionAttribInst schSelectionAttribInst = mapper.readValue(payLoad, SchSelectionAttribInst.class);
				schSelectionAttribInstRepo.deleteById(schSelectionAttribInst.getId());
				
			   logger.info("schSelectionAttribInst extracted from payload : {} ", schSelectionAttribInst);
			   sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHELIGIATTRIBINSTDELETE:
			try {
				
				SchEligibilityAttribInst schEligibilityAttribInst = mapper.readValue(payLoad, SchEligibilityAttribInst.class);
				schEligibilityAttribInstRepo.delete(schEligibilityAttribInst);
				
			   logger.info("schSelectionAttribInst extracted from payload : {} ", schEligibilityAttribInst);
			   sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;
		case SCHOFFERATTRINSTDELETE:
			try {
				
				SchOfferAttrInst schOfferAttrInst = mapper.readValue(payLoad, SchOfferAttrInst.class);
				schOfferAttrInstRepo.deleteById(schOfferAttrInst.getId());
				
				
			   logger.info("schSelectionAttribInst extracted from payload : {} ", schOfferAttrInst);
			   sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;

			
			
        case SCHEMEMESSAGE:
			
			try {
				SchemeMessage schemeMessage = mapper.readValue(payLoad, SchemeMessage.class);
				
				SchemeInstance schemeInstance = new SchemeInstance();
				schemeInstance = schemeMessage.getSchemeInstance();
				schemeInstanceRepo.save(schemeInstance);
				
				SchOfferDef schOfferDef = new SchOfferDef();
				schOfferDef = schemeMessage.getSchOfferDef();
				schOfferDefRepo.save(schOfferDef);
				for(SchOfferAttrInst schOfferAttrInst : schemeMessage.getSchOfferAttrInst()) {
					schOfferAttrInstRepo.save(schOfferAttrInst);
				}
				for(SchOfferAttrListDef schOfferAttrListDef : schemeMessage.getSchOfferAttrListDef()) {
					schOfferAttrListDefRepo.save(schOfferAttrListDef);
				}
				
				SchEligibilityDef schEligibilityDef = new SchEligibilityDef();
				schEligibilityDef = schemeMessage.getSchEligibilityDef();
				schEligibilityDefRepo.save(schEligibilityDef);
				for(SchEligiAttrListDef schEligiAttrListDef : schemeMessage.getSchEligiAttrListDef()) {
					schEligiAttrListDefRepo.save(schEligiAttrListDef);
				}
				for(SchEligibilityAttribInst schEligibilityAttribInst : schemeMessage.getSchEligibilityAttribInst()) {
					schEligibilityAttribInstRepo.save(schEligibilityAttribInst);
				}
				
				SchSelectDef schSelectDef = new SchSelectDef();
				schSelectDef = schemeMessage.getSchSelectDef();
				schSelectDefRepo.save(schSelectDef);
				for(SchSelectionAttribInst schSelectionAttribInst : schemeMessage.getSchSelectionAttribInst()) {
					schSelectionAttribInstRepo.save(schSelectionAttribInst);
				}
				for(SchSelectAttrListDef schSelectAttrListDef : schemeMessage.getSchSelectAttrListDef()) {
					schSelectAttrListDefReoo.save(schSelectAttrListDef);
				}
				
				logger.info("schemeMessage extracted from payload : {} ", schemeMessage);
				
				//String sourceFileToDelete =  destination + "$" + voucherNumber + MapleConstants.msgfilextention;;		
				sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			}catch (IOException e) {

				e.printStackTrace();
			}
			break;
			
			
        case KITDEFINITIONHDRANDDTL:
			try {
				
				KitDefinitionHdrAndDtlMessageEntity kitDefinitionHdrAndDtlMessageEntity = mapper.readValue(payLoad, KitDefinitionHdrAndDtlMessageEntity.class);
				KitDefinitionMst kitDefMst = new KitDefinitionMst();
				kitDefMst = kitDefinitionHdrAndDtlMessageEntity.getKitDefinitionMst();
				kitDefinitionMstRepo.save(kitDefMst);
				
				for(KitDefenitionDtl kitDefenitionDtl : kitDefinitionHdrAndDtlMessageEntity.getKitDefinitionDtlList()) {
					kitDefenitionDtlRepo.save(kitDefenitionDtl);
				}
				
			   logger.info("kitDefinitionHdrAndDtlMessageEntity extracted from payload : {} ", kitDefinitionHdrAndDtlMessageEntity);
			   sendReplyForProcessedFile(replySourceFileName,replyTargetFileName);
			} catch (IOException e) {

				e.printStackTrace();
			}

			break;

		/*
		 * case REPLYMSG:
		 * 
		 * // Move source file from outgoing folder to outgoing processed,
		 * 
		 * //File from = new File(outgoing_processed_folder + File.separatorChar +
		 * sourceFile);
		 * 
		 * //File to = new File(outgoing_processed_archived_folder + File.separatorChar
		 * + sourceFile); fileMover.moveFromOutProcessToOutProcessArchive(sourceFile);
		 * break;
		 */

		
		default:
			break;
		}
		logger.info("ConsumerRecord : {} ", payLoad);

	}
	
	
	private boolean sendReplyForProcessedFile( String replySourceFileName, String replyTargetFileName) {

		ObjectMapper mapper = new ObjectMapper().registerModule(new ParameterNamesModule())
				.registerModule(new Jdk8Module()).registerModule(new JavaTimeModule());

		JSONParser parser = new JSONParser();
		try {
			FileReader fr = new FileReader(replySourceFileName);
			Object obj = parser.parse(fr);
			fr.close();
			JSONObject jsonObject = (JSONObject) obj;

			String payLoad = (String) jsonObject.get("payLoad");

			KafkaMapleEventType libraryEventType = KafkaMapleEventType.REPLYMSG;
			String desination = (String) jsonObject.get("source");
			String voucherNumber = (String) jsonObject.get("voucherNumber");
			String source = (String) jsonObject.get("destination");
			String objectType = (String) jsonObject.get("objectType");
			Integer libraryEventId = ((Long) jsonObject.get("libraryEventId")).intValue();

			KafkaMapleEvent kafkaMapleEvent = new KafkaMapleEvent();
			kafkaMapleEvent.setDestination(desination);
			kafkaMapleEvent.setLibraryEventId(libraryEventId);
			kafkaMapleEvent.setLibraryEventType(libraryEventType);
			kafkaMapleEvent.setObjectType(KafkaMapleEventType.REPLYMSG.toString());
			kafkaMapleEvent.setPayLoad(payLoad);
			kafkaMapleEvent.setSource(source);
			kafkaMapleEvent.setVoucherNumber(voucherNumber);
			kafkaMapleEvent.setSourceFile(replyTargetFileName);
			
			int hashCodePayLoad = kafkaMapleEvent.getPayLoad().hashCode();
			
			kafkaMapleEvent.setHashCode(hashCodePayLoad);
			mFileWriter.replyMessageWriteToFile( kafkaMapleEvent);
			///mapleConnect.readAndProcessOutGoingFile();
			
//			sendToKafkaTopic.sendKafkaMessageLibraryEvent(kafkaMapleEvent, kafkaMapleEvent.getSource(),
//					kafkaMapleEvent.getDestination(), mapper.writeValueAsString(kafkaMapleEvent));

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
