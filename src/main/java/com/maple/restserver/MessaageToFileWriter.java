package com.maple.restserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.jms.send.KafkaMapleEvent;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.SalesHdrAndDtlMessageEntity;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import java.io.IOException;

import java.nio.file.Files;

import java.nio.file.Path;

import static java.nio.file.StandardCopyOption.*;

import static java.nio.file.LinkOption.*;

@Component
public class MessaageToFileWriter {
	@Autowired
	ObjectMapper objectMapper;

	@Value("${outgoing_folder}")
	private String outgoing_folder;

	
	@Value("${tally_outgoing_folder}")
	private String tally_outgoing_folder;
	
	
	@Value("${outgoing_processed_folder}")
	private String outgoing_processed_folder;

	@Value("${incoming_folder}")
	private String incoming_folder;

	@Value("${incoming_processed_folder}")
	private String incoming_processed_folder;

	@Value("${incoming_processed_archived_folder}")
	private String incoming_processed_archived_folder;

	private static FileWriter file;
	private String file_sufix = ".dat";
	private String name_differentiator = "$";

	public void writeToFile(KafkaMapleEvent kafkaMapleEvent) {

		File theDir = new File(outgoing_folder);
		if (!theDir.exists()) {
			theDir.mkdirs();
		}

		try {

			file = new FileWriter(outgoing_folder + File.separatorChar + kafkaMapleEvent.getSourceFile());

			String outString = objectMapper.writeValueAsString(kafkaMapleEvent);
			file.write(outString);

		} catch (IOException e) {
			e.printStackTrace();

		} finally {

			try {
				file.flush();
				file.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	public void writeToFileTallyOut(KafkaMapleEvent kafkaMapleEvent) {

		File theDir = new File(tally_outgoing_folder);
		if (!theDir.exists()) {
			theDir.mkdirs();
		}

		try {

			file = new FileWriter(tally_outgoing_folder + File.separatorChar + kafkaMapleEvent.getSourceFile());

			String outString = objectMapper.writeValueAsString(kafkaMapleEvent);
			file.write(outString);

		} catch (IOException e) {
			e.printStackTrace();

		} finally {

			try {
				file.flush();
				file.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	
	public void replyMessageWriteToFile(KafkaMapleEvent kafkaMapleEvent) {

		File theDir = new File(outgoing_folder);
		if (!theDir.exists()) {
			theDir.mkdirs();
		}

		try {

			file = new FileWriter(outgoing_folder + File.separatorChar + kafkaMapleEvent.getLibraryEventType()+kafkaMapleEvent.getSourceFile());

			String outString = objectMapper.writeValueAsString(kafkaMapleEvent);
			file.write(outString);

		} catch (IOException e) {
			e.printStackTrace();

		} finally {

			try {
				file.flush();
				file.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
