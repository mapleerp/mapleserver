package com.maple.restserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.jms.send.KafkaMapleEvent;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.SalesHdrAndDtlMessageEntity;
 
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;


import java.io.IOException;

import java.nio.file.Files;

import java.nio.file.Path;



import static java.nio.file.StandardCopyOption.*;

import static java.nio.file.LinkOption.*;


@Component
public class FileProcessorIncoming {
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	ProcessIncomingMessage processIncomingMessage;

	@Value("${outgoing_folder}")
	private String outgoing_folder;

	@Value("${outgoing_processed_folder}")
	private String outgoing_processed_folder;

	@Value("${incoming_folder}")
	private String incoming_folder;

	@Value("${incoming_processed_folder}")
	private String incoming_processed_folder;

	@Value("${incoming_processed_archived_folder}")
	private String incoming_processed_archived_folder;

	private static FileWriter file;
	private String file_sufix = ".dat";
	private String name_differentiator = "$";

	

	public void readAndProcessIncomingFile() {

		File theDir = new File(incoming_processed_folder);
		if(!theDir.exists()) {
			theDir.mkdirs();
		}
		
		File folder = new File(incoming_folder);
		if(null==folder.listFiles() || folder.listFiles().length == 0) {
			return ;
		}
		
		for (File file : folder.listFiles()) {
			if (!file.isDirectory()) {

				if (processFile(file.getAbsolutePath())) {

					File to = new File(incoming_processed_folder + File.separatorChar + file.getName());

					boolean fileCopied = false;
					 
						fileCopied = copyFileToProcessed(file, to);
					 
					if (fileCopied) {
				
						
						
						file.delete();
						
						 
					}

				}

			}
		}

		return;
	}

	
	private boolean copyFileToProcessed(File src, File dest) {
		try {

			Path bytes = Files.copy(

			new java.io.File(src.getAbsolutePath()).toPath(),

			new java.io.File(dest.getAbsolutePath()).toPath(),

			REPLACE_EXISTING,

			COPY_ATTRIBUTES,

			NOFOLLOW_LINKS);

			System.out.println("File successfully copied using Java 7 way");



			} catch (IOException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();
			return false;

			}
		return true;

		
	}
	 

	private boolean processFile(String fileName) {

		JSONParser parser = new JSONParser();
		try {
			FileReader fr = new FileReader(fileName);
			Object obj = parser.parse(fr);
			JSONObject jsonObject = (JSONObject) obj;

			String libraryEventType = (String) jsonObject.get("libraryEventType");
			String payLoad = (String) jsonObject.get("payLoad");
			String destination = (String) jsonObject.get("destination");
			String voucherNumber = (String) jsonObject.get("voucherNumber");
			String sourceFile = (String) jsonObject.get("sourceFile");

			processIncomingMessage.processedReceivedMessage(libraryEventType, payLoad,fileName,
					destination,voucherNumber,sourceFile);

			
			KafkaMapleEventType libraryEventTypeValue = KafkaMapleEventType.valueOf(libraryEventType);
			
			 
			fr.close();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

}
