package com.maple.restserver.accounting.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.AccountBalanceReport;
import com.maple.restserver.report.entity.BranchAssetReport;
import com.maple.restserver.report.entity.PaymentVoucher;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.service.AccountHeadsServiceImpl;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.entity.LedgerClassfc;
import com.maple.restserver.accounting.repository.LedgerClassfcRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.entity.AccountHeads;

@Component
public class AccountingServiceFCImpl implements AccountingServiceFC {
	// -------------version 4.14
	@Autowired
	AccountHeadsServiceImpl accountHeadsServiceImpl;

	// -------------version 4.14
	@Autowired
	LedgerClassfcRepository ledgerClassRepo;

	@Override
	public List<Object> getTrialBalance(String companymstid) {
		// TODO Auto-generated method stub

		return ledgerClassRepo.getTrialBalance(companymstid);
	}

	@Override
	public List<AccountBalanceReport> getAccountStatement(String companymstid, String accountid, Date startingDate,
			Date endigDate, String branchCode) {
		List<AccountBalanceReport> AccountStatementList = new ArrayList();
		boolean found = false;

		List<Object> obj = ledgerClassRepo.getAccountStatement(companymstid, accountid, startingDate, endigDate);
		
		Double closingBalance = ledgerClassRepo.getledgerClosingBalance(endigDate, accountid);
		Double openingBalance = ledgerClassRepo.getledgerBalance(startingDate, accountid);
		
		for (int i = 0; i < obj.size(); i++) {
			found = true;
			Object[] objAray = (Object[]) obj.get(i);
			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();

			

			if (null == openingBalance) {
				openingBalance = 0.0;
			}
			

			if (null == closingBalance) {
				closingBalance = 0.0;
			}

			if (openingBalance < 0) {
				accountBalanceReport.setOpeningBalanceType("Dr");
				accountBalanceReport.setOpeningBalance(openingBalance * -1);
			} else {
				accountBalanceReport.setOpeningBalanceType("Cr");
				accountBalanceReport.setOpeningBalance(openingBalance);
			}

			if (closingBalance < 0) {
				accountBalanceReport.setClosingBalanceType("Dr");
				accountBalanceReport.setClosingBalance(closingBalance * -1);
			} else {
				accountBalanceReport.setClosingBalanceType("Cr");
				accountBalanceReport.setClosingBalance(closingBalance);
			}

			accountBalanceReport.setSlNo(i + 1);
			accountBalanceReport.setDate((Date) objAray[0]);
			accountBalanceReport.setAccountHeads((String) objAray[1]);
			accountBalanceReport.setRemark((String) objAray[2]);
			accountBalanceReport.setCredit((Double) objAray[3]);
			accountBalanceReport.setDebit((Double) objAray[4]);

			accountBalanceReport.setCompanyName((String) objAray[5]);

			AccountStatementList.add(accountBalanceReport);
		}

		if (!found) {
			  openingBalance = ledgerClassRepo.getledgerBalance(startingDate, accountid);

			if (null == openingBalance) {
				openingBalance = 0.0;
			}
			  closingBalance = ledgerClassRepo.getledgerClosingBalance(endigDate, accountid);

			if (null == closingBalance) {
				closingBalance = 0.0;
			}
			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();
			if (openingBalance < 0) {
				accountBalanceReport.setOpeningBalanceType("Dr");
				accountBalanceReport.setOpeningBalance(openingBalance * -1);
			} else {
				accountBalanceReport.setOpeningBalanceType("Cr");
				accountBalanceReport.setOpeningBalance(openingBalance);
			}

			if (closingBalance < 0) {
				accountBalanceReport.setClosingBalanceType("Dr");
				accountBalanceReport.setClosingBalance(closingBalance * -1);
			} else {
				accountBalanceReport.setClosingBalanceType("Cr");
				accountBalanceReport.setClosingBalance(closingBalance);
			}

			AccountStatementList.add(accountBalanceReport);

		}

		return AccountStatementList;

	}

	@Override
	public Double getAccountBalance(String accountName) {

		return ledgerClassRepo.getAccountBalance(accountName);
	}

	@Override
	public Double getOpeningAccountBalance(String accountName, Date startingDate) {

		return ledgerClassRepo.getOpeningBalance(accountName, startingDate);
	}

	@Override
	public Double getClosingAccountBalance(String accountName, Date endingDate) {

		return ledgerClassRepo.getClosingBalance(accountName, endingDate);
	}

	public List<AccountBalanceReport> getSupplierAccountStatement(String companymstid, String accountid, Date sDate,
			Date eDate, String branchCode) {
		List<AccountBalanceReport> AccountStatementList = new ArrayList();
		List<Object> obj = ledgerClassRepo.getSupplierAccountStatement(companymstid, accountid, sDate, eDate);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();

			Double openingBalance = ledgerClassRepo.getledgerBalance(sDate, accountid);
			Double closingBalance = ledgerClassRepo.getledgerClosingBalance(eDate, accountid);

			if (openingBalance != null) {

				if (openingBalance < 0) {
					accountBalanceReport.setOpeningBalanceType("Debit");
					accountBalanceReport.setOpeningBalance(openingBalance * -1);
				} else {
					accountBalanceReport.setOpeningBalanceType("Credit");
					accountBalanceReport.setOpeningBalance(openingBalance);
				}
			}

			if (closingBalance != null) {

				if (closingBalance < 0) {
					accountBalanceReport.setClosingBalanceType("Debit");
					accountBalanceReport.setClosingBalance(closingBalance * -1);
				} else {
					accountBalanceReport.setClosingBalanceType("Credit");
					accountBalanceReport.setClosingBalance(closingBalance);
				}

			}

			accountBalanceReport.setSlNo(i + 1);
			accountBalanceReport.setDate((Date) objAray[5]);
			accountBalanceReport.setAccountHeads((String) objAray[1]);
			accountBalanceReport.setRemark((String) objAray[1]);
			accountBalanceReport.setCredit((Double) objAray[2]);
			accountBalanceReport.setDebit((Double) objAray[3]);

			accountBalanceReport.setCompanyName((String) objAray[4]);

			AccountStatementList.add(accountBalanceReport);
		}
		return AccountStatementList;
	}



	@Override
	public List<Object> getTrialBalance(String companymstid, Date date) {
		return ledgerClassRepo.getTrialBalanceNew(companymstid, date);
	}

	@Override
	public List<Object> getSundryDebtorBalance(String companymstid, Date date) {
		return ledgerClassRepo.getSundryDebtorsBalanceNew(companymstid, date);

	}

	public List<AccountBalanceReport> getBankAccountStatement(String companymstid, Date startingDate, Date endigDate,
			String branchCode) {
		List<AccountBalanceReport> AccountStatementList = new ArrayList();
		boolean found = false;

		List<AccountHeads> bankAccounts = accountHeadsServiceImpl.findByBankAccount(companymstid);

		for (AccountHeads accountHeads : bankAccounts) {
			String accountid = accountHeads.getId();

			List<Object> obj = ledgerClassRepo.getAccountStatement(companymstid, accountid, startingDate, endigDate);
			for (int i = 0; i < obj.size(); i++) {
				found = true;
				Object[] objAray = (Object[]) obj.get(i);
				AccountBalanceReport accountBalanceReport = new AccountBalanceReport();

				Double openingBalance = ledgerClassRepo.getledgerBalance(startingDate, accountid);

				if (null == openingBalance) {
					openingBalance = 0.0;
				}
				Double closingBalance = ledgerClassRepo.getledgerClosingBalance(endigDate, accountid);

				if (null == closingBalance) {
					closingBalance = 0.0;
				}

				if (openingBalance < 0) {
					accountBalanceReport.setOpeningBalanceType("Dr");
					accountBalanceReport.setOpeningBalance(openingBalance * -1);
				} else {
					accountBalanceReport.setOpeningBalanceType("Cr");
					accountBalanceReport.setOpeningBalance(openingBalance);
				}

				if (closingBalance < 0) {
					accountBalanceReport.setClosingBalanceType("Dr");
					accountBalanceReport.setClosingBalance(closingBalance * -1);
				} else {
					accountBalanceReport.setClosingBalanceType("Cr");
					accountBalanceReport.setClosingBalance(closingBalance);
				}

				accountBalanceReport.setSlNo(i + 1);
				accountBalanceReport.setDate((Date) objAray[0]);
				accountBalanceReport.setAccountHeads((String) objAray[1]);
				accountBalanceReport.setRemark((String) objAray[2]);
				accountBalanceReport.setCredit((Double) objAray[3]);
				accountBalanceReport.setDebit((Double) objAray[4]);

				accountBalanceReport.setCompanyName((String) objAray[5]);

				AccountStatementList.add(accountBalanceReport);
			}

			if (!found) {
				Double openingBalance = ledgerClassRepo.getledgerBalance(startingDate, accountid);

				if (null == openingBalance) {
					openingBalance = 0.0;
				}
				Double closingBalance = ledgerClassRepo.getledgerClosingBalance(endigDate, accountid);

				if (null == closingBalance) {
					closingBalance = 0.0;
				}
				AccountBalanceReport accountBalanceReport = new AccountBalanceReport();
				if (openingBalance < 0) {
					accountBalanceReport.setOpeningBalanceType("Dr");
					accountBalanceReport.setOpeningBalance(openingBalance * -1);
				} else {
					accountBalanceReport.setOpeningBalanceType("Cr");
					accountBalanceReport.setOpeningBalance(openingBalance);
				}

				if (closingBalance < 0) {
					accountBalanceReport.setClosingBalanceType("Dr");
					accountBalanceReport.setClosingBalance(closingBalance * -1);
				} else {
					accountBalanceReport.setClosingBalanceType("Cr");
					accountBalanceReport.setClosingBalance(closingBalance);
				}

				AccountStatementList.add(accountBalanceReport);

			}

		}
		return AccountStatementList;

	}

	public List<AccountBalanceReport> getAccountBalanceBetweendate(Date eDate, Date sDate) {

		List<AccountBalanceReport> AccountStatementList = new ArrayList();
		List<Object> objList = ledgerClassRepo.getAccountBalanceBetweendate(eDate, sDate);
		for (int i = 0; i < objList.size(); i++) {
			Object[] objAray = (Object[]) objList.get(i);
			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();

			accountBalanceReport.setAccountHeads((String) objAray[3]);
			accountBalanceReport.setClosingBalance((Double) objAray[2]);
			accountBalanceReport.setDebit((Double) objAray[0]);
			accountBalanceReport.setCredit((Double) objAray[1]);
			if (accountBalanceReport.getClosingBalance() < 0) {
				accountBalanceReport.setClosingBalanceType("Dr");
				accountBalanceReport.setClosingBalance(accountBalanceReport.getClosingBalance() * -1);
			} else {
				accountBalanceReport.setClosingBalanceType("Cr");
				accountBalanceReport.setClosingBalance(accountBalanceReport.getClosingBalance());
			}
			AccountStatementList.add(accountBalanceReport);
		}
		return AccountStatementList;
	}

	@Override
	public Double getCustomerBalance(String companymstid, Date date, String custid) {
		return ledgerClassRepo.getCustomerBalance(companymstid, date, custid);
	}

	@Override
	public List<BranchAssetReport> getCustomerClosingBalance(String companymstid, Date date) {

		List<BranchAssetReport> accountBalanceReportList = new ArrayList<BranchAssetReport>();

		List<Object> objectList = ledgerClassRepo.getCustomerClosingBalance(companymstid, date);

		for (int i = 0; i < objectList.size(); i++) {
			Object[] objAray = (Object[]) objectList.get(i);
			BranchAssetReport accountBalanceReport = new BranchAssetReport();

			accountBalanceReport.setName((String) objAray[0]);
			accountBalanceReport.setAmount((Double) objAray[1]);

			accountBalanceReportList.add(accountBalanceReport);

		}

		return accountBalanceReportList;
	}

	@Override
	public List<BranchAssetReport> getSupplierClosingBalance(String companymstid, Date date) {
		List<BranchAssetReport> accountBalanceReportList = new ArrayList<BranchAssetReport>();

		List<Object> objectList = ledgerClassRepo.getSupplierClosingBalance(companymstid, date);

		for (int i = 0; i < objectList.size(); i++) {
			Object[] objAray = (Object[]) objectList.get(i);
			BranchAssetReport accountBalanceReport = new BranchAssetReport();

			accountBalanceReport.setName((String) objAray[0]);
			accountBalanceReport.setAmount((Double) objAray[1]);

			accountBalanceReportList.add(accountBalanceReport);

		}

		return accountBalanceReportList;
	}

	@Override
	public List<BranchAssetReport> getBankAccountClosingBalance(String companymstid, Date date) {
		
		
		List<BranchAssetReport> accountBalanceReportList = new ArrayList<BranchAssetReport>();
		List<AccountHeads> bankAccounts = accountHeadsServiceImpl.findByBankAccount(companymstid);

		for (AccountHeads accountHeads : bankAccounts) {

			Double closingBalance = ledgerClassRepo.getBankAccountClosingBalance(companymstid, date,accountHeads.getId());

			BranchAssetReport accountBalanceReport = new BranchAssetReport();

				accountBalanceReport.setName(accountHeads.getAccountName());
				if(null == closingBalance)
				{
					closingBalance = 0.0;
				}
				accountBalanceReport.setAmount(closingBalance);

				accountBalanceReportList.add(accountBalanceReport);


		}
		return accountBalanceReportList;

	}

	@Override
	public List<BranchAssetReport> getTotalBranchAsset(String companymstid, Date date) {

		List<BranchAssetReport> accountBalanceReportList = new ArrayList<BranchAssetReport>();

		
		return null;
	}

	 
	
	@Override
	public LedgerClassfc saveLedger(LedgerClassfc ledgerClass) {
		ledgerClass.setCreditAmount(ledgerClass.getCreditAmount().setScale(2, BigDecimal.ROUND_HALF_EVEN));
		ledgerClass.setDebitAmount(ledgerClass.getDebitAmount().setScale(2, BigDecimal.ROUND_HALF_EVEN));

		ledgerClass = ledgerClassRepo.saveAndFlush(ledgerClass);
		return ledgerClass;
	}
	

	// -----------------version 4.14 end
}
