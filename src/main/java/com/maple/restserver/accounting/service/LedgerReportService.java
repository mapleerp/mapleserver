package com.maple.restserver.accounting.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.report.entity.LedgerReport;

@Service
@Transactional
public interface LedgerReportService {
	
	
	public List<LedgerReport> getLedgerReport( );
	

}
