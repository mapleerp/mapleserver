package com.maple.restserver.accounting.service;

import java.util.Date;
import java.util.List;

import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.entity.OpeningBalanceMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.AccountBalanceReport;
import com.maple.restserver.report.entity.BranchAssetReport;

public interface OpeningBalanceMstService {

	OpeningBalanceMst findByAccountIdAndCompanyMst(CompanyMst companyMst);
	
	
}
