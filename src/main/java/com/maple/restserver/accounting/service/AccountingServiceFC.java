package com.maple.restserver.accounting.service;

import java.util.Date;
import java.util.List;


import com.maple.restserver.accounting.entity.LedgerClassfc;
import com.maple.restserver.report.entity.AccountBalanceReport;
import com.maple.restserver.report.entity.BranchAssetReport;

public interface AccountingServiceFC {

	List<Object> getTrialBalance(String companymstid);
	
	Double getAccountBalance(String accountName);
	List<AccountBalanceReport>  getAccountStatement(String companymstid, String accountName, Date startingDate, Date endigDate,String branchCode); 
	Double getOpeningAccountBalance(String accountName, Date endingDate);
	Double getClosingAccountBalance(String accountName, Date endingDate);
	LedgerClassfc saveLedger(LedgerClassfc ledgerClass);
	List<Object> getSundryDebtorBalance(String companymstid, Date date);

	List<Object> getTrialBalance(String companymstid, Date date);

	Double getCustomerBalance(String companymstid, Date date, String custid);

	List<BranchAssetReport> getCustomerClosingBalance(String companymstid, Date date);

	List<BranchAssetReport> getSupplierClosingBalance(String companymstid, Date date);

	List<BranchAssetReport> getBankAccountClosingBalance(String companymstid, Date date);

	List<BranchAssetReport> getTotalBranchAsset(String companymstid, Date date);
}
