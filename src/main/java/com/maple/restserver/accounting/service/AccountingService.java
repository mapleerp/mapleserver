package com.maple.restserver.accounting.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.report.entity.AccountBalanceReport;
import com.maple.restserver.report.entity.BranchAssetReport;
import com.maple.restserver.report.entity.ExpenseReport;

public interface AccountingService {

	List<Object> getTrialBalance(String companymstid);
	
	Double getAccountBalance(String accountName);
	List<AccountBalanceReport>  getAccountStatement(String companymstid, String accountName, Date startingDate, Date endigDate,String branchCode); 
	Double getOpeningAccountBalance(String accountName, Date endingDate);
	Double getClosingAccountBalance(String accountName, Date endingDate);
	LedgerClass saveLedger(LedgerClass ledgerClass);
	List<Object> getSundryDebtorBalance(String companymstid, Date date);

	List<Object> getTrialBalance(String companymstid, Date date);
	
	List<Object> getTrialBalance(String companymstid, Date startDate, Date endDate, String userId, Date reportDate);

	Double getCustomerBalance(String companymstid, Date date, String custid);

	List<BranchAssetReport> getCustomerClosingBalance(String companymstid, Date date);

	List<BranchAssetReport> getSupplierClosingBalance(String companymstid, Date date);

	List<BranchAssetReport> getBankAccountClosingBalance(String companymstid, Date date);

	List<BranchAssetReport> getTotalBranchAsset(String companymstid, Date date);

	void changeFinancialYr(Date enddate, String companymstid,Integer finanayrid);

	List<AccountBalanceReport> getTrialBalanceByParentId(String companymstid, Date date, String parentid);

	List<AccountBalanceReport> getTrialBalanceById(String companymstid, Date date, String accountid);
	
	String getRootParentOfAccount(String accountName);

	List<AccountBalanceReport> getTrialBalanceByIncomeParentId(String companymstid, Date date, String parentid);

	List<AccountBalanceReport> getBranchWiseAccountClosingBalance(String companymstid, Date date,String accountid);


	void deleteBySourceVoucherNumber(String voucherNumber );
	
	boolean isChilcAccountOf(String accountId, String companMstID, String searchParentAccountNAme) ;
	
	public List<AccountHeads> getallChildAccounts(String accountName, String companMstID) ;
	
	

	
	
}
