package com.maple.restserver.accounting.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.AccountBalanceReport;
import com.maple.restserver.report.entity.BranchAssetReport;
import com.maple.restserver.report.entity.ExpenseReport;
import com.maple.restserver.report.entity.PaymentVoucher;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.FinancialYearMstRepository;
import com.maple.restserver.repository.TrialBalanceDtlRepository;
import com.maple.restserver.repository.TrialBalanceHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.AccountHeadsServiceImpl;
import com.maple.restserver.service.SaveAndPublishServiceImpl;
import com.maple.restserver.utils.SystemSetting;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.entity.OpeningBalanceMst;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.accounting.repository.OpeningBalanceConfigMstRepository;
import com.maple.restserver.accounting.repository.OpeningBalanceMstRepository;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.FinancialYearMst;

import com.maple.restserver.report.entity.AccountBalanceReport;
import com.maple.restserver.report.entity.BranchAssetReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.FinancialYearMstRepository;
import com.maple.restserver.service.AccountHeadsServiceImpl;
import com.maple.restserver.utils.SystemSetting;

import com.maple.restserver.entity.TrialBalanceDtl;
import com.maple.restserver.entity.TrialBalanceHdr;

@Component
public class AccountingServiceImpl implements AccountingService {
	// -------------version 4.14
	
	@Autowired
	DebitClassRepository debitclassRepo;

	@Value("${spring.datasource.url}")
	private String dbType;

	@Autowired
	CreditClassRepository creditClassRepo;
	@Autowired
	AccountClassRepository accountClassRepo;
	@Autowired
	BranchMstRepository branchMstRepo;
 
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	AccountClassRepository accountClassRepository;
	@Autowired
	OpeningBalanceMstRepository openingBalanceMstRepo;
	@Autowired
	FinancialYearMstRepository financialYrMstRepo;
	@Autowired
	OpeningBalanceConfigMstRepository openingBalanceConfigMstRepo;
	@Autowired
	AccountHeadsServiceImpl accountHeadsServiceImpl;

	/*
	 * @Autowired CustomerMstRepository customerMstRepository;
	 */


	@Autowired
	AccountHeadsRepository accountHeadsRepo;
	// -------------version 4.14
	@Autowired
	LedgerClassRepository ledgerClassRepo;
	
	@Autowired
	TrialBalanceHdrRepository trialBalanceHdrRepository;
	@Autowired
	TrialBalanceDtlRepository trialBalanceDtlRepository;
	
	@Autowired
	SaveAndPublishServiceImpl saveAndPublishServiceImpl;
	
	@Autowired
	private VoucherNumberService voucherService;

	@Override
	public List<Object> getTrialBalance(String companymstid) {
		// TODO Auto-generated method stub

		return ledgerClassRepo.getTrialBalance(companymstid);
	}

	@Override
	public List<AccountBalanceReport> getAccountStatement(String companymstid, String accountid, Date startingDate,
			Date endigDate, String branchCode) {
		List<AccountBalanceReport> AccountStatementList = new ArrayList();
		boolean found = false;


		Optional<AccountHeads> customerMstOpt = accountHeadsRepo.findById(accountid);

//		Optional<Supplier> supplierOpt = supplierRepository.findById(accountid);

		List<Object> obj = null;
		
		java.sql.Date sqlStartingDate ;
		
		java.sql.Date sqlEndingDate ;
		
		sqlStartingDate = SystemSetting.UtilDateToSQLDate(startingDate);
		
		sqlEndingDate = SystemSetting.UtilDateToSQLDate(endigDate);
		
		if (customerMstOpt.isPresent() ) {
			obj = ledgerClassRepo.getAccountStatementMultipleEntryFormat(companymstid, accountid, sqlStartingDate,
					sqlEndingDate);
		} else {
			obj = ledgerClassRepo.getAccountStatementMultipleEntryFormat(companymstid, accountid, sqlStartingDate,
					sqlEndingDate);
		}

		Double closingBalance = ledgerClassRepo.getledgerClosingBalance(sqlEndingDate, accountid);
		Double openingBalance = ledgerClassRepo.getledgerBalance(sqlStartingDate, accountid);

		for (int i = 0; i < obj.size(); i++) {
			found = true;
			Object[] objAray = (Object[]) obj.get(i);
			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();

			if (null == openingBalance) {
				openingBalance = 0.0;
			}

			if (null == closingBalance) {
				closingBalance = 0.0;
			}

			if (openingBalance < 0) {
				accountBalanceReport.setOpeningBalanceType("Dr");
				accountBalanceReport.setOpeningBalance(openingBalance * -1);
			} else {
				accountBalanceReport.setOpeningBalanceType("Cr");
				accountBalanceReport.setOpeningBalance(openingBalance);
			}

			if (closingBalance < 0) {
				accountBalanceReport.setClosingBalanceType("Dr");
				accountBalanceReport.setClosingBalance(closingBalance * -1);
			} else {
				accountBalanceReport.setClosingBalanceType("Cr");
				accountBalanceReport.setClosingBalance(closingBalance);
			}

			Date trasDate = (Date) objAray[0];
			String accountHead = (String) objAray[1];
			System.out.println("Value == "+objAray[2]);
			String remark = (String) objAray[2];
			Double credit = (Double) objAray[3];

			if (null == credit) {
				credit = 0.0;
			}
			Double debit = (Double) objAray[4];

			if (null == debit) {
				debit = 0.0;
			}

			String companyName = (String) objAray[5];

			String voucherType = (String) (objAray[7]);
			String accountClassId = (String) objAray[6];

			accountBalanceReport.setSlNo(i + 1);
			accountBalanceReport.setDate(trasDate);
			accountBalanceReport.setAccountHeads(accountHead);
			accountBalanceReport.setRemark(voucherType);
			accountBalanceReport.setCredit(credit);
			accountBalanceReport.setDebit(debit);

			accountBalanceReport.setCompanyName(companyName);
			accountBalanceReport.setDescription(remark);
			accountBalanceReport.setAccountClassId(accountClassId);
			String voucherDate=SystemSetting.UtilDateToString(trasDate);
			accountBalanceReport.setvDate(voucherDate);
			AccountStatementList.add(accountBalanceReport);
		}

		if (!found) {
			openingBalance = ledgerClassRepo.getledgerBalance(sqlStartingDate, accountid);

			if (null == openingBalance) {
				openingBalance = 0.0;
			}
			closingBalance = ledgerClassRepo.getledgerClosingBalance(sqlEndingDate, accountid);

			if (null == closingBalance) {
				closingBalance = 0.0;
			}
			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();
			if (openingBalance < 0) {
				accountBalanceReport.setOpeningBalanceType("Dr");
				accountBalanceReport.setOpeningBalance(openingBalance * -1);
			} else {
				accountBalanceReport.setOpeningBalanceType("Cr");
				accountBalanceReport.setOpeningBalance(openingBalance);
			}

			if (closingBalance < 0) {
				accountBalanceReport.setClosingBalanceType("Dr");
				accountBalanceReport.setClosingBalance(closingBalance * -1);
			} else {
				accountBalanceReport.setClosingBalanceType("Cr");
				accountBalanceReport.setClosingBalance(closingBalance);
			}

			AccountStatementList.add(accountBalanceReport);

		}

		return AccountStatementList;

	}

	@Override
	public Double getAccountBalance(String accountName) {

		return ledgerClassRepo.getAccountBalance(accountName);
	}

	@Override
	public Double getOpeningAccountBalance(String accountName, Date startingDate) {

		return ledgerClassRepo.getOpeningBalance(accountName, startingDate);
	}

	@Override
	public Double getClosingAccountBalance(String accountName, Date endingDate) {

		return ledgerClassRepo.getClosingBalance(accountName, endingDate);
	}

	public List<AccountBalanceReport> getSupplierAccountStatement(String companymstid, String accountid, Date sDate,
			Date eDate, String branchCode) {
		List<AccountBalanceReport> AccountStatementList = new ArrayList();
		List<Object> obj = ledgerClassRepo.getSupplierAccountStatement(companymstid, accountid, sDate, eDate);
		for (int i = 0; i < obj.size(); i++) {
			Object[] objAray = (Object[]) obj.get(i);
			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();

			Double openingBalance = ledgerClassRepo.getledgerBalance(sDate, accountid);
			Double closingBalance = ledgerClassRepo.getledgerClosingBalance(eDate, accountid);

			if (openingBalance != null) {

				if (openingBalance < 0) {
					accountBalanceReport.setOpeningBalanceType("Debit");
					accountBalanceReport.setOpeningBalance(openingBalance * -1);
				} else {
					accountBalanceReport.setOpeningBalanceType("Credit");
					accountBalanceReport.setOpeningBalance(openingBalance);
				}
			}

			if (closingBalance != null) {

				if (closingBalance < 0) {
					accountBalanceReport.setClosingBalanceType("Debit");
					accountBalanceReport.setClosingBalance(closingBalance * -1);
				} else {
					accountBalanceReport.setClosingBalanceType("Credit");
					accountBalanceReport.setClosingBalance(closingBalance);
				}

			}

			accountBalanceReport.setSlNo(i + 1);
			accountBalanceReport.setDate((Date) objAray[5]);
			accountBalanceReport.setAccountHeads((String) objAray[1]);
			accountBalanceReport.setRemark((String) objAray[1]);
			accountBalanceReport.setCredit((Double) objAray[2]);
			accountBalanceReport.setDebit((Double) objAray[3]);

			accountBalanceReport.setCompanyName((String) objAray[4]);

			AccountStatementList.add(accountBalanceReport);
		}
		return AccountStatementList;
	}

	@Override
	public LedgerClass saveLedger(LedgerClass ledgerClass) {
		ledgerClass.setCreditAmount(ledgerClass.getCreditAmount().setScale(2, BigDecimal.ROUND_HALF_EVEN));
		ledgerClass.setDebitAmount(ledgerClass.getDebitAmount().setScale(2, BigDecimal.ROUND_HALF_EVEN));

//		ledgerClass = ledgerClassRepo.saveAndFlush(ledgerClass);
		ledgerClass = saveAndPublishServiceImpl.saveLedgerClass(ledgerClass, ledgerClass.getBranchCode());
		return ledgerClass;
	}

	
	@Override
	public List<Object> getTrialBalance(String companymstid, Date date) {
		return ledgerClassRepo.getTrialBalanceNew(companymstid, date);
	}

	@Override
	public List<Object> getTrialBalance(String companymstid, Date startDate, Date endDate, String userId, Date reportDate) {
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		TrialBalanceHdr trialBalanceHdrSaved=new TrialBalanceHdr();
		if(null != startDate && null != endDate) {
			TrialBalanceHdr trialBalanceHdr=new TrialBalanceHdr();
		
			trialBalanceHdr.setFromDate(startDate);
			trialBalanceHdr.setToDate(endDate);
			trialBalanceHdr.setUserId(userId);
			trialBalanceHdr.setCompanyMst(companyMst);
			VoucherNumber voucherNo = voucherService.generateInvoice(reportDate.toString(), "TB");
			trialBalanceHdr.setReportId(voucherNo.getCode());
			trialBalanceHdr.setReportDate(reportDate);
			trialBalanceHdrSaved=trialBalanceHdrRepository.save(trialBalanceHdr);
		}
		
		List<Object> obj = new ArrayList<>();
		if (dbType.contains("mysql")) {
			obj = ledgerClassRepo.getTrialBalanceNew(companymstid, startDate);
			for (int i = 0; i < obj.size(); i++) {
				Object[] objAray = (Object[]) obj.get(i);
				TrialBalanceDtl trialBalanceDtl=new TrialBalanceDtl();
				trialBalanceDtl.setAccountId((String) objAray[0]);
				trialBalanceDtl.setCreditAmount((Double) objAray[1]);
				trialBalanceDtl.setDebitAmount((Double) objAray[2]);
				
				trialBalanceDtl.setTrialBalanceHdr(trialBalanceHdrSaved);
				trialBalanceDtlRepository.save(trialBalanceDtl);
			}
		}else if (dbType.contains("derby")) {
			obj = ledgerClassRepo.getTrialBalanceNewInDerby(companymstid, startDate);
			for (int i = 0; i < obj.size(); i++) {
				Object[] objAray = (Object[]) obj.get(i);
				TrialBalanceDtl trialBalanceDtl=new TrialBalanceDtl();
				trialBalanceDtl.setAccountId((String) objAray[0]);
				
				BigDecimal bdCredit =  (BigDecimal) objAray[1];
				Double dbCredit = bdCredit.doubleValue();

				trialBalanceDtl.setCreditAmount(dbCredit);
				
				BigDecimal bdDebit =  (BigDecimal) objAray[2];
				Double dbDebit = bdDebit.doubleValue();
				trialBalanceDtl.setDebitAmount(dbDebit);
				
				trialBalanceDtl.setTrialBalanceHdr(trialBalanceHdrSaved);
				trialBalanceDtlRepository.save(trialBalanceDtl);
			}
		}
			
			
		
		return obj;
	}

	@Override
	public List<Object> getSundryDebtorBalance(String companymstid, Date date) {
		return ledgerClassRepo.getSundryDebtorsBalanceNew(companymstid, date);

	}

	public List<AccountBalanceReport> getBankAccountStatement(String companymstid, Date startingDate, Date endigDate,
			String branchCode) {
		List<AccountBalanceReport> AccountStatementList = new ArrayList();
		boolean found = false;

		List<AccountHeads> bankAccounts = accountHeadsServiceImpl.findByBankAccount(companymstid);

		for (AccountHeads accountHeads : bankAccounts) {
			String accountid = accountHeads.getId();

			List<Object> obj = ledgerClassRepo.getAccountStatementMultipleEntryFormat(companymstid, accountid,
					startingDate, endigDate);
			for (int i = 0; i < obj.size(); i++) {
				found = true;
				Object[] objAray = (Object[]) obj.get(i);
				AccountBalanceReport accountBalanceReport = new AccountBalanceReport();

				Double openingBalance = ledgerClassRepo.getledgerBalance(startingDate, accountid);

				if (null == openingBalance) {
					openingBalance = 0.0;
				}
				Double closingBalance = ledgerClassRepo.getledgerClosingBalance(endigDate, accountid);

				if (null == closingBalance) {
					closingBalance = 0.0;
				}

				if (openingBalance < 0) {
					accountBalanceReport.setOpeningBalanceType("Dr");
					accountBalanceReport.setOpeningBalance(openingBalance * -1);
				} else {
					accountBalanceReport.setOpeningBalanceType("Cr");
					accountBalanceReport.setOpeningBalance(openingBalance);
				}

				if (closingBalance < 0) {
					accountBalanceReport.setClosingBalanceType("Dr");
					accountBalanceReport.setClosingBalance(closingBalance * -1);
				} else {
					accountBalanceReport.setClosingBalanceType("Cr");
					accountBalanceReport.setClosingBalance(closingBalance);
				}

				accountBalanceReport.setSlNo(i + 1);
				accountBalanceReport.setDate((Date) objAray[0]);
				accountBalanceReport.setAccountHeads((String) objAray[1]);
				accountBalanceReport.setRemark((String) objAray[2]);
				accountBalanceReport.setCredit((Double) objAray[3]);
				accountBalanceReport.setDebit((Double) objAray[4]);

				accountBalanceReport.setCompanyName((String) objAray[5]);

				AccountStatementList.add(accountBalanceReport);
			}

			if (!found) {
				Double openingBalance = ledgerClassRepo.getledgerBalance(startingDate, accountid);

				if (null == openingBalance) {
					openingBalance = 0.0;
				}
				Double closingBalance = ledgerClassRepo.getledgerClosingBalance(endigDate, accountid);

				if (null == closingBalance) {
					closingBalance = 0.0;
				}
				AccountBalanceReport accountBalanceReport = new AccountBalanceReport();
				if (openingBalance < 0) {
					accountBalanceReport.setOpeningBalanceType("Dr");
					accountBalanceReport.setOpeningBalance(openingBalance * -1);
				} else {
					accountBalanceReport.setOpeningBalanceType("Cr");
					accountBalanceReport.setOpeningBalance(openingBalance);
				}

				if (closingBalance < 0) {
					accountBalanceReport.setClosingBalanceType("Dr");
					accountBalanceReport.setClosingBalance(closingBalance * -1);
				} else {
					accountBalanceReport.setClosingBalanceType("Cr");
					accountBalanceReport.setClosingBalance(closingBalance);
				}

				AccountStatementList.add(accountBalanceReport);

			}

		}
		return AccountStatementList;

	}

	public List<AccountBalanceReport> getAccountBalanceBetweendate(Date eDate, Date sDate) {

		List<AccountBalanceReport> AccountStatementList = new ArrayList();
		List<Object> objList = ledgerClassRepo.getAccountBalanceBetweendate(eDate, sDate);
		for (int i = 0; i < objList.size(); i++) {
			Object[] objAray = (Object[]) objList.get(i);
			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();

			accountBalanceReport.setAccountHeads((String) objAray[3]);
			accountBalanceReport.setClosingBalance((Double) objAray[2]);
			accountBalanceReport.setDebit((Double) objAray[0]);
			accountBalanceReport.setCredit((Double) objAray[1]);
			if (accountBalanceReport.getClosingBalance() < 0) {
				accountBalanceReport.setClosingBalanceType("Dr");
				accountBalanceReport.setClosingBalance(accountBalanceReport.getClosingBalance() * -1);
			} else {
				accountBalanceReport.setClosingBalanceType("Cr");
				accountBalanceReport.setClosingBalance(accountBalanceReport.getClosingBalance());
			}
			AccountStatementList.add(accountBalanceReport);
		}
		return AccountStatementList;
	}

	@Override
	public Double getCustomerBalance(String companymstid, Date date, String custid) {
		return ledgerClassRepo.getCustomerBalance(companymstid, date, custid);
	}

	@Override
	public List<BranchAssetReport> getCustomerClosingBalance(String companymstid, Date date) {

		List<BranchAssetReport> accountBalanceReportList = new ArrayList<BranchAssetReport>();
		List<Object> objectList=new ArrayList<Object>();
		if (dbType.contains("mysql")) {
			objectList = ledgerClassRepo.getCustomerClosingBalance(companymstid, date);
		}
		else if (dbType.contains("derby")) {
			objectList = ledgerClassRepo.getCustomerClosingBalanceDerby(companymstid, date);
		}

		for (int i = 0; i < objectList.size(); i++) {
			Object[] objAray = (Object[]) objectList.get(i);
			BranchAssetReport accountBalanceReport = new BranchAssetReport();

			accountBalanceReport.setName((String) objAray[0]);
			accountBalanceReport.setAmount((Double) objAray[1]);

			accountBalanceReportList.add(accountBalanceReport);

		}

		return accountBalanceReportList;
	}

	@Override
	public List<BranchAssetReport> getSupplierClosingBalance(String companymstid, Date date) {
		List<BranchAssetReport> accountBalanceReportList = new ArrayList<BranchAssetReport>();

		List<Object> objectList = ledgerClassRepo.getSupplierClosingBalance(companymstid, date);

		for (int i = 0; i < objectList.size(); i++) {
			Object[] objAray = (Object[]) objectList.get(i);
			BranchAssetReport accountBalanceReport = new BranchAssetReport();

			accountBalanceReport.setName((String) objAray[0]);
			accountBalanceReport.setAmount((Double) objAray[1]);

			accountBalanceReportList.add(accountBalanceReport);

		}

		return accountBalanceReportList;
	}

	@Override
	public List<BranchAssetReport> getBankAccountClosingBalance(String companymstid, Date date) {

		List<BranchAssetReport> accountBalanceReportList = new ArrayList<BranchAssetReport>();
		List<AccountHeads> bankAccounts = accountHeadsServiceImpl.findByBankAccount(companymstid);

		for (AccountHeads accountHeads : bankAccounts) {

			Double closingBalance = ledgerClassRepo.getBankAccountClosingBalance(companymstid, date,
					accountHeads.getId());

			BranchAssetReport accountBalanceReport = new BranchAssetReport();

			accountBalanceReport.setName(accountHeads.getAccountName());
			if (null == closingBalance) {
				closingBalance = 0.0;
			}
			accountBalanceReport.setAmount(closingBalance);

			accountBalanceReportList.add(accountBalanceReport);

		}
		return accountBalanceReportList;

	}

	@Override
	public List<BranchAssetReport> getTotalBranchAsset(String companymstid, Date date) {

		List<BranchAssetReport> accountBalanceReportList = new ArrayList<BranchAssetReport>();

		return null;
	}

	@Override
	public void changeFinancialYr(Date enddate, String companymstid, Integer finanayrid) {
		openingBalanceMstRepo.deleteAll();
		String yearEndProcessed="YEAR END PROCESSED";
		Optional<FinancialYearMst> financialYearMstOpt
         =financialYrMstRepo.findById(finanayrid);
		FinancialYearMst finacialYearMst=financialYearMstOpt.get();
		accountClassRepository.deleteAccountClassDuplicaleOpeningBalance(yearEndProcessed,finacialYearMst.getFinancialYear());
	
		List<Object> obj = ledgerClassRepo.getTrialBalanceNewForOpeningBalance(companymstid, enddate);
		Double totalIncome =0.0,totalExpense =0.0,differenceOfIcomeAndExpense=0.0;
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		for (int i = 0; i < obj.size(); i++) {
			
			Object[] objAray = (Object[]) obj.get(i);
			
			OpeningBalanceMst openingBalanceMst = new OpeningBalanceMst();
			AccountHeads accHeads = accountHeadsRepo.findByAccountName((String) objAray[0]);
			String accroot = this.getRootParentOfAccount((String) objAray[0]);
			if(null != accroot && accroot.equalsIgnoreCase("EXPENSE"))
			{
				totalExpense = totalExpense + (Double) objAray[3];
			}
			else if (null != accroot && accroot.equalsIgnoreCase("INCOME"))
			{
				totalIncome = totalIncome + (Double) objAray[3];
			}
			else
			{
			openingBalanceMst.setAccountId(accHeads.getId());
			if((Double) objAray[3]>0)
			{
				openingBalanceMst.setDebitAmount((Double) objAray[3]);
				openingBalanceMst.setCreditAmount(0.0);
			}
			else
			{
				openingBalanceMst.setDebitAmount(0.0);
				openingBalanceMst.setCreditAmount(((Double) objAray[3])*-1);
			}
			
			
			FinancialYearMst financeYr = financialYrMstRepo.findById(finanayrid).get();
			openingBalanceMst.setTransDate(SystemSetting.systemDate);
			financeYr.getEndDate();
			openingBalanceMst.setFinancialYearMst(financeYr);
			openingBalanceMst.setCompanyMst(companyMst);
			openingBalanceMstRepo.save(openingBalanceMst);
			}
		}
		differenceOfIcomeAndExpense = totalIncome-totalExpense;
		if(differenceOfIcomeAndExpense > 0)
		{
			//Profit
			OpeningBalanceMst openingBalanceMst = new OpeningBalanceMst();
			AccountHeads accHeads = accountHeadsRepo.findByAccountName("PROFIT");
			openingBalanceMst.setAccountId(accHeads.getId());
			openingBalanceMst.setCreditAmount(differenceOfIcomeAndExpense);
			openingBalanceMst.setDebitAmount(0.0);
			openingBalanceMst.setTransDate(SystemSetting.systemDate);
			FinancialYearMst financeYr = financialYrMstRepo.findById(finanayrid).get();
			openingBalanceMst.setFinancialYearMst(financeYr);
			openingBalanceMst.setCompanyMst(companyMst);
			openingBalanceMstRepo.save(openingBalanceMst);
		}
		else if(differenceOfIcomeAndExpense < 0) 
		{
			//loss
			OpeningBalanceMst openingBalanceMst = new OpeningBalanceMst();
			AccountHeads accHeads = accountHeadsRepo.findByAccountName("LOSS");
			openingBalanceMst.setAccountId(accHeads.getId());
			openingBalanceMst.setCreditAmount(0.0);
			openingBalanceMst.setDebitAmount(differenceOfIcomeAndExpense*-1);
			openingBalanceMst.setTransDate(SystemSetting.systemDate);
			FinancialYearMst financeYr = financialYrMstRepo.findById(finanayrid).get();
			openingBalanceMst.setFinancialYearMst(financeYr);
			openingBalanceMst.setCompanyMst(companyMst);
			openingBalanceMstRepo.save(openingBalanceMst);
		}

		/*
		 * Post an entry into ledger against opening balance.
		 * 
		 */
		BranchMst branchMst = branchMstRepo.getMyBranch();
		List<OpeningBalanceMst> openingBalanceList = openingBalanceMstRepo.findAll();
		/*
		 * Post entry to accountclass
		 */
		
		/*
		 * Delete previous account entry if any
		 * with account name 'OPENING BALANCE'
		 * and financial year from openingbalance financial yr
		 */
		Double debitAmount =0.0,creditAmount =0.0;
		for(int i =0;i<openingBalanceList.size();i++)
		{
			debitAmount = debitAmount + openingBalanceList.get(i).getDebitAmount();
			creditAmount = creditAmount + openingBalanceList.get(i).getCreditAmount();
			
		}
		AccountClass accountClass = new AccountClass();
		accountClass.setBrachCode(branchMst.getBranchCode());
		accountClass.setCompanyMst(companyMst);
		accountClass.setFinancialYear(openingBalanceList.get(0).getFinancialYearMst().getFinancialYear());
		BigDecimal bdCredit = new BigDecimal(creditAmount);
		bdCredit = bdCredit.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		accountClass.setTotalCredit(bdCredit);
		BigDecimal bddebit = new BigDecimal(debitAmount);
		bddebit = bddebit.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		accountClass.setTotalDebit(bddebit);
		accountClass.setYearEndProcessing("YEAR END PROCESSED");
		accountClass =accountClassRepo.save(accountClass);
		for(int i =0;i<openingBalanceList.size();i++)
		{
			LedgerClass ledgerClass = new LedgerClass();
			ledgerClass.setAccountClass(accountClass);
			ledgerClass.setAccountId(openingBalanceList.get(i).getAccountId());
			AccountHeads accHeadsById = accountHeadsRepo.findById(openingBalanceList.get(i).getAccountId()).get();

			ledgerClass.setBranchCode(branchMst.getBranchCode());
			ledgerClass.setCompanyMst(companyMst);
			ledgerClass.setCreditAmount(BigDecimal.valueOf(openingBalanceList.get(i).getCreditAmount()));
			ledgerClass.setDebitAmount(BigDecimal.valueOf(openingBalanceList.get(i).getDebitAmount()));
			ledgerClass.setFinacialYear(openingBalanceList.get(i).getFinancialYearMst().getFinancialYear());
			ledgerClass.setSlNo("-1");
			FinancialYearMst financeYr = financialYrMstRepo.findById(finanayrid).get();
			Date financialYearEndUtilDate=SystemSetting.SqlDateToUtilDate(financeYr.getEndDate());
			SystemSetting.nextDay(financialYearEndUtilDate);
			ledgerClass.setTransDate(SystemSetting.nextDay(financialYearEndUtilDate));
			ledgerClass.setRemarkAccountName(accHeadsById.getAccountName());
			ledgerClassRepo.save(ledgerClass);
			
			
			AccountHeads accHeads = accountHeadsRepo.findByAccountName("OPENING BALANCE");
			LedgerClass ledgerClassopening = new LedgerClass();
			ledgerClassopening.setAccountId(accHeads.getId());
			ledgerClassopening.setAccountClass(accountClass);
			ledgerClassopening.setBranchCode(branchMst.getBranchCode());
			ledgerClassopening.setFinacialYear(openingBalanceList.get(i).getFinancialYearMst().getFinancialYear());
			ledgerClassopening.setSlNo("-1");
			ledgerClassopening.setRemark("From opening balance");
			ledgerClassopening.setRemarkAccountName(accHeads.getAccountName());
	
			SystemSetting.nextDay(financialYearEndUtilDate);
			
			
			ledgerClassopening.setTransDate(SystemSetting.nextDay(financialYearEndUtilDate));
			ledgerClassopening.setCompanyMst(companyMst);
			BigDecimal zero = new BigDecimal("0");
			if(openingBalanceList.get(i).getCreditAmount()>0)
			{
				BigDecimal bd = new BigDecimal(openingBalanceList.get(i).getCreditAmount());
				bd = bd.setScale(2,BigDecimal.ROUND_HALF_EVEN);
				ledgerClassopening.setDebitAmount(bd);
				ledgerClassopening.setCreditAmount(zero);
			}
			else if(openingBalanceList.get(i).getDebitAmount()>0)
			{
				BigDecimal bd = new BigDecimal(openingBalanceList.get(i).getDebitAmount());
				bd = bd.setScale(2,BigDecimal.ROUND_HALF_EVEN);
				ledgerClassopening.setCreditAmount(bd);
				ledgerClassopening.setDebitAmount(zero);
			}
				
			ledgerClassRepo.save(ledgerClassopening);
			
			/*
			 *Post entry to debitclass
			 */
			if(openingBalanceList.get(i).getDebitAmount()>0)
			{
			DebitClass debitClass = new DebitClass();
			debitClass.setAccountClass(accountClass);
			debitClass.setAccountId(openingBalanceList.get(i).getAccountId());
			debitClass.setBranchCode(branchMst.getBranchCode());
			debitClass.setCompanyMst(companyMst);
			debitClass.setDrAmount(BigDecimal.valueOf(openingBalanceList.get(i).getDebitAmount()));
			debitClass.setTransDate(SystemSetting.systemDate);
			debitClass.setRemark(accHeadsById.getAccountName());
			debitclassRepo.save(debitClass);
			}
			if(openingBalanceList.get(i).getCreditAmount()>0)
			{
				CreditClass creditclass = new CreditClass();
				creditclass.setAccountClass(accountClass);
				creditclass.setTransDate(SystemSetting.systemDate);
				creditclass.setAccountId(openingBalanceList.get(i).getAccountId());
				creditclass.setBranchCode(branchMst.getBranchCode());
				creditclass.setCompanyMst(companyMst);
				creditclass.setCrAmount(BigDecimal.valueOf(openingBalanceList.get(i).getCreditAmount()));
				creditclass.setRemark(accHeadsById.getAccountName());
				creditClassRepo.save(creditclass);
			}
		}
	}

	@Override
	public List<AccountBalanceReport> getTrialBalanceByParentId(String companymstid, Date date, String parentid) {

		List<AccountBalanceReport> accountBalanceReportList = new ArrayList<AccountBalanceReport>();

		List<Object> objList = ledgerClassRepo.getTrialBalanceByParentId(companymstid, date, parentid);

		for (int i = 0; i < objList.size(); i++) {
			Object[] objAray = (Object[]) objList.get(i);

			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();

			accountBalanceReport.setAccountHeads((String) objAray[0]);
			accountBalanceReport.setClosingBalance((Double) objAray[3]);

			accountBalanceReportList.add(accountBalanceReport);
		}

		return accountBalanceReportList;
	}

	@Override
	public List<AccountBalanceReport> getTrialBalanceById(String companymstid, Date date, String accountid) {
		List<AccountBalanceReport> accountBalanceReportList = new ArrayList<AccountBalanceReport>();

		List<Object> objList = ledgerClassRepo.getTrialBalanceById(companymstid, date, accountid);

		for (int i = 0; i < objList.size(); i++) {
			Object[] objAray = (Object[]) objList.get(i);

			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();

			accountBalanceReport.setAccountHeads((String) objAray[0]);
			accountBalanceReport.setClosingBalance((Double) objAray[3]);

			accountBalanceReportList.add(accountBalanceReport);
		}

		return accountBalanceReportList;
	}
	public List<AccountBalanceReport> getAccountStatementSingle(String companymstid, String accountid, Date startingDate,
			Date endigDate, String branchCode) {

		List<AccountBalanceReport> AccountStatementList = new ArrayList();
		boolean found = false;
		
		
		Optional<AccountHeads> customerMstOpt =accountHeadsRepo.findById(accountid);
		
//		Optional<Supplier> supplierOpt =supplierRepository.findById(accountid);
		
		
		List<Object> obj  = null;

			obj = ledgerClassRepo.getAccountStatementSigleEntryFormat(companymstid, accountid, startingDate, endigDate);


	

		Double closingBalance = ledgerClassRepo.getledgerClosingBalance(endigDate, accountid);
		Double openingBalance = ledgerClassRepo.getledgerBalance(startingDate, accountid);
		
		for (int i = 0; i < obj.size(); i++) {
			found = true;
			Object[] objAray = (Object[]) obj.get(i);
			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();

			

			if (null == openingBalance) {
				openingBalance = 0.0;
			}
			

			if (null == closingBalance) {
				closingBalance = 0.0;
			}

			if (openingBalance < 0) {
				accountBalanceReport.setOpeningBalanceType("Dr");
				accountBalanceReport.setOpeningBalance(openingBalance * -1);
			} else {
				accountBalanceReport.setOpeningBalanceType("Cr");
				accountBalanceReport.setOpeningBalance(openingBalance);
			}

			if (closingBalance < 0) {
				accountBalanceReport.setClosingBalanceType("Dr");
				accountBalanceReport.setClosingBalance(closingBalance * -1);
			} else {
				accountBalanceReport.setClosingBalanceType("Cr");
				accountBalanceReport.setClosingBalance(closingBalance);
			}

			
			
			Date trasDate = (Date) objAray[0];
			String accountHead = (String) objAray[1];
			String remark = (String) objAray[2];
			Double credit = (Double) objAray[3];
			
			if(null==credit) {
				credit=0.0;
			}
			Double debit = (Double) objAray[4];
			
			if(null==debit) {
				debit=0.0;
			}
			
			String companyName = (String) objAray[5];
			
			String voucherType = (String) (objAray[7]);
			String accountClassId = (String) objAray[6];
			
			accountBalanceReport.setSlNo(i + 1);
			accountBalanceReport.setDate(trasDate);
			accountBalanceReport.setAccountHeads(accountHead);
			accountBalanceReport.setRemark(voucherType);
			accountBalanceReport.setCredit(credit);
			accountBalanceReport.setDebit(debit);

			accountBalanceReport.setCompanyName(companyName);
			accountBalanceReport.setDescription(remark);
			accountBalanceReport.setAccountClassId(accountClassId);
			
			
			AccountStatementList.add(accountBalanceReport);
		}

		if (!found) {
			  openingBalance = ledgerClassRepo.getledgerBalance(startingDate, accountid);

			if (null == openingBalance) {
				openingBalance = 0.0;
			}
			  closingBalance = ledgerClassRepo.getledgerClosingBalance(endigDate, accountid);

			if (null == closingBalance) {
				closingBalance = 0.0;
			}
			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();
			if (openingBalance < 0) {
				accountBalanceReport.setOpeningBalanceType("Dr");
				accountBalanceReport.setOpeningBalance(openingBalance * -1);
			} else {
				accountBalanceReport.setOpeningBalanceType("Cr");
				accountBalanceReport.setOpeningBalance(openingBalance);
			}

			if (closingBalance < 0) {
				accountBalanceReport.setClosingBalanceType("Dr");
				accountBalanceReport.setClosingBalance(closingBalance * -1);
			} else {
				accountBalanceReport.setClosingBalanceType("Cr");
				accountBalanceReport.setClosingBalance(closingBalance);
			}

			AccountStatementList.add(accountBalanceReport);

		}

		return AccountStatementList;

	
	}

	// Recursice Call
	@Override
	public String getRootParentOfAccount(String accountName) {
		 
		
		AccountHeads accountHead = accountHeadsRepo.findByAccountName(accountName);
		String parentAccountId = accountHead.getParentId();
		if(null==parentAccountId || parentAccountId.equalsIgnoreCase("0") || parentAccountId.equalsIgnoreCase("")) {
			return accountName;
		}else {
			
			Optional<AccountHeads> accountHeadOpt = accountHeadsRepo.findById(parentAccountId);
			if(accountHeadOpt.isPresent()) {
				accountName = accountHeadOpt.get().getAccountName();
				accountName = getRootParentOfAccount(  accountName);
			}else {
				return null;
			}
		}
		return accountName;
	}
	// -----------------version 4.14 end


	//--------------------new version 1.12 surya  
	@Override
	public List<AccountBalanceReport> getTrialBalanceByIncomeParentId(String companymstid, Date date, String parentid) {
		List<AccountBalanceReport> accountBalanceReportList = new ArrayList<AccountBalanceReport>();

		List<Object> objList = ledgerClassRepo.getTrialBalanceByIncomeParentId(companymstid, date, parentid);

		for (int i = 0; i < objList.size(); i++) {
			Object[] objAray = (Object[]) objList.get(i);

			AccountBalanceReport accountBalanceReport = new AccountBalanceReport();

			accountBalanceReport.setAccountHeads((String) objAray[0]);
			accountBalanceReport.setClosingBalance((Double) objAray[3]);

			accountBalanceReportList.add(accountBalanceReport);
		}

		return accountBalanceReportList;
	}
	//--------------------new version 1.12 surya  end

	@Override
	public List<AccountBalanceReport> getBranchWiseAccountClosingBalance(String companymstid, Date date,String accountid) {
		
		
		List<AccountBalanceReport> accountHeadsReportList = new ArrayList<AccountBalanceReport>();

		List<Object> objlist = ledgerClassRepo.getBranchWiseAccountClosingBalance(companymstid,date,accountid);
		
		for (int i = 0; i < objlist.size(); i++) {

			Object[] objAray = (Object[]) objlist.get(i);
			
			AccountBalanceReport report = new AccountBalanceReport();
			report.setBranchName((String) objAray[0]);
			report.setClosingBalance((Double) objAray[1]);
		
			
			
			accountHeadsReportList.add(report);

		}
		
		return accountHeadsReportList;

	}

	@Override
	public void deleteBySourceVoucherNumber(String voucherNumber) {
	
		
		accountClassRepository.deleteBySourceVoucherNumber(voucherNumber);
	}
	
//Recursive Call
	// If you want to check if a specific  account is a child of an Account call this.
	// For example check an account if it is child of BANK ACCOUNT
	 
	@Override
	public boolean isChilcAccountOf(String accountId, String companMstID, String searchParentAccountNAme) {
		// Bank Parent Account = BANK ACCOUNTS
		AccountHeads accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId(searchParentAccountNAme,
				companMstID);
		return RecursiveParentAccountSearch(accountId, accountHeads.getId());

	}

	public boolean RecursiveParentAccountSearch(String AccountId, String searchKindAccountId) {

		AccountHeads accountHeads = accountHeadsRepo.findById(AccountId).get();
		if (null == accountHeads.getParentId()) {
			return false;
		} else if (accountHeads.getParentId().trim().length() == 0) {
			return false;
		} else if (accountHeads.getParentId().equalsIgnoreCase("0")) {
			return false;
		} else if (accountHeads.getParentId().equalsIgnoreCase(searchKindAccountId)) {
			return true;
		} else {
			return RecursiveParentAccountSearch(accountHeads.getParentId(), searchKindAccountId);

		}

		// Get ParentAccount of AccountID

		// If ParentAccount = AccountId for Bank Account then exit true
		// if ParentAccount = null then exit false
		// else call RecursiveParentAccountSearch(parentAccountId)

	}

	@Override
	public List<AccountHeads> getallChildAccounts(String accountName, String companMstID) {
		// Bank Parent Account = BANK ACCOUNTS
		AccountHeads accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMstId(accountName,
				companMstID);
		
		
		List<AccountHeads> resultList = new ArrayList();
		
		return RecursiveChildAccountSearch( accountHeads ,resultList );

	}

	private List<AccountHeads> RecursiveChildAccountSearch(AccountHeads accountId,List<AccountHeads> resultList) {

		List<AccountHeads> accountHeads = accountHeadsRepo.fetchAccountHeads(accountId.getId());
		
		for (AccountHeads accountHeads2 : accountHeads)  {
			  RecursiveChildAccountSearch(accountHeads2,resultList);
		 
		}

		resultList.add(accountId);
	
		
		return  resultList;

	 

		// Get ParentAccount of AccountID

		// If ParentAccount = AccountId for Bank Account then exit true
		// if ParentAccount = null then exit false
		// else call RecursiveParentAccountSearch(parentAccountId)

	}
	
	
}
