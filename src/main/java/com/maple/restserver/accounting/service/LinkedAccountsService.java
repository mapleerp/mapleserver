package com.maple.restserver.accounting.service;



import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.accounting.entity.LinkedAccounts;

@Service
@Transactional
@Component
public interface LinkedAccountsService {

	LinkedAccounts saveLinkedAccounts(LinkedAccounts linkedAccounts);

	List<LinkedAccounts> searchLikeParentAccountByName(String string, String companymstid, Pageable topFifty);


}

