package com.maple.restserver.accounting.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maple.restserver.report.entity.AccountBalanceReport;
import com.maple.restserver.report.entity.BranchAssetReport;
import com.maple.restserver.report.entity.PaymentVoucher;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.FinancialYearMstRepository;
import com.maple.restserver.service.AccountHeadsServiceImpl;
import com.maple.restserver.utils.SystemSetting;

import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.entity.OpeningBalanceMst;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.accounting.repository.OpeningBalanceConfigMstRepository;
import com.maple.restserver.accounting.repository.OpeningBalanceMstRepository;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.FinancialYearMst;

import com.maple.restserver.report.entity.AccountBalanceReport;
import com.maple.restserver.report.entity.BranchAssetReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.FinancialYearMstRepository;
import com.maple.restserver.service.AccountHeadsServiceImpl;
import com.maple.restserver.utils.SystemSetting;


@Component
public class OpeningBalanceMstServiceImpl implements OpeningBalanceMstService {

	@Autowired
	AccountHeadsServiceImpl accountHeadsServiceImpl;

	@Autowired
	OpeningBalanceMstRepository openingBalanceMstRepo;

	@Override
	public OpeningBalanceMst findByAccountIdAndCompanyMst(CompanyMst companyMst) {
		List<AccountHeads> bankAccountsList = accountHeadsServiceImpl.findByBankAccount(companyMst.getId());

		Double bankBalance = 0.0;

		for (AccountHeads account : bankAccountsList) {
			OpeningBalanceMst openingBalanceMst = openingBalanceMstRepo.findByIdAndCompanyMst(account.getId(),
					companyMst);
			
			if(null != openingBalanceMst)
			{
				bankBalance = bankBalance + (openingBalanceMst.getCreditAmount() - openingBalanceMst.getDebitAmount());

			}


		}

		OpeningBalanceMst openingBalanceMst = new OpeningBalanceMst();
		openingBalanceMst.setCreditAmount(bankBalance);

		return openingBalanceMst;
	}

}
