package com.maple.restserver.accounting.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.accounting.entity.LinkedAccounts;
import com.maple.restserver.accounting.repository.LinkedAccountsRepository;

@Service
@Transactional
@Component
public class LinkedAccountsServiceImpl implements LinkedAccountsService{

	@Autowired
	LinkedAccountsRepository linkedAccountsRepository;
	
	@Override
	public LinkedAccounts saveLinkedAccounts(LinkedAccounts linkedAccounts) {
		
		return linkedAccountsRepository.save(linkedAccounts);
	}

	@Override
	public List<LinkedAccounts> searchLikeParentAccountByName(String searchString, String companymstid, Pageable topFifty) {
		return linkedAccountsRepository.findSearch(searchString.toLowerCase(),companymstid,topFifty);
	}



}
