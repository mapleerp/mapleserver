package com.maple.restserver.accounting.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.InventoryLedgerMst;
import com.maple.restserver.accounting.repository.InventoryLedgerMstRepository;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.StockTransferInDtl;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.StockTransferInDtlRepository;
import com.maple.restserver.repository.StockTransferOutDtlRepository;
import com.maple.restserver.service.SaveAndPublishService;

@Service
@Transactional
@Component
public class InventoryLedgerMstServiceImpl implements InventoryLedgerMstService {
	
	@Value("${spring.datasource.url}")
	private String dbType;
	
	@Autowired
	private SalesDetailsRepository salesDtlRepo;
	
	@Autowired
	PurchaseDtlRepository purchaseDtlRepo;
	
	@Autowired
	ItemMstRepository itemMstRepository;
	@Autowired
	BranchMstRepository branchMstRepository;
	@Autowired
	InventoryLedgerMstRepository inventoryLedgerMstRepository;
	
	@Autowired
	StockTransferInDtlRepository stockTransferInDtlRepository;
	
	@Autowired
	StockTransferOutDtlRepository stockTransferOutDtlRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@Override
	public void wholesaleinventoryAccounting(String voucherNumber, Date voucherDate, String salesTransHdrId,
			CompanyMst companyMst,
			AccountClass accountClass) {
		
		List<SalesDtl> salesDtlList = salesDtlRepo.findBySalesTransHdrId(salesTransHdrId);
		Iterator iter = salesDtlList.iterator();

		while (iter.hasNext()) {
			SalesDtl salesDtl = (SalesDtl) iter.next();
		InventoryLedgerMst inventoryLedgerMst =new InventoryLedgerMst();
		Optional<ItemMst> itemMst =itemMstRepository.findById(salesDtl.getItemId());
		inventoryLedgerMst.setItemMst(itemMst.get());
		inventoryLedgerMst.setCompanyMst(companyMst);
		inventoryLedgerMst.setBatch(salesDtl.getBatch());
		inventoryLedgerMst.setDrQty(0.0);
		inventoryLedgerMst.setCrQty(salesDtl.getQty());
		inventoryLedgerMst.setDrAmount(new BigDecimal(0));
		if (dbType.contains("mysql")) {
			inventoryLedgerMst.setCrAmount(new BigDecimal(salesDtl.getRate()*salesDtl.getQty()));
		}
		else if (dbType.contains("derby")) {
			inventoryLedgerMst.setCrAmount(new BigDecimal(salesDtl.getRate()*salesDtl.getQty()).setScale(2, BigDecimal.ROUND_HALF_EVEN));
		}
		inventoryLedgerMst.setDescription("SALES");
		inventoryLedgerMst.setVoucherNumber(voucherNumber);
		inventoryLedgerMst.setVoucherDate(voucherDate);
		inventoryLedgerMst.setAccountClass(accountClass);
		
		Optional<BranchMst>  branchMst= branchMstRepository.findByBranchCodeAndCompanyMstId(accountClass.getBrachCode(),salesDtl.getCompanyMst().getId());
		inventoryLedgerMst.setBranchMst(branchMst.get());
		//inventoryLedgerMstRepository.save(inventoryLedgerMst);
		saveAndPublishService.saveInventoryLedgerMst(inventoryLedgerMst, accountClass.getBrachCode());
		
		
		
		}
		
	}
	
	
	@Override
	public void purchaseinventoryAccounting(String voucherNumber, Date voucherDate, String purchaseHdrId,
			CompanyMst companyMst, AccountClass accountClass) {
		List<PurchaseDtl> purchaseDtlList = purchaseDtlRepo.findByPurchaseHdrId(purchaseHdrId);
		Iterator iter = purchaseDtlList.iterator();

		while (iter.hasNext()) {
			PurchaseDtl purchaseDtl = (PurchaseDtl) iter.next();
			InventoryLedgerMst inventoryLedgerMst = new InventoryLedgerMst();
			Optional<ItemMst> itemMst = itemMstRepository.findById(purchaseDtl.getItemId());
			inventoryLedgerMst.setItemMst(itemMst.get());
			inventoryLedgerMst.setCompanyMst(companyMst);
			inventoryLedgerMst.setBatch(purchaseDtl.getBatch());
			inventoryLedgerMst.setDrQty(purchaseDtl.getQty());
			inventoryLedgerMst.setCrQty(0.0);
			inventoryLedgerMst.setDrAmount(new BigDecimal(purchaseDtl.getNetCost() * purchaseDtl.getQty()));
			inventoryLedgerMst.setCrAmount(new BigDecimal(0));
			inventoryLedgerMst.setDescription("PURCHASE");
			inventoryLedgerMst.setVoucherNumber(voucherNumber);
			inventoryLedgerMst.setVoucherDate(voucherDate);
			inventoryLedgerMst.setAccountClass(accountClass);

			Optional<BranchMst> branchMst = branchMstRepository
					.findByBranchCodeAndCompanyMstId(accountClass.getBrachCode(), companyMst.getId());
			inventoryLedgerMst.setBranchMst(branchMst.get());
			//inventoryLedgerMstRepository..save(inventoryLedgerMst);
			saveAndPublishService.saveInventoryLedgerMst(inventoryLedgerMst, accountClass.getBrachCode());
			

		}

}
	
	@Override
	public void stockTransferOutInventoryAccounting(String voucherNumber, Date voucherDate, 
			String stockTransferOutHdrid,
			CompanyMst companyMst, AccountClass accountClass) {
		List<StockTransferOutDtl> stockTransferOutDtlList = stockTransferOutDtlRepo.findByStockTransferOutHdrId(stockTransferOutHdrid);
		Iterator iter = stockTransferOutDtlList.iterator();

		while (iter.hasNext()) {
			StockTransferOutDtl stockTransferOutDtl = (StockTransferOutDtl) iter.next();
			InventoryLedgerMst inventoryLedgerMst = new InventoryLedgerMst();
			Optional<ItemMst> itemMst = itemMstRepository.findById(stockTransferOutDtl.getItemId().getId());
			inventoryLedgerMst.setCompanyMst(companyMst);
			inventoryLedgerMst.setBatch(stockTransferOutDtl.getBatch());
			inventoryLedgerMst.setDrQty(0.0);
			inventoryLedgerMst.setCrQty(stockTransferOutDtl.getQty());
			inventoryLedgerMst.setDrAmount(new BigDecimal(0));
			inventoryLedgerMst.setCrAmount(new BigDecimal(stockTransferOutDtl.getRate() * stockTransferOutDtl.getQty()));
			inventoryLedgerMst.setDescription("STOCKTRANSFEROUT");
			inventoryLedgerMst.setVoucherNumber(voucherNumber);
			inventoryLedgerMst.setVoucherDate(voucherDate);
			inventoryLedgerMst.setAccountClass(accountClass);
			inventoryLedgerMst.setItemMst(itemMst.get());

			Optional<BranchMst> branchMst = branchMstRepository
					.findByBranchCodeAndCompanyMstId(stockTransferOutDtl.getStockTransferOutHdr().getFromBranch(), companyMst.getId());
			inventoryLedgerMst.setBranchMst(branchMst.get());
			//inventoryLedgerMstRepository.save(inventoryLedgerMst);
			saveAndPublishService.saveInventoryLedgerMst(inventoryLedgerMst, accountClass.getBrachCode());
	
	}
	}

	
	@Override
	public void acceptstockInventoryAccounting(String voucherNumber, Date voucherDate, String stockTransferInHdrid, CompanyMst companyMst,
			AccountClass accountClass) {
		
		List<StockTransferInDtl> stockTransferInDtlList = stockTransferInDtlRepository.findBystockTransferInHdrIdAndCompanyMst(stockTransferInHdrid,companyMst);
		Iterator iter = stockTransferInDtlList.iterator();

		while (iter.hasNext()) {
			StockTransferInDtl stockTransferInDtl = (StockTransferInDtl) iter.next();
			InventoryLedgerMst inventoryLedgerMst = new InventoryLedgerMst();
			Optional<ItemMst> itemMst = itemMstRepository.findById(stockTransferInDtl.getItemId().getId());
			inventoryLedgerMst.setCompanyMst(companyMst);
			inventoryLedgerMst.setBatch(stockTransferInDtl.getBatch());
			inventoryLedgerMst.setDrQty(stockTransferInDtl.getQty());
			inventoryLedgerMst.setCrQty(0.0);
			inventoryLedgerMst.setDrAmount(new BigDecimal(stockTransferInDtl.getRate() * stockTransferInDtl.getQty()));
			inventoryLedgerMst.setCrAmount(new BigDecimal(0));
			inventoryLedgerMst.setDescription("ACCEPTSTOCK");
			inventoryLedgerMst.setVoucherNumber(voucherNumber);
			inventoryLedgerMst.setVoucherDate(voucherDate);
			inventoryLedgerMst.setAccountClass(accountClass);
			
			inventoryLedgerMst.setItemMst(itemMst.get());

			Optional<BranchMst> branchMst = branchMstRepository
					.findByBranchCodeAndCompanyMstId(accountClass.getBrachCode(), companyMst.getId());
			inventoryLedgerMst.setBranchMst(branchMst.get());
			//inventoryLedgerMstRepository.save(inventoryLedgerMst);
			saveAndPublishService.saveInventoryLedgerMst(inventoryLedgerMst, accountClass.getBrachCode());
	
	}
		
		
		
		
		
	}
	
	
}
