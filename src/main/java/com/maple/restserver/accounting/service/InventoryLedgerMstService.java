package com.maple.restserver.accounting.service;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.entity.CompanyMst;

@Service
@Transactional
public interface InventoryLedgerMstService {


	void wholesaleinventoryAccounting(String voucherNumber, Date voucherDate, String salesTransHdrId, CompanyMst companyMst,
			AccountClass accountClass);
	
	void purchaseinventoryAccounting(String voucherNumber, Date voucherDate, String purchaseHdrId,CompanyMst comnpanyMst, AccountClass accountClass);
	
	void stockTransferOutInventoryAccounting(String voucherNumber, Date voucherDate, String stockTransferOutHdrid,
			CompanyMst companyMst, AccountClass accountClass);
	
	void acceptstockInventoryAccounting(String voucherNumber, Date voucherDate, String stockTransferInHdrid, CompanyMst companyMst,
			AccountClass accountClass);
}
