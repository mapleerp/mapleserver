

package com.maple.restserver.accounting.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
 
import com.maple.restserver.entity.CompanyMst;
@Entity
public class  DebitClassfc   implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
    private String id;
	
	
	@Column(length = 50)
	String accountId;
	Date transDate;
	String remark;
	
	@Column(length = 50)
	String sourceVoucherNumber;
	
	@Column(length = 50)
	String currency;
	
	 
	@Column(name = "dr_Amount", nullable = false, columnDefinition = "double default 0.0") 
	BigDecimal drAmount;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "account_class_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private AccountClassfc accountClassfc;
	

	 

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	private String branchCode;
	
	String itemId;
	
 
	Double rate;
	Double qty;
	private String  processInstanceId;
	private String taskId;	

 
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Date getTransDate() {
		return transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public BigDecimal getDrAmount() {
		return drAmount;
	}

	public void setDrAmount(BigDecimal drAmount) {
		this.drAmount = drAmount;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	 

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getSourceVoucherNumber() {
		return sourceVoucherNumber;
	}

	public void setSourceVoucherNumber(String sourceVoucherNumber) {
		this.sourceVoucherNumber = sourceVoucherNumber;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public AccountClassfc getAccountClassfc() {
		return accountClassfc;
	}

	public void setAccountClassfc(AccountClassfc accountClassfc) {
		this.accountClassfc = accountClassfc;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "DebitClassfc [id=" + id + ", accountId=" + accountId + ", transDate=" + transDate + ", remark=" + remark
				+ ", sourceVoucherNumber=" + sourceVoucherNumber + ", currency=" + currency + ", drAmount=" + drAmount
				+ ", accountClassfc=" + accountClassfc + ", companyMst=" + companyMst + ", branchCode=" + branchCode
				+ ", itemId=" + itemId + ", rate=" + rate + ", qty=" + qty + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}

	 

	 
	 
	
}

