package com.maple.restserver.accounting.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;

@Entity
public class InventoryLedgerMst implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	private String id;
	Date voucherDate;
	String voucherNumber;

	String batch;

	@Column(name = "dr_Qty", nullable = false, columnDefinition = "double default 0.0")
	Double drQty;

	@Column(name = "cr_Qty", nullable = false, columnDefinition = "double default 0.0")
	Double crQty;

	@Column(name = "dr_Amount", nullable = false, columnDefinition = "double default 0.0")
	BigDecimal drAmount;

	@Column(name = "cr_Amount", nullable = false, columnDefinition = "double default 0.0")
	BigDecimal crAmount;

	@Column(length = 256)
	String description;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "itemMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	ItemMst itemMst;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "branchMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	BranchMst branchMst;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "account_class_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private AccountClass accountClass;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Double getDrQty() {
		return drQty;
	}

	public void setDrQty(Double drQty) {
		this.drQty = drQty;
	}

	public Double getCrQty() {
		return crQty;
	}

	public void setCrQty(Double crQty) {
		this.crQty = crQty;
	}

	public BigDecimal getDrAmount() {
		return drAmount;
	}

	public void setDrAmount(BigDecimal drAmount) {
		this.drAmount = drAmount;
	}

	public BigDecimal getCrAmount() {
		return crAmount;
	}

	public void setCrAmount(BigDecimal crAmount) {
		this.crAmount = crAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public BranchMst getBranchMst() {
		return branchMst;
	}

	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}

	public AccountClass getAccountClass() {
		return accountClass;
	}

	public void setAccountClass(AccountClass accountClass) {
		this.accountClass = accountClass;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public ItemMst getItemMst() {
		return itemMst;
	}

	public void setItemMst(ItemMst itemMst) {
		this.itemMst = itemMst;
	}

	@Override
	public String toString() {
		return "InventoryLedgerMst [id=" + id + ", voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber
				+ ", batch=" + batch + ", drQty=" + drQty + ", crQty=" + crQty + ", drAmount=" + drAmount
				+ ", crAmount=" + crAmount + ", description=" + description + ", itemMst=" + itemMst + ", companyMst="
				+ companyMst + ", branchMst=" + branchMst + ", accountClass=" + accountClass + "]";
	}

	

}
