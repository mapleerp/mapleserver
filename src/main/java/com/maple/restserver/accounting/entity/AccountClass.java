
package com.maple.restserver.accounting.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;

@Entity
public class AccountClass  implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	private String id;

	@Column(name = "total_Debit", nullable = false)
	BigDecimal totalDebit;

	@Column(name = "total_Credit", nullable = false)
	BigDecimal totalCredit;
	String oldId;
	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	Date transDate;
	
	@Column(length = 50)
	String machineId;
	
	@Column(length = 50)
	String voucherType;
	
	@Column(length = 100)
	String sourceVoucherNumber;
	@Column(length = 256)
	String sourceParentId;
	
	@Column(length = 20)
	String brachCode;

	@Column(length = 20)
	String financialYear;

	@Column(length = 20)
   String yearEndProcessing;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	
	@Column(length = 50)
	private String  processInstanceId;
	
	@Column(length = 100)
	private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getTransDate() {
		return transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public String getSourceVoucherNumber() {
		return sourceVoucherNumber;
	}

	public void setSourceVoucherNumber(String sourceVoucherNumber) {
		this.sourceVoucherNumber = sourceVoucherNumber;
	}

	public String getSourceParentId() {
		return sourceParentId;
	}

	public void setSourceParentId(String sourceParentId) {
		this.sourceParentId = sourceParentId;
	}

	public String getBrachCode() {
		return brachCode;
	}

	public void setBrachCode(String brachCode) {
		this.brachCode = brachCode;
	}

	public String getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

	public BigDecimal getTotalDebit() {
		return totalDebit;
	}

	public void setTotalDebit(BigDecimal totalDebit) {
		this.totalDebit = totalDebit;
	}

	public BigDecimal getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(BigDecimal totalCredit) {
		this.totalCredit = totalCredit;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	

	public String getYearEndProcessing() {
		return yearEndProcessing;
	}

	public void setYearEndProcessing(String yearEndProcessing) {
		this.yearEndProcessing = yearEndProcessing;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "AccountClass [id=" + id + ", totalDebit=" + totalDebit + ", totalCredit=" + totalCredit + ", oldId="
				+ oldId + ", transDate=" + transDate + ", machineId=" + machineId + ", voucherType=" + voucherType
				+ ", sourceVoucherNumber=" + sourceVoucherNumber + ", sourceParentId=" + sourceParentId + ", brachCode="
				+ brachCode + ", financialYear=" + financialYear + ", yearEndProcessing=" + yearEndProcessing
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}

	

	

}
