package com.maple.restserver.accounting.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;

@Entity
public class AccountMergeMst   implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	private String id;
	
	
	String fromAccountId;
	
	
	String toAccountId;
	
	@Column(length = 20)
	String fromAccountName;
	
	@Column(length = 20)
	String toAccountName;
	
	@Column(length = 20)
	String BranchCode;
	
	
	Date UpdatedDate;
	
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getFromAccountId() {
		return fromAccountId;
	}



	public void setFromAccountId(String fromAccountId) {
		this.fromAccountId = fromAccountId;
	}



	public String getToAccountId() {
		return toAccountId;
	}



	public void setToAccountId(String toAccountId) {
		this.toAccountId = toAccountId;
	}



	public String getFromAccountName() {
		return fromAccountName;
	}



	public void setFromAccountName(String fromAccountName) {
		this.fromAccountName = fromAccountName;
	}



	public String getToAccountName() {
		return toAccountName;
	}



	public void setToAccountName(String toAccountName) {
		this.toAccountName = toAccountName;
	}



	public String getBranchCode() {
		return BranchCode;
	}



	public void setBranchCode(String branchCode) {
		BranchCode = branchCode;
	}



	



	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	



	public Date getUpdatedDate() {
		return UpdatedDate;
	}



	public void setUpdatedDate(Date updatedDate) {
		UpdatedDate = updatedDate;
	}



	@Override
	public String toString() {
		return "AccountMergeMst [id=" + id + ", fromAccountId=" + fromAccountId + ", toAccountId=" + toAccountId
				+ ", fromAccountName=" + fromAccountName + ", toAccountName=" + toAccountName + ", BranchCode="
				+ BranchCode + ", UpdatedDate=" + UpdatedDate + ", companyMst=" + companyMst + "]";
	}

}
