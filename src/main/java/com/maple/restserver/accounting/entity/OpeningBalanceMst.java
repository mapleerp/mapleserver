package com.maple.restserver.accounting.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.FinancialYearMst;

@Entity
public class OpeningBalanceMst {


	

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	private String id;
	
	String accountId;
	Double debitAmount;
	Double creditAmount;
	Date transDate;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "financialYearMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	FinancialYearMst financialYearMst;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 100)
	private String taskId;

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getAccountId() {
		return accountId;
	}


	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}


	public Double getDebitAmount() {
		return debitAmount;
	}


	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}


	public Double getCreditAmount() {
		return creditAmount;
	}


	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}


	public Date getTransDate() {
		return transDate;
	}


	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}


	public FinancialYearMst getFinancialYearMst() {
		return financialYearMst;
	}


	public void setFinancialYearMst(FinancialYearMst financialYearMst) {
		this.financialYearMst = financialYearMst;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "OpeningBalanceMst [id=" + id + ", accountId=" + accountId + ", debitAmount=" + debitAmount
				+ ", creditAmount=" + creditAmount + ", transDate=" + transDate + ", financialYearMst="
				+ financialYearMst + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}

}
