package  com.maple.restserver.accounting.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
 
import com.maple.restserver.entity.CompanyMst;

@Entity
public class LedgerClass  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
    private String id;
	 
	@Column(length = 50)
	String accountId;
	
	
	@Column(length = 50)
	String oldId;
	@Column(name = "debit_Amount", nullable = false, columnDefinition = "double default 0.0") 
	BigDecimal debitAmount;
	
	@Column(name = "credit_Amount", nullable = false, columnDefinition = "double default 0.0") 
	BigDecimal creditAmount;
	Date transDate;
	
	@Column(length = 50)
	String sourceParentId;
	
	
	@Column(length = 50)
	String sourceVoucherId;
	
	@Column(length = 50)
	String ledgerVoucherId;
	
	@Column(length = 50)
	String finacialYear;
	String remark;
	
	
	String remarkAccountName;
	String sourceVoucherType;
	String slNo;
	private String voucherType;
	
	private String branchCode;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "account_class_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private AccountClass accountClass;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	private String  processInstanceId;
	private String taskId;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	 

	public Date getTransDate() {
		return transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public String getSourceParentId() {
		return sourceParentId;
	}

	public void setSourceParentId(String sourceParentId) {
		this.sourceParentId = sourceParentId;
	}

	public String getSourceVoucherId() {
		return sourceVoucherId;
	}

	public void setSourceVoucherId(String sourceVoucherId) {
		this.sourceVoucherId = sourceVoucherId;
	}

	public String getLedgerVoucherId() {
		return ledgerVoucherId;
	}

	public void setLedgerVoucherId(String ledgerVoucherId) {
		this.ledgerVoucherId = ledgerVoucherId;
	}

	 
	public String getFinacialYear() {
		return finacialYear;
	}

	public void setFinacialYear(String finacialYear) {
		this.finacialYear = finacialYear;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSourceVoucherType() {
		return sourceVoucherType;
	}

	public void setSourceVoucherType(String sourceVoucherType) {
		this.sourceVoucherType = sourceVoucherType;
	}

 
 
	public AccountClass getAccountClass() {
		return accountClass;
	}

	public void setAccountClass(AccountClass accountClass) {
		this.accountClass = accountClass;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getRemarkAccountName() {
		return remarkAccountName;
	}

	public void setRemarkAccountName(String remarkAccountName) {
		this.remarkAccountName = remarkAccountName;
	}

	
	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}


	public String getSlNo() {
		return slNo;
	}

	public void setSlNo(String slNo) {
		this.slNo = slNo;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "LedgerClass [id=" + id + ", accountId=" + accountId + ", oldId=" + oldId + ", debitAmount="
				+ debitAmount + ", creditAmount=" + creditAmount + ", transDate=" + transDate + ", sourceParentId="
				+ sourceParentId + ", sourceVoucherId=" + sourceVoucherId + ", ledgerVoucherId=" + ledgerVoucherId
				+ ", finacialYear=" + finacialYear + ", remark=" + remark + ", remarkAccountName=" + remarkAccountName
				+ ", sourceVoucherType=" + sourceVoucherType + ", slNo=" + slNo + ", voucherType=" + voucherType
				+ ", branchCode=" + branchCode + ", accountClass=" + accountClass + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
