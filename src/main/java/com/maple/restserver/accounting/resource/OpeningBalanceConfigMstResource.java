package com.maple.restserver.accounting.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.accounting.entity.OpeningBalanceConfigMst;
import com.maple.restserver.accounting.repository.OpeningBalanceConfigMstRepository;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
public class OpeningBalanceConfigMstResource {

	private static final Logger logger = LoggerFactory.getLogger(OpeningBalanceConfigMstResource.class);
	@Value("${mybranch}")
	private String mybranch;
	@Autowired
	OpeningBalanceConfigMstRepository openingBalanceConfigMstRepo;

	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@PostMapping("{companymstid}/openingbalanceconfigmst/saveopeningbalanceconfigmst")
	public OpeningBalanceConfigMst createOpeningBalanceConfigMst(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 OpeningBalanceConfigMst  openingBalanceConfigMst)
		{
			
			
				Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
				
				CompanyMst companyMst = companyMstOpt.get();
				openingBalanceConfigMst.setCompanyMst(companyMst);
				
				OpeningBalanceConfigMst saved=saveAndPublishService.saveOpeningBalanceConfigMst(openingBalanceConfigMst, mybranch);
				if(null!=saved) {
					  saveAndPublishService.publishObjectToKafkaEvent(saved, mybranch, 
							  KafkaMapleEventType.OPENINGBALANCECONFIGMST, 
							  KafkaMapleEventType.SERVER, saved.getId());
				  }else {
					  return null;
				  }
				
				
				logger.info("OpeningBalanceConfigMst send to KafkaEvent: {}", saved);
				return saved;
		} 
	@GetMapping("{companymstid}/openingbalanceconfigmst/getallopeningbalanceconfigmst")
	public List<OpeningBalanceConfigMst> getAllOpeningBalanceConfigMst(
			@PathVariable(value = "companymstid")String companymstid )
	{
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		return openingBalanceConfigMstRepo.findAll();
	}
	
	@DeleteMapping("{companymstid}/openingbalanceconfigmst/deleteopeningbalanceconfigmstbyid/{mstid}")
	public void deleteOpeningBalanceConfigMstById(
			@PathVariable(value = "mstid")String mstid )
	{
		 openingBalanceConfigMstRepo.deleteById(mstid);
	}
}
