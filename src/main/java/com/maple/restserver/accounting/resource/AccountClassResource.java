
package com.maple.restserver.accounting.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.resource.PurchaseHdrResource;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
public class AccountClassResource {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountClassResource.class);
	@Autowired
	private AccountClassRepository accountClass;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@Value("${mybranch}")
	private String mybranch;

	@GetMapping("/{companymstid}/accountclass/account")
	public List<AccountClass> retrieveAllbalance(@PathVariable(value = "companymstid") String companymstid) {
		return accountClass.findByCompanyMstId(companymstid);
	}

	@GetMapping("/{companymstid}/accountclass/accountclassbyid/{id}")
	public AccountClass getAccountClassById(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {
		return accountClass.findByCompanyMstIdAndId(companymstid, id);
	}

	@PostMapping("{companymstid}/account")
	public AccountClass createUser(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody AccountClass accountClass1) {
		
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);


			accountClass1.setCompanyMst(companyMst.get());
			logger.info("saveAccountClass send to KafkaEvent: {}", accountClass1);
			 
			accountClass1 = saveAndPublishService.saveAccountClass(accountClass1, accountClass1.getBrachCode());
			if(null != accountClass1) {
				saveAndPublishService.publishObjectToKafkaEvent(accountClass1, 
						accountClass1.getBrachCode(),
						KafkaMapleEventType.ACCOUNTCLASS, 
						KafkaMapleEventType.SERVER,accountClass1.getId());
			}else {
				return null;
			}
		return accountClass1;
	}

}
