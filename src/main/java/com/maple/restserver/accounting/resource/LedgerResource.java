package com.maple.restserver.accounting.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
	
	@RestController
	public class LedgerResource {
		
		private static final Logger logger = LoggerFactory.getLogger(LedgerResource.class);
		@Autowired
		private LedgerClassRepository ledgerClassRepo;
		@Autowired
		CompanyMstRepository companyMstRepo;
		
		@Autowired
		SaveAndPublishService saveAndPublishService;
		@GetMapping("{companymstid}/led")
		public List<LedgerClass> retrieveAllcash()
		{
			return ledgerClassRepo.findAll();
		}
		
		 
		

		@PostMapping("{companymstid}/led")
		public LedgerClass createUser(@PathVariable(value = "companymstid") String companymstid,
				@Valid @RequestBody LedgerClass ledger) {
			Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

			ledger.setCompanyMst(companyMst.get());
			logger.info("ledger send to KafkaEvent: {}", ledger);

			ledger = saveAndPublishService.saveLedgerClass(ledger, ledger.getBranchCode());
			if (null != ledger) {
				saveAndPublishService.publishObjectToKafkaEvent(ledger, ledger.getBranchCode(),
						KafkaMapleEventType.LEDGERCLASS,
						KafkaMapleEventType.SERVER, ledger.getId());
			} else {
				return null;
			}

			return ledger;
		}

	}

