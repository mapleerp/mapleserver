package com.maple.restserver.accounting.resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.TrialBalanceHdr;
import com.maple.restserver.report.entity.AccountBalanceReport;
import com.maple.restserver.report.entity.BranchAssetReport;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.accounting.service.AccountingService;
import com.maple.restserver.accounting.service.AccountingServiceImpl;
import com.maple.restserver.utils.SystemSetting;

@RestController
@CrossOrigin("http://localhost:4200")
public class AccountingResource {
	@Autowired
	SalesTransHdrRepository salesTransHdrRepo;

	@Autowired
	private AccountingServiceImpl   accountingServiceImpl;
	
	@Autowired
	private AccountingService   accountingService;
	
	@GetMapping("{companymstid}/trialbalance")
	public List<Object> getTrialBalance(
			@PathVariable(value="companymstid") String companymstid,
			@RequestParam("startdate") String startdate){
		java.util.Date date = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");

		return accountingService.getTrialBalance(companymstid,date);
	}
	
	
	@GetMapping("{companymstid}/trialbalance/gettrialbalancedetail/{userid}")
	public List<Object> getTrialBalance(
			@PathVariable(value="companymstid") String companymstid,
			@RequestParam("startdate") String startdate,
			@RequestParam("enddate") String enddate,
			@RequestParam("reportdate") String reportdate,
			@PathVariable(value="userid") String userId){
		java.util.Date startDate = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");
		java.util.Date endDate = SystemSetting.StringToUtilDate(enddate,"yyyy-MM-dd");
		java.util.Date reportDate = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return accountingService.getTrialBalance(companymstid,startDate,endDate,userId,reportDate);
	}
	
	
	@GetMapping("{companymstid}/accountingresource/changefinancilayr/{finanayrid}")
	public String changeFinancialYr(@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="finanayrid") Integer finanayrid,
			@RequestParam("enddate")String enddate)
	{
		java.util.Date date = SystemSetting.StringToUtilDate(enddate,"yyyy-MM-dd");
		accountingService.changeFinancialYr(date,companymstid,finanayrid);
		return "Done";
	}
	
	@GetMapping("{companymstid}/accountingresource/getcustomerbalance/{custid}")
	public Double getCustomerBalance(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="custid") String custid,
			@RequestParam("startdate") String startdate){
		java.util.Date date = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");
		return accountingService.getCustomerBalance(companymstid,date,custid);
	}
	
	@GetMapping("{companymstid}/accountingresource/accountstatement/{accountid}/{branchCode}")
	public List<AccountBalanceReport> getaccountstatement(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="accountid") String accountid,@PathVariable(value="branchCode") String branchCode,
			@RequestParam("startdate") String startdate,
			@RequestParam("enddate") String enddate){
		java.util.Date eDate = SystemSetting.StringToUtilDate(enddate,"yyyy-MM-dd");
		java.util.Date SDate = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");
		return accountingServiceImpl.getAccountStatement(companymstid, accountid,SDate,eDate,branchCode );
	}
	
	
	@GetMapping("{companymstid}/accountingresource/accountstatementsingle/{accountid}/{branchCode}")
	public List<AccountBalanceReport> getaccountstatementSingle(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="accountid") String accountid,@PathVariable(value="branchCode") String branchCode,
			@RequestParam("startdate") String startdate,
			@RequestParam("enddate") String enddate){
		java.util.Date eDate = SystemSetting.StringToUtilDate(enddate,"yyyy-MM-dd");
		java.util.Date SDate = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");
		return accountingServiceImpl.getAccountStatementSingle(companymstid, accountid,SDate,eDate,branchCode );
	}
	
	@GetMapping("{companymstid}/accountingresource/supplieraccountstatement/{accountid}/{branchCode}")
	public List<AccountBalanceReport> getSupplierAccountstatement(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="accountid") String accountid,@PathVariable(value="branchCode") String branchCode,
			@RequestParam("startdate") String startdate,
			@RequestParam("enddate") String enddate){
		java.util.Date eDate = SystemSetting.StringToUtilDate(enddate,"yyyy-MM-dd");
		java.util.Date SDate = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");
		return accountingServiceImpl.getSupplierAccountStatement(companymstid, accountid,SDate,eDate,branchCode );
	}

	@GetMapping("{companymstid}/sundrydebitorbalance")
	public List<Object> getSundryDebitorBalance(
			@PathVariable(value="companymstid") String companymstid,
			@RequestParam("startdate") String startdate){
		java.util.Date date = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");

		return accountingService.getSundryDebtorBalance(companymstid,date);
	}
	//----------------version 4.14
	
		@GetMapping("{companymstid}/accountingresource/bankaccountstatement/{branchCode}")
		public List<AccountBalanceReport> getbanckaccountstatement(
				@PathVariable(value="companymstid") String companymstid,
				@PathVariable(value="branchCode") String branchCode,
				@RequestParam("startdate") String startdate,
				@RequestParam("enddate") String enddate){
			java.util.Date eDate = SystemSetting.StringToUtilDate(enddate,"yyyy-MM-dd");
			java.util.Date SDate = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");
			return accountingServiceImpl.getBankAccountStatement(companymstid, SDate,eDate,branchCode );
		}
		//-----------------version 4.14 end
		
		
		@GetMapping("{companymstid}/accountingresource/accountstatement/{branchCode}")
		public List<AccountBalanceReport> getaccountstatementBetweenDate(
				@PathVariable(value="companymstid") String companymstid,
				@PathVariable(value="branchCode") String branchCode,
				@RequestParam("startdate") String startdate,
				@RequestParam("enddate") String enddate){
			java.util.Date eDate = SystemSetting.StringToUtilDate(enddate,"yyyy-MM-dd");
			java.util.Date SDate = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");

			
			return accountingServiceImpl.getAccountBalanceBetweendate(eDate,SDate);
		}
		
		
		
		@GetMapping("{companymstid}/accountingresource/customerclosingbalance")
		public List<BranchAssetReport> getCustomerClosingBalance(
				@PathVariable(value="companymstid") String companymstid,
				@RequestParam("date") String startdate){
			java.util.Date date = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");

			return accountingService.getCustomerClosingBalance(companymstid,date);
		}
		

		@GetMapping("{companymstid}/accountingresource/supplierclosingbalance")
		public List<BranchAssetReport> getSupplierClosingBalance(
				@PathVariable(value="companymstid") String companymstid,
				@RequestParam("date") String startdate){
			java.util.Date date = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");

			return accountingService.getSupplierClosingBalance(companymstid,date);
		}
		
		
		@GetMapping("{companymstid}/accountingresource/bankaccountclosingbalance")
		public List<BranchAssetReport> getBankAccountClosingBalance(
				@PathVariable(value="companymstid") String companymstid,
				@RequestParam("date") String startdate){
			java.util.Date date = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");

			return accountingService.getBankAccountClosingBalance(companymstid,date);
		}
		
		
		@GetMapping("{companymstid}/accountingresource/gettotalbranchasset")
		public List<BranchAssetReport> getTotalBranchAsset(
				@PathVariable(value="companymstid") String companymstid,
				@RequestParam("date") String startdate){
			java.util.Date date = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");

			return accountingService.getTotalBranchAsset(companymstid,date);
		}
		
		@GetMapping("{companymstid}/accountingresource/accountbalancebyparentaccount/{parentid}")
		public List<AccountBalanceReport> getAccountTrialBalanceByParentId(
				@PathVariable(value="companymstid") String companymstid,
				@PathVariable(value="parentid") String parentid,
				@RequestParam("date") String startdate){
			java.util.Date date = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");

			return accountingService.getTrialBalanceByParentId(companymstid,date,parentid);
		}
		
		//--------------------new version 1.12 surya
		
		@GetMapping("{companymstid}/accountingresource/accountbalancebyincomeparentaccount/{parentid}")
		public List<AccountBalanceReport> getAccountTrialBalanceByIncomeParentId(
				@PathVariable(value="companymstid") String companymstid,
				@PathVariable(value="parentid") String parentid,
				@RequestParam("date") String startdate){
			java.util.Date date = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");

			return accountingService.getTrialBalanceByIncomeParentId(companymstid,date,parentid);
		}
		
		//--------------------new version 1.12 surya end

		
		@GetMapping("{companymstid}/accountingresource/accountbalancebyid/{accountid}")
		public List<AccountBalanceReport> getAccountTrialBalanceById(
				@PathVariable(value="companymstid") String companymstid,
				@PathVariable(value="accountid") String accountid,
				@RequestParam("date") String startdate){
			java.util.Date date = SystemSetting.StringToUtilDate(startdate,"yyyy-MM-dd");

			return accountingService.getTrialBalanceById(companymstid,date,accountid);
		}
		
		
		
		
		
}
