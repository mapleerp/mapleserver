package com.maple.restserver.accounting.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.accounting.entity.OpeningBalanceMst;
import com.maple.restserver.accounting.repository.OpeningBalanceMstRepository;
import com.maple.restserver.accounting.service.OpeningBalanceMstService;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.entity.InsuranceCompanyMst;
import com.maple.restserver.entity.PatientMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.AccountHeadsServiceImpl;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
public class OpeningBalanceMstResource {
	
	private static final Logger logger = LoggerFactory.getLogger(OpeningBalanceMstResource.class);
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	OpeningBalanceMstService openingBalanceMstService;


	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	OpeningBalanceMstRepository openingBalanceMstRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@PostMapping("{companymstid}/openingbalancemst/saveopeningbalancemst")
	public OpeningBalanceMst createOpeningBalanceMst(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 OpeningBalanceMst  openingBalanceMst)
		{
			
			
				Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
				
				CompanyMst companyMst = companyMstOpt.get();
				openingBalanceMst.setCompanyMst(companyMst);
//				OpeningBalanceMst  saved = openingBalanceMstRepo.save(openingBalanceMst);
				
				
				OpeningBalanceMst saved=saveAndPublishService.saveOpeningBalanceMst(openingBalanceMst, mybranch);
				if(null!=saved) {
					  saveAndPublishService.publishObjectToKafkaEvent(saved, 
							  mybranch,  KafkaMapleEventType.OPENINGBALANCEMST,
							  KafkaMapleEventType.SERVER, saved.getId());
				  }else {
					  return null;
				  }
				
				logger.info("OpeningBalanceMst send to KafkaEvent: {}", saved);
				return saved;
		} 

	@GetMapping("{companymstid}/openingbalancemst/getopeningbalancebyaccountid/{accountId}")
	public OpeningBalanceMst getOpeningBalanceMstByAccountId(
			@PathVariable(value = "accountId")String accountId,
			@PathVariable(value = "companymstid")String companymstid )
	{
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		return openingBalanceMstRepo.findByIdAndCompanyMst(accountId,companyMst);
	}
	
	@GetMapping("{companymstid}/openingbalancemst/getallopeningbalance")
	public List<OpeningBalanceMst> getAllOpeningBalanceMst(
			@PathVariable(value = "companymstid")String companymstid )
	{
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		return openingBalanceMstRepo.findAll();
	}
	
	@GetMapping("{companymstid}/openingbalancemst/getallopeningbalancebyaccid/{accountId}")
	public OpeningBalanceMst getAllOpeningBalanceMstByAccid(
			@PathVariable(value = "accountId")String accountId,
			@PathVariable(value = "companymstid")String companymstid )
	{
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		return openingBalanceMstRepo.findByAccountIdAndCompanyMst(accountId,companyMst);
	}
	
	@DeleteMapping("{companymstid}/openingbalancemst/deleteopeningbalancebyid/{id}")
	public void deleteOpeningBalanceMstById(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "id")String id)
	{
		 openingBalanceMstRepo.deleteById(id);
	}
	
	@DeleteMapping("{companymstid}/openingbalancemst/deleteopeningbalancebyaccountid/{accid}")
	public void deleteOpeningBalanceMstByAccId(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "accid")String accid)
	{
		 openingBalanceMstRepo.deleteByAccountId(accid);
	}
	
	@PutMapping("{companymstid}/openingbalancemst/updateopeningbalance")
	public void UpdateOpeningBalance(@PathVariable(value = "companymstid")String companymstid,
			@RequestBody OpeningBalanceMst openingBalanceRqst)
	{
		Optional<OpeningBalanceMst> openingBalanceop = openingBalanceMstRepo.findById(openingBalanceRqst.getId());
		if(openingBalanceop.isPresent())
		{
			OpeningBalanceMst openingBalanceMst = openingBalanceop.get();
			openingBalanceMst.setDebitAmount(openingBalanceRqst.getDebitAmount());
			openingBalanceMst.setCreditAmount(openingBalanceRqst.getCreditAmount());
			openingBalanceMst.setTransDate(openingBalanceRqst.getTransDate());
			//openingBalanceMstRepo.save(openingBalanceMst);
			OpeningBalanceMst saved=saveAndPublishService.saveOpeningBalanceMst(openingBalanceMst, mybranch);
			if(null!=saved) {
				  saveAndPublishService.publishObjectToKafkaEvent(saved,
						  mybranch,  KafkaMapleEventType.OPENINGBALANCEMST,
						  KafkaMapleEventType.SERVER, saved.getId());
			  }else {
				  return;
			  }
			logger.info("OpeningBalanceMst send to KafkaEvent: {}", saved);
			
		}
				
	}
	
	
	@GetMapping("{companymstid}/openingbalancemst/getopeningbalancebybankaccount")
	public OpeningBalanceMst getOpeningBalanceMstByBankAccount(
			@PathVariable(value = "companymstid")String companymstid )
	{
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		

		return openingBalanceMstService.findByAccountIdAndCompanyMst(companyMst);
	}
}
