

package com.maple.restserver.accounting.resource;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.resource.PurchaseHdrResource;
import com.maple.restserver.service.SaveAndPublishService;



@RestController
public class DebitClassResource {
	private static final Logger logger = LoggerFactory.getLogger(DebitClassResource.class);
	@Autowired
	private DebitClassRepository debitClass;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@GetMapping("{companymstid}/debitclass")
	public List<DebitClass> retrieveAllcash()
	{
		return debitClass.findAll();
	}
	@PostMapping("{companymstid}/debitclass")
	public ResponseEntity<Object> createUser(@Valid @RequestBody DebitClass debitClass1)
	{
//		DebitClass saved=debitClass.save(debitClass1);
		
		DebitClass saved=saveAndPublishService.saveDebitClass(debitClass1, debitClass1.getBranchCode());
		if(null != saved) {
			saveAndPublishService.publishObjectToKafkaEvent(saved, 
					debitClass1.getBranchCode(), KafkaMapleEventType.DEBITCLASS, 
					KafkaMapleEventType.SERVER, saved.getId());
		}else {
			return null;
		}
		
		logger.info("DebitClass send to KafkaEvent: {}", saved);
	URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	}

}


