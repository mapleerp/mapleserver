package com.maple.restserver.accounting.resource;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.service.SaveAndPublishService;




@RestController
public class CreditClassResource {
	private static final Logger logger = LoggerFactory.getLogger(CreditClassResource.class);
	@Autowired
	private CreditClassRepository creditClass;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	@GetMapping("/{companymstid}/creditclass")
	public List<CreditClass> retrieveAllcash(@PathVariable String companymstid)
	{
		return creditClass.findByCompanyMstId(companymstid);
	}
	@PostMapping("{companymstid}/creditclass")
	public ResponseEntity<Object> createUser(@Valid @RequestBody CreditClass creditclass1)
	{
//		CreditClass saved=creditClass.save(creditclass1);
		CreditClass saved=saveAndPublishService.saveCreditClass(creditclass1, creditclass1.getBranchCode());
		if(null != saved) {
			saveAndPublishService.publishObjectToKafkaEvent(saved, 
					creditclass1.getBranchCode(), KafkaMapleEventType.CREDITCLASS, 
					KafkaMapleEventType.SERVER, saved.getId());
		}else {
			return null;
		}
		logger.info("CreditClass send to KafkaEvent: {}", creditclass1);
	URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	}

}


