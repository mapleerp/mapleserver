package com.maple.restserver.accounting.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.accounting.entity.LinkedAccounts;
import com.maple.restserver.accounting.repository.LinkedAccountsRepository;
import com.maple.restserver.accounting.service.LinkedAccountsService;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.repository.CompanyMstRepository;

@RestController
public class LinkedAccountsResource {
	@Autowired
	LinkedAccountsService linkedAccountsService;
	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	LinkedAccountsRepository linkedAccountsRepository;
	
	Pageable topFifty =   PageRequest.of(0, 50);
	
	@PostMapping("{companymstid}/linkedaccountsresource/savelinkedaccounts")
	public LinkedAccounts saveLinkedAccounts(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody LinkedAccounts linkedAccounts)
	{
		Optional<CompanyMst> company=companyMstRepository.findById(companymstid);
		linkedAccounts.setCompanyMst(company.get());
		return linkedAccountsService.saveLinkedAccounts(linkedAccounts);
	}
	
	
	@GetMapping("{companymstid}/linkedaccountsresource/getalllinkedaccounts")
	public List<LinkedAccounts> getAllLinkedAccounts(
			@PathVariable(value = "companymstid") String companymstid){
		return linkedAccountsRepository.findAll();
	}
	
	@GetMapping("{companymstid}/searchparentaccount")
	public @ResponseBody List<LinkedAccounts> searchParentAccount(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("data") String searchstring) {
		return linkedAccountsService.searchLikeParentAccountByName("%" + searchstring + "%", companymstid, topFifty);
	}
	
	@GetMapping("{companymstid}/linkedaccountsresource/getlinkedaccountsbyparentandchild/{parentaccountid}/{childaccountid}")
	public @ResponseBody LinkedAccounts getLinkedAccounts(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "parentaccountid") String parentAccountId,
			@PathVariable(value = "childaccountid") String childAccountId) {
		return linkedAccountsRepository.findByParentAndChild(parentAccountId,childAccountId);
	}

}
