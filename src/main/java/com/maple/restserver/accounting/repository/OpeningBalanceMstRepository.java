package com.maple.restserver.accounting.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.accounting.entity.OpeningBalanceMst;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;

@Repository
public interface OpeningBalanceMstRepository extends JpaRepository<OpeningBalanceMst,String> {

	OpeningBalanceMst findByIdAndCompanyMst(String accountId, CompanyMst companyMst);

	void deleteByAccountId(String accid);

	OpeningBalanceMst findByAccountIdAndCompanyMst(String accountId, CompanyMst companyMst);


}
