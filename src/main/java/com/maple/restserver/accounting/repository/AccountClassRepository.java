
package com.maple.restserver.accounting.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.entity.CompanyMst;


@Component
@Transactional
@Repository
public interface AccountClassRepository extends JpaRepository<AccountClass, String>{
	
	
	@Query(nativeQuery = true,value="select * from account_class "
			+ "where date(trans_date) >= :fdate and date(trans_date) <= :tdate")
	List<AccountClass> getAccountClassBetweenDate(Date fdate,Date tdate);
	List<AccountClass>findByTransDate(Date voucherDate);
	
List<AccountClass>findByCompanyMstId(String companymstid);
List<AccountClass>findBySourceVoucherNumberAndTransDate(String voucherNumber, Date voucherDate );
List<AccountClass>findBySourceVoucherNumber(String voucherNumber);

void deleteBySourceVoucherNumberAndTransDate(String voucherNumber, Date voucherDate );
void deleteBySourceVoucherNumber(String voucherNumber );

@Query(nativeQuery = true,value = "select count(*) from account_class where brach_code=:branchCode"
		+ " and date(trans_date) =:vDate and company_mst = :companyMst") 
int countOfAccountClassByDate(String branchCode,Date vDate, String companyMst);
List<AccountClass> findByCompanyMstAndBrachCode(CompanyMst companyMst, String branchcode);

void deleteBySourceParentId(String string);
List<AccountClass> findBySourceParentId(String salesTransHdrId);
List<AccountClass> findBySourceParentIdAndSourceVoucherNumberAndTransDate(String purchaseHdrId, String voucherNumber,
		Date voucherDate);
AccountClass findByCompanyMstIdAndId(String companymstid, String id);

@Modifying
@Query(nativeQuery = true, value = "delete from account_class  where  financial_year=:financialYear and year_end_processing=:yearEndProcessed")
void deleteAccountClassDuplicaleOpeningBalance(String yearEndProcessed,String financialYear);




@Query(nativeQuery = true, value = "select * from account_class  where "
		+ "date(voucher_date)= :f1date")
List<AccountClass> getAccountClassForDate(Date f1date);







}
