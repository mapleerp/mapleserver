package com.maple.restserver.accounting.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;

@Component
@Repository
public interface DebitClassRepository extends JpaRepository<DebitClass, String> {
	
	List<DebitClass>findByTransDate(Date vdate);
	@Query(nativeQuery = true,value= "select count(*) from debit_class where branch_code=:branchCode "
			+ "and date(trans_date)=:vdate and company_mst=:companyMst")
	int countOfDebitClassByDate(String branchCode,Date vdate,String companyMst);
	List<DebitClass> findByAccountClassId(String accountClassId);
	
	 List<DebitClass> findByAccountClass(AccountClass accountClass);
	 List<DebitClass>  findByAccountClassAndAccountId(AccountClass accountClass, String accountId);

	
	void deleteByRemark(String remrk);
	
	void deleteByaccountClass(AccountClass accountClass);
	List<DebitClass> findByCompanyMst(CompanyMst companyMst);
	
	@Modifying
	@Query(nativeQuery = true,value="update debit_class  set account_id =:toAccountId "
			+ "where account_id =:fromAccountId ")
	void accountMerge(String fromAccountId, String toAccountId);
	
}
