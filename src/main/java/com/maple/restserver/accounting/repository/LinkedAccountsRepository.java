package com.maple.restserver.accounting.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.accounting.entity.LinkedAccounts;

@Component
@Transactional
@Repository
public interface LinkedAccountsRepository extends JpaRepository<LinkedAccounts, String>{
	
	


	@Query(nativeQuery = true,value="SELECT * FROM linked_accounts la,account_heads ah WHERE lower(ah.account_name) LIKE  lower(:searchString) AND la.parent_account=ah.id AND la.company_mst=:companymstid" )
	List<LinkedAccounts> findSearch(String searchString, String companymstid, Pageable topFifty);

	@Query(nativeQuery = true,value="SELECT * FROM linked_accounts WHERE parent_account=:parentAccountId AND child_account=:childAccountId")
	LinkedAccounts findByParentAndChild(String parentAccountId, String childAccountId);


}

