package com.maple.restserver.accounting.repository;

 
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.LedgerReport;

@Transactional
@Repository
public interface LedgerClassRepository extends JpaRepository<LedgerClass, String>{
	@Query(nativeQuery = true, value = " select  a.account_name,  sum(l.credit_amount ) ,\n" + 
			"	sum( l.debit_amount ), (sum( l.debit_amount )-sum(l.credit_amount )) as total"
			+ "  from ledger_class l ,  "
			+ " account_heads a  where a.id = l.account_id  and l.company_mst = :companymstid   "
			+ " and date(l.trans_date)<=:date and "
			+ " (a.PARENT_ID=(select b.id from account_heads b where b.account_name='SUNDRY DEBTORS')) " + 
			"	group by a.account_name ")

	List<Object> getSundryDebtorsBalanceNew(String companymstid, Date date);
	
	@Query(nativeQuery = true,value="select count(*) from ledger_class where "
			+ "branch_code = :branchCode and date(trans_date) = :vdate and "
			+ "company_mst =:companyMst")
	int countOfLedgerClass(String branchCode,Date vdate,String companyMst);
	
	List<LedgerClass>findByTransDate(Date vdate);
	List<LedgerClass> findByAccountClassIdAndAccountId(String accountClassId, String accountId);
	
	List<LedgerClass> findByAccountClassId(String accountClassId);

	void deleteByAccountClass(AccountClass accountClass);
	void deleteByRemark(String remark);
	
	
	@Query(nativeQuery = true, value = " select  a.account_name,  sum(l.credit_amount ) ,\n" + 
			"	sum( l.debit_amount ), (sum( l.debit_amount )-sum(l.credit_amount )) as total"
			+ "  from ledger_class l ,  "
			+ " account_heads a  where a.id = l.account_id  and l.company_mst = :companymstid   "
			+"	group by a.account_name ")

	List<Object> getTrialBalance(String companymstid);
	
	@Query(nativeQuery = true, value = " SELECT B.account, SUM(B.Credit), SUM(B.Debit) FROM (\n" + 
			"select A.account, case when (sum(A.cr) -  sum(A.dr)) > 0 \n" + 
			"then round(sum(A.cr) -  sum(A.dr),2) else 0 end as Credit , \n" + 
			"case when  (sum(A.dr)  -  sum(A.cr)) >0 \n" + 
			"then  round(sum(A.dr)  -  sum(A.cr),2) else 0 end  as Debit\n" + 
			"from\n" + 
			"(\n" + 
			"select \n" + 
			"case when  a.root_parent_id in(  'INCOME', 'EXPENSE')   then\n" + 
			"'RETAINED EARNINGS' else a.account_name  end  as account  ,   sum(l.credit_amount) as cr ,\n" + 
			"sum(l.debit_amount) as dr \n" + 
			"from ledger_class l ,  account_heads a\n" + 
			"where l.account_id = a.id \n" + 
			"and date(l.trans_date) < (select date(start_date) from financial_year_mst where current_financial_year = 1  )\n" + 
			"group by a.account_name,a.root_parent_id ) A  group by A.account\n" + 
			"UNION\n" + 
			"select A.account, case when (sum(A.cr) -  sum(A.dr)) > 0 \n" + 
			"then round(sum(A.cr) -  sum(A.dr),2) else 0 end as Credit , \n" + 
			"case when  (sum(A.dr)  -  sum(A.cr)) >0 \n" + 
			"then  round(sum(A.dr)  -  sum(A.cr),2) else 0 end  as Debit\n" + 
			"from\n" + 
			"(\n" + 
			"select a.account_name as account  ,   sum(l.credit_amount) as cr ,\n" + 
			"sum(l.debit_amount) as dr \n" + 
			"from ledger_class l ,  account_heads a\n" + 
			"where l.account_id = a.id \n" + 
			"and date(l.trans_date) >=(select date(start_date) from financial_year_mst where current_financial_year = 1  )\n" + 
			"and date(l.trans_date) < :date and  l.company_mst = :companymstid  "
			+ " group by a.account_name ) A  group by A.account\n" + 
			")B GROUP BY B.account \n" + 
			" ")

	List<Object> getTrialBalanceNew(String companymstid, Date date);
	
	
	@Query(nativeQuery = true, value = " SELECT B.account, SUM(B.Credit), SUM(B.Debit) FROM (\n" + 
			"select A.account, case when (sum(A.cr) -  sum(A.dr)) > 0 \n" + 
			"then cast((sum(A.cr)-sum(A.dr)) as decimal(10,2)) else 0 end as Credit , \n" + 
			"case when  (sum(A.dr)  -  sum(A.cr)) >0 \n" + 
			"then  cast((sum(A.dr)-sum(A.cr)) as decimal(10,2)) else 0 end  as Debit\n" + 
			"from\n" + 
			"(\n" + 
			"select \n" + 
			"case when  a.root_parent_id in(  'INCOME', 'EXPENSE')   then\n" + 
			"'RETAINED EARNINGS' else a.account_name  end  as account  ,   sum(l.credit_amount) as cr ,\n" + 
			"sum(l.debit_amount) as dr \n" + 
			"from ledger_class l ,  account_heads a\n" + 
			"where l.account_id = a.id \n" + 
			"and date(l.trans_date) < (select date(start_date) from financial_year_mst where current_financial_year = 1  )\n" + 
			"group by a.account_name,a.root_parent_id ) A  group by A.account\n" + 
			"UNION\n" + 
			"select A.account, case when (sum(A.cr) -  sum(A.dr)) > 0 \n" + 
			"then cast((sum(A.cr)-sum(A.dr)) as decimal(10,2)) else 0 end as Credit , \n" + 
			"case when  (sum(A.dr)  -  sum(A.cr)) >0 \n" + 
			"then  cast((sum(A.dr)-sum(A.cr)) as decimal(10,2)) else 0 end  as Debit\n" + 
			"from\n" + 
			"(\n" + 
			"select a.account_name as account  ,   sum(l.credit_amount) as cr ,\n" + 
			"sum(l.debit_amount) as dr \n" + 
			"from ledger_class l ,  account_heads a\n" + 
			"where l.account_id = a.id \n" + 
			"and date(l.trans_date) >=(select date(start_date) from financial_year_mst where current_financial_year = 1  )\n" + 
			"and date(l.trans_date) < :date and  l.company_mst = :companymstid  "
			+ " group by a.account_name ) A  group by A.account\n" + 
			")B GROUP BY B.account \n" + 
			" ")

	List<Object> getTrialBalanceNewInDerby(String companymstid, Date date);
	
	
	@Query(nativeQuery = true, value = " select  a.account_name,  sum(l.credit_amount ) ,\n" + 
			"	sum( l.debit_amount ), (sum( l.debit_amount )-sum(l.credit_amount )) as total"
			+ "  from ledger_class l ,  "
			+ " account_heads a  where a.id = l.account_id  and l.company_mst = :companymstid   "
			+ "and date(l.trans_date)>=:startDate and  date(l.trans_date)<=:endDate " + 
			"	group by a.account_name ")

	List<Object> getTrialBalanceByStartAndEndDate(String companymstid, Date startDate,Date endDate);
	
	@Query(nativeQuery = true, value = " select  a.account_name,  sum(l.credit_amount ) ,\n" + 
			"	sum( l.debit_amount ), (sum( l.debit_amount )-sum(l.credit_amount )) as total"
			+ "  from ledger_class l ,  "
			+ " account_heads a  where a.id = l.account_id  and l.company_mst = :companymstid   "
			+ "and date(l.trans_date)<=:date and a.id NOT IN (select account_id from opening_balance_config_mst) " + 
			"	group by a.account_name ")

	List<Object> getTrialBalanceNewForOpeningBalance(String companymstid, Date date);
	

	@Query(nativeQuery = true, value = " select l.trans_date,  a.account_name,   l.SOURCE_VOUCHER_ID ,"
			+ "  l.credit_amount  , " + 
			"	l.debit_amount,  c.company_name , l.id as id , l.remark "


			+ "    from ledger_class l ,account_heads a , company_mst c where l.company_mst = :companymstid  "
			+ "   and  a.id = l.account_id "
			+ " and a.id =:accountid  and date(l.trans_date) >= :startingDate and date(l.trans_date)<= :endingDate   and c.id=l.company_mst  " + 
			"	order  by l.trans_date ")

 
	List<Object> getAccountStatementMultipleEntryFormat(String companymstid, String accountid, Date startingDate , Date endingDate );
	
	
	
	
	
	
	@Query(nativeQuery = true, value = " select * from (select"
			+ "  l.trans_date , "
			+ " al.account_name , "
			+ " a.account_name  || '(' ||  ac.SOURCE_VOUCHER_NUMBER || ')' as remark ,  "
			/*+ "   ac.SOURCE_VOUCHER_NUMBER   as remark ,  "*/
			
			
			+ " c.cr_amount as crAmount , "
			+ " 0 as DrAmount , "+ 
			" m.company_name , ac.id as id , ac.VOUCHER_TYPE || '(' ||  ac.SOURCE_VOUCHER_NUMBER || ')' " + 
			" from " + 
			"credit_class c , " + 
			"account_heads a, " + 
			"company_mst m, " + 
			"account_class ac ," + 
			"ledger_class l ," + 
			"account_heads al where " + 
			"c.account_id <> l.account_id and " + 
			"c.company_mst = m.id   " + 
			 
			"and ac.id = c.account_class_id and c.account_id = a.id " + 
			"and l.account_id = al.id and l.account_class_id = c.account_class_id " +
	
		    " and l.account_id =:accountid  "
		    + " and l.trans_date >= :startingDate and l.trans_date<= :endingDate   and m.id=l.company_mst  " + 
			" and 	l.company_mst = :companymstid "
			+ " UNION "
			+ " select  " + 
			"			  l.trans_date ,  " + 
			 "			  al.account_name , " +  
			"			  a.account_name  || '(' ||  ac.SOURCE_VOUCHER_NUMBER || ')' as remark , " +
			
			/* "   ac.SOURCE_VOUCHER_NUMBER   as remark ,  "+ */
			
			"			  0 as crAmount ,  " + 
			"			   c.dr_amount as DrAmount ,  " + 
			"			  m.company_name , ac.id as id , ac.VOUCHER_TYPE  || '(' ||  ac.SOURCE_VOUCHER_NUMBER || ')' " + 
			"			  from  " + 
			"			 debit_class c ,  " + 
			"			 account_heads a,  " + 
			"			 company_mst m,  " + 
			"			 account_class ac , " + 
			"			 ledger_class l , " + 
			"			 account_heads al where  " + 
			"			 c.account_id <> l.account_id and  " + 
			"			 c.company_mst = m.id    " + 
	 
			"			 and ac.id = c.account_class_id and c.account_id = a.id  " + 
			"			 and l.account_id = al.id and l.account_class_id = c.account_class_id  " + 
			"  " + 
			"		    and l.account_id =:accountid   " + 
			"		     and l.trans_date >= :startingDate and l.trans_date<= :endingDate   and m.id=l.company_mst   " + 
			"		  and 	l.company_mst = :companymstid  ) A order by A.trans_date, A.id " +
		    
			
			
			"	 ")
	
	
	List<Object> getAccountStatementSigleEntryFormat(String companymstid, String accountid, Date startingDate , Date endingDate );
	
	
	
	/*
	
	@Query(nativeQuery = true, value = " select * from (select"
			+ "  l.trans_date , "
			+ " al.account_name , "
			+ " a.account_name  || '(' ||  ac.SOURCE_VOUCHER_NUMBER || ')' as remark ,  "
		
			 
			
			
			+ " c.cr_amount as crAmount , "
			+ " 0 as DrAmount , "+ 
			" m.company_name , ac.id as id , ac.VOUCHER_TYPE  " + 
			" from " + 
			"credit_class c , " + 
			"account_heads a, " + 
			"company_mst m, " + 
			"account_class ac ," + 
			"ledger_class l ," + 
			"account_heads al where " + 
			"c.account_id <> l.account_id and " + 
			"c.company_mst = m.id   " + 
			 
			"and ac.id = c.account_class_id and c.account_id = a.id " + 
			"and l.account_id = al.id and l.account_class_id = c.account_class_id " +
	
		    " and l.account_id =:accountid  "
		    + " and l.trans_date >= :startingDate and l.trans_date<= :endingDate   and m.id=l.company_mst  " + 
			" and 	l.company_mst = :companymstid "
			+ " UNION "
			+ " select  " + 
			"			  l.trans_date ,  " + 
			"			  al.account_name , " + 
			"			  a.account_name  || '(' ||  ac.SOURCE_VOUCHER_NUMBER || ')' as remark , " + 
			
		 
			
			"			  0 as crAmount ,  " + 
			"			   c.dr_amount as DrAmount ,  " + 
			"			  m.company_name , ac.id as id , ac.VOUCHER_TYPE " + 
			"			  from  " + 
			"			 debit_class c ,  " + 
			"			 account_heads a,  " + 
			"			 company_mst m,  " + 
			"			 account_class ac , " + 
			"			 ledger_class l , " + 
			"			 account_heads al where  " + 
			"			 c.account_id <> l.account_id and  " + 
			"			 c.company_mst = m.id    " + 
	 
			"			 and ac.id = c.account_class_id and c.account_id = a.id  " + 
			"			 and l.account_id = al.id and l.account_class_id = c.account_class_id  " + 
			"  " + 
			"		    and l.account_id =:accountid   " + 
			"		     and l.trans_date >= :startingDate and l.trans_date<= :endingDate   and m.id=l.company_mst   " + 
			"		  and 	l.company_mst = :companymstid  ) A order by A.trans_date, A.id " +
		    
			
			
			"	 ")
	
	
	List<Object> getAccountStatement(String companymstid, String accountid, Date startingDate , Date endingDate );
	
	*/
	
	
	
	
	
	@Query(nativeQuery = true, value = " select   sum(l.credit_amount )  - 	sum( l.debit_amount )   from ledger_class l , "
			+ "account_heads a  where a.id = l.account_id and a.account_name = :aAccount " + 
			"	  ")
	Double getAccountBalance(String aAccount);
	
	
	@Query(nativeQuery = true, value = " select   sum(l.credit_amount )  - 	sum( l.debit_amount )   from ledger_class l , "
			+ "account_heads a  where a.id = l.account_id and a.account_name = :aAccount and l.transDate <= :endDate " + 
			"	  ")
	Double getClosingBalance(String aAccount, Date endDate);
	
	@Query(nativeQuery = true, value = " select   sum(l.credit_amount )  - 	sum( l.debit_amount )   from ledger_class l , "
			+ "account_heads a  where a.id = l.account_id and a.account_name = :account and l.transDate < :startDate " + 
			"	  ")
	Double getOpeningBalance(String account, Date startDate);
	
	
	@Query(nativeQuery = true, value ="select sum(l.debit_amount),sum(l.credit_amount) from ledger_class l,"
			+"account_heads a where l.account_id=a.id") 
	public List<Object> getLedgerReport();

	
	@Query(nativeQuery = true, value = " select   sum(l.credit_amount )  - 	sum( l.debit_amount )   from ledger_class l ,account_heads a  "
			+ "  where date(l.trans_date) < :startingDate and l.account_id=a.id and l.account_id=:accountid " + 
			"	  ")
	Double getledgerBalance(Date startingDate,String accountid);
	
	
	@Query(nativeQuery = true, value = " select   sum(l.credit_amount )  - 	sum( l.debit_amount )   from ledger_class l ,account_heads a  "
			+ "  where date(l.trans_date) <= :startingDate and l.account_id=a.id and l.account_id=:accountid " + 
			"	  ")
	Double getledgerClosingBalance(Date startingDate,String accountid);

	
	@Query(nativeQuery = true, value = " select l.trans_date, l.remark, l.credit_amount,"
			+ " l.debit_amount, "
			+ "c.company_name, "
			+ "p.supplier_inv_date, "
			+ "a.account_name "
			+ "from ledger_class l, company_mst c, purchase_hdr p, account_heads a "
			+ "where p.FINAL_SAVED_STATUS = 'Y' and "
			+ "  l.company_mst=c.id "
			+ "and l.account_id=p.supplier_id "
			+ "and l.account_id=a.id and   l.source_voucher_id = p.voucher_number "
			+ "and c.id=:companymstid and l.account_id=:accountid "
			+ "and date(l.trans_date) between :sDate and :eDate")
	List<Object> getSupplierAccountStatement(String companymstid, String accountid, Date sDate, Date eDate);


	
	
	@Query(nativeQuery = true, value = " select l.trans_date,  a.account_name,  l.remark, l.credit_amount  ,\n" + 
			"	l.debit_amount,  c.company_name "

			+ "    from ledger_class l ,account_heads a , company_mst c where l.company_mst = :companymstid  "
			+ "   and  a.id = l.account_id "
			+ " and date(l.trans_date) >= :startingDate and date(l.trans_date)<= :endingDate   and c.id=l.company_mst  " + 
			"	order  by l.trans_date ")


	List<Object> getAccountStatementBetweenDate(String companymstid, Date startingDate , Date endingDate );

	List<LedgerClass> findByCompanyMst(CompanyMst companyMst);

//	@Query(nativeQuery = true,value="select sum(l.debit_amount),sum(l.credit_amount),"
//			+ "sum(l.credit_amount )  - 	sum( l.debit_amount ),"
//			+ "c.customer_name from account_heads a,ledger_class l,customer_mst c "
//			+ " where c.id=a.id and a.id = l.account_id and l.trans_date >= :eDate and l.trans_date <=:sDate group by c.customer_name")
	
	@Query(nativeQuery = true,value="select sum(l.debit_amount),sum(l.credit_amount),"
			+ "sum(l.credit_amount )  - 	sum( l.debit_amount ),"
			+ "a.account_name from account_heads a,ledger_class l "
			+ " where a.id = l.account_id and l.trans_date >= :eDate and l.trans_date <=:sDate group by a.account_name")
	List<Object> getAccountBalanceBetweendate(Date eDate, Date sDate);
	@Query(nativeQuery = true, value = " select  "  
			+ " (sum( l.debit_amount )-sum(l.credit_amount )) as total"
			+ "  from ledger_class l ,  "
			+ " account_heads a  where a.id = l.account_id  and l.company_mst = :companymstid   "
			+ "and date(l.trans_date)<=:date and a.id=:accid")

	Double getCustomerBalance(String companymstid, Date date,String accid);

	
//	@Query(nativeQuery = true, value = " select a.account_name, "  
//			+ " (sum( l.debit_amount )-sum(l.credit_amount )) as balance"
//			+ "  from ledger_class l ,  "
//			+ " account_heads a , customer_mst  c "
//			+ " where a.id = l.account_id  and l.company_mst = :companymstid   "
//			+ "and date(l.trans_date)<=:date and a.id=c.id group by a.account_name")
	
	@Query(nativeQuery = true, value = " select a.account_name, "  
			+ " (sum( l.debit_amount )-sum(l.credit_amount )) as balance"
			+ "  from ledger_class l ,  "
			+ " account_heads a  "
			+ " where a.id = l.account_id  and l.company_mst = :companymstid and  "
			+ " a.customer_or_supplier='BOTH' or a.customer_or_supplier='CUSTOMER' "
			+ " and date(l.trans_date)<=:date ")
	List<Object> getCustomerClosingBalance(String companymstid, Date date);
	
	/*
	 * Code for Derby db to get customer report in Account Closing Balance Report
	 */
	@Query(nativeQuery = true, value = " select a.account_name, "  
			+ " (sum( l.debit_amount )-sum(l.credit_amount )) as balance"
			+ "  from ledger_class l ,  "
			+ " account_heads a  "
			+ " where a.id = l.account_id  and l.company_mst = :companymstid and  "
			+ " a.customer_or_supplier='BOTH' or a.customer_or_supplier='CUSTOMER' "
			+ " and date(l.trans_date)<=:date group by a.account_name")
	List<Object> getCustomerClosingBalanceDerby(String companymstid, Date date);

	
//	@Query(nativeQuery = true, value = " select a.account_name, "  
//			+ " (sum(l.credit_amount )-sum( l.debit_amount )) as balance"
//			+ "  from ledger_class l ,  "
//			+ " account_heads a , supplier  s "
//			+ " where a.id = l.account_id  and l.company_mst = :companymstid   "
//			+ "and date(l.trans_date)<=:date and a.id=s.id group by a.account_name")
	
	@Query(nativeQuery = true, value = " select a.account_name, "  
			+ " (sum(l.credit_amount )-sum( l.debit_amount )) as balance"
			+ "  from ledger_class l ,  "
			+ " account_heads a "
			+ " where a.id = l.account_id  and l.company_mst = :companymstid and  "
			+ "a.customer_or_supplier='BOTH' or a.customer_or_supplier='SUPPLIER'  "
			+ "and date(l.trans_date)<=:date  group by a.account_name")
	List<Object> getSupplierClosingBalance(String companymstid, Date date);

	
	@Query(nativeQuery = true, value = " select  "  
			+ " (sum( l.debit_amount )-sum(l.credit_amount )) as balance"
			+ "  from ledger_class l ,  "
			+ " account_heads a  "
			+ " where a.id = l.account_id  and l.company_mst = :companymstid   "
			+ "and date(l.trans_date)<=:date and a.id=:id ")
	Double getBankAccountClosingBalance(String companymstid, Date date, String id);
	
	@Modifying
	@Query(nativeQuery = true,value="update ledger_class  set account_id =:toAccountId "
			+ "where account_id =:fromAccountId ")
	void accountMerge(String fromAccountId, String toAccountId);

	@Query(nativeQuery = true, value = " select  a.account_name,  sum(l.credit_amount ) ,\n" + 
			"	sum( l.debit_amount ), (sum( l.debit_amount )-sum(l.credit_amount )) as total, a.serial_number "
			+ "  from ledger_class l ,  "
			+ " account_heads a  where a.id = l.account_id  and l.company_mst = :companymstid  "
			+ "and a.parent_id=:parentid and date(l.trans_date)<=:date "
			+"	group by a.account_name,a.serial_number order by a.serial_number")

	List<Object> getTrialBalanceByParentId(String companymstid, Date date, String parentid);

	
	@Query(nativeQuery = true, value = " select  a.account_name,  sum(l.credit_amount ) ,\n" + 
			"	sum( l.debit_amount ), (sum( l.debit_amount )-sum(l.credit_amount )) as total"
			+ "  from ledger_class l ,  "
			+ " account_heads a  where a.id = l.account_id  and l.company_mst = :companymstid  "
			+ "and a.id=:accountid and date(l.trans_date)<=:date "
			+"	group by a.account_name ")
	List<Object> getTrialBalanceById(String companymstid, Date date, String accountid);
	
	
	@Query(nativeQuery = true, value = " select  a.account_name,  sum(l.credit_amount ) ,\n" + 
			"	sum( l.debit_amount ), (sum(l.credit_amount)-sum(l.debit_amount)) as total, a.serial_number "
			+ "  from ledger_class l ,  "
			+ " account_heads a  where a.id = l.account_id  and l.company_mst = :companymstid  "
			+ "and a.parent_id=:parentid and date(l.trans_date)<=:date "
			+"	group by a.account_name,a.serial_number order by a.serial_number")

	List<Object> getTrialBalanceByIncomeParentId(String companymstid, Date date, String parentid);

	
	@Query(nativeQuery = true, value = " select  b.branch_name,"
			+ " (sum(l.credit_amount)-sum(l.debit_amount)) as total "
			+ "  from ledger_class l , branch_mst b where l.account_id=:accountid  and l.company_mst =:companymstid  "
			+ " and date(l.trans_date)<=:date and l.branch_code=b.branch_code "
			+"	group by b.branch_name")
	List<Object> getBranchWiseAccountClosingBalance(String companymstid, Date date, String accountid);

	
	

}
