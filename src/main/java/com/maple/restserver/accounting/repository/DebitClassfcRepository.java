package com.maple.restserver.accounting.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.accounting.entity.AccountClassfc;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClassfc;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;

@Component
@Repository
public interface DebitClassfcRepository extends JpaRepository<DebitClassfc, String> {
	
	List<DebitClassfc>findByTransDate(Date vdate);
	@Query(nativeQuery = true,value= "select count(*) from debit_class_fc where branch_code=:branchCode "
			+ "and date(trans_date)=:vdate and company_mst=:companyMst")
	int countOfDebitClassfcByDate(String branchCode,Date vdate,String companyMst);
	List<DebitClassfc> findByAccountClassfcId(String accountClassId);
	
	 List<DebitClassfc> findByAccountClassfc(AccountClassfc accountClass);
	 List<DebitClassfc>  findByAccountClassfcAndAccountId(AccountClassfc accountClass, String accountId);

	
	void deleteByRemark(String remrk);
	
	void deleteByaccountClassfc(AccountClassfc accountClass);
	List<DebitClassfc> findByCompanyMst(CompanyMst companyMst);
	
	@Modifying
	@Query(nativeQuery = true,value="update debit_class_fc  set account_id =:toAccountId "
			+ "where account_id =:fromAccountId ")
	void accountMerge(String fromAccountId, String toAccountId);
	
}
