package com.maple.restserver.accounting.repository;

 
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
 
import com.maple.restserver.accounting.entity.AccountClassfc;
import com.maple.restserver.accounting.entity.LedgerClassfc;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.LedgerReport;

@Transactional
@Repository
public interface LedgerClassfcRepository extends JpaRepository<LedgerClassfc, String>{
	@Query(nativeQuery = true, value = " select  a.account_name,  sum(l.credit_amount ) ,\n" + 
			"	sum( l.debit_amount ), (sum( l.debit_amount )-sum(l.credit_amount )) as total"
			+ "  from ledger_class_fc l ,  "
			+ " account_heads a  where a.id = l.account_id  and l.company_mst = :companymstid   "
			+ " and date(l.trans_date)<=:date and "
			+ " (a.PARENT_ID=(select b.id from account_heads b where b.account_name='SUNDRY DEBTORS')) " + 
			"	group by a.account_name ")

	List<Object> getSundryDebtorsBalanceNew(String companymstid, Date date);
	
	@Query(nativeQuery = true,value="select count(*) from ledger_class_fc where "
			+ "branch_code = :branchCode and date(trans_date) = :vdate and "
			+ "company_mst =:companyMst")
	int countOfLedgerClass(String branchCode,Date vdate,String companyMst);
	
	List<LedgerClassfc>findByTransDate(Date vdate);
	List<LedgerClassfc> findByAccountClassfcIdAndAccountId(String accountClassId, String accountId);
	
	List<LedgerClassfc> findByAccountClassfcId(String accountClassId);

	void deleteByAccountClassfc(AccountClassfc accountClass);
	void deleteByRemark(String remark);
	
	
	@Query(nativeQuery = true, value = " select  a.account_name,  sum(l.credit_amount ) ,\n" + 
			"	sum( l.debit_amount ), (sum( l.debit_amount )-sum(l.credit_amount )) as total"
			+ "  from ledger_class_fc l ,  "
			+ " account_heads a  where a.id = l.account_id  and l.company_mst = :companymstid   "
			+"	group by a.account_name ")

	List<Object> getTrialBalance(String companymstid);
	
	@Query(nativeQuery = true, value = " select  a.account_name,  sum(l.credit_amount ) ,\n" + 
			"	sum( l.debit_amount ), (sum( l.debit_amount )-sum(l.credit_amount )) as total"
			+ "  from ledger_class_fc l ,  "
			+ " account_heads a  where a.id = l.account_id  and l.company_mst = :companymstid   "
			+ "and date(l.trans_date)<=:date " + 
			"	group by a.account_name ")

	List<Object> getTrialBalanceNew(String companymstid, Date date);
	
	@Query(nativeQuery = true, value = " select l.trans_date,  a.account_name,  l.remark, l.credit_amount  ,\n" + 
			"	l.debit_amount,  c.company_name "

			+ "    from ledger_class_fc l ,account_heads a , company_mst c where l.company_mst = :companymstid  "
			+ "   and  a.id = l.account_id "
			+ " and a.id =:accountid  and l.trans_date >= :startingDate and l.trans_date<= :endingDate   and c.id=l.company_mst  " + 
			"	order  by l.trans_date ")


	List<Object> getAccountStatement(String companymstid, String accountid, Date startingDate , Date endingDate );
	
	
	@Query(nativeQuery = true, value = " select   sum(l.credit_amount )  - 	sum( l.debit_amount )   from ledger_class_fc l , "
			+ "account_heads a  where a.id = l.account_id and a.account_name = :aAccount " + 
			"	  ")
	Double getAccountBalance(String aAccount);
	
	
	@Query(nativeQuery = true, value = " select   sum(l.credit_amount )  - 	sum( l.debit_amount )   from ledger_class_fc l , "
			+ "account_heads a  where a.id = l.account_id and a.account_name = :aAccount and l.transDate <= :endDate " + 
			"	  ")
	Double getClosingBalance(String aAccount, Date endDate);
	
	@Query(nativeQuery = true, value = " select   sum(l.credit_amount )  - 	sum( l.debit_amount )   from ledger_class_fc l , "
			+ "account_heads a  where a.id = l.account_id and a.account_name = :account and l.transDate < :startDate " + 
			"	  ")
	Double getOpeningBalance(String account, Date startDate);
	
	
	@Query(nativeQuery = true, value ="select sum(l.debit_amount),sum(l.credit_amount) from ledger_class_fc l,"
			+"account_heads a where l.account_id=a.id") 
	public List<Object> getLedgerReport();

	
	@Query(nativeQuery = true, value = " select   sum(l.credit_amount )  - 	sum( l.debit_amount )   from ledger_class_fc l ,account_heads a  "
			+ "  where l.trans_date < :startingDate and l.account_id=a.id and l.account_id=:accountid " + 
			"	  ")
	Double getledgerBalance(Date startingDate,String accountid);
	
	
	@Query(nativeQuery = true, value = " select   sum(l.credit_amount )  - 	sum( l.debit_amount )   from ledger_class_fc l ,account_heads a  "
			+ "  where l.trans_date <= :startingDate and l.account_id=a.id and l.account_id=:accountid " + 
			"	  ")
	Double getledgerClosingBalance(Date startingDate,String accountid);

	
	@Query(nativeQuery = true, value = " select l.trans_date, l.remark, l.credit_amount,"
			+ " l.debit_amount, "
			+ "c.company_name, "
			+ "p.supplier_inv_date, "
			+ "a.account_name "
			+ "from ledger_class_fc l, company_mst c, purchase_hdr p, account_heads a "
			+ "where p.FINAL_SAVED_STATUS = 'Y' and "
			+ "  l.company_mst=c.id "
			+ "and l.account_id=p.supplier_id "
			+ "and l.account_id=a.id and   l.source_voucher_id = p.voucher_number "
			+ "and c.id=:companymstid and l.account_id=:accountid "
			+ "and l.trans_date between :sDate and :eDate")
	List<Object> getSupplierAccountStatement(String companymstid, String accountid, Date sDate, Date eDate);


	
	
	@Query(nativeQuery = true, value = " select l.trans_date,  a.account_name,  l.remark, l.credit_amount  ,\n" + 
			"	l.debit_amount,  c.company_name "

			+ "    from ledger_class_fc l ,account_heads a , company_mst c where l.company_mst = :companymstid  "
			+ "   and  a.id = l.account_id "
			+ " and l.trans_date >= :startingDate and l.trans_date<= :endingDate   and c.id=l.company_mst  " + 
			"	order  by l.trans_date ")


	List<Object> getAccountStatementBetweenDate(String companymstid, Date startingDate , Date endingDate );

	List<LedgerClassfc> findByCompanyMst(CompanyMst companyMst);

	@Query(nativeQuery = true,value="select sum(l.debit_amount),sum(l.credit_amount),"
			+ "sum(l.credit_amount )  - 	sum( l.debit_amount ),"
			+ "c.customer_name from account_heads a,ledger_class_fc l,customer_mst c "
			+ " where c.id=a.id and a.id = l.account_id and l.trans_date >= :eDate and l.trans_date <=:sDate group by c.customer_name")
	List<Object> getAccountBalanceBetweendate(Date eDate, Date sDate);
	@Query(nativeQuery = true, value = " select  "  
			+ " (sum( l.debit_amount )-sum(l.credit_amount )) as total"
			+ "  from ledger_class_fc l ,  "
			+ " account_heads a  where a.id = l.account_id  and l.company_mst = :companymstid   "
			+ "and date(l.trans_date)<=:date and a.id=:accid")

	Double getCustomerBalance(String companymstid, Date date,String accid);

	
	@Query(nativeQuery = true, value = " select a.account_name, "  
			+ " (sum( l.debit_amount )-sum(l.credit_amount )) as balance"
			+ "  from ledger_class_fc l ,  "
			+ " account_heads a , customer_mst  c "
			+ " where a.id = l.account_id  and l.company_mst = :companymstid   "
			+ "and date(l.trans_date)<=:date and a.id=c.id group by a.account_name")
	List<Object> getCustomerClosingBalance(String companymstid, Date date);

	
//	@Query(nativeQuery = true, value = " select a.account_name, "  
//			+ " (sum(l.credit_amount )-sum( l.debit_amount )) as balance"
//			+ "  from ledger_class_fc l ,  "
//			+ " account_heads a , supplier  s "
//			+ " where a.id = l.account_id  and l.company_mst = :companymstid   "
//			+ "and date(l.trans_date)<=:date and a.id=s.id group by a.account_name")
	
	@Query(nativeQuery = true, value = " select a.account_name, "  
			+ " (sum(l.credit_amount )-sum( l.debit_amount )) as balance"
			+ "  from ledger_class_fc l ,  "
			+ " account_heads a  "
			+ " where a.id = l.account_id  and l.company_mst = :companymstid   "
			+ "and date(l.trans_date)<=:date  group by a.account_name")
	List<Object> getSupplierClosingBalance(String companymstid, Date date);

	
	@Query(nativeQuery = true, value = " select  "  
			+ " (sum( l.debit_amount )-sum(l.credit_amount )) as balance"
			+ "  from ledger_class_fc l ,  "
			+ " account_heads a  "
			+ " where a.id = l.account_id  and l.company_mst = :companymstid   "
			+ "and date(l.trans_date)<=:date and a.id=:id ")
	Double getBankAccountClosingBalance(String companymstid, Date date, String id);
	
	@Modifying
	@Query(nativeQuery = true,value="update ledger_class_fc  set account_id =:toAccountId "
			+ "where account_id =:fromAccountId ")
	void accountMerge(String fromAccountId, String toAccountId);

	
}
