package com.maple.restserver.accounting.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.AccountClassfc;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.CreditClassfc;
import com.maple.restserver.entity.AcceptStock;



@Component
@Repository
public interface CreditClassfcRepository extends JpaRepository<CreditClassfc, String>{
	
	List<CreditClassfc>findByTransDate(Date vdate);
	@Query(nativeQuery = true,value = "select count(*) from credit_class_fc where branch_code=:branchCode "
			+ "and date(trans_date) =:vdate and company_mst = :companyMst")
	int countOfCreditClass(String branchCode,Date vdate,String companyMst);
	
	List<CreditClassfc> findByAccountClassfcId(String accountClassId);
	
	
	 List<CreditClassfc>  findByAccountClassfc(AccountClassfc accountClass);
	 List<CreditClassfc>  findByAccountClassfcAndAccountId(AccountClassfc accountClass, String accountId);
	 
	void deleteByRemark(String remark);
	
	void deleteByAccountClassfc(AccountClassfc accountClass);
	List<CreditClass> findByCompanyMstId(String id);
	
	@Modifying
	@Query(nativeQuery = true,value="update credit_class_fc  set account_id =:toAccountId "
			+ "where account_id =:fromAccountId ")
	void accountMerge(String fromAccountId, String toAccountId);
}
