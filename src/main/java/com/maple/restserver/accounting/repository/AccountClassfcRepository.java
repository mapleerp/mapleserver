
package com.maple.restserver.accounting.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.AccountClassfc;
import com.maple.restserver.entity.CompanyMst;


@Component
@Transactional
@Repository
public interface AccountClassfcRepository extends JpaRepository<AccountClassfc, String>{
	
	
	@Query(nativeQuery = true,value="select * from account_class_fc "
			+ "where date(trans_date) >= :fdate and date(trans_date) <= :tdate")
	List<AccountClassfc> getAccountClassBetweenDate(Date fdate,Date tdate);
	List<AccountClassfc>findByTransDate(Date voucherDate);
	
List<AccountClassfc>findByCompanyMstId(String companymstid);
List<AccountClassfc>findBySourceVoucherNumberAndTransDate(String voucherNumber, Date voucherDate );
List<AccountClassfc>findBySourceVoucherNumber(String voucherNumber);

void deleteBySourceVoucherNumberAndTransDate(String voucherNumber, Date voucherDate );
void deleteBySourceVoucherNumber(String voucherNumber );

@Query(nativeQuery = true,value = "select count(*) from account_class_fc where brach_code=:branchCode"
		+ " and date(trans_date) =:vDate and company_mst = :companyMst") 
int countOfAccountClassByDate(String branchCode,Date vDate, String companyMst);
List<AccountClassfc> findByCompanyMstAndBrachCode(CompanyMst companyMst, String branchcode);

void deleteBySourceParentId(String string);


}
