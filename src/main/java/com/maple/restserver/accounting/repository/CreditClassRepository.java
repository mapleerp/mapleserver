package com.maple.restserver.accounting.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.entity.AcceptStock;



@Component
@Repository
public interface CreditClassRepository extends JpaRepository<CreditClass, String>{
	
	List<CreditClass>findByTransDate(Date vdate);
	@Query(nativeQuery = true,value = "select count(*) from credit_class where branch_code=:branchCode "
			+ "and date(trans_date) =:vdate and company_mst = :companyMst")
	int countOfCreditClass(String branchCode,Date vdate,String companyMst);
	
	List<CreditClass> findByAccountClassId(String accountClassId);
	
	
	 List<CreditClass>  findByAccountClass(AccountClass accountClass);
	 List<CreditClass>  findByAccountClassAndAccountId(AccountClass accountClass, String accountId);
	 
	void deleteByRemark(String remark);
	
	void deleteByAccountClass(AccountClass accountClass);
	List<CreditClass> findByCompanyMstId(String id);
	
	@Modifying
	@Query(nativeQuery = true,value="update credit_class  set account_id =:toAccountId "
			+ "where account_id =:fromAccountId ")
	void accountMerge(String fromAccountId, String toAccountId);
}
