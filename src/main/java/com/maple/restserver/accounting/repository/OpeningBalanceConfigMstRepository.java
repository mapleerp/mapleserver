package com.maple.restserver.accounting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.accounting.entity.OpeningBalanceConfigMst;
@Repository
public interface OpeningBalanceConfigMstRepository extends JpaRepository<OpeningBalanceConfigMst,String> {

}
