package com.maple.restserver.accounting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.accounting.entity.InventoryLedgerMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;

@Component
@Repository
public interface InventoryLedgerMstRepository extends JpaRepository<InventoryLedgerMst, String> {

	
	@Query(nativeQuery = true,value= "select  sum(dr_qty)"
			+ " from inventory_ledger_mst where "
			+ " item_mst =:itemMst  "
			+ " and  batch = :batch "
			+ " and company_mst=:companyMst"
			+ " and  branch_mst=:branchCode  ")
	Double findBySumQtyItemMstAndatchAndCompanyMstAndBranchMst(String itemMst, String batch, 
			String companyMst,
			String branchCode);
	
	
	@Query(nativeQuery = true,value= "select sum(dr_amount) "
			+ " from inventory_ledger_mst where "
			+ " item_mst =:itemMst  "
			+ " and  batch = :batch "
			+ " and company_mst=:companyMst"
			+ " and  branch_mst=:branchCode  ")
	Double findBySumAmtItemMstAndatchAndCompanyMstAndBranchMst(String itemMst, String batch, 
			String companyMst,
			String branchCode);
	

}
