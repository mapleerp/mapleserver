package com.maple.restserver.mobileResource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.MobileCustomerMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.mobileEntity.MobileCartDtl;
import com.maple.restserver.mobileEntity.MobileCartHdr;
import com.maple.restserver.mobileRepository.MobileCartDtlRepository;
import com.maple.restserver.mobileRepository.MobileCartHdrRepository;
import com.maple.restserver.mobileRepository.MobileCustomerRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
public class MobCartDtlResource {

	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	MobileCartDtlRepository mobileCartDtlRepository;
	
	@Autowired
	MobileCartHdrRepository mobileCartHdrRepository;
	
	@Autowired
	MobileCustomerRepository mobileCustomerRepository;
	
	

	@PostMapping("{companymstid}/mobcartdtlresource/savemobcart")
	public List <MobileCartDtl> createMobileCartDtl(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("custmobno") String custmobno,
			@RequestParam("itemname") String itemname,
			@RequestParam("itemid") String itemid,
			@RequestParam("qty") String qty
			) {
		
		
		    return companyMstRepository.findById(companymstid).map(companyMst -> {
		    	
		    	
		    MobileCartHdr mobileCartHdr = null;
			

//			List<MobileCartHdr> MobileCartHdrList=mobileCartHdrRepository.findByMobileCustomerMstCustomerContact(custmobno);
			
			
			
			
//			if(MobileCartHdrList.size()==0) {
			 mobileCartHdr = new MobileCartHdr();

				MobileCustomerMst mobileCustomerMst=mobileCustomerRepository.findByMobNumber(custmobno);
				if(null==mobileCustomerMst) {
				return null;
				}
				mobileCartHdr.setMobileCustomerMst(mobileCustomerMst);
				mobileCartHdr.setCompanyMst(companyMst);
				mobileCartHdr.setStatus("ACTIVE");
				mobileCartHdr = mobileCartHdrRepository.saveAndFlush(mobileCartHdr);
				
//			} else {
//				mobileCartHdr =MobileCartHdrList.get(0);
//			}
			
			
			
			MobileCartDtl mobileCartDtl = new MobileCartDtl();
			mobileCartDtl.setCompanyMst(companyMst);
			mobileCartDtl.setItemId(itemid);
			mobileCartDtl.setItemName(itemname);
			mobileCartDtl.setMobileCartHdr(mobileCartHdr);
			mobileCartDtl.setQty(qty);
			mobileCartDtl=mobileCartDtlRepository.saveAndFlush(mobileCartDtl);
			
			
			return mobileCartDtlRepository.findByMobileCartHdr(mobileCartHdr);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}
	
	
//	 @GetMapping("{companymstid}/mobcarthdrresource/activemoilecarts")
//		public List<MobileCartDtl> getMobileCartDtlByCustomer(
//				@RequestParam("custmobno") String custmobno,
//				@PathVariable(value = "companymstid") String  companymstid) {
////			List<MobileCartHdr> MobileCartHdrList=mobileCartHdrRepository.findByMobileCustomerMstCustomerContact(custmobno);
//
//		 
//			return mobileCartDtlRepository.findByMobileCartHdr(MobileCartHdrList.get(0));
//
//			
//		}
}
