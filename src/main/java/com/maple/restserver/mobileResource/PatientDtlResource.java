package com.maple.restserver.mobileResource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.mobileEntity.PatientDtl;
import com.maple.restserver.mobileRepository.PatientDtlRepository;

@RestController
public class PatientDtlResource {

	
	@Autowired
	PatientDtlRepository patientDtlRepository;
	
	
	
	@GetMapping("{companymstid}/patientdtlresource/savepatientdtl")
	public List <PatientDtl> createPatientDtl(
			
			@RequestParam("deviceid") String deviceid,
			@RequestParam("locationlongitude") String locationlongitude,
			@RequestParam("locationlatitude") String locationlatitude,
			@RequestParam("covidstatus") String covidstatus
			) {
		
		PatientDtl patientDtl=new PatientDtl();
		patientDtl.setDeviceId(deviceid);
		
		patientDtl.setLocationLatitude(locationlatitude);
		patientDtl.setLocationLongitude(locationlongitude);
		patientDtl.setCovidStatus(covidstatus);
		patientDtl=	patientDtlRepository.saveAndFlush(patientDtl);
		
		return patientDtlRepository.findAll();
		
	}
}
