package com.maple.restserver.mobileResource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.mobileEntity.MobileCartDtl;
import com.maple.restserver.mobileEntity.MobileCartHdr;

import com.maple.restserver.mobileRepository.MobileCartHdrRepository;
import com.maple.restserver.mobileService.MobileCartHdrService;
import com.maple.restserver.report.entity.MobileOrderReports;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class MobCartHdrResource {

	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	
	@Autowired
	MobileCartHdrRepository mobileCartHdrRepository;
	
	@Autowired
	MobileCartHdrService mobileCartHdrService;
	
	
	@PostMapping("{companymstid}/mobcarthdrresource/mobcarthdr")
	public MobileCartHdr createMobileCartHdr(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody MobileCartHdr mobileCartHdr) {
		return companyMstRepository.findById(companymstid).map(companyMst -> {
			mobileCartHdr.setCompanyMst(companyMst);
			return mobileCartHdrRepository.saveAndFlush(mobileCartHdr);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}
	
	
	
	 @PutMapping("{companymstid}/mobcarthdrresource/updatemobilecarthdr{mobileecarthdrid}")
	    public MobileCartHdr MobileCartHdrFinalSave(@PathVariable String moblilecustid,
	    @Valid @RequestBody MobileCartHdr mobileCartRequest,
	    @PathVariable(value = "companymstid") String companymstid) {

	 //   Optional<CompanyMst> companyMstOPt=companyMstRepository.findById(companymstid);
		Optional<MobileCartHdr> mobileCartHdrOpt = mobileCartHdrRepository.findById(moblilecustid);
		MobileCartHdr MobileCartHdr= new MobileCartHdr();
		MobileCartHdr=mobileCartHdrOpt.get();
		MobileCartHdr.setVoucherNumber(mobileCartRequest.getVoucherNumber());
		MobileCartHdr.setVoucherDate(mobileCartRequest.getVoucherDate());
		return MobileCartHdr;
	}
	 
	 
	 @GetMapping("{companymstid}/mobcarthdrresource/activemoilecarts")
		public List<MobileOrderReports> getMobileOrderReports(
				
				@PathVariable(value = "companymstid") String  companymstid) {
		 
		 return mobileCartHdrService.getActiveCarts(companymstid);
			
		}
	 
	
	
}
