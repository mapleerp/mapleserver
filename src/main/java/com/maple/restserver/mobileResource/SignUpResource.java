package com.maple.restserver.mobileResource;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Random;
import java.util.ResourceBundle.Control;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.catalina.WebResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductionBatchStockDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.mobileEntity.MobileCustomer;
import com.maple.restserver.mobileRepository.MobileCustomerRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;

import com.maple.restserver.utils.SystemSetting;

import ch.qos.logback.core.net.server.Client;
 
@Transactional
@RestController
public class SignUpResource {
@Autowired
CompanyMstRepository companyMstRepository;
	
@Autowired
MobileCustomerRepository mobileCustomerRepository;

@Autowired
AccountHeadsRepository accountHeadsRepository;
@PostMapping("{companymstid}/signupresource/loginmobcustomer")
public MobileCustomer loginMobCustomer(@PathVariable(value = "companymstid") String
		  companymstid,
		   @Valid @RequestBody 
		  MobileCustomer  mobileCustomer)
{
				String otp = getOtp(4);
				System.out.println("otp  "+otp);
				mobileCustomer.setOtp(otp);
				otp = " Your Login OTP for Ambrosia Food Order System is " +otp;
				
				otp = otp.replaceAll(" ", "%20");
			
				System.out.println("http://alerts.smsclogin.com/api/web2sms.php?workingkey=A4be256ac86f90f8809d00f77a6059e26&sender=STMARY&to="+mobileCustomer.getMobNumber()+"&message="+otp);
				//Client client = Client.create();
				//WebResource webResource = client
				//		.resource("http://alerts.smsclogin.com/api/web2sms.php?workingkey=A4be256ac86f90f8809d00f77a6059e26&sender=STMARY&to="+mobileCustomer.getMobNumber()+"&message="+otp);

				
			return mobileCustomer;
			}



private String getOtp(int len)
{ 
	System.out.println("Generating OTP using random() : "); 
	System.out.print("You OTP is : "); 
  String strOTP =""; 
// Using numeric values 
String numbers = "0123456789"; 

// Using random method 
Random rndm_method = new Random(); 

char[] otp = new char[len]; 

for (int i = 0; i < len; i++) 
{ 
    // Use of charAt() method : to get character value 
    // Use of nextInt() as it is scanning the value as int 
    otp[i] = 
     numbers.charAt(rndm_method.nextInt(numbers.length())); 
    System.out.print( otp[i]);
    strOTP = strOTP + otp[i];
} 
	return strOTP; 
	
	
}


    @Transactional
    @PutMapping("{companymstid}/signupresource/updatemobilecustomerstatus{moblilecustid}")
    public MobileCustomer MobileCustomerFinalSave(@PathVariable String moblilecustid,
    @Valid @RequestBody MobileCustomer mobileCustomerRequest,
    @PathVariable(value = "companymstid") String companymstid) {

    Optional<CompanyMst> companyMstOPt=companyMstRepository.findById(companymstid);
	Optional<MobileCustomer> mobileCustMstOpt = mobileCustomerRepository.findById(moblilecustid);
	MobileCustomer  mobileCustomer= new MobileCustomer();
	mobileCustomer=mobileCustMstOpt.get();
	mobileCustomer.setCustomerStatus(mobileCustomerRequest.getCustomerStatus());
	mobileCustomer= mobileCustomerRepository.saveAndFlush(mobileCustomer);
	AccountHeads accountHeads= new AccountHeads();
	
	accountHeads.setCompanyMst(companyMstOPt.get());
	accountHeads.setCustomerContact(mobileCustomer.getMobNumber());
	accountHeads.setAccountName(mobileCustomer.getCustomerName());
	accountHeads =accountHeadsRepository.saveAndFlush(accountHeads);
	
	return mobileCustomer;
}




}
