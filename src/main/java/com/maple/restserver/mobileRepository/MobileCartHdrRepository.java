package com.maple.restserver.mobileRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.mobileEntity.MobileCartHdr;

@Repository
public interface MobileCartHdrRepository extends JpaRepository<MobileCartHdr, String> {

//	List<MobileCartHdr>findByMobileCustomerMstCustomerContact(String mobNo);

	List<MobileCartHdr> findByStatusAndCompanyMst(String string, String companymstid);

	
	
}
