package com.maple.restserver.mobileRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.mobileEntity.PatientDtl;

@Repository
public interface PatientDtlRepository  extends JpaRepository<PatientDtl, String>{

	
	
}
