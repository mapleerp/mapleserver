package com.maple.restserver.mobileRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.mobileEntity.MobileCartDtl;
import com.maple.restserver.mobileEntity.MobileCartHdr;

@Repository
public interface MobileCartDtlRepository extends JpaRepository<MobileCartDtl, String>{

	 List <MobileCartDtl> findByMobileCartHdr(MobileCartHdr mobileCartHdr);


}
