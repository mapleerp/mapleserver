package com.maple.restserver.mobileRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.MobileCustomerMst;
import com.maple.restserver.mobileEntity.MobileCustomer;

@Repository
public interface MobileCustomerRepository extends JpaRepository<MobileCustomer, String> {

	MobileCustomerMst findByMobNumber(String custmobno);

	
	
}
