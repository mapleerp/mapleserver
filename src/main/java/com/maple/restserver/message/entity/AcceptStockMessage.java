package com.maple.restserver.message.entity;

import java.io.Serializable;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.restserver.entity.CompanyMst;
 

@Component
public class AcceptStockMessage implements Serializable {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	String stockTransferFrom;
	@JsonProperty("voucherNumber")
	private String  voucherNumber;
	
	String StockTransferOutHdrId;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStockTransferFrom() {
		return stockTransferFrom;
	}

	public void setStockTransferFrom(String stockTransferFrom) {
		this.stockTransferFrom = stockTransferFrom;
	}



 

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getStockTransferOutHdrId() {
		return StockTransferOutHdrId;
	}

	public void setStockTransferOutHdrId(String stockTransferOutHdrId) {
		StockTransferOutHdrId = stockTransferOutHdrId;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	@Override
	public String toString() {
		return "AcceptStockMessage [id=" + id + ", stockTransferFrom=" + stockTransferFrom + ", voucherNumber="
				+ voucherNumber + ", StockTransferOutHdrId=" + StockTransferOutHdrId + ", companyMst=" + companyMst
				+ "]";
	}

	
	
	
	

}
