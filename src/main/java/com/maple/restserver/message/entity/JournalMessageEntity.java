package com.maple.restserver.message.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.entity.PaymentDtl;
@Component
public class JournalMessageEntity implements Serializable{
JournalHdr journalHdr;
ArrayList<JournalDtl> journalDtlList =new ArrayList();
public JournalHdr getJournalHdr() {
	return journalHdr;
}
public void setJournalHdr(JournalHdr journalHdr) {
	this.journalHdr = journalHdr;
}
public ArrayList<JournalDtl> getJournalDtlList() {
	return journalDtlList;
}
public void setJournalDtlList(ArrayList<JournalDtl> journalDtlList) {
	this.journalDtlList = journalDtlList;
}
@Override
public String toString() {
	return "JournalMessageEntity [journalHdr=" + journalHdr + ", journalDtlList=" + journalDtlList + "]";
}
	
	
}
