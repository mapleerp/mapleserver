package com.maple.restserver.message.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.LocalCustomerMst;

@Component
public class SalesTransHdrMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	// private CompanyMst companyMst;
	private CustomerMstMessage customerMst;
	private LocalCustomerMst localCustomerMst;

	private String customerName;
	private String customerId;
	private String salesManId;
	private String deliveryBoyId;

	private String salesMode;
	private String creditOrCash;
	private String userId;
	String branchCode;

	String fbKey;

	private String invoiceNumberPrefix;

	private Long numericVoucherNumber;

	private String machineId;

	private String voucherNumber;

	private Date voucherDate;

	private String strVoucherDate;

	private String strInvoiceAmount;

	private String totalCGST;
	private String totalSGST;
	private String totalKFC;

	private Double invoiceAmount;

	private Double invoiceDiscount;
	private Double cashPay;
	private String discount;

	private Double amountTendered;

	private Double itemDiscount;

	private Double paidAmount;

	private Double changeAmount;

	private String cardNo;

	private String cardType;

	private Double cardamount;

	private String voucherType;

	private String servingTableName;

	private String isBranchSales;

	private Double sodexoAmount;

	private Double paytmAmount;

	private Double creditAmount;

	private String saleOrderHrdId;

	private String salesReceiptsVoucherNumber;
	private String SourceIP;
	private String SourcePort;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CustomerMstMessage getCustomerMst() {
		return customerMst;
	}
	public void setCustomerMst(CustomerMstMessage customerMst) {
		this.customerMst = customerMst;
	}
	public LocalCustomerMst getLocalCustomerMst() {
		return localCustomerMst;
	}
	public void setLocalCustomerMst(LocalCustomerMst localCustomerMst) {
		this.localCustomerMst = localCustomerMst;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getSalesManId() {
		return salesManId;
	}
	public void setSalesManId(String salesManId) {
		this.salesManId = salesManId;
	}
	public String getDeliveryBoyId() {
		return deliveryBoyId;
	}
	public void setDeliveryBoyId(String deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}
	public String getSalesMode() {
		return salesMode;
	}
	public void setSalesMode(String salesMode) {
		this.salesMode = salesMode;
	}
	public String getCreditOrCash() {
		return creditOrCash;
	}
	public void setCreditOrCash(String creditOrCash) {
		this.creditOrCash = creditOrCash;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getFbKey() {
		return fbKey;
	}
	public void setFbKey(String fbKey) {
		this.fbKey = fbKey;
	}
	public String getInvoiceNumberPrefix() {
		return invoiceNumberPrefix;
	}
	public void setInvoiceNumberPrefix(String invoiceNumberPrefix) {
		this.invoiceNumberPrefix = invoiceNumberPrefix;
	}
	public Long getNumericVoucherNumber() {
		return numericVoucherNumber;
	}
	public void setNumericVoucherNumber(Long numericVoucherNumber) {
		this.numericVoucherNumber = numericVoucherNumber;
	}
	public String getMachineId() {
		return machineId;
	}
	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getStrVoucherDate() {
		return strVoucherDate;
	}
	public void setStrVoucherDate(String strVoucherDate) {
		this.strVoucherDate = strVoucherDate;
	}
	public String getStrInvoiceAmount() {
		return strInvoiceAmount;
	}
	public void setStrInvoiceAmount(String strInvoiceAmount) {
		this.strInvoiceAmount = strInvoiceAmount;
	}
	public String getTotalCGST() {
		return totalCGST;
	}
	public void setTotalCGST(String totalCGST) {
		this.totalCGST = totalCGST;
	}
	public String getTotalSGST() {
		return totalSGST;
	}
	public void setTotalSGST(String totalSGST) {
		this.totalSGST = totalSGST;
	}
	public String getTotalKFC() {
		return totalKFC;
	}
	public void setTotalKFC(String totalKFC) {
		this.totalKFC = totalKFC;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public Double getInvoiceDiscount() {
		return invoiceDiscount;
	}
	public void setInvoiceDiscount(Double invoiceDiscount) {
		this.invoiceDiscount = invoiceDiscount;
	}
	public Double getCashPay() {
		return cashPay;
	}
	public void setCashPay(Double cashPay) {
		this.cashPay = cashPay;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public Double getAmountTendered() {
		return amountTendered;
	}
	public void setAmountTendered(Double amountTendered) {
		this.amountTendered = amountTendered;
	}
	public Double getItemDiscount() {
		return itemDiscount;
	}
	public void setItemDiscount(Double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Double getChangeAmount() {
		return changeAmount;
	}
	public void setChangeAmount(Double changeAmount) {
		this.changeAmount = changeAmount;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public Double getCardamount() {
		return cardamount;
	}
	public void setCardamount(Double cardamount) {
		this.cardamount = cardamount;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getServingTableName() {
		return servingTableName;
	}
	public void setServingTableName(String servingTableName) {
		this.servingTableName = servingTableName;
	}
	public String getIsBranchSales() {
		return isBranchSales;
	}
	public void setIsBranchSales(String isBranchSales) {
		this.isBranchSales = isBranchSales;
	}
	public Double getSodexoAmount() {
		return sodexoAmount;
	}
	public void setSodexoAmount(Double sodexoAmount) {
		this.sodexoAmount = sodexoAmount;
	}
	public Double getPaytmAmount() {
		return paytmAmount;
	}
	public void setPaytmAmount(Double paytmAmount) {
		this.paytmAmount = paytmAmount;
	}
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public String getSaleOrderHrdId() {
		return saleOrderHrdId;
	}
	public void setSaleOrderHrdId(String saleOrderHrdId) {
		this.saleOrderHrdId = saleOrderHrdId;
	}
	public String getSalesReceiptsVoucherNumber() {
		return salesReceiptsVoucherNumber;
	}
	public void setSalesReceiptsVoucherNumber(String salesReceiptsVoucherNumber) {
		this.salesReceiptsVoucherNumber = salesReceiptsVoucherNumber;
	}
	public String getSourceIP() {
		return SourceIP;
	}
	public void setSourceIP(String sourceIP) {
		SourceIP = sourceIP;
	}
	public String getSourcePort() {
		return SourcePort;
	}
	public void setSourcePort(String sourcePort) {
		SourcePort = sourcePort;
	}
	@Override
	public String toString() {
		return "SalesTransHdrMessage [id=" + id + ", customerMst=" + customerMst + ", localCustomerMst="
				+ localCustomerMst + ", customerName=" + customerName + ", customerId=" + customerId + ", salesManId="
				+ salesManId + ", deliveryBoyId=" + deliveryBoyId + ", salesMode=" + salesMode + ", creditOrCash="
				+ creditOrCash + ", userId=" + userId + ", branchCode=" + branchCode + ", fbKey=" + fbKey
				+ ", invoiceNumberPrefix=" + invoiceNumberPrefix + ", numericVoucherNumber=" + numericVoucherNumber
				+ ", machineId=" + machineId + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", strVoucherDate=" + strVoucherDate + ", strInvoiceAmount=" + strInvoiceAmount + ", totalCGST="
				+ totalCGST + ", totalSGST=" + totalSGST + ", totalKFC=" + totalKFC + ", invoiceAmount=" + invoiceAmount
				+ ", invoiceDiscount=" + invoiceDiscount + ", cashPay=" + cashPay + ", discount=" + discount
				+ ", amountTendered=" + amountTendered + ", itemDiscount=" + itemDiscount + ", paidAmount=" + paidAmount
				+ ", changeAmount=" + changeAmount + ", cardNo=" + cardNo + ", cardType=" + cardType + ", cardamount="
				+ cardamount + ", voucherType=" + voucherType + ", servingTableName=" + servingTableName
				+ ", isBranchSales=" + isBranchSales + ", sodexoAmount=" + sodexoAmount + ", paytmAmount=" + paytmAmount
				+ ", creditAmount=" + creditAmount + ", saleOrderHrdId=" + saleOrderHrdId
				+ ", salesReceiptsVoucherNumber=" + salesReceiptsVoucherNumber + ", SourceIP=" + SourceIP
				+ ", SourcePort=" + SourcePort + "]";
	}

	
	
}
