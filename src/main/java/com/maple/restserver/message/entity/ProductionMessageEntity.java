package com.maple.restserver.message.entity;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionDtlDtl;
import com.maple.restserver.entity.ProductionMst;
@Component
public class ProductionMessageEntity  implements Serializable{
private static final long serialVersionUID = 1L;
ProductionMst productionMst;
ArrayList<ProductionDtl> productionDtlList =new ArrayList();
ArrayList<ProductionDtlDtl> productionDtlDtlList =new ArrayList();
public ProductionMst getProductionMst() {
	return productionMst;
}
public void setProductionMst(ProductionMst productionMst) {
	this.productionMst = productionMst;
}
public ArrayList<ProductionDtl> getProductionDtlList() {
	return productionDtlList;
}
public void setProductionDtlList(ArrayList<ProductionDtl> productionDtlList) {
	this.productionDtlList = productionDtlList;
}
public ArrayList<ProductionDtlDtl> getProductionDtlDtlList() {
	return productionDtlDtlList;
}
public void setProductionDtlDtlList(ArrayList<ProductionDtlDtl> productionDtlDtlList) {
	this.productionDtlDtlList = productionDtlDtlList;
}
@Override
public String toString() {
	return "ProductionMessageEntity [productionMst=" + productionMst + ", productionDtlList=" + productionDtlList
			+ ", productionDtlDtlList=" + productionDtlDtlList + "]";
}



}
