package com.maple.restserver.message.entity;

import java.util.ArrayList;

import com.maple.restserver.entity.SchEligiAttrListDef;
import com.maple.restserver.entity.SchEligibilityAttribInst;
import com.maple.restserver.entity.SchEligibilityDef;
import com.maple.restserver.entity.SchOfferAttrInst;
import com.maple.restserver.entity.SchOfferAttrListDef;
import com.maple.restserver.entity.SchOfferDef;
import com.maple.restserver.entity.SchSelectAttrListDef;
import com.maple.restserver.entity.SchSelectDef;
import com.maple.restserver.entity.SchSelectionAttribInst;
import com.maple.restserver.entity.SchemeInstance;

public class SchemeMessage {

	SchemeInstance schemeInstance;
	
	SchOfferDef schOfferDef;
	ArrayList<SchOfferAttrInst> schOfferAttrInst = new ArrayList();
	ArrayList<SchOfferAttrListDef> schOfferAttrListDef = new ArrayList();
	
	SchEligibilityDef schEligibilityDef;
	ArrayList<SchEligiAttrListDef> schEligiAttrListDef = new ArrayList();
	ArrayList<SchEligibilityAttribInst> schEligibilityAttribInst = new ArrayList();
	
	SchSelectDef schSelectDef;
	ArrayList<SchSelectionAttribInst> schSelectionAttribInst = new ArrayList();
	ArrayList<SchSelectAttrListDef> schSelectAttrListDef = new ArrayList();
	public SchemeInstance getSchemeInstance() {
		return schemeInstance;
	}
	public void setSchemeInstance(SchemeInstance schemeInstance) {
		this.schemeInstance = schemeInstance;
	}
	public SchOfferDef getSchOfferDef() {
		return schOfferDef;
	}
	public void setSchOfferDef(SchOfferDef schOfferDef) {
		this.schOfferDef = schOfferDef;
	}
	public ArrayList<SchOfferAttrInst> getSchOfferAttrInst() {
		return schOfferAttrInst;
	}
	public void setSchOfferAttrInst(ArrayList<SchOfferAttrInst> schOfferAttrInst) {
		this.schOfferAttrInst = schOfferAttrInst;
	}
	public ArrayList<SchOfferAttrListDef> getSchOfferAttrListDef() {
		return schOfferAttrListDef;
	}
	public void setSchOfferAttrListDef(ArrayList<SchOfferAttrListDef> schOfferAttrListDef) {
		this.schOfferAttrListDef = schOfferAttrListDef;
	}
	public SchEligibilityDef getSchEligibilityDef() {
		return schEligibilityDef;
	}
	public void setSchEligibilityDef(SchEligibilityDef schEligibilityDef) {
		this.schEligibilityDef = schEligibilityDef;
	}
	public ArrayList<SchEligiAttrListDef> getSchEligiAttrListDef() {
		return schEligiAttrListDef;
	}
	public void setSchEligiAttrListDef(ArrayList<SchEligiAttrListDef> schEligiAttrListDef) {
		this.schEligiAttrListDef = schEligiAttrListDef;
	}
	public ArrayList<SchEligibilityAttribInst> getSchEligibilityAttribInst() {
		return schEligibilityAttribInst;
	}
	public void setSchEligibilityAttribInst(ArrayList<SchEligibilityAttribInst> schEligibilityAttribInst) {
		this.schEligibilityAttribInst = schEligibilityAttribInst;
	}
	public SchSelectDef getSchSelectDef() {
		return schSelectDef;
	}
	public void setSchSelectDef(SchSelectDef schSelectDef) {
		this.schSelectDef = schSelectDef;
	}
	public ArrayList<SchSelectionAttribInst> getSchSelectionAttribInst() {
		return schSelectionAttribInst;
	}
	public void setSchSelectionAttribInst(ArrayList<SchSelectionAttribInst> schSelectionAttribInst) {
		this.schSelectionAttribInst = schSelectionAttribInst;
	}
	public ArrayList<SchSelectAttrListDef> getSchSelectAttrListDef() {
		return schSelectAttrListDef;
	}
	public void setSchSelectAttrListDef(ArrayList<SchSelectAttrListDef> schSelectAttrListDef) {
		this.schSelectAttrListDef = schSelectAttrListDef;
	}
	
	
	
}
