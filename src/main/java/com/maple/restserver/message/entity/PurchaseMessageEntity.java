package com.maple.restserver.message.entity;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;

@Component
public class PurchaseMessageEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	PurchaseHdr purchaseHdr;
	ArrayList<PurchaseDtl> purchaseDtlList =new ArrayList();
	public PurchaseHdr getPurchaseHdr() {
		return purchaseHdr;
	}
	public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
		this.purchaseHdr = purchaseHdr;
	}
	public ArrayList<PurchaseDtl> getPurchaseDtlList() {
		return purchaseDtlList;
	}
	public void setPurchaseDtlList(ArrayList<PurchaseDtl> purchaseDtlList) {
		this.purchaseDtlList = purchaseDtlList;
	}
	@Override
	public String toString() {
		return "PurchaseMessageEntity [purchaseHdr=" + purchaseHdr + ", purchaseDtlList=" + purchaseDtlList + "]";
	}



}
