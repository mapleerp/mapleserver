package com.maple.restserver.message.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.SalesReceipts;
@Component
public class PaymentMessageEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	    PaymentHdr paymenthdr;
		ArrayList<PaymentDtl> paymentDtlList =new ArrayList();
		public PaymentHdr getPaymenthdr() {
			return paymenthdr;
		}
		public void setPaymenthdr(PaymentHdr paymenthdr) {
			this.paymenthdr = paymenthdr;
		}
		public ArrayList<PaymentDtl> getPaymentDtlList() {
			return paymentDtlList;
		}
		public void setPaymentDtlList(ArrayList<PaymentDtl> paymentDtlList) {
			this.paymentDtlList = paymentDtlList;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		@Override
		public String toString() {
			return "PaymentMessageEntity [paymenthdr=" + paymenthdr + ", paymentDtlList=" + paymentDtlList + "]";
		}
		
		
		
		
}
