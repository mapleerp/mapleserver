package com.maple.restserver.message.entity;

import java.util.ArrayList;

import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.JournalHdr;

public class JournalHdrAndDtlMessage {
	
	JournalHdr journalHdr;
	ArrayList<JournalDtl> journalDtlList =  new ArrayList();
	public JournalHdr getJournalHdr() {
		return journalHdr;
	}
	public void setJournalHdr(JournalHdr journalHdr) {
		this.journalHdr = journalHdr;
	}
	public ArrayList<JournalDtl> getJournalDtlList() {
		return journalDtlList;
	}
	public void setJournalDtlList(ArrayList<JournalDtl> journalDtlList) {
		this.journalDtlList = journalDtlList;
	}
	
	
}
