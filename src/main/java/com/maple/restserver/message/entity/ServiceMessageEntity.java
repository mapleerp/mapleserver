package com.maple.restserver.message.entity;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.ServiceInDtl;
import com.maple.restserver.entity.ServiceInHdr;

@Component
public class ServiceMessageEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	ServiceInHdr serviceInHdr;
	ArrayList<ServiceInDtl> serviceInDtlList =new ArrayList();
	public ServiceInHdr getServiceInHdr() {
		return serviceInHdr;
	}
	public void setServiceInHdr(ServiceInHdr serviceInHdr) {
		this.serviceInHdr = serviceInHdr;
	}
	public ArrayList<ServiceInDtl> getServiceInDtlList() {
		return serviceInDtlList;
	}
	public void setServiceInDtlList(ArrayList<ServiceInDtl> serviceInDtlList) {
		this.serviceInDtlList = serviceInDtlList;
	}
	@Override
	public String toString() {
		return "ServiceMessageEntity [serviceInHdr=" + serviceInHdr + ", serviceInDtlList=" + serviceInDtlList + "]";
	}
	
	
}
