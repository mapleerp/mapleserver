package com.maple.restserver.message.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class CompanyMstMessage implements Serializable {
private String id;
	
	private String companyName;
	private String state;
	private String companyGst;
	private String ipAdress;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCompanyGst() {
		return companyGst;
	}
	public void setCompanyGst(String companyGst) {
		this.companyGst = companyGst;
	}
	public String getIpAdress() {
		return ipAdress;
	}
	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}
	@Override
	public String toString() {
		return "CompanyMstMessage [id=" + id + ", companyName=" + companyName + ", state=" + state + ", companyGst="
				+ companyGst + ", ipAdress=" + ipAdress + "]";
	}
	
	
}
