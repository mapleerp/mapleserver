package com.maple.restserver.message.entity;

import java.util.ArrayList;

import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;

public class PaymentHdrAndDtlMessage {
	
	PaymentHdr paymentHdr;
	ArrayList<PaymentDtl> paymentDtlList =  new ArrayList();
	public PaymentHdr getPaymentHdr() {
		return paymentHdr;
	}
	public void setPaymentHdr(PaymentHdr paymentHdr) {
		this.paymentHdr = paymentHdr;
	}
	public ArrayList<PaymentDtl> getPaymentDtlList() {
		return paymentDtlList;
	}
	public void setPaymentDtlList(ArrayList<PaymentDtl> paymentDtlList) {
		this.paymentDtlList = paymentDtlList;
	}
	
	

}
