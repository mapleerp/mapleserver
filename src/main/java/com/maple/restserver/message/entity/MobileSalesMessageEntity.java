package com.maple.restserver.message.entity;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.message.entity.SalesDtlMessage;
import com.maple.restserver.message.entity.SalesTransHdrMessage;
@Component
public class MobileSalesMessageEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	SalesTransHdrMessage salesTransHdr;
	ArrayList<SalesDtlMessage> salesDtlList =new ArrayList();
	ArrayList<SalesReceipts> salesReceiptsList =new ArrayList();
	public SalesTransHdrMessage getSalesTransHdr() {
		return salesTransHdr;
	}
	public void setSalesTransHdr(SalesTransHdrMessage salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}
	public ArrayList<SalesDtlMessage> getSalesDtlList() {
		return salesDtlList;
	}
	public void setSalesDtlList(ArrayList<SalesDtlMessage> salesDtlList) {
		this.salesDtlList = salesDtlList;
	}
	public ArrayList<SalesReceipts> getSalesReceiptsList() {
		return salesReceiptsList;
	}
	public void setSalesReceiptsList(ArrayList<SalesReceipts> salesReceiptsList) {
		this.salesReceiptsList = salesReceiptsList;
	}
	@Override
	public String toString() {
		return "MobileSalesMessageEntity [salesTransHdr=" + salesTransHdr + ", salesDtlList=" + salesDtlList
				+ ", salesReceiptsList=" + salesReceiptsList + "]";
	}
	 
 
	 
	
 
	
}
