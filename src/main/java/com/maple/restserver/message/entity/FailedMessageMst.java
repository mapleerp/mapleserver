package com.maple.restserver.message.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.restserver.jms.send.KafkaMapleEventType;

@Entity
public class FailedMessageMst implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	private String id;

 
	@Type(type="text")
	private String message;

	private String source;
	private KafkaMapleEventType eventtype;
	private Integer eventKey;
	private String destination;
	

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Integer getEventKey() {
		return eventKey;
	}

	public void setEventKey(Integer eventKey) {
		this.eventKey = eventKey;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

 

	public KafkaMapleEventType getEventtype() {
		return eventtype;
	}

	public void setEventtype(KafkaMapleEventType eventtype) {
		this.eventtype = eventtype;
	}

	@Override
	public String toString() {
		return "FailedMessageMst [id=" + id + ", message=" + message + ", source=" + source + ", eventtype=" + eventtype
				+ ", eventKey=" + eventKey + ", destination=" + destination + "]";
	}
 

}
