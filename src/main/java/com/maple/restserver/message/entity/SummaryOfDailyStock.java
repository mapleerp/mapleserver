package com.maple.restserver.message.entity;

import java.util.Date;

public class SummaryOfDailyStock {
	
	Double stockIn;
	Double stockOut;
	String batch;
	String branchCode;
	Date expiryDate;
	String itemId;
	Date processDate;
	
	public Date getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	public Double getStockIn() {
		return stockIn;
	}
	public void setStockIn(Double stockIn) {
		this.stockIn = stockIn;
	}
	public Double getStockOut() {
		return stockOut;
	}
	public void setStockOut(Double stockOut) {
		this.stockOut = stockOut;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	@Override
	public String toString() {
		return "SummaryOfDailyStock [stockIn=" + stockIn + ", stockOut=" + stockOut + ", batch=" + batch
				+ ", branchCode=" + branchCode + ", expiryDate=" + expiryDate + ", itemId=" + itemId + "]";
	}
	
	
	

}
