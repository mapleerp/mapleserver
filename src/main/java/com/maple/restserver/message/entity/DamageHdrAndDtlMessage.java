package com.maple.restserver.message.entity;

import java.util.ArrayList;

import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.DamageHdr;

public class DamageHdrAndDtlMessage {
	
	DamageHdr damageHdr;
	ArrayList<DamageDtl> damageDtlList = new ArrayList();
	
	
	public DamageHdr getDamageHdr() {
		return damageHdr;
	}
	public void setDamageHdr(DamageHdr damageHdr) {
		this.damageHdr = damageHdr;
	}
	public ArrayList<DamageDtl> getDamageDtlList() {
		return damageDtlList;
	}
	public void setDamageDtlList(ArrayList<DamageDtl> damageDtlList) {
		this.damageDtlList = damageDtlList;
	}
	
	

}
