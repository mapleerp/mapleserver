package com.maple.restserver.message.entity;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;

@Controller
public class AccountMessage implements Serializable{
	private static final long serialVersionUID = 1L;
	
	AccountClass accountClass;
	ArrayList<LedgerClass> ledgerArray  = new ArrayList() ;
	ArrayList<DebitClass> debitArray = new ArrayList() ;
	ArrayList<CreditClass> creditArray = new ArrayList() ;
	String accountType;
	String processTally;
	
	
	public AccountClass getAccountClass() {
		return accountClass;
	}
	public void setAccountClass(AccountClass accountClass) {
		this.accountClass = accountClass;
	}
	public ArrayList<LedgerClass> getLedgerArray() {
		return ledgerArray;
	}
	public void setLedgerArray(ArrayList<LedgerClass> ledgerArray) {
		this.ledgerArray = ledgerArray;
	}
	public ArrayList<DebitClass> getDebitArray() {
		return debitArray;
	}
	public void setDebitArray(ArrayList<DebitClass> debitArray) {
		this.debitArray = debitArray;
	}
	public ArrayList<CreditClass> getCreditArray() {
		return creditArray;
	}
	public void setCreditArray(ArrayList<CreditClass> creditArray) {
		this.creditArray = creditArray;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getProcessTally() {
		return processTally;
	}
	public void setProcessTally(String processTally) {
		this.processTally = processTally;
	}
	

}
