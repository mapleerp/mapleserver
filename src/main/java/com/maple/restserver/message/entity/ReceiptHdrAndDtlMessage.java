package com.maple.restserver.message.entity;

import java.util.ArrayList;

import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;

public class ReceiptHdrAndDtlMessage {

	ReceiptHdr receiptHdr;
	ArrayList<ReceiptDtl> receiptDtlList = new ArrayList();
	public ReceiptHdr getReceiptHdr() {
		return receiptHdr;
	}
	public void setReceiptHdr(ReceiptHdr receiptHdr) {
		this.receiptHdr = receiptHdr;
	}
	public ArrayList<ReceiptDtl> getReceiptDtlList() {
		return receiptDtlList;
	}
	public void setReceiptDtlList(ArrayList<ReceiptDtl> receiptDtlList) {
		this.receiptDtlList = receiptDtlList;
	}
	
	
}
