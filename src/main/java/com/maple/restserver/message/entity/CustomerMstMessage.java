package com.maple.restserver.message.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class CustomerMstMessage implements Serializable
{
	private static final long serialVersionUID = 1L;
	 private String id;
		
	 	private String customerName;
		private String customerAddress;
		private String customerContact;
		private String customerMail;
		private String customerGst;
		private String customerState;
		private String customerPlace;
		private String priceTypeId;
		private String customerCountry;
		private String customerPin;
		private String customerGstType;
		private Integer rank;
		 
		private String customerGroup;
		private Integer creditPeriod;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getCustomerName() {
			return customerName;
		}
		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}
		public String getCustomerAddress() {
			return customerAddress;
		}
		public void setCustomerAddress(String customerAddress) {
			this.customerAddress = customerAddress;
		}
		public String getCustomerContact() {
			return customerContact;
		}
		public void setCustomerContact(String customerContact) {
			this.customerContact = customerContact;
		}
		public String getCustomerMail() {
			return customerMail;
		}
		public void setCustomerMail(String customerMail) {
			this.customerMail = customerMail;
		}
		public String getCustomerGst() {
			return customerGst;
		}
		public void setCustomerGst(String customerGst) {
			this.customerGst = customerGst;
		}
		public String getCustomerState() {
			return customerState;
		}
		public void setCustomerState(String customerState) {
			this.customerState = customerState;
		}
		public String getCustomerPlace() {
			return customerPlace;
		}
		public void setCustomerPlace(String customerPlace) {
			this.customerPlace = customerPlace;
		}
		public String getPriceTypeId() {
			return priceTypeId;
		}
		public void setPriceTypeId(String priceTypeId) {
			this.priceTypeId = priceTypeId;
		}
		public String getCustomerCountry() {
			return customerCountry;
		}
		public void setCustomerCountry(String customerCountry) {
			this.customerCountry = customerCountry;
		}
		public String getCustomerPin() {
			return customerPin;
		}
		public void setCustomerPin(String customerPin) {
			this.customerPin = customerPin;
		}
		public String getCustomerGstType() {
			return customerGstType;
		}
		public void setCustomerGstType(String customerGstType) {
			this.customerGstType = customerGstType;
		}
		public Integer getRank() {
			return rank;
		}
		public void setRank(Integer rank) {
			this.rank = rank;
		}
		public String getCustomerGroup() {
			return customerGroup;
		}
		public void setCustomerGroup(String customerGroup) {
			this.customerGroup = customerGroup;
		}
		public Integer getCreditPeriod() {
			return creditPeriod;
		}
		public void setCreditPeriod(Integer creditPeriod) {
			this.creditPeriod = creditPeriod;
		}
		
}
