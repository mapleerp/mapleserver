package com.maple.restserver.message.entity;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;

@Component
public class SalesHdrAndDtlMessageEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	SalesTransHdr salesTransHdr ;
	
	ArrayList<SalesDtl> SalesDtlList =  new ArrayList();

	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}

	public ArrayList<SalesDtl> getSalesDtlList() {
		return SalesDtlList;
	}

	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}

	public void setSalesDtlList(ArrayList<SalesDtl> salesDtlList) {
		SalesDtlList = salesDtlList;
	} 

}
