package com.maple.restserver.message.entity;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;

@Component
public class KitDefinitionHdrAndDtlMessageEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	KitDefinitionMst kitDefinitionMst;
	ArrayList<KitDefenitionDtl> kitDefinitionDtlList =  new ArrayList();
	
	
	public KitDefinitionMst getKitDefinitionMst() {
		return kitDefinitionMst;
	}
	public void setKitDefinitionMst(KitDefinitionMst kitDefinitionMst) {
		this.kitDefinitionMst = kitDefinitionMst;
	}
	public ArrayList<KitDefenitionDtl> getKitDefinitionDtlList() {
		return kitDefinitionDtlList;
	}
	public void setKitDefinitionDtlList(ArrayList<KitDefenitionDtl> kitDefinitionDtlList) {
		this.kitDefinitionDtlList = kitDefinitionDtlList;
	}
	@Override
	public String toString() {
		return "KitDefinitionHdrAndDtlMessageEntity [kitDefinitionMst=" + kitDefinitionMst + ", kitDefinitionDtlList="
				+ kitDefinitionDtlList + "]";
	}
	

}
