package com.maple.restserver.message.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;
@Component
public class ReceiptMessageEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	


	ReceiptHdr receiptHdr;
	
	ArrayList<ReceiptDtl> receiptDtlList =new ArrayList();

	public ReceiptHdr getReceiptHdr() {
		return receiptHdr;
	}

	public void setReceiptHdr(ReceiptHdr receiptHdr) {
		this.receiptHdr = receiptHdr;
	}

	public ArrayList<ReceiptDtl> getReceiptDtlList() {
		return receiptDtlList;
	}

	public void setReceiptDtlList(ArrayList<ReceiptDtl> receiptDtlList) {
		this.receiptDtlList = receiptDtlList;
	}

	@Override
	public String toString() {
		return "ReceiptMessageEntity [receiptHdr=" + receiptHdr + ", receiptDtlList=" + receiptDtlList + "]";
	}
	
}
