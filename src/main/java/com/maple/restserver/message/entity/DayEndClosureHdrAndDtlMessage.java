package com.maple.restserver.message.entity;

import java.util.ArrayList;

import com.maple.restserver.entity.DayEndClosureDtl;
import com.maple.restserver.entity.DayEndClosureHdr;

public class DayEndClosureHdrAndDtlMessage {
	
	DayEndClosureHdr dayEndClosureHdr;
	ArrayList<DayEndClosureDtl> dayEndClosureDtlList = new ArrayList();
	public DayEndClosureHdr getDayEndClosureHdr() {
		return dayEndClosureHdr;
	}
	public void setDayEndClosureHdr(DayEndClosureHdr dayEndClosureHdr) {
		this.dayEndClosureHdr = dayEndClosureHdr;
	}
	public ArrayList<DayEndClosureDtl> getDayEndClosureDtlList() {
		return dayEndClosureDtlList;
	}
	public void setDayEndClosureDtlList(ArrayList<DayEndClosureDtl> dayEndClosureDtlList) {
		this.dayEndClosureDtlList = dayEndClosureDtlList;
	}
	
	

}
