package com.maple.restserver.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesReturnDtl;
import com.maple.restserver.entity.SalesReturnHdr;
@RestController
public interface SalesRetunDtlRepository extends JpaRepository<SalesReturnDtl, String> {

	
	 
    @Query(nativeQuery = true, value = "SELECT SUM(qty) as totalQty, "
    		+ " SUM(mrp*qty)  as totalAmount, "
    		+ " SUM(((rate*qty) * ((tax_rate)/100 ))) as totalTax,  "
    		+ " SUM( (rate*qty) * cess_Rate/100) as totalCessAmt  "
    		+ " FROM sales_return_dtl d, sales_return_hdr h "
    	 
    		+ " where h.id = d.sales_return_hdr_id AND h.id = :salesHdrId")
    List<Object> findSumSalesDtl(String salesHdrId);
    
    List<SalesReturnDtl> findBySalesReturnHdrId(String returnHdrId);

    @Query(nativeQuery = true, value = "SELECT   "
    		+ " SUM(rate*qty) + sum(CGST_AMOUNT +SGST_AMOUNT +  CESS_AMOUNT) as totalAmount "
    		+ " FROM sales_return_dtl d, sales_return_hdr h "
    		+ " where h.id = d.sales_return_hdr_id AND h.id = :salesHdrId")
    BigDecimal  findSumCrAmountSalesDtl(@Param("salesHdrId")String salesHdrId);

    
    
    @Query(nativeQuery = true, value = "SELECT   "
    		+ " SUM((rate * qty))  as totalAmount "
    		+ " FROM sales_return_dtl d, sales_return_hdr h "
    		+ " where h.id = d.sales_return_hdr_id AND h.id = :salesHdrId AND"
    		+ " d.item_id = :itemId ")
    Double  findSumAcessibleAmountItemWise(String salesHdrId, String itemId);
    
    @Query(nativeQuery = true, value = "SELECT   "
    		+ " SUM((mrp * qty))  as totalAmount "
    		+ " FROM sales_return_dtl d, sales_return_hdr h "
    		+ " where h.id = d.sales_return_hdr_id AND h.id = :salesHdrId  "
    		+ "   ")
    Double  findSumTotalAmount(String salesHdrId);

    
    
    
    @Query(nativeQuery = true, value = "SELECT SUM( (rate* qty) * (tax_rate)/100 )    as totalQty "
    		+ " FROM sales_return_dtl d, sales_return_hdr h "
    		+ " where h.id = d.sales_return_hdr_id AND h.id = :salesHdrId AND d.item_id =:itemId ")
    Double findTotalTaxSumSalesDtlItemWise( String salesHdrId,  String itemId);
    
    
    
    @Query(nativeQuery = true, value = "SELECT   "
    		+ " SUM(d.cess_amount)  as totalAmount "
    		+ " FROM sales_return_dtl d, sales_return_hdr h "
    		+ " where h.id = d.sales_return_hdr_id AND h.id = :salesHdrId and d.item_id = :itemID  ")
    Double  findSumCessAmountItemWise(String salesHdrId, String itemID);

	SalesReturnDtl findBySalesReturnHdrAndItemIdAndUnitIdAndBatchAndBarcodeAndRate(SalesReturnHdr salesReturnHdr,
			String itemId, String unitId, String batch, String barcode, Double rate);
    
    
}
