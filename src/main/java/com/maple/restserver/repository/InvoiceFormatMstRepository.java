package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.InvoiceFormatMst;
@Component
@Repository
public interface InvoiceFormatMstRepository extends JpaRepository<InvoiceFormatMst, String>{
	
	
	List<InvoiceFormatMst> findByCompanyMst(CompanyMst companyMst);

	List<InvoiceFormatMst> findByCompanyMstAndPriceTypeId(CompanyMst companyMst, String pricetypeid);

	List<InvoiceFormatMst> findByCompanyMstAndPriceTypeIdAndGstAndDiscountAndBatch(CompanyMst companyMst,
			String pricetypeid, String gst, String discount, String batch);

	List<InvoiceFormatMst> findByCompanyMstAndGstAndDiscountAndBatch(CompanyMst companyMst, String gst,
			String discount, String batch);

	List<InvoiceFormatMst> findByCompanyMstAndGstAndBatch(CompanyMst companyMst, String gst, String batch);

	List<InvoiceFormatMst> findByCompanyMstAndGst(CompanyMst companyMst, String gst);

	List<InvoiceFormatMst> findByCompanyMstAndDiscount(CompanyMst companyMst, String discount);

	List<InvoiceFormatMst> findByReportName(String taxInvoiceFormat);

	List<InvoiceFormatMst> findByCompanyMstAndStatus(CompanyMst companyMst, String string);

	
	
	

}
