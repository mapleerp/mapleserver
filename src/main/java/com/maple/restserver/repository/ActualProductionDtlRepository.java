package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.hibernate.type.TrueFalseType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.CompanyMst;

public interface ActualProductionDtlRepository extends JpaRepository<ActualProductionDtl, String>{
	
	 List<ActualProductionDtl> findByActualProductionHdrId (String prodcutionhdrid);

	 @Query(nativeQuery = true,value = "select ad.* from actual_production_dtl ad,"
	 		+ "actual_production_hdr ah where ad.actual_production_hdr=ah.id and "
	 		+ "date(ah.voucher_date) >= :fdate and date(ah.voucher_date)<=:tdate")
	List<ActualProductionDtl> getActualProductionDtlBtwnDate(Date fdate,Date tdate);
	 
	 @Query(nativeQuery = true,value="select d.* from actual_production_hdr h, actual_production_dtl d "
	 		+ " where h.id=d.actual_production_hdr and date(h.voucher_date)=:date")
	 List<ActualProductionDtl>  findByActualProductionHdrVoucherDate(Date date);
	 
	  Optional<ActualProductionDtl> findById(String dtlid);
	 
	  ActualProductionDtl findByProductionDtlId(String id);

	  
	  @Query(nativeQuery = true, value = "select sum(d.production_cost), "
	  		+ "h.voucher_date "
	  	
	  		+ "from actual_production_dtl d, actual_production_hdr h "
	  		+ "where d.actual_production_hdr=h.id and "
	  		+ "h.voucher_number is not null and date(h.voucher_date)>=:fromdate and "
	  		+ "date(h.voucher_date)<=:todate and h.company_mst=:companyMst and h.branch_code=:branch "
	 
	  		+ " group by h.voucher_date") 
	List<Object> findProductionValue(Date fromdate, Date todate, CompanyMst companyMst, String branch);
	  
	  
	  @Query(nativeQuery = true, value="select sum(ad.actual_qty),"
	  		+ "ah.voucher_date from actual_production_hdr ah,"
	  		+ "actual_production_dtl ad where "
	  		+ "ad.actual_production_hdr = ah.id and ah.branch_code =:branchcode"
	  		+ " and ad.item_id = :itemid "
	  		+ "and date(ah.voucher_date) between :fudate and :tudate group by ah.voucher_date")
	List<Object> getSumOfActualQty(Date fudate, Date tudate, String itemid, String branchcode);

@Query(nativeQuery = true,value="select dl.raw_material_item_id,sum(dl.qty),ad.store from actual_production_hdr ah, "
		+ "actual_production_dtl ad,production_dtl pd , production_dtl_dtl dl "
		+ "where ah.id=ad.actual_production_hdr  and ad.production_dtl=pd.id and dl.production_dtl_id=pd.id and "
		+ " ah.id=:acthdrid group by dl.raw_material_item_id,ad.store")
	 List<Object> getRawMaterialQty(String acthdrid);


  @Query(nativeQuery = true,value="select i.item_name ,sum(id.qty)  as currentstock,u.unit_name"
  		+ " from item_batch_mst id,item_mst i,unit_mst u where i.id=id.item_id and "
  		+ " id.item_id=:itemId and u.id=i.unit_id  group by i.item_name,u.unit_name HAVING sum(id.qty)<:qty  ")
 List<Object>cheackRowMaterialStock(String itemId,Double qty);
  
  @Query(nativeQuery = true,value = "select sum(dl.qty) from actual_production_hdr ah,actual_production_dtl ad , production_dtl_dtl dl,production_dtl pd where ah.id=actual_production_hdr and "
  		+ "ah.id=:acthdrid and ad.production_dtl=pd.id and dl.production_dtl_id=pd.id and "
  		+ "dl.raw_material_item_id=:itemIds")
  Double requiredItemStock(String acthdrid,String itemIds);
  
  @Query(nativeQuery = true,value="select i.item_name ,sum(id.qty)  as currentstock,u.unit_name"
	  		+ " from item_batch_mst id,item_mst i,unit_mst u where i.id=id.item_id and "
	  		+ " id.item_id=:itemId and u.id=i.unit_id and id.store=:store  group by i.item_name,u.unit_name HAVING sum(id.qty)<:qty  ")
	 List<Object>cheackRowMaterialStockByStore(String itemId,Double qty,String store);
}
