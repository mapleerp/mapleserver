package com.maple.restserver.repository;

import java.util.Date;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.DailyCashSummary;
@Component
@Repository
public interface DailyCashSummaryRepository extends JpaRepository< DailyCashSummary, String>{

	
	@Query(nativeQuery =true,value="select h.customer_name,(h.paid_amount+h.cardamount) AS totalamount  from Sales_Order_Trans_Hdr  h where date(h.voucher_date)<=:date")
  List<Object>DailyCashFromOrder(@Param("date")Date date);

	
	
	List<DailyCashSummary>getDailyCashSummaryByReportDate(Date date);
}
