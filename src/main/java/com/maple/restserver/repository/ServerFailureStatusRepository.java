package com.maple.restserver.repository;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ServerFailureStatusMst;

@Component
@Repository
public interface ServerFailureStatusRepository extends JpaRepository<ServerFailureStatusMst,String>{

	ServerFailureStatusMst saveAndFlush(@Valid ServerFailureStatusMst serverFailureStatusMst);

}
