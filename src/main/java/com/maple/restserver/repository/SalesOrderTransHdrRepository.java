package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.report.entity.SaleOrderReport;

@Repository
public interface SalesOrderTransHdrRepository extends JpaRepository<SalesOrderTransHdr, String> {

	@Query(nativeQuery = true, value = "select * from sales_order_trans_hdr "
			+ "where date(voucher_date) >= :fdate and date(voucher_date) <= :tdate ")
	List<SalesOrderTransHdr> getSaleOrderTransHdrBtwnDate(Date fdate, Date tdate);
	// version5.1

	// end 5.1
	// --------------version 4.10
	@Query(nativeQuery = true, value = "select i.item_name, sum(d.qty),u.unit_name "
			+ "from sales_order_dtl d, sales_order_trans_hdr h, item_mst i,unit_mst u "
			+ " where h.branch_code=:branchCode and h.company_mst=:companymstid "
			+ "and d.item_id=i.id and d.sales_order_trans_hdr=h.id and h.voucher_number is not null "
			+ "and  date(h.voucher_date)=:date and i.unit_id=u.id group by i.item_name,u.unit_name")
	List<Object> getDailyOrderSummuryReport(Date date, String branchCode, String companymstid);

	// --------------version 4.10 end
//	List<AcceptStock> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
	// ----------------version 4.9
	@Query(nativeQuery = true, value = "select lc.localcustomer_name, lc.phone_no,lc.address, "
			+ "h.voucher_number, h.delivery_mode, h.order_due, h.order_due_time, "
			+ "h.invoice_amount,h.paid_amount,(h.invoice_amount-h.paid_amount) as balance, i.item_name " + " from "
			+ "sales_order_dtl d, sales_order_trans_hdr h, item_mst i,  local_customer_mst lc "
			+ "where d.sales_order_trans_hdr=h.id  " + "and date(h.voucher_date)=:date "
			+ "and h.branch_code=:branchCode and h.company_mst=:companymstid "
			+ "and h.local_customer_id=lc.id and h.delivery_mode='HOME_DELIVERY' and d.item_id=i.id "
			+ "group by lc.localcustomer_name, lc.phone_no,lc.address,h.voucher_number,"
			+ " h.delivery_mode, h.order_due, h.order_due_time,h.invoice_amount,h.paid_amount,i.item_name ")
	List<Object> SaleOrderHomeDeliveryReport(Date date, String branchCode, String companymstid);
	// ----------------version 4.9 end

	@Query(nativeQuery = true, value = " select i.item_name, MRP,ORDER_ADVICE, ORDER_MESSAGE_TO_PRINT,QTY, c.VOUCHER_DATE,VOUCHER_NUMBER  "
			+ "      from Sales_Order_Trans_Hdr  c ," + " sales_order_dtl d , item_mst i "
			+ " where c.id = d.SALES_TRANS_HDR_ID " + " AND d.item_id = i.id AND d.company_mst=:companymstid"
			+ "AND c.voucher_Number is not null ")

	List<Object> getdailySalesOrder(String companymstid);

	@Query(nativeQuery = true, value = "select h.customer_name,(h.paid_amount+h.cardamount) AS totalamount  from Sales_Order_Trans_Hdr  h where date(h.voucher_date)<=:date")
	List<Object> DailyCashFromOrder(@Param("date") Date date);

	@Query(nativeQuery = true, value = "select * from sales_order_trans_hdr h where order_status='Orderd'")
	List<SalesOrderTransHdr> getsSaleOrder();

	List<SalesOrderTransHdr> findByCompanyMst(String companymstid);

	@Query(nativeQuery = true, value = "select h.* from sales_order_trans_hdr h where h.order_due between :rdate and :tdate and h.order_status='Orderd'"
			+ " and h.branch_code = :branchCode")
	List<SalesOrderTransHdr> getPendingSaleOrder(Date rdate, Date tdate, String branchCode);

	@Query(nativeQuery = true, value = "select lc.localcustomer_name," + "h.voucher_number," + "h.order_due,"
			+ "h.order_due_time," + "h.invoice_amount," + "d.item_name," + "d.qty," + "d.rate," + "d.mrp,d.amount,"
			+ "u.unit_name,"

			+ "h.delivery_mode " + "from sales_order_trans_hdr h,"
			+ "sales_order_dtl d,local_customer_mst lc,unit_mst u where h.id=d.sales_order_trans_hdr and "
			+ "h.local_customer_id = lc.id and h.order_due between :rdate and :tdate and h.branch_code =:branchCode and u.id=d.unit_id"
			+ " and h.order_status='Orderd'")
	List<Object> getPendinSaleOrderReport(Date rdate, Date tdate, String branchCode);

//	@Query(nativeQuery = true, value = "select c.customer_name," + " c.customer_contact,h.voucher_number,"
//			+ " h.converted_to_invoice," + " b.branch_name , " + " h.invoice_amount,"
//			+ " h.paid_amount,lc.localcustomer_name,lc.Phone_no from  local_customer_mst lc,sales_order_trans_hdr h,customer_mst c,branch_mst b where c.id=h.customer_id and lc.id=h.local_customer_id "
//			+ " and b.branch_code=h.branch_code and h.order_status='Orderd' and h.voucher_date=:date and h.branch_code=:branchCode")
	
	
	@Query(nativeQuery = true, value = "select c.account_name," + " c.customer_contact,h.voucher_number,"
			+ " h.converted_to_invoice," + " b.branch_name , " + " h.invoice_amount,"
			+ " h.paid_amount,lc.localcustomer_name,lc.Phone_no from  local_customer_mst lc,sales_order_trans_hdr h,account_heads c,branch_mst b where c.id=h.customer_id and lc.id=h.local_customer_id "
			+ " and b.branch_code=h.branch_code and h.order_status='Orderd' and h.voucher_date=:date and h.branch_code=:branchCode")
	List<Object> getSaleOrderPendingReport(Date date, String branchCode);

//	@Query(nativeQuery = true, value = "select c.customer_name,\"\n"
//			+ "	 		+ \" c.customer_phone_no,h.voucher_number,\"\n" + "	 		+ \" h.converted_to_invoice,\"\n"
//			+ "	 		+ \" b.branch_name , \"\n" + "	 		+ \" h.invoice_amount ,\"\n"
//			+ "	 		+ \" h.paid_amount  from sales_order_trans_hdr h, customer_mst c,branch_mst b where c.id=h.customer_id and \"\n"
//			+ "	 		+ \"   and b.branch_code=h.branch_code and h.order_status='Realized' and h.voucher_date=:date and h.branch_code=:branchCode")
	
	@Query(nativeQuery = true, value = "select c.account_name,\"\n"
			+ "	 		+ \" c.customer_phone_no,h.voucher_number,\"\n" + "	 		+ \" h.converted_to_invoice,\"\n"
			+ "	 		+ \" b.branch_name , \"\n" + "	 		+ \" h.invoice_amount ,\"\n"
			+ "	 		+ \" h.paid_amount  from sales_order_trans_hdr h, account_heads c,branch_mst b where c.id=h.customer_id and \"\n"
			+ "	 		+ \"   and b.branch_code=h.branch_code and h.order_status='Realized' and h.voucher_date=:date and h.branch_code=:branchCode")
	List<Object> getSaleOrderRealizedReport(Date date, String branchCode);

	/*
	 * @Query(nativeQuery = true, value = "select c.company_name," +
	 * "b.branch_address1," + "b.branch_tel_no," + "c.state," + "b.branch_gst," +
	 * "h.voucher_number," + " h.voucher_date," + "h.delivery_mode," +
	 * "lc.localcustomer_name,lc.address, lc.phone_no," + "d.item_name," + "d.rate,"
	 * + "(d.sgst_tax_rate+d.cgst_tax_rate) as gst," + "d.unit_name," +
	 * "(d.rate*d.qty) as amount ," + "cu.customer_name ," + "h.order_due_time," +
	 * "h.order_time_mode," + "o.order_taker_name," + "d.qty," + "h.orded_by," +
	 * "h.invoice_amount," + "b.branch_website," + "b.branch_email, d.order_msg, " +
	 * " lc.PHONENO2 " + " from company_mst c,order_taker_mst o," +
	 * "sales_order_trans_hdr h,local_customer_mst lc," +
	 * " customer_mst cu,sales_order_dtl d,branch_mst b where c.id=h.company_mst and "
	 * +
	 * "cu.id=h.customer_id and h.id=d.sales_order_trans_hdr and h.branch_code=b.branch_code "
	 * + " and h.voucher_number=:voucernumber and o.id=h.sales_man_id " +
	 * " and lc.id=h.local_customer_id and date(h.voucher_date)=:date and h.branch_code=:branchCode"
	 * )
	 * 
	 * List<Object> getSaleOrderInvoiceReport(Date date, String voucernumber, String
	 * branchCode);
	 */
	@Query(nativeQuery = true, value = "select c.company_name,b.branch_address1,b.branch_tel_no,"
			+ "c.state,b.branch_gst,h.voucher_number, h.voucher_date,h.delivery_mode,"
			+ "lc.localcustomer_name,lc.address, lc.phone_no,d.item_name,"
			+ "d.rate,(d.sgst_tax_rate+d.cgst_tax_rate) as gst,d.unit_name,"
			+ "(d.rate*d.qty) as amount ,cu.account_name ,h.order_due_time,"
			+ "h.order_time_mode,o.order_taker_name,d.qty,h.orded_by,h.invoice_amount,"
			+ "b.branch_website,b.branch_email, d.order_msg, lc.PHONENO2 from company_mst c,"
			+ "order_taker_mst o,sales_order_trans_hdr h,local_customer_mst lc,"
			+ "account_heads cu,sales_order_dtl d,branch_mst b where c.id=h.company_mst"
			+ " and cu.id=h.account_heads and h.id=d.sales_order_trans_hdr and "
			+ "h.branch_code=b.branch_code and h.voucher_number=:voucernumber "
			+ "and o.id=h.sales_man_id and lc.id=h.local_customer_id "
			+ "and "
			+ "date(h.voucher_date)=:date and h.branch_code=:branchCode")
	
	List<Object> getSaleOrderInvoiceReport(Date date, String voucernumber, String branchCode);
	
	
	

	

	/*
	 * Date dueDate; String day; String time; String otName; String paymentMode;
	 * Double advance; Double balance;
	 */
	@Query(nativeQuery = true, value = "select h.order_due ," + " h.paid_amount ," + "h.payment_mode,"
			+ "(h.invoice_amount-h.paid_amount) " + " AS balanceamount," + "h.order_due_day," + "h.order_time_mode,"
			+ " h.order_due_time," + "o.order_taker_name ,h.order_message_to_print "
			+ " from order_taker_mst o,sales_order_trans_hdr h" + " where h.voucher_number=:vouchernumber "
			+ " and o.id = h.sales_man_id and  date(h.voucher_date)=:date " + "and h.branch_code=:branchcode ")
	List<Object> getSaleOrderDueReport(Date date, String vouchernumber, String branchcode);

	/*
	 * String itemName; Double sgst; Double cgst; Double roundOff;
	 */

	@Query(nativeQuery = true, value = "select (sum(d.sgst_amount) ), (sum(d.cgst_amount)  ) "

			+ "from sales_order_dtl d,sales_order_trans_hdr h"
			+ " where d.sales_order_trans_hdr =h.id and h.voucher_number =:vouchernumber "
			+ "and date(h.voucher_date)=:date and h.branch_code =:branchCode ")
	List<Object> getTaxSaleOrderTaxSummary(String vouchernumber, Date date, String branchCode);

	Optional<SalesOrderTransHdr> findByVoucherNumber(String voucherNumber);

	@Query(nativeQuery = true, value = "select sum(d.rate*d.qty) as totalamount from " + " sales_order_trans_hdr h,"
			+ "sales_order_dtl d where h.id=d.sales_order_trans_hdr "
			+ " and h.voucher_date=:date and h.branch_code =:branchCode and " + " h.voucher_number =:vouchernumber ")
	Double getTotalAmountOfSaleOrder(Date date, String vouchernumber, String branchCode);

	@Query(nativeQuery = true, value = "select " + "sum(d.sgst_amount+d.cgst_amount) as total from"
			+ " sales_order_dtl d," + "sales_order_trans_hdr h  " + " where d.sales_order_trans_hdr =h.id "
			+ "and h.voucher_number =:vouchernumber " + "and h.voucher_date=:date " + "and h.branch_code =:branchCode")
	Double toalTax(Date date, String vouchernumber, String branchCode);

//List<SalesOrderDtl> findByVoucherDate( Date date);

	@Query(nativeQuery = true, value = "select " + "h.voucher_date," + "h.voucher_number," + "lc.localcustomer_name,"
			+ "h.invoice_amount," + "h.paid_amount," + "h.invoice_amount-h.paid_amount as balanceAmount, "
			+ "b.branch_name," + "b.branch_address1," + "b.branch_address2," + "b.branch_gst," + "b.branch_tel_no,"
			+ "h.sodexo_amount," + "c.company_name "
			+ "from sales_order_trans_hdr h ,local_customer_mst lc,company_mst c," + "branch_mst b where "
			+ "date(h.voucher_date)=:date and h.branch_code=:branchCode and c.id=h.company_mst "
			+ "and lc.id=h.local_customer_id and " + "b.branch_code=h.branch_code ")
	List<Object> getSaleOrderBalanceAdvanceReportByDate(Date date, String branchCode);

//
	@Query(nativeQuery = true, value = "select h.voucher_date," + "lc.localcustomer_name," + "lc.address,lc.phone_no,"
			+ "h.order_due_time," + "h.paid_amount," + " h.invoice_amount,"
			+ " h.invoice_amount-h.paid_amount as balanceAmount," + " i.item_name," + "c.company_name,"
			+ "b.branch_name," + "b.branch_email," + "b.branch_website," + "b.branch_address1," + "b.branch_tel_no,"
			+ "b.branch_state ," + "h.voucher_number " + " from sales_order_trans_hdr h," + " sales_order_dtl d,"
			+ "item_mst i," + "company_mst c," + "branch_mst b," + "local_customer_mst lc "
			+ " where h.id=d.sales_order_trans_hdr and " + "date(h.order_due)=:date and lc.id=h.local_customer_id "
			+ "and i.id=d.item_id and h.delivery_mode='HOME_DELIVERY' and h.ORDER_STATUS='Orderd' and c.id=h.company_mst and b.branch_code=h.branch_code and h.branch_code=:branchCode")
	List<Object> getHomeDelivaryReportByDate(Date date, String branchCode);
	
	@Query(nativeQuery = true, value = "select dm.delivery_boy_name," + "h.orded_by," + "h.order_due,"
			+ "h.voucher_number," + "lc.localcustomer_name," + "h.invoice_amount,"
			+ "h.invoice_amount-h.paid_amount as balanceAmount," + "m.company_name," + "b.branch_name,"
			+ "b.branch_email," + "b.branch_website," + "b.branch_address1," + "b.branch_tel_no," + "b.branch_state "
			+ "from sales_order_trans_hdr h,sales_order_dtl d,local_customer_mst lc,"
			+ "branch_mst b,company_mst m,delivery_boy_mst dm " + "where h.id=d.sales_order_trans_hdr and "
			+ "lc.id=h.local_customer_id and b.branch_code=h.branch_code "
			+ "and m.id=h.company_mst and dm.id=h.delivery_boy_id " + "and h.voucher_date=:date and "
			+ "h.delivery_boy_id=:deliveryboyid and h.branch_code=:branchCode ")
	List<Object> getSaleOrderReportByDeliverBoy(Date date, String deliveryboyid, String branchCode);

	@Query(nativeQuery = true, value = "select " + " h.voucher_number," + "lc.localcustomer_name," + "lc.phone_no,"
			+ "h.delivery_mode," + "h.order_due_time," + "h.order_message_to_print, " + "h.order_advice,"
			+ "h.invoice_amount-h.paid_amount as balanceAmount," + "m.company_name," + "b.branch_name,"
			+ "b.branch_email," + "b.branch_website," + "b.branch_address1," + "b.branch_tel_no," + "b.branch_state,"
			+ "h.order_due," + "h.invoice_amount," + "h.paid_amount " + "from sales_order_trans_hdr h,"
			+ "local_customer_mst lc," + "branch_mst b," + "company_mst m "
			+ " where lc.id=h.local_customer_id and m.id=h.company_mst and  "
			+ " b.branch_code=h.branch_code and date(h.voucher_date)=:date  and h.branch_code=:branchCode ")
	List<Object> getSaleOrderItemDetailReport(Date date, String branchCode);

	List<SalesOrderTransHdr> findByVoucherNumberAndVoucherDate(String voucherNumber, Date voucherDate);

	SalesOrderTransHdr findByIdAndCompanyMst(String salesordertrandhdrid, CompanyMst companyMst);
	
	
	@Query(nativeQuery = true, value = "select h.id, h.voucher_number, h.customer_id, h.local_customer_id "
			+ "from sales_order_trans_hdr h where h.order_status='Orderd' and h.company_mst=:companyMst "
			+ " and lower(h.voucher_number) LIKE  :searchItemName" + "")
	List<Object> SearchSaleOrder(String searchItemName, Pageable topFifty, CompanyMst companyMst);


//	@Query(nativeQuery = true, value = "select h.id, h.voucher_number, h.customer_id, l.localcustomer_name,c.customer_name "
//			+ "from sales_order_trans_hdr h , local_customer_mst l, customer_mst c where h.order_status='Orderd' and h.company_mst=:companyMst "
//			+ " and h.local_customer_id = l.id "
//			+ " and lower(h.voucher_number) LIKE  :searchItemName and c.id=h.customer_id")
	
	@Query(nativeQuery = true, value = "select h.id, h.voucher_number, h.customer_id, l.localcustomer_name,c.account_name "
			+ "from sales_order_trans_hdr h , local_customer_mst l, account_heads c where h.order_status='Orderd' and h.company_mst=:companyMst "
			+ " and h.local_customer_id = l.id "
			+ " and lower(h.voucher_number) LIKE  :searchItemName and c.id=h.customer_id")
	List<Object> SearchSaleOrderRetunLcustomerName(String searchItemName, Pageable topFifty, CompanyMst companyMst);

	
//	@Query(nativeQuery = true, value = "select h.id, h.voucher_number, h.customer_id "
//			+ " from sales_order_trans_hdr h, customer_mst c , local_customer l "
//			+ " where h.order_status='Orderd' and h.company_mst=:companyMst and "
//			+ " h.customer_id = c.id and h.local_customer_id = l.id "
//			+ " and lower(h.voucher_number) LIKE  :searchItemName" + "")
//	List<Object> SearchSaleOrder(String searchItemName, Pageable topFifty, CompanyMst companyMst);

	@Query(nativeQuery = true, value = "select lc.localcustomer_name, lc.address, lc.phone_no, "
			+ "h.PAID_AMOUNT, h.INVOICE_AMOUNT, h.VOUCHER_NUMBER, " + "dtl.ITEM_NAME, dtl.QTY from "
			+ "sales_order_trans_hdr h, sales_order_dtl dtl, local_customer_mst lc "
			+ " where  h.VOUCHER_NUMBER is not null and h.id=dtl.SALES_ORDER_TRANS_HDR and "
			+ "lc.id=h.LOCAL_CUSTOMER_ID and h.COMPANY_MST=:companymstid and "
			+ "h.DELIVERY_MODE='TAKE_AWAY' and date(h.order_due)=:date and h.ORDER_STATUS='Orderd'")
	List<Object> getSaleOrderTakeAwayReport(Date date, String companymstid);

//	@Query(nativeQuery = true, value = "select lc.localcustomer_name, lc.address, lc.phone_no, "
//			+ "sh.voucher_number, sh.invoice_amount, "
//// 		+ "h.voucher_number, "
//			+ "h.voucher_date, h.order_due , h.order_due_time, h.order_time_mode , c.customer_name "
//			+ "from sales_trans_hdr sh, receipt_invoice_dtl dtl, "
//			+ "local_customer_mst lc , sales_order_trans_hdr h, customer_mst c " + "where  "
//			+ "sh.sale_order_hrd_id=h.id and h.customer_id=c.id and h.local_customer_id=lc.id "
//			+ "and dtl.sales_trans_hdr!=sh.id and date(sh.voucher_date)=:date and "
//			+ "sh.company_mst=:companymstid and h.delivery_boy_id=:deliveryboyid and "
//			+ "sh.branch_code=:branchcode group by lc.localcustomer_name, lc.address, lc.phone_no,"
//			+ "sh.voucher_number, sh.invoice_amount,h.voucher_date, h.order_due , "
//			+ "h.order_due_time, h.order_time_mode , c.customer_name")

	
	@Query(nativeQuery = true, value = "select lc.localcustomer_name, lc.address, lc.phone_no, "
			+ "sh.voucher_number, sh.invoice_amount, "
// 		+ "h.voucher_number, "
			+ "h.voucher_date, h.order_due , h.order_due_time, h.order_time_mode , c.account_name "
			+ "from sales_trans_hdr sh, receipt_invoice_dtl dtl, "
			+ "local_customer_mst lc , sales_order_trans_hdr h, account_heads c " + "where  "
			+ "sh.sale_order_hrd_id=h.id and h.customer_id=c.id and h.local_customer_id=lc.id "
			+ "and dtl.sales_trans_hdr!=sh.id and date(sh.voucher_date)=:date and "
			+ "sh.company_mst=:companymstid and h.delivery_boy_id=:deliveryboyid and "
			+ "sh.branch_code=:branchcode group by lc.localcustomer_name, lc.address, lc.phone_no,"
			+ "sh.voucher_number, sh.invoice_amount,h.voucher_date, h.order_due , "
			+ "h.order_due_time, h.order_time_mode , c.account_name")
	List<Object> getDeliveryBoyPendingReport(Date date, String companymstid, String deliveryboyid, String branchcode);

//---------------------version 4.8
//	@Query(nativeQuery = true, value = "select c.customer_name, c.customer_contact, "
//			+ "h.voucher_number, h.order_advice, d.order_msg, h.order_due, h.order_due_time, "
//			+ "i.item_name, u.unit_name, " + "d.qty from "
//			+ "sales_order_dtl d, sales_order_trans_hdr h, item_mst i, unit_mst u, customer_mst c "
//			+ "where d.sales_order_trans_hdr=h.id and d.item_id=i.id and i.unit_id=u.id and i.category_id=:categoryid "
//			+ "and date(h.voucher_date)=:date and h.branch_code=:branchCode and h.company_mst=:companymstid and h.customer_id=c.id")
	
	@Query(nativeQuery = true, value = "select c.account_name, c.customer_contact, "
			+ "h.voucher_number, h.order_advice, d.order_msg, h.order_due, h.order_due_time, "
			+ "i.item_name, u.unit_name, " + "d.qty from "
			+ "sales_order_dtl d, sales_order_trans_hdr h, item_mst i, unit_mst u, account_heads c "
			+ "where d.sales_order_trans_hdr=h.id and d.item_id=i.id and i.unit_id=u.id and i.category_id=:categoryid "
			+ "and date(h.voucher_date)=:date and h.branch_code=:branchCode and h.company_mst=:companymstid and h.customer_id=c.id")


	List<Object> DailyCakeSettingReport(Date date, String categoryid, String branchCode, String companymstid);
//---------------------version 4.8 end

	@Query(nativeQuery = true, value = "select * from sale_order_trans_hdr h where h.company_mst:companymstid and h.branch_code=:branchCode and date(h.voucher_date) between :fromDate and :toDate")
	List<SalesOrderTransHdr> fetchSalesOrderTransHdr(String companymstid, String branchCode, Date fromDate,
			Date toDate);

	@Query(nativeQuery = true, value = "select h.id ,d.item_id,d.qty,i.bar_code from sales_order_trans_hdr h,sales_order_dtl d ,unit_mst u,item_mst i where h.id=d.sales_order_trans_hdr and u.id=d.unit_id and h.order_status='Orderd' and i.id=d.item_id and h.id not in (select pl.sale_order_hdr_id from sale_order_production_link_mst pl) and d.item_id in(select kd.item_id from kit_defenition_dtl kd) ")
	List<Object> pendingSaleOrder();

// ----------------------version 6.2 surya

	List<SalesOrderTransHdr> findByCompanyMstAndBranchCodeAndVoucherDate(CompanyMst companyMst, String branchcode,
			Date fdate);

	SalesOrderTransHdr findByVoucherNumberAndCompanyMst(String invoiceno, CompanyMst companyMst);
//----------------------version 6.2 surya end

//-------------------version 6.5 surya 

	SalesOrderTransHdr findByVoucherNumberAndCompanyMstAndBranchCode(String invoiceno, CompanyMst companyMst,
			String branchcode);
//-------------------version 6.5 surya end

	List<SalesOrderTransHdr> findByCompanyMstAndBranchCode(CompanyMst companyMst, String branchcode);

	List<SalesOrderTransHdr> findByCompanyMstAndBranchCodeAndOrderStatus(CompanyMst companyMst, String branchcode,
			String string);

	@Query(nativeQuery = true, value = "select max(h.voucher_number) from sales_order_trans_hdr h where "
			+ " h.branch_code=:branchcode and " + "h.company_mst=:companyMst ")
	String getMaxVoucherNo(String branchcode, CompanyMst companyMst);

	@Modifying
	@Query(nativeQuery = true, value = "update sales_order_trans_hdr  set customer_id =:toAccountId where customer_id =:fromAccountId ")
	void accountMerge(String fromAccountId, String toAccountId);

	@Query(nativeQuery = true, value = "select * from sales_order_trans_hdr " + "where voucher_number is null")
	List<SalesOrderTransHdr> getHoldedSalesOrderTransHdr(CompanyMst companyMst);

	@Query(nativeQuery = true, value = "select * from sales_order_trans_hdr " +
	" where date(voucher_date)>=:sdate and date(voucher_date)<=:edate and company_mst=:companyMst and branch_code=:branchcode "
	+ " and voucher_number is not null and order_status='CANCELED'")
	List<SalesOrderTransHdr> getCanceledSalesOrderTransHdr(CompanyMst companyMst, String branchcode, Date sdate,
			Date edate);

//	@Query(nativeQuery = true,value ="select h.voucher_date,h.invoice_amount,"
//			+ "c.customer_name,sr.receipt_mode,sr.receipt_amount from sales_trans_hdr h,"
//			+ "sales_receipts sr,sales_order_trans_hdr so,customer_mst c where "
//			+ "h.id=sr.sales_trans_hdr_id and so.id=h.sale_order_hrd_id and "
//			+ "c.id=h.customer_id and "
//			+ "date(so.voucher_date) < :date and date(h.voucher_date) = :date and "
//			+ "h.branch_code = :branchcode")
	
	@Query(nativeQuery = true,value ="select h.voucher_date,h.invoice_amount,"
			+ "c.account_name,sr.receipt_mode,sr.receipt_amount from sales_trans_hdr h,"
			+ "sales_receipts sr,sales_order_trans_hdr so,account_heads c where "
			+ "h.id=sr.sales_trans_hdr_id and so.id=h.sale_order_hrd_id and "
			+ "c.id=h.customer_id and "
			+ "date(so.voucher_date) < :date and date(h.voucher_date) = :date and "
			+ "h.branch_code = :branchcode")
	List<Object> getSaleOrderConvertedReport(Date date,String branchcode);

	@Query(nativeQuery = true,value = "select sor.receipt_mode,sum(sor.receipt_amount),sorm.REALIZED_AMOUNT "
			+ "from sales_receipts sor,sale_order_realized_mst sorm,sales_order_trans_hdr soh,"
			+ "sales_trans_hdr sh "
			+ "where sor.sales_trans_hdr_id=sh.id and sorm.sales_trans_hdr=sh.id and "
			+ "date(soh.voucher_date) < :date and "
			+ "sorm.sales_order_trans_hdr=soh.id and date(sh.voucher_date)=:date "
			+ "and sh.branch_code=:branchcode and sh.company_mst = :companymstid group by sor.receipt_mode,sorm.REALIZED_AMOUNT")
	List<Object> getSaleOrderReceiptByBranchCodeDateAndCompanymst(Date date, String branchcode, String companymstid);

	@Query(nativeQuery = true,value = "select sor.receipt_mode,sum(sor.receipt_amount) from "
			+ "sale_order_receipt sor,sales_order_trans_hdr soh"
			+ " where "
			+ "sor.sale_order_trans_hdr_id= soh.id and "
			+ "	date(soh.voucher_date)=:date and "
			+ "soh.branch_code=:branchcode and soh.company_mst = :companymstid and soh.id not in(select so.id from "
			+ "sales_order_trans_hdr so, sale_order_realized_mst sor, sales_trans_hdr sh "
			+ " where so.id=sor.sales_order_trans_hdr and sh.id=sor.sales_trans_hdr and date(sh.voucher_date)=:date) "
			+ " group by sor.receipt_mode  ")
	List<Object> getSalesOrderByDateBranchCodeAndCompanyMst(Date date, String branchcode, String companymstid);
	
	

@Query(nativeQuery = true,value ="select * from sales_order_trans_hdr where company_mst=:companyMst "
		+ "and branch_code=:branchcode and date(voucher_date)=:date and voucher_number=:invoiceno")
SalesOrderTransHdr findByVoucherNumberAndCompanyMstAndBranchCodeAndVoucherDate(String invoiceno, CompanyMst companyMst,
		String branchcode, Date date);

Optional<SalesOrderTransHdr> findByIdAndCompanyMstId(String salesordertranshdrid, String companymstid);





@Query(nativeQuery = true, value = "select * from sales_order_trans_hdr where customer_id=:customerId and "
		+ "date(voucher_date)=:udate and company_mst=:companyMst and voucher_number is null and customise_sales_mode =:customsalesmode")

SalesOrderTransHdr getSalesOrderTransHdrBycustomerAndVoucherNull(String customerId, Date udate, CompanyMst companyMst,
		String customsalesmode);


//@Query(nativeQuery = true, value = "  select "
//				+ "sh.voucher_date,"
//				+ "sh.voucher_number," 
//				+ "im.item_name,"  
//				+ "d.tax_rate," 
//				+ "d.rate, "
//				+ "d.qty,"
//				+ "d.discount as detail_discount ,"
//				+ "sh.invoice_amount," 
//				+ "d.igst_amount,"
//				+ " SUM(d.rate*d.qty)," 
//				+ "sh.invoice_discount,"
//				+ "d.batch,"
//				+ "im.item_code,"
//				+ " d.expiry_date "
//				+ "from branch_mst bm, sales_order_trans_hdr sh, sales_order_dtl d, customer_mst cm, unit_mst u,"
//				+ "item_mst im  where sh.branch_code = bm.branch_code "
//				+ " and sh.customer_id = cm.id and sh.id = d.sales_order_trans_hdr " + " and d.item_id = im.id "
//				+ "and d.unit_id = u.id  " + " and sh.voucher_number = :voucherNumber  " + " and date(sh.voucher_date) = :date "
//				+ " and sh.company_mst = :companymstid group by sh.voucher_date,sh.voucher_number,im.item_name,d.tax_rate,"
//				+ "d.rate,d.qty,d.discount,sh.invoice_amount,d.igst_amount, sh.invoice_discount,d.batch,im.item_code,d.expiry_date")


@Query(nativeQuery = true, value = "  select "
		+ "sh.voucher_date,"
		+ "sh.voucher_number," 
		+ "im.item_name,"  
		+ "d.tax_rate," 
		+ "d.rate, "
		+ "d.qty,"
		+ "d.discount as detail_discount ,"
		+ "sh.invoice_amount," 
		+ "d.igst_amount,"
		+ " SUM(d.rate*d.qty)," 
		+ "sh.invoice_discount,"
		+ "d.batch,"
		+ "im.item_code,"
		+ " d.expiry_date "
		+ "from branch_mst bm, sales_order_trans_hdr sh, sales_order_dtl d, account_heads cm, unit_mst u,"
		+ "item_mst im  where sh.branch_code = bm.branch_code "
		+ " and sh.customer_id = cm.id and sh.id = d.sales_order_trans_hdr " + " and d.item_id = im.id "
		+ "and d.unit_id = u.id  " + " and sh.voucher_number = :voucherNumber  " + " and date(sh.voucher_date) = :date "
		+ " and sh.company_mst = :companymstid group by sh.voucher_date,sh.voucher_number,im.item_name,d.tax_rate,"
		+ "d.rate,d.qty,d.discount,sh.invoice_amount,d.igst_amount, sh.invoice_discount,d.batch,im.item_code,d.expiry_date")
List<Object> getSaleOrderinoiceReport(String companymstid, String voucherNumber,Date date);


@Query(nativeQuery = true, value = "select * from sales_order_trans_hdr where voucher_number=:vouchernumber and "
		+ "date(voucher_date)=:date and company_mst=:companymstid")
SalesOrderTransHdr findByVoucherNumberAndVoucherDateAndCompanyMstId(String vouchernumber, Date date,
		String companymstid);

}
