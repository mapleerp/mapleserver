package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;



public interface KitDefinitionMstRepository extends JpaRepository<KitDefinitionMst,String>{

	//version3.6sari
	List<KitDefinitionMst> findAllByCompanyMstAndBranchCode(CompanyMst companyMstId,String branchcode);
	@Query(nativeQuery = true,
			  value="select * from kit_definition_mst k where lower(k.kit_name) like :data and "
			  		+ "k.company_mst=:companyMst "
			  )
	List<KitDefinitionMst> findAllByCompanyMstAndKitName(CompanyMst companyMst,String data);
	//version3.6end
	
	public KitDefinitionMst findByKitName(String kitName);
	
	public List<KitDefinitionMst> findByItemId(String itemId);
	
	
	@Query(nativeQuery = true,
			  value="select * from kit_defenition_dtl where kit_defenitionmst=:kitdefinitionMstId"
			  )
			  List<Object> findBykitDefenitionmst(String kitdefinitionMstId );
	@Modifying
	@Query(nativeQuery = true,value = "update kit_definition_mst set barcode =:barCode where item_id=:itemid")
	void updateBarcode(String itemid, String barCode);
	
	List<KitDefinitionMst> findByCompanyMst(CompanyMst companyMst);
			 
			
	@Query(nativeQuery = true,value="select m.minimum_qty,d.item_id,d.unit_id,d.qty  from kit_definition_mst m,kit_defenition_dtl d ,category_mst c,item_mst i where m.id=d.kit_defenitionmst and m.item_id=:itemId  and i.category_id=c.id and i.id=d.item_id and c.category_name!='PACKING MATERIAL' ")
	List<Object>fetchKitDtls(String itemId);
	
	
	
	@Query(nativeQuery = true,value="select * from kit_definition_mst")
	List<KitDefinitionMst> getKitDefinitionMst();
	
	
}
