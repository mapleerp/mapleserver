package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import com.maple.restserver.entity.PurchaseReturnHdr;

public interface PurchaseReturnHdrRepository extends JpaRepository<PurchaseReturnHdr, String>{

	List<PurchaseReturnHdr> findByUserIdAndCompanyMst(String companymstid, String id);

	void deleteById(String id);

}
