package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.WebUser;

@Repository
public interface WebUserRepository extends JpaRepository<WebUser, String>{

}
