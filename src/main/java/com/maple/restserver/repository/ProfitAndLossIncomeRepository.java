package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProfitAndLossIncome;


@Repository
public interface ProfitAndLossIncomeRepository extends JpaRepository<ProfitAndLossIncome, String>{

	List<ProfitAndLossIncome> findByCompanyMst(CompanyMst companyMst);

}
