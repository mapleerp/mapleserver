package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.DepartmentMst;
@Component
@Repository
public interface DepartmentMstRepository extends JpaRepository<DepartmentMst,Integer>{

	
	@Query(nativeQuery = true,value = "SELECT * FROM DEPARTMENT_MST WHERE department_name = :departmentname")
	List<DepartmentMst> getDepartmentByName(String departmentname);

//	List<DepartmentMst> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);






}
