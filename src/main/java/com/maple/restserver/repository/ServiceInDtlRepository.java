package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ServiceInDtl;
import com.maple.restserver.entity.ServiceInHdr;

@Repository

public interface ServiceInDtlRepository extends JpaRepository<ServiceInDtl,String>{

	
	List<ServiceInDtl> findByServiceInHdrId(String hdrId);
	
	
//	@Query(nativeQuery = true,value = "select sd.item_name,sd.model,p.product_name,b.brand_name,"
//			+ "sd.complaints,sd.under_warranty,sd.observation,c.customer_name,c.customer_address,sh.voucher_number,"
//			+ "sh.voucher_date,sd.qty,sd.serial_id from product_mst p, brand_mst b,service_in_dtl sd,service_in_hdr sh,customer_mst c where sd.service_in_hdr=sh.id"
//			+ " and p.id=sd.product_id "
//			+ "and sh.customer_mst = c.id and b.id=sd.brand_id and sh.voucher_number =:voucherNumber and sh.voucher_date =:fDate and "
//			+ "sh.branch_code =:branchcode and sh.company_mst=:companymstid ")
	
	@Query(nativeQuery = true,value = "select sd.item_name,sd.model,p.product_name,b.brand_name,"
			+ "sd.complaints,sd.under_warranty,sd.observation,c.account_name,c.party_address1,sh.voucher_number,"
			+ "sh.voucher_date,sd.qty,sd.serial_id from product_mst p, brand_mst b,service_in_dtl sd,service_in_hdr sh,account_heads c where sd.service_in_hdr=sh.id"
			+ " and p.id=sd.product_id "
			+ "and sh.account_heads = c.id and b.id=sd.brand_id and sh.voucher_number =:voucherNumber and sh.voucher_date =:fDate and "
			+ "sh.branch_code =:branchcode and sh.company_mst=:companymstid ")
	List<Object>generateServiceInvoice(CompanyMst companymstid,String branchcode,Date fDate,String voucherNumber);


	
	@Query(nativeQuery = true,value = "select dtl.qty, "
			+ "dtl.model,"
			+ " dtl.item_name, "
			+ "dtl.under_warranty, "
			+ "dtl.observation, "
			+ "dtl.complaints,"
			+ "c.customer_name, "
			+ "p.product_name, "
			+ "b.brand_name,"
			+ " hdr.voucher_date, "
			+ "dtl.serial_id "
			+ "from service_in_hdr hdr, service_in_dtl dtl, product_mst p, brand_mst b, "
			+ "customer_mst c where "
			+ "dtl.service_in_hdr=hdr.id and dtl.product_id=p.id "
			+ "and dtl.brand_id=b.id and hdr.customer_mst=c.id and hdr.company_mst=:companyMst and date(hdr.voucher_date) between :fromDate and :toDate")
	List<Object> findByCompanyMstAndBetweenDate(CompanyMst companyMst, Date fromDate, Date toDate);

	List <ServiceInDtl> findByServiceInHdr(ServiceInHdr serviceInHdr);

	@Query(nativeQuery = true,value = "select sum(d.qty) as qty, "
			+ "sum(d.qty * i.standard_price)-sum(d.qty * d.cost_price)-sum(d.discount) as amount, "
			+ "i.item_name, d.cost_price, "
			+ "i.standard_price, d.discount from "
			+ "sales_trans_hdr h, sales_dtl d, item_mst i "
			+ "where d.sales_trans_hdr_id=h.id and h.voucher_number is not null "
			+ "and d.item_id=i.id and i.service_or_goods=:type and h.company_mst=:companyMst and "
			+ "date(h.voucher_date) between :sdate and :edate group by i.item_name, i.item_name, d.cost_price,"
			+ " i.standard_price, d.discount")
	List<Object> findByServiceOrItemProfit(CompanyMst companyMst, Date sdate, Date edate, String type);


}
