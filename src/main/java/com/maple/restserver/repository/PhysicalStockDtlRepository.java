package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.PhysicalStockDtl;
import com.maple.restserver.entity.PhysicalStockHdr;
import com.maple.restserver.entity.SalesDtl;

@Repository
public interface PhysicalStockDtlRepository extends JpaRepository< PhysicalStockDtl,String>{
	
	List<PhysicalStockDtl> findByPhysicalStockHdrId(PhysicalStockHdr salesTransId);
    Optional<PhysicalStockDtl> findByIdAndId(String Id, String salesTransId);
    List<PhysicalStockDtl> findByCompanyMstId(String companymstid);

}
