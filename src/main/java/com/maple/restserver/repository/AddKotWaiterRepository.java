package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AddKotTable;
import com.maple.restserver.entity.AddKotWaiter;
@Component
@Repository
public interface AddKotWaiterRepository extends JpaRepository<AddKotWaiter, String>{
	
//	List<AcceptStock> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
	List<AddKotWaiter>findByCompanyMstId(String companymstid);
	AddKotWaiter findByCompanyMstIdAndWaiterName(String companymstid, String waiterName);
	
	
	@Query(nativeQuery=true,value="select *from add_kot_waiter a where a.status ='ACTIVE' and a.company_mst=:companymstid") 
	List<AddKotWaiter> getKotWaiterbyStatus(String companymstid);
}
