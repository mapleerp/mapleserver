package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.UserCreatedTaskIn;



public interface UseCreatedTaskInRepository extends JpaRepository<UserCreatedTaskIn, String> {

	List<UserCreatedTaskIn> findByUserIdAndCompanyMst(String companymstid, String userid);

}

