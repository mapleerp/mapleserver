package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
 
@Component
@Repository
public interface AccountHeadsRepository extends JpaRepository<AccountHeads, String>{

	
	@Query("SELECT a FROM AccountHeads a WHERE LOWER(a.accountName) like   LOWER(:accoutName) AND a.companyMst.id=:companymstid")
	 List<AccountHeads> findByAccountNameAndCompanyMstIdLike(String accoutName,String companymstid,Pageable topfifty);
	
  	
	 AccountHeads findByAccountNameAndCompanyMstId(String accoutName,String companymstid);
	 
	 AccountHeads findByAccountName (String accoutName);
		
	 Optional<AccountHeads> findById (String accountId);
 
	
	 List<AccountHeads> findByCompanyMstId(String companymstid);

	 
	 @Query(nativeQuery=true,value="select * from account_heads a where a.group_only ='Y' and a.company_mst=:companymstid") 
	List<AccountHeads> getAccountHeads(String companymstid);
		 
 
	 
	 AccountHeads findByIdAndCompanyMstId(String id, String companymstid);
	 
 @Query(nativeQuery=true,value="select a.id from account_heads a where a.account_name='BANK ACCOUNTS'and a.company_mst=:companymstid")
	 List<String> findByBankAccount(String  companymstid);
 
 @Query(nativeQuery=true,value="select * from account_heads a where a.parent_id=:id")
 List<AccountHeads> fetchAccountHeads(String id);


AccountHeads findByIdAndCompanyMst(String id, CompanyMst companyMst);

@Query(nativeQuery = true,value ="select * from account_heads where parent_id=:id")
List<AccountHeads> getAccountHeadsUnderExpense(String id);

@Query(nativeQuery=true,value="select a.id from account_heads a where a.account_name='ASSETS'and a.company_mst=:companymstid")
List<String> findByAssetAccount(String companymstid);


@Query("SELECT a FROM AccountHeads a WHERE a.parentId=:parentid and LOWER(a.accountName) like   LOWER(:string) AND a.companyMst.id=:companymstid")

List<AccountHeads> findByParentIdAccountNameAndCompanyMstIdLike(String parentid, String string, String companymstid,
		Pageable topFifty);


@Query("SELECT a FROM AccountHeads a WHERE a.serialNumber is not null AND a.companyMst.id=:companymstid")

List<AccountHeads> getAccountHeadsBySerialNumber(String companymstid);

@Query(nativeQuery = true,value = "select * from account_heads where parent_id is null")
List<AccountHeads> getaccountHeadsWithNullParent();

//@Query(nativeQuery = true,value = "SELECT sth.voucher_date,sth.voucher_number,cm.customer_name,cm.customer_gst,sth.invoice_amount,SUM(sd.qty*sd.rate),"
//		+ "SUM(sd.cgst_amount+sd.igst_amount+sd.sgst_amount),br.branch_name,sd.tax_rate,sum(case when sd.tax_rate=0 THEN sd.qty*sd.rate END ) as exempted "
//		+ "FROM ledger_class lc,sales_trans_hdr sth,customer_mst cm,sales_dtl sd,branch_mst br "
//		+ "WHERE lc.source_voucher_id=sth.voucher_number AND "
//		+ "sth.customer_mst=cm.id AND date(lc.trans_date) >= :fdate "
//		+ "AND date(lc.trans_date) <= :tdate AND sth.id=sd.sales_trans_hdr_id AND sth.branch_code=br.branch_code "
//		+ "GROUP BY "
//		+ "sth.voucher_date,sth.voucher_number,cm.customer_name,sth.invoice_amount,br.branch_name,sd.tax_rate")

@Query(nativeQuery = true,value = "SELECT sth.voucher_date,sth.voucher_number,cm.account_name,cm.party_gst,sth.invoice_amount,SUM(sd.qty*sd.rate),"
		+ "SUM(sd.cgst_amount+sd.igst_amount+sd.sgst_amount),br.branch_name,sd.tax_rate,sum(case when sd.tax_rate=0 THEN sd.qty*sd.rate END ) as exempted "
		+ "FROM ledger_class lc,sales_trans_hdr sth,account_heads cm,sales_dtl sd,branch_mst br "
		+ "WHERE lc.source_voucher_id=sth.voucher_number AND "
		+ "sth.account_heads=cm.id AND date(lc.trans_date) >= :fdate "
		+ "AND date(lc.trans_date) <= :tdate AND sth.id=sd.sales_trans_hdr_id AND sth.branch_code=br.branch_code "
		+ "GROUP BY "
		+ "sth.voucher_date,sth.voucher_number,cm.account_name,sth.invoice_amount,br.branch_name,sd.tax_rate")
List<Object> findLedgerDetails(Date fdate, Date tdate);

@Query(nativeQuery = true,value = "SELECT sth.voucher_date,sth.voucher_number,cm.account_name,cm.party_gst,sth.invoice_amount,SUM(sd.qty*sd.rate),"
		+ "SUM(sd.cgst_amount+sd.igst_amount+sd.sgst_amount),br.branch_name,sd.tax_rate,sum(case when sd.tax_rate=0 THEN sd.qty*sd.rate END ) as exempted "
		+ "FROM ledger_class lc,sales_trans_hdr sth,account_heads cm,sales_dtl sd,branch_mst br "
		+ "WHERE lc.source_voucher_id=sth.voucher_number AND "
		+ "sth.account_heads=cm.id AND date(lc.trans_date) >= :fdate "
		+ "AND date(lc.trans_date) <= :tdate AND sth.id=sd.sales_trans_hdr_id AND sth.branch_code=br.branch_code "
		+ "GROUP BY "
		+ "sth.voucher_date,sth.voucher_number,cm.account_name,sth.invoice_amount,br.branch_name,sd.tax_rate,cm.party_gst")
List<Object> findLedgerDetailsDerby(Date fdate, Date tdate);


@Query(nativeQuery = true,value ="SELECT ph.voucher_date,ph.voucher_number,sup.account_name,sup.party_gst,ph.invoice_total,"
		+ "SUM(pd.purchse_rate*pd.qty),pd.tax_amt,pd.tax_rate,br.branch_name,SUM(case when pd.tax_rate=0 THEN pd.qty*pd.purchse_rate END ) as exempted "
		+ "FROM ledger_class lc,purchase_hdr ph,purchase_dtl pd,account_heads sup,branch_mst br "
		+ "WHERE lc.source_voucher_id=ph.voucher_number AND ph.supplier_id=sup.id AND ph.id=pd.purchase_hdr_id AND "
		+ "date(lc.trans_date) >= :fdate AND date(lc.trans_date) <= :tdate AND ph.branch_code=br.branch_code "
		+ "GROUP BY "
		+ "ph.voucher_date,ph.voucher_number,sup.account_name,sup.party_gst,ph.invoice_total,pd.tax_amt,pd.tax_rate,br.branch_name")
List<Object> findGstInputDtlAndSmry(Date fdate, Date tdate);




@Query(nativeQuery = true,value ="SELECT id,account_name FROM account_heads WHERE  parent_id = :assetId")
List<AccountHeads> getAccountHeadsByParentId(String assetId);


@Modifying
@Query(nativeQuery = true,value ="UPDATE  account_heads SET status=:objectType WHERE id=:id")
void updateStatusInAccountHeads(String id, String objectType);
@Modifying
@Query(nativeQuery = true,value ="UPDATE  account_heads SET account_name=:objectName,status=:objectType WHERE id=:id")
void updateNameAndStatusInAccountHeads(String id, String objectName, String objectType);


@Query(nativeQuery = true,value ="SELECT * FROM account_heads "
		+ "WHERE LOWER(account_name) LIKE  LOWER(:searchData) "
		+ "AND company_mst=:companyMst")
List<AccountHeads> findSearchByData(String searchData, CompanyMst companyMst);




List<AccountHeads> findByCompanyMstAndPartyGst(CompanyMst companyMst, String gst);

@Query(nativeQuery = true,value ="SELECT c.*  FROM account_heads c WHERE LOWER(c.account_name) like  "
		+ " LOWER(:account_name) AND c.company_mst=:companyMst"
		+ " ORDER BY c.customer_rank DESC" )
List<AccountHeads> findSearch(String account_name, CompanyMst companyMst, Pageable topFifty);




AccountHeads findByCustomerContact(String customerphoneno);

@Query(nativeQuery = true,value ="SELECT c.* FROM account_heads c WHERE LOWER(c.id) like  "
		+ " LOWER(:string) AND c.company_mst=:companyMst"
			+ " and c.account_name=:accountname" )
Optional<AccountHeads> findByAccountNameAndCompanyMstIdAndBranchCode(String accountname, String companyMst,
		String string);


AccountHeads findByAccountNameAndCompanyMst(String accountname, CompanyMst companyMst);

@Query(nativeQuery = true,value="select count(id) from account_heads")
int accountHeadsCount();

@Query(nativeQuery = true,value="select * from account_heads OFFSET :nCount ROWS FETCH NEXT 1 ROW ONLY")
AccountHeads randomAccountHeads(int n);

@Query(nativeQuery = true,value="select "
 		+ "c.company_name,b.branch_name,b.branch_address1,"
 		+ "b.branch_address2,b.branch_state,b.branch_gst,"
 		+ "b.branch_email,b.branch_website,b.branch_tel_no,"
 		+ "s.account_name,s.company_name,s.customer_contact,s.party_mail,l.remark,"
 		+ "l.debit_amount,l.credit_amount,l.source_voucher_id,"
 		+ "l.trans_date,sum(l.credit_amount)-sum(l.debit_amount) as pendingAmount from "
 		+ "company_mst c,branch_mst b,account_heads s,ledger_class l "
 		+ "where b.branch_code=:branchcode and s.id=:supplierid and c.id=:companymstid and "
 		+ "l.account_id =:supplierid and date(l.trans_date) between :fromdate and :todate group "
 		+ "by c.company_name,b.branch_name,b.branch_address1,b.branch_address2,"
 		+ "b.branch_state,b.branch_gst,"
 		+ "b.branch_email,b.branch_website,b.branch_tel_no,"
 		+ "s.account_name,s.company_name,s.customer_contact,s.party_mail,l.remark,"
 		+ "l.debit_amount,l.credit_amount ,l.source_voucher_id ,l.trans_date")
List<Object> getMonthlyAccountHeadsLedgerReport(String companymstid, String supplierid, String branchcode,
		Date fromdate, Date todate);

@Query(nativeQuery = true,value=" select "
 		+ "sum(l.credit_amount)-sum(l.debit_amount) from ledger_class l,"
 		+ " account_heads s, company_mst c "
 		+ "where l.account_id=s.id "
 		+ "and l.company_mst=c.id "
 		+ "and s.id=:supplierid "
 		+ "and c.id=:companymstid "
 		+ "and l.trans_date<:fromdate ")
Double openingBalance(String companymstid, Date fromdate, String supplierid);

@Query(nativeQuery = true,value=" select  a.voucher_number  , s.account_name, s.customer_contact,"
 		+ " s.party_mail, a.voucher_date, a.due_date, a.due_amount, a.paid_amount, "
 		+ "s.company_name,"
 		+ "( a.due_amount-a.paid_amount) as pendingAmont from account_payable a , "
 		+ "account_heads s, company_mst c   where a.account_id = s.id  and "
 		+ "a.company_mst  = c.id and a.account_id=:supplierid"
 		+ " and c.id=:companymstid"
 		+ "  and ( a.due_amount-a.paid_amount) != 0.0"
 		+ " and a.voucher_date between :fromDate and :toDate ")
List<Object> getAccountHeadsDueDayReport(String companymstid, String supplierid, Date fromDate, Date toDate);

@Query(nativeQuery = true,value=" select "
	+ "sum(l.credit_amount)-sum(l.debit_amount) from ledger_class l,"
	+ " account_heads s, company_mst c "
	+ "where l.account_id=s.id "
	+ "and l.company_mst=c.id "
	+ "and s.id=:supplierid "
	+ "and c.id=:companymstid "
	+ "and l.trans_date<:fromDate ")
Double getOpeningBalanceFromLedgerClass(String companymstid, String supplierid, Date fromDate);

@Modifying
@Query(nativeQuery = true,value="update account_heads set customer_discount=0 where customer_discount is NULL")
void updateCustDiscount();




@Query(nativeQuery = true,value ="SELECT * FROM account_heads  WHERE LOWER(id) like  "
		+ " LOWER(:string) AND company_mst=:companyMst"
			+ " and account_name=:accountname" )
Optional<AccountHeads> findByAccountNameAndBranchCodeAndCompanyMstId(String accountname,String string, String companyMst);

@Query(nativeQuery = true,value="select id, account_name, parent_id, group_only, machine_id, tax_id from account_heads "
		+ " where lower(account_name) LIKE  :searchAccountName")
List<Object> findSearch(String searchAccountName, Pageable topFifty);



 

	
}
