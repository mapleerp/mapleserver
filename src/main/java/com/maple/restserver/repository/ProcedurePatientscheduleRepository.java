package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.ProcedurePatientschedule;

@Repository
public interface ProcedurePatientscheduleRepository extends JpaRepository<ProcedurePatientschedule, Integer>{

//	List<ProcedurePatientschedule> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
