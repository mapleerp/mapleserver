package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.DamageHdr;
@Component
@Repository
public interface DamageDtlRepository extends JpaRepository<DamageDtl, String>{
	DamageDtl findByIdAndCompanyMstId(String id,String companymstid);
	
	List<DamageDtl> findByDamageHdrIdAndCompanyMstId(String hdrid,String companymstid);
	
	
	 @Query(nativeQuery=true,value="select c.company_name,"
	 		+ " b.branch_name,"
	 		+ "b.branch_code,"
	 		+ " i.item_name,ib.batch,d.qty,h.voucher_date,"
	 		+ "d.narration from damage_hdr h,damage_dtl d,"
	 		+ "branch_mst b,item_batch_mst ib,company_mst c, "
	 		+ "item_mst i where "
	 		+ "d.damage_hdr= h.id and ib.item_id=d.item_id"
	 		+ " and d.item_id=i.id and "
	 		+ " b.branch_code=h.branch_code and c.id =h.company_mst and ib.batch=d.batch_code and "
	 		+ "h.company_mst=:companymst "
	 		+ " and h.branch_code=:branchCode and date(h.voucher_date) between :fromdate and :todate  "
	 		+ "")
	List<Object>getDamageEntryReport(CompanyMst companymst,Date fromdate,Date todate,String branchCode);
//	 
	List<DamageDtl> findByDamageHdr(DamageHdr damageHdr);

	   @Query(nativeQuery = true,value="select sum(d.qty) from damage_dtl d, damage_hdr h where d.damage_hdr = h.id and d.item_id =:itemId and h.id=:hdrId and d.batch_code=:batch")
	    Double getSalesDetailItemQty(String hdrId,String itemId,String batch);

	   
	   @Query(nativeQuery=true,value="SELECT "
		 		+ "i.item_name,"
		 		+ "i.item_code,"
		 		+ "dd.batch_code,"
		 		+ "i.standard_price,"
		 		+ "dd.qty,"
		 		+ "i.standard_price * dd.qty,"
		 		+ "um.unit_name,"
		 		+ "ib.exp_date,"
		 		+ "dh.voucher_date,"
		 		+ "dh.vouche_number,"
		 		+ "dd.narration "
		 		+ "FROM "
		 		+ "damage_hdr dh,"
		 		+ "damage_dtl dd,"
		 		+ "branch_mst b,"
		 		+ "item_batch_mst ib,"
		 		+ "company_mst c,"
		 		+ "item_mst i,"
		 		+ "unit_mst um "
		 		+ "WHERE "
		 		+ "dd.damage_hdr= dh.id AND "
		 		+ "ib.item_id=dd.item_id AND "
		 		+ "dd.item_id=i.id AND "
		 		+ "b.branch_code=dh.branch_code AND "
		 		+ "c.id =dh.company_mst AND "
		 		+ "um.id = i.unit_id AND "
		 		+ "ib.batch=dd.batch_code AND "
		 		+ "dh.company_mst=:companyMst AND "
		 		+ "dh.branch_code=:branchCode AND "
		 		+ "date(dh.voucher_date) >= :fDate AND "
		 		+ "date(dh.voucher_date) <= :tDate AND "
		 		+ "dd.reason = :reason")
	List<Object> getWriteOffDetailsAndSummaryReport(CompanyMst companyMst, Date fDate, Date tDate, String branchCode,
			String reason);
	
}
