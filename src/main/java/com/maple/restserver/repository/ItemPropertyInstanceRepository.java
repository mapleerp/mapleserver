package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.GoodReceiveNoteDtl;
import com.maple.restserver.entity.ItemPropertyInstance;
import com.maple.restserver.entity.PurchaseDtl;

@RestController
public interface ItemPropertyInstanceRepository extends JpaRepository<ItemPropertyInstance, String> {

	
	List<ItemPropertyInstance>findByCompanyMst(CompanyMst companyMst);

	List<ItemPropertyInstance> findByItemIdAndPropertyName(String itemId, String propertyName);

	List<ItemPropertyInstance> findByCompanyMstAndBranchCodeAndPurchaseDtl(CompanyMst companyMst, String branchcode,
			PurchaseDtl purchasedtl);

	ItemPropertyInstance findByBranchCodeAndCompanyMstAndPropertyNameAndPurchaseDtl(String branchcode, CompanyMst companyMst,
			String property, PurchaseDtl purchaseDtl);

//	List<ItemPropertyInstance> findByCompanyMstAndBranchCodeAndGoodReceiveNoteDtl(CompanyMst companyMst,
//			String branchcode, String goodreceivenotedtl);
}
