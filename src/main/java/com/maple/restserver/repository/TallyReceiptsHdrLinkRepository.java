package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.TallyReceiptsHdrLink;
import com.maple.restserver.entity.TallySalesTransHdrLink;
import com.maple.restserver.entity.UserMst;
@Component
@Repository
public interface TallyReceiptsHdrLinkRepository extends JpaRepository<TallyReceiptsHdrLink, String>{
	
 
	 
	 @Query(nativeQuery=true, value="select a.id from Receipt_Hdr  a where a.id not in (select b.receipts_Hdr  "
	 		+ "  from Tally_Receipts_Hdr_Link b ) and date(voucher_date) >='2019-11-01'   ")
	 List<String>selectNotPostedReceipts ( );

	

}
