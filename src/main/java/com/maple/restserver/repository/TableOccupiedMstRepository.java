package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.TableOccupiedMst;

public interface TableOccupiedMstRepository extends JpaRepository<TableOccupiedMst,String> 
{
	List<TableOccupiedMst> findByTableId(String tableid);
	
	TableOccupiedMst findBySalesTransHdr(SalesTransHdr salesTransHdr);
}
