package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;

public interface ItemSearchRepositoryCustom {
	 List<ItemMst> findItemBySearch(String searchString , Pageable pageable);
	 List<ItemMst> findItemByBarcodeSearch(String searchString , Pageable pageable);
	 
 
}
