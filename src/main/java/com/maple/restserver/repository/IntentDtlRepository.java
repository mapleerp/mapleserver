package com.maple.restserver.repository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.StockTransferOutDtl;
@Component
@Repository
public interface IntentDtlRepository extends JpaRepository<IntentDtl, String>{
	
	List<IntentDtl> findByIntentHdrId(String intentHdrId );
    Optional<IntentDtl> findByIdAndId(String id, String intentHdrId);
    
    public void  deleteByItemId(String itemId);
    
    @Query(nativeQuery=true,value="select * from intent_dtl where intent_hdr_id=:id and branch_code=:branch")
    List<IntentDtl> findByBranchCode(String id, String branch);
    
    @Query(nativeQuery=true,value="select DISTINCT to_branch from intent_dtl where intent_hdr_id=:id")
    List<String> getDifferentBranchCode(String id);

	@Modifying
	@Query(nativeQuery = true, value ="update intent_hdr set to_branc=:branchCode where id=:hdrId")
     void UpdateIntetnHdrBranchCode(String branchCode,String hdrId);
	
 //   List<IntentDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
