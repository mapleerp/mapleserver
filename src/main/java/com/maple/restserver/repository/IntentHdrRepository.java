package com.maple.restserver.repository;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.ReorderMst;
@Component
@Repository
public interface IntentHdrRepository extends JpaRepository<IntentHdr, String>{
	
	IntentHdr findByIdAndCompanyMstId(String id,String companyid);
	List<IntentHdr>findByVoucherNumberAndVoucherDateAndVoucherType(String VoucherNumber, Date voucherDate,String voucherType);
	List<IntentHdr>findByVoucherNumberAndVoucherDate(String VoucherNumber, Date voucherDate);
	
	@Query("SELECT m FROM IntentHdr m WHERE date(voucherDate)+:date < :currentdate  ")
	List<ReorderMst>findSearchbytimeperiod(Integer  date, Date currentdate);
	
	List<IntentHdr>findByCompanyMstId(String company);
	
	 @Query(nativeQuery=true,value="select c.company_name,b.branch_address1,"
		 		+ "b.branch_address2 ,h.voucher_number,"
		 		+ "h.voucher_date,"
		 		+ "d.item_id,d.unit_name,"
		 		+ "d.qty,"
		 	
		 		+ "from intent_hdr h,intent_dtl d ,company_mst c,branch_mst b "
		 		+ "where h.id=d.intent_in_hdr"
		 		+ " and h.voucher_number=:voucherNumber"
		 		+ "h.company_mst=c.id "
		 		+ "and date(h.in_voucher_date)=:date"
		 		+ " and h.company_mst_id=:companymstid and b.branch_code=h.branch_code and h.branch_code=:branchCode")
	List<Object>	 getIntentVoucherReport(String voucherNumber, Date date, String companymstid,String branchCode);
	 @Query(nativeQuery=true,value="select c.company_name,b.branch_address1, "
		 		+ "b.branch_name, b.branch_code, b.branch_tel_no, b.branch_email, "
			 		+ "h.voucher_number,"
			 		+ "h.voucher_date,"
			 		+ " i.item_name,u.unit_name,"
			 		//--------------version 3.28
			 		+ "d.qty, s.account_name, s.address, s.phone_no, i.id "
			 	
			 		+ " from intent_hdr h , intent_dtl d ,company_mst c,branch_mst b, unit_mst u, account_heads s, reorder_mst r,"
			 		+ "item_mst i "
			 		//--------------version 3.28 end

			 		+ " where h.id=d.intent_hdr  "
			 		+ " and "
			 		+ "h.company_mst=c.id "
			 		+ "and date(h.voucher_date)=:fDate"
			 		+ " and h.company_mst=:companymstid and "
			 		+ "b.branch_code=h.branch_code and h.branch_code=:branchcode and u.id=d.unit_id  "
			 		//--------------version 3.28 

			 		+ " and r.item_id=d.item_id and s.id=r.supplier_id and d.item_id=i.id")
			//--------------version 3.28 end


		 List<Object> IntentReportByDate(String companymstid, String branchcode,Date fDate);
	// 
@Query(nativeQuery = true,value = "select from ")
List<Object>intentExportToExcel(String companymstid, String branchcode,Date fDate);




@Query(nativeQuery=true,value="select s.account_name,s.party_address1,"
	 		+ " s.customer_contact, MAX(h.voucher_date) "
	 		+ " from account_heads s , purchase_hdr h, purchase_dtl d "
	 		+ " where s.id=h.supplier_id and d.purchase_hdr_id=h.id and d.item_id:itemId and h.company_mst=:companymstid "
	 		+ "group by "
	 		+ "  s.account_name,s.party_address1,s.customer_contact")
List<Object> IntentSupplierReport(String companymstid, String itemId);

}

