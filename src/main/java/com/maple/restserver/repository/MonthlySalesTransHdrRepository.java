package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.MonthlySalesTransHdr;


@Repository
public interface MonthlySalesTransHdrRepository extends JpaRepository<MonthlySalesTransHdr, String>{

	

	@Modifying(flushAutomatically = true)
	 @Query(nativeQuery = true,value="INSERT INTO monthly_sales_trans_hdr (id ,customer_mst,local_customer_mst,customer_id,sales_man_id,delivery_boy_id,sales_mode,credit_or_cash,source_branch_code,take_order_number,fb_key,invoice_number_prefix ,machine_id,numeric_voucher_number,voucher_number,voucher_date,invoice_amount,invoice_discount,branch_code,company_mst,cash_pay,discount,amount_tendered,item_discount,paid_amount,change_amount,card_no,card_type,cardamount,"
	 		+ "voucher_type,serving_table_name, is_branch_sales,sodexo_amount,paytm_amount,credit_amount,sale_order_hrd_id,sales_receipts_voucher_number) "
	 		+ "SELECT h.id , h.customer_mst, h.local_customer_mst, h.customer_id,h.sales_man_id, h.delivery_boy_id, h.sales_mode, h.credit_or_cash, h.source_branch_code, h.take_order_number, h.fb_key, h.invoice_number_prefix , h.machine_id, h.numeric_voucher_number, h.voucher_number, h.voucher_date, h.invoice_amount, h.invoice_discount, h.branch_code,h.company_mst, h.cash_pay, h.discount, h.amount_tendered, h.item_discount, h.paid_amount, h.change_amount, h.card_no, h.card_type, "
	 		+ "h.cardamount," + 
	 	" h.voucher_type, h.serving_table_name, h.is_branch_sales, h.sodexo_amount, h.paytm_amount, h.credit_amount,h.sale_order_hrd_id , h.sales_receipts_voucher_number FROM sales_trans_hdr h WHERE date(h.voucher_date) between :fromDate and :toDate")
	void insertingIntoMonthlySalesTransHdr (Date fromDate ,Date toDate);
	
	
	@Query(nativeQuery = true,value = "select distinct(customer_id) from sales_trans_hdr h"
			+ " "
			+ "where date(h.voucher_date) between "
			+ " :fromDate and :toDate ")
			
	List<String> getDistinctCustId(Date fromDate,Date toDate);
	
	
	@Query(nativeQuery = true,value ="select sum(h.invoice_amount) from monthly_sales_trans_hdr h"
			+ " "
			+ "where h.customer_id =:custId "
			+ " and date(voucher_date) between :fromDate and :toDate  "
			+ " ")
	Double sumOfInvoiceAmount(String custId,Date fromDate, Date toDate);
	
	
	@Modifying
	@Query
	(nativeQuery = true,value ="delete from monthly_sales_trans_hdr where id not in "
			+ "(select monthly_sales_trans_hdr_id from monthly_sales_dtl)")
	void deleteOrphanMonthlySalesTransHdr();
	
	@Modifying
	@Query(nativeQuery = true, value ="delete from monthly_sales_trans_hdr")
	void deleteMonthlySalesTransHdrInitially();
	
	@Query
	(nativeQuery = true,value="select * from monthly_sales_trans_hdr h  where h.branch_code=:branchcode and h.company_mst=:companymstid and date(h.voucher_date) >=:fromdate and date(h.voucher_date)<=:todate ")
	List<MonthlySalesTransHdr>	getMonthlySalesReporthdr(String branchcode,
			Date fromdate,Date todate,String companymstid);
	

	MonthlySalesTransHdr findByVoucherNumber(String voucherNumber);
}
