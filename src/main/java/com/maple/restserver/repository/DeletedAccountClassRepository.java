package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.DeletedAccountClass;

@Repository
public interface DeletedAccountClassRepository extends JpaRepository<DeletedAccountClass, String>{
    @Modifying
	@Query(nativeQuery = true,value = "delete from supplier  where id=:accountId")
	void deleteMergedSupplier(String accountId);
    
    @Modifying
   	@Query(nativeQuery = true,value = "delete from customer_mst   where id=:accountId")
    void  deleteMergedCustomer(String accountId);
    
    @Modifying

    @Query(nativeQuery = true,value = "delete from account_heads   where id=:accountId")

    void deleteMergedAccountHeads(String accountId);
}
