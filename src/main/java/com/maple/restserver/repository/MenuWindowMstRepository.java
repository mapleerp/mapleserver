package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.MenuWindowMst;

@Controller
@Repository
public interface MenuWindowMstRepository extends JpaRepository<MenuWindowMst,String> {

	MenuWindowMst findByMenuName(String menuName);

}
