package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.OwnAccount;
@Component
@Repository
public interface OwnAccountRepository extends JpaRepository<OwnAccount, String> {

	
	 @Query(nativeQuery=true,value="select sum(o.debit_amount)-sum(o.credit_amount)-sum(o.realized_amount) from own_account  o where o.account_id=:accountid and o.company_mst=:companyMst")
	
	Double accountBalanceAmount(String accountid,CompanyMst companyMst);
	 
	 @Query(nativeQuery=true,value="select * from own_account o where o.debit_amount-o.credit_amount-o.realized_amount != 0 and o.account_id=:accid and o.company_mst=:companymstid")
	 List <OwnAccount> findByCompanyMstIdAndAccountId(String companymstid,String accid);
	 
	 OwnAccount findByCompanyMstAndId(CompanyMst companyMst,String id);
	 
	 @Query(nativeQuery=true,value=" select sum(o.credit_amount)-sum(o.debit_amount)-sum(o.realized_amount) from own_account  o where o.account_id=:accountid and o.company_mst=:companyMst")
	 Double accountBalanceAmountBycustomer(String accountid,CompanyMst companyMst );

	 @Query(nativeQuery=true,value="select sum(credit_amount) from own_account where receipt_hdr =:receipthdrId and company_mst=:companyMst")
	 Double sumOfCreditByReceiptHdrId(String receipthdrId, CompanyMst companyMst);
	 
	 
	 @Query(nativeQuery=true,value="select sum(debit_amount) from own_account where paymenthdr_id =:paymentdrid and company_mst=:companyMst")
	 Double sumOfDebitByPaymentHdrId(String paymentdrid, CompanyMst companyMst);
	 
	 @Query(nativeQuery=true,value="select * from own_account o where o.credit_amount-o.debit_amount-o.realized_amount != 0 and o.account_id=:accid and o.company_mst=:companymstid")
	 List <OwnAccount> findByCompanyMstIdAndAccountIdcustomer(String companymstid,String accid);
	 
	 List<OwnAccount> findByCompanyMstIdAndReceiptHdrId(String companymstid,String receipthrid);
	 
	 List<OwnAccount> findByCompanyMstIdAndPaymenthdrId(String companymstid,String paymentId);
	 
	 @Modifying
	 @Query(nativeQuery=true,value="delete from own_account where receipt_hdr =:receipthdr")
	 void deleteByReceiptHdrId(String receipthdr);

	 
	 @Modifying
	 @Query(nativeQuery=true,value="delete from own_account where paymenthdr_id =:paymenthdr")
	 void deleteByPaymenthdrId(String paymenthdr);

}
