package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MonthlySalesDtl;
import com.maple.restserver.entity.OpeningStockDtl;

@Repository
public interface OpeningStockDtlRepository  extends JpaRepository<OpeningStockDtl, String>{

List<OpeningStockDtl> findAllByCompanyMst(Optional<CompanyMst> companyMstOpt);


@Query(nativeQuery =true,value= "Select * from opening_stock_dtl where voucher_number is null")
List<OpeningStockDtl> findOpeningStockVoucherNumberNull(Optional<CompanyMst> companyMstOpt);

}
