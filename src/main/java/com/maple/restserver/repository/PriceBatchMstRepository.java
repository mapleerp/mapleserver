package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.PrescriptionDtl;
import com.maple.restserver.entity.PriceBatchMst;

 

@Repository

public interface PriceBatchMstRepository extends JpaRepository<PriceBatchMst,String>{

	
	//List<PrescriptionDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
