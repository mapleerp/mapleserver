
package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.ReceiptHdr;

@Transactional
@Repository
public interface PaymentHdrRepository extends JpaRepository<PaymentHdr, String> {

	@Query(nativeQuery = true, value = "select count(*) from payment_hdr where branch_code =:branchCode"
			+ " and date(voucher_date)=:sqlDate and company_mst =:companymstid")
	int countOfPaymentHdr(String branchCode, Date sqlDate, String companymstid);

	@Query(nativeQuery = true, value = "select * from payment_hdr h  where date(voucher_date) = :voucherDate AND  h.voucher_number = :voucherNumber ")
	List<PaymentHdr> findByVoucherNumberAndVoucherDate(String voucherNumber, Date voucherDate);

	List<PaymentHdr> findByVoucherNumber(String voucherNumber);

	List<PaymentHdr> findByVoucherDateAndCompanyMstAndBranchCode(Date date, CompanyMst companyMst, String branchcode);

	PaymentHdr findByVoucherNumberAndCompanyMst(String invoiceno, CompanyMst companyMst);

	// here we are writing payment summary report betweeen two date
	@Query(nativeQuery = true, value = "from payment_hdr  where date(voucher_date) >= :startDate AND date(voucher_date)<=:endDate")
	List<PaymentHdr> paymentSummaryReport(Date startDate, Date endDate);

	@Query(nativeQuery = true, value = "select a.account_name," + " d.amount " + " from payment_dtl d , payment_hdr h ,"
			+ " account_heads a " + " where a.id = d.account_id and"
			+ " d.paymenthdr_id = h.id AND date(h.voucher_date) >= :date AND " + " date(h.voucher_date) <= :date "

			+ "")
	List<Object> PaymentDetails(@Param("date") Date date);

	@Query(nativeQuery = true, value = "select b.branch_name," + " h.voucher_number ," + " h.voucher_date ,"
			+ " a.account_name ," + " d.amount," + " d.remark " + " from payment_dtl d , payment_hdr h ,"
			+ " account_heads a ,branch_mst b " + "  where a.id = d.account_id and" + " h.voucher_number=:vNumber and"
			+ " date(h.voucher_date)=:vDate and" + "h.branch_code=b.branch_code" + "")
	List<Object> getDailyPayment(String vNumber, Date vDate);

	@Query(nativeQuery = true, value = " SELECT b.branch_name, " + " h.voucher_number ," + " h.voucher_date ,"
			+ " a.account_name ," + " d.amount ," + " d.remark " + " from payment_Dtl d , payment_hdr h ,"
			+ " account_heads a ,branch_mst b " + " where  a.id = d.account_id  and" + " d.id = h.id  and"
			+ " h.branch_code= b.branch_code")
	List<Object> getAllDailyPayments();

	// h.voucher_date=:date

	@Query(nativeQuery = true, value = "select b.branch_name,b.branch_email,b.branch_website,h.voucher_number,h.voucher_date,d.remark,d.member_id ,d.amount,d.instrument_number,d.instrument_date,d.mode_of_payment,"
			+ " ac.account_name ,c.company_name,sum(d.amount) as totalamount from company_mst c, payment_hdr h,payment_dtl d,branch_mst b, account_heads ac"
			+ " where h.id=d.paymenthdr_id and h.voucher_number=:vouchernumber and b.branch_code=h.branch_code and ac.id=d.account_id and h.branch_code=:branchCode and date(h.voucher_date)=:date "
			+ "and c.id=h.company_mst  group by b.branch_name,b.branch_email,h.voucher_number,b.branch_website,h.voucher_number,h.voucher_date,d.remark ,d.member_id,d.amount,d.instrument_number,"
			+ "d.instrument_date,d.mode_of_payment,ac.account_name,c.company_name ")
	List<Object> getPaymentReport(String vouchernumber, Date date, String branchCode);

	@Query(nativeQuery = true, value = "select h.voucher_number,h.voucher_date,sum(d.amount) as totalamount,"
			+ "ac.account_name,h.id from payment_hdr h,payment_dtl d,account_heads ac "
			+ "where h.id=d.paymenthdr_id and"
			+ " ac.id=d.account_id and h.company_mst=:companymstid and h.branch_code =:branchCode and "
			+ "date(h.voucher_date) between :fdate and :tdate"
			+ " group by h.voucher_number,h.voucher_date,d.amount,ac.account_name,h.id")
	List<Object> getPaymentReportBetweenDate(Date fdate, Date tdate, String branchCode, String companymstid);

	@Query(nativeQuery = true, value = "select h.voucher_number,h.voucher_date,sum(d.amount) as totalamount,"
			+ "ac.account_name,h.id from payment_hdr h,payment_dtl d,account_heads ac "
			+ "where h.id=d.paymenthdr_id and"
			+ " ac.id=d.account_id and h.company_mst=:companymstid and h.branch_code =:branchCode and "
			+ "date(h.voucher_date) between :fdate and :tdate and d.account_id =:accountid"
			+ " group by h.voucher_number,h.voucher_date,d.amount,ac.account_name,h.id")
	List<Object> getPaymentReportBetweenDateByAccount(String accountid, Date fdate, Date tdate, String branchCode,
			String companymstid);

	@Query(nativeQuery = true, value = "select b.branch_name,b.branch_email,b.branch_website,h.voucher_number,h.voucher_date,d.remark,d.member_id ,d.amount,d.instrument_number,d.instrument_date,d.mode_of_payment,"
			+ " f.family_name,m.member_name ,ac.account_name ,c.company_name,sum(d.amount) as totalamount from company_mst c, payment_hdr h,payment_dtl d,branch_mst b, account_heads ac,family_mst f,member_mst m"
			+ " where h.id=d.paymenthdr_id and h.voucher_number=:vouchernumber and b.branch_code=h.branch_code and ac.id=d.account_id and h.branch_code=:branchCode and date(h.voucher_date)=:date "
			+ "and c.id=h.company_mst and m.member_id=:memberId and f.family_mst_id=m.family_id group by b.branch_name,b.branch_email,h.voucher_number,b.branch_website,h.voucher_number,h.voucher_date,d.remark ,d.member_id,d.amount,d.instrument_number,"
			+ "d.instrument_date,d.mode_of_payment,ac.account_name,c.company_name,f.family_name,m.member_name")
	List<Object> getOrgPaymentReport(String vouchernumber, Date date, String branchCode, String memberId);

	@Query(nativeQuery = true, value = "select sum(d.amount) from payment_dtl d, payment_hdr h where "
			+ "h.id=d.paymenthdr_id and date(h.voucher_date)=:fdate and h.branch_code =:branchCode and "
			+ "h.company_mst = :companymstid and d.mode_of_payment=:acid")
	Double getPaymentReportbyDate(Date fdate, String branchCode, String companymstid, String acid);

	@Query(nativeQuery = true, value = "select distinct d.MODE_OF_PAYMENT " + "from payment_dtl d, payment_hdr h where "
			+ "d.PAYMENTHDR_ID=h.id and h.company_mst=:companymstid   ")
	List<String> findAllPaymentMode(String companymstid);

	@Query(nativeQuery = true, value = "select sum(d.AMOUNT) as amount, "
			+ "a.ACCOUNT_NAME, d.MODE_OF_PAYMENT from account_heads a, payment_dtl d, payment_hdr h "
			+ "where d.PAYMENTHDR_ID=h.id and h.company_mst=:companymstid and "
			+ "h.VOUCHER_NUMBER is NOT NULL and a.id=d.CREDIT_ACCOUNT_ID and "
			+ "date(h.VOUCHER_DATE)=:date and d.MODE_OF_PAYMENT=:paymentMode group by "
			+ "a.ACCOUNT_NAME, d.MODE_OF_PAYMENT" + "")
	List<Object> findSumOfPaymentBySalesModeAndDate(String paymentMode, Date date, String companymstid);

	List<PaymentHdr> findByVoucherDate(Date date);

	@Query(nativeQuery = true, value = "select count(*) from payment_hdr where voucher_date=:date and voucher_number is NOT NULL ")
	int clientPaymentCount(Date date);

	@Query(nativeQuery = true, value = "select sum(d.amount) from payment_hdr h, payment_dtl d h.id=d.paymenthdr_id and h.branch_code=:branch and h.company_mst=:companymstid and h.voucher_date=:date")
	Double getDailyPaymentAmount(Date date, String branch, String companymstid);

	// version 3.1 sathyajith
	@Query(nativeQuery = true, value = "select " + "h.voucher_number," + "d.amount," + "d.instrument_date,"
			+ "d.instrument_number," + "d.mode_of_payment," + "a.account_name ," + "h.voucher_date  from payment_hdr h,"
			+ " payment_dtl d, " + "account_heads a " + "where h.id=d.payment_hdr_id " + "and a.id=d.account_id and"
			+ " date(h.voucher_date) between :sdate and :edate " + "and h.company_mst=:companyMst"
			+ " and h.branch_code=:branchCode")
	List<Object> getPaymentDtlReport(Date sdate, Date edate, CompanyMst companyMst, String branchCode);
// version 3.1 end 

	// ----------------version 4.14

	@Query(nativeQuery = true, value = "select sum(d.AMOUNT) as amount "
			+ " from account_heads a, payment_dtl d, payment_hdr h "
			+ "where d.PAYMENTHDR_ID=h.id and h.company_mst=:companymstid and "
			+ "h.VOUCHER_NUMBER is NOT NULL and a.id=d.CREDIT_ACCOUNT_ID and "
			+ "date(h.VOUCHER_DATE)=:date and d.MODE_OF_PAYMENT=:paymentMode  " + "")
	Double findSumOfPaymentBySalesModeAndDateByPaymentMode(String paymentMode, Date date, String companymstid);

	// ----------------version 4.14 end
	@Query(nativeQuery = true, value = "select * from payment_hdr h where h.company_mst=:companyMst and h.branch_code=:branchcode and date(h.voucher_date) between :fromDate and :toDate")

	List<PaymentHdr> fetchPaymetHdr(CompanyMst companyMst, String branchcode, Date fromDate, Date toDate);

	@Query(nativeQuery = true, value = "select h.voucher_number," + "h.voucher_date," + "h.voucher_type,"
			+ " d.credit_account_id," + "d.account_id," + "d.instrument_number," + "d.bank_account_number,"
			+ "d.mode_of_payment," + "d.amount"
			+ " from payment_hdr h,payment_dtl d where h.id=d.paymenthdr_id and  h.company_mst=:companyMst and h.branch_code=:branchcode and date(h.voucher_date) between :fromDate and :toDate ")
	List<Object> exportPaymentReport(CompanyMst companyMst, Date fromDate, Date toDate, String branchcode);

	@Query(nativeQuery = true, value = "select h.voucher_number," + "h.voucher_date," + "h.voucher_type,"
			+ " d.credit_account_id," + "d.account_id," + "d.instrument_number," + "d.bank_account_number,"
			+ "d.mode_of_payment," + "d.amount"
			+ " from payment_hdr h,payment_dtl d where h.id=d.paymenthdr_id and  h.company_mst=:companyMst and h.branch_code=:branchcode and date(h.voucher_date) between :fromDate and :toDate ")
	List<Object> paymentReportReportPrinting(CompanyMst companyMst, Date fromDate, Date toDate, String branchcode);

	@Query(nativeQuery = true, value = "select a.account_name, sum(d.amount) from payment_hdr h,payment_dtl d,account_heads a where h.id=d.paymenthdr_id and a.id=d.account_id and date(h.voucher_date)=:fromDate and h.company_mst=:companyMst and h.branch_code=:branchcode group by a.account_name ")
	List<Object> paymentReport(CompanyMst companyMst, Date fromDate, String branchcode);
//-------------------version 6.5 surya 

	PaymentHdr findByVoucherNumberAndCompanyMstAndBranchCode(String invoiceno, CompanyMst companyMst,
			String branchcode);
//-------------------version 6.5 surya end

	@Query(nativeQuery = true, value = "select * from payment_hdr where "
			+ "date(voucher_date) >= :fdate and date(voucher_date) <= :tdate")
	List<PaymentHdr> getPaymentHdrBetweenDate(Date fdate, Date tdate);

	@Query(nativeQuery = true, value = "select * from payment_hdr h where h.voucher_number=:voucherNumber")
	PaymentHdr fetchPaymentHdrByVoucherNumber(String voucherNumber);

	@Query(nativeQuery = true, value = "select a.account_name,pd.amount from account_heads a,"
			+ "payment_hdr p,payment_dtl pd where pd.paymenthdr_id=p.id and "
			+ "pd.account_id=a.id and date(p.voucher_date)=:fromDate " + "and p.company_mst=:companyMst and "
			+ "p.branch_code=:branchcode" + " and pd.mode_of_payment=:modeofpayment")
	List<Object> pettycashPayment(CompanyMst companyMst, Date fromDate, String branchcode, String modeofpayment);

	@Query(nativeQuery = true, value = "select h.id,h.voucher_date,h.voucher_number,h.voucher_type from payment_hdr h where h.company_mst=:companyMst and h.branch_code=:branchcode and date(h.voucher_date) between :fromDate and :toDate")

	List<Object> getPaymetHdr(CompanyMst companyMst, String branchcode, Date fromDate, Date toDate);

	@Query(nativeQuery = true, value = "select *  from payment_hdr where  voucher_date >= :date")
	List<PaymentHdr> paymentVoucherGreateAndEquel(Date date);

	void deleteByVoucherNumber(String voucherNumber);

	@Query(nativeQuery = true, value = "select * from payment_hdr where voucher_number=:invoiceno and company_mst=:companyMst "
			+ "and branch_code=:branchcode and date(voucher_date)=:date ")

	PaymentHdr findByVoucherNumberAndCompanyMstAndBranchCodeAndVoucherDate(String invoiceno, CompanyMst companyMst,
			String branchcode, Date date);
}
