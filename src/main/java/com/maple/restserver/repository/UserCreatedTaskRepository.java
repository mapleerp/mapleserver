package com.maple.restserver.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.maple.restserver.entity.UserCreatedTask;

@Repository
@Transactional
public interface UserCreatedTaskRepository extends JpaRepository<UserCreatedTask, String>{

	List<UserCreatedTask> findByUserIdAndCompanyMst(String companymstid, String userid);

}
