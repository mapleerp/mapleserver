package com.maple.restserver.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.MixCategoryMst;


@Repository
public interface SubCategoryMstRepository extends JpaRepository<MixCategoryMst, String> {

	

	
	@Query(nativeQuery = true,value="select   rc.mix_name,mr.machine_resource_name from mix_category_mst rc,machine_resource_mst mr where "
			+ " mr.id=mechine_id")
	List<Object>fetchResourceCategory();
	
	@Query(nativeQuery = true,value="select * from mix_category_mst")
	ArrayList<MixCategoryMst>fetchResourceSubCategory();
	

     @Query(nativeQuery = true,value="select m.capcity from machine_resource_mst m,mix_category_mst mc "
     		+ "where mc.mechine_id=m.id and mc.mix_name=:currentMix")
	Double findMachineCapacity(String currentMix);
     MixCategoryMst findByMixName(String mixName);

     @Query(nativeQuery = true,value = "select m.unit_id from machine_resource_mst m,mix_category_mst mc where "
     		+ "m.id=mc.mechine_id and mc.mix_name=:currentMix")
	String findMachineUnitId(String currentMix);
}
