package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DailySalesSummary;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.report.entity.ReceiptModeReport;

@Component
@Repository
public interface DailySalesSummaryRepository extends JpaRepository<DailySalesSummary, String> {

	DailySalesSummary findByRecordDateAndBranchCode(String branchCode, Date reportDate);

	@Query(nativeQuery = true, value =

	" select 1 as  Srl , 'Starting Bill No'  as Description , min(numeric_voucher_number) as amount  from sales_trans_hdr h where\n"
			+ "date(h.voucher_date)>=:vdate AND date(h.voucher_date)< :vdatePlusOne and h.branch_code=:branchCode Union  select  2  as  Srl , 'Ending Bill No'  as Description , max(numeric_voucher_number) as amount  from sales_trans_hdr h  where date(h. voucher_date)>=:vdate AND date(h.voucher_date)< :vdatePlusOne and h.branch_code=:branchCode "

			+ "  Union " +

			"  select   3 as  Srl , 'Total  Bill No'  as Description ,"
			+ " max(numeric_voucher_number) - min(numeric_voucher_number)+1  as amount"
			+ "  from sales_trans_hdr h where date(h.voucher_date)>=:vdate AND date(h.voucher_date)< :vdatePlusOne and h.branch_code=:branchCode "

			+ " Union " +

			"  select 4 as  Srl, 'Opening petty cash'  as Description , sum(debit_amount) - sum(credit_amount)  as amount \n"
			+ " from ledger_class where account_id =:cashaccountid and date(trans_date)<:vdate "

			+ "union" +

			"  select 5 as  Srl, 'Cash Received '  as Description , sum(debit_amount)    as amount \n"
			+ " from ledger_class where account_id =:cashaccountid and date(trans_date)>=:vdate AND date(trans_date)< :vdatePlusOne \n"

			+ " union   " +

			" select 6 as  Srl  , 'Cash Paid '  as Description , sum(credit_amount)   as amount  from ledger_class where account_id =:cashaccountid and date(trans_date)>=:vdate AND date(trans_date)< :vdatePlusOne"

			+ " union   " +

			"  select 7 as  Srl , 'Closing  Balance'  as Description , sum(debit_amount) - sum(credit_amount)  as amount \n"
			+ "from ledger_class where account_id =:cashaccountid and date(trans_date)>=:vdate AND date(trans_date)< :vdatePlusOne\n"

//			  + " union " +

			// " select 8 as Srl, 'Advance Received from Orders'as Description,null as
			// amount from company_mst\n"

			+ " union   " +

			"  select 9 as  Srl,'Cash SaleOrder '  as Description , " + " sum(sr.receipt_amount) as amount"
			+ " from sales_order_receipts sr , sales_order_trans_hdr th  " + " where sr.receipt_mode='CASH' and "
			+ " th.id=sr.sales_order_trans_hdr_id and "
			+ " date(th.voucher_date)>=:vdate AND date(th.voucher_date)< :vdatePlusOne  and th.branch_code=:branchCode  "

			+ " union   "
			+ "  select 10 as  Srl, 'Online Sales Orders'as Description , sum(mrp*qty) as amount from sales_dtl d, sales_trans_hdr h where h.id = d.sales_trans_hdr_id and "
			+ "date(h.voucher_date)>=:vdate AND date(h.voucher_date)< :vdatePlusOne and h.branch_code=:branchCode and h.sales_mode ='ONLINE'"

			+ " union   "
			+ "  select 11 as  Srl, 'Swiggy'as Description , sum(mrp*qty) as amount from sales_dtl d, sales_trans_hdr h where h.id = d.sales_trans_hdr_id and "
			+ "date(h.voucher_date)>=:vdate AND date(h.voucher_date)< :vdatePlusOne and h.branch_code=:branchCode and h.sales_mode ='SWIGGY'"

			+ " union   "
			+ "  select 12 as  Srl, 'Zomato'as Description , sum(mrp*qty) as amount from sales_dtl d, sales_trans_hdr h where h.id = d.sales_trans_hdr_id and "
			+ "date(h.voucher_date)>=:vdate AND date(h.voucher_date)< :vdatePlusOne and h.branch_code=:branchCode and h.sales_mode ='ZOMATO'"

			+ " union   "
			+ "  select 13 as  Srl, 'Uber Sales'as Description , sum(mrp*qty) as amount from sales_dtl d, sales_trans_hdr h where h.id = d.sales_trans_hdr_id and "
			+ "date(h.voucher_date)>=:vdate AND date(h.voucher_date) < :vdatePlusOne and h.branch_code=:branchCode and h.sales_mode ='UBER SALES'"

			+ " union   "
			+ "  select 14 as  Srl, 'Food panda'as Description , sum(mrp*qty) as amount from sales_dtl d, sales_trans_hdr h where h.id = d.sales_trans_hdr_id and "
			+ "date(h.voucher_date)>=:vdate AND date(h.voucher_date)< :vdatePlusOne and h.branch_code=:branchCode and h.sales_mode ='FOOD PANDA'"

			/*
			 * + " union   " +
			 * "  select 10  as  Srl,'Debit Card SaleOrder'as Description , sum(card_paid) as amount from  sales_receipts d , "
			 * + " sales_trans_hdr h where h.id = d.sales_trans_hdr_id and \n" +
			 * "h.voucher_date >=:vdate AND h.voucher_date < :vdatePlusOne and d.branch_code=:branchCode"
			 * + "\n" + " union\n" +
			 * "  select 10 , sum(card_paid) from  sales_receipts d , \n" +
			 * " sales_trans_hdr h where h.id = d.sales_trans_hdr_id and \n" +
			 * "h.voucher_date =:vdate and h.branch_code=:branchCode" + ""
			 */

			+ " union " +

			" select  15 as  Srl, c.customer_name as Description," + "sum(h.paid_amount+h.cardamount) as amount "
			+ "from sales_order_trans_hdr  h , customer_registration c where c.id=h.customer_id "
			+ " and h.branch_code=:branchCode and date(h.voucher_date)>=:vdate AND date(h.voucher_date)< :vdatePlusOne group by 15,c.customer_name "

			/*
			 * + " union "+
			 * 
			 * 
			 * "select 16 as  Srl, 'Advance Received from Oders'as Description, sum(receipt_amount) - sum(invoice_realized_amount) "
			 * + " from sales_order_receipts ar, sales_order_trans_hdr so" +
			 * 
			 * " where ar.sales_order_trans_hdr_id = so.id" +
			 * 
			 * "	and so.voucher_date >=:vdate AND so.voucher_date <:vdatePlusOne and so.branch_code=:branchCode "
			 */

			/*
			 * +"union"+
			 * "select 14 as Srl,'Credit Sales Details'as Description, amount as null from company_mst"
			 */

			+ " union " + " select 16 as Srl,'Credit' as Description, sum(r.receipt_amount) as amount "
			+ "from sales_receipts r,sales_trans_hdr h " + "where r.sales_trans_hdr_id=h.id "
			+ "and r.branch_code=:branchCode "
			+ " and date(h.voucher_date) >=:vdate AND date(h.voucher_date) <:vdatePlusOne and r.receipt_mode='CREDIT' "

	)
	List<Object> billSummaryReport(String branchCode, Date vdate, Date vdatePlusOne,
			String cashaccountid /* ,Date previousDay */);

	DailySalesSummary findByRecordDate(Date reportDate);

	DailySalesSummary findByRecordDateAndBranchCode(Date reportDate, String brancCode);

	@Query(nativeQuery = true, value = "select s.receipt_mode ,sum(receipt_amount) as amount , h.voucher_date  from sales_receipts s , sales_trans_hdr "
			+ "h where h.id=s.sales_trans_hdr_id and h.branch_code=:branchCode and date(h.voucher_date)=:date and h.company_mst=:companymstid and s.receipt_mode=:rMode group by s.receipt_mode,h.voucher_date ")
	List<Object> getPaymentModeWiseReport(String branchCode, Date date, CompanyMst companymstid, String rMode);

	@Query(nativeQuery = true, value = "select sum(s.receipt_amount) as amount  from sales_receipts s ,sales_trans_hdr "
			+ "h where h.id=s.sales_trans_hdr_id and h.branch_code=:branchCode and date(h.voucher_date)=:date and h.company_mst=:companymstid")
	Double getpaymentModeTotal(String branchCode, Date date, CompanyMst companymstid);

	@Query(nativeQuery = true, value = "select sum(h.receipt_amount) as amount,h.rereceipt_date from sale_order_receipt h where "
			+ "date(h.rereceipt_date)=:date and h.branch_code=:branchCode and h.company_mst=:companymstid group by h.rereceipt_date ")
	Double getAdvanceAmount(String branchCode, Date date, CompanyMst companymstid);

	List<DailySalesSummary> findByBranchCodeAndRecordDateAndCompanyMst(Date date, String branchCode,
			CompanyMst companyMst);

//List<Object> getCompanyDailySalesSumary(Date date,CompanyMst companyMst,String BranchCode);

	@Query(nativeQuery = true, value = "select ")
	List<Object> getSalesReport();

	@Query(nativeQuery = true, value = "select sum(invoice_amount) from sales_trans_hdr h  where h.customer_id=:customerid and date(h.voucher_date)=:vDate and h.branch_code=:branch and h.company_mst=:companymstid")
	Double getSumOfOnlineSale(String branch, Date vDate, String companymstid, String customerid);

	@Query(nativeQuery = true, value = "select sr.receipt_mode,sum(receipt_amount) from sales_receipts sr,"
			+ "sales_trans_hdr sh where sh.id = sr.sales_trans_hdr_id and date(sh.voucher_date) =:fdate "
			+ "and sh.branch_code=:branch and sh.company_mst = :companyMst and sh.voucher_number is not null   group by sr.receipt_mode")
	List<Object> getSalesReceipts(String branch, java.sql.Date fdate, String companyMst);

	@Query(nativeQuery = true, value = "select sr.receipt_mode,sum(receipt_amount) from sales_receipts sr,"
			+ " sales_trans_hdr sh where sh.id = sr.sales_trans_hdr_id and date(sh.voucher_date) =:fdate "
			+ " and sh.branch_code=:branch and sh.company_mst = :companyMst and receipt_mode = :mode and sh.voucher_number is not null  "
			+ " group by sr.receipt_mode")
	List<Object> getSalesReceiptsByMode(String branch, java.sql.Date fdate, String companyMst, String mode);

//	@Query(nativeQuery = true, value = "select c.customer_name,sum(sr.receipt_amount) "
//			+ "from sales_receipts sr,customer_mst c,"
//			+ " sales_trans_hdr sh where sh.id = sr.sales_trans_hdr_id and c.id = sr.account_id and "
//			+ "date(sh.voucher_date) =:fdate " + " and sh.branch_code=:branch and "
//			+ "sh.company_mst = :companyMst and sr.account_id =:accId " + "and sh.voucher_number is not null  "
//			+ " group by c.customer_name")
	
	@Query(nativeQuery = true, value = "select c.account_name,sum(sr.receipt_amount) "
			+ "from sales_receipts sr,account_heads c,"
			+ " sales_trans_hdr sh where sh.id = sr.sales_trans_hdr_id and c.id = sr.account_id and "
			+ "date(sh.voucher_date) =:fdate " + " and sh.branch_code=:branch and "
			+ "sh.company_mst = :companyMst and sr.account_id =:accId " + "and sh.voucher_number is not null  "
			+ " group by c.account_name")
	List<Object> getSalesReceiptsByModeByAccId(String branch, java.sql.Date fdate, String companyMst, String accId);

	@Query(nativeQuery = true, value = "select sr.receipt_mode,sum(receipt_amount) from sales_receipts sr,"
			+ "sales_trans_hdr sh where sh.id = sr.sales_trans_hdr_id and date(sh.voucher_date)=:fdate "
			+ " and sh.company_mst = :companyMst and sh.voucher_number is not null   group by sr.receipt_mode")
	List<Object> getSalesReceiptsWeb(Date fdate, String companyMst);

	@Query(nativeQuery = true, value = "select sr.RECEIPT_MODE,sum(RECEIPT_AMOUNT) from sale_order_receipt sr,"
			+ "sales_order_trans_hdr sh where sh.id = sr.SALE_ORDER_TRANS_HDR_ID  and date(sh.VOUCHER_DATE)=:date "
			+ " and sh.company_mst = :companymstid " + "and sh.ORDER_STATUS='Orderd' " + "group by sr.RECEIPT_MODE")
	List<Object> getSaleOrderReceiptsWeb(Date date, String companymstid);

	@Query(nativeQuery = true, value = "select sr.RECEIPT_MODE,sum(RECEIPT_AMOUNT) from sale_order_receipt sr,"
			+ "sales_order_trans_hdr sh where sh.id=sr.SALE_ORDER_TRANS_HDR_ID  and date(sh.VOUCHER_DATE)=:date "
			+ " and sh.company_mst =:companymstid " + "and sh.ORDER_STATUS='Orderd' and sr.RECEIPT_MODE=:mode "
			+ "group by sr.RECEIPT_MODE")
	List<Object> getSaleOrderReceiptsWebByMode(Date date, String companymstid, String mode);

	@Query(nativeQuery = true, value = "select sr.receipt_mode,sum(sr.receipt_amount) from sales_trans_hdr h ,sales_receipts sr where h.id=sr.sales_trans_hdr_id and h.branch_code=:branchcode and  date(h.voucher_date)=:date and h.company_mst=:companymstid group by sr.receipt_mode ")
	List<Object> receiptModeWiseReportSalesAndSoConverted(Date date, String companymstid, String branchcode);

	@Query(nativeQuery = true, value = "select sor.receipt_mode ,sum(receipt_amount)  from sales_order_trans_hdr so ,sale_order_receipt sor  "
			+ " where so.id=sor.sale_order_trans_hdr_id and date(so.voucher_date)=:date "
			+ "and so.company_mst=:companymstid and so.branch_code=:branchcode   group by sor.receipt_mode ")
	List<Object> soAdvanceReceivedToday(Date date, String companymstid, String branchcode);

	@Query(nativeQuery = true, value = "select d.mode_ofpayment ,sum(d.amount)  from receipt_hdr h,"
			+ " receipt_dtl d where h.id=d.receipt_hdr_id and date(h.voucher_date)=:date "
			+ "and h.company_mst=:companymstid and h.branch_code=:branchcode group by d.mode_ofpayment  ")
	List<Object> dayEndReceiptModewiseReport(Date date, String companymstid, String branchcode);

	@Query(nativeQuery = true, value = "select sum(d.amount) from receipt_hdr h ,"
			+ " receipt_dtl d where h.id=d.receipt_hdr_id and date(h.voucher_date)=:vdate "
			+ " and h.company_mst=:companymstid and h.branch_code=:branchcode and d.mode_ofpayment=:accountId ")
	Double dayEndPettyCashReceipt(Date vdate, String companymstid, String branchcode, String accountId);

	@Query(nativeQuery = true, value = "select sum(d.amount) from payment_hdr h ,payment_dtl d "
			+ " where h.id=d.paymenthdr_id and date(h.voucher_date)=:vdate and "
			+ " h.company_mst=:companymstid and h.branch_code=:branchcode and d.mode_of_payment=:accountId ")
	Double dayEndPettyCashPayment(Date vdate, String companymstid, String branchcode, String accountId);

	/*
	 * @Query(nativeQuery =
	 * true,value=" select sum(d.amount) from payment_hdr h , payment_dtl d " +
	 * " where h.id=d.paymenthdr_id and date(h.voucher_date)=date(:vdate) and " +
	 * " h.company_mst=:companymstid and h.branch_code=:branchcode and d.account_id=:accountId "
	 * ) Double dayendreportsummarybyaccountid(Date vdate,String companymstid,String
	 * branchcode,String accountId);
	 */

	@Query(nativeQuery = true, value = "select sum(d.amount) from payment_hdr h ,payment_dtl d "
			+ " where h.id=d.paymenthdr_id  and h.company_mst=:companymstid and h.branch_code=:branchcode "
			+ " and date(h.voucher_date)=:date and " + "  d.account_id=:accountId ")
	Double dayendreportsummarybyaccountid(Date date, String companymstid, String branchcode, String accountId);

	@Query(nativeQuery = true, value = "select sum(sor.receipt_amount) from sales_order_trans_hdr sh ,sale_order_receipts sor, sales_trans_hdr h where sh.id=sor.sale_order_trans_hdr_id and date(h.voucher_date)=:date and h.branch_code=:branchcode and h.company_mst=:companymstid ")
	Double dayEndPreviousAdvance(Date date, String companymstid, String branchcode);

	@Query(nativeQuery = true, value = "select sum(d.amount) from receipt_hdr h ,receipt_dtl d "
			+ " where h.id=d.receipt_hdr_id and date(h.voucher_date)=:date and h.company_mst=:companymstid "
			+ " and h.branch_code=:branchcode and d.account=:accountid")
	Double dayEndReceiptCash(Date date, String companymstid, String branchcode, String accountid);

	
	@Query(nativeQuery = true,value="select b.branch_name,sum(h.invoice_amount) as total from "
			+ " sales_trans_hdr h,branch_mst b where b.branch_code=h.branch_code and h.voucher_number is not null "
			+ "and date(voucher_date)=:date and h.company_mst=:companymstid group by  b.branch_name ")
	List<Object> getBranchWiseSalesSummary(Date date, String companymstid);

	@Query(nativeQuery = true,value="select d.receipt_mode ,sum(d.receipt_amount)  from sale_order_receipt d," + 
		" sales_order_trans_hdr so, sales_trans_hdr sh where so.id=d.sale_order_trans_hdr_id " + 
			" and so.company_mst=:companymstid and so.branch_code=:branchcode  "
			+ " and so.id=sh.sale_order_hrd_id and  date(sh.voucher_date)=:date and sh.voucher_number is not null "
			+ " and date(so.voucher_date)<:date"
			+ " group by d.receipt_mode  ")
	List<Object> dayEndReceptModeWisePreviousSalesReport(Date date, String companymstid, String branchcode);

	
	@Query(nativeQuery = true, value = "select sum(d.amount) from receipt_hdr h ,receipt_dtl d "
			+ " where h.id=d.receipt_hdr_id and date(h.voucher_date)=:date and h.company_mst=:companymstid "
			+ " and h.branch_code=:branchcode and d.mode_ofpayment=:receiptmode")
	Double dayEndReceiptCashByReceiptMode(Date date, String companymstid, String branchcode, String receiptmode);
}
