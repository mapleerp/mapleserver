package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.TaskApproveMst;

@Repository
public interface TaskApproveMstRepository extends JpaRepository<TaskApproveMst, String>{

	TaskApproveMst findByCompanyMstAndId(CompanyMst companyMst, String id);

	List<TaskApproveMst> findByCompanyMst(CompanyMst companyMst);

}
