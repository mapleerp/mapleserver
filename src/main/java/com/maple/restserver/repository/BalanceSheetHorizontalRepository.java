package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.BalanceSheetHorizontal;

@Repository
public interface BalanceSheetHorizontalRepository extends JpaRepository<BalanceSheetHorizontal,String>{

}
