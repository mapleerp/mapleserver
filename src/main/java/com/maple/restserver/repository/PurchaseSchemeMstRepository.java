package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.PurchaseSchemeMst;
@Component
@Repository
public interface PurchaseSchemeMstRepository extends JpaRepository<PurchaseSchemeMst,String>{

	@Query(nativeQuery = true,value = "select * from purchase_scheme_mst "
			+ "where supplier_id=:supid and item_id=:itemId and ((from_date<=:vdate and"
			+ " to_date >= :vdate) or (from_date is null and to_date is null)) ")
	List<PurchaseSchemeMst> findBySupplierIdAndItemIdWithDate(String supid,String itemId,Date vdate);


	@Modifying
	@Query(nativeQuery = true,value="update purchase_scheme_mst  set supplier_id =:toAccountId "
			+ "where supplier_id =:fromAccountId ")
    void accountMerge(String fromAccountId, String toAccountId);
}
