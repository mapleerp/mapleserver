package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.maple.restserver.entity.IntentInDtl;
import com.maple.restserver.entity.IntentInHdr;

public interface IntentInHdrRepository extends JpaRepository<IntentInHdr,String>{

	
	 List<IntentInHdr> findByIdAndIntentStatus(String id, String intentStatus);
	 
	 List<IntentInHdr> findByFromBranch(String fromBranch);
	 
	 @Query(nativeQuery = true,value="select i.item_name,indtl.qty,indtl.allocated_qty,u.unit_name,inhdr.from_branch,indtl.id,inhdr.in_voucher_date from intent_in_hdr inhdr,"
	 		+ "intent_in_dtl indtl"
	 		+ ",item_mst i, unit_mst u where indtl.item_id=i.id and indtl.unit_id=u.id and indtl.intent_in_hdr = inhdr.id and indtl.status='PENDING' and date(inhdr.in_voucher_date) between :fdate and :tdate")
	 List<Object> getPendingIntent(Date fdate,Date tdate);
	 
	 
	 @Query(nativeQuery = true,value="select i.item_name,indtl.qty,indtl.allocated_qty,u.unit_name,inhdr.from_branch,indtl.id,inhdr.in_voucher_date from intent_in_hdr inhdr,"
		 		+ "intent_in_dtl indtl"
		 		+ ",item_mst i, unit_mst u where indtl.item_id=i.id and indtl.unit_id=u.id and indtl.intent_in_hdr = inhdr.id and inhdr.from_branch=:branch and indtl.status='PENDING'")
		 List<Object> getPendingIntentByBranch(String branch);
	 
 
	 @Query("SELECT i FROM IntentInHdr i WHERE LOWER(i.fromBranch) like   LOWER(:fromBranch) AND i.intentStatus='PENDING'")

	    public List<IntentInHdr> findSearch(@Param("fromBranch") String fromBranch);
	 
	

	  @Query(nativeQuery = true,value="select h.in_voucher_number from intent_in_hdr h "
		  		+ "where date(h.in_voucher_date)=:vDate "
		  		+ " and  h.company_mst = :company_mst_id and h.in_voucher_number is not null  order by h.in_voucher_number")
		  List<Object>getIntentInVoucherNumber(String company_mst_id, Date vDate);

	  @Query(nativeQuery = true,value="select h.* from intent_in_hdr h"
	  		+ " where date(h.in_voucher_date)=:vdate and h.from_branch=:branch "
	  		+ "and h.intent_status='PENDING'") 
	  List<IntentInHdr> findByInVoucherDateAndFromBranch(Date vdate,String branch);
	  
	  List<IntentInHdr> findByInVoucherNumberAndInVoucherDate(String vouchernumber,Date vdate);

	/*
	 * String branchCode; String inVoucherNumber; String fromBranch; String
	 * intentStatus; Date inVoucherDate; Double Qty; String itemName; String
	 * unitName;
	 */
	  
	  
	  @Query(nativeQuery = true,value="select h.branch_code,"
	  		+ "h.in_voucher_number,h.from_branch,"
	  		+ "h.intent_status,h.in_voucher_date,d.qty,"
	  		+ "i.item_name,u.unit_name from intent_in_hdr h,"
	  		+ "intent_in_dtl d,item_mst i,unit_mst u where h.id=d.intent_in_hdr and i.id=d.item_id and "
	  		+ "u.id=d.unit_id and "
	  		+ "h.in_voucher_number=:voucherNumber and h.in_voucher_date=:date ")
	  List<Object> getIntentInReport(String voucherNumber,Date date);
	  
	@Query(nativeQuery=true,value="select * from intent_in_hdr h "
			+ "where lower(h.in_voucher_number) like   lower(:vouchernumber) "
			+ "AND h.intent_status='PENDING'")
	List<IntentInHdr> getPendingVoucherNumber(String vouchernumber);

}

