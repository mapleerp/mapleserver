package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SqlLogMst;

@Component
@Repository
public interface SqlLogMstRepository extends JpaRepository<SqlLogMst,String>{

}
