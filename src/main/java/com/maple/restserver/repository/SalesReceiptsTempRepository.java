package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SalesReceiptsTemp;

@Repository
public interface SalesReceiptsTempRepository extends JpaRepository<SalesReceiptsTemp, String>{

	

	@Modifying(flushAutomatically = true)
	 @Query(nativeQuery = true,value="INSERT INTO sales_receipts_temp (id, receipt_mode ,branch_code , user_id , account_id , receipt_amount , voucher_number, monthly_sales_trans_hdr_temp_id , company_mst ) "
	 		+ "SELECT TRIM(cast( row_number() over () as CHAR(15))) ,r.receipt_mode,r.branch_code,r.user_id,r.account_id,sum(r.receipt_amount),r.voucher_number,r.monthly_sales_trans_hdr_id, r.company_mst FROM monthly_sales_receipts r "
	 		+ "group by  r.receipt_mode,r.branch_code,r.user_id,r.account_id,r.voucher_number,r.monthly_sales_trans_hdr_id , r.company_mst  "
	 		)
	 void insertingIntoSalesReceiptsTemp ();
	
	
	
	
	@Modifying(flushAutomatically = true)
	 @Query(nativeQuery = true,value="INSERT INTO monthly_sales_receipts (id, receipt_mode ,branch_code , user_id , account_id , receipt_amount , voucher_number, monthly_sales_trans_hdr_id , company_mst ) "
	 		+ "SELECT TRIM(cast( row_number() over () as CHAR(15))) ,r.receipt_mode,r.branch_code,r.user_id,r.account_id,sum(r.receipt_amount),r.voucher_number,r.monthly_sales_trans_hdr_temp_id, r.company_mst FROM sales_receipts_temp r "
	 		+ "group by  r.receipt_mode,r.branch_code,r.user_id,r.account_id,r.voucher_number,r.monthly_sales_trans_hdr_temp_id , r.company_mst  "
	 		)
	 void insertingIntoMonthlySalesReceiptsFromSalesReceiptsTemp ();
	@Modifying
	@Query(nativeQuery = true,value = "delete from sales_receipts_temp")
	void deleteSalesReceiptsInitially();
}
