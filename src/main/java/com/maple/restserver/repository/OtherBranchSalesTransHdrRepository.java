package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.OtherBranchSalesTransHdr;
@Component
@Repository
public interface OtherBranchSalesTransHdrRepository extends JpaRepository<OtherBranchSalesTransHdr, String>{
	
	//-------------------------version 5.10 surya 

	
//	  @Query(nativeQuery = true,
//			  value="  select bm.branch_name, \n" + 
//				  		"bm.branch_address1, \n" + 
//				  		"bm.branch_address2,\n" + 
//				  		"bm.branch_Place, \n" + 
//				  		"bm.branch_Tel_No, \n" + 
//				  		"bm.branch_Gst salesTransHdrRepository, \n" + 
//				  		"bm.branch_state,\n" + 
//				  		"bm.branch_Email,\n" + 
//				  		"bm.bank_name, \n" + 
//				  		"bm.account_number, \n" + 
//				  		"bm.bank_branch, \n" + 
//				  		"bm.ifsc, \n" + 
//				  		"sh.voucher_date,\n" + 
//				  		"sh.voucher_number, \n" + 
//				  		"cm.customer_name,\n" + 
//				  		"cm.customer_place,\n" + 
//				  		"cm.customer_state,\n" + 
//				  		"cm.customer_gst,\n" + 
//				  		"im.item_name, \n" + 
//				  		"d.tax_rate, \n" + 
//				  		"im.hsn_code, \n" + 
//				  		"d.rate, d.qty,\n" + 
//				  		"u.unit_name, \n" + 
//				  		"d.discount as detail_discount , \n" + 
//				  		" sh.invoice_amount    ,  " + 
//				  		" "
//				  		+ "   d.sgst_amount    , \n" + 
//				  		"     d.cgst_amount     ,  \n" + 
//				  		"       d.igst_amount   ,  \n" + 
//				  		"   sgst_tax_rate , \n" + 
//				  		"   cgst_tax_rate   , "  + 
//				  		"   igst_tax_rate, \n " + 
//				  		"   d.cess_rate, \n " + 
//				  		"   d.cess_amount, \n " + 
//				  	 
//				  		" bm.branch_website,"
//				  		+ " cm.customer_contact, "
//				  		+ " rate * qty , "
//				  		+ "sh.discount , "
//				  		+ " sh.invoice_discount, sh.sales_mode, d.mrp,d.batch,d.standard_price "
//				  		+ "  from branch_mst bm, other_branch_sales_trans_hdr sh, other_branch_sales_dtl d, customer_mst cm, unit_mst u,\n" + 
//				  		"item_mst im  where sh.branch_sale_customer = bm.id "
//				  		+ " and sh.customer_mst = cm.id and sh.id = d.other_branch_sales_trans_hdr "
//				  		+ " and d.item_id = im.id "
//				  		+ "and d.unit_id = u.id  "
//				  		+ " and sh.voucher_number = :vnumber  "
//				  		+ " and date(sh.voucher_date) = :vdate "
//				  		+ " and sh.company_mst = :company_mst_id \n" 
//			  )
	
	 @Query(nativeQuery = true,
			  value="  select bm.branch_name, \n" + 
				  		"bm.branch_address1, \n" + 
				  		"bm.branch_address2,\n" + 
				  		"bm.branch_Place, \n" + 
				  		"bm.branch_Tel_No, \n" + 
				  		"bm.branch_Gst salesTransHdrRepository, \n" + 
				  		"bm.branch_state,\n" + 
				  		"bm.branch_Email,\n" + 
				  		"bm.bank_name, \n" + 
				  		"bm.account_number, \n" + 
				  		"bm.bank_branch, \n" + 
				  		"bm.ifsc, \n" + 
				  		"sh.voucher_date,\n" + 
				  		"sh.voucher_number, \n" + 
				  		"cm.account_name,\n" + 
				  		"cm.customer_place,\n" + 
				  		"cm.customer_state,\n" + 
				  		"cm.party_gst,\n" + 
				  		"im.item_name, \n" + 
				  		"d.tax_rate, \n" + 
				  		"im.hsn_code, \n" + 
				  		"d.rate, d.qty,\n" + 
				  		"u.unit_name, \n" + 
				  		"d.discount as detail_discount , \n" + 
				  		" sh.invoice_amount    ,  " + 
				  		" "
				  		+ "   d.sgst_amount    , \n" + 
				  		"     d.cgst_amount     ,  \n" + 
				  		"       d.igst_amount   ,  \n" + 
				  		"   sgst_tax_rate , \n" + 
				  		"   cgst_tax_rate   , "  + 
				  		"   igst_tax_rate, \n " + 
				  		"   d.cess_rate, \n " + 
				  		"   d.cess_amount, \n " + 
				  	 
				  		" bm.branch_website,"
				  		+ " cm.customer_contact, "
				  		+ " rate * qty , "
				  		+ "sh.discount , "
				  		+ " sh.invoice_discount, sh.sales_mode, d.mrp,d.batch,d.standard_price "
				  		+ "  from branch_mst bm, other_branch_sales_trans_hdr sh, other_branch_sales_dtl d, account_heads cm, unit_mst u,\n" + 
				  		"item_mst im  where sh.branch_sale_customer = bm.id "
				  		+ " and sh.account_heads = cm.id and sh.id = d.other_branch_sales_trans_hdr "
				  		+ " and d.item_id = im.id "
				  		+ "and d.unit_id = u.id  "
				  		+ " and sh.voucher_number = :vnumber  "
				  		+ " and date(sh.voucher_date) = :vdate "
				  		+ " and sh.company_mst = :company_mst_id \n" 
			  )
	List<Object> salesInvoice(String company_mst_id, String vnumber, Date vdate);

	@Query(nativeQuery = true,value="select ( sum(d.qty*d.rate) ) as ttaxableAmount "
			+ " from other_branch_sales_trans_hdr h,other_branch_sales_dtl d where h.id=d.other_branch_sales_trans_hdr "
			+ "and h.voucher_number=:voucherNumber and date(h.voucher_date) =:date and h.company_mst=:companymstid ")
	Double getTaxableAmount(String voucherNumber, Date date, String companymstid);

	
	@Query(nativeQuery = true,value="select d.tax_rate , "
	  		+ "  (sum(d.rate*d.qty*d.tax_rate/100)   ),  "
	  		+ "   (SUM(d.sgst_amount)   )  as sgst_amount , \n" + 
	  		"     (SUM( d.cgst_amount )   )  as cgst_amount ,  \n" + 
	  		"   (SUM( d.igst_amount )  )  as igst_amount ,  \n" + 
	  		"   sgst_tax_rate , \n" + 
	  		"   cgst_tax_rate \n , "  + 
	  		"   igst_tax_rate \n ,"
	  	 
	  		+ " sum(d.rate * d. qty) " 
	  		+ "from other_branch_sales_dtl d,other_branch_sales_trans_hdr h"
		  		+ " where d.other_branch_sales_trans_hdr=h.id and h.voucher_number=:voucherNumber "
		  		+ "and date(h.voucher_date) =:date and h.company_mst=:companymstid"
		  		+ " group by d.tax_rate , "
		  		+ "   sgst_tax_rate , \n" + 
		  		"   cgst_tax_rate \n , "  + 
		  		"   igst_tax_rate \n "
		  		+ "   ")
	List<Object> getTaxSummary(String companymstid, String voucherNumber, Date date);

	@Query(nativeQuery = true,value="select (SUM(d.sgst_amount))  as sgst_amount ,(SUM( d.cgst_amount )) "
			+ " as cgst_amount , (SUM( d.igst_amount ))  as igst_amount from other_branch_sales_trans_hdr h,"
			+ "other_branch_sales_dtl d where h.id=d.other_branch_sales_trans_hdr "
			+ "and h.voucher_number=:voucherNumber and date(h.voucher_date) =:date and h.company_mst=:companymstid ")
	List<Object> getSumOfTaxAmounts(String companymstid, String voucherNumber, Date date);

	
	@Query(nativeQuery = true,value="select cm.localcustomer_name,"
			+ "cm.address,"
			+ "cm.phone_no,"
			+ "cm.address_line1,cm.address_line2,cm.land_mark "
			+ "from local_customer_mst cm,other_branch_sales_trans_hdr h "
			+ "where h.local_customer_mst=cm.id and date(h.voucher_date ) =:vdate "
			+ "and h.voucher_number=:vnumber and h.company_mst=:company_mst_id ")
	List<Object> getTaxInvoiceCustomer(String company_mst_id, String vnumber, Date vdate);

	
//	@Query(nativeQuery = true,value="select cm.customer_name,"
//			+ "cm.customer_address,cm.customer_state,cm.customer_contact,cm.customer_gst "
//			+ "from customer_mst cm,other_branch_sales_trans_hdr h where h.customer_mst=cm.id "
//			+ "and date(h.voucher_date) =:vdate and h.voucher_number=:vnumber and h.company_mst=:company_mst_id ")
	
	@Query(nativeQuery = true,value="select cm.account_name,"
			+ "cm.party_address1,cm.customer_state,cm.customer_contact,cm.party_gst "
			+ "from account_heads cm,other_branch_sales_trans_hdr h where h.account_heads=cm.id "
			+ "and date(h.voucher_date) =:vdate and h.voucher_number=:vnumber and h.company_mst=:company_mst_id ")
	List<Object> getTaxInvoiceGstCustomer(String company_mst_id, String vnumber, Date vdate);

	@Query(nativeQuery = true,value="select sum(d.cess_amount) as cessamount "
			+ "from other_branch_sales_trans_hdr h,other_branch_sales_dtl d where h.id=d.other_branch_sales_trans_hdr and "
			+ "h.voucher_number=:vouchernumber and date(h.voucher_date) =:date ")
	Double getCessAmount(String vouchernumber, java.util.Date date);

	
	@Query(nativeQuery = true,value="select sum(d.qty*d.rate) from other_branch_sales_trans_hdr h,other_branch_sales_dtl d "
			+ "where h.id=d.other_branch_sales_trans_hdr and d.tax_rate>0 and h.voucher_number=:vouchernumber and date( h.voucher_date) =:date")
	Double getNonTaxableAmount(String vouchernumber, java.util.Date date);

	
	
	
	@Query(nativeQuery = true,value="select * from other_branch_sales_trans_hdr"
			+ " where voucher_number = :vouchernumber "
			+ "and date(voucher_date) =  :date and  "
			+ " company_mst = :companymstid ")
	OtherBranchSalesTransHdr findByVoucherNumberAndVoucherDateAndCompanyMstId(String vouchernumber, java.util.Date date,
			String companymstid);

	
	//-------------------------version 5.10 surya end

	//List<OtherBranchSalesTransHdr> findByVoucherNumberAndVoucherDate(String vno,java.util.Date vDate);
/*
 * modified by Regy on March 6th 2021. After deployment in WBH failed o  other branch sales
 */
	
	@Query(nativeQuery = true,value="select * from other_branch_sales_trans_hdr where date(voucher_date)= :vDate "
			+ "and voucher_number = :vno ")
	List<OtherBranchSalesTransHdr> findByVoucherNumberAndVoucherDate(String vno, Date vDate);

	
	
	
	@Modifying
	@Query(nativeQuery = true,value="update other_branch_sales_trans_hdr  set customer_mst =:toAccountId where customer_mst =:fromAccountId ")
	void accountMerge(String fromAccountId, String toAccountId);

	@Query(nativeQuery = true,value="select * from other_branch_sales_trans_hdr where date(voucher_date)= :date "
			+ "and company_mst = :companyMst ")
	List<OtherBranchSalesTransHdr> findByVoucherDateAndCompanyMst(java.util.Date date, CompanyMst companyMst);

	OtherBranchSalesTransHdr findBySalesTransHdrIdAndCompanyMst(String hdrid, CompanyMst companyMst);

	OtherBranchSalesTransHdr findByIdAndCompanyMst(String id, CompanyMst companyMst);

	@Query(nativeQuery = true,value="select * from other_branch_sales_trans_hdr where date(voucher_date)>=:fuDate "
			+ "and date(voucher_date) >= :tudate")
	List<OtherBranchSalesTransHdr> findBetweenVoucherDate(java.util.Date fuDate, java.util.Date tudate);


	 @Query(nativeQuery=true,value="select * from other_branch_sales_trans_hdr h where "
	 		+ " lower(h.voucher_number) like   lower(:vouchernumber) ")
	List<OtherBranchSalesTransHdr> getOtherBranchVoucherNumber(String vouchernumber);



}
