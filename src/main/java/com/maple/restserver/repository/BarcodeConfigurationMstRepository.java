package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.BarcodeConfigurationMst;
@Repository
public interface BarcodeConfigurationMstRepository extends JpaRepository<BarcodeConfigurationMst,String>{

	
	BarcodeConfigurationMst findByBranchCode(String barcode);
@Query(nativeQuery = true,value="select *from barcode_configuration_mst where branch_code=:branchcode")
	BarcodeConfigurationMst fetchBarcodeConfigMst(String branchcode);
}
