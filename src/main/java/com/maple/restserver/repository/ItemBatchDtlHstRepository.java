package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ItemBatchDtlHst;

@Repository
public interface ItemBatchDtlHstRepository extends JpaRepository<ItemBatchDtlHst, String> {

	@Modifying(flushAutomatically = true)
	 @Query(nativeQuery = true,value="INSERT INTO item_batch_dtl_hst(id, item_id,batch,barcode,qty_in,qty_out,mrp,voucher_number,voucher_date,source_voucher_number,source_voucher_date,exp_date,branch_code,particulars,source_parent_id,source_dtl_id,store,company_mst) "
	 		+ "SELECT id ,item_id,batch,barcode,qty_in,qty_out,mrp,voucher_number,voucher_date,source_voucher_number,source_voucher_date,exp_date,branch_code,particulars,source_parent_id,source_dtl_id,store,company_mst FROM item_batch_dtl")
	 void insertingItemBatchDtlToItemBatchDtlHst ();
	
	@Modifying(flushAutomatically = true)
	 @Query(nativeQuery = true,value="TRUNCATE TABLE item_batch_dtl")
	void truncateItemBatchDtl();
	/*
	 * @Modifying(flushAutomatically = true)
	 * 
	 * @Query(nativeQuery = true,value
	 * ="INSERT INTO item_batch_dtl (id, item_id,batch,barcode,qty_in,qty_out,mrp,exp_date,branch_code,particulars,source_parent_id,source_dtl_id,store,company_mst) "
	 * +
	 * " SELECT id, item_id,batch,barcode,qty_in,qty_out,mrp,exp_date,branch_code,particulars,source_parent_id,source_dtl_id,store,company_mst FROM item_batch_dtl_hst "
	 * ) void InsertingInToItemBatchDtlFromItemBatchDtlHistory();
	 */
}
