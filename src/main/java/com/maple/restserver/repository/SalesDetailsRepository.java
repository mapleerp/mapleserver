package com.maple.restserver.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;

@Repository
public interface SalesDetailsRepository extends JpaRepository<SalesDtl, String> {

	@Query(nativeQuery = true, value = "SELECT   " + " SUM(d.cess_amount)  as totalAmount "
			+ " FROM Sales_Dtl d, Sales_Trans_Hdr h "
			+ " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId and d.item_id = :itemID AND d.id = :dtlID  ")
	Double findSumCessAmountItemWise(String salesHdrId, String itemID, String dtlID);

	@Query(nativeQuery = true, value = "SELECT SUM( (rate* qty) * (tax_rate)/100 )    as totalQty "
			+ " FROM Sales_Dtl d, Sales_Trans_Hdr h "
			+ " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId AND d.item_id =:itemId  AND d.id = :dtlID ")
	Double findTotalTaxSumSalesDtlItemWise(String salesHdrId, String itemId, String dtlID);

	List<SalesDtl> findBySalesTransHdrIdOrderByUpdatedTimeDesc(String salesTransId, Pageable pageable);

	Optional<SalesDtl> findByIdAndId(String Id, String salesTransId);

	// List<SalesDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String
	// voucherNumber, Date voucherDate,String voucherType);
	List<SalesDtl> findBySalesTransHdrId(String salesTransId);

	@Query(nativeQuery = true, value = "SELECT   " + " SUM((rate * qty))  as totalAmount "
			+ " FROM Sales_Dtl d, Sales_Trans_Hdr h " + " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId AND"
			+ " d.item_id = :itemId AND d.id = :dtlID ")
	Double findSumAcessibleAmountItemWise(String salesHdrId, String itemId, String dtlID);

	@Query(nativeQuery = true, value = "SELECT SUM(qty) as totalQty, "
			+ " SUM((rate*qty) + (  (rate*qty) * (tax_rate)/100 ) + (rate*qty) * cess_Rate/100)  as totalAmount, "
			+ " SUM(((rate*qty) * ((tax_rate)/100 ))) as totalTax,  "
			+ " SUM( (rate*qty) * cess_Rate/100) as totalCessAmt  " + " FROM Sales_Dtl d, Sales_Trans_Hdr h "

			+ " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId")
	List<Object> findSumSalesDtl(String salesHdrId);
	
	
	@Query(nativeQuery = true, value = "SELECT SUM(qty) as totalQty, "
			+ " SUM((fc_rate*qty) + (  (fc_rate*qty) * (fc_tax_rate)/100 ) + (fc_rate*qty) * fc_cess_Rate/100)  as totalAmount, "
			+ " SUM(((fc_rate*qty) * ((fc_tax_rate)/100 ))) as totalTax,  "
			+ " SUM( (fc_rate*qty) * fc_cess_Rate/100) as totalCessAmt  " + " FROM Sales_Dtl d, Sales_Trans_Hdr h "

			+ " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId")
	List<Object> findFCSumSalesDtl(String salesHdrId);

	@Query(nativeQuery = true, value = "SELECT SUM(qty) as totalQty, "
			+ " SUM( (rate*qty) + (  (rate*qty) * (tax_rate)/100 ))  as totalAmount, "
			+ " SUM(((rate*qty) * ((tax_rate)/100 ))) as totalTax,  "
			+ " SUM( (rate*qty) * cess_Rate/100) as totalCessAmt  " + " FROM Sales_Dtl d, Sales_Trans_Hdr h "

			+ " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId")
	List<Object> findSumSalesDtlforDiscount(String salesHdrId);
	
	
	@Query(nativeQuery = true, value = "SELECT SUM(qty) as totalQty, "
			+ " SUM( (rate*qty) + (  (rate*qty) * (tax_rate)/100 )+((rate*qty)*(cess_rate)/100 )) as totalAmount, "
			+ " SUM(((rate*qty) * ((tax_rate)/100 ))) as totalTax,  "
			+ " SUM( (rate*qty) * cess_Rate/100) as totalCessAmt  " + " FROM Sales_Dtl d, Sales_Trans_Hdr h "

			+ " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId")
	List<Object> findSumSalesDtlforB2CDiscount(String salesHdrId);
	
	@Query(nativeQuery = true, value = "SELECT SUM(qty) as totalQty, "
			+ " SUM( (rate*qty) + (  (rate*qty) * (tax_rate)/100 ))  as totalAmount, "
			+ " SUM(((rate*qty) * ((tax_rate)/100 ))) as totalTax,  "
			+ " SUM( (rate*qty) * cess_Rate/100) as totalCessAmt  " + " FROM Sales_Dtl d, Sales_Trans_Hdr h "

			+ " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId")
	List<Object> findSumSalesDtlforDiscountForB2C(String salesHdrId);

	
	@Query(nativeQuery = true, value = "SELECT SUM(qty) as totalQty, "
			+ " SUM( (fc_rate*qty) + (  (fc_rate*qty) * (fc_tax_rate)/100 ))  as totalAmount, "
			+ " SUM(((fc_rate*qty) * ((fc_tax_rate)/100 ))) as totalTax,  "
			+ " SUM( (fc_rate*qty) * fc_cess_Rate/100) as totalCessAmt  " + " FROM Sales_Dtl d, Sales_Trans_Hdr h "

			+ " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId")
	List<Object> findFCSumSalesDtlforDiscount(String salesHdrId);
	@Query(nativeQuery = true, value = "SELECT   "
			+ " SUM(rate*qty) + sum(CGST_AMOUNT +SGST_AMOUNT +  CESS_AMOUNT) as totalAmount "
			+ " FROM Sales_Dtl d, Sales_Trans_Hdr h " + " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId")
	BigDecimal findSumCrAmountSalesDtl(@Param("salesHdrId") String salesHdrId);
	
	
	
	
	

	@Query(nativeQuery = true, value = "SELECT   " + " SUM((rate * qty))  as totalAmount "
			+ " FROM Sales_Dtl d, Sales_Trans_Hdr h " + " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId")
	BigDecimal findSumAcessibleAmountSalesDtl(String salesHdrId);

	/*
	 * Assessible Amount with Tax Rate > 5 %
	 */
	@Query(nativeQuery = true, value = "SELECT   " + " SUM(d.cess_amount)  as totalAmount "
			+ " FROM Sales_Dtl d, Sales_Trans_Hdr h " + " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId  ")
	BigDecimal findSumCessAmount(String salesHdrId);

	/*
	 * Group by Tax Rate
	 */
	@Query(nativeQuery = true, value = " " + " SELECT SUM((rate*qty) * (tax_rate)/100 )) as totalQty, tax_rate "
			+ " FROM Sales_Dtl d, Sales_Trans_Hdr h "
			+ " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId group by tax_rate ")
	List<Object> findTaxwiseSumSalesDtl(@Param("salesHdrId") String salesHdrId);

	/*
	 * Summary of Tax
	 */
	@Query(nativeQuery = true, value = " SELECT  SUM((rate* qty) * (tax_rate))/100       as totalQty "
			+ " FROM Sales_Dtl d, Sales_Trans_Hdr h " + " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId  ")
	BigDecimal findTotalTaxSumSalesDtl(String salesHdrId);

	@Query(nativeQuery = true, value = "SELECT   " + " SUM(receipt_amount ) as totalAmount "
			+ " FROM sales_receipts d, Sales_Trans_Hdr h "
			+ " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId")
	BigDecimal findSumDrAmountSalesDtl(@Param("salesHdrId") String salesHdrId);

	@Query(nativeQuery = true, value = "select sum(d.qty) from sales_dtl d, sales_trans_hdr h where d.sales_trans_hdr_id = h.id and d.item_id =:itemId and h.id=:hdrId")
	Double getSalesDetailItemQty(String hdrId, String itemId);

	@Query(nativeQuery = true, value = "select sum(d.qty) from sales_dtl d, "
			+ "sales_trans_hdr h where d.sales_trans_hdr_id = h.id and d.item_id =:itemId and h.id=:hdrId AND"
			+ " d.batch = :batch")
	Double getSalesDetailItemQtyByBatch(String hdrId, String itemId, String batch);

	@Query(nativeQuery = true, value = "select distinct d.tax_rate from sales_dtl d where d.sales_trans_hdr_id=:hdrId")
	List<Object> getTaxRate(String hdrId);

	@Query(nativeQuery = true, value = "select distinct d.tax_rate from sales_dtl d where d.sales_trans_hdr_id=:hdrId and d.item_id=:itemId")
	List<Double> getItemWiseTaxRate(String hdrId, String itemId);

	@Query(nativeQuery = true, value = "select sum(rate*qty*tax_rate/100) from sales_dtl d "
			+ " where d.sales_trans_hdr_id=:hdrId and tax_rate=:taxRate ")
	BigDecimal retrieveTaxAmount(String hdrId, Double taxRate);

	@Query(nativeQuery = true, value = "select sum(rate*qty*tax_rate/100) "
			+ " from sales_dtl d where d.sales_trans_hdr_id=:hdrId  ")
	BigDecimal retrieveAllTaxAmount(String hdrId);

	@Query(nativeQuery = true, value = "select sum(rate*qty*cess_rate/100) "
			+ " from sales_dtl d where d.sales_trans_hdr_id=:hdrId  ")
	BigDecimal retrieveAllCessAmount(String hdrId);

	@Query(nativeQuery = true, value = "select sum(mrp*qty ) from sales_dtl d where d.sales_trans_hdr_id=:hdrId ")
	BigDecimal retrieveGrandTotalincludingTax(String hdrId);
	
//	@Query(nativeQuery = true, value = "select sum((rate*qty)+(rate*qty*tax_rate)/100) from sales_dtl d where d.sales_trans_hdr_id=:hdrId ")
//	BigDecimal retrieveGrandTotalincludingTaxForPOsDiscount(String hdrId);

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr  where id =:hdrId")

	List<Object> getsalesHdrDtl(String hdrId);

	@Query(nativeQuery = true, value = "select " + "  im.hsn_code, " + " im.item_code, " + " dtl.tax_rate,"
			+ " dtl.qty , " + " dtl.rate ,dtl.item_name, " + "( dtl.qty * (dtl.rate + 0.005 ) ) as amount , " +

			"   dtl.expiry_date "

			+ "from sales_dtl  dtl, " + "item_mst im "

			+ " where dtl.sales_trans_hdr_id=:salesTranshdrId and " + " dtl.item_id = im.id "

			+ "  ")

	List<Object> getItemDetails(String salesTranshdrId);
	
//	@Query(nativeQuery = true, value = "select " + "  im.hsn_code, " + " im.item_code, " + " dtl.tax_rate,"
//			+ " dtl.qty , " + " im.standard_price ,dtl.item_name, " + "( dtl.qty * (dtl.rate + 0.005 ) ) as amount , " +
//
//			"   dtl.expiry_date "
//
//			+ "from sales_dtl  dtl, " + "item_mst im "
//
//			+ " where dtl.sales_trans_hdr_id=:salesTranshdrId and " + " dtl.item_id = im.id "
//
//			+ "  ")
//
//	List<Object> getItemDetailswithMrp(String salesTranshdrId);

	@Query(nativeQuery = true, value = "select " + "  im.hsn_code, " + " im.item_code, " + " dtl.tax_rate,"
			+ " dtl.qty , " + " dtl.rate ,dtl.item_name, " + "( dtl.qty * (dtl.rate + 0.005 ) ) as amount , "
			+ "   dtl.expiry_date,p.printer_name  " + " from sales_dtl  dtl, " + "item_mst im," + "kot_category_mst k,"
			+ "printer_mst p  " + "where dtl.sales_trans_hdr_id=:salesTranshdrId " + "and  dtl.item_id = im.id "
			+ "and im.category_id=k.category_id " + "and k.printer_id=p.id " + "and p.id=:printerId" + "  ")

	List<Object> getItemDetailsForPrint(String salesTranshdrId, String printerId);

//	@Query(nativeQuery = true, value = "select " + "  im.hsn_code, " + " im.item_code, " + " dtl.tax_rate,"
//			+ " dtl.qty , " + " dtl.rate ,dtl.item_name, " + "( dtl.qty * (dtl.rate + 0.005 ) ) as amount , "
//			+ "   dtl.expiry_date,p.printer_name  " + " from sales_dtl  dtl, " + "item_mst im," + "kot_category_mst k,"
//			+ "printer_mst p,kot_item_mst ki  " + "where dtl.sales_trans_hdr_id=:salesTranshdrId " + "and  dtl.item_id = im.id "
//			+ "and im.category_id=k.category_id " + "and k.printer_id=p.id "
//			+ "and p.id=:printerId and ki.item_id=im.id and (dtl.print_kot_status='N' or dtl.print_kot_status is null)" + "  ")
//
//	List<Object> getItemDetailsForPrintWithStatus(String salesTranshdrId, String printerId);
	@Query(nativeQuery = true, value = "select " + "  im.hsn_code, " + " im.item_code, " + " dtl.tax_rate,"
			+ " dtl.qty , " + " dtl.rate ,dtl.item_name, " + "( dtl.qty * (dtl.rate + 0.005 ) ) as amount , "
			+ "   dtl.expiry_date " + " from sales_dtl  dtl, " + "item_mst im "
			+ "  where dtl.sales_trans_hdr_id=:salesTranshdrId " + "and  dtl.item_id = im.id ")

	
	List<Object> getItemDetailsForPrintWithStatus(String salesTranshdrId);

	@Query(nativeQuery = true, value = "select " + "  im.hsn_code, " + " im.item_code, " + " dtl.tax_rate,"
			+ " dtl.qty , " + " dtl.rate ,dtl.item_name, " + "( dtl.qty * (dtl.rate + 0.005 ) ) as amount , "
			+ "   dtl.expiry_date,p.printer_name  " + " from sales_dtl  dtl, " + "item_mst im," + "kot_category_mst k,"
			+ "printer_mst p,kot_item_mst ki  " + "where dtl.sales_trans_hdr_id=:salesTranshdrId " + "and  dtl.item_id = im.id "
			+ "and im.category_id=k.category_id " + "and k.printer_id=p.id "
			+ "and p.id=:printerId and ki.item_id=im.id   ")

	List<Object> getItemDetailsForPrintPerforma(String salesTranshdrId, String printerId);
	
// @Query(nativeQuery = true,value= "select dtl.* from sales_dtl dtl where "
// 		+ "dtl.sales_trans_hdr_id=:salesTransHdrId and dtl.item_id =:ItemId "
// 		+ "and dtl.batch=:Batch and dtl.barcode=:Barcode and dtl.unit_id=:UnitId and dtl.scheme_id is null")
// SalesDtl findBySalesTransHdrIdAndItemIdAndBatchAndBarcodeAndUnitId(String salesTransHdrId,String ItemId, String Batch, String Barcode,String UnitId );

	List<SalesDtl> findBySalesTransHdrIdAndItemId(String salesTransHdrID, String itemId);

	List<SalesDtl> findBySalesTransHdrIdAndItemIdAndBatch(String salesTransHdrID, String itemId, String batch);

	List<SalesDtl> findBySalesTransHdrAndItemId(SalesTransHdr salesTransHdr, String itemid);

	Optional<SalesDtl> findBySalesTransHdrAndItemIdAndQty(SalesTransHdr salesTransHdr, String itemid, Double qty);

	Optional<SalesDtl> findByFbKey(String fbKey);

	@Query(nativeQuery = true, value = "select " + " dtl.item_id," + " dtl.qty, " + " dtl.item_name,"
			+ " dtl.unit_id, dtl.id," + "dtl.returned_qty,  " + "dtl.offer_reference_id," + "dtl.amount "
			+ "  from sales_dtl  dtl, "
			+ "item_mst im  where dtl.sales_trans_hdr_id=:salesTransHdrId and dtl.item_id = im.id "
			+ "and im.category_id=:categoryid and scheme_id is null")
	List<Object> findBySalesTransHdrAndCategoryIdd(String salesTransHdrId, String categoryid);

	List<SalesDtl> findByItemIdAndBatchAndSalesTransHdr(String itemid, String batch, SalesTransHdr salesTransHdr);

	List<SalesDtl> findByIdAndSalesTransHdr(String offerreferenceid, SalesTransHdr salesTransHdr);

	SalesDtl findBySalesTransHdrIdAndItemIdAndBatchAndBarcodeAndUnitIdAndRate(String id, String itemId, String batch,
			String barcode, String unitId, Double rate);

	List<SalesDtl> findBySalesTransHdrAndItemIdAndBatch(SalesTransHdr salesTransHdr, String itemId, String batch);

	@Query(nativeQuery = true, value = "select i.hsn_code,sum(d.qty),sum(d.cgst_amount),"
			+ "sum(d.sgst_amount),sum(amount),h.voucher_date from  item_mst i,sales_trans_hdr h,sales_dtl d where h.id=d.sales_trans_hdr_id and d.item_id =i.id and h.branch_code=:branchCode"
			+ " and h.voucher_date between :fdate and :tdate and h.company_mst=:companyMst group by i.hsn_code,h.voucher_date")
	List<Object> getHSNCodeWiseSalesDtl(java.util.Date fdate, java.util.Date tdate, CompanyMst companyMst,
			String branchCode);

	@Query(nativeQuery = true, value = "select count(*) from sales_dtl where sales_trans_hdr_id = :hdrid")
	Integer countOfSalesDtl(String hdrid);

	@Modifying
	@Query(nativeQuery = true, value = "update sales_dtl set print_kot_status='1' where sales_trans_hdr_id=:hdrid"
			+ " and (print_kot_status='N' or print_kot_status is null)")
	void updatePrintStatus(String hdrid);
	
	@Modifying
	@Query(nativeQuery = true, value = "update sales_dtl set print_kot_status='1' where sales_trans_hdr_id=:hdrid")
	void updateFinalPrintStatus(String hdrid);
	
	@Modifying
	@Query(nativeQuery = true, value = "update sales_dtl set print_kot_status='1' where sales_trans_hdr_id=:hdrid and id in (:salesDtlIds)")
	void updateFinalPrintStatus(String hdrid,List<String> salesDtlIds);


	@Modifying
	@Query(nativeQuery = true, value = "update sales_dtl set barcode =:barCode where item_id=:itemid")
	void updateBarcode(String itemid, String barCode);

	@Query(nativeQuery = true, value = "select sum(amount) from sales_dtl d, sales_trans_hdr h, item_temp_mst i where"
			+ " h.id=d.sales_trans_hdr_id and  date(h.voucher_date)=:date and h.voucher_number is not null and "
			+ "d.item_id=i.item_id and h.branch_code=:branchCode and h.company_mst=:companyid")
	Double getCard2SalesSummary(String branchCode, Date date, String companyid);

	//----------------------version 6.12 surya 

	@Modifying
	@Query(nativeQuery = true, value = "update sales_dtl set cost_price =:amount where item_id=:itemId")
	void updateCostPrice(Double amount,String itemId);

	List<SalesDtl> findByCompanyMst(CompanyMst companyMst);
	
	  
    @Query(nativeQuery = true,value= "select i.item_name,sum(d.qty),sum(d.amount),h.voucher_date,h.voucher_number "
    		+ "from item_mst i,sales_trans_hdr h,sales_dtl d where i.id=:itemid and d.item_id =i.id and "
    		+ " d.sales_trans_hdr_id=h.id and "
    		+ "h.branch_code=:branchcode and h.voucher_number is not null and h.company_mst=:companymstid "
    		+ "group by i.item_name,h.voucher_date,h.voucher_number  ORDER BY h.voucher_date DESC ")

	List<Object> getItemStockReport(String companymstid, String branchcode, String itemid);

	List<SalesDtl> findBySalesTransHdr(SalesTransHdr salesTransHdr);

	
//	@Query(nativeQuery = true,value = "select h.voucher_number,"
//			+ "h.voucher_date,i.item_name,d.igst_amount,d.qty,"
//			+ "d.rate,d.amount,c.customer_name,d.batch from "
//			+ "sales_trans_hdr h,sales_dtl d,item_mst i,customer_mst c where "
//			+ "h.id=d.sales_trans_hdr_id and c.id=h.customer_mst and "
//			+ "i.id=d.item_id and "
//			+ "date(h.voucher_date)>=:fudate and date(h.voucher_date)<=:tudate and "
//			+ "h.company_mst=:companyMst and i.category_id=:id and c.customer_name=:customername "
//			+ "and h.branch_code=:branchCode")
	
	@Query(nativeQuery = true,value = "select h.voucher_number,"
			+ "h.voucher_date,i.item_name,d.igst_amount,d.qty,"
			+ "d.rate,d.amount,c.account_name,d.batch from "
			+ "sales_trans_hdr h,sales_dtl d,item_mst i,account_heads c where "
			+ "h.id=d.sales_trans_hdr_id and c.id=h.account_heads and "
			+ "i.id=d.item_id and "
			+ "date(h.voucher_date)>=:fudate and date(h.voucher_date)<=:tudate and "
			+ "h.company_mst=:companyMst and i.category_id=:id and c.account_name=:customername "
			+ "and h.branch_code=:branchCode")
	List<Object> getCustomerwiseSalesByCategoryAndDate(Date fudate, Date tudate, CompanyMst companyMst, String id,
			String customername,String branchCode);

	
//	@Query(nativeQuery = true,value = "select h.voucher_number,"
//			+ "h.voucher_date,i.item_name,d.igst_amount,d.qty,"
//			+ "d.rate,d.amount,c.customer_name,d.batch from "
//			+ "sales_trans_hdr h,sales_dtl d,item_mst i,customer_mst c where "
//			+ "h.id=d.sales_trans_hdr_id and c.id=h.customer_mst and "
//			+ "i.id=d.item_id and "
//			+ "date(h.voucher_date)>=:fudate and date(h.voucher_date)<=:tudate and "
//			+ "h.company_mst=:companyMst and c.customer_name=:customername "
//			+ "and h.branch_code=:branchcode")
	
	@Query(nativeQuery = true,value = "select h.voucher_number,"
			+ "h.voucher_date,i.item_name,d.igst_amount,d.qty,"
			+ "d.rate,d.amount,c.account_name,d.batch from "
			+ "sales_trans_hdr h,sales_dtl d,item_mst i,account_heads c where "
			+ "h.id=d.sales_trans_hdr_id and c.id=h.account_heads and "
			+ "i.id=d.item_id and "
			+ "date(h.voucher_date)>=:fudate and date(h.voucher_date)<=:tudate and "
			+ "h.company_mst=:companyMst and c.account_name=:customername "
			+ "and h.branch_code=:branchcode")
	List<Object> getCustomerwiseSalesByDate(Date fudate, Date tudate, CompanyMst companyMst, String customername,
			String branchcode);

//	@Query(nativeQuery = true,value ="select h.voucher_date,h.voucher_number,"
//			+ "c.customer_name,p.insurance_card,"
//			+ "p.policy_type,h.cash_pay,h.cardamount,h.invoice_amount,u.user_name,p.patient_name "
//			+ "from sales_trans_hdr h,patient_mst p,customer_mst c,user_mst u where "
//			+ "h.customer_mst = c.id and p.customer_mst=c.id and u.id=h.user_id "
//			+ "and h.branch_code = :branchcode and h.id=:id and h.voucher_number is not null")
//	List<Object> getSalesSummaryByInsurance(String id, String branchcode);
	
	
	
	@Query(nativeQuery = true,value ="select h.voucher_date,h.voucher_number,"
			+ "p.insurance_card,"
			+ "p.policy_type,h.cash_pay,h.cardamount,h.invoice_amount,u.user_name,p.patient_name "
			+ "from sales_trans_hdr h,patient_mst p,user_mst u where "
			+ "u.id=h.user_id and p.id=h.patient_mst "
			+ "and h.branch_code = :branchcode and h.id=:id and h.voucher_number is not null")
	List<Object> getSalesSummaryByInsurance(String id, String branchcode);

	@Query(nativeQuery = true, value = "select distinct(c.id) from "
			+ "category_mst c,sales_dtl dtl,item_mst i,sales_trans_hdr h where "
			+ "i.id=dtl.item_id and c.id=i.category_id and "
			+ "h.id = dtl.sales_trans_hdr_id and "
			+ "h.id=:parentID and (dtl.print_kot_status='N' or dtl.print_kot_status is null)")
	List<String> getItemsGroupByCategory(String parentID);
	
	
	@Query(nativeQuery = true, value = "select " + " "
			+ " im.hsn_code, " + " im.item_code, " + " dtl.tax_rate,"
			+ " dtl.qty , " + " dtl.rate ,dtl.item_name, "
			+ "( dtl.qty * (dtl.rate + 0.005 ) ) as amount , "
			+ "   dtl.expiry_date,p.printer_name  "
			+ " from sales_dtl  dtl, " + "item_mst im,"
			+ "kot_category_mst k,"
			+ "printer_mst p,kot_item_mst ki  " + "where dtl.sales_trans_hdr_id=:parentID "
			+ "and  dtl.item_id = im.id "
			+ "and im.category_id=k.category_id " + "and k.printer_id=p.id "
			+ "and k.category_id=:catId and ki.item_id = im.id and (dtl.print_kot_status='1') ")

	List<Object> getItemDetailsForSplitPrintWithStatus(String parentID, String catId);
	
	
	
	
	@Query(nativeQuery = true, value = "select  im.hsn_code,   "
			+ "im.item_code,   dtl.tax_rate, "
			+ "dtl.qty ,   dtl.rate ,dtl.item_name,  "
			+ "( dtl.qty * (dtl.rate + 0.005 ) ) as amount ,  "
			+ "dtl.expiry_date, dtl.id ,"
			+ "dtl.kot_id  "
			+ "from "
			+ "sales_dtl  dtl,  "
			+ "item_mst im,"
			+ "kot_item_mst ki   "
			+ "where dtl.sales_trans_hdr_id=:parentID "
			+ "and  dtl.item_id = im.id "
			+ " and ki.item_id = im.id   "
			+ "and im.category_id=:catId"
			+ "  and (dtl.print_kot_status='N' or dtl.print_kot_status is null) " + 
			" ")

	List<Object> getItemListForSplitKot(String parentID, String catId);

	
	@Query(nativeQuery = true, value = "select  d.item_id, i.item_name,i.bar_code, sum(d.AMOUNT),sum(d.CGST_AMOUNT),i.standard_price, "
			+ " sum(d.qty), sum(d.RATE),d.ADD_CESS_RATE, sum(d.MRP), d.CGST_TAX_RATE, sum(d.ADD_CESS_AMOUNT),"
			+ " sum(d.CESS_AMOUNT), sum(d.SGST_AMOUNT), d.TAX_RATE, sum(d.IGST_AMOUNT), d.IGST_TAX_RATE, d.CESS_RATE "
			+ " from sales_dtl d, item_mst i, sales_trans_hdr h "
			+ " where h.id=d.sales_trans_hdr_id and date(h.voucher_date)=:date and h.company_mst=:companyMst and h.branch_code=:branchCode "
			+ " and  h.voucher_number is not null and d.item_id=i.id group by d.item_id, i.item_name,i.bar_code,i.standard_price,"
			+ " d.TAX_RATE,d.IGST_TAX_RATE, d.CESS_RATE,d.ADD_CESS_RATE,d.CGST_TAX_RATE")
	List<Object> salesDtlByItemAndDate(Date date, CompanyMst companyMst, String branchCode);

	SalesDtl findByItemIdAndSalesTransHdr(String itemId, SalesTransHdr salesTransHdr);
	
	
	SalesDtl findByItemIdAndSalesTransHdrAndOfferReferenceId(String itemId, SalesTransHdr salesTransHdr,String OfferReferenceId);
	

	@Query(nativeQuery = true, value = "SELECT   "
			+ " SUM((rate*qty) + (  (rate*qty) * (tax_rate)/100 ) + (rate*qty) * cess_Rate/100)  as totalAmount  "
			 
			+ "   FROM Sales_Dtl d, Sales_Trans_Hdr h "

			+ " where h.id = d.sales_trans_hdr_id AND h.id = :salesHdrId")
	Double  findSumSalesDtlByHdrId(String salesHdrId);
	
	@Query(nativeQuery = true,value = "select"
			+ " h.voucher_number,"
			+ "h.voucher_date,"
			+ "i.item_name,"
			+ "d.qty,"
			+ "d.amount,"
			+ "d.batch ,"
			+ "d.tax_rate from "
			+ "sales_trans_hdr h,sales_dtl d,item_mst i where "
			+ "h.id=d.sales_trans_hdr_id and "
			+ "i.id=d.item_id and "
			+ "date(h.voucher_date)>=:fdate and date(h.voucher_date)<=:tdate AND h.account_heads = :string")
	List<Object> findAMDCCustomerWiseSaleReport(Date fdate, Date tdate, String string);
	
	@Query(nativeQuery = true,value = "select "
		+"h.voucher_date,"
			+ "i.item_name,"
			+ "d.qty,"
			+ "c.customer_name,"
			+ "d.rate,"
			+ "d.batch "
			+ " from "
			+ "customer_mst c,"
			+ "sales_trans_hdr h,"
			+ "sales_dtl d,"
			+ "item_mst i "
			+ "where "
			+ "c.id=h.customer_mst and "
			+ "h.id=d.sales_trans_hdr_id and "
			+ "i.id=d.item_id and "
			+ "date(h.voucher_date)>=:fromDate and date(h.voucher_date)<=:toDate")
	List<Object> findPharmacyItemWiseSalesReport(Date fromDate, Date toDate);

	
	
	
	@Query(nativeQuery=true,value="SELECT "
			+ "sth.voucher_date, "
			+ "sth.voucher_number, "
			+ "cus.account_name, "
			+ "pat.id, "
			+ "pat.patient_name, "
			+ "itm.item_name, "
			+ "cat.category_name, "
			+ "sd.expiry_date, "
			+ "sd.batch, "
			+ "itm.item_code, "
			+ "sd.qty, "
			+ "sd.rate, "
			+ "sd.igst_tax_rate, "
			+ "sd.mrp * sd.qty, "
			+ "sd.cost_price, "
			+ "sd.cost_price * sd.qty "
			+ "FROM "
			+ "sales_trans_hdr sth, "
			+ "sales_dtl sd, "
			+ "account_heads cus, "
			+ "patient_mst pat, "
			+ "item_mst itm, "
			+ "category_mst cat "
			+ "WHERE "
			+ "sd.sales_trans_hdr_id = sth.id AND "
			+ "sth.patient_mst = pat.id AND "
			+ "sd.item_id = itm.id AND "
			+ "itm.category_id = cat.id AND "
			+ "sth.account_heads =cus.id AND "
			+ "sth.voucher_date >= :fromDate AND "
			+ "sth.voucher_date <= :toDate AND "
			+ "(sth.sales_mode = 'POS' OR sth.sales_mode = '"
			+ "KOT' OR sth.sales_mode = 'B2C') ")
	List<Object> getRetailSalesDetailReport(Date fromDate, Date toDate);
	
	
	@Query(nativeQuery=true,value="SELECT "
			+ "sth.voucher_date, "
			+ "sth.voucher_number, "
			+ "cus.account_name, "
			+ "itm.item_name, "
			+ "cat.category_name, "
			+ "sd.expiry_date, "
			+ "sd.batch, "
			+ "itm.item_code, "
			+ "sd.qty, "
			+ "sd.rate, "
			+ "sd.igst_tax_rate, "
			+ "sd.mrp * sd.qty, "
			+ "sd.cost_price, "
			+ "sd.cost_price * sd.qty "
			+ "FROM sales_trans_hdr sth, "
			+ "sales_dtl sd, "
			+ "account_heads cus, "
			+ "item_mst itm, "
			+ "category_mst cat "
			+ "WHERE sd.sales_trans_hdr_id = sth.id  "
			+ "AND sd.item_id = itm.id "
			+ "AND itm.category_id = cat.id "
			+ "AND sth.account_heads =cus.id "
			+ "AND sth.voucher_date >= '2022-02-01' "
			+ "AND sth.voucher_date <= '2022-04-30' "
			+ "AND (sth.sales_mode = 'POS' OR sth.sales_mode = 'KOT' OR sth.sales_mode = 'B2C')")
	List<Object> getRetailSalesDetailReportNew(Date fromDate, Date toDate);

	
	
	@Query(nativeQuery=true,value="SELECT "
			+ "sth.voucher_date, "
			+ "sth.voucher_number, "
			+ "cus.customer_name, "
			+ "pat.id, "
			+ "pat.patient_name, "
			+ "ndm.name, "
			+ "sd.rate, "
			+ "sd.igst_amount, "
			+ "sth.invoice_amount, "
			+ "SUM(sth.paid_amount), "
			+ "SUM(CASE WHEN sr.receipt_mode='CREDIT' THEN sr.receipt_amount ELSE 0.0 END),"
			+ "usr.user_name,"  
			+ "SUM(CASE WHEN rm.credit_card_status='NO' AND sr.receipt_mode='CASH' THEN sr.receipt_amount ELSE 0.0 END),"  
			+ "SUM(CASE WHEN rm.credit_card_status='YES' AND rm.receipt_mode='CASH' THEN sr.receipt_amount ELSE 0.0 END),"  
			+ "SUM(CASE WHEN sr.receipt_mode='INSURANCE' THEN sr.receipt_amount ELSE 0.0 END) "
			+ "FROM "
			+ "sales_trans_hdr sth, "
			+ "sales_dtl sd, "
			+ "patient_mst pat, "
			+ "new_doctor_mst ndm, "
			+ "customer_mst cus, "
			+ "user_mst usr,"
			+ "sales_receipts sr,"
			+ "receipt_mode_mst rm "
			+ "WHERE "
			+ "sd.sales_trans_hdr_id = sth.id AND "
			+ "sth.patient_mst = pat.id AND "
			+ "sth.customer_mst =cus.id AND "
			+ "sth.doctor_id = ndm.id AND "
			+ "sth.user_id = usr.id AND "
			+ "sr.sales_trans_hdr_id = sth.id AND "
			+ "sr.receipt_mode = rm.receipt_mode AND "
			+ "date(sth.voucher_date) >= :fromDate AND "
			+ "date(sth.voucher_date) <= :toDate AND "
			+ "(sth.sales_mode = 'POS' OR sth.sales_mode = 'KOT' OR sth.sales_mode = 'B2C') "
			+ "GROUP BY "
			+ "sth.voucher_date,sth.voucher_number,cus.customer_name,pat.id,"
			+ "pat.patient_name,ndm.name,sd.rate,sd.igst_amount,sth.invoice_amount,"
			+ "sth.credit_amount,usr.user_name")
	List<Object> getRetailSalesSummaryReport(Date fromDate, Date toDate);


	
	@Query(nativeQuery=true,value="SELECT "
			+ "sth.voucher_date, "
			+ "sth.voucher_number, "
			+ "cus.account_name, "
			+ "sd.rate, "
			+ "sd.igst_amount+sd.sgst_amount+sd.cgst_amount, "
			+ "sth.invoice_amount, "
			+ "SUM(sth.paid_amount), "
			+ "SUM(CASE WHEN sr.receipt_mode='CREDIT' THEN sr.receipt_amount ELSE 0.0 END),"
			+ "usr.user_name,"  
			+ "SUM(CASE WHEN rm.credit_card_status='NO' AND sr.receipt_mode='CASH' THEN sr.receipt_amount ELSE 0.0 END),"  
			+ "SUM(CASE WHEN rm.credit_card_status='YES' AND rm.receipt_mode='CASH' THEN sr.receipt_amount ELSE 0.0 END),"  
			+ "SUM(CASE WHEN sr.receipt_mode='INSURANCE' THEN sr.receipt_amount ELSE 0.0 END) "
			+ "FROM "
			+ "sales_trans_hdr sth, "
			+ "sales_dtl sd, "
			+ "account_heads cus, "
			+ "user_mst usr,"
			+ "sales_receipts sr,"
			+ "receipt_mode_mst rm "
			+ "WHERE "
			+ "sd.sales_trans_hdr_id = sth.id AND "
			+ "sth.account_heads =cus.id AND "
			+ "sth.user_id = usr.id AND "
			+ "sr.sales_trans_hdr_id = sth.id AND "
			+ "sr.receipt_mode = rm.receipt_mode AND "
			+ "date(sth.voucher_date) >= :fromDate AND "
			+ "date(sth.voucher_date) <= :toDate AND "
			+ "(sth.sales_mode = 'POS' OR sth.sales_mode = 'KOT' OR sth.sales_mode = 'B2C') "
			+ "GROUP BY "
			+ "sth.voucher_date,sth.voucher_number,cus.account_name,"
			+ "sd.rate,sd.igst_amount,sth.invoice_amount,"
			+ "sth.credit_amount,usr.user_name")
	List<Object> getRetailSalesSummaryReportNew(Date fromDate, Date toDate);
	
	
	
	
	@Query(nativeQuery = true,value = "select "
			+"h.voucher_date,"
				+ "i.item_name,"
				+ "sum(d.qty),"
				+ "c.account_name,"
				+ "sum(d.rate),"
				+ "d.batch "
				+ " from "
				+ "account_heads c,"
				+ "sales_trans_hdr h,"
				+ "sales_dtl d,"
				+ "item_mst i "
				+ "where "
				+ "c.id=h.account_heads and "
				+ "h.id=d.sales_trans_hdr_id and "
				+ "i.id=d.item_id and "
				+ "date(h.voucher_date)>=:fromDate and date(h.voucher_date)<=:toDate and i.id=:id group by i.item_name,c.account_name,"
				+ "d.batch")
	List<Object> findPharmacyItemWiseSalesReportByItemName(Date fromDate, Date toDate, String id);
	
	@Query(nativeQuery = true,value = "select "
			+"h.voucher_date,"
				+ "i.item_name,"
				+ "sum(d.qty),"
				+ "c.account_name,"
				+ "sum(d.rate),"
				+ "d.batch "
				+ " from "
				+ "account_heads c,"
				+ "sales_trans_hdr h,"
				+ "sales_dtl d,"
				+ "item_mst i "
				+ "where "
				+ "c.id=h.account_heads and "
				+ "h.id=d.sales_trans_hdr_id and "
				+ "i.id=d.item_id and "
				+ "date(h.voucher_date)>=:fromDate and date(h.voucher_date)<=:toDate and i.id=:id group by i.item_name,c.account_name,"
				+ "d.batch,h.voucher_date")
	List<Object> findPharmacyItemWiseSalesReportByItemNameDerby(Date fromDate, Date toDate, String id);

	
	@Query(nativeQuery = true,value = "select h.voucher_date,h.voucher_number,h.id as hdrid,d.id as dtlid, "
			+ "d.item_id,d.barcode,d.batch,d.mrp,d.qty,c.customer_name from sales_dtl d, sales_trans_hdr h, customer_mst c "
			+ " where h.id=d.sales_trans_hdr_id and h.voucher_number is not null and h.company_mst=:companymstid "
			+ " and h.branch_code=:branchcode and d.id not in (select dtl.source_dtl_id from item_batch_dtl dtl where "
			+ " date(dtl.voucher_date)=:date) and date(h.voucher_date)=:date and h.customer_mst=c.id")
	List<Object> SalesToBatchDtl(String companymstid, String branchcode,Date date);

	
	
	@Query(nativeQuery = true,value = "select i.item_name,sum(d.qty) as qty from "
			+ "sales_dtl d ,item_mst i , sales_trans_hdr h  where h.company_mst=:companymstid "
			+ "and date(h.voucher_date)<=:udate and d.sales_trans_hdr_id=h.id and "
			+ "d.item_id=i.id  group by i.item_name order by qty desc")
	List<Object> getTopSellingProducts(String companymstid, Date udate, Pageable topFifty);

	
	
	
	
	
	@Query(nativeQuery = true,value = "select h.voucher_date,i.item_name,h.branch_code,sum(d.qty),"
			+ "c.account_name,sum(d.rate),d.batch "
			+ "from account_heads c,sales_trans_hdr h,sales_dtl d,item_mst i"
			+ " where c.id=h.account_heads and"
			+ " h.id=d.sales_trans_hdr_id and "
			+ "i.id=d.item_id and"
			+ " date(h.voucher_date)>=:fromDate "
			+ "and date(h.voucher_date)<=:toDate "
			+ "and h.branch_code=:branchcode "
			+ " group by i.item_name,c.account_name,d.batch;")
	List<Object> findItemWiseSalesReportByItemNameWithBranchcode(Date fromDate, Date toDate, 
			String branchcode);

	
	
	

}
