package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.UserGroupMst;

public interface UserGroupMstReository extends JpaRepository<UserGroupMst, String> {

	
	
	List<UserGroupMst> findByUserIdAndCompanyMst(String userId,String companymstid);
	
	List<UserGroupMst>findByGroupIdAndCompanyMst(String groupId,String companymstid);
	
	List<UserGroupMst>findByCompanyMst(CompanyMst companymstid);

//	List<UserGroupMst> findByGroupIdAndGroupName(String groupid, String groupname,String companymstid);

	
}
