package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.WeighBridgeTareWeight;
@Component
@Repository
public interface WeighBridgeTareWeightRepository extends JpaRepository<WeighBridgeTareWeight, String>{


	WeighBridgeTareWeight findByVehicleno(String vehicleno);

	@Query(nativeQuery = true,value="select * from weigh_bridge_tare_weight where "
			+ "vehicleno like :data")
	
	List<WeighBridgeTareWeight> searchByVehicleNo(String data);
	

	

}
