package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AccountHeadsProperties;
import com.maple.restserver.entity.CompanyMst;

@Component
@Repository
public interface AccountHeadsPropertiesRepository extends JpaRepository<AccountHeadsProperties, String>{

	List<AccountHeadsProperties> findByCompanyMst(CompanyMst companyMst);

	AccountHeadsProperties findByAccountIdAndPropertyName(String accountid, String propertyname);

}
