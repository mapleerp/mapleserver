package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SalesOrderReceipts;
@Component
@Repository
public interface AdvanceReceiptsRepository extends JpaRepository<SalesOrderReceipts,String>{

	

	@Query(nativeQuery=true,value="select sum(advance_receipt_amount) - sum( invoice_realized_amount) "
			+ " from advance_receipts ar, sales_order_trans_hdr so" + 
			
			" where ar.sales_order_trans_hdr_id = so.id" + 
			 
			"	and date(so.voucher_date) < :receiptDate and ar.company_mst=:companymstid")

	
	List<Object> findAdvanceReceipt(Date receiptDate,String companymstid);
	List<SalesOrderReceipts>findByCompanyMstId(String companymstid);
}
