package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.NutritionValueDtl;
@Transactional
@Repository
public interface NutrintionValueDtlRepository extends JpaRepository<NutritionValueDtl, String> {

	List<NutritionValueDtl> findByCompanyMstIdAndNutritionValueMstId(String companyMst,String itemmst);
	NutritionValueDtl findByCompanyMstIdAndNutritionValueMstIdAndNutrition(String companyMst,String itemmst,String nutrition);
}
