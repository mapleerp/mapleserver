package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.DynamicProductionHdr;

@Component
@Repository
public interface DynamicProductionHdrRepository extends JpaRepository<DynamicProductionHdr, String>{

	DynamicProductionHdr findByIdAndCompanyMstId(String dynamicproductionhdrid, String companymstid);

}
