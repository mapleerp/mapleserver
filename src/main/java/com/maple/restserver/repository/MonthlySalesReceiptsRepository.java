package com.maple.restserver.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.MonthlySalesReceipts;

@Repository
public interface MonthlySalesReceiptsRepository extends JpaRepository<MonthlySalesReceipts, String>{

	

	@Modifying(flushAutomatically = true)
	 @Query(nativeQuery = true,value="INSERT INTO monthly_sales_receipts (id, receipt_mode ,branch_code , user_id , account_id , receipt_amount , voucher_number, monthly_sales_trans_hdr_id , company_mst ) "
	 		+ "SELECT r.id ,r.receipt_mode,r.branch_code,r.user_id,r.account_id,r.receipt_amount,r.voucher_number,r.sales_trans_hdr_id, r.company_mst FROM sales_receipts r,sales_trans_hdr h where h.id=r.sales_trans_hdr_id and date(h.voucher_date)between :fromDate and :toDate "
	 		)
	 void insertingIntoMonthlySalesReceipts (Date fromDate ,Date toDate);

	

	@Modifying(flushAutomatically = true)
	 @Query(nativeQuery = true,value="INSERT INTO monthly_sales_receipts (id, receipt_mode ,branch_code , user_id , account_id , receipt_amount , voucher_number, monthly_sales_trans_hdr_id , company_mst ) "
	 		+ "SELECT r.id ,r.receipt_mode,r.branch_code,r.user_id,r.account_id,r.receipt_amount,r.voucher_number,r.monthly_sales_trans_hdr_temp_id, r.company_mst FROM sales_receipts_temp r,sales_trans_hdr h  "
	 		)
	 void insertingIntoMonthlySalesReceiptsFromMonthlySalesReceiptsTemp ();

	
	@Modifying
	@Query(nativeQuery = true,value = "update monthly_sales_receipts set "
			+ "monthly_sales_trans_hdr_id =:hdrId "
			+ "where monthly_sales_trans_hdr_id IN (select h.id from "
			+ "monthly_sales_trans_hdr h where "
			+ "customer_id = :custId and date(voucher_date) between :fromDate and :toDate "
			
			+ ") ")
	void updateMonthlySalesReceipts(String hdrId,String custId,Date fromDate,Date toDate);
	
	
	@Modifying
	@Query(nativeQuery = true,value="delete from monthly_sales_receipts")
    void deleteMonthlySalesReceipts();


	@Modifying
	@Query(nativeQuery = true,value="update monthly_sales_receipts set voucher_number = :voucherNumber,"
			+ "receipt_mode=:mode,account_id=:accid "
			+ "where monthly_sales_trans_hdr_id =:id")
	void updateMonthlySalesReceiptsVoucherNumber(String id, String voucherNumber,String mode,String accid);


}
