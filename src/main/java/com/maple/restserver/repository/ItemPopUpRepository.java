package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ItemPopUp;



@Repository
public interface ItemPopUpRepository extends JpaRepository<ItemPopUp, String>  , ItemSearchRepositoryCustom {


	
//	List<ItemPopUp> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);


	@Query("SELECT i.itemName  , i.barCode   , u.unitName   ,i.standardPrice ,"


			+ " i.cess, i.taxRate , i.id, u.id ,"
			+ "i.categoryId , "
			+ "i.hsnCode,"+"i.isDeleted"
			+ " FROM ItemMst i , UnitMst u  "
			+ " WHERE i.unitId= u.id  AND lower(i.itemName) LIKE  :searchItemName")
 
	public List<Object> findSearch(@Param("searchItemName") String searchItemName ,  Pageable pageable);
	
	
	@Query("SELECT i.itemName  ,"
			+ " trim(i.barCode)  , "
			+ " u.unitName   ,"
			+ " i.standardPrice ,"
			+ " i.cess, "
			+ " i.taxRate , "
		 
			+ " i.id, u.id ,"
			+ "i.categoryId , "
			+ "i.hsnCode,"
			+"i.isDeleted, "
			+ "i.productId, "
			+ "i.brandId, "
			+ "i.model, "
			+ "i.itemCode "
		
			+ " FROM ItemMst i , UnitMst u  "
			+ " WHERE i.unitId= u.id  AND lower(i.itemName) LIKE  :searchItemName AND i.companyMst =:companyMst")
 
	public List<Object> findSearchWithComp(@Param("searchItemName") String searchItemName ,  Pageable pageable,CompanyMst companyMst);
	
	
	
	
	
	
	
	@Query("SELECT i.itemName  ,"
			+ " i.barCode   , "
			+ " u.unitName   ,"
			+ " i.standardPrice ,"
			+ " i.cess, "
			+ " i.taxRate , "
		 
			+ " i.id, u.id ,"
			+ "i.categoryId , "
			+ "i.hsnCode,"
			+"i.isDeleted, "
			+ "i.productId, "
			+ "i.brandId, "
			+ "i.model, "
			+ "i.itemCode"
		
			+ " FROM ItemMst i , UnitMst u ,CategoryMst c "
			+ " WHERE i.unitId= u.id and c.id=i.categoryId AND lower(i.itemName) LIKE  :searchItemName AND i.companyMst =:companyMst and c.id=:catId ")
 
	public List<Object> findSearchWithCategory(@Param("searchItemName") String searchItemName ,  Pageable pageable,CompanyMst companyMst,String catId);
	
	@Query("SELECT i.itemName  , trim(i.barCode)   , u.unitName   ,i.standardPrice ,"
			+ " i.cess,"
			+ " i.taxRate , "
		 
			+ " i.id, u.id ,"
			+ "i.categoryId , "
			+ "i.hsnCode,"
			+"i.isDeleted, "
			+ "i.productId, "
			+ "i.brandId, "
			+ "i.model, "
			+ "i.itemCode "
		
			+ " FROM ItemMst i , UnitMst u  "
			+ " WHERE i.unitId= u.id  AND lower(i.itemCode) =  :searchItemName AND i.companyMst =:companyMst")
 
	public List<Object> findSearchByItemCode(@Param("searchItemName") String searchItemName ,  Pageable pageable,CompanyMst companyMst);
	

	
	
	
	@Query("SELECT i.itemName  , trim(i.barCode)   , u.unitName   ,i.standardPrice ,"
			+ " i.cess, "
			+ " i.taxRate , "
			 
			+ " i.id, "
			+ " u.id ,"
			+ "i.categoryId , "
			+ "i.hsnCode,"
			+"i.isDeleted, "
			+ "i.productId, "
			+ "i.brandId, "
			+ "i.model, "
			+ "i.itemCode"
		
			+ " FROM ItemMst i , UnitMst u  "
			+ " WHERE i.unitId= u.id  AND lower(i.barCode) =  :searchItemName AND i.companyMst =:companyMst")
 
	public List<Object> findSearchByBarcodeWithComp(@Param("searchItemName") String searchItemName ,  Pageable pageable,CompanyMst companyMst);
	

	

	@Query("SELECT i.itemName  , i.barCode   , u.unitName   ,i.standardPrice ,"
			+ " i.cess, "
			+ " i.taxRate , "
			 
			+ " i.id, "
			+ " u.id ,"
			+ "i.categoryId , "
			+ "i.hsnCode,"
			+"i.isDeleted, "
			+ "i.productId, "
			+ "i.brandId, "
			+ "i.model, "
			+ "i.itemCode"
		
			+ " FROM ItemMst i , UnitMst u,CategoryMst c  "
			+ " WHERE i.unitId= u.id and c.id=i.categoryId AND lower(i.barCode) =  :searchItemName AND i.companyMst =:companyMst and c.id=:catId ")
 
	public List<Object> findSearchByBarcodeWithCategory(@Param("searchItemName") String searchItemName ,  Pageable pageable,CompanyMst companyMst,String catId);
	
	
	
	
	@Query("SELECT i.itemName  , "
			+ "i.barCode   , "
			+ "u.unitName   ,"
			+ "i.standardPrice ,"
			+ " i.cess, "
			+ " i.taxRate , "
			+ " 10000.0, "
			+ " i.id, "
			+ " u.id ,"
			+ " 'NOBATCH' , "
			+ "i.categoryId , "
			+ "i.hsnCode,"
			+"i.isDeleted, "
			+ "i.productId, "
			+ "i.brandId, "
			+ "i.model, "
			+ "i.itemCode"
		
			+ " FROM ItemMst i , UnitMst u  "
			+ " WHERE i.unitId= u.id  AND lower(i.barCode) =  :searchItemName AND i.companyMst =:companyMst")
 
	public List<Object> findSearchByBarcodeDummyStockWithComp(@Param("searchItemName") String searchItemName ,  Pageable pageable,CompanyMst companyMst);

	
	
	
	
	 List<ItemMst> findItemBySearch(String searchString,Pageable pageable);
	 List<ItemMst> findItemByBarcodeSearch(String searchString, Pageable pageable);
	 @Query("SELECT i.itemName  , i.barCode   , u.unitName   ,i.standardPrice ,"
				+ " i.cess,"
				+ " i.taxRate , "
			 
				+ " i.id, u.id ,"
				+ "i.categoryId , "
				+ "i.hsnCode,"
				+"i.isDeleted, "
				+ "i.productId, "
				+ "i.brandId, "
				+ "i.model, "
				+ "i.itemCode"
			
				+ " FROM ItemMst i , UnitMst u  "
				+ " WHERE i.unitId= u.id")
	 
		public List<Object> itemsForInMemDb();

	 
	 @Query("SELECT i.itemName  , i.barCode   , u.unitName   ,i.standardPrice ,"
				+ " i.cess, i.taxRate , i.id, u.id ,"
				+ "i.categoryId , "
				+ "i.hsnCode,"+"i.isDeleted"
				+ " FROM ItemMst i , UnitMst u , KitDefinitionMst k "
				+ " WHERE i.unitId= u.id  AND lower(i.itemName) LIKE  :searchKitItemName and i.id=k.itemId")
	public List<Object> findKitNameSearch(@Param("searchKitItemName")String searchKitItemName, Pageable topFifty);
		
}

