package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SchOfferAttrInst;
import com.maple.restserver.entity.SchOfferDef;
import com.maple.restserver.entity.SchemeInstance;

@Repository
public interface SchOfferDefRepository extends JpaRepository<SchOfferDef,String>  {

	SchOfferDef findByOfferTypeAndCompanyMst(String offerType, CompanyMst comapnymst);
	SchOfferDef findByIdAndCompanyMst(String offerId, CompanyMst comapnymst);
	List<SchOfferDef> findByCompanyMst( CompanyMst comapnymst);
//	List<SchOfferAttrInst> findByOfferId(String offerId);

}
