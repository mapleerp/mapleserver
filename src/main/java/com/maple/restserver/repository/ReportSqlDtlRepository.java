package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReportSqlDtl;
import com.maple.restserver.entity.ReportSqlHdr;


@Component
@Repository
public interface ReportSqlDtlRepository extends JpaRepository<ReportSqlDtl,String>{

	List<ReportSqlDtl> findByReportSqlHdrId(String id);

	List<ReportSqlDtl> findByCompanyMst(CompanyMst companyMst);

	List<ReportSqlDtl> findByCompanyMstIdAndReportSqlHdrId(String companymstid, String id);



}
