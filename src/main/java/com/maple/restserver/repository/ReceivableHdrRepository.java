package com.maple.restserver.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.ReceivableHdr;

@Repository
public interface ReceivableHdrRepository extends JpaRepository<ReceivableHdr, Integer>{
	
	
	List<ReceivableHdr> findByVoucherNumberAndVoucherDate(String voucherNumber, Date voucherDate);
	
	
	List<ReceivableHdr> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);

//	@Query(nativeQuery=true,value="select sum(amount) as totalamount from receipt_dtl  where receipt_hdr_id =:hdrId ")
	
	





}
