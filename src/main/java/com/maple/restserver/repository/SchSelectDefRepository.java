package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SchOfferDef;
import com.maple.restserver.entity.SchSelectDef;

@Repository
public interface SchSelectDefRepository  extends JpaRepository<SchSelectDef,String>  {

	SchSelectDef findBySelectionNameAndCompanyMst(String selectName , CompanyMst companyMst);
	
	SchSelectDef findByIdAndCompanyMst(String selectid , CompanyMst companyMst);
	List<SchSelectDef> findByCompanyMst(CompanyMst companymst);
}
