package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.MonthlySalesDtl;
import com.maple.restserver.entity.MonthlySalesTransHdr;

@Repository
public interface MonthlySalesDtlRepository extends JpaRepository<MonthlySalesDtl, String> {

	@Modifying(flushAutomatically = true)
	 @Query(nativeQuery = true,value="INSERT INTO monthly_sales_dtl (id,item_id,qty,rate,cgst_tax_rate,sgst_tax_rate,cess_rate,add_cess_rate,igst_tax_rate,item_taxax_id,unit_id,item_name,expiry_date,batch,barcode,tax_rate,mrp,amount,unit_name,discount,cgst_amount,sgst_amount,igst_amount,cess_amount,returned_qty,status,add_cess_amount,cost_price,print_kot_status,monthly_sales_trans_hdr_id,company_mst,offer_reference_id,scheme_id,standard_price,fb_key) SELECT d.id,d.item_id,d.qty,d.rate,d.cgst_tax_rate,d.sgst_tax_rate,d.cess_rate,d.add_cess_rate,d.igst_tax_rate,d.item_taxax_id,d.unit_id,d.item_name,d.expiry_date,d.batch,d.barcode,d.tax_rate,d.mrp,d.amount,d.unit_name,d.discount,d.cgst_amount,d.sgst_amount,d.igst_amount,d.cess_amount,d.returned_qty,d.status,d.add_cess_amount,d.cost_price,d.print_kot_status,d.sales_trans_hdr_id,d.company_mst,d.offer_reference_id,d.scheme_id,d.standard_price,d.fb_key FROM sales_dtl d,sales_trans_hdr h where h.id=d.sales_trans_hdr_id and date(h.voucher_date)between :fromDate and :toDate "
	 		)
	 void insertingIntoMonthlySalesDtl (Date fromDate ,Date toDate);
	
	
	@Modifying
	@Query(nativeQuery = true, value ="update monthly_sales_dtl set monthly_sales_trans_hdr_id =:hdrId "
			+ "where monthly_sales_trans_hdr_id IN (select h.id from monthly_sales_trans_hdr h"
			+ ""
			+ " where "
			+ "customer_id =:custId and date(voucher_date) between :fromDate and :toDate "
			+ " 	)")
	void updateMonthlySalesDtlWithId(String hdrId,String custId,Date fromDate,Date toDate);
	
	
	@Modifying
	@Query(nativeQuery = true,value="delete from monthly_sales_dtl" )
	void deleteMonthlySalesDtl();
	
	List<MonthlySalesDtl>findByMonthlySalesTransHdr(MonthlySalesTransHdr monthlysalestranshdrid);
	
}



