//package com.maple.restserver.repository;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.stereotype.Component;
//import org.springframework.stereotype.Repository;
//
//import com.maple.restserver.entity.CompanyMst;
//import com.maple.restserver.entity.CustomerMst;
//import com.maple.restserver.entity.LocalCustomerMst;
//@Component
//@Repository
//public interface CustomerMstRepository extends JpaRepository<CustomerMst, String>{
//
//	List<CustomerMst>findByCustomerName(String customerName);
//	
//	
//	//version3.5sari
//	@Modifying
//	@Query(nativeQuery = true,value="update customer_mst set customer_discount=0 where customer_discount is NULL")
//	void updateCustDiscount();
//	//version3.5ends
//
//	@Query("SELECT c FROM CustomerMst c WHERE LOWER(c.customerName) like   LOWER(:custmerName) AND c.companyMst.id=:companymst"
//			+ " ORDER BY c.customerRank DESC" )
//
//    public List<CustomerMst> findSearch( String custmerName,String companymst,Pageable topfifty);
//
//	@Query(nativeQuery = true,value="select * from customer_mst where customer_name=:customerName"
//			+ " and company_mst = :compantmstid")
//	  public List<CustomerMst>  findByCustomerNameAndCompanyMstIdList(String customerName, String compantmstid);
//
//	  public Optional<CustomerMst>  findByCustomerNameAndCompanyMstId(String customerName, String compantmstid);
//	
//	  
//	 List<CustomerMst>  findByCompanyMstId(String companymstid);
//	 Optional<CustomerMst> findByIdAndCompanyMstId(String custid,String companymstid);
//	 Optional<CustomerMst> findById(CustomerMst customerMst);
//		Optional<LocalCustomerMst> findById(LocalCustomerMst localCustomerMst);
//
//		
//		CustomerMst findByCustomerContact(String customerphoneno);
//		
//		CustomerMst	findByCompanyMstAndCustomerContact(CompanyMst customerMst,String customerphoneno);
//		
//		Optional <CustomerMst>	findByCompanyMstAndCustomerName(CompanyMst customerMst,String customerName);
//
//	
//		@Query(nativeQuery = true,value="select * from customer_mst OFFSET :nCount ROWS FETCH NEXT 1 ROW ONLY")
//      CustomerMst randomCustomer(int nCount);
//     @Query(nativeQuery = true,value="select count(id) from customer_mst")
//      int customerCount();
//
//
//     @Query("SELECT c FROM CustomerMst c WHERE LOWER(c.id) like   LOWER(:string) AND c.companyMst.id=:companymstid"
// 			+ " and c.customerName=:customername" )
//
//	Optional<CustomerMst> findByCustomerNameAndCompanyMstIdAndBranchCode(String customername, String companymstid,
//			String string);
//
//
//	List<CustomerMst> findByCustomerNameAndCompanyMst(String customer, CompanyMst companyMst);
//
//
//	@Modifying
//	@Query(nativeQuery = true,value = "update customer_mst set tax_invoice_format=:reportname")
//	void updateAllCustomerByReportName(String reportname);
//	@Modifying
//	@Query(nativeQuery = true,value = "update customer_mst set tax_invoice_format=:reportname where "
//			+ "price_type_id=:priceid")
//	void updateAllCustomerByReportNameAndPriceId(String reportname, String priceid);
//
//
//	List<CustomerMst> findByCompanyMstAndCustomerGst(CompanyMst companyMst, String gst);
//
//	CustomerMst findByOldId(String oldId);
//
//
//}
