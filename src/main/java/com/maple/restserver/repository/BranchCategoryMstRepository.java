package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.BranchCategoryMst;
import com.maple.restserver.entity.CompanyMst;

@Repository
public interface BranchCategoryMstRepository extends JpaRepository<BranchCategoryMst, String > {

	List<BranchCategoryMst> findByCompanyMst(CompanyMst companyMst);
	

	List<BranchCategoryMst> findByCompanyMstAndBranchCode(CompanyMst companyMst, String branchcode);


	List<BranchCategoryMst> findByCompanyMstAndCategoryId(CompanyMst companyMst, String categoryid);
	@Query(nativeQuery = true,value="select bc.branch_code,c.category_name from branch_category_mst bc,category_mst c where  bc.category_id=c.id and bc.company_mst=:companyMst")
	List<Object>retrieveBranchCategory(CompanyMst companyMst);
	 public void  deleteById(String id);
}
