package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SalesDltdDtl;

@Repository
public interface SalesDeletedDtlRepository extends JpaRepository<SalesDltdDtl,String >{

	List<SalesDltdDtl> findByBranchCodeAndVoucherDateAndCompanyMstId(String branchCode,Date voucherDate,String comp);
	
	
}
