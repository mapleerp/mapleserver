package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ItemDeviceMst;

@Repository
@Component
public interface ItemDeviceMstRepository extends JpaRepository<ItemDeviceMst, String>{

	@Query(nativeQuery = true,value = "select im.* from item_device_mst,item_mst i "
			+ "where i.id = im.item_id and i.item_name=:itemname")
	List<ItemDeviceMst> findItemDeviceMstByItemName(String itemname);

	List<ItemDeviceMst> findByBranchCode(String branchcode);

}
