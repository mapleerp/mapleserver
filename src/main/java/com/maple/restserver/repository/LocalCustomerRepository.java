package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LocalCustomerMst;

public interface LocalCustomerRepository extends JpaRepository<LocalCustomerMst, String> {

	

	@Query("SELECT c FROM LocalCustomerMst c WHERE LOWER(c.localcustomerName) like   LOWER(:custmerName)")

    public List<LocalCustomerMst> searchLikeLocalCustomerByName( String custmerName);
	

 

	public List<LocalCustomerMst> findByCustomerId(String customerid);
	
	LocalCustomerMst findByPhoneNo(String phno);

	public List<LocalCustomerMst> findByLocalcustomerNameAndCompanyMst(String string, CompanyMst companyMst);

	public LocalCustomerMst findByLocalcustomerName(String localcustomername);

	

//	 LocalCustomerMst findByLocalcustomerNameAndCompanyMst(String customerName , CompanyMst companyMst);

	//public  List<LocalCustomerMst> findByCompanyMst(CompanyMst companyMst);
}
