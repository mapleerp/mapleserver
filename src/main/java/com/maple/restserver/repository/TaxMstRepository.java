package com.maple.restserver.repository;

 
import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.TaxMst;

public interface TaxMstRepository extends JpaRepository<TaxMst, String> {

List<TaxMst>findByCompanyMst(String companymstid);


List<TaxMst> findByCompanyMstIdAndItemId(String compId,String itemId);


List<TaxMst> findByCompanyMstIdAndItemIdAndTaxRate(String compId,String itemId,Double taxRate);


List<TaxMst> findByCompanyMstIdAndItemIdAndTaxId(String compId,String itemId,String taxId);

List<TaxMst> findByCompanyMstIdAndItemIdAndTaxIdAndTaxRate(String compId,String itemId,String taxId,Double taxRate);

@Modifying
@Query(nativeQuery = true, value ="update tax_mst p set p.end_date=:startdate where p.item_id=:itemid and p.tax_id =:taxId and p.end_date is null")
 void UpdateTaxMstByItemId(String itemid,java.util.Date startdate,String taxId);


@Query(nativeQuery = true,value="select * from tax_mst tm where  "
		+ " tm.tax_rate=:taxRate and tm.tax_id=:taxId and tm.item_id=:itemId "
		+ "and tm.company_mst=:companymstid and ( :mdate between date(tm.start_date) and date(tm.end_date) or date(tm.end_date) is NULL) ")
List<TaxMst> findByTaxIdandTaxRate(String itemId,Double taxRate,String taxId, String companymstid,Date mdate);
//	List<TaxMst> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);


@Query(nativeQuery = true,value="select * from tax_mst where item_id=:itemId and tax_id=:taxId and "
		+ "end_date is NULL")	
TaxMst getTaxByItemIdAndTaxId(String itemId,String taxId);


@Query(nativeQuery = true,value="select * from tax_mst where item_id=:itemId and "
		+ "end_date is NULL")	
List<TaxMst> getTaxByItemId(String itemId);


}
