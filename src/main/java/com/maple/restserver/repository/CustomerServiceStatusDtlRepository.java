package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CustomerServiceStatusDtl;

@Repository
public interface CustomerServiceStatusDtlRepository extends JpaRepository<CustomerServiceStatusDtl,String>{

	List<CustomerServiceStatusDtl> findByCompanyMstAndCustomerServiceStatusHdr(CompanyMst companyMst,String hdrid);
}
