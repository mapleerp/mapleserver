package com.maple.restserver.repository;

 
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.UrsMst;
import com.maple.restserver.entity.WeighBridgeWeights;
@Component
@Repository
public interface WeighBridgeWeightsRepository extends JpaRepository<WeighBridgeWeights, String>{
	
	@Query(nativeQuery = true,value = "select * from weigh_bridge_weights where "
			+ "lower(vehicleno) like :vno and (nextweight is null or nextweight=0) order by voucher_date desc")
	List<WeighBridgeWeights> searchByVehicleNoWithSecndWtNull(String vno);
	
	@Query(nativeQuery = true,value = "select * from weigh_bridge_weights where "
			+ "lower(vehicleno) like :vno order by voucher_date desc")
	List<WeighBridgeWeights> searchByVehicleNo(String vno);

	List<WeighBridgeWeights> findByVehicleno(String vno);
	 
	
	@Query(nativeQuery = true,value = "select * from weigh_bridge_weights where (nextweightid is null ) "
			+ " AND vehicleno = :VehicleNo and voucher_date >= :vdate "
			+ " order by voucher_date DESC ")
	List<WeighBridgeWeights> getListOfFirstWeightOfVehicle(Date vdate,String VehicleNo);
	
	@Query(nativeQuery = true,value = "select  * from weigh_bridge_weights h  where h.company_mst=:companymstid and  date(h.voucher_date)=:fdate   ")
	List<WeighBridgeWeights>getWeighBridgeReport( String  companymstid ,Date fdate 
		);
	@Query(nativeQuery = true,value = "select * from weigh_bridge_weights where (nextweight is null or nextweight=0)")
	List<WeighBridgeWeights> getWeighBridgeWithSecondWtNull();
	
	
	
 
	 
	
	//,Date tDate
	//,Date tDate
	
	//date(h.voucher_date) >= :fromDate and date(h.voucher_date) <= :tDate

	//and h.voucher_date <=:tdate
//and date(h.voucher_date) <= :tDate 
}