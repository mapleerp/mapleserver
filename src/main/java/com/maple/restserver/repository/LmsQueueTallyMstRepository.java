package com.maple.restserver.repository;
 
import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.LmsQueueTallyMst;
import com.maple.restserver.entity.ReceiptHdr;

 
@Transactional
@Repository
public interface LmsQueueTallyMstRepository extends JpaRepository<LmsQueueTallyMst, String>{

	
	LmsQueueTallyMst findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,
			String voucherType);

	
	 @Query(value=" select p from LmsQueueTallyMst p where p.postedToTally='NO'  and voucherType ='forwardSales' ")
	 	List<LmsQueueTallyMst>findLmsQueueMstNotPosted();


	List<LmsQueueTallyMst> findByVoucherDateAndVoucherTypeAndCompanyMstAndPostedToTally(java.util.Date udate,
			String vouchertype, CompanyMst companyMst, String string);
 
 
}
