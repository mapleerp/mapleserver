
package com.maple.restserver.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.report.entity.OrgReceiptInvoice;

@Repository
public interface  ReceiptHdrRepository extends JpaRepository<ReceiptHdr, String>{

	@Query(nativeQuery = true,value="select * from receipt_hdr where "
			+ "date(voucher_date)=:vdate and voucher_number=:vno")
	ReceiptHdr getReceiptByVdateAndVno(Date vdate,String vno);
	@Query(nativeQuery = true,value="select count(*) from receipt_hdr where "
			+ "branch_code =:branchCode and date(voucher_date)=:sqlDate and "
			+ "company_mst =:companymstid")
	int countOfReceiptHdr(String branchCode,Date sqlDate,String companymstid);
	List<ReceiptHdr> findByVoucherNumberAndVoucherDate(String voucherNumber, Date voucherDate);
	List<ReceiptHdr> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
	List<ReceiptHdr> findByVoucherDateAndCompanyMstAndBranchCode(Date date, CompanyMst companyMst, String branchcode);
	ReceiptHdr findByVoucherNumberAndCompanyMst(String invoiceno, CompanyMst companyMst);

// here we are finding the recipt summary report between two days
	 @Query(nativeQuery=true,value="select * from recipt_hdr  where date(voucher_date) >= :startDate AND date(voucher_date)<=:endDate")
	 	List<ReceiptHdr>reciptSummaryReport(@Param("startDate")Date startDate,@Param("endDate")Date endDate);
	 
	 @Query(nativeQuery=true,value="SELECT "
	 		+ "s.account_name ,"
				+"p.voucher_number , "
				+ " p.voucher_date ,"
				+ " i.item_name, "
				+ "d.amount "
			    +" from receipt_hdr p, "
			    + "receipt_Dtl d,"
			    + "Item_Mst i,account_heads s"
			    + "  where p.id=d.receipt_hdr_id "
			    + " ")
			 List<Object> getAllReceipts();
 
 	

	 @Query(nativeQuery=true,value="SELECT s.account_name ,"
				+"p.voucher_number ,  p.voucher_date , i.item_name, d.amount "
			    +" from receipt_hdr p, receipt_Dtl d,Item_Mst i,account_heads s"
			    + "  where p.id=d.receipt_hdr_id AND date(h.voucher_date)=:voucherNo "
			    + " ")
	 
	 List<Object> getDailyReceipt(String voucherNo);
	 
	
	 
   	 @Query(nativeQuery=true,value=" select "
   	 		+ " b.branch_name ,"
				+" b.branch_address1,"
				+ " b.branch_place, "
				+ "b.branch_gst,"
				+ "b.branch_tel_no,"
				+ " h.voucher_number,"
				+ " d.amount,"
				+ "h.voucher_date,"
				+ " a.account_name,"
				+ "d.remark,"
				+ " d.instrument_date , "
	         	+ " d.mode_ofpayment,"
	         	+ "d.instnumber "
			    +" from receipt_hdr h, receipt_dtl d, company_mst c , branch_mst b ,account_heads a"
			    +" where h.id=d.receipt_hdr_id  and h.voucher_number=:vouchernumber and "
			    		+ " date(h.voucher_date)=:date and h.company_mst=c.id  "
			    		+ " and c.id= :companymstid  and "
			    		+ " b.branch_code=h.branch_code and a.id=d.account"
			    		)
	 List<Object> receiptsInvoiceByVoucherAndDate(String vouchernumber,Date date, String companymstid);
		
 	 @Query(nativeQuery=true,value=" select rid.invoice_number,rid.received_amount from receipt_invoice_dtl rid, "
 	 		+ "receipt_hdr rd where rid.receipt_hdr = rd.id and rd.voucher_number =:vouchernumber and date(rd.voucher_date) =:date and "
 	 		+ "rd.company_mst =:companymstid")
 			 List<Object> receiptsInvoiceDtlByVoucherAndDate(String vouchernumber,Date date, String companymstid);

   	 @Query(nativeQuery=true,value=" select "
   	 		+ " b.branch_name ,"
				+" b.branch_address1,"
				+ " b.branch_place, "
				+ "b.branch_gst,"
				+ "b.branch_tel_no,"
				+ " h.voucher_number,"
				+ " d.amount,"
				+ "h.voucher_date,"
				+ " a.account_name,"
				+ "d.remark,"
				+ " d.instrument_date , "
	         	+ " d.mode_ofpayment, "
	        	+ " m.member_name ,"
	        	+ " f.family_name ,"
	        	+ " d.instnumber "
			    +" from receipt_hdr h, receipt_dtl d, company_mst c , branch_mst b ,account_heads a,family_mst f,member_mst m "
			    +"  where h.id=d.receipt_hdr_id  and h.voucher_number=:voucherNumber and "
			    		+ " date(h.voucher_date)= :date  and h.company_mst=c.id  "
			    		+ " and c.id= :companymstid  and "
			    		+ " b.branch_code=h.branch_code and a.id=d.account and m.member_id=:memberId and f.family_mst_id=m.family_id"
			    		)
   	 List<Object>getReceiptByVoucherAndDateAndMemberId( String voucherNumber, Date date,String companymstid,String memberId);
			
   	 
   	 List<ReceiptHdr> findByCompanyMstIdAndVoucherDate(String companymst,Date voucherDate);
	
   	 
   	List<ReceiptHdr> findByVoucherDate(Date date);
   	
   	
   	@Query(nativeQuery = true,value="select count(*) from receipt_hdr where voucher_number is NOT NULL and voucher_date=:date")
   	int clientReceiptCount(Date date );
   	
   	
 	@Query(nativeQuery = true,value="select sum(d.amount) from receipt_hdr h,receipt_dtl d where h.id=d.receipt_hdr_id and h.branch_code=:branch and h.voucher_date=:date and h.company_mst=:companymstid")
   	Double getDailyReceiptAmount(Date date,String branch,String companymstid);
 	
 	
 	//version 3.1
 	@Query(nativeQuery = true,value="select "
 			+ "h.voucher_number,"
 			+ "h.voucher_date,"
 			+ "d.amount,"
 			+ "d.instrument_date,"
 			+ "d.instnumber,"
 			+ "d.mode_ofpayment,"
 			+ "b.branch_name from receipt_hdr h,"
 			+ "receipt_dtl d , branch_mst b where  h.id=d.receipt_hdr_id and h.branch_code=b.branch_code and date(h.voucher_date) between :sdate and :edate and h.company_mst=:companyMst and h.branch_code=:branchCode  ")
 	List<Object>getReceiptInvoiceReport(Date sdate, Date edate,CompanyMst companyMst,String branchCode);
//version 3.1 end

 	
 	@Query(nativeQuery = true,value="select *  from receipt_hdr where  voucher_date >= :date")
 	 List<ReceiptHdr> receiptVoucherGreateAndEquel(Date date );
 	
 	

 	@Query(nativeQuery = true,value = "select h.voucher_date,"
 			+ "h.voucher_number,"
 			+ "h.transdate,"
 			+ "h.voucher_type,"
 			+ "d.account,"
 			+ "d.amount,"
 			+ "d.bank_account_number,"
 			+ "d.deposit_bank,d.mode_ofpayment, d.debit_account_id "
 			+ " from receipt_hdr h,"
 			+ "receipt_dtl d where h.id=d.receipt_hdr_id "
 			+ "and h.company_mst=:companyMst "
 			+ "and h.branch_code=:branchCode "
 			+ "and date(h.voucher_date) between :fromDate and :toDate")
 	List<Object>exportReceiptToExcel(CompanyMst companyMst,String branchCode ,Date fromDate,Date toDate);
 	
 	
 	

 	@Query(nativeQuery = true,value = "select h.voucher_date,"
 			+ "h.voucher_number,"
 			+ "h.transdate,"
 			+ "h.voucher_type,"
 			+ "d.account,"
 			+ "d.amount,"
 			+ "d.bank_account_number,"
 			+ "d.deposit_bank,d.mode_ofpayment, d.debit_account_id "
 			+ " from receipt_hdr h,"
 			+ "receipt_dtl d where h.id=d.receipt_hdr_id "
 			+ "and h.company_mst=:companyMst "
 			+ "and h.branch_code=:branchCode "
 			+ "and date(h.voucher_date) between :fromDate and :toDate")
 	List<Object>receiptReportPrinting(CompanyMst companyMst,String branchCode ,Date fromDate,Date toDate);
 	
 	
 	@Query(nativeQuery = true,value="select * from receipt_hdr h where h.company_mst=:companyMst and h.branch_code=:branchCode and date(h.voucher_date) between :fromDate and :toDate ")
 	List<ReceiptHdr>getReceiptHdrReport(CompanyMst companyMst,String branchCode ,Date fromDate,Date toDate);
 	
	//-------------------version 6.5 surya 

	ReceiptHdr findByVoucherNumberAndCompanyMstAndBranchCode(String invoiceno, CompanyMst companyMst,
			String branchcode);
	//-------------------version 6.5 surya end

	 @Query(nativeQuery=true,value=" select m.member_id,m.member_name,f.family_name,m.head_of_family,m.mobile_no,m.email,a.account_name from member_mst m,family_mst f,company_mst c,account_heads a where m.member_id not in(select member_id from receipt_dtl  where  account=:accountId and trans_date >=:sdate and trans_date <= :eDate ) and c.id=:companymstid and f.family_mst_id=m.family_id and a.id=:accountId") 
		List<Object> dueAmountReport(String companymstid, Date sdate, Date eDate, String accountId);
		

 	@Query(nativeQuery = true,value="select * from receipt_hdr "
 			+ "where date(voucher_date)>=:fdate and date(voucher_date)<=:tdate ")
 	List<ReceiptHdr> getReceiptHdrBetweenDate(Date fdate,Date tdate);



	 @Query(nativeQuery=true,value="select m.member_name,d.amount,f.family_name,m.head_of_family,e.event_name from receipt_dtl d,receipt_hdr h,event_mst e,member_mst m,family_mst f where h.id=d.receipt_hdr_id and e.event_id=h.event_id and h.event_id=:eventId and h.company_mst=:companymstid and f.family_mst_id=m.family_id and d.member_id=m.member_id")
	 
		List<Object> eventReport(String companymstid, String eventId);
		

	 @Query(nativeQuery=true,value=" select m.member_id,m.member_name,f.family_name,a.account_name,sum(r.amount),o.org_name from member_mst m,family_mst f,company_mst c,receipt_dtl r,org_mst o,account_heads a  where date(r.trans_date)>=:sdate and date(trans_date)<=:eDate and  c.id=:companymstid and f.family_mst_id=m.family_id "
			                      +" and m.member_id=r.member_id and f.family_mst_id=m.family_id and o.org_id=m.org_id and a.id=r.account and r.account=:accountId  group by m.member_id,m.member_name,f.family_name,a.account_name,o.org_name") 

	 List<Object> orgPayMemberWiseReport(String companymstid, Date sdate, Date eDate, String accountId);
	 
	 @Query(nativeQuery = true,value = "select m.member_name,fm.family_name,a.account_name,rd.amount,rd.trans_date from member_mst m,receipt_hdr rh,receipt_dtl rd ,family_mst fm , account_heads a where rh.id=rd.receipt_hdr_id and rh.member_id=m.member_id and fm.family_mst_id =m.family_id and rd.account=a.id and rh.member_id=:memberId and rh.company_mst=:companymstid and date(rd.trans_date) >=:SDate and date(rd.trans_date)<=:eDate"
	 		)
	 List<Object>getMemberWiseReceiptReport(String companymstid, Date SDate, Date eDate,
				String memberId);
//
	 
	 
	 
	 @Query(nativeQuery = true,value="select  h.voucher_date,h.voucher_number,h.transdate,h.voucher_type,h.id from receipt_hdr h  where h.company_mst=:companyMst and h.branch_code=:branchCode and date(h.voucher_date) between :fromDate and :toDate ")
	 	List<Object>fetchReceiptHdrReport(CompanyMst companyMst,String branchCode ,Date fromDate,Date toDate);
	 
	 
	 
	 
	  @Query
	  (nativeQuery = true,value = "select sum(sr.AMOUNT) as receipt_amount,sr.MODE_OFPAYMENT "
	  		+ " from receipt_dtl sr , receipt_hdr sh "
	  		+ " where sh.id= sr.RECEIPT_HDR_ID and sh.branch_code =:branchcode and "
	  		+ " sh.company_mst=:companyMst and sh.voucher_number is not null "
	  		+ " and date(sh.voucher_date)=:sdate and sr.MODE_OFPAYMENT in (select r.RECEIPT_MODE from receipt_mode_mst r "
	  		+ " where r.CREDIT_CARD_STATUS='YES' and r.COMPANY_MST=:companyMst) group by sr.MODE_OFPAYMENT")
	List<Object> totalReceiptCardAmount(CompanyMst companyMst, String branchcode, Date sdate);
	 	
	  
	  
	  @Query
	  (nativeQuery = true,value="select h.voucher_date,h.voucher_number,a.account_name,m.member_name,d.amount  from receipt_hdr h, receipt_dtl d,member_mst m,account_heads a where  h.id=d.receipt_hdr_id and d.member_id=m.member_id and h.company_mst=:companyMst and a.id=d.account and h.branch_code=:branchCode and date(h.voucher_date) >=:fromDate and date(voucher_date) <=:toDate  ")
	  List<Object>orgReceiptReport(CompanyMst companyMst,String branchCode ,Date fromDate,Date toDate);
	  //CompanyMst companyMst,String branchCode ,Date fromDate,Date toDate
	  //and h.company_mst=:companyMst and a.id=d.account and h.branch_code=:branchCode and date(h.voucher_date) >=:fromDate and date(voucher_date) <=:toDate

	  @Query
	  (nativeQuery = true,value="select h.voucher_date,h.voucher_number,a.account_name,m.member_name,d.amount  from receipt_hdr h, receipt_dtl d,member_mst m,account_heads a where  h.id=d.receipt_hdr_id and d.member_id=m.member_id and h.company_mst=:companyMst and a.id=d.account and h.branch_code=:branchCode and date(h.voucher_date) >=:fromDate and date(voucher_date) <=:toDate and d.account=:accountId ")
	
List<Object>orgReceiptReportAccountWise(CompanyMst companyMst,String branchCode, String accountId ,Date fromDate,Date toDate);

	  
	  void  deleteByVoucherNumber(String voucherNumber);
	  
	  @Query(nativeQuery = true,value="select * from receipt_hdr where voucher_number=:invoiceno and "
	 			+ "company_mst=:companyMst and branch_code=:branchcode and date(voucher_date)=:date")
		ReceiptHdr findByVoucherNumberAndCompanyMstAndBranchCodeAndVoucherDate(String invoiceno, CompanyMst companyMst,
				String branchcode, Date date);
}

