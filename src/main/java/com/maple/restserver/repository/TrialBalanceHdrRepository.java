package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.TrialBalanceHdr;

public interface TrialBalanceHdrRepository extends JpaRepository<TrialBalanceHdr, String>{

	
	
	@Query(nativeQuery = true, value = "SELECT * FROM trial_balance_hdr WHERE report_id = :reportId")
	TrialBalanceHdr getTrialBalanceHdrByReportId(String reportId);

	@Query(nativeQuery = true, value ="SELECT * FROM trial_balance_hdr WHERE from_date >= :startdate AND to_date <= :startdate AND user_id = :userId")
	List<Object> getReportIdByUserIdAndDate(Date startdate, String userId);


}
