package com.maple.restserver.repository;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.maple.restserver.entity.GroupMst;
import com.maple.restserver.entity.GroupPermissionMst;
@Component
@Repository
public interface GroupPermissionRepository extends JpaRepository<GroupPermissionMst,String> {

	List<GroupPermissionMst>findByProcessId(String processId);
	
	List<GroupPermissionMst>findByGroupIdAndCompanyMstId(String groupId,String companymstid);
	List<GroupPermissionMst> findByCompanyMstId(String companymstid);
	
	List<GroupPermissionMst> findByProcessIdAndCompanyMstId(String id,String companymstid);
	
public void	deleteByIdAndCompanyMstId(String id,String companymstid);
	
}
