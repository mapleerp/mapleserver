package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.LmsQueueTallyMst;
import com.maple.restserver.entity.ReceiptHdr;

 
@Transactional
@Repository
public interface LmsQueueMstRepository extends JpaRepository<LmsQueueMst, String>{

//--------------------- version 1.1	
	
	LmsQueueMst findBySourceObjectId(String id);
	LmsQueueMst findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,
			String voucherType);
	
	 @Query(nativeQuery=true,value="select * from lms_queue_mst where voucher_number = :voucherNumber "
	 		+ "and date(voucher_date)=:voucherDate and voucher_type =:voucherType") 
	LmsQueueMst findByVoucherNumberAndVDateAndVtypt(String voucherNumber, Date voucherDate,
			String voucherType);

	
	 @Query(value=" select p from LmsQueueMst p where p.postedToServer='NO' ")
	 	List<LmsQueueMst>findLmsQueueMstNotPosted();


	 @Query(nativeQuery=true,value=" select * from lms_queue_mst l "
	 		+ "where l.VOUCHER_TYPE=:vouchertype and "
	 		+ "l.COMPANY_MST=:companymstid and date(VOUCHER_DATE)>=:fdate and date(VOUCHER_DATE)<=:tdate  ")
	List<LmsQueueMst> findByCompanyMstAndVoucherTypeBetweenDate(String companymstid, String vouchertype, java.util.Date fdate,
			java.util.Date tdate);
	 
	 @Query(nativeQuery=true,value=" select distinct l.VOUCHER_TYPE from lms_queue_mst l "
		 		+ "where "
		 		+ "l.COMPANY_MST=:companymstid  ")
	List<String> LmsQueueMstDistinctVoucherType(String companymstid);
	 
	 
	 @Query(nativeQuery=true,value=" select * from lms_queue_mst l "
		 		+ "where l.VOUCHER_TYPE=:vouchertype and "
		 		+ "l.COMPANY_MST=:companymstid and date(VOUCHER_DATE)>=:fudate and date(VOUCHER_DATE)<=:tudate and "
		 		+ "l.posted_to_server=:status  order by VOUCHER_DATE DESC")
	List<LmsQueueMst> getLmsQueueMstByVoucherTypeAndStatus(String vouchertype, String status, java.util.Date fudate,
			java.util.Date tudate, String companymstid);
	 
	 @Query(nativeQuery=true,value=" select * from lms_queue_mst l "
		 		+ "where l.VOUCHER_TYPE=:vouchertype and "
		 		+ "l.COMPANY_MST=:companymstid and date(VOUCHER_DATE)>=:fudate and date(VOUCHER_DATE)<=:tudate and "
		 		+ "l.posted_to_server=:status order by VOUCHER_DATE DESC ")
	List<LmsQueueMst> findSourceObjectIdByVoucherTypeAndDateAndStatus(String vouchertype, String status,
			java.util.Date fudate, java.util.Date tudate, String companymstid);

/*
 *  @Query(nativeQuery=true,value=" select * from lms_queue_mst  "
		 		+ "where repeat_time < 60003 and posted_to_server = 'NO'  ")
 */

	 @Query(value=" select p from LmsQueueMst p where p.postedToServer='NO' and repeatTime < 60003")
	List<LmsQueueMst> findPendingLms(Pageable topFifty);


	 

	 
}
