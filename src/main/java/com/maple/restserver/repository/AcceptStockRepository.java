package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
@Component
@Repository
public interface AcceptStockRepository extends JpaRepository<AcceptStock, String>{
	
	
	List<AcceptStock>findByCompanyMst(String companymstid);
//	List<AcceptStock> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
	
	
	

}
