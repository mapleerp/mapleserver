package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.StockTransferInDtl;

@Repository
public interface StockTransferInDtlRepository extends JpaRepository<StockTransferInDtl, String>{

	List<StockTransferInDtl>findBystockTransferInHdrIdAndCompanyMst(String stocktransferinhdrId,CompanyMst companymstid);
	
	List<StockTransferInDtl>findByCompanyMst(String companymstid);
	
	List<StockTransferInDtl>findByCompanyMstAndStockTransferInHdrVoucherNumberAndStockTransferInHdrBranchCode(CompanyMst companyMst,String vouchernumber,String branchcode);

	
	 @Query(nativeQuery = true, value = "SELECT h.voucher_number,h.voucher_date,h.from_branch,i.item_name, "
	 		+ "c.category_name,i.item_code, d.batch,d.expiry_date,d.qty,u.unit_name,d.rate,sum(d.mrp*d.qty) "
	 		+ "  FROM stock_transfer_in_hdr h, stock_transfer_in_dtl d, item_mst i, category_mst c, unit_mst u "
	 		+ " where h.id=d.stock_transfer_in_hdr and i.id=d.item_id and u.id=d.unit_id and c.id=i.category_id "
	 		+ " and h.branch_code=:branchcode and h.company_mst=:companyMst and date(h.voucher_date)<=:tdate and "
	 		+ " date(h.voucher_date)>=:fdate group by h.voucher_number,h.voucher_date,h.from_branch,i.item_name,"
	 		+ " c.category_name,i.item_code, d.batch,d.expiry_date,d.qty,u.unit_name,d.rate "
	    		)
	List<Object> getTransferInDetailReport(CompanyMst companyMst, String branchcode, java.util.Date fdate,
			java.util.Date tdate);
	 
	 

	 @Query(nativeQuery = true, value = "SELECT h.voucher_number,h.voucher_date,h.from_branch,i.item_name, "
		 		+ "c.category_name,i.item_code, d.batch,d.expiry_date,d.qty,u.unit_name,d.rate,sum(d.mrp*d.qty) "
		 		+ "  FROM stock_transfer_in_hdr h, stock_transfer_in_dtl d, item_mst i, category_mst c, unit_mst u "
		 		+ " where h.id=d.stock_transfer_in_hdr and i.id=d.item_id and u.id=d.unit_id and c.id=i.category_id "
		 		+ " and h.branch_code=:branchcode and h.company_mst=:companyMst and  "
		 		+ " h.from_branch=:frombranchcode and date(h.voucher_date)<=:tdate and  "
		 		+ " date(h.voucher_date)>=:fdate group by h.voucher_number,h.voucher_date,h.from_branch,i.item_name,"
		 		+ " c.category_name,i.item_code, d.batch,d.expiry_date,d.qty,u.unit_name,d.rate "
		    		)
	List<Object> getTransferInDetailReportByFromBranch(CompanyMst companyMst, String branchcode, String frombranchcode,
			java.util.Date fdate, java.util.Date tdate);

	 
	 @Query(nativeQuery = true, value = "SELECT h.voucher_number,h.voucher_date,h.from_branch,i.item_name, "
		 		+ "c.category_name,i.item_code, d.batch,d.expiry_date,d.qty,u.unit_name,d.rate,sum(d.mrp*d.qty) "
		 		+ "  FROM stock_transfer_in_hdr h, stock_transfer_in_dtl d, item_mst i, category_mst c, unit_mst u "
		 		+ " where h.id=d.stock_transfer_in_hdr and i.id=d.item_id and u.id=d.unit_id and c.id=i.category_id "
		 		+ " and h.branch_code=:branchcode and h.company_mst=:companyMst and  "
		 		+ " h.from_branch=:frombranch and date(h.voucher_date)<=:tdate and  "
		 		+ " date(h.voucher_date)>=:fdate "
		 		+ " and i.category_id=:categoryids "
		 		+ " group by h.voucher_number,h.voucher_date,h.from_branch,i.item_name,"
		 		+ " c.category_name,i.item_code, d.batch,d.expiry_date,d.qty,u.unit_name,d.rate "
		    		)
	List<Object> getTransferInDetailReportByFromBranchAndCategory(CompanyMst companyMst, String branchcode,
			String frombranch, String categoryids, java.util.Date fdate, java.util.Date tdate);

	 @Query(nativeQuery = true, value = "SELECT h.voucher_number,h.voucher_date,h.from_branch,i.item_name, "
		 		+ "c.category_name,i.item_code, d.batch,d.expiry_date,d.qty,u.unit_name,d.rate,sum(d.mrp*d.qty) "
		 		+ "  FROM stock_transfer_in_hdr h, stock_transfer_in_dtl d, item_mst i, category_mst c, unit_mst u "
		 		+ " where h.id=d.stock_transfer_in_hdr and i.id=d.item_id and u.id=d.unit_id and c.id=i.category_id "
		 		+ " and h.branch_code=:branchcode and h.company_mst=:companyMst and  "
		 		+ " date(h.voucher_date)<=:tdate and  "
		 		+ " date(h.voucher_date)>=:fdate "
		 		+ " and i.category_id=:categoryids "
		 		+ " group by h.voucher_number,h.voucher_date,h.from_branch,i.item_name,"
		 		+ " c.category_name,i.item_code, d.batch,d.expiry_date,d.qty,u.unit_name,d.rate "
		    		)
	List<Object> getTransferInDetailReportByCategory(CompanyMst companyMst, String branchcode, String categoryids,
			java.util.Date fdate, java.util.Date tdate);

	StockTransferInDtl findByItemIdAndStockTransferInHdr(ItemMst id, String stockTransferInHdr);
	
	StockTransferInDtl	findBystockTransferInHdrIdAndItemId(String stockTransferInHdrId ,ItemMst itemId);
	
	
}
