//package com.maple.restserver.repository;
//import java.util.Date;
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import com.maple.restserver.entity.CompanyMst;
//
//@Repository
//public interface SupplierRepository extends JpaRepository<Supplier, String> {
//
//	@Query("SELECT p FROM Supplier p WHERE LOWER(p.supplierName) like   LOWER(:lastName) AND p.companyMst.id=:companymstid ")
//    public List<Supplier> findSearch(@Param("lastName") String lastName,String companymstid);
//	
//	List<Supplier> findByCompanyMstId(String companymstid);
//	
//
//	
//	 Optional<Supplier> findByIdAndCompanyMstId(String id,String companymstid);
////	List<Supplier> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
//
//	public Supplier findBySupplierNameAndCompanyMst(String supplierName, CompanyMst companymst);
//
//	
//	
//	@Query(nativeQuery = true,value="select * from supplier s where s.account_id=:account_id and company_mst=:companyMst")
//	public Supplier findByAccount_idAndCompanyMst(String account_id, CompanyMst companyMst);
//
//	
//	 @Query(nativeQuery = true,value="select "
//	 		+ "c.company_name,b.branch_name,b.branch_address1,"
//	 		+ "b.branch_address2,b.branch_state,b.branch_gst,"
//	 		+ "b.branch_email,b.branch_website,b.branch_tel_no,"
//	 		+ "s.supplier_name,s.company,s.phone_no,s.emailid,l.remark,"
//	 		+ "l.debit_amount,l.credit_amount,l.source_voucher_id,"
//	 		+ "l.trans_date,sum(l.credit_amount)-sum(l.debit_amount) as pendingAmount from "
//	 		+ "company_mst c,branch_mst b,supplier s,ledger_class l "
//	 		+ "where b.branch_code=:branchcode and s.id=:supplierid and c.id=:companymstid and "
//	 		+ "l.account_id =:supplierid and date(l.trans_date) between :fromDate and :toDate group "
//	 		+ "by c.company_name,b.branch_name,b.branch_address1,b.branch_address2,"
//	 		+ "b.branch_state,b.branch_gst,"
//	 		+ "b.branch_email,b.branch_website,b.branch_tel_no,"
//	 		+ "s.supplier_name,s.company,s.phone_no,s.emailid,l.remark,"
//	 		+ "l.debit_amount,l.credit_amount ,l.source_voucher_id ,l.trans_date")
//	List<Object>getMonthlySupplierLedgerReport(String companymstid,String supplierid,String branchcode,Date fromDate ,Date toDate);
//
//
//
//		@Query(nativeQuery=true,value="select sum(credit_amount) - sum(debit_amount)  "
//				+ "from ledger_class where trans_date < :startDate and "
//				+ "  account_id =:supplierid and company_mst =:companymstid ")  
//			 
//	    Double openingBalance(String companymstid,Date startDate,String supplierid);
//
//		 @Query(nativeQuery = true,value=" select  a.voucher_number  , s.supplier_name, s.phone_no,"
//		 		+ " s.emailid, a.voucher_date, a.due_date, a.due_amount, a.paid_amount, "
//		 		+ "s.company,"
//		 		+ "( a.due_amount-a.paid_amount) as pendingAmont from account_payable a , "
//		 		+ "supplier s, company_mst c   where a.account_id = s.account_id  and "
//		 		+ "a.company_mst  = c.id and a.account_id=:supplierid"
//		 		+ " and c.id=:companymstid"
//		 		+ "  and ( a.due_amount-a.paid_amount) != 0.0"
//		 		+ " and a.voucher_date between :fromDate and :toDate ")
//		public List<Object> getSupplierDueDayReport(String companymstid, String supplierid,
//				Date fromDate, Date toDate);
//
//		 
//		 @Query(nativeQuery = true,value=" select "
//		 		+ "sum(l.credit_amount)-sum(l.debit_amount) from ledger_class l,"
//		 		+ " supplier s, company_mst c "
//		 		+ "where l.account_id=s.account_id "
//		 		+ "and l.company_mst=c.id "
//		 		+ "and s.account_id=:supplierid "
//		 		+ "and c.id=:companymstid "
//		 		+ "and l.trans_date<:fromDate ")
//		public Double getOpeningBalanceFromLedgerClass(String companymstid, String supplierid, Date fromDate);
//
//		public Supplier findByPhoneNoAndCompanyMst(String phoneno, CompanyMst companyMst);
//
//		 
//	public  Supplier findByPhoneNo(String phoneNo);
//
//
//	@Query(nativeQuery = true,value="select * from supplier OFFSET :nCount ROWS FETCH NEXT 1 ROW ONLY ")
// 
//    Supplier fechRandomSupplier(int nCount);
//
//	@Query(nativeQuery = true,value="select count(id) from supplier")
//int supplierCount();
//
//	@Query(nativeQuery = true,value="select  s.supplier_name,s.id   from supplier s,"
//			+ " account_heads a where a.account_name = s.supplier_name and a.id <> s.id")
//	public List<Object> getSupplierListNotInAccountHeads();
//
//	@Modifying
//	@Query(nativeQuery = true,value = "update supplier set id =:id where id=:supId")
//	public void updateSupplierId(String supId, String id);
//}
//
// 
