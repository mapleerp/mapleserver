package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ItemLocationMst;
@Repository
public interface ItemLocationMstRepository extends JpaRepository<ItemLocationMst,String>{

	List<ItemLocationMst> findByFloor(String floor);
	
	List<ItemLocationMst> findByFloorAndShelf(String floor,String shelf);
	
	List<ItemLocationMst> findByFloorAndShelfAndRack(String floor,String shelf,String rack);
	List<ItemLocationMst> findByItemId(String itemid);
	
	@Query(nativeQuery = true,value ="select distinct(floor) from item_location_mst where floor is not null")
	List<String> getDisctinctFloor();
	
	@Query(nativeQuery = true,value ="select distinct(shelf) from item_location_mst where shelf is not null")
	List<String> getDisctinctShelf();
	
	@Query(nativeQuery = true,value ="select distinct(rack) from item_location_mst where rack is not null")
	List<String> getDisctinctRack();
	
	ItemLocationMst findByItemIdAndFloorAndShelfAndRack(String itemid,String floor,String shelf,String rack);

	List<ItemLocationMst> findByItemIdAndBranchCode(String itemid, String branchcode);
}
