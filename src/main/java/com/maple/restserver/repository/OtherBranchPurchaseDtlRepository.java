package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.OtherBranchPurchaseDtl;
@Repository
public interface OtherBranchPurchaseDtlRepository extends JpaRepository<OtherBranchPurchaseDtl,String>{

	List<OtherBranchPurchaseDtl> findByOtherBranchPurchaseHdrId(String purchasehdrId);
	
	
    @Query(nativeQuery = true, value = "SELECT SUM(amount) as totalAmount, "
    		+ " SUM(qty) as totalQty,SUM(discount) as totalDiscount,"
    		+ " SUM(tax_Amt) as totalTax,SUM(cess_Amt) as totalCessAmt  "
    		+ " FROM other_branch_purchase_dtl d, other_branch_purchase_hdr h "
    		+ " "

    		+ " where h.id = d.other_branch_purchase_hdr_id AND h.id =:hdrid")
	List<Object> findSumPurchaseDetail(String hdrid);

}
