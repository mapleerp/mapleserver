package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.StockTransferOutDltdDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
@Component
@Repository
public interface StockTransferOutDltdDtlRepository extends JpaRepository<StockTransferOutDltdDtl, String>{

	
	@Query(nativeQuery=true,value="SELECT s.stock_transfer_out_hdr from stock_transfer_out_dltd_dtl s,"
			+ "stock_transfer_out_hdr h where date(h.voucher_date)=:edate and s.company_mst=:companymstid and "
			+ "s.stock_transfer_out_hdr=h.id and h.voucher_number is not null group by s.stock_transfer_out_hdr  "
			+ " "
			)
	List<String> findByCompanyMstAndDate(String companymstid, java.util.Date edate);


	List<StockTransferOutDltdDtl> findByCompanyMstAndStockTransferOutHdr(CompanyMst companyMst,
			StockTransferOutHdr stockTransferOutHdr);
	
	
	

}
