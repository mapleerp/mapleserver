package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ProductConversionConfigMst;

@Repository
public interface ProductConfigurationMstRepository extends JpaRepository<ProductConversionConfigMst, String> {

ProductConversionConfigMst findByFromItemIdAndToItemId(String fromitemid,String toitemid);


}
