package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.SalesDtlDeleted;
@Component
@Repository
public interface SalesDtlDeletedRepository extends JpaRepository<SalesDtlDeleted, String>{
	


}
