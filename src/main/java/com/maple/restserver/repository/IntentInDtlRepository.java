package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.entity.IntentInDtl;
import com.maple.restserver.entity.PurchaseDtl;
@Component
@Repository
public interface IntentInDtlRepository extends JpaRepository<IntentInDtl, String>{

	List<IntentInDtl> findByIntentInHdrId(String intentHdr );

    Optional<IntentInDtl> findByIdAndId(String id, String intentHdrId);
    
    public void  deleteByItemId(String itemId);

    
    @Query(nativeQuery = true,value = "select i.item_name,sum(qty) from intent_in_dtl d, intent_in_hdr h, item_mst i where "
    		+ "i.id=d.item_id and d.intent_in_hdr=h.id and date(h.in_voucher_date)=:fdate "
    		+ "and h.in_voucher_number is not null and "
    		+ "h.branch_code=:branchcode and company_mst=:companymstid group by i.item_name")
	List<Object> getItemWiseIntentSummary(String branchcode, String companymstid, java.util.Date fdate);

 //   List<IntentDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
