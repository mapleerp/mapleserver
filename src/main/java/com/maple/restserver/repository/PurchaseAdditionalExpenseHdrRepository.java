package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.PurchaseAdditionalExpenseHdr;
import com.maple.restserver.entity.PurchaseDtl;

@Repository
@Controller

public interface PurchaseAdditionalExpenseHdrRepository extends JpaRepository<PurchaseAdditionalExpenseHdr, String>{

	PurchaseAdditionalExpenseHdr findByPurchaseDtl(PurchaseDtl dtlid);

	void deleteByPurchaseDtlId(String dtlid);

}
