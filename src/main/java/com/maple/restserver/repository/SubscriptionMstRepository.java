package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SubscriptionMst;

@Repository
public interface SubscriptionMstRepository extends JpaRepository<SubscriptionMst,String> {


}
