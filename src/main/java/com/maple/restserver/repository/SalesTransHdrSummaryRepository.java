package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.OtherBranchSalesTransHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SalesTransHdrSummary;

@Repository
public interface SalesTransHdrSummaryRepository extends JpaRepository<SalesTransHdrSummary, String> {
	
}
