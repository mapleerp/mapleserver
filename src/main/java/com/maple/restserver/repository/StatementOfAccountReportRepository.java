package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.accounting.entity.LedgerClass;

public interface StatementOfAccountReportRepository extends JpaRepository<LedgerClass, String>{

	 
	 
		/*
		 * @Query(nativeQuery=true,
		 * value=" select a.account_name,l.debit_amount,l.credit_ammount,l.remark from  account_heads a"
		 * + " INNER JOIN ledger_class l ON a.id=l.account_id")
		 */
		 
		/*  Opening :
		 * select sum(credit_amount) - sum(debit_amount) 
		 * from ledger_class where trans_date < :StartDate and account_id  = :accountID
		 *   and company_mst = 'AMB' ;
		 */
		
		
		/*
		 *  select l.trans_date, a.account_name, round(l.debit_amount,2), round(l.credit_amount,2), "
				+ "  l.remark from account_heads a,ledger_class l where "
				+ "   a.id=l.account_id and a.id = :accountID and l.trans_date >= :startDate and trans_date <= :endDate and 
				company_mst = :CompanyId
		 */
		
		/*  Closing :
		 * select sum(credit_amount) - sum(debit_amount) 
		 * from ledger_class where trans_date < :endDate and account_id  = :accountID
		 *   and company_mst = 'AMB' ;
		 */
	
	@Query(nativeQuery=true,value="select sum(credit_amount) - sum(debit_amount) \n" + 
			"		 * from ledger_class where date(trans_date) <:startDate and account_id  =:acconuntId\n" + 
			"		 *   and company_mst =:companymstid")
		Double openingBalance(String companymstid,Date startDate,String acconuntId);
		
		@Query(nativeQuery=true,value="   select l.trans_date, a.account_name,  (l.debit_amount   ),  (l.credit_amount   ), \"\n" + 
				"				+ \"  l.remark from account_heads a,ledger_class l where \"\n" + 
				"				+ \"   a.id=l.account_id and a.id =:acconuntId and date(l.trans_date) >=:startDate and date(trans_date) <= :endDate and \n" + 
				"				company_mst =:companymstid ")
	List<Object> statementOfAccountReport(String companymstid,Date startDate,Date endDate,String acconuntId);
    
		
		@Query(nativeQuery=true,value=" select sum(credit_amount) - sum(debit_amount) \n" + 
				"		 * from ledger_class where date(trans_date) <:endDate and account_id  =:acconuntId\n" + 
				"		 *   and company_mst =:companymstid")
		Double ClosingBalance(String companymstid,Date endDate,String acconuntId);

}
