package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SessionEndClosureMst;

@Repository
public interface SessionEndClosureMstRepository extends JpaRepository<SessionEndClosureMst, String> {

	
	
	// @Query(nativeQuery=true,value="Select * from session_end_closure_mst m where m.session_date=:date and user_id=:userId")
//	List<SessionEndClosureMst> retriveSessionEndClosureMst();
	 SessionEndClosureMst findByUserIdAndSessionDate(String userId,Date date);
	 
	
	  @Query(nativeQuery=true,
	  value="Select * from session_end_closure_mst m where date(m.session_date)=:date and user_id=:userId"
	  ) SessionEndClosureMst getSessionEndClosureMst(Date date,String userId);
	  
	  
	  @Query(nativeQuery=true,value=
	  "SELECT sum(A.cashSale) as CASH , sum(A.card) AS CARD  , " +
	  "sum(a.temp)  as CARD2   FROM " + " " + "" +
	  "( select sum(h.cash_pay) as cashSale ," +
	  "sum(h.cardamount) as card  , 0 as temp \n" + " FROM " +
	  "  SALES_TRANS_HDR  h  WHERE date(h.voucher_date) = :vDate and h.user_id=:userId " +
	  " union \n" +
	  "SELECT 0 as cashSale ,0 card , sum(qty*MRP)  as temp  from sales_dtl  d , sales_trans_hdr h where h.id = d.sales_trans_hdr_id and   d.item_id  in (select item_id from item_temp_mst) \n"
	  + " and  date(h.voucher_date) = :vDate and h.user_id=:userId ) " + "" + "A" + " ")
	  
	  List<Object> getSessionEndDetails(Date vDate,String userId);
	  
	

	  @Query(nativeQuery=true,value=
			  "select max(sl_no) from session_end_closure_mst where company_mst=:companymstid and "
			  + "date(session_date)=:udate")
	Integer findMaxOfSlNo(Date udate, String companymstid);


	SessionEndClosureMst findByIdAndCompanyMst(String id, CompanyMst companyMst);

	@Query(nativeQuery=true,value=
			  "select * from session_end_closure_mst where company_mst=:companyMst and "
			  + "date(session_date)=:date and sl_no=:slno")
	SessionEndClosureMst retrieveSessionEndClosureByDateAndSlno(Date date, String slno, CompanyMst companyMst);
	 
}
