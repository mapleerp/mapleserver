package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionMst;

public interface ProductionDetailRepository extends JpaRepository<ProductionDtl, String> {

 
	
	
		
		  @Query(nativeQuery=true,
		  value=" select d.item_id,d.batch,d.qty, from production_dtl d"
		  + " INNER JOIN production_mst m ON d.id=m.production_mst_id and (date(m.voucher_date)<=:startDate "
		  )
		 
	/*
	 * @Query(nativeQuery=true,
	 * value="from production_dtl  where voucher_date BETWEEN:startDate AND :endDate"
	 * )
	 */
	 	List<ProductionDtl>productionDtlSummary(@Param("startDate") Date startDate);
	 	
	
			List<ProductionDtl>findByProductionMstVoucherDate(Date productiondate);
			
			

			  @Query(nativeQuery=true,
			  value=" select d.id , "
			  		+ " d.batch ,"
			  		+ " d.item_id,"
			  		+ " d. qty , "
			  		+ " d.production_mst_id ,"
			  		+ " d.status,"
			  		+ "d.store "
			  		+ " from production_dtl d , "
			  + "   production_mst m  where d.production_mst_id = m.id and "
			  + " date(m.voucher_date) = :date and "
			  + "d.status = 'N' and m.voucher_number is not null"
			  )
			 
			List<Object>ProductionKitDtlByDate(Date date );
			  
			  
	
			  
			  List<ProductionDtl>  findByProductionMst(ProductionMst productionMst);

			  @Query(nativeQuery=true, value=" select sum(d.qty) as qty, "
			  		+ "h.voucher_date from production_dtl d, production_mst h "
			  		+ "where d.production_mst_id = h.id and d.item_id=:itemid and "
			  		+ "h.branch_code=:branchcode and date(h.voucher_date)>=:fudate and "
			  		+ "date(h.voucher_date)<=:tudate group by h.voucher_date ")
			List<Object> getSumOfPlaningQty(Date fudate, Date tudate, String itemid, String branchcode);
			  
			  
			  
			  @Query(nativeQuery=true,
					  value=" select d.id , "
					  		+ " d.batch ,"
					  		+ " d.item_id,"
					  		+ " d. qty , "
					  		+ " d.production_mst_id ,"
					  		+ " d.status,"
					  		+ "d.store "
					  		+ " from production_dtl d , "
					  + "   production_mst m  where d.production_mst_id = m.id and "
					  + " date(m.voucher_date) = :date and "
					  + "d.status = 'N' and m.voucher_number is not null and m.store=:store"
					  )
					 
					List<Object>ProductionKitDtlByDateAndStore(Date date,String store);
}
