package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AddKotTable;
import com.maple.restserver.entity.OrderTakerMst;

@Repository
public interface OrderTakerMstRepository extends JpaRepository<OrderTakerMst, String>{

	
List<OrderTakerMst>	findByCompanyMstId(String companymstid);

OrderTakerMst findByCompanyMstIdAndOrderTakerName(String companymstid,String delivaryboyname);


@Query(nativeQuery=true,value="select *from order_taker_mst o where o.status ='ACTIVE' and o.company_mst=:companymstid") 
	List<OrderTakerMst> getOrderTakerbyStatus(String companymstid);

OrderTakerMst findByCompanyMstIdAndId(String companymstid, String id);

}
