package com.maple.restserver.repository;


import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesTransHdr;
 
@Component
@Repository
public interface AccountReceivableRepository extends JpaRepository<AccountReceivable, String> , AccountReceivableRepositoryCustom {
	List<AccountReceivable> findByCompanyMstId(String companymstid);
	
	 @Query(nativeQuery=true,value="select * from account_receivable ar where ar.company_mst=:companyMst and ar.account_id=:accountId and ar.due_amount-ar.paid_amount!=0")
	List<AccountReceivable>  findByCompanyMstAndAccountId(CompanyMst companyMst, String accountId) ;
	
	AccountReceivable findByCompanyMstAndVoucherNumber(CompanyMst companyMst,String vouchernumber) ;
	List<AccountReceivable>  findByCompanyMstAndSalesTransHdr(CompanyMst companyMst,SalesTransHdr salesTransHdr) ;

	
	AccountReceivable	findByVoucherNumber(String vouchernumber);
	
	
	AccountReceivable findByCompanyMstAndId(CompanyMst companyMst,String  id) ;

//	 @Query(nativeQuery=true,value="select c.customer_name,"
//	 		+ "ac.voucher_number,ac.due_amount-ac.paid_amount,"
//	 		+ "ac.remark,ac.voucher_date from customer_mst c,"
//	 		+ "account_receivable ac "
//	 		+ "where c.id=ac.account_id and date(ac.voucher_date) between :fDate and :tDate and "
//	 		+ "ac.company_mst = :companyMst and ac.customer_mst = :id and ac.due_amount-ac.paid_amount > 0") 
	
	 @Query(nativeQuery=true,value="select c.account_name,"
		 		+ "ac.voucher_number,ac.due_amount-ac.paid_amount,"
		 		+ "ac.remark,ac.voucher_date from account_heads c,"
		 		+ "account_receivable ac "
		 		+ "where c.id=ac.account_id and date(ac.voucher_date) between :fDate and :tDate and "
		 		+ "ac.company_mst = :companyMst and ac.account_heads = :id and ac.due_amount-ac.paid_amount > 0") 
	List<Object> findByCustomerBalanceById(CompanyMst companyMst, String id, Date fDate,
			Date tDate);
	 
	 
//	 @Query(nativeQuery=true,value="select c.customer_name,"
//		 		+ "ac.voucher_number,ac.due_amount-ac.paid_amount,"
//		 		+ "ac.remark,ac.voucher_date from customer_mst c,"
//		 		+ "account_receivable ac "
//		 		+ "where c.id=ac.account_id and date(ac.voucher_date) between :fDate and :tDate and "
//		 		+ "ac.company_mst = :companyMst and ac.due_amount-ac.paid_amount > 0") 
	 
	 @Query(nativeQuery=true,value="select c.account_name,"
		 		+ "ac.voucher_number,ac.due_amount-ac.paid_amount,"
		 		+ "ac.remark,ac.voucher_date from account_heads c,"
		 		+ "account_receivable ac "
		 		+ "where c.id=ac.account_id and date(ac.voucher_date) between :fDate and :tDate and "
		 		+ "ac.company_mst = :companyMst and ac.due_amount-ac.paid_amount > 0") 
		List<Object> findByAllCustomerBalance(CompanyMst companyMst, Date fDate,
				Date tDate);

	 
	 @Query(nativeQuery = true,value = "select * from account_receivable where "
	 		+ "due_amount-paid_amount > 0 and "
	 		+ "date(voucher_date)<=:udate and account_id=:accountid and company_mst=:companyMst"
	 		+ " and voucher_number is not null order by voucher_number ASC")
	List<AccountReceivable> findAccountReceivablewithBalancegreaterThanzero(Date udate, CompanyMst companyMst,
			String accountid);

	void deleteBySalesTransHdr(SalesTransHdr salesTransHdr);


	 @Query(nativeQuery = true,value = "select * from account_receivable where "
	 		+ "voucher_number=:vouchernumber and date(voucher_date)=:voucherDate")
	
	AccountReceivable findByVoucherNumberAndVoucherDate(String vouchernumber, Date voucherDate);

	 
	 @Query(nativeQuery = true,value = "select a.* from account_receivable a, sales_trans_hdr h where "
		 		+ " a.company_mst=:companyMst and h.voucher_number=:vouchernumber and date(h.voucher_date)=:date "
		 		+ " and a.sales_trans_hdr=h.id")
	AccountReceivable findByCompanyMstAndVoucherNumberAndDate(CompanyMst companyMst, String vouchernumber, Date date);

	List<AccountReceivable> findByCompanyMstAndSalesOrderTransHdr(CompanyMst companyMst,
			SalesOrderTransHdr salesOrderTransHdr);

	
	@Modifying
	@Query(nativeQuery = true,value="update account_receivable  set account_id =:toAccountId "
			+ "where account_id =:fromAccountId ")
	void accountMerge(String fromAccountId, String toAccountId);
	
	/*
	 * AccountReceivable findByCompanyMstAndReceiptHdr(CompanyMst companyMst,
	 * ReceiptHdr receiptHdr);
	 */
}
