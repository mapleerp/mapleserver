package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.BalanceSheetConfigAsset;
import com.maple.restserver.entity.CompanyMst;

@Repository
public interface BalanceSheetConfigAssetRepository extends JpaRepository<BalanceSheetConfigAsset, String>{

	List<BalanceSheetConfigAsset> findByCompanyMst(CompanyMst companyMst);

	
	
}
