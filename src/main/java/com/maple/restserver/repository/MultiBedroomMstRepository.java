package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.MultiBedroomMst;


@Repository

public interface MultiBedroomMstRepository extends JpaRepository<MultiBedroomMst,Integer> {

	
//	List<MultiBedroomMst> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
