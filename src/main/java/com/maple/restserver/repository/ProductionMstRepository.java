package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.hibernate.type.TrueFalseType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ProductionDtl;

import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.ProductionMst;

@Repository
public interface ProductionMstRepository extends JpaRepository<ProductionMst, String>{

	
	@Query(nativeQuery = true, value = "select dd.item_name,dd.qty,"
			+ "u.unit_name from production_mst m,production_dtl d,production_dtl_dtl,unit_mst u,"
			+ " dd where m.id=d.production_mst_id and d.id=dd.production_dtl_id and"
			+ " u.id=dd.unit_id and date(m.voucher_date)=:vdate and m.branch_code=:branchCode"
			+ " and m.voucher_number=:vouchernumber")
	List<Object>getProductionDtlReport(Date vdate,String branchCode,String vouchernumber);

	
	
	@Query(nativeQuery = true, value = "select i.item_name,d.qty,"
			+ "u.unit_name from production_mst m,production_dtl d,item_mst i,unit_mst u "
			+ " where m.id=d.production_mst_id and i.id=d.item_id and "
			+ " u.id=i.unit_id and date(m.voucher_date)=:date and m.branch_code=:branchcode")
	List<Object>	fetchProductionDtlReport(String  branchcode,Date date);
           
	List<ProductionMst> findByVoucherDate(Date date);
	
	@Query(nativeQuery = true, value = "select count (*) from production_mst where voucher_date=:date and voucher_number is NOT NULL")
	int getClientProductionCount(Date date);
	
	
	@Query(nativeQuery = true, value = "select count (*) from production_mst m  where m.voucher_date=:sqlDate and m.voucher_number is NOT NULL "
			+ "and m.branch_code=:branchCode and m.company_mst=:companymstid ")
	int productionMstCount(String branchCode,java.sql.Date sqlDate,String companymstid);


	
	@Query(nativeQuery = true, value = "select count (*) from production_mst m,production_dtl d   where  m.id=d.production_mst_id and  m.voucher_date=:sqlDate and m.voucher_number is NOT NULL "
			+ "and m.branch_code=:branchCode and m.company_mst=:companymstid ") 
	int productionDtlCount(String branchCode,java.sql.Date sqlDate,String companymstid);

ProductionMst findByVoucherNumber(String voucherNumber);


@Query(nativeQuery = true,value="select d.item_name ,u.unit_name, sum(d.qty) , from sale_order_trans_hdr h ,sales_order_dtl d, resource_cat_item_link_mst rl ,item_mst i ,unit_mst u where h.id=d.sales_order_trans_hdr and  d.item_id=i.id and u.id=d.unit_id and "
		+ "rl.item_id=d.item_id and rl.resource_cat_id =:resCatId and h.branch_code=:branchcode  and date(h.voucher_date) >:date group by  d.item_name ,u.unit_name")
List<Object>fetchSaleOrderItem(String branchcode,Date date,String resCatId);


@Query(nativeQuery = true,value="select i.item_name,d.batch,d.qty,d.id from production_mst m, production_dtl d, item_mst i where  m.id=d.production_mst_id and d.status='PRE PLANING' and i.id=d.item_id")
List<Object>getPlanedProduction();

@Query(nativeQuery = true,value = "select * from production_mst where voucher_number is null and "
		+ "voucher_date=:vdate")
ProductionMst getPartiallySavedByDate(Date vdate);



List<ProductionMst> findByCompanyMst(CompanyMst companyMst);



List<ProductionMst> findByCompanyMstAndBranchCode(CompanyMst companyMst, String branchcode);


@Query
(nativeQuery = true,value = "select max(h.voucher_number) from production_mst h where "
		+ " h.branch_code=:branchcode and "
		+ "h.company_mst=:companyMst ")
String getMaxVoucherNo(String branchcode, CompanyMst companyMst);






@Query(nativeQuery = true,value="select i.item_name,d.batch,d.qty,d.id from production_mst m, production_dtl d, item_mst i where  m.id=d.production_mst_id and d.status='N' and i.id=d.item_id")
List<Object>fetchProductionPrePlaningToProduction();


@Query(nativeQuery = true,value = "select * from production_mst where "
		+ "date(voucher_date)>=:tdate and date(voucher_date)<=:fdate")
List<ProductionMst> findProductionMstBetweenDate(Date tdate, Date fdate);

}
