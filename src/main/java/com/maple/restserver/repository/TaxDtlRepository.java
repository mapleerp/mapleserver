package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.TaxDtl;

public interface TaxDtlRepository extends JpaRepository<TaxDtl, Integer>
{
	
	
	List<TaxDtl>findByCompanyMst(String companymst);
//	List<TaxDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
	
	
}
