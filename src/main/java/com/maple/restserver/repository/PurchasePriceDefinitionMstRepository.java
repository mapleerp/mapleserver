package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.PurchasePriceDefinitionMst;
@Component
@Repository
public interface PurchasePriceDefinitionMstRepository extends JpaRepository<PurchasePriceDefinitionMst, String>{
	

	List<PurchasePriceDefinitionMst> findByPurchaseHdrIdAndCompanyMstId(String hdrid, String companymstid);
	 
	
	

}
