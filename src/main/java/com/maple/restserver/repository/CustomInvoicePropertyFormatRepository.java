package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CustomInvoicePropertyFormat;

@Repository
public interface CustomInvoicePropertyFormatRepository extends JpaRepository<CustomInvoicePropertyFormat, String> {

	List<CustomInvoicePropertyFormat> findByCustomSalesMode(String customsalesmode);

}
