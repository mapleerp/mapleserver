package com.maple.restserver.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.InsuranceCompanyMst;

@Repository
@Transactional
public interface InsuranceCompanyRepository extends JpaRepository<InsuranceCompanyMst, String>{

	 InsuranceCompanyMst findByCompanyMstIdAndId(String companyMstid,String id);
	 
	 List<InsuranceCompanyMst> findByCompanyMstIdAndInsuranceCompanyName(String companyMstid,String insurancecompName);

	List<InsuranceCompanyMst> findByInsuranceCompanyName(String insurancename);

	InsuranceCompanyMst findByInsuranceCompanyNameAndCompanyMstId(String string, String companymstid);

	List<InsuranceCompanyMst> findByCompanyMst(CompanyMst companyMst);

	InsuranceCompanyMst findByCompanyMstAndId(CompanyMst companyMst, String id);

}
