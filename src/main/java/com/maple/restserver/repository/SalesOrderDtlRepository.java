package com.maple.restserver.repository;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.SaleOrderReceipt;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesOrderDtl;
import com.maple.restserver.entity.SalesOrderDtl;
import com.maple.restserver.entity.SalesOrderTransHdr;

@Repository
public interface SalesOrderDtlRepository extends JpaRepository<SalesOrderDtl, String>{
	

	List<SalesOrderDtl> findBySalesOrderTransHdrId(String salesTransId, Pageable pageable);
    Optional<SalesOrderDtl> findByIdAndId(String Id, String salesOrderTransId);

 //   List<SalesOrderDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
   
    
    @Query(nativeQuery = true, value = " select SUM( (rate*qty) + (  (rate*qty) * (tax_rate)/100 )) as totalAmount, \n" + 
    		" SUM(((rate*qty) * ((tax_rate)/100 ))) as totalTax, \n" + 
    		" SUM( (rate*qty) * cess_Rate/100) as totalCessAmt \n" + 
    		" FROM Sales_Order_Dtl d, Sales_Order_Trans_Hdr h \n" + 
    		" where h.id = d.sales_order_trans_hdr  "
    		+ "  AND  h.id = :salesorderHdrId")
    List<Object> findSumSalesOrderDtl(@Param("salesorderHdrId")String salesorderHdrId);
    
    
    
    List<SalesOrderDtl> findBySalesOrderTransHdrId(String salesTransId);	
    
    
    List<SalesOrderDtl> findBySalesOrderTransHdrVoucherDate( Date date);
	SalesOrderDtl findBySalesOrderTransHdrAndItemIdAndBatchAndBarodeAndUnitIdAndRate(
			SalesOrderTransHdr saleordertranshdr, String itemId, String batch, String barode, String unitId,
			Double rate);
	
	List<SalesOrderDtl> findBySalesOrderTransHdrAndItemId(SalesOrderTransHdr salesOrderTransHdr, String itemId);
	
	//-----------------new version 2.2 surya
	@Query(nativeQuery = true, value = "SELECT SUM(qty) as totalQty, "
			+ " SUM( (rate*qty) + (  (rate*qty) * (tax_rate)/100 ))  as totalAmount, "
			+ " SUM(((rate*qty) * ((tax_rate)/100 ))) as totalTax,  "
			+ " SUM( (rate*qty) * cess_Rate/100) as totalCessAmt  " + " FROM Sales_Order_Dtl d, Sales_Order_Trans_Hdr h "

			+ " where h.id = d.sales_order_trans_hdr AND h.id = :saleOrdrTransHdrId")
	List<Object> findSumOfSaleOrderDtlforDiscount(String saleOrdrTransHdrId);
	
	@Query(nativeQuery = true, value = "SELECT SUM(qty) as totalQty, "
			+ " SUM((rate*qty) + (  (rate*qty) * (tax_rate)/100 ) + (rate*qty) * cess_Rate/100)  as totalAmount, "
			+ " SUM(((rate*qty) * ((tax_rate)/100 ))) as totalTax,  "
			+ " SUM( (rate*qty) * cess_Rate/100) as totalCessAmt  " + " FROM Sales_Order_Dtl d, Sales_Order_Trans_Hdr h "

			+ " where h.id = d.sales_order_trans_hdr AND h.id = :saleOrdrTransHdrId")
	List<Object> findSumSaleOrderDtl(String saleOrdrTransHdrId);
	
	
	@Query(nativeQuery = true, value = "SELECT SUM(qty) as totalQty, "
			+ " SUM( (rate*qty) + (  (rate*qty) * (tax_rate)/100 ) + ((rate*qty) * cess_Rate/100))  as totalAmount, "
			+ " SUM(((rate*qty) * ((tax_rate)/100 ))) as totalTax,  "
			+ " SUM( (rate*qty) * cess_Rate/100) as totalCessAmt  " + " FROM Sales_Order_Dtl d, Sales_Order_Trans_Hdr h "

			+ " where h.id = d.sales_order_trans_hdr AND h.id = :saleOrdrTransHdrId")
	List<Object> findSumOfB2CSaleOrderDtlforDiscount(String saleOrdrTransHdrId);
	
	List<SalesOrderDtl> findBySalesOrderTransHdrAndItemIdAndBatch(SalesOrderTransHdr salesOrderTransHdr, String itemId,String batch);
	
//	@Query(nativeQuery = true, value = "select sum(d.qty) from sales_order_dtl d, sales_order_trans_hdr h where d.sales_order_trans_hdr_id = h.id and d.item_id =:itemId and h.id=:hdrId")
//	Double getSalesOrderDetailItemQty(String hdrId, String itemId);
	
	@Query(nativeQuery = true, value = "select sum(d.qty) from sales_order_dtl d, "
			+ "sales_order_trans_hdr h where d.sales_order_trans_hdr = h.id and d.item_id =:itemId and h.id=:hdrID AND"
			+ " d.batch = :batch")
	
	Double getSalesOrderDetailItemQtyByBatch(String hdrID, String itemId, String batch);
	
	@Query(nativeQuery = true, value ="select * from sales_order_dtl where sales_order_trans_hdr=:salesOrderTransHdr")
	List<SalesOrderDtl> findBySalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr);
	
	

  
    

}
