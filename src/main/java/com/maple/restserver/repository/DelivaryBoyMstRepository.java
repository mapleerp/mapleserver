package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.DeliveryBoyMst;
import com.maple.restserver.entity.OrderTakerMst;

@RestController
public interface DelivaryBoyMstRepository extends JpaRepository<DeliveryBoyMst, String> {

	
List<DeliveryBoyMst> findByCompanyMstIdAndBranchCode(String companymstid,String branchcode);

DeliveryBoyMst findByCompanyMstIdAndDeliveryBoyNameAndBranchCode(String companymstid,String delivaryboyname,String branchcode);



@Query(nativeQuery=true,value="select *from delivery_boy_mst o where o.status ='ACTIVE' and"
		+ " o.company_mst=:companymstid and o.branch_code=:branchcode") 
	List<DeliveryBoyMst> getDeliveryBoyByStatus(String companymstid,String branchcode);

DeliveryBoyMst findByCompanyMstIdAndId(String companymstid, String id);
}
