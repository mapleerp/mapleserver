package com.maple.restserver.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CurrencyMst;

@Repository
@Transactional
public interface CurrencyMstRepository extends JpaRepository<CurrencyMst, String>{

	CurrencyMst findByCurrencyName(String currencyname);

}
