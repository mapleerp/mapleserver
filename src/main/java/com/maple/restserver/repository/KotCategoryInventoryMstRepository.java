package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.KotCategoryInventoryMst;

@Repository
public interface KotCategoryInventoryMstRepository extends JpaRepository<KotCategoryInventoryMst, String>{


	@Query(nativeQuery=true,value="select * from kot_category_inventory_mst "
			+ " where parent_item is NULL and leaf='N' and company_mst=:id")
	List<KotCategoryInventoryMst> findAllCategoryByParentItemNull(String id);
	
	@Query(nativeQuery=true,value="select k.id, i.item_name as child_item, k.leaf,k.parent_item from "
			+ "item_mst i, kot_category_inventory_mst k where k.child_item=i.id "
			+ " and  k.parent_item is NULL and k.company_mst=:id and k.leaf = 'Y' ")
	List<Object> findAllItemByParentItemNull(String id);

	@Query(nativeQuery=true,value="select k.id, i.item_name as child_item, k.leaf,k.parent_item from "
			+ "item_mst i, kot_category_inventory_mst k where k.child_item=i.id "
			+ " and  k.parent_item=:parentitem and k.company_mst=:companyId and k.leaf = 'Y' ")
	List<Object> findItemByParentItemAndCompanyMstId(String companyId, String parentitem);
	
	@Query(nativeQuery=true,value="select * from kot_category_inventory_mst"
			+ "  where parent_item =:parentitem and leaf='N' and company_mst=:id")
	List<KotCategoryInventoryMst> findCategoryByParentItemAndCompanyMstId(String id, String parentitem);

}
