package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ConsumptionDtl;
@Repository
public interface ConsumptionDtlRepository extends JpaRepository<ConsumptionDtl,String>{

	List<ConsumptionDtl> findByConsumptionHdrId(String id);
}
