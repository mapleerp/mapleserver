package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.BalanceSheetConfigLiability;
import com.maple.restserver.entity.CompanyMst;

@Repository
public interface BalanceSheetConfigLiabilityRepository extends JpaRepository<BalanceSheetConfigLiability, String>{

	List<BalanceSheetConfigLiability> findByCompanyMst(CompanyMst companyMst);

}
