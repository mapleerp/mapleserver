package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitchenCategoryDtl;

@Repository
public interface KitchenCategoryDtlRepository extends JpaRepository<KitchenCategoryDtl,String> {

	List<KitchenCategoryDtl> findByCompanyMst(CompanyMst companyMst);
	
	@Query(nativeQuery =  true,value="select item_name from item_mst where category_id=:categoryId")
	List<String> getItemMstByKitchenCategory(String categoryId);
}
