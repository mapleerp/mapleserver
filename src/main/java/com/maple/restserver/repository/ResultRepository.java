package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.Result;

@Repository
public interface ResultRepository extends JpaRepository<Result,String> {

}
