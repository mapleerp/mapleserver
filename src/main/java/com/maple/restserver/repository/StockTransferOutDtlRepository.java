package com.maple.restserver.repository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;

@RestController
public interface StockTransferOutDtlRepository extends JpaRepository<StockTransferOutDtl, String>{
	
	
	
	ArrayList<StockTransferOutDtl> findByStockTransferOutHdrIdAndCompanyMstId(String stockTransferHdrId,String companymstid );
	
    Optional<StockTransferOutDtl> findByIdAndId(String id, String stockTransferHdrId);
    //  List<StockTransferOutDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);

    List<StockTransferOutDtl> findByStockTransferOutHdrIdAndItemIdAndBatch(String hdrid,ItemMst itemId,String batch);
  
  
    // here  we are finding the sum of the amount stocktransouthdr
    
    @Query(nativeQuery = true, value = "SELECT SUM(amount) as totalamount  FROM stock_transfer_out_dtl d where stock_transfer_out_hdr =:stockTranOutHdrId"
    		)
    
    public Double findSumAmount(@Param("stockTranOutHdrId") String stockTranOutHdrId);
    
    List<StockTransferOutDtl> findByCompanyMst(String companymstid);
    
    
    List<StockTransferOutDtl> findByCompanyMstAndStockTransferOutHdrVoucherNumberAndStockTransferOutHdrFromBranch(CompanyMst companymstid,String voucherNumber,String fromBranch);

    List<StockTransferOutDtl> findByStockTransferOutHdrId(String id);
    
    @Query(nativeQuery = true, value = "SELECT *  FROM stock_transfer_out_dtl d where stock_transfer_out_hdr =:id order by sl_no  "
    		)
    
    List<StockTransferOutDtl> findByStockTransferOutHdrIdOrderBySrl(String id);
 
    @Query(nativeQuery = true, value = "SELECT *  FROM stock_transfer_out_dtl d where stock_transfer_out_hdr =:id order by display_serial  "
    		)
    
    List<StockTransferOutDtl> findByStockTransferOutHdrIdOrderByDisplaySrl(String id);
 
    
    
  
    
    List<StockTransferOutDtl> findByItemIdAndUnitIdAndStockTransferOutHdr(
    		ItemMst itemId,String unitId , StockTransferOutHdr stockTransferOutHdr);
 
    
    
}


