package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentItemBranchMst;

@Component
@Repository
public interface IntentItemBranchRepository extends JpaRepository<IntentItemBranchMst,String>{

	IntentItemBranchMst findByCompanyMstAndItemIdAndBranchCode(CompanyMst companyMst,String itemId,
			String branchCode);
	IntentItemBranchMst findByCompanyMstAndItemId(CompanyMst companyMst,String itemId);
	
}
