package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionDtlDtl;

public interface ProductionDtlDtlRepository extends JpaRepository<ProductionDtlDtl, String> {

	@Query(nativeQuery = true,value = "select pdd.* from production_dtl_dtl pdd,"
			+ "production_dtl pd,production_mst pm "
			+ "where pdd.production_dtl_id = pd.id and pd.production_mst_id = pm.id "
			+ "and date(pm.voucher_date) >= :fdate and date(pm.voucher_date) <= :tdate")
	List<ProductionDtlDtl> getProductionDtlDtlBtwnDate(Date fdate,Date tdate);
//	
//	@Query(nativeQuery = true,value = "select pdd.* production_dtl_dtl pdd,production_dtl pd,"
//			+ "production_mst pm where pdd.production_dtl = pd.id and pd.production_mst = pm.id "
//			+ "and pm.voucher_date from ")
//	List<ProductionDtlDtl>getProdDtlDltbetweenDate(Date fdate,Date tdate);
	List<ProductionDtlDtl>findByproductionDtlId(String productiondetaiId);
	

	List<ProductionDtlDtl>findByproductionDtl( ProductionDtl productionDtl);
	 @Query(nativeQuery=true,
			  value=" select pdd.* from production_dtl_dtl pdd , production_mst pm,  production_dtl pd"
			  		+ "  where pdd.production_dtl_id=pd.id and pd.production_mst_id = pm.id and "
			  		+ "  date(pm.voucher_date) =:voucherDate ")
	List<ProductionDtlDtl> findByProductionDtlIdProductionMstVoucherDate(Date voucherDate);

	
	
	 @Query(nativeQuery=true,
			  value=" select i.item_name, u.id,  sum(dd.qty )  "
			  		+ " from production_dtl_dtl  dd , item_mst i, unit_mst u, production_mst m,production_dtl d "
			  + " where  d.production_mst_id = m.id and "
			  + " dd.production_dtl_id = d.id and dd.raw_material_item_id = i.id and " + 
			  " i.unit_id = u.id and "
			  + " date(m.voucher_date) = :date group by  i.item_name, u.id "
			  )
	List<Object>getRawmaterialByVoucherDate(Date date);
 

	 @Query(nativeQuery=true,
			  value=" select i.item_name, u.unit_name,  sum(dd.qty )  "
			  		+ " from production_dtl_dtl  dd , item_mst i, unit_mst u, production_mst m,production_dtl d "
			  + " where  d.production_mst_id = m.id and "
			  + " dd.production_dtl_id = d.id and dd.raw_material_item_id = i.id and " + 
			  " i.unit_id = u.id and "
			  + "date(m.voucher_date) = :date group by  i.item_name,u.unit_name "
			  )
	List<Object>getRawMaterialsSumByDate(Date date);
	 
	 @Query(nativeQuery=true,
			  value=" select i.item_name, u.unit_name,  sum(dd.qty ),m.voucher_date  "
			  		+ " from production_dtl_dtl  dd , item_mst i, unit_mst u, production_mst m,production_dtl d "
			  + " where  d.production_mst_id = m.id and "
			  + " dd.production_dtl_id = d.id and dd.raw_material_item_id = i.id and " + 
			  " i.unit_id = u.id and "
			  + " date(m.voucher_date) between :fdate and :tdate group by  i.item_name,u.unit_name,m.voucher_date order by m.voucher_date "
			  )
	List<Object>getRawMaterialsSumByBetweenDate(Date fdate,Date tdate);
 

	 @Query(nativeQuery=true,
			  value=" select i.item_name,u.unit_name,dd.qty "
			  		+ " from production_dtl  dd , item_mst i, unit_mst u, production_mst m  "
			  + " where  dd.production_mst_id = m.id and "
			  + "   dd.item_id = i.id and " + 
			  " i.unit_id = u.id and "
			  + "date(m.voucher_date) = :date group by  i.item_name, u.unit_name "
			  )
	List<Object>getPlanningKitByVoucherDate(Date date);
 List<ProductionDtlDtl>findByProductionDtl(ProductionDtl productionDtl);

 @Query(nativeQuery = true, value = "select sum(dd.qty * p.amount) as costprice, "
 		+ "h.voucher_date from "
 		+ "production_dtl_dtl dd,  price_definition p, price_defenition_mst pm, "
 		+ "actual_production_dtl d, actual_production_hdr h "
 		+ "where dd.production_dtl_id=d.production_dtl and  d.actual_production_hdr=h.id "
 		+ "and h.voucher_number is not null and date(h.voucher_date)>=:fromdate and "
 		+ "date(h.voucher_date)<=:todate and h.company_mst=:companyMst and "
 		+ "h.branch_code=:branch and "
 		+ " p.item_id=dd.raw_material_item_id and p.price_id = pm.id "
 		+ "and pm.price_level_name='COST PRICE' "
 		+ "and p.unit_id =dd.unit_id and p.end_date is null "
 		+ "group by h.voucher_date ") 
List<Object> findRawMeterialValue(Date fromdate, Date todate, CompanyMst companyMst, String branch);


 @Query(nativeQuery = true, value = "select sum(dd.qty * i.standard_price) as costprice, "
	 		+ "h.voucher_date from "
	 		+ "production_dtl_dtl dd, item_mst i,  "
	 		+ "actual_production_dtl d, actual_production_hdr h "
	 		+ "where dd.production_dtl=d.production_dtl and  d.actual_production_hdr=h.id "
	 		+ "and h.voucher_number is not null and date(h.voucher_date)>=:fromdate and "
	 		+ "date(h.voucher_date)<=:todate and h.company_mst=:companyMst and h.branch_code=:branch and "
	 		+ "dd.raw_material_item_id=i.id "
	 		+ "group by h.voucher_date ") 
List<Object> findRawMeterialValueFromStanrdPrice(Date fromdate, Date todate, CompanyMst companyMst, String branch);


}
