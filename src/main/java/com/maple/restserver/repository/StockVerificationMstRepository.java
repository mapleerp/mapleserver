package com.maple.restserver.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.StockVerificationMst;

public interface StockVerificationMstRepository extends JpaRepository<StockVerificationMst,String>{

	
	@Query(nativeQuery = true, value ="select * from stock_verification_mst where voucher_number is NULL and company_mst =:companyMst and voucher_date =:voucherDate")
	StockVerificationMst getStockVerification(CompanyMst companyMst,Date voucherDate);
	
	
	StockVerificationMst findByCompanyMstAndVoucherDate(CompanyMst companyMst,Date voucherDate);

	
}
