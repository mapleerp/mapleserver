package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.JobCardInDtl;

@Component
@Repository
public interface JobCardInDtlRepository extends JpaRepository<JobCardInDtl,String>{

	
	List<JobCardInDtl> findByJobCardInHdrId(String hdrId);
}
