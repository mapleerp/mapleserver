package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SchEligiAttrListDef;
import com.maple.restserver.entity.SchEligibilityAttribInst;

@Repository
public interface SchEligiAttrListDefRepository extends JpaRepository<SchEligiAttrListDef,String>  {

	SchEligiAttrListDef findByIdAndCompanyMst(String id , CompanyMst companyMst);
	
	List<SchEligiAttrListDef> findByEligibilityIdAndCompanyMst(String eligibilityid , CompanyMst companyMst);
	
	SchEligiAttrListDef findByAttribNameAndCompanyMstAndEligibilityId(String attribName , CompanyMst companyMst,String eligibilityid);

	List<SchEligibilityAttribInst> findByEligibilityId(String eligibilityId);
}
