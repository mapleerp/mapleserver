package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CreditNote;

@Component
@Repository

public interface CreditNoteRepository extends JpaRepository <CreditNote, String>{

	List<CreditNote> findByCompanyMst(CompanyMst companyMst);
	@Query(nativeQuery =  true,value="select u.user_name,c.credit_account,c.credit_account,c.amount,c.voucher_number,c.remark,c.voucher_date ,c.branch ,c.id from  debit_note c, user_mst u where c.user_id=u.id  and c.company_mst=:companyMst")
	List<Object> findAllCreditNote (CompanyMst  companyMst);

}
