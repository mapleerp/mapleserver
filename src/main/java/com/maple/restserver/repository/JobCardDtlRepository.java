package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JobCardDtl;
import com.maple.restserver.entity.JobCardHdr;

@Repository
public interface JobCardDtlRepository extends JpaRepository<JobCardDtl,String> {

	List<JobCardDtl> findByCompanyMstAndJobCardHdr(CompanyMst companyMst, JobCardHdr jobCardHdr);

	JobCardDtl findByCompanyMstAndJobCardHdrAndItemId(CompanyMst companyMst, JobCardHdr jobCardHdr, String itemId);

	@Query(nativeQuery = true,value = "select sum(d.qty) "
			+ "from job_card_dtl d where d.job_card_hdr=:jobCardHdr and "
			+ "d.company_mst=:companyMst and d.item_id=:itemid")
	Double JobCardDtlItemIdByHdrById(CompanyMst companyMst, JobCardHdr jobCardHdr, String itemid);

	
	
	
}
