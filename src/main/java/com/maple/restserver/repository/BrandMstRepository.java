package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.BrandMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProductMst;
@Component
@Repository
public interface BrandMstRepository extends JpaRepository<BrandMst, String>{
	


	BrandMst findByCompanyMstAndBrandName(CompanyMst companyMst, String brandname);

	BrandMst findByCompanyMstAndId(CompanyMst companyMst, String brandid);
	
	
	@Query(nativeQuery = true,value="select b.id, b.brand_name from brand_mst b where lower(b.brand_name) LIKE  :searchItemName")
	public List<Object> findSearch(@Param("searchItemName") String searchItemName ,  Pageable pageable);

	BrandMst findByBrandName(String brandname);

}
