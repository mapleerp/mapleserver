package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ServiceLocationMst;

@Repository
public interface ServiceLocationMstRepository extends JpaRepository<ServiceLocationMst,String>{

	ServiceLocationMst findByCompanyMstAndId(CompanyMst companyMst,String hdrid);
}
