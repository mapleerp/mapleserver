package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProductMst;
import com.maple.restserver.entity.WatchStrapMst;
@Component
@Repository
public interface WatchStrapMstRepository extends JpaRepository<WatchStrapMst, String>{
	
	@Query(nativeQuery = true,value="select s.id, s.strap_name from watch_strap_mst s where lower(s.strap_name) LIKE  :searchItemName")
	public List<Object> findSearch(@Param("searchItemName") String searchItemName ,  Pageable pageable);

	WatchStrapMst findByCompanyMstAndStrapName(CompanyMst companyMst, String strapname);


	public WatchStrapMst findByCompanyMstAndId(CompanyMst companyMst, String id);
}
