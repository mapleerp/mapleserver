package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.maple.restserver.entity.BranchPriceDefinitionMst;
import com.maple.restserver.entity.CompanyMst;

@Repository
public interface BranchPriceDefinitionRepository extends JpaRepository<BranchPriceDefinitionMst, String>
{

	BranchPriceDefinitionMst findByCompanyMstAndId(CompanyMst companyMst, String id);

	List<BranchPriceDefinitionMst> findByCompanyMst(CompanyMst companyMst);

}
//Sibi............12.03.2021