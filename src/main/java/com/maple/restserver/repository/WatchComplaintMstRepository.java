package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProductMst;
import com.maple.restserver.entity.WatchComplaintMst;
@Component
@Repository
public interface WatchComplaintMstRepository extends JpaRepository<WatchComplaintMst, String>{
	

	WatchComplaintMst findByCompanyMstAndComplaint(CompanyMst companyMst, String name);

	public WatchComplaintMst findByCompanyMstAndId(CompanyMst companyMst, String id);

	@Query(nativeQuery = true,value="select p.id, p.complaint "
			+ "from watch_complaint_mst p where lower(p.complaint) LIKE  :searchItemName and"
			+ " p.company_mst=:companyMst")

	public List<Object> findSearch(String searchItemName, Pageable topFifty, CompanyMst companyMst);
}
