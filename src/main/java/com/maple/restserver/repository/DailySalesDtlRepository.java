package com.maple.restserver.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.DailySalesDtl;

public interface DailySalesDtlRepository extends JpaRepository<DailySalesDtl, String>{
	
	DailySalesDtl findByReportDate(java.util.Date rDate);
	
	DailySalesDtl  findByReportDateAndBranchCode(Date  date,String branchCode);

}
