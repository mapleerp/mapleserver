package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.PhysicalStockHdr;
import com.maple.restserver.entity.SalesTransHdr;

@Repository
public interface PhysicalStockHdrRepository extends JpaRepository<PhysicalStockHdr, String> {

	@Query(nativeQuery = true, value = " select * from physical_stock_hdr h  where h.voucher_number is null and "
			+ " company_mst =:companymstid")

	List<PhysicalStockHdr> getPhysicalStockNoVoucher(String companymstid);

	Optional<PhysicalStockHdr> findByIdAndCompanyMstId(String hdrid, String companymstid);

	@Query(nativeQuery = true, value = " select h.voucher_number,h.voucher_date,i.item_name, "
			+ "d.expiry_date,d.mrp,d.qty_in,d.manufacture_date,d.batch  "
			+ " from physical_stock_hdr h, physical_stock_dtl d, item_mst i where   "
			+ "i.id=d.item_id and d.physical_stock_hdr_id=h.id and "
			+ " h.company_mst =:companymstid and date(h.voucher_date)>=:sdate and date(h.voucher_date)<=:edate "
			+ "")
	List<Object> getPhysicalStockDtlReport(String companymstid, Date sdate, Date edate);

	@Query(nativeQuery = true,value = "select * from physical_stock_hdr where "
			+ "date(voucher_date) >= :tdate and date(voucher_date)<=:fdate")
	List<PhysicalStockHdr> getPhysicalStockHdrBetweenDate(Date tdate, Date fdate);
	
	
	@Query(nativeQuery = true,value = " SELECT psh.voucher_number,"
			+ "psh.voucher_date,"
			+ "itm.item_group_id,"
			+ "itm.item_code,"
			+ "itm.item_name,"
			+ "psd.batch,"
			+ "psd.system_qty,"
			+ "psd.qty_in,"
			+ "psd.system_qty - psd.qty_in,"
			+ "psd.mrp,"
			+ "pd.amount,"
			+ "usr.user_name "
			+ "FROM "
			+ "physical_stock_dtl psd,"
			+ "physical_stock_hdr psh,"
			+ "item_mst itm,"
			+ "user_mst usr,"
			+ "price_definition pd "
			+ "WHERE "
			+ "psd.physical_stock_hdr_id = psh.id AND "
			+ "psd.item_id = itm.id AND "
			+ "psh.user_id = usr.id AND "
			+ "psd.item_id = pd.item_id AND "
			+ "date(psh.voucher_date) >=:fromDate AND date(psh.voucher_date) <=:toDate")
	List<Object> findPhysicalStockVarianceReport(Date fromDate, Date toDate);

}
