package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.KotTableHdr;
@Repository
public interface KotTableHdrRepository extends JpaRepository<KotTableHdr, String>{
	
	@Query(nativeQuery=true,value="select * from kot_table_hdr where "
			+ "table_name=:tablename and table_status='RUNNING' and billed_status='NO'")
	List<KotTableHdr> findByRunningTable(String tablename);

	

//	KotTableHdr findByTableNameAndCompanyMst(String tablename, CompanyMst companyMst);

	
//	@Query(nativeQuery = true,value="select * from kot_table_hdr k where table_name=:tablename and k.table_status='RUNNING' and company_mst=companyMst")
//	List<KotTableHdr> findTableRunningByTableId(String tablename,CompanyMst companyMst);

}
