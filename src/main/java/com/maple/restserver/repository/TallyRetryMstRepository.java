package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.TallyRetryMst;

@Repository
public interface TallyRetryMstRepository  extends JpaRepository<TallyRetryMst, String> {
	

}
