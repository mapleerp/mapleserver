package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.GstDtl;

 

 
@Component
@Repository
public interface GstDtlRepository extends JpaRepository<GstDtl,String>{
	//List<GstDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);

	List<GstDtl>findByCompanyMstId(String companymstid);

}
