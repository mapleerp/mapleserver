package com.maple.restserver.repository;

 
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.ReorderMst;

@Repository
public interface JournalHdrRepository extends JpaRepository<JournalHdr, String>{
	
	
	@Query(nativeQuery = true,value="select *  from journal_hdr where  voucher_date >= :date")
	 List<JournalHdr> journalVoucherGreateAndEquel(Date date );

	
	
	List<JournalHdr>  findByVoucherNumberAndVoucherDate(String voucherNumber, Date voucherDate);
	
	
	List<JournalHdr> findByVoucherDate(Date date);
	 @Query(nativeQuery = true,value="select count(*) from journal_hdr where voucher_date=:date and voucher_number is NOT NULL ")
	int clientJournalCount(Date date );


	List<JournalHdr> findByCompanyMst(CompanyMst companyMst);


	JournalHdr findByCompanyMstAndId(CompanyMst companyMst, String hdrid);


@Query(nativeQuery = true,value="select h.id,h.voucher_date,h.voucher_number,h.trans_date from journal_hdr h where h.company_mst=:companyMst and date(h.voucher_date)>=:fromDate "
		+ "and date(h.voucher_date)<=:toDate and h.branch_code=:branchcode")
	List<Object> journalHdrRepository(CompanyMst companyMst, Date fromDate, Date toDate, String branchcode);

@Query(nativeQuery = true,value = "select * from journal_hdr where date(voucher_date)>=:tdate "
		+ "and date(voucher_date)<=:fdate")
List<JournalHdr> getJournalHdrBetweenDate(Date tdate, Date fdate);


}
