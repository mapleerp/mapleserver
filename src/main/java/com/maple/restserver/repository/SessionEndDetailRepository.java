package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SessionEndDtl;

@Repository
public interface SessionEndDetailRepository extends JpaRepository<SessionEndDtl, String>{

	
	List<SessionEndDtl>	findBySessionEndClosureId(String id);

	 @Query(nativeQuery=true,value="SELECT  SUM(COUNT*DENOMINATION) FROM SESSION_END_DTL AS TOTAL  WHERE  SESSION_END_CLOSURE_MST_ID=:hdrid ") 
	Double gettotal(String hdrid);
}
