
package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.his.entity.LabTestDtl;

@Repository
public interface LabTestDtlRepository extends JpaRepository<LabTestDtl,Integer>{

}
