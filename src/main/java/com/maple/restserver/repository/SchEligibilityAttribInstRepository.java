package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SchEligibilityAttribInst;
import com.maple.restserver.entity.SchOfferAttrInst;

@Repository
public interface SchEligibilityAttribInstRepository  extends JpaRepository<SchEligibilityAttribInst,String>  {
	
	
	 @Query(nativeQuery = true, value = " select * from sch_eligibility_attrib_inst where id= :id")
	
	Optional<SchEligibilityAttribInst> findById(String id);
	
	
	 @Query(nativeQuery = true, value = " delete from sch_eligibility_attrib_inst where id= :id")
	 void deleteByIdFromTable(String id);
	
	List<SchEligibilityAttribInst>	findByCompanyMst(CompanyMst companymst);



	SchEligibilityAttribInst findByCompanyMstAndAttributeNameAndSchemeId(CompanyMst companyMst,
			String attributename, String schemeid);



	List<SchEligibilityAttribInst> findByCompanyMstAndSchemeId(CompanyMst companyMst, String schemeid);



	
		
	



	List<SchEligibilityAttribInst> findByEligibilityId(String eligibilityId);



	SchEligibilityAttribInst findByCompanyMstAndAttributeNameAndSchemeIdAndEligibilityId(CompanyMst companyMst,
			String attributename, String schemeid, String eligibilityid);



	List<SchEligibilityAttribInst> findBySchemeIdAndEligibilityId(String id, String eligibilityId);

	
	List<SchEligibilityAttribInst> findByCompanyMstAndSchemeIdAndEligibilityId(CompanyMst companyMst, String id,
			String eligibilityId);



	
	}
		

