package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.ReceiptHdr;

@Repository
public interface PurchaseHdrRepository extends JpaRepository<PurchaseHdr, String>{
	
	
	
	@Query(nativeQuery = true,value="select *  from purchase_hdr where  voucher_date >= :date")
	 List<PurchaseHdr> purchaseVoucherGreateAndEquel(Date date );

	
	@Query(nativeQuery = true,value ="select * from purchase_hdr"
			+ " where date(supplier_inv_date) >= :fdate and date(supplier_inv_date)<=:tdate")
	List<PurchaseHdr> getPurchaseHdrBetweenDate(Date fdate,Date tdate);
	
	 @Query(nativeQuery=true,value=" SELECT * FROM purchase_hdr p WHERE p.voucher_number = :voucherNumber and date(voucher_date) = date(:voucherDate)") 
	List<PurchaseHdr> findByVoucherNumberAndVoucherDate(String voucherNumber, Date voucherDate);
	
	List<PurchaseHdr> findByVoucherDateAndCompanyMstAndBranchCode(Date date, CompanyMst companyMst, String branchcode);
	
	PurchaseHdr findBySupplierInvNoAndSupplierInvDate(String supInvNo,Date supInvDate);
	
	PurchaseHdr findByVoucherNumberAndCompanyMst(String invoiceno, CompanyMst companyMst);

	PurchaseHdr findByVoucherNumberAndBranchCode(String invoiceno, String branchCode);

	
	
	
	 @Query(nativeQuery=true,value=" SELECT * FROM purchase_hdr p WHERE p.voucher_number like  :voucherNumber  ") 
		List<PurchaseHdr> findByVoucherNumberLike(String voucherNumber );

	 
	 
	  //here finding the report purchase
	
	
	/*
	 * @Query(nativeQuery=true,
	 * value="from purchase_hdr  where voucher_date BETWEEN:startDate AND :endDate")
	 * List<PurchaseHdr>purchaseSummaryReport(@Param("itemName")String
	 * itemName,@Param("supplierName")String supplierName);
	 */
	  
	  
	/*
	 * @Query(nativeQuery=true,value=" SELECT " +
	 * " from purchase_hdr  where voucher_date BETWEEN:startDate AND :endDate")
	 * 
	 * List<Object>getDailyPurchase(Date purchaseDate);
	 */
	 
 @Query(nativeQuery=true,value=" SELECT * FROM purchase_hdr p WHERE p.final_saved_status = 'N'  ") 
		List<PurchaseHdr> fetchPurchaseHdr();
	 
	 @Query(nativeQuery=true,value="SELECT s.account_name ,"
		+"p.voucher_number ,  p.voucher_date , i.item_name , "
		+"d.qty, d.purchse_rate , d.tax_rate ,"
	    + "((d.qty * d.purchse_rate  * d.tax_rate/100) + (d.purchse_rate * d.qty)) "
		+" from purchase_hdr p, purchase_Dtl d,Item_Mst i,account_heads s"
	    + "  where p.id=d.purchase_hdr_id AND p.supplier_id=s.id AND d.item_id=i.id "
	    + " ")
	 List<Object> getAllPurchase(); 
	 
	 
	 
	  @Query(nativeQuery = true,value="select h.voucher_number from purchase_hdr h "
		  		+ "where h.voucher_type=:vType and date(h.voucher_date)=:vDate "
		  		+ " and  h.company_mst = :company_mst_id and h.voucher_number is not null  order by h.voucher_number")
		  List<Object>getpurchaseVoucherNumber(String company_mst_id, Date vDate,String vType);

	  
	 
	/*
	 * @Query(nativeQuery=true,
	 * value="SELECT s.supplier_name,p.voucher_name,  p.voucher_date,i.item_name, d.qty, d.purchse_rate ,d.tax_rate"
	 * +
	 * "((d.qty*d.purchse_rate *d.tax_rate/100) + (d.purchse_rate*d.qty)) from purchase_hdr p, purchase_Dtl d,Item_Mst i,Supplier_Mst s"
	 * + "  where p.id=d.purchase_hdr_id AND p.supplier_id=s.id AND d.item_id=i.id "
	 * + "AND p.voucher_number=:vNo") List<Object> getDailyPurchase(String vNo);
	 */
	 
	 List<PurchaseHdr>  findByCompanyMstId(String companymstid);

	/*
	 * String supplierName; String voucherNumber; Date voucherDate; String itemName;
	 * Double qty; Double purchseRate; Double taxRate;
	 */
	 
	/*
	 * String supplierName; String supvoucherNumber; Date supvoucherDate; String
	 * supplierInvNo;
	 */
		
	  @Query(nativeQuery = true,value="select "
	  		+ " s.account_name,"
	  		+ "h.voucher_number,"
	  		
	  		+ "i.item_name,"
	  		+ "d.qty,"
	  		+ "d.purchse_rate,"
			+ "d.tax_rate, "
	  		+ "h.voucher_date ,h.supplier_inv_no , u.unit_name ,sum(d.amount)+sum(tax_amt) as amount,s.party_gst"
	  	
	  		+ " from purchase_hdr h, "
	  		+ "account_heads s, "
	  		+ "item_mst i, unit_mst u,"
	  		+ " purchase_dtl d where s.id=h.supplier_id "
	  		+ "and i.id=d.item_id and h.id=d.purchase_hdr_id "
	  		+ "and h.voucher_number=:vouchernumber and date(h.voucher_date)=:date"
	  		+ " and u.id=d.unit_id and h.branch_code =:branchcode "
	  		+ "group by s.account_name,h.voucher_number,"
	  		+ " i.item_name,d.qty,d.purchse_rate,d.tax_rate,h.voucher_date ,h.supplier_inv_no , u.unit_name,s.party_gst "
	  		)
	  	
 
	List<Object> retrieveReportPurchase(String vouchernumber, String branchcode, Date date);
	  
	  
	  @Query(nativeQuery = true,value="select sum(d.amount)+sum(d.tax_amt) as totalamount from purchase_hdr h, purchase_dtl d where h.id=d.purchase_hdr_id and h.voucher_number=:vouchernumber and h.branch_code=:branchcode and date(h.voucher_date)=:date")
	  Double retrieveTotalAmount(String vouchernumber, String branchcode, Date date);
	  @Query(nativeQuery = true,value="select s.account_name,h.voucher_number from purchase_hdr h ,account_heads s where s.id=h.supplier_id and h.company_mst=:companyMst and h.branch_code=:branchcode and date(h.voucher_date)=:vDate and h.final_saved_status='Y' ")
		List<Object>  getSupplierAndVouchernumber(String branchcode,CompanyMst companyMst,String vDate);

	  
	  @Query(nativeQuery = true,value="select *from purchase_hdr h where h.branch_code=:branchCode and h.supplier_id=:suppplierid and h.company_mst=:companyMst and date(h.voucher_date) between :fromDate and :toDate  ")
	  List<PurchaseHdr>  findBySupplier(String suppplierid,Date fromDate,Date toDate,String branchCode,CompanyMst companyMst);
	  
	  @Query(nativeQuery = true,value="select * from purchase_hdr h where h.final_saved_status='Y' and h.branch_code=:branchCode and h.company_mst=:companyMst and date(h.voucher_date) between :fromDate and :toDate  ")
	  List<PurchaseHdr> purchaseReport(Date fromDate,Date toDate,String branchCode,CompanyMst companyMst);


	  List<PurchaseHdr>findByVoucherDate(Date date);
	  @Query(nativeQuery = true,value="select count(*) from purchase_hdr where voucher_date=:date and voucher_number is NOT NULL")
	  int getClientPurchaseCount(Date date);
	  @Query(nativeQuery = true,value="select id from purchase_hdr where voucher_number is null ")
	  String fetchPrograssingPurchaseHdrId();
	  
	  //version 3.1 sathyajith 
	  @Query(nativeQuery =true,value="select "
				+ "h.voucher_number,"
				+ "h.voucher_date,"
				+ "s.account_name,"
				+ "i.item_name, "
				+ "d.qty, "
				+ " d.purchse_rate,"
				+ "d.tax_rate, "
				+ "u.unit_name "
				+ "from "
				+ " purchase_hdr h,"
				+ "purchase_dtl d,"
				+ "account_heads s,"
				+ "item_mst i,"
				+ "unit_mst u "
				+ " where h.id=d.purchase_hdr_id "
				+ " and date(h.voucher_date) >= :fromDate and   date(h.voucher_date) <= :toDate "
				+ " and h.company_mst=:companyMst and h.branch_code=:branchCode and "
				+ " s.id=h.supplier_id and u.id=d.unit_id and i.id=d.item_id  ")
		List<Object>  getPurchaseDtlReport( java.sql.Date fromDate, java.sql.Date toDate, String branchCode,
					CompanyMst companyMst);
	//version 3.1 end
	  
	  @Query(nativeQuery = true,value = "select count(*) from purchase_hdr h  "
	  		+ "where h.branch_code=:branchCode a"
	  		+ "nd h.voucher_date=:vDate "
	  		+ "and h.company_mst=:companymstid "
	  		+ "and h.voucher_number is NOT NULL ")
	  int countOfPurchaseHdr(String branchCode,java.sql.Date  vDate,String companymstid);


	  @Query(nativeQuery = true,value = "select count(*) from purchase_hdr h,purchase_dtl d  where h.id=d.purchase_hdr_id and h.branch_code=:branchCode and h.voucher_date=:vDate and h.company_mst=:companymstid and h.voucher_number is NOT NULL")
	  int countOfPurchaseDtl(String branchCode,java.sql.Date  vDate,String companymstid);

	  
	  @Query(nativeQuery = true,value="select d.tax_rate,sum(tax_amt),sum(qty*purchse_rate),sum(cess_amt) from purchase_hdr h , purchase_dtl d where h.id=d.purchase_hdr_id and h.company_mst=:companyMst and h.branch_code=:branchCode and date(h.voucher_date) between :fromDate and :toDate group by d.tax_rate ")
	  List<Object>purchaseTaxReport(java.util.Date fromDate,java.util.Date toDate,CompanyMst companyMst ,String  branchCode);
	  
	  @Query(nativeQuery = true,value="select"
				+ " h.voucher_number,"
				+ "h.voucher_date,"
				+ "h.invoice_total,"
				+ "s.account_name,"
				+ "i.item_name,"
				+ "d.qty,"
				+ "u.unit_name,"
				+ "d.tax_rate ,d.amount,d.purchse_rate, d.tax_amt,s.party_gst "
				+ "from purchase_hdr h,"
				+ "purchase_dtl d ,"
				+ "item_mst i ,"
				+ "unit_mst u ,"
				+ "account_heads s where h.id=d.purchase_hdr_id"
				+ " and  s.id=h.supplier_id "
				+ "and d.item_id=i.id and"
				+ " d.unit_id=u.id and  h.company_mst=:companyMst"
				+ " and h.branch_code=:branchCode and "
				+ "date(h.voucher_date) between :fromDate and :toDate  ")
			  
			  List<Object>purchaseExportToExcel(java.util.Date fromDate,java.util.Date toDate, CompanyMst companyMst,String branchCode);
		//-------------------version 6.5 surya 

	PurchaseHdr findByVoucherNumberAndCompanyMstAndBranchCode(String invoiceno, CompanyMst companyMst,
			String branchcode);
	//-------------------version 6.5 surya end

	List<PurchaseHdr> findByCompanyMstAndBranchCode(CompanyMst companyMst, String branchcode);
	
	
	@Query
	(nativeQuery = true,value = "select max(h.voucher_number) from purchase_hdr h where "
			+ " h.branch_code=:branchcode and "
			+ "h.company_mst=:companyMst ")
	String getMaxVoucherNo(String branchcode, CompanyMst companyMst);

	@Modifying
	@Query(nativeQuery = true,value = "update purchase_hdr set supplier_id=:id "
			+ "where supplier_id=:supId")
	void updateSupplierId(String supId, String id);

	@Modifying
	@Query(nativeQuery = true,value="update purchase_hdr set supplier_id =:toAccountId where supplier_id =:fromAccountId ")
    void accountMerge(String fromAccountId, String toAccountId);

	List<PurchaseHdr> findByVoucherNumber(String vno);

	void deleteByVoucherNumber(String vno);

	@Query(nativeQuery = true,value = "select s.account_name,"
			+ "p.voucher_number,p.voucher_date,pd.item_name,"
			+ "pd.qty,pd.purchse_rate,pd.amount,p.supplier_inv_no "
			+ "from account_heads s,purchase_hdr p,"
			+ "purchase_dtl pd,item_mst i where "
			+ "s.id=p.supplier_id and pd.purchase_hdr_id=p.id and "
			+ "i.id=pd.item_id and i.category_id=:id and s.account_name = :supname "
			+ "and date(voucher_date) >= :fromDate and date(voucher_date)<=:toDate and p.voucher_number  is not null")
	List<Object> getSupplierwisePurchaseReport(String id, String supname, Date fromDate, Date toDate);

	
	@Query(nativeQuery = true,value = "select s.account_name,"
			+ "p.voucher_number,p.voucher_date,pd.item_name,"
			+ "pd.qty,pd.purchse_rate,pd.amount,p.supplier_inv_no "
			+ "from account_heads s,purchase_hdr p,"
			+ "purchase_dtl pd,item_mst i where "
			+ "s.id=p.supplier_id and pd.purchase_hdr_id=p.id and "
			+ "i.id=pd.item_id and s.account_name = :supname "
			+ "and date(voucher_date) >= :fromDate and date(voucher_date)<=:toDate and p.voucher_number  is not null")
	List<Object> getSupplierwisePurchaseReportWithDate(String supname, Date fromDate, Date toDate);
	  
	
	
	
	@Query(nativeQuery = true,value="select "
			+ "h.tansaction_entry_date ,"
			+ " s.account_name, "
			+ ""
			+ "h.voucher_date, "
			+ "s.party_gst,"
			+ "h.supplier_inv_date,"
			+ " h.invoice_total "
			+ "from purchase_hdr h , account_heads s,purchase_dtl d,item_mst i  where h.id=d.purchase_hdr_id and i.id=d.item_id and h.supplier_id=s.id and  date(h.voucher_date) between :fdate and :tdate and h.branch_code=:branchCode and i.category_id=:catId and h.company_mst=:companyMst")
	List<Object>getPharmacyPurchaseReport(CompanyMst companyMst,Date fdate ,Date tdate,String  branchCode, String catId);

	@Query(nativeQuery = true,value = "select * from purchase_hdr where date(voucher_date) >= :tdate "
			+ "and date(voucher_date) <= :fdate")
	List<PurchaseHdr> findPurchaseHdrBetweenVoucherDate(Date tdate, Date fdate, Pageable topFifty);
	
	
	@Query(nativeQuery = true,value = "select i.item_name,i.hsn_code,u.unit_name,"
			+ "sum(pd.qty),sum(pd.qty*pd.mrp) as value,pd.purchse_rate,"
			+ "pd.tax_rate from purchase_hdr ph,purchase_dtl pd,category_mst c,"
			+ "item_mst i,unit_mst u where "
			+ "date(ph.voucher_date) >= :fromDate and "
			+ "date(ph.voucher_date) <= :toDate and i.category_id=c.id and i.unit_id = u.id "
			+ "and pd.purchase_hdr_id = ph.id and ph.branch_code = :branchcode and "
			+ "ph.company_mst = :companyMst and c.category_name in (:array) and pd.item_id = i.id "
			+ "group by i.item_name,i.hsn_code,u.unit_name,pd.purchse_rate,pd.tax_rate order by i.hsn_code")
	
	List<Object> getHsnPurchaseDtlReport(Date fromDate, Date toDate, String[] array, String branchcode,
			CompanyMst companyMst);
	
	
	@Query(nativeQuery = true,value = "select i.item_name,i.hsn_code,u.unit_name,"
			+ "sum(pd.qty),sum(pd.qty*pd.mrp) as value,pd.purchse_rate,"
			+ "pd.tax_rate from purchase_hdr ph,purchase_dtl pd,"
			+ "item_mst i,unit_mst u where "
			+ "date(ph.voucher_date) >= :fromDate and "
			+ "date(ph.voucher_date) <= :toDate and  i.unit_id = u.id "
			+ "and pd.purchase_hdr_id = ph.id and ph.branch_code = :branchcode and "
			+ "ph.company_mst = :companyMst and pd.item_id = i.id "
			+ "group by i.item_name,i.hsn_code,u.unit_name,pd.purchse_rate,pd.tax_rate order by i.hsn_code")
	List<Object> getHsnPurchaseDtlReportWithoutGroup(Date fromDate, Date toDate, String branchcode,
			CompanyMst companyMst);
	@Query(nativeQuery = true,value = "select h.voucher_number,s.account_name from purchase_hdr h,account_heads s where s.id=h.supplier_id and  date(voucher_date) = :voucherDate and h.company_mst=:CompanyMst "
			)
	List<Object>getPurchaseHdrReport(CompanyMst CompanyMst, Date voucherDate);
	
	
	@Query
	(nativeQuery = true,value = "select * from purchase_hdr where voucher_number=:invoiceno and "
			+ " company_mst=:companyMst and branch_code=:branchcode and date(voucher_date)=:date")
	PurchaseHdr findByVoucherNumberAndCompanyMstAndBranchCodeAndVoucherDate(String invoiceno, CompanyMst companyMst,
			String branchcode, Date date);


	PurchaseHdr save(String purchaseHdrSaved);
	
	
	//...............Sibi......12/05/2021.......//
/*	@Query
	  (nativeQuery = true, value = "select ph.voucher_number,ph.supplier_inv_no, ph.currency,ph.voucher_date,ph.supplier_id,pd.purchase_order_dtl, pd.fc_amount, "
	  		+ "from purchase_hdr ph, purchase_dtl pd ,additional_expense ae ,where ph.id=pd.purchase_hdr_id and ae.purchase_hdr=ph.id"
	  		+ "and date(ph.po_date) >= :fromDate and date(ph.po_date) <= :toDate"
			  )
	List<Object> getImportpurchaseReport(Date fromDate, Date toDate);
*/	

	
	@Query
	  (nativeQuery = true, value = "select AA.voucher_date, "
	  		+ "AA.voucher_number, AA.account_name,"
	  		+ "AA.supplier_inv_no,AA.supplier_inv_date,"
	  		+ "AA.ponum,AA.a as item_total_mvr,"
	  		+ "AA.b as item_total_fc, AA.c as additional_expense, "
	  		+ "AA.d as additional_expense_in_fc,AA.e as included_in_bill,"
	  		+ "AA.f as not_included_in_bill,AA.g as import_duty, "
	  		+ "AA.a+AA.b+AA.c+AA.d+AA.e+AA.f+AA.g as grand_total "
	  		+ "from "
	  		+ "(select  "
	  		+ "h.voucher_date, "
	  		+ "h.voucher_number,"
	  		+ "s.account_name, "
	  		+ "h.supplier_inv_no, "
	  		+ "h.supplier_inv_date, "
	  		+ "h.ponum,sum(d.amount) as  a , "
	  		+ "sum(d.fc_amount) as b , "
	  		+ "sum(ae.amount) as c ,"
	  		+ "sum(ae.fc_amount) as d,"
	  		+ "sum(case when ae.expense_head='NOT IN BILL' THEN ae.amount END ) as e,"
	  		+ "sum(case when ae.expense_head='INCLUDED IN BILL' THEN ae.amount END) as f,"
	  		+ "sum(case when paed.expense='IMPORT DUTY' THEN paed.amount END) as g "
	  		+ "from  "
	  		+ "purchase_dtl d,"
	  		+ "purchase_hdr h,"
	  		+ "additional_expense ae ,"
	  		+ "purchase_additional_expense_dtl paed,"
	  		+ "purchase_additional_expense_hdr paeh,"
	  		+ "account_heads s "
	  		+ "where "
	  		+ "h.id=d.purchase_hdr_id and h.id=ae.purchase_hdr and d.id=paeh.purchase_dtl "
	  		+ "and paeh.id=paed.purchase_additional_expense_hdr "
	  		+ "and h.supplier_id=s.id "
	  		+ "AND date(h.voucher_date) >=:fromDate "
	  		+ "AND date(h.voucher_date) <= :toDate "
	  		+ "group by "
	  		+ "h.voucher_date,"
	  		+ "h.voucher_number,"
	  		+ "s.account_name,"
	  		+ "h.supplier_inv_no,"
	  		+ "h.supplier_inv_date,"
	  		+ "h.ponum,h.voucher_number)AA")
	List<Object> getImportpurchaseReport(Date fromDate, Date toDate);

	
	
	
	/*
	 * // @Query // (nativeQuery = true, value
	 * ="SELECT sup.supplier_name,sup.address,sup.emailid,sup.supgst," // +
	 * "phr.voucher_number,phr.voucher_date,phr.supplier_inv_no,phr.supplier_inv_date,"
	 * // + "itm.item_code,itm.item_name," // +
	 * "pd.batch,pd.expiry_date,pd.qty,pd.fc_purchase_rate,pd.fc_amount,pd.fc_net_cost,pd.fc_tax_rate,"
	 * // + "um.unit_name ,phr.invoice_total,sup.state,pd.fc_tax_amt " // +
	 * "FROM purchase_hdr phr,account_heads sup,item_mst itm,purchase_dtl pd,unit_mst um "
	 * // + "WHERE " // + "phr.id = :phdrid AND " // +
	 * "phr.supplier_id = sup.id AND " // + "pd.item_id = itm.id AND " // +
	 * "pd.purchase_hdr_id = phr.id AND " // + "pd.unit_id = um.id") // List<Object>
	 * getImportPurchaseInvoiceDetailRepo(String phdrid);
	 */	
	
		//new query modifications january 10 -2022 
	@Query
	  (nativeQuery = true, value ="SELECT sup.account_name,sup.party_address1,sup.party_mail,"
	  		+ "sup.party_gst,phr.voucher_number,phr.voucher_date,phr.supplier_inv_no,"
	  		+ "phr.supplier_inv_date,itm.item_code,itm.item_name,pd.batch,pd.expiry_date,"
	  		+ "pd.qty,pd.fc_purchase_rate,pd.fc_amount,pd.fc_net_cost,pd.fc_tax_rate,um.unit_name ,"
	  		+ "phr.invoice_total,sup.customer_state,pd.fc_tax_amt "
	  		+ "FROM purchase_hdr phr,account_heads sup,item_mst itm,purchase_dtl pd,unit_mst um"
	  		+ " WHERE phr.id = :phdrid "
	  		+ "AND phr.supplier_id = sup.id AND pd.item_id = itm.id AND "
	  		+ "pd.purchase_hdr_id = phr.id AND pd.unit_id = um.id")
	List<Object> getImportPurchaseInvoiceDetailRepo(String phdrid);
	
	
	
	@Query
	  (nativeQuery = true, value ="SELECT sup.account_name,sup.party_address1,sup.party_mail,"
	  		+ "sup.party_gst,phr.voucher_number,phr.voucher_date,phr.supplier_inv_no,"
	  		+ "phr.supplier_inv_date,itm.item_code,itm.item_name,pd.batch,pd.expiry_date,"
	  		+ "pd.qty,pd.purchse_rate,pd.amount,pd.net_cost,pd.tax_rate,um.unit_name ,"
	  		+ "phr.invoice_total,sup.customer_state,pd.tax_amt "
	  		+ "FROM purchase_hdr phr,account_heads sup,item_mst itm,purchase_dtl pd,unit_mst um"
	  		+ " WHERE phr.id = :phdrid "
	  		+ "AND phr.supplier_id = sup.id AND pd.item_id = itm.id AND "
	  		+ "pd.purchase_hdr_id = phr.id AND pd.unit_id = um.id")
	List<Object> getImportPurchaseInvoiceDetail(String phdrid);
	
	
	
	

	
	
	@Query
	  (nativeQuery = true, value ="SELECT SUM(ae.amount) FROM additional_expense ae WHERE ae.purchase_hdr=:phdrid")
	Double getTotalAdditionalExpenseByHdrIdRepo(String phdrid);


	@Query
	  (nativeQuery = true, value ="SELECT SUM(paed.amount) "
	  		+ "FROM purchase_additional_expense_dtl paed, "
	  		+ "purchase_additional_expense_hdr hdr "
	  		+ "WHERE paed.purchase_additional_expense_hdr=hdr.id "
	  		+ "and "
	  		+ "hdr.purchase_dtl IN (SELECT pd.id FROM purchase_dtl pd "
	  		+ "where pd.purchase_hdr_id=:phdrid)"
	  		+ "")
	Double getTotalImportExpenseRepo(String phdrid);
	
	
	@Query
	  (nativeQuery = true, value ="SELECT sup.account_name,sup.party_address1,sup.party_mail,sup.party_gst,"
	  		+ "phr.voucher_number,phr.voucher_date,phr.supplier_inv_no,phr.supplier_inv_date,"
	  		+ "itm.item_code,itm.item_name,"
	  		+ "pd.batch,pd.expiry_date,pd.qty,pd.purchse_rate,pd.amount,pd.net_cost,pd.tax_rate,"
	  		+ "um.unit_name, phr.invoice_total,sup.customer_state,pd.tax_amt "
	  		+ "FROM purchase_hdr phr,account_heads sup,item_mst itm,purchase_dtl pd,unit_mst um "
	  		+ "WHERE "
	  		+ "phr.id = :phdrid AND "
	  		+ "phr.supplier_id = sup.id AND "
	  		+ "pd.item_id = itm.id AND "
	  		+ "pd.purchase_hdr_id = phr.id AND "
	  		+ "pd.unit_id = um.id order by pd.item_serial")
	List<Object> getImportPurchaseInvoiceReportByHdrId(String phdrid);
	
	
	
	@Query(nativeQuery = true, value ="SELECT "
			+ "ph.voucher_date,"
			+ "ph.voucher_number,"
			+ "acc.account_name,"
			+ "acc.party_gst,"
			+ "ph.supplier_inv_no,"
			+ "ph.supplier_inv_date,"
			+ "ph.ponum,"
			+ "ph.invoice_total,"
			+ "pd.purchse_rate,"
			+ "pd.tax_amt,"
			+ "um.user_name "
			+ "FROM "
			+ "purchase_hdr ph,"
			+ "purchase_dtl pd,"
			+ "account_heads acc,"
			+ "user_mst um "
			+ "WHERE "
			+ "ph.id = pd.purchase_hdr_id AND "
			+ "acc.id = ph.supplier_id AND "
			+ "um.id = ph.user_id AND "
			+ "date(ph.voucher_date) >= :fromDate AND "
			+ "date(ph.voucher_date) <= :toDate AND (acc.customer_or_supplier = 'SUPPLIER' OR acc.customer_or_supplier = 'BOTH')")
	List<Object> getLocalPurchaseSummaryReport(Date fromDate, Date toDate);
	
	
	@Query(nativeQuery = true, value ="SELECT "
			  + "ph.voucher_date,"
			  + "ph.voucher_number,"
		      + "ph.tansaction_entry_date,"
			  + "ph.invoice_total,"
			  + "sup.account_name,"
			  + "sup.party_gst,"
			  + "ph.supplier_inv_no,"
			  + "ph.supplier_inv_date,"
			  + "ph.ponum,"
			  + "pd.purchse_rate,"
			  + "pd.tax_amt,"
			  + "i.item_name,"
			  + "i.item_code,"
			  + "i.item_group_id,"
			  + "pd.batch,"
			  + "pd.expiry_date,"
			  + "pd.qty,"
			  + "pd.amount,"
			  + "um.user_name "
			+ "FROM "
			  + "purchase_hdr ph,"
			  + "purchase_dtl pd,"
			  + "account_heads sup,"
			  + "user_mst um,"
			  + "item_mst i "
			+ "WHERE "
			  + "ph.id = pd.purchase_hdr_id AND "
			  + "sup.id = ph.supplier_id AND "
			  + "um.id = ph.user_id AND "
			  + "pd.item_id=i.id AND "
              + "date(ph.voucher_date) >= :fromDate AND "
			  + "date(ph.voucher_date) <= :toDate")
			
	List<Object> getPharmacyLocalPurchaseDetailsReport(Date fromDate, Date toDate);


	
	@Query(nativeQuery = true, value ="SELECT "
			+ "prh.return_voucher_date,"
			+ "prh.voucher_number,"
			+ "sup.account_name,"
			+ "prh.return_voucher_number,"
			+ "itm.item_name,"
			+ "cat.category_name,"
			+ "itm.item_code,"
			+ "prd.batch,"
			+ "prd.purchase_rate,"
			+ "prd.amount,"
			+ "prd.expiry_date,"
			+ "prd.qty,"
			+ "prd.purchse_rate,"
			+ "prd.tax_rate,"
			+ "prd.purchase_rate * prd.qty,"
			+ "usr.user_name "
			+ "FROM "
			+ "purchase_return_hdr prh,"
			+ "purchase_return_dtl prd,"
			+ "account_heads sup,"
			+ "user_mst usr,"
			+ "item_mst itm,"
			+ "category_mst cat "
			+ "WHERE "
			+ "prd.purchas_return_hdr = prh.id AND "
			+ "prh.supplier_id = sup.id AND "
			+ "prh.user_id = usr.id AND "
			+ "prd.item_id = itm.id AND "
			+ "itm.category_id = cat.id AND "
			+ "date(prh.return_voucher_date) >= :fdate AND "
			+ "date(prh.return_voucher_date) <= :tdate")
	List<Object> getPurchaseReturnDetailAndSummaryData(Date fdate, Date tdate);


	
	

	@Query(nativeQuery = true, value ="SELECT "
			+ "prh.return_voucher_date,"
			+ "prh.voucher_number,"
			+ "sup.account_name,"
			+ "prh.return_voucher_number,"
			+ "itm.item_name,"
			+ "cat.category_name,"
			+ "itm.item_code,"
			+ "prd.batch,"
			+ "prd.purchase_rate,"
			+ "prd.amount,"
			+ "prd.expiry_date,"
			+ "prd.qty,"
			+ "prd.purchse_rate,"
			+ "prd.tax_rate,"
			+ "prd.purchase_rate * prd.qty,"
			+ "usr.user_name,"
			+ "prh.supplier_inv_date,"
			+ "prh.supplier_inv_no,"
			+ "sup.party_address1,"
			+ "sup.party_mail,"
			+ "sup.customer_state,"
			+ "prh.voucher_date,"
			+ "um.unit_name,"
			+ "prd.unit "
			+ "FROM "
			+ "purchase_return_hdr prh,"
			+ "purchase_return_dtl prd,"
			+ "account_heads sup,"
			+ "user_mst usr,"
			+ "item_mst itm,"
			+ "unit_mst um,"
			+ "category_mst cat "
			+ "WHERE "
			+ "prd.purchas_return_hdr = prh.id AND "
			+ "prh.supplier_id = sup.id AND "
			+ "prh.user_id = usr.id AND "
			+ "prd.item_id = itm.id AND "
			+ "itm.category_id = cat.id AND "
			+ "prd.unit_id=um.id AND  "
			+ "prh.id=:purchasehdrid AND "
			+ "prh.company_mst=:companyMst")
	     List<Object> getPurchaseReturnInvoiceReport(CompanyMst companyMst, String purchasehdrid);

	

	@Query(nativeQuery = true, value ="SELECT "
			+ "SUM(prd.qty) from purchase_return_dtl prd WHERE prd.Purchase_dtl_id=:purchasedtlId ")
		
           Double getpurchaseReturnDtlQty(String purchasedtlId);



	
	@Query(nativeQuery = true, value = "SELECT count(*)  FROM purchase_hdr ph where date(ph.voucher_date)=:fromDate and "
			+ "ph.company_mst=:companyMst and ph.branch_code=:branchcode") 
	Double getPurchaseHdrCount(CompanyMst companyMst, Date fromDate,String branchcode);


	
	
	
	@Modifying
	@Query(nativeQuery = true, value ="delete from purchse_dtl where "
			+ "id in (select d.id from purchse_dtl d, purchase_hdr h where d.purchase_hdr_id=h.id and "
			+ " date(h.voucher_date)<=:date and h.company_mst=:companyMst)")
	void deletePurchaseDtlsByVoucherDate(CompanyMst companyMst, Date date);

	@Modifying
	@Query(nativeQuery = true, value ="delete from purchase_hdr where "
			+ "date(voucher_date)<=:date and company_mst=:companyMst)")
	void deletePurchaseHdrByVoucherDate(CompanyMst companyMst, Date date);

	@Modifying
	@Query(nativeQuery = true, value = "update purchase_hdr set posted_to_server='YES' where id=:hdrIds")
	void updatePurchasePostedToServerStatus(String hdrIds);

	@Query(nativeQuery = true, value = " select count(*) from purchase_hdr where company_mst=:companymstid and date(voucher_date)>=:purchaseBestDate and "
			+ "date(voucher_date)<=:dayendDate and branch_code=:branchcode")
	int getCountofPurchaseForDateWindow(String companymstid, Date purchaseBestDate, Date dayendDate,
			String branchcode);
	
	
	
	@Query(nativeQuery = true,value = "select * from purchase_hdr where date(voucher_date) >= :tdate "
			+ "and date(voucher_date) <= :fdate and id not in (:hdrIds)")
	List<PurchaseHdr> findPurchaseHdrBetweenVoucherDate(Date tdate, Date fdate, Pageable topFifty,String hdrIds);

	@Query(nativeQuery = true,value ="select * from purchase_hdr where date(voucher_date) >= :fromDate "
			+ "and date(voucher_date) <= :toDate and voucher_number is not null")
	List<PurchaseHdr> findAllPurchaseBetweenDate(Date fromDate, Date toDate);

	
	
}

