package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PrinterMst;
 

@Repository
public interface PrinterMstRepository extends  JpaRepository<PrinterMst, String >{

	PrinterMst findByCompanyMstAndId(CompanyMst companyMst,String id);
	List<PrinterMst>findByCompanyMst(CompanyMst companyMst);
	
	PrinterMst findByCompanyMstAndPrinterName(CompanyMst companyMst,String printername);
	Optional<PrinterMst> findById(String id);
}
