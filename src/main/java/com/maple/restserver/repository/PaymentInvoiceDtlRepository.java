package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PaymentInvoiceDtl;
@Repository
public interface PaymentInvoiceDtlRepository extends JpaRepository<PaymentInvoiceDtl, String> {

	List<PaymentInvoiceDtl> findByPaymentHdr(PaymentHdr paymentHdr);
	
	 @Query(nativeQuery=true,value="select sum(d.paid_amount) from payment_invoice_dtl d where d.payment_hdr=:paymenthdr")
	Double sumOfPaidAmount(PaymentHdr paymenthdr);
	
	 @Query(nativeQuery=true,value="select sum(d.paid_amount) from payment_invoice_dtl d where d.account_heads=:accountHeads and d.invoice_number='OWN ACCOUNT'  ")
	 Double sumOfPaymentInvoiceDtlPaidAmount(AccountHeads accountHeads);
	 

	
	
	 

		@Modifying
		@Query(nativeQuery=true,value="delete from payment_invoice_dtl d where d.payment_hdr =:paymentHdrId")
	void  deleteByPaymentHdrId(String paymentHdrId);
	 

		@Modifying
		@Query(nativeQuery = true,value="update payment_invoice_dtl d set d.account_heads =:toAccountId where d.account_heads =:fromAccountId ")
	    void accountMerge(String fromAccountId, String toAccountId);   
}
