package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CustomerServiceStatusHdr;

@Repository
public interface CustomerServiceStatusHdrRepository extends JpaRepository<CustomerServiceStatusHdr,String>{

}
