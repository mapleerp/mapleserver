package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SaleOrderEdit;
import com.maple.restserver.entity.SaleOrderReceipt;
import com.maple.restserver.entity.SalesOrderTransHdr;

@Repository
public interface SaleOrderEditRepository  extends JpaRepository<SaleOrderEdit, String>{

	List<SaleOrderEdit>  findByCompanyMstAndSalesOrderTransHdr(CompanyMst companyMst ,SalesOrderTransHdr  salesordertrandhdrid);

	  @Query(nativeQuery=true,value="  select sum((rate*qty) + (  (rate*qty) * (tax_rate)/100 )) as amount  from sale_order_edit s where s.sales_order_trans_hdr_id=:salesordertrandhdrid")
Double	retrieveSalesorderEditAmount(String salesordertrandhdrid);

	SaleOrderEdit findBySalesOrderTransHdrAndItemIdAndBatchAndBarodeAndUnitIdAndRate(
			SalesOrderTransHdr saleordertranshdr, String itemId, String batch, String barode, String unitId,
			Double rate);


}
