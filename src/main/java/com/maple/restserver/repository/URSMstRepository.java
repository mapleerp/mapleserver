package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.UrsMst;
@Repository
@Transactional
public interface URSMstRepository extends JpaRepository<UrsMst, String> {

	@Query(nativeQuery = true,value = "select * from urs_mst where date(trans_date)=:vdate")
	List<UrsMst> getAllByDate(Date vdate);
	
	@Query(nativeQuery = true,value = "select * from urs_mst where date(trans_date)=:vdate "
			+ "and item_id=:itemId")
	UrsMst getAllByDateAndItemId(Date vdate,String itemId);

	@Query(nativeQuery =true,value ="select * from urs_mst where "
			+ "date(trans_date) >= :tdate and date(trans_date)<=:fdate" )
	List<UrsMst> getUrsBetweenDate(Date tdate, Date fdate);
}
