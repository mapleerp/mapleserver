package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.KotItemMst;

@Repository
public interface KotItemMstRepository extends JpaRepository<KotItemMst,String>{

	List<KotItemMst> findByCompanyMst(CompanyMst companyMst);

	List<KotItemMst> findByItemId(String id);

	List<KotItemMst> findByCompanyMstAndItemId(CompanyMst companyMst, String id);

}
