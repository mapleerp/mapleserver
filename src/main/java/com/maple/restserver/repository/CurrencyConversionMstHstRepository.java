package com.maple.restserver.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CurrencyConversionMstHst;

@Repository
@Controller
@Transactional
public interface CurrencyConversionMstHstRepository extends JpaRepository<CurrencyConversionMstHst,String>{

}
