package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.MonthlyJournalDtl;
@Repository
@Component
public interface MonthlyJournalDtlRepository extends JpaRepository<MonthlyJournalDtl,String>{

	
	
	@Query(nativeQuery = true,value="select sum(d.debit_amount) from monthly_journal_hdr h,monthly_journal_dtl d where h.id=d.monthly_journal_hdr and h.voucher_number=:hdrVoucherNumber")
	Double sumOfDebitAmount(String hdrVoucherNumber);
	
	@Query(nativeQuery = true,value="select sum(d.credit_amount) from monthly_journal_hdr h,monthly_journal_dtl d where h.id=d.monthly_journal_hdr and h.voucher_number=:hdrVoucherNumber")
	Double sumOfCreditAmount(String hdrVoucherNumber);

	@Query(nativeQuery = true,value="select sum(d.debit_amount) from monthly_journal_hdr h,monthly_journal_dtl d where h.id=d.monthly_journal_hdr and h.voucher_number=:hdrVoucherNumber")
	List<Double> sumOfDebitAmountCrAmount(String hdrVoucherNumber);
	
}
