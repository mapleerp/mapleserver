package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.InsuranceCompanyDtl;

@Component
@Repository
public interface InsuranceCompanyDtlRepository extends JpaRepository<InsuranceCompanyDtl,String>{

	InsuranceCompanyDtl findByCompanyMstIdAndPolicyType(String companymstid, String policytype);

	InsuranceCompanyDtl findByCompanyMstIdAndId(String companymstid, String insurancecompanydtlid);

	List<InsuranceCompanyDtl> findByCompanyMst(CompanyMst companyMst);

	InsuranceCompanyDtl findByCompanyMstIdAndInsuranceCompanyMst(String companymstid, String insurancecompanymstid);

	InsuranceCompanyDtl findByCompanyMstAndId(CompanyMst companyMst, String id);

	List<InsuranceCompanyDtl> findByInsuranceCompanyMstId(String hdrid);




	



}
