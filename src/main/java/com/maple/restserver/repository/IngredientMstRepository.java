package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.IngredientsMst;
import com.maple.restserver.entity.ItemMst;

public interface IngredientMstRepository extends JpaRepository<IngredientsMst,String> {

	IngredientsMst findByItemMst(ItemMst itemMst);
	
	IngredientsMst	findByItemMstId(String id);
}
