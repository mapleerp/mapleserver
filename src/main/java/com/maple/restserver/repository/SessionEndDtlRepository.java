package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SessionEndDtl;
@Repository
public interface SessionEndDtlRepository extends JpaRepository<SessionEndDtl, String> {

}
