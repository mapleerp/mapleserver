package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.NutritionValueMst;

public interface NutritionValueMstRepository extends JpaRepository<NutritionValueMst, String> {

	NutritionValueMst findByCompanyMstIdAndItemMstId(String companyid,String itemid);
}
