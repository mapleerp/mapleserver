package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
 
import com.maple.restserver.entity.StockTransferOutDtl;
 
@Repository
public interface KitDefenitionDtlRepository extends JpaRepository<KitDefenitionDtl, String> {

 
	 List<KitDefenitionDtl >findByItemIdAndQtyAndKitDefenitionmst(String itemId,Double qty,KitDefinitionMst mstid);
	
	
 
	 //version2.8
	 List<KitDefenitionDtl >findByItemIdAndKitDefenitionmst(String itemId,KitDefinitionMst mstid);
	//version2.8ends

List<KitDefenitionDtl> findBykitDefenitionmstId(String kitdefinitionMstId );

@Modifying
@Query(nativeQuery = true,value = "update kit_defenition_dtl set barcode =:barCode where item_id=:itemid")
void updateBarcode(String itemid, String barCode);




 
}
