package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProductionPreplanningDtl;
import com.maple.restserver.entity.ProductionPreplanningMst;
@Repository
public interface ProductionPreplanningDtlRepository extends JpaRepository<ProductionPreplanningDtl,String>{

	
	
	@Query(nativeQuery = true,value="select * from production_preplanning_mst h where "
			+ "h.company_mst=:companyMst and h.branch_code=:branchCode and date(h.voucher_date) =:sdate")
	ProductionPreplanningMst getProductionPreplanningMst( Date sdate,CompanyMst  companyMst, String branchCode);
	
	
	@Query(nativeQuery = true,value="select i.item_name,d.qty, d.batch,d.id,d.status,d.item_id from production_preplanning_mst h,production_preplanning_dtl d,item_mst i "
			+ " where  i.id=d.item_id and h.id=d.production_preplanning_mst and h.branch_code=:branchCode  and h.company_mst=:companyMst "
			+ "   and d.status='PREPLANNING'")
	List<Object>getProductionPreplanningDtl( CompanyMst  companyMst ,String branchCode);
//and date(h.voucher_date)<:sdate

	@Modifying
	@Query(nativeQuery = true,value="update production_preplanning_dtl d  set d.status='MOVEDTOPLANING' where d.id=:productionpreplaningdtlid  ")
	void updateProductionPlaningStatus(String productionpreplaningdtlid);


}
