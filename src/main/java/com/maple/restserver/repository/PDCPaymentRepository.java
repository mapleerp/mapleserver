package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PDCPayment;

@Repository
public interface PDCPaymentRepository extends JpaRepository<PDCPayment, String> {

	
	List<PDCPayment> findByTransDateAndCompanyMstAndBranchCode(Date cdate,CompanyMst companyMst,String branchcode);
	
	List<PDCPayment> findByStatusAndBankAndCompanyMst(String string, String bankname, CompanyMst companyMst);

	List<PDCPayment> findByAccountAndStatus(String accountid, String status);


}
