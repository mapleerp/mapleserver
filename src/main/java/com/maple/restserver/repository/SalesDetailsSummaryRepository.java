package com.maple.restserver.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesDtlSummary;
import com.maple.restserver.entity.SalesTransHdr;

@Repository
public interface SalesDetailsSummaryRepository extends JpaRepository<SalesDtlSummary, String> {
	
	
}
