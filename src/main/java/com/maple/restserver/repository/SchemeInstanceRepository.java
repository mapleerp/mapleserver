package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SchemeCategory;
import com.maple.restserver.entity.SchemeInstance;

@Repository
public interface SchemeInstanceRepository extends JpaRepository<SchemeInstance,String>  {

	SchemeInstance findBySchemeNameAndCompanyMstId(String shemename, String companymst);

	List<SchemeInstance> findByCompanyMst(CompanyMst companyMst);


	 @Query(nativeQuery=true,value="select sch.* from scheme_instance sch "
	 		+ "where sch.company_mst=:companyMst "
	 		+ " and sch.is_active=:active and "
	 		+ " (sch.scheme_wholesale_retail=:retailorwholesale OR sch.scheme_wholesale_retail='Both')")
	List<SchemeInstance> findByCompanyMstAndSchemeWholesaleRetailAndIsActive(String companyMst,
			String retailorwholesale, String active);

	SchemeInstance findByIdAndCompanyMst(String id, CompanyMst companyMst);

	
	 @Query(nativeQuery=true,value="select sum(d.qty), c.id "
	 		+ "from sales_trans_hdr h, category_mst c, sales_dtl d , item_mst i  "
	 		+ "where d.sales_trans_hdr_id = h.id and i.category_id = c.id "
	 		+ "and i.id = d.item_id and h.id = :hdrid group by c.id")
	List<Object> getSchemeCategory(String hdrid );

	 
	 @Query(nativeQuery=true,value="select attribute_value from sch_offer_attr_inst "
	 		+ "where attribute_name = :attriValue and offer_id = :offer and scheme_id = :schemeId")
	String getAttributeValueByOfferAttriName(String attriValue, String offer, String schemeId);

	 @Query(nativeQuery=true,value="select attribute_value from sch_eligibility_attrib_inst "
		 		+ "where attribute_name = :attriValue and eligibility_id = :eligibility and scheme_id = :schemeId")
	String getAttriValueByEligiAttriName(String attriValue, String eligibility, String schemeId);
	
	
	

}
