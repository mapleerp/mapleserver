package com.maple.restserver.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PriceDefinition;
@Repository
@Transactional
public interface PriceDefinitionRepository extends JpaRepository<PriceDefinition, String>{

	  List<PriceDefinition> findByItemId(String itemid);
	   List<PriceDefinition> findByPriceId(String priceId);
	   List<PriceDefinition> findByStartDate(Date date);
	   @Query(nativeQuery = true, value ="select p.amount from price_definition p where p.item_id=:itemid and p.price_id=:priceId")
	   Double FetchAmountBy(String itemid,String priceId);
	   List<PriceDefinition>findByEndDate(Date date);
	@Query(nativeQuery = true, value ="select * from price_definition p where p.end_date is null")
    List<PriceDefinition> fetchPriceDefinition();

	@Modifying
	@Query(nativeQuery = true, value ="update price_definition p set p.end_date=:startdate where p.item_id=:itemid and p.price_id =:priceId and p.unit_id =:unitId and p.end_date is null")
     void UpdatePriceDefinitionbyItemIdUnitId(String itemid,Date startdate,String priceId,String unitId);
	
	
	@Modifying
	@Query(nativeQuery = true, value ="update price_definition p set p.end_date=:startdate where p.item_id=:itemid and p.price_id =:priceId and p.end_date is null")
     void UpdatePriceDefinitionbyItemId(String itemid,Date startdate,String priceId);
	
	
	@Query(nativeQuery = true, value ="select * from price_definition p where p.end_date is null and p.item_id =:itemId and p.price_id=:priceId")
    List<PriceDefinition> findByItemIdAndStartDate(String itemId,String priceId);
	
	/*@Query(nativeQuery = true, value =" select p.* from price_definition p where p.item_id =:itemId and p.price_id=:priceId"
			+ " and p.unit_id=:unitid and ((:sdate between date(p.start_date) and date(p.end_date)) "
			+ " or (date(p.start_date) <= :sdate and date(p.end_date) is null))")
    PriceDefinition findByItemIdAndStartDateCostprice(String itemId,String priceId,Date sdate,String unitid);
	*/
	
	/*
	 * In case below query doesnt return a record then use date between as a different call.
	 * Do not change below query to address both scenario
	 */
	
@Query(nativeQuery = true, value =" select p.* from price_definition p where p.item_id =:itemId and p.price_id=:priceId"
	+ " and p.unit_id=:unitid and   (date(p.start_date) <= :sdate and date(p.end_date) is null)")
PriceDefinition findByItemIdAndStartDateCostprice(String itemId,String priceId,Date sdate,String unitid);

@Query(nativeQuery = true, value =" select p.* from price_definition p where p.item_id =:itemId and p.price_id=:priceId"
		+ " and p.unit_id=:unitid and   (date(p.start_date) <= :sdate and date(p.end_date) is null)")
	List<PriceDefinition> findByItemIdAndStartDateCostpriceList(String itemId,String priceId,Date sdate,String unitid);


	
	@Query(nativeQuery = true, value ="select * from price_definition p where"
			+ " date(p.end_date)>=:tdate and date(p.start_date) <:tdate "
			+ " and p.item_id =:itemId and p.price_id=:priceId and p.unit_id =:unitId")
    List<PriceDefinition> findByItemIdAndUnitId(String itemId,String priceId,String unitId,Date tdate);
	
	
	@Query(nativeQuery = true, value ="select * from price_definition p where"
			+ " date(p.end_date) is null and date(p.start_date) <=:tdate "
			+ " and p.item_id =:itemId and p.price_id=:priceId and p.unit_id =:unitId")
    List<PriceDefinition> findByItemIdAndUnitIdEnddateNull(String itemId,String priceId,String unitId,Date tdate);
	
	
	@Modifying(flushAutomatically = true)
    @Query(nativeQuery = true, value ="update price_definition ud set unit_id =(select im.unit_id from item_mst im where im.id=ud.item_id and im.branch_code =:branchcode) where unit_id is null")
    void	updatePriceDefenition(String branchcode );
	
	
	List<PriceDefinition> findByCompanyMst(CompanyMst companyMst);
	
	
	@Query(nativeQuery = true, value ="select p.* from price_definition p where p.end_date is null "
			+ "and p.item_id =:itemid and p.price_id=:priceid and p.unit_id=:unitid and p.company_mst=:companymstid")
	PriceDefinition findPriceDefenitionByItemUnitPriceAndDate(String itemid, String priceid, String unitid,String companymstid);
	
	//----------------------version 6.12 surya 

	@Query(nativeQuery = true, value ="select i.id,i.item_name from item_mst i"
			+ " where i.id not in:itemIds "
			+ " and "
			+ "i.COMPANY_MST=:companyMst"
			+ "")
	List<Object> getAllExceptionItems(CompanyMst companyMst,List<String> itemIds);
	
	@Query(nativeQuery = true, value ="select  d.item_id from price_definition d,price_defenition_mst m where "
			+ "d.PRICE_ID=m.id and m.PRICE_LEVEL_NAME='COST PRICE' and "
			+ "d.COMPANY_MST=:companyMst"
			+ "")
	List<String> ItemsInCostPrice(CompanyMst companyMst);
	
	
	@Query("SELECT f FROM PriceDefinition f, ItemMst i WHERE i.id=f.itemId AND i.categoryId=:categoryid AND f.companyMst=:companyMst")

	List<PriceDefinition> getPriceDefinitionByCategoryId(String categoryid, CompanyMst companyMst);

	
	//----------------------version 6.12 surya end

	
	}
