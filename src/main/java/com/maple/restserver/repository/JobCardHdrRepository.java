package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JobCardHdr;
@Repository
public interface JobCardHdrRepository extends JpaRepository<JobCardHdr,String>{

	@Query(nativeQuery = true,value = "select h.* from job_card_hdr h "
			+ "where h.company_mst=:companyMst and "
			+ "(h.status='ACTIVE' OR h.status='READY FOR DELIVERY') ")
	List<JobCardHdr> findByCompanyMstAndStatus(CompanyMst companyMst);

	@Query(nativeQuery = true,value = "select h.* from job_card_hdr h "
			+ "where h.company_mst=:companyMst and "
			+ "date(h.voucher_date) between :sdate and :edate ")
	List<JobCardHdr> findByCompanyMstBetweenDate(CompanyMst companyMst, Date sdate, Date edate);

	
//	@Query(nativeQuery = true,value = "select c.customer_name, "
//			+ "sdtl.item_name, "
//			+ "sdtl.complaints, "
//			+ "sdtl.observation, "
//			+ "shdr.voucher_number, "
//			+ "shdr.voucher_date, "
//			+ "jh.id from service_in_hdr shdr, service_in_dtl sdtl, "
//			+ "customer_mst c, job_card_hdr jh where "
//			+ "jh.service_in_dtl=sdtl.id and sdtl.service_in_hdr=shdr.id and "
//			+ "c.id=jh.customer_id and (jh.status='ACTIVE' OR jh.status='READY FOR DELIVERY') and jh.company_mst=:companyMst "
//			+ "and date(shdr.voucher_date) between :sdate and :edate"
//			+ "")
	
	
	@Query(nativeQuery = true,value = "select c.account_name, "
			+ "sdtl.item_name, "
			+ "sdtl.complaints, "
			+ "sdtl.observation, "
			+ "shdr.voucher_number, "
			+ "shdr.voucher_date, "
			+ "jh.id from service_in_hdr shdr, service_in_dtl sdtl, "
			+ "account_heads c, job_card_hdr jh where "
			+ "jh.service_in_dtl=sdtl.id and sdtl.service_in_hdr=shdr.id and "
			+ "c.id=jh.customer_id and (jh.status='ACTIVE' OR jh.status='READY FOR DELIVERY') and jh.company_mst=:companyMst "
			+ "and date(shdr.voucher_date) between :sdate and :edate"
			+ "")
	List<Object> findAllActiveJobCardReport(CompanyMst companyMst, Date sdate, Date edate);

	
	@Query(nativeQuery = true,value = "select sum(j.qty*i.standard_price) as amount "
			+ "from item_mst i, job_card_hdr h, job_card_dtl j where "
			+ "j.job_card_hdr=h.id and i.id=j.item_id and h.company_mst=:companyMst and "
			+ "h.id=:jobCardHdr")
	Double findAllActiveJobCardReport(CompanyMst companyMst, JobCardHdr jobCardHdr);

	/*
	 * @Query(value="select from job_card_hdr  ") List<Object> getjobcarddetail();
	 */
}
