package com.maple.restserver.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SaleOrderProductionPlaningLinkMst;

@Repository
public interface SaleOrderProductionPlaningLinkMstRepository extends JpaRepository<SaleOrderProductionPlaningLinkMst, String> {

	
	
	@Modifying
	@Query(nativeQuery = true,value="delete from sale_order_production_planing_link_mst sp "
			+ "where date(sp.voucher_date) >=:fromDate and  date(sp.voucher_date)<=:tdate")
	
void deleteSaleOrderProductionPlaningLinkMst(Date fromDate,Date tdate);
}
