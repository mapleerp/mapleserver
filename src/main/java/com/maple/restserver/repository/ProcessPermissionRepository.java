package com.maple.restserver.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProcessPermissionMst;

public interface ProcessPermissionRepository extends JpaRepository<ProcessPermissionMst,String>{

	
	List<ProcessPermissionMst> findByProcessId(String processId);
	
	List<ProcessPermissionMst>findByUserId(String userId);

	List<ProcessPermissionMst> findByCompanyMst(CompanyMst companyMst);

	@Query(nativeQuery = true,value = "select ppm.* from process_permission_mst ppm,process_mst pm "
			+ "where  ppm.process_id=pm.id  and pm.process_name=:processname")
	List<ProcessPermissionMst> getprocessPermissionMstByProcessName(String processname);
	
}
