package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.PurchasePriceDefinitionDtl;
import com.maple.restserver.entity.PurchasePriceDefinitionMst;
@Component
@Repository
public interface PurchasePriceDefinitionDtlRepository extends JpaRepository<PurchasePriceDefinitionDtl, String>{

	
	 
	List<PurchasePriceDefinitionDtl> findByPurchaseHdrIdAndCompanyMstId(String hdrid, String companymstid);

	 
	
	

}
