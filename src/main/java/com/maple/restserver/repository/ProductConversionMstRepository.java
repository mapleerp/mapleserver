package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ProductConversionMst;

@Repository
public interface ProductConversionMstRepository extends JpaRepository<ProductConversionMst, String> {
	ProductConversionMst findByIdAndCompanyMstId(String id ,String companymstid);


	  
	  @Query(nativeQuery =true,value
	  ="select i.item_name,i.standard_price ,d.from_qty as fromqty , 0.0 as toqty,m.conversion_date from product_conversion_mst m,"
	  		+ "product_conversion_dtl d ,item_mst i where m.id=d.product_conversion_mst_id and m.company_mst=:companymstid "
	  +
	  " and i.id=d.from_item and m.branch_code=:branchcode and conversion_date between:fDate and :TDate "
	  + " union "+" select im.item_name ,im.standard_price, 0.0 as fromqty , dl.to_qty as toqty,mt.conversion_date  from product_conversion_mst mt , product_conversion_dtl dl ,item_mst im"  + 
	  			  		" where mt.id=dl.product_conversion_mst_id and mt.company_mst=:companymstid and im.id=dl.to_item and mt.branch_code=:branchcode and conversion_date between:fDate and :TDate ")
	  
	
		
	List<Object> getproductconversionreport(String companymstid, String branchcode, Date fDate, Date TDate);


	  @Query(nativeQuery = true, value ="select * product_conversion_mst where "
	  		+ "date(conversion_date)>=:tdate and date(conversion_date)<=:fdate")
	List<ProductConversionMst> getProductConversionMstBetweenDate(Date tdate, Date fdate);



	
	  
	 


}
