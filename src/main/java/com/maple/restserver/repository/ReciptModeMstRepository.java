package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReceiptModeMst;

@Repository
public interface ReciptModeMstRepository extends JpaRepository<ReceiptModeMst, String>{
	List<ReceiptModeMst>	findByCompanyMst(CompanyMst companymst);
	ReceiptModeMst findByCompanyMstAndReceiptMode(CompanyMst companymst ,String receiptmode);
	
	@Query(nativeQuery = true,value="SELECT * FROM receipt_mode_mst WHERE receipt_mode NOT IN('CREDIT', 'INSURANCE', 'DISCOUNT')")
	List<ReceiptModeMst> findByNewReceiptMode(CompanyMst companymst);
	
	
	@Query(nativeQuery = true,value="SELECT * FROM receipt_mode_mst WHERE receipt_mode NOT IN('INSURANCE')")
	List<ReceiptModeMst> findByNewReceiptModeExceptInsurance(CompanyMst companymst);
	
	
	List<ReceiptModeMst> findByReceiptModeAndCompanyMst(String receiptmode, CompanyMst companyMst);
	

	@Query(nativeQuery=true,value="select * from receipt_mode_mst where company_mst=:companymstid and status='ACTIVE' and LOWER(receipt_mode) like LOWER(:string) ")
	List<ReceiptModeMst> searchLikeReceiptModeByName(String string, String companymstid, Pageable topFifty);
}
