package com.maple.restserver.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.DailySalesSumary2;

public interface DailySaleSummary2Repository extends JpaRepository<DailySalesSumary2, String>{

	
	DailySalesSumary2 findByReportDateAndBranchCode(Date date,String branchCode);
}
