package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ItemPopUp;
import com.maple.restserver.report.entity.CustomerCategorywiseStockReport;



@Repository
public interface ItemStockPopUpRepository extends JpaRepository<ItemPopUp, String>{


	
//	List<ItemPopUp> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);

	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,b.mrp  , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
			+ " b.batch,i.itemCode FROM ItemMst i , UnitMst u , ItemBatchMst b "
			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND "
			+ "  lower(i.itemName) LIKE  :searchItemName")
 

	public List<Object> findSearch(  String searchItemName,  Pageable pageable);
	
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,i.standardPrice   , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
			+ " b.batch,i.itemCode,date(b.expDate),i.itemPriceLock FROM ItemMst i , UnitMst u , ItemBatchMst b "
			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND "
			+ "  lower(i.itemName) LIKE  :searchItemName AND i.companyMst=:companyMst "
			+ " and i.isDeleted='N' "
			+ "ORDER BY i.itemRank DESC")
 

	public List<Object> findSearchWithComp( String searchItemName,  Pageable pageable,CompanyMst companyMst);
	
	@Query(nativeQuery = true,value="select A.item_name,A.barcode,A.unit_name,"
			+ "A.standard_price,A.qty,A.cess,A.tax_rate,A.iid,A.id,A.batch,A.item_code,"
			+ "A.exp_date,A.item_price_lock,A.item_rank from "
			+ "(select i.item_name  , b.barcode   , "
			+ " u.unit_name   ,i.standard_price   , b.qty , "
			+ " i.cess, i.tax_rate ,"
			+ "  i.id as iid,"
			+ "  u.id ,"
			+ " b.batch,i.item_code,b.exp_date,i.item_price_lock,i.item_rank from item_mst i , unit_mst u ,"
			+ " item_batch_mst b "
			+ " where i.unit_id= u.id  and i.id = b.item_id and "
			+" i.company_mst=:companyMst "
			+ " and i.is_deleted='N' and b.qty>0 "
			+ "union "
			+ "select i.item_name  , b.barcode   ,"
			+ " u.unit_name   ,i.standard_price   , b.qty , "
			+ " i.cess, i.tax_rate ,"
			+ " i.id as iid,"
			+ "u.id ,b.batch,i.item_code,b.exp_date,i.item_price_lock,i.item_rank from item_mst i , unit_mst u ,"
			+ "item_batch_mst b where i.unit_id= u.id  and i.id = b.item_id and "
			+ "i.company_mst=:companyMst and i.is_deleted='N' and "
			+ "i.id in (select k.item_id from kit_definition_mst k)) A"
			+ " where lower(A.item_name) like  :searchItemName order by A.item_rank desc")
 

	public List<Object> findSearchWithCompWithouZero( String searchItemName,  Pageable pageable,CompanyMst companyMst);
//	+ "  lower(i.itemName) LIKE  :searchItemName AND
	
	
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,b.mrp  , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
 
			+ " b.batch , i.itemCode FROM ItemMst i , UnitMst u , ItemBatchMst b "
 

			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND "
			+ "     lower(i.itemCode) =  :searchItemName")
 

	public List<Object> findSearchByItemCode(  String searchItemName,  Pageable pageable);
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,i.standardPrice  , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
 
			+ " b.batch , i.itemCode,date(b.expDate),i.itemPriceLock FROM ItemMst i , UnitMst u , ItemBatchMst b "
 

			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND "
			+ "   (  lower(i.itemCode) =  :searchItemName OR lower(b.barcode) =:searchItemName) AND i.companyMst =:companyMst"
			+ " and i.isDeleted='N' "
			+ "ORDER BY i.itemRank DESC,b.qty DESC")
 

	public List<Object> findSearchByItemCodeWithComp(  String searchItemName,  Pageable pageable,CompanyMst companyMst);
	


	
	@Query(nativeQuery =  true,value ="select A.item_name,A.barcode,A.unit_name,"
			+ "A.standard_price,A.qty,A.cess,A.tax_rate,A.iid,A.id,A.batch,A.item_code,"
			+ "A.exp_date,A.item_price_lock,A.item_rank from ("
			+ "select i.item_name  , b.barcode   , "
			+ " u.unit_name   ,i.standard_price  , b.qty , "
			+ " i.cess, i.tax_rate ,"
			+ "  i.id as iid,"
			+ "  u.id ,"
 
			+ " b.batch , i.item_code,b.exp_date,i.item_price_lock,i.item_rank from item_mst i , unit_mst u , "
			+ "item_batch_mst b "
 

			+ " where i.unit_id= u.id  and i.id = b.item_id and "
			+ " i.company_mst =:companyMst"
			+ " and i.is_deleted='N' and b.qty>0 "
			+ "union "
			+"select i.item_name  , b.barcode   , "
					+ " u.unit_name   ,i.standard_price  , b.qty , "
					+ " i.cess, i.tax_rate ,"
					+ "  i.id as iid,"
					+ "  u.id ,"
		 
					+ " b.batch , i.item_code,b.exp_date,i.item_price_lock,i.item_rank from item_mst i , "
					+ "unit_mst u , "
					+ "item_batch_mst b "
		 

					+ " where i.unit_id= u.id  and i.id = b.item_id and  "
					+ " i.company_mst =:companyMst"
					+ " and i.is_deleted='N' and i.id in (select k.item_id from kit_definition_mst k) ) A where "
					+ " (lower(A.item_code) =  :searchItemName or lower(A.barcode) =:searchItemName) "
					+ "order by A.item_rank desc")

// 
	
	public List<Object> findSearchByItemCodeWithCompWithoutZeroStock(  String searchItemName,  Pageable pageable,CompanyMst companyMst);
	
	
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,i.standardPrice   , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
 
			+ " b.batch , i.itemCode,date(b.expDate),i.itemPriceLock FROM ItemMst i , UnitMst u , ItemBatchMst b "
 

			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND "
			+ "   (  lower(i.barCode) =:searchItemName) AND i.companyMst =:companyMst "
			+ "and i.isDeleted='N' "
			+ "ORDER BY i.itemRank DESC,b.qty DESC")
 

	public List<Object> findSearchByBarcodeWithComp(  String searchItemName,  Pageable pageable,CompanyMst companyMst);
	
	
	@Query(nativeQuery = true,value ="select A.item_name,A.barcode,A.unit_name,"	
			+ "A.standard_price,A.qty,A.cess,A.tax_rate,A.iid,A.id,A.batch,A.item_code,"
			+ "A.exp_date,A.item_price_lock,A.item_rank from ("
			+ "select i.item_name  , b.barcode   , "
			+ " u.unit_name   ,i.standard_price   , b.qty , "
			+ " i.cess, i.tax_rate ,"
			+ "  i.id iid,"
			+ "  u.id ,"
 
			+ " b.batch , i.item_code,b.exp_date,i.item_price_lock,i.item_rank from item_mst i , unit_mst u ,"
			+ " item_batch_mst b "
 

			+ " where i.unit_id= u.id  and i.id = b.item_id "
			+ "and i.is_deleted='N' and b.qty>0 "
			
			+ "union "
			+ "select i.item_name  , b.barcode   , "
			+ " u.unit_name   ,i.standard_price   , b.qty , "
			+ " i.cess, i.tax_rate ,"
			+ "  i.id iid,"
			+ "  u.id ,"
 
			+ " b.batch , i.item_code,b.exp_date,i.item_price_lock,i.item_rank from item_mst i , unit_mst u ,"
			+ " item_batch_mst b "

			+ " where i.unit_id= u.id  and i.id = b.item_id and "
			+ "  i.company_mst =:companyMst "
			+ "and i.is_deleted='N' and i.id in (select k.item_id from kit_definition_mst k)) A where "
			+ "(  lower(A.barcode) =:searchItemName) order by A.item_rank desc")
			

	public List<Object> findSearchByBarcodeWithCompWithouZeroStock(  String searchItemName,  Pageable pageable,CompanyMst companyMst);
	
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,b.mrp  , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
			+ " b.batch FROM ItemMst i , UnitMst u , ItemBatchMst b "
			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND  i.itemName=:searchItemName AND b.batch=:batch  ")
 

	public List<Object> findSearchbyItemName(  String searchItemName ,String batch, Pageable pageable);
	
	
	// ========================stock checking using store name =================
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,b.mrp  , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
			+ " b.batch,b.store FROM ItemMst i , UnitMst u , ItemBatchMst b "
			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND  i.itemName=:searchItemName AND b.batch=:batch AND b.store=:storename ")
 

	public List<Object> findSearchbyItemNameAndStore(  String searchItemName ,String batch, Pageable pageable, String storename);
	
	//===========================end stock checking =====================
	
	@Query("SELECT i.itemName,b.particulars ,"
			+ "u.unitName,b.mrp,b.qtyOut,"
			+ "b.qtyIn,"
			+ " i.id,"
			+ "u.id,"
			+ " b.batch FROM ItemMst i , UnitMst u , ItemBatchDtl b where "
			+ "i.unitId= u.id  AND i.id = b.itemId AND  i.itemName=:searchItemName AND b.particulars not like 'PHYSICAL STOCK'"
			+ " and b.particulars not like 'Physical Stock%'"
			+ " ORDER BY b.sourceVoucherDate")
	
	public List<Object> findSearchbyItemNameTop(  String searchItemName , Pageable pageable);
	
	
	@Query("SELECT i.itemName,b.particulars ,"
			+ "u.unitName,b.mrp,b.qtyOut,"
			+ "b.qtyIn,"
			+ " i.id,"
			+ "u.id, p.id,"
			+ " b.batch From ItemMst i, UnitMst u, ItemBatchDtl b,SalesTransHdr p where "
			+ "i.unitId= u.id  AND i.id = b.itemId AND  i.itemName=:searchItemName "
			+ "AND p.voucherNumber = b.sourceVoucherNumber AND (p.customerId=:custId OR b.particulars = 'Purchase%') ORDER BY b.sourceVoucherDate")
	public List<Object> findSearchbyItemNameByCustomerTop(String searchItemName, String custId,Pageable pageable);
			
	
	
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,b.mrp  , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
			+ " b.batch,b.expDate,b.store  FROM ItemMst i , UnitMst u , ItemBatchMst b "
			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND lower(i.barCode) =  :searchItemName and b.qty > 0 ")
 
	public List<Object> findSearchBarcode(  String searchItemName ,  Pageable pageable);
	
	
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,i.standardPrice  , sum(b.qtyIn-b.qtyOut) , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
			+ " b.batch,b.expDate FROM ItemMst i , UnitMst u , ItemBatchDtl b "
			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND  lower(i.barCode) =  :searchItemName "
			+ "AND lower(b.batch) = :batch GROUP BY i.itemName  , b.barcode,u.unitName   ,i.standardPrice  ,"
			+ "i.cess, i.taxRate ,i.id, u.id ,b.batch,b.expDate")
 
	public List<Object> findSearchBarcodeAndBatch(  String searchItemName ,String batch,  Pageable pageable);
	
	
	
	
//	@Query(nativeQuery=true,value="  select i.item_name "
//			+ " from item_mst i, "
//			+ "item_batch_mst ib, "
//			+ "customer_stock_category c, "
//			+ "company_mst com, "
//			+ "customer_mst cust "
//			+ "where i.id=ib.item_id "
//			+ "and i.category_id=c.category_mst "
//			+ "and com.id=i.company_mst "
//			+ "and cust.id=c.customer_mst "
//			+ "and cust.id=:customerid "
//			+ "and com.id=:companymstid "
//			+ "and lower(i.item_name) LIKE  :itemName ")
	
	@Query(nativeQuery=true,value="  select i.item_name "
			+ " from item_mst i, "
			+ "item_batch_mst ib, "
			+ "customer_stock_category c, "
			+ "company_mst com, "
			+ "account_heads cust "
			+ "where i.id=ib.item_id "
			+ "and i.category_id=c.category_mst "
			+ "and com.id=i.company_mst "
			+ "and cust.id=c.account_heads "
			+ "and cust.id=:customerid "
			+ "and com.id=:companymstid "
			+ "and lower(i.item_name) LIKE  :itemName ")
	public List<Object> findSearchItemsCustomerStockCategory(String companymstid,
			String itemName, String customerid);
	
	
	
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,i.standardPrice  , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
 
			+ " b.batch , i.itemCode,date(b.expDate),i.itemPriceLock FROM ItemMst i , UnitMst u ,"
			+ " ItemBatchMst b, CategoryMst c "
			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND c.id=i.categoryId AND "
			+ "   c.categoryName =  :catName AND i.companyMst =:companyMst"
			+ " and i.isDeleted='N' "
			+ "ORDER BY i.itemRank DESC")
 

	public List<Object> findItemStockSearchByCategoryName(String catName, CompanyMst companyMst);
	
	//----------------------new version 1.6 surya
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,i.standardPrice  , round(b.qty,2) as qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
 
			+ " b.batch , i.itemCode,date(b.expDate),i.itemPriceLock,b.store  FROM ItemMst i , UnitMst u , ItemBatchMst b  "
 

			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND  "
			+ "   (  lower(i.itemCode) =  :searchItemName OR lower(b.barcode) =:searchItemName) AND i.companyMst =:companyMst"
			+ " and i.isDeleted='N' "
			+ " and (date(b.expDate)>=:sdate OR date(b.expDate) is null  )  and b.qty > 0 "
			+ "ORDER BY i.itemRank DESC,b.qty DESC,date(b.expDate) ASC")

	public List<Object> findSearchByItemCodeWithBatch(  String searchItemName,  Pageable pageable,CompanyMst companyMst,
			java.util.Date sdate);
	
	
	
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,i.standardPrice   , round(b.qty,2) as qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
 
			+ " b.batch , i.itemCode,date(b.expDate),i.itemPriceLock,b.store FROM ItemMst i , UnitMst u , ItemBatchMst b  "
 

			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND "
			+ "   (  lower(i.barCode) =:searchItemName) AND i.companyMst =:companyMst "
			+ "and i.isDeleted='N' "
			+ " and  (date(b.expDate)>=:sdate OR date(b.expDate) is null)  and  b.qty > 0 "
			+ "ORDER BY i.itemRank DESC,b.qty DESC, date(b.expDate) ASC")
 

	public List<Object> findSearchByBarcodeWithBatch(  String searchItemName,  Pageable pageable,CompanyMst companyMst,
			java.util.Date sdate);
	
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,i.standardPrice   , round(b.qty,2) as qty  , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
			+ " b.batch,i.itemCode,date(b.expDate),i.itemPriceLock,b.store FROM ItemMst i , UnitMst u , ItemBatchMst b "
			
			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND "
			+ "  lower(i.itemName) LIKE  :searchItemName AND i.companyMst=:companyMst "
			+ " and i.isDeleted='N' "
			+ " and  (date(b.expDate)>=:sdate OR date(b.expDate) is null) and b.qty > 0 "
			+ "ORDER BY i.itemRank DESC,date(b.expDate) ASC")
 

	public List<Object> findSearchWithBatch( String searchItemName,  Pageable pageable,CompanyMst companyMst,
			java.util.Date sdate);

	
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,i.standardPrice   , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
			+ " b.batch,i.itemCode,date(b.expDate),i.itemPriceLock FROM ItemMst i , UnitMst u , ItemBatchMst b "
			
			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND "
			+ "  i.id=:itemid AND i.companyMst=:companyMst "
			+ " and i.isDeleted='N' "
			+ " and  (date(b.expDate)>=:sdate OR date(b.expDate) is null) "
			+ "ORDER BY i.itemRank DESC,date(b.expDate) ASC")
 
	public List<Object> findSearchWithBatchAndItemId(String itemid, Pageable topFifty, CompanyMst companyMst,
			java.util.Date sdate);
	//----------------------new version 1.6 surya end
	
	
	//------------------------------sharon ------------storeitemPopUp------------------
	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,b.mrp  , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
			+ " b.batch,b.expDate,b.store  FROM ItemMst i , UnitMst u , ItemBatchMst b "
			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND lower(i.barCode) =  :searchItemName and b.qty > 0 ")
 
	public List<Object> findSearchStoreBarcode(  String searchItemName ,  Pageable pageable);

	
	

	
	@Query(nativeQuery=true,value="SELECT i.item_name  , b.barcode   , "
			+ " u.unit_name   ,i.standard_price   , cast((b.qty) as  decimal(10,2)) , "
			+ " i.cess, i.tax_rate ,"
			+ "  i.id as item_id,"
			+ "  u.id as unit_id,"
			+ " b.batch,i.item_code,date(b.exp_date),i.item_price_lock,b.store FROM item_mst i , unit_mst u , item_batch_mst b "
			
			+ " WHERE i.unit_id= u.id  AND i.id = b.item_id AND "
			+ "  (  lower(i.item_code) =  :searchItemName OR lower(b.barcode) =:searchItemName) AND i.company_mst=:companyMst "
			+ " and i.is_deleted='N' "
			+ " and  (date(b.exp_date)>=:sdate OR date(b.exp_date) is null) and b.qty > 0 "
			+ "ORDER BY i.item_rank DESC,b.qty DESC,date(b.exp_date) ASC")
	public List<Object> findSearchByItemCodeWithBatchDerby(String searchItemName, Pageable topFifty, CompanyMst companyMst,
			java.util.Date sdate);

	
	
	
	@Query(nativeQuery=true,value="SELECT i.item_name  , b.barcode   , "
			+ " u.unit_name   ,i.standard_price   , cast((b.qty) as  decimal(10,2)) , "
			+ " i.cess, i.tax_rate ,"
			+ "  i.id as item_id,"
			+ "  u.id as unit_id,"
			+ " b.batch,i.item_code,date(b.exp_date),i.item_price_lock,b.store FROM item_mst i , unit_mst u , item_batch_mst b "
			
			+ " WHERE i.unit_id= u.id  AND i.id = b.item_id AND "
			+ "  lower(i.bar_code) =  :searchItemName AND i.company_mst=:companyMst "
			+ " and i.is_deleted='N' "
			+ " and  (date(b.exp_date)>=:sdate OR date(b.exp_date) is null) and b.qty > 0 "
			+ "ORDER BY i.item_rank DESC,b.qty DESC,date(b.exp_date) ASC")
	
	public List<Object> findSearchByBarcodeWithBatchDerby(String searchItemName, Pageable topFifty, CompanyMst companyMst,
			java.util.Date sdate);

	

	@Query(nativeQuery=true,value="SELECT i.item_name  , b.barcode   , "
			+ " u.unit_name   ,i.standard_price   , cast((b.qty) as  decimal(10,2)) , "
			+ " i.cess, i.tax_rate ,"
			+ "  i.id as item_id,"
			+ "  u.id as unit_id,"
			+ " b.batch,i.item_code,date(b.exp_date),i.item_price_lock,b.store FROM item_mst i , unit_mst u , item_batch_mst b "
			
			+ " WHERE i.unit_id= u.id  AND i.id = b.item_id AND "
			+ "  lower(i.item_name) LIKE  :searchItemName AND i.company_mst=:companyMst "
			+ " and i.is_deleted='N' "
			+ " and  (date(b.exp_date)>=:sdate OR date(b.exp_date) is null) and b.qty > 0 "
			+ "ORDER BY i.item_rank DESC,date(b.exp_date) ASC")
	public List<Object> findSearchWithBatchDerby(String searchItemName, Pageable topFifty, CompanyMst companyMst,
			java.util.Date sdate);

	
	

}
/*

SELECT i.item_Name  , b.barcode   , "
		+ " u.unit_Name   ,i.standard_Price  , b.qty , "
		+ " i.cess, i.tax_Rate ,"
		+ "  i.id,"
		+ "  u.id ,"
		+ " b.batch FROM Item_Mst i , Unit_Mst u , Item_Batch_Mst b "
		+ " WHERE i.unitId= u.id  AND i.id = b.item_Id  
*/






