package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SalesReturnHdr;

@Repository
public interface SalesReturnHdrRepository extends JpaRepository<SalesReturnHdr, String>{

	SalesReturnHdr	findByVoucherNumberAndVoucherDateAndCompanyMstId(String vouchernumber,Date date,String companymstid);


	SalesReturnHdr findByIdAndCompanyMstId(String salesreturnhdrid,String companymstid);


	List<SalesReturnHdr> findByVoucherNumberAndVoucherDate(String voucherNumber, Date voucherDate);

@Query(nativeQuery = true,value="select i.item_name,"
		+ "d.batch,"
		+ "d.expiry_date,d.qty,d.rate,h.invoice_amount from "
		+ " sales_return_hdr h,"
		+ "sales_return_dtl d,"
		+ "item_mst i,category_mst cm  "
		+ "where h.id=d.sales_return_hdr_id "
		+ "and d.item_id=i.id "
		+ "and cm.id=i.item_id "
		+ "and h.company_mst=:companymstid "
		+ "and h.barnch_code=:branchcode "
		+ "and date(h.voucher_date) between :fDate and :TDate and i.category_id=:groupId ")
List<Object>batchwiseSalesReturnreport(String  companymstid, String  branchcode,Date fDate,Date TDate,String groupId);



@Query(nativeQuery = true,
value="  select bm.branch_name, \n" + 
	  		"bm.branch_address1, \n" + 
	  		"bm.branch_address2,\n" + 
	  		"bm.branch_Place, \n" + 
	  		"bm.branch_Tel_No, \n" + 
	  		"bm.branch_Gst , \n" + 
	  		"bm.branch_state,\n" + 
	  		"bm.branch_Email,\n" + 
	  		"bm.bank_name, \n" + 
	  		"bm.account_number, \n" + 
	  		"bm.bank_branch, \n" + 
	  		"bm.ifsc, \n" + 
	  		"sh.voucher_date,\n" + 
	  		"sh.voucher_number, \n" + 
	  		"cm.account_name,\n" + 
	  		"cm.customer_place,\n" + 
	  		"cm.customer_state,\n" + 
	  		"cm.party_gst,\n" + 
	  		"im.item_name, \n" + 
	  		"d.tax_rate, \n" + 
	  		"im.hsn_code, \n" + 
	  		"d.rate, d.qty,\n" + 
	  		"u.unit_name, \n" + 
	  		" sh.invoice_amount    ,  " + 
	  		" "
	  		+ "   d.sgst_amount    , \n" + 
	  		"     d.cgst_amount     ,  \n" + 
	  		"       d.igst_amount   ,  \n" + 
	  		"   sgst_tax_rate , \n" + 
	  		"   cgst_tax_rate   , "  + 
	  		"   igst_tax_rate, \n " + 
	  		"   d.cess_rate, \n " + 
	  		"   d.cess_amount, \n " + 
	  		
	  		" bm.branch_website,"
	  		+ " cm.customer_contact, "
	  		+ " rate * qty , "

	  		+ " sh.sales_mode, d.mrp "
	  		+ "  from branch_mst bm, sales_return_hdr sh, sales_return_dtl d, account_heads cm, unit_mst u,\n" + 
	  		"item_mst im  where sh.branch_code = bm.branch_code "
	  		+ " and sh.customer_id = cm.id and sh.id = d.sales_return_hdr_id "
	  		+ " and d.item_id = im.id "
	  		+ "and d.unit_id = u.id  "
	  		+ " and sh.voucher_number = :vnumber  "
	  		+ " and date(sh.voucher_date) = :vdate "
	  		+ " and sh.company_mst = :company_mst_id \n" 
)

List<Object> salesrReturnInvoice(String company_mst_id,String vnumber,Date vdate);

@Query(nativeQuery = true,value="select ( sum(d.qty*d.rate) ) as taxableAmount  from sales_return_hdr h,sales_return_dtl d where h.id=d.sales_return_hdr_id and h.voucher_number=:voucherNumber and date(h.voucher_date) =:date and h.company_mst=:companymstid ")
Double getSalesReturnTaxableAmount(String voucherNumber,Date date,String companymstid);


@Query(nativeQuery = true,value="select d.tax_rate , "
		+ "  (sum(d.rate*d.qty*d.tax_rate/100)   ),  "
		+ "   (SUM(d.sgst_amount)   )  as sgst_amount , \n" + 
		"     (SUM( d.cgst_amount )   )  as cgst_amount ,  \n" + 
		"   (SUM( d.igst_amount )  )  as igst_amount ,  \n" + 
		"   sgst_tax_rate , \n" + 
		"   cgst_tax_rate \n , "  + 
		"   igst_tax_rate \n ,"
		+ " sum(d.rate * d. qty) " 
		+ "from sales_return_dtl d,sales_return_hdr h"
	  		+ " where d.sales_return_hdr_id=h.id and h.voucher_number=:vouchernumber "
	  		+ "and date(h.voucher_date) =:date and h.company_mst=:companymstid"
	  		+ " group by d.tax_rate , "
	  		+ "   sgst_tax_rate , \n" + 
	  		"   cgst_tax_rate \n , "  + 
	  		"   igst_tax_rate \n "
	  		+ "   ")
List<Object>getTaxSummary(String companymstid, String vouchernumber,Date date); 


@Query(nativeQuery = true,value="select (SUM(d.sgst_amount))  as sgst_amount ,(SUM( d.cgst_amount ))  as cgst_amount , (SUM( d.igst_amount ))  as igst_amount from sales_return_hdr h,sales_return_dtl d where h.id=d.sales_return_hdr_id and h.voucher_number=:vouchernumber and date(h.voucher_date) =:date and h.company_mst=:companymstid ")
List<Object>getSumOfTaxAmounts(String companymstid, String vouchernumber,Date date);




@Query(nativeQuery = true,value="select cm.localcustomer_name,"
		+ "cm.address,"
		+ "cm.phone_no,"
		+ "cm.address_line1,cm.address_line2,cm.land_mark "
		+ "from local_customer_mst cm,sales_trans_hdr h "
		+ "where h.local_customer_mst=cm.id and date(h.voucher_date ) =:vdate "
		+ "and h.voucher_number=:vnumber and h.company_mst=:company_mst_id ")

List<Object>	getTaxInvoiceCustomer(String company_mst_id , String vnumber,Date vdate);



//@Query(nativeQuery = true,value="select cm.customer_name,cm.customer_address,cm.customer_state,cm.customer_contact,cm.customer_gst from customer_mst cm,sales_return_hdr h where h.customer_id=cm.id and date(h.voucher_date) =:vdate and h.voucher_number=:vnumber and h.company_mst=:company_mst_id ")
 
@Query(nativeQuery = true,value="select cm.account_name,cm.party_address1,cm.customer_state,cm.customer_contact,cm.party_gst "
		+ "from account_heads cm,sales_return_hdr h where h.customer_id=cm.id and date(h.voucher_date) =:vdate and h.voucher_number=:vnumber "
		+ "and h.company_mst=:company_mst_id ")
List<Object> getTaxInvoiceGstCustomer(String company_mst_id , String vnumber,Date vdate);


@Query(nativeQuery = true,value="select sum(d.cess_amount) as cessamount from sales_return_hdr h,sales_return_dtl d where h.id=d.sales_return_hdr_id and h.voucher_number=:vouchernumber and date(h.voucher_date) =:date ")
Double getCessAmount(String vouchernumber, Date date);

@Query(nativeQuery = true,value="select sum(d.qty*d.rate) from sales_return_hdr h,sales_return_dtl d where h.id=d.sales_return_hdr_id and d.tax_rate>0 and h.voucher_number=:vouchernumber and date( h.voucher_date) =:date")
Double getNonTaxableAmount(String vouchernumber,Date date);


@Modifying
@Query(nativeQuery = true,value="update sales_return_hdr h  set h.customer_mst =:toAccountId,h.customer_id=:toAccountId  where h.customer_mst =:fromAccountId ")
void accountMerge(String fromAccountId, String toAccountId); 


//======================Pharmacy Sales return report with category========================
//@Query(nativeQuery = true,value="select h.voucher_date,h.sales_voucher_number,h.voucher_number,c.customer_name,"
//		+ "i.item_name,i.item_code,cat.category_name,d.batch,ibex.expiry_date,d.qty,d.rate,d.qty*d.rate,"
//		+ "usr.user_name,s.sales_man_name,d.igst_amount,d.rate+d.igst_amount "
//		+ "from sales_return_hdr h,sales_return_dtl d , item_mst i,customer_mst c,category_mst cat,item_batch_expiry_dtl ibex,"
//		+ "sales_man_mst s,user_mst usr "
//		+ "where h.id=d.sales_return_hdr_id and h.branch_code=:branchCode and "
//		+ "c.id = h.customer_mst and cat.id = i.category_id and usr.id = h.user_id and s.id = h.sales_man_id and "
//		+ "d.batch = ibex.batch and d.item_id = ibex.item_id and "
//		+ "i.category_id=:groupId and d.item_id=i.id "
//		+ " and date(h.voucher_date )>=:fromDate and date(h.voucher_date )<=:toDate ")

@Query(nativeQuery = true,value="select h.voucher_date,h.sales_voucher_number,h.voucher_number,c.account_name,"
		+ "i.item_name,i.item_code,cat.category_name,d.batch,ibex.expiry_date,d.qty,d.rate,d.qty*d.rate,"
		+ "usr.user_name,s.sales_man_name,d.igst_amount,d.rate+d.igst_amount "
		+ "from sales_return_hdr h,sales_return_dtl d , item_mst i,account_heads c,category_mst cat,item_batch_expiry_dtl ibex,"
		+ "sales_man_mst s,user_mst usr "
		+ "where h.id=d.sales_return_hdr_id and h.branch_code=:branchCode and "
		+ "c.id = h.account_heads and cat.id = i.category_id and usr.id = h.user_id and s.id = h.sales_man_id and "
		+ "d.batch = ibex.batch and d.item_id = ibex.item_id and "
		+ "i.category_id=:groupId and d.item_id=i.id "
		+ " and date(h.voucher_date )>=:fromDate and date(h.voucher_date )<=:toDate ")
List<Object>getPharmacySalesReturn(Date fromDate,Date toDate,String branchCode, String groupId);



//======================Pharmacy Sales return report without category========================
//@Query(nativeQuery = true,value="select h.voucher_date,h.sales_voucher_number,h.voucher_number,c.customer_name,"
//		+ "i.item_name,i.item_code,cat.category_name,d.batch,ibex.expiry_date,d.qty,d.rate,d.qty*d.rate,"
//		+ "usr.user_name,s.sales_man_name,d.igst_amount,d.rate+d.igst_amount "
//		+ "from sales_return_hdr h,sales_return_dtl d , item_mst i,customer_mst c,category_mst cat,item_batch_expiry_dtl ibex,"
//		+ "sales_man_mst s,user_mst usr "
//		+ "where h.id=d.sales_return_hdr_id and h.branch_code=:branchCode and "
//		+ "c.id = h.customer_mst and cat.id = i.category_id and usr.id = h.user_id and s.id = h.sales_man_id and "
//		+ "d.batch = ibex.batch and d.item_id = ibex.item_id and "
//		+ " d.item_id=i.id "
//		+ " and date(h.voucher_date )>=:fromDate and date(h.voucher_date )<=:toDate ")


@Query(nativeQuery = true,value="select h.voucher_date,h.sales_voucher_number,h.voucher_number,c.account_name,"
		+ "i.item_name,i.item_code,cat.category_name,d.batch,ibex.expiry_date,d.qty,d.rate,d.qty*d.rate,"
		+ "usr.user_name,s.sales_man_name,d.igst_amount,d.rate+d.igst_amount "
		+ "from sales_return_hdr h,sales_return_dtl d , item_mst i,account_heads c,category_mst cat,item_batch_expiry_dtl ibex,"
		+ "sales_man_mst s,user_mst usr "
		+ "where h.id=d.sales_return_hdr_id and h.branch_code=:branchCode and "
		+ "c.id = h.account_heads and cat.id = i.category_id and usr.id = h.user_id and s.id = h.sales_man_id and "
		+ "d.batch = ibex.batch and d.item_id = ibex.item_id and "
		+ " d.item_id=i.id "
		+ " and date(h.voucher_date )>=:fromDate and date(h.voucher_date )<=:toDate ")
List<Object>getPharmacySalesReturnWithoutCategory(Date fromDate,Date toDate,String branchCode);

}

