package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CashBook;
@Component
@Repository
public interface CashBookRepository extends JpaRepository<CashBook, Integer>{

	List<CashBook>findByCompanyMstId(String companymstid);
//	List<CashBook> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
