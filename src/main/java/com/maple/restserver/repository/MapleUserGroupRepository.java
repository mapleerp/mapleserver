package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.MapleUserGroup;

@Component
@Repository
public interface MapleUserGroupRepository extends JpaRepository <MapleUserGroup,String>{

	List<MapleUserGroup> findByUserIdAndCompanyMst(String companymstid, String id);

}
