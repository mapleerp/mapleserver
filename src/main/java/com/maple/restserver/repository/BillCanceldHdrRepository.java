package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.BillCanceldHdr;

public interface BillCanceldHdrRepository  extends JpaRepository<BillCanceldHdr, String>{

	
	
}
