package com.maple.restserver.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;

@Repository
public class ItemSearchRepositoryCustomImpl implements ItemSearchRepositoryCustom {

	@PersistenceContext
	EntityManager em;

	// constructor

	@Override
	public List<ItemMst> findItemBySearch(String searchString , Pageable pageable) {

		searchString = searchString.replaceAll("%", "");
		/*
		 * CriteriaBuilder cb = em.getCriteriaBuilder(); CriteriaQuery<ItemMst> cq =
		 * cb.createQuery(ItemMst.class);
		 * 
		 * Root<ItemMst> itemMst = cq.from(ItemMst.class); List<Predicate> predicates =
		 * new ArrayList<>();
		 * 
		 * 
		 * if (searchString != null) { predicates.add(cb.like(itemMst.get("itemName"),
		 * searchString) );
		 * 
		 * 
		 * } cq.where(predicates.toArray(new Predicate[0]));
		 * 
		 * return em.createQuery(cq) .setFirstResult(0) // offset .setMaxResults(50) //
		 * limit .getResultList();
		 * 
		 * 
		 * }
		 */

		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<ItemMst> query = builder.createQuery(ItemMst.class);
		EntityType<ItemMst> type = em.getMetamodel().entity(ItemMst.class);
		Root<ItemMst> root = query.from(ItemMst.class);

		query.where(builder.or(
				builder.like(builder.lower(root.get(type.getDeclaredSingularAttribute("itemName", String.class))),
						"%" + searchString.toLowerCase() + "%"),
				builder.like(builder.lower(root.get(type.getDeclaredSingularAttribute("itemCode", String.class))),
						"%" + searchString.toLowerCase() + "%"),
				builder.like(builder.lower(root.get(type.getDeclaredSingularAttribute("barCode", String.class))),
						"%" + searchString.toLowerCase() + "%")));

		query.orderBy(builder.asc(root.get("itemName")));

		return em.createQuery(query).setFirstResult(0) // offset
				.setMaxResults(50) // limit
				.getResultList();
	}

	@Override
	public List<ItemMst> findItemByBarcodeSearch(String searchString, Pageable pageable) {

		//searchString = searchString.replaceAll("%", "");
		/*
		 * CriteriaBuilder cb = em.getCriteriaBuilder(); CriteriaQuery<ItemMst> cq =
		 * cb.createQuery(ItemMst.class);
		 * 
		 * Root<ItemMst> itemMst = cq.from(ItemMst.class); List<Predicate> predicates =
		 * new ArrayList<>();
		 * 
		 * 
		 * if (searchString != null) { predicates.add(cb.like(itemMst.get("itemName"),
		 * searchString) );
		 * 
		 * 
		 * } cq.where(predicates.toArray(new Predicate[0]));
		 * 
		 * return em.createQuery(cq) .setFirstResult(0) // offset .setMaxResults(50) //
		 * limit .getResultList();
		 * 
		 * 
		 * }
		 */

		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<ItemMst> query = builder.createQuery(ItemMst.class);
		EntityType<ItemMst> type = em.getMetamodel().entity(ItemMst.class);
		Root<ItemMst> root = query.from(ItemMst.class);

		query.where(builder.and(
			 
				builder.equal(builder.lower(root.get(type.getDeclaredSingularAttribute("barCode", String.class))),
						  searchString.toLowerCase()  )));

		query.orderBy(builder.asc(root.get("itemName")));

		return em.createQuery(query).setFirstResult(0) // offset
				.setMaxResults(50) // limit
				.getResultList();
	}

	
}
