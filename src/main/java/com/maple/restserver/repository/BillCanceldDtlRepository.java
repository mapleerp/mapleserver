package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.maple.restserver.entity.BillCanceldDtl;

@Repository
public interface BillCanceldDtlRepository extends JpaRepository<BillCanceldDtl, String>{

}
