package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DebitNote;


@Component
@Repository

public interface DebitNoteRepository extends JpaRepository<DebitNote, String> {

	List<DebitNote> findByCompanyMst(CompanyMst companyMst);
	
	

	
	@Query(nativeQuery =  true,value="select u.user_name,d.debit_account,d.credit_account,d.amount,d.voucher_number,d.remark,d.voucher_date ,d.branch ,d.id from  debit_note d, user_mst u where d.user_id=u.id  and d.company_mst=:companyMst")
	List<Object> findAllDebitNote (CompanyMst  companyMst);

}
