package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AdditionalExpense;
import com.maple.restserver.entity.ItemMst;
@Component
@Repository
public interface AdditionalExpenseRepository extends JpaRepository<AdditionalExpense, String>{

	Optional<AdditionalExpense> findById(String id);
	
	List<AdditionalExpense>findByCompanyMst(String companymstid);
//	List<AdditionalExpense> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);

	@Query(nativeQuery = true,value = "select sum(amount) from additional_expense where purchase_hdr=:hdrid ")
	Double findSumOfAmountByPurchaseHdrId(String hdrid);
	
	@Query(nativeQuery = true,value = "select sum(fc_amount) from additional_expense where purchase_hdr=:hdrid ")

	Double findSumOfFCAmountByPurchaseHdrId(String hdrid);

	List<AdditionalExpense> findByPurchaseHdrId(String purchasehdrid);

	@Query(nativeQuery = true,value = "select sum(amount) from additional_expense where purchase_hdr=:hdrid"
			+ " and expense_head=:expensetype")
	Double findSumOfAmountBYExpenseTyepe(String hdrid, String expensetype);

	
	@Query(nativeQuery = true,value = "select sum(amount) from additional_expense where purchase_hdr=:hdrid"
			+ " and calculated_status='NO'")
	Double findSumOfAmountBYStatus(String hdrid);
	@Modifying
	@Query(nativeQuery = true, value = "update additional_expense set calculated_status ='YES' where "
			+ "purchase_hdr=:puchasehdrid")
	void updateAdditionalExpenseStatus(String puchasehdrid);
	
	
	
	@Query(nativeQuery = true,value = "select sum(fc_amount) from additional_expense where purchase_hdr=:hdrid"
			+ " and expense_head=:expensetype")
	Double getSumOfFCAmountByExpenseType(String hdrid, String expensetype);
	
	@Query(nativeQuery = true,value = "select sum(amount) from additional_expense where purchase_hdr=:hdrid"
			+ " and expense_head=:expensetype and currency_id=:currencyid")
	Double findSumOfAmountBYExpenseTyepeByCurrency(String hdrid, String expensetype,String currencyid);
}
