package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PriceDefenitionMst;
@Repository
public interface PriceDefinitionMstRepository extends JpaRepository<PriceDefenitionMst, String>{
	List<PriceDefenitionMst>findByPriceLevelName(String name);
	List<PriceDefenitionMst>findByPriceCalculator(String name);
	Optional<PriceDefenitionMst> findByPriceLevelNameAndCompanyMst(String pricetype, CompanyMst companyMst);
	PriceDefenitionMst findByCompanyMstAndPriceLevelName(CompanyMst companyMst, String pricename);
	
	 
}
