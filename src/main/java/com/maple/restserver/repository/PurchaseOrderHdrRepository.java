package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseOrderHdr;
@Repository
public interface PurchaseOrderHdrRepository extends JpaRepository<PurchaseOrderHdr, String> {

	
	PurchaseOrderHdr findByVoucherNumber(String voucherNumber);
	
	//version1.7
	List<PurchaseOrderHdr> findByFinalSavedStatusAndSupplierId(String status,String supid);

	@Query(nativeQuery=true,value="select ph.* from purchase_order_hdr ph,purchase_order_dtl pd where "
			+ "pd.purchase_order_hdr_id=ph.id and pd.id=:id")
	PurchaseOrderHdr findByDtlid(String id);
	// version1.7 ends
	
	//version 1.4
	List<PurchaseOrderHdr> findByVoucherDateAndFinalSavedStatus(Date voucherDate,String status);
	
	//end 1.4
	
	
	@Query(nativeQuery = true,value="select s.account_name,po.voucher_number,s.party_gst,s.id from account_heads s,"
			+ "purchase_order_hdr po where  po.supplier_id=s.id AND"
			+ " po.voucher_number is NOT NULL AND po.final_saved_status = 'OPEN'")
	public List<Object> findSearch();

	@Query(nativeQuery = true,value="select s.account_name,po.voucher_number,s.party_gst,s.id from account_heads s,"
			+ "purchase_order_hdr po where lower(s.id) LIKE  :searchItemName and po.supplier_id=s.id AND"
			+ " po.voucher_number is NOT NULL AND po.final_saved_status = 'OPEN'")
	public List<Object> findSearch(@Param("searchItemName") String searchItemName);
	
//	  @Query(nativeQuery = true,value="select "
//	  		+ " s.supplier_name,"
//	  		+ "h.voucher_number,"
//	  		
//	  		+ "i.item_name,"
//	  		+ "d.qty,"
//	  		+ "d.purchse_rate,"
//			+ "d.tax_rate, "
//	  		+ "h.voucher_date ,h.supplier_inv_no , u.unit_name ,sum(d.amount)+sum(tax_amt) as amount,s.supgst"
//	  	
//	  		+ " from purchase_order_hdr h, "
//	  		+ "supplier s, "
//	  		+ "item_mst i, unit_mst u,"
//	  		+ " purchase_order_dtl d where s.id=h.supplier_id "
//	  		+ "and i.id=d.item_id and h.id=d.purchase_order_hdr_id "
//	  		+ "and h.voucher_number=:vouchernumber and date(h.voucher_date)=:date"
//	  		+ " and u.id=d.unit_id and h.branch_code =:branchcode "
//	  		+ "group by s.supplier_name,h.voucher_number,"
//	  		+ " i.item_name,d.qty,d.purchse_rate,d.tax_rate,h.voucher_date ,h.supplier_inv_no , u.unit_name,s.supgst "
//	  		)
	
	 @Query(nativeQuery = true,value="select "
		  		+ " s.account_name,"
		  		+ "h.voucher_number,"
		  		
		  		+ "i.item_name,"
		  		+ "d.qty,"
		  		+ "d.purchse_rate,"
				+ "d.tax_rate, "
		  		+ "h.voucher_date ,h.supplier_inv_no , u.unit_name ,sum(d.amount)+sum(tax_amt) as amount,s. party_gst "
		  	
		  		+ " from purchase_order_hdr h, "
		  		+ "account_heads s, "
		  		+ "item_mst i, unit_mst u,"
		  		+ " purchase_order_dtl d where s.id=h.supplier_id "
		  		+ "and i.id=d.item_id and h.id=d.purchase_order_hdr_id "
		  		+ "and h.voucher_number=:vouchernumber and date(h.voucher_date)=:date"
		  		+ " and u.id=d.unit_id and h.branch_code =:branchcode "
		  		+ "group by s.account_name,h.voucher_number,"
		  		+ " i.item_name,d.qty,d.purchse_rate,d.tax_rate,h.voucher_date ,h.supplier_inv_no , u.unit_name,s. party_gst  "
		  		)
	  	
	List<Object> retrieveReportPurchaseOrder( String vouchernumber,String  branchcode,Date  date);  




	  @Query(nativeQuery = true,value="select sum(d.amount)+sum(d.tax_amt) as totalamount from purchase_order_hdr h, purchase_order_dtl d where h.id=d.purchase_order_hdr_id and h.voucher_number=:vouchernumber and h.branch_code=:branchcode and date(h.voucher_date)=:date")
Double retrieveTotalAmount(String vouchernumber, String branchcode, Date date);

	  
		//---------------------------version 1.7	

		List<PurchaseOrderHdr> findByCompanyMst(CompanyMst companyMst);

		List<PurchaseOrderHdr> findBySupplierIdAndCompanyMst(String supplierid, CompanyMst companyMst);
		
		//---------------------------end version 1.7	


@Modifying
@Query(nativeQuery = true,value="update purchase_order_hdr  set supplier_id =:toAccountId where supplier_id =:fromAccountId ")
void accountMerge(String fromAccountId, String toAccountId);

//PurchaseOrderHdr findByHdrId(String poId); 
Optional<PurchaseOrderHdr> findById(String poId); 

}
