package com.maple.restserver.repository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.OrderTakerMst;
import com.maple.restserver.report.entity.CategoryWiseStockMovement;

@Component
@Repository
public interface ItemBatchDtlRepository extends JpaRepository<ItemBatchDtl, String> {

	List<ItemBatchDtl> findByItemIdAndBatchAndCompanyMst(String itemId, String batch, CompanyMst companyMst);

	void deleteBySourceVoucherNumber(String souveVoucer);

//	ItemBatchDtl  findByItemId(String itemId);
	void deleteBySourceParentId(String hdrid);

	@Query(nativeQuery = true, value = "select sum(qty_in - qty_out) as qty,batch from item_batch_dtl "
			+ "where item_id =:itemId and store =:store " + "group by batch")
	List<Object> findByItemIdOrderByExpDateByStore(String itemId, String store);

	@Query(nativeQuery = true, value = "select * from item_batch_dtl " + "where date(voucher_date) >= :fudate and "
			+ "date(voucher_date) <= :tudate")
	List<ItemBatchDtl> getItemBatchDtlBtwnDate(java.util.Date fudate, java.util.Date tudate,Pageable pageable);

	
	
 /*
  * Below query was modified in may 7th by Regy while moving Sales From Order to back end Service
  */
	
	@Query(nativeQuery = true, value = " select sum(qty_in) - sum(qty_out) as qty , batch, exp_date , item_id from item_batch_dtl where item_id = :itemId  "
			+ " group by batch,exp_date having qty > 0 order by exp_date asc ")
	List<Object> findByItemIdOrderByExpDate(String itemId);
	
	
	
	

	@Query(nativeQuery = true, value = ""
			+ "	select * from item_batch_dtl where source_voucher_number =:sourceVoucher AND "
			+ "	item_id not in (select item_id from sales_dtl where sales_trans_hdr_id = (select id from "
			+ "	sales_trans_hdr where voucher_number =:sourceVoucher) )")
	List<ItemBatchDtl> getItemNotInSalesDtl(String sourceVoucher);

	@Modifying(flushAutomatically = true)
	@Query(nativeQuery = true, value = " update item_batch_dtl set barcode =:barCode  where item_id=:id")
	void udateBarcode(String id, String barCode);

	@Query(nativeQuery = true, value = "select count(*) from item_batch_dtl "
			+ "where branch_code = :branchCode and company_mst=:companymstid and " + "date(voucher_date) = :vDate")
	int ItemBatchDtlCount(String branchCode, java.sql.Date vDate, String companymstid);

	@Query(nativeQuery = true, value = "    " + " select    sum(qty_in) - sum(qty_out) from item_batch_dtl "
			+ " where item_id  = :ItemId ")

	Double getItemStock(String ItemId);

	@Query(nativeQuery = true, value = "select c.category_name,sum(i.qty_in),sum(i.qty_out),i.voucher_date from category_mst c,item_batch_dtl "
			+ " i,item_mst im where im.id=i.item_id and im.category_id = c.id and i.branch_code =:branchcode "
			+ " and i.company_mst=:companymstid and date(i.voucher_date) between :fdate and :tdate GROUP BY c.category_name,date(i.voucher_date)")
	List<Object> getCategoryStockMovement(String branchcode, String companymstid, Date fdate, Date tdate);
	
	/*
	 * Code for Derby db to get the category wise stock report
	 */
	@Query(nativeQuery = true, value = "select c.category_name,sum(i.qty_in),sum(i.qty_out),i.voucher_date from category_mst c,item_batch_dtl "
			+ " i,item_mst im where im.id=i.item_id and im.category_id = c.id and i.branch_code =:branchcode "
			+ " and i.company_mst=:companymstid and date(i.voucher_date) between :fdate and :tdate GROUP BY c.category_name,i.voucher_date")
	List<Object> getCategoryStockMovementDerby(String branchcode, String companymstid, Date fdate, Date tdate);

	List<ItemBatchDtl> findByVoucherDate(Date vDate);

	@Query(nativeQuery = true, value = "select c.category_name,sum(d.qty_in - d.qty_out) as openingStock from "
			+ " item_batch_dtl d ,item_mst i, category_mst c  where d.item_id=i.id and c.id=i.category_id "
			+ " and date(d.voucher_date)<:fdate "
			+ "and d.branch_code=:branchcode and d.company_mst=:companymstid group by c.category_name")
	List<Object> CategoryWiseOPeningStock(String branchcode, String companymstid, Date fdate);

	@Query(nativeQuery = true, value = "select c.category_name, sum(d.qty_in - d.qty_out) as closingStock from "
			+ " item_batch_dtl d ,item_mst i, category_mst c  where d.item_id=i.id and c.id=i.category_id "
			+ " and date(d.voucher_date) >=:fdate and date(d.voucher_date)<=:tdate "
			+ "and d.branch_code=:branchcode and d.company_mst=:companymstid group by c.category_name ")
	List<Object> CategoryWiseClosingStock(String branchcode, String companymstid, Date fdate, Date tdate);

	@Query(nativeQuery = true, value = "select sum(d.qty_in - d.qty_out) as closingStock from "
			+ " item_batch_dtl d where " + " date(d.voucher_date) >=:fdate and date(d.voucher_date)<=:tdate "
			+ "and d.branch_code=:branchcode and d.company_mst=:companymstid ")
	List<Object> ClosingStockforPandL(String branchcode, String companymstid, Date fdate, Date tdate);

	@Query(nativeQuery = true, value = "select im.item_name,sum(i.qty_in),sum(i.qty_out),i.voucher_date from category_mst c,item_batch_dtl"
			+ " i,item_mst im where im.id=i.item_id and im.category_id = c.id and i.branch_code =:branchcode and im.category_id=:categoryid "
			+ "and i.company_mst=:companymstid and date(i.voucher_date) between :fdate and :tdate GROUP BY im.item_name,i.voucher_date")
	List<Object> getItemStockMovement(String branchcode, String companymstid, Date fdate, Date tdate,
			String categoryid);

	@Query(nativeQuery = true, value = "select  i.item_name,sum(d.qty_in - d.qty_out) as openingStock from "
			+ " item_batch_dtl d ,item_mst i, category_mst c  where d.item_id=i.id and c.id=i.category_id and i.category_id=:categoryid"
			+ " and date(d.voucher_date)<:fdate "
			+ "and d.branch_code=:branchcode and d.company_mst=:companymstid group by i.item_name ")
	List<Object> ItemWiseOPeningStockReport(String branchcode, String companymstid, Date fdate, String categoryid);

	@Query(nativeQuery = true, value = "select i.item_name, sum(d.qty_in - d.qty_out) as closingStock from "
			+ " item_batch_dtl d ,item_mst i, category_mst c  where d.item_id=i.id and c.id=i.category_id and i.category_id=:categoryid"
			+ " and date(d.voucher_date) >=:fdate and date(d.voucher_date)<=:tdate "
			+ "and d.branch_code=:branchcode and d.company_mst=:companymstid group by i.item_name ")
	List<Object> ItemWiseClosingStockReport(String branchcode, String companymstid, Date fdate, Date tdate,
			String categoryid);

	@Modifying
	@Query(nativeQuery = true, value = "update item_batch_dtl set exp_date= null where batch ='NOBATCH'")
	void deleteExpDateItemBatchDtl();

	ItemBatchDtl findBySourceDtlId(String id);

	List<ItemBatchDtl> findBySourceParentId(String id);

	@Modifying
	@Query(nativeQuery = true, value = "delete from item_batch_dtl where source_dtl_id=:sourceDtlId")
	void deleteItemBatchDtl(String sourceDtlId);

	@Modifying
	@Query(nativeQuery = true, value = "update item_batch_dtl set exp_date=:exDate")
	void updateExpdate(java.util.Date exDate);

	@Modifying
	@Query(nativeQuery = true, value = "update item_batch_dtl set barcode =:barCode where item_id=:itemid")
	void updateBarcode(String itemid, String barCode);

//List<ItemBatchDtl> findByItemIdAndBatchAndOrderByVoucherDate

	@Query(nativeQuery = true, value = "select sum(d.qty_in-d.qty_out) , d.item_id, d.batch, d.barcode, d.mrp "
			+ "  from item_batch_dtl d " + " where  d.branch_code=:branchcode and  "
			+ " d.company_mst=:companyMst  group by d.item_id, d.batch, d.barcode, d.mrp ")
	List<Object> findBranchStock(String branchcode, CompanyMst companyMst);

	@Query(nativeQuery = true, value = "select i.item_name,sum(ib.qty_out) from "
			+ "item_mst i,item_batch_dtl ib,category_mst c "
			+ "where i.id=ib.item_id and date(ib.voucher_date) >= :fuDate " + "and date(ib.voucher_date) <= :tuDate "
			+ "and ib.company_mst = :companymstid "
			+ "and ib.branch_code = :branchcode and i.category_id=c.id and c.id not in "
			+ "(select el.category_id from category_exception_list el where el.report_name=:reportName)"
			+ "GROUP BY i.item_name ORDER BY sum(ib.qty_out) DESC")
	List<Object> getFastMovingItems(java.util.Date fuDate, java.util.Date tuDate, String companymstid,
			String branchcode, String reportName);

	@Query(nativeQuery = true, value = "select sum(qty_in)- sum(qty_out) from item_batch_dtl  where "
			+ "item_id=:itemName")
	Double getTotalQty(String itemName);

	@Query(nativeQuery = true, value = "select i.item_name,sum(d.qty) from "
			+ "sales_trans_hdr h,sales_dtl d,category_mst c, item_mst i "
			+ "where i.id=d.item_id and date(h.voucher_date) >= :fuDate " + "and date(h.voucher_date) <= :tuDate "
			+ "and h.company_mst = :companymstid " + "and h.sales_mode=:salesmode " + "and h.id=d.sales_trans_hdr_id "
			+ "and h.voucher_number is not null "
			+ "and h.branch_code = :branchcode and i.category_id=c.id and c.id not in "
			+ "(select el.category_id from category_exception_list el where el.report_name=:reportname)"
			+ "GROUP BY i.item_name ORDER BY sum(d.qty) DESC")
	List<Object> fastMovingItemsBySalesMode(java.util.Date fuDate, java.util.Date tuDate, String companymstid,
			String branchcode, String salesmode, String reportname);

	@Query(nativeQuery = true, value = "select i.item_name,sum(d.qty) from "
			+ "sales_trans_hdr h,sales_dtl d,category_mst c, item_mst i "
			+ "where i.id=d.item_id and date(h.voucher_date) >= :fuDate " + "and date(h.voucher_date) <= :tuDate "
			+ "and h.company_mst = :companymstid " + "and h.sales_mode=:salesmode " + "and h.id=d.sales_trans_hdr_id "
			+ "and h.voucher_number is not null "
			+ "and h.branch_code = :branchcode and i.category_id=c.id and c.id=:category "
			+ "GROUP BY i.item_name ORDER BY sum(d.qty) DESC")
	List<Object> fastMovingItemsBySalesModeAndCategory(java.util.Date fuDate, java.util.Date tuDate,
			String companymstid, String branchcode, String salesmode, String category);

	List<ItemBatchDtl> findByCompanyMst(CompanyMst companyMst);

	@Query(nativeQuery = true, value = "select im.item_name,sum(i.qty_in),sum(i.qty_out)"
			+ " from item_batch_dtl  i, item_mst im where im.id=i.item_id and "
			+ " i.branch_code =:branchcode "
			+ "and i.company_mst=:companymstid and date(i.voucher_date) between :fdate "
			+ "and :tdate GROUP BY im.item_name")
	List<Object> getItemStockMovement(String branchcode, String companymstid, Date fdate, Date tdate);

	
	
	@Query(nativeQuery=true,value="select  i.item_name,sum(d.qty_in - d.qty_out) as openingStock from "
			+ " item_batch_dtl d ,item_mst i where d.item_id=i.id and "
			+ "  date(d.voucher_date)<:fdate "
			+ "and d.branch_code=:branchcode and d.company_mst=:companymstid group by i.item_name ")
	List<Object> ItemWiseOPeningStockReport(String branchcode, String companymstid, Date fdate);

	@Query(nativeQuery = true, value = "select i.item_name, sum(d.qty_in - d.qty_out) as closingStock from "
			+ " item_batch_dtl d ,item_mst i  where d.item_id=i.id and "
			+ "  date(d.voucher_date) >=:fdate and date(d.voucher_date)<=:tdate "
			+ "and d.branch_code=:branchcode and d.company_mst=:companymstid group by i.item_name ")
	List<Object> ItemWiseClosingStockReport(String branchcode, String companymstid, Date fdate, Date tdate);

	
	
	@Query(nativeQuery = true, value = "select b.branch_name,sum(d.profit) as totalprofit "
			+ "from  item_batch_dtl d,branch_mst b where d.branch_code=b.branch_code and d.company_mst=:companymstid "
			+ "and date(d.voucher_date)=:profitDate group by  b.branch_name")
	List<Object> getBranchwiseTotalProfit(String companymstid, java.util.Date profitDate);

	
	
	@Query(nativeQuery = true, value = "select bd.id, "
			+ "bd.barcode,"
			+ "bd.batch,"
			+ "bd.branch_code,"
			+ "bd.exp_date,"
			+ "bd.mrp,"
			+ "bd.particulars,"
			+ "bd.process_instance_id,"
			+ "bd.qty_in, "
			+ "bd.qty_out,"
//			+ "bd.source_dtl_id,"
//			+ "bd.source_parent_id,"
//			+ "bd.source_voucher_date,"
//			+ "bd.source_voucher_number,"
			+ "bd.store,"
			+ "bd.task_id,"
			+ "bd.voucher_date,"
			+ "bd.voucher_number,"
			+ "bd.item_id,"
			+ "i.item_name ,i.standard_price "
			+ "from opening_stock_dtl bd,"
			+ "item_mst i "
			+ "where  i.id=bd.item_id and  bd.company_mst=:companyMst")
	List<Object> findAllItemBatchDtl(CompanyMst companyMst);
	

	@Query(nativeQuery = true, value = "SELECT "
			+ "cat.category_name,"
			+ "itm.item_code,"
			+ "itm.item_name,"
			+ "sd.batch,"
			+ "sd.expiry_date,"
			+ "sd.qty,"
			+ "sd.rate,"
			+ "sd.qty*sd.rate,"
			+ "sd.cost_price,"
			+ "sd.qty*sd.cost_price,"
			+ "sd.rate*sd.cost_price "
			+ "FROM "
			+ "sales_dtl sd,"
			+ "category_mst cat,"
			+ "item_mst itm,"
			+ "sales_trans_hdr sth "
			+ "WHERE "
			+ "sd.item_id=itm.id AND "
			+ "itm.category_id=cat.id AND "
			+ "sd.sales_trans_hdr_id=sth.id AND "
			+ "sth.branch_code=:branchcode AND "
			+ "date(sth.voucher_date) >= :fromDate AND "
		    + "date(sth.voucher_date) <= :toDate "
		    + "GROUP BY "
		    + "cat.category_name,itm.item_code,itm.item_name,sd.batch,sd.expiry_date,sd.qty")
	List<Object> getPharmacyBranchStockMarginReport(java.util.Date fromDate, java.util.Date toDate, String branchcode );	
	

	//==============stock verification using itmbatchdtl ======18-06-2021=====anandu=========
	@Query(nativeQuery=true,value=" select sum(qty_in-qty_out) as qty from item_batch_dtl ibdtl "
			+ " where ibdtl.item_id=:itemId and ibdtl.barcode=:barcode and ibdtl.batch=:batch AND ibdtl.store = :storeName ")
	Double findQtyByItem(String itemId, String barcode, String batch, String storeName);

	
	//============end =============18-06-2021====================================
	
	
	
	//=============MAP-146-add round off method in stock verification  on mysql ========anandu=====20-09-2021==
		@Query(nativeQuery=true,value=" select round(sum(qty_in-qty_out),2) as qty from item_batch_dtl ibdtl "
				+ " where ibdtl.item_id=:itemId and ibdtl.barcode=:barcode and ibdtl.batch=:batch AND ibdtl.store = :storeName ")
		Double findQtyByItemMySql(String itemId, String barcode, String batch, String storeName);
		
	//=============MAP-146-add cast method in stock verification  on  derby========anandu=======20-09-2021====
		@Query(nativeQuery=true,value=" select cast((sum(qty_in-qty_out)) as decimal(10,2)) from item_batch_dtl ibdtl "
				+ " where ibdtl.item_id=:itemId and ibdtl.barcode=:barcode and ibdtl.batch=:batch AND ibdtl.store = :storeName ")
		Double findQtyByItemDerby(String itemId, String barcode, String batch, String storeName);
		
		
		
	
	
	@Query(nativeQuery = true, value = " select sum(qty_in) - sum(qty_out) as qty , batch,"
			+ " exp_date , item_id, store from item_batch_dtl where item_id = :itemId and store=:store "
			+ " group by batch,exp_date,store having qty > 0 order by exp_date asc ")
	List<Object> findByItemIdOrderByExpDateWithStore(String itemId,String store);

	//================sibi===============14-07-2021===============================//
	
	
	

//	  @Query(nativeQuery = true, value = "select c.category_name,sum(i.qty_in),sum(i.qty_out),i.voucher_date from category_mst c,item_batch_dtl i "
//			+ " ,item_mst im where im.id=i.item_id and im.category_id = c.id and i.branch_code =:branchcode "
//			+ " and i.company_mst=:companyMst and date(i.voucher_date) between :fdate and :tdate AND im.category_id =:id"
//			+ " GROUP BY c.category_name,i.voucher_date")
//List<Object> getCategoryWiseStockMovement(String branchcode, CompanyMst companyMst, java.util.Date fdate,
//		java.util.Date tdate, String id);
	
	
	  @Query(nativeQuery = true, value = "select sum(qty_in),sum(qty_out),c.category_name,d.voucher_date "
	  		+ " from item_batch_dtl d,item_mst i, category_mst c where c.id=:id "
	  		+ " and i.id=d.item_id and i.category_id=c.id and d.branch_code=:branchcode "
	  		+ " and d.company_mst=:companyMst and date(d.voucher_date)>=:fdate "
	  		+ " and date(d.voucher_date)<=:tdate  group by c.category_name,d.voucher_date")
List<Object> getCategoryWiseStockMovement(String branchcode, CompanyMst companyMst, java.util.Date fdate,
		java.util.Date tdate, String id);
	
	@Query(nativeQuery = true, value = "select c.category_name,sum(d.qty_in - d.qty_out) as openingStock from "
			+ " item_batch_dtl d ,item_mst i, category_mst c  where d.item_id=i.id and c.id=i.category_id "
			+ " and date(d.voucher_date)<:fdate "
			+ "and d.branch_code=:branchcode and d.company_mst=:companyMst and i.category_id = :id group by c.category_name")
	List<Object> getCategoryWiseOPeningStock(String branchcode, CompanyMst companyMst, java.util.Date fdate, String id);

	
	  @Query(nativeQuery = true, value = "select c.category_name, sum(d.qty_in - d.qty_out) as closingStock from "
			+ " item_batch_dtl d ,item_mst i, category_mst c  where d.item_id=i.id and c.id=i.category_id "
			+ " and date(d.voucher_date) >=:fdate and date(d.voucher_date)<=:tdate "
			+ "and d.branch_code=:branchcode and d.company_mst=:companyMst and i.category_id = :id group by c.category_name ")
	List<Object> getCategoryWiseClosingStock(String branchcode, CompanyMst companyMst, java.util.Date fdate,
			java.util.Date tdate, String id);

	
	  @Query(nativeQuery = true, value = "select sum(d.qty_in - d.qty_out) as qtybystore from item_batch_dtl d "
	  		+ " where d.item_id=:itemId and d.company_mst=:companymstid and d.store=:storeFrom and d.batch=:batch ")
	  
	  Double getItemStockDetailsByStore(String itemId, String batch, String companymstid, String storeFrom);

	  
	  @Query(nativeQuery = true, value ="select count(*) from item_batch_dtl where company_mst=:companymstid"
	  		+ " and date(voucher_date)>=:lastSuccessDate and date(voucher_date)<=:dayendDate and branch_code=:branchcode")
	int getCountOfStockFromItemBatchDtl(String companymstid, java.util.Date lastSuccessDate, java.util.Date dayendDate, String branchcode);

	
	  
	  @Query(nativeQuery = true, value = "select * from item_batch_dtl " + "where date(voucher_date) >= :fudate and "
				+ "date(voucher_date) <= :tudate and id not in (:hdrIds)")
		List<ItemBatchDtl> getItemBatchDtlBtwnDate(java.util.Date fudate, java.util.Date tudate,Pageable pageable,String hdrIds);

	  
	  @Query(nativeQuery = true, value = "select * from item_batch_dtl")
	List<ItemBatchDtl> findAllBatchDtl(Pageable topFifty);

	  @Query(nativeQuery = true, value = "select sum(d.qty_In), sum(d.qty_Out) , i.item_name, b.branch_code"
				+ "  from item_batch_dtl d, branch_mst b, item_mst i "
				+ " where  d.item_id = i.id and d.branch_code = b.branch_code and d.voucher_Date >= :startdate and "
				+ " d.voucher_Date <= :enddate and b.branch_code = :branchcode  group by i.item_name, b.branch_code ")

		ArrayList<Object> findByCompanyMstAndVoucherDateBetWeenDate(Date startdate, Date enddate, String branchcode);

	@Query(nativeQuery = true, value ="select sum(qty_in-qty_out) from item_batch_dtl where "
			+ "date(voucher_date) <= :fudate and branch_code = :branchCode and item_id = :itemId")
	Double getOpeningBalanceQty(java.util.Date fudate, String branchCode, String itemId);

	@Query(nativeQuery = true, value ="select sum(qty_in) from item_batch_dtl where date(voucher_date) >= :fudate "
			+ "and date(voucher_date) <= :tudate and branch_code = :branchCode and item_id = :itemId "
			+ "and particulars like :stockInParticular")
	Double getStockInQty(java.util.Date fudate, java.util.Date tudate, String branchCode, String itemId,
			String stockInParticular);

	@Query(nativeQuery = true, value ="select sum(qty_out) from item_batch_dtl where date(voucher_date) >= :fudate "
			+ "and date(voucher_date) <= :tudate and branch_code = :branchCode and item_id = :itemId "
			+ "and particulars like :salesParticular")
	Double getSaledQuantity(java.util.Date fudate, java.util.Date tudate, String branchCode, String itemId,
			String salesParticular);

	@Query(nativeQuery = true, value ="select sum(qty_out) from item_batch_dtl where date(voucher_date) >= :fudate "
			+ "and date(voucher_date) <= :tudate and branch_code = :branchCode and item_id = :itemId "
			+ "and particulars like :stockOutParticular")
	Double getStockOutQty(java.util.Date fudate, java.util.Date tudate, String branchCode, String itemId,
			String stockOutParticular);

	@Query(nativeQuery = true, value ="select sum(qty_in) from item_batch_dtl where date(voucher_date) >= :fudate "
			+ "and date(voucher_date) <= :tudate and branch_code = :branchCode and item_id = :itemId and "
			+ "particulars like :physicalStockParticular")
	Double getPhysicalStockIn(java.util.Date fudate, java.util.Date tudate, String branchCode, String itemId,
			String physicalStockParticular);

	@Query(nativeQuery = true, value ="select sum(qty_out) from item_batch_dtl where date(voucher_date) >= :fudate "
			+ "and date(voucher_date) <= :tudate and branch_code = :branchCode and item_id = :itemId and "
			+ "particulars like :physicalStockParticular")
	Double getPhysicalStockOut(java.util.Date fudate, java.util.Date tudate, String branchCode, String itemId,
			String physicalStockParticular);

	
	@Query(nativeQuery = true, value ="select * from item_batch_dtl where date(voucher_date) >= :fromDate "
			+ "and date(voucher_date) <= :toDate")
	List<ItemBatchDtl> findAllItemBatchDtlBetweenDate(java.util.Date fromDate, java.util.Date toDate);

	
	@Query(nativeQuery = true, value ="select sum(qty_in),sum(qty_out),batch,item_id,exp_date "
			+ "from item_batch_dtl where branch_code = :branchCode and date(voucher_date) <= :day_end_date "
			+ "group by item_id,batch,exp_date")
	List<Object> getSummaryOfDailyStock(String branchCode, Date day_end_date);

}
