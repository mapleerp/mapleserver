package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SchSelectionAttribInst;
@Repository
public interface SchSelectionAttribInstRepository extends JpaRepository<SchSelectionAttribInst,String> {

	List<SchSelectionAttribInst>	findByCompanyMst(CompanyMst companymst);

	SchSelectionAttribInst findByCompanyMstAndAttributeNameAndSchemeId(CompanyMst companyMst, String attribname,
			String schemeid);

	List<SchSelectionAttribInst> findByCompanyMstAndSchemeId(CompanyMst companyMst, String schemeid);

	List<SchSelectionAttribInst> findBySelectionId(String selectionId);

	List<SchSelectionAttribInst> findByCompanyMstAndSchemeIdAndSelectionId(CompanyMst companyMst, String schemeid,
			String selectionid);

	SchSelectionAttribInst findByCompanyMstAndAttributeNameAndSchemeIdAndSelectionId(CompanyMst companyMst,
			String attribname, String schemeid, String selectionid);

	List<SchSelectionAttribInst> findBySchemeIdAndSelectionId(String id, String selectionId);
	
}
