package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.UnitMst;

public interface UnitMstRepository extends JpaRepository<UnitMst, String>{
	UnitMst findByUnitNameAndCompanyMstId(String unitName,String companymstid);
	
	List<UnitMst>findByCompanyMstId(String companymstid);
	UnitMst findByUnitName(String unitName);
	
	
//	List<UnitMst> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
