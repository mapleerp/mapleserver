package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.RawMaterialIssueDtl;
import com.maple.restserver.entity.RawMaterialIssueHdr;
import com.maple.restserver.report.entity.RawMaterialIssueReport;

public interface RawMaterialIssueDtlRepository extends JpaRepository<RawMaterialIssueDtl, String>{
	List<RawMaterialIssueDtl> findByRawMaterialIssueHdrId(String hdrid);
	
}
