package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.GroupMst;
@Component
@Repository
public interface GroupMstRepository  extends JpaRepository<GroupMst,String>{

	Optional<GroupMst> findByGroupNameAndCompanyMstId(String groupName,String companymstid);
	List<GroupMst>findByCompanyMstId(String companymstid);
	
	Optional<GroupMst> findByIdAndCompanyMstId(String id,String companymstid);
}
