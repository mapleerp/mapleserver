package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.LoyalityVouchers;

@Repository
public interface VoucherRepository extends JpaRepository<LoyalityVouchers, String>{

}
