package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.RawMaterialReturnHdr;

public interface RawMaterialReturnHdrRepository extends JpaRepository<RawMaterialReturnHdr, String>{
	
	
	@Query(nativeQuery=true,value="select rd.voucher_number,rd.voucher_date ,i.item_name, u.unit_name,rm.batch,rm.qty from raw_material_issue_dtl rm , item_mst i,unit_mst u,raw_material_issue_hdr rd where i.id= rm.item_id and u.id= rm.unit_id and rm.raw_material_issue_hdr =rd.id and rd.voucher_number =:voucherNumber and date(rd.voucher_date) = :vDate  and rd.company_mst = :companyid")
			
	
	List<Object> rawMaterialByVoucherNoAndDate(String voucherNumber, Date vDate, String companyid);
	
	
@Query(nativeQuery=true,value="select rd.voucher_number,rd.voucher_date ,i.item_name, u.unit_name,rm.batch,rm.qty from raw_material_return_dtl rm , item_mst i,unit_mst u,raw_material_return_hdr rd where i.id= rm.item_id and u.id= rm.unit_id and rm.raw_material_return_hdr =rd.id and rd.voucher_number =:voucherNumber and date(rd.voucher_date) = :vDate  and rd.company_mst = :companyid")
			
	
	List<Object> rawMaterialRetunrByVoucherNoAndDate(String voucherNumber, Date vDate, String companyid);
	
   	 	


@Query(nativeQuery=true,value="select rd.voucher_number,rd.voucher_date, "
		+ "i.item_name, u.unit_name, rm.batch, rm.qty "
		+ "from raw_material_issue_dtl rm ,item_mst i ,unit_mst u, "
		+ "raw_material_issue_hdr rd where i.id=rm.item_id "
		+ "and u.id=rm.unit_id and rm.raw_material_issue_hdr=rd.id "
		+ "and rd.branch_code=:branchCode and rd.company_mst=:companymstid "
		+ "and date(rd.voucher_date) between :fdate and :vdate")


List<Object> rawMaterialIssueBetweenDate(String companymstid, String branchCode, Date fdate, Date vdate);

	
}
