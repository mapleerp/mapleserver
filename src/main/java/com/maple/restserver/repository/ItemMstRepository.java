package com.maple.restserver.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;

@Repository
public interface ItemMstRepository extends JpaRepository<ItemMst, String> {

	
	@Modifying(flushAutomatically = true)
    @Query(nativeQuery = true, value ="delete from item_mst where id=:itemid and id not in(select item_id from item_batch_dtl)")
    void	deleteItem(String itemid);
 
	  List<ItemMst>  findByBarCode(String barcode);
	  
	  
 
	 ItemMst  findByItemName(String itename);
	  
	Optional<ItemMst> findById(String id);
	
	
	@Query(nativeQuery = true,value="select count(*) from item_mst")
	Integer getitemCount();
	  @Query(nativeQuery = true, value = " select c.category_name from "
	  		+ " category_mst c , item_mst i where "
	  		+ " i.category_id = c.id and i.item_name = :itemName ")
	String getCategoryByItemName(String itemName);

	  
	  @Query(nativeQuery = true, value = " select c.category_name from "
		  		+ " category_mst c , item_mst i where "
		  		+ " i.category_id = c.id and i.id = :itemId ")
		String getCategoryByItemId(String itemId);
	  
	  
	 
		@Modifying(flushAutomatically = true)
	    @Query(nativeQuery = true, value ="update item_mst set branch_code =:branchcode where branch_code is null")
	    void	updateItemMst(String branchcode );
	    
	  
	List<ItemMst> findByCompanyMstAndCategoryId(CompanyMst companyId, String categoryId);



	Optional<ItemMst> findByItemNameAndCompanyMst(String itemname, CompanyMst companyMst);



	List<ItemMst> findByCompanyMst(CompanyMst companyMst);





	 @Query(nativeQuery = true, value = " select * from item_mst i where i.company_mst=:companyMst and i.service_or_goods=:serviceorgoods")
	List<ItemMst> findByCompanyMstAndItemType(CompanyMst companyMst, String serviceorgoods);
	
//	List<ItemMst> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
	
	 @Query(nativeQuery = true,value="select * from item_mst OFFSET :nCount ROWS FETCH NEXT 1 ROW ONLY")
     ItemMst  fetchRandomItem(int nCount);
	 
	 
	 
	 @Query(nativeQuery = true,value="select count(id) from item_mst" )
	 	int itemCount();



	List<ItemMst> findAllByOrderByItemName();

	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,i.standardPrice  , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
 
			+ " b.batch , i.itemCode,date(b.expDate),i.itemPriceLock FROM ItemMst i , UnitMst u ,"
			+ " ItemBatchMst b, CategoryMst c "
			+ " WHERE i.unitId= u.id  AND i.id = b.itemId AND c.id=i.categoryId AND "
			+ "   c.categoryName =  :catName AND i.companyMst =:companyMst"
			+ " and i.isDeleted='N' AND lower(i.itemName) LIKE :searchItemName "
			+ "ORDER BY i.itemRank DESC")
	List<Object> getitemByCategoryAndName(String catName, String searchItemName,CompanyMst companyMst);
	 
	 
	
	
	
	@Query("SELECT i.itemName  ,"
		
			+ " i.id, "
			+ "c.categoryName  "
			+ " FROM ItemMst i , CategoryMst c  "
			+ " WHERE i.categoryId= c.id  AND lower(i.itemName) LIKE  :searchItemName AND i.companyMst =:companyMst "
			+ "")
 
	public List<Object> findSearchByName(@Param("searchItemName") String searchItemName ,  Pageable pageable,CompanyMst companyMst);

	@Query(nativeQuery = true,value="select item_name from item_mst")
	List<String> findAllItemName();

	@Query(nativeQuery = true,value = "select * from item_mst where category_id is null")
	List<ItemMst> getItemWithNullcategory();

	@Query(nativeQuery = true,value = "select * from item_mst where category_id = :categoryId "
			+ " and id not in (select item_id from kot_item_mst)")
	List<ItemMst> getItemNotInKotItemMst(String categoryId);
	
	@Query(nativeQuery = true,value = "select * from item_mst where category_id = :categoryId "
			+ " and lower(item_name) like :itemname and id not in (select item_id from kot_item_mst)")
	List<ItemMst> searchItemNotInKotItemMst(String categoryId,String itemname);
	 
	
	@Query(nativeQuery = true,value="select i.item_name, i.standard_price,i.item_code,tax_rate ,c.category_name,u.unit_name,i.sgst,i.id,i.hsn_code,i.bar_code from item_mst i ,category_mst c ,unit_mst u  where c.id=i.category_id and u.id=i.unit_id")
	
	List<Object>exportItemdetails();

	
	@Query(nativeQuery = true,value=""
			+ "select A.itemName, A.itemId, sum(A.salesCount),sum(A.purchaseCount) , sum(A.stockCount) from   " + 
			" (select i.item_name as itemName  , i.id as itemId , 0 as salesCount ,0 as purchaseCount ,0 as stockCount  from item_mst i " + 
			" union " + 
			" select i.item_name as itemName, i.id as itemId, count(d.item_id) as salesCount, 0 as purchaseCount, 0 as stockCount " + 
			" from item_mst i, sales_dtl d where d.item_id=i.id group by  i.item_name ,  i.id" + 
			" union " + 
			" select i.item_name as itemName, i.id as itemId,0 as salesCount , count(p.item_id) as purchaseCount, 0 as stockCount " + 
			" from item_mst i, purchase_dtl p where p.item_id=i.id group by  i.item_name ,  i.id " + 
			" union " + 
			" select i.item_name as itemName, i.id as itemId ,0 as salesCount ,0 as purchaseCount, count(s.item_id) as stockCount from " + 
			" item_mst i, item_batch_dtl s where  s.item_id=i.id group by  i.item_name ,  i.id)A " + 
			" group by A.itemName, A.itemId order by A.itemName "
			+ "")
	List<Object> getItemListWithCountofSaleStockPurchase(String companyid);
}

