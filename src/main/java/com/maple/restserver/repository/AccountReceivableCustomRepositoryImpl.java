package com.maple.restserver.repository;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.ItemBatchMst;

@Repository
public class AccountReceivableCustomRepositoryImpl implements AccountReceivableRepositoryCustom {

	@PersistenceContext
	 EntityManager em;
	 
	    // constructor
	
	
	
	 
	   

		@Override
		public List<AccountReceivable> findByCompanyMstAndAccountHeads(String companyId, String CustomerId) {
			CriteriaBuilder cb = em.getCriteriaBuilder();
	        CriteriaQuery<AccountReceivable> cq = cb.createQuery(AccountReceivable.class);
	     
	        Root<AccountReceivable> accountRecevable = cq.from(AccountReceivable.class);
	        List<Predicate> predicates = new ArrayList<>();
	         
	        
	        if (companyId != null) {
	            predicates.add(cb.equal(accountRecevable.get("companyMst"), companyId)  );
	            
	            predicates.add(cb.equal(accountRecevable.get("accountHeads"),CustomerId ) );
	            
	            
	            
	          //  predicates.add(cb.add(Restrictions.ge(accountRecevable.get("voucherDate"), fromdate )) ));
	             
	            
	            //criteria.add(Restrictions.between("auditDate", sDate, eDate));
	           
	            
	        }
	        cq.where(predicates.toArray(new Predicate[0]));
	     
	        return em.createQuery(cq).getResultList();
			 
		}

	 /*
	  * return em.createQuery(query)
         .setFirstResult(offset) // offset
         .setMaxResults(limit) // limit
         .getResultList();
	  */
	 
	    
}
