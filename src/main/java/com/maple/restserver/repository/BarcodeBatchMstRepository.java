package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.BarcodeBatchMst;
@Component
@Repository
public interface BarcodeBatchMstRepository extends JpaRepository<BarcodeBatchMst, String>{

	Optional<BarcodeBatchMst> findByBarcodeAndBatchCodeAndBranchCode(String barcode, String batch, String branchcode);
	
	

}
