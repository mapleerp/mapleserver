package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.GoodReceiveNoteHdr;

@Component
@Repository
public interface GoodReceiveNoteHdrRepository extends JpaRepository<GoodReceiveNoteHdr,String>{

	List<GoodReceiveNoteHdr> findByCompanyMstId(String companymstid);

	List<GoodReceiveNoteHdr> findAll();

	List<GoodReceiveNoteHdr> findByVoucherDate( Date vdate);

	GoodReceiveNoteHdr findByBranchCode(String branchcode);

	List<GoodReceiveNoteHdr> findBySupplierId(String suppplierid
			);

	
	@Query(nativeQuery = true, value ="SELECT * FROM good_receive_note_hdr WHERE voucher_number = :voucherNumber AND voucher_date = :voucherDate")
	Optional<GoodReceiveNoteHdr> findByVoucherDateAndVoucherNumber(java.util.Date voucherDate, String voucherNumber);

	//List<Object> getSupplierIdAndVouchernumber(String branchcode, CompanyMst companyMst, String vDate);

	

}
