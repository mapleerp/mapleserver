package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.StockVerificationDtl;

public interface StockVerificationDtlRepository extends JpaRepository<StockVerificationDtl, String> {

	List<StockVerificationDtl> findByStockVerificationMstId(String hdrId);

	@Query(nativeQuery = true, value = "select im.item_name, "
			+ "u.user_name, s.qty, s.system_qty, sm.branch_code "
			+ "from stock_verification_dtl s, item_mst im, user_mst u, stock_verification_mst sm "
			+ "where u.id=sm.user_id and im.id=s.item_id and sm.company_mst=:companymstid and "
			+ "s.stock_verification_mst=sm.id and date(voucher_date) between :fromdate and :todate")
	List<Object> findStockVerificationByDate(Date fromdate, Date todate, String companymstid);
}
