package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.SubBranchSalesDtl;
@Component
@Repository
public interface SubBranchSalesDtlRepository extends JpaRepository<SubBranchSalesDtl, String>{

	
	@Query(nativeQuery=true,value="select sum(d.qty) as qty, d.item_id, d.batch, d.unit_id "
			+ "from sub_branch_sales_dtl d, sub_branch_sales_trans_hdr h "
			+ "where h.company_mst=:companymstid and h.branch_code=:branchcode "
			+ "and d.sub_branch_sales_trans_hdr=h.id and date(h.voucher_date)>=:fudate and "
			+ "h.STATUS='NOT CONVERTED' and "
			+ "date(h.voucher_date)<=:ftdate and h.voucher_number is not null  "
			+ "group by d.item_id, d.batch, d.unit_id ")
	List<Object> getSpneserDtlBySpenserIdAndDate(String branchcode, String companymstid, java.util.Date fudate,
			java.util.Date ftdate);



}
