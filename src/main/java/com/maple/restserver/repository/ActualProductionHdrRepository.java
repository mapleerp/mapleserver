package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.ActualProductionHdr;

public interface ActualProductionHdrRepository extends JpaRepository<ActualProductionHdr, String>{
	
	Optional<ActualProductionHdr> findByIdAndCompanyMstId(String productionhdrid, String companymstid) ; 
	@Query(nativeQuery = true, value = "select * from actual_production_hdr where date(voucher_date)=:fdate and voucher_number is NULL and company_mst=:companymstid")
	Optional<ActualProductionHdr> findByIdAndCompanyMstIdAndVoucherDateAndVoucherNumberIsNull(String companymstid,Date fdate);
	
	
	@Query(nativeQuery = true, value = "select * from actual_production_dtl ap where ")
List<Object>	actualProductionDtlByVoucherNo(String companymstid,String branchcode,Date fdate,Date tdate);
	

	
	
	
	
	@Query(nativeQuery = true, value = "select sum(d.actual_qty),u.unit_name ,h.voucher_date,i.item_name,d.id,d.batch from actual_production_hdr h,actual_production_dtl d,unit_mst u,item_mst i  where h.id=d.actual_production_hdr"
			+ " and d.item_id=i.id and i.unit_id=u.id and h.branch_code=:branchcode  and h.company_mst=:companymstid"
			+ " and date(h.voucher_date) between :fdate and :tdate and h.voucher_number is not null group by u.unit_name, h.voucher_date,i.item_name,d.id,d.batch ORDER BY d.batch ASC,h.voucher_date ASC")
List<Object>	actualProductionReport(String companymstid,String branchcode,Date fdate,Date tdate);
	
	
	
	@Query(nativeQuery = true, value = "select sum(d.actual_qty),u.unit_name ,h.voucher_date,i.item_name from actual_production_hdr h,actual_production_dtl d,unit_mst u,item_mst i  where h.id=d.actual_production_hdr"
			+ " and d.item_id=i.id and i.unit_id=u.id and h.branch_code=:branchcode  and h.company_mst=:companymstid"
			+ " and date(h.voucher_date) between :fdate and :tdate and h.voucher_number is not null group by u.unit_name, h.voucher_date,i.item_name")
List<Object>	actualProductionPrintReport(String companymstid,String branchcode,Date fdate,Date tdate);
	
	
	@Query(nativeQuery = true, value = "select sum(pd.qty)from production_dtl_dtl pd,actual_production_hdr ah,"
			+ "actual_production_dtl ad,production_dtl pdi where ad.actual_production_hdr = ah.id and ad.production_dtl = pd.id"
			+ " and pd.raw_material_item_id=:itemId and ah.id=:actualProductionHdrId and pd.production_dtl_id = pdi.id") 
	double getSumOfQty(String actualProductionHdrId,String itemId);
	
	
	@Query(nativeQuery = true,value="select"
			+ " d.actual_qty,"
			+ "pd.qty,"
			+ "d.production_cost,"
			+ "i.item_name,"
			+ "d.batch,"
			+ " h.voucher_date "
			+ "from actual_production_hdr h,"
			+ " actual_production_dtl d,production_dtl pd,"
			+ "item_mst i where h.id=d.actual_production_hdr and i.id=d.item_id and pd.id=d.production_dtl "
			+ "and h.company_mst=:companymstid and h.branch_code=:branchcode and date(h.voucher_date) between :fdate and :tdate and h.voucher_number is not null")
	
	List<Object>	actualproductionbetweendate(String companymstid,String branchcode,Date fdate,Date tdate);


	
	@Query(nativeQuery = true, value = "select count (*) from actual_production_hdr h  where h.voucher_date=:sqlDate and h.voucher_number is NOT NULL "
			+ "and h.branch_code=:branchCode and h.company_mst=:companymstid ")
	int actualproductionHdrCount(String branchCode,java.sql.Date sqlDate,String companymstid);


	
	@Query(nativeQuery = true, value = "select count (*) from actual_production_hdr h,actual_production_dtl d  "
			+ " where  h.id=d.actual_production_hdr "
			+ " and  h.voucher_date=:sqlDate and h.voucher_number is NOT NULL "
			+ "and h.branch_code=:branchCode and h.company_mst=:companymstid ") 
	int actualproductionDtlCount(String branchCode,java.sql.Date sqlDate,String companymstid);
	
	@Query(nativeQuery = true,value = "select * from actual_production_hdr where "
			+ "date(voucher_date) >= :tdate and date(voucher_date)<=:fdate")
	List<ActualProductionHdr> findByProductionHdrBetweenDate(Date tdate, Date fdate);

}
