package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.StoreChangeDtl;
import com.maple.restserver.entity.StoreChangeMst;

@Repository
public interface StoreChangeDtlRepository extends JpaRepository<StoreChangeDtl, String>{

	
	List<StoreChangeDtl> findByStoreChangeMst(StoreChangeMst storechangeMst);

	List<StoreChangeDtl> findByStoreChangeMstAndCompanyMst(StoreChangeMst storeChangeMst, CompanyMst comapnyMst);
	
}
