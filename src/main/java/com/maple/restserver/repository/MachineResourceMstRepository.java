package com.maple.restserver.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.MachineResourceMst;
import com.maple.restserver.entity.SalesOrderTransHdr;


@Repository
public interface MachineResourceMstRepository extends JpaRepository<MachineResourceMst, String> {

	
	MachineResourceMst findByMachineResourceName(String machineresourcename);
	
	
	@Query(nativeQuery = true,value="select mr.machine_resource_name,mr.capcity,u.unit_name from machine_resource_mst mr ,unit_mst u where u.id=mr.unit_id")
	List<Object>findMachineDeltails();
	
@Query(nativeQuery = true,value = "select * from machine_resource_mst " )
	ArrayList<MachineResourceMst> fetchMachineDetais();

@Query(nativeQuery = true,value="select sum(d.qty),d.item_id , rl.resource_cat_id,d.unit_id from sales_order_trans_hdr h ,sales_order_dtl d , resource_cat_item_link_mst rl "
		+ "where h.id=d.sales_order_trans_hdr and d.item_id=rl.item_id  and h.order_status='Orderd' group by d.item_id , rl.resource_cat_id,d.unit_id  ")
List<Object>SaleOrderItemCategoryList();

// Date fromDate
//and date(h.voucher_date)=:fromDate
@Query(nativeQuery = true,value="select d.item_id,sum(d.qty),d.unit_id ,mc.mix_name, h.id from sales_order_trans_hdr h ,sales_order_dtl d ,mix_category_mst mc,mix_cat_item_link_mst il  where  h.id =d.sales_order_trans_hdr  "
		+ "and h.order_status='Orderd' and il.item_id=d.item_id and il.resource_cat_id =mc.id and date(h.voucher_date) >=:fromDate and date(h.voucher_date) <= :toDate "
		+ " and h.company_mst=:companymstid and d.item_id in(select kd.item_id from kit_definition_mst kd ) and h.id not in (select sp.sale_order_hdr_id  from sale_order_production_planing_link_mst sp ) group by d.item_id,d.unit_id ,mc.mix_name,h.id order by mc.mix_name")
List<Object>fetchSaleOrderItemQty( String companymstid,Date fromDate ,Date toDate);
//sp where date(sp.voucher_date)<:fromDate
//,Date fromDate
// and date(h.voucher_date)=:fromDate
@Query(nativeQuery = true,value="select m.capcity from machine_resource_mst where m.id=:id")
Double machineCapacity(String id);

@Query(nativeQuery = true,value="select h.id from sales_order_trans_hdr h where h.id not in (select sp.sale_order_hdr_id from sale_order_production_planing_link_mst sp ) ")
//where date(sp.voucher_date)<:fromDate
//Date 
//Date fromDate
List<String> fetchSaleOrderHdrId();
@Modifying
@Query(nativeQuery = true,value = "update production_dtl d set d.status='N' where d.id is not null" )
void productionPrePlaningToProduction();

@Query(nativeQuery = true,value="select count(h.id) from sales_order_trans_hdr h ,sales_order_dtl d   where  h.id =d.sales_order_trans_hdr "  
			+ "and h.order_status='Orderd'" + 
		" and h.company_mst=:companymstid and date(h.voucher_date) >= :fromDate and date(h.voucher_date) <= :tDate and d.item_id in(select kd.item_id from kit_definition_mst kd ) and h.id not in (select sp.sale_order_hdr_id  from sale_order_production_planing_link_mst sp ) ")
Integer fetchCountOfSaleOrder(String companymstid,Date fromDate,Date tDate);


@Query(nativeQuery = true,value="select count(h.id) from sales_order_trans_hdr h ,sales_order_dtl d ,mix_category_mst mc,mix_cat_item_link_mst il where  h.id =d.sales_order_trans_hdr "  
		+ "and h.order_status='Orderd' and il.item_id=d.item_id and il.resource_cat_id =mc.id" + 
	" and h.company_mst=:companymstid and date(h.voucher_date) >= :fromDate and date(h.voucher_date) <= :tDate  and d.item_id in(select kd.item_id from kit_definition_mst kd ) and h.id not in (select sp.sale_order_hdr_id  from sale_order_production_planing_link_mst sp ) ")
Integer fetchCountOfSaleOrderMixCategory( String companymstid,Date fromDate,Date tDate);

















}
