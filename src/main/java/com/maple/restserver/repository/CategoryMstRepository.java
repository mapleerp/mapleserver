
package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CategoryManagementMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;


@Repository
@Component
public interface CategoryMstRepository extends JpaRepository<CategoryMst, String>{
	
	Optional<CategoryMst> findById(String id);
	

	@Query(nativeQuery = true,value="select * from category_mst where parent_id is null")
	List<CategoryMst> getParentCategory();

	@Query(nativeQuery = true,value="select * from category_mst where parent_id in "
			+ "(select id from category_mst where "
			+ "category_name=:catName)")
	List<CategoryMst> getChildCategory(String catName);
	CategoryMst findByCompanyMstIdAndCategoryName(String comapnyMstId,
			String categoryname);
	
	 List<CategoryMst> findByCompanyMstId(String companymstid);

		@Query("SELECT c FROM CategoryMst c WHERE LOWER(c.categoryName) like   LOWER(:categoryname) AND c.companyMst.id=:companymstid")

	List<CategoryMst> findSearch(String categoryname, String companymstid);
		
		
		@Query("SELECT c FROM CategoryMst c")

		List<CategoryMst> findSearchAll();
		
		

		CategoryMst findByCompanyMstAndId(CompanyMst companyMst, String categoryid);

		CategoryMst findByCategoryNameAndCompanyMst(String categoryName,CompanyMst companyMst);
}
