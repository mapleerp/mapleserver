package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndReportStore;
@Component
@Repository
public interface DayEndReportStoreRepository extends JpaRepository<DayEndReportStore, String>{


	List<DayEndReportStore> findByCompanyMstAndDate(CompanyMst companyMst, java.util.Date date);
	
	
	
	

}
