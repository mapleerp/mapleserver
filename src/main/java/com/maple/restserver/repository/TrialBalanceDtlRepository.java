package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.TrialBalanceDtl;

public interface TrialBalanceDtlRepository extends JpaRepository<TrialBalanceDtl, String>{

	@Query(nativeQuery = true, value = "SELECT * FROM trial_balance_dtl WHERE trial_balance_hdr = :trialBalanceHdrId")
	List<Object> getTrialBalanceDtlByHdrId(String trialBalanceHdrId);

	

}
