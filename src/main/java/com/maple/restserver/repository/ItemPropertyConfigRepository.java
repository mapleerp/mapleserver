package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemPropertyConfig;

@Repository
public interface ItemPropertyConfigRepository  extends JpaRepository<ItemPropertyConfig, String>{

	
	List<ItemPropertyConfig>findByCompanyMst(CompanyMst companyMst);

	ItemPropertyConfig findByItemIdAndPropertyName(String itemid, String propertyname);

	List<ItemPropertyConfig> findByItemId(String itemid);
}
