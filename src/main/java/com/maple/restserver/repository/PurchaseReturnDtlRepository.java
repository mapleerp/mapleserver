package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseReturnDtl;
import com.maple.restserver.entity.PurchaseReturnHdr;

public interface PurchaseReturnDtlRepository extends JpaRepository<PurchaseReturnDtl, String> {

	List<PurchaseReturnDtl> findByPurchaseReturnHdrAndCompanyMst( PurchaseReturnHdr purchaseReturnHdr ,CompanyMst companyMst);

	void deleteById(String id);

	List<PurchaseReturnDtl> findByPurchaseReturnHdr(PurchaseReturnHdr purchaseReturnHdr);




}
