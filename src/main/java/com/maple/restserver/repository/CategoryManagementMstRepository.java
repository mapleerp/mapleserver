package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CategoryManagementMst;
@Repository

public interface CategoryManagementMstRepository extends JpaRepository<CategoryManagementMst, String>{

	@Query(nativeQuery = true,value="select * from category_management_mst where category_id is null")
	List<CategoryManagementMst> getParentCategory();
	
	@Query(nativeQuery = true,value="select * from category_management_mst where sub_category_id in"
			+ "(select id from category_mst where category_name=:catName) ")
	List<CategoryManagementMst> getCategoryWithParent(String catName);
	CategoryManagementMst findByCategoryIdAndSubCategoryId(String catid, String subcatid);

}
