
package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.his.entity.BedBooking;

 

@Component
@Repository
public interface BedBookingRepository extends JpaRepository<BedBooking,String>{
	
	
	List<BedBooking>findByCompanyMstId(String companymstid);
	
	
//	List<BedBooking> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
