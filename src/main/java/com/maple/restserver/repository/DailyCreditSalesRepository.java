package com.maple.restserver.repository;


import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.DailyCreditSales;
 
@Component
@Repository
public interface DailyCreditSalesRepository extends JpaRepository<DailyCreditSales, String>{
	
//	List<AcceptStock> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
	
	DailyCreditSales  findByReportDate(java.util.Date voucherDate);
	

	
	DailyCreditSales	findByReportDateAndBranchCode(Date date,String branchCode);


}
