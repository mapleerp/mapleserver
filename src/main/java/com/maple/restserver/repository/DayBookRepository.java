package com.maple.restserver.repository;



import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayBook;
@Component
@Repository
public interface DayBookRepository extends JpaRepository<DayBook, String>{
	List< DayBook> 	findByVoucheNumberAndVoucherDate(String voucherNumber , Date voucherDate );
	
	void 	deleteByVoucheNumberAndVoucherDate(String voucherNumber , Date voucherDate );
	void deleteBySourceVoucheNumber(String voucherNumber);
	List< DayBook> 	findBySourceVoucherDateAndBranchCodeAndCompanyMst(Date date,String branchCode,CompanyMst companyMst);
	
	

}
