package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesTypeMst;

@Repository
public interface SalesTypeMstRepository extends JpaRepository<SalesTypeMst, String>{

	
	List<SalesTypeMst> findByCompanyMst(CompanyMst companyMst);
	SalesTypeMst findByCompanyMstAndSalesType(CompanyMst companyMst,String salesType);
	
	
	
	
	List<SalesTypeMst> findBySalesTypeAndCompanyMst(String string, CompanyMst companyMst);
}
