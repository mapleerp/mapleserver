package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SalesPropertiesConfigMst;

@Repository
public interface SalesPropertiesConfigMstRepository extends JpaRepository<SalesPropertiesConfigMst, String> {

	List<SalesPropertiesConfigMst> findByPropertyName(String propertyName);

}
