package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.BatchPriceDefinition;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.report.entity.ItemNotInBatchPriceReport;

@Repository
public interface BatchPriceDefinitionRepository extends JpaRepository<BatchPriceDefinition, String>{

	
	@Query(nativeQuery = true, value ="select p.* from batch_price_definition p where p.end_date is null "
			+ "and p.item_id =:itemid and p.price_id=:priceid and p.unit_id=:unitid and p.company_mst=:companymstid")
	BatchPriceDefinition findPriceDefenitionByItemUnitPriceAndDate(String itemid, String priceid, String unitid,String companymstid);
    
	
	@Query(nativeQuery = true, value ="select * from batch_price_definition p where "
			+ "date(p.end_date)>=:udate and date(p.start_date)<=:udate and p.item_id =:itemId and p.price_id=:priceId and "
			+ "p.batch=:batch")
    List<BatchPriceDefinition> findByItemIdAndStartDate(String itemId,String priceId,String batch,Date udate);
	
	@Query(nativeQuery = true, value ="select * from batch_price_definition p where "
			+ "date(p.end_date) is null and date(p.start_date)<=:udate and p.item_id =:itemId and p.price_id=:priceId and "
			+ "p.batch=:batch")
    List<BatchPriceDefinition> findByItemIdAndStartDateEnddateNull(String itemId,String priceId,String batch,Date udate);
	
	@Modifying
	@Query(nativeQuery = true, value ="update batch_price_definition p set p.end_date=:startdate where p.item_id=:itemid and p.price_id =:priceId and p.unit_id =:unitId and p.batch=:batch and p.end_date is null")
     void UpdatePriceDefinitionbyItemIdUnitIdBatch(String itemid,Date startdate,String priceId,String unitId,String batch);
	
	@Query(nativeQuery = true, value ="select * from batch_price_definition p where p.end_date is null")
    List<BatchPriceDefinition> fetchPriceDefinition();
	
	@Query(nativeQuery = true, value ="select * from batch_price_definition p where p.item_id =:itemId and "
			+ "p.price_id=:priceId and p.unit_id =:unitId and p.batch =:batch"
			+ " and date(p.start_date) <= :tdate and date(p.end_date) > :tdate ")
	List<BatchPriceDefinition> findByItemIdAndUnitId(String itemId,String priceId,String unitId,String batch,Date tdate);

	@Query(nativeQuery = true, value ="select * from batch_price_definition p where p.item_id =:itemId and "
			+ "p.price_id=:priceId and p.unit_id =:unitId and p.batch =:batch"
			+ " and date(p.start_date) <= :tdate and date(p.end_date) is null")
	List<BatchPriceDefinition> findByItemIdAndUnitIdEndDateNull(String itemId,String priceId,String unitId,String batch,Date tdate);


	List<BatchPriceDefinition> findByCompanyMstAndItemId(CompanyMst companyMst, String itemid);


	List<BatchPriceDefinition> findByCompanyMstAndPriceId(CompanyMst companyMst, String pricetypeid);


	@Query(nativeQuery = true, value =" select p.* from batch_price_definition p where p.item_id =:itemId and p.price_id=:priceId"
			+ " and p.unit_id=:unitid and p.batch=:batch and p.company_mst=:companyMst and (date(p.start_date) <= :sdate and date(p.end_date) is null)")
	List<BatchPriceDefinition> findByCompanyMstAndItemIdAndPriceIdAndUnitIdAndBatchAndStartDate(CompanyMst companyMst,
			String itemId, String priceId, String unitid, String batch, Date sdate);





	@Query(nativeQuery = true,value = "select i.item_id,i.batch "
			+ "from item_batch_mst i where "
			+ "not exists (select 1 from batch_price_definition b where b.item_id = i.item_id and "
			+ "b.batch = i.batch)")
	List<Object> getItemsNotInBatchPrice();




	List<BatchPriceDefinition> findByItemIdAndPriceId(String itemid, String priceid);

	@Query(nativeQuery = true, value ="select p.* from batch_price_definition p where p.end_date is null "
			+ "and p.item_id =:itemid and p.price_id=:priceid and p.unit_id=:unitid and p.company_mst=:companymstid")
	BatchPriceDefinition findBatchPriceDefinitionByItemUnitPriceAndDate(String itemid, String priceid, String unitid,
			String companymstid);

	
	
}
