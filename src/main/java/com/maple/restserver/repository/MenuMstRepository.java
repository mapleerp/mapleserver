package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MenuConfigMst;
import com.maple.restserver.entity.MenuMst;
@Component
@Repository
public interface MenuMstRepository extends JpaRepository<MenuMst, String>{

	List<MenuMst> findByCompanyMst(CompanyMst companyMst);

	MenuMst findByMenuIdAndCompanyMst(String id, CompanyMst companyMst);
	

	/*
	 * select * from  MenuConfigMst c, MenuMst m where c.id = m.parentId and c.menuName= :param
	 */
//	Li
	
	/*
	 *  select * from  MenuConfigMst c where c.id in (select id from menuMst where parentId = :param)
	 *  
	 */
	
	

	@Query("SELECT w FROM MenuConfigMst w WHERE LOWER(w.menuName) like   LOWER(:menuname) AND w.companyMst.id=:companymstid")

List<MenuConfigMst> findSearch(String menuname, String companymstid);

	List<MenuMst> findByParentIdAndCompanyMst(String id, CompanyMst companyMst);
	
}
