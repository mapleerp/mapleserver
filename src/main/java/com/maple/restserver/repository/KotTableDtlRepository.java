package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.KotTableDtl;
import com.maple.restserver.entity.KotTableHdr;
@Component
@Repository
public interface KotTableDtlRepository extends JpaRepository<KotTableDtl, String>{

	List<KotTableDtl> findByKotTableHdr(KotTableHdr kotTableHdr);

//	List<KotTableDtl> findByKotTableHdrId(String kottablehdrid);
	@Query(nativeQuery=true,value="select k.qty,k.rate ,i.item_name,k.id,k.kot_number,k.kot_table_hdr from kot_table_dtl k, item_mst i "
		    + " where k.kot_table_hdr=:kotTableHdr and k.item_id=i.id")
List<Object> findByKotHdr(KotTableHdr kotTableHdr);
	
}
