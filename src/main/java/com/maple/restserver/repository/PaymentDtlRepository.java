

package com.maple.restserver.repository;

 
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.report.entity.PaymentVoucher;


@Transactional
@Repository
public interface PaymentDtlRepository extends JpaRepository<PaymentDtl, String>{

	List<PaymentDtl> findBypaymenthdrVoucherDate(Date vdate);
	
	
 
	 List<PaymentDtl> findBypaymenthdrId(String paymenthdrId);
	    Optional<PaymentDtl> findByIdAndId(String id, String paymenthdrId);
	 	    
	    @Query(nativeQuery = true, value = "SELECT SUM(amount) as amount  FROM payment_dtl d, payment_hdr h "
	    		+ " "
	    		+ " where h.id = d.paymenthdr_id and h.voucher_number = :voucherNumber and date(h.voucher_date) = :voucherDate")
	    
 
	    public Double findSumDebit(@Param("voucherNumber") String voucherNumber ,  @Param("voucherDate") Date voucherDate);
	    
//	    List<PaymentDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
//	    List<PaymentDtl> findByVoucherNumberAndVoucherDate(String voucherNumber, Date voucherDate,String voucherType);
		
	    @Query(nativeQuery = true, value = "SELECT SUM(amount) as totalamount  FROM payment_dtl d where paymenthdr_id =:hdrId"
	    		)
public Double getSumOfPaymentDtlAmount(String hdrId);
	    
		 @Query(nativeQuery=true,value="select d.amount,d.remark from payment_hdr h, payment_dtl d where h.id=d.paymenthdr_id "
		 		+ "and d.account_id=:accountid and date(h.voucher_date)=:reportdate")
	    List<Object> findByAccountIdAndInstrumentDate(String accountid,Date reportdate);
	    
	    
			@Modifying
			@Query(nativeQuery = true,value="update payment_dtl d set d.account_id =:toAccountId where d.account_id =:fromAccountId ")
		    void accountMerge(String fromAccountId, String toAccountId);   
			
			@Query(nativeQuery = true,value = "select a.account_name,d.remark,d.amount,d.instrument_number,d.bank_account_number,d.mode_of_payment from  payment_dtl d ,account_heads a where a.id=d.account_id and d.paymenthdr_id=:paymentHdrId")
			List<Object>fetchBypaymenthdrId(String paymentHdrId);
}
