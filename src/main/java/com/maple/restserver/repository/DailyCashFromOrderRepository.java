package com.maple.restserver.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.DailyCashFromOrdersDtl;

public interface DailyCashFromOrderRepository extends JpaRepository<DailyCashFromOrdersDtl, String> {
	DailyCashFromOrdersDtl findByRecordDateAndBranchCode(Date date,String branchCode);
}
