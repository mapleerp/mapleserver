package com.maple.restserver.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.DayEndClosureDtl;
import com.maple.restserver.entity.DayEndClosureHdr;

@Component
@Repository
public interface DayEndClosureDtlRepository extends JpaRepository<DayEndClosureDtl, String> {

	List<DayEndClosureDtl> findByDayEndClosureHdrId(String hdrId);

	@Query(nativeQuery = true, value = " SELECT sum(denomination*count)  as total from day_end_closure_dtl where day_end_closure_hdr_id=:hdrId ")
	Double gettotal(String hdrId);

	ArrayList<DayEndClosureDtl> findByDayEndClosureHdr(DayEndClosureHdr dayEndHdr);
}
