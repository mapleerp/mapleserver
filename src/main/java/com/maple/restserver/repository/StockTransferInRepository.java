package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.StockTransferIn;



public interface StockTransferInRepository extends JpaRepository<StockTransferIn, Integer>{

//	List<StockTransferIn> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);

	List<StockTransferIn> findByCompanyMst(String companymstid);
	
}
