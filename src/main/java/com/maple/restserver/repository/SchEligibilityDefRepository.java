package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SchEligibilityDef;

@Repository
public interface SchEligibilityDefRepository extends JpaRepository<SchEligibilityDef,String> {

	
	SchEligibilityDef findByEligibilityNameAndCompanyMst(String eligname, CompanyMst companymst);
	SchEligibilityDef findByIdAndCompanyMst(String eligname, CompanyMst companymst);
	List<SchEligibilityDef> findByCompanyMst(CompanyMst companymst);
	
	
}
