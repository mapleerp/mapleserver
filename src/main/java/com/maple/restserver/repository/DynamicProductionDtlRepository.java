package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.DynamicProductionDtl;

@Component
@Repository
public interface DynamicProductionDtlRepository extends JpaRepository<DynamicProductionDtl,String>{

	List<DynamicProductionDtl> findByDynamicProductionHdrId(String dynamicproductionhdrid);

}
