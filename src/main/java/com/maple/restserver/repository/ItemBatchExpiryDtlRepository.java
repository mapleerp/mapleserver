package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchExpiryDtl;
@Component
@Repository
public interface ItemBatchExpiryDtlRepository extends JpaRepository<ItemBatchExpiryDtl,String> {

	@Query(nativeQuery = true,value="select im.item_name,"
			+ "bm.qty,ed.batch,"
			+ "ed.expiry_date,"
			+ "ed.updated_date "
			+ "from item_batch_expiry_dtl ed,"
			+ "item_batch_mst bm, item_mst im "
			+ "where bm.item_id=ed.item_id and "
			+ "im.id=ed.item_id"
			+ " and bm.company_mst=:companyMst"
			+ " and bm.branch_code=:branchcode "
			+ "order by ed.expiry_date ASC")
	List<Object>getShortExpiry(CompanyMst companyMst,String  branchcode);
	
	
	@Query(nativeQuery = true,value="select im.item_name,"
			+ "im.item_code,"
			+ "im.standard_price,"
			+ "im.standard_price * bm.qty,"
			+ "cat.category_name,"
			+ "bm.qty,ed.batch,"
			+ "ed.expiry_date,"
			+ "ed.updated_date "
			+ "from item_batch_expiry_dtl ed,"
			+ "item_batch_mst bm, item_mst im,category_mst cat  "
			+ "where bm.item_id=ed.item_id and "
			+ "im.id=ed.item_id and cat.id=im.category_id "
			+ " and bm.company_mst=:companyMst"
			+ " and bm.branch_code=:branchcode "
			+ "order by ed.expiry_date ASC")
	List<Object>getShortExpiryNew(CompanyMst companyMst,String  branchcode);
	

	@Query(nativeQuery = true,value="select im.item_name,"
			+ "bm.qty,ed.batch,"
			+ "ed.expiry_date,"
			+ "ed.updated_date ,cm.category_name "
			+ "from item_batch_expiry_dtl ed,"
			+ "item_batch_mst bm, item_mst im,category_mst cm "
			+ "where bm.item_id=ed.item_id and im.id=ed.item_id "
			+ "and im.category_id=cm.id and cm.id=:categoryId "
			+ "and bm.company_mst=:companyMst and bm.branch_code=:branchCode "
			
			)
	List<Object>getCatogarywiseShortExpiry(CompanyMst companyMst,String branchCode,String categoryId);

	@Query(nativeQuery = true,value="select im.item_name,"
			+ "bm.qty,ed.batch,"
			+ "ed.expiry_date,"
			+ "ed.updated_date "
			+ "from item_batch_expiry_dtl ed,"
			+ "item_batch_mst bm, item_mst im "
			+ "where bm.item_id=ed.item_id and "
			+ "im.id=ed.item_id"
			+ " and bm.company_mst=:companyMst"
			+ " and bm.branch_code=:branchcode and im.id=:itemid "
			+ " order by ed.expiry_date ASC")
	
	List<Object> getItemWiseShortExpiry(CompanyMst companyMst,String branchcode,String itemid);
	

	List<ItemBatchExpiryDtl> findByCompanyMstAndItemIdAndBatch(CompanyMst companyMst,String itemId,String batch);
	 ItemBatchExpiryDtl findByCompanyMstAndItemIdAndBatchAndExpiryDate(CompanyMst companyMst,String itemId,String batch,Date expDate);
	 
	 
	 @Modifying
		@Query(nativeQuery = true, value ="update item_batch_expiry_dtl p set p.expiry_date=:expDate where p.item_id=:itemid and p.batch =:batch")
	     void UpdateItemBatchExpiryDtl(String itemid,Date expDate,String batch);


	 @Query(nativeQuery = true,value = "select i.item_name,ib.batch "
	 		+ "from item_batch_expiry_dtl ib,"
	 		+ "item_mst i "
	 		+ "where i.id = ib.item_id and lower(i.item_name) like :data")
	List<Object> searchItemBatchExpDtlByItemName(String data);
	 
	 @Query(nativeQuery = true,value = "select i.item_name,ib.batch from item_batch_expiry_dtl ib,"
	 		+ "item_mst i "
	 		+ "where i.id = ib.item_id and lower(ib.batch) like :batch")
	 List<Object> searchItemBatchExpDtlByBatch(String batch);

//	 @Query("select "
//				+ "c.category_name,i.item_name,i.item_code,b.batch,b.amount,td.exp_date,sum(td.qty_in-td.qty_out)as quantity"
//				+ "from item_batch_dtl td,item_mst i,batch_price_definition b, category_mst c "
//				+ "where i.id =td.item_id and td.voucher_date=:date and b.batch = td.batch and i.category_id=c.id"
//				+ "group by c.category_name,i.item_name,i.item_code,b.batch,b.amount,td.exp_date")
//	List<Object> findAMDCShortExpiryReport(Date date);
		
}
