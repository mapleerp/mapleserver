package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.PurchaseDtl;

@Repository
public interface PurchaseDtlRepository extends JpaRepository<PurchaseDtl, String> {
	
	
 
    Optional<PurchaseDtl> findByIdAndId(String id, String purchaseHdrId);
    
    
    @Query(nativeQuery = true, value = "SELECT *  FROM purchase_dtl d where purchase_hdr_id =:purchaseHdrId order by item_serial  ") 
    List<PurchaseDtl> findByPurchaseHdrId(String purchaseHdrId);
    
    @Query(nativeQuery = true, value = "SELECT *  FROM purchase_dtl d where purchase_hdr_id =:purchaseHdrId order by item_serial  "
    		)
    List<PurchaseDtl> findByPurchaseHdrIdOrderBySerial(String purchaseHdrId);
    
    List<PurchaseDtl> findByPurchaseHdrVoucherDate(Date voucherDate);

    @Query(nativeQuery = true, value = "SELECT SUM(d.amount) as totalAmount, "
    		+ " SUM(d.qty) as totalQty,SUM(d.discount) as totalDiscount,"
    		+ " SUM(d.tax_Amt) as totalTax,SUM(d.cess_Amt) as totalCessAmt    "
    		+ " FROM purchase_dtl d, purchase_hdr h "
    		+ " where h.id = d.purchase_hdr_id AND h.id =:purchaseHdrId  ")
    List<Object> findSumPurchaseDetail(@Param("purchaseHdrId")String PurchaseHdrId);
    
    
    @Query(nativeQuery = true, value = "SELECT SUM(d.amount) as totalAmount, "
    		+ " SUM(d.qty) as totalQty,SUM(d.discount) as totalDiscount,"
    		+ " SUM(d.tax_Amt) as totalTax,SUM(d.cess_Amt) as totalCessAmt , tax_rate as taxRate  "
    		+ " FROM purchase_dtl d, purchase_hdr h "
    		+ " where h.id = d.purchase_hdr_id AND h.id =:purchaseHdrId group by tax_rate ")
    List<Object> findSumPurchaseDetailTaxwise(@Param("purchaseHdrId")String PurchaseHdrId);
    
    
 //   @Query("delete from purchase_dtl d where d.id=:purchaseDtlId")
  
    @Query(nativeQuery = true, value =" select sum((d.purchse_rate*qty) * (d.tax_rate)/100 ) as totalQty,d.tax_rate"
    		+ " from purchase_dtl d,purchase_hdr h  where h.id =d.purchase_hdr_id and h.voucher_number =:vouchernumber and "
    		+ " h.branch_code=:branchcode and date(h.voucher_date)=:date group by d.tax_rate "

    		)
    List<Object> findTaxwiseSumPurchaseDtl(String vouchernumber,String  branchcode, Date date);
    
    
      
    
    @Query(nativeQuery = true, value ="select c.company_name,b.branch_name,b.branch_adress1,b.branch_adress2,b.branch_state,b.branch_gst,b.branch_email,b.branch_website,b.branch_tel_no,pd.debit_amount,pd.credit_amount,pd.closing_balance,startDate,endDate "
    		+ "s.account_name,s.company_name,s.customer_contact,s.party_mail i.item_name"
    		+ " from purchase_hdr h,purchase_dtl d,branch_mst b,item_mst i,payable_dtl pd,"
    		+ "company_mst c, account_heads s where h.id=d.purchase_hdr_id "
    		+ " and s.id=h.supplier_id and h.company_mst=c.id "
    		+ " and b.branch_code=h.branch_code and i.id=d.item_id "
    		+ " and pd.account_heads_id=s.id and date(h.voucher_date) between :startDate and:endDate and h.branch_code:branchcode and h.voucher_number=:vouchernumber ")
    List<Object>   retrievePurchaseReport( String vouchernumber,String branchcode,String startDate,String endDate);
  
    
    @Modifying(flushAutomatically = true)
    @Query(nativeQuery = true, value ="update purchase_dtl ud set unit_id =(select im.unit_id from item_mst im where im.id=ud.item_id and im.branch_code =:branchcode) where unit_id is null")
    void	updatePuchase_Dtl(String branchcode );
    
    @Query(nativeQuery = true,value= "select i.hsn_code,sum(d.qty),sum(d.tax_amt),"
    		+ "sum(amount),h.voucher_date from  item_mst i,purchase_hdr h,purchase_dtl d where h.id=d.purchase_hdr_id and d.item_id =i.id and h.branch_code=:branchCode"
    		+ " and h.voucher_date between :fdate and :tdate group by i.hsn_code,h.voucher_date order by h.voucher_date")
	List<Object> getHSNCodeWiseSalesDtl(Date fdate, Date tdate, String branchCode);

    @Modifying
    @Query(nativeQuery = true,value = "update purchase_dtl set barcode =:barCode where item_id=:itemid")
    void updateBarcode(String itemid, String barCode);
    
    
    @Query(nativeQuery = true,value= "select i.item_name,sum(d.qty),d.purchse_rate,h.voucher_date,h.voucher_number "
    		+ "from item_mst i,purchase_hdr h,purchase_dtl d where i.id=:itemid and d.item_id =i.id and "
    		+ "h.branch_code=:branchcode and h.voucher_number is not null and h.company_mst=:companymstid "
    		+ "and d.purchase_hdr_id=h.id "
    		+ "group by i.item_name,h.voucher_date,h.voucher_number,d.purchse_rate  ORDER BY h.voucher_date DESC ")
	List<Object> getItemStockReport(String companymstid, String branchcode, String itemid);
//	List<PurchaseDtl> findByVoucherNumberAndVoucherDate(String voucherNumber, Date voucherDate);


    @Query(nativeQuery = true,value = "select h.voucher_date,i.item_name,h.branch_code,sum(d.qty),"
			+ "c.account_name,sum(d.qty*d.mrp),d.batch "
			+ "from account_heads c,purchase_hdr h,purchase_dtl d,item_mst i"
			+ " where c.id=h.supplier_id and"
			+ " h.id=d.purchase_hdr_id and "
			+ "i.id=d.item_id and"
			+ " date(h.voucher_date)>=:fromDate "
			+ "and date(h.voucher_date)<=:toDate "
			+ "and h.branch_code=:branch "
			+ " group by i.item_name,c.account_name,d.batch;")
	List<Object> getPurchaseConsolidatedReport(Date fromDate, Date toDate, String branch);
    
    
	@Query(nativeQuery = true, value = "SELECT   " + " SUM((purchse_rate * qty))  as totalAmount "
			+ " FROM purchase_dtl d, purchase_hdr  h " + " where h.id = d.purchase_hdr_id AND h.id = :purchaseHdrId AND"
			+ " d.item_id = :itemId AND d.id = :dtlID ")
	Double findSumAcessibleAmountItemWise(String purchaseHdrId, String itemId, String dtlID);

}
