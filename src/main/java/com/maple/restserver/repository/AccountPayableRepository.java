package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AccountPayable;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseHdr;


import com.maple.restserver.entity.AccountReceivable;
 
 
import com.maple.restserver.entity.PurchaseHdr;
 
import com.maple.restserver.entity.CompanyMst;
 
 

import com.maple.restserver.entity.PurchaseHdr;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseHdr;

 
@Component
@Repository
public interface AccountPayableRepository extends JpaRepository<AccountPayable, String>{
	
	List<AccountPayable> findByCompanyMstId(String companymstid);
	AccountPayable findByCompanyMstAndVoucherNumber(CompanyMst companymstid,String voucherNumber);
	
	 @Query(nativeQuery=true,value="select * from account_payable ap where ap.company_mst=:companymstid and ap.account_id=:accountId and ap.due_amount-ap.paid_amount>0")
	List<AccountPayable> findByAccountId(CompanyMst companymstid,String accountId);
	
	 @Query(nativeQuery=true,value="select * from account_payable ap where ap.company_mst=:companymstid and ap.account_id=:accountId ")
	 List<AccountPayable> 	 findByCompanyMstAndAccountId(String companymstid,String accountId);
	 
	 AccountPayable findByCompanyMstAndPurchaseHdr(CompanyMst companyMst,PurchaseHdr purchaseHdr);

	 void deleteByVoucherNumber(String voucherNumber);

		 AccountPayable  findByCompanyMstIdAndPurchaseHdr(String companymstid, PurchaseHdr purchaseHdr);

		 AccountPayable findByCompanyMstAndId(CompanyMst companyMst,String id);
		 
		Optional<AccountPayable> findByVoucherNumberAndVoucherDate(String voucherNumber, Date voucherDate);
		
		void deleteByVoucherNumberAndVoucherDate(String voucherNumber, Date voucherDate);
		
		@Query(nativeQuery = true,value="select * from account_payable where account_id=:accountid and "
				+ " (due_amount-paid_amount) > 0 and voucher_number is not null order by voucher_number ASC")
		List<AccountPayable> getAccountPayableBySupId(String accountid);
		void deleteByPurchaseHdr(PurchaseHdr purchaseHdr);
		
		@Modifying
		@Query(nativeQuery = true,value="update account_payable  set account_id =:toAccountId "
				+ "where account_id =:fromAccountId ")
		void accountMerge(String fromAccountId, String toAccountId);
		
		 
}
