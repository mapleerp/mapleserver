package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.StockTransferInExceptionDtl;

@Repository
public interface StockTransferInExceptionDtlRepository  extends JpaRepository<StockTransferInExceptionDtl, String>{

	
	
	
	@Query(nativeQuery = true ,value="select i.item_name,ed.batch,ed.qty,ed.barcode,ed.rate,ed.item_code,ed.reason,u.unit_name , ed.amount from stock_transfer_in_exception_dtl ed ,item_mst i,unit_mst u where ed.item_id i.id and u.id=ed.unit_id and ed.voucher_number=:voucherNumber and date(voucher_date) =:voucherDate")
	List<Object> stockTransferInExceptionReport(String voucherNumber,Date voucherDate);



	
	
}
