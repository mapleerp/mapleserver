
package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PatientMst;




public interface PatientMstRepository extends JpaRepository<PatientMst,String>{

	PatientMst findByCompanyMstIdAndId(String companymstid, String patientmstid);

	PatientMst findByCompanyMstIdAndPatientName(String companymstid, String patientname);
	
	
	
	@Query("SELECT p FROM PatientMst p WHERE LOWER(p.patientName) like   LOWER(:lastName) AND p.companyMst.id=:companymstid ")
	List<PatientMst> searchLikePatientByName(@Param("lastName") String lastName,String companymstid);
	@Query(nativeQuery = true,value="SELECT * FROM patient_mst WHERE company_mst=:companyMst and customer_mst=:customerid ")
	List<PatientMst> findByNewPatientWithCustomer(CompanyMst companyMst, String customerid);


//	List<PatientMst> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
