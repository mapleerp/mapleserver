package com.maple.restserver.repository;

import java.util.List;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseOrderDtl;
@Repository
public interface PurchaseOrderDtlRepository  extends JpaRepository<PurchaseOrderDtl, String>{

	

	//version1.7
	@Query(nativeQuery = true, value="select pd.* from purchase_order_dtl pd,purchase_order_hdr ph "
			+ "where pd.purchase_order_hdr_id=ph.id and ph.voucher_date=:voucherDate and "
			+ "ph.voucher_number = :voucherNo and pd.status='OPEN'")
	List<PurchaseOrderDtl> findByVoucherDateVoucherNoAndStatus(Date voucherDate,String voucherNo);
	
	//version1.7ends
	
	
	List<PurchaseOrderDtl> findByPurchaseOrderHdrId(String purchaseOrderHdrId);
	@Query(nativeQuery = true, value = "SELECT pd.* from purchase_order_dtl pd,purchase_order_hdr ph where pd.purchase_order_hdr_id=ph.id and ph.voucher_number =:voucherNumber and pd.status='OPEN'")
	List<PurchaseOrderDtl> findByPurchaseOrderHdrVoucherNumber(String voucherNumber);
	@Query(nativeQuery = true, value = "SELECT SUM(amount) as totalAmount, "
    		+ " SUM(qty) as totalQty , SUM(tax_Amt) as totalTax "
    		+ " FROM  purchase_order_dtl d, purchase_order_hdr h "
    		+ " "

    		+ " where h.id = d.purchase_order_hdr_id AND h.id =:purchaseorderHdrId")

	List<Object>findSumPurchaseOrderDetail(String purchaseorderHdrId);
}
