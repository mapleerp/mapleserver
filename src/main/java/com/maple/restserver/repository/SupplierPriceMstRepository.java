package com.maple.restserver.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
 
import com.maple.restserver.entity.SupplierPriceMst;
@Repository
public interface SupplierPriceMstRepository extends JpaRepository<SupplierPriceMst, String> {
	
	

	List<SupplierPriceMst> findByCompanyMst(CompanyMst companyMst);
	/*
	 * 
	 */
	@Query(nativeQuery = true,value="select s.account_name, i.item_name,sp.dp,sp.updated_date,sp.id " + 
			"			 from supplier_price_mst sp,account_heads s,item_mst i where sp.supplier_id=s.id and sp.item_id=i.id " + 
			"			 and sp.company_mst=:companyMst")
	ArrayList<Object> findallSupplierPriceMst(String companyMst);

	}

/*
select s.supplier_name, i.item_name,sp.dp,sp.updated_date,sp.id  from supplier_price_mst sp,supplier s,item_mst i where sp.supplier_id=s.id and sp.item_id=i.id 
  
*/