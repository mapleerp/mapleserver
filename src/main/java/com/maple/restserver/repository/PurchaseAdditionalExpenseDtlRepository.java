package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.PurchaseAdditionalExpenseDtl;

@Repository
@Controller
public interface PurchaseAdditionalExpenseDtlRepository extends JpaRepository<PurchaseAdditionalExpenseDtl,String>{

	List<PurchaseAdditionalExpenseDtl> findByPurchaseAdditionalExpenseHdrId(String hdrid);

	@Query(nativeQuery = true,value ="select * from purchase_additional_expense_dtl where "
			+ "purchase_additional_expense_hdr=:hdrid and calculated='NO'")
	List<PurchaseAdditionalExpenseDtl> findByPurchaseAdditionalExpenseHdrIdNotCalculated(String hdrid);

}
