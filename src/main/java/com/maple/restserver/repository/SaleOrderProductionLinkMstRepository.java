package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SaleOrderProductionLinkMst;

@Repository
public interface SaleOrderProductionLinkMstRepository extends JpaRepository<SaleOrderProductionLinkMst, String> {

}
