package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SchOfferAttrListDef;
import com.maple.restserver.entity.SchOfferDef;
import com.maple.restserver.entity.SchSelectAttrListDef;
import com.maple.restserver.entity.SchemeInstance;

@Repository
public interface SchSelectAttrListDefReoository extends JpaRepository<SchSelectAttrListDef,String>  {

	SchSelectAttrListDef findByAttribNameAndCompanyMst(String schoffeattname, CompanyMst companyMst);

	List<SchSelectAttrListDef> findBySelectionIdAndCompanyMst(String selctiondefid, CompanyMst companyMst);



}
