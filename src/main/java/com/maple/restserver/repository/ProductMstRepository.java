package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProductMst;
@Component
@Repository
public interface ProductMstRepository extends JpaRepository<ProductMst, String>{
	

	ProductMst findByCompanyMstAndProductName(CompanyMst companyMst, String productname);

	ProductMst findByCompanyMstAndId(CompanyMst companyMst, String productid);
	

	
	@Query(nativeQuery = true,value="select p.id, p.product_name from product_mst p where lower(p.product_name) LIKE  :searchItemName")
	public List<Object> findSearch(@Param("searchItemName") String searchItemName ,  Pageable pageable);
}
