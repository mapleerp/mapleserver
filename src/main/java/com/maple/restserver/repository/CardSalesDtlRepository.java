package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CardSalesDtl;

@Repository
public interface CardSalesDtlRepository  extends JpaRepository<CardSalesDtl, String>{

	
	
void	deleteBySalesReceiptId(String salesReceiptId);
}
