

package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ReceivableDtl;


@Repository
public interface  ReceivableDtlRepository extends JpaRepository<ReceivableDtl, Integer>{

	List<ReceivableDtl>findByReceivableHdrId(String receivableHdrId);
	
	//List<ReceivableDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
