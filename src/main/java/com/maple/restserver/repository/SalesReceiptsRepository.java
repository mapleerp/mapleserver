package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReceiptModeMst;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.report.entity.ReceiptModeReport;

@Repository
public interface SalesReceiptsRepository extends JpaRepository<SalesReceipts, String> {
//	@Query(nativeQuery = true, value = "select sh.voucher_number, sr.receipt_amount," + "c.customer_name from "
//			+ "sales_trans_hdr sh,sales_receipts sr,customer_mst c where sh.id=sr.sales_trans_hdr_id and "
//			+ "c.id=sh.customer_mst and " + "sh.branch_code =:branchcode and date(sh.voucher_date)=:vdate "
//			+ "and sr.account_id=:acctid")
	
	@Query(nativeQuery = true, value = "select sh.voucher_number, sr.receipt_amount," + "c.account_name from "
			+ "sales_trans_hdr sh,sales_receipts sr,account_heads c where sh.id=sr.sales_trans_hdr_id and "
			+ "c.id=sh.account_heads and " + "sh.branch_code =:branchcode and date(sh.voucher_date)=:vdate "
			+ "and sr.account_id=:acctid")
	List<Object> getSalesReceiptByAccIdAndDate(String branchcode, Date vdate, String acctid);

	@Query(nativeQuery = true, value = "select sum(sr.receipt_amount) as totalAmount from "
			+ "sales_receipts sr,sales_trans_hdr sh " + "where sh.id = sr.sales_trans_hdr_id and "
			+ "date(sh.voucher_date) = :vdate and sh.company_mst=:companyMst and "
			+ "sr.receipt_mode <> :mode and sr.receipt_mode <> 'CREDIT' and sr.branch_code=:branchCode")
	Double sumOfCardAmountBranch(Date vdate, String companyMst, String mode, String branchCode);

//	@Query(nativeQuery = true, value = "select sh.voucher_number, sr.receipt_amount," + "c.customer_name from "
//			+ "sales_trans_hdr sh,sales_receipts sr,customer_mst c where sh.id=sr.sales_trans_hdr_id and "
//			+ "c.id=sh.customer_mst and " + "sh.branch_code =:branchcode and date(sh.voucher_date)=:vdate "
//			+ "and sr.receipt_mode<> :mode and sr.receipt_mode <> 'CREDIT' ")
	
	@Query(nativeQuery = true, value = "select sh.voucher_number, sr.receipt_amount," + "c.account_name from "
			+ "sales_trans_hdr sh,sales_receipts sr,account_heads c where sh.id=sr.sales_trans_hdr_id and "
			+ "c.id=sh.account_heads and " + "sh.branch_code =:branchcode and date(sh.voucher_date)=:vdate "
			+ "and sr.receipt_mode<> :mode and sr.receipt_mode <> 'CREDIT' ")
	List<Object> getSalesReceiptByCard(String branchcode, Date vdate, String mode);

	@Query(nativeQuery = true, value = "select sum(sr.receipt_amount) as totalAmount from "
			+ " sales_receipts sr,sales_trans_hdr sh " + " where sh.id = sr.sales_trans_hdr_id and "
			+ " date(sh.voucher_date) = :vdate and sh.company_mst=:companyMst and "
			+ " sr.receipt_mode <> :mode and sr.receipt_mode <> 'CREDIT'")
	Double sumOfCardAmount(Date vdate, String companyMst, String mode);

//	@Query(nativeQuery = true, value = "select sh.voucher_number, sr.receipt_amount," + "c.customer_name from "
//			+ "sales_trans_hdr sh,sales_receipts sr,customer_mst c where sh.id=sr.sales_trans_hdr_id and "
//			+ "c.id=sh.customer_mst and " + "sh.branch_code =:branchcode and date(sh.voucher_date)=:vdate "
//			+ "and sr.receipt_mode=:mode")
	
	@Query(nativeQuery = true, value = "select sh.voucher_number, sr.receipt_amount," + "c.account_name from "
			+ "sales_trans_hdr sh,sales_receipts sr,account_heads c where sh.id=sr.sales_trans_hdr_id and "
			+ "c.id=sh.account_heads and " + "sh.branch_code =:branchcode and date(sh.voucher_date)=:vdate "
			+ "and sr.receipt_mode=:mode")
	List<Object> getSalesReceiptByModeAndDate(String branchcode, Date vdate, String mode);

	@Query(nativeQuery = true, value = "select r.receipt_mode , "
			+ " sum(receipt_amount) from sales_receipts r , sales_trans_hdr h " +

			"where r.sales_trans_hdr_id = h.id " +

			"and date(h.voucher_date) = :receiptDate " + "\n" + "group by r.receipt_mode")

	List<Object> getSalesReciptsDetails(Date receiptDate);

	@Query(nativeQuery = true, value = "select sr.receipt_mode,sum(sr.receipt_amount) as receipt_amount from sales_receipts sr ,"
			+ "sales_trans_hdr sh , receipt_mode_mst r "
			+ "where sh.id= sr.sales_trans_hdr_id and sh.branch_code = :branchcode and sh.company_mst = :companyMst "
			+ "and sh.voucher_number is not null and  "
			+ "date(sh.voucher_date) between :fdate and :tdate and sr.RECEIPT_MODE=r.RECEIPT_MODE group by sr.receipt_mode")
	List<Object> getReceiptModeBetweenDate(Date fdate, Date tdate, String companyMst, String branchcode);

	/*
	 * Date: April 7th 2021 Below query is written by Regy to get all receipt by
	 * receipt mode wise. Not required as per Sathyajith. He claimed that this is
	 * already done
	 * 
	 */

	@Query(nativeQuery = true, value = "select sr.receipt_mode,sum(sr.receipt_amount) as receipt_amount from sales_receipts sr ,"
			+ "sales_trans_hdr sh   "
			+ "where sh.id= sr.sales_trans_hdr_id and sh.branch_code = :branchcode and sh.company_mst = :companyMst "
			+ "and sh.voucher_number is not null and  "
			+ "date(sh.voucher_date) >= :fdate and date(sh.voucher_date) <= :tdate  group by sr.receipt_mode")
	List<Object> getSalesReceiptModeDateWise(Date fdate, Date tdate, String companyMst, String branchcode);

	/*
	 * Below query give summary of Card Receipts.
	 * 
	 * select sum(A.receipt_amount) from (select sum(sr.receipt_amount) as
	 * receipt_amount from sales_receipts sr , sales_trans_hdr sh , receipt_mode_mst
	 * rm where sh.id= sr.sales_trans_hdr_id and sh.branch_code = 'HCHO' and
	 * sh.company_mst = 'HOTCAKES' and sh.voucher_number is not null and
	 * date(sh.voucher_date) = '2021-03-28' and rm.credit_card_status = 'YES' union
	 * select sum(sr.receipt_amount) as receipt_amount from sale_order_receipt sr ,
	 * sales_order_trans_hdr sh , receipt_mode_mst rm where sh.id=
	 * sr.sale_order_trans_hdr_id and sh.branch_code = 'HCHO' and sh.company_mst =
	 * 'HOTCAKES' and sh.voucher_number is not null and date(sh.voucher_date) =
	 * '2021-03-28' and rm.credit_card_status = 'YES' ) A
	 */

	@Query(nativeQuery = true, value = "select sum(A.receipt_amount) from (select  sum(sr.receipt_amount) as receipt_amount from sales_receipts sr ,  "
			+ "	   sales_trans_hdr sh , receipt_mode_mst rm  "
			+ "	   where sh.id= sr.sales_trans_hdr_id and rm.receipt_mode = sr.receipt_mode and  sh.branch_code = :branchcode  and sh.company_mst = :companyMst   "
			+ "	   and sh.voucher_number is not null and   date(sh.voucher_date) = :fdate and  rm.credit_card_status = 'YES' "
			+ "  and sh.sale_order_hrd_id is null " + "	   union "
			+ "	   select  sum(sr.receipt_amount) as receipt_amount from sale_order_receipt sr ,  "
			+ "	   sales_order_trans_hdr sh , receipt_mode_mst rm  "
			+ "	   where sh.id= sr.sale_order_trans_hdr_id and rm.receipt_mode = sr.receipt_mode and  sh.branch_code = :branchcode  and sh.company_mst = :companyMst  "
			+ "	   and sh.voucher_number is not null and   date(sh.voucher_date) = :fdate and  rm.credit_card_status = 'YES' ) "
			+ "	   A ")
	Double getSalesReceiptModeDateWiseCard(Date fdate, String companyMst, String branchcode);

	// version4.1
	SalesReceipts findBySalesTransHdrIdAndReceiptModeAndReceiptAmount(String salesTransHdr, String receiptMode,
			Double receiptAmt);

	// version4.1 end

	List<SalesReceipts> findBySalesTransHdr(SalesTransHdr salesTransHdr);

	void deleteBySalesTransHdr(SalesTransHdr salesTransHdr);

	List<SalesReceipts> findByCompanyMst(String companymstid);

	List<SalesReceipts> findByCompanyMstIdAndSalesTransHdrId(String companymstid, String ssalestrandhdrid);

	@Query(nativeQuery = true, value = "  select sum(receipt_amount) as amount  from sales_receipts s where s.sales_trans_hdr_id=:salestrandhdrid")
	Double retrieveSalesReceiptsAmount(String salestrandhdrid);

	@Query(nativeQuery = true, value = "select s.voucher_number,s.receipt_mode,"
			+ "s.receipt_amount,sh.voucher_date from sales_receipts s,sales_trans_hdr sh "
			+ "where s.voucher_number=sh.voucher_number and date(sh.voucher_date) between :fudate and :tudate"
			+ " and s.receipt_mode=:receiptMode and s.company_mst=:companyMst and s.branch_code=:branchCode")
	List<Object> getSalesReceiptBetweenDate(Date fudate, Date tudate, String receiptMode, CompanyMst companyMst,
			String branchCode);

	@Modifying
	@Query(nativeQuery = true, value = "update sales_receipts  set account_id =:toAccountId "
			+ "where account_id =:fromAccountId ")
	void accountMerge(String fromAccountId, String toAccountId);

	List<SalesReceipts> findByAccountId(String id);

	@Query(nativeQuery = true, value = "select sr.* from sales_receipts sr,sales_trans_hdr h "
			+ "where sr.sales_trans_hdr_id=h.id and date(h.voucher_date)>=:fudate and date(h.voucher_date)<=:tudate and "
			+ "sr.account_id=:id")
	List<SalesReceipts> findByAccountIdAndDate(String id, Date fudate, Date tudate);

	@Query(nativeQuery = true, value = "select sr.receipt_mode,sum(sr.receipt_amount) as receipt_amount "
			+ " from sales_receipts sr ," + "sales_trans_hdr sh "
			+ "where sh.id= sr.sales_trans_hdr_id and sh.branch_code =:branchcode and "
			+ " sh.company_mst=:companymstid and sh.voucher_number is not null and  sh.user_id=:userid and "
			+ "date(sh.voucher_date) between :fdate and :tdate group by sr.receipt_mode")
	List<Object> UserWiseSalesReceiptsSummary(Date fdate, Date tdate, String companymstid, String branchcode,
			String userid);

	// ------------------new version surya 1.8
	@Query(nativeQuery = true, value = "select sum(sr.receipt_amount) as receipt_amount,sr.RECEIPT_MODE "
			+ "from sales_receipts sr , sales_trans_hdr sh , receipt_mode_mst r"
			+ " where sh.id= sr.sales_trans_hdr_id and sh.branch_code =:branchcode and "
			+ " sh.company_mst=:companyMst and sh.voucher_number is not null "
			+ " and date(sh.voucher_date)=:sdate  and " + " sr.RECEIPT_MODE = r.RECEIPT_MODE   "
			+ " and  r.CREDIT_CARD_STATUS='YES' and r.COMPANY_MST=:companyMst and sh.sale_order_hrd_id is null  group by sr.RECEIPT_MODE")
	List<Object> totalSalesCardAmount(CompanyMst companyMst, String branchcode, Date sdate);

	@Query(nativeQuery = true, value = "select sum(sr.receipt_amount) as receipt_amount, sr.RECEIPT_MODE "
			+ " from sales_receipts sr , sales_trans_hdr sh , receipt_mode_mst r , sales_order_trans_hdr so "
			+ "where sh.id=sr.sales_trans_hdr_id and sh.branch_code =:branchcode and "
			+ "sh.company_mst=:companyMst and sh.voucher_number is not null   and  "
			+ "sr.RECEIPT_MODE = r.RECEIPT_MODE    and  r.CREDIT_CARD_STATUS='YES' and sh.sale_order_hrd_id is not null  "
			+ "and date(sh.voucher_date)=:sdate and sh.sale_order_hrd_id=so.id and date(so.voucher_date)<:sdate"
			+ " group by sr.RECEIPT_MODE ")

	List<Object> cardPreviousReceiptRealized(CompanyMst companyMst, String branchcode, Date sdate);

	@Query(nativeQuery = true, value = "select sum(sr.receipt_amount) as receipt_amount, sr.RECEIPT_MODE "
			+ " from sales_receipts sr , sales_trans_hdr sh , receipt_mode_mst r , sales_order_trans_hdr so "
			+ "where sh.id=sr.sales_trans_hdr_id and sh.branch_code =:branchcode and "
			+ "sh.company_mst=:companyMst and sh.voucher_number is not null   and  "
			+ "sr.RECEIPT_MODE = r.RECEIPT_MODE    and  r.CREDIT_CARD_STATUS='YES' and sh.sale_order_hrd_id is not null  "
			+ "and date(sh.voucher_date)=:sdate and sh.sale_order_hrd_id=so.id and date(so.voucher_date)=:sdate and sr.SO_STATUS is null "
			+ " group by sr.RECEIPT_MODE ")
	List<Object> cardPreviousSoRealizedWithTodayCard(CompanyMst companyMst, String branchcode, Date sdate);

	// ------------------new version surya 1.8 end

	@Query(nativeQuery = true, value = "select sum(sor.receipt_amount) "
			+ "from sales_trans_hdr soh, sales_receipts sor  where  " + "soh.id = sor.sales_trans_hdr_id   and  "
			+ "date(soh.voucher_date) = :vdate and sor.receipt_mode = :mode ")
	Double getSalesrReceiptSummaryByDatendMode(Date vdate, String mode);

	@Query(nativeQuery = true, value = "select  sum(sr.receipt_amount) as receipt_amount   "
			+ "from sales_receipts sr , sales_trans_hdr sh , receipt_mode_mst rm where "
			+ "sh.id= sr.sales_trans_hdr_id and rm.receipt_mode = sr.receipt_mode "
			+ "and sh.voucher_number is not null "
			+ "and  sh.branch_code =:branchcode  and sh.company_mst =:companymstid and    date(sh.voucher_date) =:date and  "
			+ "rm.credit_card_status ='YES'  ")
	Double getCardTotalSalesReceipt(java.sql.Date date, String companymstid, String branchcode);

	@Query(nativeQuery = true, value = "select  sum(sr.receipt_amount) as receipt_amount   "
			+ "from sale_order_receipt sr , sales_order_trans_hdr sh , receipt_mode_mst rm where "
			+ "sh.id= sr.sale_order_trans_hdr_id and rm.receipt_mode = sr.receipt_mode "
			+ "and  sh.branch_code =:branchcode  and sh.company_mst =:companymstid and "
			+ "sh.voucher_number is not null and   date(sh.voucher_date) =:date and  "
			+ "rm.credit_card_status ='YES' ")
	Double getCardTotalSOReceipt(java.sql.Date date, String companymstid, String branchcode);

	@Query(nativeQuery = true, value = "select  sum(sr.receipt_amount) as receipt_amount " + " from "
			+ "sale_order_receipt sr ," + "sales_order_trans_hdr sh ," + "receipt_mode_mst rm ," + "sales_trans_hdr t "
			+ "where " + "sh.id= sr.sale_order_trans_hdr_id " + "and t.sale_order_hrd_id = sh.id "
			+ "and rm.receipt_mode = sr.receipt_mode " + "and  sh.branch_code =:branchcode  " + "and sh.company_mst =:companymstid "
			+ "and sh.voucher_number is not null " + "and   date(sh.voucher_date) <:date "
			+ "and   date(t.voucher_date) = :date  "
			+ "and  rm.credit_card_status ='YES' ")
	Double getCardTotalPreviousSalesReceipt(java.sql.Date date, String companymstid, String branchcode);
	
	
 
	
	List<SalesReceipts> findBySalesTransHdrIdAndReceiptMode(String salesTransHdr, String receiptMode);
	
	
	@Query(nativeQuery = true, value ="SELECT "
			+ "branch,totalcard,totalcredit,totalcash,totalInsurance,totalbank,SUM(totalcard+totalcredit+totalcash) AS granttotal FROM "
			+ "(SELECT "
			+ "br.branch_name AS branch,"
			+ "SUM(CASE WHEN   sr.receipt_mode='CREDIT' THEN sr.receipt_amount ELSE 0.0 END) AS totalcredit ,"
			+ "SUM(CASE WHEN   rmm.credit_card_status='NO' AND sr.receipt_mode='CASH' THEN sr.receipt_amount ELSE 0.0 END) AS totalcash,"
			+ "SUM(CASE WHEN rmm.credit_card_status='YES' AND rmm.receipt_mode='CASH' THEN sr.receipt_amount ELSE 0.0 END) AS totalcard,"
			+ "SUM(CASE WHEN sr.receipt_mode='INSURANCE' THEN sr.receipt_amount ELSE 0.0 END) AS totalInsurance,"
			+ "SUM(CASE WHEN sr.receipt_mode='BANK' THEN sr.receipt_amount ELSE 0.0 END) AS totalbank "
			+ "from sales_receipts sr, receipt_mode_mst rmm,branch_mst br,sales_trans_hdr sth "
			+ "WHERE sth.id=sr.sales_trans_hdr_id AND sr.receipt_mode=rmm.receipt_mode AND "
			+ "sth.branch_code=br.branch_code AND date(sth.voucher_date) = :cdate) DailySales")
	List<Object> findamdcDailySalesSummary(Date cdate);
	
	@Query(nativeQuery = true, value ="SELECT "
			+ "branch,totalcard,totalcredit,totalcash,totalInsurance,totalbank,SUM(totalcard+totalcredit+totalcash) AS granttotal FROM "
			+ "(SELECT "
			+ "br.branch_name AS branch,"
			+ "SUM(CASE WHEN   sr.receipt_mode='CREDIT' THEN sr.receipt_amount ELSE 0.0 END) AS totalcredit ,"
			+ "SUM(CASE WHEN   rmm.credit_card_status='NO' AND sr.receipt_mode='CASH' THEN sr.receipt_amount ELSE 0.0 END) AS totalcash,"
			+ "SUM(CASE WHEN rmm.credit_card_status='YES' AND rmm.receipt_mode='CASH' THEN sr.receipt_amount ELSE 0.0 END) AS totalcard,"
			+ "SUM(CASE WHEN sr.receipt_mode='INSURANCE' THEN sr.receipt_amount ELSE 0.0 END) AS totalInsurance,"
			+ "SUM(CASE WHEN sr.receipt_mode='BANK' THEN sr.receipt_amount ELSE 0.0 END) AS totalbank "
			+ "from sales_receipts sr, receipt_mode_mst rmm,branch_mst br,sales_trans_hdr sth "
			+ "WHERE sth.id=sr.sales_trans_hdr_id AND sr.receipt_mode=rmm.receipt_mode AND "
			+ "sth.branch_code=br.branch_code AND date(sth.voucher_date) = :cdate group by br.branch_name) DailySales group by branch,totalcard,totalcredit,totalcash,totalInsurance,totalbank")
	List<Object> findamdcDailySalesSummaryDerby(Date cdate);
	
	
	
	
	
	
	//---------------------------------------------pharmacy day closing report-----------------------------
	@Query(nativeQuery = true, value ="SELECT "
			+ "branch,totalcard,totalcredit,totalcash,totalInsurance,"
			+ "totalbank,SUM(totalcard+totalcredit+totalcash)"
			+ " AS granttotal, PhysicalCash ,"
			+ "(PhysicalCash - SUM(totalcard+totalcredit+totalcash)) as Difference  FROM "
			+ "(SELECT "
			+ "b.branch_name AS branch,"
			+ "SUM(CASE WHEN   r.receipt_mode='CREDIT' THEN r.receipt_amount ELSE 0.0 END) "
			+ "AS totalcredit ,"
			+ "SUM(CASE WHEN   rm.credit_card_status='NO' AND r.receipt_mode='CASH' "
			+ "THEN r.receipt_amount ELSE 0.0 END) AS totalcash,"
			+ "SUM(CASE WHEN rm.credit_card_status='YES' AND rm.receipt_mode='CASH' "
			+ "THEN r.receipt_amount ELSE 0.0 END) AS totalcard,"
			+ "SUM(CASE WHEN r.receipt_mode='INSURANCE' THEN r.receipt_amount ELSE 0.0 END) "
			+ "AS totalInsurance,"
			+ "SUM(CASE WHEN r.receipt_mode='BANK' THEN r.receipt_amount ELSE 0.0 END) "
			+ "AS totalbank ,"
			+ " ph.physical_cash AS PhysicalCash"
			+ " from sales_receipts r, receipt_mode_mst rm,branch_mst b,sales_trans_hdr st ,"
			+ "day_end_closure_hdr ph"
			+ " WHERE st.id=r.sales_trans_hdr_id AND r.receipt_mode=rm.receipt_mode AND date(st.voucher_date) = date(ph.voucher_date) AND "
			+ "st.branch_code=b.branch_code  AND ph.branch_code = b.branch_code AND date(st.voucher_date) = :date ) DailySales")
	List<Object> findPharmacyDayClosingReport(Date date);

	
	
	
	

}
