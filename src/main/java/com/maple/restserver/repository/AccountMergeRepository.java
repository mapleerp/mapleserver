package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.accounting.entity.AccountMergeMst;


public interface AccountMergeRepository extends JpaRepository<AccountMergeMst, String> {

	
	@Query(nativeQuery = true,value ="SELECT * FROM account_merge_mst WHERE  company_mst = :companymstid")
	List<AccountMergeMst> findAllByCompanyMst(String companymstid);
	
	
	@Query(nativeQuery = true,value ="SELECT * FROM account_merge_mst WHERE  company_mst = :companymstid and from_account_id= :fromAccountId"
			+ " and to_account_id= :toAccountId ")
AccountMergeMst getAccountMergeMstById(String companymstid, String fromAccountId, String toAccountId);

}
