package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AdditionalExpense;
import com.maple.restserver.entity.AppointmentDtl;

@Component
@Repository

public interface AppointmentDtlRepository extends JpaRepository<AppointmentDtl,Integer>{
//	Optional<AppointmentDtl> findById(String id);
//	List<AppointmentDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
