package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.RawMaterialIssueHdr;

public interface RawMaterialMstRepository  extends JpaRepository<RawMaterialIssueHdr, String>{

	List<RawMaterialIssueHdr> findByVoucherNumberAndVoucherDate(String voucherNo, Date voucherDate);
	
}
