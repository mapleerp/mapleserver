package com.maple.restserver.repository;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ItemMstHst;

@Repository
public interface ItemMstHstRepository extends JpaRepository<ItemMstHst, String> {

 
	ItemMstHst  findByBarCode(String barcode);
	  
	  
 
	  ItemMstHst  findByItemName(String itename);
	  
	Optional<ItemMstHst> findById(String id);
	
	
	  @Query(nativeQuery = true, value = " select c.category_name from "
	  		+ " category_mst c , item_mst_hst i where "
	  		+ " i.category_id = c.id and i.item_name = :itemName ")
	String getCategoryByItemName(String itemName);

	  
	  @Query(nativeQuery = true, value = " select c.category_name from "
		  		+ " category_mst c , item_mst_hst i where "
		  		+ " i.category_id = c.id and i.id = :itemId ")
		String getCategoryByItemId(String itemId);
	  
	  
	 
		@Modifying(flushAutomatically = true)
	    @Query(nativeQuery = true, value ="update item_mst_hst set branch_code =:branchcode where branch_code is null")
	    void	updateItemMst(String branchcode );
	    
	  
	List<ItemMstHst> findByCompanyMstAndCategoryId(CompanyMst companyId, String categoryId);



	Optional<ItemMstHst> findByItemNameAndCompanyMst(String itemname, CompanyMst companyMst);
	
//	List<ItemMst> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}

