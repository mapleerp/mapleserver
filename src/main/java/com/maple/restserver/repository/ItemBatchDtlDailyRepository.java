package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.ItemBatchDtlDaily;
@Component
@Repository
public interface ItemBatchDtlDailyRepository extends JpaRepository<ItemBatchDtlDaily, String>{

	
	@Modifying(flushAutomatically = true)
	@Query(nativeQuery=true,value="  delete from  item_batch_dtl_daily ")
	void updateDailyStockFromDtlStep0();

	
	@Modifying(flushAutomatically = true)
	@Query(nativeQuery=true,value="  update item_batch_dtl set branch_code = :branchcode where branch_code is null ")
	void updateDailyStockFromDtlStep1(String branchcode);


	@Modifying(flushAutomatically = true)
	@Query(nativeQuery=true,value="insert into item_batch_dtl_daily(id,barcode, batch,  item_id, qty_in, company_mst) " + 
			" select item_id||company_mst, barcode, 'NOBATCH',  item_id, 0 , company_mst from kit_definition_mst"
			+ " where item_id=:itemId and company_mst is not null" + 
			" group by item_id ,  barcode, branch_code,  company_mst   ")
	void updateItemBatchDtlDailyMstWithKit(String itemId);


	@Modifying(flushAutomatically = true)
	@Query(nativeQuery=true,value=" update item_batch_dtl_daily u "
			+ "set mrp = ( select standard_price from item_mst p where u.item_id = p.id ) ")
	void updateDailyStockFromDtlStep3();

	@Modifying
	@Query(nativeQuery = true,value = "update item_batch_dtl_daily set qty_in =0 where batch='NOBATCH' and qty_in <0")
	void correctNegativStock();

	@Query(nativeQuery=true,value=" select sum(qty_in-qty_out) as qty from item_batch_dtl_daily d "
			+ " where d.item_id=:itemId and d.barcode=:barcode and d.batch=:batch ")

	Double findQtyByItem(String itemId, String barcode, String batch);
	
	@Query(nativeQuery=true,value=" select sum(qty_in-qty_out) as qty from item_batch_dtl_daily d "
			+ " where d.item_id=:itemId and d.batch=:batch ")

	Double findQtyByItemAndBatch(String itemId,  String batch);
	@Modifying
	@Query(nativeQuery = true,value = "update item_batch_dtl_daily set barcode =:barCode where item_id=:itemid")
	void updateBarcode(String itemid, String barCode);


	@Modifying
	@Query(nativeQuery = true, value = "update item_batch_dtl_daily m set exp_date=( select "
			+ "e.expiry_date from item_batch_expiry_dtl e where e.batch=m.batch and e.item_id=m.item_id)")
	void updateExpiryDateFromItemBatchExpiryDtl();
	
	
	

}
