
package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.ClosingBalance;


@Component
@Repository
public interface ClosingBalanceRepository extends JpaRepository<ClosingBalance, Integer>{
	
	List<ClosingBalance>findByCompanyMstId(String companymstid);
//	List<ClosingBalance> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
