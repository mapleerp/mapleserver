package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.MixCatItemLinkMst;

public interface ResourceCatItemLinkRepository extends JpaRepository<MixCatItemLinkMst, String> {

	@Query(nativeQuery = true,value="select i.item_name,rc.mix_name from mix_cat_item_link_mst  rl,item_mst i,"
			+ "mix_category_mst rc where rl.item_id=i.id and rl.resource_cat_id=rc.id ")
	List<Object> 	retrieveResourceCatItemLinkMst();
	
	
	List<MixCatItemLinkMst>findByItemIdAndResourceCatId(String itemid,String resourcecatid);


List<MixCatItemLinkMst> findByItemId(String itemid);

@Query(nativeQuery = true,value="select * from  mix_cat_item_link_mst mc where mc.item_id=:itemid")
MixCatItemLinkMst  fetchByItemId(String itemid);


@Modifying
@Query(nativeQuery = true,value = "update  mix_cat_item_link_mst mc set mc.resource_cat_id =:subcatid where mc.item_id=:itemid " )
void updateMixCategoryMst(String itemid,String subcatid);
}
