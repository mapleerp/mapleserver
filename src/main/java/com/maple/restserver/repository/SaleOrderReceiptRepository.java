package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SaleOrderReceipt;
import com.maple.restserver.entity.SalesOrderReceipts;
import com.maple.restserver.entity.SalesOrderTransHdr;
@Component
@Transactional
@Repository
public interface SaleOrderReceiptRepository extends JpaRepository<SaleOrderReceipt, String>{
	
	//------version 4.7
	@Query(nativeQuery = true,value = "select sum(sor.receipt_amount) as totalamt from "
			+ "sale_order_receipt sor,sales_order_trans_hdr sh where receipt_mode <>:mode and "
			+ "sh.id=sor.sale_order_trans_hdr_id and sh.order_status='Orderd' and "
			+ " date(sor.rereceipt_date)=:vdate and sh.company_mst =:companymst and sor.BRANCH_CODE=:branchCode")
	Double getTotalCardSOBarnch(String mode,String companymst,java.sql.Date vdate, String branchCode);
	
	//------version 4.7 end
	@Query(nativeQuery = true,value = "select sum(sor.receipt_amount) as totalamt from "
			+ "sale_order_receipt sor,sales_order_trans_hdr sh where "
			+ " sor.receipt_mode in (select r.receipt_mode from  receipt_mode_mst r where r.CREDIT_CARD_STATUS='YES' and"
			+ " r.CREDIT_CARD_STATUS is not null) "
			+ " and "
			+ "sh.id=sor.sale_order_trans_hdr_id and sh.order_status='Orderd' and "
			+ " date(sh.VOUCHER_DATE)=:vdate and sh.company_mst =:companymst")
	Double getTotalCardSO(String companymst,java.sql.Date vdate);

//	@Query(nativeQuery = true,value = "select soh.voucher_number , sor.receipt_amount,c.customer_name "
//			+ "from sales_order_trans_hdr soh, sale_order_receipt sor,customer_mst c where "
//			+ "soh.id = sor.sale_order_trans_hdr_id and c.id=soh.customer_id and "
//			+ "date(sor.rereceipt_date) = :vdate and sor.receipt_mode =:mode and soh.order_status='Orderd'")
	
	@Query(nativeQuery = true,value = "select soh.voucher_number , sor.receipt_amount,c.account_name "
			+ "from sales_order_trans_hdr soh, sale_order_receipt sor,account_heads c where "
			+ "soh.id = sor.sale_order_trans_hdr_id and c.id=soh.customer_id and "
			+ "date(sor.rereceipt_date) = :vdate and sor.receipt_mode =:mode and soh.order_status='Orderd'")
	List<Object> getSaleOrderReceiptByDatendMode(Date vdate,String mode);
	
	
	@Query(nativeQuery = true,value = " select sum(sor.receipt_amount) "
			+ " from sales_order_trans_hdr soh, sale_order_receipt sor  where "
			+ " soh.id = sor.sale_order_trans_hdr_id   and "
			+ " date(sor.rereceipt_date) = :vdate and sor.receipt_mode =:mode ")
	Double  getSaleOrderReceiptSummaryByDatendMode(Date vdate,String mode);

	


	
	
//	@Query(nativeQuery = true,value = "select soh.voucher_number , sor.receipt_amount,c.customer_name "
//			+ "from sales_order_trans_hdr soh, sale_order_receipt sor,customer_mst c where "
//			+ "soh.id = sor.sale_order_trans_hdr_id and c.id=soh.customer_id and "
//			+ "date(sor.rereceipt_date) = :vdate and sor.receipt_mode <> 'CASH' and soh.order_status='Orderd'")
	
	@Query(nativeQuery = true,value = "select soh.voucher_number , sor.receipt_amount,c.account_name "
			+ "from sales_order_trans_hdr soh, sale_order_receipt sor,account_heads c where "
			+ "soh.id = sor.sale_order_trans_hdr_id and c.id=soh.customer_id and "
			+ "date(sor.rereceipt_date) = :vdate and sor.receipt_mode <> 'CASH' and soh.order_status='Orderd'")
	List<Object> getSaleOrderReceiptByDatendCard(Date vdate);
	
	

//@Query(nativeQuery = true,value="select h.voucher_number,sor.receipt_amount,c.customer_name "
//		+ " from sales_trans_hdr h, "
//		+ "sales_order_trans_hdr o,customer_mst c,"
//		+ " sale_order_receipt sor where h.sale_order_hrd_id = o.id "
//		+ "and o.id = sor.sale_order_trans_hdr_id "
//		+ "and c.id = h.customer_id "
//		+ "and o.order_status = 'Realized'"
//		+ " and sor.receipt_mode <>'CASH'"
//		+ "and  sor.receipt_mode <>'CREDIT' "
//		+ " and date(o.voucher_date) <:vdate  "
//		+ "and date(h.voucher_date) =:vdate ") 
	
	@Query(nativeQuery = true,value="select h.voucher_number,sor.receipt_amount,c.account_name "
			+ " from sales_trans_hdr h, "
			+ "sales_order_trans_hdr o,account_heads c,"
			+ " sale_order_receipt sor where h.sale_order_hrd_id = o.id "
			+ "and o.id = sor.sale_order_trans_hdr_id "
			+ "and c.id = h.customer_id "
			+ "and o.order_status = 'Realized'"
			+ " and sor.receipt_mode <>'CASH'"
			+ "and  sor.receipt_mode <>'CREDIT' "
			+ " and date(o.voucher_date) <:vdate  "
			+ "and date(h.voucher_date) =:vdate ") 
	
List<Object> getSaleOrderReceiptPreviousCard(Date vdate);
	
	
	
	
	List<SaleOrderReceipt> 	findByCompanyMstAndSalesOrderTransHdr(CompanyMst companymst,SalesOrderTransHdr salestrandhdr);


	  @Query(nativeQuery=true,value=" select sum(receipt_amount) as amount  from sale_order_receipt s where s.sale_order_trans_hdr_id=:salesordertrandhdrid")
		Double  retrieveSalesorderReceiptsAmount(String salesordertrandhdrid);

	  
	  List<SaleOrderReceipt> findBySalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr);
		//---------------------version 6.4 surya 

		List<SaleOrderReceipt> findByCompanyMstAndSalesOrderTransHdrAndReceiptMode(CompanyMst companyMst,
				SalesOrderTransHdr salesOrderTrandHdr, String receiptmode);
	//---------------------version 6.4 surya end

		SaleOrderReceipt findBySalesOrderTransHdrAndReceiptModeAndReceiptAmount(SalesOrderTransHdr salesOrderTransHdr,
				String receiptMode, Double receiptAmount);

		
		

//		  @Modifying
//			@Query(nativeQuery = true,value="update sale_order_receipt m, set m.account_id =:toAccountId "
//					+ "where d.account_id =:fromAccountId ")
//		void accountMerge(String fromAccountId, String toAccountId);



	  
	  /* Form SalesOrderReceipts - which was deleted on may 5 2020
	   *  @Query(nativeQuery=true,value="select r.receipt_mode , sum(receipt_amount) from sales_orer_receipts r , sales_order_trans_hdr h " + 
	  	  
	  		"where r.sales_order_trans_hdr_id = h.id " + 
	  		 
	  		"and date(h.voucher_date) = :receiptDate " + 
	  		"\n" +    		"group by r.receipt_mode")
	 
		List<Object> getSalesOrderReciptsDetails(Date receiptDate);	
		
	  List<SalesOrderReceipts> findBySalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr);

	   */
		
		//--------------- new version 1.8 surya 
		
		  @Query
		  (nativeQuery = true,value = "select sum(sr.receipt_amount) as receipt_amount, sr.RECEIPT_MODE "
		  		+ " from sale_order_receipt sr , sales_order_trans_hdr sh  , receipt_mode_mst rm "
		  		+ " where sh.id= sr.SALE_ORDER_TRANS_HDR_ID and sh.branch_code =:branchcode and "
		  		+ " sh.company_mst=:companyMst and sh.voucher_number is not null "
		  		+ " and date(sh.voucher_date)=:sdate and sr.RECEIPT_MODE = rm.RECEIPT_MODE   "
		  		+ " and rm.CREDIT_CARD_STATUS='YES'  group by sr.RECEIPT_MODE")
		List<Object> totalSalesOrderCardAmount(CompanyMst companyMst, String branchcode, Date sdate);
		
		//--------------- new version 1.8 surya  end


			@Query(nativeQuery = true,value = " select sum(sor.receipt_amount) "
					+ " from sales_order_trans_hdr soh, sale_order_receipt sor  where "
					+ " soh.id = sor.sale_order_trans_hdr_id   and "
					+ " date(sor.rereceipt_date) = :vdate ")
			Double  getTotalAdvanceForDate(Date vdate);

}
