package com.maple.restserver.repository;

import java.util.List;

import java.util.Optional;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SiteMst;
import com.maple.restserver.entity.StockTransferInHdr;


@Repository
public interface SiteMstRepository extends JpaRepository<SiteMst, String> {


	List<SiteMst> findByAccountHeadsAndCompanyMst(AccountHeads accountHeads, CompanyMst companyMst);


	List<SiteMst>	findByCompanyMst(CompanyMst companymst);

}
