package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SchOfferAttrInst;


@Repository
public interface SchOfferAttrInstRepository extends JpaRepository<SchOfferAttrInst,String>  {
	List<SchOfferAttrInst>	findByCompanyMst(CompanyMst companymst);

	SchOfferAttrInst findByCompanyMstAndAttributeNameAndSchemeId(CompanyMst companyMst, String attribname,
			String schemeid);

	List<SchOfferAttrInst> findByCompanyMstAndSchemeId(CompanyMst companyMst, String schemeid);

	List<SchOfferAttrInst> findByOfferId(String offerId);

	List<SchOfferAttrInst> findByCompanyMstAndSchemeIdAndOfferId(CompanyMst companyMst, String schemeid,
			String offerid);

	SchOfferAttrInst findByCompanyMstAndAttributeNameAndSchemeIdAndOfferId(CompanyMst companyMst, String attribname,
			String schemeid, String offerid);

	List<SchOfferAttrInst> findBySchemeIdAndOfferId(String id, String offerId);
}
