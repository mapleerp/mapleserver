package com.maple.restserver.repository;

import java.util.List;

import com.maple.restserver.entity.ItemBatchMst;

public interface ItemBatchMstRepositoryCustom {
	 List<ItemBatchMst> findItemBatchMstByItemIdAndQty(String itemId, Double qty);
 
	// List<ItemBatchMst> findItemNameByBarCodeAndQty(String barCode,String itemName, Double qty);
 
}
