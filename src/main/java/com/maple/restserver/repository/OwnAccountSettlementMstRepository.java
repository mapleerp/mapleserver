package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.OwnAccountSettlementMst;
@RestController
public interface OwnAccountSettlementMstRepository extends JpaRepository<OwnAccountSettlementMst, String> {

}
