package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ServiceItemMst;

@Repository
public interface ServiceItemMstRepository extends JpaRepository<ServiceItemMst,String>{

	
	ServiceItemMst findByCompanyMstAndId(CompanyMst companyMst,String hdrid);

	List<ServiceItemMst> findByCompanyMst(CompanyMst companyMst);
	
	
	@Query(nativeQuery = true,value="select l.item_name, l.id, l.standard_price,"
			+ " l.category_id, l.unit_id"
			+ " from  item_mst l "
			+ " where "

			+ " (lower(l.item_name) like :searchLocationName) and l.company_mst =:companyMst and l.service_or_goods='SERVICE ITEM'")

	List<Object> searchByLocationWithCompany(String searchLocationName, Pageable topFifty, CompanyMst companyMst);

	ServiceItemMst findByCompanyMstAndItemName(CompanyMst companyMst, String itemname);
}
