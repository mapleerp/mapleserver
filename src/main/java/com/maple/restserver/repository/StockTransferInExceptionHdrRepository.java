package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.StockTransferInExceptionHdr;
@Repository
public interface StockTransferInExceptionHdrRepository extends JpaRepository<StockTransferInExceptionHdr, String> {

	
	@Query(nativeQuery = true,value=("select * from stock_transfer_in_exception_hdr h where date(h.voucher_date) >= :fromDate and date(h.voucher_date) <= :toDate and h.company_mst=:companyMst and h.branch_code=:branchCode"))  
	List<StockTransferInExceptionHdr> stockTransferExceptionReport(Date fromDate , Date toDate,String branchCode ,String companyMst);

}
