package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maple.restserver.entity.ItemBranchDtl;

public interface ItemBranchDtlRepository extends JpaRepository<ItemBranchDtl, String>{

	
	List<ItemBranchDtl> findByCompanyMstId(String companymstid);
	
	List<ItemBranchDtl> findByItemBranchHdrId(String itembranchhdrid );
	
}
