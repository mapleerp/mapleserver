package com.maple.restserver.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.BarcodeFormatMst;
@Repository
@Transactional
public interface BarcodeFormatMstRepository extends JpaRepository<BarcodeFormatMst,String>{

	List<BarcodeFormatMst> findByBarcodeFormatName(String formatname);

}
