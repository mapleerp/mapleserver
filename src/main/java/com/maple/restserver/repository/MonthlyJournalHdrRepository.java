package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.MonthlyJournalHdr;
@Repository
@Component
public interface MonthlyJournalHdrRepository extends JpaRepository<MonthlyJournalHdr,String>{

	MonthlyJournalHdr	findByVoucherNumber(String hdrVoucherNumber);
}
