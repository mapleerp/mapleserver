package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReportSqlHdr;


@Component
@Repository
public interface ReportSqlHdrRepository extends JpaRepository<ReportSqlHdr,String>{

	List<ReportSqlHdr> findByCompanyMstId(String companymstid);

	List<ReportSqlHdr> findByCompanyMst(CompanyMst companyMst);

	ReportSqlHdr findByCompanyMstIdAndId(String companymstid, String reportsqlhdrid);

	List<ReportSqlHdr> findByCompanyMstIdAndReportName(String companymstid, String reportsqlhdrname);
	
	@Query(nativeQuery = true, value = "select * from report_sql_hdr "
			+ "where id in (select u.report_id from user_report_permission u "
			+ "where u.user_id=:userid and company_mst=:companymstid)")
	List<ReportSqlHdr> findByReportSqlHdrByCompanyAndUser(String companymstid, String userid);
	



}
