package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.LoyaltyDtl;

@Repository
public interface LoyaltyDtlRepository extends JpaRepository<LoyaltyDtl,String>{

}
