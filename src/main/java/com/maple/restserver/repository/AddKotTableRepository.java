package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
 
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AddKotTable;
@Component
@Repository

public interface AddKotTableRepository extends JpaRepository<AddKotTable, String>{
	
	
	List<AddKotTable>findByCompanyMstId(String companyMstid);
//	List<AcceptStock> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
 
   AddKotTable findByCompanyMstIdAndTableName(String companyMstid,String tableName);
   
   
   
   
	 @Query(nativeQuery=true,value="select * from add_kot_table a where a.status ='ACTIVE' and a.company_mst=:companymstid") 
		List<AddKotTable> getKotTablebyStatus(String companymstid);
}
