package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PDCPayment;
import com.maple.restserver.entity.PDCReceipts;

public interface PDCReceiptRepository extends JpaRepository<PDCReceipts, String>{

	

	List<PDCReceipts> findByTransDateAndCompanyMstAndBranchCode(Date date, CompanyMst companyMst, String branchcode);

	List<PDCReceipts> findByStatusAndDepositBankAndCompanyMst(String string, String bankname, CompanyMst companyMst);

//	@Query( "SELECT * FROM PDCReceipts WHERE "
//			+ "account=:accountid and depositBank=:bankname and transDate=:trasDate "
//			+ "and status='PENDING'")
//	List<PDCReceipts> getPDCReceiptByAccountAndBankAndTransDate(String accountid, String bankname,
//			Date trasDate);
//	
	List<PDCReceipts> findByStatusAndTransDateAndDepositBankAndAccount(String status,Date tdate,String bank,String accid);

	List<PDCReceipts> findByAccountAndStatus(String accountid, String status);
}
