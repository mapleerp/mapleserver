package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.MessageObjectMst;
@Repository
public interface MessageObjectMstRepository extends JpaRepository<MessageObjectMst, String>{

	
	@Query(nativeQuery = true,value="SELECT * FROM message_object_mst "
			+ "WHERE vouvcher_number = :string AND voucher_date = :date")
	List<MessageObjectMst> findByVoucherNumberAndDate(String string, Date date);
	
	
	

}
