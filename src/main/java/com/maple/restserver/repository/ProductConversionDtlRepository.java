package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ProductConversionDtl;

@Repository
public interface ProductConversionDtlRepository extends JpaRepository<ProductConversionDtl, String>{

	 List<ProductConversionDtl> findByProductConversionMstId(String hdrId);
	 
	 List<ProductConversionDtl> findByIdAndBatchCode(String hdrId,String batchCode);
	 
	 
		@Query(nativeQuery =true,value ="select m.conversion_Date from Product_Conversion_Mst m")
	List<Object> getproductconversionreport();  
	
	 
	   @Query(nativeQuery = true,value="select sum(d.from_qty) from product_conversion_dtl d, product_conversion_mst h where d.product_conversion_mst_id = h.id and d.from_item =:itemId and h.id=:hdrId and d.batch_code=:batch")
	    Double getProductConversionDetailItemQty(String hdrId,String itemId,String batch);
}
