package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SalesPropertiesConfigMst;
import com.maple.restserver.entity.SalesPropertyTransHdr;

@Repository
public interface SalesPropertyTransHdrRepository extends JpaRepository<SalesPropertyTransHdr,String> {

	List<SalesPropertyTransHdr> findBySalesTransHdrId(String hdrid);

	void deleteBySalesTransHdrId(String id);

}
