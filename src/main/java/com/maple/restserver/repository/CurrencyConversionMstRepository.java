package com.maple.restserver.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CurrencyConversionMst;

@Repository
@Transactional
public interface CurrencyConversionMstRepository extends JpaRepository<CurrencyConversionMst,String> {

	CurrencyConversionMst findByToCurrencyIdAndFromCurrencyId(String currencyId,String fromCurrencyId);

		@Query(nativeQuery = true,value ="select cm.* from currency_conversion_mst cm,"
		+ "currency_mst c where c.id = cm.currency_id and c.currency_name=:currencyname")
	CurrencyConversionMst getCurrencyConversionByCurrencyName(String currencyname);

	List<CurrencyConversionMst> findByCompanyMstId(String companymstid);

	CurrencyConversionMst findByToCurrencyId(String currencyid);

}
