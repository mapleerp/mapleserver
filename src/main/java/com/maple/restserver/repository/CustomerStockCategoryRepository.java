package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CustomerStockCategory;

@Repository
public interface CustomerStockCategoryRepository extends JpaRepository<CustomerStockCategory, String>{

	
	 @Query(nativeQuery=true,value="select csg.customer_name,csg.category_name,csg.status from customer_stock_category csg where csg.company_mst=:companymstid")
	List<Object>fetchCustomerNameAndCategoryName(String companymstid); 

	 List<CustomerStockCategory> findByCompanyMst(CompanyMst companyMst);
	 

	 
	 
	 List<CustomerStockCategory> findByAccountHeadsCustomerContact(String custphoneno);
	 
	
	/*
	 * String categoryName; String itemName; Double qty; Double standardPrice;
	 */
	 
	 @Query(nativeQuery=true,value="select  i.item_name,c.category_name,ib.qty,i.standard_price"
	 		+ "  from item_batch_mst ib,category_mst c,item_mst i "
	 		+ " where i.id=ib.item_id and c.id=i.category_id  "
	 		+ " and ib.company_mst =:companymstid and i.category_id=:catId ")
	 
	 List<Object> fetchCustomerCategorywiseStockReport(String catId,CompanyMst companymstid);

	 @Query(nativeQuery=true,value="select  i.item_name,c.category_name,ib.qty,i.standard_price"
		 		+ "  from item_batch_mst ib,category_mst c,item_mst i "
		 		+ " where i.id=ib.item_id and c.id=i.category_id  "
		 		+ " and ib.company_mst =:companymstid and i.category_id=:categoryid and i.id=:itemid")
	 List<Object> findByCustomerMstCustomerContactByItem(String categoryid, String companymstid,
			String itemid);
}
