package com.maple.restserver.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndClosureDtl;
import com.maple.restserver.entity.DayEndClosureHdr;
@Component
@Repository
public interface DayEndClosureRepository extends JpaRepository<DayEndClosureHdr, String>{

	List<DayEndClosureHdr> findByProcessDateAndCompanyMstAndBranchCode(Date date, CompanyMst companyMst, String branchCode);

	

    @Query(nativeQuery=true,value= "SELECT  sum(A.cashSale) as CASH , sum(A.card) AS CARD  , "
    		+ " sum(a.temp)  as CARD2   FROM "
    		+ " "
    		+ ""
    		+ " ( select sum(h.cash_pay) as cashSale ,"
    		+ " sum(h.cardamount) as card  , 0 as temp \n" + 
    		" FROM " + 
    		"  SALES_TRANS_HDR  h  WHERE date(h.voucher_date) = :vDate AND h.company_mst=:companymstid  " + 
    		"  union\n" + 
    		"  SELECT 0 as cashSale ,0 card , sum(qty*MRP)  as temp  from sales_dtl  d , sales_trans_hdr h where h.id = d.sales_trans_hdr_id and   d.item_id  in (select item_id from item_temp_mst) \n" + 
    		" and  date(h.voucher_date) = :vDate and h.company_mst=:companymstid ) "
    		+ ""
    		+ "A"
    		+ " ")

List<Object>getDayEndDetails(Date vDate,String companymstid);
    
    
    @Query(nativeQuery=true,value= "select * from day_end_closure_hdr where date(process_date)=:date")
    DayEndClosureHdr getDayEndClosureHdr(Date date);
    
    
    @Query(nativeQuery=true,value= "select * from day_end_closure_hdr   where     process_date in (\n" + 
    		"select  max(process_date)  from day_end_closure_hdr)  ")
    DayEndClosureHdr getLastDayEndClosureHdr();
    
    
  List<DayEndClosureHdr> findByCompanyMstId(String companymstid);

  @Query(nativeQuery=true,value= "select h.day_end_status "
  		+ "from day_end_closure_hdr h where h.branch_code=:branchCode and "
  		+ "date(process_date)=:date and day_end_status=:'DONE'")
  String gerDayEndCosureStatus(Date date,String branchCode);
  
  @Query(nativeQuery = true,value="select h.physical_cash from day_end_closure_hdr h where date(process_date)=:date "
  		+ " and h.branch_code=:branchCode and day_end_status='DONE'")
  Double getPhysicalCash(Date date,String branchCode);
  
  
  @Query(nativeQuery = true, value = "select * from day_end_closure_hdr where company_mst =:companymst and branch_code=:branchcode and voucher_date = (select max(voucher_date) from day_end_closure_hdr)")
  List<DayEndClosureHdr> getMaxOfDayEnd(String companymst,String branchcode);
  
  
  
  
  @Query(nativeQuery = true, value = "select d.* from day_end_closure_dtl d,day_end_closure_hdr m where m.company_mst =:companymst and m.branch_code=:branchcode and "
  		+ "date(m.process_date) =:date and m.id=d.day_end_closure_hdr_id")
  List<Object> getCountofDenomination(String companymst,String branchcode,Date date);
  
  
//  @Query(nativeQuery=true,value= "SELECT A.branch_code, sum(A.totalAmount) as totalAmount  ,  sum(A.cashSale) as CASH , sum(A.card) AS CARD  , "
//  		+ " sum(a.temp)  as CARD2   FROM "
//  		+ " "
//  		+ ""
//  		+ " ( select h.branch_code as BRANCH_CODE , sum(h.invoice_amount) as totalAmount , sum(h.cash_pay) as cashSale ,"
//  		+ " sum(r.cardamount) as card  , 0 as temp \n" + 
//  		" FROM " + 
//  		"  SALES_TRANS_HDR  h  , sales_receipts r "
//  		+ "WHERE date(h.voucher_date) = :vDate AND h.company_mst=:companymstid  AND h.id = r.sales_trans_hdr_id "
//  		+ " group by branch_code  " + 
//  		"  union " + 
//  		"  SELECT  h.branch_code  as BRANCH_CODE , sum(h.invoice_amount) as totalAmount , 0 as cashSale ,0 card , sum(qty*MRP)  as temp  "
//  		+ " from sales_dtl  d , sales_trans_hdr h where h.id = d.sales_trans_hdr_id and   d.item_id  in (select item_id from item_temp_mst) \n" + 
//  		" and  date(h.voucher_date) = :vDate and h.company_mst=:companymstid "
//  		+ " group by branch_code   ) "
//  		+ ""
//  		+ "A group by branch_code "
//  		+ " ")
// List<Object> getDayEndReport(Date vDate,String companymstid);
  
  //-------------------derby specific---------------

@Query(nativeQuery=true,value= "SELECT A.branch_code,sum(A.totalAmount) as totalAmount "
		+ ",sum(A.cashSale) as CASH , sum(A.card) AS CARD  ,   sum(a.temp)  as CARD2 "
		+ "FROM ( "
		+ ""
		+ " select h.branch_code as BRANCH_CODE , sum(h.invoice_amount) as totalAmount ,  "
		+ "sum(h.cash_pay) as cashSale , 0 as card , 0 as temp FROM  SALES_TRANS_HDR  h   "
		+ "WHERE date(h.voucher_date) = :vDate AND h.company_mst=:companymstid  "
		+ "group by branch_code   "
		
		
		+ "union  select t.branch_code as BRANCH_CODE ,0 as totalAmount  ,  0 as cashSale "
		+ ", sum(receipt_amount) as CARD , 0 as temp  from sales_receipts r  ,  sales_trans_hdr t  "
		+ " where  r.receipt_mode  not in('CREDIT' , t.branch_code||'-CASH', 'CASH','UBER SALES', 'SWIGGY' ,'ZOMATO', 'FOOD PANDA', 'ONLINE') and r.SALES_TRANS_HDR_ID = t.id  "
		+ "and date(t.voucher_date) = :vDate AND t.company_mst=:companymstid  "
		+ "group by t.branch_code  "
		
		
		+ "union SELECT  h.branch_code  as BRANCH_CODE , sum(h.invoice_amount) as totalAmount ,0 as cashSale ,0 card , "
		+ "sum(qty*MRP)  as temp   from sales_dtl  d , sales_trans_hdr h  "
		+ "where h.id = d.sales_trans_hdr_id and d.item_id  in (select item_id from item_temp_mst) and  "
		+ "date(h.voucher_date) =:vDate  and h.company_mst=:companymstid  group by branch_code   ) "
		+ "A  group by branch_code ")
 
List<Object> getDayEndReport(Date vDate,String companymstid);
  
  
  @Query(nativeQuery=true,value= "select h.branch_code from sales_trans_hdr h where date(h.voucher_date) = :vDate and h.company_mst=:companymstid")
  List<String> getBranch(Date vDate,String companymstid);


List<DayEndClosureHdr> findByProcessDateAndCompanyMst(Date voucherdate, CompanyMst companyMst);


//-------------------derby specific---------------

@Query(nativeQuery=true,value= "select sum(B.cash)-sum(B.changeamount) as total "
		+ "from "
		
		+ "(select sum(r.receipt_amount)  as cash  , 0 as changeamount from sales_receipts r  ,"
		+ "  sales_trans_hdr t where  r.receipt_mode in ('CASH',t.branch_code||'-CASH') "
		+ "and r.SALES_TRANS_HDR_ID = t.id and date(t.voucher_date) =:date AND "
		+ "t.company_mst=:companymstid "
	
		
		+ "union select 0 as cash , "
		+ "sum(A.receipt)-sum(A.invoice) as changeamount "
		+ "from(select sum(receipt_amount) as receipt, 0 as invoice from sales_receipts r  ,  "
		+ "sales_trans_hdr t  where  "
//		+ "r.receipt_mode not in( 'CREDIT' , 'MFP-CASH' , 'CASH' ,'UBER SALES', 'SWIGGY' ,'ZOMATO', 'FOOD PANDA', 'ONLINE') and "
//		+ "r.receipt_mode not in( 'CREDIT' , 'UBER SALES', 'SWIGGY' ,'ZOMATO', 'FOOD PANDA', 'ONLINE') and "
		+ " r.SALES_TRANS_HDR_ID = t.id  and "
		+ "date(t.voucher_date) =:date AND t.company_mst=:companymstid  "
		
		
		
		+ "union select 0 as receipt, sum(t.invoice_amount) as invoice from  "
		+ "sales_trans_hdr t  where date(t.voucher_date) =:date AND t.company_mst=:companymstid "
		+ "and t.voucher_number is not null)A)B" + 
		"")
Double totalDayEndCash(Date date, String companymstid );

//  @Query(nativeQuery=true,value= "select from day_end_closure_hdr h,user_mst u,branch_mst b where u.id=h.user_id and b.brach_code=h.branch_code and h.company_mst=:companymstid ")
//  List<Object>getDayEndClosureReport(Date voucherdate,String companymstid);
//  
  
  
  
  
@Query(nativeQuery = true,value="select *  from day_end_closure_hdr where company_mst=:companymstid and "
		+ "date(process_date)=:date and  branch_code=:branchCode")
DayEndClosureHdr  getDayEndClosureHdrByDateAndBranchCode(java.sql.Date date,String companymstid, String branchCode);
  
  
  
  
@Modifying
@Query(nativeQuery = true, value ="delete from day_end_closure_hdr where "
		+ "date(voucher_date)< :date and company_mst=:companyMst")
void deleteDayEndByVoucherDate(CompanyMst companyMst, java.sql.Date date);
  
  
  
  
}
