package com.maple.restserver.repository;


import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.his.entity.DoctorMst;
import com.maple.restserver.his.entity.NewDoctorMst;
@Component
@Repository
public interface DoctorMstRepository extends JpaRepository<DoctorMst,Integer>{

	DoctorMst saveAndFlush(@Valid NewDoctorMst doctormst1);

	//NewDoctorMst saveAndFlush(@Valid NewDoctorMst doctormst1);

//	List<DoctorMst> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
