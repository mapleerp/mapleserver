package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.ItemBranchDtl;
import com.maple.restserver.entity.ItemBranchHdr;
import com.maple.restserver.entity.JournalDtl;

public interface ItemBranchMstRepository extends JpaRepository<ItemBranchHdr, String> {

	List<ItemBranchHdr>	findByCompanyMstId(String companymstid);
	
}
