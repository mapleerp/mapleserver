package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.JournalHdr;

@Repository
public interface JournalDtlRepository extends JpaRepository<JournalDtl, String>{
	
	List<JournalDtl> findByJournalHdrId(String journalhdrid );
	
	
	@Query(nativeQuery = true, value = "SELECT SUM(debit_amount) as totalamount  FROM journal_dtl d where journal_hdr =:journalhdrid"
    		)
	 public Double findSumDebitAmount(@Param("journalhdrid") String journalhdrid);
	
	
	@Query(nativeQuery = true, value = "SELECT SUM(credit_amount) as totalamount  FROM journal_dtl d where journal_hdr =:journalhdrid"
    		)
	 public Double findSumCreditAmount(@Param("journalhdrid") String journalhdrid);
	
List<JournalDtl> findByJournalHdr(JournalHdr journalHdr);
	
	@Query(nativeQuery = true,value="select a.account_name ,d.remarks,d.credit_amount,d.debit_amount from journal_dtl d, account_heads a where a.id=d.account_head and d.journal_hdr=:journalhdrid " )
	
List<Object>getJournalDtl(String journalhdrid);
	@Query(nativeQuery = true,value="select a.account_name ,d.remarks,d.credit_amount,d.debit_amount from journal_hdr h,journal_dtl d, account_heads a where a.id=d.account_head and h.id=d.journal_hdr and "
			+ "h.company_mst=:companyMst and date(h.voucher_date)>=:fromDate "+ 
					"and date(h.voucher_date)<=:toDate and h.branch_code=:branchcode" )
	List<Object> getJournalJasperReportr(CompanyMst companyMst,Date fromDate,Date toDate,String branchcode);
}
