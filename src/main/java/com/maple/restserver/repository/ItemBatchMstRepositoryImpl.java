package com.maple.restserver.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ItemBatchMst;

@Repository
public class ItemBatchMstRepositoryImpl implements ItemBatchMstRepositoryCustom {

	@PersistenceContext
	 EntityManager em;
	 
	    // constructor
	
	
	
	 
	    @Override
	   public List<ItemBatchMst> findItemBatchMstByItemIdAndQty(String itemId, Double qty) {
		
	    	CriteriaBuilder cb = em.getCriteriaBuilder();
	        CriteriaQuery<ItemBatchMst> cq = cb.createQuery(ItemBatchMst.class);
	     
	        Root<ItemBatchMst> itemBatchMst = cq.from(ItemBatchMst.class);
	        List<Predicate> predicates = new ArrayList<>();
	         
	        
	        if (qty != null) {
	            predicates.add(cb.greaterThan(itemBatchMst.get("qty"), qty)  );
	            
	            predicates.add(cb.equal(itemBatchMst.get("itemId"),itemId ) );
	        }
	        cq.where(predicates.toArray(new Predicate[0]));
	     
	        return em.createQuery(cq).getResultList();
	        
	    }

	 /*
	  * return em.createQuery(query)
         .setFirstResult(offset) // offset
         .setMaxResults(limit) // limit
         .getResultList();
	  */
	 
	    
}
