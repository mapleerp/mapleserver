package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.StockTransferInReturnDtl;

public interface StockTransferInReturnDtlRepository extends JpaRepository<StockTransferInReturnDtl, String>{


	List<StockTransferInReturnDtl> findByCompanyMst(CompanyMst companymstid);

	@Modifying
	@Query(nativeQuery = true,value="update stock_transfer_in_dtl  d  set d.qty =:qty where d.item_id=:ItemId and d.stock_transfer_in_hdr=:stockTransferInHdrId  ")
	void updateStockTransferInDtlQty(String stockTransferInHdrId,ItemMst ItemId,Double qty);
	
	 
}
