package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.OpeningBalance;
import com.maple.restserver.entity.OrderReadyMst;

@Repository
public interface OrderReadyMstRepository extends JpaRepository<OrderReadyMst, String> {

}
