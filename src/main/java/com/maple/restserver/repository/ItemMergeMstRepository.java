package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMergeMst;
@Transactional
@Repository
public interface ItemMergeMstRepository extends JpaRepository<ItemMergeMst, String> {

	
	
	@Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update actual_production_dtl set item_id=:toItemId where item_id=:fromItemId"
	  
	  ) 
     void updateActulProductionDtl(String fromItemId,String toItemId);
	 
	@Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update damage_dtl set item_id=:toItemId where item_id=:fromItemId") 
	void  updateDamageDtl(String fromItemId,String toItemId);
	  
	  
	
	@Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update item_batch_dtl set item_id=:toItemId,barcode=:fromItemBarCode where item_id=:fromItemId")
	 void updateItemBatchDtl(String fromItemId,String toItemId,String fromItemBarCode);
	  
	  
	@Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update kit_defenition_dtl set item_id=:toItemId,barcode=:fromItemBarCode, "
	  		+ "item_name=:toItemName"
	  		+ " where item_id=:fromItemId")
   void updatekitdefinitondtl(String fromItemId,String toItemId,String fromItemBarCode, String toItemName);

	 @Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update kit_definition_mst set item_id=:toItemId,barcode=:fromItemBarCode where item_id=:fromItemId")
	  void updatekitdefinitionmst(String fromItemId,String toItemId,String fromItemBarCode);
	  

	  @Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update purchase_dtl set item_id=:toItemId, barcode=:fromItemBarCode, item_name=:toItemName "
	  		+ "where item_id=:fromItemId")
	  void updatepurchasedtl(String fromItemId,String toItemId,String fromItemBarCode, String toItemName);
	    //--------------version 4.3 end

	@Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update kit_defenition_dtl set item_id=:toItemId,barcode=:fromItemBarCode where item_id=:fromItemId")
     void updatekitdefinitondtl(String fromItemId,String toItemId,String fromItemBarCode);

	 @Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update physical_stock_dtl set item_id=:toItemId,barcode=:fromItemBarCode where item_id=:fromItemId")
     void updatephysicalstockdtl(String fromItemId,String toItemId,String fromItemBarCode);
	  
//	  @Query(nativeQuery = true, value =
//	  "update product_conversion_dtl set item_id=:toItemId where item_id=:fromItemId"
//	  ) Boolean updateproductconversiondtl(String fromItemId,String toItemId);
//	  
//	  @Modifying(flushAutomatically = true)
//	  @Query(nativeQuery = true, value =
//	  "update product_conversion_dtl_dtl set item_id=:toItemId where item_id=:fromItemId"
//	  ) void updateproductconversiondtldtl(String fromItemId,String toItemId);
//	  
	  @Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update purchase_dtl set item_id=:toItemId,barcode=:fromItemBarCode where item_id=:fromItemId")
	  void updatepurchasedtl(String fromItemId,String toItemId,String fromItemBarCode);
	  
	  @Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update raw_material_issue_dtl set item_id=:toItemId where item_id=:fromItemId") 
	  void updaterowmaterialissue(String fromItemId,String toItemId);
	   
	  
	  @Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update raw_material_return_dtl set item_id=:toItemId where item_id=:fromItemId") 
	  void updaterowmaterialreturn(String fromItemId,String toItemId);
	  
	  
//	  @Modifying(flushAutomatically = true)
//	  @Query(nativeQuery = true, value =
//	  "update row_material_return_dtl set item_id=:toItemId where item_id=:fromItemId"
//	  ) void updatesalesdtlresult(String fromItemId,String toItemId);
//	  
	  @Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update sales_order_dtl set item_id=:toItemId, "
	  		+ "barode=:fromItemBarCode,item_name=:toItemName where item_id=:fromItemId")
	  
	 void updatesaleorderdtlresult(String fromItemId,String toItemId,String fromItemBarCode, String toItemName);
	 


    @Modifying(flushAutomatically = true)
	@Query(nativeQuery = true, value ="update item_mst set is_deleted='Y',item_name= :itemName where id=:fromItemId" )
	void deleteItem(String fromItemId,String itemName);

	List<ItemMergeMst> findByCompanyMst(CompanyMst companyMst);

	 @Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update sales_dtl set item_id=:toItemId, item_name=:toItemName "
	  		+ "where item_id=:fromItemId")
	void updatesalesdtlresult(String fromItemId, String toItemId, String toItemName);
	 
	 @Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update item_batch_mst set item_id=:toItemId,barcode=:fromItemBarCode where item_id=:fromItemId")
	 
		void updateItemBatchMst(String fromItemId, String toItemId, String fromItemBarCode);
	  
	  
	  @Modifying(flushAutomatically = true)
	  @Query(nativeQuery = true, value ="update sales_order_dtl set item_id=:toItemId,barode=:fromItemBarCode where item_id=:fromItemId")
	 void updatesaleofrderdtlresult(String fromItemId,String toItemId,String fromItemBarCode);
	  @Modifying(flushAutomatically = true)
	  
	  
	  @Query(nativeQuery = true, value ="update stock_transfer_out_dtl set item_id=:toItemId,barcode=:fromItemBarCode where item_id=:fromItemId")
	void updateStockTransferOutDtl(String fromItemId, String toItemId, String fromItemBarCode);

	  
	  
	  @Query(nativeQuery = true,value="select *  from item_merge_mst where  voucher_date = :toDate , company_mst = :companyMst")
	  List<ItemMergeMst> getItemMergeReportwithDate(CompanyMst companyMst, Date toDate);
	 
	 

	
}
