package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemPopUp;
import com.maple.restserver.entity.ItemPriceMst;

public interface ItemStockRepository  extends JpaRepository<ItemPopUp, String>{

	@Query("SELECT i.itemName  , b.barcode   , "
			+ " u.unitName   ,b.mrp  , b.qty , "
			+ " i.cess, i.taxRate ,"
			+ "  i.id,"
			+ "  u.id ,"
			+ " b.batch FROM ItemMst i , UnitMst u , ItemBatchMst b "
			+ " WHERE i.unitId= u.id  AND i.id = b.itemId ")
 
	public List<Object> finditemstockreport();

	
	@Query(nativeQuery = true, value = "SELECT "
			+ "cat.category_name,"
			+ "itm.item_name,"
			+ "itmbat.batch,"
			+ "itm.item_code,"
			+ "itmbat.exp_date,"
			+ "SUM(itmbat.qty_in - itmbat.qty_out) AS quantity,"
			+ "itm.standard_price "
			+ "FROM "
			+ "item_mst itm,"
			+ "item_batch_dtl itmbat,"
			+ "category_mst cat "
			+ "WHERE "
			+ "itm.id = itmbat.item_id AND "
			+ "itm.category_id = cat.id AND "
			+ "itmbat.voucher_date = :date "
			+ "GROUP BY "
			+ "cat.category_name,itm.item_name,itmbat.batch,itm.item_code,itmbat.exp_date,itm.standard_price")
	public List<Object> findBranchStockReport(Date date);


	@Query(nativeQuery = true, value = "SELECT "
			+ "cat.category_name,"
			+ "itm.item_name,"
			+ "itmbat.batch,"
			+ "itm.item_code,"
			+ "itmbat.exp_date,"
			+ "SUM(itmbat.qty_in - itmbat.qty_out) AS quantity,"
			+ "itm.standard_price "
			+ "FROM "
			+ "item_mst itm,"
			+ "item_batch_dtl itmbat,"
			+ "category_mst cat "
			+ "WHERE "
			+ "itm.id = itmbat.item_id AND "
			+ "itm.category_id = cat.id AND "
			+ "itmbat.voucher_date <= :date AND "
			+ "itmbat.company_mst=:companyMst and itmbat.branch_code=:branchCode "
			+ "GROUP BY "
			+ "cat.category_name,itm.item_name,itmbat.batch,itm.item_code,itmbat.exp_date,itm.standard_price")
	
	
	public List<Object> findPharmacyStockBranchWiseReport(CompanyMst companyMst, Date date, String branchCode);


	@Query(nativeQuery = true, value = "SELECT "
			+ "cat.category_name,"
			+ "itm.item_name,"
			+ "itmbat.batch,"
			+ "itm.item_code,"
			+ "itmbat.exp_date,"
			+ "SUM(itmbat.qty_in - itmbat.qty_out) AS quantity,"
			+ "itm.standard_price "
			+ "FROM "
			+ "item_mst itm,"
			+ "item_batch_dtl itmbat,"
			+ "category_mst cat "
			+ "WHERE "
			+ "itm.id = itmbat.item_id AND "
			+ "itm.category_id = cat.id AND "
			+ "itmbat.voucher_date <= :date AND "
			+ "itmbat.company_mst=:companyMst and itmbat.branch_code=:branchCode and "
			+ "itm.category_id=:id "
	        + "GROUP BY "
	        + "cat.category_name,itm.item_name,itmbat.batch,itm.item_code,itmbat.exp_date,itm.standard_price")
	
	public List<Object> findPharmacyStockBranchAndItemWiseReport(CompanyMst companyMst, Date date, String branchCode,
			String id);
	


	

	
}
