package com.maple.restserver.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.InMessageXml;
@Repository
@Transactional
public interface InMsgXmlRepository extends JpaRepository<InMessageXml, String> {

	
	List<InMessageXml>findByCompanyMstId(String companymstid);
}