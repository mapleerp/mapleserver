package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ChequePrintMst;

@Repository
@Component
public interface ChequePrintMstRepository extends JpaRepository<ChequePrintMst, String>{

	List<ChequePrintMst> findByAccountId(String accid);

}
