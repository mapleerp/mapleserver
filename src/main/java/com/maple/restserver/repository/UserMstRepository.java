package com.maple.restserver.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
//import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.UserMst;

@Repository
public interface UserMstRepository  extends JpaRepository<UserMst, String>{
	 
	Optional<UserMst> findByIdAndCompanyMstId(String id,String companymstid);
	 
	 Optional<UserMst> findByUserNameAndBranchCode(String userName,String branchCode);
	 
	  
	 
	 Optional<UserMst> findByUserNameAndPasswordAndBranchCode(String userName,String password,String branchCode);
	 
	
	 
	 @Query(nativeQuery=true,value="update  user_mst u set u.user_name =:newUserName, u.password =:newPassword where u.id=:id")
	 Optional<UserMst> 	changepassword (  String newUserName,  String newPassword, String id);

	 
@Query(nativeQuery = true,value="select pm.process_name "
		+ " from process_permission_mst pp , process_mst pm"
		+ " where pp.process_id  = pm.id and user_id=:Id "
		+ " and pm.company_mst = :comapnymstid"
		+ " union "
		+ "select pm.process_name  from"
		+ " group_permission_mst gm, process_mst pm "
		+ " where  gm.process_id = pm.id and "
		+ " gm.group_id in (select group_id "
		+ "from user_group_mst where user_id=:Id"
		+ " and company_mst = :comapnymstid )")

ArrayList<String> getUserRoles(String Id, String comapnymstid);


UserMst findByUserNameAndCompanyMstId(String userName,String companyMstId);


//and pp.end_date<=:currentDate or pp.end_date is null
/*
 * 
 * In put UserId
 * Out put List of Permissions
 * 
 * select process_id from process_permission_mst where user_id = :user
 * union
 * select process_id from group_permission_mst where 
 * group_id in (select group_id from user_group_mst where user_id = :user)
 * 
 */


List<UserMst> findByCompanyMstId(String companymstid);

Optional<UserMst> findByUserNameAndPassword(String userName,String password);


@Query(nativeQuery=true,value="select * from user_mst where company_mst=:companymstid and LOWER(user_name) like LOWER(:string) ")

List<UserMst> searchLikeUserByName(String string, String companymstid, Pageable topFifty);

}
