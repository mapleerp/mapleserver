package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.FinanceMst;
@Component
@Repository
public interface FinanceRepository extends JpaRepository<FinanceMst, String>{

	List<FinanceMst> findByCompanyMst(CompanyMst companyMst);
	
	FinanceMst findByName(String name);
	
	
	
	

}
