package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CreditCardReconcileHdr;

@Repository
public interface CreditCardReconcileRepository extends JpaRepository<CreditCardReconcileHdr, String>{

	
	@Query(nativeQuery = true,value = "select * from credit_card_reconcile_hdr h where h.trans_date=:fromDate and h.branch_code=:branchcode ")
	List<CreditCardReconcileHdr>fetchCreditCardReconcile(String branchcode ,Date fromDate);
	//date(h.voucher_date) between :fdate and :tdate

@Query(nativeQuery = true,value="select * from credit_card_reconcile_hdr h where date(h.trans_date) >= :fromdate and date(h.trans_date) <=:todate and h.branch_code=:branchcode ")
	List<CreditCardReconcileHdr>getCreditCardReconcileHdrReport(String branchcode,Date fromdate,Date todate );
}
