package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SubBranchSalesTransHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
@Component
@Repository
public interface SubBranchSalesHdrRepository extends JpaRepository<SubBranchSalesTransHdr, String>{

	@Modifying
	@Query(nativeQuery=true,value="update sub_branch_sales_trans_hdr set status='CONVERTED' "
			+ "where company_mst=:companymstid and branch_code=:branchcode and  "
			+ "date(voucher_date)>=:fudate and date(voucher_date)<=:ftdate")
	void updateSubBranchSalesHdr(String branchcode, String companymstid, java.util.Date fudate, java.util.Date ftdate);
	
	@Modifying
	@Query(nativeQuery = true,value="update sub_branch_sales_trans_hdr h. set h.company_mst =:toAccountId where h.company_mst =:fromAccountId ")
    void accountMerge(String fromAccountId, String toAccountId);

	
	

}
