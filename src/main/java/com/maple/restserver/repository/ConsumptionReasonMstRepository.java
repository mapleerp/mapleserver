package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ConsumptionReasonMst;

@Repository
public interface ConsumptionReasonMstRepository extends  JpaRepository<ConsumptionReasonMst, String>{

	ConsumptionReasonMst findByReason(String reason);
}
