package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.DamageHdr;
@Component
@Repository
public interface DamageHdrRepository extends JpaRepository<DamageHdr, String> {
	
	
List<DamageHdr>findByCompanyMstId(String companymstid);

DamageHdr findByIdAndCompanyMstId(String id ,String companymstid);

Optional<DamageHdr> findById(String id);

@Query(nativeQuery = true,value = "select * from damage_hdr "
		+ "where date(voucher_date) >= :fdate and date(voucher_date) <= :tdate")
List<DamageHdr> getDamageHdrBtwnDate(Date fdate,Date tdate);


}
