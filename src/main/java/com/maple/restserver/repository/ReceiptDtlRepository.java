

package com.maple.restserver.repository;

 
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;

@Repository
public interface ReceiptDtlRepository extends JpaRepository<ReceiptDtl, String>{
	
	
	@Query(nativeQuery = true,value="select sum(dtl.RECIEVED_AMOUNT) from "
			+ "receipt_invoice_dtl dtl,receipt_hdr h, "
			+ "receipt_dtl d "
			+ "where h.id = dtl.receipt_hdr and d.receipt_hdr_id=h.id and "
			+ " date(dtl.invoice_date) = :date and d.mode_ofpayment=:mode ")
	
	
	
//	 @Query(nativeQuery = true,value="select sum(d.AMOUNT) as amount "
//		  		+ "from receipt_dtl d, receipt_hdr h, sales_trans_hdr sh, receipt_invoice_dtl dtl "
//		  		+ "where  "
//		  		+ "d.receipt_hdr_id=h.id and date(h.voucher_date)=date(sh.voucher_date) and "
//		  		+ "date(h.voucher_date)=:date and "
//		  		+ "h.company_mst=:companymstid and h.branch_code=:branchcode and "
//		  		+ "dtl.sales_trans_hdr=sh.id and dtl.receipt_hdr=h.id "
//		  		+ "and d.mode_ofpayment=:paymentmode and h.voucher_number is not null")
			      
		Double getCreditSaleAdjustment(Date date,String mode);
		  
		  @Query(nativeQuery = true,value="select sum(d.AMOUNT) as amount "
			  		+ "from receipt_dtl d, receipt_hdr h, sales_trans_hdr sh, receipt_invoice_dtl dtl "
			  		+ "where  "
			  		+ "d.receipt_hdr_id=h.id and date(h.voucher_date)!=date(sh.voucher_date) and "
			  		+ "date(h.voucher_date)=:date and "
			  		+ "h.company_mst=:companymstid and h.branch_code=:branchcode and "
			  		+ "dtl.sales_trans_hdr=sh.id and dtl.receipt_hdr=h.id "
			  		+ "and d.mode_ofpayment=:paymentmode and h.voucher_number is not null")
				      
		Double getOtherCashSale(Date date, String companymstid, String branchcode, String paymentmode);
		
	List<ReceiptDtl> findByReceiptHdrVoucherDate(Date vDate);

	List<ReceiptDtl> findByReceiptHdrId(String receiptsHdrId, Pageable pageable);
    Optional<ReceiptDtl> findByIdAndId(String id, String receiptsHdrId);
  
    List<ReceiptDtl>findByReceiptHdrId(String receiptsHdrId);
    
    
 //   List<ReceiptDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);

	 @Query(nativeQuery = true, value = "SELECT SUM(amount) as totalamount  FROM receipt_dtl d where receipt_hdr_id =:hdrId"
	    		)
public Double getSumOfReceiptDtlAmount(String hdrId);
	 
	 
	 @Query(nativeQuery = true, value ="select d.amount,d.remark from receipt_hdr h,receipt_dtl d where h.id=d.receipt_hdr_id"
	 		+ " and date(h.voucher_date)=:reportdate and d.account=:accountid")
	 List<Object> findByAccountAndInstrumentDate(String accountid,Date reportdate);
	 
	 
	 @Query(nativeQuery = true, value ="select rhdr.voucher_number,rhdr.voucher_date,sum(rdtl.amount),rhdr.id,ac.account_name from receipt_hdr rhdr, receipt_dtl rdtl,account_heads ac where date(rhdr.voucher_date) between :fdate and :tdate and rhdr.id = rdtl.receipt_hdr_id and rdtl.account =:accId and ac.id =:accId group by "
	 		+ "rhdr.voucher_number,rhdr.voucher_date,rhdr.id,ac.account_name")
	 List<Object> findByReceiptHdrBetweenDate(String accId,Date fdate,Date tdate);
	 
	 
	 
	 @Query(nativeQuery = true, value ="select rhdr.voucher_number,rhdr.voucher_date,sum(rdtl.amount),"
	 		+ "rhdr.id,ac.account_name from receipt_hdr rhdr, receipt_dtl rdtl,account_heads ac where "
	 		+ "date(rhdr.voucher_date) between :fdate and :tdate and rhdr.id = rdtl.receipt_hdr_id "
	 		+ "and ac.id = rdtl.account group by rhdr.voucher_number,rhdr.voucher_date,rhdr.id,ac.account_name")
	 List<Object> findByReceiptHdrBetweenDate(Date fdate,Date tdate);
	 
	 
	 @Query(nativeQuery = true, value ="select d.invoice_number,d.invoice_date,d.recieved_amount"
	 		+ " from receipt_hdr h,receipt_invoice_dtl d where h.id=d.receipt_hdr_id"
		 		+ " and h.voucher_date=:reportdate and date(h.voucher_number)=:voucherNo")
		 List<Object> findReceiptInvDtlByHdrId(String voucherNo,Date reportdate);
	 
	 
	 @Query(nativeQuery = true, value ="select sum(d.amount) from receipt_dtl d,receipt_hdr h where"
	 		+ " h.id= d.receipt_hdr_id and date(h.voucher_date) =:fdate and h.branch_code=:branchcode and "
	 		+ "h.company_mst = :companymstid and d.mode_ofpayment=:acid")
	Double getSumOfReceiptDtlAmountByDate(String companymstid, Date fdate,String branchcode,String acid);
	

     @Query(nativeQuery = true,value="select distinct d.MODE_OFPAYMENT "
    			+ "from receipt_dtl d, receipt_hdr h where "
    			+ "d.RECEIPT_HDR_ID=h.id and h.company_mst=:companymstid   ")
	 List<String> findAllReceiptMode(String companymstid);
     
     
     @Query(nativeQuery = true,value="select sum(d.AMOUNT) as amount, "
      		+ "a.ACCOUNT_NAME, d.MODE_OFPAYMENT from account_heads a, receipt_dtl d, receipt_hdr h "
      		+ "where d.RECEIPT_HDR_ID=h.id and h.company_mst=:companymstid and "
      		+ "h.VOUCHER_NUMBER is NOT NULL and a.id=d.DEBIT_ACCOUNT_ID and "
      		+ "date(h.VOUCHER_DATE)=:date and d.MODE_OFPAYMENT=:receiptMode group by "
      		+ "a.ACCOUNT_NAME, d.MODE_OFPAYMENT"
      		+ "")
	List<Object> receiptSummaryReport(String companymstid, Date date, String receiptMode);

	  List<ReceiptDtl> findByReceiptHdr(ReceiptHdr receiptHdr);
	  
		// ------------------version 4.14
		@Query(nativeQuery = true, value = "select sum(d.AMOUNT) as amount "
				+ " from account_heads a, receipt_dtl d, receipt_hdr h "
				+ "where d.RECEIPT_HDR_ID=h.id and h.company_mst=:companymstid and "
				+ "h.VOUCHER_NUMBER is NOT NULL and a.id=d.DEBIT_ACCOUNT_ID and "
				+ "date(h.VOUCHER_DATE)=:date and d.MODE_OFPAYMENT=:receiptmode ")
		Double receiptSummaryReportByReceiptMode(String companymstid, Date date, String receiptmode);
		// ------------------version 4.14 end

		//------------------version 5.1 surya
		@Query(nativeQuery = true, value = "select sum(d.AMOUNT) as amount, "
				+ " a.ACCOUNT_NAME "
				+ " from account_heads a, receipt_dtl d, receipt_hdr h "
				+ "where d.RECEIPT_HDR_ID=h.id and h.company_mst=:companymstid and "
				+ "h.VOUCHER_NUMBER is NOT NULL and a.id=d.ACCOUNT and "
				+ "date(h.VOUCHER_DATE)=:date and h.BRANCH_CODE=:branchcode group by a.ACCOUNT_NAME ")
		List<Object> getDailyReciptSummaryByDateAndAccount(String companymstid, Date date, String branchcode);
		//------------------version 5.1 surya end

		
		@Modifying
		@Query(nativeQuery = true,value="update receipt_dtl d set d.account =:toAccountId where d.account =:fromAccountId ")
	    void accountMerge(String fromAccountId, String toAccountId);

 
		@Query(nativeQuery = true,value = "select a.account_name,d.mode_ofpayment,d.amount,d.deposit_bank, d.bank_account_number,d.remark from receipt_dtl d,account_heads a where d.account=a.id and d.receipt_hdr_id=:receipthdrId")
		List<Object>fetchByReceiptHdrId(String receipthdrId);
}

