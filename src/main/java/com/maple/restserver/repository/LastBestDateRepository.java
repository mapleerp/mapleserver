package com.maple.restserver.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LastBestDate;

@Component
@Repository
public interface LastBestDateRepository extends JpaRepository<LastBestDate, String>{

	@Query(nativeQuery = true, value = "SELECT * FROM last_best_date where activity_type = :activityType") 

	LastBestDate findByActivityType(String activityType);
	@Query(nativeQuery = true, value = "SELECT last_success_date FROM last_best_date where activity_type = :activityType") 

	Date findBestDateByActivityType(String activityType);
	
	
//	@Query(nativeQuery = true, value = "SELECT count(*)  FROM sales_trans_hdr sth where date(sth:voucher_date)>=:fromDate and"
//			+ "sth:company_mst=:companyMst ") 
//	Double getSalesTransHdrCount(CompanyMst companyMst, Date fromDate);

}

