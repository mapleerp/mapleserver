package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.StockTransferIn;
import com.maple.restserver.entity.StockTransferInHdr;
import com.maple.restserver.entity.StockTransferOutDtl;

@Repository
public interface StockTransferInHdrRepository extends JpaRepository<StockTransferInHdr, String>{
	
//	List<StockTransferInHdr> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);

//	here we calculating the stock transferin report
	
	StockTransferInHdr findByVoucherDateAndVoucherNumber(Date voucherDate,String vNo);
	
	
	StockTransferInHdr findByStockTransferOutHdrId(String outhdrid);

	
	@Query(nativeQuery=true,value="from stock_trans_in_hdr  where date(voucher_date) BETWEEN:startDate AND :endDate")
	    List<StockTransferOutDtl>StockTranOUtSummary(Date startDate,Date endDate);
	    
	 
	 StockTransferInHdr findByInVoucherDateAndInVoucherNumber(Date voucherDate,String vNo);
	 
	 @Query(nativeQuery=true,value=" SELECT * FROM stock_transfer_in_hdr s WHERE s.status_accept_stock = 'N' and s.company_mst=:companymstid ") 
		List<StockTransferInHdr> fetchStockTransferInHdr(String companymstid);
	

	 @Query(nativeQuery=true,value="select * from stock_transfer_in_hdr h where lower(h.in_voucher_number) like   lower(:vouchernumber) AND h.status_accept_stock='PENDING'")

	 List<StockTransferInHdr> getPendingVoucherNumber(String vouchernumber);

	 @Query(nativeQuery=true,value="select * from stock_transfer_in_hdr h where date(h.voucher_date) between :fdate and :tdate and h.branch_code=:branchcode and h.company_mst=:companymstid and voucher_number is not null")
	 List<StockTransferInHdr>getStockTransferInHdr(String branchcode, java.util.Date fdate,java.util.Date tdate,String companymstid );

	 
	 @Modifying
	 @Query(nativeQuery = true,value="update stock_transfer_in_hdr set status_accept_stock= 'RETURNED' where "
	 		+ "date(in_voucher_date)=:uvdate and in_voucher_number=:vno and status_accept_stock='PENDING'")
	 void updateStockTransferInHdr(String vno, java.util.Date uvdate);

	 
	 StockTransferInHdr findByInVoucherNumber(String inVoucherNumber);

	 @Query(nativeQuery = true,value="select sum(d.qty) from stock_transfer_in_hdr h, stock_transfer_in_dtl d "
	 		+ "where h.id=d.stock_transfer_in_hdr and  h.branch_code = :branchCode "
	 		+ "and date(h.voucher_date) >= :fudate and date(h.voucher_date) <= :tudate and d.item_id = :itemId")
	Double getStockInQtyByItemName(java.util.Date fudate, java.util.Date tudate,String branchCode,
			String itemId);
}
