package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ServiceInHdr;

@Repository

public interface ServiceInHdrRepository extends JpaRepository<ServiceInHdr,String> {
	List<ServiceInHdr> findByVoucherDate(Date date);
	
	@Query(nativeQuery = true,value="select count(*) from service_in_hdr where voucher_number is NOT NULL and voucher_date=:date")
	int getClientServiceCount(Date date);
}
