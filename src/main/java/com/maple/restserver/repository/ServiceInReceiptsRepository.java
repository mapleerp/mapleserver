package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ServiceInHdr;
import com.maple.restserver.entity.ServiceInReceipt;
@Component
@Repository
public interface ServiceInReceiptsRepository extends JpaRepository<ServiceInReceipt, String>{


	  @Query(nativeQuery=true,value="  select sum(receipt_amount) as amount  "
	  		+ "from service_in_receipt s where s.service_in_hdr=:serviceinhdrid and "
	  		+ "company_mst =:companymstid ")
	Double retrieveServiceInReceiptAmount(String serviceinhdrid, String companymstid);

	List<ServiceInReceipt> findByServiceInHdrAndCompanyMst(ServiceInHdr serviceInHdr, CompanyMst companyMst);
	


}
