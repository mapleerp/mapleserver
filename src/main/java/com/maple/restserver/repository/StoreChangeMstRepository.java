package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.StoreChangeMst;

@Repository
public interface StoreChangeMstRepository  extends JpaRepository<StoreChangeMst, String>{

	@Query(nativeQuery = true,value = "select * from store_change_mst where "
			+ "date(voucher_date) >= :fudate and date(voucher_date) <= :tudate")
	List<StoreChangeMst> getStoreChangeMstBtwnDate(Date fudate, Date tudate);

}
