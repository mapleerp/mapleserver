package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MenuConfigMst;

@Component
@Repository
public interface MenuConfigMstRepository extends JpaRepository<MenuConfigMst, String> {

	List<MenuConfigMst> findByMenuName(String menuname);

	List<MenuConfigMst> findByCompanyMstAndMenuDescription(CompanyMst companyMst, String description);

	List<MenuConfigMst> findByCompanyMst(CompanyMst companyMst);

	MenuConfigMst findByCompanyMstAndId(CompanyMst companyMst, String id);

	List<MenuConfigMst> findByMenuNameAndCompanyMst(String string, CompanyMst companyMst);

	@Query(nativeQuery = true, value = "select * from  menu_config_mst c where c.id in"
			+ " (select menu_id from menu_mst where parent_id = :param)")
	List<MenuConfigMst> getMenuConfigMstByParentId(String param);

	@Query("SELECT m FROM MenuConfigMst m WHERE LOWER(m.menuName) like   LOWER(:menuName) AND m.companyMst.id=:companymst")

	List<MenuConfigMst> searchLikeMenuByName(String menuName, String companymst);

	 
}
/*
 * 
 * 
 * 700bd09e-f4e4-4d0e-a473-043bafbc4920 select * from menu_mst where menu_id =
 * '700bd09e-f4e4-4d0e-a473-043bafbc4920'
 * 
 * select * from menu_config_mst c where c.id in (select menu_id from menu_mst
 * where parent_id = '700bd09e-f4e4-4d0e-a473-043bafbc4920')
 * 
 * http://localhost:8185/AMB/executesql/select%20*%20from%20%20menu_config_mst%
 * 20c%20where%20c.id%20in%20(select%20menu_id%20from%20menu_mst%20where%
 * 20parent_id%20=%20'700bd09e-f4e4-4d0e-a473-043bafbc4920')
 * 
 */
