package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CashBook;
import com.maple.restserver.entity.ClosingBalance;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.FinanceMst;

 
 
@Component
@Repository
public interface CompanyMstRepository extends JpaRepository<CompanyMst,String>{


	

	
}
 
