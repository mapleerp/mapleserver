package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.MultiUnitMst;

public interface MultiUnitMstRepository extends JpaRepository<MultiUnitMst, String>{
	

	List<MultiUnitMst> findByCompanyMstIdAndItemId(String companymstid,String itemid);

	MultiUnitMst findByCompanyMstIdAndItemIdAndUnit2(String companymstid,String itemId,String unit);
	MultiUnitMst findByCompanyMstIdAndItemIdAndUnit1(String companymstid,String itemId,String unit);
	 

}
