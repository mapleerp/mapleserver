package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SysDateMst;

@Repository
public interface SysDateMstRepository extends JpaRepository<SysDateMst,String>{

	
	SysDateMst findByCompanyMstAndBranchCode(CompanyMst companyMst,String branchCode);
}
