package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ItemBatchDtlTemp;
@Repository
public interface ItemBatchDtlTempRepository extends JpaRepository<ItemBatchDtlTemp, String > {

	@Modifying(flushAutomatically = true)
	@Query(nativeQuery = true,value ="INSERT INTO item_batch_dtl_temp (id,item_id, batch, barcode,qty_in,qty_out,mrp,exp_date,branch_code,store,company_mst) "
	 		+ "SELECT d.id, d.item_id,d.batch,d.barcode,sum(d.qty_in),sum(d.qty_out),d.mrp,d.exp_date,d.branch_code,d.store,d.company_mst FROM item_batch_dtl d group by d.item_id,d.batch,d.barcode,d.mrp,d.branch_code,d.store,d.exp_date,d.company_mst " )
	void itembatchDtlInsertingIntoItemBatchDtlTemp();
	
	@Query(nativeQuery = true,value ="select d.item_id,d.batch,d.barcode,sum(d.qty_in),sum(d.qty_out),d.mrp,d.exp_date,d.branch_code,d.store,d.company_mst from item_batch_dtl d group by d.item_id,d.batch,d.barcode,d.mrp,d.branch_code,d.store,d.exp_date,d.company_mst")
  List<Object>fetchItemBatchDtl();
	@Modifying(flushAutomatically = true)
	@Query(nativeQuery = true,value ="INSERT INTO item_batch_dtl (id, item_id,batch,barcode,qty_in,qty_out,mrp,exp_date,branch_code,particulars,source_parent_id,source_dtl_id,store,company_mst) "
			+ " SELECT id, item_id,batch,barcode,qty_in,qty_out,mrp,exp_date,branch_code,particulars,source_parent_id,source_dtl_id,store,company_mst FROM item_batch_dtl_temp ")
void InsertingInToItemBatchDtlFromItemBatchDtlTemp();

	
}
