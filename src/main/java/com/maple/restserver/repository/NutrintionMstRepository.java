package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.NutritionMst;

public interface NutrintionMstRepository extends JpaRepository<NutritionMst, String> {

	
	 NutritionMst findByNutrition(String nutritionfacts);
}
