package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CessDtl;

 
 
 
@Component
@Repository
public interface CessDtlRepository extends JpaRepository<CessDtl,String>{
	
	List<CessDtl>findByCompanyMstId(String companymstid);
//	List<CessDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
