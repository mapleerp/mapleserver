package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.OtherBranchSalesDtl;
import com.maple.restserver.entity.OtherBranchSalesTransHdr;
@Component
@Repository
public interface OtherBranchSalesDtlRepository extends JpaRepository<OtherBranchSalesDtl, String>{

	
	  @Query(nativeQuery = true, value = "SELECT SUM(qty) as totalQty, "
	    		+ " SUM( (rate*qty) + (  (rate*qty) * (tax_rate)/100 ))  as totalAmount, "
	    		+ " SUM(((rate*qty) * ((tax_rate)/100 ))) as totalTax,  "
	    		+ " SUM( (rate*qty) * cess_Rate/100) as totalCessAmt  "
	    		+ " FROM other_branch_sales_dtl d, other_branch_sales_trans_hdr h "
	    	 
	    		+ " where h.id = d.other_branch_sales_trans_hdr AND h.id = :hdrid")
	List<Object> findSumBranchSalesDtlforDiscount(String hdrid);

	  
	  @Query(nativeQuery = true, value = "SELECT SUM(qty) as totalQty, "
	    		+ " SUM((rate*qty) + (  (rate*qty) * (tax_rate)/100 ) + (rate*qty) * cess_Rate/100)  as totalAmount, "
	    		+ " SUM(((rate*qty) * ((tax_rate)/100 ))) as totalTax,  "
	    		+ " SUM( (rate*qty) * cess_Rate/100) as totalCessAmt  "
	    		+ " FROM other_branch_sales_dtl d, other_branch_sales_trans_hdr h "
	    	 
	    		+ " where h.id = d.other_branch_sales_trans_hdr AND h.id = :hdrid")
	List<Object> findBranchSumSalesDtl(String hdrid);


	  @Query(nativeQuery = true,value ="select * from other_branch_sales_dtl where "
	  		+ "other_branch_sales_trans_hdr = :id")
	List<OtherBranchSalesDtl> findByOtherBranchSalesTransHdr(String id);


	void deleteByOtherBranchSalesTransHdr(OtherBranchSalesTransHdr otherBranchSalesTransHdr);


	
	
	

}
