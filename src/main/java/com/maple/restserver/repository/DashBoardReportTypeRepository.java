package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DashBoardReportType;


@Repository
public interface DashBoardReportTypeRepository extends JpaRepository<DashBoardReportType, String>{
	List<DashBoardReportType> findByCompanyMst(CompanyMst companyMst);

	List<DashBoardReportType> findByDashBoardNameAndCompanyMstId(String dashBoardName, String companymstid);
	
}
