package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ParamMst;

public interface ParamMstRepository extends JpaRepository<ParamMst,String>{
	

	

	List<ParamMst> findByCompanyMst(CompanyMst companyMst);

	ParamMst findByParamName(String string);

	@Query("SELECT p FROM ParamMst p WHERE LOWER(p.paramName) like   LOWER(:paramname) AND p.companyMst.id=:companymstid")
    public List<ParamMst> findSearch(String paramname, String companymstid);
	

	

	Optional<ParamMst> findByIdAndCompanyMstId(String id, String companymstid);

	List<ParamMst> findByCompanyMstId(String companymstid);

	

}
