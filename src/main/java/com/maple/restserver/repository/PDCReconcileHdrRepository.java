package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.PDCReconcileHdr;
@Repository
public interface PDCReconcileHdrRepository extends  JpaRepository<PDCReconcileHdr, String> {

	List<PDCReconcileHdr>findByTransDateAndCompanyMstAndBranchCode(Date date,CompanyMst companyMst,String 
			branchcode);

	List<PDCReconcileHdr> findByVoucherNumberAndTransDate(String voucherNumber, Date voucherDate);
}
