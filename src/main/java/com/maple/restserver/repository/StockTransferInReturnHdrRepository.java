package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.StockTransferInReturnHdr;
@Repository
public interface StockTransferInReturnHdrRepository extends JpaRepository<StockTransferInReturnHdr, String>{


	List<StockTransferInReturnHdr> findByCompanyMst(CompanyMst companymstid);



}
