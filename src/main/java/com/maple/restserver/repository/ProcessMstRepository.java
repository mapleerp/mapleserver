package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProcessMst;
@Repository
public interface ProcessMstRepository extends JpaRepository<ProcessMst, String>{

	ProcessMst findByProcessName(String processName);

	ProcessMst findByProcessNameAndCompanyMst(String processname, CompanyMst companyMst);

	List<ProcessMst> findByCompanyMst(CompanyMst companyMst);

	List<ProcessMst> findByCompanyMstAndProcessType(CompanyMst companyMst, String processtype);
	
	
	

	@Query("SELECT c FROM ProcessMst c WHERE LOWER(c.processName) like   LOWER(:processname) AND c.companyMst.id=:companymstid")

List<ProcessMst> findSearch(String processname, String companymstid);
	
}
