package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ItemBatchDtlDeleteTemp;

@Repository
@Controller
public interface ItemBatchDtlDeleteTempRepository extends JpaRepository<ItemBatchDtlDeleteTemp,String>{
	@Modifying(flushAutomatically = true)
	@Query(nativeQuery=true,value="  delete from  item_batch_dtl_delete_temp ")
	void updateDailyStockFromDtlStep0();
	@Modifying(flushAutomatically = true)
	@Query(nativeQuery=true,value=" update item_batch_dtl_delete_temp u "
			+ "set mrp = ( select standard_price from item_mst p where u.item_id = p.id ) ")
	void updateDailyStockFromDtlStep3();
	

	@Modifying(flushAutomatically = true)
	@Query(nativeQuery=true,value="  delete from  item_batch_dtl ")
	void deleteItemBatchDtl();

}
