package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.GoodReceiveNoteDtl;
import com.maple.restserver.entity.GoodReceiveNoteHdr;

@Component
@Repository
public interface GoodReceiveNoteDtlRepository extends JpaRepository<GoodReceiveNoteDtl,String>{

	List<GoodReceiveNoteDtl> findByGoodReceiveNoteHdr(String goodreceivenotehdrid);

	@Query(nativeQuery = true, value = "SELECT *  FROM good_receive_note_dtl d where good_receive_note_hdr_id =:goodReceiveNoteHdrId order by item_serial")
	List<GoodReceiveNoteDtl> findByGoodReceiveNoteHdrIdOrderBySerial(String goodReceiveNoteHdrId);

	Optional<CompanyMst> findById(GoodReceiveNoteDtl goodReceiveNoteDtl);

	List<GoodReceiveNoteDtl> findByGoodReceiveNoteHdrId(String goodReceiveNoteHdrId);

	@Query(nativeQuery = true, value = "SELECT  SUM(qty) as totalQty "
			+ " FROM  good_receive_note_dtl d, good_receive_note_hdr h "
			+ " where h.id = d.good_receive_note_hdr_id AND h.id =:goodreceivenotehdrid")
	Double findSumGoodReceiveNoteDetail(String goodreceivenotehdrid);

	List<GoodReceiveNoteDtl> findByGoodReceiveNoteHdr(GoodReceiveNoteHdr goodReceiveNoteHdr);

	List<GoodReceiveNoteDtl> findByGoodReceiveNoteHdr(Optional<GoodReceiveNoteHdr> goodReceiveNoteHdr);

	GoodReceiveNoteDtl findByGoodReceiveNoteHdrIdAndItemIdAndBatchAndBarcodeAndUnitId(String id, String itemId,
			String batch, String barcode, String unitId);

}
