package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.OwnAccountSettlementDtl;
import com.maple.restserver.entity.OwnAccountSettlementMst;

@Repository
public interface OwnAccountSettlementDtlRepository extends JpaRepository<OwnAccountSettlementDtl, String> {

	List<OwnAccountSettlementDtl>	findByOwnAccountSettlementMstAndCompanyMst(OwnAccountSettlementMst ownAccountSettlementMst,CompanyMst companyMst);
	
	
	OwnAccountSettlementDtl	findByInvoiceNumberAndCompanyMst(String invoicenumber,CompanyMst companyMst);

	Optional<OwnAccountSettlementDtl>	findByInvoiceNumber(String invoicenumber);

}

