package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;

@Repository
public interface StockTransferOutHdrRepository extends JpaRepository<StockTransferOutHdr, String> {

	@Query(nativeQuery = true, value = "select * from stock_transfer_out_hdr "
			+ "where date(voucher_date)=:vdate and from_branch=:branch")
	List<StockTransferOutHdr> getVoucherNoByFromBranchAndDate(String branch, Date vdate);

	@Query(nativeQuery = true, value = "select * from stock_transfer_out_hdr "
			+ "where date(voucher_date)=:vdate and to_branch=:tobranch")
	List<StockTransferOutHdr> getVoucherNoByToBranchAndDate(String tobranch, Date vdate);

	List<StockTransferOutHdr> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,
			String voucherType);

//List<Object> findByBatchAndVoucherDate  (String batch,Date vdate);
	// here we calculating the stock tans Out summary report
	@Query(nativeQuery = true, value = "from stock_trans_out_hdr  where date(voucher_date) BETWEEN:startDate AND :endDate")
	List<StockTransferOutDtl> StockTranOUtSummary(Date startDate, Date endDate);

	/*
	 * stockTransferOutReport.setIntentNumber((String) objAray[0]);
	 * stockTransferOutReport.setVoucherDate((Date) objAray[1]);
	 * stockTransferOutReport.setVoucherNumber((String) objAray[2]);
	 * stockTransferOutReport.setVoucherType((String) objAray[3]);
	 * stockTransferOutReport.setToBranch((String) objAray[4]);
	 * stockTransferOutReport.setFromBranch((String) objAray[5]);
	 * stockTransferOutReport.setInVoucherNumber((String) objAray[6]);
	 * stockTransferOutReport.setInVoucherDate((Date) objAray[7]);
	 * stockTransferOutReport.setBatch((String) objAray[9]);
	 * stockTransferOutReport.setQty((Double) objAray[9]);
	 * stockTransferOutReport.setRate((Double) objAray[10]);
	 * stockTransferOutReport.setUnitId((String) objAray[11]);
	 * stockTransferOutReport.setBarcode((String) objAray[12]);
	 * stockTransferOutReport.setItemId((String) objAray[13]);
	 * stockTransferOutReport.setExpiryDate((Date) objAray[14]);
	 * stockTransferOutReport.setAmount((Double) objAray[15]);
	 * stockTransferOutReport.setTaxRate((Double) objAray[16]);
	 * stockTransferOutReport.setMrp((Double) objAray[17]);
	 */

	@Query(nativeQuery = true, value = " " + " select  h.intent_number," + " h.voucher_date,"

			+ " h.voucher_number,"

			+ " h.voucher_type ,  "

			+ " b2.branch_name as to_branch, "

			+ " d.batch," + " d.qty," + " d.rate," + " u.unit_name ,"

			+ " d.barcode, " + " i.item_name ,"

			+ " d.expiry_date," + " d.amount," + " d.tax_rate,d.mrp,d.sl_no,"
			+ " i.hsn_code,b.branch_name,b.branch_place ," + " b2.branch_place as to_branch_place "
			+ " from stock_transfer_out_hdr h," + " stock_transfer_out_dtl d ,"
			+ "  unit_mst u, item_mst i ,branch_mst b, branch_mst b2 "
			+ " where h.id=d.stock_transfer_out_hdr  and h.voucher_number=:vouchernumber and "
			+ " date(h.voucher_date)=:date and b.branch_code=h.from_branch and h.company_mst=:companymstid AND "
			+ " d.unit_id = u.id AND" + " d.item_id = i.id AND h.to_branch = b2.branch_code " + " order by d.sl_no ")
	List<Object> stockTransferOutReportByvoucerNumberAndDate(String vouchernumber, Date date, String companymstid);
	
	
	
	
	
	@Query(nativeQuery = true, value = " " + " select  h.intent_number," + " h.voucher_date,"

			+ " h.voucher_number,"

			+ " h.voucher_type ,  "

			+ " b2.branch_name as to_branch, "

			+ " d.batch," + " d.qty," + " d.rate," + " u.unit_name ,"

			+ " d.barcode, " + " i.item_name ,"

			+ " d.expiry_date," + " d.amount," + " d.tax_rate,d.mrp,d.sl_no,"
			+ " i.hsn_code,b.branch_name,b.branch_place ," + " b2.branch_place as to_branch_place "
			+ " from stock_transfer_out_hdr h," + " stock_transfer_out_dtl d ,"
			+ "  unit_mst u, item_mst i ,branch_mst b, branch_mst b2 "
			+ " where h.id=d.stock_transfer_out_hdr  and h.id=:hdrid and "
			+ "  b.branch_code=h.from_branch   AND "
			+ " d.unit_id = u.id AND" + " d.item_id = i.id AND h.to_branch = b2.branch_code   order by d.sl_no ")
	List<Object> stockTransferOutReportByHdrId(String hdrid);
	
	

	StockTransferOutHdr findByIdAndCompanyMstId(String stocktransferouthdrid, String companymstid);

	List<StockTransferOutHdr> findByCompanyMstId(String companymstid);

	@Query(nativeQuery = true, value = " select * from stock_transfer_out_hdr where date(voucher_date)>=:fdate "
			+ " and date(voucher_date)<=:tDate and voucher_number is not NULL and company_mst =:companyMst and from_branch=:branchCode")
	List<StockTransferOutHdr> getStockTransferHdrBetweenDate(java.sql.Date fdate, java.sql.Date tDate,
			String companyMst, String branchCode);

	@Query(nativeQuery = true, value = " select h.voucher_number from stock_transfer_out_hdr h "
			+ "where date(h.voucher_date)=:vDate and  h.company_mst =:company_mst_id and h.voucher_number is not null  order by h.voucher_number")
	List<Object> getStockVoucherNumber(String company_mst_id, Date vDate);

	List<StockTransferOutHdr> findByVoucherNumberAndVoucherDate(String vouchernumber, Date vdate);

	@Query(nativeQuery = true, value = "select h.voucher_date,h.voucher_number,"
			+ "h.to_branch,i.item_name,c.category_name," + "d.batch,i.item_code,d.expiry_date,"
			+ "d.qty,u.unit_name,d.rate,d.amount from " + "stock_transfer_out_hdr h," + "item_mst i,"
			+ "stock_transfer_out_dtl d," + "category_mst c," + "unit_mst u where "
			+ "d.stock_transfer_out_hdr=h.id and " + "i.id=d.item_id and " + "i.category_id=c.id and "
			+ "i.unit_id = u.id and date(h.voucher_date)>=:fudate and "
			+ "date(h.voucher_date)<=:tudate and h.from_branch=:branchcode and "
			+ "h.company_mst = :companymstid and h.voucher_number is not null")
	List<Object> getStockTransferDtlBetweenDate(Date fudate, Date tudate, String branchcode, String companymstid);

	@Query(nativeQuery = true, value = "select h.voucher_date,h.voucher_number,"
			+ "h.to_branch,i.item_name,c.category_name," + "d.batch,i.item_code,d.expiry_date,"
			+ "d.qty,u.unit_name,d.rate,d.amount from " + "stock_transfer_out_hdr h," + "item_mst i,"
			+ "stock_transfer_out_dtl d," + "category_mst c," + "unit_mst u where "
			+ "d.stock_transfer_out_hdr=h.id and " + "i.id=d.item_id and " + "i.category_id=c.id and "
			+ "i.unit_id = u.id and date(h.voucher_date)>=:fudate and "
			+ "date(h.voucher_date)<=:tudate and h.from_branch=:branchcode and " + "h.to_branch = :tobranch and "
			+ "h.company_mst = :companymstid")
	List<Object> getStockTransferDtlBetweenDateAndToBranch(Date fudate, Date tudate, String branchcode,
			String companymstid, String tobranch);

	@Query(nativeQuery = true, value = "select h.voucher_date,h.voucher_number,"
			+ "h.to_branch,i.item_name,c.category_name," + "d.batch,i.item_code,d.expiry_date,"
			+ "d.qty,u.unit_name,d.rate,d.amount from " + "stock_transfer_out_hdr h," + "item_mst i,"
			+ "stock_transfer_out_dtl d," + "category_mst c," + "unit_mst u where "
			+ "d.stock_transfer_out_hdr=h.id and " + "i.id=d.item_id and " + "i.category_id=c.id and "
			+ "i.unit_id = u.id and date(h.voucher_date)>=:fudate and "
			+ "date(h.voucher_date)<=:tudate and h.from_branch=:branchcode and "
			+ "h.company_mst = :companymstid and c.category_name IN (:array)")
	List<Object> getStockTransferDtlBetweenDateAndCategoryList(Date fudate, Date tudate, String branchcode,
			String companymstid, String[] array);

	@Query(nativeQuery = true, value = "select h.voucher_date,h.voucher_number,"
			+ "h.to_branch,i.item_name,c.category_name," + "d.batch,i.item_code,d.expiry_date,"
			+ "d.qty,u.unit_name,d.rate,d.amount from " + "stock_transfer_out_hdr h," + "item_mst i,"
			+ "stock_transfer_out_dtl d," + "category_mst c," + "unit_mst u where "
			+ "d.stock_transfer_out_hdr=h.id and " + "i.id=d.item_id and " + "i.category_id=c.id and "
			+ "i.unit_id = u.id and date(h.voucher_date)>=:fudate and "
			+ "date(h.voucher_date)<=:tudate and h.from_branch=:branchcode and " + "h.to_branch = :tobranch and "
			+ "h.company_mst = :companymstid and c.category_name IN (:array)")
	List<Object> getStockTransferDtlBetweenDateAndCategoryListAndToBranch(Date fudate, Date tudate, String branchcode,
			String companymstid, String[] array, String tobranch);

	@Query(nativeQuery = true, value = "select h.voucher_date,h.voucher_number,"
			+ "h.to_branch,sum(d.amount) from stock_transfer_out_hdr h,"
			+ "stock_transfer_out_dtl d where h.id = d.stock_transfer_out_hdr and date(h.voucher_date)>=:fdate and "
			+ "date(h.voucher_date)<=:tdate group by h.voucher_date,h.voucher_number," + "h.to_branch")
	List<Object> getStockTransferOutSummaryReport(Date fdate, Date tdate);

	// -------------------new version 1.4 surya end

	@Query(nativeQuery = true, value = "select * from stock_transfer_out_hdr  where voucher_number is null and company_mst=:companyMst")
	List<StockTransferOutHdr> getHoldedStockTransferOutHdr(CompanyMst companyMst);

	StockTransferOutHdr findByCompanyMstAndId(CompanyMst companyMst, String id);


	@Query(nativeQuery = true, value = "SELECT count(*)  FROM stock_transfer_out_hdr sth where date(sth.voucher_date)=:fromDate and "
			+ "sth.company_mst=:companyMst and sth.from_branch=:branchcode") 
	Double getStockTransferOutHdrCount(CompanyMst companyMst, Date fromDate, String branchcode);




	@Modifying
	@Query(nativeQuery = true, value ="delete from stock_transfer_out_dtl where "
			+ "id in (select d.id from stock_transfer_out_dtl d, stock_transfer_out_hdr h where d.stock_transfer_out_hdr=h.id and "
			+ " date(h.voucher_date)<=:date and h.company_mst=:companyMst)")
	void deleteStockTransferDtlsByVoucherDate(CompanyMst companyMst, Date date);

	
	@Modifying
	@Query(nativeQuery = true, value ="delete from stock_transfer_out_hdr where "
			+ "date(voucher_date)<=:date and company_mst=:companyMst)")
	void deleteStockTransferHdrByVoucherDate(CompanyMst companyMst, Date date);
	// -------------------new version 1.4 surya end
	
	
	
	@Query(nativeQuery = true, value = "select * from stock_transfer_out_hdr where "
			+ "date(voucher_date)>= :fdate and date(voucher_date) <= :tdate")
	List<StockTransferOutHdr> getStockTransferOutHdrBetweenDate(Date fdate, Date tdate,Pageable pageable);

	@Modifying
	@Query(nativeQuery = true, value = "update stock_transfer_out_hdr set posted_to_server='YES' where id=:hdrIds")
	void updateStockTransferPostedToServerStatus(String hdrIds);

	
	
	//==============MAP-82-count-verification-and-retry-of-bulk-publishing-in-stock-transfer=============anandu=========	
	@Query(nativeQuery = true, value ="SELECT COUNT(*) FROM stock_transfer_out_hdr WHERE company_mst=:companymstid "
			+ "AND date(voucher_date)>=:lastSuccessDate AND date(voucher_date)<=:dayendDate AND "
			+ "from_branch=:branchcode")
	int getCountofStockTransferForDateWindow(String companymstid, Date lastSuccessDate,
			Date dayendDate, String branchcode);
	//=====end=========MAP-82-count-verification-and-retry-of-bulk-publishing-in-stock-transfer=========

	
	@Query(nativeQuery = true, value ="select sum(d.qty) from stock_transfer_out_hdr h, stock_transfer_out_dtl d " 
				 		+ "where h.id=d.stock_transfer_out_hdr and h.from_branch = :branchCode "  
				 		+ "and date(h.voucher_date) >= :fudate and date(h.voucher_date) <= :tudate and d.item_id = :itemId")
	Double getStockOutQtyByItemName(Date fudate, Date tudate, String branchCode, String itemId);

}
