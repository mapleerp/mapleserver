package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.FinancialYearMst;



@Component
@Repository
public interface FinancialYearMstRepository extends JpaRepository<FinancialYearMst, Integer> {

	//version2.0
	@Modifying
	@Query(nativeQuery =true,value="update financial_year_mst set current_financial_year=0" )
	void updateFinancialYearMst();
	
	@Query(nativeQuery = true,value = "select count(*) from financial_year_mst where current_financial_year =1")
	Integer getCurrentFinancialYearCount();
	
	
	List<FinancialYearMst>findByCompanyMstId(String companymstid);
//------------------version 1.10

	@Query(nativeQuery = true,value="select  h.* "
			+ " from financial_year_mst h where h.company_mst=:companymstid and current_financial_year = 1 and "
			+ ":udate>=date(h.start_date) and  "
			+ ":udate <= date(h.end_date)")

	FinancialYearMst findByLoginDate(String companymstid, java.sql.Date udate);
	
	
	@Query(nativeQuery = true,value="select  h.* "
			+ " from financial_year_mst h where"
			+ " h.company_mst=:companymstid order by end_date DESC limit 1 ")

	FinancialYearMst findLastFY(String companymstid);
	

	@Query(nativeQuery = true,value="select max(h.end_date) "
			+ " from financial_year_mst h where h.company_mst=:companymstid ")
	java.sql.Date findMaxOfEndDate(String companymstid);
	
	
	@Query(nativeQuery = true,value="select  h.* "
			+ " from financial_year_mst h where current_financial_year = 1 ")

	FinancialYearMst getCurrentFinancialYear();

	
	
//------------------version 1.10 end

//	List<FinancialYearMst> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
	
	List<FinancialYearMst> findByStartDateAndEndDate(java.sql.Date sdate,java.sql.Date edate);

	
	@Modifying
	@Query(nativeQuery =true,value="update financial_year_mst set current_financial_year=0,open_close_status='CLOSE'" )
	void updateStatusAndCurrentYearOfFYMst();
	
}
