package com.maple.restserver.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.annotation.QueryAnnotation;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.OtherBranchSalesTransHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.report.entity.KOTItems;
import com.maple.restserver.report.entity.SalesInvoiceReport;

@Repository
public interface SalesTransHdrRepository extends JpaRepository<SalesTransHdr, String> {

	@Query(nativeQuery = true, value = "select distinct(ac.id) from account_heads ac,sales_trans_hdr sh,"
			+ "sales_receipts sr where sh.account_heads = ac.id "
			+ "and sr.sales_trans_hdr_id = sh.id and date(sh.voucher_date) >= :fdate and "
			+ "date(sh.voucher_date) <= :tdate and sr.receipt_mode = 'CREDIT'")
	List<String> getCustId(Date fdate, Date tdate);

	@Modifying
	@Query(nativeQuery = true, value = "delete from sales_trans_hdr where voucher_number is null")
	void deleteNullVoucherHdr();

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr where "
			+ "date(voucher_date)>= :fdate and date(voucher_date) <= :tdate")
	List<SalesTransHdr> getSalesTransHdrBetweenDate(Date fdate, Date tdate, Pageable topFifty);

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr where voucher_number=:vouchernumber and "
			+ "date(voucher_date)=:date and company_mst=:companymstid")
	SalesTransHdr findByVoucherNumberAndVoucherDateAndCompanyMstId(String vouchernumber, Date date,
			String companymstid);

	List<SalesTransHdr> findByVoucherNumber(String voucherNumber);

	SalesTransHdr findByVoucherNumberAndBranchCode(String voucherNumber, String branchCode);

	@Query(nativeQuery = true, value = " select * from sales_trans_hdr h where date(h.voucher_date) >= :voucherDate  ")
	List<SalesTransHdr> findByVoucherDateGreaterEqual(Date voucherDate);

	@Query(nativeQuery = true, value = " select * from sales_trans_hdr h where numeric_voucher_number >= :numericVoucher ")
	List<SalesTransHdr> findByVoucherNumberGreateEqual(int numericVoucher);

	@Query(nativeQuery = true, value = "select sum(invoice_amount) as totalAmt from sales_trans_hdr "
			+ "where date(voucher_date)=:voucherDate and branch_code = :branchcode")

	Double sumofTotalSale(Date voucherDate, String branchcode);

	@Query(nativeQuery = true, value = " select * from sales_trans_hdr h where h.VOUCHER_NUMBER like :voucherNumber  ")
	List<SalesTransHdr> findByVoucherNumberLike(String voucherNumber);

	@Query(nativeQuery = true, value = " select h.* from sales_trans_hdr h, tally_failed_voucher_mst  f "
			+ " where h.VOUCHER_NUMBER = f.VOUCHER_NUMBER  ")
	List<SalesTransHdr> findFailedVouchers();

	@Query(nativeQuery = true, value = " select voucher_number from sales_trans_hdr h where h.VOUCHER_NUMBER like :prefix%  ")
	List findByVoucherNumberLikePrefix(String prefix);

	@Modifying
	@Query(nativeQuery = true, value = " update sales_trans_hdr set voucher_number =:newvoucher where voucher_number =:oldvoucher ")
	void updateVoucherNumber(String newvoucher, String oldvoucher);

	@Modifying
	@Query(nativeQuery = true, value = " update sales_trans_hdr set voucher_date = :newDate "
			+ "  where  numeric_voucher_number >= :startvoucher  and numeric_voucher_number<= :endvoucher "
			+ " AND date(voucher_date) = :oldDate  ")
	void updateVoucherDate(int startvoucher, int endvoucher, java.sql.Date oldDate, java.sql.Date newDate);
	
	@Query(nativeQuery = true, value = " select count(*) from sales_trans_hdr where company_mst=:companymstid and date(voucher_date)>=:salesBestDate and "
			+ "date(voucher_date)<=:dayendDate and branch_code=:branchcode")
	int getCountofSalesForDateWindow(String companymstid , Date  salesBestDate , Date dayendDate , String branchcode);

	/*
	 * @Modifying
	 * 
	 * @Query(nativeQuery=true,
	 * value=" update account_class set trans_date = :newDate " +
	 * "  where  source_voucher_number in (select voucher_number from sales_trans_hdr where numeric_voucher_number >= :startvoucher  and numeric_voucher_number<= :endvoucher "
	 * + " AND date(voucher_date) = :oldDate)  ") void
	 * updateAccountClassVoucherDate(int startVoucher, int endVoucher ,java.sql.Date
	 * oldDate,java.sql.Date newDate);
	 * 
	 * 
	 * @Modifying
	 * 
	 * @Query(nativeQuery=true,
	 * value=" update ledger_class set trans_date = :newDate " +
	 * "  where  source_voucher_number in (select voucher_number from sales_trans_hdr where numeric_voucher_number >= :startvoucher  and numeric_voucher_number<= :endvoucher "
	 * + " AND date(voucher_date) = :oldDate)  ") void
	 * updateLedgerClassVoucherDate(int startVoucher, int endVoucher,java.sql.Date
	 * oldDate, java.sql.Date newDate);
	 * 
	 * 
	 * @Modifying
	 * 
	 * @Query(nativeQuery=true,
	 * value=" update debit_class set trans_date = :newDate " +
	 * "  where  source_voucher_number in (select voucher_number from sales_trans_hdr where numeric_voucher_number >= :startvoucher  and numeric_voucher_number<= :endvoucher "
	 * + " AND date(voucher_date) = :oldDate)  ")
	 * 
	 * void updateDebitClassVoucherDate(int startVoucher,int
	 * endVoucher,java.sql.Date oldDate,java.sql.Date newDate);
	 * 
	 * @Modifying
	 * 
	 * @Query(nativeQuery=true,
	 * value=" update credit_class set trans_date = :newDate " +
	 * "  where  source_voucher_number in (select voucher_number from sales_trans_hdr where numeric_voucher_number >= :startvoucher  and numeric_voucher_number<= :endvoucher "
	 * + " AND date(voucher_date) = :oldDate)  ")
	 * 
	 * void updateCreditClassVoucherDate(int startVoucher, int
	 * endVoucher,java.sql.Date oldDate,java.sql.Date newDate);
	 */
	@Modifying
	@Query(nativeQuery = true, value = " update sales_trans_hdr set invoice_number_prefix =:newvoucher where voucher_number =:oldvoucher ")
	void updateinvoice_number_prefix(String newvoucher, String oldvoucher);

	@Query(nativeQuery = true, value = " select * from sales_trans_hdr h where date(h.voucher_date) = :voucherDate AND h.VOUCHER_NUMBER = :voucherNumber ")
	List<SalesTransHdr> findByVoucherNumberAndVoucherDate(String voucherNumber, Date voucherDate);

	List<SalesTransHdr> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,
			String voucherType);

	// here we are finding the sales summary of given dates

	@Query(nativeQuery = true, value = "select *  from sales_trans_hdr  where date(voucher_date) BETWEEN:startDate AND :endDate")
	List<SalesTransHdr> salesSummaryReport(Date startDate, Date endDate);

	@Query(nativeQuery = true, value = " select c.*  from Sales_Trans_Hdr  c where c.sales_Man_Id = :salesManId AND "
			+ "c.serving_Table_Name = :stableName AND " + " c.voucher_Number is null ")
	List<SalesTransHdr> getInprogressKotInvoices(String salesManId, String stableName);

	@Query(nativeQuery = true, value = " select c.*  from Sales_Trans_Hdr  c where c.sales_Man_Id = :salesManId AND "
			+ "c.serving_Table_Name = :stableName AND c.customise_sales_mode= :customSalesMode AND "
			+ " c.voucher_Number is null ")
	List<SalesTransHdr> getInprogressKotInvoicesCustomiseSalesmode(String salesManId, String stableName,
			String customSalesMode);

	@Query(nativeQuery = true, value = " select c.*  from Sales_Trans_Hdr  c where  c.customer_id = :stableName AND "
			+ " c.voucher_Number is null ")
	List<SalesTransHdr> getInprogressWholeSaleInvoices(String stableName);

	@Query(nativeQuery = true, value = " select c.*  from Sales_Trans_Hdr  c where c.customer_id = :custId AND "
			+ " c.voucher_Number is null ")
	List<SalesTransHdr> getInprogressPosInvoices(String custId);

	@Query(nativeQuery = true, value = "select c.*  from Sales_Trans_Hdr  c where c.sales_Man_Id = :salesManId AND "
			+ "c.serving_Table_Name = :stableName AND " + "c.voucher_Number is null")
	List<SalesTransHdr> printKotByTablenameAndSalesman(String salesManId, String stableName);

	@Query(nativeQuery = true, value = "select c.*  from Sales_Trans_Hdr  c where c.id = :salesTransHdrId " + " AND  "
			+ "c.voucher_Number is null")
	List<SalesTransHdr> printKotBySalesTransHdr(String salesTransHdrId);

	@Query(nativeQuery = true, value = "select c.*  from Sales_Trans_Hdr  c where c.sales_Man_Id = :salesManId AND c.serving_Table_Name = :stableName AND "
			+ "c.voucher_Number is null")
	List<SalesTransHdr> printKotInvoiceByTablenameAndSalesman(String salesManId, String stableName);

	@Query(nativeQuery = true, value = " select c.VOUCHER_DATE,VOUCHER_NUMBER,ORDER_MESSAGE_TO_PRINT,"
			+ "ORDER_ADVICE , i.item_name ,QTY, MRP  from Sales_Order_Trans_Hdr  c ,"
			+ " sales_order_dtl d , item_mst i " + " where c.id = d.SALES_TRANS_HDR_ID " + " AND d.item_id = i.id AND "
			+ " c.voucher_Number is not null ")

	List<Object> getdailySales();

	@Query(nativeQuery = true, value = " select h.sales_mode,d.qty, d.mrp*d.qty "
			+ "  from  sales_trans_hdr h ,  sales_dtl d " + "  where  d.sales_trans_hdr_id=h.id  ")

	List<Object> onlineSaleReport();

	@Query(nativeQuery = true, value = " select h.sales_mode,d.qty, " + "  from  sales_trans_hdr h ,  sales_dtl d "
			+ "  where  d.sales_trans_hdr_id=h.id GROUP BY h.sales_mode,d.qty sum(d.mrp*d.qty)")

	List<Object> onlineSaleSummaryReport();
//
//	@Query(nativeQuery = true, value = "  select bm.branch_name, \n" + "bm.branch_address1, \n"
//			+ "bm.branch_address2,\n" + "bm.branch_Place, \n" + "bm.branch_Tel_No, \n"
//			+ "bm.branch_Gst salesTransHdrRepository, \n" + "bm.branch_state,\n" + "bm.branch_Email,\n"
//			+ "bm.bank_name, \n" + "bm.account_number, \n" + "bm.bank_branch, \n" + "bm.ifsc, \n" + "sh.voucher_date,\n"
//			+ "sh.voucher_number, \n" + "cm.customer_name,\n" + "cm.customer_place,\n" + "cm.customer_state,\n"
//			+ "cm.customer_gst,\n" + "im.item_name, \n" + "d.tax_rate, \n" + "im.hsn_code, \n" + "d.rate, d.qty,\n"
//			+ "u.unit_name, \n" + "d.discount as detail_discount , \n" + " sh.invoice_amount    ,  " + " "
//			+ "   d.sgst_amount    , \n" + "     d.cgst_amount     ,  \n" + "       d.igst_amount   ,  \n"
//			+ "   sgst_tax_rate , \n" + "   cgst_tax_rate   , " + "   igst_tax_rate, \n " + "   d.cess_rate, \n "
//			+ "   d.cess_amount, \n " +
//
//			" bm.branch_website," + " cm.customer_contact, " + " rate * qty , " + "sh.discount , "
//			+ " sh.invoice_discount, sh.sales_mode, d.mrp,d.batch,d.standard_price,d.warranty_serial, "
//			+ " d.list_price, im.item_code , d.expiry_date "
//			+ "  from branch_mst bm, sales_trans_hdr sh, sales_dtl d, customer_mst cm, unit_mst u,\n"
//			+ "item_mst im  where sh.branch_code = bm.branch_code "
//			+ " and sh.customer_id = cm.id and sh.id = d.sales_trans_hdr_id " + " and d.item_id = im.id "
//			+ "and d.unit_id = u.id  " + " and sh.voucher_number = :vnumber  " + " and date(sh.voucher_date) = :vdate "
//			+ " and sh.company_mst = :company_mst_id \n")

	@Query(nativeQuery = true, value = "  select bm.branch_name, \n" + "bm.branch_address1, \n"
			+ "bm.branch_address2,\n" + "bm.branch_Place, \n" + "bm.branch_Tel_No, \n"
			+ "bm.branch_Gst salesTransHdrRepository, \n" + "bm.branch_state,\n" + "bm.branch_Email,\n"
			+ "bm.bank_name, \n" + "bm.account_number, \n" + "bm.bank_branch, \n" + "bm.ifsc, \n" + "sh.voucher_date,\n"
			+ "sh.voucher_number, \n" + "cm.account_name,\n" + "cm.customer_place,\n" + "cm.customer_state,\n"
			+ "cm.party_gst,\n" + "im.item_name, \n" + "d.tax_rate, \n" + "im.hsn_code, \n" + "d.rate, d.qty,\n"
			+ "u.unit_name, \n" + "d.discount as detail_discount , \n" + " sh.invoice_amount    ,  " + " "
			+ "   d.sgst_amount    , \n" + "     d.cgst_amount     ,  \n" + "       d.igst_amount   ,  \n"
			+ "   sgst_tax_rate , \n" + "   cgst_tax_rate   , " + "   igst_tax_rate, \n " + "   d.cess_rate, \n "
			+ "   d.cess_amount, \n " +

			" bm.branch_website," + " cm.customer_contact, " + " rate * qty , " + "sh.discount , "
			+ " sh.invoice_discount, sh.sales_mode, d.mrp,d.batch,d.standard_price,d.warranty_serial, "
			+ " d.list_price, im.item_code , d.expiry_date "
			+ "  from branch_mst bm, sales_trans_hdr sh, sales_dtl d, account_heads cm, unit_mst u,\n"
			+ "item_mst im  where sh.branch_code = bm.branch_code "
			+ " and sh.customer_id = cm.id and sh.id = d.sales_trans_hdr_id " + " and d.item_id = im.id "
			+ "and d.unit_id = u.id  " + " and sh.voucher_number = :vnumber  " + " and date(sh.voucher_date) = :vdate "
			+ " and sh.company_mst = :company_mst_id \n")
	/*
	 * "   d.cess_rate, \n " + "   d.cess_amount, \n " +
	 * 
	 */
	/*
	 * "group by bm.branch_name, \n" + "bm.branch_address1, \n" +
	 * "bm.branch_address2,\n" + "bm.branch_Place, \n" + "bm.branch_Tel_No, \n" +
	 * "bm.branch_Gst , \n" + "bm.branch_state,\n" + "bm.branch_Email,\n" +
	 * "bm.bank_name, \n" + "bm.account_number, \n" + "bm.bank_branch, \n" +
	 * "bm.ifsc, \n" + "sh.voucher_date,\n" + "sh.voucher_number, \n" +
	 * "cm.customer_name,\n" + "cm.customer_place,\n" + "cm.customer_state,\n" +
	 * "cm.customer_gst,\n" + "im.item_name, \n" + "im.tax_rate, \n" +
	 * "im.hsn_code, \n" + "d.rate, d.qty,\n" + "u.unit_name, \n" + "d.discount ," +
	 * " d.tax_rate , bm.branch_website,cm.cm.customer_contact," +
	 * " sgst_tax_rate ,  " + "   cgst_tax_rate ,  " + "	  igst_tax_rate  ," +
	 * " sh.discount , " + " sh.invoice_discount
	 */

	List<Object> salesInvoice(String company_mst_id, String vnumber, Date vdate);

	@Query(nativeQuery = true, value = "  select bm.branch_name, \n" + "bm.branch_address1, \n"
			+ "bm.branch_address2,\n" + "bm.branch_Place, \n" + "bm.branch_Tel_No, \n" + "bm.branch_Gst , \n"
			+ "bm.branch_state,\n" + "bm.branch_Email,\n" + "bm.bank_name, \n" + "bm.account_number, \n"
			+ "bm.bank_branch, \n" + "bm.ifsc, \n" + "sh.voucher_date,\n" + "sh.voucher_number, \n"
			+ "cm.account_name,\n" + "cm.customer_place,\n" + "cm.customer_state,\n" + "cm.party_gst,\n"
			+ "im.item_name,  \n" + "  (d.tax_rate   ), \n" + "im.hsn_code, \n" + "d.rate, d.qty,\n" + "u.unit_name, \n"
			+ "d.discount, \n" + "  (SUM((d.rate*d.qty*d.tax_rate/100) + (d.rate*d.qty ) ) ) \n" + "as amount , "
			+ "   (SUM(d.sgst_amount) )  as sgst_amount , \n" + "    (SUM( d.cgst_amount ) )  as cgst_amount ,  \n"
			+ "    (SUM( d.igst_amount ) )  as igst_amount ,  \n" + "   sgst_tax_rate , \n" + "   cgst_tax_rate \n , "
			+ "   igst_tax_rate \n , "
			+ " from branch_mst bm, sales_trans_hdr sh, sales_dtl d, account_heads cm, unit_mst u,\n"
			+ " item_mst im  where sh.branch_code = bm.branch_code and sh.customer_id = cm.id "
			+ "and sh.id = d.sales_trans_hdr_id " + "and d.item_id = im.id " + "and d.unit_id = u.id \n"
			+ " and sh.company_mst = :company_mst_id \n" + "group by bm.branch_name, \n" + "bm.branch_address1, \n"
			+ "bm.branch_address2,\n" + "bm.branch_Place, \n" + "bm.branch_Tel_No, \n" + "bm.branch_Gst , \n"
			+ "bm.branch_state,\n" + "bm.branch_Email,\n" + "bm.bank_name, \n" + "bm.account_number, \n"
			+ "bm.bank_branch, \n" + "bm.ifsc, \n" + "sh.voucher_date,\n" + "sh.voucher_number, \n"
			+ "cm.account_name,\n" + "cm.customer_place,\n" + "cm.customer_state,\n" + "cm.party_gst,\n"
			+ "im.item_name, \n" + "d.tax_rate, \n" + "im.hsn_code, \n" + "d.rate, d.qty,\n" + "u.unit_name, \n"
			+ "d.discount, ")

	List<Object> salesInvoiceAll(String company_mst_id);

	@Query(nativeQuery = true, value = "select h.voucher_number from SALES_TRANS_HDR h "
			+ "where  date(h.VOUCHER_DATE) =:vDate " + " and h.company_mst =:company_mst_id and h.voucher_number is not null  order by h.voucher_number")
	List<Object> getsalesVoucherNumber(String company_mst_id, Date vDate);

	@Query(nativeQuery = true, value = "select i.item_code, i.hsn_code, d.tax_rate ,  "
			+ "   d.qty  , d.rate, i.item_name,  d.mrp * d.qty ,  u.unit_name , d.mrp  from "
			+ " sales_trans_hdr h, sales_dtl d, item_mst i, unit_mst u " + " where h.id= d.sales_trans_hdr_id and "
			+ " d.item_id = i.id and d.unit_id = u.id and " + " h.id = :salesTransHdrId "
			+ " and h.company_mst =:company_mst_id ")
	List<Object> getItemBySalesTransHdr(String company_mst_id, String salesTransHdrId);

	Optional<SalesTransHdr> findByIdAndCompanyMstId(String salestranshdrid, String companymstid);

	@Query(nativeQuery = true, value = "select d.tax_rate , " + "  (sum(d.rate*d.qty*d.tax_rate/100)   ),  "
			+ "   (SUM(d.sgst_amount)   )  as sgst_amount , \n" + "     (SUM( d.cgst_amount )   )  as cgst_amount ,  \n"
			+ "   (SUM( d.igst_amount )  )  as igst_amount ,  \n" + "   sgst_tax_rate , \n" + "   cgst_tax_rate \n , "
			+ "   igst_tax_rate \n ,"

			+ " sum(d.rate * d. qty) " + "from sales_dtl d,sales_trans_hdr h"
			+ " where d.sales_trans_hdr_id=h.id and h.voucher_number=:voucherNumber "
			+ "and date(h.voucher_date) =:date and h.company_mst=:companymstid" + " group by d.tax_rate , "
			+ "   sgst_tax_rate , \n" + "   cgst_tax_rate \n , " + "   igst_tax_rate \n " + "   ")

	List<Object> getTaxSummary(String companymstid, String voucherNumber, Date date);

	@Query(nativeQuery = true, value = "select i.item_name," + " u.unit_name," + " d.qty," + " d.rate,"
			+ " c.category_name " + " from sales_order_dtl d  ," + " sales_trans_hdr h," + " category_mst c ,"
			+ " item_mst i," + " unit_mst u" + " where d.item_id= i.id and " + " d.unit_id = u.id and"
			+ " i.category_id = c.id " + " h.id = d.sales_trans_hdr_id  "
			+ "and i.id = :itemid and date(h.voucher_date) =:date")
	List<Object> getItemSaleReport(String itemid, Date date);

	@Query(nativeQuery = true, value = "select sum(h.invoice_amount) as dailysales,sum(h.cash_pay),sum(h.cardamount) "
			+ "from sales_trans_hdr h " + " where  date(h.voucher_date) =:date and "
			+ "h.branch_code=:branchCode and h.voucher_number is not null")
	List<Object> findDailySalesReport(Date date, String branchCode);

	@Query(nativeQuery = true, value = "select sum(d.qty*d.mrp) from " + "  sales_trans_hdr h,sales_dtl d "
			+ " where d.item_id in (select item_id  from  item_temp_mst )  and h.id = d.sales_trans_hdr_id and h.branch_code=:branchCode "
			+ " and date(h.voucher_date) =:date")
	List<Object> findCard2Report(Date date, String branchCode);


//	@Query(nativeQuery = true, value = "select h.voucher_number,c.customer_name, h.sales_mode , "
//			+ "sr.receipt_amount,sr.receipt_mode,h.voucher_date from sales_trans_hdr h,customer_mst c ,sales_receipts sr "
//
//			+ " WHERE  h.branch_code=:branchcode and "
//			+ "sr.sales_trans_hdr_id = h.id and date(h.voucher_date)>=:fdate and date(h.voucher_date)<=:tdate and h.company_mst=:companymstid and c.id=h.customer_id and h.voucher_number is not null order by  h.voucher_number ")

	@Query(nativeQuery = true, value = "select h.voucher_number,c.account_name, h.sales_mode , "
			+ "sr.receipt_amount,sr.receipt_mode,h.voucher_date from sales_trans_hdr h,account_heads c ,sales_receipts sr "

			+ " WHERE  h.branch_code=:branchcode and "
			+ "sr.sales_trans_hdr_id = h.id and date(h.voucher_date)>=:fdate and date(h.voucher_date)<=:tdate and h.company_mst=:companymstid and c.id=h.customer_id and h.voucher_number is not null order by  h.voucher_number ")

	List<Object> getDailySalesDtl(String branchcode, Date fdate, Date tdate, String companymstid);

	@Query(nativeQuery = true, value = "select h.voucher_number,c.account_name, h.sales_mode , h.invoice_amount,h.voucher_date from sales_return_hdr h,account_heads c "

			+ " WHERE  h.branch_code=:branchcode and date(h.voucher_date) between :fdate and :tdate and h.company_mst=:companymstid and c.id=h.customer_id  order by  h.voucher_number ")

	List<Object> getDailySalesReturnDtl(String branchcode, Date fdate, Date tdate, String companymstid);

	@Query(nativeQuery = true, value = " select " + " h.voucher_date , "

			+ " i.item_name  , sum(d.qty),sum(d.qty*d.mrp)as value,c.company_name,b.branch_name,b.branch_address1,b.branch_tel_no,b.branch_state,b.branch_email,b.branch_website "

			+ "  from sales_trans_hdr h  ," + " sales_dtl d ,  item_mst i,branch_mst b,company_mst c "
			+ "where  h.branch_code=:branchcode and date(h.voucher_date) =:date and h.company_mst=:companymstid"
			+ " and d.item_id = i.id and d.sales_trans_hdr_id = h.id and b.branch_code=h.branch_code and c.id=h.company_mst "

			+ "  group  by  h.voucher_date ,  i.item_name,c.company_name,b.branch_name,b.branch_address1,b.branch_tel_no,b.branch_state,b.branch_email,b.branch_website  ")

	List<Object> getDailyItemWiseSalesReportdtl(String branchcode, String date, String companymstid);

	@Query(nativeQuery = true, value = " select  " + "h.voucher_date ,  " + "i.item_name  , " + "sum(d.qty), "
			+ "b.branch_name," + "b.branch_website ," + "b.branch_email," + " cm.company_name"
			+ " from sales_trans_hdr h  , sales_dtl d , item_mst i ," + "category_mst c ,"
			+ "branch_mst b, company_mst cm "
			+ "WHERE  h.branch_code=:branchcode and date(h.voucher_date)  between :fromDate and :toDate and h.company_mst=:companymstid"
			+ " and d.item_id = i.id and d.sales_trans_hdr_id = h.id "
			+ "  group  by    h.voucher_date ,  i.item_name,b.branch_name,b.branch_website,b.branch_email,cm.company_name  ")

	List<Object> getDailyItemWiseMonthlySalesReportdtl(String branchcode, String fromDate, String toDate,
			String companymstid);

	// this url is not working in day end checking because sales mode pos
	// not checking

	@Query(nativeQuery = true, value = " select * from  sales_trans_hdr h where " + "h.voucher_number is not null and "
			+ "date(h.voucher_date)=:reportdate and h.branch_code=:branchCode and " + "h.sales_mode='B2C'")
	List<SalesTransHdr> findByVoucherDate(Date reportdate, String branchCode);

	// New url with pos mode checking
	@Query(nativeQuery = true, value = " select * from  sales_trans_hdr h where " + "h.voucher_number is not null and "
			+ "date(h.voucher_date)=:reportdate and "
			+ "(h.sales_mode='B2C' or h.sales_mode ='POS' or h.sales_mode ='KOT' or " + "h.sales_mode ='ONLINE')")
	List<SalesTransHdr> findByVoucherDateWithSalesMode(Date reportdate);

	@Query(nativeQuery = true, value = "select " + " h.voucher_date , " + " i.item_name  , " + "sum(d.qty),"
			+ "sum(d.mrp*d.qty) as value  " + " from sales_trans_hdr h  ," + " sales_dtl d , item_mst i "
			+ " where  h.branch_code=:branchcode and date(h.voucher_date)  between :reportdate and :todate and h.company_mst=:companymstid"
			+ " and i.id=d.item_id  and d.sales_trans_hdr_id = h.id and i.id=:itemid  group  by    h.voucher_date , "
			+ " i.item_name ")
	List<Object> getitemWiseSalesReportDateBetweenDates(String branchcode, String reportdate, String todate,
			String companymstid, String itemid);

	@Query(nativeQuery = true, value = "select h.voucher_date," + "i.item_name," + "sum(d.qty),b.branch_name,"
			+ "b.branch_website ," + "b.branch_email,"
			+ " cm.company_name,sum(d.qty*d.mrp) as value,b.branch_address1,b.branch_tel_no,b.branch_state"
			+ " from sales_trans_hdr h  ," + "sales_dtl d , " + "item_mst i, " + "category_mst c ,"
			+ "branch_mst b, company_mst cm "
			+ " WHERE  h.branch_code=:branchcode and date(h.voucher_date)  between :reportdate and :todate and h.company_mst=:companymstid and b.branch_code=h.branch_code and cm.id=h.company_mst  "
			+ "and d.item_id = i.id and d.sales_trans_hdr_id = h.id and c.id=i.category_id and i.category_id=:categoryid  group  by h.voucher_date,i.item_name,b.branch_name,b.branch_website ,b.branch_email,cm.company_name,b.branch_address1,b.branch_tel_no,b.branch_state ")
	List<Object> getcategoryWiseSalesReportDateBetweenDates(String branchcode, Date reportdate, Date todate,
			String companymstid, String categoryid);

	@Query(nativeQuery = true, value = "select" + " c.account_name," + "c.party_gst," + "h.voucher_number,"
			+ "h.paytm_amount," + "h.cardamount," + "h.cash_pay," + "ar.paid_amount,h.credit_amount, "
			+ "h.invoice_amount," + "b.branch_code," + "b.branch_name," + "b.branch_address1," + "b.branch_tel_no,"
			+ "cm.company_name , h.voucher_date " + "from " + " sales_trans_hdr h," + "account_heads c,"
			+ "branch_mst b,company_mst cm,account_receivable ar" + " where " + "c.id=h.customer_id "
			+ "and b.branch_code=h.branch_code " + "and h.branch_code=:branchCode and cm.id=h.company_mst "

			+ " and date(h.voucher_date ) between :date and :toDate and  "
			+ "cm.id=h.company_mst and h.company_mst=:companymst and "
			+ "h.sales_mode='B2B' and ar.voucher_number=h.voucher_number")

	List<Object> getB2BSalesReport(Date date, Date toDate, String branchCode, CompanyMst companymst);

	@Query(nativeQuery = true, value = "select c.account_name,c.party_gst," + "h.voucher_number,"
			+ "h.voucher_date,h.invoice_amount,d.tax_rate,d.rate*d.qty from " + " sales_trans_hdr h,"
			+ "account_heads c,sales_dtl d " + " where " + "c.id=h.customer_id "
			+ "and h.branch_code=:branchCode  and h.id= d.sales_trans_hdr_id "

			+ " and date(h.voucher_date )>= :date and date(h.voucher_date )<= :toDate and  "
			+ "h.company_mst=:companymst and " + "h.sales_mode='B2B' order by d.tax_rate")

	List<Object> getB2BSalesReportdtl(Date date, Date toDate, String branchCode, CompanyMst companymst);

	@Query(nativeQuery = true, value = "select b.branch_name,m.item_name,d.qty,d.tax_rate,d.mrp,u.unit_name,d.amount,c.account_name from sales_trans_hdr h,sales_dtl d,item_mst m,branch_mst b,account_heads c,unit_mst u where "
			+ "h.company_mst=:companymstid and h.voucher_number=:vouchernumber and b.branch_code=h.branch_code and h.branch_code=:branchcode and d.item_id = m.id and h.id=d.sales_trans_hdr_id and c.id=h.customer_id and u.id=d.unit_id")
	List<Object> getVoucherReprint(String companymstid, String vouchernumber, String branchcode);

	@Query(nativeQuery = true, value = "select b.branch_name,m.item_name,d.qty,d.tax_rate,d.mrp,u.unit_name,d.amount,c.account_name from sales_return_hdr h,sales_return_dtl d,item_mst m,branch_mst b,account_heads c,unit_mst u where "
			+ "h.company_mst=:companymstid and h.voucher_number=:vouchernumber and b.branch_code=h.branch_code and h.branch_code=:branchcode and d.item_id = m.id and h.id=d.sales_return_hdr_id and c.id=h.customer_id and u.id=d.unit_id")
	List<Object> getSalesReturnVoucherReprint(String companymstid, String vouchernumber, String branchcode);

	@Query(nativeQuery = true, value = "select h.id, h.serving_table_name,h.sales_man_id,h.customise_sales_mode "
			+ "from sales_trans_hdr h where  h.company_mst=:companymstid and h.branch_code=:branchcode "
			+ "and h.customise_sales_mode=:customiseSalesMode " + "and h.voucher_number is null "
			+ "and h.serving_table_name is not null ")
	List<Object> retrivePalcedTable(String companymstid, String branchcode, String customiseSalesMode);

//		@Query(nativeQuery = true,value="select lc.localcustomer_name,"
//				+ "lc.address,"
//				+ "lc.phone_no,"
//				+ "lc.phoneno2,cm.customer_name,"
//				+ "cm.customer_address,"
//				+ "cm.customer_place,"
//				+ "cm.customer_contact "
//				+ "from sales_trans_hdr h,"
//				+ "sales_order_trans_hdr oh,customer_mst cm,"
//				+ "local_customer_mst lc where oh.id=h.sale_order_hrd_id "
//				+ " and h.company_mst=:company_mst_id and "
//				+ "h.voucher_number=:vnumber and h.voucher_date=:vdate and cm.id=h.customer_id and oh.local_customer_id=lc.id")
//		
//
//	@Query(nativeQuery = true, value = "select cm.localcustomer_name," + "cm.address," + "cm.phone_no,"
//			+ "cm.address_line1,cm.address_line2,cm.land_mark " + "from local_customer_mst cm,sales_trans_hdr h "
//			+ "where h.local_customer_mst=cm.id and date(h.voucher_date ) =:vdate "
//			+ "and h.voucher_number=:vnumber and h.company_mst=:company_mst_id ")
	

	@Query(nativeQuery = true, value = "select cm.localcustomer_name," + "cm.address," + "cm.phone_no,"
			+ "cm.address_line1,cm.address_line2,cm.land_mark " + "from local_customer_mst cm,sales_trans_hdr h "
			+ "where h.local_customer_mst=cm.id and date(h.voucher_date ) =:vdate "
			+ "and h.voucher_number=:vnumber and h.company_mst=:company_mst_id ")

	List<Object> getTaxInvoiceCustomer(String company_mst_id, String vnumber, Date vdate);

	//@Query(nativeQuery = true, value = "select cm.customer_name,cm.customer_address,cm.customer_state,cm.customer_contact,cm.customer_gst from customer_mst cm,sales_trans_hdr h where h.customer_id=cm.id and date(h.voucher_date) =:vdate and h.voucher_number=:vnumber and h.company_mst=:company_mst_id ")
	
	@Query(nativeQuery = true, value = "select cm.account_name,cm.party_address1,cm.customer_state,cm.customer_contact,cm.party_gst from account_heads cm,sales_trans_hdr h where h.customer_id=cm.id and date(h.voucher_date) =:vdate and h.voucher_number=:vnumber and h.company_mst=:company_mst_id ")

	List<Object> getTaxInvoiceGstCustomer(String company_mst_id, String vnumber, Date vdate);

	@Query(nativeQuery = true, value = "select sum(d.cess_amount) as cessamount from sales_trans_hdr h,sales_dtl d where h.id=d.sales_trans_hdr_id and h.voucher_number=:vouchernumber and date(h.voucher_date) =:date ")
	Double getCessAmount(String vouchernumber, Date date);

	// List<Object> getCustomerLedgerReport( CompanyMst companymst, String
	// customerid, String startdate,
	// String enddate, String branchcode);

	@Query(nativeQuery = true, value = "select h.voucher_number,c.account_name from sales_trans_hdr h,account_heads c "
			+ "where  h.customer_id=c.id and (h.sales_mode='B2B' OR h.sales_mode='B2C')  and date(h.voucher_date) =:vDate "
			+ "and h.company_mst=:companymst and h.branch_code=:branchcode " + "union " + "select h.voucher_number,"
			+ "c.account_name from sales_trans_hdr h,account_heads c where  h.customer_id=c.id and h.sales_mode='B2C' "
			+ " and date(h.voucher_date) =:vDate and h.company_mst=:companymst and h.branch_code=:branchcode ")
	List<Object> getCustomerAndVouchernumber(String branchcode, CompanyMst companymst, Date vDate);

	@Query(nativeQuery = true, value = "select h.voucher_number,c.account_name,h.sales_man_id from sales_trans_hdr h,account_heads c "
			+ "where  h.customer_id=c.id and (h.sales_mode='B2B' OR h.sales_mode='B2C')  and date(h.voucher_date) =:vDate "
			+ "and h.company_mst=:companymst and h.branch_code=:branchcode ")
	List<Object> getSalesCustomerAndVouchernumber(String branchcode, CompanyMst companymst, Date vDate);
	/*
	 * 
	 * @Query(nativeQuery = true,
	 * value="select cast (SUM(d.sgst_amount) as decimal(10,2) )  as sgst_amount ,cast(SUM( d.cgst_amount )  as decimal(10,2) )  as cgst_amount , cast (SUM( d.igst_amount )  as decimal(10,2) )  as igst_amount from sales_trans_hdr h,sales_dtl d where h.id=d.sales_trans_hdr_id and h.voucher_number=:voucherNumber and h.voucher_date=:date and h.company_mst=:companymstid "
	 * ) List<Object> getSumOfTaxAmounts(String companymstid, String
	 * voucherNumber,Date date);
	 * 
	 */

	@Query(nativeQuery = true, value = "select (SUM(d.sgst_amount))  as sgst_amount ,(SUM( d.cgst_amount ))  as cgst_amount , (SUM( d.igst_amount ))  as igst_amount from sales_trans_hdr h,sales_dtl d where h.id=d.sales_trans_hdr_id and h.voucher_number=:voucherNumber and date(h.voucher_date) =:date and h.company_mst=:companymstid ")
	List<Object> getSumOfTaxAmounts(String companymstid, String voucherNumber, Date date);

	@Query(nativeQuery = true, value = "select ( sum(d.qty*d.rate) ) as ttaxableAmount  from sales_trans_hdr h,sales_dtl d where h.id=d.sales_trans_hdr_id and h.voucher_number=:voucherNumber and date(h.voucher_date) =:date and h.company_mst=:companymstid ")
	Double getTaxableAmount(String voucherNumber, Date date, String companymstid);

	@Query(nativeQuery = true, value = "select sum(d.qty*d.rate) from sales_trans_hdr h,sales_dtl d where h.id=d.sales_trans_hdr_id and d.tax_rate>0 and h.voucher_number=:vouchernumber and date( h.voucher_date) =:date")
	Double getNonTaxableAmount(String vouchernumber, Date date);

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr h where " + "h.branch_code=:branchCode "
			+ "and h.voucher_number IS NULL " + "and h.company_mst = :companymstid and "
			+ "(h.sales_mode = 'B2C' or h.sales_mode = 'B2B'  or h.sales_mode = 'RETAIL' )")
	List<SalesTransHdr> getHoldedSales(String companymstid, String branchCode);

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr h where " + "h.branch_code=:branchCode "
			+ "and h.voucher_number IS NULL and h.take_order_number IS NULL " + "and h.company_mst = :companymstid and "
			+ "h.sales_mode = 'POS'")
	List<SalesTransHdr> getHoldedPOSSales(String companymstid, String branchCode);

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr h where " + "h.branch_code=:branchCode "
			+ "and h.voucher_number IS NULL and h.take_order_number IS NULL " + "and h.company_mst = :companymstid and "
			+ "h.sales_mode = 'POS' and (h.user_id=:userid or h.user_id is null)")
	List<SalesTransHdr> getHoldedPOSSalesByUser(String companymstid, String branchCode, String userid);

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr h where " + "h.branch_code=:branchCode "
			+ "and h.voucher_number IS NULL and h.take_order_number IS NOT NULL "
			+ "and h.company_mst = :companymstid and " + "h.sales_mode = 'POS'")
	List<SalesTransHdr> getHoldedTakeOrderSales(String companymstid, String branchCode);

	@Query(nativeQuery = true, value = "select " + " h.voucher_date , " + " i.item_name  , " + "sum(d.qty),"
			+ "sum(d.mrp*d.qty) as value," + "c.account_name  " + " from sales_trans_hdr h  ,"
			+ " sales_dtl d , item_mst i, account_heads c "
			+ " where  h.branch_code=:branchcode and date(h.voucher_date)  between :reportdate and :todate and h.company_mst=:companymstid"
			+ " and i.id=d.item_id  and d.sales_trans_hdr_id = h.id and i.id=:itemid and h.customer_id =c.id group  by    h.voucher_date , "
			+ " i.item_name,c.account_name ")
	List<Object> getitemWiseSalesReportDateBetweenDatesWithCustomer(String branchcode, String reportdate, String todate,
			String companymstid, String itemid);

	@Query(nativeQuery = true, value = "select h.voucher_number," + "h.invoice_amount," + "h.invoice_discount,"
			+ " c.account_name, " + " c.party_gst, " + " h.sales_man_id " + " from sales_trans_hdr h,"
			+ "sales_dtl d," + "item_mst i," + "account_heads c" + " where h.id=d.d.sales_trans_hdr_id "
			+ "and d.item_id=i.id" + " and h.customer_id=c.id " + "and  date(h.voucher_date) between :fDate and :TDate "
			+ "and i.category_id=:catId" + " and h.company_mst=:companymstid and h.branch_code=:branchcode ")
	List<Object> batchwiseSalesTransactionReport(String companymstid, String branchcode, Date fDate, Date TDate,
			String catId);

	@Query(nativeQuery = true, value = "select h.voucher_number," + "h.voucher_date," + "i.item_name," + "h.batch,"
			+ "d.qty," + "d.amount,c.account_name " + " from sales_trans_hdr h," + "item_mst i, " + "sales_dtl d,"
			+ "account_heads c" + " where h.id=d.sales_trans_hdr_id " + "and i.id=d.item_id and"
			+ " c.id=h.customer_id and h.company_mst=:companymstid and h.branch_code=:branchcode  and date(h.voucher_date) between :fDate and :TDate and d.item_id=:itemId  ")

	List<Object> customerwisesalesreport(String companymstid, String branchcode, Date fDate, Date TDate, String itemId);

	@Query(nativeQuery = true, value = "select " + "h.voucher_date," + "i.item_name," + "d.qty,"
			+ "d.rate,c.account_name " + " from sales_trans_hdr h," + "item_mst i, " + "sales_dtl d," + "account_heads c"
			+ " where h.id=d.sales_trans_hdr_id " + "and i.id=d.item_id and"
			+ " c.id=h.customer_id and h.company_mst=:companymstid and h.branch_code=:branchcode  and date(h.voucher_date) between :fDate and :TDate and d.item_id=:itemId  ")

	List<Object> itemwisesalesreport(String companymstid, String branchcode, Date fDate, Date TDate, String itemId);

	@Query(nativeQuery = true, value = "select h.voucher_number," + "h.voucher_date," + "c.account_name,"
			+ "h.invoice_amount ," + "i.item_name," + "cm.category_name," + "d.batch," + "d.qty," + "d.amount,"
			+ "i.tax_rate " + "from sales_trans_hdr h," + "sales_dtl d," + "item_mst i,"
			+ "category_mst cm,account_heads c  where h.id=d.sales_trans_hdr_id " + " and i.id =d.item_id " + "and c.id=h.customer_id  "
			+ "and cm.id=i.category_id " + "and h.company_mst=:companymstid " + "and h.branch_code=:branchcode"
			+ " and date(h.voucher_date) between :fDate and :TDate and i.category_id=:catId ")

	List<Object> batchwisePerformanceReport(String companymstid, String branchcode, Date fDate, Date TDate,
			String catId);

//	@Query(nativeQuery = true, value = "select h.voucher_number," + "c.customer_name, " + "h.sales_mode , "
//			+ "h.invoice_amount," + "h.voucher_date " + "from sales_trans_hdr h," + "customer_mst c "
//			+ " WHERE  h.branch_code=:branchcode " + "and date(h.voucher_date) between :fromdate and :todate "
//			+ "and h.company_mst=:companymstid and c.id=h.customer_id and c.id=:customerid order by  h.voucher_number ")
	
	@Query(nativeQuery = true, value = "select h.voucher_number," + "c.account_name, " + "h.sales_mode , "
			+ "h.invoice_amount," + "h.voucher_date " + "from sales_trans_hdr h," + "account_heads c "
			+ " WHERE  h.branch_code=:branchcode " + "and date(h.voucher_date) between :fromdate and :todate "
			+ "and h.company_mst=:companymstid and c.id=h.customer_id and c.id=:customerid order by  h.voucher_number ")

	List<Object> getCustomerDailySalesDtl(String branchcode, Date fromdate, Date todate, String companymstid,
			String customerid);

	@Query(nativeQuery = true, value = "select h.sales_mode, " + "min(h.numeric_voucher_number) as firstVoucher, "
			+ "max(h.numeric_voucher_number) as lastVoucher, "
			+ "h.branch_code from sales_trans_hdr h where h.company_mst=:companymstid and "
			+ "h.sales_mode=:salesmode and " + "h.numeric_voucher_number is not null and "
			+ "date(h.voucher_date)=:date group by h.sales_mode ,h.branch_code ")
	List<Object> findNumericVoucherNoBySalesModeAndDate(java.sql.Date date, String companymstid, String salesmode);

	@Query(nativeQuery = true, value = "select count(*) "
			+ "from sales_trans_hdr h where h.company_mst=:companymstid and " + "h.sales_mode=:salesMode and "
			+ "date(h.voucher_date)=:date ")
	int findNoOfVoucherNo(Date date, String companymstid, String salesMode);

	@Query(nativeQuery = true, value = "select distinct h.sales_mode "
			+ "from sales_trans_hdr h where h.company_mst=:companymstid   ")

	List<String> findAllSalesMode(String companymstid);

	List<SalesTransHdr> findByVoucherDateAndCompanyMstAndBranchCode(Date date, CompanyMst companyMst,
			String branchcode);

	SalesTransHdr findByVoucherNumberAndCompanyMst(String invoiceno, CompanyMst companyMst);

	@Query(nativeQuery = true, value = "select h.branch_code, sum( (d.rate - p.amount) * d.qty) "
			+ "from sales_trans_hdr h, price_definition p, sales_dtl d, price_defenition_mst pmst "
			+ "where pmst.id=p.price_id and d.sales_trans_hdr_id=h.id and d.item_id=p.item_id "
			+ "and pmst.price_level_name='COST PRICE' and (date(h.voucher_date) >= date(p.start_date) and "
			+ "(date(h.voucher_date) <= date(p.end_date)) OR (date(p.end_date) is NULL)) and "
			+ "date(h.voucher_date) between :sdate and :edate and h.company_mst=:companyMst "
			+ "group by h.branch_code")
	List<Object> getBranchWiseProfit(Date sdate, Date edate, CompanyMst companyMst);

	@Query(nativeQuery = true, value = "select i.item_name,  sum( (d.rate - p.amount) * d.qty) "
			+ " from sales_trans_hdr h, price_definition p, sales_dtl d, price_defenition_mst pmst ," + " item_mst i  "
			+ "where h.branch_code = :branchCode and  d.item_id = i.id and pmst.id=p.price_id and d.sales_trans_hdr_id=h.id and d.item_id=p.item_id "
			+ "and pmst.price_level_name='COST PRICE' and (date(h.voucher_date) >= date(p.start_date) and "
			+ "(date(h.voucher_date) <= date(p.end_date)) OR (date(p.end_date) is NULL)) and "
			+ "date(h.voucher_date) between :sdate and :edate and h.company_mst=:companyMst " + "group by i.item_name")
	List<Object> getProfitByBranch(Date sdate, Date edate, CompanyMst companyMst, String branchCode);

	@Query(nativeQuery = true, value = "select count(*) form sales_trans_hdr where voucher_number is NOT NULL and voucher_date=:date")
	int getClientSalesCount(Date date);

	
	/*
	 * parameter changed by account_heads instead of customer_mst=======07/01/2022
	 */
//	@Query(nativeQuery = true, value = "select * from sales_trans_hdr h where "
//			+ "h.company_mst =:companyMst and h.customer_mst=:customerMst and "
//			+ "h.sales_man_id=:salesman and h.voucher_number is nll")
//	SalesTransHdr findByCompanyMstSalesManIdAndCustomerId(CompanyMst companyMst, String salesman,
//			CustomerMst customerMst);
	@Query(nativeQuery = true, value = "select * from sales_trans_hdr h where "
			+ "h.company_mst =:companyMst and h.account_heads=:accountHeads and "
			+ "h.sales_man_id=:salesman and h.voucher_number is nll")
	SalesTransHdr findByCompanyMstSalesManIdAndAccountHeads(CompanyMst companyMst, String salesman,
			AccountHeads accountHeads);
	

	@Query(nativeQuery = true, value = "select s.id from sales_trans_hdr s  where s.voucher_number is null")
	String testInputProgressWholesalebill();

	@Query(nativeQuery = true, value = "select d.id, h.voucher_number, h.voucher_date, "
			+ " d.qty, d.item_name, d.amount  " + " from sales_trans_hdr h, sales_dtl d "
			+ " where h.voucher_number is not null and date(h.voucher_date)>=:sdate and "
			+ "date(h.voucher_date)<=:edate and h.company_mst=:companymstid and h.branch_code=:branchCode and "
			+ "d.sales_trans_hdr_id=h.id")
	List<Object> getSalesAnalysisReport(Date sdate, Date edate, String companymstid, String branchCode);

	List<SalesTransHdr> findAllByBranchCodeOrderByVoucherDateAsc(Date vDate);

	@Query(nativeQuery = true, value = "select sum(receipt_amount) from  sales_trans_hdr h , sales_receipts s where s.sales_trans_hdr_id=h.id and s.receipt_mode=:receiptMode and h.voucher_date=:vdate ")
	Double getDailyCashSales(String receiptMode, Date vdate);

//	@Query(nativeQuery = true, value = " select" + " h.voucher_number," + "h.voucher_date, " + "h.voucher_type ,"
//			+ "cu.customer_name ," + "cu.customer_address ," + "cu.customer_contact," + "cu.customer_type ,"
//			+ "cu.customer_gst," + "cu.customer_state," + "cu.customer_country," + "cu.bank_name,"
//			+ "cu.bank_account_name," + "cu.bank_ifsc," + "i.item_name," + "i.hsn_code," + "c.category_name ,"
//			+ "i.tax_rate ," + " d.qty,"
//
//			// version 1.3.1
//			// Changed by Regy onMarch 4th bsed on input from Mnu
//			+ "d.rate_before_discount as rate," // Was d.rate
//			// version 1.3.1 Ends
//
//			+ "d.cgst_amount," + "d.sgst_amount," + "d.igst_amount," + "d.cess_amount," + "d.discount,"
//			+ "u.unit_name,h.invoice_amount,d.amount ,d.rate as ratewithdiscount from sales_trans_hdr h, "
//			+ "sales_dtl d," + "customer_mst cu,item_mst i,unit_mst u ,category_mst c"
//			+ " where h.id=d.sales_trans_hdr_id " + "and cu.id= h.customer_id and u.id=d.unit_id  "
//			+ " and h.company_mst=:companymstid "
//			+ "and h.branch_code=:branchcode and d.item_id = i.id and i.category_id=c.id and   "
//			+ "date(h.voucher_date) between :fDate and :TDate and h.voucher_number is not null")
	
	@Query(nativeQuery = true, value = " select" + " h.voucher_number," + "h.voucher_date, " + "h.voucher_type ,"
			+ "cu.account_name ," + "cu.party_address1 ," + "cu.customer_contact," + "cu.customer_type ,"
			+ "cu.party_gst," + "cu.customer_state," + "cu.customer_country," + "cu.bank_name,"
			+ "cu.bank_account_name," + "cu.bank_ifsc," + "i.item_name," + "i.hsn_code," + "c.category_name ,"
			+ "i.tax_rate ," + " d.qty,"

			// version 1.3.1
			// Changed by Regy onMarch 4th bsed on input from Mnu
			+ "d.rate_before_discount as rate," // Was d.rate
			// version 1.3.1 Ends

			+ "d.cgst_amount," + "d.sgst_amount," + "d.igst_amount," + "d.cess_amount," + "d.discount,"
			+ "u.unit_name,h.invoice_amount,d.amount ,d.rate as ratewithdiscount from sales_trans_hdr h, "
			+ "sales_dtl d," + "account_heads cu,item_mst i,unit_mst u ,category_mst c"
			+ " where h.id=d.sales_trans_hdr_id " + "and cu.id= h.customer_id and u.id=d.unit_id  "
			+ " and h.company_mst=:companymstid "
			+ "and h.branch_code=:branchcode and d.item_id = i.id and i.category_id=c.id and   "
			+ "date(h.voucher_date) between :fDate and :TDate and h.voucher_number is not null")
	List<Object> tallyImportReportReport(String companymstid, String branchcode, Date fDate, Date TDate);

//----------------version 1.9   new 

	@Query(nativeQuery = true, value = "select h.sales_prefix "
			+ "from sales_type_mst h where h.company_mst=:companymstid   ")

	List<String> findAllVoucherTypes(String companymstid);

	@Query(nativeQuery = true, value = "select  " + "min(h.numeric_voucher_number) as firstVoucher, "
			+ "max(h.numeric_voucher_number) as lastVoucher, "
			+ "h.branch_code from sales_trans_hdr h where h.company_mst=:companymstid and "
			+ "h.numeric_voucher_number is not null and "
			+ "date(h.voucher_date)=:date and LOWER(h.voucher_number) like   LOWER(:salesmode) group by h.branch_code ")
	List<Object> findNumericVoucherNoByBillSeriesAndDate(java.sql.Date date, String companymstid, String salesmode);

//verify the query findCountOfBillSeries
	@Query(nativeQuery = true, value = "select count(*) "
			+ " from sales_trans_hdr h where h.company_mst=:companymstid and "
			+ " h.numeric_voucher_number is not null and "
			+ "date(h.voucher_date)=:date and LOWER(h.voucher_number) like   LOWER(:salesmode) ")
	int findCountOfBillSeries(java.sql.Date date, String companymstid, String salesmode);

	@Query(nativeQuery = true, value = "select  h.sales_mode"
			+ " from sales_trans_hdr h where h.company_mst=:companymstid and "
			+ "h.numeric_voucher_number is not null and "
			+ "date(h.voucher_date)=:date and LOWER(h.voucher_number) like   LOWER(:salesmode) ")

	List<String> findBillSeriesSalesMode(java.sql.Date date, String companymstid, String salesmode);

//----------------version 1.9 end

//----------------version 1.5
	/*
	 * @Query(nativeQuery = true,value="select h.voucher_type_prefix " +
	 * "from voucher_type_hdr h where h.company_mst=:companymstid   ")
	 * 
	 * List<String> findAllVoucherTypes(String companymstid);
	 * 
	 * 
	 * 
	 * @Query(nativeQuery = true,value="select h.sales_mode, " +
	 * "min(h.numeric_voucher_number) as firstVoucher, " +
	 * "max(h.numeric_voucher_number) as lastVoucher, " +
	 * "h.branch_code from sales_trans_hdr h where h.company_mst=:companymstid and "
	 * + "h.numeric_voucher_number is not null and " +
	 * "date(h.voucher_date)=:date and LOWER(h.voucher_number) like   LOWER(:salesmode) group by h.sales_mode ,h.branch_code "
	 * ) List<Object> findNumericVoucherNoByBillSeriesAndDate(java.sql.Date
	 * date,String companymstid,String salesmode); //----------------version 1.5
	 * ENDS
	 */

//version 3.1
	@Query(nativeQuery = true, value = "select " + "h.voucher_number," + "h.voucher_date," + "c.account_name,"
			+ "i.item_name," + "d.qty," + "d.mrp," + "d.rate," + "d.rate," + "u.unit_name ," + "d.tax_rate "
			+ "from sales_trans_hdr h," + "sales_dtl d," + "account_heads c ," + "item_mst i ," + "unit_mst u,"
			+ " where h.id=and d.sales_trans_hdr_id " + "and date(h.voucher_date) between:sdate and :edate and "
			+ "h.company_mst=:companyMst" + " and c.id=h.customer_id "
			+ "and i.id=d.item_id and u.id=d.unit_id and h.branch_code=:branchcode ")
	List<Object> getSalesDtlReport(Date sdate, Date edate, CompanyMst companyMst, String branchcode);

//version 3.1 end

//@Query(nativeQuery = true,value="select  max( numeric_voucher_number) from sales_trans_hdr h     where date(h.voucher_date)  =:vDate and voucher_number  not like (select  '%'||sales_prefix||'%'  from sales_type_mst ) and h.company_mst=:companymstid and h.branch_code=:branch ") 
	@Query(nativeQuery = true, value = "select  max( numeric_voucher_number) from sales_trans_hdr h   "
			+ "  where date(h.voucher_date)  =:vDate and ( INVOICE_NUMBER_PREFIX  not in ( select  sales_prefix   from sales_type_mst  ) "
			+ " OR INVOICE_NUMBER_PREFIX  is null  )   and h.company_mst=:companymstid and h.branch_code=:branch ")

	Integer posEndingVoucherNumber(Date vDate, String branch, String companymstid);

//@Query(nativeQuery = true,value="	select  min( numeric_voucher_number) from sales_trans_hdr h  where date(h.voucher_date) =:vDate  and voucher_number  not like (select  '%'||sales_prefix||'%'  from sales_type_mst ) and h.branch_code=:branch and h.company_mst=:companymstid  ")
	@Query(nativeQuery = true, value = "	select  min( numeric_voucher_number) from sales_trans_hdr h  where date(h.voucher_date) =:vDate and  ( INVOICE_NUMBER_PREFIX  not in ( select  sales_prefix   from sales_type_mst  ) OR INVOICE_NUMBER_PREFIX  is null  )  and h.branch_code=:branch and h.company_mst=:companymstid  ")

	Integer postStartingVoucherNumber(Date vDate, String branch, String companymstid);

//@Query(nativeQuery = true,value="select  min( numeric_voucher_number) from sales_trans_hdr h where date(h.voucher_date)  =:vDate and voucher_number   like (select  '%'||sales_prefix||'%'  from sales_type_mst ) and h.branch_code=:branch and h.company_mst=:companymstid" )
	@Query(nativeQuery = true, value = "select  min( numeric_voucher_number) from sales_trans_hdr h where date(h.voucher_date)  =:vDate and INVOICE_NUMBER_PREFIX    in ( select  sales_prefix   from sales_type_mst  )  and h.branch_code=:branch and h.company_mst=:companymstid")

	Integer wholeSaleStartingVBillNumber(Date vDate, String branch, String companymstid);

//@Query(nativeQuery = true,value="select  max( numeric_voucher_number) from sales_trans_hdr h where date(h.voucher_date)  =:vDate and voucher_number   like (select  '%'||sales_prefix||'%'  from sales_type_mst ) and h.branch_code=:branch and h.company_mst=:companymstid  ")

	@Query(nativeQuery = true, value = "select  max( numeric_voucher_number) from sales_trans_hdr h where date(h.voucher_date)  =:vDate and INVOICE_NUMBER_PREFIX    in ( select  sales_prefix   from sales_type_mst  ) and h.branch_code=:branch and h.company_mst=:companymstid  ")

	Integer wholeSaleEndingVBillNumber(Date vDate, String branch, String companymstid);

	@Query(nativeQuery = true, value = "select h.voucher_number,r.receipt_amount,r.receipt_mode,h.voucher_date from sales_trans_hdr h,sales_receipts r where h.id=r.sales_trans_hdr_id "
			+ "and date(h.voucher_date)  =:fromdate and r.receipt_mode not in(:cashMode,'CREDIT') and h.branch_code=:branchcode and h.company_mst=:companymstid and h.voucher_number is not null  ")
	List<Object> getReceiptModeWiseReort(String branchcode, Date fromdate, String companymstid, String cashMode);

	/*
	 * Following quesry id not used since we need to include sales Order as well.
	 */
	@Query(nativeQuery = true, value = "select sr.receipt_mode,sum(sr.receipt_amount) as receipt_amount from sales_receipts sr ,"
			+ "sales_trans_hdr sh "
			+ "where sh.id= sr.sales_trans_hdr_id and sh.branch_code = :branchcode and  sr.receipt_mode not in(:cashMode,'CREDIT') and sh.company_mst = :companyMst and sh.voucher_number is not null  and "
			+ "date(sh.voucher_date) =:fdate group by sr.receipt_mode")
	List<Object> getReceiptModeRepport(Date fdate, String companyMst, String branchcode, String cashMode);

	@Query(nativeQuery = true, value = " select "
			+ "  sh.voucher_number,sr.receipt_amount,sr.receipt_mode,sh.voucher_date  " + "from sales_receipts sr ,"
			+ "sales_trans_hdr sh " + "where sh.id= sr.sales_trans_hdr_id and sh.branch_code = :branchcode and"
			+ "  sr.receipt_mode not in(:cashMode,'CREDIT') and sh.company_mst = :companyMst and sh.voucher_number is not null  and "
			+ "date(sh.voucher_date) =:fdate " + " UNION " + " select "
			+ "  soh.voucher_number,sor.receipt_amount,sor.receipt_mode,soh.voucher_date  "
			+ " from sales_order_receipts sor ," + "sales_order_trans_hdr soh "
			+ "where soh.id= sor.sales_order_trans_hdr_id "
			+ "and soh.branch_code = :branchcode and  sor.receipt_mode not in(:cashMode,'CREDIT') and soh.company_mst = :companyMst and soh.voucher_number is not null  and "
			+ "date(soh.voucher_date) =:fdate "

	)

	List<Object> getReceiptModeWiseWhithSoReport(Date fdate, String companyMst, String branchcode, String cashMode);

	/*
	 * Below quesry is used instead of above query to include SO receipts.
	 * 
	 */

	@Query(nativeQuery = true, value = "SELECT A.rcpmode, sum(A.RECEIPT_AMOUNT)  from  ( " + " select "
			+ " sr.receipt_mode as rcpmode , sum(sr.receipt_amount) as RECEIPT_AMOUNT " + "from sales_receipts sr ,"
			+ "sales_trans_hdr sh " + "where sh.id= sr.sales_trans_hdr_id and sh.branch_code = :branchcode and"
			+ "  sr.receipt_mode not in(:cashMode,'CREDIT') and sh.company_mst = :companyMst and sh.voucher_number is not null  and "
			+ "date(sh.voucher_date) =:fdate group by  sr.receipt_mode " + " UNION " + " select "
			+ " sr.receipt_mode  as rcpmode ,sum(sr.receipt_amount) as RECEIPT_AMOUNT"
			+ " from sales_order_receipts sr ," + "sales_order_trans_hdr sh "
			+ "where sh.id= sr.sales_order_trans_hdr_id "
			+ "and sh.branch_code = :branchcode and  sr.receipt_mode not in(:cashMode) and sh.company_mst = :companyMst and sh.voucher_number is not null  and "
			+ "date(sh.voucher_date) =:fdate group by  sr.receipt_mode " + " ) A group by A.rcpmode "

	)

	List<Object> getReceiptModeRepportWithSo(Date fdate, String companyMst, String branchcode, String cashMode);

	@Query(nativeQuery = true, value = "select sum(sor.receipt_amount) from sales_trans_hdr h, "
			+ "sales_receipts r,sales_order_trans_hdr o," + " sale_order_receipt sor where h.sale_order_hrd_id = o.id "
			+ "and o.id = sor.sale_order_trans_hdr_id "
			+ "and h.id = r.sales_trans_hdr_id and o.order_status = 'Realized' "
			+ "and sor.receipt_mode in ('YES BANK', 'EBS')  and date(o.voucher_date) <:fromdate  "
			+ "and date(h.voucher_date) =:fromdate and h.branch_code =:branchcode ")

	Double getSettlementReport(String branchcode, Date fromdate);

	@Query(nativeQuery = true, value = "select h.voucher_number,sor.receipt_amount,h.voucher_date from sales_trans_hdr h, "
			+ "sales_receipts r,sales_order_trans_hdr o," + " sale_order_receipt sor where h.sale_order_hrd_id = o.id "
			+ "and o.id = sor.sale_order_trans_hdr_id "
			+ "and h.id = r.sales_trans_hdr_id and o.order_status = 'Realized' "
			+ "and sor.receipt_mode in ('YES BANK', 'EBS')  and date(o.voucher_date) <:fromdate  "
			+ "and date(h.voucher_date) =:fromdate and h.branch_code =:branchcode ")

	List<Object> getSettlementReportVoucherNo(String branchcode, Date fromdate);

	@Query(nativeQuery = true, value = "select sum(sor.receipt_amount) from sales_trans_hdr h, "
			+ "sales_order_trans_hdr o," + " sale_order_receipt sor where h.sale_order_hrd_id = o.id "
			+ "and o.id = sor.sale_order_trans_hdr_id " + "and o.order_status = 'Realized'"
			+ " and sor.receipt_mode=:receiptmode  " + " and date(o.voucher_date) <:fromdate  "
			+ "and date(h.voucher_date) =:fromdate and h.branch_code =:branchcode ")

	Double getPreviousAdvance(String branchcode, Date fromdate, String receiptmode);

	@Query(nativeQuery = true, value = "select h.voucher_number,sor.receipt_amount,c.account_name from sales_trans_hdr h, "
			+ "sales_order_trans_hdr o,account_heads c," + " sale_order_receipt sor where h.sale_order_hrd_id = o.id "
			+ "and o.id = sor.sale_order_trans_hdr_id and c.id= h.customer_id " + "and o.order_status = 'Realized'"
			+ " and sor.receipt_mode=:receiptmode  " + " and date(o.voucher_date) <:fromdate  "
			+ "and date(h.voucher_date) =:fromdate and h.branch_code =:branchcode ")

	List<Object> getPreviousAdvanceDtl(String branchcode, Date fromdate, String receiptmode);

	@Query(nativeQuery = true, value = "select sum(sor.receipt_amount) from sales_trans_hdr h, "
			+ "sales_order_trans_hdr o," + " sale_order_receipt sor where h.sale_order_hrd_id = o.id "
			+ "and o.id = sor.sale_order_trans_hdr_id " + "and o.order_status = 'Realized'"
			+ " and sor.receipt_mode <>:receiptmode  " + " and date(o.voucher_date) <:fromdate  "
			+ "and date(h.voucher_date) =:fromdate and h.branch_code =:branchcode ")

	Double getPreviousAdvanceCard(String branchcode, Date fromdate, String receiptmode);

	@Query(nativeQuery = true, value = "select sum(sor.receipt_amount) from sales_trans_hdr h, "
			+ "sales_order_trans_hdr o," + " sale_order_receipt sor where h.sale_order_hrd_id = o.id "
			+ "and o.id = sor.sale_order_trans_hdr_id " + "and o.order_status = 'Realized'"
			+ " and sor.receipt_mode =:receiptmode  " + " and date(o.voucher_date) <:fromdate  "
			+ "and date(h.voucher_date) =:fromdate and h.branch_code =:branchcode ")

	Double getPreviousAdvanceCardRealized(String branchcode, Date fromdate, String receiptmode);

	@Query(nativeQuery = true, value = "select h.voucher_number,sor.receipt_amount,h.voucher_date from sales_trans_hdr h, "
			+ "sales_receipts r,sales_order_trans_hdr o," + " sale_order_receipt sor where h.sale_order_hrd_id = o.id "
			+ "and o.id = sor.sale_order_trans_hdr_id "
			+ "and h.id = r.sales_trans_hdr_id and o.order_status = 'Realized' "
			+ "and sor.receipt_mode in ('CASH')  and date(o.voucher_date) <:fromdate  "
			+ "and date(h.voucher_date) =:fromdate and h.branch_code =:branchcode ")

	List<Object> getSettledAmountDtlCashWiseReport(String branchcode, Date fromdate);

	@Query(nativeQuery = true, value = "select sum(sor.receipt_amount) from sales_trans_hdr h, "
			+ "sales_receipts r,sales_order_trans_hdr o," + " sale_order_receipt sor where h.sale_order_hrd_id = o.id "
			+ "and o.id = sor.sale_order_trans_hdr_id "
			+ "and h.id = r.sales_trans_hdr_id and o.order_status = 'Realized' "
			+ "and sor.receipt_mode in ('CASH')  and date(o.voucher_date) <:fromdate  "
			+ "and date(h.voucher_date) =:fromdate and h.branch_code =:branchcode ")

	Double getSettlementCashReport(String branchcode, Date fromdate);

	@Query(nativeQuery = true, value = "select h.voucher_number,r.receipt_amount,r.receipt_mode,h.voucher_date from sales_trans_hdr h,sales_receipts r where h.id=r.sales_trans_hdr_id "
			+ " and date(h.voucher_date)  =:fromdate and r.receipt_mode =:receiptMode "
			+ " and h.branch_code=:branchcode and h.company_mst=:companymstid  and h.voucher_number is not null ")
	List<Object> getReceiptModeCashWiseReort(String branchcode, Date fromdate, String companymstid, String receiptMode);

	@Query(nativeQuery = true, value = "select h.voucher_number,c.account_name, h.sales_mode , h.invoice_amount,h.voucher_date from sales_trans_hdr h,account_heads c "

			+ " WHERE  h.branch_code=:branchcode and date(h.voucher_date) =:fdate  and h.company_mst=:companymstid and c.id=h.customer_id and h.h.voucher_number is not null order by  h.voucher_number ")

	List<Object> getDailySalesReport(String branchcode, Date fdate, String companymstid);

//-------------version 4.11
	@Query(nativeQuery = true, value = "select sum(d.rate*d.qty) as taxableamount, "
			+ "sum(d.rate*d.qty*d.tax_rate/100) as taxAmount, d.tax_rate " + "from sales_dtl d, sales_trans_hdr h "
			+ "where date(h.voucher_date)=:edate and h.company_mst=:companyMst and h.branch_code=:branchCode and h.id=d.sales_trans_hdr_id "
			+ "group by d.tax_rate")
	List<Object> SaleTaxSplit(Date edate, CompanyMst companyMst, String branchCode);
//-------------version 4.11 end

	@Query(nativeQuery = true, value = "select d.* from sales_dtl d, sales_trans_hdr h  where h.id=d.sales_trans_hdr_id and h.voucher_number is null")
	List<SalesDtl> fetchSalesDtl();

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr where voucher_number is null ")
	List<SalesTransHdr> fetchSalesTanHdr();

	@Query(nativeQuery = true, value = "select d.tax_rate ,sum(cgst_amount+sgst_amount+igst_amount),sum(cgst_amount),sum(sgst_amount),sum(igst_amount),sum(amount),sum(d.cess_amount)  from sales_trans_hdr h  ,sales_dtl d where h.id=d.sales_trans_hdr_id and h.branch_code=:branchCode and date(h.voucher_date) between :fromDate and :toDate and h.company_mst=:companyMst group by d.tax_rate")
	List<Object> salesTaxReport(java.util.Date fromDate, java.util.Date toDate, CompanyMst companyMst,
			String branchCode);

	@Query(nativeQuery = true, value = "select sum(d.rate*d.qty) from sales_trans_hdr h,sales_dtl d where h.id=d.sales_trans_hdr_id and date(h.voucher_date) between :fromDate and :toDate  ")
	Double salesAsasableAmount(Date fromDate, Date toDate);

	@Query(nativeQuery = true, value = "select sum(d.sgst_amount) from sales_trans_hdr h,sales_dtl d where h.id=d.sales_trans_hdr_id and date(h.voucher_date) between :fromDate and :toDate ")
	Double sumOfSGSTamount(Date fromDate, Date toDate);

	@Query(nativeQuery = true, value = "select sum(d.cgst_amount) from sales_trans_hdr h,sales_dtl d where h.id=d.sales_trans_hdr_id and date(h.voucher_date) between :fromDate and :toDate ")
	Double sumOfCGSTamount(Date fromDate, Date toDate);

	@Query(nativeQuery = true, value = "select sum(d.igst_amount) from sales_trans_hdr h,sales_dtl d where h.id=d.sales_trans_hdr_id and date(h.voucher_date) between :fromDate and :toDate ")
	Double sumOfIGSTamount(Date fromDate, Date toDate);

	@Query(nativeQuery = true, value = "select sum(d.cess_amount) from sales_trans_hdr h,sales_dtl d where h.id=d.sales_trans_hdr_id and date(h.voucher_date) between :fromDate and :toDate ")
	Double sumOfCESSamount(Date fromDate, Date toDate);

	@Query(nativeQuery = true, value = "select sum(r.receipt_amount) from sales_trans_hdr h,sales_receipts r where h.id=r.sales_trans_hdr_id and r.receipt_mode=:receiptMode and date(h.voucher_date) between :fromDate and :toDate")
	Double sumOfCashAmount(Date fromDate, Date toDate, String receiptMode);

	@Query(nativeQuery = true, value = "select sum(r.receipt_amount) from sales_trans_hdr h,sales_receipts r where h.id=r.sales_trans_hdr_id and r.receipt_mode='G pay' and date(h.voucher_date) between :fromDate and :toDate")
	Double sumOfGpayAmount(Date fromDate, Date toDate);

	@Query(nativeQuery = true, value = "select sum(r.receipt_amount) from sales_trans_hdr h,sales_receipts r where h.id=r.sales_trans_hdr_id and r.receipt_mode='PAYTM' and date(h.voucher_date) between :fromDate and :toDate")
	Double sumOfPaytmAmount(Date fromDate, Date toDate);

	@Query(nativeQuery = true, value = "select sum(r.receipt_amount) from sales_trans_hdr h,sales_receipts r where h.id=r.sales_trans_hdr_id and r.receipt_mode='SBI' and date(h.voucher_date) between :fromDate and :toDate")
	Double sumOfsbiAmount(Date fromDate, Date toDate);

	@Query(nativeQuery = true, value = "select r.account_id, sum(r.receipt_amount) from sales_trans_hdr h,sales_receipts r where h.id=r.sales_trans_hdr_id and r.receipt_mode='CREDIT' and date(h.voucher_date) between :fromDate and :toDate group by r.account_id")
	List<Object> sumOfCreditAmount(Date fromDate, Date toDate);
//-------------------version 6.5 surya 

	SalesTransHdr findByVoucherNumberAndCompanyMstAndBranchCode(String invoiceno, CompanyMst companyMst,
			String branchcode);

//-------------------version 6.5 surya end
	@Query(nativeQuery = true, value = "select * from sales_trans_hdr where voucher_number =:vno")
	List<SalesTransHdr> findByVoucherNumberList(String vno);

	List<SalesTransHdr> findByCompanyMstAndBranchCode(CompanyMst companyMst, String branchcode);

	@Query(nativeQuery = true, value = "select max(h.voucher_number) from sales_trans_hdr h where "
			+ " h.branch_code=:branchcode and " + "h.company_mst=:companyMst and h.sales_mode=:salesMode")

	String getMaxVoucherNo(String branchcode, CompanyMst companyMst, String salesMode);

	@Query(nativeQuery = true, value = "select h.voucher_number from sales_trans_hdr h where "
			+ " h.branch_code=:branchcode and " + "h.company_mst=:companyMst and h.numeric_voucher_number=:numeric")
	String getVoucherNumberByNumericNoAndBranch(String numeric, String branchcode, CompanyMst companyMst);

	@Query(nativeQuery = true, value = "select distinct h.sales_mode from sales_trans_hdr h where "
			+ " h.branch_code=:branchcode and " + "h.company_mst=:companyMst")
	List<String> getSalesMode(String branchcode, CompanyMst companyMst);

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr where customer_id=:customerId and "
			+ "date(voucher_date)=:vdate and company_mst=:companyMst and voucher_number is null and customise_sales_mode =:customsalesmode")
	SalesTransHdr getSalesTransHdrBycustomerAndVoucherNull(String customerId, Date vdate, CompanyMst companyMst,
			String customsalesmode);

	@Modifying
	@Query(nativeQuery = true, value = "update sales_trans_hdr set customer_id =:toAccountId,account_heads=:toAccountId  where customer_id =:fromAccountId ")
	void accountMerge(String fromAccountId, String toAccountId);

	@Query(nativeQuery = true, value = "select distinct(voucher_number) from sales_trans_hdr "
			+ "where date(voucher_date)>=:vdate group by voucher_number having count(voucher_number)>1")
	List<String> getDuplicateVoucher(Date vdate);

	@Query(nativeQuery = true, value = "select id from sales_trans_hdr where voucher_number=:string FETCH FIRST ROW ONLY")
	List<String> getSalesTransHdrIdbyVoucher(String string);

	@Query(nativeQuery = true, value = "select distinct c.category_name ,c.id from sales_trans_hdr h,sales_dtl d ,category_mst c,item_mst i"
			+ " where h.id=d.sales_trans_hdr_id and d.item_id=i.id and c.id=i.category_id and  h.company_mst=:companyMst and h.branch_code=:branchCode and date(h.voucher_date) >=:fromDate and "
			+ "date(h.voucher_date)<=:toDate  ")
	List<Object> getCategoryWiseSalesReport(Date fromDate, Date toDate, String branchCode, CompanyMst companyMst);

//c.id=i.category_id and
	@Query(nativeQuery = true, value = "select i.item_name ,sum(d.qty),d.rate  from sales_trans_hdr h,sales_dtl d,item_mst i,category_mst c where h.id=d.sales_trans_hdr_id and h.company_mst=:companyMst and h.branch_code=:branchCode and date(h.voucher_date)>=:fromDate and date(h.voucher_date)<=:toDate and c.id=i.category_id and d.item_id=i.id and  i.category_id=:categoryid group by i.item_name,d.rate ")
	List<Object> fetchSalesDtlsByCategory(String categoryid, Date fromDate, Date toDate, String branchCode,
			CompanyMst companyMst);

	@Query(nativeQuery = true, value = "select h.voucher_number, "
			+ "h.voucher_date,c.account_name,h.invoice_amount, b.branch_name "
			+ "from sales_trans_hdr h, account_heads c, branch_mst b "
			+ "where h.BRANCH_SALE_CUSTOMER=c.id and h.account_heads=b.id and "
			+ "h.voucher_number is not null and date(h.voucher_date)>=:sdate and "
			+ "date(h.voucher_date)<=:edate and h.company_mst=:companymstid and h.branch_code=:branchcode")
	List<Object> getBranchSaleHdrReport(String companymstid, String branchcode, Date sdate, Date edate);

	@Query(nativeQuery = true, value = "select h.voucher_number, " + "h.voucher_date,c.account_name,h.invoice_amount "
			+ "from sales_trans_hdr h, account_heads c " + "where h.account_heads=c.id and "
			+ "h.voucher_number is not null and date(h.voucher_date)>=:sdate and "
			+ "date(h.voucher_date)<=:edate and h.company_mst=:companymstid and h.branch_code=:branchcode")
	List<Object> getBranchSaleCustomerReport(String companymstid, String branchcode, Date sdate, Date edate);

	@Query(nativeQuery = true, value = "select h.voucher_number, "
			+ "h.voucher_date,c.account_name,h.invoice_amount, b.branch_name "
			+ "from sales_trans_hdr h, account_heads c, branch_mst b "
			+ "where h.voucher_number is not null and h.account_heads=c.id  " + " and date(h.voucher_date)>=:sdate and "
			+ "date(h.voucher_date)<=:edate and h.company_mst=:companymstid and h.branch_code=:branchcode "
			+ "and c.price_type_id=:pricetype")
	List<Object> getSalesReportByPriceTypeReport(String companymstid, String branchcode, String pricetype, Date sdate,
			Date edate);

	void deleteByVoucherNumber(String string);

	@Query(nativeQuery = true, value = "select i.item_name,ib.batch,sum(d.qty),h.invoice_amount,sum(d.igst_amount) from sales_trans_hdr h, sales_dtl d ,item_mst i,item_batch_mst ib where h.id=d.sales_trans_hdr_id and  d.item_id=i.id and i.id=ib.item_id and date(h.voucher_date) between:fdate and :tDate and h.company_mst=:companyMst and h.branch_code=:branchCode and i.category_id=:catId group by i.item_name,ib.batch,h.invoice_amount")
	List<Object> getPharmacyGroupWiseSalesReport(CompanyMst companyMst, Date fdate, Date tDate, String branchCode,
			String catId);


//	@Query(nativeQuery = true, value = "select h.voucher_date," + "h.voucher_number,c.customer_name,i.item_name,"
//			+ "cm.category_name,d.batch,d.expiry_date,"
//			+ "d.qty,d.igst_tax_rate,d.rate,u.user_name from sales_trans_hdr h,category_mst cm," + "user_mst u,"
//			+ "customer_mst c,item_mst i,sales_dtl d where d.sales_trans_hdr_id=h.id "
//			+ "and d.item_id=i.id and i.category_id=cm.id and h.customer_mst = c.id and" + " u.id=h.user_id and  "
//			+ "date(voucher_date)>=:fromdate and date(voucher_date)<=:todate and "
//			+ "h.branch_code=:branchcode and h.company_mst = :companymstid and h.voucher_number is not null")
	
	@Query(nativeQuery = true, value = "select h.voucher_date," + "h.voucher_number,c.account_name,i.item_name,"
			+ "cm.category_name,d.batch,d.expiry_date,"
			+ "d.qty,d.igst_tax_rate,d.rate,u.user_name from sales_trans_hdr h,category_mst cm," + "user_mst u,"
			+ "account_heads c,item_mst i,sales_dtl d where d.sales_trans_hdr_id=h.id "
			+ "and d.item_id=i.id and i.category_id=cm.id and h.account_heads = c.id and" + " u.id=h.user_id and  "
			+ "date(voucher_date)>=:fromdate and date(voucher_date)<=:todate and "
			+ "h.branch_code=:branchcode and h.company_mst = :companymstid and h.voucher_number is not null")
	List<Object> salesDtlDaily(String branchcode, Date fromdate, Date todate, String companymstid);

	@Query(nativeQuery = true, value = "select " + "i.item_name," + "cm.category_name," + "h.voucher_number,"
			+ "h.voucher_date," + "ib.batch," + "d.igst_amount," + "h.invoice_amount,d.amount," + "c.account_name "
			+ ",sum(d.qty) "
			+ "from sales_trans_hdr h ,sales_dtl d,item_mst i,account_heads c,category_mst cm,item_batch_mst ib where h.id=d.sales_trans_hdr_id and d.item_id=i.id and ib.item_id=i.id and c.id=h.account_heads and date(h.voucher_date) between :fdate and :tDate and h.branch_code=:branchCode and h.company_mst=:companyMst and cm.id=i.category_id and i.category_id=:catId group by i.item_name,cm.category_name,h.voucher_number,ib.batch,d.igst_amount,h.invoice_amount,"
			+ "h.voucher_date,ib.batch,d.amount,c.account_name")
	List<Object> getPharmacyGroupWiseSalesDtlReport(CompanyMst companyMst, Date fdate, Date tDate, String branchCode,
			String catId);

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr where date(voucher_date)>= :fuDate "
			+ "and date(voucher_date)>=:tudate and voucher_number is not null and is_branch_sales ='Y'")

	List<SalesTransHdr> findSalesBetweenVoucherDate(Date fuDate, Date tudate);

//@Query(nativeQuery = true,value="select h.voucher_date ,h.voucher_number,h.invoice_amount,c.customer_name,sum(d.cost_price)  from sales_trans_hdr h,sales_dtl d ,customer_mst c where h.id=d.sales_trans_hdr_id and h.customer_id=c.id  and  h.company_mst=:companymstid and h.branch_code=:branchcode and date(h.voucher_date) >=:fromdate and date(h.voucher_date)>=:todate group by h.voucher_date ,h.voucher_number,h.invoice_amount,c.customer_name ")

	@Query(nativeQuery = true, value = "select h.voucher_number,c.account_name, h.sales_mode , h.invoice_amount,h.voucher_date, sum(d.cost_price) from sales_trans_hdr h,account_heads c ,sales_dtl d "

			+ " WHERE h.id=d.sales_trans_hdr_id and  h.branch_code=:branchcode and date(voucher_date)>=:fromdate and date(voucher_date)<=:todate  and h.company_mst=:companymstid and c.id=h.customer_id and h.voucher_number is not null  group by h.voucher_number,c.account_name, h.sales_mode , h.invoice_amount,h.voucher_date order by  h.voucher_number  ")
	List<Object> getSalesProfitReportl(String branchcode, Date fromdate, Date todate, String companymstid);

	// new query for salesprofitreport including
	// batch==========anandu==========08-07-2021=======
	@Query(nativeQuery = true, value = "select h.voucher_number,c.account_name, h.sales_mode , h.invoice_amount,h.voucher_date, sum(d.cost_price * d.qty), d.batch from sales_trans_hdr h,account_heads c ,sales_dtl d "

			+ " WHERE h.id=d.sales_trans_hdr_id and  h.branch_code=:branchcode and date(voucher_date)>=:fromdate and date(voucher_date)<=:todate  and h.company_mst=:companymstid and c.id=h.customer_id and h.voucher_number is not null  group by h.voucher_number,c.account_name, h.sales_mode , h.invoice_amount,h.voucher_date order by  h.voucher_number  ")
	List<Object> getSalesProfitReportWithBatch(String branchcode, Date fromdate, Date todate, String companymstid);
	// new query end===========================
	@Query(nativeQuery = true, value = "select h.voucher_number,c.account_name, h.sales_mode , h.invoice_amount,h.voucher_date, sum(d.cost_price * d.qty), d.batch from sales_trans_hdr h,account_heads c ,sales_dtl d "

			+ " WHERE h.id=d.sales_trans_hdr_id and  h.branch_code=:branchcode and date(voucher_date)>=:fromdate and date(voucher_date)<=:todate  and h.company_mst=:companymstid and c.id=h.customer_id and h.voucher_number is not null  group by h.voucher_number,c.account_name, h.sales_mode , h.invoice_amount,h.voucher_date,d.batch order by  h.voucher_number  ")
	List<Object> getSalesProfitReportWithBatchDerby(String branchcode, Date fromdate, Date todate, String companymstid);

//order by  h.voucher_number

	@Query(nativeQuery = true, value = "select h.voucher_number,c.account_name, h.sales_mode , "
			+ "sr.receipt_amount,sr.receipt_mode,h.voucher_date from sales_trans_hdr h,account_heads c ,sales_receipts sr "

			+ " WHERE  h.branch_code=:branchcode and "
			+ "sr.sales_trans_hdr_id = h.id and date(h.voucher_date) between :fdate and :tdate and "
			+ "h.company_mst=:companymstid and c.id=h.customer_id and h.voucher_number is not null and"
			+ " h.user_id=:userid order by  h.voucher_number ")

	List<Object> getUserWiseSummarySalesReportDtl(String branchcode, String userid, Date fdate, Date tdate,
			String companymstid);

	@Query(nativeQuery = true, value = "select i.item_name, sum(d.qty),sum(d.amount),h.voucher_date,h.voucher_number,c.account_name "
			+ " from sales_trans_hdr h,account_heads c, sales_dtl d, item_mst i "
			+ " where d.sales_trans_hdr_id=h.id and c.id=h.account_heads and i.id=d.item_id and h.voucher_number is not null "
			+ " and date(h.voucher_date) between :fdate and :tdate and h.user_id=:userid  and "
			+ " h.company_mst=:companymstid "
			+ " and  h.branch_code=:branchcode group by i.item_name,h.voucher_date,h.voucher_number,c.account_name "
			+ " order by  h.voucher_number  ")
	List<Object> getUserWiseSalesReport(String branchcode, String userid, Date fdate, Date tdate, String companymstid);

//	@Query(nativeQuery = true, value = "select c.customer_name,  sum( (d.rate - p.amount) * d.qty) "
//			+ " from sales_trans_hdr h, price_definition p, sales_dtl d, price_defenition_mst pmst , item_mst i, customer_mst c  "
//			+ "where h.branch_code = :branchCode and  d.item_id = i.id and pmst.id=p.price_id and "
//			+ "d.sales_trans_hdr_id=h.id and d.item_id=p.item_id "
//			+ "and pmst.price_level_name='COST PRICE' and (date(h.voucher_date) >= date(p.start_date) and "
//			+ "(date(h.voucher_date) <= date(p.end_date)) OR (date(p.end_date) is NULL)) and "
//			+ "date(h.voucher_date) between :sdate and :edate and h.company_mst=:companyMst and h.customer_mst=c.id "
//			+ " group by c.customer_name")
	
	@Query(nativeQuery = true, value = "select c.account_name,  sum( (d.rate - p.amount) * d.qty) "
			+ " from sales_trans_hdr h, price_definition p, sales_dtl d, price_defenition_mst pmst , item_mst i, account_heads c  "
			+ "where h.branch_code = :branchCode and  d.item_id = i.id and pmst.id=p.price_id and "
			+ "d.sales_trans_hdr_id=h.id and d.item_id=p.item_id "
			+ "and pmst.price_level_name='COST PRICE' and (date(h.voucher_date) >= date(p.start_date) and "
			+ "(date(h.voucher_date) <= date(p.end_date)) OR (date(p.end_date) is NULL)) and "
			+ "date(h.voucher_date) between :sdate and :edate and h.company_mst=:companyMst and h.account_heads=c.id "
			+ " group by c.account_name")

	List<Object> getCustomerWiseProfit(Date sdate, Date edate, CompanyMst companyMst, String branchCode);

	@Query(nativeQuery = true, value = "select i.item_name,i.hsn_code,u.unit_name," + "sum(d.qty)," + "d.rate,"
			+ "sum(d.qty*rate) as value,sum(d.cgst_amount)," + "sum(d.sgst_amount),d.tax_rate," + "d.cess_rate "
			+ "from item_mst i,unit_mst u,sales_trans_hdr h,sales_dtl d where d.sales_trans_hdr_id=h.id and "
			+ "d.item_id=i.id and d.unit_id=u.id and date(h.voucher_date) >= :fromDate and "
			+ "date(h.voucher_date) <= :toDate and h.branch_code = :branchcode and h.company_mst =:companyMst "
			+ "group by i.item_name,i.hsn_code,u.unit_name,d.tax_rate,d.cess_rate,d.rate order by i.hsn_code")
	List<Object> getHsnSalesDtlReportWithoutGroup(Date fromDate, Date toDate, String branchcode, CompanyMst companyMst);

	@Query(nativeQuery = true, value = "select i.item_name,i.hsn_code,u.unit_name," + "sum(d.qty)," + "d.rate,"
			+ "sum(d.qty*rate) as value,sum(d.cgst_amount)," + "sum(d.sgst_amount),d.tax_rate," + "d.cess_rate "
			+ "from item_mst i,unit_mst u,sales_trans_hdr h,sales_dtl d,category_mst c"
			+ " where c.id = i.category_id and d.sales_trans_hdr_id=h.id and "
			+ "d.item_id=i.id and d.unit_id=u.id and date(h.voucher_date) >= :fromDate and "
			+ "date(h.voucher_date) <= :toDate and h.branch_code = :branchcode and h.company_mst =:companyMst"
			+ " and c.category_name in (:array) "
			+ "group by i.item_name,i.hsn_code,u.unit_name,d.tax_rate,d.cess_rate,d.rate " + "order by i.hsn_code")
	List<Object> getHsnSalesDtlReportWithGroup(Date fromDate, Date toDate, String[] array, String branchcode,
			CompanyMst companyMst);

	// -------------------new version 1.1 surya
	@Query(nativeQuery = true, value = "select h.sales_mode,d.TAX_RATE,sum(d.qty*d.rate),sum(d.CGST_AMOUNT),"
			+ "sum(d.CESS_AMOUNT), sum(d.SGST_AMOUNT),sum(d.IGST_AMOUNT),sum(d.AMOUNT),"
			+ "sum(d.CGST_AMOUNT+d.CESS_AMOUNT+d.SGST_AMOUNT+d.IGST_AMOUNT) "
			+ " from sales_trans_hdr h, sales_dtl d where date(h.voucher_date)>=:date and date(h.voucher_date)<=:tdate and "
			+ " d.sales_trans_hdr_id=h.id and (h.sales_mode='B2C' or h.sales_mode='B2B') and "
			+ " h.branch_code=:branchCode and h.company_mst=:companymst group by h.sales_mode,d.TAX_RATE order by d.TAX_RATE")
	List<Object> getNewB2CSalesReportBetweenDate(Date date, Date tdate, String branchCode, CompanyMst companymst);
	// -------------------new version 1.1 surya end
	// -------------------new version 2.5 surya end

	@Query(nativeQuery = true, value = "select count(*) from sales_trans_hdr where date(voucher_date)=:date")
	Double getCountOfSalesHdrByDate(Date date);

	@Query(nativeQuery = true, value = "select sum(h.PAID_AMOUNT),sum(h.CARDAMOUNT),sum(h.CASH_PAY),sum(h.INVOICE_AMOUNT),"
			+ " sum(h.CHANGE_AMOUNT) "
			+ " from sales_trans_hdr h where date(h.voucher_date)>=:date and h.company_mst =:companyMst and h.branch_code =:branchcode "
			+ "and h.voucher_number is not null")
	List<Object> findSalesTransHdrSummaryByDate(Date date, CompanyMst companyMst, String branchcode);

	List<SalesTransHdr> findByVoucherDate(Date voucherDate);

	@Query(nativeQuery = true, value = "select h.invoice_amount,c.account_name,"
			+ "h.voucher_date,h.voucher_number,h.id from sales_trans_hdr h,account_heads c"
			+ " where h.customer_id=c.id and (h.sales_mode = 'B2B' or h.sales_mode='B2C') and "
			+ "h.company_mst =:companyMst and h.branch_code=:branchCode and "
			+ "date(h.voucher_date) >= :date and date(h.voucher_date) <=:tdate ")
	List<Object> getWholesSalesInvoiceReport(Date date, Date tdate, String branchCode, CompanyMst companyMst);

	void deleteBySaleOrderHrdId(String orderid);

	@Query(nativeQuery = true, value = "select sr.receipt_mode,sum(sr.receipt_amount) from sales_receipts sr,"
			+ "sales_trans_hdr sh where sr.sales_trans_hdr_id=sh.id and date(sh.voucher_date)=:date "
			+ "and sh.branch_code=:branchcode and sh.company_mst=:companymstid  " + "group by sr.receipt_mode")
	List<Object> getSalesTransHdrByDateAndBranchCodeAndCompanyMstId(Date date, String branchcode, String companymstid);

	@Query(nativeQuery = true, value = "select sh.sales_mode,sum(sr.receipt_amount) "
			+ "from sales_trans_hdr sh, sales_receipts sr where  "
			+ " (sh.sales_mode='B2B' or sh.sales_mode='B2C') and sh.branch_code=:branchcode "
			+ "and date(sh.voucher_date)=:date and sh.company_mst = :companymstid "
			+ " and sr.sales_trans_hdr_id=sh.id and sr.receipt_mode='CREDIT' group by sh.sales_mode")
	List<Object> getSalesTransHdrCreditSplit(Date date, String branchcode, String companymstid);

	@Query(nativeQuery = true, value = "select c.account_name,sum(sh.invoice_amount) from "
			+ "sales_trans_hdr sh,account_heads c where c.id=sh.customer_id and "
			+ "sh.sales_mode='ONLINE' and date(sh.voucher_date)=:date and "
			+ "sh.branch_code=:branchcode and sh.company_mst = :companymstid group by c.account_name")
	List<Object> getSalesTransHdrOnline(Date date, String branchcode, String companymstid);

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr"
			+ " where date(voucher_date)=:rdate and serving_table_name=:tablename " + " and voucher_number is null")
	List<SalesTransHdr> getKotNumberBytableName(String tablename, Date rdate);

	// -------------------new version 2.5 surya end

	@Query(nativeQuery = true, value = "select b.branch_name , sum(sh.invoice_amount) "
			+ "from sales_trans_hdr sh,branch_mst b " + "where b.branch_code=sh.branch_code and  "
			+ " sh.company_mst=:companymstid and date(sh.voucher_date )=:date  group by b.branch_name ")

	List<Object> findallSaleOrderinoiceReport(String companymstid, Date date);

	@Query(nativeQuery = true, value = "select h.voucher_number as svoucher , "
			+ "h.voucher_date,c.account_name,h.invoice_amount, b.branch_name, o.voucher_number as ovoucher "
			+ " from sales_trans_hdr h, account_heads c, branch_mst b, other_branch_sales_trans_hdr o "
			+ "where h.BRANCH_SALE_CUSTOMER=c.id and h.account_heads=b.id and "
			+ "h.voucher_number is not null and date(h.voucher_date)>=:sdate and "
			+ "date(h.voucher_date)<=:edate and h.company_mst=:companymstid and h.branch_code=:branchcode and "
			+ "o.sales_trans_hdr_id=h.id")
	List<Object> getBranchSaleHdrReportSummary(String companymstid, String branchcode, Date sdate, Date edate);

//	@Query(nativeQuery = true, value = "select b.branch_name,m.item_name,d.qty,d.tax_rate,d.mrp,u.unit_name,"
//			+ "d.amount,c.customer_name from sales_trans_hdr h,sales_dtl d,item_mst m,branch_mst b,customer_mst c,"
//			+ "unit_mst u where "
//			+ "h.company_mst=:companymstid and h.voucher_number=:vouchernumber and date(h.voucher_date)=:date "
//			+ " and b.branch_code=h.branch_code and h.branch_code=:branchcode and "
//			+ "d.item_id = m.id and h.id=d.sales_trans_hdr_id and c.id=h.customer_id and u.id=d.unit_id")
	
	@Query(nativeQuery = true, value = "select b.branch_name,m.item_name,d.qty,d.tax_rate,d.mrp,u.unit_name,"
			+ "d.amount,c.account_name from sales_trans_hdr h,sales_dtl d,item_mst m,branch_mst b,account_heads c,"
			+ "unit_mst u where "
			+ "h.company_mst=:companymstid and h.voucher_number=:vouchernumber and date(h.voucher_date)=:date "
			+ " and b.branch_code=h.branch_code and h.branch_code=:branchcode and "
			+ "d.item_id = m.id and h.id=d.sales_trans_hdr_id and c.id=h.customer_id and u.id=d.unit_id")
	List<Object> getVoucherReprint(String companymstid, String vouchernumber, String branchcode, Date date);

	@Query(nativeQuery = true, value = " select sum(invoice_amount) from sales_trans_hdr where date(voucher_date)>=:date"
			+ " and date(voucher_date)<=:tdate and voucher_number is not null and company_mst=:companymstid")
	Double dailySalesReportTotalSales(Date date, String companymstid, Date tdate);

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr where voucher_number=:invoiceno "
			+ "and company_mst=:companyMst and branch_code=:branchcode and date(voucher_date)=:date")
	SalesTransHdr findByVoucherNumberAndCompanyMstAndBranchCodeAndVoucherDate(String invoiceno, CompanyMst companyMst,
			String branchcode, Date date);

	@Query(nativeQuery = true, value = "select  sum(sr.receipt_amount) as receipt_amount from sales_receipts sr , "
			+ "	  			   sales_trans_hdr sh , receipt_mode_mst rm ,sales_order_trans_hdr so "
			+ "	  			   where sh.id= sr.sales_trans_hdr_id and rm.receipt_mode = sr.receipt_mode and "
			+ " sh.branch_code = :branchcode  and sh.company_mst = :companyMst   "
			+ "	  		   and sh.voucher_number is not null and   date(sh.voucher_date) = :fdate "
			+ "	  and sh.sale_order_hrd_id=so.id and date(so.voucher_date)<>:fdate and so.voucher_number is not null")
	Double dailySalesReportTotalPreviousAdvance(Date fdate, String branchcode, String companyMst);

	@Query(nativeQuery = true, value = "select  min( numeric_voucher_number) from sales_trans_hdr h "
			+ " where date(h.voucher_date) =:vDate and  "
			+ "( INVOICE_NUMBER_PREFIX  not in ( select  sales_prefix   from sales_type_mst  ) OR INVOICE_NUMBER_PREFIX  is null  ) "
			+ " and h.branch_code=:branch and h.company_mst=:companymstid and voucher_number like :salesprefix  ")

	Integer postStartingVoucherNumberByPreFix(Date vDate, String branch, String companymstid, String salesprefix);

	@Query(nativeQuery = true, value = "select  max(numeric_voucher_number) from sales_trans_hdr h "
			+ " where date(h.voucher_date) =:vDate and "
			+ "( INVOICE_NUMBER_PREFIX  not in ( select  sales_prefix   from sales_type_mst  ) OR INVOICE_NUMBER_PREFIX  is null  ) "
			+ "and h.branch_code=:branch and h.company_mst=:companymstid and voucher_number like :salesprefix")

	Integer posEndingVoucherNumberByPreFix(Date vDate, String branch, String companymstid, String salesprefix);

	@Query(nativeQuery = true, value = "select * from sales_trans_hdr where " + "date(voucher_date)= :fdate")
	List<SalesTransHdr> getSalesTransHdrForDate(Date fdate);

//	//--------------Sibi-----22-4-2021...///
	@Query(nativeQuery = true, value = "select c.*  from Sales_Trans_Hdr  c where c.sales_man_id = :salesman and "
			+ " c.serving_table_name=:tablename and c.kot_number=:kotnumber and date(voucher_date)=:date" + " AND  "
			+ "c.voucher_Number is null")

	List<SalesTransHdr> printKotBySalesmanAndKotNumber(String salesman, String tablename, String kotnumber, Date date);

	@Query(nativeQuery = true, value = "select c.*  from Sales_Trans_Hdr  c where c.sales_man_id = :salesman and "
			+ " c.serving_table_name=:tablename and c.kot_number=:kotnumber")
	List<SalesTransHdr> printRunningKot(String salesman, String tablename, String kotnumber);

	// ------------Print Invoice -----Call API to print KOT. passing Table Name and
	// KOT number -------

	@Query(nativeQuery = true, value = "select c.*  from sales_trans_hdr  c where c.kot_number = :kotnumber AND c.serving_table_name = :tablename AND "
			+ "c.voucher_Number is not null and date(c.voucher_date)=:vdate")
	List<SalesTransHdr> printKotInvoiceByTableNameAndKotNumber(String kotnumber, String tablename, Date vdate);

	// ------------Print performa Invoice -----Call API to print KOT. passing Table
	// Name and KOT number -------

	@Query(nativeQuery = true, value = "select c.*  from sales_trans_hdr  c where c.kot_number = :kotnumber AND c.serving_table_name = :tablename AND "
			+ "c.voucher_Number is null and date(c.voucher_date)=:vdate")

	List<SalesTransHdr> printPerformaInvoiceByTableNameAndKotNumber(String kotnumber, String tablename, Date vdate);

	@Query(nativeQuery = true, value = "select sum(d.receipt_amount)  from sale_order_receipt d,"
			+ " sales_order_trans_hdr so, sales_trans_hdr sh where so.id=d.sale_order_trans_hdr_id "
			+ " and so.company_mst=:companymstid and so.branch_code=:branchcode  "
			+ " and so.id=sh.sale_order_hrd_id and  date(sh.voucher_date)=:date and sh.voucher_number is not null "
			+ " and date(so.voucher_date)<:date and d.receipt_mode ='CASH' " + "   ")
	Double dayEndReceptModeWisePreviousCashSalesReport(Date date, String companymstid, String branchcode);

	List<SalesTransHdr> findBySaleOrderHrdId(String orderid);

	@Query(nativeQuery = true, value = "SELECT sth.voucher_date,sth.voucher_number,ac.account_name,cat.category_name,itm.item_name,itm.item_code,sd.batch,"
			+ "sd.expiry_date,sd.qty,sd.rate,sd.tax_rate,sd.cost_price,sth.invoice_amount,urs.user_name,sm.sales_man_name,"
			+ "SUM(sd.qty*sd.mrp) AS salevalue,SUM(sd.cost_price*sd.qty) AS costvalue,SUM(sd.cgst_amount+sd.igst_amount+sd.sgst_amount) AS gst,"
			+ "ac.party_gst "
			+ "FROM sales_trans_hdr sth,sales_dtl sd,user_mst urs,sales_man_mst sm,item_mst itm,category_mst cat,account_heads ac "
			+ "WHERE sth.id=sd.sales_trans_hdr_id AND sd.item_id=itm.id AND sth.user_id=urs.id AND itm.category_id=cat.id AND sth.customer_id=ac.id "
			+ "AND sth.sales_man_id=sm.id AND date(sth.voucher_date) >= :fromDate AND date(sth.voucher_date) <= :toDate AND "
			+ "sth.sales_mode = 'B2B' "
			+ "GROUP BY sth.voucher_date,sth.voucher_number,ac.account_name,cat.category_name,itm.item_name,itm.item_code,sd.batch,"
			+ "sd.expiry_date,sd.qty,sd.rate,sd.tax_rate,sd.cost_price,sth.invoice_amount,urs.user_name,sm.sales_man_name")
	List<Object> findWholeSaleDetailReport(Date fromDate, Date toDate);

	// (0_0) SharonN

	@Query(nativeQuery = true, value = "SELECT " + "sth.voucher_date," + "sth.voucher_number," + "ac.account_name,"
			+ "sth.invoice_amount," + "sth.cash_pay," + "sth.cardamount," + "sth.credit_amount," + "p.id,"
			+ "r.receipt_amount " + "FROM " + "sales_trans_hdr sth," + "sales_receipts r,"
			+ "account_heads ac,patient_mst p," + "receipt_mode_mst rm " + "WHERE " + "sth.id=r.sales_trans_hdr_id AND"
			+ " r.receipt_mode=rm.receipt_mode AND" + " sth.patient_mst=p.id AND" + " ac.id=:groupId AND"
			+ " sth.customer_id=ac.id  AND" + " sth.voucher_date >=:fromDate AND" + " sth.voucher_date <=:toDate "
			+ "GROUP BY " + "sth.voucher_date," + "sth.voucher_number," + "ac.account_name," + "sth.invoice_amount,"
			+ "sth.cash_pay," + "sth.cardamount," + "sth.credit_amount")

	List<Object> findPoliceReport(Date fromDate, Date toDate, String groupId);

//  (0_0)       SharonN    

	@Query(nativeQuery = true, value = "SELECT " + "sth.voucher_date," + "sth.voucher_number," + "ac.account_name,"
			+ "sth.invoice_amount," + "ac.party_gst," + "sth.invoice_discount," + "s.sales_man_name " + "FROM "
			+ "sales_trans_hdr sth," + "account_heads ac," + "sales_man_mst s " + "WHERE "
			+ " sth.sales_man_id=s.id AND" + " sth.customer_id=ac.id  AND" + " sth.voucher_date >=:fromDate AND"
			+ " sth.voucher_date <=:toDate " + "GROUP BY " + "sth.voucher_date," + "sth.voucher_number,"
			+ "ac.account_name," + "sth.invoice_amount," + "ac.party_gst," + "sth.invoice_discount,"
			+ "s.sales_man_name")
	List<Object> findPharmacyDailySalesTransactionReport(Date fromDate, Date toDate);

//--------------------sibi..................//

//@Query(nativeQuery = true, value = "select "
//        + "h.voucher_number,"
//        + "h.voucher_type,"
//        + "h.invoice_amount,"
//        + "h.credit_amount,"
//		+ "c.customer_name,p.policy_type from sales_trans_hdr h,customer_mst c,patient_mst p where "
//		+ "c.id=h.customer_mst and p.id = h.patient_mst and date(voucher_date)=:fudate ")

	@Query(nativeQuery = true, value = "select " + "h.voucher_number," + "h.voucher_type," + "h.invoice_amount,"
			+ "sr.receipt_amount," + "ac.account_name from sales_trans_hdr h,account_heads ac,sales_receipts sr  where "

			+ "ac.id=h.account_heads and  h.id=sr.sales_trans_hdr_id and sr.receipt_mode='CREDIT' and date(voucher_date)=:fudate ")

	List<Object> getPharmacyDayEndReport(Date fudate);

	@Query(nativeQuery = true, value = "SELECT " + "SUM(sr.receipt_amount) "
			+ "FROM sales_receipts sr,sales_trans_hdr sth " + "WHERE date(sth.voucher_date) =:fudate AND "
			+ "sr.sales_trans_hdr_id = sth.id AND " + "sr.receipt_mode =:branchCash AND "
			+ "sth.company_mst=:companymstid ")
	Double getCashSales(Date fudate, String branchCash, CompanyMst companymstid);

	@Query(nativeQuery = true, value = "SELECT SUM(pd.amount) " + "FROM payment_dtl pd,payment_hdr ph "
			+ "WHERE pd.paymenthdr_id = ph.id AND " + "date(ph.voucher_date) =:fudate AND "
			+ "pd.mode_of_payment =:branchCash AND " + "ph.company_mst=:companymstid ")
	Double getCashPaid(Date fudate, String branchCash, CompanyMst companymstid);

	@Query(nativeQuery = true, value = "SELECT SUM(rd.amount) " + "FROM receipt_dtl rd,receipt_hdr rh "
			+ "WHERE rd.receipt_hdr_id = rh.id AND " + "date(rh.voucher_date) =:fudate AND "
			+ "rd.mode_ofpayment =:branchCash AND " + "rh.company_mst =:companymstid ")
	Double getcashReceived(Date fudate, String branchCash, CompanyMst companymstid);

	@Query(nativeQuery = true, value = "select "

			+ "SUM(credit_amount) - SUM(debit_amount) " + "from ledger_class " + "where "
			+ "date(trans_date) <:fudate and account_id  =:branchCash " + "and company_mst =:companymstid ")

	Double getOpeningBalance(Date fudate, String branchCash, CompanyMst companymstid);

	@Query(nativeQuery = true, value = "select " + "sum(credit_amount) - sum(debit_amount)  \n" + "from ledger_class \n"
			+ "where date(trans_date) =:fudate and \n" + "account_id  =:branchCash  and \n"
			+ "company_mst =:companymstid ")

	Double getclosingBalance(Date fudate, String branchCash, CompanyMst companymstid);

	@Query(nativeQuery = true, value = "select id from account_heads where account_name=:branchCash")

	String AccountIdByAccountName(String branchCash);

	@Query(nativeQuery = true, value = "SELECT count(*)  FROM sales_trans_hdr sth where date(sth.voucher_date)=:fromDate and "
			+ "sth.company_mst=:companyMst and sth.branch_code=:branchcode")
	Double getSalesTransHdrCount(CompanyMst companyMst, Date fromDate, String branchcode);

	@Modifying
	@Query(nativeQuery = true, value = "delete from sales_dtl where "
			+ "id in (select d.id from sales_dtl d, sales_trans_hdr h where d.sales_trans_hdr_id=h.id and "
			+ " date(h.voucher_date)<=:date and h.company_mst=:companyMst)")
	void deleteSalesDtlsByVoucherDate(CompanyMst companyMst, Date date);

	@Modifying
	@Query(nativeQuery = true, value = "delete from sales_trans_hdr where "
			+ "date(voucher_date)<=:date and company_mst=:companyMst)")
	void deleteSalesHdrByVoucherDate(CompanyMst companyMst, Date date);

	
	@Modifying
	@Query(nativeQuery = true, value = "update sales_trans_hdr set posted_to_server='YES' where id=:hdrIds")
	void updatePostedToServerStatus(String hdrIds);

	
	

	
	//=============MAP-131-api-for-fetch-kot-details===============anandu==================
	@Query(nativeQuery = true, value ="select * from sales_trans_hdr "
			+ "where voucher_number is null and sales_mode='KOT' ")
	List<SalesTransHdr> getKotDetail();

	//=============MAP-131-api-for-fetch-kot-details===============anandu=============end=====
	
	
	@Query(nativeQuery = true, value = "select * from sales_trans_hdr where "
			+ "date(voucher_date)>= :fdate and date(voucher_date) <= :tdate and id not in (:hdrId)")
	List<SalesTransHdr> getSalesTransHdrBetweenDate(Date fdate, Date tdate, Pageable topFifty,String hdrId);
	
	
	
	@Query(nativeQuery = true, value = "SELECT B.branch_code, sum(B.cash), sum(B.card), sum(B.credit), sum(B.card2) from ( " + 
			"SELECT A.branch_code as branch_code , sum(A.cash) as cash , sum(A.card) as card , " + 
			"sum(A.credit) as credit , 0 as card2   from ( " + 
			"select s.branch_code ,  " + 
			"case when s.receipt_mode like '%CASH' then sum(s.receipt_amount) else 0 END as cash,  " + 
			"case when s.receipt_mode not like '%CASH' and receipt_mode<>'CREDIT'  " + 
			"then sum(s.receipt_amount) else 0 END as card,  " + 
			"case when s.receipt_mode = 'CREDIT' then sum(s.receipt_amount) else 0 END as credit   " + 
			"from sales_receipts s, sales_trans_hdr h " + 
			"where s.sales_trans_hdr_id=h.id and date(h.voucher_date)= :date and  " + 
			"h.voucher_number is not null and h.company_mst= :companymstid " + 
			"group by branch_code, receipt_mode ) A group by A.branch_code  " + 
			"union "
			+ "select h.branch_code, 0,0,0, sum(d.amount) as card2  " + 
			"from sales_dtl d , sales_trans_hdr h  " + 
			"where h.id = d.sales_trans_hdr_id and  " + 
			"d.item_id in (select id from temp_item_mst) " + 
			"and date(h.voucher_date)= :date " + 
			"group by h.branch_code "
			+ "  ) B  " + 
			" group by B.branch_code "
			)
	List<Object> getDayEndWebReportForHC(java.sql.Date date, String companymstid);

	@Query(nativeQuery = true, value ="select u.user_name, "
			+ "round(sum(d.rate * d.qty),2) as amount, "
			+ "h.id from sales_trans_hdr h, "
			+ "sales_dtl d, "
			+ "user_mst u"
			+ " where "
			+ "h.id=d.sales_trans_hdr_id and "
			+ "h.company_mst=:companymstid and "
			+ "h.branch_code=:branch and "
			+ "h.voucher_number is NULL  "
			+ "group by h.id;")
	ArrayList getPosDetail(String companymstid, String branch);

	@Query(nativeQuery = true, value ="select sum(d.qty) from sales_trans_hdr h, sales_dtl d "
			+ "where h.id = d.sales_trans_hdr_id and date(h.voucher_date) >= :fudate and "
			+ "date(h.voucher_date) <= :tudate and d.item_id = :itemId")
	Double getSalesQtyByItemId(Date fudate, Date tudate, String itemId);

	@Query(nativeQuery = true, value ="select * from sales_trans_hdr where date(voucher_date) >= :fromDate "
			+ "and date(voucher_date) <= :toDate and voucher_number is not null")
	List<SalesTransHdr> findAllSalesBetweenDate(Date fromDate, Date toDate);

	
	@Query(nativeQuery = true, value = "select sum(d.mrp*qty) "
			+ " from sales_trans_hdr h , sales_dtl d  "
			+ " where d.sales_trans_hdr_id=h.id and  "
			+ "  "
			+ " h.company_mst=:companymstid "
			+ " and  h.id =:hdrid")
	double getInvoiceAmountByIdAndCompanyMst(String hdrid,  String companymstid);


}
