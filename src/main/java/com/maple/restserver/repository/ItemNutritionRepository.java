package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemNutrition;

@Repository
public interface ItemNutritionRepository extends JpaRepository<ItemNutrition, String>  {

	List<ItemNutrition> findByCompanyMst(CompanyMst companyMst);

	
	
}
