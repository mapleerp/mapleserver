package com.maple.restserver.repository;

import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.SchOfferAttrInst;

@Repository
public interface ItemBatchMstRepository extends JpaRepository<ItemBatchMst, String>, ItemBatchMstRepositoryCustom {

	List<ItemBatchMst> findByItemIdAndBatchAndBarcode(String itemid, String batch, String barcode);
//	
//	@Query(nativeQuery=true,value="select * from item_batch_mst i,kit_defenition_mst k where i.item_id=k.item_id") 
//	List<ItemBatchMst> findKitItem();

	List<ItemBatchMst> findByBatchAndCompanyMstId(String batch, String companymstid);

	@Query(nativeQuery = true, value = "SELECT * FROM item_batch_mst"
			+ "   WHERE item_id = :itemid and batch = :batch   ")
	List<ItemBatchMst> findByItemIdAndBatch(String itemid, String batch);

	@Query(nativeQuery = true, value = "SELECT * FROM item_batch_mst"
			+ "   WHERE item_id = :itemid and barcode = :barcode   ")
	List<ItemBatchMst> findByItemIdAndBarcode(String itemid, String barcode);

	List<ItemBatchMst> findByItemIdOrderByExpDate(String itemid);

	List<ItemBatchMst> findByItemId(String itemid);

	List<ItemBatchMst> findByCompanyMstId(String companymstid);

//	@Query(nativeQuery = true, value = " select i.item_name, sum(d.qty_in - d.qty_out) as stockqty, "
//			+ "i.standard_price," + " b.branch_name ,c.category_name"
//			+ " from item_mst i, branch_mst b, category_mst c,item_batch_dtl d where  "
//			+ "d.branch_code = b.branch_code " + "and d.company_mst=:companymstid and "
//			+ "b.branch_code= :branchcode and " + "c.id=i.category_id and " + "i.id=d.item_id  and " + "c.id=:categoryid"
//			+ " and date(d.voucher_date)<= :fromdate "
//			+ " group by i.item_name, i.standard_price, b.branch_name , c.category_name  order by c.category_name")
//	List<Object> findByBranchCodeAndDateAndCompanyMstId(String branchcode, String companymstid, Date fromdate,
//			String categoryid);
	//.........................Sibi ..................//
	@Query(nativeQuery = true, value = " select i.item_name, sum(d.qty_in - d.qty_out) as stockqty, "
			+ "i.standard_price," + " b.branch_name ,c.category_name,d.batch,d.exp_date"
			+ " from item_mst i, branch_mst b, category_mst c,item_batch_dtl d where  "
			+ "d.branch_code = b.branch_code " + "and d.company_mst=:companymstid and "
			+ "b.branch_code= :branchcode and " + "c.id=i.category_id and " + "i.id=d.item_id  and " + "c.id=:categoryid"
			+ " and date(d.voucher_date)<= :fromdate "
			+ " group by i.item_name, i.standard_price, b.branch_name , c.category_name  order by c.category_name")
	List<Object> findByBranchCodeAndDateAndCompanyMstId(String branchcode, String companymstid, Date fromdate,
			String categoryid);
	/*
	 * Code for Derby - Stock Report
	 */
	@Query(nativeQuery = true, value = " select i.item_name, sum(d.qty_in - d.qty_out) as stockqty, "
			+ "i.standard_price," + " b.branch_name ,c.category_name,d.batch,d.exp_date"
			+ " from item_mst i, branch_mst b, category_mst c,item_batch_dtl d where  "
			+ "d.branch_code = b.branch_code " + "and d.company_mst=:companymstid and "
			+ "b.branch_code= :branchcode and " + "c.id=i.category_id and " + "i.id=d.item_id  and " + "c.id=:categoryid"
			+ " and date(d.voucher_date)<= :fromdate "
			+ " group by i.item_name, i.standard_price, b.branch_name , c.category_name, d.batch,d.exp_date  order by c.category_name")
	List<Object> findByBranchCodeAndDateAndCompanyMstIdDerby(String branchcode, String companymstid, Date fromdate,
			String categoryid);
	//.........................Sibi ..................//

//	from stock_trans_out_hdr  where voucher_date BETWEEN:startDate AND :endDate")
	@Query(nativeQuery = true, value = "select sum(d.qty*d.mrp) " + "as value ,im.item_name,sum(d.qty) ,"
			+ "d.mrp,h.voucher_date from sales_trans_hdr h,sales_dtl d," + "item_mst im,category_mst c "
			+ " where h.id=d.sales_trans_hdr_id and d.item_id=im.id and c.id=im.category_id " + " and c.id=:categoryid "
			+ "and date(h.voucher_date) between :fromdate and :todate " + "and h.branch_code=:branchCode "
			+ "and h.company_mst=:companymstid group by im.item_name,d.mrp,h.voucher_date ")
	List<Object> getCategoryWiseStockSummaryReport(String companymstid, String categoryid, Date fromdate, Date todate,
			String branchCode);

	@Query(nativeQuery = true, value = "select c.category_name,sum(d.qty*d.mrp) as value,c.id "
			+ " from sales_trans_hdr h,sales_dtl d,category_mst c,"
			+ " item_mst im where  h.id=d.sales_trans_hdr_id and c.id=im.category_id and im.id=d.item_id"
			+ " and date(h.voucher_date) between :fromdate and :todate "
			+ " and h.branch_code=:branchCode and h.company_mst=:companymstid group by c.category_name,c.id ")
	List<Object> getAllCategorySalesSummaryReport(String companymstid, Date fromdate, Date todate, String branchCode);

	@Query(nativeQuery = true, value = " select u.unit_name, " + " c.company_name," + " i.item_name, "
			+ " d.voucher_number," + " sum(d.qty_in) as qtyin," + " sum(d.qty_out),"
			+ " sum(i.standard_price * d.qty_in) as inwardValue," + "sum(i.standard_price * d.qty_out) as outwardValue,"
			+ " d.voucher_date,d.branch_code," + " sum(d.qty_in - d.qty_out) as closingStock,"

			+ " d.particulars,d.source_voucher_number,d.updated_time "

			+ " from item_batch_dtl d ,company_mst c,item_mst i,"
			+ "  branch_mst b,unit_mst u where d.company_mst=c.id and "
			+ " d.branch_code=b.branch_code and d.item_id=i.id and d.branch_code=:branchCode and "
			+ " date(d.updated_time) between :fromdate and :todate and d.company_mst=:companymstid and"
			+ " d.item_id=:itemid and u.id=i.unit_id group by "
			+ " u.unit_name,c.company_name,i.item_name,d.voucher_number,d.source_voucher_number,"
			+ " d.voucher_date,d.branch_code,d.particulars,d.updated_time ORDER BY d.updated_time ")
	List<Object> getStockSummaryDtls(String companymstid, String itemid, Date fromdate, Date todate, String branchCode);

	@Query(nativeQuery = true, value = "select sum(d.qty_in - d.qty_out) as openingStock,u.unit_name from "
			+ " item_batch_dtl d,unit_mst u,item_mst i where d.item_id=i.id and u.id=i.unit_id and"
			+ " d.item_id=:itemid and date(d.updated_time)<:fromdate "
			+ "and d.branch_code=:branchCode and d.company_mst=:companymstid group by u.unit_name")

	List<Object> getClosingQty(String companymstid, String itemid, Date fromdate, String branchCode);

	@Query(nativeQuery = true, value = "select sum(d.qty_in - d.qty_out) as openingStock from "
			+ " item_batch_dtl d and date(d.voucher_date)<:fromdate "
			+ "and d.branch_code=:branchCode and d.company_mst=:companymstid")

	List<Object> getOpeningQtyforPandL(String companymstid, Date fromdate, String branchCode);

	@Query(nativeQuery = true, value = "select sum(d.qty_in - d.qty_out) as closingStock  from "
			+ "item_batch_dtl d , item_mst i where "
			+ " d.item_id=:itemid and d.barcode = i.bar_code and d.item_id = i.id "
			+ "  and d.company_mst=:companymstid  and i.bar_code = :barcode and d.batch = :batch  ")

	Double getClosingQtyItemIdAndBathAndBarcode(String companymstid, String itemid, String batch, String barcode);

	@Query(nativeQuery = true, value = "select sum(d.qty_in) - sum(d.qty_out) as closingStock  from "
			+ "item_batch_dtl d , item_mst i where "
			+ " d.item_id = :itemid and d.barcode = i.bar_code and d.item_id = i.id "
			+ "  and d.company_mst=:companymstid  and i.bar_code = :barcode " + " and d.batch = :batch and "
			+ "date(d.voucher_date) <= :voucherDate ")

	Double getClosingQtyItemIdAndBathAndBarcodeAsOnDate(String companymstid, String itemid, String batch,
			String barcode, java.sql.Date voucherDate);

	@Query(nativeQuery = true, value = "select sum(d.qty_in) - sum(d.qty_out) as closingStock  from "
			+ "item_batch_dtl d , item_mst i where "
			+ " d.item_id = :itemid and d.barcode = i.bar_code and d.item_id = i.id "
			+ "  and d.company_mst=:companymstid  and i.bar_code = :barcode AND "

			+ "date(d.voucher_date) <= :voucherDate ")

	Double getClosingQtyItemIdAndAndBarcodeAsOnDate(String companymstid, String itemid, String barcode,
			java.sql.Date voucherDate);

	/*
	 * update stock from dtl
	 * 
	 * 
	 * update item_batch_dtl set branch_code ='MFP'; insert into item_batch_mst(id,
	 * barcode, batch, branch_code, item_id, qty, company_mst) select item_id ,
	 * barcode, batch, branch_code, item_id, sum(qty_in) - sum(qty_out) ,
	 * company_mst from item_batch_dtl group by barcode, batch, branch_code, item_id
	 * , company_mst ;
	 */

	@Modifying(flushAutomatically = true)
	@Query(nativeQuery = true, value = "  delete from  item_batch_mst ")

	void updateStockFromDtlStep0();

	@Modifying(flushAutomatically = true)
	@Query(nativeQuery = true, value = "  update item_batch_dtl set branch_code = :branchcode where branch_code is null ")

	void updateStockFromDtlStep1(String branchcode);

	@Modifying(flushAutomatically = true)
	@Query(nativeQuery = true, value = "  insert into item_batch_mst(id,    barcode, batch, branch_code, item_id, qty, company_mst) "
			+ " select item_id||batch||branch_code||company_mst, barcode, batch, branch_code, item_id, sum(qty_in) - sum(qty_out) , company_mst from item_batch_dtl "
			+ " group by item_id ,  barcode, batch, branch_code,  company_mst ")

	void updateStockFromDtlStep2();

//select A.item_id,A.batch,A.barcode from(select item_id,batch , barcode,sum(qty_in) - sum(qty_out) as qty ,exp_date,company_mst from item_batch_dtl where store='MAIN' and item_id is not null and batch is not null%20%20%20%20 group by item_id ,%20 barcode,batch,exp_date ) A group by A.item_id,A.batch,A.barcode

	@Modifying(flushAutomatically = true)
	@Query(nativeQuery = true, value = "insert into item_batch_mst(id,barcode, batch, branch_code, item_id, qty, company_mst,exp_date) "
			+ " select TRIM(cast( row_number() over () as CHAR(15)))," + " A.barcode," + " A.batch," + " A.branch_code,"
			+ " A.item_id," + " A.qty," + " A.company_mst," + " A.exp_date" + " from(select item_id,batch ,"
			+ " barcode,sum(qty_in- qty_out) as qty ," + "company_mst,branch_code, exp_date from item_batch_dtl "
			+ "where date(voucher_date)<=:curDate and store='MAIN' and item_id is not null "
			+ "and batch is not null group by item_id ,barcode,batch,company_mst,exp_date,branch_code) A")

	void updateStockFromDtlStep2withDate(java.sql.Date curDate);

	@Query(nativeQuery = true, value = "select  A.barcode," + " A.batch," + " A.branch_code," + "   A.item_id,"
			+ " A.qty ,A.store " + " from(select item_id,batch ," + " barcode,sum(qty_in ) - sum(qty_out) as qty ,"
			+ " branch_code ,store from item_batch_dtl " + " where date(voucher_date)<=:curDate  and item_id is not null  "
			+ "  group by item_id ,barcode,batch,branch_code,store) A")
	List<Object> getClosingStockAsOnDate(Date curDate);

//
//select TRIM(cast( row_number() over () as CHAR(15))),A.barcode,
//A.batch,A.branch_code,A.item_id,A.qty from(select item_id,batch ,
//		barcode,sum(qty_in- qty_out) as qty ,company_mst,branch_code 
//		from item_batch_dtl where store='MAIN' and item_id is not null 
//		and batch is not null group by item_id ,barcode,batch,company_mst,branch_code) A

//A group by A.item_id,A.batch,A.barcode,A.branch_code,A.qty,A.company_mst,A.exp_date
//@Modifying(flushAutomatically = true)
//	@Query(nativeQuery=true,value="insert into item_batch_mst(id,barcode, batch, branch_code, item_id, qty, company_mst,exp_date) " + 
//			" select item_id||batch||branch_code||company_mst , barcode, batch, branch_code, item_id, sum(qty_in) - sum(qty_out) , company_mst,exp_date from item_batch_dtl"
//			+ " where date(voucher_date)<=:curDate and store='MAIN' and item_id is not null "
//			+ " and batch is not null and branch_code is not null and company_mst is not null" + 
//			" group by item_id ,  barcode, batch, branch_code,  company_mst , exp_date   ")
//
//	//item_id||batch||branch_code||company_mst
//	void	updateStockFromDtlStep2withDate( Date curDate);

	@Modifying(flushAutomatically = true)
	@Query(nativeQuery = true, value = "insert into item_batch_mst(id,barcode, batch,  item_id, qty, company_mst) "
			+ " select item_id||company_mst, barcode, 'NOBATCH',  item_id, 0 , company_mst from kit_definition_mst"
			+ " where item_id=:itemId and company_mst is not null"
			+ " group by item_id ,  barcode, branch_code,  company_mst   ")

	void updateItemBatchMstWithKit(String itemId);

	@Modifying(flushAutomatically = true)
	@Query(nativeQuery = true, value = " update item_batch_mst u set mrp = ( select standard_price from item_mst p where u.item_id = p.id ) ")

	void updateStockFromDtlStep3();

	@Modifying
	@Query(nativeQuery = true, value = "update item_batch_mst set qty =0 where batch='NOBATCH' and qty <0")
	void correctNegativStock();

	@Modifying(flushAutomatically = true)
	@Query(nativeQuery = true, value = " update item_mst set item_name  = trim(item_name) ")

	void updateStockFromDtlStep4();

	List<ItemBatchMst> findByCompanyMstAndItemId(CompanyMst companyMst, String itemid);

	ItemBatchMst findByCompanyMstAndItemIdAndBatch(CompanyMst companyMst, String itemid, String batch);

	@Modifying(flushAutomatically = true)
	@Query(nativeQuery = true, value = " update item_batch_mst set barcode =:barCode  where item_id=:itemid")
	void udateBarcode(String itemid, String barCode);

	@Query(nativeQuery = true, value = " select i.item_name, sum(d.qty_in - d.qty_out) as stockqty, "
			+ "i.standard_price," + " b.branch_name, c.category_name,d.batch "
			+ " from item_mst i, branch_mst b, category_mst c, item_batch_dtl d where  "
			+ "d.branch_code = b.branch_code " + "and d.company_mst=:companymstid and b.branch_code= :branchcode and "
			+ "c.id=i.category_id and i.id=d.item_id and " + "c.id=:categoryid "
			+ " and date(d.voucher_date)<= :fromdate "
			+ " group by i.item_name, i.standard_price, b.branch_name,d.batch, c.category_name order by c.category_name")
	List<Object> findByBranchCodeAndDateAndCompanyMstIdAndBatch(String branchcode, String companymstid, Date fromdate,
			String categoryid);

	@Query(nativeQuery = true, value = " select c.category_name,i.item_name,i.item_code,ib.batch,ib.expiry_date,ib.qty,ib.mrp from category_mst c,item_mst i,item_batch_mst ib where ib.item_id = i.id and i.category_id = c.id "
			+ "and ib.branch_code=:branchcode and ib.company_mst=:companymstid and ib.item_id=:itemId")

	List<Object> findByShortExpiryItem(String companymstid, String branchcode, String itemId);

	@Query(nativeQuery = true, value = " select u.unit_name, " + " c.company_name," + " i.item_name, "
			+ "  d.branch_code," + " sum(d.qty_in - d.qty_out) as closingStock,"
			+ " sum( (d.qty_in - d.qty_out) * i.standard_price) as closingValue "
			+ " from item_batch_dtl d , company_mst c,item_mst i,"
			+ "  branch_mst b,unit_mst u where d.company_mst=c.id and "
			+ " d.branch_code=b.branch_code and d.item_id=i.id and d.branch_code=:branchCode  "
			+ " and date(d.voucher_date) <= :todate" + " and d.company_mst=:companymstid  "
			+ "   and u.id=i.unit_id and i.category_id=:categoryid group by "
			+ "  u.unit_name,c.company_name,i.item_name , " + "  d.branch_code  ")
	List<Object> getAllStockSummaryValue(String companymstid, Date todate, String branchCode, String categoryid);

//
//@Query(nativeQuery=true,value="select i.item_name, "
//		+ "sum(d.qty_in - d.qty_out) as closingStock, "
//		+ "sum( (d.qty_in - d.qty_out) * i.standard_price) as closingValue "
//		+ "from item_batch_dtl d , company_mst c,item_mst i "
//		+ "where d.item_id=i.id and d.branch_code=:branchCode "
//		+ "and d.company_mst=:companymstid and i.category_id=:categoryid "
//		+ "and date(d.voucher_date) <= :todate "
//		+ "group by c.company_name,i.item_name, d.branch_code" + 
//		"")
//   List<Object> getAllStockSummaryValue( String companymstid,   
//      Date todate, String branchCode, String categoryid);

	@Query(nativeQuery = true, value = " select u.unit_name, " + " c.company_name," + " i.item_name, "
			+ "  d.branch_code," + " sum(d.qty_in - d.qty_out) as closingStock," + " d.item_id, u.id "
			+ " from item_batch_dtl d , company_mst c,item_mst i," + "  branch_mst b,unit_mst u"
			+ " where d.company_mst=c.id and "
			+ " d.branch_code=b.branch_code and d.item_id=i.id and d.branch_code=:branchCode and "
			+ "i.category_id=:categoryid and " + " date(d.voucher_date) <= :tDate and d.company_mst=:companymstid  "
			+ "   and u.id=i.unit_id " + " group by " + "  u.unit_name,c.company_name,i.item_name , "
			+ "  d.branch_code,d.item_id, u.id  ")
	List<Object> getAllStockSummaryByCostPrice(String companymstid, Date tDate, String branchCode, String categoryid);

//@Query(nativeQuery=true,value=" select u.unit_name, "
//		+ " c.company_name,"
//		+ " i.item_name, "
//			+ "  d.branch_code,"
//		+ " sum(d.qty_in - d.qty_out) as closingStock,"
//		+ " sum( (d.qty_in - d.qty_out) * p.amount) as closingValue "
//			+ " from item_batch_dtl d , company_mst c,item_mst i,"
//		+ "  branch_mst b,unit_mst u, price_definition p "
//		+ " where d.company_mst=c.id and "
//		+ " d.branch_code=b.branch_code and d.item_id=i.id and d.branch_code=:branchCode and "
//		+ "i.category_id=:categoryid and "
//		+ " date(d.voucher_date) <= :tDate and d.company_mst=:companymstid  "
//		+ "   and u.id=i.unit_id and p.item_id=d.item_id and p.unit_id=i.unit_id and p.end_date is not null and p.price_id=:priceid"
//		+ " group by "
//		+ "  u.unit_name,c.company_name,i.item_name , "
//		+ "  d.branch_code ")

	@Query(nativeQuery = true, value = " select i.item_name, sum(d.qty_in - d.qty_out) as stockqty, "
			+ "i.standard_price," + " b.branch_name " + " from item_mst i, branch_mst b, item_batch_dtl d where  "
			+ "d.branch_code = b.branch_code " + "and d.company_mst=:companymstid and b.branch_code= :branchcode and "
			+ " i.id=d.item_id " + " and date(d.voucher_date)<= :fromdate "
			+ " group by i.item_name, i.standard_price, b.branch_name order by i.item_name")
	List<Object> getDailyStockReport(String branchcode, String companymstid, Date fromdate);

	@Query(nativeQuery = true, value = " select u.unit_name, " + " i.item_name, " + " sum(d.qty_in),"
			+ " sum(d.qty_out)," + " sum(i.standard_price * d.qty_in) as inwardValue,"
			+ "sum(i.standard_price * d.qty_out) as outwardValue," + " sum(d.qty_in - d.qty_out) as closingStock"
			+ " from item_batch_dtl d ,company_mst c,item_mst i,"
			+ "  branch_mst b,unit_mst u where d.company_mst=c.id and "
			+ " d.branch_code=b.branch_code and d.item_id=i.id and d.branch_code=:branchCode and "
			+ " date(d.voucher_date) between :fromdate and :todate and d.company_mst=:companymstid and"
			+ " d.item_id=:itemid and u.id=i.unit_id group by " + " u.unit_name,i.item_name")
	List<Object> getStockSummaryDtlsByItem(String companymstid, String itemid, Date fromdate, Date todate,
			String branchCode);

	@Modifying
	@Query(nativeQuery = true, value = "delete from item_batch_mst where trim(item_id)=:item_id and trim(batch)=:batch")
	void deleteByItemIdAndBatch(String item_id, String batch);

	@Query(nativeQuery = true, value = "select i.item_name ,sum(d.qty_in-d.qty_out) as currentstock,u.unit_name from item_mst i,item_batch_dtl d,unit_mst u where i.id=d.item_id and date(d.voucher_date)<=:date and d.company_mst=:companymstid and d.branch_code=:branchcode and d.item_id=:itemid and i.unit_id=u.id group by i.item_name ,u.unit_name  ")
	List<Object> getItemWiseStockReport(String branchcode, String companymstid, java.util.Date date, String itemid);

	@Modifying
	@Query(nativeQuery = true, value = "update item_batch_mst set barcode =:barCode where item_id=:itemid")
	void updateBarcode(String itemid, String barCode);

	@Query(nativeQuery = true, value = "select sum(d.qty_in-d.qty_out) as currentstock from item_batch_dtl_daily d where  date(d.voucher_date)<=:fromDate and d.item_id=:productId ")

	Double getItemWiseStock(Date fromDate, String productId);

	@Query("SELECT m FROM ItemBatchMst m WHERE LOWER(m.itemId) like   LOWER(:itemId) ")
	List<ItemBatchMst> searchByItemName(String itemId);

	@Query(nativeQuery = true, value = "select id.voucher_date,c.category_name , im.item_code,ib.batch,id.particulars,u.unit_name ,id.voucher_number,id.qty_in,id.qty_out,id.mrp ,im.item_name from item_batch_mst ib ,item_batch_dtl id ,item_mst im,category_mst c,unit_mst u where id.item_id=im.id and im.category_id=c.id and u.id=im.unit_id and date(id.voucher_date) between :fdate and :tdate and id.branch_code=:branchCode and c.category_name IN (:categoryArray) ")
	List<Object> getPharmacyInventoryMovementReportByCategory(Date fdate, Date tdate, String branchCode,
			String[] categoryArray);

	@Query(nativeQuery = true, value = "select id.voucher_date,c.category_name , "
			+ "im.item_code,id.batch,id.particulars,u.unit_name ,id.voucher_number,"
			+ "id.qty_in,id.qty_out,id.mrp ,im.item_name from "
			+ "item_batch_dtl id ,item_mst im,category_mst c,unit_mst u where "
			+ "id.item_id=im.id and im.category_id=c.id and u.id=im.unit_id and "
			+ "date(id.voucher_date)>=:fdate and date(id.voucher_date)<=:tdate " + "and id.branch_code=:branchCode "
			+ "and c.category_name IN (:categoryArray) " + "and im.item_name IN (:itemArray) and  " + "id.batch=:batch")

	List<Object> getPharmacyInventoryMovementReportByCatItemBatch(Date fdate, Date tdate, String branchCode,
			String[] itemArray, String[] categoryArray, String batch);

	@Query(nativeQuery = true, value = "select id.voucher_date,c.category_name , im.item_code,id.batch,id.particulars,u.unit_name ,id.voucher_number,id.qty_in,id.qty_out,id.mrp ,im.item_name from item_batch_dtl id ,item_mst im,category_mst c,unit_mst u where id.item_id=im.id and im.category_id=c.id and u.id=im.unit_id  and date(id.voucher_date) between :fdate and :tdate and id.branch_code=:branchCode and  c.category_name IN (:categoryArray) and im.item_name IN (:itemArray) ")
	List<Object> getPharmacyInventoryMovementReportByCatItem(Date fdate, Date tdate, String branchCode,
			String[] itemArray, String[] categoryArray);

	@Query(nativeQuery = true, value = "select sum(id.qty_in-id.qty_out) from item_batch_dtl id,item_mst i where id.item_id=:itemId and date(id.voucher_date)<:fDate and id.branch_code=:branchCode ")
	Double getOpeningStock(Date fDate, String branchCode, String itemId);

	@Query(nativeQuery = true, value = "select sum(id.qty_in-id.qty_out) from item_batch_dtl id,item_mst i where id.item_id=:itemId and date(id.voucher_date) between:fDate and :tDate and id.branch_code=:branchCode ")
	Double getCurrentStock(Date fDate, Date tDate, String branchCode, String itemId);

	@Query(nativeQuery = true, value = "select sum(id.qty_in-id.qty_out) from item_batch_dtl id,item_mst i where id.item_id=:itemId and date(id.voucher_date)<:fDate and id.branch_code=:branchCode and id.batch=:batch")
	Double getOpeningStockBatchWise(Date fDate, String branchCode, String itemId, String batch);

	@Query(nativeQuery = true, value = "select sum(id.qty_in-id.qty_out) from item_batch_dtl id,item_mst i where id.item_id=:itemId and date(id.voucher_date) between:fDate and :tDate and id.branch_code=:branchCode,id.batch=:batch")
	Double getCurrentStockBatchWise(Date fDate, Date tDate, String branchCode, String itemId, String batch);

	@Query(nativeQuery = true, value = "select sum(id.qty_in-id.qty_out) from item_batch_dtl id,item_mst i  where id.item_id=:itemId and date(id.voucher_date)<:fDate and id.branch_code=:branchCode and i.category_id=:categoryId")
	Double getOpeningStockByCategory(Date fDate, String branchCode, String categoryId);

	@Query(nativeQuery = true, value = "select sum(id.qty_in-id.qty_out) from item_batch_dtl id,item_mst i where i.category_id=:categoryId and date(id.voucher_date) between:fDate and :tDate and id.branch_code=:branchCode")
	Double getCurrentStockByCategory(Date fDate, Date tDate, String branchCode, String categoryId);

	@Query(nativeQuery = true, value = " select i.item_name, sum(d.qty_in - d.qty_out) as stockqty, "
			+ "i.standard_price," + " b.branch_name,c.category_name "
			+ " from item_mst i, branch_mst b, item_batch_dtl d,category_mst c where  "
			+ "d.branch_code = b.branch_code " + "and d.company_mst=:companymstid and b.branch_code= :branchcode and "
			+ " i.id=d.item_id and c.id=i.category_id " + " and date(d.voucher_date)<= :fromdate and d.store='MOB'"
			+ " group by i.item_name, i.standard_price, b.branch_name,c.category_name order by c.category_name,i.item_name")
	List<Object> getDailyMobileStockReport(String branchcode, String companymstid, Date fromdate);

	@Query(nativeQuery = true, value = " select i.item_name, sum(d.qty_in - d.qty_out) as stockqty, "
			+ "i.standard_price," + " b.branch_name,c.category_name "
			+ " from item_mst i, branch_mst b, item_batch_dtl d,category_mst c where  "
			+ "d.branch_code = b.branch_code " + "and d.company_mst=:companymstid and b.branch_code= :branchcode and "
			+ " i.id=d.item_id and c.id=i.category_id "
			+ " and date(d.voucher_date)<= :date and d.store='MOB' and c.category_name IN (:array)"
			+ " group by i.item_name, i.standard_price, b.branch_name,c.category_name order by c.category_name,i.item_name")
	List<Object> getDailyMobileStockReportByCategory(String branchcode, String companymstid, Date date, String[] array);

	@Query(nativeQuery = true, value = " select i.item_name, sum(d.qty_in - d.qty_out) as stockqty, "
			+ "i.standard_price," + " b.branch_name,c.category_name "
			+ " from item_mst i, branch_mst b, item_batch_dtl d,category_mst c where  "
			+ "d.branch_code = b.branch_code " + "and d.company_mst=:companymstid and b.branch_code= :branchcode and "
			+ " i.id=d.item_id and c.id=i.category_id "
			+ " and date(d.voucher_date)<= :date and d.store='MOB' and i.item_name =:itemname"
			+ " group by i.item_name, i.standard_price, b.branch_name,c.category_name order by c.category_name,i.item_name")

	List<Object> getDailyMobileStockReportByItemName(String branchcode, String companymstid, Date date,
			String itemname);

	@Query(nativeQuery = true, value = "SELECT  i.item_name, sum(d.qty_in - d.qty_out) as stockqty "
			+ "  FROM item_batch_dtl d, item_mst i " + "   WHERE i.id=d.item_id and d.store=:store and "
			+ " d.branch_code=:branchcode and d.company_mst=:companymstid group by i.item_name ")
	List<Object> getStoreWiseStockReport(String branchcode, String companymstid, String store);

	@Modifying
	@Query(nativeQuery = true, value = "update item_batch_mst m set exp_date=( select "
			+ "e.expiry_date from item_batch_expiry_dtl e where e.batch=m.batch and e.item_id=m.item_id)")
	void updateExpiryDateFromItemBatchExpiryDtl();

	@Query(nativeQuery = true, value = "select amount from  batch_price_definition where "
			+ "item_id = :itemid and batch = :batch and "
			+ " start_date <=:startdate and end_date <=:enddate and "
			+ " end_date >= start_date")
	List<Object> findByCompanyMstIdAndDate(String itemid,String batch,Date startdate,Date enddate);
	
	
	
	
	
	@Query(nativeQuery = true, value = " SELECT itm.item_name, sum(d.qty_in - d.qty_out) as stockqty, "
			+ "itm.standard_price, br.branch_name ,c.category_name,SUM(bpd.amount) AS totalamount"
			+ " FROM item_mst itm, branch_mst br, category_mst c,item_batch_dtl d,"
			+ "batch_price_definition bpd,price_defenition_mst pdm where  "
			+ "bpd.branch_code = br.branch_code and "
			+ "bpd.branch_code= :branchcode and c.id=itm.category_id and itm.id=bpd.item_id and "
			+ "d.item_id = itm.id "
			+ "and bpd.price_id = pdm.id and date(bpd.start_date)>= :fdate and "
			+ "(date(bpd.end_date)<= :tdate or date(bpd.end_date) is null) and pdm.price_level_name=:pricetype "
			+ "group by itm.item_name, itm.standard_price, br.branch_name , "
			+ "c.category_name  order by c.category_name")
	List<Object> getPricetypeWiseStockReport(Date fdate, Date tdate, String pricetype, String branchcode);
	
	List<ItemBatchMst> findByItemIdAndBatchAndBarcodeAndStore(String itemId, String batch, String barcode,
			String store);

//	@Query(nativeQuery = true, value = "SELECT SUM(qty) FROM item_batch_mst WHERE item_id = :itemid AND batch = :batch")
//	Double findQtyFromItemBatchMst(String itemid, String batch);

	
	
	@Query(nativeQuery=true,value=" select sum(qty) as qty from item_batch_mst d "
				+ " where d.item_id=:itemId and d.batch=:batch and d.store=:store")
		Double findQtyByItemAndBatch(String itemId, String batch,String store);

	
	
	

	
}
