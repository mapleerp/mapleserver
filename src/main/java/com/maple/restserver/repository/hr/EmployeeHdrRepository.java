package com.maple.restserver.repository.hr;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.hr.EmployeeHdr;

@Repository
@Transactional
public interface EmployeeHdrRepository extends JpaRepository<EmployeeHdr, String>{


	List<EmployeeHdr> findByCompanyMst(CompanyMst companyMst);

//	List<EmployeeHdr> findByAge(Integer age);
	

}
