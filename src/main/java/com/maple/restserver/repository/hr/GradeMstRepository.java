package com.maple.restserver.repository.hr;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.hr.GradeMst;

@Repository
@Transactional
public interface GradeMstRepository extends JpaRepository <GradeMst, String>{

	List<GradeMst> findByCompanyMst(CompanyMst companyMst);
	

}
