package com.maple.restserver.repository.hr;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.hr.EmployeeDtl;

@Repository
@Transactional
public interface EmployeeDtlRepository extends JpaRepository<EmployeeDtl,String> {

	
	List<EmployeeDtl>findByCompanyMst(String companymstid);

	List<EmployeeDtl> findByCompanyMstIdAndEmployeeHdrId(String companymstid, String hdrid);

}


