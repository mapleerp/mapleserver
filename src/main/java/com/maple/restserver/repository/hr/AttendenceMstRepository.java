package com.maple.restserver.repository.hr;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.hr.AttendenceMst;
import com.maple.restserver.entity.hr.SalaryCalculationMst;

@Repository
@Transactional
public interface AttendenceMstRepository extends JpaRepository< AttendenceMst , String>{

	List<AttendenceMst> findByCompanyMst(CompanyMst companyMst);

}
