package com.maple.restserver.repository;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.GroupMst;
import com.maple.restserver.entity.HeartBeatsMst;

@Component
@Repository
public interface HeartBeatsMstRepository extends JpaRepository<HeartBeatsMst, String>{

	List<HeartBeatsMst> findByCompanyMst(CompanyMst companyMst);

	
	
	@Modifying
	@Query(nativeQuery=true,value="update heart_beats_mst set status='OFFLINE' where company_mst=:companymstid")
	void updateOffLineStatus(String companymstid);


	
}
