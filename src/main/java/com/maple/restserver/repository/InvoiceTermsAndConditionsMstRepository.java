package com.maple.restserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.InvoiceTermsAndConditionsMst;

@Repository
public interface InvoiceTermsAndConditionsMstRepository extends JpaRepository<InvoiceTermsAndConditionsMst,String>{

	void deleteByCompanyMstIdAndId(String id, String companymstid);

}
