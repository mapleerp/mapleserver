package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maple.restserver.entity.RawMaterialReturnDtl;

public interface RawMaterialReturnDtlRepository extends JpaRepository<RawMaterialReturnDtl,String> {

	
	List<RawMaterialReturnDtl> findByRawMaterialReturnHdrId(String id);
}
