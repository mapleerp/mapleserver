package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.ReceiptInvoiceDtl;
@Repository
public interface ReceiptInvoiceDtlRepository extends JpaRepository<ReceiptInvoiceDtl, String> {

	List<ReceiptInvoiceDtl>	findByReceiptHdr(ReceiptHdr  receiptHdr);
	@Query(nativeQuery=true,value="select sum(d.recieved_amount) from receipt_invoice_dtl d where"
			+ " d.receipt_hdr=:receipthdr")
	  
Double	getReceivedAmount(String receipthdr);
	
	@Query(nativeQuery=true,value="select * from receipt_invoice_dtl d "
			+ "where d.customer_mst_id=:customerMstid and d.invoice_number='OWN ACCOUNT' ")
	 List<ReceiptInvoiceDtl> findByCustomerMstIdAndOwnAccount(String  customerMstid);
	
	@Modifying
	@Query(nativeQuery=true,value="delete from receipt_invoice_dtl where receipt_hdr =:receipthdr")
	void deleteByReceiptHdrId(String receipthdr);
	
	  @Modifying
	  
	  @Query(nativeQuery = true,
	  value="update receipt_invoice_dtl d set d.customer_registration =:toAccountId where d.customer_registration =:fromAccountId "
	  ) void accountMerge(String fromAccountId, String toAccountId);
	 
}
