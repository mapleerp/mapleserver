
package com.maple.restserver.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.his.entity.InsuranceDtl;
import com.maple.restserver.report.entity.InsuranceWiseSalesReport;

@Component
@Repository
public interface InsuranceDtlRepository extends JpaRepository<InsuranceDtl,Integer>{

	
	List<InsuranceDtl>findByCompanyMstId(String companymstid);
	//List<InsuranceDtl> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
	
	
	
//	@Query(nativeQuery=true,value="SELECT sth.voucher_date, sth.voucher_number,pm.id,sth.invoice_amount,sth.credit_amount,"
//			+ "sth.cash_pay,sth.cardamount, cm.customer_name,um.user_name,pm.insurance_card,pm.policy_type,icm.insurance_company_name "
//			+ "FROM sales_trans_hdr sth,customer_mst cm,user_mst um,patient_mst pm,insurance_company_mst icm "
//			+ "WHERE sth.customer_id=cm.id AND sth.user_id=um.id AND sth.patient_mst=pm.id AND "
//			+ "pm.insurance_company_id=icm.id AND sth.voucher_date >= :sdate AND sth.voucher_date <= :edate")
	
	@Query(nativeQuery=true,value="SELECT sth.voucher_date, sth.voucher_number,pm.id,sth.invoice_amount,sth.credit_amount,"
			+ "sth.cash_pay,sth.cardamount, cm.account_name,um.user_name,pm.insurance_card,pm.policy_type,icm.insurance_company_name "
			+ "FROM sales_trans_hdr sth,account_heads cm,user_mst um,patient_mst pm,insurance_company_mst icm "
			+ "WHERE sth.customer_id=cm.id AND sth.user_id=um.id AND sth.patient_mst=pm.id AND "
			+ "pm.insurance_company_id=icm.id AND sth.voucher_date >= :sdate AND sth.voucher_date <= :edate")
	List<Object> findInsurancewisesales(Date sdate, Date edate);
	
}
