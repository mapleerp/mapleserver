

package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.his.entity.SingleRoomMst;



@Repository
public interface SingleRoomMstRepository extends JpaRepository<SingleRoomMst,Integer>{
//	List<SingleRoomMst> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
