package com.maple.restserver.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.SalesDtlTemp;

@Repository
public interface SalesDtlTempRepository extends JpaRepository<SalesDtlTemp, String> {

	

	@Modifying(flushAutomatically = true)
	 @Query(nativeQuery = true,value="INSERT INTO sales_dtl_temp "
	 		+ "(id,item_id,qty,rate,cgst_tax_rate,sgst_tax_rate,cess_rate,add_cess_rate,igst_tax_rate,item_taxax_id,unit_id,item_name,expiry_date,batch,barcode,tax_rate,mrp,amount,unit_name,discount,cgst_amount,sgst_amount,igst_amount,cess_amount,returned_qty,status,add_cess_amount,cost_price,print_kot_status,company_mst,offer_reference_id,scheme_id,standard_price,fb_key,monthly_sales_trans_hdr_id) "
	 		+ "SELECT d.id,d.item_id,sum(d.qty),d.rate,d.cgst_tax_rate,d.sgst_tax_rate,d.cess_rate,d.add_cess_rate,d.igst_tax_rate,d.item_taxax_id,d.unit_id,d.item_name,d.expiry_date,d.batch,d.barcode,d.tax_rate,d.mrp,sum(d.qty*d.rate),d.unit_name,sum(d.discount),sum(d.cgst_amount),sum(d.sgst_amount),sum(d.igst_amount),sum(d.cess_amount),sum(d.returned_qty),d.status,sum(d.add_cess_amount),d.cost_price,d.print_kot_status,d.company_mst,d.offer_reference_id,d.scheme_id,d.standard_price,d.fb_key,d.monthly_sales_trans_hdr_id FROM monthly_sales_dtl d,monthly_sales_trans_hdr h  "
	 		+ " group by d.id,d.item_id,d.rate,d.cgst_tax_rate,d.sgst_tax_rate,d.cess_rate,d.add_cess_rate,d.igst_tax_rate,d.item_taxax_id,d.unit_id,d.item_name,d.expiry_date,d.batch,d.barcode,d.tax_rate,d.mrp,d.unit_name,d.status,d.cost_price,d.print_kot_status ,d.company_mst,d.offer_reference_id,d.scheme_id,d.standard_price,d.fb_key,d.monthly_sales_trans_hdr_id   "
	 		)
	 void insertingIntoSalesDtlTem ();
	
	
	

	@Modifying(flushAutomatically = true)
	 @Query(nativeQuery = true,value="INSERT INTO monthly_sales_dtl (id,item_id,qty,rate,cgst_tax_rate,sgst_tax_rate,cess_rate,add_cess_rate,igst_tax_rate,item_taxax_id,unit_id,item_name,expiry_date,batch,barcode,tax_rate,mrp,amount,unit_name,discount,cgst_amount,sgst_amount,igst_amount,cess_amount,returned_qty,status,add_cess_amount,cost_price,print_kot_status,company_mst,offer_reference_id,scheme_id,standard_price,fb_key,monthly_sales_trans_hdr_id) "
	 		+ "SELECT TRIM(cast( row_number() over () as CHAR(15))),d.item_id,d.qty,d.rate,d.cgst_tax_rate,d.sgst_tax_rate,d.cess_rate,d.add_cess_rate,d.igst_tax_rate,d.item_taxax_id,d.unit_id,d.item_name,d.expiry_date,d.batch,d.barcode,d.tax_rate,d.mrp,d.amount,d.unit_name,d.discount,d.cgst_amount,d.sgst_amount,d.igst_amount,d.cess_amount,d.returned_qty,d.status,d.add_cess_amount,d.cost_price,d.print_kot_status,d.company_mst,d.offer_reference_id,d.scheme_id,d.standard_price,d.fb_key,d.monthly_sales_trans_hdr_id FROM sales_dtl_temp d   "
	 		
	 		)
	 void insertingIntoMonthlySalesDtlFromSalesDtlTemp();

	
	@Modifying
	@Query(nativeQuery = true, value= "delete from sales_dtl_temp")
	void deleteSalesDtlTempInitially();
}
