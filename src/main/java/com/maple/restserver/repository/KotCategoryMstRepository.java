package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.KotCategoryMst;

@Repository
public interface KotCategoryMstRepository extends JpaRepository<KotCategoryMst, String> {

	KotCategoryMst findByCategoryId(String categoryId);
	
	KotCategoryMst findByCompanyMstAndCategoryIdAndPrinterId(CompanyMst companymst,String categoryid,String printId);
	KotCategoryMst findByCompanyMstAndCategoryId(CompanyMst companymst,String categoryid);
	
	List<KotCategoryMst>findByCompanyMst(CompanyMst companymst);
	@Query(nativeQuery = true,value = "select i.category_id " + 
			"from item_mst i, kot_item_mst k " + 
			"where i.id = k.item_id " + 
			"and i.category_id not in (select category_id from kot_category_mst)")
	List<String> getCategoryNotInKotCategory();
}
