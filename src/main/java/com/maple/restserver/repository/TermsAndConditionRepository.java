package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.TermsAndConditionsMst;
@Component
@Repository
public interface TermsAndConditionRepository extends JpaRepository<TermsAndConditionsMst, String>{


	List<TermsAndConditionsMst> findByCompanyMst(CompanyMst companyMst);

	@Query(nativeQuery = true, value = "select * from terms_and_conditions_mst  where company_mst=:companyMst and "
			+ " invoice_format=:invoiceformat  order by serial_number")
	List<TermsAndConditionsMst> findByCompanyMstAndInvoiceFormat(CompanyMst companyMst, String invoiceformat);
	
	
}
