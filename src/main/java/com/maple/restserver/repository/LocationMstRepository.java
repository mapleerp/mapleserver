package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LocationMst;
import com.maple.restserver.entity.ProductMst;
@Component
@Repository
public interface LocationMstRepository extends JpaRepository<LocationMst, String>{

	List<LocationMst> findByCompanyMst(CompanyMst companyMst);
	
	@Query(nativeQuery = true,value="select l.location, l.id"
			+ " from  location_mst l"
			+ " where "

			+ " (lower(l.location) like :searchLocationName) and l.company_mst =:companyMst")

 
	List<Object> searchByLocationWithCompany(String  searchLocationName , Pageable topFifty,CompanyMst companyMst);

	LocationMst findByCompanyMstAndId(CompanyMst companyMst, String id);
	
}
