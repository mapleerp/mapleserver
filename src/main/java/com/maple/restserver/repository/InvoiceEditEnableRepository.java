package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CashBook;
import com.maple.restserver.entity.ClosingBalance;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.InvoiceEditEnableMst;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.UserMst;

 
 
@Component
@Repository
public interface InvoiceEditEnableRepository extends JpaRepository<InvoiceEditEnableMst,String>{

	List<InvoiceEditEnableMst> findByCompanyMst(CompanyMst companyMst);

	List<InvoiceEditEnableMst> findByVoucherNumberAndStatusAndVoucherDate(String voucherNumber,String status,Date voucherDate);
	
//	@Query(nativeQuery = true,value="select h.voucher_number,c.customer_name,ie.user_name "
//			+ "from sales_trans_hdr h, invoice_edit_enable_mst ie, customer_mst c where "
//			+ "h.customer_id=c.id and h.voucher_number=ie.voucher_number and "
//			+ "(h.sales_mode='B2B' OR h.sales_mode='B2C' OR h.sales_mode='ONLINE' OR h.sales_mode='BRANCH_SALES' ) "
//			+ " and h.company_mst=ie.company_mst and h.branch_code=ie.branch_code and "
//			+ "ie.user_mst=:userMst and date(h.voucher_date)=date(ie.voucher_date) and date(ie.voucher_date)=:date"
//			+ " and ie.company_mst=:companyMst and ie.branch_code=:branchcode and ie.voucher_type='SALES' and ie.status = 'N'")
	
	@Query(nativeQuery = true,value="select h.voucher_number,c.account_name,ie.user_name "
			+ "from sales_trans_hdr h, invoice_edit_enable_mst ie, account_heads c where "
			+ "h.customer_id=c.id and h.voucher_number=ie.voucher_number and "
			+ "(h.sales_mode='B2B' OR h.sales_mode='B2C' OR h.sales_mode='ONLINE' OR h.sales_mode='BRANCH_SALES' ) "
			+ " and h.company_mst=ie.company_mst and h.branch_code=ie.branch_code and "
			+ "ie.user_mst=:userMst and date(h.voucher_date)=date(ie.voucher_date) and date(ie.voucher_date)=:date"
			+ " and ie.company_mst=:companyMst and ie.branch_code=:branchcode and ie.voucher_type='SALES' and ie.status = 'N'")

	List<Object> findByCompanyMstAndUserMstAndBranchCodeAndVoucherDate(CompanyMst companyMst,
			UserMst userMst, String branchcode, Date date);


	@Query(nativeQuery = true,value="select s.account_name ,h.voucher_number,ie.branch_code"
			+ " from purchase_hdr h, invoice_edit_enable_mst ie, account_heads s where "
			+ "s.id=h.supplier_id and h.voucher_number=ie.voucher_number and "
			+ "  h.company_mst=ie.company_mst and h.branch_code=ie.branch_code and "
			+ " date(h.voucher_date)=date(ie.voucher_date) and date(ie.voucher_date)=:date"
			+ " and ie.company_mst=:companyMst and ie.branch_code=:branchcode and "
			+ "ie.voucher_type='PURCHASE'")

	List<Object> getSupplierAndVouchernumber(String branchcode, CompanyMst companyMst, Date date);

	
	

	@Query(nativeQuery = true,value="select h.voucher_number,h.voucher_date "
			+ "from receipt_hdr h, invoice_edit_enable_mst ie where "
			+ " h.voucher_number=ie.voucher_number and "
			+ "  h.company_mst=ie.company_mst and h.branch_code=ie.branch_code and"
			+ " ie.user_mst=:userId  and ie.status='N' and "
			+ " date(h.voucher_date)=date(ie.voucher_date) and date(ie.voucher_date)=:fdate"
			+ " and ie.company_mst=:companymstid and ie.branch_code=:branchcode and ie.voucher_type='RECEIPT'")


	List<Object> findByCompanyMstIdAndVoucherDate(String companymstid, String branchcode, Date fdate, String userId);

	//-------------------version 6.4 surya 
	
	@Query(nativeQuery = true,value="select * from invoice_edit_enable_mst em where "
			+ " user_mst=:userMst and  company_mst=:companyMst and voucher_number=:invoicenumber and "
			+ " voucher_type=:vouchertype and status=:status and date(voucher_date)=:fdate"
			+ " ")

	List<InvoiceEditEnableMst> findByUserMstAndCompanyMstAndVoucherNumberAndVoucherDateAndVoucherTypeAndStatus(
			UserMst userMst, CompanyMst companyMst, String invoicenumber, Date fdate, String vouchertype, String status);
	//-------------------version 6.4 surya end
	//-------------------version 6.5 surya 

	List<InvoiceEditEnableMst> findByVoucherNumberAndStatusAndVoucherDateAndBranchCode(String invoiceno, String string,
			Date date, String branchcode);
	//-------------------version 6.5 surya end

/*
 * select h.voucher_number,c.customer_name from sales_trans_hdr h, customer_mst c ,  invoice_edit_enable_mst ie where h.customer_id=c.id  and date(h.voucher_date)='2020-11-05' and h.company_mst='STM'  AND ie.voucher_number = h.voucher_number  
 * 
 * select * from sales_trans_hdr h, invoice_edit_enable_mst i where h.voucher_number = i.voucher_number 
 * 
 * 
 * select  i.*  from   invoice_edit_enable_mst i  , sales_trans_hdr h where h.voucher_number = i.voucher_number 
 */


	@Query(nativeQuery = true,value="select h.voucher_number,h.voucher_date,h.id "
			+ "from payment_hdr h, invoice_edit_enable_mst ie where "
			+ " h.voucher_number=ie.voucher_number and "
			+ "  h.company_mst=ie.company_mst and h.branch_code=ie.branch_code and"
			+ " ie.user_mst=:userId  and "
			+ " date(h.voucher_date)=date(ie.voucher_date) and date(ie.voucher_date)=:fdate"
			+ " and ie.company_mst=:companymstid and ie.branch_code=:branchcode and ie.voucher_type='PAYMENT'"
			+ " union "
			+ " select ph.voucher_number,ph.voucher_date,ph.id from payment_hdr ph where date(ph.voucher_date)=:fdate "
			+ " and ph.company_mst=:companymstid and ph.branch_code=:branchcode and voucher_number is null")
	List<Object>findPaymentByCompanyMstIdAndVoucherDate(String companymstid,String branchcode,Date fdate,String userId);

	
//	@Query(nativeQuery = true,value="select h.voucher_number,c.customer_name "
//			+ "from sales_trans_hdr h, customer_mst c where "
//			+ "h.customer_id=c.id and  "
//			+ "(h.sales_mode='B2B' OR h.sales_mode='B2C' OR h.sales_mode='ONLINE' OR h.sales_mode='BRANCH_SALES' ) "
//			+ " and date(h.voucher_date)=:date "
//			+ " and h.company_mst=:companyMst ")
	
	@Query(nativeQuery = true,value="select h.voucher_number,c.account_name "
			+ "from sales_trans_hdr h, account_heads c where "
			+ "h.customer_id=c.id and  "
			+ "(h.sales_mode='B2B' OR h.sales_mode='B2C' OR h.sales_mode='ONLINE' OR h.sales_mode='BRANCH_SALES' ) "
			+ " and date(h.voucher_date)=:date "
			+ " and h.company_mst=:companyMst ")


	List<Object> findByCompanyMstAndVoucherDate(CompanyMst companyMst, Date date);
	
	@Query(nativeQuery = true,value="select s.account_name ,h.voucher_number"
			+ " from purchase_hdr h,  account_heads s where "
			+ "s.id=h.supplier_id and  "
			+ " date(h.voucher_date)=:date "
			+ " and h.company_mst=:companyMst")

	List<Object> getAllSupplierAndVouchernumber(CompanyMst companyMst, Date date);
	
	@Query(nativeQuery = true,value="select h.voucher_number,h.voucher_date "
			+ "from receipt_hdr h where "
			+ "  h.company_mst=:companyMst and "
			+ " date(h.voucher_date)=:fdate "
			+ " ")

	List<Object> getAllReceiptsVoucherNumbers(CompanyMst companyMst, Date fdate);
	
	@Query(nativeQuery = true,value="select h.voucher_number,h.voucher_date "
			+ "from payment_hdr h where "
			+ "  h.company_mst=:companyMst and "
			+ " date(h.voucher_date)=:fdate "
			)
	List<Object> getAllPaymentVoucherNumbers(CompanyMst companyMst);

	@Query(nativeQuery = true,value="select h.voucher_number from invoice_edit_enable_mst h,user_mst u "
			+ "where h.voucher_type='STOCK TRANSFER' and date(h.voucher_date)=:vdate and h.company_mst=:companymstid"
			+ " and u.id=:userid and h.status='N' ")
	List<Object> getstockTransferVoucherNumber(Date vdate, String companymstid,String userid);

	@Query(nativeQuery = true,value = "select * from invoice_edit_enable_mst m where "
			+ "m.user_name=:username and m.vouche_number=:customername and "
			+ "m.status='N'")
	List<InvoiceEditEnableMst> getInvoiceEditEnableByCustomer(String username, String customername);
	
	
//	@Query(nativeQuery = true,value="select h.voucher_number,c.customer_name,ie.user_name "
//			+ "from sales_trans_hdr h, invoice_edit_enable_mst ie, customer_mst c where "
//			+ "h.customer_id=c.id and h.voucher_number=ie.voucher_number and "
//			+ "(h.sales_mode='B2B' OR h.sales_mode='B2C' OR h.sales_mode='ONLINE' OR h.sales_mode='BRANCH_SALES' ) "
//			+ " and h.company_mst=ie.company_mst and h.branch_code=ie.branch_code and "
//			+ "ie.user_mst=:userMst and date(h.voucher_date)=date(ie.voucher_date) and date(ie.voucher_date)=:date"
//			+ " and ie.company_mst=:companyMst and ie.branch_code=:branchcode and ie.voucher_type='SALES' and ie.status = 'N'")
//
//	List<Object> getOtherBranchSalesEditInvoiceNumbet(CompanyMst companyMst,
//			UserMst userMst, String branchcode, Date date);

}
 
