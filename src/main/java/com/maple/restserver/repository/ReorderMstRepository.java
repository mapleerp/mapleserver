package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReorderMst;

public interface ReorderMstRepository extends JpaRepository<ReorderMst, String>{
//	List<ReorderMst> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);


	 @Query(nativeQuery=true,value="select * from reorder_mst  where item_id =:itemName AND supplier_id=:supplierName")
	 List<ReorderMst>fetchreorderbyItemNameAndSupplierName(String itemName,String supplierName);
		
	/*
	 * @Query("SELECT p FROM Supplier p WHERE LOWER(p.supplierName) like   LOWER(:lastName)   "
	 * ) public List<Supplier> findSearch(@Param("lastName") String lastName);
	 */
	/*
	 * @Query("SELECT c FROM CustomerRegistration c WHERE LOWER(c.customerName) like   LOWER(:custmerName)"
	 * )
	 * 
	 * public List<CustomerRegistration> findSearch(@Param("custmerName") String
	 * custmerName);
	 * 
	 * itemName
	 */

		@Query("SELECT m FROM ReorderMst m WHERE item_id = :itemId   ")

 
		List<ReorderMst>findSearch(@Param("itemId") String  itemId);
 
// List<ReorderMst>findByItemNameAndSupplierName(String itemName,String SupplierName);endingDate
		

		@Query(" SELECT m FROM ReorderMst m WHERE item_id = :itemId  AND date(starting_date) <= :date AND date(ending_date )>= :date AND companyMst=:companymstid")

		List<ReorderMst>findSearchbydate(String itemId,  Date  date,CompanyMst  companymstid);

  
		@Query(" SELECT m FROM ReorderMst m WHERE item_id = :itemId  "
				+ "AND date(starting_date) <= :date AND date(ending_date )>= :date AND m.companyMst=:companymstid")
 
		List<ReorderMst>findSearchbydate(String itemId,  Date  date,String companymstid);




		

//List<Object> inventoryReorderStatusReport(String startDate,String endDate,String branchCode,String companymstid);



}


