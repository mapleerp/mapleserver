package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.OtherBranchPurchaseHdr;
import com.maple.restserver.entity.PurchaseHdr;
@Repository
public interface OtherBranchPurchaseHdrRepository extends JpaRepository<OtherBranchPurchaseHdr, String>{

	List<OtherBranchPurchaseHdr> findByCompanyMstId(String companymstid);
	 
@Query(nativeQuery=true,value=" SELECT * FROM other_branch_purchase_hdr p WHERE p.final_saved_status = 'N'  ") 
	List<OtherBranchPurchaseHdr> fetchPurchaseHdr();


@Query(nativeQuery=true,value=" SELECT * FROM other_branch_purchase_hdr p WHERE p.voucher_number = :vouchernumber and date(voucher_date) = :vdate ") 
List<OtherBranchPurchaseHdr> findByVoucherNumberAndVoucherDate(String vouchernumber, Date vdate);

OtherBranchPurchaseHdr findByVoucherNumberAndBranchCode(String vouchernumber, String branchcode);
@Query(nativeQuery = true,value="select *from other_branch_purchase_hdr h where h.branch_code=:branchCode and h.supplier_id=:suppplierid and h.company_mst=:companyMst and date(h.voucher_date) between :fromDate and :toDate  ")
List<OtherBranchPurchaseHdr> findBySupplier(String suppplierid, java.util.Date fromDate, java.util.Date toDate,
		String branchCode, CompanyMst companyMst);

@Query(nativeQuery = true,value="select s.account_name,h.voucher_number from other_branch_purchase_hdr h ,account_heads s where s.id=h.supplier_id and h.company_mst=:companyMst and h.branch_code=:branchcode and date(h.voucher_date)=:vDate and h.final_saved_status='Y' ")
List<Object> getSupplierAndVouchernumber(String branchcode, CompanyMst companyMst, String vDate);


@Query(nativeQuery = true,value="select * from other_branch_purchase_hdr h where "
		+ " h.branch_code=:branchCode and "
		+ "h.company_mst=:companyMst and "
		+ "date(h.voucher_date) between :fromDate and :toDate  ")

List<OtherBranchPurchaseHdr> getOtherBranchPurchaseSummary(java.util.Date fromDate, java.util.Date toDate, String branchCode,
		CompanyMst companyMst);
	

}
