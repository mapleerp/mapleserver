package com.maple.restserver.repository;

import java.util.Date;
import java.util.List;

import org.hibernate.type.TrueFalseType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ConsumptionHdr;
import com.maple.restserver.report.entity.ConsumptionReport;
@Repository
public interface ConsumptionHdrRepository extends JpaRepository<ConsumptionHdr,String>{

	@Query(nativeQuery = true,value = "select sum(cd.amount) as totalAmount from "
			+ "consumption_dtl cd,consumption_hdr ch "
			+ "where cd.consumption_hdr = ch.id and ch.id=:hdrid and ch.company_mst = :companyMstId"
			)
	Double getTotalAmount(String hdrid,String companyMstId);
	
	
	

	@Query(nativeQuery = true,value = "select sum(cd.qty) as totalQty from "
			+ "consumption_dtl cd,consumption_hdr ch "
			+ "where cd.consumption_hdr = ch.id and ch.id=:hdrid and "
			+ "ch.company_mst = :companyMstId and cd.item_id =:itemId and cd.batch=:batch"
			)
	Double getQty(String hdrid,String companyMstId,String itemId,String batch);


	
	@Query(nativeQuery = true,value="select "
			+ "i.item_name ,"
			+ "d.qty,"
			+ "d.mrp,"
			+ "crs.reason,"
			+ "u.unit_name ,d.amount "
			+ "from consumption_hdr h ,consumption_dtl d, "
			+ "item_mst i,unit_mst u, consumption_reason_mst crs "
			+ "where h.company_mst=:companymstid and "
			+ " date(h.voucher_date) between :fromdate and :todate"
			+ " and h.branch_code =:branchcode and i.id=d.item_id "
			+ "and u.id=d.unit_id and d.reason_id=crs.id")
List<Object>getConsumptionReport( String   branchcode, Date   fromdate,Date  todate,String  companymstid);


	@Query(nativeQuery = true,value="select h.voucher_number,h.voucher_date,cr.reason from consumption_hdr h ,consumption_reason_mst cr,consumption_dtl d where h.id=d.consumption_hdr and h.company_mst=:companymstid and"
			+ "  date(h.voucher_date) between :fromdate and :todate and h.branch_code=:branchcode and cr.id=d.reason_id  and cr.id=:reasonid ")
	List<Object>getConsumptionHdrReport(String branchcode, Date fromdate, Date todate,
			String companymstid, String reasonid);
	
	
	@Query(nativeQuery = true,value = "select i.item_name,d.batch,d.qty,d.mrp,d.amount,unit_name from consumption_hdr h ,consumption_dtl d ,unit_mst u ,item_mst i where h.id=d.consumption_hdr and u.id=d.unit_id and date(h.voucher_date) between :fromdate and :todate and i.id=d.item_id  and d.reason_id=:reasonid")
	List<Object>getConsumptionDtlReport( String reasonid,Date fromdate, Date todate);
	
	
	@Query(nativeQuery = true,value="select sum(d.amount) from consumption_hdr h ,consumption_reason_mst cr,consumption_dtl d where h.id=d.consumption_hdr and h.company_mst=:companymstid and " + 
			"  date(h.voucher_date) between :fromdate and :todate and h.branch_code=:branchcode  ")
	Double getConsumptionAmount(String branchcode, Date fromdate, Date todate,
			String companymstid );

	
	@Query(nativeQuery = true,value=" select distinct(d.reason_id) from  consumption_hdr h ,consumption_dtl d where h.id=d.consumption_hdr and  date(h.voucher_date) between :fromdate and :todate and h.branch_code=:branchcode and h.company_mst=:companymstid ")
	List <String > getConsumptionReason(String branchcode, Date fromdate, Date todate,
			String companymstid);




	@Query(nativeQuery = true,value = "select cm.reason,"
			+ "i.item_name,cd.qty,cd.mrp,cd.amount,"
			+ "cd.batch,ch.voucher_date,u.unit_name,c.category_name from unit_mst u,"
			+ "consumption_reason_mst cm,consumption_hdr ch,"
			+ "consumption_dtl cd,item_mst i,category_mst c "
			+ "where cm.id=cd.reason_id and c.id = i.category_id "
			+ " and cd.item_id=i.id and "
			+ "cd.consumption_hdr=ch.id and u.id=cd.unit_id and  "
			+ "cm.reason=:department and i.category_id=:id and "
			+ "ch.branch_code=:branchcode and "
			+ "date(voucher_date) >= :strfromdate "
			+ "and date(voucher_date) <= :strtodate and ch.company_mst = :companyMst")
	List<Object> getDepertmentwiseConsupmtion(String department, String id, String branchcode, Date strfromdate,
			Date strtodate, CompanyMst companyMst);




	@Query(nativeQuery = true,value = "select * from consumption_hdr "
			+ "where date(voucher_date) >= :tdate and date(voucher_date)<=:fdate")
	List<ConsumptionHdr> getConsumptionHdrBetweenDate(Date tdate, Date fdate);






	@Query(nativeQuery = true,value = "select cm.reason,"
			+ "i.item_name,cd.qty,cd.mrp,cd.amount,"
			+ "cd.batch,ch.voucher_date,u.unit_name,c.category_name from unit_mst u,"
			+ "consumption_reason_mst cm,consumption_hdr ch,"
			+ "consumption_dtl cd,item_mst i,category_mst c "
			+ "where cm.id=cd.reason_id and c.id = i.category_id "
			+ " and cd.item_id=i.id and "
			+ "cd.consumption_hdr=ch.id and u.id=cd.unit_id and  "
			+ "cm.reason=:department and i.id=:id and "
			+ "ch.branch_code=:branchcode and "
			+ "date(voucher_date) >= :fromdate "
			+ "and date(voucher_date) <= :todate and ch.company_mst = :companyMst")
	List<Object> getItemWiseConsumption(String department, String id, String branchcode, Date fromdate, Date todate,
			CompanyMst companyMst);


	
	@Query(nativeQuery = true,value = "select ch.voucher_date, ch.voucher_number, ch.total_amount, um.user_name,dm.department_name,sth.user_id "
			+ " from consumption_hdr ch, user_mst um, department_mst dm,sales_trans_hdr sth where ch.voucher_number= sth.voucher_number and sth.user_id=dm.user_id"
			+ " and date(ch.voucher_date)>= :fromdate and  date(ch.voucher_date) <= :todate and  ch.branch_code =:branchcode")
	
	List<Object>  getConsumptionreportsummary(String branchcode, Date fromdate, Date todate);


//============consumption detail report with category===================
	@Query(nativeQuery = true,value = "select dm.department_name,ch.voucher_number,ch.voucher_date, "
			+ "itm.item_name,cat.category_name,cd.batch,ibex.expiry_date,cd.qty,itm.standard_price,cd.qty*itm.standard_price "
			+ " from consumption_hdr ch,consumption_dtl cd, department_mst dm,category_mst cat,item_mst itm,item_batch_expiry_dtl ibex "
			+ "where ch.id = cd.consumption_hdr and cd.batch = ibex.batch and cd.item_id = ibex.item_id and ch.department_id = dm.id "
			+ "and cd.item_id = itm.id and itm.category_id = cat.id and itm.category_id=:id "
			+ " and date(ch.voucher_date)>= :fromdate and  date(ch.voucher_date) <= :todate ")
	List<Object> getConsumptionDetail(Date fromdate, Date todate, String id);
	
	
	//============consumption detail report without category===================
	@Query(nativeQuery = true,value = "select dm.department_name,ch.voucher_number,ch.voucher_date, "
			+ "itm.item_name,cat.category_name,cd.batch,ibex.expiry_date,cd.qty,itm.standard_price,cd.qty*itm.standard_price "
			+ " from consumption_hdr ch,consumption_dtl cd, department_mst dm,category_mst cat,item_mst itm,item_batch_expiry_dtl ibex "
			+ "where ch.id = cd.consumption_hdr and cd.batch = ibex.batch and cd.item_id = ibex.item_id and ch.department_id = dm.id "
			+ "and cd.item_id = itm.id and itm.category_id = cat.id "
			+ " and date(ch.voucher_date)>= :fromdate and  date(ch.voucher_date) <= :todate ")
	List<Object> getConsumptionDetailWithoutCategory(Date fromdate, Date todate);


	//============consumption summary report with category===================
	@Query(nativeQuery = true,value = "select ch.voucher_date, ch.voucher_number, cd.qty*itm.standard_price,dm.department_name "
			+ " from consumption_hdr ch, department_mst dm,item_mst itm,consumption_dtl cd "
			+ "where ch.id = cd.consumption_hdr and  itm.category_id=:id and ch.department_id = dm.id and cd.item_id = itm.id"
			+ " and date(ch.voucher_date)>= :fromdate and  date(ch.voucher_date) <= :todate ")
	List<Object> getConsumptionSummary(Date fromdate, Date todate, String id);

	//============consumption summary report without category===================
	@Query(nativeQuery = true,value = "select ch.voucher_date, ch.voucher_number, cd.qty*itm.standard_price,dm.department_name "
			+ " from consumption_hdr ch, department_mst dm,item_mst itm,consumption_dtl cd "
			+ "where ch.id = cd.consumption_hdr and ch.department_id = dm.id and cd.item_id = itm.id"
			+ " and date(ch.voucher_date)>= :fromdate and  date(ch.voucher_date) <= :todate ")
	List<Object> getConsumptionSummaryWithoutCategory(Date fromdate, Date todate);
}
