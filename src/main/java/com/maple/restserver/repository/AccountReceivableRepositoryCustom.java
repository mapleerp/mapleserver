package com.maple.restserver.repository;


import java.util.Date;
import java.util.List;

import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.ItemBatchMst;

public interface AccountReceivableRepositoryCustom {
	// List<AccountReceivable> findByCompanyMstAndCustomerMst(String companyId, String CustomerId );
 
	// List<ItemBatchMst> findItemNameByBarCodeAndQty(String barCode,String itemName, Double qty);
	
	 List<AccountReceivable> findByCompanyMstAndAccountHeads(String companyId, String CustomerId );
 
}
