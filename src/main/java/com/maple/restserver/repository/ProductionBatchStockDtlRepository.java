package com.maple.restserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProductionBatchStockDtl;

@Repository
public interface ProductionBatchStockDtlRepository extends JpaRepository<ProductionBatchStockDtl, String>{

	@Query(nativeQuery = true,value="select d.item_id from kit_definition_mst m,kit_defenition_dtl d where m.id=d.kit_defenitionmst and m.company_mst=:companymstid and m.item_id=:itemid")
	List<String>findRowMaterialsByItemId(String companymstid,String itemid);
	
	
	
	@Query(nativeQuery = true,value="select m.item_id,m.qty,m.batch,m.exp_date from item_batch_mst m  where  m.company_mst=:companymstid and m.item_id=:itemId")
	List<Object>rowMaterialStockAvailable(String companymstid,String itemId);

@Query(nativeQuery = true,value="select distinct m.batch from item_batch_mst m where m.item_id=:itemId")
List<String>getbatchList(String itemId);

List<ProductionBatchStockDtl> findByActualProductionDtl(ActualProductionDtl prodctDtlId);



	@Query(nativeQuery = true,value="select count(distinct p.item_id) from production_batch_stock_dtl p where p.actual_production_dtl =:actid and p.company_mst=:companymstid")
	Integer getProductionBatchRowMaterialCount(CompanyMst companymstid,ActualProductionDtl actid) ;

	ProductionBatchStockDtl findByItemIdAndActualProductionDtl(String id,ActualProductionDtl actualproductiondtl);

	
	@Query(nativeQuery=true,value="select sum(allocated_qty) as qty from production_batch_stock_dtl d where d.item_id=:itemid and d.actual_production_dtl =:actualproductiondtlid and d.company_mst=:companymstid ")
Double getCountOfProductionBatchRowMaterial(String itemid,String companymstid,String actualproductiondtlid);
}
