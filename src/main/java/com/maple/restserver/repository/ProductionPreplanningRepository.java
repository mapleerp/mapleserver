package com.maple.restserver.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.ProductionPreplanningMst;

@Repository
public interface ProductionPreplanningRepository extends JpaRepository<ProductionPreplanningMst, String> {
    @Modifying
	@Query(nativeQuery = true ,value="delete from production_preplanning_mst pp where  date(pp.voucher_date) >=:fromDate and  date(pp.voucher_date) <=:toDate ")
	void deleteProductionPrePlanning(Date fromDate,Date toDate);
}
