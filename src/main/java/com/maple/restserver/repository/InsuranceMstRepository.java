
package com.maple.restserver.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.his.entity.InsuranceMst;
@Component
@Repository
public interface InsuranceMstRepository extends JpaRepository<InsuranceMst,String>{
	
	List<InsuranceMst>findByCompanyMstId(String companymstid);
//	List<InsuranceMst> findByVoucherNumberAndVoucherDateAndVoucherType(String voucherNumber, Date voucherDate,String voucherType);
}
