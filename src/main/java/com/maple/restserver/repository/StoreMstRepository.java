package com.maple.restserver.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.StoreMst;
@Repository
public interface StoreMstRepository extends JpaRepository<StoreMst, String>{

	List<StoreMst>findByCompanyMst(CompanyMst companyMst);

	Optional<StoreMst> findByShortCodeAndCompanyMst(String store, CompanyMst companyMst);

	StoreMst findByMobile(String string);

	List<StoreMst> findByCompanyMstAndName(CompanyMst companyMst, String name);
	
	
}
