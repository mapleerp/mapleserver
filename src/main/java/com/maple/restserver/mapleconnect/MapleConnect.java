package com.maple.restserver.mapleconnect;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
 

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.io.IOException;

import java.nio.file.Files;

import java.nio.file.Path;
import java.sql.SQLException;

import static java.nio.file.StandardCopyOption.*;

import static java.nio.file.LinkOption.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

@Component
public class MapleConnect {
	private final Logger logger = LoggerFactory.getLogger(MapleConnect.class);

	
	@Autowired
	MCFileMover fileMover;
	
	@Value("${mybranch}")
	private String mybranch;
	@Value("${mycompany}")
	private String mycompany;
	
	
	
	@Autowired
	ObjectMapper objectMapper;

	@Value("${outgoing_folder}")
	private String outgoing_folder;

	@Value("${outgoing_processed_folder}")
	private String outgoing_processed_folder;

	
	@Value("${outgoing_processed_archived_folder}")
	private String outgoing_processed_archived_folder;

	
	
	@Value("${incoming_folder}")
	private String incoming_folder;

	@Value("${incoming_processed_folder}")
	private String incoming_processed_folder;

	@Value("${incoming_processed_archived_folder}")
	private String incoming_processed_archived_folder;

	
	@Autowired
	MCSendToKafkaTopic sendToKafkaTopic;
	
	
	private static FileWriter file;
	private String file_sufix = ".dat";
	private String name_differentiator = "$";


	 

	
	@KafkaListener(topics = { "common-events" })
	public void OnKafkaMessageReceive(@Payload String data,
		                        @Header(KafkaHeaders.OFFSET) Long offset,
		    
		                        @Header(KafkaHeaders.TIMESTAMP_TYPE) String timestampType,
		                        @Header(KafkaHeaders.RECEIVED_TOPIC) String topic,
		                        @Header(KafkaHeaders.RECEIVED_PARTITION_ID) Integer partitionId,
		                        @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String messageKey,
		                        @Header(KafkaHeaders.RECEIVED_TIMESTAMP) Long timestamp,
		                        @Headers MessageHeaders messageHeaders) {

			logger.info("- - - - - - - - - - - - - - -");
			logger.info("received message='{}'", data);
			
			logger.info("topic: {}", topic);
			logger.info("message key: {}", messageKey);
			logger.info("partition id: {}", partitionId);
			logger.info("offset: {}", offset);
			logger.info("timestamp type: {}", timestampType);
			logger.info("timestamp: {}", timestamp);
			logger.info("custom header: {}", messageHeaders);
			boolean processMe = true;
			Iterator itr = messageHeaders.keySet().iterator();
			while(itr.hasNext()) {
				
				 String key = (String) itr.next();
				 
				 if(key.equalsIgnoreCase("event-destination")) {
					 byte[]  value =  (byte[]) messageHeaders.get(key);
						//HashMap value = (HashMap)itr.next();
					 String destination = new String(value);    
					 logger.info("destination: {}", destination);
						if (destination.equalsIgnoreCase(mybranch) || destination.equalsIgnoreCase("ALL")) {
						}else {
							processMe = false;
							logger.info("Return from Message rcv " );
						 
						}
				 }
				 
				 if(key.equalsIgnoreCase("event-source")) {
					 byte[]  value =  (byte[]) messageHeaders.get(key);
						 
					 String source = new String(value);    
						if (source.equalsIgnoreCase(mybranch) ) {
							 
							logger.info("Return from Message rcv since source branch is same {} " , mybranch);
							return;
						}
				 }
				    
			
				 
			}
			
			if(!processMe) {
				return;
			}
			
		 
			
		String payLoad = "";
		MCKafkaMapleEventType objectType = null;
		String sourceFile = "";
	
		ObjectMapper mapper = new ObjectMapper()
				.registerModule(new ParameterNamesModule()).registerModule(new Jdk8Module())
				.registerModule(new JavaTimeModule());
		MCKafkaMapleEvent kafkaMapleEvent = null;
		try {
			kafkaMapleEvent = mapper.readValue(data, MCKafkaMapleEvent.class);

			payLoad = kafkaMapleEvent.getPayLoad();
			objectType = kafkaMapleEvent.getLibraryEventType();
			sourceFile = kafkaMapleEvent.getSourceFile();

		} catch (com.fasterxml.jackson.core.JsonParseException e1) {

			e1.printStackTrace();
		} catch (JsonMappingException e1) {

			e1.printStackTrace();
		} catch (IOException e1) {

			e1.printStackTrace();
		}


		switch (objectType) {
		case REPLYMSG:

			 // Move source file from outgoing folder to outgoing processed,
			
			//File from = new File(outgoing_processed_folder + File.separatorChar + sourceFile);

			//File to = new File(outgoing_processed_archived_folder + File.separatorChar + sourceFile);
			fileMover.moveFromOutProcessToOutProcessArchive(sourceFile);
		break;
 
		default:
			
			saveIncomingEventToFile(kafkaMapleEvent);
			break;
		}
		 
		 
		 
		
		logger.info("ConsumerRecord : {} ", kafkaMapleEvent);

	}
	
	

	public void readAndProcessOutGoingFile() {

		 
//		File theDir = new File(outgoing_processed_folder);
//		if (!theDir.exists()){
//		    theDir.mkdirs();
//		}
//		
//		
//		File folder = new File(outgoing_folder);
//
//		for (File file : folder.listFiles()) {
//			if (!file.isDirectory()) {
//
//				if (processOutGoingFileToKafka(file.getAbsolutePath())) {
//
//					File to = new File(outgoing_processed_folder + File.separatorChar + file.getName());
//
//					boolean fileCopied = false;
//					 
//						fileCopied = copyFileToProcessed(file, to);
//					 
//					if (fileCopied) {
//						 
//						
//						file.delete();
//						
//						 
//					}
//
//				}
//
//			}
//		}
//
//		return;
		
		File theDir = new File(outgoing_folder);
		if (!theDir.exists()){
		    theDir.mkdirs();
		}
		
		File theDir2 = new File(outgoing_processed_folder);
		if (!theDir2.exists()){
		    theDir2.mkdirs();
		}
		
		

		File theDir3 = new File(outgoing_processed_archived_folder);
		if (!theDir3.exists()){
		    theDir3.mkdirs();
		}
		
		File folder = new File(outgoing_folder);
		String destinationFolder = "";

		
		if(folder.length()==0) {
			return;
		}
		
		
		for (File file : folder.listFiles()) {
			if (!file.isDirectory()) {

				if (processOutGoingFile(file.getAbsolutePath())) {

					if(file.getAbsolutePath().contains(MCKafkaMapleEventType.REPLYMSG.toString())) {
						destinationFolder = 	outgoing_processed_archived_folder;
					}else {
						destinationFolder = outgoing_processed_folder;
					}
					
					File to = new File(destinationFolder + File.separatorChar + file.getName());

					boolean fileCopied = false;
					 
						fileCopied = copyFileToProcessed(file, to);
					 
					if (fileCopied) {
						 
						
						file.delete();
						
						 
					}

				}

			}
		}

		return;
	}

	

	public void readAndProcessOutGoingProcessedFile() {

		
//		File folder = new File(outgoing_processed_folder);
//
//		for (File file : folder.listFiles()) {
//			if (!file.isDirectory()) {
//
//				processOutGoingFileToKafka(file.getAbsolutePath()); 
//
//			}
//		}
//
//		return;
		
		File theDir = new File(outgoing_processed_folder);
		if(!theDir.exists()) {
			theDir.mkdirs();
		}
		
		File folder = new File(outgoing_processed_folder);
if(folder.length()==0) {
	return;
}
		for (File file : folder.listFiles()) {
			if (!file.isDirectory()) {

			 processOutGoingFile(file.getAbsolutePath()); 

			}
		}

		return;
	}

	
	private boolean copyFileToProcessed(File src, File dest) {
		try {

			Path bytes = Files.copy(

			new java.io.File(src.getAbsolutePath()).toPath(),

			new java.io.File(dest.getAbsolutePath()).toPath(),

			REPLACE_EXISTING,

			COPY_ATTRIBUTES,

			NOFOLLOW_LINKS);

			System.out.println("File successfully copied using Java 7 way");



			} catch (IOException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();
			return false;

			}
		return true;

		
	}
	 
 
	private boolean processOutGoingFileToKafka(String fileName) {

		JSONParser parser = new JSONParser();
		try {
			FileReader fr = new FileReader(fileName);
			Object obj = parser.parse(fr);
			
			fr.close();
			
			JSONObject jsonObject = (JSONObject) obj;

			
			
			
			String libraryEventType = (String) jsonObject.get("libraryEventType");
			String payLoad = (String) jsonObject.get("payLoad");
			String desination  =  (String) jsonObject.get("destination");
			String voucherNumber  =  (String) jsonObject.get("voucherNumber");
			String source  =  (String) jsonObject.get("source");
			String objectType  =  (String) jsonObject.get("objectType");
			Long libraryEventId  =  (Long) jsonObject.get("libraryEventId");
			Long hashCodeInFile  = (Long) jsonObject.get("hashCode");
			
			int hashCodeComputed = payLoad.hashCode();
			if(hashCodeInFile != hashCodeComputed) {
				logger.error("Data file tampered. Not processing");
				return false;
			}
			
			MCKafkaMapleEvent kafkaMapleEvent = new MCKafkaMapleEvent();
			kafkaMapleEvent.setDestination(desination);
			kafkaMapleEvent.setLibraryEventId(libraryEventId.intValue());
			
			MCKafkaMapleEventType libraryEventTypeValue = MCKafkaMapleEventType.valueOf(libraryEventType);
			
			kafkaMapleEvent.setLibraryEventType(libraryEventTypeValue);
			kafkaMapleEvent.setObjectType(desination);
			kafkaMapleEvent.setPayLoad(payLoad);
			kafkaMapleEvent.setSource(source);
			kafkaMapleEvent.setVoucherNumber(voucherNumber);
			kafkaMapleEvent.setSourceFile(fileName);
			
			
			sendToKafkaTopic.sendKafkaMessageLibraryEvent(kafkaMapleEvent, kafkaMapleEvent.getSource(),
					kafkaMapleEvent.getDestination(), objectMapper.writeValueAsString(kafkaMapleEvent));
			
		
			
			 
			

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	public void saveIncomingEventToFile(MCKafkaMapleEvent kafkaMapleEvent) {

		File theDir = new File(incoming_folder);
		if (!theDir.exists()){
		    theDir.mkdirs();
		}
		
 
		try {
 
			file = new FileWriter(incoming_folder+File.separatorChar + kafkaMapleEvent.getDestination() + name_differentiator
					+ kafkaMapleEvent.getVoucherNumber() + file_sufix);
			String outString = objectMapper.writeValueAsString(kafkaMapleEvent);
			file.write(outString);

		} catch (IOException e) {
			e.printStackTrace();

		} finally {

			try {
				file.flush();
				file.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	
	
	public void sendReplyForProcessedIncoming() {

		
		 
			File theDir = new File(incoming_processed_archived_folder);
			if (!theDir.exists()){
			    theDir.mkdirs();
			}
			
			
		File folder = new File(incoming_processed_folder);

		for (File file : folder.listFiles()) {
			if (!file.isDirectory()) {

				if (sendReplyForProcessedFile(file.getAbsolutePath())) {

					File to = new File(incoming_processed_archived_folder + File.separatorChar + file.getName());
					
					boolean fileCopied = false;
						fileCopied = copyFileToProcessed(file, to);
					if (fileCopied) {
						file.delete();
					}

				}

			}
		}

		return;
	}
	
	public boolean sendReplyForProcessedFile(String fileName) {
		JSONParser parser = new JSONParser();
		try {
			FileReader fr = new FileReader(fileName);
			Object obj = parser.parse(fr);
			fr.close();	
			JSONObject jsonObject = (JSONObject) obj;

			String payLoad = (String) jsonObject.get("payLoad");

				MCKafkaMapleEventType libraryEventType = MCKafkaMapleEventType.REPLYMSG;
				String desination  =  (String) jsonObject.get("source");
				String voucherNumber  =  (String) jsonObject.get("voucherNumber");
				String source  =  (String) jsonObject.get("destination");
				String objectType  =  (String) jsonObject.get("objectType");
				Integer libraryEventId  =  (Integer) jsonObject.get("libraryEventId");
				
				MCKafkaMapleEvent kafkaMapleEvent = new MCKafkaMapleEvent();
				kafkaMapleEvent.setDestination(desination);
				kafkaMapleEvent.setLibraryEventId(libraryEventId);
				kafkaMapleEvent.setLibraryEventType(libraryEventType);
				kafkaMapleEvent.setObjectType(desination);
				kafkaMapleEvent.setPayLoad(payLoad);
				kafkaMapleEvent.setSource(desination);
				kafkaMapleEvent.setVoucherNumber(voucherNumber);
				kafkaMapleEvent.setSourceFile(fileName);
				
				sendToKafkaTopic.sendKafkaMessageLibraryEvent(kafkaMapleEvent, kafkaMapleEvent.getSource(),
						kafkaMapleEvent.getDestination(), objectMapper.writeValueAsString(kafkaMapleEvent));
				
		 		} catch (Exception e) {
				e.printStackTrace();
				return false;
			}	
		return true;
	}

	private boolean processOutGoingFile(String fileName) {

		JSONParser parser = new JSONParser();
		try {
			FileReader fr = new FileReader(fileName);
			Object obj = parser.parse(fr);
			
			fr.close();
			
			JSONObject jsonObject = (JSONObject) obj;

			
			
			
			String libraryEventType = (String) jsonObject.get("libraryEventType");
			String payLoad = (String) jsonObject.get("payLoad");
			String desination  =  (String) jsonObject.get("destination");
			String voucherNumber  =  (String) jsonObject.get("voucherNumber");
			String source  =  (String) jsonObject.get("source");
			String objectType  =  (String) jsonObject.get("objectType");
			Long libraryEventId  =  (Long) jsonObject.get("libraryEventId");
			Long hashCodeInFile  = (Long) jsonObject.get("hashCode");
			String targetFile = (String) jsonObject.get("sourceFile");
			int hashCodeComputed = payLoad.hashCode();
			if(hashCodeInFile != hashCodeComputed) {
				logger.error("Data file tampered. Not processing");
				return false;
			}
			
			MCKafkaMapleEvent kafkaMapleEvent = new MCKafkaMapleEvent();
			kafkaMapleEvent.setDestination(desination);
			kafkaMapleEvent.setLibraryEventId(libraryEventId.intValue());
			
			MCKafkaMapleEventType libraryEventTypeValue = MCKafkaMapleEventType.valueOf(libraryEventType);
			
			kafkaMapleEvent.setLibraryEventType(libraryEventTypeValue);
			kafkaMapleEvent.setObjectType(desination);
			kafkaMapleEvent.setPayLoad(payLoad);
			kafkaMapleEvent.setSource(source);
			kafkaMapleEvent.setVoucherNumber(voucherNumber);
			kafkaMapleEvent.setSourceFile(targetFile);
			
			
			sendToKafkaTopic.sendKafkaMessageLibraryEvent(kafkaMapleEvent, kafkaMapleEvent.getSource(),
					kafkaMapleEvent.getDestination(), objectMapper.writeValueAsString(kafkaMapleEvent));
			
		
			
			 
			

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	 
}
