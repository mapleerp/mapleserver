package com.maple.restserver.mapleconnect;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import org.springframework.stereotype.Component;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Component
public class MCKafkaMapleEvent {

	int hashCode;
	String sourceFile ;

	String destination ;
	String source ="";
	
	String voucherNumber ;
    private Integer libraryEventId;
    private MCKafkaMapleEventType libraryEventType;
    
    private String  payLoad;
    
    private String objectType ;
    
    
	public int getHashCode() {
		return hashCode;
	}
	public void setHashCode(int hashCode) {
		this.hashCode = hashCode;
	}
	public String getSourceFile() {
		return sourceFile;
	}
	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}

	
	
	public Integer getLibraryEventId() {
		return libraryEventId;
	}
	public void setLibraryEventId(Integer libraryEventId) {
		this.libraryEventId = libraryEventId;
	}
	public MCKafkaMapleEventType getLibraryEventType() {
		return libraryEventType;
	}
	public void setLibraryEventType(MCKafkaMapleEventType libraryEventType) {
		this.libraryEventType = libraryEventType;
	}
	public String getPayLoad() {
		return payLoad;
	}
	public void setPayLoad(String payLoad) {
		this.payLoad = payLoad;
	}
	 
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	@Override
	public String toString() {
		return "KafkaMapleEvent [sourceFile=" + sourceFile + ", destination=" + destination + ", source=" + source
				+ ", voucherNumber=" + voucherNumber + ", libraryEventId=" + libraryEventId + ", libraryEventType="
				+ libraryEventType + ", payLoad=" + payLoad + ", objectType=" + objectType + "]";
	}
	 
	 
 
}
