package com.maple.restserver.mapleconnect;

import java.util.ArrayList;
import java.util.List;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
 

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
 

@Component
@Slf4j
public class MCSendToKafkaTopic {
	private static final Logger log = LoggerFactory.getLogger(MCSendToKafkaTopic.class);
    @Autowired
    KafkaTemplate<Integer,String> kafkaTemplate;
    
    String topic = "common-events";
    
    @Autowired
    ObjectMapper objectMapper;
    
    
     
   
    
    String source ="";
  	
    public boolean saveotOutFile(MCKafkaMapleEvent kafkaEvent, String source, String destination, String value) {
    	
    	
    	return true;
    	
    }
	 public ListenableFuture<SendResult<Integer,String>> sendKafkaMessageLibraryEvent
	 (MCKafkaMapleEvent kafkaEvent, String source, String destination, String value) throws JsonMappingException, JsonProcessingException {
		 	this.source =source;

	        Integer key = kafkaEvent.getLibraryEventId();
	       
		

	        ProducerRecord<Integer,String> producerRecord = buildProducerRecord( value, topic,  source,destination);

	       // FailedMessageMst failedMessageMst =  saveBeforeTry(  key,   value,      destination);
	        
	        ListenableFuture<SendResult<Integer,String>> listenableFuture = kafkaTemplate.send(producerRecord);

	        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {
	            @Override
	            public void onFailure(Throwable ex) {
	                handleFailure(key, value,  destination);
	            }

	            @Override
	            public void onSuccess(SendResult<Integer, String> result) {
	                //handleSuccess(failedMessageMst);
	            }
	        });

	       
	        return listenableFuture;
	    }

	 
	 private void handleFailure(Integer key, String value,   String  destination) {
	        log.error("Save to failure message" );
	       

	    }
	 
	 
 
	
	 private ProducerRecord<Integer, String> buildProducerRecord( String value, String topic, String source,String destination ) {
		 Integer key = 1;

		 List<Header> recordHeaders = new ArrayList();
		 
	         recordHeaders.add(new RecordHeader("event-source", source.getBytes()));
	         recordHeaders.add(new RecordHeader("event-destination", destination.getBytes()));

	        return new ProducerRecord<>(topic, null, key, value, recordHeaders);
	    }

}
