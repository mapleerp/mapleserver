package com.maple.restserver.mapleconnect;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
 
 
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;


 

/*
 * Comment  @Configuration to disable embeded broker.
 * 
 */
@Configuration
public class MCScheduleConfig {

	private static final Logger logger = LoggerFactory.getLogger(MCScheduleConfig.class);

 

	@Value("${mycompany}")
	private String mycompany;
	@Value("${mybranch}")
	private String mybranch;

 
  	
	@Autowired
	MapleConnect mapleConnect;
 
	//@Scheduled(fixedDelay = 6000)
	public void scheduleFixedDelayTask() {
		
		//mFileReader.readAndProcessIncomingFile();
	//resendToKafkaTopic.resendFailedMessage();
	 
	 mapleConnect.readAndProcessOutGoingFile();
		
		 
		
	}

 
	 
 //	@Scheduled(fixedDelay =180000) //every 3 minutes
	public void scheduleFixedDelayTask2() {

		mapleConnect.readAndProcessOutGoingProcessedFile();
	}

 
	
 

//	@Scheduled(fixedDelay = 6000)
	public void PerformanceTestAndGc() {
		System.out.println("Memory Usage Checking");

		long kb = 1024000;

		Runtime runtime = Runtime.getRuntime();

		long totalMemory = runtime.totalMemory() / kb;

		long freeMemory = runtime.freeMemory() / kb;

		long memoryUsage = totalMemory - freeMemory;
		System.out.println("Total Memory=" + totalMemory);
		System.out.println("Free Memory=" + freeMemory);
		System.out.println("Memory Usage=" + memoryUsage);

		double usedSpace = Double.longBitsToDouble(memoryUsage);
		double totalSpace = Double.longBitsToDouble(totalMemory);

		double consumption = usedSpace / totalSpace * 100;

		System.out.println("Memory Usage in Percentage=" + consumption + "%");

 

	}
	 
 

}
