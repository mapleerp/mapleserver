package com.maple.restserver.mapleconnect;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

 
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;


import java.io.IOException;

import java.nio.file.Files;

import java.nio.file.Path;



import static java.nio.file.StandardCopyOption.*;

import static java.nio.file.LinkOption.*;


@Component
public class MCFileMover {
	@Autowired
	ObjectMapper objectMapper;
	

	@Value("${outgoing_folder}")
	private String outgoing_folder;

	@Value("${outgoing_processed_folder}")
	private String outgoing_processed_folder;

	
	@Value("${outgoing_processed_archived_folder}")
	private String outgoing_processed_archived_folder;
	
	@Value("${incoming_folder}")
	private String incoming_folder;

	@Value("${incoming_processed_folder}")
	private String incoming_processed_folder;

	@Value("${incoming_processed_archived_folder}")
	private String incoming_processed_archived_folder;

	private static FileWriter file;
	private String file_sufix = ".dat";
	private String name_differentiator = "$";

	

	private boolean copyFileToProcessed(File src, File dest) {
		try {

			Path bytes = Files.copy(

			new java.io.File(src.getAbsolutePath()).toPath(),

			new java.io.File(dest.getAbsolutePath()).toPath(),

			REPLACE_EXISTING,

			COPY_ATTRIBUTES,

			NOFOLLOW_LINKS);
 
			} catch (IOException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();
			return false;

			}
		return true;

		
	}
	 

	 
public void moveFromOutProcessToOutProcessArchive(String fileName) {
	
	

	File theDir = new File(outgoing_processed_archived_folder);
	if(!theDir.exists()) {
		theDir.mkdirs();
	}
 
	
	File from = new File(outgoing_processed_folder + File.separatorChar +fileName);
	File to = new File(  outgoing_processed_archived_folder + File.separatorChar +fileName);
	copyFileToProcessed(from,to);
	from.delete();
}

public void moveFromInProcessToInProcessArchive(String fileName) {
	

	File theDir = new File(incoming_processed_archived_folder);
	if(!theDir.exists()) {
		theDir.mkdirs();
	}
 
	
	
	File from = new File(incoming_processed_folder + File.separatorChar +fileName);
	File to = new File(incoming_processed_archived_folder + File.separatorChar +fileName);
	copyFileToProcessed(from,to);
	from.delete();
}

}
