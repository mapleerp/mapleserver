
package com.maple.restserver.resourcejavapos.print;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.maple.javapos.print.POSThermalPrintABS;
import com.maple.javapos.print.PosTaxLayoutPrint;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.KotCategoryMst;
import com.maple.restserver.entity.PrinterMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.repository.PrinterMstRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.resource.SalesDetailsResource;
import com.maple.restserver.resource.StockTransferInHdrResource;
import com.maple.restserver.service.CategoryMstServiceImpl;

@Component
public class ImperialKitchenPrint2 extends POSThermalPrintABS {
	private static final Logger logger = LoggerFactory.getLogger(ImperialKitchenPrint2.class);
	@Autowired
	CategoryMstServiceImpl categoryMstServiceImpl;

	@Autowired
	PrinterMstRepository printerMstRepo;
	@Autowired
	SalesDetailsRepository salesdtlRepo;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@Autowired
	SalesDetailsResource salesDetailsResource;
	public static String TableTitleToPrint = "";
	public static String customerName = "";
	public static String space[] = new String[] { " " };
	public final static Logger log = LoggerFactory.getLogger(PosTaxLayoutPrint.class);
	static JTable itemsTable;
	public static int total_item_count = 0;

	public static final String DATE_FORMAT_NOW = "dd/MM/yyyy hh:mm  a";
	public static String title[] = new String[] { "Srl", "Item Name", "Qty", "Price" ,"KOTID"};
	static String parentID;
	public static PrintService KitcherPrinterService = null;

	static BufferedImage read;

	public ImperialKitchenPrint2() {

	}

	public void setupLogo() throws IOException {
	
		if(null==read) {
			FileSystem fs =  FileSystems.getDefault();
			Path path = fs.getPath(ClientSystemSetting.getLogo_name());
			
		
		//Path path = FileSystems.getDefault().getPath(ClientSystemSetting.getLogo_name());

		// List<String> lines = Files.readAllLines(path, charset);

		InputStream iStream = Files.newInputStream(path);

		read = ImageIO.read(iStream);
		
		
		iStream.close();
		fs.close();
		}
		
		
	}

	public static void setItems(Object[][] printitem) {
		Object data[][] = printitem;
		DefaultTableModel model = new DefaultTableModel();
		// assume jtable has 4 columns.
		model.addColumn(title[0]);
		model.addColumn(title[1]);
		model.addColumn(title[2]);
		model.addColumn(title[3]);
		// model.addColumn(title[4]);

		int rowcount = printitem.length;

		addtomodel(model, data, rowcount);

		itemsTable = new JTable(model);
		itemsTable.setRowSorter(null);
	}

	public static void addtomodel(DefaultTableModel model, Object[][] data, int rowcount) {
		int count = 0;
		while (count < rowcount) {
			model.addRow(data[count]);
			count++;
		}
		if (model.getRowCount() != rowcount)
			addtomodel(model, data, rowcount);

		System.out.println("Check Passed.");
	}

	public Object[][] getTableData(JTable table) {
		int itemcount = table.getRowCount();
		System.out.println("Item Count:" + itemcount);

		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
		Object[][] tableData = new Object[nRow][nCol];
		if (itemcount == nRow) // check is there any data loss.
		{
			for (int i = 0; i < nRow; i++) {
				for (int j = 0; j < nCol; j++) {
					tableData[i][j] = dtm.getValueAt(i, j); // pass data into object array.
				}
			}
			if (tableData.length != itemcount) { // check for data losses in object array
				getTableData(table); // recursively call method back to collect data
			}
			System.out.println("Data check passed");
		} else {
			// collecting data again because of data loss.
			getTableData(table);
		}
		return tableData; // return object array with data.
	}

	public static PageFormat getPageFormat(PrinterJob pj) {
		PageFormat pf = pj.defaultPage();
		Paper paper = pf.getPaper();

		double middleHeight = total_item_count * 1.0; // dynamic----->change with the row count of jtable
		double headerHeight = 10; // fixed----->but can be mod
		double footerHeight = 500.0; // fixed----->but can be mod

		double width = convert_CM_To_PPI(7); // printer know only point per inch.default value is 72ppi
		double height = convert_CM_To_PPI(headerHeight + middleHeight + footerHeight);
		paper.setSize(width, height);
		paper.setImageableArea(convert_CM_To_PPI(0.001), convert_CM_To_PPI(0.05), width - convert_CM_To_PPI(0.35),
				height - convert_CM_To_PPI(1)); // define boarder size after that print area width is about 180 points

		pf.setOrientation(PageFormat.PORTRAIT); // select orientation portrait or landscape but for this time portrait
		pf.setPaper(paper);

		return pf;
	}

	protected static double convert_CM_To_PPI(double cm) {
		return toPPI(cm * 0.393600787);
	}

	protected static double toPPI(double inch) {
		return inch * 72d;
	}

	public static String now() {
		// get current date and time as a String output
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());

	}

	public class MyPrintable implements Printable {
		public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
			int result = NO_SUCH_PAGE;
			if (pageIndex == 0) {

				Graphics2D g2d = (Graphics2D) graphics;

				double width = pageFormat.getImageableWidth();
				double height = pageFormat.getImageableHeight();
				g2d.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
				Font font = new Font("MONOSPACED", Font.BOLD, 12);
				Font font2 = new Font("Monospaced", Font.PLAIN, 7);
				g2d.setFont(font);

				// try {
				/*
				 * Draw Image* assume that printing reciept has logo on top that logo image is
				 * in .gif format .png also support image resolution is width 100px and height
				 * 50px image located in root--->image folder
				 */
				int x = 1; // print start at 100 on x axies
				int y = 1; // print start at 10 on y axies
				int imagewidth = 150;
				int imageheight = 50;

				// String logoName = App.cSqlFunctions11.GetParamValue("LOGO_NAME_HEADER");
				// if(logoName.length()>0){
				// BufferedImage read = ImageIO.read(getClass().getResource(logoName));
				if (null != read) {
					g2d.drawImage(read, x, y, imagewidth, imageheight, null); // draw image
				}

				// }

				g2d.setFont(font2);

				// g2d.drawLine(10, y , 180, y ); // draw line
				// } catch (IOException e) {
				// logger.error(e.getMessage());
				// }
				try {
					/* Draw Header */
					y = 50;

					String SalesManId = "";
					// String Voucher_number = "";
					// Timestamp Voucher_time = null;
					Date invoice_date = null;

					/*
					 * String sqlstr1 = "select   t.invoice_date  from   sales_trans_hdr t" +
					 * " where     t.record_id   =  '" + parentID + "'";
					 */

					font = new Font(Font.SERIF, Font.BOLD, 12);
					g2d.setFont(font);
					// g2d.drawString(CompanyNameTitle, 30, y);

					String InvoiceTimeToPrint = "";

					// Date date = new Date(Voucher_time.getTime());
					String s1 = "";
					if (null != invoice_date) {
						s1 = ClientSystemSetting.SqlDateTostring(invoice_date);
					} else {
						invoice_date = (Date) ClientSystemSetting.getSystemDate();
					}

					// String s2 = CJavaFunctions.SqlDateTostring(Voucher_time);

					// InvoiceTimeToPrint = " TABLE :" +TableName;
					// g2d.drawString(InvoiceTimeToPrint , 10, y + 20);
					// y +=10;

					InvoiceTimeToPrint = "Date  " + now();
					font = new Font(Font.SERIF, Font.BOLD, 14);
					g2d.setFont(font);
					g2d.drawString(TableTitleToPrint, 10, y);
					font = new Font(Font.SERIF, Font.BOLD, 12);
					g2d.setFont(font);
					if (!customerName.equalsIgnoreCase("") || !customerName.equalsIgnoreCase(null)) {
						g2d.drawString(customerName, 10, y + 10);
					}
					g2d.drawString(InvoiceTimeToPrint, 10, y + 20);

					font = new Font("Arial", Font.BOLD, 8);
					g2d.setFont(font);

					/* Draw Colums */
					g2d.drawLine(1, y + 30, 180, y + 30);
					g2d.drawString(title[0], 1, y + 40);
					g2d.drawString(title[1], 30, y + 40);
					g2d.drawString(title[2], 130, y + 40);
					g2d.drawString(title[3], 160, y + 40);
					// g2d.drawString(title[4], 150, y + 40);
					g2d.drawLine(1, y + 50, 180, y + 50);

					int cH = 0;
					TableModel mod = itemsTable.getModel();
					int lastLine = 0;

					double totalamount = 0.0;
					int intP = 0;
					BigDecimal bdPrice = new BigDecimal("0");

					for (int i = 0; i < mod.getRowCount(); i++) {
						String itemNameLine1 = "", itemNameLine2 = "";
						/*
						 * Assume that all parameters are in string data type for this situation All
						 * other premetive data types are accepted.
						 */
						String itemid = mod.getValueAt(i, 0).toString();
						String itemname = mod.getValueAt(i, 1).toString();
						if (itemname.length() > 15) {
							itemNameLine1 = itemname.substring(0, 15);
							itemNameLine2 = itemname.substring(15);
						} else {
							itemNameLine1 = itemname;
						}
						String price = mod.getValueAt(i, 2).toString();
						try {

							bdPrice = new BigDecimal(price);

							bdPrice = bdPrice.setScale(2, BigDecimal.ROUND_HALF_EVEN);

							price = bdPrice.toPlainString();
						} catch (Exception e) {

						}

						String quantity = mod.getValueAt(i, 3).toString();
						// String amount = mod.getValueAt(i, 4).toString();

						cH = (y + 60) + (10 * i); // shifting drawing line

						font = new Font("Arial", Font.BOLD, 10); // changed font size
						g2d.setFont(font);

						g2d.drawString(itemid, 0, cH);
						g2d.drawString(itemNameLine1, 10, cH);

						g2d.drawString(quantity, 130, cH);
						g2d.drawString(price, 160, cH);
						// String amountFmt = formatDecimal(Float.parseFloat(amount));
						// g2d.drawString(padLeft(amount,10), 150, cH);
						if (itemNameLine2.length() > 0) {
							y = y + 10;
							cH = (y + 60) + (10 * i);
							g2d.drawString(itemNameLine2, 10, cH);
						}
						lastLine = i;

						BigDecimal qty = (BigDecimal) mod.getValueAt(i, 3);

						totalamount = totalamount + (bdPrice.doubleValue() * qty.doubleValue());

					}

					cH = (y + 60) + (10 * (lastLine + 2));
					g2d.drawString("", 150, cH);

					g2d.drawLine(1, cH, 180, cH);
					cH = cH + 20;

					BigDecimal bd = new BigDecimal(totalamount);
					bd = bd.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					g2d.drawString(bd.toPlainString(), 150, cH);

				} catch (Exception r) {
					r.printStackTrace();
				}

				result = PAGE_EXISTS;
			}
			return result;
		}
	}

	public static String padLeft(String s, int n) {
		return String.format("%1$" + n + "s", s);
	}

	public static String formatDecimal(float number) {
		float epsilon = 0.004f; // 4 tenths of a cent
		if (Math.abs(Math.round(number) - number) < epsilon) {
			return String.format("%10.0f", number); // sdb
		} else {
			return String.format("%10.2f", number); // dj_segfault
		}
	}

	public double getItemTotalTax(double vatrate, String ParentId) {

		double InvTotal = 0.0;

		try {

			/*
			 * String sqlstr = "select sum( rate * qty * tax_rate/100)  " +
			 * "  from xpos_sales_dtl where parent_id  = ?  "; PreparedStatement pst =
			 * APlicationWindow.LocalConn.prepareStatement(sqlstr);
			 * 
			 * pst.setString(1, ParentId); // pst.setDouble(2, vatrate); ResultSet rs =
			 * pst.executeQuery();
			 * 
			 * while (rs.next()) {
			 */
			BigDecimal ojjList = salesdtlRepo.retrieveAllTaxAmount(ParentId);
			// RestCaller.getAllTaxAmount(ParentId);

			InvTotal = ojjList.doubleValue();
		} catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

	public double getItemTotalTaxSummary(String ParentId) {

		double InvTotal = 0.0;

		try {

			/*
			 * String sqlstr = "select sum( rate * qty * tax_rate/100)  " +
			 * "  from xpos_sales_dtl where parent_id  = ?  "; PreparedStatement pst =
			 * APlicationWindow.LocalConn.prepareStatement(sqlstr);
			 * 
			 * pst.setString(1, ParentId); // pst.setDouble(2, vatrate); ResultSet rs =
			 * pst.executeQuery();
			 * 
			 * while (rs.next()) {
			 */
			BigDecimal ojjList = salesdtlRepo.retrieveAllTaxAmount(ParentId);
			// RestCaller.getAllTaxAmount(ParentId);

			InvTotal = ojjList.doubleValue();
//			ResponseEntity<Double> ojjList =  RestCaller.getAllTaxAmount(ParentId);
//
//				InvTotal = ojjList.getBody();
		} catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

	public double getGrandTotalIncTax(String ParentId) {

		double InvTotal = 0.0;

		try {
//
//			String sqlstr = "select sum(( rate * qty * tax_rate/100) + (rate * qty))   "
//					+ "  from xpos_sales_dtl where parent_id  = ?  ";

			BigDecimal ojjList = salesdtlRepo.retrieveAllTaxAmount(ParentId);
			// RestCaller.getAllTaxAmount(ParentId);

			InvTotal = ojjList.doubleValue();
			// pst.setString(1, ParentId);
			// pst.setDouble(2, vatrate);
			// ResultSet rs = pst.executeQuery();

			// while (rs.next()) {
			// InvTotal = ojjList.getBody() ;
		} catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

	public static synchronized void PrintInvoiceThermalPrinter(String ParentID, String tableName) throws SQLException {

	}

	public static double round(double value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}
	
	
	
	@Override
	public void PrintPerformaInvoiceThermalPrinter(String ParentID) throws SQLException {

		parentID = ParentID;
		DefaultTableModel model = null;

		boolean isIGST = false;
		String CustStateCode = " 32 (KL) ";

		double insurance_comapny = 0.0;
		double cash_paid_amt = 0.0;

		BigDecimal TotalTax = new BigDecimal("0");

		double CreditAmount = 0.0;
		double card_paid_amt = 0.0;

		double bank_paid_amt = 0.0;
		double OtherChartges = 0.0;
		String remark1 = "";
		String insurance_cardno = "";
		String remark2 = "";

		String TelNo = "";

		String deleivery_address = "";
		String delivery_vehicle = "";

		Date InvoiceDate = null;
		String InvoiceNo = "";
		String strInvoiceDate = "";
		String deliveryAddrerss = "";
		// String logoPath = "";
		int X1 = 0;
		int Y1 = 0;
		float x2 = 0f;
		float y2 = 0f;

		List<PrinterMst> printerList = printerMstRepo.findAll();

		for (int printi = 0; printi < printerList.size(); printi++) {
			
			String printerName  =printerList.get(printi).getId();
			

			List<Object> ojjList = salesdtlRepo.getItemDetailsForPrintPerforma(parentID,
					printerList.get(printi).getId());

			System.out.println("Prinere Name from Table " + printerList.get(printi).getPrinterName());

			int i = 0;
			BigDecimal TotalAmount = new BigDecimal("0");
			BigDecimal TaxRate = new BigDecimal("0");

			BigDecimal Qty = new BigDecimal("0");

			BigDecimal Rate = new BigDecimal("0");
			BigDecimal Amount = new BigDecimal("0");
			BigDecimal AmountIncludingTax = new BigDecimal("0");
			BigDecimal rateIncludingTax = new BigDecimal("0");
			BigDecimal TaxAmount = new BigDecimal("0");

			String itemName = "";

			int jj = 1;
			// model.removeRow(0);
			for (int k = 0; k < ojjList.size(); k++) {
				Object[] objAray = (Object[]) ojjList.get(k);

				String hsn_code = ((String) objAray[0]);
				String itemCode = ((String) objAray[1]);
				if (null == hsn_code) {
					hsn_code = "";
				}

				try {
					TaxRate = new BigDecimal((Double) objAray[2]);
					TaxRate = TaxRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					Qty = new BigDecimal((Double) objAray[3]);
					Qty = Qty.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					Rate = new BigDecimal((Double) objAray[4]);
					Rate = Rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					itemName = ((String) objAray[5]);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					Amount = new BigDecimal((Double) objAray[6]);
				} catch (Exception e) {
					System.out.println(e);
				}
				// String item_code = rs.getString("item_code");

				try {
					AmountIncludingTax = (Qty.multiply(Rate))
							.add(Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100")))));
					AmountIncludingTax = AmountIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					rateIncludingTax = Rate.add(Rate.multiply(TaxRate.divide(new BigDecimal("100"))));
					rateIncludingTax = rateIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					TaxAmount = (Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100")))));
					TaxAmount = TaxAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					TotalTax = TotalTax.add(TaxAmount);

					Amount = Amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					TotalAmount = TotalAmount.add(Amount);
				} catch (Exception e) {
					System.out.println(e);
				}

				int Mop = 0;
				String hexMOP = "";

				int j = itemName.length();
				String itemNameLeft = "";
				int lineLength = 20;
				int sublineCount = 0;
				int ii = 0;
				String strToPrint = "";

				try {
					BigDecimal sgst_rate = TaxRate.divide(new BigDecimal("2"));

					BigDecimal cgst_rate = TaxRate.divide(new BigDecimal("2"));

					BigDecimal bdSgst_rate = sgst_rate;
					BigDecimal bdCgst_rate = cgst_rate;

					BigDecimal bdIgst_rate = cgst_rate.add(sgst_rate);

					bdSgst_rate = bdSgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					bdCgst_rate = bdCgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					bdIgst_rate = bdIgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				if (i == 0) {

					Object[][] insertRowData = { { jj, itemName, rateIncludingTax, Qty, AmountIncludingTax } };
					model = new DefaultTableModel(insertRowData, title);
					i = i + 1;

				} else {

					Object[] insertRowData2 = { jj, itemName, rateIncludingTax, Qty, AmountIncludingTax };
					model.insertRow(i, insertRowData2);
					i = i + 1;

				}

				jj = jj + 1;
			}
			// model.removeRow(1);
			try {
				BigDecimal dbsgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);
				BigDecimal dbcgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);

				BigDecimal bdSgstAmount = dbsgstAmount;
				BigDecimal bdCgstAmount = dbcgstAmount;
				bdSgstAmount = bdSgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				bdCgstAmount = bdCgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			} catch (Exception e) {
				System.out.println(e);
			}
			JTable table = new JTable(model);

			Object printitem[][] = getTableData(table);
			ImperialKitchenPrint2.setItems(printitem);

			SalesTransHdr salesTransHdr = salesTransHdrRepository.findById(ParentID).get();

			String serveringTable = "";
			String salesManId = "";

			if (null == salesTransHdr.getServingTableName()) {
				serveringTable = "ONLINE";
			} else {
				serveringTable = salesTransHdr.getServingTableName();
			}
			if (null != salesTransHdr.getTakeOrderNumber()) {
				serveringTable = "TAKE AWAY-" + salesTransHdr.getTakeOrderNumber();
			}

			if (null == salesTransHdr.getSalesManId()) {
				salesManId = salesTransHdr.getSalesMode();
				if (null != salesTransHdr.getLocalCustomerMst()) {
					salesManId = salesTransHdr.getLocalCustomerMst().getLocalcustomerName();
					if (salesManId.length() > 10)
						salesManId = salesManId.substring(salesManId.length() - 10);
				}
			} else {
				salesManId = salesTransHdr.getSalesManId();
			}

			if (null != salesManId) {
				if (salesManId.length() > 4) {
					ImperialKitchenPrint2.TableTitleToPrint = serveringTable;
					ImperialKitchenPrint2.customerName = salesManId;
				} else {
					ImperialKitchenPrint2.TableTitleToPrint = serveringTable + "/" + salesManId;
				}
			} else {
				ImperialKitchenPrint2.TableTitleToPrint = serveringTable;
			}

			PrintService pservice = PrintServiceLookup.lookupDefaultPrintService();

			PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
			System.out.println("Number of print services: " + printServices.length);

			for (PrintService printer : printServices) {
				System.out.println("Printer: " + printer.getName());
				if (printerList.get(printi).getPrinterName().equalsIgnoreCase(printer.getName())) {
					// printLabel(printer,barcodeLabel, barcodeQty);
					KitcherPrinterService = printer;
					System.out.println("Kitchen Printer Name found  = " + printer.getName());
					log.info("Kitchen Printer Name found  = " + printer.getName());
					break;
				} else {
					System.out.println("Not a kitchen printer  = " + printer.getName());
					log.info("Not a kitchen printer  =  " + printer.getName());
				}
			}

			PrinterJob pj = PrinterJob.getPrinterJob();

			// REGY COMMENTED ON JAN 6 2021 - To test with out printer

			 try {

				pj.setPrintService(KitcherPrinterService);
			} catch (PrinterException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			}
 
			PageFormat pf = pj.defaultPage();
			Paper paper = new Paper();
			double margin = 0.1;
			paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
			pf.setPaper(paper);

			pj.setPrintable(new MyPrintable(), getPageFormat(pj));

			try {
				pj.print();

			} catch (PrinterException ex) {
				ex.printStackTrace();
			}

		}
		salesdtlRepo.updatePrintStatus(parentID);
	}
	
	

	@Override
	public void PrintInvoiceThermalPrinter(String ParentID) throws SQLException {

		parentID = ParentID;
		DefaultTableModel model = null;

		boolean isIGST = false;
		String CustStateCode = " 32 (KL) ";

		double insurance_comapny = 0.0;
		double cash_paid_amt = 0.0;

		BigDecimal TotalTax = new BigDecimal("0");

		double CreditAmount = 0.0;
		double card_paid_amt = 0.0;

		double bank_paid_amt = 0.0;
		double OtherChartges = 0.0;
		String remark1 = "";
		String insurance_cardno = "";
		String remark2 = "";

		String TelNo = "";

		String deleivery_address = "";
		String delivery_vehicle = "";

		Date InvoiceDate = null;
		String InvoiceNo = "";
		String strInvoiceDate = "";
		String deliveryAddrerss = "";
		// String logoPath = "";
		int X1 = 0;
		int Y1 = 0;
		float x2 = 0f;
		float y2 = 0f;

		List<PrinterMst> printerList = printerMstRepo.findAll();

		for (int printi = 0; printi < printerList.size(); printi++) {
			
			String printerName  =printerList.get(printi).getId();
			

			List<Object> ojjList = salesdtlRepo.getItemDetailsForPrintWithStatus(parentID);
			

			System.out.println("Prinere Name from Table " + printerList.get(printi).getPrinterName());

			int i = 0;
			BigDecimal TotalAmount = new BigDecimal("0");
			BigDecimal TaxRate = new BigDecimal("0");

			BigDecimal Qty = new BigDecimal("0");

			BigDecimal Rate = new BigDecimal("0");
			BigDecimal Amount = new BigDecimal("0");
			BigDecimal AmountIncludingTax = new BigDecimal("0");
			BigDecimal rateIncludingTax = new BigDecimal("0");
			BigDecimal TaxAmount = new BigDecimal("0");

			String itemName = "";

			int jj = 1;
			// model.removeRow(0);
			for (int k = 0; k < ojjList.size(); k++) {
				Object[] objAray = (Object[]) ojjList.get(k);

				String hsn_code = ((String) objAray[0]);
				String itemCode = ((String) objAray[1]);
				if (null == hsn_code) {
					hsn_code = "";
				}

				try {
					TaxRate = new BigDecimal((Double) objAray[2]);
					TaxRate = TaxRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					Qty = new BigDecimal((Double) objAray[3]);
					Qty = Qty.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					Rate = new BigDecimal((Double) objAray[4]);
					Rate = Rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					itemName = ((String) objAray[5]);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					Amount = new BigDecimal((Double) objAray[6]);
				} catch (Exception e) {
					System.out.println(e);
				}
				// String item_code = rs.getString("item_code");

				try {
					AmountIncludingTax = (Qty.multiply(Rate))
							.add(Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100")))));
					AmountIncludingTax = AmountIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					rateIncludingTax = Rate.add(Rate.multiply(TaxRate.divide(new BigDecimal("100"))));
					rateIncludingTax = rateIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					TaxAmount = (Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100")))));
					TaxAmount = TaxAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					TotalTax = TotalTax.add(TaxAmount);

					Amount = Amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					TotalAmount = TotalAmount.add(Amount);
				} catch (Exception e) {
					System.out.println(e);
				}

				int Mop = 0;
				String hexMOP = "";

				int j = itemName.length();
				String itemNameLeft = "";
				int lineLength = 20;
				int sublineCount = 0;
				int ii = 0;
				String strToPrint = "";

				try {
					BigDecimal sgst_rate = TaxRate.divide(new BigDecimal("2"));

					BigDecimal cgst_rate = TaxRate.divide(new BigDecimal("2"));

					BigDecimal bdSgst_rate = sgst_rate;
					BigDecimal bdCgst_rate = cgst_rate;

					BigDecimal bdIgst_rate = cgst_rate.add(sgst_rate);

					bdSgst_rate = bdSgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					bdCgst_rate = bdCgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					bdIgst_rate = bdIgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				if (i == 0) {

					Object[][] insertRowData = { { jj, itemName, rateIncludingTax, Qty, AmountIncludingTax } };
					model = new DefaultTableModel(insertRowData, title);
					i = i + 1;

				} else {

					Object[] insertRowData2 = { jj, itemName, rateIncludingTax, Qty, AmountIncludingTax };
					model.insertRow(i, insertRowData2);
					i = i + 1;

				}

				jj = jj + 1;
			}
			// model.removeRow(1);
			try {
				BigDecimal dbsgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);
				BigDecimal dbcgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);

				BigDecimal bdSgstAmount = dbsgstAmount;
				BigDecimal bdCgstAmount = dbcgstAmount;
				bdSgstAmount = bdSgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				bdCgstAmount = bdCgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			} catch (Exception e) {
				System.out.println(e);
			}
			JTable table = new JTable(model);

			Object printitem[][] = getTableData(table);
			ImperialKitchenPrint2.setItems(printitem);

			SalesTransHdr salesTransHdr = salesTransHdrRepository.findById(ParentID).get();

			String serveringTable = "";
			String salesManId = "";

			if (null == salesTransHdr.getServingTableName()) {
				serveringTable = "ONLINE";
			} else {
				serveringTable = salesTransHdr.getServingTableName();
			}
			if (null != salesTransHdr.getTakeOrderNumber()) {
				serveringTable = "TAKE AWAY-" + salesTransHdr.getTakeOrderNumber();
			}

			if (null == salesTransHdr.getSalesManId()) {
				salesManId = salesTransHdr.getSalesMode();
				if (null != salesTransHdr.getLocalCustomerMst()) {
					salesManId = salesTransHdr.getLocalCustomerMst().getLocalcustomerName();
					if (salesManId.length() > 10)
						salesManId = salesManId.substring(salesManId.length() - 10);
				}
			} else {
				salesManId = salesTransHdr.getSalesManId();
			}

			if (null != salesManId) {
				if (salesManId.length() > 4) {
					ImperialKitchenPrint2.TableTitleToPrint = serveringTable;
					ImperialKitchenPrint2.customerName = salesManId;
				} else {
					ImperialKitchenPrint2.TableTitleToPrint = serveringTable + "/" + salesManId;
				}
			} else {
				ImperialKitchenPrint2.TableTitleToPrint = serveringTable;
			}

			PrintService pservice = PrintServiceLookup.lookupDefaultPrintService();

			PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
			System.out.println("Number of print services: " + printServices.length);

			for (PrintService printer : printServices) {
				System.out.println("Printer: " + printer.getName());
				if (printerList.get(printi).getPrinterName().equalsIgnoreCase(printer.getName())) {
					// printLabel(printer,barcodeLabel, barcodeQty);
					KitcherPrinterService = printer;
					System.out.println("Kitchen Printer Name found  = " + printer.getName());
					log.info("Kitchen Printer Name found  = " + printer.getName());
					break;
				} else {
					System.out.println("Not a kitchen printer  = " + printer.getName());
					log.info("Not a kitchen printer  =  " + printer.getName());
				}
			}

			PrinterJob pj = PrinterJob.getPrinterJob();

			// REGY COMMENTED ON JAN 6 2021 - To test with out printer

			 try {

				pj.setPrintService(KitcherPrinterService);
			} catch (PrinterException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			}
 
			PageFormat pf = pj.defaultPage();
			Paper paper = new Paper();
			double margin = 0.1;
			paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
			pf.setPaper(paper);

			pj.setPrintable(new MyPrintable(), getPageFormat(pj));

			try {
				pj.print();

			} catch (PrinterException ex) {
				ex.printStackTrace();
			}

		}
		salesdtlRepo.updatePrintStatus(parentID);
	}
	
	/*
	 * written by Regy on Jan 21 , 2021
	 */

	public void PrintCategorySplitKot(String ParentIDL) {
		/*
		 * Approach:
		 * First get list of categories, Then loop each category and print items in the category.
		 * Printer for the category is selected based on parameter  category passed to the function 
		 */

		
		/*
		 * Get list of categories
		 */
		List<String> categoryList = salesdtlRepo.getItemsGroupByCategory(ParentIDL);
		Iterator iter = categoryList.iterator();
		while (iter.hasNext()) {
			String category = (String) iter.next();

			/*
			 * Find the printer for the selected cagtegory
			 */
			KotCategoryMst kotCategoryMst = categoryMstServiceImpl.getPrinterByCategory(category);

			String printerIdforCategory = kotCategoryMst.getPrinterId();

			/*
			 * Now calling print function with category and printername and parameter.
			 * 
			 */
			doKotPrintByCategory(ParentIDL, category, printerIdforCategory);
			
		}

	}
/*
 * New function written by Regy on Jan 21 , 2021
 */
	private void doKotPrintByCategory(String parentId, String categoryId, String printerId) {
		
		
		/*
		 * Get printer name from Printer Id.
		 */
		 Optional<PrinterMst> printerMst= printerMstRepo.findById(printerId);
		 
		 if(!printerMst.isPresent()) {
			 return;
		 }
		 
		String printerName =printerMst.get().getPrinterName();
		
		
		/*
		 * Find list of item in KOT for the specific category.
		 */
		List<Object> ojjList = salesdtlRepo.getItemListForSplitKot(parentId, categoryId);
		
		List<String> salesDtlIds = new ArrayList<String>();

		DefaultTableModel model = null;
		
		
		BigDecimal TotalTax = new BigDecimal("0");
		
		BigDecimal TotalAmount = new BigDecimal("0");
		BigDecimal TaxRate = new BigDecimal("0");

		BigDecimal Qty = new BigDecimal("0");

		BigDecimal Rate = new BigDecimal("0");
		BigDecimal Amount = new BigDecimal("0");
		BigDecimal AmountIncludingTax = new BigDecimal("0");
		BigDecimal rateIncludingTax = new BigDecimal("0");
		BigDecimal TaxAmount = new BigDecimal("0");

		String itemName = "";
		
		int rowNumber = 0;
		

		 int columnnumber = 1;
		
/*
 * Add items in the category to Print table.
 */
		for (int k = 0; k < ojjList.size(); k++) {
			Object[] objAray = (Object[]) ojjList.get(k);
			
			salesDtlIds.add((String) objAray[8]);
			String kotid  = ((String) objAray[9]);
			
			
			String hsn_code = ((String) objAray[0]);
			String itemCode = ((String) objAray[1]);
			if (null == hsn_code) {
				hsn_code = "";
			}

			try {
				TaxRate = new BigDecimal((Double) objAray[2]);
				TaxRate = TaxRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			} catch (Exception e) {
				System.out.println(e);
			}
			try {
				Qty = new BigDecimal((Double) objAray[3]);
				Qty = Qty.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			} catch (Exception e) {
				System.out.println(e);
			}
			try {
				Rate = new BigDecimal((Double) objAray[4]);
				Rate = Rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			} catch (Exception e) {
				System.out.println(e);
			}
			try {
				itemName = ((String) objAray[5]);
			} catch (Exception e) {
				System.out.println(e);
			}
			try {
				Amount = new BigDecimal((Double) objAray[6]);
			} catch (Exception e) {
				System.out.println(e);
			}
			// String item_code = rs.getString("item_code");

			try {
				AmountIncludingTax = (Qty.multiply(Rate))
						.add(Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100")))));
				AmountIncludingTax = AmountIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			} catch (Exception e) {
				System.out.println(e);
			}
			try {
				rateIncludingTax = Rate.add(Rate.multiply(TaxRate.divide(new BigDecimal("100"))));
				rateIncludingTax = rateIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			} catch (Exception e) {
				System.out.println(e);
			}
			try {
				TaxAmount = (Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100")))));
				TaxAmount = TaxAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			} catch (Exception e) {
				System.out.println(e);
			}
			try {
				TotalTax = TotalTax.add(TaxAmount);

				Amount = Amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				TotalAmount = TotalAmount.add(Amount);
			} catch (Exception e) {
				System.out.println(e);
			}

			int Mop = 0;
			String hexMOP = "";

			int itemNameLenght = itemName.length();
			
			
			String itemNameLeft = "";
			int lineLength = 20;
			int sublineCount = 0;
			int ii = 0;
			String strToPrint = "";

			try {
				BigDecimal sgst_rate = TaxRate.divide(new BigDecimal("2"));

				BigDecimal cgst_rate = TaxRate.divide(new BigDecimal("2"));

				BigDecimal bdSgst_rate = sgst_rate;
				BigDecimal bdCgst_rate = cgst_rate;

				BigDecimal bdIgst_rate = cgst_rate.add(sgst_rate);

				bdSgst_rate = bdSgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				bdCgst_rate = bdCgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				bdIgst_rate = bdIgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			} catch (Exception e) {
				System.out.println(e);
			}
			if (rowNumber == 0) {

				Object[][] insertRowData = { { columnnumber, itemName, rateIncludingTax, Qty, AmountIncludingTax } };
				model = new DefaultTableModel(insertRowData, title);
				rowNumber = rowNumber + 1;

			} else {

				Object[] insertRowData2 = { columnnumber, itemName, rateIncludingTax, Qty, AmountIncludingTax };
				model.insertRow(rowNumber, insertRowData2);
				rowNumber = rowNumber + 1;

			}

			columnnumber = columnnumber + 1;
		}

		try {
			BigDecimal dbsgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);
			BigDecimal dbcgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);

			BigDecimal bdSgstAmount = dbsgstAmount;
			BigDecimal bdCgstAmount = dbcgstAmount;
			bdSgstAmount = bdSgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			bdCgstAmount = bdCgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		} catch (Exception e) {
			System.out.println(e);
		}
		JTable table = new JTable(model);

		Object printitem[][] = getTableData(table);
		ImperialKitchenPrint2.setItems(printitem);

		SalesTransHdr salesTransHdr = salesTransHdrRepository.findById(parentId).get();

		String serveringTable = "";
		String salesManId = "";

		if (null == salesTransHdr.getServingTableName()) {
			serveringTable = "ONLINE";
		} else {
			serveringTable = salesTransHdr.getServingTableName();
		}
		if (null != salesTransHdr.getTakeOrderNumber()) {
			serveringTable = "TAKE AWAY-" + salesTransHdr.getTakeOrderNumber();
		}

		if (null == salesTransHdr.getSalesManId()) {
			salesManId = salesTransHdr.getSalesMode();
			if (null != salesTransHdr.getLocalCustomerMst()) {
				salesManId = salesTransHdr.getLocalCustomerMst().getLocalcustomerName();
				if (salesManId.length() > 10)
					salesManId = salesManId.substring(salesManId.length() - 10);
			}
		} else {
			salesManId = salesTransHdr.getSalesManId();
		}

		if (null != salesManId) {
			if (salesManId.length() > 4) {
				ImperialKitchenPrint2.TableTitleToPrint = serveringTable;
				ImperialKitchenPrint2.customerName = salesManId;
				
			} else {
				ImperialKitchenPrint2.TableTitleToPrint = serveringTable + "/" + salesManId;
			}
		} else {
			ImperialKitchenPrint2.TableTitleToPrint = serveringTable;
		}

		PrintService pservice = PrintServiceLookup.lookupDefaultPrintService();

		PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
		
		
		System.out.println("Number of print services: " + printServices.length);
/*
 * Find the printer from available printers.
 */
		for (PrintService printer : printServices) {
			System.out.println("Printer: " + printer.getName());
			if (printerName.equalsIgnoreCase(printer.getName())) {
				 
				KitcherPrinterService = printer;
				System.out.println("Kitchen Printer Name found  = " + printer.getName());
				log.info("Kitchen Printer Name found  = " + printer.getName());
				
				
				break;
			} else {
				
				
				System.out.println("Not a kitchen printer  = " + printer.getName());
				log.info("Not a kitchen printer  =  " + printer.getName());
			}
		}

		PrinterJob pj = PrinterJob.getPrinterJob();
		try {
			pj.setPrintService(KitcherPrinterService);
		} catch (PrinterException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		PageFormat pf = pj.defaultPage();
		Paper paper = new Paper();
		double margin = 0.1;
		paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
		pf.setPaper(paper);

		pj.setPrintable(new MyPrintable(), getPageFormat(pj));

		try {
			pj.print();

		} catch (PrinterException ex) {
			ex.printStackTrace();
		}
		salesdtlRepo.updateFinalPrintStatus(parentId,salesDtlIds);
	

	}

	/*
	 * Following functiin to be deleted. Jan 6 , 2121
	 * 
	 */
	public void PrintInvoiceThermalPrinterCategorySplitForOnePrinter(String ParentIDL) {

		parentID = ParentIDL;
		DefaultTableModel model = null;

		boolean isIGST = false;
		String CustStateCode = " 32 (KL) ";

		double insurance_comapny = 0.0;
		double cash_paid_amt = 0.0;

		BigDecimal TotalTax = new BigDecimal("0");

		double CreditAmount = 0.0;
		double card_paid_amt = 0.0;

		double bank_paid_amt = 0.0;
		double OtherChartges = 0.0;
		String remark1 = "";
		String insurance_cardno = "";
		String remark2 = "";

		String TelNo = "";

		String deleivery_address = "";
		String delivery_vehicle = "";

		Date InvoiceDate = null;
		String InvoiceNo = "";
		String strInvoiceDate = "";
		String deliveryAddrerss = "";
		// String logoPath = "";
		int X1 = 0;
		int Y1 = 0;
		float x2 = 0f;
		float y2 = 0f;

		List<PrinterMst> printerList = printerMstRepo.findAll();

		List<String> catlist = salesdtlRepo.getItemsGroupByCategory(parentID);
		for (int printi = 0; printi < catlist.size(); printi++) {

			List<Object> ojjList = salesdtlRepo.getItemDetailsForSplitPrintWithStatus(parentID, catlist.get(printi));

			System.out.println("Prinere Name from Table " + printerList.get(printi).getPrinterName());

			int i = 0;
			BigDecimal TotalAmount = new BigDecimal("0");
			BigDecimal TaxRate = new BigDecimal("0");

			BigDecimal Qty = new BigDecimal("0");

			BigDecimal Rate = new BigDecimal("0");
			BigDecimal Amount = new BigDecimal("0");
			BigDecimal AmountIncludingTax = new BigDecimal("0");
			BigDecimal rateIncludingTax = new BigDecimal("0");
			BigDecimal TaxAmount = new BigDecimal("0");

			String itemName = "";

			int jj = 1;
			// model.removeRow(0);
			for (int k = 0; k < ojjList.size(); k++) {
				Object[] objAray = (Object[]) ojjList.get(k);

				
				String kotid=((String) objAray[9]);
				String hsn_code = ((String) objAray[0]);
				String itemCode = ((String) objAray[1]);
				if (null == hsn_code) {
					hsn_code = "";
				}

				try {
					TaxRate = new BigDecimal((Double) objAray[2]);
					TaxRate = TaxRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					Qty = new BigDecimal((Double) objAray[3]);
					Qty = Qty.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					Rate = new BigDecimal((Double) objAray[4]);
					Rate = Rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					itemName = ((String) objAray[5]);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					Amount = new BigDecimal((Double) objAray[6]);
				} catch (Exception e) {
					System.out.println(e);
				}
				// String item_code = rs.getString("item_code");

				try {
					AmountIncludingTax = (Qty.multiply(Rate))
							.add(Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100")))));
					AmountIncludingTax = AmountIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					rateIncludingTax = Rate.add(Rate.multiply(TaxRate.divide(new BigDecimal("100"))));
					rateIncludingTax = rateIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					TaxAmount = (Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100")))));
					TaxAmount = TaxAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				try {
					TotalTax = TotalTax.add(TaxAmount);

					Amount = Amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					TotalAmount = TotalAmount.add(Amount);
				} catch (Exception e) {
					System.out.println(e);
				}

				int Mop = 0;
				String hexMOP = "";

				int j = itemName.length();
				String itemNameLeft = "";
				int lineLength = 20;
				int sublineCount = 0;
				int ii = 0;
				String strToPrint = "";

				try {
					BigDecimal sgst_rate = TaxRate.divide(new BigDecimal("2"));

					BigDecimal cgst_rate = TaxRate.divide(new BigDecimal("2"));

					BigDecimal bdSgst_rate = sgst_rate;
					BigDecimal bdCgst_rate = cgst_rate;

					BigDecimal bdIgst_rate = cgst_rate.add(sgst_rate);

					bdSgst_rate = bdSgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					bdCgst_rate = bdCgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					bdIgst_rate = bdIgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				} catch (Exception e) {
					System.out.println(e);
				}
				if (i == 0) {

					Object[][] insertRowData = { { jj, itemName, rateIncludingTax, Qty, AmountIncludingTax ,kotid} };
					model = new DefaultTableModel(insertRowData, title);
					i = i + 1;

				} else {

					Object[] insertRowData2 = { jj, itemName, rateIncludingTax, Qty, AmountIncludingTax,kotid };
					model.insertRow(i, insertRowData2);
					i = i + 1;

				}

				jj = jj + 1;
			}
			// model.removeRow(1);
			try {
				BigDecimal dbsgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);
				BigDecimal dbcgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);

				BigDecimal bdSgstAmount = dbsgstAmount;
				BigDecimal bdCgstAmount = dbcgstAmount;
				bdSgstAmount = bdSgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				bdCgstAmount = bdCgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			} catch (Exception e) {
				System.out.println(e);
			}
			JTable table = new JTable(model);

			Object printitem[][] = getTableData(table);
			ImperialKitchenPrint2.setItems(printitem);

			SalesTransHdr salesTransHdr = salesTransHdrRepository.findById(ParentIDL).get();

			String serveringTable = "";
			String salesManId = "";

			if (null == salesTransHdr.getServingTableName()) {
				serveringTable = "ONLINE";
			} else {
				serveringTable = salesTransHdr.getServingTableName();
			}
			if (null != salesTransHdr.getTakeOrderNumber()) {
				serveringTable = "TAKE AWAY-" + salesTransHdr.getTakeOrderNumber();
			}

			if (null == salesTransHdr.getSalesManId()) {
				salesManId = salesTransHdr.getSalesMode();
				if (null != salesTransHdr.getLocalCustomerMst()) {
					salesManId = salesTransHdr.getLocalCustomerMst().getLocalcustomerName();
					if (salesManId.length() > 10)
						salesManId = salesManId.substring(salesManId.length() - 10);
				}
			} else {
				salesManId = salesTransHdr.getSalesManId();
			}

			if (null != salesManId) {
				if (salesManId.length() > 4) {
					ImperialKitchenPrint2.TableTitleToPrint = serveringTable;
					ImperialKitchenPrint2.customerName = salesManId;
				} else {
					ImperialKitchenPrint2.TableTitleToPrint = serveringTable + "/" + salesManId;
				}
			} else {
				ImperialKitchenPrint2.TableTitleToPrint = serveringTable;
			}

			PrintService pservice = PrintServiceLookup.lookupDefaultPrintService();

			PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
			System.out.println("Number of print services: " + printServices.length);

			for (PrintService printer : printServices) {
				System.out.println("Printer: " + printer.getName());
				if (printerList.get(0).getPrinterName().equalsIgnoreCase(printer.getName())) {
					// printLabel(printer,barcodeLabel, barcodeQty);
					KitcherPrinterService = printer;
					System.out.println("Kitchen Printer Name found  = " + printer.getName());
					log.info("Kitchen Printer Name found  = " + printer.getName());
					break;
				} else {
					System.out.println("Not a kitchen printer  = " + printer.getName());
					log.info("Not a kitchen printer  =  " + printer.getName());
				}
			}

			PrinterJob pj = PrinterJob.getPrinterJob();
			try {
				pj.setPrintService(KitcherPrinterService);
			} catch (PrinterException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			}

			PageFormat pf = pj.defaultPage();
			Paper paper = new Paper();
			double margin = 0.1;
			paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
			pf.setPaper(paper);

			pj.setPrintable(new MyPrintable(), getPageFormat(pj));

			try {
				pj.print();

			} catch (PrinterException ex) {
				ex.printStackTrace();
			}

		}
		salesdtlRepo.updateFinalPrintStatus(ParentIDL);
	}

}
