package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.OwnAccount;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.OwnAccountRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Transactional
public class OwnAccountResource {
	
private static final Logger logger = LoggerFactory.getLogger(OwnAccountResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	OwnAccountRepository ownAccountRepository;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	
	
	@PostMapping("/{companymstid}/ownaccount")
	public OwnAccount createOwnAccount(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody OwnAccount ownaccount)
	{
		return companyMstRepository.findById(companymstid).map(companyMst-> {
			ownaccount.setCompanyMst(companyMst);
//	return ownAccountRepository.saveAndFlush(ownaccount);
			return	saveAndPublishService.saveOwnAccount(ownaccount, mybranch);
			
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
	
	@GetMapping("{companymstid}/ownaccount/{accountid}/sumofpaidamountbysupplier")
	public Double sumOfPaymentInvoiceDtlPaidAmount(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "accountid") String accountid) {
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
	return ownAccountRepository.accountBalanceAmount(accountid,companyMst);

	}
	
	
	@GetMapping("{companymstid}/ownaccount/{accountid}/sumofpaidamountbycustomer")
	public Double sumOfPaymentInvoiceDtlPaidAmountByCustomer(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "accountid") String accountid) {
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
	return ownAccountRepository.accountBalanceAmountBycustomer(accountid,companyMst);

	}

	@GetMapping("{companymstid}/ownaccount/{receipthdrid}/sumofpaidbyreceipthdr")
	public Double suoOfPaidByReceipthdr(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "receipthdrid") String receipthdrid) {
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
	return ownAccountRepository.sumOfCreditByReceiptHdrId(receipthdrid,companyMst);

	}
	
	@GetMapping("{companymstid}/ownaccount/{paymentdrid}/sumofpaidbypaymenthdr")
	public Double suoOfPaidByPaymenthdr(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "paymentdrid") String paymentdrid) {
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
	return ownAccountRepository.sumOfDebitByPaymentHdrId(paymentdrid,companyMst);

	}
	
	@GetMapping("{companymstid}/ownaccount/{accountid}")
	public List<OwnAccount> getOwnAccountByAccountId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "accountid") String accountid) {
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
	return ownAccountRepository.findByCompanyMstIdAndAccountId(companyMst.getId(), accountid);

	}
	

	@PutMapping("{companymstid}/updateownaccountbyid/{ownaccountid}")
	public OwnAccount updateOwnAccountById(
			@PathVariable(value="ownaccountid") String ownaccountid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody OwnAccount  ownaccountRequest)
	{
				

		return ownAccountRepository.findById(ownaccountid).map(ownaccount -> {
					Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
					CompanyMst companyMst = comapnyMstOpt.get();
					ownaccount.setCompanyMst(companyMst);
					ownaccount.setRealizedAmount(ownaccountRequest.getRealizedAmount());
					//ownaccount =ownAccountRepository.saveAndFlush(ownaccount);
					ownaccount=saveAndPublishService.saveOwnAccount(ownaccount, mybranch);
		            return ownaccount;
		         }).orElseThrow(() -> new ResourceNotFoundException("ownaccountid " + ownaccountid + " not found"));
				
	}	
	
	@GetMapping("{companymstid}/ownaccount/fetchownaccountbyid/{id}")
	public OwnAccount retrieveOwnAccountById(
			@PathVariable(value = "companymstid") String  companymstid,
			@PathVariable(value = "id") String  id
			){

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
		
		return ownAccountRepository.findByCompanyMstAndId(companyMst,id) ;
		 
	}	
	
	@GetMapping("{companymstid}/ownaccount/{accountid}/customer")
	public List<OwnAccount> getOwnAccountByAccountIdcustomer(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "accountid") String accountid) {
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
	return ownAccountRepository.findByCompanyMstIdAndAccountIdcustomer(companyMst.getId(), accountid);

	}
	@GetMapping("{companymstid}/ownaccount/{receipthdrid}/ownaccounthdrid")
	public List<OwnAccount> getOwnAccountByReceiptHdrId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "receipthdrid") String receipthdrid) {
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
	return ownAccountRepository.findByCompanyMstIdAndReceiptHdrId(companyMst.getId(), receipthdrid);

	}
	@GetMapping("{companymstid}/ownaccount/{paymentid}/ownaccountpaymenthdrid")
	public List<OwnAccount> getOwnAccountByPaymentHdrId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "paymentid") String paymentid) {
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
	return ownAccountRepository.findByCompanyMstIdAndPaymenthdrId(companyMst.getId(), paymentid);

	}
	@DeleteMapping("{companymstid}/ownaccdelete/{ownaccid}")
	public void ownAccDelete(@PathVariable String ownaccid) {
		ownAccountRepository.deleteById(ownaccid);

	}
	@DeleteMapping("{companymstid}/ownaccdelete/{receipthdrid}/deleteownaccbyreceipthdr")
	public void ownAccDeleteByReceiptHdrId(@PathVariable String receipthdrid) {
		ownAccountRepository.deleteByReceiptHdrId(receipthdrid);

	}
}
