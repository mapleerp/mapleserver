package com.maple.restserver.resource;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.InstallationModelClassService;

@RestController
public class InstallationModelClassResource {
	
@Autowired
	InstallationModelClassService installationModelClassService;
	

@Autowired
CompanyMstRepository companyMstRepository;




	@GetMapping("{companymstid}/installationmodelclassresource/installationmodel")
	public  String InstallationModel(
			@PathVariable(value = "companymstid") String companymstid 
			)
	{
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		installationModelClassService.InstallationModelFileCreation(companyMst);
		return "FILE CREATED ";
	}
	
	
	
	
	@GetMapping("{companymstid}/installationmodelclassresource/fileinstallation")
	public  String InstallationFile(
			@PathVariable(value = "companymstid") String companymstid 
			)
	{
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		installationModelClassService.FileInstallation(companyMst);
		return "SUCCESS";
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
