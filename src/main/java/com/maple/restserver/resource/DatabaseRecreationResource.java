package com.maple.restserver.resource;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.LocalCustomerMst;
import com.maple.restserver.entity.ProcessMst;
import com.maple.restserver.entity.ProcessPermissionMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionDtlDtl;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SaleOrderReceipt;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesMessageEntity;
import com.maple.restserver.entity.SalesOrderDtl;
import com.maple.restserver.entity.SalesOrderMessageEntity;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.message.entity.AccountMessage;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.ProcessMstRepository;
import com.maple.restserver.repository.ProcessPermissionRepository;
import com.maple.restserver.repository.ProductionDetailRepository;
import com.maple.restserver.repository.ProductionDtlDtlRepository;
import com.maple.restserver.repository.ProductionMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SaleOrderReceiptRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesOrderDtlRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.repository.UserMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class DatabaseRecreationResource {

	@Autowired
	ItemMstRepository itemMstRepo;

	@Autowired
	CategoryMstRepository categoryMstRepository;

	@Autowired
	private AcceptStockRepository acceptStockRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;


	@Autowired
	BranchMstRepository branchMstRepository;

	@Autowired
	UserMstRepository userMstRepository;


	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	@Autowired
	KitDefenitionDtlRepository kitDefenitionDtlRepo;

	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepo;

	@Autowired
	UnitMstRepository unitMstRepo;

	@Autowired
	ProcessMstRepository processMstRepository;

	@Autowired
	ProcessPermissionRepository processPermissionRepository;

	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	AccountClassRepository accountClassRepository;

	@Autowired
	LedgerClassRepository ledgerClassRepository;

	@Autowired
	CreditClassRepository creditClassRepository;

	@Autowired
	DebitClassRepository debitClassRepository;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@Autowired
	SalesDetailsRepository salesDetailsRepository;

	@Autowired
	SalesReceiptsRepository salesReceiptsRepository;

	@Autowired
	PurchaseDtlRepository purchaseDtlRepository;

	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;

	@Autowired
	ProductionMstRepository productionMstRepository;

	@Autowired
	ProductionDetailRepository productionDetailRepository;

	@Autowired
	ProductionDtlDtlRepository productionDtlDtlRepository;

	@Autowired
	SalesOrderDtlRepository salesOrderDtlRepository;

	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepository;

	@Autowired
	SaleOrderReceiptRepository saleOrderReceiptRepository;

	@Autowired
	private VoucherNumberService voucherService;

	

	@GetMapping("{companymstid}/masterdatabaserecreation")
	public String DatabaseCreation(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "branchcode") String branchcode, @RequestParam(value = "sales") String sales,
			@RequestParam(value = "purchase") String purchase, @RequestParam(value = "production") String production,
			@RequestParam(value = "saleorder") String saleorder) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		

		sendBranch(branchcode, companyMst);

		sendUserMst(branchcode, companyMst);

		sendUnitMst(branchcode, companyMst);

		sendItemMst(branchcode, companyMst);

//		sendSupplier(branchcode, companyMst);

		sendCustomers(branchcode, companyMst);

		sendAccountHeads(branchcode, companyMst);

		sendKitDefenition(branchcode, companyMst);

		sendProcessMst(branchcode, companyMst);

		sendProcessPermission(branchcode, companyMst);

		sendItemBatchDtl(branchcode, companyMst);

		sendAccountingClass(branchcode, companyMst);

		if (sales.equalsIgnoreCase("SALES")) {

			System.out.println("Sales------");
			sendSales(branchcode, companyMst);
		} 
		if (purchase.equalsIgnoreCase("PURCHASE")) {
			sendPurchase(branchcode, companyMst);
		}

		if (production.equalsIgnoreCase("PRODUCTION")) {
			sendProduction(branchcode, companyMst);
		} 

		if (saleorder.equalsIgnoreCase("SALE ORDER")) {
			sendSaleOrder(branchcode, companyMst);
		}
		return "Success";
	}

	private List<String> sendSalesOrderVoucherNo(String branchcode, CompanyMst companyMst) {


		List<String> voucherNumberList = new ArrayList<String>();

		String voucherNoNumeric = salesOrderTransHdrRepository.getMaxVoucherNo(branchcode, companyMst);
		
		if(null != voucherNoNumeric)
		{
			voucherNumberList.add(voucherNoNumeric);
		}

		return voucherNumberList;
	}

	private List<String> sendProductionVoucherNo(String branchcode, CompanyMst companyMst) {
		List<String> voucherNumberList = new ArrayList<String>();

		String voucherNoNumeric = productionMstRepository.getMaxVoucherNo(branchcode, companyMst);
		
		if(null != voucherNoNumeric)
		{
			voucherNumberList.add(voucherNoNumeric);
		}

		return voucherNumberList;
				
	}

	private List<String> sendPurchaseVoucherNo(String branchcode, CompanyMst companyMst) {
		
		List<String> voucherNumberList = new ArrayList<String>();

		String voucherNoNumeric = purchaseHdrRepository.getMaxVoucherNo(branchcode, companyMst);
		if(null != voucherNoNumeric)
		{
			voucherNumberList.add(voucherNoNumeric);
		}

		return voucherNumberList;
	}

	private List<String> sendSalesVoucherNo(String branchcode, CompanyMst companyMst) {
		
		List<String> voucherNumberList = new ArrayList<String>();

		List<String> salesModeList = salesTransHdrRepository.getSalesMode(branchcode, companyMst);
		
		for(String salesMode : salesModeList)
		{

		String voucherNoNumeric = salesTransHdrRepository.getMaxVoucherNo(branchcode, companyMst,salesMode);
		if(null != voucherNoNumeric)
		{
			voucherNumberList.add(voucherNoNumeric);
		}
		
		}
		
		return voucherNumberList;

	}

	private void sendSaleOrder(String branchcode, CompanyMst companyMst) {

		List<SalesOrderTransHdr> salesOrderHdrList = salesOrderTransHdrRepository
				.findByCompanyMstAndBranchCode(companyMst, branchcode);

		for (SalesOrderTransHdr salesOrderTransHdr : salesOrderHdrList) {

			SalesOrderMessageEntity salesOrderMessageEntity = new SalesOrderMessageEntity();

			salesOrderMessageEntity.setSalesOrderTransHdr(salesOrderTransHdr);

//			CustomerMst customerMst = salesOrderTransHdr.getCustomerId();
			AccountHeads accountHeads = salesOrderTransHdr.getAccountHeads();

			LocalCustomerMst localCustomerMst = salesOrderTransHdr.getLocalCustomerId();

//			salesOrderMessageEntity.setCustomerMst(customerMst);
			salesOrderMessageEntity.setAccountHeads(accountHeads);
			
			salesOrderMessageEntity.setLocalCustomerMst(localCustomerMst);

			List<SalesOrderDtl> salesOrderDtl = salesOrderDtlRepository
					.findBySalesOrderTransHdrId(salesOrderTransHdr.getId());
			List<SaleOrderReceipt> salesOrderReceipts = saleOrderReceiptRepository
					.findByCompanyMstAndSalesOrderTransHdr(salesOrderTransHdr.getCompanyMst(), salesOrderTransHdr);
			salesOrderMessageEntity.getSalesOrderDtlList().addAll(salesOrderDtl);
			salesOrderMessageEntity.getSalesOrderReceiptsList().addAll(salesOrderReceipts);
			

		}
	}

	private void sendProduction(String branchcode, CompanyMst companyMst) {

		List<ProductionMst> productionMstList =  productionMstRepository.findByCompanyMstAndBranchCode(companyMst,
				branchcode);

		for (ProductionMst productionMst : productionMstList) {

			List<ProductionDtl> productDtlList = productionDetailRepository.findByProductionMst(productionMst);

			Iterator iter = productDtlList.iterator();
			while (iter.hasNext()) {
				ProductionDtl productionDtl = (ProductionDtl) iter.next();

				List<ProductionDtlDtl> productDtlDtlList = productionDtlDtlRepository
						.findByproductionDtl(productionDtl);
				try {
					//jmsTemplate.convertAndSend(branchcode + ".productiondtldtl", (ArrayList) productDtlDtlList);

				} catch (Throwable e) {
					System.out.print(e.toString());
				}
			}

		}
	}

	private void sendPurchase(String branchcode, CompanyMst companyMst) {

		List<PurchaseHdr> purchaseHdrList = purchaseHdrRepository.findByCompanyMstAndBranchCode(companyMst, branchcode);

		for (PurchaseHdr purchaseHdr : purchaseHdrList) {
			List<PurchaseDtl> purchaseDtlList = purchaseDtlRepository.findByPurchaseHdrId(purchaseHdr.getId());

			//jmsTemplate.convertAndSend(branchcode + ".purchase", purchaseDtlList);
		}

	}

	private void sendSales(String branchcode, CompanyMst companyMst) {

		SalesMessageEntity salesMessageEntity = new SalesMessageEntity();

		List<SalesTransHdr> hdrIdS = salesTransHdrRepository.findByCompanyMstAndBranchCode(companyMst, branchcode);

		Iterator iter = hdrIdS.iterator();
		while (iter.hasNext()) {
			SalesTransHdr transHdr = (SalesTransHdr) iter.next();

			List<SalesDtl> salesDtl = salesDetailsRepository.findBySalesTransHdrId(transHdr.getId());

			List<SalesReceipts> salesReceipts = salesReceiptsRepository.findBySalesTransHdr(transHdr);

			salesMessageEntity.getSalesDtlList().addAll(salesDtl);

			salesMessageEntity.getSalesReceiptsList().addAll(salesReceipts);

			//jmsTemplate.convertAndSend(branchcode + ".sales", salesMessageEntity);
		}
	}

	private void sendAccountingClass(String branchcode, CompanyMst companyMst) {

		List<AccountClass> accountClassList = accountClassRepository.findByCompanyMstAndBrachCode(companyMst,
				branchcode);

		Iterator iterForward = accountClassList.iterator();
		while (iterForward.hasNext()) {
			AccountClass accountClassForward = (AccountClass) iterForward.next();

			List<LedgerClass> ledgerList = ledgerClassRepository.findByAccountClassId(accountClassForward.getId());
			List<CreditClass> creditList = creditClassRepository.findByAccountClassId(accountClassForward.getId());
			List<DebitClass> debitList = debitClassRepository.findByAccountClassId(accountClassForward.getId());

			AccountMessage accountMessage = new AccountMessage();
			accountMessage.setAccountClass(accountClassForward);
			accountMessage.setCreditArray((ArrayList) creditList);
			accountMessage.setDebitArray((ArrayList) debitList);
			accountMessage.setLedgerArray((ArrayList) ledgerList);
			accountMessage.setAccountType(accountClassForward.getVoucherType());
			accountMessage.setProcessTally("TRUE");
			try {
				//jmsTemplate.convertAndSend(branchcode + ".bulkaccounting", accountMessage);

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		System.out.println("Send Accounting Bulk");

	}

	private void sendItemBatchDtl(String branchcode, CompanyMst companyMst) {

		List<ItemBatchDtl> itemBatchDtlList = new ArrayList<ItemBatchDtl>();

		List<Object> objList = itemBatchDtlRepository.findBranchStock(branchcode, companyMst);

		for (int i = 0; i < objList.size(); i++) {

			Object[] obj = (Object[]) objList.get(i);

			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();

			itemBatchDtl.setBarcode((String) obj[3]);
			itemBatchDtl.setBatch((String) obj[2]);
			itemBatchDtl.setCompanyMst(companyMst);
			itemBatchDtl.setItemId((String) obj[1]);
			itemBatchDtl.setMrp((Double) obj[4]);
			itemBatchDtl.setParticulars("DATABASE RECREATION");
			itemBatchDtl.setQtyIn((Double) obj[0]);
			itemBatchDtl.setQtyOut(0.0);
			itemBatchDtl.setStore("MAIN");
			itemBatchDtl.setSourceVoucherDate(SystemSetting.getSystemDate());
			itemBatchDtl.setSourceVoucherNumber("SRV");
			itemBatchDtl.setVoucherDate(SystemSetting.getSystemDate());
			VoucherNumber voucherNo = voucherService.generateInvoice(SystemSetting.getFinancialYear() + "SRV",
					companyMst.getId());

			itemBatchDtl.setVoucherNumber(voucherNo.getCode());

			itemBatchDtlList.add(itemBatchDtl);
		}

		try {
			//jmsTemplate.convertAndSend(branchcode + ".bulkitembatchdtl", itemBatchDtlList);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void sendProcessPermission(String branchcode, CompanyMst companyMst) {

		List<ProcessPermissionMst> itemList = processPermissionRepository.findByCompanyMst(companyMst);

		Iterator iter = itemList.iterator();
		while (iter.hasNext()) {
			ProcessPermissionMst process = (ProcessPermissionMst) iter.next();

			try {
				//jmsTemplate.convertAndSend(branchcode + ".processpermission", process);
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
	}

	private void sendProcessMst(String branchcode, CompanyMst companyMst) {

		List<ProcessMst> itemList = processMstRepository.findByCompanyMst(companyMst);

		Iterator iter = itemList.iterator();
		while (iter.hasNext()) {
			ProcessMst process = (ProcessMst) iter.next();

			try {
				//jmsTemplate.convertAndSend(branchcode + ".process", process);
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
	}

	private void sendUnitMst(String branchcode, CompanyMst companyMst) {

		List<UnitMst> unitList = unitMstRepo.findByCompanyMstId(companyMst.getId());

		Iterator iter = unitList.iterator();
		while (iter.hasNext()) {
			UnitMst unitMst = (UnitMst) iter.next();

			try {
			//	jmsTemplate.convertAndSend(branchcode + ".unit", unitMst);
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
	}

	private void sendKitDefenition(String branchcode, CompanyMst companyMst) {

		List<KitDefinitionMst> itemList = kitDefinitionMstRepo.findByCompanyMst(companyMst);

		Iterator iter = itemList.iterator();
		while (iter.hasNext()) {
			KitDefinitionMst kitdefMst = (KitDefinitionMst) iter.next();
			List<KitDefenitionDtl> kitDtl = kitDefenitionDtlRepo.findBykitDefenitionmstId(kitdefMst.getId());

			for (KitDefenitionDtl kitD : kitDtl)
				try {
					//jmsTemplate.convertAndSend(branchcode + ".kit", kitD);
				} catch (Exception e) {
					// TODO: handle exception
				}

			System.out.println("Send kit" + kitDtl);

		}
	}

	private void sendAccountHeads(String branchcode, CompanyMst companyMst) {

		List<AccountHeads> accountList = accountHeadsRepository.findByCompanyMstId(companyMst.getId());

		try {
			///jmsTemplate.convertAndSend(branchcode + ".accountheads", accountList);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

//	private void sendSupplier(String branchcode, CompanyMst companyMst) {
//
//		List<Supplier> supplierList = supplierRepository.findByCompanyMstId(companyMst.getId());
//		try {
//			//jmsTemplate.convertAndSend(branchcode + ".supplier", supplierList);
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//
//	}

	private void sendUserMst(String branchcode, CompanyMst companyMst) {

		List<UserMst> userMstList = userMstRepository.findByCompanyMstId(companyMst.getId());
		try {

			//jmsTemplate.convertAndSend(branchcode + ".user", userMstList);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void sendBranch(String branchcode, CompanyMst companyMst) {

		List<BranchMst> branchList = branchMstRepository.findByCompanyMst(companyMst);

		try {
			//jmsTemplate.convertAndSend(branchcode + ".branch", branchList);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void sendCustomers(String branchcode, CompanyMst companyMst) {

//		List<CustomerMst> customerList = customerMstRepository.findAll();
		List<AccountHeads> accountHeadsList = accountHeadsRepository.findAll();

		try {

			//jmsTemplate.convertAndSend(branchcode + ".customer", customerList);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void sendItemMst(String branchcode, CompanyMst companyMst) {

		ItemMst itemMst = new ItemMst();
		CategoryMst category = new CategoryMst();
		List<ItemMst> itemList = itemMstRepo.findByCompanyMst(companyMst);
		List<CategoryMst> categoryMstList = categoryMstRepository.findByCompanyMstId(companyMst.getId());

		try {
			//jmsTemplate.convertAndSend(branchcode + ".category", categoryMstList);

			//jmsTemplate.convertAndSend(branchcode + ".itemmst", itemList);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	
	
	@GetMapping("{companymstid}/databaserecreation/salesvouchernumber")
	public List<String> SalesVoucherNumberList(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		
		
		return sendSalesVoucherNo(branchcode, companyMst);
	}
	
	@GetMapping("{companymstid}/databaserecreation/purchasevouchernumber")
	public List<String> PurchaseVoucherNumberList(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		
		
		return sendPurchaseVoucherNo(branchcode, companyMst);
	}
	
	@GetMapping("{companymstid}/databaserecreation/productionvouchernumber")
	public List<String> ProductionVoucherNumberList(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		
		
		return sendProductionVoucherNo(branchcode, companyMst);
	}
	
	@GetMapping("{companymstid}/databaserecreation/saleordervouchernumber")
	public List<String> SaleOrderVoucherNumberList(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		
		return sendSalesOrderVoucherNo(branchcode, companyMst);
	}
	
	@GetMapping("{companymstid}/databaserecreation/vouchernumberrecreation")
	public String VoucherNumberRecreation(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "branchcode") String branchcode,
			@RequestParam(value = "code") String code,
			@RequestParam(value = "number") String number) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		
		List<String> voucherNumber = new ArrayList<String>();
		
		voucherNumber.add(number);
		voucherNumber.add(code);
		voucherNumber.add(companyMst.getId());
		try {

			//jmsTemplate.convertAndSend(branchcode + ".vouchernumber", voucherNumber);
		} catch (Exception e) {
			// TODO: handle exception
		}	
		
		return "Success";
	}

}
