package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.PharmacyNewCustomerWiseSaleReport;
import com.maple.restserver.service.PharmacyCustomerWiseSalesReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class PharmacyNewCustomerWiseSalesReportResource {
	@Autowired
	 PharmacyCustomerWiseSalesReportService amdcCustomerWiseSalesReportService;
	
	//---------------PharmacyNewCustomerWiseSalesReport-----sharon-------------
		@GetMapping("{companymstid}/pharmacynewcustomerwisesalesreportresource/getpharmacynewcustomerwisesalesreport")		
		public List<PharmacyNewCustomerWiseSaleReport> getAMDCCustomerWiseSaleReport(
				@PathVariable(value = "companymstid") String companymstid,
				@RequestParam("fromdate") String fromDate,
				@RequestParam("todate") String toDate,
				@RequestParam("customer") String customer) {
			
			java.util.Date fdate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
			java.util.Date tdate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");

			String customerNames=customer;
			String[]array=customerNames.split(";");
			
			return amdcCustomerWiseSalesReportService.findAMDCCustomerWiseSaleReport(fdate,tdate,array);
		}
}
