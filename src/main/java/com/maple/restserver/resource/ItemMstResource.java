
package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CommonReceiveMsgEntity;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IngredientsMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ItemUnitChangeTracker;
import com.maple.restserver.entity.KotCategoryMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.StockTransferInHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.ItemCorrectionReport;
import com.maple.restserver.report.entity.ItemExport;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.IngredientMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstHstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.ItemUnitChangeTrackerRepository;
import com.maple.restserver.repository.KotCategoryMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.service.ItemmstService;
import com.maple.restserver.service.ItemmstServiceImpl;
import com.maple.restserver.service.SaveAndPublishServiceImpl;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@CrossOrigin("http://localhost:4200")
public class ItemMstResource {

	private static final Logger logger = LoggerFactory.getLogger(PaymentHdrResource.class);

	@Value("${serverorclient}")
	private String serverorclient;
	
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	KotCategoryMstRepository kotCategoryMstRepo;
	@Autowired
	private ItemmstServiceImpl itemmstServiceImpl;

	@Autowired
	ItemmstService itemmstService;
	@Autowired
	private UnitMstRepository unitMstRepository;

	@Autowired
	private ItemBatchMstRepository itemBatchMstRepository;
	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	private IngredientMstRepository ingredientMstRepository;
	@Autowired
	private CompanyMstRepository companyMstRepository;

	@Autowired
	private CategoryMstRepository categoryMstRepository;
	@Autowired
	private ItemMstRepository itemMstRepository;

	@Autowired
	private ItemMstHstRepository itemMstHstRepository;

	@Autowired
	private ItemUnitChangeTrackerRepository itemUnitChangeTrackerRepository;
	@Autowired
	private ExternalApi externalApi;
	Pageable topFifty = PageRequest.of(0, 50);
	
	
	@Autowired
	SaveAndPublishServiceImpl saveAndPublishServiceImpl;

	// @Autowired
	// private RuntimeService runtimeService;

	EventBus eventBus = EventBusFactory.getEventBus();

	@GetMapping("{companyid}/itemmsts")
	public List<ItemMst> retrieveAllItem() {
		return itemmstServiceImpl.findAll();
	}

	@GetMapping("{companyid}/itemmstresource/exportitem")
	public List<ItemExport> exportItemDtls() {
		return itemmstService.exportItem();
	}

	@GetMapping("{companymstid}/allitemmsts")
	public List<ItemMst> retrieveAllItemByCompany(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		return itemMstRepository.findByCompanyMst(companyOpt.get());
	}

	@GetMapping("{companymstid}/itemmst/findallitemname")
	public List<String> retrieveAllItemBName(@PathVariable(value = "companymstid") String companymstid) {

		return itemMstRepository.findAllItemName();
	}

	@PostMapping("{companymstid}/itemmst")
	public ItemMst createItem(@Valid @RequestBody ItemMst itemMst1,
			@PathVariable(value = "companymstid") String companymstid) {

		logger.info("itemMstRepo" + itemMst1.getItemName());

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		itemMst1.setCompanyMst(companyMst);

		ItemMst saved = saveAndPublishServiceImpl.saveItemMst(itemMst1, mybranch);

		//publishItemMst(saved);

		/*
		 * Iniitate process
		 */

//		Map<String, Object> variables = new HashMap<String, Object>();
//
//		variables.put("companyid", companyMst);
//
//		variables.put("voucherNumber", saved.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("id", saved.getId());
//
//		variables.put("branchcode", saved.getBranchCode());
//
//		if (serverorclient.equalsIgnoreCase("REST")) {
//			variables.put("REST", 1);
//		} else {
//			variables.put("REST", 0);
//		}
//
//		// variables.put("voucherDate", purchase.getVoucherDate());
//		// variables.put("id",purchase.getId());
//		variables.put("inet", 0);
//
//		variables.put("WF", "itemsave");
//
//		String workflow = (String) variables.get("WF");
//		String voucherNumber = (String) variables.get("voucherNumber");
//		String sourceID = (String) variables.get("id");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//		// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
//
//		java.util.Date uDate = (Date) variables.get("voucherDate");
//		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//		lmsQueueMst.setVoucherDate(sqlVDate);
//
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("itemsave");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow + sourceID);
//		lmsQueueMst.setJobGroup(workflow);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID);
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//
//		variables.put("lmsqid", lmsQueueMst.getId());
//
//		eventBus.post(variables);

		/*
		 * ProcessInstanceWithVariables pVariablesInReturn =
		 * runtimeService.createProcessInstanceByKey("itemsave")
		 * .setVariables(variables)
		 * 
		 * .executeWithVariablesInReturn();
		 *
		 */

		return saved;
	}

	private void publishItemMst(ItemMst saved) {

		// forwardItem.execute(lmsQueueMst.getVoucherNumber(),
		// lmsQueueMst.getVoucherDate());

		logger.info("-------------INSIDE ITEM SAVE -----------");
		Optional<ItemMst> itemMst = itemMstRepository.findById(saved.getId());

		ItemMst item = itemMst.get();
		CategoryMst category = null;

		UnitMst unitMst = null;
//		if (null != item.getUnitId()) {
//			unitMst = unitMstRepository.findById(item.getUnitId()).get();
//
//			CommonReceiveMsgEntity commonReceiveMsgEntity = new CommonReceiveMsgEntity();
//
//			ArrayList<UnitMst> unitArray = new ArrayList<UnitMst>();
//			unitArray.add(unitMst);
//
//			commonReceiveMsgEntity.setUnitMst(unitArray);
//			commonReceiveMsgEntity.setMessageKey("UNITMST");
//
////			jmsTemplate.convertAndSend("server.common", commonReceiveMsgEntity);
//			try {
//				String companymstid = unitMst.getCompanyMst().getId();
//				ResponseEntity<UnitMst> message = externalApi.unitCreationToCloud(companymstid, unitMst);
//
//			} catch (Throwable e) {
//				System.out.print(e.toString());
//			}
//
//		}
//		if (null == item.getCategoryId()) {
//
//		} else {
//			category = categoryMstRepository.findById(item.getCategoryId()).get();
//
//			CommonReceiveMsgEntity commonReceiveMsgEntity = new CommonReceiveMsgEntity();
//
//			ArrayList<CategoryMst> categoryMstArray = new ArrayList<CategoryMst>();
//			categoryMstArray.add(category);
//
//			commonReceiveMsgEntity.setCategoryMst(categoryMstArray);
//			commonReceiveMsgEntity.setMessageKey("CATEGORY");
////			jmsTemplate.convertAndSend("server.common", commonReceiveMsgEntity);
//			try {
//				String companymstid = category.getCompanyMst().getId();
//				ResponseEntity<CategoryMst> message = externalApi.categoryCreationToCloud(companymstid, category);
//
//			} catch (Throwable e) {
//				System.out.print(e.toString());
//			}
//		}

		// CategoryMst category = categoryMst.get();
		try {

			CommonReceiveMsgEntity commonReceiveMsgEntity = new CommonReceiveMsgEntity();

			ArrayList<ItemMst> itemMstArray = new ArrayList<ItemMst>();
			itemMstArray.add(item);

			commonReceiveMsgEntity.setItemMst(itemMstArray);
			commonReceiveMsgEntity.setMessageKey("ITEMMST");
//			jmsTemplate.convertAndSend("server.common", commonReceiveMsgEntity);

			String companymstid = item.getCompanyMst().getId();
			ResponseEntity<ItemMst> message = externalApi.itemCreationToCloud(companymstid, item);

		} catch (Throwable e) {
			System.out.print(e.toString());
		}

	}
	
	

	@GetMapping("{companyid}/itemmst/{barcode}/barcode")

	public List<ItemMst> retrieveAllItemByBaroce(@PathVariable(value = "barcode") String barcode,

			Pageable pageable) {
		return itemmstServiceImpl.findByBarCode(barcode);
	}

	@GetMapping("{companyid}/itemmst/{itemname}/itemname")
	public ItemMst retrieveItemByItemname(@PathVariable(value = "itemname") String itemname) {
		itemname = itemname.replaceAll("%20", " ");
		return itemmstServiceImpl.findByItemName(itemname.trim());

	}

	@GetMapping("{companyid}/itemmst/itemname")
	public ItemMst retrieveItemByItemnamereqiestparam(@RequestParam(value = "itemdata") String itemdata) {
		return itemmstServiceImpl.findByItemName(itemdata.trim());

	}

//		
// 		 
// 		@GetMapping("{companyid}/itemmst/itemname")
//		public  ItemMst retrieveItemByItemnameparam(@RequestParam (value = "item") String itemdata){
//			return  itemmstServiceImpl.findByItemName(itemdata.trim());
//			
//			
//			
//		}

	@GetMapping("{companyid}/itemmst/{itemid}/itemmst")
	public Optional<ItemMst> retrieveAllItemById(@PathVariable(value = "itemid") String itemid, Pageable pageable) {
		return itemmstServiceImpl.findById(itemid);
	}

	@GetMapping("{companyid}/itemmst/deleteitem/{itemid}")
	public String DeleteItem(@PathVariable(value = "itemid") String itemid) {
		itemMstRepository.deleteItem(itemid);
		Optional<ItemMst> itemmstOpt = itemMstRepository.findById(itemid);
		if (itemmstOpt.isPresent()) {
			ItemMst itemMst = itemmstOpt.get();
			itemMst.setIsDeleted("Y");
			itemMstRepository.saveAndFlush(itemMst);
		}

		return "success";
	}

	@GetMapping("{companyid}/itemmst/{categoryid}/categorymst")
	public List<ItemMst> getItemMstByCategoryId(@PathVariable(value = "companyid") String companyid,
			@PathVariable(value = "categoryid") String categoryid, Pageable pageable) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		// Optional<CategoryMst> categoryMstOpt =
		// categoryMstRepository.findById(categoryid);
		// CategoryMst categoryMst = categoryMstOpt.get();

		return itemmstServiceImpl.FindByComapnyMstAndCategoryMst(companyMst, categoryid);

	}

	@PutMapping("{companyid}/itemmstresource/{branchcode}/barcodeprintingupdate")
	public ItemMst itemMstUpdateByBarCode(@PathVariable String branchcode,
			@PathVariable(value = "companyid") String companyid, @Valid @RequestBody ItemMst itemMstRequest) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();
		itemMstRequest.setCompanyMst(companyMst);

		Optional<ItemMst> itemmstOpt = itemMstRepository.findById(itemMstRequest.getId());
		System.out.println(itemmstOpt.get() + "item mst object issssssssssss");
		ItemMst itemmst = itemmstOpt.get();
		itemmst.setBarCode(itemMstRequest.getBarCode());
		itemmst.setNetWeight(itemMstRequest.getNetWeight());
		System.out.println(itemMstRequest.getBarCodeLine2() + "barcode line 2 issssssssssssssssssssss");
		itemmst.setBestBefore(itemMstRequest.getBestBefore());
		System.out.println(itemMstRequest.getBarCodeLine1() + "barcode line one isssssssssssssssssss");
		itemmst.setBarCodeLine1(itemMstRequest.getBarCodeLine1());
		itemmst.setBarCodeLine2(itemMstRequest.getBarCodeLine2());
		itemmst.setBarcodeFormat(itemMstRequest.getBarcodeFormat());

		itemmst.setNutrition1(itemMstRequest.getNutrition1());
		itemmst.setNutrition2(itemMstRequest.getNutrition2());
		itemmst.setNutrition3(itemMstRequest.getNutrition3());
		itemmst.setNutrition4(itemMstRequest.getNutrition4());
		itemmst.setNutrition5(itemMstRequest.getNutrition5());
		itemmst.setNutrition6(itemMstRequest.getNutrition6());
		itemmst.setNutrition7(itemMstRequest.getNutrition7());
		itemmst.setNutrition8(itemMstRequest.getNutrition8());
		itemmst.setNutrition9(itemMstRequest.getNutrition9());
		itemmst.setNutrition10(itemMstRequest.getNutrition10());
		itemmst.setNutrition11(itemMstRequest.getNutrition11());
		itemmst.setNutrition12(itemMstRequest.getNutrition12());
		itemmst.setNutrition13(itemMstRequest.getNutrition13());
		itemmst.setNutrition14(itemMstRequest.getNutrition14());

		itemmstServiceImpl.saveItemMst(itemmst);

		return itemMstRequest;

	}

	@PutMapping("{companyid}/itemmstresource/{branchcode}/ingredientsupdate")
	public IngredientsMst imgredientUpdateByBranchCode(@PathVariable String branchcode,
			@PathVariable(value = "companyid") String companyid,
			@Valid @RequestBody IngredientsMst ingredientMstRequest) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();
		ingredientMstRequest.setCompanyMst(companyMst);

		IngredientsMst ingredientsMst = ingredientMstRepository.findByItemMst(ingredientMstRequest.getItemMst());
		if (null == ingredientsMst) {
			ingredientsMst = ingredientMstRequest;
		}

//		if(ingredientsMst != null) {
		ingredientsMst.setIngredientText1(ingredientMstRequest.getIngredientText1());
		ingredientsMst.setIngredientText2(ingredientMstRequest.getIngredientText2());
		ingredientMstRepository.save(ingredientsMst);
//			}else {
//
//				ingredientsMst.setItemMst(ingredientMstRequest.getItemMst());
//				ingredientsMst.setIngredientText1(ingredientMstRequest.getIngredientText1());
//				ingredientsMst.setIngredientText2(ingredientMstRequest.getIngredientText2());
//
//				ingredientMstRepository.save(ingredientsMst);
//			}
		return ingredientMstRequest;

	}

	@PutMapping("{companyid}/itemmst/{itemid}/itemmstrankupdate")
	public ItemMst itemMstRankUpdate(@PathVariable String itemid, @PathVariable(value = "companyid") String companyid,
			@Valid @RequestBody ItemMst itemMstRequest) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();
		itemMstRequest.setCompanyMst(companyMst);
		Optional<ItemMst> itemmstOpt = itemMstRepository.findById(itemMstRequest.getId());
		System.out.println(itemmstOpt.get() + "item mst object issssssssssss");
		ItemMst itemmst = itemmstOpt.get();
		itemmst.setRank(itemMstRequest.getRank());
		return itemMstRepository.saveAndFlush(itemmst);

	}

	@PutMapping("{companyid}/itemmst/{itemid}/itemmst")
	public ItemMst itemMstUpdate(@PathVariable String itemid, @PathVariable(value = "companyid") String companyid,
			@Valid @RequestBody ItemMst itemMstRequest) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		itemMstRequest.setCompanyMst(companyMst);

		return itemmstServiceImpl.findById(itemid).map(itemmst -> {

			itemmst.setStandardPrice(itemMstRequest.getStandardPrice());
			itemmst.setItemName(itemMstRequest.getItemName());
			itemmst.setHsnCode(itemMstRequest.getHsnCode());
			itemmst.setReorderWaitingPeriod(itemMstRequest.getReorderWaitingPeriod());
			itemmst.setCategoryId(itemMstRequest.getCategoryId());
			itemmst.setCess(itemMstRequest.getCess());
			itemmst.setTaxRate(itemMstRequest.getTaxRate());
			itemmst.setIsDeleted(itemMstRequest.getIsDeleted());
			itemmst.setItemCode(itemMstRequest.getItemCode());
			if (null != itemMstRequest.getItemPriceLock()) {
				itemmst.setItemPriceLock(itemMstRequest.getItemPriceLock());
			}
			if (!itemmst.getBarCode().equalsIgnoreCase(itemMstRequest.getBarCode())) {
				// update barcode from all the tables
				itemmstServiceImpl.updateItemBarcode(itemmst, itemMstRequest.getBarCode());
			}
			itemmst.setBarCode(itemMstRequest.getBarCode());
			itemmst.setBestBefore(itemMstRequest.getBestBefore());

			if (!itemmst.getUnitId().equalsIgnoreCase(itemMstRequest.getUnitId())) {
				saveItemUnitChangeTracker(itemmst);
			}
			itemmst.setUnitId(itemMstRequest.getUnitId());
//					itemBatchMst.setBarcode(itemMstRequest.getBarCode());
//					itembatchdtl.setBarcode(itemMstRequest.getBarCode());
			//ItemMst saved = itemmstServiceImpl.saveItemMst(itemmst);
			ItemMst saved = saveAndPublishServiceImpl.saveItemMst(itemmst, mybranch);
			//publishItemMst(saved);

			/*
			 * Save History
			 */
//					ItemMstHst itemMstHst = new ItemMstHst();
//					itemMstHst.setBarCode(itemmst.getBarCode());
//					itemMstHst.setBarCodeLine1(itemmst.getBarCodeLine1());
//					itemMstHst.setBarCodeLine2(itemmst.getBarCodeLine2());
//					itemMstHst.setBestBefore(itemmst.getBestBefore());
//					itemMstHst.setBinNo(itemmst.getBinNo());
//					itemMstHst.setBranchCode(itemmst.getBranchCode());
//					itemMstHst.setCategoryId(itemmst.getCategoryId());
//					itemMstHst.setCess(itemmst.getCess());
//					itemMstHst.setCgst(itemmst.getCgst());
//					itemMstHst.setCompanyMst(itemmst.getCompanyMst());
//					itemMstHst.setHsnCode(itemmst.getHsnCode());
//					//itemMstHst.setId(id);
//					itemMstHst.setIsDeleted(itemmst.getIsDeleted());
//					itemMstHst.setIsKit(itemmst.getIsKit());
//					itemMstHst.setItemCode(itemmst.getItemCode());
//					itemMstHst.setItemDiscount(itemmst.getItemDiscount());
//					itemMstHst.setItemDtl(itemmst.getItemDtl());
//					itemMstHst.setItemGenericName(itemmst.getItemGenericName());
//					itemMstHst.setItemGroupId(itemmst.getItemGroupId());
//					itemMstHst.setItemName(itemmst.getItemName());
//					itemMstHst.setMachineId(itemmst.getMachineId());
//					itemMstHst.setReorderWaitingPeriod(itemmst.getReorderWaitingPeriod());
//					itemMstHst.setSgst(itemmst.getSgst());
//					itemMstHst.setStandardPrice(itemmst.getStandardPrice());
//					itemMstHst.setSupplierId(itemmst.getSupplierId());
//					itemMstHst.setTaxRate(itemmst.getTaxRate());
//					itemMstHst.setUnitId(itemmst.getUnitId());
//					itemMstHst.setItemId(itemmst.getId());
//					
//					itemMstHstRepository.saveAndFlush(itemMstHst);

			// Remarked because Physical stock was saving
			// itemBatchMstRepository.udateBarcode(itemMstRequest.getId(),itemMstRequest.getBarCode());
			// itemBatchDtlRepository.udateBarcode(itemMstRequest.getId(),itemMstRequest.getBarCode());

//			Map<String, Object> variables = new HashMap<String, Object>();
//
//			variables.put("companyid", companyMst);
//
//			variables.put("voucherNumber", itemmst.getId());
//			variables.put("voucherDate", SystemSetting.getSystemDate());
//			variables.put("id", itemmst.getId());
//
//			variables.put("branchcode", itemmst.getBranchCode());
//
//			if (serverorclient.equalsIgnoreCase("REST")) {
//				variables.put("REST", 1);
//			} else {
//				variables.put("REST", 0);
//			}
//
//			// variables.put("voucherDate", purchase.getVoucherDate());
//			// variables.put("id",purchase.getId());
//			variables.put("inet", 0);
//
//			variables.put("WF", "itemsave");
//
//			String workflow = (String) variables.get("WF");
//			String voucherNumber = (String) variables.get("voucherNumber");
//			String sourceID = (String) variables.get("id");
//
//			LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//			// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//			java.util.Date uDate = (Date) variables.get("voucherDate");
//			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//			lmsQueueMst.setVoucherDate(sqlVDate);
//
//			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//			lmsQueueMst.setVoucherType(workflow);
//			lmsQueueMst.setPostedToServer("NO");
//			lmsQueueMst.setJobClass("itemsave");
//			lmsQueueMst.setCronJob(true);
//			lmsQueueMst.setJobName(workflow + sourceID);
//			lmsQueueMst.setJobGroup(workflow);
//			lmsQueueMst.setRepeatTime(60000L);
//			lmsQueueMst.setSourceObjectId(sourceID);
//
//			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//			lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//
//			variables.put("lmsqid", lmsQueueMst.getId());
//
//			eventBus.post(variables);

			return itemMstRequest;

		}).orElseThrow(() -> new ResourceNotFoundException("itemmst " + itemMstRequest + " not found"));

	}

	private void saveItemUnitChangeTracker(ItemMst itemmst) {

		ItemUnitChangeTracker itemUnitChangeTracker = new ItemUnitChangeTracker();
		itemUnitChangeTracker.setCompanyMst(itemmst.getCompanyMst());
		itemUnitChangeTracker.setItemId(itemmst.getId());

		itemUnitChangeTrackerRepository.save(itemUnitChangeTracker);
	}

	@PostMapping("/{companymstid}/itemmstresource/ingredients")
	public IngredientsMst createIngredient(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody IngredientsMst ingredientsMst) {

		return companyMstRepository.findById(companymstid).map(companyMst -> {

			ingredientsMst.setCompanyMst(companyMst);

			IngredientsMst existingIngredient = ingredientMstRepository.findByItemMst(ingredientsMst.getItemMst());
			existingIngredient.setIngredientText1(ingredientsMst.getIngredientText1());
			existingIngredient.setIngredientText2(ingredientsMst.getIngredientText2());

			return ingredientMstRepository.saveAndFlush(existingIngredient);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));

	}

	@GetMapping("{companymstid}/web/saveitem/{itemname}/{itemgenericname}/{unitname}/{itemid}")
	public ItemMst createItemMstWeb(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemname") String itemname,
			@PathVariable(value = "itemgenericname") String itemgenericname,
			@PathVariable(value = "itemid") String itemid, @PathVariable(value = "unitname") String unitname) {

		ItemMst itemMst = new ItemMst();

		itemname = itemname.replaceAll("%20", " ");
		itemgenericname = itemgenericname.replaceAll("%20", " ");
		itemid = itemid.replaceAll("%20", " ");
		unitname = unitname.replaceAll("%20", " ");

		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		itemMst.setCompanyMst(companyOpt.get());
		itemMst.setItemName(itemname);
		itemMst.setItemGenericName(itemgenericname);
		itemMst.setId(itemid);

		UnitMst unit = unitMstRepository.findByUnitNameAndCompanyMstId(unitname, companymstid);
		itemMst.setUnitId(unit.getId());

		itemMst = itemMstRepository.saveAndFlush(itemMst);

		return itemMst;

	}

	@GetMapping("{companymstid}/serviceitemmsts")
	public List<ItemMst> getServiceItemMstByCompany(@PathVariable("companymstid") String companymstid)

	{

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		return itemMstRepository.findByCompanyMstAndItemType(companyMst, "SERVICE ITEM");
	}

	@GetMapping("{companymstid}/itemandcategorysearchwithname")
	public synchronized @ResponseBody List<Object> searchItemsWithName(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid) {
		Optional<CompanyMst> companymst = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companymst.get();

		searchstring = searchstring.replaceAll("20%", " ");

		List<Object> objByItemCode = itemMstRepository.findSearchByName("%" + searchstring.toLowerCase() + "%",
				topFifty, companyMst);

		System.out.println(objByItemCode);

		return objByItemCode;

	}

	@GetMapping("{companymstid}/itemmst/getitembycategory/{catname}")
	public List<Object> getByCategoryAndSubCategory(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "catname") String catname, @RequestParam(value = "itemname") String itemname) {
		Optional<CompanyMst> companyMst1 = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMst1.get();
		itemname = itemname.replaceAll("%20", " ");
		return itemMstRepository.getitemByCategoryAndName(catname, "%" + itemname.toLowerCase() + "%", companyMst);
	}

	@GetMapping("{companymstid}/itemmst/getitemwithnullcategory")
	public List<ItemMst> getItemWithNullCategory() {
		return itemMstRepository.getItemWithNullcategory();
	}

	/*
	 * This url is used to find item with category in kotcategorymst bt item name
	 * not in kotitemmst
	 */
	@GetMapping("{companyid}/itemmst/finditemnotinkotitem")
	public List<ItemMst> getItemNotInKotItemMst(@PathVariable(value = "companyid") String companyid) {

		List<ItemMst> itemList = new ArrayList();
		List<KotCategoryMst> kotcatList = kotCategoryMstRepo.findAll();
		for (int i = 0; i < kotcatList.size(); i++) {
			List<ItemMst> itemmstlist = itemMstRepository.getItemNotInKotItemMst(kotcatList.get(i).getCategoryId());
			itemList.addAll(itemmstlist);
		}
		return itemList;
	}

	@GetMapping("{companyid}/itemmst/searchitemnotinkotitem")
	public List<ItemMst> searchItemNotInKotItemMst(@PathVariable(value = "companyid") String companyid,
			@RequestParam(value = "data") String data) {
		data = data.replaceAll("%20", " ");
		List<ItemMst> itemList = new ArrayList();
		List<KotCategoryMst> kotcatList = kotCategoryMstRepo.findAll();
		for (int i = 0; i < kotcatList.size(); i++) {
			List<ItemMst> itemmstlist = itemMstRepository.searchItemNotInKotItemMst(kotcatList.get(i).getCategoryId(),
					"%" + data.toLowerCase() + "%");
			itemList.addAll(itemmstlist);
		}
		return itemList;
	}

	// ------------------new url ----publish offline button

	@PostMapping("{companymstid}/itemmst/{status}")
	public ItemMst sendOffLineMessageToCloud(@Valid @RequestBody ItemMst itemMst1,
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("status") String status) {
//		ResponseEntity<ItemMst> saved = externalApi.sendItemMstOnLineToCloud(companymstid, itemMst1);
//
//		return saved.getBody();
		try {
			ResponseEntity<ItemMst> saved = externalApi.sendItemMstOnLineToCloud(companymstid, itemMst1);

			return saved.getBody();
		} catch (Exception e) {
			return null;
		}

	}

	// ================MAP-70-api-for-item-deletion===============anandu===========================

	@DeleteMapping("{companyid}/itemmstresource/deleteitemnewurl/{itemid}")
	public void DeleteItemWithoutStatusUpdate(@PathVariable(value = "itemid") String itemId) {
		itemMstRepository.deleteById(itemId);

	}

	// ======================end======MAP-70-api-for-item-deletion============================

	@GetMapping("{companyid}/itemmst/getitemlistwithcountofsalesstockandpurchase")
	public List<ItemCorrectionReport> getItemListWithCountofSaleStockPurchase(
			@PathVariable(value = "companyid") String companyid) {

		return itemmstService.getItemListWithCountofSaleStockPurchase(companyid);

	}
	
	
	 
	  /*
	   * itemid and price publishing to server..............29-04-2022.........
	   */
	  @GetMapping("{companymstid}/itemmstresource/publishitemidpricetoserver/{itemid}/{standardPrice}/{destination}")
	 	public String publishitemidpricetoserver(
	 			@PathVariable(value = "companymstid") String companymstid,
	 			@PathVariable("itemid") String itemid,
	 	        @PathVariable("standardPrice") Double standardPrice,
	 	       @PathVariable("destination") String destination)
	 	
	 	{
	 	  
	 		return itemmstService.publishItemIdAndPriceToServer(itemid,standardPrice,companymstid,destination);
	 		
	 	}
	
	
	
	
	
	
	
}
