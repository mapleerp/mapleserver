package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.StockTransferInReturnHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.StockTransferInReturnHdrRepository;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class StockTransferInReturnHdrResource {
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	
	@Autowired
	StockTransferInReturnHdrRepository  stockTransferInReturnHdrRepository;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository; 
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	
	//save 
		@PostMapping("{companymstid}/stocktransferinreturnhdrresource/savestocktransferinreturnhdr")
		public StockTransferInReturnHdr createStockTransferInReturnHdr(
				@PathVariable(value="companymstid")	String commpanymstid,
				@Valid @RequestBody StockTransferInReturnHdr stockTransferInReturnHdr)
		{
			CompanyMst companymst=CompanyMstRepository.findById(commpanymstid).get();
			stockTransferInReturnHdr.setCompanyMst(companymst);
			stockTransferInReturnHdr=stockTransferInReturnHdrRepository.save(stockTransferInReturnHdr);
			return stockTransferInReturnHdr;
		}
		
		
		// delete
		@DeleteMapping("{companymstid}/stocktransferinreturnhdrresource/deletestocktransferinreturnhdr/{id}")
		public void DeleteStockTransferInReturnHdrt(@PathVariable(value = "id") String Id) {
			stockTransferInReturnHdrRepository.deleteById(Id);
		}
		
		
		
		@GetMapping("{companymstid}/stocktransferinreturnhdrresource/showallstocktransferinreturnhdr")
		public List<StockTransferInReturnHdr> retrieveAllStockTransferInReturnHdr(
				@PathVariable(value = "companymstid") String companymstid) {

			Optional<CompanyMst> companymstOpt=CompanyMstRepository.findById(companymstid);
			return stockTransferInReturnHdrRepository.findByCompanyMst(companymstOpt.get());

		}
		
		
		
		//update 
		
		@PutMapping("{companymstid}/stocktransferinreturnhdrresource/stocktransferinreturnhdrupdate/{stocktransferinreturnhdrid}")
		public StockTransferInReturnHdr StockTransferInReturnHdrFinalSave(@PathVariable String stocktransferinreturnhdrid,
				@PathVariable(value = "companymstid") String companymstid,
				@Valid @RequestBody StockTransferInReturnHdr stockTransferInReturnHdrRequest) {
			
			Optional<CompanyMst> companyMst= CompanyMstRepository.findById(companymstid);

			Optional<StockTransferInReturnHdr> stockTransferInReturnHdrOpt=stockTransferInReturnHdrRepository.findById(stocktransferinreturnhdrid);
			StockTransferInReturnHdr stockTransferInReturnHdr =stockTransferInReturnHdrOpt.get();
			stockTransferInReturnHdr.setVoucherNumber(stockTransferInReturnHdrRequest.getVoucherNumber());
			stockTransferInReturnHdr.setCompanyMst(companyMst.get());
			stockTransferInReturnHdr.setVoucherDate(stockTransferInReturnHdrRequest.getVoucherDate());
		
			stockTransferInReturnHdr = stockTransferInReturnHdrRepository.saveAndFlush(stockTransferInReturnHdr);
			
			updateStockTransferInReturnHdr(stockTransferInReturnHdr,companyMst.get());
			
			Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", stockTransferInReturnHdr.getVoucherNumber());
			variables.put("voucherDate", stockTransferInReturnHdr.getVoucherDate());
			variables.put("id", stockTransferInReturnHdr.getId());
			variables.put("companyid", stockTransferInReturnHdr.getCompanyMst());
			variables.put("branchcode", stockTransferInReturnHdr.getBranchCode());

			variables.put("inet", 0);
			variables.put("REST", 1);

			variables.put("WF", "forwardStoreChange");

			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");

			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));

			java.util.Date uDate = (Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);

			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardStoreChange");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);

			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);

			
			return stockTransferInReturnHdr;
		}


	private void updateStockTransferInReturnHdr(StockTransferInReturnHdr stockTransferInReturnHdr,
	CompanyMst companyMst) {
//
//
//			List<StockTransferInReturnHdrDtl> stockTransferInReturnDtlList = stockTransferInReturnDtlRepository
//					.findByStoreChangeMstAndCompanyMst(storeChangeMst,comapnyMst);
//
//			Iterator iter = storeChangeDtlList.iterator();
//			while (iter.hasNext()) {
//				StoreChangeDtl storeChangeDtl = (StoreChangeDtl) iter.next();
//
//
//
//				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
//				final VoucherNumber voucherNo = voucherService.generateInvoice("STV", comapnyMst.getId());
//
//				itemBatchDtl.setBarcode(storeChangeDtl.getBarcode());
//				itemBatchDtl.setBatch(storeChangeDtl.getBatch());
//				itemBatchDtl.setItemId(storeChangeDtl.getItemId());
//				itemBatchDtl.setMrp(storeChangeDtl.getMrp());
//				itemBatchDtl.setQtyIn(0.0);
//				itemBatchDtl.setQtyOut(storeChangeDtl.getQty());
//				itemBatchDtl.setStore(storeChangeMst.getStore());
//				itemBatchDtl.setVoucherNumber(voucherNo.getCode());
//				itemBatchDtl.setVoucherDate(storeChangeMst.getVoucherDate());
//				itemBatchDtl.setItemId(storeChangeDtl.getItemId());
//
//				itemBatchDtl.setSourceVoucherNumber(storeChangeMst.getVoucherNumber());
//				itemBatchDtl.setSourceVoucherDate(storeChangeMst.getVoucherDate());
//
//				itemBatchDtl.setParticulars("STORE CHANGE FROM " + storeChangeMst.getStore());
//				itemBatchDtl.setBranchCode(storeChangeMst.getFromBranch());
//				itemBatchDtl.setCompanyMst(comapnyMst);
//				itemBatchDtl = itemBatchDtlRepository.saveAndFlush(itemBatchDtl);
//				
//
//				Map<String, Object> variables = new HashMap<String, Object>();
//
//				variables.put("voucherNumber", itemBatchDtl.getId());
//				variables.put("id", itemBatchDtl.getId());
//				variables.put("voucherDate", SystemSetting.getSystemDate());
//				variables.put("inet", 0);
//				variables.put("REST", 1);
//				variables.put("branchcode", itemBatchDtl.getBranchCode());
//				variables.put("companyid", itemBatchDtl.getCompanyMst());
//
//				variables.put("WF", "forwardItemBatchDtl");
//
//				LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//				String workflow = (String) variables.get("WF");
//				String sourceID = itemBatchDtl.getId();
//
//				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//				// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//				java.util.Date uDate = (Date) variables.get("voucherDate");
//				java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//				lmsQueueMst.setVoucherDate(sqlVDate);
//
//				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//				lmsQueueMst.setVoucherType(workflow);
//				lmsQueueMst.setPostedToServer("NO");
//				lmsQueueMst.setJobClass("forwardItemBatchDtl");
//				lmsQueueMst.setCronJob(true);
//				lmsQueueMst.setJobName(workflow + sourceID);
//				lmsQueueMst.setJobGroup(workflow);
//				lmsQueueMst.setRepeatTime(60000L);
//				lmsQueueMst.setSourceObjectId(sourceID);
//
//				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//				variables.put("lmsqid", lmsQueueMst.getId());
//
//				eventBus.post(variables);
//				
//				ItemBatchDtl itemBatchDtl1 = new ItemBatchDtl();
//				final VoucherNumber voucherNo1 = voucherService.generateInvoice("STV", comapnyMst.getId());
//
//				itemBatchDtl1.setBarcode(storeChangeDtl.getBarcode());
//				itemBatchDtl1.setBatch(storeChangeDtl.getBatch());
//				itemBatchDtl1.setItemId(storeChangeDtl.getItemId());
//				itemBatchDtl1.setMrp(storeChangeDtl.getMrp());
//				itemBatchDtl1.setQtyIn(storeChangeDtl.getQty());
//				itemBatchDtl1.setQtyOut(0.0);
//				itemBatchDtl1.setVoucherNumber(voucherNo1.getCode());
//				itemBatchDtl1.setVoucherDate(storeChangeMst.getVoucherDate());
//				itemBatchDtl1.setItemId(storeChangeDtl.getItemId());
//
//				itemBatchDtl1.setSourceVoucherNumber(storeChangeMst.getVoucherNumber());
//				itemBatchDtl1.setSourceVoucherDate(storeChangeMst.getVoucherDate());
//
//				itemBatchDtl1.setParticulars("STORE CHANGE TO " + storeChangeMst.getStore());
//				itemBatchDtl1.setBranchCode(storeChangeMst.getFromBranch());
//				itemBatchDtl1.setCompanyMst(comapnyMst);
//				itemBatchDtl1.setStore(storeChangeMst.getToStore());
//				itemBatchDtl1 = itemBatchDtlRepository.saveAndFlush(itemBatchDtl1);
//				
//				Map<String, Object> variables1 = new HashMap<String, Object>();
//
//				variables1.put("voucherNumber", itemBatchDtl1.getId());
//				variables1.put("id", itemBatchDtl1.getId());
//				variables1.put("voucherDate", SystemSetting.getSystemDate());
//				variables1.put("inet", 0);
//				variables1.put("REST", 1);
//				variables1.put("branchcode", itemBatchDtl1.getBranchCode());
//				variables1.put("companyid", itemBatchDtl1.getCompanyMst());
//
//				variables1.put("WF", "forwardItemBatchDtl");
//
//				LmsQueueMst lmsQueueMst1 = new LmsQueueMst();
//
//				String workflow1 = (String) variables1.get("WF");
//				String sourceID1 = itemBatchDtl1.getId();
//
//				lmsQueueMst1.setCompanyMst((CompanyMst) variables1.get("companyid"));
//				// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//				java.util.Date uDate1 = (Date) variables1.get("voucherDate");
//				java.sql.Date sqlVDate1 = SystemSetting.UtilDateToSQLDate(uDate1);
//				lmsQueueMst1.setVoucherDate(sqlVDate1);
//
//				lmsQueueMst1.setVoucherNumber((String) variables1.get("voucherNumber"));
//				lmsQueueMst1.setVoucherType(workflow1);
//				lmsQueueMst1.setPostedToServer("NO");
//				lmsQueueMst1.setJobClass("forwardItemBatchDtl");
//				lmsQueueMst1.setCronJob(true);
//				lmsQueueMst1.setJobName(workflow1 + sourceID1);
//				lmsQueueMst1.setJobGroup(workflow1);
//				lmsQueueMst1.setRepeatTime(60000L);
//				lmsQueueMst1.setSourceObjectId(sourceID1);
//
//				lmsQueueMst1.setBranchCode((String) variables1.get("branchcode"));
//
//				lmsQueueMst1 = lmsQueueMstRepository.saveAndFlush(lmsQueueMst1);
//				variables1.put("lmsqid", lmsQueueMst1.getId());
//
//				eventBus.post(variables1);}
//
			}
//
//		
			
		}
		
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

