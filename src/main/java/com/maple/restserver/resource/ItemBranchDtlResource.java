package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBranchDtl;
import com.maple.restserver.entity.ItemBranchHdr;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBranchDtlRepository;
import com.maple.restserver.repository.ItemBranchMstRepository;

@RestController
@Transactional
public class ItemBranchDtlResource {
	
	
@Autowired
ItemBranchDtlRepository itemBranchDtlRepository;
	 
@Autowired
CompanyMstRepository companyMstRepository;

@Autowired
ItemBranchMstRepository itemBranchMstRepository;



@PostMapping("{companymstid}/itembranchdtl/itembranchdtl/{itembranchmstid}")
public ItemBranchDtl createItemBranchDtl(
		@PathVariable(value = "itembranchmstid") String itembranchmstid,
		@PathVariable (value = "companymstid") String companymstid,
		@Valid @RequestBody ItemBranchDtl itemBranchDtlRequest) {
	
	
	
	Optional<ItemBranchHdr> itemBranchHdrOpt= itemBranchMstRepository.findById(itembranchmstid);
	ItemBranchHdr itemBranchHdr=itemBranchHdrOpt.get();
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
		itemBranchDtlRequest.setCompanyMst(companyMst);
		
		itemBranchDtlRequest.setItemBranchHdr(itemBranchHdr);;
		return itemBranchDtlRepository.saveAndFlush(itemBranchDtlRequest);
	
}


@GetMapping("{companymstid}/itembranchhdr/{itembranchhdrid}/itembranchdtls")
public List<ItemBranchDtl> retrieveAllItemBranchDtlByItemBranchHdrId(
		@PathVariable(value = "itembranchhdrid") String itembranchhdrid) {
	return itemBranchDtlRepository.findByItemBranchHdrId(itembranchhdrid);

}


@GetMapping("{companymstid}/itembranchdtl/itembranchdtls")
public List<ItemBranchDtl> retrieveAllitemBranchDtl(@PathVariable(value="companymstid") String companymstid){
	return itemBranchDtlRepository.findByCompanyMstId(companymstid);
}
@DeleteMapping("{companymstid}/itembranchdtl/{id}")
public void ItemBranchDelete(@PathVariable(value = "id") String Id) {
	itemBranchDtlRepository.deleteById(Id);

}






}
