package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.PharmacyItemMovementAnalisysReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.PharmacyInventoryMovementReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class PharmacyInventoryMovementReportResource {

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	PharmacyInventoryMovementReportService pharmacyInventoryMovementReportService;

	@GetMapping("/{companymstid}/pharmacyinventorymovementreportresource/{category}/getitemmovementreport/{branchCode}")
	public List<PharmacyItemMovementAnalisysReport> getPharmacyInventoryMovementReport(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branchCode") String branchCode,
			@PathVariable("category") String category, @RequestParam("fdate") String fromdate,
			@RequestParam("tdate") String todate) {
		java.sql.Date fdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.sql.Date tdate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");

		String categoryNames = category;

		String[] array = categoryNames.split(";");
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return pharmacyInventoryMovementReportService.getPharmacyInventoryMovementReport(companyMst, fdate, tdate,
				branchCode, array);

	}

	@GetMapping("/{companymstid}/pharmacyinventorymovementreportresource/{batch}/getitemmovementreportbycatanditem/{branchCode}")
	public List<PharmacyItemMovementAnalisysReport> getPharmacyInventoryMovementReportByCatItem(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branchCode") String branchCode,
			 @PathVariable("batch") String batch,
			 @RequestParam("fdate") String fromdate,
			@RequestParam("tdate") String todate,
			@RequestParam("category") String category,
			@RequestParam("item") String item) {
		java.sql.Date fdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.sql.Date tdate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");

		String categoryNames = category;

		String[] categoryArray = categoryNames.split(";");

		String itemNames = item;

		String[] itemArray = itemNames.split(";");

		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		
		CompanyMst companyMst = companyOpt.get();
		return pharmacyInventoryMovementReportService.getPharmacyInventoryMovementReportByCatItem(companyMst, fdate,
				tdate, branchCode, categoryArray, itemArray, batch);

	}

}
