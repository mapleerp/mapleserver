package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.report.entity.AMDCDailySalesSummaryReport;
import com.maple.restserver.report.entity.DayEndProcessing;
import com.maple.restserver.report.entity.DayEndReceiptMode;
import com.maple.restserver.report.entity.DayEndReport;
import com.maple.restserver.report.entity.DayEndWebReport;
import com.maple.restserver.report.entity.PharmacyDayClosingReport;
import com.maple.restserver.report.entity.VoucherNumberDtlReport;
import com.maple.restserver.report.entity.WholeSaleDetailReport;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.service.DayEndReportService;
import com.maple.restserver.utils.SystemSetting;

/*
 * 	//POS MAX
			select  max( numeric_voucher_number) from sales_trans_hdr  where date(voucher_date)  = '2020-04-16' and voucher_number  not like (select  '%'||sales_prefix||'%'  from sales_type_mst ) 
			
			
			//POS MIN
			select  min( numeric_voucher_number) from sales_trans_hdr  where date(voucher_date)  = '2020-04-16' and voucher_number  not like (select  '%'||sales_prefix||'%'  from sales_type_mst ) 
			
			
			
			//WHOLESALE MAX
			select  max( numeric_voucher_number) from sales_trans_hdr  where date(voucher_date)  = '2020-04-16' and voucher_number   like (select  '%'||sales_prefix||'%'  from sales_type_mst ) 
			
			
			//WHOLESALE MIN
			select  min( numeric_voucher_number) from sales_trans_hdr  where date(voucher_date)  = '2020-04-16' and voucher_number   like (select  '%'||sales_prefix||'%'  from sales_type_mst ) 
		
		
 */
@CrossOrigin("http://localhost:4200")
@RestController
public class DayEndReportResource {



	@Autowired

	DayEndReportService dayEndReportService;

	@Autowired

	BranchMstRepository branchMstRepository;

	@GetMapping("{companymstid}/dayendreportresource/{branch}/dayendreport")
	public DayEndReport getDayEndReport(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branch") String branch, @PathVariable(value = "customerid") String customerid,
			@RequestParam("reportdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dayEndReportService.getDayEndReport(branch, date, companymstid);

	}

	@GetMapping("{companymstid}/dayendreportresource/{branch}/vouchernuberdtlreport")
	public VoucherNumberDtlReport getVoucherNumberDetailReport(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branch") String branch,
			@RequestParam("reportdate") String reportdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dayEndReportService.getVoucherNumberDetailReport(branch, date, companymstid);

	}

	@GetMapping("{companymstid}/dayendreportresource/creditsaleadjustment/{branchcode}")
	public Double getCreditSaleAdjustment(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam("reportdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dayEndReportService.getCreditSaleAdjustment(date, companymstid, branchcode);

	}

	@GetMapping("{companymstid}/dayendreportresource/othercashsale/{branchcode}")
	public Double getOtherCashSale(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam("reportdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dayEndReportService.getOtherCashSale(date, companymstid, branchcode);

	}

	@GetMapping("{companymstid}/dayendreportresource/dayendreceiptmode/{branchcode}")
	public List<DayEndProcessing> getDayEndReceptMode(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam("reportdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dayEndReportService.getDayEndReceiptMode(companymstid, branchcode, date);
	}

	@GetMapping("{companymstid}/dayendreportresource/paymentbyaccountid/{branchcode}/{accountid}")
	public Double getpaymentbyaccountid(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @PathVariable(value = "accountid") String accountid,
			@RequestParam("reportdate") String reportdate) {
		java.sql.Date date = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");

		Double pettyCashReceipt = dayEndReportService.getPaymentbyaccountId(date, companymstid, branchcode, accountid);
		return pettyCashReceipt;
	}



	@GetMapping("{companymstid}/dayendreportresource/{branch}/posvouchernumbersummary/{salesprefix}")
	public VoucherNumberDtlReport getPosVoucherNumberSummary(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branch") String branch, @PathVariable(value = "salesprefix") String salesprefix,
			@RequestParam("reportdate") String reportdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dayEndReportService.getPosVoucherNumberSummary(branch, date, companymstid, salesprefix);

	}
	//--------------------------------sharon-----------------------------------------
	@GetMapping("{companymstid}/dayendreportresource/getpharmacydayclosingreport")		
	public List<PharmacyDayClosingReport> getPharmacyDayClosingReport(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fdate") String fdate) {
		
		java.util.Date date = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		

		return dayEndReportService.findPharmacyDayClosingReport(date);
	}
	
	
	@GetMapping("{companymstid}/dayendreportresource/dayendwebreportwithtotalsalesforhc")
	public List<DayEndWebReport> getDayEndWebReportForWithTotalSalesHC(
			@PathVariable(value = "companymstid") String companymstid,
			   @RequestParam("reportdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return dayEndReportService.getDayEndWebReportForWithTotalSalesHC(date,companymstid);
		
	}
	
	
	
}
