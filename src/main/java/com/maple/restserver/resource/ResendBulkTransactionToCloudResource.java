package com.maple.restserver.resource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.service.ResendBulkTransactionToCloudService;

public class ResendBulkTransactionToCloudResource {
	
	@Autowired
	ResendBulkTransactionToCloudService resendBulkTransactionToCloudService;

	
	@GetMapping("{companymstid}/resendbulktransactiontocloudresource/resendBulkTransactionToCloud/{branchcode}")
	public void resendBulkTransactionToCloud(
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "vouchertype") String vouchertype,
			@RequestParam(value = "fdate") String fdate, 
			@RequestParam(value = "tdate") String tdate) {
		java.util.Date fromDate = ClientSystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");

		java.util.Date toDate = ClientSystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");

		  resendBulkTransactionToCloudService.resendBulkTransactionToCloud(fromDate, toDate,vouchertype);
	}
}
