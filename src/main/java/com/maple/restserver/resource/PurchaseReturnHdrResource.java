package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseReturnDtl;
import com.maple.restserver.entity.PurchaseReturnHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PurchaseReturnDtlRepository;
import com.maple.restserver.repository.PurchaseReturnHdrRepository;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.service.ItemBatchDtlServiceimpl;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class PurchaseReturnHdrResource {


	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	PurchaseReturnHdrRepository purchaseReturnHdrRepository;
	
	@Autowired
	PurchaseReturnDtlRepository purchaseReturnDtlRepository;
	
	@Autowired
	ItemBatchDtlServiceimpl itemBatchDtlServiceimpl;
	
	@GetMapping("{companymstid}/purchasereturnhdrresource/getpurchasereturnhdr/{id}")
	public List<PurchaseReturnHdr>getpurchaseReturnHdrByUserId(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "userid") String id)
	
	
{
	return purchaseReturnHdrRepository.findByUserIdAndCompanyMst(companymstid, id);
}
	

	@PostMapping("{companymstid}/purchasereturnhdrresource/savepurchasereturnhdr")
	public PurchaseReturnHdr createpurchaseReturnHdr(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody PurchaseReturnHdr purchaseReturnHdr)  {

		return companyMstRepository.findById(companymstid).map(companyMst -> {
			purchaseReturnHdr.setCompanyMst(companyMst);
			return purchaseReturnHdrRepository.saveAndFlush(purchaseReturnHdr);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}
	
	@DeleteMapping("{companymstid}/purchasereturnhdrresource/deletepurchasereturnhdr/{id}")
	public void MapleUserGroupDeleteById(@PathVariable(value = "id") String Id) {
		purchaseReturnHdrRepository.deleteById(Id);

	}
	
	

	@PutMapping("{companymstid}/purchasereturnhdrresource/{id}/purchasehdrupdate")
	public PurchaseReturnHdr updatePurchaseDtl(@PathVariable(value = "id") String id,
			@Valid @RequestBody PurchaseReturnHdr RequestPurchaseReturnHdr) {

		Optional<PurchaseReturnHdr> purchaseReturnHdrlOpt = purchaseReturnHdrRepository.findById(id);

		PurchaseReturnHdr purchaseReturnHdr = purchaseReturnHdrlOpt.get();
		purchaseReturnHdr.setReturnVoucherDate((RequestPurchaseReturnHdr.getReturnVoucherDate()));
		purchaseReturnHdr.setInvoiceTotal(RequestPurchaseReturnHdr.getInvoiceTotal());
		purchaseReturnHdr.setVoucherNumber(RequestPurchaseReturnHdr.getVoucherNumber());
		
		
		
		
	List<PurchaseReturnDtl> purchaseReturnDtl=purchaseReturnDtlRepository.findByPurchaseReturnHdr(purchaseReturnHdr);
		
//	purchaseReturnDtl.get(0).getBarcode(),purchaseReturnDtl.get(0).getBatch(),purchaseReturnHdr.getBranchCode(),
//	purchaseReturnDtl.get(0).getExpiryDate(),purchaseReturnDtl.get(0).getItemId(),purchaseReturnDtl.get(0).getMrp(),
//	"PURCHASE RETURN"+ purchaseReturnHdr.getSupplierId(),purchaseReturnHdr.getProcessInstanceId(),"",purchaseReturnDtl.get(0).getQty(),"","",
//	purchaseReturnHdr.getVoucherDate(),purchaseReturnHdr.getVoucherNumber(),"MAIN",purchaseReturnHdr.getTaskId(),
//	purchaseReturnHdr.getReturnVoucherDate(),purchaseReturnHdr.getReturnVoucherNumber(),purchaseReturnHdr.getCompanyMst()
	
	String date =RequestPurchaseReturnHdr.getReturnVoucherDate();
	
	System.out.println("Date"+date);
	java.util.Date returnVoucherDate=SystemSetting.StringToUtilDate(date, "dd-mm-yyyy");
	
	
	//.....................................Stock updation function called.................//
		   itemBatchDtlServiceimpl.itemBatchDtlInsertionNew(purchaseReturnDtl.get(0).getBarcode(),purchaseReturnDtl.get(0).getBatch(),purchaseReturnHdr.getBranchCode(),
				purchaseReturnDtl.get(0).getExpiryDate(),purchaseReturnDtl.get(0).getItemId(),purchaseReturnDtl.get(0).getMrp(),
				"PURCHASE RETURN",null,0.0,purchaseReturnDtl.get(0).getQty(),purchaseReturnDtl.get(0).getId(),purchaseReturnHdr.getId(),
				purchaseReturnHdr.getVoucherDate(),purchaseReturnHdr.getVoucherNumber(),purchaseReturnHdr.getStore(),null,
				returnVoucherDate,purchaseReturnHdr.getReturnVoucherNumber(),purchaseReturnHdr.getCompanyMst());
		   
		  Double qty = -1 * purchaseReturnDtl.get(0).getQty();
		   
		   itemBatchDtlServiceimpl.itemBatchMstUpdation(purchaseReturnDtl.get(0).getBarcode(), purchaseReturnDtl.get(0).getBatch(), 
				   purchaseReturnHdr.getBranchCode(), purchaseReturnDtl.get(0).getExpiryDate(), purchaseReturnDtl.get(0).getItemId(), 
				   purchaseReturnDtl.get(0).getMrp(), null, qty,
				  null, purchaseReturnHdr.getCompanyMst(), purchaseReturnHdr.getStore());
		
		return purchaseReturnHdrRepository.save(purchaseReturnHdr);

	}
	
}

