package com.maple.restserver.resource;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BatchPriceDefinition;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.SaleOrderEdit;
import com.maple.restserver.entity.SaleOrderReceipt;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.TaxMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.SaleOrderEditRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.repository.TaxMstRepository;
import com.maple.restserver.repository.UserMstRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class SaleOrderEditResource {
	
	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	SaleOrderEditRepository saleOrderEditRepository;

	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepository;

	@Autowired
	private SalesOrderTransHdrRepository salesOrderTransHdrRepo;

	@Autowired
	TaxMstRepository taxMstRepository;

	@Autowired
	UserMstRepository userMstRepository;

	@Autowired
	TaxMstResource taxMstResource;
	
	@Autowired
	ItemMstRepository itemMstRepository;
	
	@Autowired
	MultiUnitMstRepository multyMstRepository;
	
	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepo;

	@Autowired
	BatchPriceDefinitionResource batchPriceDefinitionResource;
	
	@Autowired
	private PriceDefinitionRepository priceDefinitionRepo;

	

	@PostMapping("{companymstid}/saleorderedit")
	public SaleOrderEdit createSalesOredrEdit(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SaleOrderEdit saleOrderEdit) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			saleOrderEdit.setCompanyMst(companyMst);

			return saleOrderEditRepository.saveAndFlush(saleOrderEdit);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	@GetMapping("{companymstid}/retrivesalesorderedit/{salesordertrandhdrid}")
	public List<SaleOrderEdit> retrieveSalesOrderReceipts(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "salesordertrandhdrid") String salesordertrandhdrid) {

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		SalesOrderTransHdr salesOrderTrandHdr = salesOrderTransHdrRepo.findByIdAndCompanyMst(salesordertrandhdrid,
				companyMst.get());
		return saleOrderEditRepository.findByCompanyMstAndSalesOrderTransHdr(companyMst.get(), salesOrderTrandHdr);
	}

	@DeleteMapping("{companymstid}/deletesaleorderedit/{id}")
	public void SaleOrderReceiptDelete(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String Id) {
		saleOrderEditRepository.deleteById(Id);

	}

	@GetMapping("{companymstid}/salesordereditamount/{salesordertranshdrid}")
	Double retrieveSalesOrderReceiptsAmount(@PathVariable(value = "companymstid") String companymstid,

			@PathVariable(value = "salesordertranshdrid") String salesordertrandhdrid) {
		return saleOrderEditRepository.retrieveSalesorderEditAmount(salesordertrandhdrid);
	}

	@GetMapping("{companymstid}/saleorderedit/saleordereditbyid/{id}")
	public Optional<SaleOrderEdit> retrieveSalesOrderEditById(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {

		return saleOrderEditRepository.findById(id);
	}

	// ------------------new version 1.10 surya ---------------------
	@PostMapping("{companymstid}/saleorderedit/{saleordertranshdrid}/savesaleorderedit")
	public SaleOrderEdit createSalesDtl(@PathVariable(value = "saleordertranshdrid") String saleordertranshdrid,
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("logdate") String logdate,
			@Valid @RequestBody SaleOrderEdit salesDtlRequest) {
		
		Date udate = SystemSetting.StringToUtilDate(logdate, "yyyy-MM-dd");

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		return salesOrderTransHdrRepo.findById(saleordertranshdrid).map(saleordertranshdr -> {
			salesDtlRequest.setCompanyMst(companyMst.get());

			SaleOrderEdit saleOrderEdit = setSaleOrderEditCalculation(salesDtlRequest,udate);

			SaleOrderEdit addedItem = saleOrderEditRepository
					.findBySalesOrderTransHdrAndItemIdAndBatchAndBarodeAndUnitIdAndRate(saleordertranshdr,
							saleOrderEdit.getItemId(), saleOrderEdit.getBatch(), saleOrderEdit.getBarode(),
							saleOrderEdit.getUnitId(), saleOrderEdit.getRate());
			if (null != addedItem && null != addedItem.getId()
					&& saleOrderEdit.getUnitId().equalsIgnoreCase(addedItem.getUnitId())) {

				addedItem.setQty(addedItem.getQty() + saleOrderEdit.getQty());
				addedItem.setCgstAmount(addedItem.getCgstAmount()+saleOrderEdit.getCgstAmount());
				addedItem.setSgstAmount(addedItem.getSgstAmount()+saleOrderEdit.getSgstAmount());
				addedItem.setCessAmount(addedItem.getCessAmount()+saleOrderEdit.getCessAmount());
				// if(salestranshdr.getCustomerMst().getCustomerDiscount() >= 0)
				// {
				addedItem.setAmount(addedItem.getQty() * (saleOrderEdit.getRate() + saleOrderEdit.getCgstAmount()
						+ saleOrderEdit.getSgstAmount()));
				// }
//				else
//				{
//				addedItem.setAmount(addedItem.getQty() * salesDtlRequest.getMrp());
//				}
				addedItem.setRate(saleOrderEdit.getRate());
				addedItem.setMrp(saleOrderEdit.getMrp());

				addedItem = saleOrderEditRepository.saveAndFlush(addedItem);

//				offerChecking(addedItem, companyMst.get());
				return addedItem;

			} else {
				salesDtlRequest.setSalesOrderTransHdr(saleordertranshdr);

				SaleOrderEdit salesDtl = saleOrderEditRepository.saveAndFlush(salesDtlRequest);
//				offerChecking(salesDtl, companyMst.get());
				return salesDtl;
			}

		}).orElseThrow(() -> new ResourceNotFoundException("salestranshdrId " + saleordertranshdrid + " not found"));

	}

	
	
	private SaleOrderEdit setSaleOrderEditCalculation(SaleOrderEdit salesDtlRequest ,Date udate ) {

		List<TaxMst> getTaxMst = taxMstRepository.getTaxByItemId(salesDtlRequest.getItemId());

		if (getTaxMst.size() > 0) {

			salesDtlRequest = calculationByTaxMst(salesDtlRequest);

		} else {

			salesDtlRequest = calculationByCustomerDiscount(salesDtlRequest);
		}
		
		
		Double taxRate = 00.0;
		Double mrpRateIncludingTax = 0.0;
		Optional<ItemMst> itemMst = itemMstRepository.findById(salesDtlRequest.getItemId());
		ItemMst item = itemMst.get();
		taxRate = item.getTaxRate();
		mrpRateIncludingTax = salesDtlRequest.getMrp();
		
		String companyState = salesDtlRequest.getCompanyMst().getState();
		String customerState = "KERALA";
		try {
			customerState = salesDtlRequest.getSalesOrderTransHdr().getAccountHeads().getCustomerState();
		} catch (Exception e) {

		}

		if (null == customerState) {
			customerState = "KERALA";
		}

		if (null == companyState) {
			companyState = "KERALA";
		}

		if (customerState.equalsIgnoreCase(companyState)) {
			
			salesDtlRequest.setSgstTaxRate(taxRate / 2);
			salesDtlRequest.setCgstTaxRate(taxRate / 2);
			Double cgstAmt = 0.0, sgstAmt = 0.0;
			cgstAmt = salesDtlRequest.getCgstTaxRate() * salesDtlRequest.getQty() * salesDtlRequest.getRate() / 100;
			BigDecimal bdcgstAmt = new BigDecimal(cgstAmt);
			bdcgstAmt = bdcgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtlRequest.setCgstAmount(bdcgstAmt.doubleValue());
			sgstAmt = salesDtlRequest.getSgstTaxRate() * salesDtlRequest.getQty() * salesDtlRequest.getRate() / 100;
			BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
			bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtlRequest.setSgstAmount(bdsgstAmt.doubleValue());
			salesDtlRequest.setIgstTaxRate(0.0);
			salesDtlRequest.setIgstAmount(0.0);

		} else {
			
			salesDtlRequest.setSgstTaxRate(0.0);
			salesDtlRequest.setCgstTaxRate(0.0);
			salesDtlRequest.setCgstAmount(0.0);
			salesDtlRequest.setSgstAmount(0.0);
			salesDtlRequest.setIgstTaxRate(taxRate);
			salesDtlRequest.setIgstAmount(salesDtlRequest.getIgstTaxRate() * salesDtlRequest.getQty() * salesDtlRequest.getRate() / 100);

		}

		BigDecimal settoamount = new BigDecimal(salesDtlRequest.getQty() * salesDtlRequest.getRate());
		double includingTax = (settoamount.doubleValue() * salesDtlRequest.getTaxRate()) / 100;
		double amount = settoamount.doubleValue() + includingTax + salesDtlRequest.getCessAmount();
		BigDecimal setamount = new BigDecimal(amount);
		setamount = setamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		salesDtlRequest.setAmount(setamount.doubleValue());
		salesDtlRequest.setAddCessRate(0.0);
		
//		PriceDefenitionMst priceDefenitionMst1 = priceDefinitionMstRepo.
//				findByCompanyMstAndPriceLevelName(salesDtlRequest.getCompanyMst(),"MRP");
//		if (null != priceDefenitionMst1) {
//			
//			BatchPriceDefinition batchpriceDef = batchPriceDefinitionResource.PriceDefinitionByItemIdAndUnitAndBatch(
//					salesDtlRequest.getItemId(), priceDefenitionMst1.getId(), salesDtlRequest.getUnitId(),
//					salesDtlRequest.getBatch(), udate);
//			if(null != batchpriceDef)
//			{
//				salesDtlRequest.setMrp(batchpriceDef.getAmount());
//			}
//			else
//			{
//				
//			PriceDefinition priceDefinition = priceDefinitionRepo.
//					findByItemIdAndStartDateCostprice(salesDtlRequest.getItemId(), priceDefenitionMst1.getId(),udate,salesDtlRequest.getUnitId());
//			if (null != priceDefinition) {
//				salesDtlRequest.setMrp(priceDefinition.getAmount());
//			}
//			}
//		}
//
//		
//		PriceDefenitionMst priceDefenitionMst = priceDefinitionMstRepo.findByCompanyMstAndPriceLevelName(
//				salesDtlRequest.getCompanyMst(),"COST PRICE");
//		
//		if (null != priceDefenitionMst) {
//			
//			PriceDefinition batchpriceDef = priceDefinitionRepo.
//					findByItemIdAndStartDateCostprice(salesDtlRequest.getItemId(), priceDefenitionMst1.getId(),udate,salesDtlRequest.getUnitId());
//			
//			if(null != batchpriceDef)
//			{
//				salesDtlRequest.setCostPrice(batchpriceDef.getAmount());
//			}
//			else
//			{
//				PriceDefinition priceDefinition = priceDefinitionRepo.
//						findByItemIdAndStartDateCostprice(salesDtlRequest.getItemId(), priceDefenitionMst1.getId(),udate,salesDtlRequest.getUnitId());
//			if (null != priceDefinition) {
//				salesDtlRequest.setCostPrice(priceDefinition.getAmount());
//			}
//			}
//		}

		return salesDtlRequest;
	}

	
	private SaleOrderEdit calculationByCustomerDiscount(SaleOrderEdit salesDtlRequest) {
		
		Double taxRate = 00.0;
		Double mrpRateIncludingTax = 0.0;

		
		Optional<ItemMst> itemMst = itemMstRepository.findById(salesDtlRequest.getItemId());
		ItemMst item = itemMst.get();
		taxRate = item.getTaxRate();
		mrpRateIncludingTax = salesDtlRequest.getMrp();
		
		SalesOrderTransHdr salesOrderTransHdr = salesDtlRequest.getSalesOrderTransHdr();

		if (salesDtlRequest.getUnitId().equalsIgnoreCase(item.getUnitId())) {
			salesDtlRequest.setStandardPrice(item.getStandardPrice());
		} else {

			MultiUnitMst multiUnitMst = multyMstRepository.
					findByCompanyMstIdAndItemIdAndUnit1(salesDtlRequest.getCompanyMst().getId(), 
							salesDtlRequest.getItemId(), salesDtlRequest.getUnitId());
			
			salesDtlRequest.setStandardPrice(multiUnitMst.getPrice());

		}
		if (null != item.getTaxRate()) {
			salesDtlRequest.setTaxRate(item.getTaxRate());
			taxRate = item.getTaxRate();

		} else {
			salesDtlRequest.setTaxRate(0.0);
		}
		Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);

		// if Discount

		// Calculate discount on base price
		if (null != salesOrderTransHdr.getAccountHeads().getDiscountProperty()) {

			if (salesOrderTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF BASE PRICE")) {
				salesDtlRequest = calcDiscountOnBasePrice(salesOrderTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate,salesDtlRequest);

			}
			if (salesOrderTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF MRP")) {
//						salesDtl.setTaxRate(0.0);
				salesDtlRequest = calcDiscountOnMRP(salesOrderTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate,salesDtlRequest);
			}
			if (salesOrderTransHdr.getAccountHeads().getDiscountProperty()
					.equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX")) {
				ambrossiaDiscount(salesOrderTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate,salesDtlRequest);
			}
		} else {

			double cessAmount = 0.0;
			double cessRate = 0.0;
			salesDtlRequest.setRate(rateBeforeTax);
			if (salesOrderTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
				if (item.getCess() > 0) {
					cessRate = item.getCess();
					rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

					salesDtlRequest.setRate(rateBeforeTax);
					cessAmount = salesDtlRequest.getQty() * salesDtlRequest.getRate() * item.getCess() / 100;
				} else {
					cessAmount = 0.0;
					cessRate = 0.0;
				}
				salesDtlRequest.setRate(rateBeforeTax);
				salesDtlRequest.setCessRate(cessRate);
				salesDtlRequest.setCessAmount(cessAmount);
			}

			// salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
			double sgstTaxRate = taxRate / 2;
			double cgstTaxRate = taxRate / 2;
			salesDtlRequest.setCgstTaxRate(cgstTaxRate);

			salesDtlRequest.setSgstTaxRate(sgstTaxRate);
			String companyState = salesDtlRequest.getCompanyMst().getState();
			String customerState = "KERALA";
			try {
				customerState = salesOrderTransHdr.getAccountHeads().getCustomerState();
			} catch (Exception e) {

			}

			if (null == customerState) {
				customerState = "KERALA";
			}

			if (null == companyState) {
				companyState = "KERALA";
			}

			if (customerState.equalsIgnoreCase(companyState)) {
				salesDtlRequest.setSgstTaxRate(taxRate / 2);

				salesDtlRequest.setCgstTaxRate(taxRate / 2);

				Double cgstAmt = 0.0, sgstAmt = 0.0;
				cgstAmt = salesDtlRequest.getCgstTaxRate() * salesDtlRequest.getQty() * salesDtlRequest.getRate() / 100;
				BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
				bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				salesDtlRequest.setCgstAmount(bdCgstAmt.doubleValue());
				sgstAmt = salesDtlRequest.getSgstTaxRate() * salesDtlRequest.getQty() * salesDtlRequest.getRate() / 100;
				BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
				bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				salesDtlRequest.setSgstAmount(bdsgstAmt.doubleValue());

				salesDtlRequest.setIgstTaxRate(0.0);
				salesDtlRequest.setIgstAmount(0.0);

			} else {
				salesDtlRequest.setSgstTaxRate(0.0);

				salesDtlRequest.setCgstTaxRate(0.0);

				salesDtlRequest.setCgstAmount(0.0);

				salesDtlRequest.setSgstAmount(0.0);

				salesDtlRequest.setIgstTaxRate(taxRate);
				salesDtlRequest.setIgstAmount(
						salesDtlRequest.getIgstTaxRate() * salesDtlRequest.getQty() * salesDtlRequest.getRate() / 100);

			}
		}
		
		return salesDtlRequest;

	}
	
	
	private SaleOrderEdit ambrossiaDiscount(SalesOrderTransHdr saleOrderTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate,SaleOrderEdit salesDtlRequest) {

		if (saleOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			double discoutAmount = (rateBeforeTax * saleOrderTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
			BigDecimal BrateAfterDiscount = new BigDecimal(discoutAmount);

			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			double newRate = rateBeforeTax - discoutAmount;
			salesDtlRequest.setDiscount(discoutAmount);
			BigDecimal BnewRate = new BigDecimal(newRate);
			BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtlRequest.setRate(BnewRate.doubleValue());
			BigDecimal BrateBeforeTax = new BigDecimal(rateBeforeTax);
			BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			// salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
//		salesOrderDtl.sets(BrateBeforeTax.doubleValue());
		} else {
			salesDtlRequest.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (saleOrderTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (saleOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + saleOrderTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtlRequest.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(rateBefrTax.doubleValue());
//					salesOrderDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesDtlRequest.setRate(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtlRequest.getQty() * salesDtlRequest.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtlRequest.setCessRate(cessRate);
		salesDtlRequest.setCessAmount(cessAmount);
		
		return salesDtlRequest;

	}
	
	private SaleOrderEdit calcDiscountOnMRP(SalesOrderTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate, SaleOrderEdit salesDtlRequest) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * mrpRateIncludingTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);

			BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtlRequest.setRate(BnewrateBeforeTax.doubleValue());
			// BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			// rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
			// salesDtl.setMrp(BrateAfterDiscount.doubleValue());
//			salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
		} else {
			salesDtlRequest.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * mrpRateIncludingTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

					BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtlRequest.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(mrpRateIncludingTax);
//					salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesDtlRequest.setRate(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtlRequest.getQty() * salesDtlRequest.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtlRequest.setCessRate(cessRate);
		salesDtlRequest.setCessAmount(cessAmount);
		
		return salesDtlRequest;

	}
	
	private SaleOrderEdit calcDiscountOnBasePrice(SalesOrderTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate,SaleOrderEdit salesDtlRequest) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * rateBeforeTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtlRequest.setRate(BrateAfterDiscount.doubleValue());
			BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			// salesDtl.setMrp(rateBefrTax.doubleValue());
//			salesOrderDtl.setStandardPrice(rateBefrTax.doubleValue());
		} else {
			salesDtlRequest.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtlRequest.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(rateBefrTax.doubleValue());
//					salesOrder/Dtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesDtlRequest.setRate(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtlRequest.getQty() * salesDtlRequest.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtlRequest.setCessRate(cessRate);
		salesDtlRequest.setCessAmount(cessAmount);
		
		return salesDtlRequest;

	}

	private SaleOrderEdit calculationByTaxMst(SaleOrderEdit salesDtlRequest) {

		List<TaxMst> getTaxMst = taxMstRepository.getTaxByItemId(salesDtlRequest.getItemId());
		SalesOrderTransHdr salesOrderTransHdr = salesDtlRequest.getSalesOrderTransHdr();
		Double mrpRateIncludingTax = salesDtlRequest.getMrp();

		for (TaxMst taxMst : getTaxMst) {

			String companyState = salesDtlRequest.getCompanyMst().getState();
			String customerState = "KERALA";
			try {
				customerState = salesOrderTransHdr.getAccountHeads().getCustomerState();
			} catch (Exception e) {

			}

			if (null == customerState) {
				customerState = "KERALA";
			}

			if (null == companyState) {
				companyState = "KERALA";
			}

			if (customerState.equalsIgnoreCase(companyState)) {
				if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
					salesDtlRequest.setCgstTaxRate(taxMst.getTaxRate());
//					BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setCgstAmount(CgstAmount.doubleValue());
				}
				if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
					salesDtlRequest.setSgstTaxRate(taxMst.getTaxRate());
//					BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setSgstAmount(SgstAmount.doubleValue());
				}
				salesDtlRequest.setIgstTaxRate(0.0);
				salesDtlRequest.setIgstAmount(0.0);
				TaxMst taxMst1 = taxMstRepository.getTaxByItemIdAndTaxId(salesDtlRequest.getItemId(), "IGST");
				if (null != taxMst1) {

					salesDtlRequest.setTaxRate(taxMst1.getTaxRate());
				}
			} else {
				if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
					salesDtlRequest.setCgstTaxRate(0.0);
					salesDtlRequest.setCgstAmount(0.0);
					salesDtlRequest.setSgstTaxRate(0.0);
					salesDtlRequest.setSgstAmount(0.0);

					salesDtlRequest.setTaxRate(taxMst.getTaxRate());
					salesDtlRequest.setIgstTaxRate(taxMst.getTaxRate());
//						BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setIgstAmount(igstAmount.doubleValue());
				}
			}
			if (salesOrderTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
				if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
					salesDtlRequest.setCessRate(taxMst.getTaxRate());
//					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setCessAmount(cessAmount.doubleValue());
				}
			}
			if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
				salesDtlRequest.setAddCessRate(taxMst.getTaxRate());
//				BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(),
//						Double.valueOf(txtRate.getText()));
//				salesDtl.setAddCessAmount(cessAmount.doubleValue());
			}

		}
		Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + salesDtlRequest.getIgstTaxRate()
				+ salesDtlRequest.getCessRate() + salesDtlRequest.getAddCessRate() + salesDtlRequest.getSgstTaxRate()
				+ salesDtlRequest.getCgstTaxRate());
		salesDtlRequest.setRate(rateBeforeTax);
		BigDecimal igstAmount = taxMstResource.taxCalculator(salesDtlRequest.getIgstTaxRate(), rateBeforeTax);
		salesDtlRequest.setIgstAmount(igstAmount.doubleValue() * salesDtlRequest.getQty());
		BigDecimal SgstAmount = taxMstResource.taxCalculator(salesDtlRequest.getSgstTaxRate(), rateBeforeTax);
		salesDtlRequest.setSgstAmount(SgstAmount.doubleValue() * salesDtlRequest.getQty());
		BigDecimal CgstAmount = taxMstResource.taxCalculator(salesDtlRequest.getCgstTaxRate(), rateBeforeTax);
		salesDtlRequest.setCgstAmount(CgstAmount.doubleValue() * salesDtlRequest.getQty());
		BigDecimal cessAmount = taxMstResource.taxCalculator(salesDtlRequest.getCessRate(), rateBeforeTax);
		salesDtlRequest.setCessAmount(cessAmount.doubleValue() * salesDtlRequest.getQty());
		BigDecimal addcessAmount = taxMstResource.taxCalculator(salesDtlRequest.getAddCessRate(), rateBeforeTax);
		salesDtlRequest.setAddCessAmount(addcessAmount.doubleValue() * salesDtlRequest.getQty());

		return salesDtlRequest;
	}

	// ------------------new version 1.10 surya ---------------------

}
