
package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductionBatchStockDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.ReqRawMaterial;
import com.maple.restserver.repository.ActualProductionDtlRepository;
import com.maple.restserver.repository.ActualProductionHdrRepository;
import com.maple.restserver.repository.CompanyMstRepository;
 
import com.maple.restserver.repository.ProductionBatchStockDtlRepository;
import com.maple.restserver.service.ProductionService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.task.ProductionFinishedGoodsStock;
import com.maple.restserver.service.task.ProductionRawMaterialStock;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ActualProductionHdrResource {
	
	private static final Logger logger = LoggerFactory.getLogger(ActualProductionHdrResource.class);

	@Value("${serverorclient}")
	private String serverorclient;
	EventBus eventBus = EventBusFactory.getEventBus();

 

	// @Autowired
	// private RuntimeService runtimeService;

	@Autowired
	private ActualProductionHdrRepository actualProductionHdrRepository;

	@Autowired
	ProductionFinishedGoodsStock productionFinishedGoodsStock;
	@Autowired
	ProductionRawMaterialStock productionRawMaterialStock;
	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	ProductionBatchStockDtlRepository productionBatchStockDtlRepository;
	@Autowired
	ActualProductionDtlRepository actualProductionDtlRepository;

	@Autowired
	ProductionService productionService;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@GetMapping("{companymstid}/actualproductionhdr/{productionhdrid}")
	public Optional<ActualProductionHdr> getActualProductionHdrById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("productionhdrid") String productionhdrid) {
		return actualProductionHdrRepository.findByIdAndCompanyMstId(productionhdrid, companymstid);

	}

	@GetMapping("{companymstid}/actualproductionhdr/getactualproductionbydate/{branchcode}")
	public Optional<ActualProductionHdr> getActualProductionHdrByVocuherDateAndVno(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return actualProductionHdrRepository.findByIdAndCompanyMstIdAndVoucherDateAndVoucherNumberIsNull(companymstid,
				date);

	}
	@GetMapping("{companymstid}/actualproductionhdr/getrawmaterialqty/{acthdrid}")
	public ArrayList getActualProductionRawMaterialQty(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value="acthdrid")String acthdrid){
		

		return productionRawMaterialStock.checkRawMaterial(acthdrid);

	}

	
	@PostMapping("{companymstid}/productionhdr")
	public ActualProductionHdr createSalesTransHdr(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody ActualProductionHdr actualProductionHdr) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			actualProductionHdr.setCompanyMst(companyMst);
//			return actualProductionHdrRepository.saveAndFlush(actualProductionHdr);
			return saveAndPublishService.saveActualProductionHdr(actualProductionHdr, actualProductionHdr.getBranchCode());

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	@PutMapping("{companymstid}/actualproductionhdr/{productionhdrid}")
	public ActualProductionHdr actualProductionFinalSave(@PathVariable String productionhdrid,
			@Valid @RequestBody ActualProductionHdr actualProductionHdrRequest) {

//			ActualProductionHdr savedSalesTransHdr = actualProductionHdrRepository
//					.saveAndFlush(actualProductionHdrRequest);
		
		ActualProductionHdr savedSalesTransHdr=saveAndPublishService.saveActualProductionHdr(actualProductionHdrRequest, actualProductionHdrRequest.getBranchCode());
		logger.info("savedSalesTransHdr send to KafkaEvent: {}", savedSalesTransHdr);
		

			productionFinishedGoodsStock.execute(savedSalesTransHdr.getVoucherNumber(),
					savedSalesTransHdr.getVoucherDate(), savedSalesTransHdr.getCompanyMst(),
					savedSalesTransHdr.getId());

			productionRawMaterialStock.execute(savedSalesTransHdr.getVoucherNumber(),
					savedSalesTransHdr.getVoucherDate(), savedSalesTransHdr.getCompanyMst(),
					savedSalesTransHdr.getId());

			Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", savedSalesTransHdr.getVoucherNumber());
			variables.put("voucherDate", savedSalesTransHdr.getVoucherDate());
			variables.put("inet", 0);
			variables.put("id", savedSalesTransHdr.getId());

			variables.put("companyid", savedSalesTransHdr.getCompanyMst());
			variables.put("branchcode", savedSalesTransHdr.getBranchCode());

			if (serverorclient.equalsIgnoreCase("REST")) {
				variables.put("REST", 1);
			} else {
				variables.put("REST", 0);
			}

			variables.put("WF", "productionprocess");

			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");

		 

			eventBus.post(variables);

			/*
			 * ProcessInstanceWithVariables pVariablesInReturn =
			 * runtimeService.createProcessInstanceByKey("productionprocess")
			 * .setVariables(variables)
			 * 
			 * .executeWithVariablesInReturn();
			 * 
			 */

			return savedSalesTransHdr;
		
	}
	@DeleteMapping("{companymstid}/actualproductiondtl/deleteactualproductiondtl/{id}")
	public void  ProductionDtlDelete(@PathVariable(value = "id") String Id) {
		actualProductionHdrRepository.deleteById(Id);
		
	}

	@Transactional
	@PutMapping("{companymstid}/actualproductionbatchwise/{productionhdrid}")
	public ActualProductionHdr actualProductionBatchWiseFinalSave(@PathVariable(value = "productionhdrid") String productionhdrid,
			@Valid @RequestBody ActualProductionHdr actualProductionHdrRequest,
			@PathVariable(value = "companymstid") String companymstid) {
		
		
		List<ProductionBatchStockDtl> productrionBatchStockDtl = new ArrayList<ProductionBatchStockDtl>();
		Optional<ActualProductionHdr> actualproductionOpt = actualProductionHdrRepository.findById(productionhdrid);

		List<ActualProductionDtl> ActualProductionDtl = actualProductionDtlRepository
				.findByActualProductionHdrId(actualproductionOpt.get().getId());

		for (ActualProductionDtl ProductionDtl : ActualProductionDtl) {
			Optional<ActualProductionDtl> actualProductionDtlOPt = actualProductionDtlRepository
					.findById(ProductionDtl.getId());
			productrionBatchStockDtl = productionBatchStockDtlRepository
					.findByActualProductionDtl(actualProductionDtlOPt.get());

		}
		if (productrionBatchStockDtl.size() > 0) {
			Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
			actualProductionHdrRequest.setCompanyMst(companyMstOpt.get());
		}

		return actualProductionHdrRepository.findById(productionhdrid).map(actualProductionHdr -> {

			actualProductionHdrRequest.setCompanyMst(actualProductionHdr.getCompanyMst());
//
//			ActualProductionHdr savedSalesTransHdrs = actualProductionHdrRepository
//					.save(actualProductionHdrRequest);
			ActualProductionHdr savedSalesTransHdrs = saveAndPublishService.saveActualProductionHdr(actualProductionHdrRequest, actualProductionHdrRequest.getBranchCode());
			logger.info("savedSalesTransHdrs send to KafkaEvent: {}", savedSalesTransHdrs);
			productionRawMaterialStock.executebatchwiserowmaterialupdate(savedSalesTransHdrs.getVoucherNumber(),
					savedSalesTransHdrs.getVoucherDate(), savedSalesTransHdrs.getCompanyMst(),
					savedSalesTransHdrs.getId());

			productionFinishedGoodsStock.executebatchwisestockupdate(savedSalesTransHdrs.getVoucherNumber(),
					savedSalesTransHdrs.getVoucherDate(), savedSalesTransHdrs.getCompanyMst(),
					savedSalesTransHdrs.getId());

		

			Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", savedSalesTransHdrs.getVoucherNumber());
			variables.put("voucherDate", savedSalesTransHdrs.getVoucherDate());
			variables.put("inet", 0);
			variables.put("id", savedSalesTransHdrs.getId());

			variables.put("companyid", savedSalesTransHdrs.getCompanyMst());
			variables.put("branchcode", savedSalesTransHdrs.getBranchCode());

			if (serverorclient.equalsIgnoreCase("REST")) {
				variables.put("REST", 1);
			} else {
				variables.put("REST", 0);
			}

			variables.put("WF", "productionprocess");

			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");

			 

			eventBus.post(variables);

			/*
			 * ProcessInstanceWithVariables pVariablesInReturn =
			 * runtimeService.createProcessInstanceByKey("productionprocess")
			 * .setVariables(variables)
			 * 
			 * .executeWithVariablesInReturn();
			 * 
			 */

			return savedSalesTransHdrs;
		}).orElseThrow(() -> new ResourceNotFoundException("productionprocess " + productionhdrid + " not found"));

	}

	
	
	
	@GetMapping("{companymstid}/actualproductionhdrresource/getrawmaterialstock/{acthdrid}")
	public List<ReqRawMaterial> getRawMaterialQty(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value="acthdrid")String acthdrid){
		

		return productionService.getRawMaterialQty(acthdrid);

	}

	
	@DeleteMapping("{companymstid}/actualproductiondtl/deleteactualproductiondtlsbyhdrid/{id}")
	public void  deleteActualProductionDtlsByHdrId(@PathVariable(value = "id") String id) {
		
		List<ActualProductionDtl> actualProductionDtlList = actualProductionDtlRepository.findByActualProductionHdrId(id);
		
		for(ActualProductionDtl actualProductionDtl : actualProductionDtlList)
		{
			actualProductionDtlRepository.deleteById(actualProductionDtl.getId());
		}
		
	}
	
	
	
}
