package com.maple.restserver.resource;

import java.net.URI;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.VisitDtl;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.VisitDtlRepository;
import com.maple.restserver.utils.EventBusFactory;

@RestController
@Transactional
public class BranchInitilizeResource {

	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	private  CompanyMstRepository  companyMstRepository;
	
	
	@Value("${serverorclient}")
	private String serverorclient;
	EventBus eventBus = EventBusFactory.getEventBus();
	//@Autowired
//	private RuntimeService runtimeService;

	@GetMapping("/{companymstid}/{branchcode}/init")
	public String initilizeBranch(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
	 
		
		/*
		 * initilize workflow to be called here -branchinit
		 */

		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("inet", 0);
		variables.put("branchcode", branchcode);
		
		variables.put("companymstid", companyMst);
		
		if (serverorclient.equalsIgnoreCase("REST")) {
			variables.put("REST", 1);
		} else {
			variables.put("REST", 0);
		}

		variables.put("WF", "forwardBranchInitialize");
		
		
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		
		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		
		
		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardBranchInitialize");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);

	/*	ProcessInstanceWithVariables pVariablesInReturn = runtimeService.createProcessInstanceByKey("branchinit")
				.setVariables(variables)

				.executeWithVariablesInReturn();
*/
		
		return "DONE";

	}

}
