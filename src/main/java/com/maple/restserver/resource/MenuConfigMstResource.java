package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MenuConfigMst;
import com.maple.restserver.entity.MenuMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.MenuConfigMstRepository;
import com.maple.restserver.repository.MenuMstRepository;
import com.maple.restserver.service.MenuConfigMstService;

@RestController
@Transactional

public class MenuConfigMstResource {

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	MenuConfigMstRepository menuConfigMstRepository;
	
	@Autowired
	MenuConfigMstService menuConfigMstService;
	
	@Autowired
	MenuMstRepository menuMstRepository;
 
	@GetMapping("{companymstid}/menuconfigmstresource/menuconfigmstbymenuname/{menuname}")
	public List<MenuConfigMst> retrieveAllMenuConfigMst(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "menuname") String menuname){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		
		if(!companyOpt.isPresent())
		{
			return null;
		}
		return menuConfigMstRepository.findByMenuNameAndCompanyMst(menuname,companyOpt.get());
	}

	@PostMapping("{companymstid}/menuconfigmstresource/savemenuconfigmst")
	public MenuConfigMst createMenuConfigMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody MenuConfigMst menuConfigMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			menuConfigMst.setCompanyMst(companyMst);

			return menuConfigMstRepository.save(menuConfigMst);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));

	}
	
	
	@GetMapping("{companymstid}/menuconfigmstresource/menuconfigmstbydescription/{description}")
	public List<MenuConfigMst> retrieveAllMenuConfigMstWithDescription(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "description") String description){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return menuConfigMstRepository.findByCompanyMstAndMenuDescription(companyOpt.get(),description);
	}
	
	@GetMapping("{companymstid}/menuconfigmstresource/menuconfigmsts")
	public List<MenuConfigMst> retrieveAllMenuConfigMst(
			@PathVariable(value = "companymstid") String companymstid){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return menuConfigMstRepository.findByCompanyMst(companyOpt.get());
	}

	@GetMapping("{companymstid}/menuconfigmstresource/menuconfigmstbyid/{id}")
	public MenuConfigMst retrieveAllMenuConfigMstId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		if(!companyOpt.isPresent())
		{
			return null;
		}
		return menuConfigMstRepository.findByCompanyMstAndId(companyOpt.get(),id);
	}
	
	@DeleteMapping("{companymstid}/menuconfigmstresource/deletemenuconfigmstbyid/{id}")
	public void DeletepurchaseDtl(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable (value="id") String id) {
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		MenuMst menuMst = menuMstRepository.findByMenuIdAndCompanyMst(id, companyOpt.get());
		if(null != menuMst)
		{
			menuMstRepository.deleteById(menuMst.getId());

		}
		
		List<MenuMst> menuByParentId = menuMstRepository.findByParentIdAndCompanyMst(id, companyOpt.get());
		if(menuByParentId.size() > 0)
		{
			for(MenuMst menu : menuByParentId)
			{
				menuMstRepository.deleteById(menu.getId());

			}

		}
			
		
		menuConfigMstRepository.deleteById(id);

	}
	
	@GetMapping("{companymstid}/menuconfigmstresource/applicationdomain")
	public String applicationDomain(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value="domaintype")String domaintype){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return menuConfigMstService.applicationDomain(companyOpt.get(),domaintype);
	}
	

	@GetMapping("{companymstid}/menuconfigsearch")
	public  @ResponseBody List<MenuConfigMst> searchMenuConfigMst(	
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("data") String searchstring){
		return menuConfigMstRepository.searchLikeMenuByName("%"+searchstring.toLowerCase()+"%",companymstid) ;
	}
	

}
