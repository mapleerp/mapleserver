package com.maple.restserver.resource;


import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ItemBatchExpiryDtl;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.ActualProductionDtlRepository;
import com.maple.restserver.repository.ActualProductionHdrRepository;
import com.maple.restserver.repository.ItemBatchExpiryDtlRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.PhysicalStockDtlRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;




@RestController
public class ActualProductionDtlResource {
	private static final Logger logger = LoggerFactory.getLogger(ActualProductionDtlResource.class);
	@Value("${mybranch}")
	private String mybranch;
 
	@Autowired
	private ActualProductionDtlRepository actualProductionDtlRepository;
	
	@Autowired
	private ActualProductionHdrRepository actualProductionHdrRepository;
	
	@Autowired
	ItemMstRepository itemMstRepo;

	EventBus eventBus = EventBusFactory.getEventBus();
	@Autowired
	private ItemBatchExpiryDtlRepository itemBatchExpiryDtlRepo;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@GetMapping("{companymstid}/actualproductionhdr/{productionhdrid}/actualproductiondtl")
	public List<ActualProductionDtl> retrieveAllSalesDtlById(@PathVariable (value = "productionhdrid") String productionhdrid,
            Pageable pageable) {
		return actualProductionDtlRepository.findByActualProductionHdrId(productionhdrid);
	}
	
	@DeleteMapping("{companymstid}/actualproductiondtl/deleteactualproduction/{actid}")
	public void deleteActualProdDtl(@PathVariable(value = "actid")String actid,
			@PathVariable(value = "companymstid")String companymstid
			)
	{
		actualProductionDtlRepository.deleteById(actid);
	}
	
	@GetMapping("{companymstid}/actualproductiondtl/{dtlid}/actualproductiondtlbyid")
	public Optional<ActualProductionDtl> retrieveAllActualProductionDtlByID(@PathVariable (value = "dtlid") String dtlid,
            Pageable pageable) {
		return actualProductionDtlRepository.findById(dtlid);
	}
	
	
	
	@GetMapping("{companymstid}/actualproductiondtlbydate")
	public List<ActualProductionDtl> retrieveAllProductionDtlByDate(
			@PathVariable (value = "companymstid") String companymstid,
			 @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		return actualProductionDtlRepository.findByActualProductionHdrVoucherDate(date);
	}
	
	@GetMapping("{companymstid}/actualproductiondtlbyDtlId/{dtlid}")
	public ActualProductionDtl retrieveAllProductionDtlByHdrId(
			@PathVariable (value = "companymstid") String companymstid,
			@PathVariable(value ="dtlid") String dtlid){
		return actualProductionDtlRepository.findByProductionDtlId(dtlid);
	}
	
	
	
	@Transactional
	@PostMapping("{companymstid}/actualproductionhdr/{productionhdrid}/actualproductiondtl")
	public ActualProductionDtl createSalesDtl(@PathVariable (value = "productionhdrid") String productionhdrid,
            @Valid @RequestBody ActualProductionDtl actualProductionDtlRequest) 
	{
		 
		
		 return actualProductionHdrRepository.findById(productionhdrid).map(actualproductionhdr -> {
			 
			 actualProductionDtlRequest.setCompanyMst(actualproductionhdr.getCompanyMst());
			 
			 actualProductionDtlRequest.setActualProductionHdr(actualproductionhdr);
			 
//			 ActualProductionDtl saved = actualProductionDtlRepository.saveAndFlush(actualProductionDtlRequest);
			 ActualProductionDtl saved =  saveAndPublishService.saveActualProductionDtl(actualProductionDtlRequest, mybranch);
			 logger.info("ActualProductionDtl send to KafkaEvent: {}", saved);
			 if(!actualProductionDtlRequest.getBatch().equalsIgnoreCase(MapleConstants.Nobatch))
			 {
				 
			 ItemBatchExpiryDtl itemBatchExpiryDtl = new ItemBatchExpiryDtl();
				List<ItemBatchExpiryDtl> items=  itemBatchExpiryDtlRepo.findByCompanyMstAndItemIdAndBatch(saved.getCompanyMst(), saved.getItemId(), saved.getBatch());
				 if(items.isEmpty())
				 {
				 itemBatchExpiryDtl.setBatch(saved.getBatch());
				 Optional<ItemMst> itemMsts = itemMstRepo.findById(saved.getItemId());
				 ItemMst itemMst = itemMsts.get();
					LocalDate due = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
					LocalDate expDate = due.plusDays(itemMst.getBestBefore());
				 itemBatchExpiryDtl.setExpiryDate(SystemSetting.localToUtilDate(expDate));
				 itemBatchExpiryDtl.setItemId(saved.getItemId());
				 itemBatchExpiryDtl.setActualProductionDtl(saved);
				 itemBatchExpiryDtl.setUpdatedDate(SystemSetting.getSystemDate());
				 itemBatchExpiryDtl.setCompanyMst(saved.getCompanyMst());
				 itemBatchExpiryDtlRepo.save(itemBatchExpiryDtl);
				 eventBus.post(itemBatchExpiryDtl);
				 }
				 else
				 {
					 itemBatchExpiryDtl.setBatch(saved.getBatch());
					 itemBatchExpiryDtl.setCompanyMst(saved.getCompanyMst());
					 itemBatchExpiryDtl.setCompanyMst(actualproductionhdr.getCompanyMst());
					 Optional<ItemMst> itemMsts = itemMstRepo.findById(saved.getItemId());
					 ItemMst itemMst = itemMsts.get();
						LocalDate due = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
						LocalDate expDate = due.plusDays(itemMst.getBestBefore());
					 itemBatchExpiryDtl.setExpiryDate(SystemSetting.localToUtilDate(expDate));
					 itemBatchExpiryDtl.setItemId(saved.getItemId());
					 itemBatchExpiryDtl.setActualProductionDtl(saved);
					 itemBatchExpiryDtl.setUpdatedDate(SystemSetting.getSystemDate());
					 itemBatchExpiryDtlRepo.UpdateItemBatchExpiryDtl(saved.getItemId(),itemBatchExpiryDtl.getExpiryDate(),saved.getBatch());
//					 itemBatchExpiryDtl = itemBatchExpiryDtlRepo.findById(saved.getId()).get();
					 eventBus.post(itemBatchExpiryDtl);
				 }
			 }
	            return saved;
	        }).orElseThrow(() -> new ResourceNotFoundException("salestranshdrId " + productionhdrid + " not found"));

		 
	}
	
	 	 
	 
		
}


