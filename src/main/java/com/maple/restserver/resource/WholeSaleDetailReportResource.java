package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.WholeSaleDetailReport;
import com.maple.restserver.service.WholeSaleDetailReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class WholeSaleDetailReportResource {
	
	@Autowired
	WholeSaleDetailReportService wholeSaleDetailReportService;
	
	@GetMapping("{companymstid}/wholesaledetailreportresource/getwholesaledetailreport")		
	public List<WholeSaleDetailReport> getWholeSaleDetailReport(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fromdate") String fDate,
			@RequestParam("todate") String tDate) {
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fDate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(tDate, "yyyy-MM-dd");

		return wholeSaleDetailReportService.findWholeSaleDetailReport(fromDate,toDate);
	}

}
