package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.service.BatchPurchaseReportServiceImpl;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class BatchPurchaseReportResource {
	
	@Autowired
	PurchaseHdrRepository puchaseHdrRepo;
	
	@Autowired
	BatchPurchaseReportServiceImpl batchPurchaseReportServiceImpl;
	

	@Autowired
	private  CompanyMstRepository  companyMstRepository;

	@GetMapping("{companymstid}/purchasehdrbybatch/{supplierid}/{branchcode}/purchasehdrbysupplier")
	public List<PurchaseHdr> getPurchaseHdrByBatch(
			@PathVariable(value = "supplierid") String suppplierid,	@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchCode") String branchCode  , @RequestParam("fromdate") String fromdate,@RequestParam("tomdate") String todate) {
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
		
		return puchaseHdrRepo.findBySupplier(suppplierid,fromDate,toDate,branchCode,companyMst);
		

	}
	

}
