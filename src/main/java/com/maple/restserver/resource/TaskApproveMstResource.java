package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.TaskApproveMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.TaskApproveMstRepository;

@RestController
public class TaskApproveMstResource {
	
	@Autowired
	private TaskApproveMstRepository taskApproveMstRepository;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	@GetMapping("{companymstid}/taskapprovemstresource/savetaskapprovemstbyid/{id}")
	public TaskApproveMst getTaskApproveMstById(
			@PathVariable(value = "companymstid") String companymstid, 
			@PathVariable(value = "id") String id) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}

		return taskApproveMstRepository.findByCompanyMstAndId(companyOpt.get(), id);
	}

	
	@GetMapping("{companymstid}/taskapprovemstresource/showalltaskapprovemst")
	public List<TaskApproveMst> getAllTaskApproveMst(
			@PathVariable(value = "companymstid") String companymstid) {
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}
		
		return taskApproveMstRepository.findByCompanyMst(companyOpt.get());
	}

	
	@DeleteMapping("{companymstid}/taskapprovemst/deletetaskapprovemst/{id}")
	public void taskApproveMst(
			@PathVariable(value = "id") String id) {
		taskApproveMstRepository.deleteById(id);
	}

	
	@PostMapping("{companymstid}/taskapprovemst/savetaskapprovemst")
	public TaskApproveMst createTaskApproveMst(@PathVariable(value = "companymstid") String
			  companymstid ,@Valid @RequestBody 
			  TaskApproveMst taskApproveMst)
	             {
		 	return companyMstRepo.findById(companymstid).map(companyMst-> {
		 		taskApproveMst.setCompanyMst(companyMst);
   		return taskApproveMstRepository.save(taskApproveMst);
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
		

}
