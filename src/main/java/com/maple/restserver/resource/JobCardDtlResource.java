package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JobCardDtl;
import com.maple.restserver.entity.JobCardHdr;
import com.maple.restserver.entity.ServiceInDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.JobCardDtlRepository;
import com.maple.restserver.repository.JobCardHdrRepository;

@RestController
@Transactional
public class JobCardDtlResource {

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	JobCardHdrRepository jobCardHdrRepo;

	@Autowired
	JobCardDtlRepository jobCardDtlRepo;

	@PostMapping("{companymstid}/jobcarddtl/{jobcardhdrid}")
	public JobCardDtl createJobCardDtl(@PathVariable(value = "jobcardhdrid") String jobcardhdrid,
			@PathVariable(value = "companymstid") String companymstid, @Valid @RequestBody JobCardDtl jobCardDtl) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		return jobCardHdrRepo.findById(jobcardhdrid).map(JobCardHdr -> {
			jobCardDtl.setJobCardHdr(JobCardHdr);
			jobCardDtl.setCompanyMst(companyOpt.get());

			if (null != jobCardDtl.getItemId()) {
				
				JobCardDtl savedJobCardDtl = jobCardDtlRepo.findByCompanyMstAndJobCardHdrAndItemId(companyOpt.get(),
						JobCardHdr, jobCardDtl.getItemId());
				if (null != savedJobCardDtl) {
					savedJobCardDtl.setQty(jobCardDtl.getQty() + savedJobCardDtl.getQty());
					savedJobCardDtl.setId(savedJobCardDtl.getId());
					savedJobCardDtl.setJobCardHdr(JobCardHdr);
					savedJobCardDtl.setCompanyMst(companyOpt.get());
					savedJobCardDtl = jobCardDtlRepo.saveAndFlush(savedJobCardDtl);
					return savedJobCardDtl;
				 }
			 return jobCardDtlRepo.saveAndFlush(jobCardDtl);
			} 
			 return jobCardDtlRepo.saveAndFlush(jobCardDtl);
		}).orElseThrow(() -> new ResourceNotFoundException("raw Material" + jobcardhdrid + " not found"));
	}

	@GetMapping("{companymstid}/jobcarddtlbyhdrid/{hdrid}")
	public List<JobCardDtl> JobCardDtlByHdrById(@PathVariable("hdrid") String hdrid,
			@PathVariable("companymstid") String companymstid)

	{

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		Optional<JobCardHdr> jobCardHdr = jobCardHdrRepo.findById(hdrid);

		return jobCardDtlRepo.findByCompanyMstAndJobCardHdr(companyOpt.get(), jobCardHdr.get());
	}

	@DeleteMapping("{companymstid}/deletejobcarddtl/{id}")
	public void JobCardDtlDelete(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String Id) {
		jobCardDtlRepo.deleteById(Id);
		jobCardDtlRepo.flush();

	}
	

	@GetMapping("{companymstid}/jobcarddtlresource/itemqty/{hdrid}/{itemid}")
	public Double JobCardDtlItemIdByHdrById(@PathVariable("hdrid") String hdrid,
			@PathVariable("itemid") String itemid,
			@PathVariable("companymstid") String companymstid)

	{

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		Optional<JobCardHdr> jobCardHdr = jobCardHdrRepo.findById(hdrid);

		Double qty = jobCardDtlRepo.JobCardDtlItemIdByHdrById(companyOpt.get(), jobCardHdr.get(),itemid);
		if(null == qty)
		{
			qty = 0.0;
		}
	
		return qty;
	}

}
