package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.HsnCodeSaleReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.HsnWiseReportService;
import com.maple.restserver.service.SalesDetailsService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@CrossOrigin("http://localhost:4200")
public class HsnWiseReportResource {

	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	HsnWiseReportService hsnWiseReportService;
	

	@Autowired
	SalesDetailsService salesDetailsService;
	@GetMapping("{companymstid}/hsnwisereportresource/hsnwisereport/{branchcode}")
	public List<HsnCodeSaleReport> getHsnWiseReport(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchCode, @RequestParam("fdate") String fromdate,
			@RequestParam("tdate") String todate) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);

		return salesDetailsService.getHSNCodeWiseSalesDtl(sdate, edate, companyMst.get(), branchCode);

	}
	
	
}
