package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.BatchWiseCustomerSalesReport;
import com.maple.restserver.service.BatchWiseCustomerSalesReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class BatchWiseCustomerSalesReportResource {

	@Autowired
	BatchWiseCustomerSalesReportService  batchWiseCustomerSalesReportService;
	
	@GetMapping("/{companymstid}/batchwisecustomerwisesalesreport/{branchcode}/{itemList}")
	public List<BatchWiseCustomerSalesReport> batchwiseDailySalesTransactionReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,	@PathVariable(value = "itemList") List<String> itemList,  
			@RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	java.util.Date TDate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		return batchWiseCustomerSalesReportService.customerwisesalesreport(companymstid,branchcode,fDate,TDate,itemList);
	}
	
	
}
