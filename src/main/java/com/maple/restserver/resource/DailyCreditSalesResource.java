package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.DailyCreditSales;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.DailyCreditSalesRepository;

@RestController
public class DailyCreditSalesResource {
	

	@Autowired
	private DailyCreditSalesRepository dailyCreditSalesRepository;
	
	@GetMapping("{companymstid}/dailycreditsales")
	public List<DailyCreditSales> retrieveAllDailyCreditSales(){
		return dailyCreditSalesRepository.findAll();
	}
	
	@PostMapping("{companymstid}/dailycreditsales")
	public DailyCreditSales createCreditSales(@Valid @RequestBody 
			DailyCreditSales dailyCreditSales)
	{
		DailyCreditSales  saved = dailyCreditSalesRepository.saveAndFlush(dailyCreditSales);
		
		 
		
		return saved;
	}
	
	

}
