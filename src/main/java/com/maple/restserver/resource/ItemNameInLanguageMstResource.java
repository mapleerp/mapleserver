package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ItemNameInLanguage;
import com.maple.restserver.entity.LanguageMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemNameInLanguageMstRepository;
import com.maple.restserver.repository.LanguageMstRepository;




@RestController
@Transactional
public class ItemNameInLanguageMstResource {
	@Autowired
	private ItemNameInLanguageMstRepository itemNameLangRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@PostMapping("{companymstid}/itemnameinlanguage")
	public ItemNameInLanguage creatItemNameInLanguage(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody ItemNameInLanguage itemNameInLanguage )
	{
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		
		itemNameInLanguage.setCompanyMst(companyMst);
			 return itemNameLangRepository.save(itemNameInLanguage);

}
	
	@GetMapping("{companyid}/itemnameinlanguages")
	public  List<ItemNameInLanguage> getItemNameInLanguage(
			@PathVariable (value = "companyid") String companyid) 
	{
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return  itemNameLangRepository.findByCompanyMst(companyMst);
		
	}
	
	@DeleteMapping("{companymstid}/deleteitemnameinlanguages/{id}")
	public void ItemNameInLanguage(@PathVariable(value = "id") String id) {
		itemNameLangRepository.deleteById(id);
		itemNameLangRepository.flush();
		 

	}
	

	
	

}
