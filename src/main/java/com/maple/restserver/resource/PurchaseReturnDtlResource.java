package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseReturnDtl;
import com.maple.restserver.entity.PurchaseReturnHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.PurchaseHdrReport;
import com.maple.restserver.report.entity.PurchaseReturnDetailAndSummaryReport;
import com.maple.restserver.report.entity.PurchaseReturnInvoice;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseReturnDtlRepository;
import com.maple.restserver.repository.PurchaseReturnHdrRepository;
import com.maple.restserver.service.PurchaseReturnService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class PurchaseReturnDtlResource {

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	PurchaseDtlRepository purchaseDtlRepository;

	@Autowired
	PurchaseReturnDtlRepository purchaseReturnDtlRepository;

	@Autowired
	PurchaseReturnHdrRepository purchaseReturnHdrRepository;

	@Autowired
	PurchaseReturnService purchaseReturnService;

	@GetMapping("{companymstid}/purchasereturndtlresource/{id}/getpurchasereturndtl")
	public List<PurchaseReturnDtl> getpurchaseReturnDtlById(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "id") String id)

	{
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		Optional<PurchaseReturnHdr> purchaseReturnHdrOpt=purchaseReturnHdrRepository.findById(id);
		return purchaseReturnDtlRepository.findByPurchaseReturnHdrAndCompanyMst(purchaseReturnHdrOpt.get(),companyMstOpt.get());

	}

	@PostMapping("{companymstid}/purchasereturndtlresource/{purchasereturnhdrid}/savepurchasereturndtl")
	public PurchaseReturnDtl createpurchaseReturnDtl(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "purchasereturnhdrid") String purchasereturnhdrid,
			 @Valid @RequestBody PurchaseReturnDtl purchaseReturnDtl) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
System.out.print(purchasereturnhdrid+"purchase return hdr id issssssssssssssssssssssssssssssssssss");
Optional<PurchaseReturnHdr> purchaseReturnHdrOpt=purchaseReturnHdrRepository.findById(purchasereturnhdrid);
		purchaseReturnDtl.setPurchaseReturnHdr(purchaseReturnHdrOpt.get());
	
			purchaseReturnDtl.setCompanyMst(companyMstOpt.get());
			return purchaseReturnDtlRepository.saveAndFlush(purchaseReturnDtl);

	}

	
	
	@DeleteMapping("{companymstid}/purchasereturndtlresource/deletepurchasereturndtl/{id}")
	public void PurchaseReturnDtlDeleteById(@PathVariable(value = "id") String Id) {
		purchaseReturnDtlRepository.deleteById(Id);

		
	}

	@GetMapping("{companymstid}/purchasereturndtlresource/getpurchasereturndtl")
	public List<PurchaseHdrReport> getPurchaseReturnDtlByVoucherDate(
			@RequestParam(value = "voucherdate") String voucherdate,
			@PathVariable(value = "companymstid") String companymstid)

	{
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		java.util.Date voucherDate = SystemSetting.StringToUtilDate(voucherdate, "yyyy-MM-dd");

		return purchaseReturnService.getPuchaseHdrByDate(companyMstOpt.get(), voucherDate);

	}

	@PutMapping("{companymstid}/purchasereturndtlresource/{id}/purchasedetailsupdate")

	public PurchaseDtl updatePurchaseDtl(@PathVariable(value = "id") String id,
			@Valid @RequestBody PurchaseDtl RequestPurchaseDtl) {

		Optional<PurchaseDtl> purchaseDtlOpt = purchaseDtlRepository.findById(id);

		PurchaseDtl purchaseDtl = purchaseDtlOpt.get();
		purchaseDtl.setQty(RequestPurchaseDtl.getQty());

		System.out.print(RequestPurchaseDtl.getQty() + "purchase return qty issssssssssssssssssssss");
		return purchaseDtlRepository.save(purchaseDtl);

	}
	
	
	@GetMapping("{companymstid}/purchasereturndtlresource/getpurchasereturndtl/{hdrid}")
	public List<PurchaseReturnDtl> getpurchaseReturnDtlByHdrId(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "hdrid") String hdrid)

	{
		
		Optional<PurchaseReturnHdr> purchaseReturnHdrOpt = purchaseReturnHdrRepository.findById(hdrid);
		return purchaseReturnDtlRepository.findByPurchaseReturnHdr(purchaseReturnHdrOpt.get());

	}
	

	@GetMapping("{companymstid}/purchasereturndetailresource/getpurchasereturndetailandsummary")
	public List<PurchaseReturnDetailAndSummaryReport> getPurchaseReturnDetailAndSummary(
			@RequestParam(value = "fromdate") String fromdate,
			@RequestParam(value = "todate") String todate,
			@PathVariable(value = "companymstid") String companymstid)

	{
		java.util.Date fdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date tdate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");

		return purchaseReturnService.getPurchaseReturnDetailAndSummary(fdate,tdate);

	}
	
	@GetMapping("{companymstid}/purchasereturndtlresource/getpurchasereturninvoicebyhdrid/{purchasehdrid}")
	public List<PurchaseReturnInvoice> getpurchaseReturnInvoiceById(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "purchasehdrid") String purchasehdrid)

	{
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		Optional<PurchaseReturnHdr> purchaseReturnHdrOpt=purchaseReturnHdrRepository.findById(purchasehdrid);
		return purchaseReturnService.getpurchaseReturnInvoiceById(purchasehdrid,companyMstOpt.get());

	}
	
	@GetMapping("{companymstid}/purchasereturndtlresource/getpurchasereturnqtydetails/{purchasedtlId}")
	public Double getpurchaseReturnDtlQty(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "purchasedtlId") String purchasedtlId)

	{
		
		
		return purchaseReturnService.getpurchaseReturnDtlQty(purchasedtlId);

	}
	
	
	

}
