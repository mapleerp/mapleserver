
package com.maple.restserver.resource;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.ClosingBalance;
import com.maple.restserver.repository.ClosingBalanceRepository;




@RestController
@Transactional
public class ClosingBalanceResource {
	@Autowired
	private ClosingBalanceRepository closingbalances;
	@GetMapping("/{companymstid}/closingbalance")
	public List<ClosingBalance> retrieveAllClosingBalance(@PathVariable(value = "companymstid") String
			  companymstid)
	{
		return closingbalances.findByCompanyMstId(companymstid);
	}
	@PostMapping("{companymstid}/closingbalance")
	public ResponseEntity<Object> createUser(@Valid @RequestBody ClosingBalance closingbalances1)
	{
		ClosingBalance saved=closingbalances.saveAndFlush(closingbalances1);
	URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	}

}


