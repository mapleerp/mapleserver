package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.InvoiceFormatMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.InvoiceFormatMstRepository;
import com.maple.restserver.service.InvoiceFormatMstService;
@RestController
@Transactional

public class InvoiceFormatMstResource {
	
	
	@Autowired
	private InvoiceFormatMstRepository jasperByPriceTypeRepository;
	
	@Autowired
	private InvoiceFormatMstService jasperReportSelectionService;
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	
	@GetMapping("{companymstid}/getalljasperbypricetype")
	public List<InvoiceFormatMst> retrieveAllJasperByPriceType(
			@PathVariable(value = "companymstid") String
			  companymstid){
		
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		return jasperByPriceTypeRepository.findByCompanyMst(companyMst.get());
	}
	
	

	@GetMapping("{companymstid}/getjasperbypricetypewithpricetypeid/{pricetypeid}")
	public List<InvoiceFormatMst> retrieveAllJasperByPriceTypeWithPriceId(
			@PathVariable(value = "companymstid") String  companymstid,
			@PathVariable(value = "pricetypeid") String  pricetypeid){
		
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		return jasperByPriceTypeRepository.findByCompanyMstAndPriceTypeId(companyMst.get(),pricetypeid);
	}
	  
	
	
	@PostMapping("{companymstid}/savejasperbypricetype")
	public InvoiceFormatMst createJasperByPriceType(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody  InvoiceFormatMst jasperByPriceType)
	{
		
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

			jasperByPriceType.setCompanyMst(companyMst.get());
		
		
		return jasperByPriceTypeRepository.saveAndFlush(jasperByPriceType);
	
	
	}
	
	@DeleteMapping("{companymstid}/deletejasperbypricetype/{id}")
	public void salesDtlDelete(
			@PathVariable(value = "companymstid") String  companymstid,
			@PathVariable(value = "id") String  id) {
		
		jasperByPriceTypeRepository.deleteById(id);
		
	}
	
	
	@GetMapping("{companymstid}/invoiceformatmst/getjasperbyreportname")
	public List<InvoiceFormatMst> retrieveJasperByReport(
			@PathVariable(value = "companymstid") String  companymstid,
			@RequestParam(value = "pricetypeid") String  pricetypeid,
			@RequestParam(value = "gst") String  gst,
			@RequestParam(value = "batch") String  batch,
			@RequestParam(value = "discount") String  discount,
			@RequestParam(value = "custid") String  custid){
		
		
		
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		
		/*
		 * modified due to change the customer mst into account heads========07/01/2022
		 */
//		CustomerMst customerMst = customerMstRepo.findById(custid).get();
		AccountHeads accountHeads = accountHeadsRepository.findById(custid).get();
		if(null != accountHeads.getTaxInvoiceFormat())
		{
			return jasperByPriceTypeRepository.findByReportName(accountHeads.getTaxInvoiceFormat());
		}
	
		else
		{

		return jasperReportSelectionService.
				findByCompanyMstAndPriceTypeIdAndGstAndDiscount(companyMst.get(),pricetypeid,gst,discount,batch);
	}
}
	

}
