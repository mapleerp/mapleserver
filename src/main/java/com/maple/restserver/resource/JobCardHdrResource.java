package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JobCardHdr;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ServiceInDtl;
import com.maple.restserver.entity.ServiceInHdr;
import com.maple.restserver.report.entity.ActiveJobCardReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.JobCardHdrRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ServiceInDtlRepository;
import com.maple.restserver.service.JobCardService;
import com.maple.restserver.utils.EventBusFactory;

@RestController
@Transactional
public class JobCardHdrResource {

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository; 
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	
	@Autowired
	JobCardService jobCardService;
	
	@Autowired
	CompanyMstRepository companyMstRepo;


	@Autowired
	JobCardHdrRepository jobCardHdrRepo;

	@Autowired
	ServiceInDtlRepository serviceInDtlRepository;

	@PostMapping("{companymstid}/jobcardhdr")
	public JobCardHdr createJobCardHdr(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody JobCardHdr jobCardHdr) {
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);

		if (!comapnyMstOpt.isPresent()) {
			return null;
		}
		CompanyMst companyMst = comapnyMstOpt.get();

		Optional<ServiceInDtl> serviceInDtlOpt = serviceInDtlRepository.findById(jobCardHdr.getServiceInDtl().getId());
		if (!serviceInDtlOpt.isPresent()) {
			return null;
		}
		jobCardHdr.setServiceInDtl(serviceInDtlOpt.get());
		jobCardHdr.setCompanyMst(companyMst);
		JobCardHdr jobCardHdrSaved = jobCardHdrRepo.saveAndFlush(jobCardHdr);

		return jobCardHdrSaved;
	}

	@GetMapping("{companymstid}/jobcardhdr/{hdrid}")
	public JobCardHdr getJobCardHdrById(@PathVariable("hdrid") String hdrid,
			@PathVariable("companymstid") String companymstid)

	{
		return jobCardHdrRepo.findById(hdrid).get();
	}

	@PutMapping("{companymstid}/jobcardhdr/{jobcardid}/updatejobcardhdr")
	public JobCardHdr updateJobCardHdr(@PathVariable(value = "jobcardid") String jobcardid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody JobCardHdr jobCardHdrRequest) {

		JobCardHdr jobCardHdr = jobCardHdrRepo.findById(jobcardid).get();

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		jobCardHdr.setCompanyMst(companyMst);
		jobCardHdr.setCustomerId(jobCardHdrRequest.getCustomerId());
		jobCardHdr.setVoucherDate(jobCardHdrRequest.getVoucherDate());
		jobCardHdr.setVoucherNumber(jobCardHdrRequest.getVoucherNumber());
		jobCardHdr.setStatus(jobCardHdrRequest.getStatus());
		jobCardHdr.setSalesTransHdrId(jobCardHdrRequest.getSalesTransHdrId());
		jobCardHdr = jobCardHdrRepo.saveAndFlush(jobCardHdr);
		
		if(jobCardHdr.getStatus().equalsIgnoreCase("CLOSED"))
		{
	
		
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", jobCardHdr.getVoucherNumber());
		variables.put("voucherDate", jobCardHdr.getVoucherDate());
		variables.put("id", jobCardHdr.getId());
		variables.put("companyid", jobCardHdr.getCompanyMst());
		variables.put("branchcode", jobCardHdr.getBranchCode());

		variables.put("inet", 0);
		variables.put("REST", 1);

		variables.put("WF", "forwardJobCard");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));

		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardJobCardOut");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);

		}
		return jobCardHdr;
	}
	
	@GetMapping("{companymstid}/activejobcardhdrs")
	public List<JobCardHdr> getJobCardHdrByCompanyAndStatus(
			@PathVariable("companymstid") String companymstid)

	{
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return jobCardHdrRepo.findByCompanyMstAndStatus(companyOpt.get());
	}
	
	@GetMapping("{companymstid}/jobcardhdrresource/jobcardhdrbetweendate")
	public List<JobCardHdr> getJobCardHdrByCompanyBetweenDate(
			@PathVariable("companymstid") String companymstid,
			@RequestParam("fdate") String fdate,
			@RequestParam("tdate") String tdate)

	{
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		
		Date sdate = ClientSystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		Date edate = ClientSystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");

		
		return jobCardHdrRepo.findByCompanyMstBetweenDate(companyOpt.get(),sdate,edate);
	}
	
	@GetMapping("{companymstid}/jobcardhdrresource/activejobcardreport")
	public List<ActiveJobCardReport> getActiveJobCardHdrReport(
			@PathVariable("companymstid") String companymstid,
			@RequestParam("fdate") String fdate,
			@RequestParam("tdate") String tdate)

	{
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		Date sdate = ClientSystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		Date edate = ClientSystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");

		
		return jobCardService.findAllActiveJobCardReport(companyOpt.get(),sdate,edate);
	}
	
	
	@GetMapping("{companymstid}/jobcardhdrresource/invoiceamount/{hdrid}")
	public Double getJobCardInvoiceAmount(
			@PathVariable("companymstid") String companymstid,
			@PathVariable("hdrid") String hdrid)

	{
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		
		Optional<JobCardHdr> jobCardHdr = jobCardHdrRepo.findById(hdrid);
		
		Double amount =  jobCardHdrRepo.findAllActiveJobCardReport(companyOpt.get(),jobCardHdr.get());
	
		if(null == amount)
		{
			amount = 0.0;
		}
		return amount;
	}
	
}
