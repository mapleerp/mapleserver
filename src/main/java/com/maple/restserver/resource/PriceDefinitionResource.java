package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AccountPayable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ItemTempMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PaymentInvoiceDtl;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.his.entity.PriceDefinitionReport;
import com.maple.restserver.pharmacy.service.PriceDefinitionService;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class PriceDefinitionResource {
	private static final Logger logger = LoggerFactory.getLogger(PriceDefinitionResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	

	EventBus eventBus = EventBusFactory.getEventBus();
	
	@Autowired
	ItemMstRepository itemMstRepository;
	

	@Autowired
	PriceDefinitionService priceDefinitionService;
	
	@Autowired
	UnitMstRepository unitMstRepository;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepository;

	@Autowired
	private PriceDefinitionRepository priceDefinitionRepo;

	@Autowired
	private CompanyMstRepository companyMstRepository;
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@GetMapping("{companymstid}/pricedefinitions")
	public List<PriceDefinition> getAllPriceDefinition() {
		return priceDefinitionRepo.findAll();
	}

	@PostMapping("{companymstid}/pricedefinition")
	public PriceDefinition createPriceDefinition(@Valid @RequestBody PriceDefinition pricedefinition,
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		pricedefinition.setCompanyMst(companyMst);
		if (null != pricedefinition.getUnitId()) {
			priceDefinitionRepo.UpdatePriceDefinitionbyItemIdUnitId(pricedefinition.getItemId(),
					pricedefinition.getStartDate(), pricedefinition.getPriceId(), pricedefinition.getUnitId());
		} else {
			priceDefinitionRepo.UpdatePriceDefinitionbyItemId(pricedefinition.getItemId(),
					pricedefinition.getStartDate(), pricedefinition.getPriceId());

		}
//		PriceDefinition saved = priceDefinitionRepo.saveAndFlush(pricedefinition);
		PriceDefinition saved =saveAndPublishService.savePriceDefinition(pricedefinition, pricedefinition.getBranchCode());
		logger.info("PriceDefinition send to KafkaEvent: {}", saved);
		
		
		

		return saved;
	}

	@PutMapping("{companymstid}/pricedefinition/{pricedefinitionId}")
	public PriceDefinition updatePricedefinition(@PathVariable String pricedefinitionId,
			@Valid @RequestBody PriceDefinition pricedefinitionRequest) {

		return priceDefinitionRepo.findById(pricedefinitionId).map(pricedefinition -> {
			pricedefinition.setItemId(pricedefinitionRequest.getItemId());
			pricedefinition.setAmount(pricedefinitionRequest.getAmount());
			pricedefinition.setPriceId(pricedefinitionRequest.getPriceId());
			pricedefinition.setStartDate(pricedefinitionRequest.getStartDate());
			pricedefinition.setEndDate(pricedefinitionRequest.getEndDate());
			//return priceDefinitionRepo.saveAndFlush(pricedefinition);
			return saveAndPublishService.savePriceDefinition(pricedefinition, pricedefinition.getBranchCode());
		}).orElseThrow(() -> new ResourceNotFoundException("pricedefinitionId " + pricedefinitionId + " not found"));

	}

	@GetMapping("{companymstid}/pricedefinitionbyitemid/{itemid}")
	public @ResponseBody List<PriceDefinition> getPriceDefinitionbyItemid(
			@PathVariable(value = "itemid") String itemid) {
		return priceDefinitionRepo.findByItemId(itemid);
	}

	@GetMapping("{companymstid}/pricedefinition/{itemid}/{priceId}/pricedefinitionbyitem")
	public List<PriceDefinition> retrieveAllPriceDefinitionByItemId(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "priceId") String priceId) {

		return priceDefinitionRepo.findByItemIdAndStartDate(itemid, priceId);
	}
	
	@GetMapping("{companymstid}/pricedefinition/{itemid}/{priceId}/{unitId}/pricedefinitionbyitemcostprice")
	public PriceDefinition retrieveAllPriceDefinitionByItemIdCostPrice(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "priceId") String priceId,
			@PathVariable(value = "unitId") String unitId,
			@RequestParam(value = "sdate")String sdate) {
		java.sql.Date stdate = SystemSetting.StringToSqlDate(sdate, "yyyy-MM-dd");
if( priceDefinitionRepo.findByItemIdAndStartDateCostpriceList(itemid, priceId,stdate,unitId).isEmpty()) {
	return null;
}else {
	return priceDefinitionRepo.findByItemIdAndStartDateCostpriceList(itemid, priceId,stdate,unitId).get(0);
}
		
	}

	@GetMapping("{companymstid}/pricedefinition/{itemid}/{priceId}/{unitId}/pricedefinitionbyitemandunit")
	public PriceDefinition retrieveAllPriceDefinitionByItemIdAndUnit(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "priceId") String priceId, 
			@PathVariable(value = "unitId") String unitId,
			@RequestParam(value ="tdate")String tdate) {

		Date udate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
		List<PriceDefinition> priceList = priceDefinitionRepo.findByItemIdAndUnitId(itemid, priceId, unitId,udate);
		if(priceList.size()>0)
		{
			return priceList.get(0);
		}
		else
		{
			List<PriceDefinition> priceList1 = priceDefinitionRepo.findByItemIdAndUnitIdEnddateNull(itemid, priceId, unitId,udate);
			if(priceList1.size()>0)
			{
				return priceList1.get(0);
			}
		}
		return null;
	}

	@GetMapping("{companymstid}/pricedefinitionbypricetype/{pricetype}")
	public @ResponseBody List<PriceDefinition> getPriceDefinitionbyPriceType(
			@PathVariable(value = "pricetype") String pricetype) {
		return priceDefinitionRepo.findByPriceId(pricetype);
	}

	@GetMapping("{companymstid}/pricedefinitionbystartdate/{startdate}")
	public @ResponseBody List<PriceDefinition> getPriceDefinitionbyStartDate(
			@PathVariable(value = "startdate") String startdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(startdate, "dd-MM-yyyy");

		return priceDefinitionRepo.findByStartDate(date);
	}

	@GetMapping("/{companymstid}/pricedefinitionbyenddate/{enddate}")
	public @ResponseBody List<PriceDefinition> getPriceDefinitionbyendDate(
			@PathVariable(value = "enddate") String enddate) {

		java.util.Date date = SystemSetting.StringToUtilDate(enddate, "dd-MM-yyyy");

		return priceDefinitionRepo.findByEndDate(date);
	}

	@DeleteMapping("/{companymstid}/pricedefinition/{id}")
	public void pricedefinitionDelete(@PathVariable(value = "id") String Id) {
		priceDefinitionRepo.deleteById(Id);

	}

	@GetMapping("/{companymstid}/getpricdefiniton")

	public @ResponseBody List<PriceDefinition> getPriceDefinition() {

		return priceDefinitionRepo.fetchPriceDefinition();
	}

	@GetMapping("/{companymstid}/getprice/{itemid}/{priceid}")

	public @ResponseBody Double getPrice(@PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "priceid") String priceid) {

		return priceDefinitionRepo.FetchAmountBy(itemid, priceid);
	}
	
	
	@GetMapping("/{companymstid}/getpricedefrnition/{itemid}/{priceid}/{unitid}")
	public PriceDefinition getPriceDefenition(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "priceid") String priceid,
			@PathVariable(value = "unitid") String unitid) {

		return priceDefinitionRepo.findPriceDefenitionByItemUnitPriceAndDate(itemid, priceid,unitid,companymstid);
	}
	
	
	
	
	
//	String path = "http://" + MapleConstants.HOSTANDCOMAPNY + "/savepricedefinition/web/" + 
//			selectedItem+"/"+unit+"/"+priceType+"/"+amount+"/"+startDate2;
	
	
	
	
	
	@GetMapping("{companymstid}/savepricedefinition/web/{itemname}/{unit}/{pricetype}/{amount}")
	public PriceDefinition createPriceDefinitionOnWeb(@PathVariable(
			value = "companymstid") String companymstid,	
			@PathVariable(value = "itemname") String itemname,
			@PathVariable(value = "unit") String unit,
			@PathVariable(value = "pricetype") String pricetype,
			@PathVariable(value = "amount") Double amount,
			@RequestParam("date") String date
			)
	{		
		
		java.sql.Date uDate = SystemSetting.StringToSqlDate(date, "yyyy-MM-dd");
		itemname = itemname.replaceAll("%20", " ");
		pricetype = pricetype.replaceAll("%20", " ");
		unit = unit.replaceAll("%20", " ");
		
		 Optional<CompanyMst> companyMstOPt=  companyMstRepository.findById(companymstid);
			 
			Optional<ItemMst> itemOpt = itemMstRepository.findByItemNameAndCompanyMst(itemname, companyMstOPt.get());
			ItemMst itemMst = itemOpt.get();
			if(null == itemMst)
			{
				return null;
			}
			
			
			UnitMst unitResp = unitMstRepository.findByUnitNameAndCompanyMstId(unit,companymstid);
			if(null == unitResp)
			{
				return null;
			}
			
			
			Optional<PriceDefenitionMst> priceTypeOpt = priceDefinitionMstRepository.findByPriceLevelNameAndCompanyMst(pricetype,companyMstOPt.get());
			PriceDefenitionMst priceDefenitionMst = priceTypeOpt.get();
			if(null == priceDefenitionMst)
			{
				return null;
			}
			
			//=====================remains
			PriceDefinition priceDefinition = new PriceDefinition();
			priceDefinition.setStartDate(uDate);
			priceDefinition.setCompanyMst(companyMstOPt.get());
			priceDefinition.setItemId(itemOpt.get().getId());
			priceDefinition.setUnitId(unitResp.getId());
			priceDefinition.setPriceId(priceDefenitionMst.getId());
			priceDefinition.setAmount(amount);
		
			
//	 	return priceDefinitionRepo.saveAndFlush(priceDefinition);
			return saveAndPublishService.savePriceDefinition(priceDefinition, priceDefinition.getBranchCode());
		
	}
	
	
	@GetMapping("{companymstid}/getpricedefinition/web")
	public List<PriceDefinitionReport> getAllPriceDefinition(@PathVariable(
			value = "companymstid") String companymstid
			)
	{		
		
		 Optional<CompanyMst> companyMstOPt=  companyMstRepository.findById(companymstid);
			 
		
			
	 	return priceDefinitionService.getPriceDefinitions(companyMstOPt.get());
		
	}
	
//----------------------version 6.12 surya
	@GetMapping("{companymstid}/exceptionitems")
	public List<ItemMst> getAllExceptionItems(@PathVariable(
			value = "companymstid") String companymstid
			)
	{		
		
		 Optional<CompanyMst> companyMstOPt=  companyMstRepository.findById(companymstid);
			 
		
			
	 	return priceDefinitionService.getAllExceptionItems(companyMstOPt.get());
		
	}
	
	
	@PostMapping("{companymstid}/saveexceptionpricedefinition")
	public PriceDefinition createExceptionPriceDefinition(@Valid @RequestBody PriceDefinition pricedefinition,
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		pricedefinition.setCompanyMst(companyMst);
		if (null != pricedefinition.getUnitId()) {
			priceDefinitionRepo.UpdatePriceDefinitionbyItemIdUnitId(pricedefinition.getItemId(),
					pricedefinition.getStartDate(), pricedefinition.getPriceId(), pricedefinition.getUnitId());
		} else {
			priceDefinitionRepo.UpdatePriceDefinitionbyItemId(pricedefinition.getItemId(),
					pricedefinition.getStartDate(), pricedefinition.getPriceId());

		}
//		PriceDefinition saved = priceDefinitionRepo.saveAndFlush(pricedefinition);
		
		PriceDefinition saved =saveAndPublishService.savePriceDefinition(pricedefinition,pricedefinition.getBranchCode());
		logger.info("PriceDefinition send to KafkaEvent: {}", saved);
		priceDefinitionService.UpdateCostPrice(saved);

		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", saved.getId());

		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", saved.getId());

		variables.put("companyid", saved.getCompanyMst());
		variables.put("branchcode", saved.getBranchCode());

		variables.put("REST", 1);

		variables.put("WF", "forwardPriceDefinition");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		
		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardPriceDefinition");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);

		variables.put("lmsqid", lmsQueueMst.getId());

		eventBus.post(variables);

		return saved;
	}
	
	//----------------------version 6.12 surya end
	
	//-------------------version 1.0 new surya
	
	@GetMapping("{companymstid}/pricedefinitionbycategoryid/{categoryid}")
	public @ResponseBody List<PriceDefinition> getPriceDefinitionbyCategoryId(
			@PathVariable(value = "categoryid") String categoryid,
			@PathVariable(value = "companymstid") String companymstid) {
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();		
		
		return priceDefinitionRepo.getPriceDefinitionByCategoryId(categoryid,companyMst);
	}
	//-------------------version 1.0 new surya end


}
