package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.ProductionDtlsReport;
import com.maple.restserver.service.ProductionReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ProductionReportResource {

	@Autowired
	ProductionReportService productionReportService;
	
	
	@GetMapping("{companymstid}/productiondtlreport/{branchcode}/{vouchernumber}")
	public List<ProductionDtlsReport>  getProductionDtlReport(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "branchcode") String
			  branchcode,@PathVariable(value = "vouchernumber") String
			  vouchernumber,
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"dd/MM/yyyy");
		

		return productionReportService.getProductionDtlReport(date,branchcode,vouchernumber);
	
	}	

	
	

	@GetMapping("{companymstid}/productiondtlreports/{branchcode}")
	public List<ProductionDtlsReport>  fetchProductionDtlReport(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "branchcode") String
			  branchcode,
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		
		return productionReportService.fetchProductionDtlReport(branchcode,date);
	
	}	
	

	@GetMapping("{companymstid}/productiondtlreports/fetchsaleorderitem/{branchcode}")
	public List<ProductionDtlsReport>  fetchSaleOrderItem(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "branchcode") String
			  branchcode,
			  @RequestParam("rdate") String reportdate,  @RequestParam("resCatId") String resCatId){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		
		return productionReportService.fetchSaleOrderItem(branchcode,date,resCatId);
	
	}	
}
