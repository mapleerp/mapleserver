package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.InsuranceWiseSalesReport;
import com.maple.restserver.service.InsuranceWiseSaleReportService;
import com.maple.restserver.utils.SystemSetting;



@RestController
@Transactional
public class InsuranceWiseSalesReportResource {
	
	@Autowired
	InsuranceWiseSaleReportService insuranceWiseSaleReportService;
	

	
	
	@GetMapping("{companymstid}/insurancewisesalesreportresource/getinsurancewisesalesreportresource")
	public List<InsuranceWiseSalesReport> getinsurancewisesalesreportresource(
		
					@PathVariable(value = "companymstid") String companymstid,
					@RequestParam("fromdate") String fromDate,
					@RequestParam("todate") String toDate) {
				
				java.util.Date sdate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
				java.util.Date edate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
				System.out.println("insurancewisesalesreportresource/getinsurancewisesalesreportresource");

				return insuranceWiseSaleReportService.getInsurancewisesalesreport(sdate,edate);
	}

}
