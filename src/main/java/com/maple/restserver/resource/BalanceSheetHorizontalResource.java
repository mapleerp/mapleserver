package com.maple.restserver.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BalanceSheetHorizontal;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.BalanceSheetHorizontalRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
public class BalanceSheetHorizontalResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	BalanceSheetHorizontalRepository balanceSheetHorizontalRepo;
	
	@PostMapping("/{companymstid}/balancesheethorizontal/savebalancesheethorizontal")
	public BalanceSheetHorizontal createBalanceSheetHorizontal(@PathVariable(value = "companymstid") String companymstid,@Valid @RequestBody 
			BalanceSheetHorizontal balanceSheetHorizontal)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			balanceSheetHorizontal.setCompanyMst(companyMst);
			BalanceSheetHorizontal saved = balanceSheetHorizontalRepo.save(balanceSheetHorizontal);
			return saved;
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found"));
		
	}
	
}
