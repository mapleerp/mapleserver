package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemPropertyConfig;
import com.maple.restserver.entity.ItemPropertyInstance;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.GoodReceiveNoteDtlRepository;
import com.maple.restserver.repository.ItemPropertyInstanceRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.service.ItemPropertyInstanceService;

@RestController
public class ItemPropertyInstanceResource {

	
	@Autowired
	ItemPropertyInstanceRepository ItemPropertyInstanceRepository;
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	PurchaseDtlRepository purchaseDtlRepo;

	@Autowired
	ItemPropertyInstanceService itemPropertyInstanceService;
	@PostMapping("/{companymstid}/itempropertyinstanceresource/createitempropertyinstance/{purchasedtlid}")
	public ItemPropertyInstance createItemPropertyInstance(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  ItemPropertyInstance itemPropertyInstance,@PathVariable(value = "purchasedtlid") String
			  purchasedtlid)
	{
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		 		itemPropertyInstance.setCompanyMst(comapnyMstOpt.get());
		 	Optional<PurchaseDtl> purchaseDtlOpt=purchaseDtlRepo.findById(purchasedtlid);
		 	itemPropertyInstance.setPurchaseDtl(purchaseDtlOpt.get());
		 	
		 		ItemPropertyInstance saveditemPropertyInstance = ItemPropertyInstanceRepository.save(itemPropertyInstance);
		 		
		 		
					return saveditemPropertyInstance;
		 		
		 	
		
	}
		 

	@GetMapping("/{companymstid}/itempropertyinstanceresource/findallitempropertyinstance")
	public List<ItemPropertyInstance> retrieveAllItemPropertyInstance(@PathVariable(value = "companymstid") String companymstid){
		
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		return ItemPropertyInstanceRepository.findByCompanyMst(companyMstOpt.get());
	}
	@GetMapping("/{companymstid}/itempropertyinstanceresource/getitempropertyinstancebypurchasedtl/{branchcode}/{purchasedtl}")
	public List<ItemPropertyInstance> getItemPropertyInstancebyItemIdandPurchaseDtl(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode")String branchcode,
			@PathVariable(value = "purchasedtl")String purchasedtl
			){
		PurchaseDtl purchaseDtl = purchaseDtlRepo.findById(purchasedtl).get();
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		return ItemPropertyInstanceRepository.findByCompanyMstAndBranchCodeAndPurchaseDtl(companyMstOpt.get(),branchcode,purchaseDtl);
	}
	
	@GetMapping("/{companymstid}/itempropertyinstanceresource/checkforallitempropertyinstance/{branchcode}/{itemid}/{purchasedtl}")
	public Boolean checkforItemPropertyInstanceByBranchCodeAndItemIdAndPurchaseDtlId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode")String branchcode,
			@PathVariable(value = "itemid")String itemid,
			@PathVariable(value = "purchasedtl")String purchasedtl
			){
		PurchaseDtl purchaseDtl = purchaseDtlRepo.findById(purchasedtl).get();
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		return itemPropertyInstanceService.checkForItemPropertyInstance(itemid,purchasedtl,companyMstOpt.get(),branchcode);
	}
	
	@GetMapping("/{companymstid}/itempropertyinstanceresource/getitempropertyinstancebypropertyandpurchasedtlid/{branchcode}/{purchasedtl}/{property}")
	public ItemPropertyInstance getItemPropertyInstanceByBranchCodeAndPropertyAndPurchaseDtlId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode")String branchcode,
			@PathVariable(value = "property")String property,
			@PathVariable(value = "purchasedtl")String purchasedtl
			){
		PurchaseDtl purchaseDtl = purchaseDtlRepo.findById(purchasedtl).get();
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		return ItemPropertyInstanceRepository.findByBranchCodeAndCompanyMstAndPropertyNameAndPurchaseDtl(branchcode,companyMstOpt.get(),property,purchaseDtl);
	}

	@PutMapping("{companymstid}/itempropertyinstanceresource/{id}/updateitempropertyinstance")
	public ItemPropertyInstance updateItemPropertyConfig(
			@PathVariable(value="id") String id, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody ItemPropertyInstance  itemPropertyInstanceRequest)
	{
			
		ItemPropertyInstance itemPropertyInstance = ItemPropertyInstanceRepository.findById(id).get();
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		itemPropertyInstance.setCompanyMst(companyMst);
		itemPropertyInstance.setItemId(itemPropertyInstanceRequest.getItemId());
		itemPropertyInstance.setBranchCode(itemPropertyInstance.getBranchCode());
		itemPropertyInstance.setPropertyName(itemPropertyInstance.getPropertyName());
		itemPropertyInstance.setPropertyValue(itemPropertyInstance.getPropertyValue());
		return itemPropertyInstance;
	}
	
	@DeleteMapping("{companymstid}/itempropertyinstanceresource/itempropertyinstanceresourcedelete/{id}")
	public void deleteItemPropertyInstance(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "id") String
			  id)
	{
		ItemPropertyInstanceRepository.deleteById(id);
	}
	//------------------good receive note---------------------------
	/*
	 * @GetMapping(
	 * "/{companymstid}/itempropertyinstanceresource/getitempropertyinstancebypurchasedtl/{branchcode}/{goodreceivenotedtl}")
	 * public List<ItemPropertyInstance>
	 * getItemPropertyInstancebyItemIdandGoodReceiveNoteDtl(
	 * 
	 * @PathVariable(value = "companymstid") String companymstid,
	 * 
	 * @PathVariable(value = "branchcode")String branchcode,
	 * 
	 * @PathVariable(value = "goodreceivenotedtl")String goodreceivenotedtl ){
	 * GoodReceiveNoteDtl goodReceiveNoteDtl =
	 * goodReceiveNoteDtlRepository.findById(goodReceiveNoteDtl).get();
	 * Optional<CompanyMst>
	 * companyMstOpt=companyMstRepository.findById(companymstid); return
	 * ItemPropertyInstanceRepository.
	 * findByCompanyMstAndBranchCodeAndGoodReceiveNoteDtl(companyMstOpt.get(),
	 * branchcode,goodreceivenotedtl); }
	 */
}
