package com.maple.restserver.resource.jasper;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.report.entity.StatementOfAccountReport;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.report.entity.TaxSummaryMst;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.DailySalesSummary;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.report.entity.AccountHeadsReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.service.AccountHeadsServiceImpl;
import com.maple.restserver.service.DailySaleSummaryServiceImpl;
import com.maple.restserver.service.IntentServiceImpl;
import com.maple.restserver.service.ItemmstServiceImpl;
import com.maple.restserver.service.KitDefinitionImpl;
import com.maple.restserver.service.PaymentHdrImpl;
import com.maple.restserver.service.SalesInvoiceServiceImpl;
import com.maple.restserver.service.SalesTransHdrService;
import com.maple.restserver.service.StockTransferOutDtlServiceImpl;
import com.maple.restserver.service.StockTransferOutHdrServiceImpl;
//import com.maple.restserver.utils.SystemSetting;
import com.maple.restserver.utils.SystemSetting;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@RestController
public class JasperReportController {

	@Autowired
	SalesDetailsRepository salesDetailsRepository;
	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@Autowired
	SalesInvoiceServiceImpl salesInvoiceServiceImpl;

	@GetMapping("{companymstid}/websalespdf")
	public ResponseEntity<byte[]> WebSalesPDF(
			@PathVariable("companymstid") String companymstid,
			@RequestParam("hdrid") String hdrid) throws JRException {
		
		SalesTransHdr salesTransHdr = salesTransHdrRepository.findById(hdrid).get();

		Date date = salesTransHdr.getVoucherDate();
		String voucherNumber = salesTransHdr.getVoucherNumber();

		String pdfToGenerate = "websales.pdf";

		// Call url

		List<SalesInvoiceReport> invoice = salesInvoiceServiceImpl.getSalesInvoice(companymstid, voucherNumber, date);

		List<TaxSummaryMst> invoiceTax = salesInvoiceServiceImpl.getSalesInvoiceTax(companymstid, voucherNumber, date);

		List<TaxSummaryMst> invoiceTaxSum = salesInvoiceServiceImpl.getSumOfTaxAmounts(companymstid, voucherNumber,
				date);

		List<SalesInvoiceReport> custDetails = salesInvoiceServiceImpl.getTaxinvoiceCustomer(companymstid,
				voucherNumber, date);

		Double cessAmount = salesInvoiceServiceImpl.getCessAmount(voucherNumber, date);

		Double taxableAmount = salesInvoiceServiceImpl.getNonTaxableAmount(voucherNumber, date);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(invoice);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(invoiceTax);
		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(invoiceTax);

		HashMap<String, Object> parameters = new HashMap();

		String jrxmlPath = "";
			parameters.put("logoname", "stmarys.png");
			if (null != invoice) {
				if (null != invoice.get(0)) {
					SalesInvoiceReport salesInvoice = invoice.get(0);
					if (null == salesInvoice.getDiscount()) {
						parameters.put("DiscountAmount", salesInvoice.getInvoiceDiscount());
						parameters.put("DiscountPercentage", salesInvoice.getDiscountPercent());

						parameters.put("DiscountAmountLabel", "Discount Amount:");
						parameters.put("DiscountPercentageLabel", "Discount Percentange:");
					}
				}
			}

			for (SalesInvoiceReport salesInvoiceReport : custDetails) {
				SalesInvoiceReport salesInvoice = new SalesInvoiceReport();
				if (null != invoice && invoice.size() > 0) {
					salesInvoice = invoice.get(0);
				}
				if (null != salesInvoice) {
					if (!salesInvoice.getSalesMode().equalsIgnoreCase("B2B")) {
						jrxmlPath = "jrxml/HWTaxInvoice.jrxml";

						if (null != salesInvoiceReport.getLocalCustomerName()) {
							salesInvoiceReport.setCustomerName(salesInvoiceReport.getLocalCustomerName());
							salesInvoiceReport.setCustomerPhone(salesInvoiceReport.getLocalCustomerPhone());
							salesInvoiceReport.setCustomerPlace(salesInvoiceReport.getLocalCustomerPlace());
							salesInvoiceReport.setCustomerState(salesInvoiceReport.getLocalCustomerState());
							salesInvoiceReport.setCustomerGst(null);
							salesInvoiceReport.setCustomerAddress(salesInvoiceReport.getLocalCustomerAddres());
						}
					} else {
						jrxmlPath = "jrxml/HWTaxInvoiceWithGst.jrxml";
					}
				}
			}

//		else {
//			SalesTransHdr getSalesHdr = RestCaller.getSalesTransHdrByVoucherAndDate(voucherNumber, date);
//
//			// jrxmlPath = "jrxml/taxinvoicee.jrxml";
//			if (null == getSalesHdr.getCustomerMst().getCustomerDiscount()) {
//				getSalesHdr.getCustomerMst().setCustomerDiscount(0.0);
//			}
//			if (getSalesHdr.getCustomerMst().getCustomerDiscount() > 0) {
//				jrxmlPath = "jrxml/discountTaxInvoice.jrxml";
//			} else {
//				jrxmlPath = "jrxml/taxinvoicee.jrxml";
//			}
//
//			if (SystemSetting.wholesale_invoice_format.equalsIgnoreCase("BAKERY_WITH_BATCH")) {
//				jrxmlPath = "jrxml/TaxinvoiceeWithbatch.jrxml";
//
//			}
//			for (SalesInvoiceReport salesInvoiceReport : custDetails) {
//				if (null != salesInvoiceReport.getLocalCustomerName()) {
//					salesInvoiceReport.setCustomerName(salesInvoiceReport.getLocalCustomerName());
//					salesInvoiceReport.setCustomerPhone(salesInvoiceReport.getLocalCustomerPhone());
//					salesInvoiceReport.setCustomerPlace(salesInvoiceReport.getLocalCustomerPlace());
//					salesInvoiceReport.setCustomerState(salesInvoiceReport.getLocalCustomerState());
//					salesInvoiceReport.setCustomerGst(null);
//					salesInvoiceReport.setCustomerAddress(salesInvoiceReport.getLocalCustomerAddres());
//				}
//			}
//		}

		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		String LocalCustomerName = null;
		String LocalCustomerAddress = null;
		String LocalCustomerPoneNo = null;
		String LocalCustomerPlace = null;
		String LocalCustomerSate = null;
		if (null != invoice) {
			SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
			if (invoice.size() > 0) {
				salesInvoiceReport = invoice.get(0);
				if (null != salesInvoiceReport.getSalesMode()) {
					if (salesInvoiceReport.getSalesMode().equalsIgnoreCase("B2B")) {
						SalesInvoiceReport salesCust = new SalesInvoiceReport();
						if (null != custDetails) {
							salesCust = custDetails.get(0);
						}
						LocalCustomerName = salesCust.getLocalCustomerName();
						LocalCustomerAddress = salesCust.getLocalCustomerAddres();
						LocalCustomerPoneNo = salesCust.getLocalCustomerPhone();
						LocalCustomerPlace = salesCust.getLocalCustomerPlace();
						LocalCustomerSate = salesCust.getLocalCustomerState();

					}
				}
			}
		}

		parameters.put("LocalCustomerName", LocalCustomerName);
		parameters.put("LocalCustomerAddress", LocalCustomerAddress);
		parameters.put("LocalCustomerPoneNo", LocalCustomerPoneNo);
		parameters.put("LocalCustomerPlace", LocalCustomerPlace);
		parameters.put("LocalCustomerSate", LocalCustomerSate);

		parameters.put("subreportdata2", jrBeanCollectionDataSource3);
		parameters.put("stax_invoice_tax_sub2", "jrxml/taxsumarytable.jasper");

		parameters.put("CustomerAddress", custDetails.get(0).getCustomerAddress());
		parameters.put("CustomerName", custDetails.get(0).getCustomerName());
		parameters.put("PhoneNumber", custDetails.get(0).getCustomerPhone());
		parameters.put("CustomerGst", custDetails.get(0).getCustomerGst());
		parameters.put("CustomerPlace", custDetails.get(0).getCustomerPlace());

		String taxableAmountS = "0.0";
		if (invoice.size() > 0) {
			Double amountWithDiscount = invoice.get(0).getAmount() + invoice.get(0).getInvoiceDiscount();
			parameters.put("InvoiceAmountWithDiscount", amountWithDiscount);
			BigDecimal amountBig = new BigDecimal(amountWithDiscount);
			amountBig = amountBig.setScale(0, BigDecimal.ROUND_CEILING);

			String amount = amountBig + "";

			if (null != taxableAmount) {
				taxableAmountS = taxableAmount + "";

			}
			String amountinwords = SystemSetting.AmountInWords(amount, "Rs", "Ps");

			String taxamount = "0.0";
			Double taxDouble = 0.0;

			if (null != invoiceTax) {
				if (invoiceTax.size() > 0) {

					for (TaxSummaryMst tax : invoiceTax) {
						taxDouble = taxDouble + tax.getTaxAmount();
					}

				}
			}

			taxamount = taxDouble + "";

			String taxableAmountInWords = SystemSetting.AmountInWords(taxableAmountS, "Rs", "Ps");
			String taxamountinwords = SystemSetting.AmountInWords(taxamount, "Rs", "Ps");
			parameters.put("AmountInWords", amountinwords);
			parameters.put("taxableAmountInWords", taxableAmountInWords);
			parameters.put("TaxAmountInWords", taxamountinwords);
			parameters.put("taxamount", taxamount);

			parameters.put("taxableAmount", taxableAmountS);
			if (null == cessAmount) {
				cessAmount = 0.0;
			}

			parameters.put("CessAmount", cessAmount);

			String gst = "";
		}

		double cgstPercent = 0.0;
		double sgstPercent = 0.0;
		double igstPercent = 0.0;

		if (invoiceTaxSum.size() > 0) {

			cgstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfcgstAmount(), 2);
			sgstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfsgstAmount(), 2);
			igstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfigstAmount(), 2);
		}
		parameters.put("cgst%", cgstPercent);
		parameters.put("sgst%", sgstPercent);
		parameters.put("igst%", igstPercent);

		byte[] contents = null;
		try {
			contents = Files.readAllBytes(Paths.get(pdfToGenerate));
		} catch (IOException e) {
		}

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_PDF);

		headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);

		return response;
	}

	/*
	 * @Autowired private JasperPdfReportService jasperPdfReportService;
	 * 
	 * @Autowired SalesInvoiceServiceImpl salesInvoiceServiceImpl;
	 * 
	 * @Autowired AccountHeadsServiceImpl accountHeadsServiceImpl;
	 * 
	 * @Autowired KitDefinitionImpl kitDefinitionImpl;
	 * 
	 * @Autowired StatementsOfAccountServiceImpl statementsOfAccountServiceImpl ;
	 * 
	 * @Autowired StockTransferOutDtlServiceImpl stockTransferOutDtlServiceImpl;
	 * 
	 * @Autowired StockTransferOutHdrServiceImpl stockTransferOutHdrServiceImpl;
	 * 
	 * @Autowired CustomerRegistrationServiceImp customerRegistrationServiceImp;
	 * 
	 * @Autowired DailySaleSummaryServiceImpl dailySaleSummaryServiceImpl;
	 * 
	 * @Autowired ItemmstServiceImpl itemmstServiceImpl;
	 * 
	 * @Autowired IntentServiceImpl intentServiceImpl;
	 * 
	 * @Autowired PaymentHdrImpl paymentHdrImpl;
	 * 
	 * @Autowired SalesTransHdrImpl salesTransHdrImpl;
	 * 
	 * 
	 * 
	 * @GetMapping("{companymstid}/salesinvoicepdf") public
	 * ResponseEntity<byte[]>salesInvoiceAll(
	 * 
	 * @RequestParam("vouchernumber") String vouchernumber,
	 * 
	 * @RequestParam("rdate") String reportdate,
	 * 
	 * @PathVariable(value = "companymstid") String companymstid) {
	 * 
	 * java.sql.Date date = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");
	 * 
	 * 
	 * 
	 * 
	 * List<SalesInvoiceReport> salesInvoiceReport =
	 * salesInvoiceServiceImpl.getSalesInvoice(companymstid, vouchernumber, date);
	 * 
	 * List<TaxSummaryMst> taxSummary =
	 * salesInvoiceServiceImpl.getSalesInvoiceTax(companymstid, vouchernumber,
	 * date);
	 * 
	 * List<TaxSummaryMst> invoiceTotal =
	 * salesInvoiceServiceImpl.getSumOfTaxAmounts(companymstid, vouchernumber,
	 * date);
	 * 
	 * List<SalesInvoiceReport> customerReport =
	 * salesInvoiceServiceImpl.getTaxinvoiceCustomer(companymstid, vouchernumber,
	 * date);
	 * 
	 * Double cessAmount = salesInvoiceServiceImpl.getCessAmount(vouchernumber,
	 * date);
	 * 
	 * Double taxableAmount =
	 * salesInvoiceServiceImpl.getNonTaxableAmount(vouchernumber, date);
	 * 
	 * 
	 * String pdfToGenerate = "salesinvoiceall.pdf"; List<SalesInvoiceReport>
	 * salesInvoicelist= salesInvoiceServiceImpl.getAllSalesInvoice(pdfToGenerate);
	 * 
	 * 
	 * JRBeanCollectionDataSource jrBeanCollectionDataSource = new
	 * JRBeanCollectionDataSource(salesInvoiceReport); JRBeanCollectionDataSource
	 * jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(taxSummary);
	 * JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new
	 * JRBeanCollectionDataSource(taxSummary);
	 * 
	 * 
	 * 
	 * 
	 * 
	 * String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
	 * "jrxml/HWTaxInvoiceWithGst.jrxml", pdfToGenerate ,null,
	 * jrBeanCollectionDataSource );
	 * 
	 * 
	 * HashMap<String, Object> parameters = new HashMap();
	 * 
	 * parameters.put("subreportdata2", jrBeanCollectionDataSource3);
	 * parameters.put("stax_invoice_tax_sub2", "jrxml/taxsumarytable.jasper");
	 * 
	 * if(null != customerReport) {
	 * parameters.put("LocalCustomerName",customerReport.get(0).getLocalCustomerName
	 * ()); parameters.put("LocalCustomerAddress",customerReport.get(0).
	 * getLocalCustomerAddres());
	 * parameters.put("LocalCustomerPoneNo",customerReport.get(0).
	 * getLocalCustomerPhone());
	 * parameters.put("LocalCustomerPlace",customerReport.get(0).
	 * getLocalCustomerPlace());
	 * parameters.put("LocalCustomerSate",customerReport.get(0).
	 * getLocalCustomerState()); }
	 * 
	 * parameters.put("subreportdata2", jrBeanCollectionDataSource3);
	 * parameters.put("stax_invoice_tax_sub2", "jrxml/taxsumarytable.jasper");
	 * if(null != customerReport) {
	 * parameters.put("CustomerAddress",customerReport.get(0).getCustomerAddress());
	 * parameters.put("CustomerName",customerReport.get(0).getCustomerName());
	 * parameters.put("PhoneNumber",customerReport.get(0).getCustomerPhone());
	 * parameters.put("CustomerGst",customerReport.get(0).getCustomerGst());
	 * parameters.put("CustomerPlace",customerReport.get(0).getCustomerPlace()); }
	 * 
	 * double cgstPercent =0.0; double sgstPercent =0.0; double igstPercent =0.0;
	 * 
	 * 
	 * if(invoiceTotal.size() >0 ) {
	 * 
	 * cgstPercent = SystemSetting.round(invoiceTotal.get(0).getSumOfcgstAmount(),
	 * 2) ; sgstPercent =
	 * SystemSetting.round(invoiceTotal.get(0).getSumOfsgstAmount(),2) ; igstPercent
	 * = SystemSetting.round(invoiceTotal.get(0).getSumOfigstAmount(),2) ; }
	 * parameters.put("cgst%",cgstPercent); parameters.put("sgst%",sgstPercent);
	 * parameters.put("igst%",igstPercent);
	 * 
	 * String taxableAmountS = "0.0"; if(salesInvoiceReport.size()>0) { BigDecimal
	 * amountBig = new BigDecimal(salesInvoiceReport.get(0).getAmount()); amountBig
	 * = amountBig.setScale(0, BigDecimal.ROUND_CEILING);
	 * 
	 * // BigDecimal taxableAmountInBig = new BigDecimal(taxableAmount); //
	 * taxableAmountInBig = taxableAmountInBig.setScale(0,
	 * BigDecimal.ROUND_HALF_EVEN); String amount = amountBig+"";
	 * 
	 * if(null != taxableAmount) { taxableAmountS = taxableAmount+"";
	 * 
	 * } String amountinwords = SystemSetting.AmountInWords(amount, "Rs", "Ps");
	 * 
	 * String taxamount = "0.0";
	 * 
	 * if(null!=taxSummary) { if(taxSummary.size() > 0 ) { taxamount =
	 * taxSummary.get(0).getTaxAmount()+""; }else { taxamount = "0.0"; } }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * String taxableAmountInWords = SystemSetting.AmountInWords(taxableAmountS,
	 * "Rs", "Ps"); String taxamountinwords = SystemSetting.AmountInWords(taxamount,
	 * "Rs", "Ps"); parameters.put("AmountInWords",amountinwords);
	 * parameters.put("taxableAmountInWords",taxableAmountInWords);
	 * parameters.put("TaxAmountInWords",taxamountinwords);
	 * parameters.put("taxamount",taxamount);
	 * 
	 * parameters.put("taxableAmount",taxableAmountS);
	 * 
	 * if(cessAmount > 0.0 && null != cessAmount) {
	 * parameters.put("CessAmount",cessAmount); } String gst = ""; }
	 * 
	 * byte[] contents = null ; try { contents =
	 * Files.readAllBytes(Paths.get(pdfToGenerate)); } catch (IOException e) { }
	 * 
	 * 
	 * HttpHeaders headers = new HttpHeaders();
	 * headers.setContentType(MediaType.APPLICATION_PDF);
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
	 * headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
	 * ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers,
	 * HttpStatus.OK);
	 * 
	 * return response; }
	 */

//	@Autowired
//	SalesInvoiceServiceImpl salesInvoiceServiceImpl;
//	@Autowired
//	private JasperPdfReportService jasperPdfReportService;
//
//	@Autowired
//	SalesInvoiceServiceImpl salesInvoiceServiceImpl;
//	
//	@Autowired
//	AccountHeadsServiceImpl accountHeadsServiceImpl;
//	
//	@Autowired
//	KitDefinitionImpl kitDefinitionImpl;
//	@Autowired
//	StatementsOfAccountServiceImpl statementsOfAccountServiceImpl ;
//	@Autowired
//	StockTransferOutDtlServiceImpl   stockTransferOutDtlServiceImpl;
//	
//	@Autowired
//	StockTransferOutHdrServiceImpl   stockTransferOutHdrServiceImpl;
//
//	@Autowired
//	CustomerRegistrationServiceImp customerRegistrationServiceImp;
//	
//	@Autowired
//	DailySaleSummaryServiceImpl dailySaleSummaryServiceImpl;
//	
//	@Autowired
//	ItemmstServiceImpl itemmstServiceImpl;
//	@Autowired
//	IntentServiceImpl intentServiceImpl;
//	@Autowired
//	PaymentHdrImpl paymentHdrImpl;
//	@Autowired
//	SalesTransHdrImpl salesTransHdrImpl;
//
////	@GetMapping("/accountheadspdf")
////	public ResponseEntity<byte[]>  empReport() {
////
////		String pdf = jasperPdfReportService.generateReport("jrxml/accountheads.jrxml", "accountheads.pdf", null); ;
////		
////	 
////
////		     
////		    byte[] contents  = null ;
////		    try {
////		    	contents = Files.readAllBytes(Paths.get("accountheads.pdf"));
////		    } catch (IOException e) {
////	        } 
////	       
////
////		    HttpHeaders headers = new HttpHeaders();
////		    headers.setContentType(MediaType.APPLICATION_PDF);
////		    // Here you have to set the actual filename of your pdf
////		    
////		        
////		    
////		    
////		    headers.setContentDispositionFormData(pdf, pdf);
////		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
////		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
////		   
////		    return response;
////		}
//	
//	/*
//	 * @GetMapping("/dailysalessummarypdf") public ResponseEntity<byte[]>
//	 * dailysalesSummary() {
//	 * 
//	 * String pdf = jasperPdfReportService.generateReport(
//	 * "jrxml/dailySalesSummaryVertical.jrxml", " dailysalessummary.pdf", null); ;
//	 * 
//	 * 
//	 * 
//	 * 
//	 * byte[] contents = null ; try { contents =
//	 * Files.readAllBytes(Paths.get("dailysalessummary.pdf")); } catch (IOException
//	 * e) { }
//	 * 
//	 * 
//	 * HttpHeaders headers = new HttpHeaders();
//	 * headers.setContentType(MediaType.APPLICATION_PDF); // Here you have to set
//	 * the actual filename of your pdf
//	 * 
//	 * 
//	 * 
//	 * 
//	 * 
//	 * headers.setContentDispositionFormData(pdf, pdf);
//	 * headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//	 * ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers,
//	 * HttpStatus.OK);
//	 * 
//	 * return response; }
//	 */
//	@GetMapping("/statementofaccountspdf")
//	public ResponseEntity<byte[]>  statementofAccount() {
// 	
//
//		String pdfToGenerate = "statementofaccounts.pdf";
//		 List< StatementOfAccountReport>  statementOfAccountsList = statementsOfAccountServiceImpl.getAllStatementOfAccount();
//		
//	 
//		 JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(statementOfAccountsList);
//		     
//		 String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//				 "jrxml/ statementofaccounts.jrxml",
//				 pdfToGenerate ,null,
//				 jrBeanCollectionDataSource );
//		 
//		 
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get(pdfToGenerate));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		   
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//	 	   
//		    return response;
//		}
//	@GetMapping("/intentdtlpdf")
//	public ResponseEntity<byte[]>  intentDtl() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/intentdtl.jrxml", "intentdtl.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("intentdtl.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//
//	/*
//	 * @GetMapping("/intenthdrpdf") public ResponseEntity<byte[]> intentHdr() {
//	 * 
//	 * String pdf = jasperPdfReportService.generateReport("jrxml/Intenthdr.jrxml",
//	 * "intenthdr.pdf", null); ;
//	 * 
//	 * 
//	 * 
//	 * 
//	 * byte[] contents = null ; try { contents =
//	 * Files.readAllBytes(Paths.get("intenthdr.pdf")); } catch (IOException e) { }
//	 * 
//	 * 
//	 * HttpHeaders headers = new HttpHeaders();
//	 * headers.setContentType(MediaType.APPLICATION_PDF); // Here you have to set
//	 * the actual filename of your pdf
//	 * 
//	 * 
//	 * 
//	 * 
//	 * 
//	 * headers.setContentDispositionFormData(pdf, pdf);
//	 * headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//	 * ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers,
//	 * HttpStatus.OK);
//	 * 
//	 * return response; }
//	 */
//	@GetMapping("/itemmstpdf")
//	public ResponseEntity<byte[]>  itemMst() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/itemmst.jrxml", "itemmst.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("itemmst.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/kitdefinitiondtlpdf")
//	public ResponseEntity<byte[]>  kitdefinitionDtl() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/kitdefinitiondtl.jrxml", "kitdefinitiondtl.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("kitdefinitiondtl.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	
////	@GetMapping("/kitdefinitionmstpdf")
////	public ResponseEntity<byte[]>  kitdefinitionMst() {
////
////		String pdf = jasperPdfReportService.generateReport("jrxml/kitdefinitionmst.jrxml", "kitdefinitionmst.pdf", null); ;
////		
////	 
////
////		     
////		    byte[] contents  = null ;
////		    try {
////		    	contents = Files.readAllBytes(Paths.get("kitdefinitionmst.pdf"));
////		    } catch (IOException e) {
////	        } 
////	       
////
////		    HttpHeaders headers = new HttpHeaders();
////		    headers.setContentType(MediaType.APPLICATION_PDF);
////		    // Here you have to set the actual filename of your pdf
////		    
////		    
////		    
////		    
////		    
////		    headers.setContentDispositionFormData(pdf, pdf);
////		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
////		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
////		   
////		    return response;
////		}
//	@GetMapping("/onlinesalespdf")
//	public ResponseEntity<byte[]>onlineSales() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/onlinesales.jrxml", "onlinesales.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("onlinesales.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	
//	@GetMapping("/paymentdtlpdf")
//	public ResponseEntity<byte[]>paymentDtl() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/paymentdtl.jrxml", "paymentdtl.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("paymentdtl.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/paymenthdrpdf")
//	public ResponseEntity<byte[]>paymentHdr() {
//
//		String pdfToGenerate = "paymenthdr.pdf";
//		List<PaymentHdr> paymentHdrList= paymentHdrImpl.getPaymentHdr(pdfToGenerate);
//		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(paymentHdrList);
//		
//		 String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//				 "jrxml/paymenthdr.jrxml",
//				 pdfToGenerate ,null,
//				 jrBeanCollectionDataSource );
//		 
//		 byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get(pdfToGenerate));
//		    } catch (IOException e) {
//	        } 
//	       
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/productiondtlpdf")
//	public ResponseEntity<byte[]>productionDtl() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/productiondtldtl.jrxml", "productiondtldtl.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("paymenthdr.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/productiondtlspdf")
//	public ResponseEntity<byte[]>productionDtls() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/productiondtls.jrxml", "productiondtls.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("productiondtls.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/productionmstpdf")
//	public ResponseEntity<byte[]>productionMst() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/productionmsts.jrxml", "productionmsts.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("productionmsts.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/purchasehdrpdf")
//	public ResponseEntity<byte[]>purchaseHdr() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/purchasehdr.jrxml", "purchasehdr.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("purchasehdr.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/reordermstpdf")
//	public ResponseEntity<byte[]>reorderMst() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/reordermsts.jrxml", "reordermsts.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("reordermsts.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/salesorderbydatepdf")
//	public ResponseEntity<byte[]>salesOrderByDate() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/salesorderbydate.jrxml", "salesorderbydate.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("salesorderbydate.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/salestranshdrpdf")
//	public ResponseEntity<byte[]>salesTransHdr() {
//
//		String pdfToGenerate = "salestranshdr.pdf";
//		List<SalesTransHdr> salesTransHdrList= salesTransHdrImpl.getSalesTransHdr(pdfToGenerate);
//		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(salesTransHdrList);
//		
//		 String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//				 "jrxml/salestranshdr.jrxml",
//				 pdfToGenerate ,null,
//				 jrBeanCollectionDataSource );
//		 
//		 byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get(pdfToGenerate));
//		    } catch (IOException e) {
//	        } 
//	       
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		   
//		    headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/stockpdf")
//	public ResponseEntity<byte[]>stock() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/stock.jrxml", "stock.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("stock.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	
//	@GetMapping("/stocktransferoutdtlpdf")
//	public ResponseEntity<byte[]>stockTransferOutDtl() {
//		String pdfToGenerate = "stocktransferoutdtl.pdf";
//		 List< StockTransferOutDtl>   stocktransferReportList = stockTransferOutDtlServiceImpl.getAllStockTransfer();
//		
//		
//		
//		 JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(stocktransferReportList);
//		
//		 String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//				 "jrxml/stocktransferoutdtl.jrxml",
//				 pdfToGenerate ,null,
//				 jrBeanCollectionDataSource );
//		
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("stocktransferoutdtl.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		 
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/stocktransferouthdrpdf")
//	public ResponseEntity<byte[]>stockTransferOutHdr() {
//
//		String pdfToGenerate = "stocktransferouthdr.pdf";
//		 List< StockTransferOutHdr>   stocktransferReportList1 = stockTransferOutHdrServiceImpl.getAllStockTransferOutHdr();
//		
//		
//	 
//		 JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(stocktransferReportList1);
//		
//		 
//		 String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//				 "jrxml/stocktransferouthdr.jrxml",
//				 pdfToGenerate ,null,
//				 jrBeanCollectionDataSource );
//		 
//	
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("stocktransferouthdr.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/supplierpdf")
//	public ResponseEntity<byte[]>supplier() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/suppliers.jrxml", "suppliers.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("suppliers.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    // Here you have to set the actual filename of your pdf
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/unitmstpdf")
//	public ResponseEntity<byte[]>unitMst() {
//
//		String pdf = jasperPdfReportService.generateReport("jrxml/unitmst.jrxml", "unitmst.pdf", null); ;
//		
//	 
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get("unitmst.pdf"));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		  
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdf, pdf);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	/*
//	 * @GetMapping("/customerregistrationpdf") public
//	 * ResponseEntity<byte[]>customerRegistration () {
//	 * 
//	 * String pdf =
//	 * jasperPdfReportService.generateReport("jrxml/customerregistration.jrxml",
//	 * "customerregistration.pdf", null); ;
//	 * 
//	 * 
//	 * 
//	 * 
//	 * byte[] contents = null ; try { contents =
//	 * Files.readAllBytes(Paths.get("customerregistration.pdf")); } catch
//	 * (IOException e) { }
//	 * 
//	 * 
//	 * HttpHeaders headers = new HttpHeaders();
//	 * headers.setContentType(MediaType.APPLICATION_PDF);
//	 * 
//	 * 
//	 * 
//	 * 
//	 * 
//	 * 
//	 * headers.setContentDispositionFormData(pdf, pdf);
//	 * headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//	 * ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers,
//	 * HttpStatus.OK);
//	 * 
//	 * return response; }
//	 */
//	
//
//	@GetMapping("{companymstid}/salesinvoicepdf") 
//	  public ResponseEntity<byte[]>salesInvoiceAll( 
//			  @RequestParam("vouchernumber") String vouchernumber,
//			  @RequestParam("rdate") String reportdate,
//			  @PathVariable(value = "companymstid") String  companymstid) {
//		  
//			java.sql.Date date = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");
//			
//			
//
//		  
//		  List<SalesInvoiceReport> salesInvoiceReport = salesInvoiceServiceImpl.getSalesInvoice(companymstid, vouchernumber, date);
//	 
//		  List<TaxSummaryMst> taxSummary = salesInvoiceServiceImpl.getSalesInvoiceTax(companymstid, vouchernumber, date);
//		  
//		  List<TaxSummaryMst> invoiceTotal = salesInvoiceServiceImpl.getSumOfTaxAmounts(companymstid, vouchernumber, date);
//		  
//		  List<SalesInvoiceReport> customerReport = salesInvoiceServiceImpl.getTaxinvoiceCustomer(companymstid, vouchernumber, date);
//		  
//		  Double cessAmount = salesInvoiceServiceImpl.getCessAmount(vouchernumber, date);
//		  
//		  Double taxableAmount = salesInvoiceServiceImpl.getNonTaxableAmount(vouchernumber, date);
//		  
//	 
//		  String pdfToGenerate = "salesinvoiceall.pdf"; List<SalesInvoiceReport>
//	  salesInvoicelist= salesInvoiceServiceImpl.getAllSalesInvoice(pdfToGenerate);
//	 
//		  
//	JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(salesInvoiceReport);
//	JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(taxSummary);
//	JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(taxSummary);
//	  
//	  
//	  
//	  
//	  
//	  String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//	  "jrxml/HWTaxInvoiceWithGst.jrxml", pdfToGenerate ,null,
//	  jrBeanCollectionDataSource );
//	  
//	  
//	  HashMap<String, Object> parameters = new HashMap();
//	  
//		parameters.put("subreportdata2", jrBeanCollectionDataSource3);
//		parameters.put("stax_invoice_tax_sub2", "jrxml/taxsumarytable.jasper");
//	  
//	if(null != customerReport)  
//	{
//	  parameters.put("LocalCustomerName",customerReport.get(0).getLocalCustomerName());
//		parameters.put("LocalCustomerAddress",customerReport.get(0).getLocalCustomerAddres());
//		parameters.put("LocalCustomerPoneNo",customerReport.get(0).getLocalCustomerPhone());
//		parameters.put("LocalCustomerPlace",customerReport.get(0).getLocalCustomerPlace());
//		parameters.put("LocalCustomerSate",customerReport.get(0).getLocalCustomerState());
//	}
//	
//	parameters.put("subreportdata2", jrBeanCollectionDataSource3);
//	parameters.put("stax_invoice_tax_sub2", "jrxml/taxsumarytable.jasper");
//	if(null != customerReport)  
//	{
//		parameters.put("CustomerAddress",customerReport.get(0).getCustomerAddress());
//		parameters.put("CustomerName",customerReport.get(0).getCustomerName());
//		parameters.put("PhoneNumber",customerReport.get(0).getCustomerPhone());
//		parameters.put("CustomerGst",customerReport.get(0).getCustomerGst());
//		parameters.put("CustomerPlace",customerReport.get(0).getCustomerPlace());
//	}
//	
//	double cgstPercent =0.0;
//	double sgstPercent =0.0;
//	double igstPercent =0.0;
//	
//	
//	if(invoiceTotal.size() >0 ) {
//		
//		  cgstPercent = SystemSetting.round(invoiceTotal.get(0).getSumOfcgstAmount(), 2) ;
//		  sgstPercent = SystemSetting.round(invoiceTotal.get(0).getSumOfsgstAmount(),2) ;
//		  igstPercent = SystemSetting.round(invoiceTotal.get(0).getSumOfigstAmount(),2) ; 
//	}
//	parameters.put("cgst%",cgstPercent);
//	parameters.put("sgst%",sgstPercent);
//	parameters.put("igst%",igstPercent);
//	
//	String taxableAmountS = "0.0";
//	if(salesInvoiceReport.size()>0)
//	{
//		BigDecimal amountBig = new BigDecimal(salesInvoiceReport.get(0).getAmount());
//		amountBig = amountBig.setScale(0, BigDecimal.ROUND_CEILING);
//		
////		BigDecimal taxableAmountInBig = new BigDecimal(taxableAmount);
////		taxableAmountInBig = taxableAmountInBig.setScale(0, BigDecimal.ROUND_HALF_EVEN);
//	String amount = amountBig+"";
//	
//	if(null != taxableAmount)
//	{
//	 taxableAmountS = taxableAmount+"";
//	 
//	}
//	String amountinwords = SystemSetting.AmountInWords(amount, "Rs", "Ps");
//
//	String taxamount = "0.0";
//	
//	if(null!=taxSummary) {
//		if(taxSummary.size() > 0 ) {
//			  taxamount = taxSummary.get(0).getTaxAmount()+"";
//			}else {
//				  taxamount = "0.0";
//			}
//	}
//	
//	
//	
//
//	 
//	String taxableAmountInWords = SystemSetting.AmountInWords(taxableAmountS, "Rs", "Ps");
//	String taxamountinwords = SystemSetting.AmountInWords(taxamount, "Rs", "Ps");
//	parameters.put("AmountInWords",amountinwords);
//	parameters.put("taxableAmountInWords",taxableAmountInWords);
//	parameters.put("TaxAmountInWords",taxamountinwords);
//	parameters.put("taxamount",taxamount);
//	
//	parameters.put("taxableAmount",taxableAmountS);
//
//	if(cessAmount > 0.0 && null != cessAmount)
//	{
//	parameters.put("CessAmount",cessAmount);
//	}
//	String gst = "";
//	}
//	  
//	  byte[] contents = null ; try { contents =
//	  Files.readAllBytes(Paths.get(pdfToGenerate)); } catch (IOException e) { }
//	  
//	  
//	  HttpHeaders headers = new HttpHeaders();
//	  headers.setContentType(MediaType.APPLICATION_PDF);
//	  
//	  
//	  
//	  
//	  
//	  
//	  headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
//	  headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//	  ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers,
//	  HttpStatus.OK);
//	  
//	  return response; }

//	@GetMapping("/accountheadspdf")
//	public ResponseEntity<byte[]>accountHeads () {
//
//		String pdfToGenerate = "accountheads.pdf";
//		List<AccountHeads>  accounheadsList = accountHeadsServiceImpl.getAccountHeads(pdfToGenerate);
//		
//	 
//		 JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(accounheadsList);
//		 /*
//		  * "jrxml/dailySalesSummaryVertical.jrxml", 
//		  * " dailysalessummary.pdf", null
//		  */
//		 
//		 String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//				 "jrxml/accountheads.jrxml",
//				 pdfToGenerate ,null,
//				 jrBeanCollectionDataSource );
//		 
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get(pdfToGenerate));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		   
//		     
//		    headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//
//	@GetMapping("/kitdefinitionmstpdf")
//	public ResponseEntity<byte[]>kitdefinitionmst() {
//		String pdfToGenerate = "kitdefinitionmst.pdf";
//		List<KitDefinitionMst> kitDefinitionMstList= kitDefinitionImpl.getKitDefinitionMst(pdfToGenerate);
//		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(kitDefinitionMstList);
//		
//		 String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//				 "jrxml/kitdefinitionmst.jrxml",
//				 pdfToGenerate ,null,
//				 jrBeanCollectionDataSource );
//		 
//		 byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get(pdfToGenerate));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		 
//		 
//	}
//
//	@GetMapping("/customerspdf")
//	public ResponseEntity<byte[]>customersget () {
//
//		String pdfToGenerate = "customers.pdf";
//		 List<CustomerRegistration> customerList = customerRegistrationServiceImp.getCustomers(pdfToGenerate);
//		
//		 JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(customerList);
//			
//		 String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//				 "jrxml/customerregistration.jrxml",
//				 pdfToGenerate ,null,
//				 jrBeanCollectionDataSource );
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get(pdfToGenerate));
//		    } catch (IOException e) {
//	        } 
//	       
//
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		   
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/dailysalessummarypdf")
//	public ResponseEntity<byte[]>getdailysales () {
//
//		String pdfToGenerate = "dailysalessummary.pdf";
//		List<DailySalesSummary> dailySalesList = dailySaleSummaryServiceImpl.getDailySales(pdfToGenerate);
//		
//	 
//		 JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(dailySalesList);
//			
//			String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//					 "jrxml/dailySalesSummaryVertical.jrxml",
//					 pdfToGenerate ,null,
//					 jrBeanCollectionDataSource );
//
//		     
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get(pdfToGenerate));
//		    } catch (IOException e) {
//	        } 
//		    HttpHeaders headers = new HttpHeaders();
//
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		   
//		    
//		    
//		    
//		    
//		    
//		    headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//	@GetMapping("/intenthdrpdf")
//	public ResponseEntity<byte[]>getIntenthdrs () {
//
//		String pdfToGenerate = "intenthdr.pdf";
//		List<IntentHdr>intenthdrList = intentServiceImpl.getIntentHdrs(pdfToGenerate);
//		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(intenthdrList);
//		 
//		 String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//				 "jrxml/Intenthdr.jrxml",
//				 pdfToGenerate ,null,
//				 jrBeanCollectionDataSource );
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get(pdfToGenerate));
//		    } catch (IOException e) {
//	        } 
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//
//		    headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		   
//		    return response;
//		}
//
//	@GetMapping("/itemmstnewpdf")
//	public ResponseEntity<byte[]>getItemmsts () {
//
//		String pdfToGenerate = "itemmsts1.pdf";
//		 List<ItemMst> itemmstList = itemmstServiceImpl.getItems(pdfToGenerate);
//		 JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(itemmstList);
//		 String pdfPath = jasperPdfReportService.generateReportWithBeanDatasource(
//				 "jrxml/itemmstnew.jrxml",
//				 pdfToGenerate ,null,
//				 jrBeanCollectionDataSource );
//		    byte[] contents  = null ;
//		    try {
//		    	contents = Files.readAllBytes(Paths.get(pdfToGenerate));
//		    } catch (IOException e) {
//	        } 
//		    HttpHeaders headers = new HttpHeaders();
//		    headers.setContentType(MediaType.APPLICATION_PDF);
//		    
//		    headers.setContentDispositionFormData(pdfToGenerate, pdfToGenerate);
//		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		    ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
//		    return response;
//		}
//
	//
}
