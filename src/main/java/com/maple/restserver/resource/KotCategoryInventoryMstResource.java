package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.KotCategoryInventoryMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.KotCategoryInventoryMstService;

@RestController
public class KotCategoryInventoryMstResource {
	
	/*
	 * @Autowired KotCategoryInventoryMstService kotCategoryInventoryMstService;
	 * 
	 * @Autowired CompanyMstRepository companyMstRepository;
	 * 
	 * @GetMapping("{companymstid}/kotcategoryinventorymstresource/allcategories")
	 * public List<KotCategoryInventoryMst> getAllCategory(
	 * 
	 * @PathVariable(value = "companymstid") String companymstid){ return
	 * kotCategoryInventoryMstService.findByParentItem(companymstid); }
	 * 
	 * @GetMapping(
	 * "{companymstid}/kotcategoryinventorymstresource/{parentitem}/allitemsbycategory")
	 * public List<KotCategoryInventoryMst> getAllItemByCategory(
	 * 
	 * @PathVariable(value = "companymstid") String companymstid,
	 * 
	 * @PathVariable(value = "parentitem") String parentitem){ return
	 * kotCategoryInventoryMstService.findByCompanyMstIdAndParentItem(companymstid,
	 * parentitem); }
	 * 
	 * @PostMapping("{companymstid}/kotcategoryinventorymstresource/save") public
	 * KotCategoryInventoryMst saveKotCategoryInventory(
	 * 
	 * @PathVariable(value = "companymstid") String companymstid,
	 * 
	 * @Valid @RequestBody KotCategoryInventoryMst kotCategoryInventoryMst) {
	 * Optional<CompanyMst> CompanyMstOpt =
	 * companyMstRepository.findById(companymstid); CompanyMst
	 * companyMst=CompanyMstOpt.get();
	 * kotCategoryInventoryMst.setCompanyMst(companyMst); return
	 * kotCategoryInventoryMstService.saveKotCatInventory(kotCategoryInventoryMst);
	 * }
	 */
	
	

	@Autowired
	KotCategoryInventoryMstService kotCategoryInventoryMstService;

	@GetMapping("{companymstid}/kotcategoryinventorymstresource/allcategories")
	public List<KotCategoryInventoryMst> getAllCategory(
			@PathVariable(value = "companymstid") String companymstid){
		return kotCategoryInventoryMstService.findByParentItem(companymstid);
	}
	
	@GetMapping("{companymstid}/kotcategoryinventorymstresource/{parentitem}/allitemsbycategory")
	public List<KotCategoryInventoryMst> getAllItemByCategory(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "parentitem") String parentitem){
		return kotCategoryInventoryMstService.findByCompanyMstIdAndParentItem(companymstid,parentitem);
	}
	
}
