package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PrinterMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PrinterMstRepository;

@RestController
public class PrinterMstResource {

	@Autowired
	CompanyMstRepository  companyMstRepository;
	
	@Autowired
	PrinterMstRepository printerMstRepository;
	
	
	@PostMapping("{companymstid}/printermstresource/printermst")
	public PrinterMst creatPrinterMst(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody PrinterMst kotPrinterMst )
	{
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		
		kotPrinterMst.setCompanyMst(companyMst);
			 return printerMstRepository.save(kotPrinterMst);
	}
	
	@DeleteMapping("{companymstid}/deleteprintermstmst/{id}")
	public void PrinterMst(@PathVariable(value = "id") String id) {
		printerMstRepository.deleteById(id);
		printerMstRepository.flush();
		 
	}
	
	
	@GetMapping("{companyid}/printermstresource/findallprintermst")
	public List<PrinterMst> getPrinterMst(
			@PathVariable (value = "companyid") String companyid  
			)
			
	{
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return  printerMstRepository.findByCompanyMst(companyMst);
		
	}
	

	@GetMapping("{companyid}/printermstresource/fetchbyprintername/{printername}")
	public  PrinterMst getKotCategoryMstByCategoryId(
			@PathVariable (value = "companyid") String companyid , 
			@PathVariable (value = "printername") String printername) 
	{
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return  printerMstRepository.findByCompanyMstAndPrinterName(companyMst,printername);
		
	}
	
	@GetMapping("{companyid}/printermstresource/fetchbyprinterid/{printerid}")
	public  PrinterMst getPrinterById(
			@PathVariable (value = "companyid") String companyid , 
			@PathVariable (value = "printerid") String printerid) 
	{
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return  printerMstRepository.findByCompanyMstAndId(companyMst,printerid);
		
	}
}
