package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.SalesOrderReceipts;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AdvanceReceiptsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.utils.SystemSetting;
@RestController
@Transactional
public class AdvanceReceiptsResource {

	@Autowired
	private  AdvanceReceiptsRepository  advanceReceiptsRepo;
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@GetMapping("/{companymstid}/advancesalesreceipt")
	public List<SalesOrderReceipts> retrieveAllAcceptStock(@PathVariable(value = "companymstid") String
			  companymstid){
		return advanceReceiptsRepo.findByCompanyMstId(companymstid);
	}
	
	@PostMapping("/{companymstid}/advancesalesreceipts")
	public SalesOrderReceipts createDailyStock(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  
			SalesOrderReceipts advanceReceipts)
	{
		
	 	return companyMstRepo.findById(companymstid).map(companyMst-> {
	 		advanceReceipts.setCompanyMst(companyMst);
	return advanceReceiptsRepo.saveAndFlush(advanceReceipts);
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
		

	
	 @GetMapping("/{companymstid}/advancesummary/advancesummary") public List<Object>
	  getAdvanceSummary(@PathVariable(value = "companymstid") String
			  companymstid, @RequestParam("reportdate") String reportdate){
	  
	  
	  java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd"
	  );
	  
	  
	  return advanceReceiptsRepo.findAdvanceReceipt(date,companymstid) ; }
	
}
