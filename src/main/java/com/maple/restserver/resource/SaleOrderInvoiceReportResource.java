package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.SaleOrderinoiceReport;
import com.maple.restserver.service.SalesOrderInvoiceReportServiceImpl;
import com.maple.restserver.utils.SystemSetting;
@RestController
@Transactional
public class SaleOrderInvoiceReportResource {
	
	@Autowired
	SalesOrderInvoiceReportServiceImpl salesOrderInvoiceReportServiceImpl;
	
	@GetMapping("{companymstid}/salesorderinvoicereportresource/{vNo}/{branchcode}")
	public List<SaleOrderinoiceReport> getSalesOrderInvoice(@PathVariable("vNo") String voucherNumber,
			@RequestParam("rdate") String reportdate,
			@PathVariable("branchcode")String branchcode,
			@PathVariable(value = "companymstid")String companymstid) {
			Date date = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");
	
		return salesOrderInvoiceReportServiceImpl.getSalesOrderInvoice(companymstid,voucherNumber, date);

	}

}
