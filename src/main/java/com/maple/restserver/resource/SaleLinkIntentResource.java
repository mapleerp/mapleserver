package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SaleLinkIntent;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.SaleLinkIntentRepository;
@RestController
@Transactional
public class SaleLinkIntentResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired 
	SaleLinkIntentRepository saleLinkIntentRepository;
	
	@GetMapping("{companymstid}/salelinkintent/getallsalelinkintent")
	public List<SaleLinkIntent> retrieveAllSaleLinkIntent(
			@PathVariable(value = "companymstid") String
			  companymstid){
		
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		return saleLinkIntentRepository.findByCompanyMst(companyMstOpt.get());
	}
	
	
	@PostMapping("{companymstid}/salelinkintent/savesalelinkintent")
	public SaleLinkIntent createSaleLinkIntent(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  SaleLinkIntent saleLinkIntent)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			saleLinkIntent.setCompanyMst(companyMst);
		
		
		return saleLinkIntentRepository.saveAndFlush(saleLinkIntent);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));
		
		
		
	
	}
	
	
	
	

}
