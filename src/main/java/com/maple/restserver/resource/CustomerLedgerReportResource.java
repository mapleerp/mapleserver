package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.customerLedgerReport;
import com.maple.restserver.report.entity.supplierLedger;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.CustomerLedgerReportService;
import com.maple.restserver.utils.SystemSetting;
@RestController
@Transactional
public class CustomerLedgerReportResource {
	@Autowired
	CustomerLedgerReportService custLedgerService;
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	//not used
	@GetMapping("{companymstid}/getcustomerledgerrepot/{customerid}/{branchcode}")
	public List<customerLedgerReport> getSupplierLedgerReport(@PathVariable("customerid") String customerid,
			@RequestParam("startdate") String startdate,
			@RequestParam("enddate") String enddate,
			@PathVariable(value = "companymstid") String  companymstid,@PathVariable(value = "branchcode") String  branchcode) {
		java.sql.Date date = SystemSetting.StringToSqlDate(startdate, "yyyy-MM-dd");
Optional<CompanyMst> companyMst=companyMstRepository.findById(companymstid);

		return custLedgerService.getCustomerLedgerReport(companyMst.get(),customerid, startdate,enddate,branchcode);

	}

}
