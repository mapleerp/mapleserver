package com.maple.restserver.resource;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CommonReceiveMsgEntity;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.KitDefinitionHdrAndDtlMessageEntity;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.SaveAndPublishServiceImpl;
import com.maple.restserver.service.kitdefinionservice;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class KItDefinitionMstResource {
	
private static final Logger logger = LoggerFactory.getLogger(KItDefinitionMstResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	@Autowired
	private KitDefinitionMstRepository kitdefMstRepository;

	// @Autowired
	// private RuntimeService runtimeService;

	@Autowired
	kitdefinionservice kitdefinionservice;
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	ItemMstRepository itemMstRepository;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	CategoryMstRepository categoryMstRepository;

	@Autowired
	KitDefenitionDtlRepository kitdefinitionDetailRepo;
	
	 @Autowired
	 SaveAndPublishService saveAndPublishService;

	 @Autowired
		SaveAndPublishServiceImpl saveAndPublishServiceImpl;
	
	@Autowired
	private ExternalApi externalApi;
	Pageable topFifty =   PageRequest.of(0, 50);

	@PostMapping("{companymstid}/kitdefinitionmst")
	public KitDefinitionMst createKitdefinitionMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody KitDefinitionMst kitdefMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			kitdefMst.setCompanyMst(companyMst);
			return kitdefMstRepository.save(kitdefMst);
//			return saveAndPublishService.saveKitDefinitionMst(kitdefMst, mybranch);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));

	}

	@GetMapping("{companymstid}/kitdefinitionmst/{kitdefinitionMstName}/{branchcode}")
	public KitDefinitionMst retrieveAllKitdefinitionByName(
			@PathVariable(value = "kitdefinitionMstName") String kitdefinitionMstName) {
		return kitdefMstRepository.findByKitName(kitdefinitionMstName);

	}

//	 @GetMapping("{companymstid}/kitdefinitionmstbyitemid/{itemid}")
//		public KitDefinitionMst retrieveAllKitdefinitionByNameByItemId(
//				@PathVariable(value ="itemid") String itemid) {
//			return kitdefMstRepository.findByItemId(itemid);
//
//		}
//		

	@PutMapping("{companymstid}/kitdefinitionmst/{kitdefinitionmstid}")
	public KitDefinitionMst kitdefinitonMstUpdate(@PathVariable(value = "kitdefinitionmstid") String kitdefinitionmstid,
			@Valid @RequestBody KitDefinitionMst kitdefinitionRequest) {
		KitDefinitionMst mst = kitdefMstRepository.findById(kitdefinitionmstid).orElseThrow(
				() -> new ResourceNotFoundException("kitdefinitionmstid " + kitdefinitionmstid + " not found"));
		mst.setBarcode(kitdefinitionRequest.getBarcode());
		mst.setItemCode(kitdefinitionRequest.getItemCode());
		mst.setKitName(kitdefinitionRequest.getKitName());
		mst.setMinimumQty(kitdefinitionRequest.getMinimumQty());
		mst.setUnitName(kitdefinitionRequest.getUnitName());
		mst.setProductionCost(kitdefinitionRequest.getProductionCost());
		mst.setTaxRate(kitdefinitionRequest.getTaxRate());
		mst.setUnitId(kitdefinitionRequest.getUnitId());
		KitDefinitionMst updateMst = kitdefMstRepository.save(mst);
		
		List<KitDefenitionDtl> kitDefDtlList = kitdefinitionDetailRepo.findBykitDefenitionmstId(updateMst.getId());
		KitDefinitionHdrAndDtlMessageEntity kitDefinitionHdrAndDtlMessageEntity = new KitDefinitionHdrAndDtlMessageEntity();
		kitDefinitionHdrAndDtlMessageEntity.setKitDefinitionMst(updateMst);
		kitDefinitionHdrAndDtlMessageEntity.getKitDefinitionDtlList().addAll(kitDefDtlList);
		
		
		saveAndPublishServiceImpl.publishObjectToKafkaEvent(kitDefinitionHdrAndDtlMessageEntity, 
				updateMst.getBranchCode(),
					KafkaMapleEventType.KITDEFINITIONHDRANDDTL, 
					KafkaMapleEventType.ALL,updateMst.getId());
		

		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("voucherNumber", updateMst.getId());
		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", updateMst.getId());
		variables.put("companyid", updateMst.getCompanyMst());
		variables.put("branchcode", updateMst.getBranchCode());

		variables.put("REST", 1);

		variables.put("WF", "forwardKitDefinition");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

		// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));

		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardKitDefinition");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);
		/*
		 * ProcessInstanceWithVariables pVariablesInReturn =
		 * runtimeService.createProcessInstanceByKey("Forwardkitdef")
		 * .setVariables(variables)
		 * 
		 * .executeWithVariablesInReturn();
		 */

		return updateMst;
	}

	@GetMapping("{companymstid}/kitdefinitionmst/{itemid}/kitdefinitionmst/{branchcode}")
	public KitDefinitionMst retrieveAllKitdefinitionByItemId(@PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "branchcode") String branchcode)

	{
		KitDefinitionMst kitDefMst = null;
		List<KitDefinitionMst> kitMstList = kitdefMstRepository.findByItemId(itemid);
		if (kitMstList.size() > 0) {
			kitDefMst = kitMstList.get(0);
		}
		return kitDefMst;

	}

	@GetMapping("{companymstid}/kitdefinitionmst/{kitdefinitionMstName}/kitname/{branchcode}")
	public Optional<KitDefinitionMst> retrieveAllKitdefinitionByName(
			@PathVariable(value = "kitdefinitionMstName") String kitdefinitionMstName,
			@PathVariable(value = "branchcode") String branchcode, Pageable pageable) {
		return kitdefMstRepository.findById(kitdefinitionMstName);
	}

	// version3.6 sari
	@GetMapping("{companymstid}/kitdefinitionmst/findallkitname/{branchcode}")
	public List<KitDefinitionMst> retrieveAllKitdefinition(@PathVariable(value = "companymstid") String companyMstId,
			@PathVariable(value = "branchcode") String branchcode) {
		CompanyMst companyMst = companyMstRepo.findById(companyMstId).get();
		return kitdefMstRepository.findAllByCompanyMstAndBranchCode(companyMst, branchcode);

	}

	@GetMapping("{companymstid}/kitdefinitionmst/findallkitnamebyname/{branchcode}")
	public List<KitDefinitionMst> retrieveAllKitdefinitionByName(
			@PathVariable(value = "companymstid") String companyMstId,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam(value = "data") String data) {
		data = data.replaceAll("20%", " ");
		CompanyMst companyMst = companyMstRepo.findById(companyMstId).get();
		return kitdefMstRepository.findAllByCompanyMstAndKitName(companyMst, "%" + data.toLowerCase() + "%");
	}
	// version3.6ends

	@DeleteMapping("{companymstid}/kitdefinitionmst/deletekitbyid/{id}")
	public void deleteKitById(@PathVariable(value = "companymstid") String companyMstId,
			@PathVariable(value = "id") String id) {
		kitdefMstRepository.deleteById(id);

	}
	//--------------------------------------------
	/*
	 * Publish to Branches
	 */

	@GetMapping("{companymstid}/kitdefinitionmstresource/itemmst/allitem/{itemid}")
	public String publishAllKitDefinition(
			@RequestParam("branchcode")String branchcode,
			@PathVariable(value = "itemid") String itemid) {

		String[] branchcodeList = branchcode.split(";");

		for (String branch : branchcodeList) {

			ArrayList<ItemMst> itemsList = new ArrayList<ItemMst>();
			ArrayList<CategoryMst> catList = new ArrayList<CategoryMst>();
			Optional<KitDefinitionMst> kitdefinitionMst = kitdefMstRepository.findById(itemid);
			
			//Optional<ItemMst> itemMst = itemMstRepository.findById(itemid);
		Optional<ItemMst> itemMst = itemMstRepository.findById(kitdefinitionMst.get().getItemId());
			ItemMst item = itemMst.get();
			Optional<CategoryMst> category = categoryMstRepository.findById(item.getCategoryId());
			CategoryMst categoryM = category.get();
			catList.add(categoryM);
			itemsList.add(item);
			String id = kitdefinitionMst.get().getId();
			List<KitDefenitionDtl> kitDtl = kitdefinitionDetailRepo.findBykitDefenitionmstId(id);

			for (int i = 0; i < kitDtl.size(); i++) {
				Optional<ItemMst> itemOpt = itemMstRepository.findById(kitDtl.get(i).getItemId());
				if (itemOpt.isPresent()) {
					itemsList.add(itemOpt.get());

					Optional<CategoryMst> categoryMst = categoryMstRepository.findById(itemOpt.get().getCategoryId());
					if (categoryMst.isPresent()) {
						CategoryMst categorymst = categoryMst.get();
						catList.add(categorymst);
					}
				}
			}
			try {
				
				
				CommonReceiveMsgEntity commonReceiveCatMsgEntity=new CommonReceiveMsgEntity();
				commonReceiveCatMsgEntity.setCategoryMst(catList);
				//jmsTemplate.convertAndSend(branch + ".common", commonReceiveCatMsgEntity);

				CommonReceiveMsgEntity commonReceiveItemMsgEntity=new CommonReceiveMsgEntity();
				commonReceiveItemMsgEntity.setItemMst(itemsList);
				//jmsTemplate.convertAndSend(branch + ".common", commonReceiveItemMsgEntity);

				CommonReceiveMsgEntity commonReceiveKitMsgEntity=new CommonReceiveMsgEntity();
				commonReceiveKitMsgEntity.setKitDefenitionDtl((ArrayList<KitDefenitionDtl>) kitDtl);
				//jmsTemplate.convertAndSend(branch + ".common", commonReceiveKitMsgEntity);
				
				

			} catch (Throwable e) {
				System.out.print(e.toString());
			}

		}
		return null;
	}
	
	
	
	 @GetMapping("{companymstid}/kitdefinitionmstresource/getkitdefinitionbyitemid/{id}")
	 public List<KitDefinitionMst>  KitDefinitionMst(
				@PathVariable(value = "id") String id) {
		 
		 return kitdefMstRepository.findByItemId(id);
	 }
	 
	 
	 @GetMapping("{companymstid}/kitdefinitionmstresource/getkitdefinition")
	 public List<KitDefinitionMst>  getKitDefinitionMst(
				) {
		 
		 return kitdefMstRepository.getKitDefinitionMst();
	 }
	 
	 
	//-------------------------------new url by using publish offline----------------
	 
	 @PostMapping("{companymstid}/kitdefinitionmst/{status}")
		public KitDefinitionMst sendOffLineMessageToCloud(@PathVariable(value = "companymstid") String companymstid,
				@Valid @RequestBody KitDefinitionMst kitDefinitionMstRequest,@PathVariable("status") String status) {
		 
		 ResponseEntity<KitDefinitionMst> saved=	externalApi.sendKitDefenitionMstOffLineMessageToCloud(companymstid, kitDefinitionMstRequest);
			
			return saved.getBody();
	 }
	 
	 
	 
	 @PutMapping("{companymstid}/kitdefinitionmst/{kitdefinitionmstid}/{status}")
		public KitDefinitionMst kitdefinitonMstOnlinePublishing(
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "kitdefinitionmstid") String kitdefinitionmstid,
				@PathVariable(value = "status") String status,
				@Valid @RequestBody KitDefinitionMst kitdefinitionRequest) {
//			KitDefinitionMst mst = kitdefMstRepository.findById(kitdefinitionmstid).orElseThrow(
//					() -> new ResourceNotFoundException("kitdefinitionmstid " + kitdefinitionmstid + " not found"));
//			mst.setBarcode(kitdefinitionRequest.getBarcode());
//			mst.setItemCode(kitdefinitionRequest.getItemCode());
//			mst.setKitName(kitdefinitionRequest.getKitName());
//			mst.setMinimumQty(kitdefinitionRequest.getMinimumQty());
//			mst.setUnitName(kitdefinitionRequest.getUnitName());
//			mst.setProductionCost(kitdefinitionRequest.getProductionCost());
//			mst.setTaxRate(kitdefinitionRequest.getTaxRate());
//			mst.setUnitId(kitdefinitionRequest.getUnitId());
//			KitDefinitionMst updateMst = kitdefMstRepository.saveAndFlush(mst);

			 ResponseEntity<KitDefinitionMst> saved=	externalApi.sendKitDefenitionMstUpdateOffLineMessageToCloud(
					 companymstid, kitdefinitionmstid,kitdefinitionRequest);
				
				return saved.getBody();
		}
	 
	
}
