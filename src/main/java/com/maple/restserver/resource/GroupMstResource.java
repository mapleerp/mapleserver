package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.GroupMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.GroupMstRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
public class GroupMstResource {
private static final Logger logger = LoggerFactory.getLogger(GroupMstResource.class);
	
	@Value("${mybranch}")
	private String mybranch;

  @Autowired
  GroupMstRepository groupMstRepo;
  
  
  
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	 @Autowired
	 SaveAndPublishService saveAndPublishService;
	
	@PostMapping("{companymstid}/group")
	public GroupMst createGroup(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 GroupMst groupMst)
	{
		
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			groupMst.setCompanyMst(companyMst);
//       return groupMstRepo.saveAndFlush(groupMst);
			return saveAndPublishService.saveGroupMst(groupMst, groupMst.getBranchCode());
		 
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); 
		/*
		 * 
		 * Work flow ForwardGroup
		 */
		
	}
	

	
	@GetMapping("{companymstid}/allgroup")
	public List<GroupMst> retrieveAllGroup(@PathVariable(value = "companymstid") String
			 companymstid)
	{
		return groupMstRepo.findByCompanyMstId(companymstid);
	};
	
	@DeleteMapping("{companymstid}/groupmst/{id}")
	public void groupMstDelete(@PathVariable(value = "id") String Id) {
		groupMstRepo.deleteById(Id);

	}


	@GetMapping("{companymstid}/groupmstbyid/{id}")
	public  @ResponseBody Optional<GroupMst> groupMstbyId(@PathVariable(value = "companymstid") String
			 companymstid,
			 @PathVariable(value = "id") String id){
		return groupMstRepo.findByIdAndCompanyMstId(id,companymstid);
	}
	

	@GetMapping("{companymstid}/groupmstbyname/{name}")
	public  @ResponseBody Optional<GroupMst> groupMstbyGroupName(@PathVariable(value = "companymstid") String
			 companymstid,
			 @PathVariable(value = "name") String name){
		return groupMstRepo.findByGroupNameAndCompanyMstId(name,companymstid);
	}
	
	

	
}
