package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SchEligibilityAttribInst;
import com.maple.restserver.entity.SchOfferAttrInst;
import com.maple.restserver.entity.SchSelectionAttribInst;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SchOfferAttrInstRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
public class SchOfferAttrInstResource {
	
private static final Logger logger = LoggerFactory.getLogger(PurchaseHdrResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	SchOfferAttrInstRepository schOfferAttrInstRepo;


	@PostMapping("{companymstid}/schofferattrinst")
	public SchOfferAttrInst createschSchOfferAttrInst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  SchOfferAttrInst  schOfferAttrInst)
	{
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		
		schOfferAttrInst.setCompanyMst(companyMst.get());
		
			return  schOfferAttrInstRepo.save(schOfferAttrInst);
//		return saveAndPublishService.saveSchOfferAttrInst(schOfferAttrInst, mybranch);
		
			
		}
	
	@GetMapping("{companymstid}/allschofferattrinst")
	public  @ResponseBody List<SchOfferAttrInst> findAllSchOfferAttrInst(@PathVariable(value = "companymstid") String
			 companymstid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schOfferAttrInstRepo.findByCompanyMst(companyMst.get());
	}
	
	@GetMapping("{companymstid}/schofferattrinstbyattrname/{attribname}/{schemeid}")
	public  @ResponseBody SchOfferAttrInst findAllSchOfferAttrInstByAttrbName(@PathVariable(value = "companymstid") String
			 companymstid , @PathVariable(value = "attribname") String attribname, 
			 @PathVariable(value = "schemeid") String schemeid
			 
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schOfferAttrInstRepo.findByCompanyMstAndAttributeNameAndSchemeId(companyMst.get(),attribname,schemeid);
	}
	
	@GetMapping("{companymstid}/allschofferattrinstbyschemeid/{schemeid}")
	public  @ResponseBody List<SchOfferAttrInst> findAllSchSchOfferAttrInstBySchemeId(@PathVariable(value = "companymstid") String
			 companymstid,  @PathVariable(value = "schemeid") String schemeid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schOfferAttrInstRepo.findByCompanyMstAndSchemeId(companyMst.get(), schemeid);
	}
	
	@DeleteMapping("{companymstid}/deleteschofferattrinst/{id}")
	public void SchOfferAttrInst(@PathVariable(value = "id") String id) {
		
		Optional<SchOfferAttrInst> schOfferAttrInst=schOfferAttrInstRepo.findById(id);
		schOfferAttrInstRepo.deleteById(id);
		schOfferAttrInstRepo.flush();
		saveAndPublishService.publishObjectDeletion(schOfferAttrInst.get(), 
				mybranch, KafkaMapleEventType.SCHOFFERATTRINSTDELETE,schOfferAttrInst.get().getId());
		 

	}
	
	@GetMapping("{companymstid}/allschofferattrinstbyschemeidandofferid/{schemeid}/{offerid}")
	public  @ResponseBody List<SchOfferAttrInst> findAllSchSchOfferAttrInstBySchemeIdAndOfferId(@PathVariable(value = "companymstid") String
			 companymstid,  @PathVariable(value = "schemeid") String schemeid,
			 @PathVariable(value = "offerid") String offerid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schOfferAttrInstRepo.findByCompanyMstAndSchemeIdAndOfferId(companyMst.get(), schemeid,offerid);
	}
	
	@GetMapping("{companymstid}/schofferattrinstbyattrnameandofferid/{attribname}/{schemeid}/{offerid}")
	public  @ResponseBody SchOfferAttrInst findAllSchOfferAttrInstByAttrbNameAndOfferId(@PathVariable(value = "companymstid") String
			 companymstid , @PathVariable(value = "attribname") String attribname, 
			 @PathVariable(value = "schemeid") String schemeid,
			 @PathVariable(value = "offerid") String offerid
			 
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schOfferAttrInstRepo.findByCompanyMstAndAttributeNameAndSchemeIdAndOfferId(companyMst.get(),attribname,schemeid,offerid);
	}
	
}
