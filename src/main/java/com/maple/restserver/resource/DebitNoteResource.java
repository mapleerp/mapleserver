package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DebitNote;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DebitNoteRepository;
import com.maple.restserver.service.DebitNoteService;


@RestController
public class DebitNoteResource {
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	DebitNoteRepository debitNoteRepository;
	
	@Autowired
	DebitNoteService debitNoteService;
	
	@GetMapping("{companymstid}/debitnoteresource/getdebitnotedetails")
	public List<DebitNote>getcreditDetailsByUserId(@PathVariable(value = "companymstid") 
	String companymstid
			)
	{
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		
		return debitNoteService.getDebitNoteItems(companyMstOpt.get());
	}

	@PostMapping("{companymstid}/debitnoteresource/savedebitnotedetails")
	public DebitNote createcreditNote(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody DebitNote debitNote)  {

		return companyMstRepository.findById(companymstid).map(companyMst -> {
			debitNote.setCompanyMst(companyMst);
			return debitNoteRepository.saveAndFlush(debitNote);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	@DeleteMapping("{companymstid}/debitnoteresource/deletedebitnotedetails/{id}")
	public void CreditNoteDeleteById(@PathVariable(value = "id") String Id) {
		debitNoteRepository.deleteById(Id);

	}

}



