package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.his.entity.DoctorMst;
import com.maple.restserver.his.entity.NewDoctorMst;
import com.maple.restserver.repository.DoctorMstRepository;




@RestController
@Transactional
public class DoctorMstResource {
	@Autowired
	private DoctorMstRepository docmst;
	@GetMapping("{companymstid}/doctormst")
	public List<DoctorMst> retrieveAlldepartments()
	{
		return docmst.findAll();
		
	}
	@PostMapping("{companymstid}/doctormst")
	public ResponseEntity<Object>createUser(@Valid @RequestBody NewDoctorMst doctormst1)
	{
		DoctorMst saved=docmst.saveAndFlush(doctormst1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}
//	@Autowired
//	private DoctorMstRepository docmst;
//	@GetMapping("{companymstid}/doctormst")
//	public List<NewDoctorMst> retrieveAlldepartments()
//	{
//		return docmst.findAll();
//		
//	}
//	@PostMapping("{companymstid}/doctormst")
//	public ResponseEntity<Object>createUser(@Valid @RequestBody NewDoctorMst doctormst1)
//	{
//		NewDoctorMst saved=docmst.saveAndFlush(doctormst1);
//		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
//				buildAndExpand(saved.getId()).toUri();
//	return ResponseEntity.created(location).build();
//	
//	}

}
