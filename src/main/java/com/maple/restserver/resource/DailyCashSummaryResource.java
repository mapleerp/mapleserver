package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.DailyCashSummary;
import com.maple.restserver.repository.DailyCashSummaryRepository;

@RestController
public class DailyCashSummaryResource {

	@Autowired
	private DailyCashSummaryRepository dailycashsummaryRepo;
	
	
	@GetMapping("{companymstid}/dailycashsummary/{reportdate}/dailycashsummary")
	public List<DailyCashSummary> getDailyOrderSummary(@PathVariable("reportdate") Date reportdate){
		return  dailycashsummaryRepo.getDailyCashSummaryByReportDate(reportdate) ;
	}
}
