package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LanguageMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LanguageMstRepository;




@RestController
@Transactional
public class LanguageMstResource {
	@Autowired
	private LanguageMstRepository languageMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@PostMapping("{companymstid}/languagemst")
	public LanguageMst creatLanguageMst(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody LanguageMst languageMst )
	{
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		
		languageMst.setCompanyMst(companyMst);
			 return languageMstRepository.save(languageMst);

}
	
	@GetMapping("{companyid}/languagemsts")
	public  List<LanguageMst> getLanguageMsts(
			@PathVariable (value = "companyid") String companyid) 
	{
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return  languageMstRepository.findByCompanyMst(companyMst);
		
	}
	
	@DeleteMapping("{companymstid}/deletelanguagemst/{id}")
	public void LanguageMst(@PathVariable(value = "id") String id) {
		languageMstRepository.deleteById(id);
		languageMstRepository.flush();
		 

	}
	
	@GetMapping("{companyid}/languagemstsbylangname/{langname}")
	public  LanguageMst getLanguageMstsByLangName(
			@PathVariable (value = "companyid") String companyid , 
			@PathVariable (value = "langname") String langname) 
	{
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return  languageMstRepository.findByCompanyMstAndLanguageName(companyMst,langname);
		
	}
	
	

}
