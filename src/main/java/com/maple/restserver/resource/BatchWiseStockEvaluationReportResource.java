package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.BatchWiseStockEvaluationReport;
import com.maple.restserver.service.BatchWiseStockEvaluationReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class BatchWiseStockEvaluationReportResource {
	
	
	
	@Autowired
	
	BatchWiseStockEvaluationReportService batchWiseStockEvaluationReportService;
	
	
	
	@GetMapping("{companymstid}/batchwisestockevaluationreportresource/{itemid}/{batch}")
	public List<BatchWiseStockEvaluationReport> getBatchWiseStockEvaluationReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("itemid") String itemid ,
			 @PathVariable("batch") String batch ,
			 @RequestParam("fromdate") String fromDate,
				@RequestParam("todate") String toDate) {
			
			java.util.Date startdate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
			java.util.Date enddate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");

		return batchWiseStockEvaluationReportService.getStockByCompanyMstIdAndDate(itemid,batch,startdate,enddate);



		
	}
	
	
	
}
