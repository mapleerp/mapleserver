package com.maple.restserver.resource;

import java.net.URI;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.ItemBranchHdr;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.LmsQueueTallyMst;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.JournalHdrAndDtlMessage;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.IntentHdrRepository;
import com.maple.restserver.repository.JournalDtlRepository;
import com.maple.restserver.repository.JournalHdrRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.LmsQueueTallyMstRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.service.IntentService;
import com.maple.restserver.service.IntentServiceImpl;
import com.maple.restserver.service.JournalHdrServiceImpl;
import com.maple.restserver.service.SaveAndPublishServiceImpl;
import com.maple.restserver.service.accounting.task.JournalAccounting;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.utils.EventBusFactory;

@RestController
@Transactional
public class JournalHdrResource {
	private static final Logger logger = LoggerFactory.getLogger(JournalHdrResource.class);
	
	@Autowired 
	JournalAccounting journalAccounting;
	
	@Autowired
	JournalHdrServiceImpl journalHdrServiceImpl;
	
	@Autowired
	 JournalHdrRepository journalHdrRepository;
	
	@Autowired
	LmsQueueTallyMstRepository lmsQueueTallyMstRepository;
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	JournalDtlRepository journalDtlRepository;
	 //@Autowired
	  //private RuntimeService runtimeService;
	EventBus eventBus = EventBusFactory.getEventBus();
	 
	@Autowired
	SaveAndPublishServiceImpl saveAndPublishServiceImpl;
	
	@GetMapping("{companymstid}/journalhdrs")
	public List<JournalHdr> retrieveJournalHdr(){
		
		return journalHdrRepository.findAll();
		
		
	}
	
	@PostMapping("{companymstid}/journalhdr")
	public JournalHdr createJournalHdr(
			@PathVariable(value = "companymstid") String companymstid, 
			@Valid @RequestBody  JournalHdr journalHdr)
	{
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		journalHdr.setCompanyMst(companyMst);
		System.out.print("OK");
		JournalHdr saved = journalHdrRepository.saveAndFlush(journalHdr);

		return saved;
		
		/*
		 * Work Flow forwardJournal
		 */
	}

	@PutMapping("{companymstid}/journalhdr/{journalhdrid}")
	public JournalHdr journalHdrFinalSave(@PathVariable String journalhdrid, 
			@Valid @RequestBody JournalHdr journalHdrRequest)
	{

				
				return journalHdrRepository.findById(journalhdrid).map(journal -> {
 				 
					journal.setVoucherNumber(journalHdrRequest.getVoucherNumber()); 

					journal.setVoucherDate(journalHdrRequest.getVoucherDate());
					journal.setTransDate(journalHdrRequest.getTransDate());
					journal.setBranchCode(journalHdrRequest.getBranchCode());
					JournalHdr journalHdr =   journalHdrRepository.save(journal);
					
					List<JournalDtl> journalDtlList = journalDtlRepository.findByJournalHdr(journalHdr);
					
					JournalHdrAndDtlMessage journalHdrAndDtlMessage = new JournalHdrAndDtlMessage();
					journalHdrAndDtlMessage.setJournalHdr(journalHdr);
					journalHdrAndDtlMessage.getJournalDtlList().addAll(journalDtlList);
					
				
					saveAndPublishServiceImpl.publishObjectToKafkaEvent(journalHdrAndDtlMessage, 
							journalHdrRequest.getBranchCode(),
								KafkaMapleEventType.JOURNALHDRANDDTL, 
								KafkaMapleEventType.SERVER,journalHdr.getId());
					
					
					 
					Map<String, Object> variables = new HashMap<String, Object>();
					
					variables.put("voucherNumber", journalHdr.getVoucherNumber());
					variables.put("id", journalHdr.getId());
					variables.put("voucherDate", journalHdr.getVoucherDate());
					variables.put("inet", 0);
					variables.put("REST", 1);
					variables.put("companyid", journalHdr.getCompanyMst());
					
					variables.put("branchcode", journalHdr.getBranchCode());

					
					
					try {
						journalAccounting.execute(journalHdr.getVoucherNumber(), journalHdr.getVoucherDate(), journalHdr.getId(), journalHdr.getCompanyMst());
					} catch (PartialAccountingException e) {
						// TODO Auto-generated catch block
						logger.error(e.getMessage());
					}
				
					eventBus.post(journalHdr);
					variables.put("WF", "forwardJournal");
					
					
					String workflow = (String) variables.get("WF");
					String voucherNumber = (String) variables.get("voucherNumber");
					String sourceID = (String) variables.get("id");


					LmsQueueMst lmsQueueMst = new LmsQueueMst();

					lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
					//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
					
					java.util.Date uDate = (Date) variables.get("voucherDate");
					java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
					lmsQueueMst.setVoucherDate(sqlVDate);
					
					
					lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
					lmsQueueMst.setVoucherType(workflow);
					lmsQueueMst.setPostedToServer("NO");
					lmsQueueMst.setJobClass("forwardJournal");
					lmsQueueMst.setCronJob(true);
					lmsQueueMst.setJobName(workflow + sourceID);
					lmsQueueMst.setJobGroup(workflow);
					lmsQueueMst.setRepeatTime(60000L);
					lmsQueueMst.setSourceObjectId(sourceID);
					
					lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

					lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
					variables.put("lmsqid", lmsQueueMst.getId());
					eventBus.post(variables);
					LmsQueueTallyMst lmsQueueTallyMst=new LmsQueueTallyMst();
					lmsQueueTallyMst.setCompanyMst((CompanyMst) variables.get("companyid"));
					lmsQueueTallyMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
					lmsQueueTallyMst.setVoucherNumber((String) variables.get("voucherNumber"));
					lmsQueueTallyMst.setVoucherType(workflow);
					lmsQueueTallyMst.setPostedToTally("NO");
					lmsQueueTallyMst.setJobClass("forwardJournal");
					lmsQueueTallyMst.setCronJob(true);
					lmsQueueTallyMst.setJobName(workflow + sourceID);
					lmsQueueTallyMst.setJobGroup(workflow);
					lmsQueueTallyMst.setRepeatTime(60000L);
					lmsQueueTallyMst.setSourceObjectId(sourceID);
					lmsQueueTallyMst.setBranchCode((String) variables.get("branchcode"));
					lmsQueueTallyMst=lmsQueueTallyMstRepository.saveAndFlush(lmsQueueTallyMst);
					variables.put("lmsqid", lmsQueueMst.getId());
					eventBus.post(variables);	
							
					/*
					/*
					ProcessInstanceWithVariables pVariablesInReturn  = runtimeService.createProcessInstanceByKey("Forwardjournal")
							.setVariables(variables)
							 
							.executeWithVariablesInReturn();
							
							*/
					
		            return journalHdr;
	 		            
		            
		        }).orElseThrow(() -> new ResourceNotFoundException("Journal " + journalhdrid + " not found"));

	}

	
	@GetMapping("{companymstid}/retrievealljournalhdr")
	public List<JournalHdr> retrieveAllJournalHdr(
			@PathVariable(value = "companymstid") String companymstid){
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		
		return journalHdrRepository.findByCompanyMst(companyMstOpt.get());
		
		
	}
	
	@GetMapping("{companymstid}/retrievejournalhdrbyid/{hdrid}")
	public JournalHdr retrieveAllJournalHdrById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "hdrid") String hdrid){
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		
		return journalHdrRepository.findByCompanyMstAndId(companyMstOpt.get(),hdrid);
		
		
	}

	
}
