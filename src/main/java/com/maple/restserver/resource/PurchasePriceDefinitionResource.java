package com.maple.restserver.resource;

import java.util.Date;

/**
 *@GetMapping("{companymstid}/acceptstocks")
 * 
 *@PostMapping("{companymstid}/acceptstock")
 *@Valid @RequestBody AcceptStock acceptStock
 * 
 */

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.PurchasePriceDefinitionMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.PurchasePriceDefinitionMstRepository;
import com.maple.restserver.service.PurchasePriceDefinitionService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class PurchasePriceDefinitionResource {
	

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	PurchasePriceDefinitionService purchasePriceDefinitionService;


	@GetMapping("{companymstid}/purchasepricedefinitionmst/purchasepricedefinitionmstbyid/{hdrid}")
	public List<PurchasePriceDefinitionMst> retrievePurchasePriceDefinitionMstByHdrId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "hdrid") String hdrid
			) {
		
		
		return purchasePriceDefinitionService.retrievePurchasePriceDefinitionMstByHdrId(companymstid,hdrid);
	}



}
