package com.maple.restserver.resource;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndClosureDtl;
import com.maple.restserver.entity.DayEndClosureHdr;
import com.maple.restserver.entity.DayEndReportStore;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.DayEndClosureHdrAndDtlMessage;
import com.maple.restserver.message.entity.SummaryOfDailyStock;
import com.maple.restserver.report.entity.CustomerBalanceReport;
import com.maple.restserver.report.entity.DailySalesReport;
import com.maple.restserver.report.entity.SalesReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DayEndClosureDtlRepository;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.repository.DayEndReportStoreRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.DailySaleReportService;
import com.maple.restserver.service.DayEndClosureService;
import com.maple.restserver.service.SaveAndPublishServiceImpl;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class DayEndClosureMstResource {

	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	private DayEndReportStoreRepository dayEndReportStoreRepository;
	@Autowired
	DayEndClosureRepository dayEndClosureRepo;
	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	DayEndClosureService DayEndClosureService;

	@Autowired
	DayEndClosureDtlRepository dayEndClosureDtlRepo;

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	DailySaleReportService dailySaleReportService;

	@Autowired
	SaveAndPublishServiceImpl saveAndPublishServiceImpl;
	
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;
	
	@Autowired
	ExternalApi externalApi;
	
	@Value("${mybranch}")
	private String mybranch;
	
	@PostMapping("{companymstid}/dayendclosure")
	public DayEndClosureHdr createDayEndClosure(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody DayEndClosureHdr dayEndClosure) {

		return companyMstRepo.findById(companymstid).map(companyMst -> {
			dayEndClosure.setCompanyMst(companyMst);

			return dayEndClosureRepo.saveAndFlush(dayEndClosure);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	@GetMapping("{companymstid}/alldayendclosuremst")
	public List<DayEndClosureHdr> retrieveDayEndClosureMstGroup(
			@PathVariable(value = "companymstid") String companymstid) {
		return dayEndClosureRepo.findByCompanyMstId(companymstid);
	};

	@PutMapping("{companymstid}/dayendclosurehdr/{dayendclosurehdrid}")
	public DayEndClosureHdr DayEndClosureFinalSave(@PathVariable String dayendclosurehdrid,
			@PathVariable String companymstid,
			@Valid @RequestBody DayEndClosureHdr dayEndClosureHdrRequest) {
		return dayEndClosureRepo.findById(dayendclosurehdrid).map(dayEndClosureHdr -> {
			
			dayEndClosureHdr.setPhysicalCash(dayEndClosureHdrRequest.getPhysicalCash());
			dayEndClosureHdr.setCardSale(dayEndClosureHdrRequest.getCardSale());
			dayEndClosureHdr.setCardSale2(dayEndClosureHdrRequest.getCardSale2());
			dayEndClosureHdr.setCashSale(dayEndClosureHdrRequest.getCashSale());
			dayEndClosureHdr.setCompanyMst(dayEndClosureHdr.getCompanyMst());
			dayEndClosureHdr.setDayEndStatus(dayEndClosureHdrRequest.getDayEndStatus());
			dayEndClosureHdr.setChangeInCash(dayEndClosureHdrRequest.getChangeInCash());
			dayEndClosureHdr.setCardSettlementAmount(dayEndClosureHdrRequest.getCardSettlementAmount());

			VoucherNumber voucherNo = voucherService.generateInvoice("DAYEND",
					dayEndClosureHdr.getCompanyMst().getId());

			String vcNo = voucherNo.getCode();

			dayEndClosureHdr.setVoucheNumber(vcNo);
			dayEndClosureHdr.setVoucherDate(dayEndClosureHdrRequest.getProcessDate());
			
			java.util.Date date = dayEndClosureHdr.getVoucherDate();
			String branchCode = dayEndClosureHdr.getBranchCode();
			
			List<Object> objList = salesTransHdrRepository.findDailySalesReport
					(date,branchCode);


			for(int i=0; i<objList.size(); i++)
			{
				
				Object[] objArray = (Object[]) objList.get(i);
				if(null != objArray[0])
				{
					dayEndClosureHdr.setTotalSales((Double) objArray[0]);

				}
				
			}
			DayEndClosureHdr saved = dayEndClosureRepo.save(dayEndClosureHdr);
			
			
			dayEndClosureRepo.deleteDayEndByVoucherDate(saved.getCompanyMst(),saved.getVoucherDate());

			List<DayEndClosureDtl> dayEndClosureDtlList = dayEndClosureDtlRepo.findByDayEndClosureHdr(saved);
			
			DayEndClosureHdrAndDtlMessage dayEndClosureHdrAndDtlMessage = new DayEndClosureHdrAndDtlMessage();
			dayEndClosureHdrAndDtlMessage.setDayEndClosureHdr(saved);
			dayEndClosureHdrAndDtlMessage.getDayEndClosureDtlList().addAll(dayEndClosureDtlList);
			
			saveAndPublishServiceImpl.publishObjectToKafkaEvent(dayEndClosureHdrAndDtlMessage, 
					saved.getBranchCode(),
						KafkaMapleEventType.DAYENDCLOSUREHDRANDDTL, 
						KafkaMapleEventType.SERVER,saved.getId());
			
			/*
			 * publishing summary-of-daily-stock
			 */
			DayEndClosureService.dailyStockSummaryToCloud(saved.getBranchCode(),saved.getCompanyMst().getId());
			
			
			

			return saved;
		}).orElseThrow(() -> new ResourceNotFoundException("dayendclosurehdrid " + dayendclosurehdrid + " not found"));

		/*
		 * Work flow
		 * 
		 * forwardDayEnd
		 */

	}

	@GetMapping("/{companymstid}/getdayendclosuremstbydate")
	public DayEndClosureHdr retrieveDayEndClosure(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("voucherdate") String voucherdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(voucherdate, "yyyy-MM-dd");
		return DayEndClosureService.getDayEndDetails(date, companymstid);

	}
	

	@GetMapping("/{companymstid}/dayendclosurehdr/getdayendreport")
	public List<SalesReport> retrieveDayEndReport(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("voucherdate") String voucherdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(voucherdate, "yyyy-MM-dd");
		return DayEndClosureService.getDayEndReport(date, companymstid);

	}

	@GetMapping("/{companymstid}/dayendclosuremst/maxofdayend/{branchcode}")
	public DayEndClosureHdr retrieveMaxOfDayEnd(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {
		List<DayEndClosureHdr> dayEndClosureHdrList = dayEndClosureRepo.getMaxOfDayEnd(companymstid, branchcode);
		DayEndClosureHdr dayEndClosureHdr = null;
		if(dayEndClosureHdrList.size()>0) {
			dayEndClosureHdr = dayEndClosureHdrList.get(0);
		}
		
		return dayEndClosureHdr;

	}

	@GetMapping("/{companymstid}/dayendclosuremst/countanddenomination/{branchcode}")
	public List<Object> retrieveDenominationCount(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam("rdate") String voucherdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(voucherdate, "yyyy-MM-dd");

		return dayEndClosureRepo.getCountofDenomination(companymstid, branchcode, date);

	}

	@GetMapping("/{companymstid}/getdayendclosurestatus/{branchcode}")
	public String retrieveDayEndStatus(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchCode, @RequestParam("voucherdate") String voucherdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(voucherdate, "dd-MM-yyyy");
		return DayEndClosureService.retrieveDayEndStatus(date, branchCode);

	}

	@GetMapping("/{companymstid}/dayendclosuremst/dayendhdrbydate")
	public List<DayEndClosureHdr> findByProcessDate(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("voucherdate") String voucherdate) {
		java.sql.Date date = SystemSetting.StringToSqlDate(voucherdate, "yyyy-MM-dd");

		Optional<CompanyMst> compOpt = companyMstRepo.findById(companymstid);

		return dayEndClosureRepo.findByProcessDateAndCompanyMst(date, compOpt.get());

	}

	@GetMapping("/{companymstid}/gettotalcardandcashsale")
	public DayEndClosureHdr retrieveCardAndCashAmount(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("voucherdate") String voucherdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(voucherdate, "yyyy-MM-dd");
		return DayEndClosureService.getDayEndTotalSalesDetails(date, companymstid);

	}

	@GetMapping("/{companymstid}/dayendclosurehdr/sendsms/{phonenumber}")
	public String sendSms(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "phonenumber") String phonenumber, @RequestParam(value = "rdate") String rdate) {
		java.util.Date udate = SystemSetting.StringToUtilDate(rdate, "yyyy-MM-dd");
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		List<DayEndReportStore> dayEndReportStore = dayEndReportStoreRepository.findByCompanyMstAndDate(companyMst,
				udate);

		String physicalCash = null;
		String cardSale = null;
		String cashSale = null;
		String totalSale = null;
		if (dayEndReportStore.size() > 0) {
			physicalCash = dayEndReportStore.get(0).getPhysicalCash();
			cardSale = dayEndReportStore.get(0).getCardSale();
			cashSale = dayEndReportStore.get(0).getCash();
			totalSale = dayEndReportStore.get(0).getTotalSales();

		}

		if (null == physicalCash || physicalCash.equalsIgnoreCase(null)) {
			physicalCash = "0.0";
		}
		if (null == cardSale || cardSale.equalsIgnoreCase(null)) {
			cardSale = "0.0";
		}
		if (null == cashSale || cashSale.equalsIgnoreCase(null)) {
			cashSale = "0.0";
		}
		if (null == totalSale || totalSale.equalsIgnoreCase(null)) {
			totalSale = "0.0";
		}
		String messageBody = "PhysicalCash=" + physicalCash + "Card Sale=" + cardSale + "Cash Sale=" + cashSale
				+ "Total Sales=" + totalSale;

		return "Done";

	}
	
	/*
	 * url to fetch stock summary from cloud to item batch dtl.........
	 */
	@GetMapping("{companymstid}/dayendclosuremstresource/getdailystocksummaryfromcloudtoitembatchdtl")
	public void getDailyStockSummaryFromCloudToItemBatchDtl(@PathVariable(
			value = "companymstid") String companymstid
			) {

		externalApi.getDailyStockSummaryFromCloudToItemBatchDtl();

	}
	
	
	/*
	 * url to sent stock summary to cloud.........
	 */
	@GetMapping("{companymstid}/dayendclosuremstresource/dailystocksummarytocloud")
	public void dailyStockSummaryToCloud(
			@PathVariable(value = "companymstid") String companymstid) 
	{
		DayEndClosureService.dailyStockSummaryToCloud(mybranch,companymstid);
	}

}
