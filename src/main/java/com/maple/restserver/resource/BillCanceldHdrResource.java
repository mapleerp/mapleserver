package com.maple.restserver.resource;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.maple.restserver.entity.BillCanceldHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.BillCanceldHdrRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
@Transactional
public class BillCanceldHdrResource {
@Autowired
BillCanceldHdrRepository billCanceldHdrRepository;

@Autowired
CompanyMstRepository companyMstRepository;

@PostMapping("{companymstid}/billcanceldhdr")
public BillCanceldHdr createBillCanceldHdr(@PathVariable(value = "companymstid") String
		  companymstid,@Valid @RequestBody BillCanceldHdr billCanceldHdr)
{
	return companyMstRepository.findById(companymstid).map(companyMst-> {
		billCanceldHdr.setCompanyMst(companyMst);

  return billCanceldHdrRepository.saveAndFlush(billCanceldHdr);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			 companymstid + " not found")); 
	}

}
