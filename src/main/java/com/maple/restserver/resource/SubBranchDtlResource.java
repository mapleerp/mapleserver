package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SubBranchSalesDtl;
import com.maple.restserver.entity.SubBranchSalesTransHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.HsnCodeSaleReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SubBranchSalesDtlRepository;
import com.maple.restserver.repository.SubBranchSalesHdrRepository;
import com.maple.restserver.service.SubBranchSalesDtlService;
@RestController
@Transactional
public class SubBranchDtlResource {
	
	@Autowired
	SubBranchSalesHdrRepository spencerSalesHdrRepository;
	
	@Autowired
	SubBranchSalesDtlRepository spencerSalesDtlRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	SubBranchSalesDtlService spencerSalesDtlService;
	
	
	@PostMapping("{companymstid}/spencersalesdtl/{salestranshdrId}")
	public SubBranchSalesDtl createSalesDtl(
			@PathVariable(value = "salestranshdrId") String salestranshdrId,
			@PathVariable(value = "companymstid") String companymstid, 
			@Valid @RequestBody SubBranchSalesDtl spencerSalesDtl) {

		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		Optional<SubBranchSalesTransHdr> spencerSalesTransHdr = spencerSalesHdrRepository.findById(salestranshdrId);
	
		spencerSalesDtl.setCompanyMst(companyMst.get());
		spencerSalesDtl.setSubBranchSalesTransHdr(spencerSalesTransHdr.get());
		
		return spencerSalesDtlRepository.saveAndFlush(spencerSalesDtl);
	
	}
	
	@GetMapping("{companymstid}/spencersalesdtl/spensersalesdtlbybranchcode/{branchcode}")
	public List<SubBranchSalesDtl> findSpneserDtlBySpenserIdAndDate(
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "fdate") String fdate, 
			@RequestParam(value = "tdate") String tdate) {
		
		java.util.Date fudate = ClientSystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		java.util.Date ftdate = ClientSystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");

		return spencerSalesDtlService.getSpneserDtlBySpenserIdAndDate(branchcode,companymstid,fudate,ftdate);
	}

}
