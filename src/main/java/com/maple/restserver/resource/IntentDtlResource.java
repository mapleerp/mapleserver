package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.IntentDtlRepository;
import com.maple.restserver.repository.IntentHdrRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Transactional
public class IntentDtlResource {
	
	private static final Logger logger = LoggerFactory.getLogger(IntentDtlResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	private IntentDtlRepository IntentDtlRepo ;

	@Autowired
	private IntentHdrRepository intentHdrRepository;
	
	@Autowired
	private CompanyMstRepository companyMstRepository ;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;


	@GetMapping("{companymstid}/intenthdr/{intenthdrId}/intentdtl")
	public List<IntentDtl> retrieveAllIntentDtlByIntentHdrID(
			@PathVariable(value = "intenthdrId") String intenthdrId) {
		return IntentDtlRepo.findByIntentHdrId(intenthdrId );

	}

	@PostMapping("{companymstid}/intenthdr/{intentHdrId}/intentdtl")
	public IntentDtl createIntentDtl(@PathVariable(value = "intentHdrId") String intentHdrId,
			@Valid @RequestBody IntentDtl intentDtlRequest) {

//		return intentHdrRepository.findById(intentHdrId).map(intenthdr -> {
//			intentDtlRequest.setIntentHdr(intenthdr);
		//	return IntentDtlRepo.saveAndFlush(intentDtlRequest);
			return saveAndPublishService.saveIntentDtl(intentDtlRequest, mybranch, intentDtlRequest.getToBranch());
//		}).orElseThrow(() -> new ResourceNotFoundException("intentHdrId " + intentHdrId + " not found"));

	}

	// here we are deleting intent detail by using itemId

	@DeleteMapping("{companymstid}/intenthdrdelete/{id}")
	public void IntentDetailDelete(@PathVariable(value = "id") String Id) {
		IntentDtlRepo.deleteById(Id);

	}
	
	@GetMapping("{companymstid}/intentdtl/intentdtlbyid/{id}")
	public IntentDtl retrieveAllIntentDtlById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {
		
		Optional<IntentDtl>  intentDtl = IntentDtlRepo.findById(id);
		return intentDtl.get();

	}
	
	
}
