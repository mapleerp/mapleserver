package com.maple.restserver.resource;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.ReceivableDtl;
import com.maple.restserver.repository.ReceivableDtlRepository;

@RestController
@Transactional
public class ReceivableDtlResource {
	@Autowired
	private ReceivableDtlRepository receivableDtl;
	@GetMapping("{companymstid}/receivabledtls")
	public List<ReceivableDtl> retrieveAllreceivabledtls()
	{
		return receivableDtl.findAll();
	}
	@PostMapping("{companymstid}/receivabledtl")
	public ResponseEntity<Object> createUser(@Valid @RequestBody ReceivableDtl receivabledtl1)
	{
		ReceivableDtl saved=receivableDtl.saveAndFlush(receivabledtl1);
	URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	}

}

