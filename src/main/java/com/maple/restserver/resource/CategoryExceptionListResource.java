package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CategoryExceptionList;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CategoryExceptionListRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
@RestController
@Transactional

public class CategoryExceptionListResource {
	
	
	@Autowired
	private CategoryExceptionListRepository categoryExceptionListRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	@GetMapping("{companymstid}/categoryexceptionlist/{branchcode}")
	public List<CategoryExceptionList> retrieveAllAcceptStock(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode){
		
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		return categoryExceptionListRepository.findByCompanyMst(companyMstOpt.get());
	}
	
	
	  
	
	
	@PostMapping("{companymstid}/savecategoryexceptionlist")
	public CategoryExceptionList createCategoryExceptionList(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody CategoryExceptionList categoryExceptionList)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			categoryExceptionList.setCompanyMst(companyMst);
		
		
		return categoryExceptionListRepository.save(categoryExceptionList);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));
	
	
	}
	
	
	@DeleteMapping("{companymstid}/categoryexceptionlistbyid/{id}")
	public void categoryDelete(@PathVariable(value = "id") String Id) {
		categoryExceptionListRepository.deleteById(Id);

	}
	
	

}
