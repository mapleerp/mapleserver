package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AddKotTable;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.UserMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.UserService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
@CrossOrigin("http://localhost:4200")
public class UserMstResource {
	

	
	 //@Autowired
	 // private RuntimeService runtimeService;

	EventBus eventBus = EventBusFactory.getEventBus();
	 
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	private  UserMstRepository usermstRepo;
	
	@Autowired
	private  CompanyMstRepository  companyMstRepo;
	
	@Autowired
	private  BranchMstRepository  branchMstRepo;

	@Autowired
	UserService userService;
	
	@Autowired
	private  BranchMstRepository branchMstRepository;
	
	
	
	Pageable topFifty =   PageRequest.of(0, 50);

	
	  @Autowired
	  private VoucherNumberService voucherService;

	
	
	@PostMapping("/{companymstid}/{branchcode}/user")
	public UserMst createUser(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@Valid @RequestBody 

			UserMst userMst)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
		String branchCode =userMst.getBranchCode();
		
		Optional<BranchMst> branchMstOpt = branchMstRepo.findByBranchCodeAndCompanyMstId(branchCode,companymstid);
		BranchMst branchMst =branchMstOpt.get();
		
		
		userMst.setCompanyMst(branchMst.getCompanyMst());
		
		userMst.setBranchCode(branchCode);
		
		
			 VoucherNumber voucherNo = voucherService.generateInvoice(branchCode,companymstid);

			 userMst.setId(voucherNo.getCode());			 
			 
			 UserMst  userMstSaved =  usermstRepo.saveAndFlush(userMst);
		 
			  	 
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("voucherNumber", userMstSaved.getId());
		variables.put("voucherDate",SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", userMstSaved.getId());		 
		variables.put("companyid", branchMst.getCompanyMst());
		variables.put("branchcode", userMstSaved.getBranchCode());

			variables.put("REST",1);
		 
			variables.put("WF", "forwardUser");
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");

			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			
			//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardUser");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);
 
		/*
ProcessInstanceWithVariables pVariablesInReturn =
runtimeService.createProcessInstanceByKey("forwardUser")
.setVariables(variables)

.executeWithVariablesInReturn();
*/
		 return userMstSaved;
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
	
	
	@GetMapping("{companymstid}/usermst/{userName}/{password}/{branchCode}/login")
	public  @ResponseBody Optional<UserMst> userLogin(@PathVariable(value = "userName") String userName,@PathVariable(value = "password") String password,@PathVariable(value = "branchCode") String branchCode){
	 
		return usermstRepo.findByUserNameAndPasswordAndBranchCode(userName,password, branchCode);

	}
	
	
	@PutMapping("{companymstid}/usermst/{userid}/changepassword")
	public UserMst updateUserPassword(
			@PathVariable(value="userid") String userid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody UserMst  userMstRequest)
	{
			
		UserMst userMst = usermstRepo.findById(userid).get();
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		userMst.setCompanyMst(companyMst);
		userMst.setPassword(userMstRequest.getPassword());
		userMst = usermstRepo.saveAndFlush(userMst);
		return userMst;
	}
	
	@GetMapping("{companymstid}/usermst/{userName}/{password}/web/weblogin")
	public  @ResponseBody Optional<UserMst> webLogin(@PathVariable(value = "userName") String userName,@PathVariable(value = "password") String password){
	 
		return usermstRepo.findByUserNameAndPassword(userName,password);
	
	}
	
	@GetMapping("{companymstid}/usermst/{userName}/{branchCode}/login")
	public  @ResponseBody Optional<UserMst> userLoginWithoutPassword(@PathVariable(value = "userName") String userName,
			 
			@PathVariable(value = "branchCode") String branchCode){
	 
		return usermstRepo.findByUserNameAndBranchCode(userName,branchCode);

	}
	
	
	@GetMapping("/{companymstid}/getallusers/{branchcode}")
	public List<UserMst> retrieveAllusers(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode){
		return usermstRepo.findByCompanyMstId(companymstid);
	}
	
	  
	 
	
	@GetMapping("{companymstid}/usermst/{newUserName}{newPassword}{id}/login")
	public  @ResponseBody Optional<UserMst> channgePassword(@PathVariable(value = "newUserName") String newUserName,
			@PathVariable(value = "newPassword") String newPassword,
			@PathVariable(value = "id") String id){
		return usermstRepo.changepassword(newUserName,newPassword,id);
	}
	
	
	  @GetMapping("{companymstid}/allusers") public List<UserMst>
	  retrieveAllUser(@PathVariable(value = "companymstid") String companymstid) {
		  
	  Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
	  return usermstRepo.findByCompanyMstId(companyMstOpt.get().getId()); 
	  
	  
	  }
	  
	@GetMapping("/{companymstid}/userrole/{userid}")
	public  @ResponseBody ArrayList<String> userDetail(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "userid") String userid){
		//java.util.Date currentDate=SystemSetting.systemDate;
		
		return userService.getUserRoles(userid,companymstid);
	}
	
	@GetMapping("/{companymstid}/userdetailbyname/{userName}")
	public  @ResponseBody UserMst userDetailbyname(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "userName") String userName){
		return usermstRepo.findByUserNameAndCompanyMstId(userName,companymstid);
	}
	
	
	@GetMapping("/{companymstid}/userdetailbyid/{id}")
	public  @ResponseBody Optional<UserMst> userDetailbyId(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "id") String id){
		return usermstRepo.findByIdAndCompanyMstId(id,companymstid);
	}
	
	@GetMapping("/{companymstid}/usermst/web/saveusermst/{username}/{fullname}/{password}/{branch}/{userid}")
	public UserMst userCreation(
			@PathVariable(value = "username") String username,
			@PathVariable(value = "fullname") String fullname,
			@PathVariable(value = "password") String password,
			@PathVariable(value = "branch") String branch,
			@PathVariable(value = "userid") String userid,

			@PathVariable(value = "companymstid") String companymstid
			){
		
		username = username.replaceAll("%20", " ");
		fullname = fullname.replaceAll("%20", " ");
		password = password.replaceAll("%20", " ");
		branch = branch.replaceAll("%20", " ");
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		
		if(null == companyMst)
		{
			return null;
		}
		
		Optional<BranchMst> btanchOpt = branchMstRepository.findByBranchCodeAndCompanyMstId(branch, companymstid);
		BranchMst branchMst = btanchOpt.get();
		
		if(null == branchMst)
		{
			return null;
		}
		
		UserMst userMst = new UserMst();
		userMst.setBranchCode(branchMst.getBranchCode());
		userMst.setCompanyMst(companyMst);
		userMst.setFullName(fullname);
		userMst.setPassword(password);
		userMst.setId(userid);
		userMst.setUserName(username);

		return usermstRepo.saveAndFlush(userMst);
	}
	//--------------------------user popup---------------
	
	@GetMapping("{companymstid}/usermstresource/usersearch")
	public  @ResponseBody List<UserMst> searchUser(	
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("data") String searchstring){
		return usermstRepo.searchLikeUserByName("%"+searchstring+"%",companymstid,topFifty) ;
	}
	
}
