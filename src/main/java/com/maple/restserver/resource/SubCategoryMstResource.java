package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.MachineResourceMst;
import com.maple.restserver.entity.MixCategoryMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SubCategoryMstRepository;
import com.maple.restserver.service.MachineResourceService;

@RestController
public class SubCategoryMstResource {
	@Autowired
	MachineResourceService machineResourceService;
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	
	@Autowired
	SubCategoryMstRepository subCategoryMstRepository;
	

@PostMapping("{companymstid}/subcategorymstresource/createsubcategory")
public MixCategoryMst createSubCategoryMst(@PathVariable(value = "companymstid") String
		  companymstid,@Valid @RequestBody 
		  MixCategoryMst subCategoryMst)
{
	return CompanyMstRepository.findById(companymstid).map(companyMst-> {
		subCategoryMst.setCompanyMst(companyMst);
		
	return subCategoryMstRepository.save(subCategoryMst);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found")); }
	




@GetMapping("{companymstid}/subcategorymstresource/findallsubcategorymst")
public List<MixCategoryMst> retrieveSubCategoryMst(@PathVariable(value = "companymstid") String
		 companymstid)
{
	return subCategoryMstRepository.findAll();
}




@DeleteMapping("{companymstid}/subcategorymstresource/deletesubcategorymst/{id}")
public void SubCategoryMstDelete(
		@PathVariable(value="companymstid") String companymstid,
		@PathVariable(value = "id") String Id) {
	subCategoryMstRepository.deleteById(Id);

}



@GetMapping("{companymstid}/subcategorymstresource/retriveresourcecategory")
public List<MixCategoryMst> retrieveResourceSubCategory(@PathVariable(value = "companymstid") String
		 companymstid)
{
	return machineResourceService.retriveResourceCategory();
}




@GetMapping("{companymstid}/subcategorymstresource/fetchresourcecategory")
public ArrayList<MixCategoryMst> fetchResourceSubCategory(@PathVariable(value = "companymstid") String
		 companymstid)
{
	return subCategoryMstRepository.fetchResourceSubCategory();
}



@GetMapping("{companymstid}/subcategorymstresource/findallsubcategorymsts/{resourcecatname}")
public MixCategoryMst retrieveSubCategoryMsts(@PathVariable(value = "companymstid") String
		 companymstid,@PathVariable(value="resourcecatname") String resourcecatname  )
{
	return subCategoryMstRepository.findByMixName(resourcecatname);
}



@GetMapping("{companymstid}/subcategorymstresource/findBysubcategorymstid/{id}")
public Optional<MixCategoryMst> retrieveSubCategoryMstByItemId(@PathVariable(value = "companymstid") String
		 companymstid,@PathVariable(value="id") String id  )
{
	return subCategoryMstRepository.findById(id);
}


}
