package com.maple.restserver.resource;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.InvoiceTermsAndConditionsMst;
import com.maple.restserver.entity.SalesPropertiesConfigMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.InvoiceTermsAndConditionsMstRepository;

@RestController
public class InvoiceTermsAndConditionsMstResources {

	@Autowired
	InvoiceTermsAndConditionsMstRepository invoiceTermsAndConditionsMstRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	@PostMapping("/{companymstid}/invoicetermsandconditions/saveinvoicetermsandconditions")
	public InvoiceTermsAndConditionsMst createInvoiceTermsAndConditionsMst(@PathVariable(value = "companymstid") String companymstid,@Valid @RequestBody 
			InvoiceTermsAndConditionsMst invoiceTermsAndConditionsMst)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			invoiceTermsAndConditionsMst.setCompanyMst(companyMst);
			Integer slNo=1;
			List<InvoiceTermsAndConditionsMst> invoiceList= invoiceTermsAndConditionsMstRepo.findAll();
			if(invoiceList.size()>0)
			{
				
				for(int i=0;i<invoiceList.size();i++)
				{
					slNo = slNo+1;
				}
			}
			invoiceTermsAndConditionsMst.setSlNo(slNo+"");
			InvoiceTermsAndConditionsMst saved = invoiceTermsAndConditionsMstRepo.save(invoiceTermsAndConditionsMst);
			return saved;
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found"));


	}
	
	@GetMapping("/{companymstid}/invoicetermsandconditions/showallinvoicetermsandconditions")
	public List<InvoiceTermsAndConditionsMst> showAllInvoiceTermsAndConditions(
			@PathVariable("companymstid")String companymstid)
	{
		return invoiceTermsAndConditionsMstRepo.findAll();
	}
	
	@Transactional
	@DeleteMapping("/{companymstid}/invoicetermsandcondiotns/deleteinvoicetermsandconditionsbyid/{id}")
	public void deleteInvoiceTermsAndConditionsMstById(@PathVariable(value = "id")String id,
			@PathVariable(value="companymstid")String companymstid)
	{
		invoiceTermsAndConditionsMstRepo.deleteByCompanyMstIdAndId(companymstid,id);
		Integer slNo=0;
		List<InvoiceTermsAndConditionsMst> invoiceList= invoiceTermsAndConditionsMstRepo.findAll();
		if(invoiceList.size()>0)
		{
			
			for(int i=0;i<invoiceList.size();i++)
			{
				slNo = slNo+1;
				InvoiceTermsAndConditionsMst invoiceTermsAndConditionsMst = invoiceList.get(i);
				invoiceTermsAndConditionsMst.setSlNo(slNo+"");
				invoiceTermsAndConditionsMstRepo.save(invoiceTermsAndConditionsMst);
			}
		}
	}
}
