package com.maple.restserver.resource;



import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MenuConfigMst;
import com.maple.restserver.entity.MenuMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.MenuConfigMstRepository;
import com.maple.restserver.repository.MenuMstRepository;
import com.maple.restserver.service.MenuMstService;

@RestController
@Transactional

public class MenuMstResource {

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	MenuMstRepository menuMstRepository;
	
	@Autowired
	MenuConfigMstRepository menuMstConfigRepository;
	
	@Autowired
	MenuMstService menuMstService;

//	@GetMapping("{companymstid}/menuconfigmstresource/menuconfigmstbymenuname/{menuname}")
//	public List<MenuConfigMst> retrieveAllMenuConfigMst(
//			@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "menuname") String menuname){
//		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
//		return menuMstConfigRepository.findByCompanyMstAndMenuName(companyOpt.get(),menuname);
//	}

	@PostMapping("{companymstid}/menumstresource/savemenumst")
	public MenuMst createMenuMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody MenuMst menuMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			menuMst.setCompanyMst(companyMst);

			return menuMstRepository.save(menuMst);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));

	}
	
//	
//	@GetMapping("{companymstid}/menuconfigmstresource/menuconfigmstbydescription/{description}")
//	public List<MenuConfigMst> retrieveAllMenuConfigMstWithDescription(
//			@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "description") String description){
//		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
//		return menuConfigMstRepository.findByCompanyMstAndMenuDescription(companyOpt.get(),description);
//	}
	
	@GetMapping("{companymstid}/menumstresource/menumsts/{branchcode}")
	public List<MenuMst> retrieveAllMenuMsts(
			@PathVariable(value = "companymstid") String companymstid){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return menuMstRepository.findAll();
	}
	
	@GetMapping("{companymstid}/menumstresource/menuconfigmstbyparentid")
	public List<MenuConfigMst> retrieveAllMenuConfigMstByParentId(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "parentid")String parentid){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return menuMstConfigRepository.getMenuConfigMstByParentId(parentid);
	}
	
	@DeleteMapping("{companymstid}/menumstresource/deletemenumsts/{id}")
	public void DeletepurchaseDtl(
			@PathVariable (value="id") String id) {
		menuMstRepository.deleteById(id);

	}
	
	
	
	
	@GetMapping("{companymstid}/menumstresource/initializemenumst/{branchcode}")
	public String InitializeMenuMsts(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return menuMstService.InitializeCompanyMst(companyOpt.get(),branchcode);
	}

	
	@GetMapping("{companymstid}/menumstresource/menusearch")
	public  @ResponseBody List<MenuConfigMst> searchMenu(	
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "searchdata") String searchstring){
		return menuMstRepository.findSearch("%"+searchstring+"%",companymstid) ;
	}
	
	
	@GetMapping("{companymstid}/menumstresource/menumstbymenuname/{menuid}")
	public MenuMst retrieveAllMenuMstByMenuMst(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "menuid") String menuid
			){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return menuMstRepository.findByMenuIdAndCompanyMst(menuid,companyOpt.get());
	}
	@GetMapping("{companymstid}/menumstresource/menumstbyid/{id}")
	public Optional<MenuMst> retrieveAllMenuMstById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id
			){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return menuMstRepository.findById(id);
	}

}
