//package com.maple.restserver.resource;
//
//import java.net.URI;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Optional;
//
//import javax.transaction.Transactional;
//import javax.validation.Valid;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
////import org.camunda.bpm.engine.RuntimeService;
////import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
//
//import com.google.common.eventbus.EventBus;
//import com.maple.maple.util.ClientSystemSetting;
//import com.maple.maple.util.MapleConstants;
//import com.maple.restserver.cloud.api.ExternalApi;
//import com.maple.restserver.entity.AcceptStock;
//import com.maple.restserver.entity.AccountHeads;
//import com.maple.restserver.entity.CompanyMst;
//import com.maple.restserver.entity.ItemMst;
//import com.maple.restserver.entity.LmsQueueMst;
//import com.maple.restserver.entity.Supplier;
//import com.maple.restserver.entity.UnitMst;
//import com.maple.restserver.exception.ResourceNotFoundException;
//import com.maple.restserver.repository.AccountHeadsRepository;
//import com.maple.restserver.repository.CompanyMstRepository;
//import com.maple.restserver.repository.LmsQueueMstRepository;
//import com.maple.restserver.repository.SupplierRepository;
//import com.maple.restserver.service.AccountHeadsService;
//import com.maple.restserver.service.SaveAndPublishService;
//import com.maple.restserver.service.SupplierService;
//import com.maple.restserver.utils.EventBusFactory;
//import com.maple.restserver.utils.SystemSetting;
//
//@RestController
//@Transactional
//public class SupplierResource {
//	
//private static final Logger logger = LoggerFactory.getLogger(SupplierResource.class);
//	
//	@Value("${mybranch}")
//	private String mybranch;
//	@Autowired
//	CompanyMstRepository companyMstRepo;
//
//	@Autowired
//	private SupplierService supplierService;
//	@Autowired
//	LmsQueueMstRepository lmsQueueMstRepository;
//	@Autowired
//	private SupplierRepository supplierRepo;
//
//
//	@Autowired
//	private AccountHeadsRepository accountHeadsRepository;
//	@Autowired
//	private ExternalApi externalApi;
//	
//	
//	@Autowired
//	SaveAndPublishService saveAndPublishService;
//	
//	@Autowired
//	AccountHeadsService accountHeadsService;
//	
//	Pageable topFifty =   PageRequest.of(0, 50);
//	// @Autowired
//	// private RuntimeService runtimeService;
//	EventBus eventBus = EventBusFactory.getEventBus();
//
//	@GetMapping("{companymstid}/suppliers")
//	public List<Supplier> retrieveAllSupplier(@PathVariable(value = "companymstid") String companymstid) {
//		return supplierService.getAllSuppliers(companymstid);
//	}
//
//	@GetMapping("{companymstid}/supplierssearch")
//	public @ResponseBody List<Supplier> searchSupplier(@PathVariable(value = "companymstid") String companymstid,
//			@RequestParam("data") String searchstring) {
//		searchstring = searchstring.replaceAll("%20", " ");
//		return supplierService.searchLikeSupplierByName("%" + searchstring + "%", companymstid);
//	}
//
//	@GetMapping("{companymstid}/suppliersbyaccountid/{accountid}")
//	public Supplier retrieveAllSupplierbyAccountId(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "accountid") String accountid) {
//
//		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
//		return supplierRepo.findByAccount_idAndCompanyMst(accountid, companymst.get());
//	}
//
//	@PostMapping("{companymstid}/supplier")
//	public Supplier createsupplier(@PathVariable(value = "companymstid") String companymstid,
//			@Valid @RequestBody Supplier supplier) {
//		return companyMstRepo.findById(companymstid).map(companyMst -> {
//			supplier.setAccount_id(supplier.getId());
//			supplier.setCompanyMst(companyMst);
////			Supplier savedsupplier = supplierRepo.save(supplier);
//			Supplier savedsupplier = saveAndPublishService.saveSupplier(supplier, mybranch);
//			logger.info("savedsupplier send to KafkaEvent: {}", savedsupplier);
//			AccountHeads accountHeads = new AccountHeads();
//
//			accountHeads.setId(supplier.getId());
//
//			System.out.print(
//					supplier.getSupplierName() + "supplier name isssssssssssssssssssssssssssssssssssssssssssssssssss");
//			accountHeads.setCustomerContact(supplier.getPhoneNo());
//			accountHeads.setAccountName(supplier.getSupplierName());
//			accountHeads.setCustomerGst(supplier.getSupGST());
//			accountHeads.setCustomerMail(supplier.getEmailid());
//			accountHeads.setCreditPeriod(supplier.getCerditPeriod());
//			accountHeads.setCompanyMst(supplier.getCompanyMst());
//			accountHeads.setCustomerAddress(supplier.getAddress());
//			accountHeads.setRank(0);
//			accountHeads.setCustomerState(supplier.getState());
//			accountHeads.setCurrencyId(supplier.getCurrencyId());
//			customerMstRepository.save(accountHeads);
//
//			AccountHeads accountHeads = accountHeadsRepository.findByIdAndCompanyMst(savedsupplier.getId(),
//					savedsupplier.getCompanyMst());
//
//			if (null == accountHeads) {
//				accountHeads = new AccountHeads();
//
//				accountHeads.setAccountName(supplier.getSupplierName());
//				accountHeads.setGroupOnly("N");
//
//				AccountHeads liabilityAccount = accountHeadsRepository.findByAccountName("LIABLITY");
//				if (null != liabilityAccount) {
//					accountHeads.setRootParentId(liabilityAccount.getId());
//				}
//
//				AccountHeads sundryCreditorAccount = accountHeadsRepository.findByAccountName("SUNDRY CREDITORS");
//
//				if (null != sundryCreditorAccount) {
//					accountHeads.setParentId(sundryCreditorAccount.getId());
//				}
//				accountHeads.setId(savedsupplier.getId());
//				accountHeads.setCurrencyId(savedsupplier.getCurrencyId());
//				accountHeads.setCompanyMst(savedsupplier.getCompanyMst());
//
//				accountHeads = accountHeadsRepository.save(accountHeads);
//				
//				Map<String, Object> variables = new HashMap<String, Object>();
//
//				variables.put("voucherNumber", accountHeads.getId());
//				variables.put("voucherDate", ClientSystemSetting.getSystemDate());
//
//				variables.put("id", accountHeads.getId());
//				variables.put("inet", 0);
//				variables.put("REST", 1);
//				variables.put("companyid", accountHeads.getCompanyMst());
//				variables.put("branchcode", accountHeads.getCompanyMst().getId());
//
//				variables.put("WF", "forwardAccountHeads");
//
//				String workflow = (String) variables.get("WF");
//				String voucherNumber = (String) variables.get("voucherNumber");
//				String sourceID = (String) variables.get("id");
//
//			 
//
//				eventBus.post(variables);
//			}
//
//			Map<String, Object> variables = new HashMap<String, Object>();
//
//			variables.put("voucherNumber", savedsupplier.getId());
//
//			variables.put("voucherDate", SystemSetting.getSystemDate());
//			variables.put("inet", 0);
//			variables.put("id", savedsupplier.getId());
//			variables.put("branchcode", savedsupplier.getCompanyMst().getId());
//			variables.put("companyid", savedsupplier.getCompanyMst());
//
//			variables.put("REST", 1);
//
//			variables.put("WF", "forwardSupplier");
//
//			String workflow = (String) variables.get("WF");
//			String voucherNumber = (String) variables.get("voucherNumber");
//			String sourceID = (String) variables.get("id");
//
//			LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//			// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//			java.util.Date uDate = (Date) variables.get("voucherDate");
//			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//			lmsQueueMst.setVoucherDate(sqlVDate);
//
//			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//			lmsQueueMst.setVoucherType(workflow);
//			lmsQueueMst.setPostedToServer("NO");
//			lmsQueueMst.setJobClass("forwardSupplier");
//			lmsQueueMst.setCronJob(true);
//			lmsQueueMst.setJobName(workflow + sourceID);
//			lmsQueueMst.setJobGroup(workflow);
//			lmsQueueMst.setRepeatTime(60000L);
//			lmsQueueMst.setSourceObjectId(sourceID);
//
//			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//
//			variables.put("lmsqid", lmsQueueMst.getId());
//			eventBus.post(variables);
//
//			Map<String, Object> variables1 = new HashMap<String, Object>();
//			variables1.put("voucherNumber", customerMst.getId());
//			variables1.put("voucherDate", SystemSetting.getSystemDate());
//			variables1.put("inet", 0);
//			variables1.put("id", customerMst.getId());
//			variables1.put("branchcode", customerMst.getCompanyMst().getId());
//			variables1.put("companyid", customerMst.getCompanyMst());
//			variables1.put("REST", 1);
//
//			variables1.put("WF", "forwardCustomer");
//
//			String workflow1 = (String) variables1.get("WF");
//			String voucherNumber1 = (String) variables1.get("voucherNumber");
//			String sourceID1 = (String) variables1.get("id");
//
//			lmsQueueMst = new LmsQueueMst();
//
//			lmsQueueMst.setCompanyMst((CompanyMst) variables1.get("companyid"));
//
//			// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
//			Object obj = variables1.get("voucherDate");
//			java.util.Date uDate1 = (java.util.Date) variables1.get("voucherDate");
//			java.sql.Date sqlVDate1 = SystemSetting.UtilDateToSQLDate(uDate1);
//			lmsQueueMst.setVoucherDate(sqlVDate1);
//
//			lmsQueueMst.setVoucherNumber((String) variables1.get("voucherNumber"));
//			lmsQueueMst.setVoucherType(workflow1);
//			lmsQueueMst.setPostedToServer("NO");
//			lmsQueueMst.setJobClass("forwardCustomer");
//			lmsQueueMst.setCronJob(true);
//			lmsQueueMst.setJobName(workflow1 + sourceID1);
//			lmsQueueMst.setJobGroup(workflow1);
//			lmsQueueMst.setRepeatTime(60000L);
//			lmsQueueMst.setSourceObjectId(sourceID1);
//
//			lmsQueueMst.setBranchCode((String) variables1.get("branchcode"));
//
//			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//			variables1.put("lmsqid", lmsQueueMst.getId());
//			eventBus.post(variables1);
//			return savedsupplier;
//
//		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
//	}
//
//	@GetMapping("{companymstid}/supplier/{id}/supplier")
//	public @ResponseBody Optional<Supplier> getSupplier(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "id") String id) {
//		return supplierService.getSupplier(id, companymstid);
//	}
//
//	@GetMapping("{companymstid}/supplierbyname/{supplierName}")
//	public Supplier retrievSupplierBySupplierName(
//
//			@PathVariable(value = "supplierName") String supplierName,
//			@PathVariable(value = "companymstid") String companymstid) {
//		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
//
//		return supplierRepo.findBySupplierNameAndCompanyMst(supplierName, companymst.get());
//	}
//
//	@GetMapping("{companymstid}/suplierresource/supplierbynamerequestparam")
//	public Supplier retrievSupplierBySupplierNameRequestParam(
//
//			@RequestParam(value = "supplierName") String supplierName,
//			@PathVariable(value = "companymstid") String companymstid) {
//		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
//
//		return supplierRepo.findBySupplierNameAndCompanyMst(supplierName, companymst.get());
//	}
//
//	@GetMapping("{companymstid}/supplierbyacountid/{account_id}")
//	public Supplier retrievSupplierByAccount_id(
//
//			@PathVariable(value = "account_id") String account_id,
//			@PathVariable(value = "companymstid") String companymstid) {
//		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
//
//		return supplierRepo.findByAccount_idAndCompanyMst(account_id, companymst.get());
//	}
//
//	@GetMapping("{companymstid}/web/savesupplier/{supplierName}/{company}/"
//			+ "{phone}/{address}/{email}/{gst}/{state}/{supplierid}")
//	public String createSupplierWeb(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "supplierName") String supplierName, @PathVariable(value = "company") String company,
//			@PathVariable(value = "phone") String phone, @PathVariable(value = "email") String email,
//			@PathVariable(value = "gst") String gst, @PathVariable(value = "state") String state,
//			@PathVariable(value = "supplierid") String supplierid, @PathVariable(value = "address") String address) {
//
//		Supplier supplier = new Supplier();
//		supplierName = supplierName.replaceAll("%20", " ");
//		company = company.replaceAll("%20", " ");
//		phone = phone.replaceAll("%20", " ");
//		email = email.replaceAll("%20", " ");
//		gst = gst.replaceAll("%20", " ");
//		state = state.replaceAll("%20", " ");
//		supplierid = supplierid.replaceAll("%20", " ");
//		address = address.replaceAll("%20", " ");
//
//		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
//		supplier.setCompanyMst(companyOpt.get());
//		supplier.setId(supplierid);
//		supplier.setSupplierName(supplierName);
//		supplier.setCompany(company);
//		supplier.setEmailid(email);
//		supplier.setAddress(address);
//		supplier.setSupGST(gst);
//		supplier.setState(state);
//		supplier.setPhoneNo(phone);
//		supplier.setCerditPeriod(0);
//
//		supplier = supplierRepo.saveAndFlush(supplier);
//
//		return "success";
//
//	}
//
//	@GetMapping("{companymstid}/web/supplierbyphoneno/{phoneno}")
//	public Boolean retrievSupplierByPhoneNo(
//
//			@PathVariable(value = "phoneno") String phoneno,
//			@PathVariable(value = "companymstid") String companymstid) {
//		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
//
//		Supplier supplier = supplierRepo.findByPhoneNoAndCompanyMst(phoneno, companymst.get());
//
//		if (null == supplier) {
//			return false;
//		}
//
//		return true;
//
//	}
//
//	@PutMapping("{companymstid}/supplierresource/{suplierid}/updatesupplier")
//	public Supplier updateSupplier(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "suplierid") String suplierid, @Valid @RequestBody Supplier supplierReq) {
//		return companyMstRepo.findById(companymstid).map(companyMst -> {
//
//			Supplier supplier = supplierRepo.findById(suplierid).get();
//
//			supplier.setCompanyMst(companyMst);
//			supplier.setAccount_id(supplierReq.getAccount_id());
//			supplier.setAddress(supplierReq.getAddress());
//			supplier.setBranchCode(supplierReq.getBranchCode());
//			supplier.setCompany(supplierReq.getCompany());
//			supplier.setCerditPeriod(supplierReq.getCerditPeriod());
//			supplier.setEmailid(supplierReq.getEmailid());
//			supplier.setState(supplierReq.getState());
//			supplier.setSupGST(supplierReq.getSupGST());
//			supplier.setSupplierName(supplierReq.getSupplierName());
//			supplier.setUserId(supplierReq.getUserId());
//			supplier.setPhoneNo(supplierReq.getPhoneNo());
//			supplier.setCurrencyId(supplierReq.getCurrencyId());
//			supplier.setCountry(supplierReq.getCountry());
//			Supplier savedsupplier = supplierRepo.save(supplier);
//
//			CustomerMst customerMst = null;
//			Optional<CustomerMst> customerMstOpt = customerMstRepository.findById(suplierid);
//			if (customerMstOpt.isPresent()) {
//				customerMst = customerMstOpt.get();
//				customerMst.setId(supplier.getId());
//
//				System.out.print(supplier.getSupplierName()
//						+ "supplier name isssssssssssssssssssssssssssssssssssssssssssssssssss");
//				customerMst.setCustomerContact(supplierReq.getPhoneNo());
//				customerMst.setCustomerName(supplierReq.getSupplierName());
//				customerMst.setCustomerGst(supplierReq.getSupGST());
//				customerMst.setCustomerMail(supplierReq.getEmailid());
//				customerMst.setCreditPeriod(supplierReq.getCerditPeriod());
//				customerMst.setCompanyMst(companyMst);
//				customerMst.setCustomerAddress(supplierReq.getAddress());
//				customerMst.setRank(0);
//				customerMst.setCustomerState(supplier.getState());
//				customerMst.setCurrencyId(supplier.getCurrencyId());
//				customerMstRepository.save(customerMst);
//			} else {
//				customerMst = new CustomerMst();
//				customerMst.setId(savedsupplier.getId());
//
//				System.out.print(savedsupplier.getSupplierName()
//						+ "supplier name isssssssssssssssssssssssssssssssssssssssssssssssssss");
//				customerMst.setCustomerContact(savedsupplier.getPhoneNo());
//				customerMst.setCustomerName(savedsupplier.getSupplierName());
//				customerMst.setCustomerGst(savedsupplier.getSupGST());
//				customerMst.setCustomerMail(savedsupplier.getEmailid());
//				customerMst.setCreditPeriod(savedsupplier.getCerditPeriod());
//				customerMst.setCompanyMst(savedsupplier.getCompanyMst());
//				customerMst.setCustomerAddress(savedsupplier.getAddress());
//				customerMst.setRank(0);
//				customerMst.setCustomerState(savedsupplier.getState());
//				customerMst.setCurrencyId(savedsupplier.getCurrencyId());
//
//				customerMst = customerMstRepository.save(customerMst);
//
//			}
//
//			// ==========================================================================================
//
//			AccountHeads accountHeads = accountHeadsRepository.findByIdAndCompanyMst(savedsupplier.getId(),
//					savedsupplier.getCompanyMst());
//
//			if (null == accountHeads) {
//				accountHeads = new AccountHeads();
//
//				accountHeads.setAccountName(supplier.getSupplierName());
//				accountHeads.setGroupOnly("N");
//
//				AccountHeads liabilityAccount = accountHeadsRepository.findByAccountName("LIABLITY");
//				if (null != liabilityAccount) {
//					accountHeads.setRootParentId(liabilityAccount.getId());
//				}
//
//				AccountHeads sundryCreditorAccount = accountHeadsRepository.findByAccountName("SUNDRY CREDITORS");
//
//				if (null != sundryCreditorAccount) {
//					accountHeads.setParentId(sundryCreditorAccount.getId());
//				}
//				accountHeads.setId(savedsupplier.getId());
//				accountHeads.setCurrencyId(savedsupplier.getCurrencyId());
//				accountHeads.setCompanyMst(savedsupplier.getCompanyMst());
//
//				accountHeads = accountHeadsRepository.save(accountHeads);
//			} else {
//				
//				accountHeads.setAccountName(supplier.getSupplierName());
//				accountHeads.setGroupOnly("N");
//
//				AccountHeads liabilityAccount = accountHeadsRepository.findByAccountName("LIABLITY");
//				if (null != liabilityAccount) {
//					accountHeads.setRootParentId(liabilityAccount.getId());
//				}
//
//				AccountHeads sundryCreditorAccount = accountHeadsRepository.findByAccountName("SUNDRY CREDITORS");
//
//				if (null != sundryCreditorAccount) {
//					accountHeads.setParentId(sundryCreditorAccount.getId());
//				}
//				accountHeads.setCurrencyId(savedsupplier.getCurrencyId());
//				accountHeads.setCompanyMst(savedsupplier.getCompanyMst());
//
//				accountHeads = accountHeadsRepository.save(accountHeads);
//			}
//
//			Map<String, Object> variables = new HashMap<String, Object>();
//
//			variables.put("voucherNumber", savedsupplier.getId());
//
//			variables.put("voucherDate", SystemSetting.getSystemDate());
//			variables.put("inet", 0);
//			variables.put("id", savedsupplier.getId());
//			variables.put("branchcode", savedsupplier.getCompanyMst().getId());
//			variables.put("companyid", savedsupplier.getCompanyMst());
//
//			variables.put("REST", 1);
//
//			variables.put("WF", "forwardSupplier");
//
//			String workflow = (String) variables.get("WF");
//			String voucherNumber = (String) variables.get("voucherNumber");
//			String sourceID = (String) variables.get("id");
//
//			LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//			// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//			java.util.Date uDate = (Date) variables.get("voucherDate");
//			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//			lmsQueueMst.setVoucherDate(sqlVDate);
//
//			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//			lmsQueueMst.setVoucherType(workflow);
//			lmsQueueMst.setPostedToServer("NO");
//			lmsQueueMst.setJobClass("forwardSupplier");
//			lmsQueueMst.setCronJob(true);
//			lmsQueueMst.setJobName(workflow + sourceID);
//			lmsQueueMst.setJobGroup(workflow);
//			lmsQueueMst.setRepeatTime(60000L);
//			lmsQueueMst.setSourceObjectId(sourceID);
//
//			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//
//			variables.put("lmsqid", lmsQueueMst.getId());
//
//			eventBus.post(variables);
//			Map<String, Object> variables1 = new HashMap<String, Object>();
//			variables1.put("voucherNumber", customerMst.getId());
//			variables1.put("voucherDate", SystemSetting.getSystemDate());
//			variables1.put("inet", 0);
//			variables1.put("id", customerMst.getId());
//			variables1.put("branchcode", customerMst.getCompanyMst().getId());
//			variables1.put("companyid", customerMst.getCompanyMst());
//			variables1.put("REST", 1);
//
//			variables1.put("WF", "forwardCustomer");
//
//			String workflow1 = (String) variables1.get("WF");
//			String voucherNumber1 = (String) variables1.get("voucherNumber");
//			String sourceID1 = (String) variables1.get("id");
//
//			lmsQueueMst = new LmsQueueMst();
//
//			lmsQueueMst.setCompanyMst((CompanyMst) variables1.get("companyid"));
//
//			// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
//			Object obj = variables1.get("voucherDate");
//			java.util.Date uDate1 = (java.util.Date) variables1.get("voucherDate");
//			java.sql.Date sqlVDate1 = SystemSetting.UtilDateToSQLDate(uDate1);
//			lmsQueueMst.setVoucherDate(sqlVDate1);
//
//			lmsQueueMst.setVoucherNumber((String) variables1.get("voucherNumber"));
//			lmsQueueMst.setVoucherType(workflow1);
//			lmsQueueMst.setPostedToServer("NO");
//			lmsQueueMst.setJobClass("forwardCustomer");
//			lmsQueueMst.setCronJob(true);
//			lmsQueueMst.setJobName(workflow1 + sourceID1);
//			lmsQueueMst.setJobGroup(workflow1);
//			lmsQueueMst.setRepeatTime(60000L);
//			lmsQueueMst.setSourceObjectId(sourceID1);
//
//			lmsQueueMst.setBranchCode((String) variables1.get("branchcode"));
//
//			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//			variables1.put("lmsqid", lmsQueueMst.getId());
//			eventBus.post(variables1);
//			
//			
//			
//			Map<String, Object> variables3 = new HashMap<String, Object>();
//
//			variables3.put("voucherNumber", accountHeads.getId());
//			variables3.put("voucherDate", ClientSystemSetting.getSystemDate());
//
//			variables3.put("id", accountHeads.getId());
//			variables3.put("inet", 0);
//			variables3.put("REST", 1);
//			variables3.put("companyid", accountHeads.getCompanyMst());
//			variables3.put("branchcode", accountHeads.getCompanyMst().getId());
//
//			variables3.put("WF", "forwardAccountHeads");
//
//			String workflow3 = (String) variables3.get("WF");
//			String voucherNumber3 = (String) variables3.get("voucherNumber");
//			String sourceID3 = (String) variables3.get("id");
//
//		 
//
//			eventBus.post(variables3);
//			
//			
//			
//			return savedsupplier;
//
//		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
//	}
//	
//	
//	
//
//	
//	
//	           // ------------------new url ----publish offline button
//
//				@PostMapping("{companymstid}/supplier/{status}")
//				public Supplier sendOffLineMessageToCloud(@Valid @RequestBody Supplier supplier,
//						@PathVariable(value = "companymstid") String companymstid,
//						@PathVariable("status") String status) {
//					 ResponseEntity<Supplier> saved=	externalApi.sendSupplierOffLineMessageToCloud(companymstid, supplier);
//						
//					return saved.getBody();
//
//				}
//	//Adding supplierDetailsToAccountHeads...///			
//				
// @GetMapping("{companymstid}/supplier/accountheadsmngmntwithsupplier")
//public String getAllsupplierDtl(@PathVariable(value = "companymstid") String companymstid)
//				{
//	 List<Supplier> SupplierList= supplierRepo.findByCompanyMstId(companymstid);
//	 
//	   if(SupplierList.size()==0)
//	   {
//		   return null;
//	   }
//	 
//	 for (Supplier supplier:SupplierList)
//	 {
//		 accountHeadsService.accountHeadsManagement(supplier.getAccount_id(),supplier.getSupplierName(),MapleConstants.SUPPLIER,companymstid);
//	 }
//	 
//	 return "Success";
//				}
//				
//}
