package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitchenCategoryDtl;
import com.maple.restserver.entity.KotCategoryMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.KitchenCategoryDtlRepository;
import com.maple.restserver.service.KitchenCategoryService;

@RestController
@Transactional
public class KitchenCategoryDtlResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	KitchenCategoryDtlRepository kitchenCategoryDtlRepo;
	
	@Autowired
	KitchenCategoryService kitchenCategoryService;
	
	@PostMapping("{companymstid}/kitchencategorydtlresource/savekitchencategory")
	public KitchenCategoryDtl creatKitchenCategoryDtl(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody KitchenCategoryDtl kitchenCategoryDtl )
	{
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		
		kitchenCategoryDtl.setCompanyMst(companyMst);
			 return kitchenCategoryDtlRepo.save(kitchenCategoryDtl);
	}
	

	@DeleteMapping("{companymstid}/kitchencategorydtlresource/deletekitchencategory/{id}")
	public void KitchenCategoryDtl(@PathVariable(value = "id") String id) {
		kitchenCategoryDtlRepo.deleteById(id);
		kitchenCategoryDtlRepo.flush();
		 
	}

	@GetMapping("{companyid}/kitchencategorydtlresource/findallkitchencategorydtl")
	public List< KitchenCategoryDtl> getKitchenCategoryDtl(
			@PathVariable (value = "companyid") String companyid  
			)
			
	{
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return  kitchenCategoryDtlRepo.findByCompanyMst(companyMst);
		
	}
	@GetMapping("{companyid}/kitchencategorydtlresource/getkitchencategoryitems")
	public List< String> getItemMstByKitchenResource(
			@PathVariable (value = "companyid") String companyid 
		 
			)
			
	{
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
		
		return kitchenCategoryService.getKitchenItems(companyMst);
	}
}
