package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.CashBook;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CashBookRepository;
import com.maple.restserver.repository.CompanyMstRepository;


@RestController
@Transactional
public class CashBookResource {
	@Autowired
	private CashBookRepository cashBookRepo;
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("/{companymstid}/cashs")
	public List<CashBook> retrieveAllcash(@PathVariable(value = "companymstid") String
			  companymstid)
	{
		return cashBookRepo.findByCompanyMstId(companymstid);
	}
	
	
	@PostMapping("/{companymstid}/cash")
	public CashBook createUser(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody CashBook cashBook1)
	{

	 	return companyMstRepo.findById(companymstid).map(companyMst-> {
	return cashBookRepo.saveAndFlush(cashBook1);
	
	 	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
	 
	}


