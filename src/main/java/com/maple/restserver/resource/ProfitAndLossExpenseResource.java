package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemNutrition;
import com.maple.restserver.entity.ProfitAndLossExpense;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ProfitAndLossExpenseRepository;

@RestController
@Transactional
public class ProfitAndLossExpenseResource {
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	ProfitAndLossExpenseRepository  profitAndLossExpenseRepository;
	
//save
	@PostMapping("{companymstid}/profitandlossexpenseresource/saveprofitandlossexpense")
	public ProfitAndLossExpense createProfitAndLossExpense(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 ProfitAndLossExpense profitAndLossExpense)
    {
		
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		profitAndLossExpense.setCompanyMst(companyMst.get());
       return profitAndLossExpenseRepository.saveAndFlush(profitAndLossExpense);
		 
	}
	//showing all 
	@GetMapping("{companymstid}/profitandlossexpenseresource/getallprofitandlossexpense")
	public List<ProfitAndLossExpense> getAllProfitAndLossExpense(
			@PathVariable(value = "companymstid") String companymstid) {
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		
		return profitAndLossExpenseRepository.findByCompanyMst(companyMst.get());
	}
	
	@DeleteMapping("{companymstid}/profitandlossexpenseresource/deleteprofitandlossexpense/{id}")
	public void DeleteProfitAndLossExpensel(@PathVariable (value="id") String Id) {
		profitAndLossExpenseRepository.deleteById(Id);

	}
	
	
	
	
	
	
	
}
