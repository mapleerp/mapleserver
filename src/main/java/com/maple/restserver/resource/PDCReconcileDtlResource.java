package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PDCReconcileDtl;
import com.maple.restserver.entity.PDCReconcileHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PDCReconcileDtlRepository;
import com.maple.restserver.repository.PDCReconcileHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
public class PDCReconcileDtlResource {
private static final Logger logger = LoggerFactory.getLogger(PDCReconcileDtlResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	PDCReconcileDtlRepository pDCReconcileDtlRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	PDCReconcileHdrRepository pDCReconcileRepository;
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@PostMapping("{companymstid}/pdcreconciledtlresource/pdcreconciledtl/{pdcreconcilehdrid}")
	public PDCReconcileDtl createPDCReconcileDtl(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody PDCReconcileDtl pDCReconcileDtl,
			@PathVariable(value = "pdcreconcilehdrid") String pdcreconcilehdrid) {
		
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		pDCReconcileDtl.setCompanyMst(companyOpt.get());
		
		Optional<PDCReconcileHdr> pDCReconcileHdrOpt = pDCReconcileRepository.findById(pdcreconcilehdrid);
		pDCReconcileDtl.setPdcReconcileHdr(pDCReconcileHdrOpt.get());
		
//		pDCReconcileDtl = pDCReconcileDtlRepository.saveAndFlush(pDCReconcileDtl);
		pDCReconcileDtl = 	saveAndPublishService.savePDCReconcileDtl(pDCReconcileDtl,mybranch);
		logger.info("pDCReconcileDtl send to KafkaEvent: {}", pDCReconcileDtl);
		return pDCReconcileDtl;
		
	}
	

	@GetMapping("{companymstid}/pdcreconciledtlsbyhdr/{pdcreconcilehdrid}")
	public List<PDCReconcileDtl> retrievePDCReconcileDtl(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "pdcreconcilehdrid") String pdcreconcilehdrid
			) {
		
		Optional<PDCReconcileHdr> pdcReconcileHdrOpt = pDCReconcileRepository.findById(pdcreconcilehdrid);
		PDCReconcileHdr pdcReconcileHdr = pdcReconcileHdrOpt.get();
		
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		
		return pDCReconcileDtlRepository.findByCompanyMstAndPdcReconcileHdr(companyMst,pdcReconcileHdr);
	}


	@DeleteMapping("{companymstid}/deletepdcreconcile/{id}")
	public ResponseEntity<?> deletePost(@PathVariable(value = "id") String id) {
		return pDCReconcileDtlRepository.findById(id).map(pdcreconcil -> {
			pDCReconcileDtlRepository.delete(pdcreconcil);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("pcdreceiptsid " + id + " not found"));
	}
}
