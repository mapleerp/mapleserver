package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReceiptModeMst;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.report.entity.CardReconcileReport;
import com.maple.restserver.report.entity.ReceiptModeReport;
import com.maple.restserver.report.entity.SaleOrderReceiptReport;
import com.maple.restserver.repository.CardSalesDtlRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DailySalesSummaryRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.service.SalesReceiptService;
import com.maple.restserver.service.SalesReceiptServiceImpl;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class SalesReceiptsResource {
	
	private static final Logger logger = LoggerFactory.getLogger(SalesReceiptsResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	SalesReceiptServiceImpl salesReceiptServiceImpl;
	@Autowired
	private SalesReceiptsRepository salesReceiptsRepo;
	@Autowired
	CardSalesDtlRepository cardSalesDtlRepository;

	@Autowired
	DailySalesSummaryRepository dailySalesSummaryRepository;
	
	@Autowired
	SalesReceiptService salesReceiptService;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@GetMapping("{companymstid}/salesreceipt")
	public List<SalesReceipts> retrieveAllSalesReceipts(@PathVariable(value = "companymstid") String companymstid) {
		return salesReceiptsRepo.findByCompanyMst(companymstid);
	}

	@GetMapping("{companymstid}/retrivesalesreceipt/{salestrandhdrid}")
	public List<SalesReceipts> retrieveSalesReceipts(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "salestrandhdrid") String salestrandhdrid) {
		return salesReceiptsRepo.findByCompanyMstIdAndSalesTransHdrId(companymstid, salestrandhdrid);
	}

	@GetMapping("{companymstid}/salesreceipts/cardreconcilereport/{receiptmode}/{branchcode}")
	public @ResponseBody List<CardReconcileReport> findCardReconcile(
			@PathVariable(value = "companymstid") String companymstid, @RequestParam(value = "fdate") String fdate,
			@RequestParam(value = "tdate") String tdate, @PathVariable(value = "receiptmode") String receiptmode,
			@PathVariable(value = "branchcode") String branchcode) {
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		java.util.Date fudate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		java.util.Date tudate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");

		return salesReceiptServiceImpl.getSalesReceiptBetweenDate(fudate, tudate, receiptmode, companyMst.get(),
				branchcode);
	}

	@GetMapping("{companymstid}/salesreceipts/getreceiptmodebetweendate/{branchcode}")
	public @ResponseBody List<ReceiptModeReport> findReceiptModeMstBetwnDate(
			@PathVariable(value = "companymstid") String companymstid, @RequestParam(value = "fdate") String fdate,
			@RequestParam(value = "tdate") String tdate, @PathVariable(value = "branchcode") String branchcode) {
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		java.util.Date fudate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		java.util.Date tudate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");

		return salesReceiptServiceImpl.getReceiptModeBetweenDate(fudate, tudate, companyMst.get().getId(), branchcode);
	}

	@GetMapping("{companymstid}/salesreceiptamount/{salestranshdrid}")
	Double retrieveSalesReceiptsAmount(@PathVariable(value = "companymstid") String companymstid,

			@PathVariable(value = "salestranshdrid") String salestrandhdrid) {
		Double amount = salesReceiptsRepo.retrieveSalesReceiptsAmount(salestrandhdrid);
		if (null == amount) {
			amount = 0.0;
		}
		return amount;
	}
	
	
	
	@GetMapping("{companymstid}/{branchcode}/cardreceiptsalesandso/{startdate}")
	Double getcardSalesReceiptsAmount(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "startdate") String startdate) {
		
		java.sql.Date date = ClientSystemSetting.StringToSqlDate(startdate, "yyy-MM-dd");
		
		
		Double amount = salesReceiptsRepo.getSalesReceiptModeDateWiseCard(date,companymstid,branchcode);
		if (null == amount) {
			amount = 0.0;
		}
		return amount;
	}
	
	

	@PostMapping("{companymstid}/salesreceipts")
	public SalesReceipts createDailySalesReceipts(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SalesReceipts salesReceipts) {
		// version4.1
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		salesReceipts.setCompanyMst(companyMst);
		SalesReceipts saved = null;
		SalesReceipts salesReceipt = salesReceiptsRepo.findBySalesTransHdrIdAndReceiptModeAndReceiptAmount(
				salesReceipts.getSalesTransHdr().getId(), salesReceipts.getReceiptMode(),
				salesReceipts.getReceiptAmount());
		if (null == salesReceipt) {
			//saved = salesReceiptsRepo.saveAndFlush(salesReceipts);
			saved=saveAndPublishService.saveSalesReceipts(salesReceipts, mybranch);
//			if(null!=saved)
//			{
//				saveAndPublishService.publishObjectToKafkaEvent(saved, mybranch, 
//						KafkaMapleEventType.SALESRECEIPTS, KafkaMapleEventType.SERVER);
//			}
//			else {
//				
//				return null;
//			}
		} else {
			saved = salesReceipt;
		}
		return saved;
		// version4.1 end

	}

	@GetMapping("{companymstid}/salesreceipts/getsalesreceiptbymodeanddate/{receiptmode}/{branchcode}")
	public @ResponseBody List<SaleOrderReceiptReport> findCardReconcile(
			@PathVariable(value = "companymstid") String companymstid, @RequestParam(value = "fdate") String fdate,
			@PathVariable(value = "receiptmode") String receiptmode,
			@PathVariable(value = "branchcode") String branchcode) {
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		java.util.Date fudate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		return salesReceiptServiceImpl.getSalesReceiptByModeAndDate(branchcode, fudate, receiptmode);
	}

	@GetMapping("{companymstid}/salesreceipts/getsalesreceiptbycard/{branchcode}")
	public @ResponseBody List<SaleOrderReceiptReport> findSalesReceiptsCard(
			@PathVariable(value = "companymstid") String companymstid, @RequestParam(value = "fdate") String fdate,
			@PathVariable(value = "branchcode") String branchcode) {
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		java.util.Date fudate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		String mode = branchcode + "-CASH";
		return salesReceiptServiceImpl.getSalesReceiptByModeCard(branchcode, fudate, mode);
	}

	@GetMapping("{companymstid}/salesreceipts/getsalesreceiptbyaccidanddate/{accid}/{branchcode}")
	public @ResponseBody List<SaleOrderReceiptReport> salesReceiptByAccidAndDate(
			@PathVariable(value = "companymstid") String companymstid, @RequestParam(value = "fdate") String fdate,
			@PathVariable(value = "accid") String accid, @PathVariable(value = "branchcode") String branchcode) {
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		java.util.Date fudate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		return salesReceiptServiceImpl.getSalesReceiptByAccIDAndDate(branchcode, fudate, accid);
	}

	@Transactional
	@DeleteMapping("{companymstid}/salesreceiptdelete/{id}")
	public void salesReceiptDelete(@PathVariable(value = "id") String Id) {

		salesReceiptsRepo.deleteById(Id);
		cardSalesDtlRepository.deleteBySalesReceiptId(Id);

	}
	
	
	@GetMapping("{companymstid}/salesreceipts/userwisesalesreceiptsummaryreport/{userid}/{branchcode}")
	public  @ResponseBody List<ReceiptModeReport> findReceiptModeReportByUserAndDate(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "userid") String userid,
			@RequestParam(value="fdate")String fdate,@RequestParam(value="tdate") String tdate,
			@PathVariable(value="branchcode")String branchcode)
	{
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		java.util.Date fudate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
		java.util.Date tudate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");

		return salesReceiptServiceImpl.UserWiseSalesReceiptsSummary(fudate,tudate,companymstid,branchcode,userid);
	}
	
	
	
	@GetMapping("{companymstid}/salesreceiptsbymodeanddate/{mode}")
	Double getsalesreceiptbymodeanddate(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "mode") String mode,
			@RequestParam(value="fdate")String fdate) {
		
		java.sql.Date date = ClientSystemSetting.StringToSqlDate(fdate, "yyy-MM-dd");
		
		
		Double amount = salesReceiptsRepo.getSalesrReceiptSummaryByDatendMode(date,mode );
		if (null == amount) {
			amount = 0.0;
		}
		return amount;
	}
	
	
	@GetMapping("{companymstid}/{branchcode}/salesreceipts/totalcardreceiptsalesandso")
	Double getCardSalesAndSoReceiptsAmount(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam(value="date")String startdate) {
		
		java.sql.Date date = ClientSystemSetting.StringToSqlDate(startdate, "yyy-MM-dd");
		
		
		Double amount = salesReceiptService.getCardSalesAndSoReceiptsAmount(date,companymstid,branchcode);
		if (null == amount) {
			amount = 0.0;
		}
		return amount;
	}
	
	
}
