package com.maple.restserver.resource;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.StockVerificationDtl;
import com.maple.restserver.entity.StockVerificationMst;
import com.maple.restserver.report.entity.StockVerificationReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.StockVerificationDtlRepository;
import com.maple.restserver.service.StockVerificationService;

@RestController
@Transactional
public class StockVerificationDtlResource {
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	StockVerificationService stockVerificationService;
	
	
	@Autowired
	StockVerificationDtlRepository stockVerificationDtlRepo;

	@PutMapping("{companymstid}/stockverificationdtl/{dtlid}/updateqty")
	public StockVerificationDtl updateStockQty(
			@PathVariable(value="dtlid") String dtlid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody StockVerificationDtl  stockVerificationDtlRequest)
	{
			
		StockVerificationDtl stockVerificationDtl = stockVerificationDtlRepo.findById(dtlid).get();
		stockVerificationDtl.setItemId(stockVerificationDtlRequest.getItemId());
		stockVerificationDtl.setQty(stockVerificationDtlRequest.getQty());
		stockVerificationDtl.setSystemQty(stockVerificationDtlRequest.getSystemQty());
		stockVerificationDtl = stockVerificationDtlRepo.save(stockVerificationDtl);
		
		return stockVerificationDtl;
	}
	@GetMapping("{companymstid}/stockverificationdtl/stockverificationdtlbyhdrid/{hdrid}")
	public List<StockVerificationDtl> retrieveStockVerificationDtlbyMstid(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "hdrid") String hdrid
		) {
		
	
		return stockVerificationDtlRepo.findByStockVerificationMstId(hdrid);
	}
	
	@GetMapping("{companymstid}/stockverificationdtl/stockverificationdtlbydate")
	public List<StockVerificationReport> retrieveStockVerificationDtlbyDate(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fdate") String fdate,
			@RequestParam("fdate") String tdate
		) {
		
		
		Date fromdate = ClientSystemSetting.StringToSqlDate(fdate, "yyyy-MM-dd");
		Date todate = ClientSystemSetting.StringToSqlDate(tdate, "yyyy-MM-dd");
	
		return stockVerificationService.findByStockVerificationByBetweenDate(fromdate,todate,companymstid);
	}
}
