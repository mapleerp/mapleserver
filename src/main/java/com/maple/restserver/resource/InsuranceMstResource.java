
package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.his.entity.InsuranceMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.InsuranceMstRepository;




@RestController
@Transactional
public class InsuranceMstResource {
	@Autowired
	private InsuranceMstRepository insurancemstr;

	@Autowired
	CompanyMstRepository companyMstRepo;
	@GetMapping("/{companymstid}/insurancemst")
	public List<InsuranceMst> retrieveAllInsuranceMst(@PathVariable(value = "companymstid") String
			  companymstid)
	{
		return insurancemstr.findByCompanyMstId(companymstid);
		
	}
	@PostMapping("/{companymstid}/insurancemst")
	public InsuranceMst createUser(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody InsuranceMst insurancemst1)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			insurancemst1.setCompanyMst(companyMst);
	return insurancemstr.saveAndFlush(insurancemst1);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
	}


