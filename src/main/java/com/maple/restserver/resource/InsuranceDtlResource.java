

package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.his.entity.InsuranceDtl;
import com.maple.restserver.repository.InsuranceDtlRepository;





@RestController
@Transactional
public class InsuranceDtlResource {
	@Autowired
	private InsuranceDtlRepository insurancedtls;
	@GetMapping("{companymstid}/insurancedtl")
	public List<InsuranceDtl> retrieveAlldepartments(@PathVariable(value = "companymstid") String
	  companymstid)
	{
		return insurancedtls.findByCompanyMstId(companymstid);
		
	}
	
	
	@PostMapping("{companymstid}/insurancedtl")
	public InsuranceDtl createUser(@Valid @RequestBody InsuranceDtl insurancedtls1)
	{
		InsuranceDtl saved=insurancedtls.saveAndFlush(insurancedtls1);
		
	return saved;
	
	}

}
