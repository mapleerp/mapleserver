package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.DamageHdr;
import com.maple.restserver.entity.DynamicProductionDtl;
import com.maple.restserver.entity.DynamicProductionHdr;
import com.maple.restserver.repository.DynamicProductionDtlRepository;
import com.maple.restserver.repository.DynamicProductionHdrRepository;

@RestController
public class DynamicProductionDtlResource {

	
	@Autowired
	DynamicProductionHdrRepository dynamicProductionHdrRepo;
	
	@Autowired
	DynamicProductionDtlRepository dynamicProductionDtlRepo;
	
	@PostMapping("{companymstid}/dynamicproductiondtl/savedynamicproductiondtl/{dynamicproductionhdrid}")
	public DynamicProductionDtl createDynamicProductionDtl(
			@PathVariable(value = "dynamicproductionhdrid") String dynamicproductionhdrid,
			@PathVariable (value = "companymstid") String companymstid,
			@Valid @RequestBody DynamicProductionDtl dynamicProductionDtlRequest) {
		
		    DynamicProductionHdr dynamicProductionHdr= dynamicProductionHdrRepo.findByIdAndCompanyMstId(dynamicproductionhdrid,companymstid);
		    dynamicProductionDtlRequest.setDynamicProductionHdr(dynamicProductionHdr);
			return dynamicProductionDtlRepo.save(dynamicProductionDtlRequest);
	}
	
	@DeleteMapping("{companymstid}/dynamicproductiondtl/deletedynamicproduction/{dtlid}")
	public void deleteDynamicProductionDtlById(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "dtlid")String dtlid)
	{
		dynamicProductionDtlRepo.deleteById(dtlid);
	}
	
	@GetMapping("{companymstid}/dynamicproductiondtl/getdynamicproductiondtlbyhdrid/{dynamicproductionhdrid}")
	public List<DynamicProductionDtl> getDynamicProductionByHdrId(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "dynamicproductionhdrid")String dynamicproductionhdrid)
	{
		return dynamicProductionDtlRepo.findByDynamicProductionHdrId(dynamicproductionhdrid);
	}
	
	
}
