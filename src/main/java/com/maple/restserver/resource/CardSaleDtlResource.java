package com.maple.restserver.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CardSalesDtl;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CardSalesDtlRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
public class CardSaleDtlResource {

	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	CardSalesDtlRepository cardSalesDtlRepository;
	
	
	
	@PostMapping("{companymstid}/cardsaledtlresource/createcardsaledtl")
	public CardSalesDtl createCardSalesDtl(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody CardSalesDtl cardSaleDtl) {
		
	 	return companyMstRepository.findById(companymstid).map(companyMst-> {
	 		cardSaleDtl.setCompanyMst(companyMst);
	 		
	 		
	 		
	 		CardSalesDtl	cardSalesDtl= cardSalesDtlRepository.save(cardSaleDtl);
	 		
				return cardSalesDtl;
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found")); }
	
	


	
	}
