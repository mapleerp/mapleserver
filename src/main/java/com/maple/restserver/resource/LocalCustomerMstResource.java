package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DeliveryBoyMst;
 
import com.maple.restserver.entity.LocalCustomerMst;
import com.maple.restserver.entity.OrderTakerMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LocalCustomerRepository;
import com.maple.restserver.service.LocalCustomerMstService;
import com.maple.restserver.service.SaveAndPublishService;

@RestController

public class LocalCustomerMstResource {
	private static final Logger logger = LoggerFactory.getLogger(LocalCustomerMstResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	LocalCustomerRepository localCustomerRepository;
	@Autowired
	LocalCustomerMstService localCustomerMstService;

	@Autowired
	CompanyMstRepository 	companyMstRepository ;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@PostMapping("{companymstid}/localcustomer")
	public LocalCustomerMst creatLocalCustomer(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody LocalCustomerMst localCustomer )
	{

		  Optional<CompanyMst> companyMst=companyMstRepository.findById(companymstid);
		  localCustomer.setCompanyMst(companyMst.get());
		
		
//		  LocalCustomerMst saved =  localCustomerRepository.save(localCustomer);
		  LocalCustomerMst saved = saveAndPublishService.saveLocalCustomerMst(localCustomer,mybranch);
		  
		  if(null!=saved)
		  {
			  saveAndPublishService.publishObjectToKafkaEvent(saved, mybranch,
					  KafkaMapleEventType.LOCALCUSTOMERMST,
					  KafkaMapleEventType.SERVER, saved.getId());
		  }
		  else {
			  return null;
		  }
		  logger.info("LocalCustomerMst send to KafkaEvent: {}", saved);
			 return saved;
			 
		/*
		 * Work Flow
		 * 
		 * forwardLocalCustomer
		 */
		
}
	
	@GetMapping("{companymstid}/localcustomersearch")
	public  @ResponseBody List<LocalCustomerMst> searchLocalCustomer(	
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("data") String searchstring){
		return localCustomerRepository.searchLikeLocalCustomerByName("%"+searchstring+"%") ;
	}
	
	
	
	@PutMapping("{companyid}/updatelocalcustomer/{localcustomerid}/localcustomer")
	public LocalCustomerMst localCustomerUpdate(
			@PathVariable (value = "localcustomerid") String localcustomerid,
			@PathVariable (value = "companyid") String companyid,
			@Valid @RequestBody LocalCustomerMst localcustomerMstRequest)
	{
 		  
		 
				return localCustomerRepository.findById(localcustomerid).map(localcustomermst -> {
					 
					localcustomermst.setLocalcustomerName(localcustomerMstRequest.getLocalcustomerName());
					localcustomermst.setAddress(localcustomerMstRequest.getAddress());
					localcustomermst.setPhoneNo(localcustomerMstRequest.getPhoneNo());
					localcustomermst.setPhoneNO2(localcustomerMstRequest.getPhoneNO2());
				//	localCustomerRepository.save(localcustomermst);
					localcustomermst= saveAndPublishService.saveLocalCustomerMst(localcustomermst,mybranch);
				 
				 if(null!=localcustomermst)
				  {
					  saveAndPublishService.publishObjectToKafkaEvent(localcustomermst,
							  mybranch, KafkaMapleEventType.LOCALCUSTOMERMST,
							  KafkaMapleEventType.SERVER, localcustomermst.getId());
				  }
				  else {
					  return null;
				  }
					  logger.info("LocalCustomerMst send to KafkaEvent: {}", localcustomermst);
		            return localcustomermst;
		            
		        }).orElseThrow(() -> new ResourceNotFoundException("localcustomermst " + localcustomerMstRequest + " not found"));
				
			    	
			   
	}
	
	
	
	@GetMapping("{companymstid}/getlocalcustomerbyname/{localcustomername}")
	public LocalCustomerMst getByCustomerName(@PathVariable(value="localcustomername") String localcustomername){
		return localCustomerRepository.findByLocalcustomerName(localcustomername);
	}
	
	@GetMapping("/{companymstid}/getlocalcustomerbyid/{id}")
	public Optional<LocalCustomerMst> retrievlocalCustomerById(@PathVariable String id){
		return localCustomerRepository.findById(id);
	}
	
	
	@GetMapping("{companymstid}/getalllocalcustomer")
	public  List<LocalCustomerMst> getByAllLocalCustomer(@PathVariable(value="companymstid") String companymstid){
		
		
		return localCustomerRepository.findAll();
	}
	
	@GetMapping("{companymstid}/localcustomerbycustomerid/{customerid}")
	public  List<LocalCustomerMst> getlLocalCustomerByCustomerId(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="customerid") String customerid){
		
		
		return localCustomerRepository.findByCustomerId(customerid);
	}
	
	@GetMapping("{companymstid}/getlocalcustomerbyphoneno/{phone}")
	public  LocalCustomerMst getlLocalCustomerByPhone(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="phone") String phone){
		
		
		return localCustomerRepository.findByPhoneNo(phone);
	}
	
	//=====================Local Customer mst=====INITIALZE===============================
	@GetMapping("{companymstid}/localcustomerresource/initializelocalcustomer")
	public String initializeLocalCustomer(
			@PathVariable(value = "companymstid") String companymstid)
			{
		
		return localCustomerMstService.initializeLocalCustomer(companymstid);
	}
	
}
