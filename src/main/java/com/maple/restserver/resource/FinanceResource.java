package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.FinanceMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.FinanceRepository;
@RestController
@Transactional
public class FinanceResource {
 
	@Autowired
	private FinanceRepository financeRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	private AccountHeadsRepository accountHeadsRepository;
	
	
	@GetMapping("{companymstid}/findallfinancemst")
	public List<FinanceMst> retrieveAllAcceptStock(
			@PathVariable(value = "companymstid") String
			  companymstid){
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		if(!companyMstOpt.isPresent())
		{
			return null;
		}
		
		return financeRepository.findByCompanyMst(companyMstOpt.get());
	}
	
	@GetMapping("{companymstid}/findallfinancemstbyname")
	public FinanceMst retrieveFinanceByName(
			@PathVariable(value = "companymstid") String
			  companymstid, @RequestParam(value="name") String name){
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		if(!companyMstOpt.isPresent())
		{
			return null;
		}
		
		return financeRepository.findByName(name);
	}
	
	
	@PostMapping("{companymstid}/savefinancemst")
	public FinanceMst createDailyStock(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  FinanceMst financeMst)
	{
		
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		if(!companyMstOpt.isPresent())
		{
			return null;
		}
		financeMst.setCompanyMst(companyMstOpt.get());
		financeMst = financeRepository.saveAndFlush(financeMst);
		
		AccountHeads accountHeads = new AccountHeads();
		accountHeads.setAccountName(financeMst.getName());
		accountHeads.setGroupOnly("N");
		accountHeads.setId(financeMst.getId());
		accountHeads.setCompanyMst(financeMst.getCompanyMst());
		
		
		/*
		 * modified due to change the customer mst into account heads========07/01/2022
		 */
		
		accountHeads.setCustomerRank(0);
		accountHeads.setCustomerContact(financeMst.getPhoneNo());
		
		
		accountHeadsRepository.saveAndFlush(accountHeads);
		
//		CustomerMst customerMst = new CustomerMst();
//		customerMst.setId(financeMst.getId());
//		customerMst.setCustomerName(financeMst.getName());
//		customerMst.setCustomerContact(financeMst.getPhoneNo());
//		customerMst.setRank(0);
//		customerMst.setCompanyMst(financeMst.getCompanyMst());
//		customerMstRepo.saveAndFlush(customerMst);
		return financeMst;
	
	}
	
	
	
	

}
