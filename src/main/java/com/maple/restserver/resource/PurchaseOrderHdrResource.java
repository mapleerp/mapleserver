package com.maple.restserver.resource;

import java.util.HashMap;
import com.maple.restserver.repository.LmsQueueMstRepository;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PurchaseOrderDtl;
import com.maple.restserver.entity.PurchaseOrderHdr;
import com.maple.restserver.entity.RawMaterialIssueHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PurchaseOrderHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.maple.util.ClientSystemSetting;
import com.google.common.eventbus.EventBus;
import com.maple.restserver.utils.EventBusFactory;
@RestController
public class PurchaseOrderHdrResource {
	
	private static final Logger logger = LoggerFactory.getLogger(PurchaseOrderHdrResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;


	//-------------------version 1.9
		
		EventBus eventBus = EventBusFactory.getEventBus();

		@Autowired
		LmsQueueMstRepository lmsQueueMstRepository;
		
	//-------------------version 1.9 end

	@Autowired
	PurchaseOrderHdrRepository purchaseOrderHdrRepository;
	
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	
	@GetMapping("{companymstid}/purchaseorderhdr/{voucherNumber}/purchaseorderhdrbyvoucher")
	public PurchaseOrderHdr findAllPurchaseOrderHdrByVoucherNumber(
			@PathVariable(value = "voucherNumber") String voucherNumber,
			@PathVariable(value ="companymstid") String companymstid) {
		return purchaseOrderHdrRepository.findByVoucherNumber(voucherNumber);

	}
	//version1.7
	@GetMapping("{companymstid}/purchaseorderhdr/purchaseorderbyvoucherdatesupid/{supid}/{status}")
	public List<PurchaseOrderHdr> findAllPurchaseOrderHdrByDateAnDsupId(
			
			@PathVariable(value ="companymstid") String companymstid,
			@PathVariable(value="supid")String supid,
			@PathVariable(value = "status")String status) {
		
		return purchaseOrderHdrRepository.findByFinalSavedStatusAndSupplierId(status,supid);

	}
	@GetMapping("{companymstid}/purchaseorderhdr/purchaseorderhdrbydtlid/{dtlid}")
	public PurchaseOrderHdr findAllPurchaseOrderHdrByDtlid(
			
			@PathVariable(value ="companymstid") String companymstid,
			@PathVariable(value = "dtlid")String dtlid) {
		
		return purchaseOrderHdrRepository.findByDtlid(dtlid);

	}
	
	//version1.7ends
	
	// version 1.4
	@GetMapping("{companymstid}/purchaseorderhdr/purchaseorderhdrbyvoucherdate")
	public List<PurchaseOrderHdr> findAllPurchaseOrderHdrByDate(
			@RequestParam(value = "voucherdate") String voucherdate,
			@PathVariable(value ="companymstid") String companymstid) {
		
		Date uDate = ClientSystemSetting.StringToUtilDate(voucherdate, "yyyy-MM-dd");
		return purchaseOrderHdrRepository.findByVoucherDateAndFinalSavedStatus(uDate,"OPEN");

	}
	// version 1.4 end
	
	
	@PostMapping("{companymstid}/purchaseorderhdr")
	public PurchaseOrderHdr createPurchaseOrderHdr(
			@Valid @RequestBody PurchaseOrderHdr purchaseOrderHdr,
			@PathVariable (value = "companymstid") String companymstid)
	{
		
		  Optional<CompanyMst> comapnyMstOpt = CompanyMstRepository.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			purchaseOrderHdr.setCompanyMst(companyMst);
			 
			//PurchaseOrderHdr saved = purchaseOrderHdrRepository.saveAndFlush(purchaseOrderHdr);
			
			PurchaseOrderHdr saved =saveAndPublishService.savePurchaseOrderHdr(purchaseOrderHdr, purchaseOrderHdr.getBranchCode());
			
	 	    return saved;
	
	
	
	
	}
	
	
	
	@GetMapping("{companymstid}/purchaseorder/getponumbers")
	public List< Object> retrieveAllProduct(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid)
	{
		Optional<CompanyMst> companymst = CompanyMstRepository.findById(companymstid);
		CompanyMst companyMst = companymst.get();
	if(null==searchstring) {
		System.out.print("inside if condition");
		return  purchaseOrderHdrRepository.findSearch();
	}else {
		return  purchaseOrderHdrRepository.findSearch("%"+searchstring.toLowerCase()+"%");
	}
		

	
}
	

	@PutMapping("{companymstid}/purchaseorderhdr/{purchaseorderhdrid}")
	public PurchaseOrderHdr PurchaseOrderHdrFinalSave(@PathVariable String purchaseorderhdrid, 
			@Valid @RequestBody PurchaseOrderHdr purchaseOrderHdrRequest)
	{

				
				return purchaseOrderHdrRepository.findById(purchaseorderhdrid).map(purchaseOrderHdr -> {

					purchaseOrderHdr.setFinalSavedStatus(purchaseOrderHdrRequest.getFinalSavedStatus());
					purchaseOrderHdr.setVoucherNumber(purchaseOrderHdrRequest.getVoucherNumber()); 

					PurchaseOrderHdr saved = purchaseOrderHdrRepository.saveAndFlush(purchaseOrderHdr);
					

					//-----------------------version 1.9
										
										Map<String, Object> variables = new HashMap<String, Object>();
							 			variables.put("voucherNumber", saved.getId());
							 			variables.put("voucherDate",ClientSystemSetting.getSystemDate());
										variables.put("inet", 0);
										variables.put("id", saved.getId());		 
										variables.put("branchcode", saved.getBranchCode());
										variables.put("companyid", saved.getCompanyMst());
									 			variables.put("REST",1);	
									 			
									 			
									 			variables.put("WF", "forwardPurchaseOrder");
									 			
									 			
									 			String workflow = (String) variables.get("WF");
									 			String voucherNumber = (String) variables.get("voucherNumber");
									 			String sourceID = (String) variables.get("id");


									 			LmsQueueMst lmsQueueMst = new LmsQueueMst();

									 			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
									 			
									 			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
									 			
									 			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
									 			java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
									 			lmsQueueMst.setVoucherDate(sqlVDate);
									 			
									 			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
									 			lmsQueueMst.setVoucherType(workflow);
									 			lmsQueueMst.setPostedToServer("NO");
									 			lmsQueueMst.setJobClass("forwardPurchaseOrder");
									 			lmsQueueMst.setCronJob(true);
									 			lmsQueueMst.setJobName(workflow + sourceID);
									 			lmsQueueMst.setJobGroup(workflow);
									 			lmsQueueMst.setRepeatTime(60000L);
									 			lmsQueueMst.setSourceObjectId(sourceID);
									 			
									 			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

									 			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
									 			variables.put("lmsqid", lmsQueueMst.getId());
									 			eventBus.post(variables);

										
					//-----------------------version end 1.9
		            return saved;
		            
		            
		        }).orElseThrow(() -> new ResourceNotFoundException("purchaseorder " + purchaseorderhdrid + " not found"));

	}
	 
	
	//---------------------------version 1.7	
	
		@GetMapping("{companymstid}/closeallpurchaseorderhdr")
		public String PurchaseOrderHdrCloseAllOrders(@PathVariable("companymstid") String companymstid)
		{

			Optional<CompanyMst> companymst = CompanyMstRepository.findById(companymstid);
			CompanyMst companyMst = companymst.get();
					List<PurchaseOrderHdr> purchaseOrderHdrList = purchaseOrderHdrRepository.findByCompanyMst(companyMst);
					
					for(int i=0; i<purchaseOrderHdrList.size(); i++)
					{
						PurchaseOrderHdr purchaseOrderHdr = purchaseOrderHdrList.get(i);
						purchaseOrderHdr.setFinalSavedStatus("CLOSED");
						
						
						//-----------------------version  1.9

						PurchaseOrderHdr saved =	purchaseOrderHdrRepository.saveAndFlush(purchaseOrderHdr);
						 
						
						Map<String, Object> variables = new HashMap<String, Object>();
			 			variables.put("voucherNumber", saved.getId());
			 			variables.put("voucherDate",ClientSystemSetting.getSystemDate());
						variables.put("inet", 0);
						variables.put("id", saved.getId());		 
						variables.put("branchcode", saved.getBranchCode());
						variables.put("companyid", saved.getCompanyMst());
					 			variables.put("REST",1);	
					 			
					 			
					 			variables.put("WF", "forwardPurchaseOrder");
					 			
					 			
					 			String workflow = (String) variables.get("WF");
					 			String voucherNumber = (String) variables.get("voucherNumber");
					 			String sourceID = (String) variables.get("id");


					 			LmsQueueMst lmsQueueMst = new LmsQueueMst();

					 			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
					 			
					 			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
					 			
					 			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
					 			java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
					 			lmsQueueMst.setVoucherDate(sqlVDate);
					 			
					 			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
					 			lmsQueueMst.setVoucherType(workflow);
					 			lmsQueueMst.setPostedToServer("NO");
					 			lmsQueueMst.setJobClass("forwardPurchaseOrder");
					 			lmsQueueMst.setCronJob(true);
					 			lmsQueueMst.setJobName(workflow + sourceID);
					 			lmsQueueMst.setJobGroup(workflow);
					 			lmsQueueMst.setRepeatTime(60000L);
					 			lmsQueueMst.setSourceObjectId(sourceID);
					 			
					 			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

					 			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
					 			variables.put("lmsqid", lmsQueueMst.getId());
					 			eventBus.post(variables);

						
	//-----------------------version end 1.9

						
					}
					
					return "SUCCESS";
		}

		@GetMapping("{companymstid}/closesupplierpurchaseorderhdr/{supplierid}")
		public String PurchaseOrderHdrCloseAllOrders(
				@PathVariable("companymstid") String companymstid,
				@PathVariable("supplierid") String supplierid)
		{
			Optional<CompanyMst> companymst = CompanyMstRepository.findById(companymstid);
			CompanyMst companyMst = companymst.get();
			
					
					List<PurchaseOrderHdr> purchaseOrderHdrList = purchaseOrderHdrRepository.findBySupplierIdAndCompanyMst(supplierid,companyMst);
					
					for(int i=0; i<purchaseOrderHdrList.size(); i++)
					{
						PurchaseOrderHdr purchaseOrderHdr = purchaseOrderHdrList.get(i);
						purchaseOrderHdr.setFinalSavedStatus("CLOSED");
						
						
						
						
						//-----------------------version  1.9

											PurchaseOrderHdr saved =	purchaseOrderHdrRepository.saveAndFlush(purchaseOrderHdr);
											
											
											Map<String, Object> variables = new HashMap<String, Object>();
								 			variables.put("voucherNumber", saved.getId());
								 			variables.put("voucherDate",ClientSystemSetting.getSystemDate());
											variables.put("inet", 0);
											variables.put("id", saved.getId());		 
											variables.put("branchcode", saved.getBranchCode());
											variables.put("companyid", saved.getCompanyMst());
										 			variables.put("REST",1);	
										 			
										 			
										 			variables.put("WF", "forwardPurchaseOrder");
										 			
										 			
										 			String workflow = (String) variables.get("WF");
										 			String voucherNumber = (String) variables.get("voucherNumber");
										 			String sourceID = (String) variables.get("id");


										 			LmsQueueMst lmsQueueMst = new LmsQueueMst();

										 			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
										 			
										 			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
										 			
										 			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
										 			java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
										 			lmsQueueMst.setVoucherDate(sqlVDate);
										 			
										 			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
										 			lmsQueueMst.setVoucherType(workflow);
										 			lmsQueueMst.setPostedToServer("NO");
										 			lmsQueueMst.setJobClass("forwardPurchaseOrder");
										 			lmsQueueMst.setCronJob(true);
										 			lmsQueueMst.setJobName(workflow + sourceID);
										 			lmsQueueMst.setJobGroup(workflow);
										 			lmsQueueMst.setRepeatTime(60000L);
										 			lmsQueueMst.setSourceObjectId(sourceID);
										 			
										 			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

										 			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
										 			variables.put("lmsqid", lmsQueueMst.getId());
										 			eventBus.post(variables);

											
						//-----------------------version end 1.9

					}
					
					return "SUCCESS";
		}
		
		//---------------------------end version 1.7	
		

}
