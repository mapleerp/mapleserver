package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.WeighBridgeVehicleMst;
import com.maple.restserver.entity.WeighBridgeWeights;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.WeighBridgeReport;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.WeighBridgeWeightsRepository;
import com.maple.restserver.service.WeighBridgeReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class WeighBridgeWeightsResource {
	@Autowired
	private WeighBridgeWeightsRepository weighBridgeWeightsRepository;

	@Autowired
	WeighBridgeReportService  weighBridgeReportService;
	@Autowired
	CompanyMstRepository companyMstRepo;

	@PostMapping("{companymstid}/saveweighbridgeweights")
	public WeighBridgeWeights createWeighBridgeWeights(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody WeighBridgeWeights weighBridgeWeights) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			weighBridgeWeights.setCompanyMst(companyMst);

			return weighBridgeWeightsRepository.save(weighBridgeWeights);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));

	}
	
	@GetMapping("{companymstid}/getfistweight/{vehicleno}")
	public WeighBridgeWeights getFirstWeightOfVehicle(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "vehicleno") String vehicleno){
		
 
				long DAY_IN_MS = 1000 * 60 * 60 * 24;
		Date twodayBefore = new Date(System.currentTimeMillis() - (2 * DAY_IN_MS));
		
		List<WeighBridgeWeights>  listOfFirstWeights = 
				weighBridgeWeightsRepository.getListOfFirstWeightOfVehicle(twodayBefore , vehicleno);
		
		WeighBridgeWeights weighBridgeWeights = null;
		if(listOfFirstWeights.size()>0) {
			weighBridgeWeights= listOfFirstWeights.get(0);
		} 
		
		
		
		
		return weighBridgeWeights;
	}

	@GetMapping("{companymstid}/getallweight")
	public List<WeighBridgeWeights> getAllWeightOfVehicle(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "data") String data){
		data = data.replaceAll("20%", " ");
		
		return weighBridgeWeightsRepository.searchByVehicleNo("%"+data.toLowerCase()+"%");
		
	}
	
	@GetMapping("{companymstid}/weighingbridgeweight/getweighbridgewithsecndwtnull")
	public List<WeighBridgeWeights> getAllWeightOfVehicleWithSecondWtNull(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "data") String data){
		data = data.replaceAll("20%", " ");
		
		return weighBridgeWeightsRepository.searchByVehicleNoWithSecndWtNull("%"+data.toLowerCase()+"%");
		
	}
	@GetMapping("{companymstid}/weighbridgeweights/{id}")
	public Optional<WeighBridgeWeights> getWeighBridgeWeightById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id){
		
		return weighBridgeWeightsRepository.findById(id);
		
	}
	
//	@GetMapping("{companymstid}/weighbridgeweights/weighbridgeweightbyvehiclenosearch")
//	public Optional<WeighBridgeWeights> getWeighBridgeWeightByVehicleNo(
//			@PathVariable(value = "companymstid") String companymstid,
//			@RequestParam(value = "data") String data){
//		
//		return weighBridgeWeightsRepository.searchByVehicleNo(vno)
//		
//	}
	
	@DeleteMapping("{companymstid}/weighbridgeweights/deletebyid/{id}")
	public void deleteWeighBridgeWts(@PathVariable(value = "id") String id)
	{
		weighBridgeWeightsRepository.deleteById(id);
	}
	

	@GetMapping("{companymstid}/weighbridgereportresurce/weighbridgereport")
	public List<WeighBridgeWeights> getWeighBridgeReport(
			@PathVariable(value = "companymstid") String companymstid,
        @RequestParam("fromdate") String fromDate,@RequestParam("todate") String todate){
		
		
		java.util.Date fdate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
		
		
		java.util.Date tdate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
		
		return weighBridgeReportService.getWeighBridgeReport( companymstid ,fdate,tdate);

	}	
	@GetMapping("{companymstid}/weighbridgereportresurce/getweighbridgewtswithsecondwtnull")
	public List<WeighBridgeWeights> getWeighBridgeWtsWithNullSecondWt()
	{
	
		return weighBridgeWeightsRepository.getWeighBridgeWithSecondWtNull();
	}
	@PutMapping("{companymstid}/weighbridgereportresurce/upateweighbridgeweightbyid")
	public void updateWeighBridgeWts(@PathVariable(value = "companymstid") String companymstid,
			@RequestBody WeighBridgeWeights weighBridgeWeightsreq)
	{
		WeighBridgeWeights weighBridgeWeights = weighBridgeWeightsRepository.findById(weighBridgeWeightsreq.getId()).get();
		weighBridgeWeights.setNextweight(weighBridgeWeightsreq.getNextweight());
		weighBridgeWeightsRepository.save(weighBridgeWeights);
	}
	
	@GetMapping("{companymstid}/getvehicledetailbyvehicleno")
	public List<WeighBridgeWeights> getvehicledetailbyvehicleno(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "vehicleno") String vehicleno){
	 
		List<WeighBridgeWeights>  listOfFirstWeights = 
				weighBridgeWeightsRepository.findByVehicleno( vehicleno);
		
		WeighBridgeWeights weighBridgeWeights = null;
		if(listOfFirstWeights.size()>0) {
			weighBridgeWeights= listOfFirstWeights.get(0);
		} 
		
	 		return listOfFirstWeights;
	}
	
}



