package com.maple.restserver.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.TransactionCountModel;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.TransactionCountService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class TransactionCountResource {

	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	
	@Autowired
	TransactionCountService transactionCountService;

	@GetMapping("{companymstid}/transactioncountresource/{branch}/transactioncount")
	public TransactionCountModel getTransactionCount(@PathVariable(value = "companymstid") String
			  companymstid,
			 @PathVariable("branch") String branch,  @RequestParam("reportdate") String reportdate){
		
		
		java.sql.Date sqlDate = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");
		
		
		return transactionCountService.getTransactionCount(branch, sqlDate ,companymstid);

	

	}
	
	
}
