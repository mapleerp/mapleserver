package com.maple.restserver.resource;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.ReceivableHdr;
import com.maple.restserver.repository.ReceivableHdrRepository;
import com.maple.restserver.utils.SystemSetting;


@RestController
@Transactional
public class ReceivableHdrResource {
	@Autowired
	private ReceivableHdrRepository receivableHdr;
	@GetMapping("{companymstid}/receivablehdr")
	public List<ReceivableHdr> retrieveAllcash()
	{
		return receivableHdr.findAll();
	}
	@PostMapping("{companymstid}/receivablehdr")
	public ResponseEntity<Object> createUser(@Valid @RequestBody ReceivableHdr receivablehdr1)
	{
		ReceivableHdr saved=receivableHdr.saveAndFlush(receivablehdr1);
	URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
		/*
		 * work Flow
		 * 
		 * forwardRceivable
		 */
	
	}

	

	
}

