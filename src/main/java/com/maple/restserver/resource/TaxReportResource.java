package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.TaxReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.TaxReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class TaxReportResource {

	
	
	@Autowired
	TaxReportService taxReportService;
	
	@Autowired
	CompanyMstRepository companyMstRepository;

	@GetMapping("{companymstid}/taxreportresource/taxreport")
	public List<TaxReport> getSalesTaxReport(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fdate") String fromdate, @RequestParam("tdate") String todate,@RequestParam("branchCode") String branchCode) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);

		return taxReportService.getTaxReport(sdate, edate, companyMst.get(),branchCode);
	
	}
	
	@GetMapping("{companymstid}/taxreportresource/purchasetaxreport")
	public List<TaxReport> getPurchaseTax(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fdate") String fromdate, @RequestParam("tdate") String todate,@RequestParam("branchCode") String branchCode) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);

		return taxReportService.getPurchaseTaxReport(sdate, edate, companyMst.get(),branchCode);
	
	}
	
	
}
