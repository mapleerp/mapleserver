package com.maple.restserver.resource;

import java.net.URI;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.ProductionDetailRepository;
import com.maple.restserver.repository.ProductionDtlDtlRepository;
import com.maple.restserver.repository.ProductionMstRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ProductionDtlResource {
	

	@Autowired
	private ProductionDetailRepository productionDtlRepo;

	@Autowired
	private ProductionMstRepository productionMstRepo;
	
	
	@Autowired
	ProductionDtlDtlRepository 	productionDtlDtlRepository; 
	
	@GetMapping("{companymstid}/productiondtls/{branchcode}")
	public List<ProductionDtl> retrieveAllAcceptStock(
			@PathVariable(value ="branchcode")String branchcode
		
			){
		return productionDtlRepo.findAll();
	}
	

	@GetMapping("{companymstid}/productiondtlsbydate/{branchcode}")
	public List<ProductionDtl> retrieveAllProductionKitByDate(
			@PathVariable (value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode")String branchcode,
			 @RequestParam("rdate") String reportdate
			){
		
		Date reportDate = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		return productionDtlRepo.findByProductionMstVoucherDate(reportDate);
	}
	
	@GetMapping("{companymstid}/productiondtlbyid")
	public Optional<ProductionDtl> retrieveProductionDtl(
			@PathVariable(value = "productiondtlid") String productiondtlid){
		return productionDtlRepo.findById(productiondtlid);
	}
	
	
	@PostMapping("{companymstid}/productiondtl")
	public ProductionDtl createDailyStock(@Valid @RequestBody 
			ProductionDtl productionDtl)
	{
		ProductionDtl saved = productionDtlRepo.saveAndFlush(productionDtl);
		
		
		return saved;
	}
	
	
	
	@PostMapping("{companymstid}/productionmst/{productionmstId}/productiondtl")
	public ProductionDtl createProductionDtl(@PathVariable(value = "productionmstId") String productionmstId,
			@Valid @RequestBody ProductionDtl productionDtlRequest) {

		return productionMstRepo.findById(productionmstId).map(productionMst -> {
			productionDtlRequest.setProductionMst(productionMst);
			return productionDtlRepo.saveAndFlush(productionDtlRequest);
		}).orElseThrow(() -> new ResourceNotFoundException("productionmstId " + productionmstId + " not found"));

	}
	
	
	
	@PutMapping("{companymstid}/productiondtlupdatebyid/{productiondtlid}")
	public ProductionDtl ProductionDtlUpdate(
			@PathVariable(value = "productiondtlid") String productiondtlid, 
			@Valid @RequestBody ProductionDtl productionDtlRequest)
	{

				
		
		return productionDtlRepo.findById(productiondtlid).map(productionDtl -> {
			productionDtl.setStatus(productionDtlRequest.getStatus());
			
			ProductionDtl productionDtlSaved = productionDtlRepo.saveAndFlush(productionDtl);
			
			
		     return productionDtlSaved;
        }).orElseThrow(() -> new ResourceNotFoundException(" Error saving ProductionDtl "));
	
					 
				
			
	}

	
	@DeleteMapping("{companymstid}/productiondtl/deleteproductiondtl/{id}")
	public void  ProductionDtlDelete(@PathVariable(value = "id") String Id) {
		productionDtlRepo.deleteById(Id);
		
	}
	

	@GetMapping("{companymstid}/productiondtlbyproductionmst/{productionmstid}")
	public List<ProductionDtl> retrieveProductionDtlByProductionMst(
			@PathVariable(value = "productionmstid") String productionmstid){
		Optional<ProductionMst> ProductionMstOpt=productionMstRepo.findById(productionmstid);
		
		return productionDtlRepo.findByProductionMst(ProductionMstOpt.get());
	}
	
		
}
