package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.report.entity.CustomerBalanceReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.AccountReceivableRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.service.AccountReceivableService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController

public class AccountReceivableResource {

	private static final Logger logger = LoggerFactory.getLogger(AccountReceivableResource.class);

	@Value("${mybranch}")
	private String mybranch;

	EventBus eventBus = EventBusFactory.getEventBus();

	@Value("${serverorclient}")
	private String serverorclient;

	@Autowired
	AccountReceivableService accountReceivableService;
	@Autowired
	private AccountReceivableRepository acceptStockRepository;
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepository;
	@Autowired
	private AccountReceivableRepository accountReceivableRepository;

	@GetMapping("{companymstid}/accountreceivable/{customerid}")
	public List<AccountReceivable> retrieveAllAcceptStock(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "customerid") String customerid, @RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate) {

		java.util.Date dateFrom = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date dateTo = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");

		return accountReceivableRepository.findByCompanyMstAndAccountHeads(companymstid, customerid);

	}

	@PostMapping("/{companymstid}/accountreceivables")
	public AccountReceivable createAccountReceivable(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody AccountReceivable accountRecivableble) {

		logger.info("accountRecivableble send to KafkaEvent: {}", accountRecivableble);

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		accountRecivableble.setCompanyMst(companyMst);
		
		accountRecivableble=saveAndPublishService.saveAccountReceivable(accountRecivableble, mybranch);
		
//		if(null!=accountRecivableble)
//		{
//			saveAndPublishService.publishObjectToKafkaEvent(accountRecivableble, mybranch, KafkaMapleEventType.ACCOUNTRECEIVABLE, KafkaMapleEventType.SERVER);
//		}
//		else {
//			return null;
//		}

		return accountRecivableble;

	}

	@PutMapping("{companymstid}/updateaccountreceivable/{accountreceivableid}")
	public AccountReceivable updateAccountReceivable(
			@PathVariable(value = "accountreceivableid") String accountreceivableid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody AccountReceivable accountreceivableRequest) {

		return accountReceivableRepository.findById(accountreceivableid).map(accountreceivable -> {
			Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			accountreceivable.setCompanyMst(companyMst);
			accountreceivable.setPaidAmount(accountreceivableRequest.getPaidAmount());
			// accountreceivable =
			// accountReceivableRepository.saveAndFlush(accountreceivable);
			accountreceivable = saveAndPublishService.saveAccountReceivable(accountreceivable, mybranch);
			if(null!=accountreceivable)
			{
				saveAndPublishService.publishObjectToKafkaEvent(accountreceivable, 
						mybranch, KafkaMapleEventType.ACCOUNTRECEIVABLE, 
						KafkaMapleEventType.SERVER, accountreceivable.getVoucherNumber());
			}
			else {
				return null;
			}
			logger.info("accountReceivable send to KafkaEvent: {}", accountreceivable);
//						Map<String, Object> variables = new HashMap<String, Object>();
//
//						variables.put("voucherNumber", accountreceivable.getVoucherNumber());
//						variables.put("voucherDate", accountreceivable.getVoucherDate());
//						variables.put("inet", 0);
//						variables.put("id", accountreceivable.getId());
//						variables.put("branchcode", accountreceivable.getSalesTransHdr().getBranchCode());
//
//						variables.put("companyid", accountreceivable.getCompanyMst());
//						if (serverorclient.equalsIgnoreCase("REST")) {
//							variables.put("REST", 1);
//						} else {
//							variables.put("REST", 0);
//						}
//
//						
//						variables.put("WF", "forwardAccountReceivable");
//						
//						String workflow = (String) variables.get("WF");
//							String voucherNumber = (String) variables.get("voucherNumber");
//							String sourceID = (String) variables.get("id");
//
//
//							LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//							lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//							
//							//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
//							
//							java.util.Date uDate = (Date) variables.get("voucherDate");
//							java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//							lmsQueueMst.setVoucherDate(sqlVDate);
//							
//							
//							lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//							lmsQueueMst.setVoucherType(workflow);
//							lmsQueueMst.setPostedToServer("NO");
//							lmsQueueMst.setJobClass("forwardAccountReceivable");
//							lmsQueueMst.setCronJob(true);
//							lmsQueueMst.setJobName(workflow + sourceID);
//							lmsQueueMst.setJobGroup(workflow);
//							lmsQueueMst.setRepeatTime(60000L);
//							lmsQueueMst.setSourceObjectId(sourceID);
//							
//							lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//							lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//							variables.put("lmsqid", lmsQueueMst.getId());
//							eventBus.post(variables);
			return accountreceivable;
		}).orElseThrow(
				() -> new ResourceNotFoundException("accountreceivableid " + accountreceivableid + " not found"));

	}

	@GetMapping("{companymstid}/accountreceivable/fetchaccountreceivable/{accountid}")
	public List<AccountReceivable> retrieveAccountReceivable(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "accountid") String accountid) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return accountReceivableService.findByCompanyMstAndAccountId(companyMst, accountid);

	}

	@GetMapping("{companymstid}/accountreceivable/fetchaccountreceivablebyvouchernumber/{vouchernumber}")
	public AccountReceivable retrieveAccountReceivableByVOucherNuber(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "vouchernumber") String vouchernumber) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return accountReceivableRepository.findByCompanyMstAndVoucherNumber(companyMst, vouchernumber);

	}

	@PutMapping("{companymstid}/updateaccountreceivablebyvouchernumber/{vouchernumber}")
	public AccountReceivable updateAccountReceivableByVoucherNumber(
			@PathVariable(value = "vouchernumber") String vouchernumber,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody AccountReceivable accountreceivableRequest) {
		AccountReceivable accountReceivable = new AccountReceivable();
		accountReceivable = accountReceivableRepository.findByVoucherNumber(vouchernumber);
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		System.out.println("Account IIIIIIIIIIIIIIIDDDDDDDD" + accountreceivableRequest.getAccountId());
		accountReceivable.setAccountId(accountreceivableRequest.getAccountId());
		accountReceivable.setCompanyMst(companyMst);
		accountReceivable.setDueAmount(accountreceivableRequest.getDueAmount());
		
		/*
		 * modified due to change the customer mst into account heads========07/01/2022
		 */
		
//		Optional<CustomerMst> customerMstOpt = customerMstRepository
//				.findById(accountreceivableRequest.getCustomerMst().getId());
		Optional<AccountHeads> accountHeadsOpt = accountHeadsRepository
				.findById(accountreceivableRequest.getAccountHeads().getId());
		AccountHeads accountHeads = accountHeadsOpt.get();
		accountReceivable.setAccountHeads(accountHeads);
		
		
		
//		accountReceivable = accountReceivableRepository.saveAndFlush(accountReceivable);
		accountReceivable = saveAndPublishService.saveAccountReceivable(accountReceivable, mybranch);
		if(null!=accountReceivable)
		{
			saveAndPublishService.publishObjectToKafkaEvent(accountReceivable, 
					mybranch, KafkaMapleEventType.ACCOUNTRECEIVABLE,
					KafkaMapleEventType.SERVER, accountReceivable.getVoucherNumber());
		}
		else {
			return null;
		}
		logger.info("accountReceivable send to KafkaEvent: {}", accountReceivable);
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", accountReceivable.getVoucherNumber());
		variables.put("voucherDate", accountReceivable.getVoucherDate());
		variables.put("inet", 0);
		variables.put("id", accountReceivable.getId());
		variables.put("branchcode", accountReceivable.getSalesTransHdr().getBranchCode());

		variables.put("companyid", accountReceivable.getCompanyMst());
		if (serverorclient.equalsIgnoreCase("REST")) {
			variables.put("REST", 1);
		} else {
			variables.put("REST", 0);
		}

		variables.put("WF", "forwardAccountReceivable");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		eventBus.post(variables);
		return accountReceivable;
	}

	/*
	 * following has to be changed. Only one record is returned. there can be
	 * multiple records
	 */
	@GetMapping("{companymstid}/accountreceivable/fetchaccreceivablebysalestranshdr/{salestranshdrid}")
	public AccountReceivable retrieveAccountReceivableBySalesTransHdr(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "salestranshdrid") String salestranshdrid) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		Optional<SalesTransHdr> salesTransHdrOpt = salesTransHdrRepository.findById(salestranshdrid);

		List<AccountReceivable> a = accountReceivableRepository.findByCompanyMstAndSalesTransHdr(companyMst,
				salesTransHdrOpt.get());

		if (a.size() > 0) {
			return a.get(0);
		} else {
			return null;
		}

	}

	@GetMapping("{companymstid}/accountreceivable/fetchaccreceivablebysalesordertranshdr/{salesordertranshdrid}")
	public AccountReceivable retrieveAccountReceivableBySalesOrderTransHdr(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "salesordertranshdrid") String salesordertranshdrid) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		Optional<SalesOrderTransHdr> salesOrderTransHdrOpt = salesOrderTransHdrRepository
				.findById(salesordertranshdrid);

		List<AccountReceivable> a = accountReceivableRepository.findByCompanyMstAndSalesOrderTransHdr(companyMst,
				salesOrderTransHdrOpt.get());

		if (a.size() > 0) {
			return a.get(0);
		} else {
			return null;
		}

	}

	@GetMapping("{companymstid}/accountreceivable/fetchaccreceivablebyid/{id}")
	public AccountReceivable retrieveAccountReceivableById(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		return accountReceivableRepository.findByCompanyMstAndId(companyMst, id);

	}

	@GetMapping("{companymstid}/accountreceivable/customerbalance/{custid}")
	public List<CustomerBalanceReport> retrieveCustomerBalance(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "custid") String custid,
			@RequestParam("fromdate") String fromdate, @RequestParam("todate") String todate) {
		java.util.Date dateFrom = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");

		java.util.Date dateTo = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		return accountReceivableService.findByCustomerBalanceById(companyMst, custid, dateFrom, dateTo);

	}

	@GetMapping("{companymstid}/accountreceivable/allcustomerbalance")
	public List<CustomerBalanceReport> retrieveAllCustomerBalance(
			@PathVariable(value = "companymstid") String companymstid,

			@RequestParam("fromdate") String fromdate, @RequestParam("todate") String todate) {
		java.util.Date dateFrom = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");

		java.util.Date dateTo = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		return accountReceivableService.findByAllCustomerBalance(companyMst, dateFrom, dateTo);

	}

	@GetMapping("{companymstid}/accountreceivable/automaticadjustment/{accountid}/" + "{receipthdrid}/{receivedamount}")
	public Double automaticalBillAdjustment(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "accountid") String accountid,
			@PathVariable(value = "receipthdrid") String receipthdrid,
			@PathVariable(value = "receivedamount") Double receivedamount,
			@RequestParam(value = "rdate") String rdate) {
		Date udate = SystemSetting.StringToUtilDate(rdate, "yyyy-MM-dd");
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return accountReceivableService.billwiseAdjustment(udate, companyMst, receipthdrid, receivedamount, accountid);
	}

	@PutMapping("{companymstid}/accountreceivable/updateaccountreceivablebyvouchernumberanddate/{vouchernumber}")
	public AccountReceivable updateAccountReceivableByVoucherNumberAndDate(
			@PathVariable(value = "vouchernumber") String vouchernumber,
			@PathVariable(value = "companymstid") String companymstid, @RequestParam(value = "date") String date,
			@Valid @RequestBody AccountReceivable accountreceivableRequest) {

		Date voucherDate = SystemSetting.StringToUtilDate(date, "yyyy-MM-dd");

		AccountReceivable accountReceivable = new AccountReceivable();
		accountReceivable = accountReceivableRepository.findByVoucherNumberAndVoucherDate(vouchernumber, voucherDate);
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		System.out.println("Account IIIIIIIIIIIIIIIDDDDDDDD" + accountreceivableRequest.getAccountId());
		accountReceivable.setAccountId(accountreceivableRequest.getAccountId());
		accountReceivable.setCompanyMst(companyMst);
		accountReceivable.setDueAmount(accountreceivableRequest.getDueAmount());
		
		/*
		 * modified due to change the customer mst into account heads========07/01/2022
		 */
		
//		Optional<CustomerMst> customerMstOpt = customerMstRepository
//				.findById(accountreceivableRequest.getCustomerMst().getId());
		Optional<AccountHeads> accountHeadsOpt = accountHeadsRepository
				.findById(accountreceivableRequest.getAccountHeads().getId());
		AccountHeads accountHeads = accountHeadsOpt.get();
		accountReceivable.setAccountHeads(accountHeads);
		
		
		
		// accountReceivable =
		// accountReceivableRepository.saveAndFlush(accountReceivable);

		accountReceivable = saveAndPublishService.saveAccountReceivable(accountReceivable, mybranch);
		if(null!=accountReceivable)
		{
			saveAndPublishService.publishObjectToKafkaEvent(accountReceivable, 
					mybranch, KafkaMapleEventType.ACCOUNTRECEIVABLE, 
					KafkaMapleEventType.SERVER, accountReceivable.getVoucherNumber());
		}
		else {
			return null;
		}
		logger.info("accountReceivable send to KafkaEvent: {}", accountReceivable);
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", accountReceivable.getVoucherNumber());
		variables.put("voucherDate", accountReceivable.getVoucherDate());
		variables.put("inet", 0);
		variables.put("id", accountReceivable.getId());
		variables.put("branchcode", accountReceivable.getSalesTransHdr().getBranchCode());

		variables.put("companyid", accountReceivable.getCompanyMst());
		if (serverorclient.equalsIgnoreCase("REST")) {
			variables.put("REST", 1);
		} else {
			variables.put("REST", 0);
		}

		variables.put("WF", "forwardAccountReceivable");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		eventBus.post(variables);
		return accountReceivable;
	}

	@GetMapping("{companymstid}/accountreceivable/fetchaccountreceivablebyvouchernumberanddate/{vouchernumber}")
	public AccountReceivable retrieveAccountReceivableByVoucherNumberAndDate(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "vouchernumber") String vouchernumber, @RequestParam(value = "rdate") String rdate) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();

		Date date = SystemSetting.StringToUtilDate(rdate, "yyyy-MM-dd");

		return accountReceivableRepository.findByCompanyMstAndVoucherNumberAndDate(companyMst, vouchernumber, date);

	}

}
