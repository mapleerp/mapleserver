package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageHdr;
import com.maple.restserver.entity.ProductionBatchStockDtl;
import com.maple.restserver.report.entity.StockSummaryReport;
import com.maple.restserver.repository.ActualProductionDtlRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ProductionBatchStockDtlRepository;
import com.maple.restserver.service.ProductionBatchStockDtlService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class ProductionBatchStockDtlResource {

	@Autowired
	ProductionBatchStockDtlRepository productionBatchStockDtlRepository;
	
	@Autowired
	ActualProductionDtlRepository actualProductionDtlRepo;
	@Autowired
	ProductionBatchStockDtlService productionBatchStockDtlService;
	@Autowired
	CompanyMstRepository companyMstRepo;
	@GetMapping("{companymstid}/productionbatchstockdtlresource/productionbatchstockdtl/{itemid}")
	public 	List<ProductionBatchStockDtl> getProductionBatchStockDtl(
		@PathVariable("itemid") String itemid,
			@PathVariable(value = "companymstid") String  companymstid) {


		return productionBatchStockDtlService.getProductionBatchStockDtl(companymstid, itemid);

	}
	
	@DeleteMapping("{companymstid}/productionbatchstockdtldelete/{dtlid}")
	public void DeletepurchaseDtl(@PathVariable (value="dtlid") String dtlid) {
		productionBatchStockDtlRepository.deleteById(dtlid);

	}
	
	
	@GetMapping("{companymstid}/productionbatchstockdtlresource/productionbatchstockdtlbyactualproduction/{actualprodndtl}")
	public 	List<ProductionBatchStockDtl> getActualProductionDtl(
		@PathVariable("actualprodndtl") String actualprodndtl,
			@PathVariable(value = "companymstid") String  companymstid) {
        Optional<ActualProductionDtl> actualProductionDtl = actualProductionDtlRepo.findById(actualprodndtl);
		return productionBatchStockDtlRepository.findByActualProductionDtl(actualProductionDtl.get());

	}
	
	@PostMapping("{companymstid}/productionbatchstockdtl")
	public ProductionBatchStockDtl createProductionBatchStockDtl(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 ProductionBatchStockDtl  productionBatchStockDtl)
	{
		
		
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		
		CompanyMst companyMst = companyMstOpt.get();
		productionBatchStockDtl.setCompanyMst(companyMst);
		
	   return productionBatchStockDtlRepository.save(productionBatchStockDtl);
		} 
	
	
	@GetMapping("{companymstid}/productionbatchstockdtlresource/productionrowmaterialcount/{itemid}")
	public 	Integer getProductionRowMaterialCount(
		@PathVariable("itemid") String itemid,
			@PathVariable(value = "companymstid") String  companymstid) {


		return productionBatchStockDtlService.getProductionRowMaterialCount(companymstid, itemid);

	}
	@GetMapping("{companymstid}/productionbatchstockdtlresource/productionsavedrowmaterialcount/{actualproductionid}")
	public 	Integer getProductionSavedBatchRowMaterialCount(
		@PathVariable("actualproductionid") String actualproductionid,
			@PathVariable(value = "companymstid") String  companymstid) {

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		 Optional<ActualProductionDtl> actualProductionDtl = actualProductionDtlRepo.findById(actualproductionid);
		return productionBatchStockDtlRepository.getProductionBatchRowMaterialCount(companyMst.get(), actualProductionDtl.get());

	}
	
	@GetMapping("{companymstid}/productionbatchstockdtlresource/productionrowmaterialcount/{itemid}/{actualproductiondtlid}")
	public 	Double getProductionBatchRowMaterialCount(
		@PathVariable("itemid") String itemid,@PathVariable("actualproductiondtlid")String actualproductiondtlid,
			@PathVariable(value = "companymstid") String  companymstid) {

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		
		return productionBatchStockDtlRepository.getCountOfProductionBatchRowMaterial(itemid,companymstid,actualproductiondtlid);

	}
	
}
