package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.DailySalesSummary;
import com.maple.restserver.report.entity.DailySalesReport;
import com.maple.restserver.report.entity.DailySalesReportDtl;
import com.maple.restserver.report.entity.DailySalesReturnReportDtl;
import com.maple.restserver.report.entity.DailySalesSummaryReport;
import com.maple.restserver.report.entity.DayEndPettyCashPaymentAndReceipt;
import com.maple.restserver.report.entity.ItemWiseDtlReport;
import com.maple.restserver.report.entity.ReceiptModeReport;
import com.maple.restserver.report.entity.ReceiptModeWiseReort;
import com.maple.restserver.report.entity.SaleOrderReceiptReport;
import com.maple.restserver.report.entity.SettledAmountDtlReport;
import com.maple.restserver.repository.DailySalesSummaryRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.service.DailySaleReportService;
import com.maple.restserver.service.SalesTransHdrReportService;
import com.maple.restserver.utils.SystemSetting;
@CrossOrigin("http://localhost:4200")
@RestController
public class DailySaleReportResource {

	@Autowired
	DailySaleReportService dailySaleReportService;
	@Autowired
	DailySalesSummaryRepository dailySalereportRepo;

	@Autowired
	SalesTransHdrReportService salesTransHdrReportService;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@GetMapping("/{companymstid}/dailysalesummary/{branchCode}")
	public DailySalesSummary getDailSaleSummary(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchCode") String branchCode, @RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySaleReportService.getDailySaleSummary(date, branchCode);

	}

	@GetMapping("{companymstid}/dailysalesreportdtl/{branchcode}")
	public List<DailySalesReportDtl> getDailySalesReportDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("rdate") String strfromdate,
			@RequestParam("tdate") String strtodate) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate, "yyyy-MM-dd");

		return salesTransHdrReportService.getDailySalesReportdtl(branchcode, fromdate, todate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalerreportresource/dailysalesreport/{branchcode}")
	public List<DailySalesReportDtl> getSalesReportDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("rdate") String strfromdate

	) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");

		return salesTransHdrReportService.getDailySalesReportdtl(branchcode, fromdate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalesreportdtl/salesreturn/{branchcode}")
	public List<DailySalesReturnReportDtl> getDailySalesReportReturnDtl(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branchcode") String branchcode,
			@RequestParam("rdate") String strfromdate, @RequestParam("tdate") String strtodate) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate, "yyyy-MM-dd");

		return salesTransHdrReportService.getDailySalesReturnReportdtl(branchcode, fromdate, todate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalereportresource/itemwise/{branchcode}")
	public List<ItemWiseDtlReport> getDailtItemWiseSales(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("rdate") String reportdate) {

		return salesTransHdrReportService.getDailyItemWiseSalesReportdtl(branchcode, reportdate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalereportresourcebetweendate/itemwise/{branchcode}")
	public List<ItemWiseDtlReport> getDailtItemWiseSales(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("rdate") String reportdate,
			@RequestParam("tdate") String todate) {

		return salesTransHdrReportService.getDailyItemWiseMonthlySalesReportdtl(branchcode, reportdate, todate,
				companymstid);

	}

	@GetMapping("{companymstid}/itemwisesalesreportbetweendate/itemwise/{branchcode}/{itemid}")
	public List<ItemWiseDtlReport> getDailyItemWiseSales(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid, @PathVariable("branchcode") String branchcode,
			@RequestParam("rdate") String reportdate, @RequestParam("tdate") String todate) {

		return salesTransHdrReportService.getItemWiseSalesReprtBetweenDates(branchcode, reportdate, todate,
				companymstid, itemid);

	}

	@GetMapping("{companymstid}/categoryWiseSalesReportbetweendate/itemwise/{branchcode}/{categoryid}")
	public List<ItemWiseDtlReport> getDailyCategoryWiseSales(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "categoryid") String categoryid, @PathVariable("branchcode") String branchcode,
			@RequestParam("rdate") String reportdate, @RequestParam("tdate") String todate) {
		java.util.Date eDate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		java.util.Date SDate = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		return salesTransHdrReportService.getCategoryWiseSalesReprtBetweenDates(branchcode, SDate, eDate, companymstid,
				categoryid);

	}

	@GetMapping("{companymstid}/openingpettycash/{branchCode}")
	public DailySalesSummaryReport getOpeningPettyCash(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchCode") String branchCode, @RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "dd/MM/yyyy");

		return dailySaleReportService.getOpeningPettyCash(date, branchCode);

	}

	@GetMapping("{companymstid}/advancerecivedfromorder/{branchCode}")
	public DailySalesSummaryReport advanceRecivedFromOrders(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchCode") String branchCode, @RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "dd/MM/yyyy");

		return dailySaleReportService.advanceRecivedFromOrders(date, branchCode);

	}

	@GetMapping("{companymstid}/dailysalesdtl/{branchCode}")
	public DailySalesSummaryReport dailySalesDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchCode") String branchCode, @RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "dd/MM/yyyy");

		return dailySaleReportService.getDailySalesDtls(date, branchCode);

	}

	@GetMapping("{companymstid}/dailycreditsalesdtl/{branchCode}")
	public DailySalesSummaryReport dailyCreditSalesDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchCode") String branchCode, @RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "dd/MM/yyyy");

		return dailySaleReportService.dailyCreditSalesDtl(date, branchCode);

	}

	@GetMapping("{companymstid}/dailysalesreport")
	public List<DailySalesReport> dailySalesReport(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySaleReportService.dailySalesReport(date, companymstid);

	}

	@GetMapping("{companymstid}/dailysalesreport/totalsales")
	public Double dailySalesReportTotalSales(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("sdate") String reportdate, @RequestParam("edate") String edate) {
		java.util.Date fdate = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		java.util.Date tdate = SystemSetting.StringToUtilDate(edate, "yyyy-MM-dd");

		return salesTransHdrRepository.dailySalesReportTotalSales(fdate, companymstid, tdate);

	}

	@GetMapping("{companymstid}/allbranchdailysalessummaryreport")
	public List<DailySalesSummary> dailySalesSummaryReport(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySaleReportService.allbranchdailySalesSummaryReport(date, companymstid);

	}

	@GetMapping("{companymstid}/customerdailysalesreportdtl/{branchcode}/{customerid}")
	public List<DailySalesReportDtl> getCustomerSalesDetails(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "customerid") String customerid, @PathVariable("branchcode") String branchcode,
			@RequestParam("fdate") String strfromdate, @RequestParam("tdate") String strtodate) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate, "yyyy-MM-dd");

		return salesTransHdrReportService.getCustomerDailySalesReportdtl(branchcode, fromdate, todate, companymstid,
				customerid);

	}

	@GetMapping("{companymstid}/dailysalesreportresourec/receiptmodewisereport/{branchcode}")
	public List<ReceiptModeWiseReort> getReceiptModeWiseReort(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("rdate") String strfromdate

	) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");

		return salesTransHdrReportService.getReceiptModeWiseReort(branchcode, fromdate, companymstid);

	}

	/*
	 * AMB/dailysalesreportresourec/receiptmodewisesummaryreport/WBH
	 */

	@GetMapping("{companymstid}/dailysalesreportresourec/receiptmodewisesummaryreport/{branchcode}")
	public List<ReceiptModeWiseReort> getReceiptModeWisSummaryeReort(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branchcode") String branchcode,
			@RequestParam("rdate") String strfromdate

	) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");

		return salesTransHdrReportService.getReceiptModeWisSummaryeReort(branchcode, fromdate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalesreportresourec/receiptmodecashwisereport/{branchcode}")
	public List<ReceiptModeWiseReort> getReceiptModeCashWiseReort(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branchcode") String branchcode,
			@RequestParam("rdate") String strfromdate

	) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");

		return salesTransHdrReportService.getReceiptModeCashWiseReort(branchcode, fromdate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalesreportresourec/saleordersettledamount/{branchcode}")
	public Double getSaleOrderSettledAmount(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("rdate") String strfromdate

	) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");

		return salesTransHdrReportService.getSaleOrderSettledAmount(branchcode, fromdate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalesreportresourec/saleordersettledcashamount/{branchcode}")
	public Double getSaleOrderSettledCashAmount(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("rdate") String strfromdate

	) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");

		return salesTransHdrReportService.getSaleOrderSettledCashAmount(branchcode, fromdate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalesreportresourec/previousadvanceamount/{branchcode}")
	public Double getPreviousAdvanceAmount(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("rdate") String strfromdate,
			@RequestParam("receiptmode") String receiptmode

	) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");

		Double total = 0.0;
		total = salesTransHdrReportService.getPreviousAdvanceAmount(branchcode, fromdate, companymstid, receiptmode);
		if (null == total) {
			total = 0.0;
		}
		return total;
	}

	@GetMapping("{companymstid}/dailysalesreportresourec/previousadvanceamountpopup/{branchcode}")
	public List<SaleOrderReceiptReport> getPreviousAdvanceAmountReport(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branchcode") String branchcode,
			@RequestParam("rdate") String strfromdate, @RequestParam("receiptmode") String receiptmode

	) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");

		return salesTransHdrReportService.getPreviousAdvanceAmountDtl(branchcode, fromdate, companymstid, receiptmode);
	}

	@GetMapping("{companymstid}/dailysalesreportresourec/previousadvanceamountcard/{branchcode}")
	public Double getPreviousAdvanceAmountCard(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("rdate") String strfromdate

	) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");

		String receiptmode = "CASH";
		return salesTransHdrReportService.getPreviousAdvanceAmountCard(branchcode, fromdate, companymstid, receiptmode);
	}

	@GetMapping("{companymstid}/dailysalesreportresourec/previousadvanceamountcardrealized/{branchcode}")
	public Double getPreviousAdvanceAmountCardRealized(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("rdate") String strfromdate,
			@RequestParam("cardMode") String cardMode

	) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");

		String receiptmode = cardMode;
		return salesTransHdrReportService.getPreviousAdvanceAmountCardRealized(branchcode, fromdate, companymstid,
				receiptmode);
	}

	@GetMapping("{companymstid}/dailysalesreportresourec/settleamountdtlreport/{branchcode}")
	public List<SettledAmountDtlReport> getSettledAmountDtlReport(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branchcode") String branchcode,
			@RequestParam("rdate") String strfromdate

	) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");

		return salesTransHdrReportService.getSettledAmountDtlReport(branchcode, fromdate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalesreportresourec/settleamountdtlcashreport/{branchcode}")
	public List<SettledAmountDtlReport> getSettledAmountDtlCashWiseReport(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branchcode") String branchcode,
			@RequestParam("rdate") String strfromdate

	) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");

		return salesTransHdrReportService.getSettledAmountDtlCashWiseReport(branchcode, fromdate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalesreport/salesdtldaily/{branchcode}")
	public List<DailySalesReportDtl> SalesDtlDaily(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("rdate") String strfromdate,
			@RequestParam("tdate") String strtodate) {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate, "yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate, "yyyy-MM-dd");

		return salesTransHdrReportService.getSalesDtlDailyReport(branchcode, fromdate, todate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalesreport/userwisesalesreportsummary/{userid}/{branchcode}")
	public List<DailySalesReportDtl> getUserWiseSummarySalesReportDtl(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("userid") String userid,
			@PathVariable("branchcode") String branchcode, @RequestParam("fdate") String fromdate,
			@RequestParam("tdate") String todate) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");

		return salesTransHdrReportService.getUserWiseSummarySalesReportDtl(branchcode, userid, sdate, edate,
				companymstid);

	}

	@GetMapping("{companymstid}/dailysalesreport/userwisesalesreport/{userid}/{branchcode}")
	public List<DailySalesReportDtl> getUserWiseSalesReport(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("userid") String userid, @PathVariable("branchcode") String branchcode,
			@RequestParam("fdate") String fromdate, @RequestParam("tdate") String todate) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");

		return salesTransHdrReportService.getUserWiseSalesReport(branchcode, userid, sdate, edate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalesreportresource/receiptmodewisesalesandsoconverted/{branchcode}")

	public List<ReceiptModeReport> receiptModeWiseReportSalesAndSoConverted(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySaleReportService.receiptModeWiseReportSalesAndSoConverted(date, companymstid, branchcode);

	}

	@GetMapping("{companymstid}/dailysalesreportresource/soadvancereceivedtoday/{branchcode}")
	public List<ReceiptModeReport> soAdvanceReceivedToday(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySaleReportService.soAdvanceReceivedToday(date, companymstid, branchcode);

	}

	@GetMapping("{companymstid}/dailysalesreportresource/receiptbyreceiptmodewisereport/{branchcode}")
	public List<ReceiptModeReport> dayEndReceptModeWiseReport(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySaleReportService.dayEndReceiptModewiseReport(date, companymstid, branchcode);

	}

	@GetMapping("{companymstid}/dailysalesreportresource/dayendpettycashreceiptandpayment/{branchcode}/{accountid}")
	public DayEndPettyCashPaymentAndReceipt dayEndPettyCashReceiptAndPettyCashPayment(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @PathVariable(value = "accountid") String accountid,
			@RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySaleReportService.dayEndPettyCashReceiptAndPettyCashPayment(date, companymstid, branchcode,
				accountid);

	}

	@GetMapping("{companymstid}/dailysalesreportresource/saleorderpreviousadvance/{branchcode}")
	public Double dayEndPreviousAdvance(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySalereportRepo.dayEndPreviousAdvance(date, companymstid, branchcode);

	}

	@GetMapping("{companymstid}/dailysalesreportresource/dayendreceiptcash/{branchcode}/{accountid}")
	public Double dayEndReceiptCash(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @PathVariable(value = "accountid") String accountid,
			@RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySalereportRepo.dayEndReceiptCash(date, companymstid, branchcode, accountid);

	}
	
	
	@GetMapping("{companymstid}/dailysalesreport/previousadvance/{branchcode}")
	public Double dailySalesReportTotalPreviousAdvance(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,

			@RequestParam("date") String reportdate) {
		
		java.util.Date fdate = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return salesTransHdrRepository.dayEndReceptModeWisePreviousCashSalesReport(fdate,companymstid,branchcode);

	}

	
	
	//----------------------------------
	
	@GetMapping("{companymstid}/dailysalesreportresource/receiptmodewiseprevioussalesreport/{branchcode}")
	public List<ReceiptModeReport> dayEndReceptModeWisePreviousSalesReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam("rdate") String reportdate) {
		
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySaleReportService.dayEndReceptModeWisePreviousSalesReport(date, companymstid, branchcode);

	}
	
	@GetMapping("{companymstid}/dailysalesreportresource/receiptmodewisepreviouscashsalesreport/{branchcode}")
	public Double dayEndReceptModeWisePreviouCashSalesReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam("rdate") String reportdate) {
		
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return salesTransHdrRepository.dayEndReceptModeWisePreviousCashSalesReport(date, companymstid, branchcode);

	}
	
	
	@GetMapping("{companymstid}/dailysalesreportresource/dayendreceiptbyreceiptmode/{branchcode}/{receiptmode}")
	public Double dayEndReceiptCashByReceiptMode(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "receiptmode") String receiptmode,
			@RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySalereportRepo.dayEndReceiptCashByReceiptMode(date, companymstid, branchcode, receiptmode);

	}
}
