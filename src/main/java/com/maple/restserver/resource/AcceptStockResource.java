package com.maple.restserver.resource;

/**
 *@GetMapping("{companymstid}/acceptstocks")
 * 
 *@PostMapping("{companymstid}/acceptstock")
 *@Valid @RequestBody AcceptStock acceptStock
 * 
 */

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;

@RestController

@Transactional
public class AcceptStockResource {
	@Autowired
	private AcceptStockRepository acceptStockRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;

	/**
	 * 
	 * @param companymstid
	 * @return  List<AcceptStock>
	 */
	@GetMapping("{companymstid}/acceptstocks")
	public List<AcceptStock> retrieveAllAcceptStock(@PathVariable(value = "companymstid") String companymstid) {
		return acceptStockRepository.findByCompanyMst(companymstid);
	}
	
	
	
	

	/**
	 * 
	 * @param companymstid
	 * @param FailedMessageMst
	 * @return AcceptStock
	 */
	@PostMapping("{companymstid}/acceptstock")
	public AcceptStock createDailyStock(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody AcceptStock acceptStock) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			acceptStock.setCompanyMst(companyMst);

			return acceptStockRepository.saveAndFlush(acceptStock);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));

	}

}
