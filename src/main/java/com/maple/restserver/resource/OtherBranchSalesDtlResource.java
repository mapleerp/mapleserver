package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.OtherBranchSalesDtl;
import com.maple.restserver.entity.OtherBranchSalesTransHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.OtherBranchSalesDtlRepository;
import com.maple.restserver.repository.OtherBranchSalesTransHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Transactional
public class OtherBranchSalesDtlResource {
	
private static final Logger logger = LoggerFactory.getLogger(OtherBranchSalesDtlResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	@Autowired
	private OtherBranchSalesTransHdrRepository otherBranchSalesTransHdrRepository;

	@Autowired
	private OtherBranchSalesDtlRepository otherBranchSalesDtlRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@PostMapping("{companymstid}/saveotherbranchsalesdtl/{hdrId}")
	public OtherBranchSalesDtl createOtherBranchSalesDtl(
			@PathVariable(value = "hdrId") String hdrId,
			@PathVariable(value = "companymstid") String companymstid, @Valid @RequestBody OtherBranchSalesDtl otherBranchSalesDtl) {

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		return otherBranchSalesTransHdrRepository.findById(hdrId).map(otherBranchSalesTransHdr -> {
			otherBranchSalesDtl.setCompanyMst(companyMst.get());

			otherBranchSalesDtl.setOtherBranchSalesTransHdr(otherBranchSalesTransHdr);

			//OtherBranchSalesDtl otherBranchSalesDtlSaved = otherBranchSalesDtlRepository.saveAndFlush(otherBranchSalesDtl);
			OtherBranchSalesDtl otherBranchSalesDtlSaved =saveAndPublishService.saveOtherBranchSalesDtl(otherBranchSalesDtl, mybranch,otherBranchSalesDtl.getOtherBranchSalesTransHdr().getBranchCode());
//				offerChecking(salesDtl, companyMst.get());
				return otherBranchSalesDtlSaved;

		}).orElseThrow(() -> new ResourceNotFoundException("salestranshdrId " + hdrId + " not found"));

	}
	
	@GetMapping("{companymstid}/otherbranchsalesdtl/{hdrid}/saleswindowsummaryfordiscount")
	public SummarySalesDtl getOtherBranchSalesWindowDiscountSummaryByHdrId(
			@PathVariable(value = "hdrid") String hdrid) {

		return salessummaryDiscount(hdrid);

	}

	private SummarySalesDtl salessummaryDiscount(String hdrid) {
		List<Object> objList = otherBranchSalesDtlRepository.findSumBranchSalesDtlforDiscount(hdrid);

		Object[] objAray = (Object[]) objList.get(0);

		SummarySalesDtl summary = new SummarySalesDtl();
		summary.setTotalQty((Double) objAray[0]);
		summary.setTotalAmount((Double) objAray[1]);
		summary.setTotalTax((Double) objAray[2]);
		summary.setTotalCessAmt((Double) objAray[3]);

		return summary;
	}
	
	
	@GetMapping("{companymstid}/otherbranchsalesdtl/{salesTransHdrId}/saleswindowsummary")
	public SummarySalesDtl getOtherBranchSalesWindowSummaryByHdrId(
			@PathVariable(value = "salesTransHdrId") String salesTransHdrId) {

		return salessummary(salesTransHdrId);

	}

	private SummarySalesDtl salessummary(String salesTransHdrId) {
		List<Object> objList = otherBranchSalesDtlRepository.findBranchSumSalesDtl(salesTransHdrId);

		Object[] objAray = (Object[]) objList.get(0);

		SummarySalesDtl summary = new SummarySalesDtl();
		summary.setTotalQty((Double) objAray[0]);
		summary.setTotalAmount((Double) objAray[1]);
		summary.setTotalTax((Double) objAray[2]);
		summary.setTotalCessAmt((Double) objAray[3]);

		return summary;
	}
	
	@DeleteMapping("{companymstid}/deleteallotherbranchsalesdtlbyhdrid/{id}")
	public void OtherBranchSalesDtlDelete(@PathVariable(value = "id") String id,
			@PathVariable(value = "companymstid") String companymstid) {
		Optional<OtherBranchSalesTransHdr>  otherBranchSalesTransHdr = otherBranchSalesTransHdrRepository.findById(id);
		otherBranchSalesDtlRepository.deleteByOtherBranchSalesTransHdr(otherBranchSalesTransHdr.get());

	}
	
	@GetMapping("{companymstid}/otherbranchsalesdtl/{hdrid}/otherbranchsalesdtl")
	public List<OtherBranchSalesDtl> getOtherBranchSalesWindowDiscountSummaryByHdrId(
			@PathVariable(value = "hdrid") String hdrid,
			@PathVariable(value = "companymstid") String companymstid) {

		return otherBranchSalesDtlRepository.findByOtherBranchSalesTransHdr(hdrid);

	}

}
