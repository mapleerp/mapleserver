package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.TrialBalanceDtl;
import com.maple.restserver.entity.TrialBalanceHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.TrialBalanceDtlRepository;
import com.maple.restserver.repository.TrialBalanceHdrRepository;

@RestController
@Transactional
public class TrialBalanceDtlResource {
	
	@Autowired
	TrialBalanceDtlRepository trialBalanceDtlRepository;
//	
//	@Autowired
//	TrialBalanceHdrRepository trialBalanceHdrRepository;
//	
	//==========================get trial balance dtl by hdrid===============
	@GetMapping("{companymstid}/trialbalancedtlresource/{trialBalanceHdrId}")
	public List<Object> getTrialBalanceDtlByHdrId(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="trialBalanceHdrId") String trialBalanceHdrId){

		return trialBalanceDtlRepository.getTrialBalanceDtlByHdrId(trialBalanceHdrId);
	}

}
