package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.DamageEntryReport;
import com.maple.restserver.report.entity.WriteOffDetailsAndSummaryReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.DamageEntryReportServive;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class DamageEntryReportResource {

	@Autowired
	DamageEntryReportServive 	damageEntryReportServive;



	@Autowired
	CompanyMstRepository companyMstRepository;
	@GetMapping("{companymstid}/damageentryreport/{branchCode}")
	public List<DamageEntryReport> getDamageEntryReport(
			@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate,@PathVariable("branchCode") String branchCode,
			@PathVariable(value = "companymstid") String  companymstid) {
		java.util.Date fDate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.util.Date tDate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");

Optional<CompanyMst> companyMst=companyMstRepository.findById(companymstid);

		return damageEntryReportServive.getDamageEntryReport(companyMst.get(), fDate,tDate,branchCode);

	}
	
	@GetMapping("{companymstid}/damageentryreportresource/writeoffdetailandsummary/{branchCode}/{reason}")
	public List<WriteOffDetailsAndSummaryReport> getWriteOffDetailAndSummary(
			@RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate,
			@PathVariable("branchCode") String branchCode,
			@PathVariable("reason") String reason,
			@PathVariable(value = "companymstid") String  companymstid) {
		java.util.Date fDate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.util.Date tDate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");

Optional<CompanyMst> companyMst=companyMstRepository.findById(companymstid);

		return damageEntryReportServive.getWriteOffDetailAndSummary(companyMst.get(), fDate,tDate,branchCode,reason);

	}
}
