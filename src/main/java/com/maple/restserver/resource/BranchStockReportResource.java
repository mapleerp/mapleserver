package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.BranchStockReport;
import com.maple.restserver.report.entity.WholeSaleDetailReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.BranchStockReportService;
import com.maple.restserver.service.WholeSaleDetailReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class BranchStockReportResource {
	@Autowired
	BranchStockReportService branchStockReportService;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@GetMapping("{companymstid}/branchstockreportresource/getbranchstockreport")		
	public List<BranchStockReport> getBranchStockReport(
			@PathVariable(value = "companymstid") String companymstid,
			
			@RequestParam("date") String ddate) {
		
		java.util.Date date = SystemSetting.StringToUtilDate(ddate, "yyyy-MM-dd");
		

		return branchStockReportService.findBranchStockReport(date);
	}
	
	@GetMapping("{companymstid}/branchstockreportresource/getpharmacystockreportbranchwise/{branchCode}")		
	public List<BranchStockReport> getPharmacyStockReportBranchWise(
			@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchCode") String branchCode,
			
			@RequestParam("startDate") String startDate) {
		
		java.util.Date date = SystemSetting.StringToUtilDate(startDate, "yyyy-MM-dd");
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		return branchStockReportService.findPharmacyStockReportBranchWise(companyMst,date,branchCode);
	}
	
	@GetMapping("{companymstid}/branchstockreportresource/getpharmacystockreportbranchwiseanditem/{branchCode}")		
	public List<BranchStockReport> getPharmacyStockReportBranchWiseWithCategory(
			@PathVariable(value = "companymstid") String companymstid,@RequestParam("category") String category,
	@PathVariable(value = "branchCode") String branchCode,
			
			@RequestParam("startDate") String startDate) {
		
		java.util.Date date = SystemSetting.StringToUtilDate(startDate, "yyyy-MM-dd");
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		
		String categoryNames=category;
		
		String[]array=categoryNames.split(";");

		return branchStockReportService.findPharmacyStockReportBranchWiseWithCategory(companyMst,date,branchCode,array);
	}

}
