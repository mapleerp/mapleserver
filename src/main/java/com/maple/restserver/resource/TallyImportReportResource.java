package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.TallyImportReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.TallyImportReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class TallyImportReportResource {

	@Autowired
	CompanyMstRepository  companyMstRepository;
	
	
	@Autowired
	TallyImportReportService tallyImportReportService;
	
	
	

	@GetMapping("/{companymstid}/tallyimportreportresource/tallyimportreport/{branchcode}")
	public List<TallyImportReport> tallyImportReportReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, 
			@RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	java.util.Date TDate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		return tallyImportReportService.tallyImportReportReport(companymstid,branchcode,fDate,TDate);
	}
	
	
}
