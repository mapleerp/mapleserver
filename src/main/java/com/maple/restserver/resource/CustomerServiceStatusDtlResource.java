package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CustomerServiceStatusDtl;
import com.maple.restserver.entity.CustomerServiceStatusHdr;
import com.maple.restserver.entity.ServiceInDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CustomerServiceStatusDtlRepository;
import com.maple.restserver.repository.CustomerServiceStatusHdrRepository;

@RestController
public class CustomerServiceStatusDtlResource {
	
	@Autowired
	CustomerServiceStatusHdrRepository customerServiceStatusHdrRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	CustomerServiceStatusDtlRepository customerServiceStatusDtlRepo;
	
	 @PostMapping("{companymstid}/customerservicestatusdtl/{servicestatushdrid}")
	    public CustomerServiceStatusDtl createCustomerServiceStatusDtl(@PathVariable (value = "servicestatushdrid") String servicestatushdrid,
	                                 @Valid @RequestBody CustomerServiceStatusDtl customerServiceStatusDtl) {
	        return customerServiceStatusHdrRepo.findById(servicestatushdrid).map(CustomerServiceStatusHdr -> {
	        	customerServiceStatusDtl.setCustomerServiceStatusHdr(CustomerServiceStatusHdr);
	            return customerServiceStatusDtlRepo.saveAndFlush(customerServiceStatusDtl);
	        }).orElseThrow(() -> new ResourceNotFoundException("customerServiceStatusDtl" + servicestatushdrid + " not found"));
	    }
	 
	 @GetMapping("{companymstid}/customerservicestatusdtl/{customerservicehdrid}/customerservicestatusdtls")
		public List<CustomerServiceStatusDtl> retriveAllCustomerServiceStatusDtlHdrId(
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "customerservicehdrid") String customerservicehdrid) {

		 Optional<CompanyMst> companyMstOpt =companyMstRepo. findById(companymstid);
			return customerServiceStatusDtlRepo.findByCompanyMstAndCustomerServiceStatusHdr(companyMstOpt.get(), customerservicehdrid);


	}
	 @DeleteMapping("{companymstid}/customerservicestatusdtl/deletebyid/{hdrid}")
		public void customerservicestatusdtlDelete(@PathVariable String hdrid) {
		 customerServiceStatusDtlRepo.deleteById(hdrid);

		}
		
}
