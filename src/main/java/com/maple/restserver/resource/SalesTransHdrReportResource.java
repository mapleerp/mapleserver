package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.report.entity.B2bSaleReport;
import com.maple.restserver.report.entity.BranchWiseProfitReport;
import com.maple.restserver.report.entity.HsnWisePurchaseDtlReport;
import com.maple.restserver.report.entity.HsnWiseSalesDtlReport;
import com.maple.restserver.report.entity.PharmacyGroupWiseSalesReport;
import com.maple.restserver.report.entity.PharmacySalesDetailReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.SalesTaxSplitReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.service.SalesTransHdrReportService;
import com.maple.restserver.utils.SystemSetting;
@CrossOrigin("http://localhost:4200")
@RestController
@Transactional
public class SalesTransHdrReportResource {
	@Autowired
	SalesTransHdrReportService salesTransHdrReportService;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	

	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@GetMapping("{companymstid}/salestranshdrreport/salesanalysisreport/{branchCode}")
	public List<SalesInvoiceReport> getSalesAnalysisReport(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fdate") String fdate,
			@RequestParam("tdate") String tdate,@PathVariable(value = "branchCode") String branchCode) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");

		return salesTransHdrReportService.getSalesAnalysisReport(sdate,edate,companymstid,branchCode);

	}

	@GetMapping("{companymstid}/salestranshdrreport/dailycustomerwisereport/{branchCode}")
	public List<SalesTransHdr> getCustmerWiseReport(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("rdate") String reportdate,@PathVariable(value = "branchCode") String branchCode) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return salesTransHdrRepository.findByVoucherDate(date,branchCode);

	}

	@GetMapping("{companymstid}/salestranshdrreport/b2bsalesreport/{branchCode}")
	public List<B2bSaleReport> getB2BSalesReport(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchCode") String branchCode, @RequestParam("rdate") String reportdate,@RequestParam("tdate") String todate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		java.util.Date tdate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);

		return salesTransHdrReportService.getB2BSalesReport(date,tdate, branchCode, companyMst.get());

	}
	//Hot cakes new report format
	@GetMapping("{companymstid}/salestranshdrreport/b2bsalesreportdtl/{branchCode}")
	public List<B2bSaleReport> getB2BSalesReportDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchCode") String branchCode, @RequestParam("rdate") String reportdate,
			@RequestParam("tdate") String todate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		java.util.Date tdate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);

		return salesTransHdrReportService.getB2BSalesReportDtl(date,tdate, branchCode, companyMst.get());

	}

	@GetMapping("{companymstid}/salestranshdrreport/brnchwiseprofit")
	public List<BranchWiseProfitReport> getBranchWiseProfit(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fdate") String fromdate, @RequestParam("tdate") String todate) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);

		return salesTransHdrReportService.getBranchWiseProfit(sdate, edate, companyMst.get());
		
	}

	@GetMapping("{companymstid}/salestranshdrreport/profitbybranch/{branchcode}")
	public List<BranchWiseProfitReport> getBranchWiseProfit(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchCode, @RequestParam("fdate") String fromdate,
			@RequestParam("tdate") String todate) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);

		return salesTransHdrReportService.getProfitByBranch(sdate, edate, companyMst.get(), branchCode);

	}
	

	
	//version 3.1
	@GetMapping("{companymstid}/salestranshdrreport/salesdtlreport/{branchcode}")
	public List<SalesInvoiceReport> getSalesDtlReport(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchCode, @RequestParam("fdate") String fromdate,
			@RequestParam("tdate") String todate) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		
		return salesTransHdrReportService.getSalesDtlReport(sdate, edate, companyMst.get(), branchCode);

	}
	
	//version 3.1 end
	


	
	//---------------version 4.11
	
		@GetMapping("{companymstid}/salestranshdrreport/taxsplit/{branchcode}")
		public List<SalesTaxSplitReport> SaleTaxSplit(@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "branchcode") String branchCode, 
				@RequestParam("tdate") String todate) {
			java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
			Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
			
			return salesTransHdrReportService.SaleTaxSplit(edate, companyMst.get(), branchCode);

		}
		
		//-----------------version 4.11 end
		
		
	
	
		
		//////////////....sibi.....................////08-07-2021..............//

		
//		@GetMapping("/{companymstid}/salestranshdrreport/{category}/getpharmacygroupwisesalesreport/{branchCode}")
//		public List<PharmacyGroupWiseSalesReport>getPharmacyGroupWiseSalesReport(@PathVariable(value = "companymstid") String
//				  companymstid,
//				 @PathVariable("branchCode") String branchCode, @PathVariable("category") String category,  @RequestParam("fdate") String fromdate,  @RequestParam("tdate") String todate){
//			java.util.Date fdate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
//			java.util.Date  tdate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
//			
//			
//		String categoryNames=category;
//			
//			String[]array=categoryNames.split(";");
//			Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
//			CompanyMst companyMst = companyOpt.get();
//			return salesTransHdrReportService.getPharmacyGroupWiseSalesReport(companyMst ,fdate,tdate,branchCode,array);
//
//		
//		
//	
//		}
		
		@GetMapping("/{companymstid}/salestranshdrreport/getpharmacygroupwisesalesreport/{branchCode}")
		public List<PharmacyGroupWiseSalesReport>getPharmacyGroupWiseSalesReport(@PathVariable(value = "companymstid") String
				  companymstid,
				// @PathVariable("branchCode") String branchCode, @PathVariable("category") String category,  @RequestParam("fdate") String fromdate,  @RequestParam("tdate") String todate){
				  @PathVariable("branchCode") String branchCode,  
					 @RequestParam("fdate") String fromdate,  @RequestParam("tdate") String todate,
					 @RequestParam("category") String category){
			java.util.Date fdate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
			java.util.Date  tdate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
			
			
	     	String categoryNames=category;
			
			String[]array=categoryNames.split(";");
			Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
			CompanyMst companyMst = companyOpt.get();
			return salesTransHdrReportService.getPharmacyGroupWiseSalesReport(companyMst ,fdate,tdate,branchCode,array);

		
		
	
		}



//		@GetMapping("{companymstid}/salestranshdrreport/{category}/getpharmacygroupwisesalesdtlreport/{branchCode}")
//		public List<PharmacySalesDetailReport>getPharmacyGroupWiseSalesDtlReport(@PathVariable(value = "companymstid") String
//				  companymstid,
//				 @PathVariable("branchCode") String branchCode, @PathVariable("category") String category, 
//				 @RequestParam("fdate") String fromdate,  @RequestParam("tdate") String todate){
//			java.util.Date fdate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
//			java.util.Date  tdate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
//			
//			
//		   String categoryNames=category;
//			
//			String[]array=categoryNames.split(";");
//			Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
//			CompanyMst companyMst = companyOpt.get();
//			return salesTransHdrReportService.getPharmacyGroupWiseSalesDtlReport(companyMst ,fdate,tdate,branchCode,array);
//
//		}
//		@GetMapping("/{companymstid}/salestranshdrreport/getpharmacygroupwisesalesdtlreport/{branchCode}")
//		public List<PharmacySalesDetailReport>getPharmacyGroupWiseSalesDtlReport(@PathVariable(value = "companymstid") String
//				  companymstid,
//				 @PathVariable("branchCode") String branchCode, @RequestParam("fdate") String fromdate,  
//				 @RequestParam("category") String category, @RequestParam("tdate") String todate){

		@GetMapping("/{companymstid}/salestranshdrreport/getpharmacygroupwisesalesdtlreport/{branchCode}")
		public List<PharmacySalesDetailReport>getPharmacyGroupWiseSalesDtlReport(@PathVariable(value = "companymstid") String
				  companymstid,
				 @PathVariable("branchCode") String branchCode,  
				 @RequestParam("fdate") String fromdate,  @RequestParam("tdate") String todate,
				 @RequestParam("category") String category){

			java.util.Date fdate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
			java.util.Date  tdate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
			
			
		   String categoryNames=category;
			
			String[]array=categoryNames.split(";");
			Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
			CompanyMst companyMst = companyOpt.get();
			return salesTransHdrReportService.getPharmacyGroupWiseSalesDtlReport(companyMst ,fdate,tdate,branchCode,array);

		
		
	
		}
		
		//------------------new version 1.5 surya 
		
		@GetMapping("{companymstid}/salestranshdrreport/customerwiseprofit/{branchcode}")
		public List<BranchWiseProfitReport> getCustomerWiseProfit(@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "branchcode") String branchCode, 
				@RequestParam("fdate") String fromdate,
				@RequestParam("tdate") String todate) {
			java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
			java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
			Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);

			return salesTransHdrReportService.getCustomerWiseProfit(sdate, edate, companyMst.get(), branchCode);

		}
		
		//------------------new version 1.5 surya  end

		@GetMapping("{companymstid}/salestranshdrreport/hsnsalesdtlreport/{branchcode}")
		public List<HsnWiseSalesDtlReport> hsnCodeSalesDtlReport(@PathVariable(value = "companymstid")
		String companymstid,@PathVariable(value = "branchcode")String branchcode,
		@RequestParam("fromdate") String fromdate,
		@RequestParam("todate") String todate,
		@RequestParam(value = "categoryName")String categoryNames)
		{
			Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
			java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
			java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
			if(categoryNames.equalsIgnoreCase(""))
			{
				return salesTransHdrReportService.getHsnWiseSalesDtlReportWithoutGroup(fromDate,toDate,branchcode,companyMstOpt.get());

			}
			else
			{
			String[]array=categoryNames.split(";");
			return salesTransHdrReportService.getHsnWiseSalesReportWithGoup(fromDate,toDate,array,branchcode,companyMstOpt.get());
			}
			}
		@GetMapping("{companymstid}/salestranshdrreport/gstsalesreport/{branchCode}")
		public List<B2bSaleReport> getNewB2CSalesReport(@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "branchCode") String branchCode, @RequestParam("rdate") String reportdate,
				@RequestParam("tdate") String todate) {
			java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
			java.util.Date tdate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");

			Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);

			return salesTransHdrReportService.getNewB2CSalesReport(date, tdate, branchCode, companyMst.get());

		}
		
		@GetMapping("{companymstid}/salestranshdrreport/getwholesalesreport/{branchCode}")
		public List<SalesInvoiceReport> getWholesSalesInvoiceReport(@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "branchCode") String branchCode, @RequestParam("rdate") String reportdate,
				@RequestParam("tdate") String todate) {
			java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
			java.util.Date tdate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
			Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
			return salesTransHdrReportService.getWholesSalesInvoiceReport(date, tdate, branchCode, companyMst.get());
		}
}
