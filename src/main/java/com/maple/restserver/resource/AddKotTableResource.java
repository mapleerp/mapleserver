package com.maple.restserver.resource;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AddKotTable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DeliveryBoyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AddKotTableRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
 
@RestController
@Transactional
public class AddKotTableResource {
private static final Logger logger = LoggerFactory.getLogger(AddKotTableResource.class);
	
	//----------------version 5.0 surya
	@Value("${mybranch}")
	private String mybranch;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	@Autowired
	private AddKotTableRepository addKotTableRepository;
	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	
	@GetMapping("/{companymstid}/addkottables")
	public List<AddKotTable> retrieveAllAddKotTable(@PathVariable(value = "companymstid") String companymstid){
		return addKotTableRepository.findByCompanyMstId(companymstid);
	}
	
	  
	 
	@GetMapping("/{companymstid}/getkottablesbystatus")
	public List<AddKotTable> retrievekottablesbystatus(@PathVariable(value = "companymstid") String companymstid){
		return addKotTableRepository.getKotTablebyStatus(companymstid);
	}
		
	@GetMapping("{companymstid}/getkottablebyname/{tablename}/{branchcode}")

	public AddKotTable retrievKotTableByName(@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="branchcode") String branchcode,
			@PathVariable(value="tablename") String tablename){
		
		
		return addKotTableRepository.findByCompanyMstIdAndTableName(companymstid, tablename);
	}
	
	@GetMapping("{companymstid}/getkottablebyid/{id}")

	public AddKotTable retrievKotTableById(@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="id") String id){
		
		
		return addKotTableRepository.findById(id).get();
	}
	

	@PostMapping("/{companymstid}/addkottable")
	public AddKotTable createDailyStock(@PathVariable(value = "companymstid") String companymstid,@Valid @RequestBody 
			AddKotTable addKotTable)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			addKotTable.setCompanyMst(companyMst);
//			AddKotTable savedAddKotTable = addKotTableRepository.saveAndFlush(addKotTable);
			AddKotTable savedAddKotTable =saveAndPublishService.saveAddKotTable(addKotTable, addKotTable.getBranchCode());
			logger.info("AddKotTable send to KafkaEvent: {}", savedAddKotTable);
	        Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", savedAddKotTable.getId());

			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", savedAddKotTable.getId());

			variables.put("companyid", savedAddKotTable.getCompanyMst());
			variables.put("branchcode", savedAddKotTable.getBranchCode());

			variables.put("REST", 1);
			
			
			variables.put("WF", "forwardKotTable");
			
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardKotTable");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			
			variables.put("lmsqid", lmsQueueMst.getId());
			
			eventBus.post(variables);
			return savedAddKotTable;
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found"));
		
		/*
		 * WorkFlow
		 * 
		 * forwardKotTable
		 */
	}
		
		 
		
		
	
	

}
