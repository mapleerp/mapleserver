package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesReturnDtl;
import com.maple.restserver.entity.SalesReturnHdr;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesRetunDtlRepository;
import com.maple.restserver.repository.SalesReturnHdrRepository;

@RestController
@Transactional
public class SalesReturnDtlResource {
	@Autowired
	SalesRetunDtlRepository salesRetunDtlRepository;
	
	@Autowired
	SalesReturnHdrRepository salesReturnHdrRepository;

@Autowired
CompanyMstRepository companyMstRepository;

	@PostMapping("{companymstid}/salesreturndtl")
	public SalesReturnDtl createSalesReturnDtl(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 SalesReturnDtl salesReturnDtl)
	{
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		
		Optional<SalesReturnHdr> salesHdr = salesReturnHdrRepository.findById(salesReturnDtl.getSalesReturnHdr().getId());
		
		
		SalesReturnDtl addedItem = salesRetunDtlRepository.
				findBySalesReturnHdrAndItemIdAndUnitIdAndBatchAndBarcodeAndRate(salesHdr.get(),salesReturnDtl.getItemId(),
						salesReturnDtl.getUnitId(),salesReturnDtl.getBatch(),salesReturnDtl.getBarcode(),salesReturnDtl.getRate());
		
		if(null != addedItem && null != addedItem.getId())
		{
			addedItem.setQty(addedItem.getQty()+salesReturnDtl.getQty());
			addedItem.setCgstAmount(salesReturnDtl.getCessAmount());
			addedItem.setSgstAmount(salesReturnDtl.getSgstAmount());
			addedItem.setCessAmount(salesReturnDtl.getCessAmount());
			addedItem.setAmount(addedItem.getQty() * salesReturnDtl.getMrp());
			addedItem.setRate(salesReturnDtl.getRate());
			addedItem.setMrp(salesReturnDtl.getMrp());
			
			  return salesRetunDtlRepository.saveAndFlush(addedItem);
		}
		
	
		salesReturnDtl.setCompanyMst(companyMst);
		
	   return salesRetunDtlRepository.saveAndFlush(salesReturnDtl);
		
		} 
	
	@GetMapping("{companymstid}/salesreturnhdr/{salesreturnrId}/saleswindowsummary")
	public SummarySalesDtl getSalesWindowSummaryByHdrId(
			@PathVariable(value ="salesreturnrId") String salesreturnrId) {
		List<Object> objList =  salesRetunDtlRepository.findSumSalesDtl(salesreturnrId);
		 
		Object[] objAray = (Object[]) objList.get(0);
 
		SummarySalesDtl summary = new SummarySalesDtl();
		summary.setTotalQty((Double) objAray[0]);
		summary.setTotalAmount((Double) objAray[1]);
		summary.setTotalTax((Double)objAray[2]);
		summary.setTotalCessAmt((Double)objAray[3]);
		
		return summary;

	}
	

	@DeleteMapping("{companymstid}/salesreturndtl/salesreturndtlsdelete/{salesreturndtlId}")
	public void salesDtlDelete(@PathVariable(value="salesreturndtlId") String salesreturndtlId) {
		salesRetunDtlRepository.deleteById(salesreturndtlId);

	}

	
	 
	
	
	@GetMapping("{companymstid}/salesreturndtl/{salesreturnHdrId}/salesreturndtls")
	public List<SalesReturnDtl> retrieveAllSalesReturnDtlBySalesReturnHdrId(@PathVariable (value = "salesreturnHdrId") String salesreturnHdrId,
            Pageable pageable) {
		return salesRetunDtlRepository.findBySalesReturnHdrId(salesreturnHdrId);
	}
	
	
	@PostMapping("{companymstid}/salesreturndtlbyitemidandhdr/{salesreturnHdrId}")
	public SalesReturnDtl retrieveAllSalesReturnDtlBySalesReturnHdrIdAndItem(
			@PathVariable (value = "salesreturnHdrId") String salesreturnHdrId,
			@Valid @RequestBody SalesDtl salesDtl) {
		
		Optional<SalesReturnHdr> salesReturnHdrOpt = salesReturnHdrRepository.findById(salesreturnHdrId);
		
		
		return salesRetunDtlRepository.
				findBySalesReturnHdrAndItemIdAndUnitIdAndBatchAndBarcodeAndRate(salesReturnHdrOpt.get(),salesDtl.getItemId(),
						salesDtl.getUnitId(),salesDtl.getBatch(),salesDtl.getBarcode(),salesDtl.getRate());
		
	}
	
	
}
