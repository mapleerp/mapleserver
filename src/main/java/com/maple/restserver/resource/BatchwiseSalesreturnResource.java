package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BatchWiseSalesReturnReport;
import com.maple.restserver.service.BatchwiseSalesReturnService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class BatchwiseSalesreturnResource {
@Autowired
BatchwiseSalesReturnService batchwiseSalesReturnService;
	

	@GetMapping("/{companymstid}/batchwisesalesreturnreport/{branchcode}/{itemgroup}")
	public List<BatchWiseSalesReturnReport> BatchwiseSalesReturnreport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,	@PathVariable(value = "itemgroup") List<String> itemgroup,  
			@RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	java.util.Date TDate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		return batchwiseSalesReturnService.batchwiseSalesReturnreport(companymstid,branchcode,fDate,TDate,itemgroup);
	}
}
