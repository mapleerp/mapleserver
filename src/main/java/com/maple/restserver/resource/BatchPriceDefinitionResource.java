package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Query;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.BatchPriceDefinition;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.report.entity.ItemNotInBatchPriceReport;
import com.maple.restserver.repository.BatchPriceDefinitionRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.BatchPriceDefinitionService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class BatchPriceDefinitionResource {
	private static final Logger logger = LoggerFactory.getLogger(BatchPriceDefinitionResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	BranchMstRepository branchMstRepo;
	
	@Autowired
	BatchPriceDefinitionService batchPriceDefinitionService;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;


	
	
	@Autowired
	private BatchPriceDefinitionRepository batchPriceDefinitionRepo;
	

	@Autowired
	private CompanyMstRepository companyMstRepository;
	
 

	@Autowired
	private VoucherNumberService voucherNumberService;
	
	
	@PostMapping("{companymstid}/batchpricedefinition")
	public BatchPriceDefinition createBatchPriceDefinition(@Valid @RequestBody BatchPriceDefinition batchPriceDefinition,
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		batchPriceDefinition.setCompanyMst(companyMst);
		
		VoucherNumber vouvherObj = voucherNumberService.generateInvoice("BPD", companymstid);
		String vno = vouvherObj.getCode();
		batchPriceDefinition.setId(vno);
			
		batchPriceDefinitionRepo.UpdatePriceDefinitionbyItemIdUnitIdBatch(batchPriceDefinition.getItemId(),
				batchPriceDefinition.getStartDate(), batchPriceDefinition.getPriceId(), batchPriceDefinition.getUnitId(),batchPriceDefinition.getBatch());

			
		
//		BatchPriceDefinition saved = batchPriceDefinitionRepo.saveAndFlush(batchPriceDefinition);
		BatchPriceDefinition saved = saveAndPublishService.saveBatchPriceDefinition(batchPriceDefinition,batchPriceDefinition.getBranchCode());
		logger.info("BatchPriceDefinition send to KafkaEvent: {}", saved);
		  Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", saved.getId());

			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", saved.getId());

			variables.put("companyid", saved.getCompanyMst());
			variables.put("branchcode", saved.getBranchCode());

			variables.put("REST", 1);
			
			
			variables.put("WF", "forwardBatchPriceDefinition");
			
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardBatchPriceDefinition");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			
			variables.put("lmsqid", lmsQueueMst.getId());
			
			eventBus.post(variables);
		
		return saved;
	}
	
	@GetMapping("/{companymstid}/getbatchpricdefiniton")

	public @ResponseBody List<BatchPriceDefinition> getPriceDefinition() {

		return batchPriceDefinitionRepo.fetchPriceDefinition();
	}
	
	@DeleteMapping("/{companymstid}/batchpricdefinition/{id}")
	public void BatchpricedefinitionDelete(@PathVariable(value = "id") String Id) {
		batchPriceDefinitionRepo.deleteById(Id);

	}
	@GetMapping("{companymstid}/batchpricedefinition/{itemid}/{priceId}/{unitId}/{batch}/batchpricedefinitionbyitemandunitandbatch")
	public BatchPriceDefinition retrieveAllPriceDefinitionByItemIdAndUnitAndBatch(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "priceId") String priceId, @PathVariable(value = "unitId") String unitId,
			@PathVariable(value = "batch") String batch,
			@RequestParam(value = "tdate")String tdate) {

		Date udate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
		//--------------------new version 1.10 surya 
		BatchPriceDefinition batchPriceDefinition = batchPriceDefinitionService.priceDefinitionByItemIdAndUnitAndBatch(itemid,priceId,unitId,batch,udate);
		
		return batchPriceDefinition;
	
	}
	
	
	@GetMapping("{companymstid}/batchpricedefinition/{itemid}/{priceId}/{batch}/batchpricedefinitionbyitem")
	public List<BatchPriceDefinition> retrieveAllPriceDefinitionByItemId(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "priceId") String priceId,
			@PathVariable(value = "batch") String batch,
			@RequestParam(value = "tdate") String tdate) {

		Date udate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
		List<BatchPriceDefinition> batchList =  batchPriceDefinitionRepo.findByItemIdAndStartDate(itemid, priceId,batch,udate);
		if(batchList.size()>0)
		{
			return batchList;
		}
		else
		{
			List<BatchPriceDefinition> batchListEndDateNull =  batchPriceDefinitionRepo.findByItemIdAndStartDateEnddateNull(itemid, priceId,batch,udate);
			if(batchListEndDateNull.size()>0)
			{
				return batchListEndDateNull;
			}
		}
		return null;
	}
	
	//---------------------new version 1.3  surya
	@GetMapping("{companymstid}/batchpricedefinition/batchpricedefinitionbyitemid/{itemid}")
	public List<BatchPriceDefinition> getAllPriceDefinitionByItemId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid) {
		
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);

		return batchPriceDefinitionRepo.findByCompanyMstAndItemId(companyOpt.get(),itemid);
	}
	
	
	@GetMapping("{companymstid}/batchpricedefinition/batchpricedefinitionbypricetypeid/{pricetypeid}")
	public List<BatchPriceDefinition> getAllPriceDefinitionByPriceTypeId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "pricetypeid") String pricetypeid) {
		
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);

		return batchPriceDefinitionRepo.findByCompanyMstAndPriceId(companyOpt.get(),pricetypeid);
	}
	
	
	@GetMapping("{companymstid}/batchpricedefinition/batchpricedefinitionbycostprice/{itemid}/{pricetypeid}/{batch}/{unitId}")
	public List<BatchPriceDefinition> getAllPriceDefinitionByCostPrice(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "batch") String batch,
			@PathVariable(value = "unitId") String unitid,
			@PathVariable(value = "pricetypeid") String priceid,
			@RequestParam("date") String date) {
		
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		
		Date sdate = SystemSetting.StringToUtilDate(date, "yyyy-MM-dd");

		return batchPriceDefinitionRepo.
				findByCompanyMstAndItemIdAndPriceIdAndUnitIdAndBatchAndStartDate
				(companyOpt.get(),itemid,priceid,unitid,batch,sdate);
	}
	
	@GetMapping("{companymstid}/batchpricedefinition/getitemsnotinbatchpricedefintion")
	public List<ItemNotInBatchPriceReport> getItemNotInBatchPrice(
			@PathVariable(value = "companymstid") String companymstid) {
		return batchPriceDefinitionService.getItemsNotInBatchPriceDefinition();
	}
	
	@GetMapping("{companymstid}/batchpricedefinition/getbatchpricebyitemidandpriceid/{itemid}/{priceid}")
	public List<BatchPriceDefinition> getBatchPriceByItemIdAndPriceId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "priceid") String priceid) {
		return batchPriceDefinitionRepo.findByItemIdAndPriceId(itemid,priceid);
	}
	
	@GetMapping("{companymstid}/batchpricedefinition/getbatchpricedefinition/{itemid}/{priceid}/{unitid}")
	public BatchPriceDefinition getBatchPriceDefinition(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "priceid") String priceid,
			@PathVariable(value = "unitid") String unitid) {

		return batchPriceDefinitionRepo.findBatchPriceDefinitionByItemUnitPriceAndDate(itemid, priceid,unitid,companymstid);
	}
	
	
	
	
}
