package com.maple.restserver.resource;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseAdditionalExpenseHdr;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseOrderHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PurchaseAdditionalExpenseHdrRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;

@RestController
public class PurchaseAdditionalExpenseHdrResource {


	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	PurchaseAdditionalExpenseHdrRepository purchaseAdditionalExpenseHdrRepo;
	
	@Autowired
	PurchaseDtlRepository purchaseDtlRepo;
	
	@PostMapping("{companymstid}/purchaseadditionalexpensehdr/savepurchaseadditionalexpense")
	public PurchaseAdditionalExpenseHdr createPurchaseAdditionalExpenseHdr(@Valid @RequestBody PurchaseAdditionalExpenseHdr purchaseAdditionalExpenseHdr,
			@PathVariable (value = "companymstid") String companymstid)
	{
		
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			purchaseAdditionalExpenseHdr.setCompanyMst(companyMst);
			 
			PurchaseAdditionalExpenseHdr saved=purchaseAdditionalExpenseHdrRepo.save(purchaseAdditionalExpenseHdr);
	 	    return saved;
	}
	
	@GetMapping("{companymstid}/purchaseadditionalexpensehdr/gethdrbyid/{id}")
	public Optional<PurchaseAdditionalExpenseHdr> getHdrById(@PathVariable(value ="companymstid")String companymstid,
			@PathVariable(value = "id")String id)
	{
		return purchaseAdditionalExpenseHdrRepo.findById(id);
	}
	
	@GetMapping("{companymstid}/purchaseadditionalexpensehdr/getbypurchasedtlid/{dtlid}")
	public PurchaseAdditionalExpenseHdr getHdrByPurchaseDtlId(@PathVariable(value ="companymstid")String companymstid,
			@PathVariable(value = "dtlid")String dtlid)
	{
		PurchaseDtl purchaseDtl = purchaseDtlRepo.findById(dtlid).get();
		return purchaseAdditionalExpenseHdrRepo.findByPurchaseDtl(purchaseDtl);
	}
	
	@DeleteMapping("{companymstid}/purchaseadditionalexpensehdr/deletedtlbypurchasedtlid/{dtlid}")
	public void deleteByPurchaseDtlId(@PathVariable(value = "dtlid")String dtlid)
	{
		purchaseAdditionalExpenseHdrRepo.deleteByPurchaseDtlId(dtlid);
	}
	
}
