package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.StockTransferInDtl;
import com.maple.restserver.entity.StockTransferInReturnDtl;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.StockTransferInDtlRepository;
import com.maple.restserver.repository.StockTransferInReturnDtlRepository;

@RestController
public class StockTransferInReturnDtlResource {
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	
	@Autowired
	StockTransferInReturnDtlRepository  stockTransferInReturnDtlRepository;
	
	
	@Autowired
	
	
	StockTransferInDtlRepository stockTransferInDtlRepository;
	
	

	//save 
	@Transactional
		@PostMapping("{companymstid}/stocktransferinreturndtlresource/savestocktransferinreturndtl")
		public StockTransferInReturnDtl createStockTransferInReturnDtl(
				@PathVariable(value="companymstid")	String commpanymstid,
				@Valid @RequestBody StockTransferInReturnDtl stockTransferInReturnDtl)
		{
			CompanyMst companymst=CompanyMstRepository.findById(commpanymstid).get();
			stockTransferInReturnDtl.setCompanyMst(companymst);
			stockTransferInReturnDtl=stockTransferInReturnDtlRepository.save(stockTransferInReturnDtl);
			
		String stockTransferInHdrId=	stockTransferInReturnDtl.getStockTransferInReturnHdr().getStockTransferInHdr().getId();
			
	StockTransferInDtl stockTransferInDtl=stockTransferInDtlRepository.findBystockTransferInHdrIdAndItemId(stockTransferInHdrId, 
			stockTransferInReturnDtl.getItemId());
	
	Double acceptinQty=stockTransferInDtl.getQty()-stockTransferInReturnDtl.getQty();
	
		stockTransferInReturnDtlRepository.updateStockTransferInDtlQty(stockTransferInHdrId,stockTransferInReturnDtl.getItemId(),acceptinQty);
		
		
			return stockTransferInReturnDtl;
		}
		
		
		// delete
		@DeleteMapping("{companymstid}/stocktransferinreturndtlresource/deletestocktransferinreturndtl/{id}")
		public void DeleteStockTransferInReturnDtl(@PathVariable(value = "id") String Id) {
			stockTransferInReturnDtlRepository.deleteById(Id);
		}
		
		
		

		@GetMapping("{companymstid}/stocktransferinreturndtlresource/showallstocktransferinreturndtl")
		public List<StockTransferInReturnDtl> retrieveAllStockTransferInReturnDtl(
				@PathVariable(value = "companymstid") String companymstid) {
			Optional<CompanyMst> companymstOpt=CompanyMstRepository.findById(companymstid);
			
			return stockTransferInReturnDtlRepository.findByCompanyMst(companymstOpt.get());

		}

	

}
