package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.PrescriptionDtl;
import com.maple.restserver.repository.PrescriptionDtlRepository;

@RestController
@Transactional
public class PrescriptionDtlResource {
	@Autowired
	private PrescriptionDtlRepository prescriptiondtl;
	@GetMapping("{companymstid}/prescription")
	public List<PrescriptionDtl> retrieveAlldepartments()
	{
		return prescriptiondtl.findAll();
		
	}
	@PostMapping("{companymstid}/prescription")
	public ResponseEntity<Object>createUser(@Valid @RequestBody PrescriptionDtl prescriptiondtl1)
	{
		PrescriptionDtl saved=prescriptiondtl.saveAndFlush(prescriptiondtl1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}

}
