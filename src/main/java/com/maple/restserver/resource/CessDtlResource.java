package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
 

import com.maple.restserver.entity.CessDtl;
 
import com.maple.restserver.repository.CessDtlRepository;
 


@RestController
@Transactional
public class CessDtlResource {
	
	@Autowired
	private CessDtlRepository cessRepository;
	
	@GetMapping("/{companymstid}/cessdtls")
	public List< CessDtl> retrieveAllCessDtl(@PathVariable(value = "companymstid") String companymstid){
		return cessRepository.findByCompanyMstId(companymstid);
	}
	
	
	
	@PostMapping("{companymstid}/cessdtl")
	public CessDtl createsCessdtl(@Valid @RequestBody 
			CessDtl  cessDtl)
	{
		CessDtl saved = cessRepository.saveAndFlush(cessDtl);
	 
		
		return saved;
	}

}
