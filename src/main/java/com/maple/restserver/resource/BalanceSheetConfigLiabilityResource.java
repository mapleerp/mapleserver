package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BalanceSheetConfigLiability;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.repository.BalanceSheetConfigLiabilityRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
@Transactional
public class BalanceSheetConfigLiabilityResource {

	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	BalanceSheetConfigLiabilityRepository balanceSheetConfigLiabilityRepo;

	@PostMapping("{companymstid}/balancesheetconfigliabilityresource/savebalancesheetconfigliability")
	public BalanceSheetConfigLiability createBalanceSheetConfigLiability(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 BalanceSheetConfigLiability balanceSheetConfigLiabilityDtl)
    {
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		balanceSheetConfigLiabilityDtl.setCompanyMst(companyMst.get());
       return balanceSheetConfigLiabilityRepo.saveAndFlush(balanceSheetConfigLiabilityDtl);
		 
	}
	
	
	@DeleteMapping("{companymstid}/balancesheetconfigassetresource/deletebalancesheetconfigliability/{id}")
	public void DeleteBalanceSheetConfigLiability(@PathVariable(value="id") String bid) {
		balanceSheetConfigLiabilityRepo.deleteById(bid);

	}

	@GetMapping("{companymstid}/balancesheetconfigliabilityresource/getallbalancesheetconfigliability")
	public List<BalanceSheetConfigLiability> getAllBalanceSheetConfigLiability(
			@PathVariable(value = "companymstid") String companymstid) {
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		
		return balanceSheetConfigLiabilityRepo.findByCompanyMst(companyMst.get());
	}
	
}
