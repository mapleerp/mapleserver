package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.AccountPayable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AccountPayableRepository;

import com.maple.restserver.repository.CompanyMstRepository;
 
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.service.AccountPayableService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class AccountPayableResource {
	
private static final Logger logger = LoggerFactory.getLogger(AccountPayableResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	
	 @Value("${serverorclient}")
		private String serverorclient;
	 
	    @Autowired
		SaveAndPublishService saveAndPublishService;
	     
	 EventBus eventBus = EventBusFactory.getEventBus();

	 @Autowired
	 AccountPayableService accountPayableService;
	@Autowired
	private AccountPayableRepository accountPayableRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;

	 
		
		@GetMapping("{companymstid}/accountpayable")
		public List<AccountPayable> retrieveAllAccountPayable(
				@PathVariable(value = "companymstid") String
				  companymstid){
			return accountPayableRepository.findByCompanyMstId(companymstid);
		}
		
		@PostMapping("/{companymstid}/accountpayables")
		public AccountPayable createAccountPayable(@PathVariable(value = "companymstid") String
				 companymstid,@Valid @RequestBody AccountPayable accountPayable)
		{
			Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
			
			CompanyMst companyMst = companyMstOpt.get();
			accountPayable.setCompanyMst(companyMst);
			
		    accountPayableRepository.save(accountPayable);
		    saveAndPublishService.saveAccountPayable(accountPayable,mybranch);
		    logger.info("accountPayable send to KafkaEvent: {}", accountPayable);
	
		
//			Map<String, Object> variables = new HashMap<String, Object>();
//
//			variables.put("voucherNumber", accountPayable.getPurchaseHdr().getVoucherNumber());
//			variables.put("voucherDate", accountPayable.getVoucherDate());
//			variables.put("inet", 0);
//			variables.put("id", accountPayable.getId());
//			variables.put("branchcode", accountPayable.getPurchaseHdr().getBranchCode());
//
//			variables.put("companyid", accountPayable.getCompanyMst());
//			if (serverorclient.equalsIgnoreCase("REST")) {
//				variables.put("REST", 1);
//			} else {
//				variables.put("REST", 0);
//			}
//
//			
//			variables.put("WF", "forwardAccountPayable");
//			
//			String workflow = (String) variables.get("WF");
//				String voucherNumber = (String) variables.get("voucherNumber");
//				String sourceID = (String) variables.get("id");
//
//
//				LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//				//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
//				
//				java.util.Date uDate = (Date) variables.get("voucherDate");
//				java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
//				lmsQueueMst.setVoucherDate(sqlVDate);
//				
//				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//				lmsQueueMst.setVoucherType(workflow);
//				lmsQueueMst.setPostedToServer("NO");
//				lmsQueueMst.setJobClass("forwardAccountPayable");
//				lmsQueueMst.setCronJob(true);
//				lmsQueueMst.setJobName(workflow + sourceID);
//				lmsQueueMst.setJobGroup(workflow);
//				lmsQueueMst.setRepeatTime(60000L);
//				lmsQueueMst.setSourceObjectId(sourceID);
//				
//				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//				variables.put("lmsqid", lmsQueueMst.getId());
//				eventBus.post(variables);
//				
				
			/*ProcessInstanceWithVariables pVariablesInReturn = runtimeService
					.createProcessInstanceByKey("damageentryprocess").setVariables(variables)

					.executeWithVariablesInReturn();
					*/
				return accountPayable;
		}
		
		

		@PutMapping("{companymstid}/updateaccountpayable/{accountpayableid}")
		public AccountPayable updateAccountPayable(
				@PathVariable(value="accountpayableid") String accountpayableid, 
				@PathVariable(value = "companymstid") String companymstid ,
				@Valid @RequestBody AccountPayable  accountpayablebleRequest)
		{
					
			return accountPayableRepository.findById(accountpayableid).map(accountpayable -> {
						Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
						CompanyMst companyMst = comapnyMstOpt.get();
						accountpayable.setCompanyMst(companyMst);
						accountpayable.setPaidAmount(accountpayablebleRequest.getPaidAmount());
						//accountpayable =accountPayableRepository.saveAndFlush(accountpayable);
						  saveAndPublishService.saveAccountPayable(accountpayable,mybranch);
						    logger.info("accountPayable send to KafkaEvent: {}", accountpayable);
						Map<String, Object> variables = new HashMap<String, Object>();

						variables.put("voucherNumber", accountpayable.getPurchaseHdr().getVoucherNumber());
						variables.put("voucherDate", accountpayable.getVoucherDate());
						variables.put("inet", 0);
						variables.put("id", accountpayable.getId());
						variables.put("branchcode", accountpayable.getPurchaseHdr().getBranchCode());

						variables.put("companyid", accountpayable.getCompanyMst());
						if (serverorclient.equalsIgnoreCase("REST")) {
							variables.put("REST", 1);
						} else {
							variables.put("REST", 0);
						}

						
						variables.put("WF", "forwardAccountPayable");
						
						String workflow = (String) variables.get("WF");
							String voucherNumber = (String) variables.get("voucherNumber");
							String sourceID = (String) variables.get("id");


						 
							eventBus.post(variables);
							
			            return accountpayable;
			        }).orElseThrow(() -> new ResourceNotFoundException("accountpayableid " + accountpayableid + " not found"));
					
		}
	 
	
		@GetMapping("{companymstid}/accountpayable/{acccountId}")
		public List<AccountPayable> retrieveAllAccountPayableByAccountId(
				@PathVariable(value = "companymstid") String
				  companymstid,@PathVariable(value = "acccountId") String
				  acccountId){
			
			Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
			return accountPayableRepository.findByAccountId(companyMst.get(),acccountId);
		}
		
		
		@GetMapping("{companymstid}/accountpayable/retriveaccountpayable/{acccountId}")
		public List<AccountPayable> retrievelAccountPayableByAccountId(
				@PathVariable(value = "companymstid") String
				  companymstid,@PathVariable(value = "acccountId") String
				  acccountId){
			return accountPayableRepository.findByCompanyMstAndAccountId(companymstid,acccountId);
		}

		@GetMapping("{companymstid}/retriveaccountpayablebypurchasehdrid/{purchaseHdrId}")
		public AccountPayable retrieveAllAccountPayableByPurchaseHdr(
				@PathVariable(value = "companymstid") String
				  companymstid,@PathVariable(value = "purchaseHdrId") String
				  purchaseHdrId){
			
			Optional<PurchaseHdr> purchaseHdr=purchaseHdrRepository.findById(purchaseHdrId);
			Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
			return accountPayableRepository.findByCompanyMstAndPurchaseHdr(companyMst.get(),purchaseHdr.get());

		}
		@GetMapping("{companymstid}/accountpayable/fetchaccountpayablebyvouchernumber/{vouchernumber}")
		public AccountPayable retrieveAccountPayableByVOucherNuber(
				@PathVariable(value = "companymstid") String  companymstid,
				@PathVariable(value = "vouchernumber") String  vouchernumber
				){

			Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			
			return accountPayableRepository.findByCompanyMstAndVoucherNumber(companyMst, vouchernumber);
			 

		}
		
		@GetMapping("{companymstid}/accountreceivable/fetchaccountpayableablebyid/{id}")
		public AccountPayable retrieveAccountPayableById(
				@PathVariable(value = "companymstid") String  companymstid,
				@PathVariable(value = "id") String  id
				){

			Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			
			
			return accountPayableRepository.findByCompanyMstAndId(companyMst,id) ;
			 
		}	
		
		@GetMapping("{companymstid}/accountpayable/fetchaccountpayableablebyid/{id}")
		public AccountPayable getSupplierDueReport(
				@PathVariable(value = "companymstid") String  companymstid,
				@PathVariable(value = "id") String  id
				){

			Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			
			
			return accountPayableRepository.findByCompanyMstAndId(companyMst,id) ;
			 
		}	
		@GetMapping("{companymstid}/accountpayable/automaticbilladjustment/{accountid}/"
				+ "{paymenthdr}/{paidamount}")
		public Double automaticBillAdjustment(
				@PathVariable(value = "companymstid") String  companymstid,
				@PathVariable(value = "accountid") String  accountid,
				@PathVariable(value="paidamount")Double paidamount,
				@PathVariable(value="paymenthdr")String paymenthdr,
				@RequestParam(value="rdate")String rdate)
		{

			Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			Date udate = SystemSetting.StringToUtilDate(rdate, "yyyy-MM-dd");
			return accountPayableService.supplierBillAdjustment(udate,accountid,paidamount,paymenthdr);
			
		}	
}
