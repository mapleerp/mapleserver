package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.InMessageXml;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.InMsgXmlRepository;

@RestController
public class InMsgXmlResource {

	
	@Autowired 
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	InMsgXmlRepository inMsgXmlRepository;
	
	@PostMapping("{companymstid}/inmsgxmlresource/addinmsgxml")
	public InMessageXml createInMessageXml(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody InMessageXml inMessageXml)
	{
		
		   Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
			inMessageXml.setCompanyMst(companyMstOpt.get());
			return 	inMsgXmlRepository.save(inMessageXml);
	       
	}
	
	
	@GetMapping("{companymstid}/inmsgxmlresource/getallinmsgxml")
	public List<InMessageXml> retrieveAllInMessageXml(@PathVariable(value = "companymstid") String
			  companymstid){
		
		return inMsgXmlRepository.findByCompanyMstId(companymstid);
		
		
	}
	
	
	
	
	@DeleteMapping("{companymstid}/inmsgxmlresource/deleteinmsgxml/{id}")
	public void InMessageXmlDelete(@PathVariable(value = "id") String Id) {
		inMsgXmlRepository.deleteById(Id);

	}
	
	}


