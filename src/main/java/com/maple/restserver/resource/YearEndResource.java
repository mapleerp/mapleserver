package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BankMst;
import com.maple.restserver.report.entity.ItemBatchDtlReport;
import com.maple.restserver.service.YearEndService;
@RestController
public class YearEndResource {

	@Autowired
	YearEndService yearEndService;
	
	@Transactional
	@GetMapping("{companymstid}/yearendresource/yearend")
	public  void retrieveAllcash(@PathVariable(value = "companymstid") String
			 companymstid)
	{
		
		
		 yearEndService.YearEnd(companymstid);
	}
	
}
