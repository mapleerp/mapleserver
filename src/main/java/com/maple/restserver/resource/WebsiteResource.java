package com.maple.restserver.resource;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.WebsiteClass;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.WebsiteModelService;
@CrossOrigin("http://localhost:4200")
@RestController
public class WebsiteResource {
	
	
	 @Autowired
	 WebsiteModelService websiteModelService;
		

	    @Autowired
	    CompanyMstRepository companyMstRepository;

	
	
	@GetMapping("{companymstid}/websiteresource/websitecontent")
	public  WebsiteClass WebsiteContent(
			@PathVariable(value = "companymstid") String companymstid 
			)
	{
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
		return websiteModelService.getWebsiteValues(companyMst);
		
	}

}
