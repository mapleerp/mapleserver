package com.maple.restserver.resource;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.TrialBalanceHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.TrialBalanceHdrRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class TrialBalanceHdrResource {

	@Autowired
	TrialBalanceHdrRepository trialBalanceHdrRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	//==========================get trial balance hdr by reportid===============
			@GetMapping("{companymstid}/trialbalancehdrresource/gettrialbalancehdrbyreportid/{reportId}")
			public TrialBalanceHdr getTrialBalanceHdrByReportId(
					@PathVariable(value="companymstid") String companymstid,
					@PathVariable(value="reportId") String reportId){

				return trialBalanceHdrRepository.getTrialBalanceHdrByReportId(reportId);
			}
			
			
	//==========================get reportid===============
			@GetMapping("{companymstid}/trialbalancehdrresource/getreportid/{userId}")
			public List<Object> getReportIdByUserIdAndDate(
					@PathVariable(value="companymstid") String companymstid,
					@RequestParam("sdate") String sdate,
					@PathVariable(value="userId") String userId){
				java.util.Date startdate = SystemSetting.StringToUtilDate(sdate,"yyyy-MM-dd");
				return trialBalanceHdrRepository.getReportIdByUserIdAndDate(startdate,userId);
			}
}
