package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.OpeningStockDtl;
import com.maple.restserver.report.entity.StockMigrationReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.OpeningStockDtlRepository;
import com.maple.restserver.service.ItemBatchDtlDailyService;
import com.maple.restserver.service.ItemBatchDtlService;

@RestController
public class OpeningStockDtlResource {

	
	@Autowired
	OpeningStockDtlRepository openingStockDtlRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	ItemBatchDtlService  itemBatchDtlService;
	
	
	
	@PutMapping("{companymstid}/openingstockdtlresource/finalsave")
	public OpeningStockDtl updateOpeningStock( @RequestBody 
			 OpeningStockDtl  openingStockDtl,@PathVariable(value = "companymstid") String
			 companymstid)
	{
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		
		CompanyMst companyMst = companyMstOpt.get();
		openingStockDtl.setCompanyMst(companyMst);
		openingStockDtlRepository.save(openingStockDtl);
		
		/*
		 * Saveto itemBatchDtl
		 */
		
		itemBatchDtlService.saveItemBatchDtlFromOpeningStock(openingStockDtl);
		return openingStockDtl;
	}
	
	
	@PostMapping("{companymstid}/openingstockdtlresource/saveopeningstockdtl")
	public OpeningStockDtl saveToOpeningStockDtl(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 OpeningStockDtl  openingStockDtl)
	{
		
		
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		
		CompanyMst companyMst = companyMstOpt.get();
		openingStockDtl.setCompanyMst(companyMst);
		openingStockDtl = openingStockDtlRepository.save(openingStockDtl);
//		   Map<String, Object> variables = new HashMap<String, Object>();
//
//					variables.put("voucherNumber", unitMst1.getId());
//
//					variables.put("voucherDate", SystemSetting.getSystemDate());
//					variables.put("inet", 0);
//					variables.put("id", unitMst1.getId());
//
//					variables.put("companyid", unitMst1.getCompanyMst());
//					variables.put("branchcode", unitMst1.getBranchCode());
//
//					variables.put("REST", 1);
//					
//					
//					variables.put("WF", "forwardUnitMst");
//					
//					
//					String workflow = (String) variables.get("WF");
//					String voucherNumber = (String) variables.get("voucherNumber");
//					String sourceID = (String) variables.get("id");
//
//
//					LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//					lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//					//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
//					
//					java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
//					java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//					lmsQueueMst.setVoucherDate(sqlVDate);
//					
//					lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//					lmsQueueMst.setVoucherType(workflow);
//					lmsQueueMst.setPostedToServer("NO");
//					lmsQueueMst.setJobClass("forwardUnitMst");
//					lmsQueueMst.setCronJob(true);
//					lmsQueueMst.setJobName(workflow + sourceID);
//					lmsQueueMst.setJobGroup(workflow);
//					lmsQueueMst.setRepeatTime(60000L);
//					lmsQueueMst.setSourceObjectId(sourceID);
//					
//					lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//					lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//					
//					variables.put("lmsqid", lmsQueueMst.getId());
//					
//					eventBus.post(variables);
//			
       return openingStockDtl;
       
		/*
		 * wf . forwardUnit
		 */
		} 
	
	
	@GetMapping("{companymstid}/openingstockdtlresource/getallopeningstockdtl")
	public List<OpeningStockDtl> showallOpeningStockDtl(
			
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		//return openingStockDtlRepository.findAllOpeningStockDtl(companyMstOpt.get());
		return openingStockDtlRepository.findAllByCompanyMst(companyMstOpt);
		
}
	
	@DeleteMapping("{companymstid}/openingstockdtlresource/deleteopeningstockdtl/{id}")
	public void deleteitembatchdtl(@PathVariable(value = "id") String Id) {
		openingStockDtlRepository.deleteById(Id);
	}
   
	
	@GetMapping("{companymstid}/openingstockdtlresource/getallopeningstockdtlnull")
	public List<OpeningStockDtl> showallOpeningStockDtlnull(
			
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		
		return openingStockDtlRepository.findOpeningStockVoucherNumberNull(companyMstOpt);
		
}
	
	
		
}
