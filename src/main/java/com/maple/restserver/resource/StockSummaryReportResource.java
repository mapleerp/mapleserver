package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.StockSummaryCategoryWiseReport;
import com.maple.restserver.report.entity.StockSummaryDtlReport;
import com.maple.restserver.report.entity.StockSummaryReport;
import com.maple.restserver.service.StockSummaryReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class StockSummaryReportResource {
	@Autowired
	StockSummaryReportService stockSummaryReportService ;

	// all category sales summary report
	@GetMapping("{companymstid}/allcategorystocksummaryreport/{branchCode}")
	public List<StockSummaryReport> getAllCategoryStockSummaryReport(
			@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate,@PathVariable("branchCode") String branchCode,
			@PathVariable(value = "companymstid") String  companymstid) {
		java.sql.Date fdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.sql.Date tdate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");



		return stockSummaryReportService.getAllCategoryStockSummaryReport(companymstid, fdate,tdate,branchCode);

	}
	@GetMapping("{companymstid}/categorywisestocksummaryreport/{categoryid}/{branchCode}")
	public List<StockSummaryCategoryWiseReport> getCategoryWiseStockSummaryReport(@PathVariable("categoryid") String categoryid,
			@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate,@PathVariable("branchCode") String branchCode,
			@PathVariable(value = "companymstid") String  companymstid) {
		java.sql.Date fdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.sql.Date tdate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");


		return stockSummaryReportService.getCategoryWiseStockSummaryReport(companymstid,categoryid, fdate,tdate,branchCode);

	}
	
	@GetMapping("{companymstid}/stocksummarydtls/{itemid}/{branchCode}")
	public List<StockSummaryDtlReport> getStockSummaryDtlsReport(@PathVariable("itemid") String itemid,
			@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate,@PathVariable("branchCode") String branchCode,
			@PathVariable(value = "companymstid") String  companymstid) {
		java.util.Date fDate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");

		java.util.Date tDate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");

		return stockSummaryReportService.getStockSummaryDtls(companymstid,itemid, fDate,tDate,branchCode);

	}
	
	@GetMapping("{companymstid}/stocksummaryallitem/{categoryid}/{branchCode}")
	public List<StockSummaryDtlReport> getStockSummaryAllItemReport( 
			@RequestParam("fdate") String todate,@PathVariable("branchCode") String branchCode,
			@PathVariable("categoryid") String categoryid,
			@PathVariable(value = "companymstid") String  companymstid) {
		 
		java.util.Date tDate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");

		return stockSummaryReportService.getAllStockSummaryValue(companymstid,tDate,branchCode,categoryid);

	}
	
	@GetMapping("{companymstid}/stocksummarybycostprice/{categoryid}/{branchCode}")
	public List<StockSummaryDtlReport> getStockSummaryByCostPrice( 
			@RequestParam("fdate") String todate,
			@PathVariable("categoryid") String categoryid,
			@PathVariable("branchCode") String branchCode,
			@PathVariable(value = "companymstid") String  companymstid) {
		 
		java.util.Date tDate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");

		return stockSummaryReportService.getAllStockSummaryByCostPrice(companymstid,tDate,branchCode,categoryid);

	}
	@GetMapping("{companymstid}/stocksummaryreport/stocksummarydtls/{branchCode}")
	public List<StockSummaryDtlReport> getStockSummaryDtlsReportWeb(
			@RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate,
			@PathVariable("branchCode") String branchCode,
			@PathVariable(value = "companymstid") String  companymstid) {
		java.util.Date fDate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");

		java.util.Date tDate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");

		return stockSummaryReportService.getStockSummaryDtls(companymstid, fDate,tDate,branchCode);

	}
	
	//
}
