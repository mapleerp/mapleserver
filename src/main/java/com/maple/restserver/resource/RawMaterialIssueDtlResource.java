package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.RawMaterialIssueDtl;
import com.maple.restserver.entity.RawMaterialIssueHdr;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.RawMaterialIssueReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.RawMaterialIssueDtlRepository;
import com.maple.restserver.repository.RawMaterialMstRepository;
import com.maple.restserver.service.RawMaterialIssueService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class RawMaterialIssueDtlResource {
private static final Logger logger = LoggerFactory.getLogger(RawMaterialIssueDtlResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	RawMaterialIssueDtlRepository rawMaterialIssueDtlRepo;
	
	@Autowired
	RawMaterialIssueService rawMaterialIssueService;
	@Autowired
	RawMaterialMstRepository RawMaterialMstRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	 @PostMapping("{companymstid}/rawmaterialissuemst/{rawmaterialhdrId}/rawmaterialissuedtl")
	    public RawMaterialIssueDtl createRawMaterialIssuDtl(@PathVariable (value = "rawmaterialhdrId") String rawmaterialhdrId,
	                                @Valid @RequestBody RawMaterialIssueDtl rawMaterialIssueDtl) {
	        return RawMaterialMstRepo.findById(rawmaterialhdrId).map(rawMaterialIssueMst -> {
	        	rawMaterialIssueDtl.setRawMaterialIssueHdr(rawMaterialIssueMst); 
	            //return rawMaterialIssueDtlRepo.saveAndFlush(rawMaterialIssueDtl);
	        	return saveAndPublishService.saveRawMaterialIssueDtl(rawMaterialIssueDtl,mybranch);
	        }).orElseThrow(() -> new ResourceNotFoundException("raw Material" + rawmaterialhdrId + " not found"));
	    }
		
		@DeleteMapping("{companymstid}/rawmaterialissuedtl/{rawmaterialhdrId}/deleterawmaterialissuedtl")
		public void DeletepurchaseDtl(@PathVariable (value="rawmaterialhdrId") String rawmaterialhdrId) {
			rawMaterialIssueDtlRepo.deleteById(rawmaterialhdrId);

		}
		
		   @GetMapping("{companymstid}/rawmaterialissuedtl/{issuemstid}/rawmaterialdtls")
			public List<RawMaterialIssueDtl> retriveAllRawMaterialByHdrId(
					@PathVariable(value = "companymstid") String companymstid,
					@PathVariable(value = "issuemstid") String issuemstid) {

				return rawMaterialIssueDtlRepo.findByRawMaterialIssueHdrId(issuemstid);


		}
		   @GetMapping("{companymstid}/rawmaterialissuedtl/{vocuhernumber}/rawmaterialissuereport")
			public List<RawMaterialIssueReport> retriveAllRawMaterialByVoucher(
					@PathVariable(value = "companymstid") String companymstid,
					@PathVariable(value = "vocuhernumber") String vocuhernumber,
					 @RequestParam("rdate") String reportdate){
				java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd"); {

				return rawMaterialIssueService.findByRawMaterialVoucher(vocuhernumber,date,companymstid);


				}
		   }
}
