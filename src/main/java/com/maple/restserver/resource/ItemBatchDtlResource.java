package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.kafka.common.protocol.types.Field.Str;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.report.entity.PharmacyBrachStockMarginReport;

import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;

import com.maple.restserver.service.PharmacyBranchStockMarginRptService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.ItemBatchDtlDailyService;
import com.maple.restserver.service.ItemBatchDtlService;

import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController


public class ItemBatchDtlResource {
	
private static final Logger logger = LoggerFactory.getLogger(ItemBatchDtlResource.class);
EventBus eventBus = EventBusFactory.getEventBus();

      @Autowired
      SaveAndPublishService saveAndPublishService;
	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;
	
	@Autowired
	PharmacyBranchStockMarginRptService pharmacyBranchStockMarginRptService;
	

	
	@Value("${ENABLENEGATIVEBILLING}")
	private String ENABLENEGATIVEBILLING;
	@Autowired 
	ItemBatchDtlService itemBatchDtlService;
	
	
	@PostMapping("{companymstid}/itembatchdtlresource/savetoitembatcgdtl")
	public ItemBatchDtl saveToItemBatchDtl(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 ItemBatchDtl  itemBatchDtl)
	{
		
		
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		
		CompanyMst companyMst = companyMstOpt.get();
		itemBatchDtl.setCompanyMst(companyMst);
//		itemBatchDtl = itemBatchDtlRepository.saveAndFlush(itemBatchDtl);
		itemBatchDtl=saveAndPublishService.saveItemBatchDtl(itemBatchDtl, itemBatchDtl.getBranchCode());
		
		if(null!=itemBatchDtl)
		{
			saveAndPublishService.publishObjectToKafkaEvent(itemBatchDtl, 
					itemBatchDtl.getBranchCode(), KafkaMapleEventType.ITEMBATCHDTL,
					KafkaMapleEventType.SERVER, itemBatchDtl.getId());
		}
		else {
			
			return null;
		}
		logger.info("itemBatchDtl send to KafkaEvent: {}", itemBatchDtl);
		
//		   Map<String, Object> variables = new HashMap<String, Object>();
//
//					variables.put("voucherNumber", unitMst1.getId());
//
//					variables.put("voucherDate", SystemSetting.getSystemDate());
//					variables.put("inet", 0);
//					variables.put("id", unitMst1.getId());
//
//					variables.put("companyid", unitMst1.getCompanyMst());
//					variables.put("branchcode", unitMst1.getBranchCode());
//
//					variables.put("REST", 1);
//					
//					
//					variables.put("WF", "forwardUnitMst");
//					
//					
//					String workflow = (String) variables.get("WF");
//					String voucherNumber = (String) variables.get("voucherNumber");
//					String sourceID = (String) variables.get("id");
//
//
//					LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//					lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//					//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
//					
//					java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
//					java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//					lmsQueueMst.setVoucherDate(sqlVDate);
//					
//					lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//					lmsQueueMst.setVoucherType(workflow);
//					lmsQueueMst.setPostedToServer("NO");
//					lmsQueueMst.setJobClass("forwardUnitMst");
//					lmsQueueMst.setCronJob(true);
//					lmsQueueMst.setJobName(workflow + sourceID);
//					lmsQueueMst.setJobGroup(workflow);
//					lmsQueueMst.setRepeatTime(60000L);
//					lmsQueueMst.setSourceObjectId(sourceID);
//					
//					lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//					lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//					
//					variables.put("lmsqid", lmsQueueMst.getId());
//					
//					eventBus.post(variables);
//			
       return itemBatchDtl;
       
		/*
		 * wf . forwardUnit
		 */
		} 
	

	@GetMapping("{companymstid}/itembatchdtlresource/getpharmacybranchstockmarginreport/{branchcode}")
	public List<PharmacyBrachStockMarginReport> getPharmacyLocalPurchaseDetailsReport(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fromdate") String fromdate,@PathVariable(value = "branchcode") String branchcode,
			@RequestParam("todate") String todate) {
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		return pharmacyBranchStockMarginRptService.getPharmacyBranchStockMarginReport (fromDate, toDate,branchcode);


}
	

	@GetMapping("{companymstid}/itembatchdtlresource/stockverificationforstocktransfer/{stkhdrid}")
	public String retrieveAllItemBatchDtlforStockTransfer(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "stkhdrid") String stkhdrid){
		if(ENABLENEGATIVEBILLING.equalsIgnoreCase("false"))
		{
		return itemBatchDtlService.stockVerificationForStkTransfer(companymstid,stkhdrid);
		}
		else
		{
			return null;
		}
	}
	
	@GetMapping("{companymstid}/itembatchdtlresource/getitemstockdetailsbystore/{itemId}/{batch}/{storeFrom}")
	public Double getItemStockDetailsByStore(
			@PathVariable(value = "itemId") String itemId,@PathVariable(value = "batch") String batch,
			@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "storeFrom") String storeFrom) {
		return itemBatchDtlService.findItemStockDetailsByStore(itemId,batch,companymstid,storeFrom);

	}
	
	//========================
	@GetMapping("{companymstid}/itembatchdtlresource/salestobatchdtl/{branchcode}")
	public String SalesToBatchDtl(
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("todate") String todate) {
		
		Date date = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		return itemBatchDtlService.SalesToBatchDtl(companymstid,branchcode,date);

	}

	
	
}
