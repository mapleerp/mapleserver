package com.maple.restserver.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.WebUser;
import com.maple.restserver.repository.WebUserRepository;

@RestController
@Transactional
public class WebUserResource {
	
	@Autowired
	WebUserRepository webUserRepo;
	
	@PostMapping("{companymstid}/webuserresource/createwebuser")
	public WebUser createWebUser(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			WebUser webUser)
	{
	
		return webUserRepo.save(webUser);
		 }

}
