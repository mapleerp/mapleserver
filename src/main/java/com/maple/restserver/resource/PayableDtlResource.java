
package com.maple.restserver.resource;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.PayableDtl;
import com.maple.restserver.repository.PayableDtlRepository;


@RestController
@Transactional
public class PayableDtlResource {
	@Autowired
	private PayableDtlRepository payabledtls;
	@GetMapping("{companymstid}/payabledtls")
	public List<PayableDtl> retrieveAllcash()
	{
		return payabledtls.findAll();
	}
	@PostMapping("{companymstid}/payabledtl")
	public ResponseEntity<Object> createUser(@Valid @RequestBody PayableDtl ledger)
	{
		PayableDtl saved=payabledtls.saveAndFlush(ledger);
	URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	}

}

