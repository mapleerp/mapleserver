package com.maple.restserver.resource;

import java.net.URI;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PhysicalStockDtl;
import com.maple.restserver.entity.PhysicalStockHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.entity.VisitDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PhysicalStockDtlRepository;
import com.maple.restserver.repository.PhysicalStockHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.repository.VisitDtlRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.PhysicalStockServiceImpl;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class PhysicalStockHdrResource {
	
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepo;
	@Autowired
	PhysicalStockServiceImpl physicalStockServiceImpl;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	PhysicalStockHdrRepository physicalStockHdrRepo;

	@Autowired
	ItemMstRepository itemMstRepo;

	@Autowired
	PhysicalStockDtlRepository physicalStockDtlRepo;

	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	UnitMstRepository unitMstRepo;
	
	@Autowired
	private VoucherNumberService voucherService;

	EventBus eventBus = EventBusFactory.getEventBus();
 
	
	
	@GetMapping("{companymstid}/physicalstockhdr")
	public List<PhysicalStockHdr> retrieveAllPhysicalStockHdr() {

		return physicalStockHdrRepo.findAll();

	}

	@GetMapping("{companymstid}/physicalstockhdrnovoucher")
	public List<PhysicalStockHdr> retrieveAllPhysicalStockHdrNoVoucher(@PathVariable(value = "companymstid") String companymstid) {

		return physicalStockHdrRepo.getPhysicalStockNoVoucher(companymstid);

	}


	
	@PostMapping("{companymstid}/physicalstockhdr")
	public PhysicalStockHdr createPhysicalStockHdr(@PathVariable(value = "companymstid") String companymstid,@Valid @RequestBody PhysicalStockHdr physicalStockHdr) {
		
		 return  companyMstRepo.findById(companymstid).map(companyMst-> {
			 physicalStockHdr.setCompanyMst(companyMst);
			 
	return physicalStockHdrRepo.save(physicalStockHdr);
		 }).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }

	

	@GetMapping("{companymstid}/physicalstockfrommobile")
	public ArrayList<ItemMst> createPhysicalStock(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("itemname") String itemname,
			@RequestParam("barcode") String barcode, 
			@RequestParam("batchcode") String batchcode,
			@RequestParam("qty") String qty, 
			@RequestParam("mrp") String mrp, 
			@RequestParam("expirydate") String expirydate,
			@RequestParam("fbKey") String fbKey) {

		System.out.println("itemname = " + itemname);
		System.out.println("barcode = " + barcode);
		System.out.println("batchcode = " + batchcode);
		System.out.println("qty = " + qty);
		System.out.println("mrp = " + mrp);
		System.out.println("expirydate = " + expirydate);
		double dQty = Double.parseDouble(qty);
		double dMrp = Double.parseDouble(mrp);
		
		
	//	Date expiryDate = SystemSetting.StringToUtilDate(expirydate, "yyyy-MM-dd");
		
		
		 List<PhysicalStockHdr>  phHdrList = physicalStockHdrRepo.getPhysicalStockNoVoucher(companymstid);
		 PhysicalStockHdr phHdr = null;
		if(phHdrList.isEmpty()) {
		

		  phHdr = new PhysicalStockHdr();
		
		}else {
			phHdr = phHdrList.get(0);
		}

	CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
	Calendar calendar = Calendar.getInstance();
	java.util.Date currentDate = calendar.getTime();
	java.sql.Date date = new java.sql.Date(currentDate.getTime());

	phHdr.setVoucherDate(date);
	
	
	
	
	phHdr.setCompanyMst(companyMst);
		PhysicalStockHdr phtsicalStockSaved = physicalStockHdrRepo.save(phHdr);

		PhysicalStockDtl phDtl = new PhysicalStockDtl();
		phDtl.setItemName(itemname);
		phDtl.setBarcode(barcode);
		phDtl.setBatch(batchcode);
		phDtl.setQtyIn(dQty);
		phDtl.setMrp(dMrp);
		phDtl.setExpiryDate(null);
		phDtl.setCompanyMst(companyMst);
		phDtl.setPhysicalStockHdrId(phHdr);
		
		
		physicalStockDtlRepo.saveAndFlush(phDtl);

		 List<ItemMst>  itemList = itemMstRepo.findByBarCode(barcode);
		 ItemMst item = itemList.get(0);
		if (null == item || null == item.getItemName()) {
			item = new ItemMst();
			item.setBarCode(barcode);
			item.setItemName(itemname);

			String unitName = SystemSetting.DEFAULT_UNIT;
			UnitMst unitMst = unitMstRepo.findByUnitNameAndCompanyMstId(unitName,companymstid);

			Double taxrate = SystemSetting.DEFAULT_TAXRATE;
			item.setTaxRate(taxrate);

			VoucherNumber itemCode = voucherService.generateInvoice(SystemSetting.ITEM_CODE_GENERATOR_ID,phtsicalStockSaved.getCompanyMst().getId());

			VoucherNumber itemId = voucherService.generateInvoice(SystemSetting.ITEM_CODE_GENERATOR_ID,phtsicalStockSaved.getCompanyMst().getId());

			item.setId("MOB"+itemId.getCode());
			
			
			item.setItemCode(itemCode.getCode());

			Double sellprice = dMrp;
			item.setStandardPrice(sellprice);

			Double statecess = SystemSetting.DEFAULT_STATECESS;
			item.setCess(statecess);

			String hsncode = "";
			item.setHsnCode(hsncode);

			item.setUnitId(unitMst.getId());
			
			item.setCompanyMst(companyMst);

			item.setRank(0);
			itemMstRepo.saveAndFlush(item);
		}
		ArrayList<ItemMst> resultArray = new ArrayList();
		
		item.setModel(fbKey);
		resultArray.add(item);
		return resultArray;

	}

	@PostMapping("{companymstid}/physicalstockfinalsave")
	public ArrayList<ItemMst> createPhysicalStockFinalSave(@PathVariable(value = "companymstid") String companymstid,@RequestParam("itemname") String itemname,
			@RequestParam("barcode") String barcode, @RequestParam("batchcode") String batchcode,
			@RequestParam("qty") String qty, @RequestParam("mrp") String mrp, 
			@RequestParam("expirydate") String expirydate 
			) {
		
		/*
		 * this functuon should not be used
		 */
		
		
		System.out.println("Physical Stock recieved");

		System.out.println("itemname = " + itemname);
		System.out.println("barcode = " + barcode);
		System.out.println("batchcode = " + batchcode);
		System.out.println("qty = " + qty);
		System.out.println("mrp = " + mrp);
		
		System.out.println("expirydate = " + expirydate);
		
		
		double dQty = Double.parseDouble(qty);
		double dMrp = Double.parseDouble(mrp);
		Date  expDate = null;
		if(expirydate.length()>1) {

			expDate = SystemSetting.StringToUtilDate(expirydate, "dd-MM-yyyy");
		}
		
		/*
		 * this functuon should not be used
		 */
		//ArrayList<ItemMst> itemArray = physicalStockServiceImpl.updatePhyisicalStock(barcode, itemname, dQty, dMrp,
		//		batchcode,expDate,companymstid);

		
		
	
			
		return null;

	}

	@PutMapping("{companymstid}/physicalstockhdr/{physicalstockhdrid}")
	public PhysicalStockHdr physicalStockHdrFinalSave(@PathVariable(value="physicalstockhdrid") String physicalstockhdrid,
			@PathVariable(value="companymstid") String companymstid,
			@Valid @RequestBody PhysicalStockHdr physicalStockHdrRequest) {

		return physicalStockHdrRepo.findById(physicalstockhdrid).map(physicalStockHdr -> {
			physicalStockHdr.setVoucherNumber(physicalStockHdrRequest.getVoucherNumber());
	
			
			physicalStockHdr.setVoucherDate(physicalStockHdrRequest.getVoucherDate());
			

			Optional<CompanyMst> CompanyMstopt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = CompanyMstopt.get();
			
			
			physicalStockHdr.setCompanyMst(companyMst);

			itemBatchDtlRepo.deleteExpDateItemBatchDtl();
			PhysicalStockHdr saved = physicalStockHdrRepo.saveAndFlush(physicalStockHdr);
			
			List<PhysicalStockDtl> physicalStockDtlList =  physicalStockDtlRepo.findByPhysicalStockHdrId(physicalStockHdr);
			
			
			for(PhysicalStockDtl ph : physicalStockDtlList)
			{
				
				 String store =null;
				 if(null != ph.getStore()) {
					 store = ph.getStore();
				 }else {
					 store = "MAIN";
				 }
				 
				ArrayList<ItemMst> itemArray = physicalStockServiceImpl.updatePhyisicalStock(ph.getBarcode(), ph.getItemName(), ph.getQtyIn(), 
						ph.getMrp(),
						ph.getBatch(),ph.getExpiryDate(),companymstid,saved.getBranchCode(), saved.getVoucherNumber(), saved.getVoucherDate(),store );
				
			//	ResponseEntity<ArrayList> respentity = RestCaller.saveStockFromPhysicalStock(ph.getItemName(),ph.getBarcode(),
			//			ph.getBatch(),Double.toString(ph.getQtyIn()),Double.toString(ph.getMrp()), expDate);
				
			}
			

			
	 
			
			Map<String, Object> variables = new HashMap<String, Object>();
				
				variables.put("voucherNumber", saved.getId());
				variables.put("id", saved.getId());
				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("inet", 0);
				variables.put("REST", 1);
				variables.put("companyid",companyMst);
				
				variables.put("branchcode", saved.getBranchCode());

				variables.put("WF", "forwardPhysicalStock");
				
				
				String workflow = (String) variables.get("WF");
	    		String voucherNumber = (String) variables.get("voucherNumber");
	    		String sourceID = (String) variables.get("id");


	    		LmsQueueMst lmsQueueMst = new LmsQueueMst();

	    		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
	    		//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
	    		
	    		java.util.Date uDate = (Date) variables.get("voucherDate");
	    		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
	    		lmsQueueMst.setVoucherDate(sqlVDate);
	    		
	    		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
	    		lmsQueueMst.setVoucherType(workflow);
	    		lmsQueueMst.setPostedToServer("NO");
	    		lmsQueueMst.setJobClass("forwardPhysicalStock");
	    		lmsQueueMst.setCronJob(true);
	    		lmsQueueMst.setJobName(workflow + sourceID);
	    		lmsQueueMst.setJobGroup(workflow);
	    		lmsQueueMst.setRepeatTime(60000L);
	    		lmsQueueMst.setSourceObjectId(sourceID);
	    		
	    		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

	    		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
	    		variables.put("lmsqid", lmsQueueMst.getId());
				eventBus.post(variables);
				
				
			return saved;
		}).orElseThrow(() -> new ResourceNotFoundException("ForwardPhysicalStock " + physicalstockhdrid + " not found"));

	}

	
	@GetMapping("{companymstid}/physicalstockhdrs/{hdrid}")
	public Optional<PhysicalStockHdr> retrievPhysicalStockHdrById(@PathVariable(value = "hdrid") String hdrid,@PathVariable (value = "companymstid")String companymstid){
		return physicalStockHdrRepo.findByIdAndCompanyMstId(hdrid,companymstid);
	}
}
