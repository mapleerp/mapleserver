package com.maple.restserver.resource;

import java.net.URI;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.AccountPayable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.GoodReceiveNoteDtl;
import com.maple.restserver.entity.GoodReceiveNoteHdr;
import com.maple.restserver.entity.GstMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.PurchaseHdrMessageEntity;
import com.maple.restserver.entity.PurchaseOrderHdr;
import com.maple.restserver.entity.SupplierPriceMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AccountPayableRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.GoodReceiveNoteDtlRepository;
import com.maple.restserver.repository.GoodReceiveNoteHdrRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.LmsQueueTallyMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ItemBatchDtlService;

import com.maple.restserver.service.MultiUnitConversionServiceImpl;
import com.maple.restserver.service.PurchaseService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.service.accounting.task.PurchaseAccounting;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class GoodReceiveNoteHdrResource {
	
private static final Logger logger = LoggerFactory.getLogger(GoodReceiveNoteHdrResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	// ----------------version 5.0 surya end
	@Autowired
	MultiUnitMstRepository multiUnitMstRepository;

	@Autowired
	private GoodReceiveNoteHdrRepository goodReceiveNoteHdrRepository;

	@Autowired
	GoodReceiveNoteDtlRepository goodReceiveNoteDtlRepository;

	@Autowired
	private CompanyMstRepository companyMstRepository;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	private ItemMstRepository itemMstRepository;

	@Autowired
	private ItemBatchMstRepository itemBatchMstRepository;

	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	LmsQueueTallyMstRepository lmsQueueTallyMstRepository;

	@Autowired
	private AccountPayableRepository accountPayableRepository;


	@Autowired
	PurchaseAccounting purchaseAccounting;

	@Autowired
	MultiUnitConversionServiceImpl multiUnitConversionServiceImpl;

	@Autowired
	PurchaseService purchaseService;
	
	 @Autowired
	 SaveAndPublishService saveAndPublishService;

 

	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	private VoucherNumberService voucherService;

	
	@PostMapping("{companymstid}/goodreceivenotehdrresource/savesgoodreceivenotehdr")
	public GoodReceiveNoteHdr createGoodReceiveNoteHdr(@Valid @RequestBody GoodReceiveNoteHdr goodReceiveNoteHdr,
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();

		goodReceiveNoteHdr.setCompanyMst(companyMst);

//		GoodReceiveNoteHdr saved = goodReceiveNoteHdrRepository.save(goodReceiveNoteHdr);
		GoodReceiveNoteHdr saved = saveAndPublishService.saveGoodReceiveNoteHr(goodReceiveNoteHdr, goodReceiveNoteHdr.getBranchCode());
		 logger.info("GoodReceiveNoteHdr send to KafkaEvent: {}", saved);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/(id)").buildAndExpand(saved.getId())
				.toUri();

		return saved;
	}

	@GetMapping("{companymstid}/goodreceivenotehdrresource/getgoodreceivenotehdr")
	public List<GoodReceiveNoteHdr> retrieveAllGoodReceiveNoteHdr(
			@PathVariable(value = "companymstid") String companymstid) {
		return goodReceiveNoteHdrRepository.findByCompanyMstId(companymstid);
	}

	

	/*
	 * @PutMapping public GoodReceiveNoteHdr updateGoodReceiveNoteHdr(
	 * 
	 * @PathVariable(value="companymstid") String commpanymstid,
	 * 
	 * @Valid @RequestBody GoodReceiveNoteHdr goodReceiveNoteHdr) { CompanyMst
	 * companymst=companyMstRepository.findById(commpanymstid).get();
	 * goodReceiveNoteHdr.setCompanyMst(companymst);
	 * goodReceiveNoteHdr=goodReceiveNoteHdrRepository.save(goodReceiveNoteHdr);
	 * 
	 * messageObjectService.CreateObjectMsgMstForGoodReceiveNote(goodReceiveNoteHdr)
	 * ; return goodReceiveNoteHdr; }
	 */

	@PutMapping("{companymstid}/goodreceivenotehdrresource/finalsavegoodreceivenotehdr/{goodreceivenotehdrid}")
	public GoodReceiveNoteHdr GoodReceiveNoteHdrFinalSave(@PathVariable String goodreceivenotehdrid,
			@Valid @RequestBody GoodReceiveNoteHdr goodReceiveNoteHdrRequest) {

		return goodReceiveNoteHdrRepository.findById(goodreceivenotehdrid).map(goodReceiveNoteHdr -> {

			goodReceiveNoteHdr.setFinalSavedStatus(goodReceiveNoteHdrRequest.getFinalSavedStatus());
			goodReceiveNoteHdr.setVoucherNumber(goodReceiveNoteHdrRequest.getVoucherNumber());

			//GoodReceiveNoteHdr saved = goodReceiveNoteHdrRepository.saveAndFlush(goodReceiveNoteHdr);
			
			GoodReceiveNoteHdr saved = saveAndPublishService.saveGoodReceiveNoteHr(goodReceiveNoteHdr, goodReceiveNoteHdr.getBranchCode());
			 logger.info("GoodReceiveNoteHdr send to KafkaEvent: {}", saved);
			
			
			// messageObjectService.CreateObjectMsgMstForGoodReceiveNote(goodReceiveNoteHdr);

			/*
			 * //-----------------------version 1.9
			 * 
			 * Map<String, Object> variables = new HashMap<String, Object>();
			 * variables.put("voucherNumber", saved.getId());
			 * variables.put("voucherDate",ClientSystemSetting.getSystemDate());
			 * variables.put("inet", 0); variables.put("id", saved.getId());
			 * variables.put("branchcode", saved.getBranchCode());
			 * variables.put("companyid", saved.getCompanyMst()); variables.put("REST",1);
			 * 
			 * 
			 * variables.put("WF", "forwardPurchaseOrder");
			 * 
			 * 
			 * String workflow = (String) variables.get("WF"); String voucherNumber =
			 * (String) variables.get("voucherNumber"); String sourceID = (String)
			 * variables.get("id");
			 * 
			 * 
			 * LmsQueueMst lmsQueueMst = new LmsQueueMst();
			 * 
			 * lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			 * 
			 * //lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			 * 
			 * java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			 * java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
			 * lmsQueueMst.setVoucherDate(sqlVDate);
			 * 
			 * lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			 * lmsQueueMst.setVoucherType(workflow); lmsQueueMst.setPostedToServer("NO");
			 * lmsQueueMst.setJobClass("forwardPurchaseOrder");
			 * lmsQueueMst.setCronJob(true); lmsQueueMst.setJobName(workflow + sourceID);
			 * lmsQueueMst.setJobGroup(workflow); lmsQueueMst.setRepeatTime(60000L);
			 * lmsQueueMst.setSourceObjectId(sourceID);
			 * 
			 * lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
			 * 
			 * lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			 * variables.put("lmsqid", lmsQueueMst.getId()); eventBus.post(variables);
			 */

			// -----------------------version end 1.9
			return saved;

		}).orElseThrow(() -> new ResourceNotFoundException("goodreceivenote " + goodreceivenotehdrid + " not found"));

	}

}
