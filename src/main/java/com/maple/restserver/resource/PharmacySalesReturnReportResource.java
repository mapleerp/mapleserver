package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.PharmacySalesReturnReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.PharmacySalesReturnService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class PharmacySalesReturnReportResource {

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	PharmacySalesReturnService pharmacySalesReturnService;

	@GetMapping("/{companymstid}/pharmacysalesreturnreportresource/{category}/getpharmacysalesreturn/{branchCode}")
	public List<PharmacySalesReturnReport> getPharmacySalesReturn(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branchCode") String branchCode,
			@PathVariable("category") String category, @RequestParam("fdate") String fromdate,
			@RequestParam("tdate") String todate) {
		java.sql.Date fdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.sql.Date tdate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");

		String categoryNames = category;

		String[] array = categoryNames.split(";");
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return pharmacySalesReturnService.getPharmacySalesReturn(companyMst, fdate, tdate, branchCode, array);

	}
	
	@GetMapping("/{companymstid}/pharmacysalesreturnreportresource/getpharmacysalesreturn/{branchCode}")
	public List<PharmacySalesReturnReport> getPharmacySalesReturnWithoutCategory(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branchCode") String branchCode,
			 @RequestParam("fdate") String fromdate,
			@RequestParam("tdate") String todate) {
		java.sql.Date fdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.sql.Date tdate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return pharmacySalesReturnService.getPharmacySalesReturnWithoutCategory(companyMst, fdate, tdate, branchCode);

	}

}
