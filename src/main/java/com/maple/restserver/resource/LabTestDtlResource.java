package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.his.entity.LabTestDtl;
import com.maple.restserver.repository.LabTestDtlRepository;




@RestController
@Transactional
public class LabTestDtlResource {
	@Autowired
	private LabTestDtlRepository labtestdtl;
	@GetMapping("{companymstid}/labtest")
	public List<LabTestDtl> retrieveAlldepartments()
	{
		return labtestdtl.findAll();
		
	}
	@PostMapping("{companymstid}/labtest")
	public ResponseEntity<Object>createUser(@Valid @RequestBody LabTestDtl labtest1)
	{
		LabTestDtl saved=labtestdtl.saveAndFlush(labtest1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}

}
