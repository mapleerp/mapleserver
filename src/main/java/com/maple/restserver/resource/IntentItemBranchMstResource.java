package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.IntentItemBranchMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.IntentItemBranchRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class IntentItemBranchMstResource {
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	
	
	
	@Autowired
	IntentItemBranchRepository intentItemBranchRepo;
	
	@PostMapping("{companymstid}/intentitembranch")
	public IntentItemBranchMst createIntentItemBranch(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			IntentItemBranchMst intentItemBranchMst)
	{
		
		return companyMstRepo.findById(companymstid).map(companyMst-> {
	intentItemBranchMst.setCompanyMst(companyMst);
	return intentItemBranchRepo.save(intentItemBranchMst);
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
		  companymstid + " not found"));
	}
	
	
	@GetMapping("/{companymstid}/intentitembranch/getintentitembranchbyitemid/{itemid}")
	public IntentItemBranchMst getIntentItemBranchByItemId(@PathVariable (value="itemid")String itemId,
			@PathVariable (value="companymstid")String companymstid
			) {
		
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

		return intentItemBranchRepo.findByCompanyMstAndItemId(companyMst.get(), itemId);

	}
	@GetMapping("/{companymstid}/intentitembranch/getintentitembranchbyitemid/{itemid}/{branchcode}")
	public IntentItemBranchMst getIntentItemBranchByItemIdandtoBranch(
			@PathVariable (value="itemid")String itemId,
			@PathVariable (value="branchcode")String branchcode,
			@PathVariable (value="companymstid")String companymstid
			) {
		
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

		return intentItemBranchRepo.findByCompanyMstAndItemIdAndBranchCode(companyMst.get(), itemId,branchcode);

	}

	@PutMapping("/{companymstid}/intentitembranch/updateitembranch/{itemid}")
	public IntentItemBranchMst updateItemBranch(
			@PathVariable (value="itemid")String itemId,
			@PathVariable (value="companymstid")String companymstid,
			@Valid @RequestBody IntentItemBranchMst intentItemBranchMstReq)
	{
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

		IntentItemBranchMst intentItemBranchMst = intentItemBranchRepo.findByCompanyMstAndItemId(companyMst.get(), itemId);
		intentItemBranchMst.setBranchCode(intentItemBranchMstReq.getBranchCode());
		return intentItemBranchRepo.saveAndFlush(intentItemBranchMst);
	
	
	}
}
