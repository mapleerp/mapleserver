package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BalanceSheetConfigAsset;
 
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.repository.BalanceSheetConfigAssetRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
@Transactional
public class BalanceSheetConfigAssetResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	BalanceSheetConfigAssetRepository balanceSheetConfigAssetRepository;
	
	@PostMapping("{companymstid}/balancesheetconfigassetresource/savebalancesheetconfigasset")
	public BalanceSheetConfigAsset createBalanceSheetConfigAsset(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 BalanceSheetConfigAsset balanceSheetConfigAssetDtl)
    {
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		balanceSheetConfigAssetDtl.setCompanyMst(companyMst.get());
       return balanceSheetConfigAssetRepository.saveAndFlush(balanceSheetConfigAssetDtl);
		 
	}
	
	
	@DeleteMapping("{companymstid}/balancesheetconfigassetresource/deletebalancesheetconfigasset/{id}")
	public void DeleteBalanceSheetConfigAsset(@PathVariable (value="id") String id) {
		balanceSheetConfigAssetRepository.deleteById(id);

	}

	@GetMapping("{companymstid}/balancesheetconfigassetresource/getallbalancesheetconfigasset")
	public List<BalanceSheetConfigAsset> getAllBalanceSheetConfigAsset(
			@PathVariable(value = "companymstid") String companymstid) {
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		
		return balanceSheetConfigAssetRepository.findByCompanyMst(companyMst.get());
	}
	
}
