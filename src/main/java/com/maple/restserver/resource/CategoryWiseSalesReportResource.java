package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.report.entity.CategoryWiseSalesReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.CategoryWiseSalesReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class CategoryWiseSalesReportResource {

	@Autowired
	CompanyMstRepository  companyMstRepository;
	
	@Autowired
    CategoryWiseSalesReportService categoryWiseSalesReportService;
	
	
	
	
	@GetMapping("{companymstid}/categorywisesalesreportresource/{branchCode}/getcategorywisesalesreport")
	public List<CategoryWiseSalesReport> getCategoryWiseSalesReport(
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "branchCode") String branchCode  , 
				@RequestParam("fromdate") String fromdate,
				@RequestParam("todate") String todate) {
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
		
		return categoryWiseSalesReportService.getCategoryWiseSalesReport(fromDate,toDate,branchCode,companyMst);
		

	}
	
	
	
	
	
}
