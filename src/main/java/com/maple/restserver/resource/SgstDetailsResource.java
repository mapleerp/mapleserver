package com.maple.restserver.resource;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.SgstDtl;
import com.maple.restserver.repository.SgstDetailsRepository;



@RestController
@Transactional
public class SgstDetailsResource {
	@Autowired
	private SgstDetailsRepository sgstDetails;
	@GetMapping("{companymstid}/sgstDetails")
	public List<SgstDtl> retrieveAll()
	{
		return sgstDetails.findAll();
	}
	@PostMapping("{companymstid}/sgstDetails")
	public ResponseEntity<Object> createUser(@Valid @RequestBody SgstDtl sgstDetails1)
	{
		SgstDtl saved=sgstDetails.saveAndFlush(sgstDetails1);
	URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	}

//	String sqlstr1 = "select v.voucher_number, v.invoice_time , t.invoice_date  from sales_voucher_hdr v, sales_trans_hdr t"
//			+ " where t.record_id = v.parent_id and    t.record_id   =  '" + parentID + "'";

	
	
	
//	"select  branch_name,branch_address1,branch_address2,branch_address3,branch_pin,branch_email  "
//	+ " from branch_mst   where id = ? " + " ");

	
}

