package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ConsumptionDtl;
import com.maple.restserver.entity.ConsumptionHdr;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ConsumptionDtlRepository;
import com.maple.restserver.repository.ConsumptionHdrRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ConsumptionHdrResource {
	
	EventBus eventBus = EventBusFactory.getEventBus();

	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
@Autowired
CompanyMstRepository companyMstRepo;

@Autowired
ItemBatchDtlRepository itemBatchDtlRepository;
@Autowired
ConsumptionHdrRepository consumptionHdrRepo;
	
@Autowired
ConsumptionDtlRepository consumptionDtlRepo;
	
@Autowired
ItemMstRepository itemMstRepository;
@Autowired
private VoucherNumberService voucherService;
@Autowired
MultiUnitMstRepository multiUnitMstRepository;
@Autowired
ItemBatchMstRepository itemBatchMstRepository;

//----------------version 6.7 surya 
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	//----------------version 6.7 surya end
boolean recurssionOff = false;
	
	@PostMapping("{companymstid}/consumptionhdr/saveconsumptionhdr")
	public ConsumptionHdr createConsumptionHdr(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody ConsumptionHdr consumptionHdr) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			consumptionHdr.setCompanyMst(companyMst);
			return consumptionHdrRepo.saveAndFlush(consumptionHdr);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}
	
	@PutMapping("{companymstid}/consumptionhdr/updateconsumptionhdr")
	public ConsumptionHdr updateConsumptionHdr(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody ConsumptionHdr consumptionHdrReq)
	{
		ConsumptionHdr consumptionHdr = consumptionHdrRepo.findById(consumptionHdrReq.getId()).get();
		consumptionHdr.setVoucherNumber(consumptionHdrReq.getVoucherNumber());
		consumptionHdr.setVoucherDate(consumptionHdrReq.getVoucherDate());
		ConsumptionHdr saved = consumptionHdrRepo.saveAndFlush(consumptionHdr);
		updateConsumptionStock(consumptionHdr);

		Map<String, Object> variables = new HashMap<String, Object>();
		
		variables.put("voucherNumber", saved.getId());
		variables.put("voucherDate", ClientSystemSetting.getSystemDate());
		
		variables.put("id", saved.getId());
		variables.put("inet", 0);
		variables.put("REST", 1);
		variables.put("companyid", saved.getCompanyMst());
		variables.put("branchcode", saved.getBranchCode());
		
		variables.put("WF", "forwardConsumption");
		
		
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		
		//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (java.util.Date)variables.get("voucherDate");
		java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		
//		String jobClass = getJobClass(workflow, lmsQueueMst);
//		if (null == jobClass) {
//			logger.info("jobClass not found for " + workflow);
//			return;
//		}
		lmsQueueMst.setJobClass("forwardConsumption");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		
		eventBus.post(variables);
		return saved;
	}
	private void updateConsumptionStock(ConsumptionHdr consumptionHdr)
	{
		List<ConsumptionDtl> consumptionDtlList = consumptionDtlRepo.findByConsumptionHdrId(consumptionHdr.getId());
		Iterator iter = consumptionDtlList.iterator();
		while (iter.hasNext()) {
			ConsumptionDtl consumptionDtl = (ConsumptionDtl) iter.next();
			ItemMst item = itemMstRepository.findById(consumptionDtl.getItemId()).get();
			double conversionQty = 0.0;
			
			//=======apply a common function for item_batch_mst updation and 
			// ======== item_batch_dtl insertion ====== by anandu === 25-06-2021 
			Double qty = null;
			if (!item.getUnitId().equalsIgnoreCase(consumptionDtl.getUnitId())) {
				conversionQty = getConvertionQty(consumptionHdr.getCompanyMst().getId(), item.getId(), consumptionDtl.getUnitId(),
						item.getUnitId(), consumptionDtl.getQty());
				qty = -1 * conversionQty;
			}else {
				qty = -1 * consumptionDtl.getQty() ;
			}
			
			String processInstanceId = null;
			String taskId = null;
			Date ExpiryDate = null;
			
			itemBatchDtlService.itemBatchMstUpdation(item.getBarCode(),consumptionDtl.getBatch(),consumptionHdr.getBranchCode(),
					ExpiryDate,consumptionDtl.getItemId(),consumptionDtl.getMrp(),processInstanceId,qty,taskId,consumptionHdr.getCompanyMst(),consumptionDtl.getStore());
			

			final VoucherNumber voucherNo = voucherService.generateInvoice("CONSUMPTION",
					consumptionHdr.getCompanyMst().getId());
			Double qtyOut = null;
			Double qtyIn = 0.0;
			if (!item.getUnitId().equalsIgnoreCase(consumptionDtl.getUnitId())) {
				conversionQty = getConvertionQty(consumptionHdr.getCompanyMst().getId(), item.getId(), consumptionDtl.getUnitId(),
						item.getUnitId(), consumptionDtl.getQty());
				qtyOut = conversionQty;
			}else {
				qtyOut = consumptionDtl.getQty() ;
			}
			
			itemBatchDtlService.itemBatchDtlInsertionNew(item.getBarCode(),consumptionDtl.getBatch(),consumptionHdr.getBranchCode(),
					ExpiryDate,consumptionDtl.getItemId(),consumptionDtl.getMrp(),"CONSUMPTION",processInstanceId,
					qtyIn,qtyOut,consumptionDtl.getId(),consumptionHdr.getId(),consumptionHdr.getVoucherDate(),consumptionHdr.getVoucherNumber(),consumptionDtl.getStore(),taskId,
					consumptionHdr.getVoucherDate(),voucherNo.getCode(),consumptionHdr.getCompanyMst());
			
			//===============end=============25-06-2021===========
			
		}
	}
	
	@GetMapping("{companymstid}/consumptionhdr/getsumofamount/{hdrid}")
	public Double getConsumptionDtlSum(@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "hdrid")String hdrid	)
	{
		return consumptionHdrRepo.getTotalAmount(hdrid, companymstid);
	}
	
	@GetMapping("{companymstid}/consumptionhdr/getsumofqty/{hdrid}/{itemid}/{batch}")
	public Double getConsumptionDtlQtySum(@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "hdrid")String hdrid	,
			@PathVariable(value = "itemid")String itemid	,
			@PathVariable(value = "batch")String batch	)
	{
		return consumptionHdrRepo.getQty(hdrid, companymstid,itemid,batch);
	}
	
	
	private double getConvertionQty(String companyMstId, String itemId, String sourceUnit, String targetUnit,
			double sourceQty) {
		if (recurssionOff) {
			return sourceQty;
		}
		MultiUnitMst multiUnitMstList = multiUnitMstRepository.findByCompanyMstIdAndItemIdAndUnit1(companyMstId, itemId,
				sourceUnit);

		while (!multiUnitMstList.getUnit2().equalsIgnoreCase(targetUnit)) {

			if (recurssionOff) {
				break;
			}
			sourceUnit = multiUnitMstList.getUnit2();

			sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();

			getConvertionQty(companyMstId, itemId, sourceUnit, targetUnit, sourceQty);

		}
		sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();
		recurssionOff = true;
		// sourceQty = sourceQty *
		// multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
		return sourceQty;
	}
}
