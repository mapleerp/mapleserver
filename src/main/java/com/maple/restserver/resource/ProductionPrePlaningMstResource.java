package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.entity.ProductionPreplanningMst;
import com.maple.restserver.report.entity.ProductionPreplaninngDtlReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ProductionDetailRepository;
import com.maple.restserver.repository.ProductionDtlDtlRepository;
import com.maple.restserver.repository.ProductionMstRepository;
import com.maple.restserver.repository.ProductionPreplanningDtlRepository;
import com.maple.restserver.service.MachineResourceService;
import com.maple.restserver.service.ProductionPrePlaningService;
import com.maple.restserver.utils.SystemSetting;
@RestController
public class ProductionPrePlaningMstResource {

	
	@Autowired
	ProductionPrePlaningService productionPrePlaningService;
	
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	
	@Autowired
	ProductionDetailRepository poroductionDetailRepository;
	
	@Autowired
	ProductionPreplanningDtlRepository productionPreplanningDtlRepository;
	@Autowired
	ProductionDtlDtlRepository productionDtlDtlRepository;
	@Autowired
	ProductionMstRepository productionMstRepository;
	@Autowired
	MachineResourceService  machineResourceService;
	
	@GetMapping("{companymstid}/producctionpreplanCompanyMstRepositoryingmstresource/{branchcode}/getproductionpreplaningmst")
	public ProductionPreplanningMst  getProductionPreplanningMst(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchCode, @RequestParam("fdate") String fromdate
			) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");

		Optional<CompanyMst> companyMst = CompanyMstRepository.findById(companymstid);

		return productionPrePlaningService.getProductionPreplanningMst(sdate, companyMst.get(), branchCode);

	}
	
	
	

	@GetMapping("{companymstid}/producctionpreplaningmstresource/{branchcode}/getproductionpreplaningdtl")
	public List<ProductionPreplaninngDtlReport>  getProductionPreplanningDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,@RequestParam("fdate") String fromdate,@RequestParam("tdate")
			String toDate) {
	

		Optional<CompanyMst> companyMst = CompanyMstRepository.findById(companymstid);
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");

		
		return productionPrePlaningService.getProductionPreplanningDtl( sdate,  companyMst.get() ,branchcode);

	}
	

	@Transactional
	@PostMapping("{companymstid}/producctionpreplaningmstresource/{productionmstid}/salevproductiondtl")
	public String saveProductionDtl(
			@PathVariable(value = "companymstid") String companymstid,	@PathVariable(value = "productionmstid") String productionmstid,
			@Valid @RequestBody List<ProductionPreplaninngDtlReport> productionDtlList )
	{
		Optional<CompanyMst> companyOpt = CompanyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		Optional <ProductionMst> productionMstOpt= 	productionMstRepository.findById(productionmstid);
		System.out.print(productionDtlList.size()+"productionDtlList issssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		for(int i=0;i<productionDtlList.size();i++) {
			ProductionDtl productionDtl =new ProductionDtl();
			productionDtl.setBatch(productionDtlList.get(i).getBatch());
			productionDtl.setItemId(productionDtlList.get(i).getItemId());
			productionDtl.setQty(productionDtlList.get(i).getQty());
			productionDtl.setProductionMst(productionMstOpt.get());
			productionDtl.setStatus(productionDtlList.get(i).getStatus());
			poroductionDetailRepository.save(productionDtl);
			productionPrePlaningService.saveProductionPlaningDtlDtl(productionDtl);
			productionPreplanningDtlRepository.updateProductionPlaningStatus(productionDtlList.get(i).getId());
		}
		return "Suscess";
	}
	
	
}
