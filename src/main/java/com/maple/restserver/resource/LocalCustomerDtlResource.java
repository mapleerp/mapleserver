package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DeliveryBoyMst;
import com.maple.restserver.entity.LocalCustomerDtl;
import com.maple.restserver.entity.LocalCustomerMst;
import com.maple.restserver.entity.OrderTakerMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LocalCustomerDtlRepository;
import com.maple.restserver.repository.LocalCustomerRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Transactional
public class LocalCustomerDtlResource {
private static final Logger logger = LoggerFactory.getLogger(LocalCustomerDtlResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	LocalCustomerDtlRepository localCustomerDtlRepository;

	@Autowired
	LocalCustomerRepository localCustomerRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	

	@PostMapping("{companymstid}/localcustomerdtl/savelocalcustomerdtl/{localcustomerid}")
	public LocalCustomerDtl creatLocalCustomerDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "localcustomerid") String localcustomerid,
			@Valid @RequestBody LocalCustomerDtl localCustomerDtl) {

		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		localCustomerDtl.setCompanyMst(companyMst.get());

		Optional<LocalCustomerMst> localCustomer = localCustomerRepository.findById(localcustomerid);
		localCustomerDtl.setLocalCustomerMst(localCustomer.get());

//		return localCustomerDtlRepository.save(localCustomerDtl);
		return saveAndPublishService.saveLocalCustomerDtl(localCustomerDtl, mybranch);
		
	}

	@GetMapping("{companymstid}/localcustomerdtl/getlocalcustomerdtl/{localcustomerid}")
	public List<LocalCustomerDtl> getLocalCustomerDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "localcustomerid") String localcustomerid) {

		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);

		Optional<LocalCustomerMst> localCustomer = localCustomerRepository.findById(localcustomerid);
		
		return localCustomerDtlRepository.findByCompanyMstAndLocalCustomerMst(companyMst.get(),localCustomer.get());
	}
	
	
	@DeleteMapping("{companymstid}/localcustomerdtl/deletelocalcustomerdtl/{id}")
	public void DeleteLocalCustomerDtl(
			@PathVariable (value="id") String id) {
		localCustomerDtlRepository.deleteById(id);

	}
	
	
}
