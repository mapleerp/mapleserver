package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.DailySalesReportDtl;
import com.maple.restserver.report.entity.ProfitAndLossReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.ProfitAndLossService;
import com.maple.restserver.service.ProfitAndLossServiceImpl;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ProfitAndLossReportResource {

	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	ProfitAndLossService profitAndLossService;
	
	
	
	@GetMapping("{companymstid}/profitreportresource/branchwisetotalprofit")
	public List<ProfitAndLossReport> getBranchwiseTotalProfit(
			@PathVariable(value = "companymstid") String companymstid,
		 
			@RequestParam("date") String date) {
		
		Date profitDate = SystemSetting.StringToSqlDate(date, "yyyy-MM-dd");
	

		return profitAndLossService.getBranchwiseTotalProfit(companymstid, profitDate);

	
	
}
}
