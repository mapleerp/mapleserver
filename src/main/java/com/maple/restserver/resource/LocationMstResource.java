package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.LocationMst;
import com.maple.restserver.entity.ProductMst;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.LocationMstRepository;
import com.maple.restserver.repository.ProductMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class LocationMstResource {
private static final Logger logger = LoggerFactory.getLogger(LocationMstResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	
	Pageable topFifty =   PageRequest.of(0, 50);

	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	 
	@Autowired
	private LocationMstRepository locationMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;


	@PostMapping("{companymstid}/locationmstresource/savelocationmst")
	public LocationMst createLocationMst(@Valid @RequestBody LocationMst locationMst,
			@PathVariable(value = "companymstid") String companymstid) {
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}
		
		
		locationMst.setCompanyMst(companyOpt.get());

//		locationMst =  locationMstRepository.saveAndFlush(locationMst);
		locationMst =  saveAndPublishService.saveLocationMst(locationMst, locationMst.getBranchCode());
		logger.info("locationMst send to KafkaEvent: {}", locationMst);
		
		   Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", locationMst.getId());

			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", locationMst.getId());

			variables.put("companyid", locationMst.getCompanyMst());
			variables.put("branchcode", locationMst.getBranchCode());

			variables.put("REST", 1);
			
			
			variables.put("WF", "forwardLocationMst");
			
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardLocationMst");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);

			return locationMst;
	}
	


	@GetMapping("{companymstid}/locationmstresource/productmsts")
	public List<LocationMst> retrieveAllLocationMst(
			@PathVariable("companymstid") String companymstid)
	{
		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
		
		if(!companymst.isPresent())
		{
			return null;
		}
		CompanyMst companyMst = companymst.get();
		return  locationMstRepository.findByCompanyMst(companyMst);
	}
	
	@GetMapping("{companymstid}/locationmstbyid/{id}")
	public LocationMst retrieveLocationMstById(
			@PathVariable("companymstid") String companymstid,
			@PathVariable("id") String id)
	{
		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
		
		if(!companymst.isPresent())
		{
			return null;
		}
		CompanyMst companyMst = companymst.get();
		return  locationMstRepository.findByCompanyMstAndId(companyMst,id);
	}
	
	@GetMapping("{companymstid}/serchforlocation")
	public  @ResponseBody List<Object> searchLocationWithCom(
			@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid){
		
		 
		Optional<CompanyMst> companyMst1 = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMst1.get();
		searchstring = searchstring.replaceAll("20%", " ");
		
		return locationMstRepository.searchByLocationWithCompany("%"+searchstring.toLowerCase()+"%", topFifty,companyMst) ; 
		
		
	}
	
}


