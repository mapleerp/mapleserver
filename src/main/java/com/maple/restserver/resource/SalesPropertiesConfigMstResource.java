package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AddKotTable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.SalesPropertiesConfigMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesPropertiesConfigMstRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class SalesPropertiesConfigMstResource {

	@Autowired
	SalesPropertiesConfigMstRepository salesPropertiesConfigMstRepo;
	
	@Autowired
	CompanyMstRepository  companyMstRepo;
	
	
	@PostMapping("/{companymstid}/salespropertiesconfigmst/savesalesproperties")
	public SalesPropertiesConfigMst createSalesPropertiesConfigMst(@PathVariable(value = "companymstid") String companymstid,@Valid @RequestBody 
			SalesPropertiesConfigMst salesPropertiesConfigMst)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			salesPropertiesConfigMst.setCompanyMst(companyMst);
			SalesPropertiesConfigMst saved = salesPropertiesConfigMstRepo.save(salesPropertiesConfigMst);
			return saved;
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found"));


	}
	@GetMapping("/{companymstid}/salespropertiesconfigmst/getallsalesproperties/{branchcode}")	
	public List<SalesPropertiesConfigMst> getAllSalesConfig(@PathVariable(value ="branchcode")String branchcode)
	{
		return salesPropertiesConfigMstRepo.findAll();
	}
	
	@DeleteMapping("/{companymstid}/salespropertiesconfigmst/deletebyid/{id}")
	public void deleteSalesProperties(@PathVariable (value = "id")String id)
	{
		salesPropertiesConfigMstRepo.deleteById(id);
	}
	
	@GetMapping("/{companymstid}/salespropertiesconfigmst/findsalespropertiesbyid/{id}")	
	public Optional<SalesPropertiesConfigMst> findsalesPropertyByid(@PathVariable (value = "id")String id)
	{
		return salesPropertiesConfigMstRepo.findById(id);
	}
	
	@GetMapping("/{companymstid}/salespropertiesconfigmst/findsalespropertiesbyname/{propertyName}")	
	public List<SalesPropertiesConfigMst> findsalesPropertyByPropertyName(@PathVariable (value = "propertyName")String propertyName)
	{
		return salesPropertiesConfigMstRepo.findByPropertyName(propertyName);
	}
}
