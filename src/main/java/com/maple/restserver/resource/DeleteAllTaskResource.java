package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;

@RestController
@Transactional
public class DeleteAllTaskResource {
	@Autowired
	private AcceptStockRepository acceptStockRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	ExternalApi externalApi;
	


	@GetMapping("{companymstid}/deletealltask")
	public String   deleteAllTask(@PathVariable(value = "companymstid") String companymstid) {
		externalApi.deleteAllTask();
		
		
		
		
		return "OK";
	}

 

}
