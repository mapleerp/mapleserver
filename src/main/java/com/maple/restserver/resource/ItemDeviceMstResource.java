package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemDeviceMst;
import com.maple.restserver.entity.ItemLocationMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemDeviceMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class ItemDeviceMstResource {

	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	ItemDeviceMstRepository itemDeviceMstRepo;

	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@PostMapping("{companymstid}/itemdevicemst/saveitemdevicemst")
	public ItemDeviceMst createItemDeviceMst(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestBody ItemDeviceMst itemDevicemstReq)
	{
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		itemDevicemstReq.setCompanyMst(companyMst);
		ItemDeviceMst saved =  itemDeviceMstRepo.save(itemDevicemstReq);
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", saved.getId());

		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", saved.getId());

		variables.put("companyid", saved.getCompanyMst());
		variables.put("branchcode", saved.getBranchCode());

		variables.put("REST", 1);
		
		
		variables.put("WF", "forwardItemDeviceMst");
		
		
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardItemDeviceMst");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		
		variables.put("lmsqid", lmsQueueMst.getId());
		
		eventBus.post(variables);
		return saved;
	}
	
	@DeleteMapping("{companymstid}/itemdevicemst/deleteitemdevicemst/{id}")
	public void deleteItemDeviceMst(@PathVariable(value = "id")String id,
			@PathVariable(value = "companymstid")String companymstid)
	{
		itemDeviceMstRepo.deleteById(id);
		try
		{
		//jmsTemplate.convertAndSend("server.deleteitemdevicemst", id);
		}
		catch (Exception e) {
			// TODO: handle exception
		}

	}
	
	@GetMapping("{companymstid}/itemdevicemst/getallitemdevicemst/{branchcode}")
	public List<ItemDeviceMst> getallItemDeviceMst(
			@PathVariable(value = "branchcode")String branchcode)
	{
		return itemDeviceMstRepo.findByBranchCode(branchcode);
	}
	@GetMapping("{companymstid}/itemdevicemst/getallitemdevicemstbyitemname/{itemname}")
	public List<ItemDeviceMst> getItemDeviceMStByItemName(@PathVariable(value ="itemname")String itemname,
			@PathVariable(value="companymstid")String companymstid)
	{
		return itemDeviceMstRepo.findItemDeviceMstByItemName(itemname);
	}
	
	@GetMapping("{companymstid}/itemdevicemst/getallitemdevicemstbyid/{id}")
	public Optional<ItemDeviceMst> getItemDeviceMStById(@PathVariable(value ="id")String id,
			@PathVariable(value="companymstid")String companymstid)
	{
		return itemDeviceMstRepo.findById(id);
	}
	
}
