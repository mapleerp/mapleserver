package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PDCPayment;
import com.maple.restserver.entity.PDCReceipts;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PDCPaymentRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class PDCPaymentResource {
	
private static final Logger logger = LoggerFactory.getLogger(PDCPaymentResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	PDCPaymentRepository pDCPaymentRepository;
	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	EventBus eventBus = EventBusFactory.getEventBus();


	@PostMapping("{companymstid}/pdcpaymentresource/pdcpayment")
	public PDCPayment createPDCReceipts(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody PDCPayment pDCPayment) {
		return companyMstRepository.findById(companymstid).map(companyMst -> {
			pDCPayment.setCompanyMst(companyMst);

//			PDCPayment pdcPaymentSaved =  pDCPaymentRepository.saveAndFlush(pDCPayment);
			PDCPayment pdcPaymentSaved =saveAndPublishService.savePDCPayment(pDCPayment, pDCPayment.getBranchCode());
			logger.info("pdcPaymentSaved send to KafkaEvent: {}", pdcPaymentSaved);
			

			Map<String, Object> variables = new HashMap<String, Object>();
			variables.put("voucherNumber", pdcPaymentSaved.getId());
			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", pdcPaymentSaved.getId());
			variables.put("companyid", pdcPaymentSaved.getCompanyMst());
			variables.put("branchcode", pdcPaymentSaved.getBranchCode());
			variables.put("REST", 1);
			variables.put("WF", "forwardPdcPayment");
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");
			LmsQueueMst lmsQueueMst = new LmsQueueMst();
			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardPdcPayment");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());

			eventBus.post(variables);
			
			return pdcPaymentSaved;

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	@DeleteMapping("{companymstid}/pdcpayment/{pdcpaymentid}")
	public ResponseEntity<?> deletePost(@PathVariable(value = "pdcpaymentid") String pdcpaymentid) {
		return pDCPaymentRepository.findById(pdcpaymentid).map(pdcpayments -> {
			pDCPaymentRepository.delete(pdcpayments);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("pcdreceiptsid " + pdcpaymentid + " not found"));
	}

	@GetMapping("{companymstid}/pdcpaymentbydate/{branchcode}")
	public List<PDCPayment> retrievePDCPayment(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam("rdate") String reportdate) {

		java.sql.Date date = SystemSetting.StringToSqlDate(reportdate, "dd-MM-yyyy");

		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		return pDCPaymentRepository.findByTransDateAndCompanyMstAndBranchCode(date, companyMst, branchcode);

	}

	@GetMapping("{companymstid}/pdcpaymentbystatus/{bankname}")
	public List<PDCPayment> retrievePDCPaymentByBankNameAndStatus(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "bankname") String bankname) {

		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		return pDCPaymentRepository.findByStatusAndBankAndCompanyMst("PENDING", bankname, companyMst);

	}

	@GetMapping("{companymstid}/pdcpaymentbyid/{id}")
	public Optional<PDCPayment> retrievePDCPaymentById(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {

		return pDCPaymentRepository.findById(id);

	}

	@PutMapping("{companymstid}/pdcpaymentresource/updatepdcpayment/{pdcpaymentid}")
	public PDCPayment updatePDCPayment(@PathVariable(value = "pdcpaymentid") String pdcpaymentid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody PDCPayment pdcpaymentRequest) {
		
		
		PDCPayment pdcpayment = pDCPaymentRepository.findById(pdcpaymentid).get();
		pdcpayment.setStatus(pdcpaymentRequest.getStatus());
	//	pdcpayment = pDCPaymentRepository.saveAndFlush(pdcpayment);
		
		pdcpayment = saveAndPublishService.savePDCPayment(pdcpayment, pdcpayment.getBranchCode());
		logger.info("pdcpayment send to KafkaEvent: {}", pdcpayment);
		

		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("voucherNumber", pdcpayment.getId());
		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", pdcpayment.getId());
		variables.put("companyid", pdcpayment.getCompanyMst());
		variables.put("branchcode", pdcpayment.getBranchCode());
		variables.put("REST", 1);
		variables.put("WF", "forwardPdcPayment");
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");
		LmsQueueMst lmsQueueMst = new LmsQueueMst();
		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardPdcPayment");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());

		eventBus.post(variables);

		return pdcpayment;

	}
	//...Sibi....... 12.03.2021
	



	
	@GetMapping("{companymstid}/pdcpaymentbyaccountid/{accountid}/{status}")
	public List<PDCPayment> retrievePDCPaymentByAccountid(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "accountid") String accountid,@PathVariable(value = "status") String status) {

		return pDCPaymentRepository.findByAccountAndStatus(accountid,status);

	}
}
