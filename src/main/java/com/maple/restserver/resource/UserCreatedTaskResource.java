package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.maple.restserver.entity.UserCreatedTask;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.UserCreatedTaskRepository;

public class UserCreatedTaskResource {

	@Autowired
	private UserCreatedTaskRepository userCreatedTaskRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;

	@GetMapping("{companymstid}/usercreatetaskresource/getusertaskbyusername/{userid}")
	public List<UserCreatedTask> getusertaskByUserId(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "userid") String userid) {
		return userCreatedTaskRepository.findByUserIdAndCompanyMst(companymstid, userid);
	}

	@PostMapping("{companymstid}/usercreatetaskresource/saveusertaskbyusername")
	public UserCreatedTask createUserCreatedTask(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody UserCreatedTask userCreatedTask) {

		return companyMstRepo.findById(companymstid).map(companyMst -> {
			userCreatedTask.setCompanyMst(companyMst);
			return userCreatedTaskRepository.saveAndFlush(userCreatedTask);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	@DeleteMapping("{companymstid}/usercreatetaskresource/deleteusertaskbyid/{id}")
	public void UserCreatedTaskDeleteById(@PathVariable(value = "id") String Id) {
		userCreatedTaskRepository.deleteById(Id);

	}

}
