package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.ItemBatchDtlDaily;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlDailyRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.service.ItemBatchDtlDailyService;

@RestController
@Transactional
public class ItemBatchDtlDailyResource {
	
	private static final Logger logger = LoggerFactory.getLogger(ItemBatchDtlDailyResource.class);
	
	@Value("${ENABLENEGATIVEBILLING}")
	private String ENABLENEGATIVEBILLING;
	@Autowired
	ItemBatchDtlDailyService itemBatchDtlDailyService;

	@Autowired
	ItemBatchDtlDailyRepository itemBatchDtlDailyRepo;

	

	@GetMapping("{companymstid}/itembatchdtldaily/itembatchdtlqty/{batch}/{itemid}")
	public Double itemBatchDtlDailyStock(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid, @PathVariable(value = "batch") String batch) {

		{
			return itemBatchDtlDailyRepo.findQtyByItemAndBatch(itemid, batch);
		}
	}

	@GetMapping("{companymstid}/itembatchdtldaily/stockverificationforstocktransfer/{stkhdrid}")
	public String retrieveAllItemBatchDtlDailyforStockTransfer(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "stkhdrid") String stkhdrid) {
		if (ENABLENEGATIVEBILLING.equalsIgnoreCase("false")) {
			return itemBatchDtlDailyService.stockVerificationForStkTransfer(companymstid, stkhdrid);
		} else {
			return null;
		}
	}

	@GetMapping("{companymstid}/itembatchdtldaily/stockverificationfordamagentry/{damagehdrid}")
	public String retrieveAllItemBatchDtlDailyforDamageEntry(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "damagehdrid") String damagehdrid) {
		if (ENABLENEGATIVEBILLING.equalsIgnoreCase("false")) {
			return itemBatchDtlDailyService.stockVerificationForDamageEntry(companymstid, damagehdrid);
		} else {
			return null;
		}
	}

	@GetMapping("{companymstid}/itembatchdtldaily/stockverificationforproductconversion/{prdcthdrid}/{branchcode}")
	public String retrieveAllItemBatchDtlDailyforProdctConversion(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "prdcthdrid") String prdcthdrid) {
		if (ENABLENEGATIVEBILLING.equalsIgnoreCase("false")) {
			return itemBatchDtlDailyService.stockVerificationForProdcutConversion(companymstid, prdcthdrid);
		} else {
			return null;
		}
	}
}
