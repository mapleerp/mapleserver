package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.SalesOrderDtl;
import com.maple.restserver.report.entity.SaleOrderDetailsReport;
import com.maple.restserver.report.entity.SaleOrderDueReport;
import com.maple.restserver.report.entity.SaleOrderReport;
import com.maple.restserver.report.entity.SaleOrderTax;
import com.maple.restserver.report.entity.SaleOrderinoiceReport;
import com.maple.restserver.report.entity.SalesOrderAdvanceBalanceReport;
import com.maple.restserver.repository.SalesOrderDtlRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.service.SaleOrderReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class SaleOrderReportResource {

	@Autowired
	SaleOrderReportService saleOrderReportService;
	
	@Autowired
	SalesOrderTransHdrRepository saleOrderTranHdrRepo;
	@Autowired
	SalesOrderDtlRepository  salesOrderDtlRepo;
	// Not Using
//	@GetMapping("{companymstid}/saleorderpendingreport/{branchcode}")
//	public List<SaleOrderDetailsReport>  getSaleOrderPendingReport(@PathVariable(value = "companymstid") String
//			  companymstid,@PathVariable(value = "companymstid") String
//			  branchcode,
//               @RequestParam("rdate") String reportdate){
//		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
//	
//
//		String cashaccountid="";
//		return saleOrderReportService.getSaleOrderPendingReport(date,branchcode);
//
//	
//	
//	}	
	
	@GetMapping("{companymstid}/saleorderreport/saleorderpendingreport/{branchcode}")
	public List<SaleOrderDetailsReport>  getPendinSaleOrderReport(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "companymstid") String
			  branchcode,
               @RequestParam("fdate") String fdate,
               @RequestParam("tdate") String tdate){
		java.util.Date date = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	
		java.util.Date tudate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		String cashaccountid="";
		return saleOrderReportService.getPendinSaleOrderReport(date,tudate,branchcode);

	
	
	}	
	
	@GetMapping("{companymstid}/saleorderrealizedrepor/{branchcode}t")
	public List<SaleOrderDetailsReport>  getSaleOrderRealizedReport(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "branchcode") String
			  branchcode,
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		
		String cashaccountid="";
		return saleOrderReportService.getSaleOrderRealizedrReport(date,branchcode);
	
	}	

	@GetMapping("{companymstid}/saleorderreport/{vouchernumber}/{branchcode}")
	public List<SaleOrderinoiceReport>  getSaleOrderinvoice(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "vouchernumber") String
			  vouchernumber, @PathVariable(value = "branchcode") String
			  branchcode,
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return saleOrderReportService.getSaleOrderInvoiceReport(date,vouchernumber,branchcode);
	
	}	
	
	@GetMapping("{companymstid}/saleorderduereport/{vouchernumber}/{branchcode}")
	public List<SaleOrderDueReport>  getSaleOrderdueReport(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "vouchernumber") String vouchernumber,  
			  @PathVariable(value = "branchcode") String branchcode,
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return saleOrderReportService.getSaleOrderDueReport(date,vouchernumber,branchcode);
	
	}	
	
	
	@GetMapping("{companymstid}/saleordertaxreport/{vouchernumber}/{branchcode}")
	public List<SaleOrderTax>  getSaleOrderTax(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "vouchernumber") String
			  vouchernumber,
			  @PathVariable(value = "branchcode") String
			  branchCode,
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		
		String cashaccountid="";
		return saleOrderReportService.getSaleOrderTax(vouchernumber,  date,branchCode);
	
	}	
	
	
	
	@GetMapping("{companymstid}/saleorderstatusreport")
	public List<SalesOrderDtl>  getSaleOrderDtlByDate(@PathVariable(value = "companymstid") String
			  companymstid,
			
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return salesOrderDtlRepo.findBySalesOrderTransHdrVoucherDate(date);
			
	
	}	
	
	@GetMapping("{companymstid}/saleorderbalanceadvancereport/{branchCode}")
	public List<SalesOrderAdvanceBalanceReport>  getSaleOrderBalanceAdvanceReportByDate(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "branchCode") String
			  branchCode,
				
			
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return saleOrderReportService.getSaleOrderBalanceAdvanceReportByDate(date, branchCode);		

		
	}	
	
	@GetMapping("{companymstid}/homedelivaryreport/{branchCode}")
	public List<SaleOrderReport>  getHomeDelivaryReportByDate(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "branchCode") String
	  branchCode,
		
			
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return saleOrderReportService.getHomeDelivaryReportByDate(date,branchCode);
			
	
	}	
	
	
	@GetMapping("{companymstid}/delivaryreportbydelivaryboyid/{deliveryboyid}/{branchCode}")
	public List<SaleOrderReport>  getSaleOrderReportByDeliverBoy(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "deliveryboyid") String
			  deliveryboyid,@PathVariable(value = "branchCode") String
			  branchCode,
				
			
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return saleOrderReportService. getSaleOrderReportByDeliverBoy(date,deliveryboyid,branchCode);
			
	
	}	
	
	
	@GetMapping("{companymstid}/saleorderreportbybranchcodeanddate/{branchCode}")
	public List<SaleOrderReport>  getSaleOrderReportByBranchCodeAndDate(@PathVariable(value = "companymstid") String
			  companymstid
			  ,@PathVariable(value = "branchCode") String
			  branchCode,
				
			
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return saleOrderReportService. getSaleOrderReportByBranchCodeAndDate(date,branchCode);
			
	
	}	
	
	
	
	@GetMapping("{companymstid}/saleorderreportresource/saleordertakeawayreport")
	public List<SaleOrderReport>  getSaleOrderDtlTakeAwayReporty(
			@PathVariable(value = "companymstid") String companymstid,
			  @RequestParam("date") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return saleOrderReportService.findBySalesOrderTransHdrVoucherDate(date,companymstid);
			
	
	}
	@GetMapping("{companymstid}/saleorderreportresource/saleordertakeawayreport/{deliveryboyid}/{branchcode}")
	public List<SaleOrderReport>  getDeliveryBoyPendingReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "deliveryboyid") String deliveryboyid,
			@PathVariable(value = "branchcode") String branchcode,

			  @RequestParam("date") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return saleOrderReportService.getDeliveryBoyPendingReport(date,companymstid,deliveryboyid,branchcode);
			
	
	}
	
	//----------------version 4.8
	
	@GetMapping("{companymstid}/saleorderreportresource/dailycakesetting/{categoryid}/{branchCode}")
	public List<SaleOrderReport>  getDailyCakeSettingReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "categoryid") String categoryid,
			@PathVariable(value = "branchCode") String branchCode,
			@RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return saleOrderReportService. getDailyCakeSettingReport(date,categoryid,branchCode,companymstid);
			
	
	}	
	
	//-----------------version 4.8 end
	//----------------version 4.9
	
			@GetMapping("{companymstid}/saleorderreportresource/homedeliveryreport/{branchCode}")
			public List<SaleOrderReport>  getSaleOrderHomeDeliveryReport(
					@PathVariable(value = "companymstid") String companymstid,
					@PathVariable(value = "branchCode") String branchCode,
					@RequestParam("rdate") String reportdate){
				java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

				return saleOrderReportService. getSaleOrderHomeDeliveryReport(date,branchCode,companymstid);
					
			
			}	
			
			//-----------------version 4.9 end
			//----------------version 4.10
			
			@GetMapping("{companymstid}/saleorderreportresource/dailyordersummury/{branchCode}")
			public List<SaleOrderReport>  getDailyOrderSummuryReport(
					@PathVariable(value = "companymstid") String companymstid,
					@PathVariable(value = "branchCode") String branchCode,
					@RequestParam("rdate") String reportdate){
				java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

				return saleOrderReportService. getDailyOrderSummuryReport(date,branchCode,companymstid);
					
			
			}	
			
			//-----------------version 4.10 end
			@GetMapping("{companymstid}/saleorderreportresource/soconvertedreport/{branchCode}")
			public List<SaleOrderReport>  getDailyOrderConvertedReport(
					@PathVariable(value = "companymstid") String companymstid,
					@PathVariable(value = "branchCode") String branchCode,
					@RequestParam("rdate") String reportdate){
				java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

				return saleOrderReportService.getSaleOrderConvertedReport(date,branchCode,companymstid);
					
			
			}	
}
