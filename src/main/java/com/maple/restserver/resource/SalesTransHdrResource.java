
package com.maple.restserver.resource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.maple.util.SocketClient;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.AddKotTable;
import com.maple.restserver.entity.AddKotWaiter;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.entity.DayBook;
import com.maple.restserver.entity.SalesBillModelClass;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.SalesDltdDtl;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesFinalSave;
import com.maple.restserver.entity.SalesMessageEntity;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SalesTypeMst;
import com.maple.restserver.entity.SchEligibilityAttribInst;
import com.maple.restserver.entity.SchOfferAttrInst;
import com.maple.restserver.entity.SchSelectionAttribInst;
import com.maple.restserver.entity.SchemeInstance;
import com.maple.restserver.entity.SummaryOnlineSales;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.entity.TaxMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.report.entity.InsuranceCompanySalesSummaryReport;
import com.maple.restserver.report.entity.KOTItems;
import com.maple.restserver.report.entity.PharmacyDayEndReport;
import com.maple.restserver.report.entity.SalesInWeb;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.SalesModeWiseBillReport;
import com.maple.restserver.report.entity.TableWaiterReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.AccountReceivableRepository;
import com.maple.restserver.repository.AddKotTableRepository;
import com.maple.restserver.repository.AddKotWaiterRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;
import com.maple.restserver.repository.DayBookRepository;
import com.maple.restserver.repository.ItemBatchDtlDailyRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.ItemStockPopUpRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.LmsQueueTallyMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.SalesDeletedDtlRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.SalesTypeMstRepository;
import com.maple.restserver.repository.SchEligibilityAttribInstRepository;
import com.maple.restserver.repository.SchOfferAttrInstRepository;
import com.maple.restserver.repository.SchSelectionAttribInstRepository;
import com.maple.restserver.repository.SchemeInstanceRepository;
import com.maple.restserver.repository.TaxMstRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ItemBatchDtlService;


import com.maple.restserver.service.SalesDetailsService;
import com.maple.restserver.service.SalesFinalSaveService;
import com.maple.restserver.service.SalesTransHdrService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.SchemeService;
import com.maple.restserver.service.TaxMstService;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.service.accounting.task.SalesAccounting;
import com.maple.restserver.service.accounting.task.SalesAccountingFC;
import com.maple.restserver.service.task.ProductionFinishedGoodsStock;
import com.maple.restserver.service.task.SalesDayEndReporting;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@CrossOrigin("http://localhost:4200")

public class SalesTransHdrResource {
	private static final Logger logger = LoggerFactory.getLogger(SalesTransHdrResource.class);

	@Autowired
	SalesTypeMstRepository salesTypeMstRepository;

	@Autowired
	CurrencyMstRepository currencyMstRepo;

	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepo;

	@Autowired
	SalesDeletedDtlRepository salesDeletedDtlRepository;

	@Autowired
	KitDefenitionDtlRepository kitDefenitionDtlRepo;

	@Autowired
	private AccountHeadsRepository accountHeadsRepository;
	
	@Autowired
	TaxMstRepository taxMstRepository;
	
	@Autowired
	TaxMstService taxMstService;

	@Autowired
	private SalesTransHdrRepository salesTransHdrRepo;
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	SchemeService schemeService;

	boolean recurssionOff = false;
	@Value("${serverorclient}")
	private String serverorclient;

	@Value("${POS_SALES_PREFIX}")
	private String POS_SALES_PREFIX;

	@Value("${voucherNoYearPrefix}")
	private String voucherNoYearPrefix;

	@Autowired
	SalesDetailsRepository salesDetailsRepository;

	@Autowired
	private UnitMstRepository unitMstRepository;

	@Autowired
	private SchOfferAttrInstRepository schOfferAttrInstRepository;

	@Autowired
	private SalesDetailsService salesDetailsService;

	@Autowired
	private CategoryMstRepository categoryMstRepository;

	@Autowired
	private SchEligibilityAttribInstRepository schEligibilityAttribInstRepository;

	@Autowired
	private SchSelectionAttribInstRepository schSelectionAttribInstRepository;

	@Autowired
	private SchemeInstanceRepository schemeInstanceRepository;

	@Autowired
	private AddKotWaiterRepository addKotWaiterRepository;

	@Autowired
	private AddKotTableRepository addKotTableRepository;
	// @Autowired
	// private RuntimeService runtimeService;
	@Autowired
	MultiUnitMstRepository multiUnitMstRepository;

	@Autowired
	private ItemMstRepository itemMstRepository;


	@Autowired
	private CompanyMstRepository companyMstRepository;

	@Autowired
	private SalesReceiptsRepository salesReceiptsRepo;

	@Autowired
	private ItemBatchMstRepository itemBatchMstRepo;

	@Autowired
	private SalesDetailsRepository salesDetailsRepo;

	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	private SalesTransHdrService salesTransHdrService;

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	private ItemStockPopUpRepository itemStockPopUpRepository;
	@Autowired
	AccountReceivableRepository accountReceivableRepository;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	@Autowired
	AccountReceivable accountReceivable;

	@Autowired
	SalesAccounting salesAccounting;

	@Autowired
	SalesAccountingFC salesAccountingfc;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	LmsQueueTallyMstRepository lmsQueueTallyMstRepository;

	@Autowired
	ProductionFinishedGoodsStock productionFinishedGoodStock;

	Pageable topFifty = PageRequest.of(0, 50);
	// ----------------version 5.0 surya
	@Autowired
	ItemBatchDtlService itemBatchDtlService;

	@Autowired
	ItemBatchDtlDailyRepository itemBatchDtlDailyRepository;
	// ----------------version 5.0 surya end
	@Autowired
	SalesDayEndReporting salesDayEndReporting;
	
	@Autowired
	DayBookRepository dayBookRepository;

	@Autowired
	private ExternalApi externalApi;

 @Autowired
 SalesFinalSaveService salesFinalSaveService;
	
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	


	// @Autowired
	// RestserverApplication restserverApplication;

	Map<String, Object> variablesAR = new HashMap<String, Object>();

	Map<String, Object> variablesFS = new HashMap<String, Object>();

	Map<String, Object> variablesTS = new HashMap<String, Object>();

	@DeleteMapping("{companymstid}/deletesalestranshdr/{Id}")
	public void SalesTypeMstDelete(@PathVariable String Id) {
		salesTransHdrRepo.deleteById(Id);
		salesTransHdrRepo.flush();

	}

	@GetMapping("{companymstid}/salestranshdr")
	public List<SalesTransHdr> retrieveAlldepartments() {
		return salesTransHdrRepo.findAll();

	}

	@GetMapping("{companymstid}/salestranshdr/sales_receipts/{salestranshdrid}")
	public List<SalesReceipts> getSalesReceiptsByTransHdr(
			@PathVariable(value = "salestranshdrid") String salestranshdrid) {

		SalesTransHdr salesTransHdr = salesTransHdrRepo.findById(salestranshdrid).get();

		return salesReceiptsRepo.findBySalesTransHdr(salesTransHdr);

	}

	@PostMapping("{companymstid}/salestranshdr/sales_receipts/delete")
	public SalesTransHdr deleteSalesReceiptsByTransHdr(@Valid @RequestBody SalesTransHdr salesTransHdr) {

		List<SalesReceipts> salesReceiptsList = salesReceiptsRepo.findBySalesTransHdr(salesTransHdr);
		salesReceiptsRepo.deleteAll(salesReceiptsList);
		salesReceiptsRepo.flush();

		return salesTransHdr;

	}

	@GetMapping("{companymstid}/salestranshdrbyvoucheranddate/{vouchernumber}/{branchcode}")
	public SalesTransHdr getSalesTransHdrByVoucherNumberAndDate(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @PathVariable("vouchernumber") String vouchernumber,
			@RequestParam("rdate") String reportdate)

	{
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		return salesTransHdrRepo.findByVoucherNumberAndVoucherDateAndCompanyMstId(vouchernumber, date, companymstid);
	}

	@GetMapping("{companymstid}/salestranshdr/{salestranshdrid}")
	public Optional<SalesTransHdr> getSalesTransHdrById(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("salestranshdrid") String salestranshdrid) {
		return salesTransHdrRepo.findByIdAndCompanyMstId(salestranshdrid, companymstid);

	}

	@PostMapping("{companymstid}/salestranshdr")
	public SalesTransHdr createSalesTransHdr(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SalesTransHdr salesTransHdr) {

		salesTransHdr.setPerformaInvoicePrinted("NO");
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		
			salesTransHdr.setCompanyMst(companyMst.get());

			SalesTransHdr saveSalesTransHdr = saveAndPublishService.saveSalesTransHdr(salesTransHdr,salesTransHdr.getBranchCode());
			
//			if (null!=saveSalesTransHdr)
//			{
//				saveAndPublishService.publishObjectToKafkaEvent(saveSalesTransHdr, salesTransHdr.getBranchCode(), KafkaMapleEventType.SALESTRANSHDR,KafkaMapleEventType.SERVER);
//			}
//			
//			else {
//				
//				return null;
//			}

			// ===============saving sales trans hdr to cloud==============by
			// anandu===============05-08-2021=======
//			ResponseEntity<SalesTransHdr> saved = externalApi.salesTransHdrSavedToCloud(companymstid, saveSalesTransHdr);

			return saveSalesTransHdr;
		 
	}

	// ------------------ url doesn't use now ,use
	// salestranshdrfinalsaveoffer------------------------------
//	@PutMapping("{companymstid}/salestranshdrfinalsave/{salestranshdrid}")
//	public SalesTransHdr SalesFinalSave(
//
//			@PathVariable("salestranshdrid") String salestranshdrid, @PathVariable("companymstid") String companymstid,
//
//			@Valid @RequestBody SalesTransHdr salesTransHdrRequest) {
//
////		Boolean offer = CheckOfferWhileFinalSave(salesTransHdrRequest);
//
//		return salesTransHdrRepo.findById(salestranshdrid).map(salestranshdr -> {
//
//			if (null == salesTransHdrRequest.getVoucherNumber()) {
//
//				if (null != salesTransHdrRequest.getInvoiceNumberPrefix()) {
//					VoucherNumber voucherNo = voucherService.generateInvoice(
//							SystemSetting.getFinancialYear() + salesTransHdrRequest.getInvoiceNumberPrefix(),
//							salestranshdr.getCompanyMst().getId());
//
//					String vcNo = voucherNo.getCode();
//
//					if (salesTransHdrRequest.getInvoiceNumberPrefix().equalsIgnoreCase("dd-MM")) {
//
//						vcNo = vcNo.replace(SystemSetting.getFinancialYear(), "");
//
//						vcNo = vcNo.replace(salesTransHdrRequest.getInvoiceNumberPrefix(), "");
//						vcNo = vcNo.replace(salestranshdr.getCompanyMst().getId(), "");
//
//						System.out.println("vcNo =" + vcNo);
//
//						vcNo = SystemSetting.SqlDateTostring(salesTransHdrRequest.getVoucherDate(), "dd-MM") + "-"
//								+ vcNo;
//						System.out.println("vcNo =" + vcNo);
//					} else if (salesTransHdrRequest.getInvoiceNumberPrefix().equalsIgnoreCase("MM-dd")) {
//						vcNo = vcNo.replace(SystemSetting.getFinancialYear(), "");
//
//						vcNo = vcNo.replace(salesTransHdrRequest.getInvoiceNumberPrefix(), "");
//						vcNo = vcNo.replace(salestranshdr.getCompanyMst().getId(), "");
//
//						System.out.println("vcNo =" + vcNo);
//
//						vcNo = SystemSetting.SqlDateTostring(salesTransHdrRequest.getVoucherDate(), "MM-dd") + "-"
//								+ vcNo;
//						System.out.println("vcNo =" + vcNo);
//					}
//					salesTransHdrRequest.setVoucherNumber(vcNo);
//				} else {
//					VoucherNumber voucherNo = voucherService.generateInvoice(SystemSetting.getFinancialYear() + "POS",
//							salestranshdr.getCompanyMst().getId());
//					salesTransHdrRequest.setVoucherNumber(voucherNo.getCode());
//				}
//
//			}
//			salestranshdr.setLocalCustomerMst(salesTransHdrRequest.getLocalCustomerMst());
//
//			salestranshdr.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
//			// salestranshdr.setNumericVoucherNumber(salesTransHdrRequest.getNumericVoucherNumber());
//			salestranshdr.setCustomerId(salesTransHdrRequest.getCustomerId());
//			salestranshdr.setCardamount(salesTransHdrRequest.getCardamount());
//			salestranshdr.setCardNo(salesTransHdrRequest.getCardNo());
//			salestranshdr.setCashPay(salesTransHdrRequest.getCashPay());
//			salestranshdr.setCreditOrCash(salesTransHdrRequest.getCreditOrCash());
//			salestranshdr.setDeliveryBoyId(salesTransHdrRequest.getDeliveryBoyId());
//			salestranshdr.setInvoiceAmount(salesTransHdrRequest.getInvoiceAmount());
//			salestranshdr.setIsBranchSales(salesTransHdrRequest.getBranchCode());
//			salestranshdr.setMachineId(salesTransHdrRequest.getMachineId());
//			salestranshdr.setPaidAmount(salesTransHdrRequest.getPaidAmount());
//			salestranshdr.setSalesManId(salesTransHdrRequest.getSalesManId());
//			salestranshdr.setSalesMode(salesTransHdrRequest.getSalesMode());
//			salestranshdr.setServingTableName(salesTransHdrRequest.getServingTableName());
//			// salestranshdr.setBranchCode(salesTransHdrRequest.getBranchCode());
//
//			salestranshdr.setCustomerMst(salesTransHdrRequest.getCustomerMst());
//
//			Calendar calendar = Calendar.getInstance();
//			java.util.Date currentDate = calendar.getTime();
//			java.sql.Date date = new java.sql.Date(currentDate.getTime());
//
//			String strDate = SystemSetting.SqlDateTostring(date);
//
//			date = SystemSetting.StringToSqlDate(strDate, "dd/MM/yyyy");
//
//			if (null == salesTransHdrRequest.getVoucherDate()) {
//				salesTransHdrRequest.setVoucherDate(date);
//			}
//
//			// salesTransHdrRequest.setVoucherDate(salesTransHdrRequest.getVoucherDate());
//			salesTransHdrRequest.setCompanyMst(salestranshdr.getCompanyMst());
//
//			salesTransHdrRequest.setSourceIP(salestranshdr.getSourceIP());
//			salesTransHdrRequest.setSourcePort(salestranshdr.getSourcePort());
//
//			if (null != salestranshdr.getBranchCode()) {
//				salesTransHdrRequest.setBranchCode(salestranshdr.getBranchCode());
//			}
//
//			salestranshdr = salesTransHdrRepo.saveAndFlush(salesTransHdrRequest);
//
//			/*
//			 * mange Sales receipts
//			 */
//
//			String SalesMode = salestranshdr.getSalesMode();
//
//			Double invoiceAmount = salestranshdr.getInvoiceAmount();
//			Double cashPaidAmount = salestranshdr.getCashPay();
//			Double cardAmount = salestranshdr.getCardamount();
//			Double sodexoAmount = salestranshdr.getSodexoAmount();
//			Double creditAmount = salestranshdr.getCreditAmount();
//
//			List<SalesReceipts> salesReceiptsList = salesReceiptsRepo.findByCompanyMstIdAndSalesTransHdrId(companymstid,
//					salestranshdr.getId());
//
//			Iterator iter = salesReceiptsList.iterator();
//			while (iter.hasNext()) {
//				SalesReceipts salesReceipts = (SalesReceipts) iter.next();
//				salesReceipts.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
//				salesReceipts.setCompanyMst(salesTransHdrRequest.getCompanyMst());
//				salesReceiptsRepo.saveAndFlush(salesReceipts);
//
//			}
//
//			/*
//			 * while (iter.hasNext()) { SalesReceipts salesReceipts = (SalesReceipts)
//			 * iter.next();
//			 * salesReceipts.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
//			 * salesReceipts.setCompanyMst(salesTransHdrRequest.getCompanyMst());
//			 * salesReceiptsRepo.saveAndFlush(salesReceipts);
//			 * 
//			 * }
//			 */
//
//			List<AccountReceivable> accountReceivableList = accountReceivableRepository
//					.findByCompanyMstAndSalesTransHdr(salestranshdr.getCompanyMst(), salestranshdr);
//
//			if (null != accountReceivableList && accountReceivableList.size() > 0) {
//				accountReceivable = accountReceivableList.get(0);
//
//				accountReceivable.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
//				// accountReceivable.setVoucherDate(salesTransHdrRequest.getVoucherDate());
//
//				accountReceivable = accountReceivableRepository.saveAndFlush(accountReceivable);
//
//			}
//
//			if (null != accountReceivable) {
//				accountReceivable.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
//				java.sql.Date ldate = SystemSetting.UtilDateToSQLDate(salesTransHdrRequest.getVoucherDate());
//				accountReceivable.setVoucherDate(ldate);
//				accountReceivable = accountReceivableRepository.saveAndFlush(accountReceivable);
//
//				variablesAR.clear();
//
//				variablesAR.put("voucherNumber", accountReceivable.getVoucherNumber());
//				variablesAR.put("voucherDate", accountReceivable.getVoucherDate());
//				variablesAR.put("inet", 0);
//				variablesAR.put("id", accountReceivable.getId());
//				variablesAR.put("branchcode", accountReceivable.getSalesTransHdr().getBranchCode());
//
//				variablesAR.put("companyid", accountReceivable.getCompanyMst());
//				if (serverorclient.equalsIgnoreCase("REST")) {
//					variablesAR.put("REST", 1);
//				} else {
//					variablesAR.put("REST", 0);
//				}
//
//				variablesAR.put("WF", "forwardAccountReceivable");
//
//				String workflow = (String) variablesAR.get("WF");
//				String voucherNumber = (String) variablesAR.get("voucherNumber");
//				String sourceID = (String) variablesAR.get("id");
//
//				LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//				lmsQueueMst.setCompanyMst((CompanyMst) variablesAR.get("companyid"));
//
//				java.util.Date uDate = (Date) variablesAR.get("voucherDate");
//				java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(uDate);
//
//				uDate = SystemSetting.SqlDateToUtilDate(sqlDate);
//				lmsQueueMst.setVoucherDate(sqlDate);
//
//				// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
//				lmsQueueMst.setVoucherNumber((String) variablesAR.get("voucherNumber"));
//				lmsQueueMst.setVoucherType(workflow);
//				lmsQueueMst.setPostedToServer("NO");
//				lmsQueueMst.setJobClass("forwardAccountReceivable");
//				lmsQueueMst.setCronJob(true);
//				lmsQueueMst.setJobName(workflow + sourceID);
//				lmsQueueMst.setJobGroup(workflow);
//				lmsQueueMst.setRepeatTime(60000L);
//				lmsQueueMst.setSourceObjectId(sourceID);
//
//				lmsQueueMst.setBranchCode((String) variablesAR.get("branchcode"));
//
//				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//				variablesAR.put("lmsqid", lmsQueueMst.getId());
//
//			}
//
//			/*
//			 * Manage Stock
//			 * 
//			 */
//
//			try {
//				salesAccounting.execute(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(),
//						salestranshdr.getId(), salestranshdr.getCompanyMst());
//			} catch (PartialAccountingException e) {
//				// TODO Auto-generated catch block
//				logger.error(e.getMessage());
//				return null;
//			}
//
//			salesDayEndReporting.execute(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(),
//					salestranshdr.getCompanyMst(), salestranshdr.getId());
//
//			SalesStockUpdate(salesTransHdrRequest.getVoucherNumber(), salesTransHdrRequest.getVoucherDate(),
//					salestranshdr);
//
//			/*
//			 * Manage Accounting
//			 */
//
//			variablesFS.clear();
//
//			String creditOrCash = salestranshdr.getCreditOrCash();
//			if (null == creditOrCash) {
//				creditOrCash = "CASH";
//			}
//
//			if (null == salestranshdr.getIsBranchSales()) {
//				salestranshdr.setIsBranchSales("N");
//			}
//
//			String isBranchSales = salestranshdr.getIsBranchSales();
//			int int_isBranchSales = 0;
//			if (isBranchSales.equalsIgnoreCase("Y")) {
//				int_isBranchSales = 1;
//
//			} else {
//				int_isBranchSales = 0;
//			}
//
//			variablesFS.put("voucherNumber", salestranshdr.getVoucherNumber());
//			variablesFS.put("voucherDate", salestranshdr.getVoucherDate());
//			variablesFS.put("inet", 0);
//			variablesFS.put("id", salestranshdr.getId());
//			variablesFS.put("isbranchsales", int_isBranchSales);
//			variablesFS.put("companyid", salestranshdr.getCompanyMst());
//
//			if (serverorclient.equalsIgnoreCase("REST")) {
//				variablesFS.put("REST", 1);
//			} else {
//				variablesFS.put("REST", 0);
//			}
//
//			if (creditOrCash.equalsIgnoreCase("CREDIT") || creditOrCash.equalsIgnoreCase("B2B")) {
//				variablesFS.put("CREDIT", 1);
//			} else {
//				variablesFS.put("CREDIT", 0);
//			}
//
//			variablesFS.put("branchcode", salestranshdr.getBranchCode());
//
//			variablesFS.put("WF", "forwardSales");
//
//			String workflow = (String) variablesFS.get("WF");
//			String voucherNumber = (String) variablesFS.get("voucherNumber");
//			String sourceID = (String) variablesFS.get("id");
//
//			LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//			lmsQueueMst.setCompanyMst((CompanyMst) variablesFS.get("companyid"));
//
//			java.util.Date uDate = (Date) variablesFS.get("voucherDate");
//			java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(uDate);
//
//			uDate = SystemSetting.SqlDateToUtilDate(sqlDate);
//			lmsQueueMst.setVoucherDate(sqlDate);
//
//			// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//			lmsQueueMst.setVoucherNumber((String) variablesFS.get("voucherNumber"));
//			lmsQueueMst.setVoucherType(workflow);
//			lmsQueueMst.setPostedToServer("NO");
//			lmsQueueMst.setJobClass("forwardSales");
//			lmsQueueMst.setCronJob(true);
//			lmsQueueMst.setJobName(workflow + sourceID);
//			lmsQueueMst.setJobGroup(workflow);
//			lmsQueueMst.setRepeatTime(60000L);
//			lmsQueueMst.setSourceObjectId(sourceID);
//
//			lmsQueueMst.setBranchCode((String) variablesFS.get("branchcode"));
//
//			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//			variablesFS.put("lmsqid", lmsQueueMst.getId());
//
//			LmsQueueTallyMst lmsQueueTallyMst = new LmsQueueTallyMst();
//
//			variablesTS.clear();
//
//			lmsQueueTallyMst.setCompanyMst((CompanyMst) variablesTS.get("companyid"));
//
//			// java.util.Date uDate = (Date) variables.get("voucherDate");
//			// java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(uDate);
//
//			uDate = SystemSetting.SqlDateToUtilDate(sqlDate);
//			lmsQueueMst.setVoucherDate(sqlDate);
//
//			// lmsQueueTallyMst.setVoucherDate((Date) variables.get("voucherDate"));
//			lmsQueueTallyMst.setVoucherNumber((String) variablesTS.get("voucherNumber"));
//			lmsQueueTallyMst.setVoucherType(workflow);
//			lmsQueueTallyMst.setPostedToTally("NO");
//			lmsQueueTallyMst.setJobClass("forwardSales");
//			lmsQueueTallyMst.setCronJob(true);
//			lmsQueueTallyMst.setJobName(workflow + sourceID);
//			lmsQueueTallyMst.setJobGroup(workflow);
//			lmsQueueTallyMst.setRepeatTime(60000L);
//			lmsQueueTallyMst.setSourceObjectId(sourceID);
//			lmsQueueTallyMst.setBranchCode((String) variablesTS.get("branchcode"));
//			lmsQueueTallyMst = lmsQueueTallyMstRepository.saveAndFlush(lmsQueueTallyMst);
//
//			variablesTS.put("lmsqid", lmsQueueMst.getId());
//
//			/*
//			 * If KOT update kot
//			 */
//			String sourceIP = salestranshdr.getSourceIP();
//			String sourcePort = salestranshdr.getSourcePort();
//
//			/*
//			 * if (null != sourceIP && null != sourcePort) { SocketConnection
//			 * socketConnection = new SocketConnection();
//			 * 
//			 * socketConnection.sendToSocket(sourceIP, sourcePort, salestranshdr.getId() +
//			 * ":" + "POS_SAVED" + ":" + salestranshdr.getServingTableName()); //
//			 * socketConnection.sendToSocket(sourceIP, sourcePort, "OVER"); }
//			 */
//
//			if (!variablesTS.isEmpty()) {
//				eventBus.post(variablesTS);
//			}
//			if (!variablesFS.isEmpty()) {
//				eventBus.post(variablesFS);
//			}
//			if (!variablesAR.isEmpty()) {
//				eventBus.post(variablesAR);
//			}
//
//			return salestranshdr;
//		}).orElseThrow(() -> new ResourceNotFoundException("salestranshdrid " + salestranshdrid + " not found"));
//
//	}

	// here we are retriving the daily sales summary report

	@GetMapping("{companymstid}/salesTransdr/{startDate}{endDate}/dailySaleReport")
	public List<SalesTransHdr> retrivedailySalesSummary(@PathVariable(value = "startDate") Date startDate,
			@PathVariable(value = "endDate)") Date endDate) {
		return salesTransHdrRepo.salesSummaryReport(startDate, endDate);

	}

	// http://localhost:8080/salestranshdr/WAITER-001/TABLE1/kotinprogressinvoice
	@GetMapping("{companymstid}/salestranshdr/{salesman}/{tablename}/{customSalesMode}/kotinprogressinvoice")
	public List<SalesTransHdr> retrieveInProgressKotInvoices(@PathVariable("companymstid") String companymstid,
			@PathVariable("salesman") String salesman, @PathVariable("customSalesMode") String customSalesMode,
			@PathVariable("tablename") String tablename) {
		if (null == customSalesMode) {
			return salesTransHdrRepo.getInprogressKotInvoices(salesman, tablename);
		} else {
			return salesTransHdrRepo.getInprogressKotInvoicesCustomiseSalesmode(salesman, tablename, customSalesMode);
		}
	}

	// for mobile
	@PutMapping("{companymstid}/salestranshdr/{salestranshdrid}/mobilefinalsave")
	public synchronized SalesTransHdr SalesMobileFinalSave(

			@PathVariable("salestranshdrid") String salestranshdrid, @PathVariable("companymstid") String companymstid,

			@Valid @RequestBody SalesTransHdr salesTransHdrRequest) {

		return salesTransHdrRepo.findById(salestranshdrid).map(salestranshdr -> {

			/*
			 * Sore is hardcoded for time being. Not sure if this url is used anymore - Regy
			 * Dec 20, 2020
			 */

			String store = "MAIN";

			if (null == salesTransHdrRequest.getVoucherNumber()) {

				if (null != salesTransHdrRequest.getInvoiceNumberPrefix()) {

					String finYear = SystemSetting.getFinancialYear();

					VoucherNumber voucherNo = voucherService.generateInvoice(
							SystemSetting.getFinancialYear() + salesTransHdrRequest.getInvoiceNumberPrefix(),
							salestranshdr.getCompanyMst().getId());

					String vcNo = voucherNo.getCode();
					salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(vcNo));
					salestranshdr.setNumericVoucherNumber(Long.parseLong(vcNo));

					

					if (salesTransHdrRequest.getInvoiceNumberPrefix().equalsIgnoreCase("dd-MM")) {

						vcNo = vcNo.replace(SystemSetting.getFinancialYear(), "");

						vcNo = vcNo.replace(salesTransHdrRequest.getInvoiceNumberPrefix(), "");
						vcNo = vcNo.replace(salestranshdr.getCompanyMst().getId(), "");

						System.out.println("vcNo =" + vcNo);

						vcNo = SystemSetting.SqlDateTostring(salesTransHdrRequest.getVoucherDate(), "dd-MM") + "-"
								+ vcNo;
						System.out.println("vcNo =" + vcNo);
					} else if (salesTransHdrRequest.getInvoiceNumberPrefix().equalsIgnoreCase("MM-dd")) {
						vcNo = vcNo.replace(SystemSetting.getFinancialYear(), "");

						vcNo = vcNo.replace(salesTransHdrRequest.getInvoiceNumberPrefix(), "");
						vcNo = vcNo.replace(salestranshdr.getCompanyMst().getId(), "");

						System.out.println("vcNo =" + vcNo);

						vcNo = SystemSetting.SqlDateTostring(salesTransHdrRequest.getVoucherDate(), "MM-dd") + "-"
								+ vcNo;
						System.out.println("vcNo =" + vcNo);
					}
					salesTransHdrRequest.setVoucherNumber(vcNo);
				} else {
					VoucherNumber voucherNo = voucherService.generateInvoice(SystemSetting.getFinancialYear() + "POS",
							salestranshdr.getCompanyMst().getId());
					salesTransHdrRequest.setVoucherNumber(voucherNo.getCode());
				}

			}
			salestranshdr.setLocalCustomerMst(salesTransHdrRequest.getLocalCustomerMst());

			salestranshdr.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			// salestranshdr.setNumericVoucherNumber(salesTransHdrRequest.getNumericVoucherNumber());
			salestranshdr.setCustomerId(salesTransHdrRequest.getCustomerId());
			salestranshdr.setCardamount(salesTransHdrRequest.getCardamount());
			salestranshdr.setCardNo(salesTransHdrRequest.getCardNo());
			salestranshdr.setCashPay(salesTransHdrRequest.getCashPay());
			salestranshdr.setCreditOrCash(salesTransHdrRequest.getCreditOrCash());
			salestranshdr.setDeliveryBoyId(salesTransHdrRequest.getDeliveryBoyId());
			salestranshdr.setInvoiceAmount(salesTransHdrRequest.getInvoiceAmount());
			salestranshdr.setIsBranchSales(salesTransHdrRequest.getBranchCode());
			salestranshdr.setMachineId(salesTransHdrRequest.getMachineId());
			salestranshdr.setPaidAmount(salesTransHdrRequest.getPaidAmount());
			salestranshdr.setSalesManId(salesTransHdrRequest.getSalesManId());
			salestranshdr.setSalesMode(salesTransHdrRequest.getSalesMode());
			salestranshdr.setServingTableName(salesTransHdrRequest.getServingTableName());

			// salestranshdr.setBranchCode(salesTransHdrRequest.getBranchCode());

			Calendar calendar = Calendar.getInstance();
			java.util.Date currentDate = calendar.getTime();
			java.sql.Date date = new java.sql.Date(currentDate.getTime());

			String strDate = SystemSetting.SqlDateTostring(date);

			date = SystemSetting.StringToSqlDate(strDate, "dd/MM/yyyy");

			if (null == salesTransHdrRequest.getVoucherDate()) {
				salesTransHdrRequest.setVoucherDate(date);
			}

			// salesTransHdrRequest.setVoucherDate(salesTransHdrRequest.getVoucherDate());
			salesTransHdrRequest.setCompanyMst(salestranshdr.getCompanyMst());

			salesTransHdrRequest.setSourceIP(salestranshdr.getSourceIP());
			salesTransHdrRequest.setSourcePort(salestranshdr.getSourcePort());

			if (null != salestranshdr.getBranchCode()) {
				salesTransHdrRequest.setBranchCode(salestranshdr.getBranchCode());
			}

			 //salestranshdr = salesTransHdrRepo.save(salesTransHdrRequest);
			salestranshdr = saveAndPublishService.saveSalesTransHdr(salesTransHdrRequest,salesTransHdrRequest.getBranchCode());
			

			if (null!=salestranshdr)
			{
				saveAndPublishService.publishObjectToKafkaEvent(salestranshdr, 
						salestranshdr.getBranchCode(), 
						KafkaMapleEventType.SALESTRANSHDR,
						KafkaMapleEventType.SERVER,salestranshdr.getVoucherNumber());
			}

			/*
			 * mange Sales receipts
			 */

			String SalesMode = salestranshdr.getSalesMode();

			Double invoiceAmount = salestranshdr.getInvoiceAmount();
			Double cashPaidAmount = salestranshdr.getCashPay();
			Double cardAmount = salestranshdr.getCardamount();
			Double sodexoAmount = salestranshdr.getSodexoAmount();
			Double creditAmount = salestranshdr.getCreditAmount();

			List<SalesReceipts> salesReceiptsList = salesReceiptsRepo.findByCompanyMstIdAndSalesTransHdrId(companymstid,
					salestranshdr.getId());

			Iterator iter = salesReceiptsList.iterator();
			while (iter.hasNext()) {
				SalesReceipts salesReceipts = (SalesReceipts) iter.next();
				salesReceipts.setVoucherNumber(salestranshdr.getVoucherNumber());
				salesReceipts.setCompanyMst(salestranshdr.getCompanyMst());
				salesReceiptsRepo.save(salesReceipts);

			}

			List<AccountReceivable> accountReceivableList = accountReceivableRepository
					.findByCompanyMstAndSalesTransHdr(salestranshdr.getCompanyMst(), salestranshdr);

			accountReceivable = accountReceivableList.get(0);

			if (null != accountReceivable) {
				accountReceivable.setVoucherNumber(salestranshdr.getVoucherNumber());
				// accountReceivable.setVoucherDate(salesTransHdrRequest.getVoucherDate());

				accountReceivableRepository.save(accountReceivable);
			}

			while (iter.hasNext()) {
				SalesReceipts salesReceipts = (SalesReceipts) iter.next();
				salesReceipts.setVoucherNumber(salestranshdr.getVoucherNumber());
				salesReceipts.setCompanyMst(salestranshdr.getCompanyMst());
				salesReceiptsRepo.save(salesReceipts);

			}

			if (null != accountReceivable) {
				accountReceivable.setVoucherNumber(salestranshdr.getVoucherNumber());
				java.sql.Date ldate = SystemSetting.UtilDateToSQLDate(salestranshdr.getVoucherDate());
				accountReceivable.setVoucherDate(ldate);

				accountReceivableRepository.save(accountReceivable);
			}

			/*
			 * Manage Stock
			 * 
			 */

			try {
				salesAccounting.execute(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(),
						salestranshdr.getId(), salestranshdr.getCompanyMst());
			} catch (PartialAccountingException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
				return null;
			}

			salesDayEndReporting.execute(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(),
					salestranshdr.getCompanyMst(), salestranshdr.getId());

			SalesStockUpdate(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(), salestranshdr, store);

			/*
			 * Manage Accounting
			 */

			Map<String, Object> variables = new HashMap<String, Object>();

			String creditOrCash = salestranshdr.getCreditOrCash();
			if (null == creditOrCash) {
				creditOrCash = "CASH";
			}

			if (null == salestranshdr.getIsBranchSales()) {
				salestranshdr.setIsBranchSales("N");
			}

			String isBranchSales = salestranshdr.getIsBranchSales();
			int int_isBranchSales = 0;
			if (isBranchSales.equalsIgnoreCase("Y")) {
				int_isBranchSales = 1;

			} else {
				int_isBranchSales = 0;
			}

			variables.put("voucherNumber", salestranshdr.getVoucherNumber());
			variables.put("voucherDate", salestranshdr.getVoucherDate());
			variables.put("inet", 0);
			variables.put("id", salestranshdr.getId());
			variables.put("isbranchsales", int_isBranchSales);
			variables.put("companyid", salestranshdr.getCompanyMst());

			if (serverorclient.equalsIgnoreCase("REST")) {
				variables.put("REST", 1);
			} else {
				variables.put("REST", 0);
			}

			if (creditOrCash.equalsIgnoreCase("CREDIT") || creditOrCash.equalsIgnoreCase("B2B")) {
				variables.put("CREDIT", 1);
			} else {
				variables.put("CREDIT", 0);
			}

			variables.put("branchcode", salestranshdr.getBranchCode());

			variables.put("WF", "forwardSales");
			eventBus.post(variables);

			/*
			 * If KOT update kot
			 */
			String sourceIP = salestranshdr.getSourceIP();
			String sourcePort = salestranshdr.getSourcePort();
			if (null != sourceIP && null != sourcePort) {
				SocketClient socketConnection = new SocketClient();

				socketConnection.sendToSocket(sourceIP, sourcePort,
						salestranshdr.getId() + ":" + "POS_SAVED" + ":" + salestranshdr.getServingTableName());
				// socketConnection.sendToSocket(sourceIP, sourcePort, "OVER");
			}

			return salestranshdr;
		}).orElseThrow(() -> new ResourceNotFoundException("salestranshdrid " + salestranshdrid + " not found"));

	}
	// for mobile
	/*
	 * Following routine is not used. But can be modified for futue use
	 */

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/{custName}/newcustomer/sales")
	public List<SalesDtl> retrieveSalesFromMobile(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("custName") String custName,
			@RequestParam("itemName") String itemName, @RequestParam("Qty") String Qty,
			@RequestParam("Rate") String Rate, @RequestParam("SourceIP") String sourceip,
			@RequestParam("SourcePort") String sourceport) {
		itemName = itemName.replaceAll("%20", " ");

		String custPhno, custName1, custAddress;

		String custDtls[] = custName.split("-");

		custName1 = custDtls[0];
		// String custAddress1[] = custDtls[1].split(",");
		custPhno = custDtls[1];
		custAddress = custDtls[2];
//		custName = custName.replaceAll("%20", " ");
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);

		CompanyMst companyMst = companyMstOpt.get();
		AccountHeads accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMst(
				custName1,companyMst);
		if (null == accountHeads) {
			AccountHeads accountHeadsss = new AccountHeads();

			accountHeadsss.setCompanyMst(companyMst);
			accountHeadsss.setCreditPeriod(0);
			accountHeadsss.setAccountName(custName1);
			accountHeadsss.setPartyAddress1(custAddress);
			accountHeadsss.setCustomerContact(custPhno);
			accountHeads.setGroupOnly("N");

			VoucherNumber custID = voucherService.generateInvoice("CU", companyMst.getId());

			accountHeadsss.setId("MOB" + custID.getCode());

			accountHeadsss.setId("MOB" + custName1);
			accountHeadsRepository.save(accountHeadsss);

			

		}

		AccountHeads accountHeadsOpt1 = accountHeadsRepository.findByAccountNameAndCompanyMst(
				custName1,companyMst);

		AccountHeads accountHeads2 = accountHeadsOpt1;

		double qty = Double.parseDouble(Qty);
		SalesTransHdr salesTransHdr = null;
		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepo.getInprogressPosInvoices(accountHeads2.getId());
		if (salesTransHdrList.size() == 0) {
			salesTransHdr = new SalesTransHdr();

			salesTransHdr.setBranchCode(branchcode);

			salesTransHdr.setCompanyMst(companyMst);
			salesTransHdr.setAccountHeads(accountHeads2);
			salesTransHdr.setSalesMode("B2C");

			salesTransHdr.setIsBranchSales("N");
			salesTransHdr.setCreditOrCash("CASH");
			salesTransHdr.setSourceIP(sourceip);
			salesTransHdr.setSourcePort(sourceport);
			salesTransHdr.setVoucherType("RETAIL");
			
			salesTransHdr=saveAndPublishService.saveSalesTransHdr(salesTransHdr,salesTransHdr.getBranchCode());
			
			if (null!=salesTransHdr)
			{
				saveAndPublishService.publishObjectToKafkaEvent(salesTransHdr, 
						salesTransHdr.getBranchCode(), 
						KafkaMapleEventType.SALESTRANSHDR,
						KafkaMapleEventType.SERVER,salesTransHdr.getVoucherNumber());
			}
			//salesTransHdrRepo.save(salesTransHdr);

		} else {
			salesTransHdr = salesTransHdrList.get(0);
		}

		SalesDtl salesDtl = new SalesDtl();
		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setCompanyMst(companyMst);

		ItemMst item = itemMstRepository.findByItemName(itemName);

		if (null == item.getBarCode()) {
			item.setBarCode(item.getItemName());
		}
		salesDtl.setBarcode(item.getBarCode());

		salesDtl.setTaxRate(item.getTaxRate());
		salesDtl.setQty(qty);
		salesDtl.setCessRate(item.getCess());
		salesDtl.setCgstTaxRate(item.getTaxRate() / 2);
		salesDtl.setSgstTaxRate(item.getTaxRate() / 2);
		salesDtl.setAddCessRate(0.0);
		salesDtl.setBatch(MapleConstants.Nobatch);
		salesDtl.setItemId(item.getId());
		salesDtl.setMrp(item.getStandardPrice());
		salesDtl.setUnitId(item.getUnitId());
		salesDtl.setItemName(item.getItemName());

		/*
		 * Other fields like sgst amount , cgst amopunt to be set
		 */

		Double rateBeforeTax = (100 * item.getStandardPrice()) / (100 + item.getTaxRate());
		salesDtl.setRate(rateBeforeTax);

		double cessRate = item.getCess();
		double cessAmount = 0.0;
		if (cessRate > 0) {

			rateBeforeTax = (100 * item.getStandardPrice()) / (100 + item.getCess() + item.getCess());
			salesDtl.setRate(rateBeforeTax);

			cessAmount = salesDtl.getQty() * salesDtl.getRate() * cessRate / 100;

		}

		salesDtl.setCessRate(cessRate);

		salesDtl.setCessAmount(cessAmount);

		salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);
		salesDtl.setDiscount(0.0);

		salesDtl.setIgstAmount(0.0);
		salesDtl.setIgstTaxRate(0.0);

		salesDtl.setAmount(item.getStandardPrice() * qty);

		salesDtl=saveAndPublishService.saveSalesDtl(salesDtl,
				salesDtl.getSalesTransHdr().getBranchCode());
		
		/*if (null!=salesDtl)
		{
			saveAndPublishService.publishObjectToKafkaEvent(salesDtl,  
					salesDtl.getSalesTransHdr().getBranchCode(), 
					KafkaMapleEventType.SALESDTL,KafkaMapleEventType.SERVER);
		}*/
		
	
		//salesDetailsRepo.save(salesDtl);

		ArrayList<SalesDtl> array = new ArrayList();
		array.add(salesDtl);
		return array;

	}

	/*
	 * FOR MOBILE KOT
	 */
	// http://localhost:8080/salestranshdr/WAITER-001/TABLE1/kotinprogressinvoice
	@GetMapping("{companymstid}/{branchcode}/salestranshdr/{salesman}/{tablename}/kotinprogressinvoicetab")
	public List<SalesDtl> retrieveInProgressKotInvoicestab(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("salesman") String salesman,
			@PathVariable("tablename") String tablename, @RequestParam("itemName") String itemName,
			@RequestParam("Qty") String Qty, @RequestParam("Rate") String Rate,
			@RequestParam("SourceIP") String sourceip, @RequestParam("SourcePort") String sourceport,
			@RequestParam("fbKey") String fbKey) {

		itemName = itemName.replaceAll("%20", " ");
		tablename = tablename.replaceAll("%20", " ");
		salesman = salesman.replaceAll("%20", " ");

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);

		CompanyMst companyMst = companyMstOpt.get();

		/*
		 * insert Table if it is new
		 */

		AddKotTable addKotTable = addKotTableRepository.findByCompanyMstIdAndTableName(companymstid, tablename);

		if (null == addKotTable) {
			addKotTable = new AddKotTable();
			addKotTable.setCompanyMst(companyMst);
			addKotTable.setTableName(tablename);
			addKotTable.setStatus("ACTIVE");

			addKotTable.setId("MOB" + tablename);
			addKotTableRepository.save(addKotTable);
		}

		AddKotWaiter addKotWaiter = addKotWaiterRepository.findByCompanyMstIdAndWaiterName(companymstid, salesman);

		if (null == addKotWaiter) {
			addKotWaiter = new AddKotWaiter();

			addKotWaiter.setCompanyMst(companyMst);
			addKotWaiter.setId("MOB" + salesman);
			addKotWaiter.setStatus("ACTIVE");
			addKotWaiter.setWaiterName(salesman);

			addKotWaiterRepository.save(addKotWaiter);
		}

		double qty = Double.parseDouble(Qty);
		SalesTransHdr salesTransHdr = null;
		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepo.getInprogressKotInvoices(salesman, tablename);
		if (salesTransHdrList.size() == 0) {
			salesTransHdr = new SalesTransHdr();

			salesTransHdr.setBranchCode(branchcode);

			AccountHeads accountHeadsOpt = accountHeadsRepository.findByAccountNameAndCompanyMstId("KOT",
					companymstid);

			AccountHeads accountHeads = accountHeadsOpt;

			salesTransHdr.setCompanyMst(companyMst);
			salesTransHdr.setAccountHeads(accountHeads);
			salesTransHdr.setSalesMode("KOT");

			salesTransHdr.setSalesManId(salesman);
			salesTransHdr.setServingTableName(tablename);
			salesTransHdr.setIsBranchSales("N");
			salesTransHdr.setCreditOrCash("CASH");
			salesTransHdr.setSourceIP(sourceip);
			salesTransHdr.setSourcePort(sourceport);

			//salesTransHdrRepo.save(salesTransHdr);
			
			salesTransHdr=saveAndPublishService.saveSalesTransHdr(salesTransHdr,salesTransHdr.getBranchCode());
			
			if (null!=salesTransHdr)
			{
				saveAndPublishService.publishObjectToKafkaEvent(salesTransHdr,  
						salesTransHdr.getBranchCode(), 
						KafkaMapleEventType.SALESTRANSHDR,
						KafkaMapleEventType.SERVER, salesTransHdr.getVoucherNumber());
			}

		} else {
			salesTransHdr = salesTransHdrList.get(0);
		}

		SalesDtl salesDtl = new SalesDtl();
		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setCompanyMst(companyMst);

		ItemMst item = itemMstRepository.findByItemName(itemName);

		UnitMst unitMst = unitMstRepository.findById(item.getUnitId()).get();

		if (null == item.getBarCode()) {
			item.setBarCode(item.getItemName());
		}
		salesDtl.setBarcode(item.getBarCode());

		salesDtl.setTaxRate(item.getTaxRate());
		salesDtl.setQty(qty);
		salesDtl.setCessRate(item.getCess());
		salesDtl.setCgstTaxRate(item.getTaxRate() / 2);
		salesDtl.setSgstTaxRate(item.getTaxRate() / 2);
		salesDtl.setAddCessRate(0.0);
		salesDtl.setBatch(MapleConstants.Nobatch);
		salesDtl.setItemId(item.getId());
		salesDtl.setMrp(item.getStandardPrice());
		salesDtl.setUnitId(item.getUnitId());
		salesDtl.setItemName(item.getItemName());

		salesDtl.setUnitName(unitMst.getUnitName());
		salesDtl.setFbKey(fbKey);

		/*
		 * Other fields like sgst amount , cgst amopunt to be set
		 */

		Double rateBeforeTax = (100 * item.getStandardPrice()) / (100 + item.getTaxRate());
		salesDtl.setRate(rateBeforeTax);

		double cessRate = item.getCess();
		double cessAmount = 0.0;
		if (cessRate > 0) {

			rateBeforeTax = (100 * item.getStandardPrice()) / (100 + item.getCess() + item.getCess());
			salesDtl.setRate(rateBeforeTax);

			cessAmount = salesDtl.getQty() * salesDtl.getRate() * cessRate / 100;

		}

		salesDtl.setCessRate(cessRate);

		salesDtl.setCessAmount(cessAmount);

		salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);
		salesDtl.setDiscount(0.0);

		salesDtl.setIgstAmount(0.0);
		salesDtl.setIgstTaxRate(0.0);

		salesDtl.setAmount(item.getStandardPrice() * qty);

		salesDetailsRepo.save(salesDtl);

		ArrayList<SalesDtl> array = new ArrayList();
		array.add(salesDtl);

		return array;

	}

	// For mobile :Sales Made through Mobile application
	@Transactional
	@GetMapping("{companymstid}/{branchcode}/salestranshdr/{ordertableid}/{custName}/mobileinprogressretail")
	public synchronized List<SalesDtl> retrieveInProgressMobCust(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("ordertableid") String ordertableid,
			@PathVariable("custName") String custName, @RequestParam("itemName") String itemName,
			@RequestParam("Qty") String Qty, @RequestParam("Rate") String Rate,
			@RequestParam("SourceIP") String sourceip, @RequestParam("SourcePort") String sourceport,
			@RequestParam("fbKey") String fbKey) {

		/*
		 * @PathVariable("companymstid") String companymstid,
		 * 
		 * @PathVariable("branchcode") String branchcode,
		 * 
		 * @PathVariable("custName") String custName,
		 * 
		 * @RequestParam("itemName") String itemName,
		 * 
		 * @RequestParam("Qty") String Qty,
		 * 
		 * @RequestParam("Rate") String Rate,
		 * 
		 * @RequestParam("SourceIP") String sourceip,
		 * 
		 * @RequestParam("SourcePort") String sourceport
		 */
		System.out.println("mobileinprogressretail called ");
		Optional<SalesDtl> salesDtlExist = null;

		String custPhno = "", custName1 = "", custAddress = "";

		try {
			salesDtlExist = salesDetailsRepo.findByFbKey(fbKey);
		} catch (Exception e) {
			return null;

		}
		if (salesDtlExist.isPresent()) {
			ArrayList<SalesDtl> array = new ArrayList();

			salesDtlExist.get().setFbKey(fbKey);

			array.add(salesDtlExist.get());
			System.out.println("Already Exist" + itemName);

			return array;
		}

		itemName = itemName.replaceAll("%20", " ");
		custName = custName.replaceAll("%20", " ");
		ordertableid = ordertableid.replaceAll("%20", " ");

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);

		CompanyMst companyMst = companyMstOpt.get();

		// Save Sale Type
		SalesTypeMst salesTypeMst = salesTypeMstRepository.findByCompanyMstAndSalesType(companyMst, "RETAIL");
		if (null == salesTypeMst) {
			salesTypeMst = new SalesTypeMst();
			salesTypeMst.setCompanyMst(companyMst);
			salesTypeMst.setSalesPrefix("RT");
			salesTypeMst.setSalesType("RETAIL");
			salesTypeMstRepository.save(salesTypeMst);
		}

		/*
		 * insert Customer if it is new
		 */
		AccountHeads accountHeads = null;

		AccountHeads accountHeadsOpt = accountHeadsRepository.findByAccountNameAndCompanyMst(
				custName,companyMst);

		if (custName.contains("-") && null==accountHeadsOpt) {
			String custDtls[] = custName.split("-");
			custName1 = custDtls[0];
			// String custAddress1[] = custDtls[1].split(",");
			custPhno = custDtls[1];
			custAddress = custDtls[2];
			accountHeadsOpt = accountHeadsRepository.findByAccountNameAndCompanyMst(custName1,companyMst);
		}

		if (null == accountHeadsOpt) {

			accountHeads = new AccountHeads();

			accountHeads.setCompanyMst(companyMst);
			accountHeads.setCreditPeriod(0);
			accountHeads.setPartyAddress1(custAddress);
			accountHeads.setCustomerContact(custPhno);

			// customerMst.setId("MOB" + custName);

			VoucherNumber custID = voucherService.generateInvoice("CU", companyMst.getId());

			accountHeads.setId("MOB" + custID.getCode());



			accountHeads.setAccountName(custName1);
			accountHeads.setGroupOnly("N");

			accountHeadsRepository.saveAndFlush(accountHeads);

		} else {
			accountHeads = accountHeadsOpt;

		}

		double qty = Double.parseDouble(Qty);
		SalesTransHdr salesTransHdr = null;
		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepo.getInprogressWholeSaleInvoices(accountHeads.getId());
		if (salesTransHdrList.size() == 0) {
			salesTransHdr = new SalesTransHdr();
			salesTransHdr.setBranchCode(branchcode);
			salesTransHdr.setCompanyMst(companyMst);
			salesTransHdr.setAccountHeads(accountHeads);
			salesTransHdr.setSalesMode("B2C");

			salesTransHdr.setFbKey(fbKey);
			// salesTransHdr.setSalesManId(salesman);
			salesTransHdr.setCustomerId(accountHeads.getId());
			salesTransHdr.setIsBranchSales("N");
			salesTransHdr.setCreditOrCash("CASH");
			salesTransHdr.setSourceIP(sourceip);
			salesTransHdr.setSourcePort(sourceport);
			salesTransHdr.setVoucherType("RETAIL");

			salesTransHdr.setInvoiceNumberPrefix("RT");

			//salesTransHdr = salesTransHdrRepo.saveAndFlush(salesTransHdr);
			salesTransHdr = saveAndPublishService.saveSalesTransHdr(salesTransHdr, salesTransHdr.getBranchCode());
			 

		} else {
			salesTransHdr = salesTransHdrList.get(0);
		}
		ItemMst item = itemMstRepository.findByItemName(itemName);
		Optional<SalesDtl> salesDtlList = salesDetailsRepo.findBySalesTransHdrAndItemIdAndQty(salesTransHdr,
				item.getId(), qty);

		UnitMst unitMst = unitMstRepository.findById(item.getUnitId()).get();

		if (salesDtlList.isPresent()) {
			ArrayList<SalesDtl> array = new ArrayList();

			SalesDtl salesDtl = salesDtlList.get();
			salesDtl.setFbKey(fbKey);

			array.add(salesDtl);
			System.out.print("Already Exist");

			return array;
		}
		SalesDtl salesDtl = new SalesDtl();
		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setCompanyMst(companyMst);

		if (null == item.getBarCode()) {
			item.setBarCode(item.getItemName());
		}
		salesDtl.setBarcode(item.getBarCode());

		salesDtl.setTaxRate(item.getTaxRate());
		salesDtl.setQty(qty);
		salesDtl.setCessRate(item.getCess());
		salesDtl.setCgstTaxRate(item.getTaxRate() / 2);
		salesDtl.setSgstTaxRate(item.getTaxRate() / 2);
		salesDtl.setAddCessRate(0.0);
		salesDtl.setBatch(MapleConstants.Nobatch);
		salesDtl.setUnitName(unitMst.getUnitName());
		salesDtl.setItemId(item.getId());
		salesDtl.setMrp(item.getStandardPrice());
		salesDtl.setUnitId(item.getUnitId());
		salesDtl.setItemName(item.getItemName());
		salesDtl.setFbKey(fbKey);
		salesDtl.setSalesTransHdr(salesTransHdr);

		/*
		 * Other fields like sgst amount , cgst amopunt to be set
		 */

		Double rateBeforeTax = (100 * item.getStandardPrice()) / (100 + item.getTaxRate());
		salesDtl.setRate(rateBeforeTax);

		double cessRate = item.getCess();
		double cessAmount = 0.0;
		if (cessRate > 0) {

			rateBeforeTax = (100 * item.getStandardPrice()) / (100 + item.getCess() + item.getCess());
			salesDtl.setRate(rateBeforeTax);

			cessAmount = salesDtl.getQty() * salesDtl.getRate() * cessRate / 100;

		}

		salesDtl.setCessRate(cessRate);

		salesDtl.setCessAmount(cessAmount);

		salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);
		salesDtl.setDiscount(0.0);

		salesDtl.setIgstAmount(0.0);
		salesDtl.setIgstTaxRate(0.0);

		salesDtl.setAmount(item.getStandardPrice() * qty);

		salesDtl = salesDetailsRepo.saveAndFlush(salesDtl);

		ArrayList<SalesDtl> array = new ArrayList();
		array.add(salesDtl);

		System.out.println("mobileinprogressretail called emr " + array.toString());
		return array;

	}

	@GetMapping("{companymstid}/salesbydate")
	public List<Object> retrieveAllSalesOrders() {
		return salesTransHdrRepo.getdailySales();

	}

	@GetMapping("{companymstid}/salesinvoiceall2")
	public List<Object> getallsales(@PathVariable(value = "companymstid") String companymstid) {
		return salesTransHdrRepo.salesInvoiceAll(companymstid);
	}

	@GetMapping("{companymstid}/getitemdetails/{salestranshdrid}")
	public List<Object> getitemdetails(@PathVariable("salestranshdrid") String salestranshdrid,
			@PathVariable(value = "companymstid") String companymstid) {
		return salesTransHdrRepo.getItemBySalesTransHdr(companymstid, salestranshdrid);

	}

	@GetMapping("{companymstid}/summaryonlinesalesnew")
	public List<SummaryOnlineSales> onlineSalesReport() {

		List<Object> objList = salesTransHdrRepo.onlineSaleReport();

		List<SummaryOnlineSales> reportsummary = new ArrayList<SummaryOnlineSales>();
		for (int i = 0; i < objList.size(); i++) {
			Object[] objAray = (Object[]) objList.get(i);
			SummaryOnlineSales summary = new SummaryOnlineSales();
			summary.setSalesMode((String) objAray[0]);
			summary.setQty((Double) objAray[1]);
			summary.setAmount((Double) objAray[2]);
			reportsummary.add(summary);
		}
		return reportsummary;
	}

	@GetMapping("{companymstid}/onlinesalesummaryreport")
	public List<SummaryOnlineSales> onlineSalesSummaryReport() {

		List<Object> objList = salesTransHdrRepo.onlineSaleSummaryReport();

		List<SummaryOnlineSales> reportsummary = new ArrayList<SummaryOnlineSales>();
		for (int i = 0; i < objList.size(); i++) {
			Object[] objAray = (Object[]) objList.get(i);
			SummaryOnlineSales summary = new SummaryOnlineSales();
			summary.setSalesMode((String) objAray[0]);
			summary.setQty((Double) objAray[1]);
			summary.setAmount((Double) objAray[2]);
			reportsummary.add(summary);
		}

		return reportsummary;

	}

	public void SalesStockUpdate(String voucherNumber, Date voucherDate, SalesTransHdr salesTransHdr, String STORE) {

		List<ItemBatchDtl> itemBatchDtlList = itemBatchDtlRepository.findBySourceParentId(salesTransHdr.getId());
		if (itemBatchDtlList.size() > 0) {

			itemBatchDtlRepository.deleteBySourceParentId(salesTransHdr.getId());
		}

		List<SalesDtl> salesDtlList = salesDetailsRepo.findBySalesTransHdrId(salesTransHdr.getId());

		Iterator iter = salesDtlList.iterator();
		while (iter.hasNext()) {

			SalesDtl salesDtl = (SalesDtl) iter.next();
			// Check stock from itembatchDtl

//			
			KitDefinitionMst kitDefinitionMst = null;
			List<KitDefinitionMst> kitDefinitionMstList = kitDefinitionMstRepo.findByItemId(salesDtl.getItemId());
			if (kitDefinitionMstList.size() > 0) {
				kitDefinitionMst = kitDefinitionMstList.get(0);
			}

			if (null != kitDefinitionMst) {

				List<Object> searchItemStockByName = itemStockPopUpRepository
						.findSearchbyItemName(salesDtl.getItemName(), salesDtl.getBatch(), topFifty);
				Double chkQty = 0.0;
				for (int i = 0; i < searchItemStockByName.size(); i++) {

					Object[] objArray = (Object[]) searchItemStockByName.get(i);
					chkQty = (Double) objArray[4];

				}
				if (chkQty < 1 || chkQty < salesDtl.getQty()) {
					Double diffQty = null;
					if (chkQty < salesDtl.getQty()) {
						diffQty = salesDtl.getQty() - chkQty;
					} else {
						diffQty = salesDtl.getQty();
					}
					runTimeProduction(salesDtl, diffQty);
					executebatchwiserowmaterialupdate(salesDtl.getSalesTransHdr().getVoucherNumber(),
							salesDtl.getSalesTransHdr().getVoucherDate(), salesDtl.getCompanyMst(), salesDtl);

				}
			}
//			

			Optional<ItemMst> itemOpt = itemMstRepository.findById(salesDtl.getItemId());
			if (!itemOpt.isPresent()) {
				return;
			}

			ItemMst item = itemOpt.get();
			if (null != item.getServiceOrGoods()) {
				if (!item.getServiceOrGoods().equalsIgnoreCase("SERVICE ITEM")) {
					// ----this code execute for service item
//					List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
//							.findByItemIdAndBatchAndBarcode(salesDtl.getItemId(), salesDtl.getBatch(),
//									salesDtl.getBarcode());
					double conversionQty = 0.0;
					Double qty = 0.0;
					if (!item.getUnitId().equalsIgnoreCase(salesDtl.getUnitId())) {
						conversionQty = getConvertionQty(salesTransHdr.getCompanyMst().getId(), item.getId(),
								salesDtl.getUnitId(), item.getUnitId(), salesDtl.getQty());

						qty = -1 * conversionQty;
					} else {
						qty = -1 * salesDtl.getQty();
					}

					String processInstanceId = null;
					String taskId = null;

					String store = null;
					if (null != salesDtl.getStore()) {
						store = salesDtl.getStore();
					} else {
						store = "MAIN";
					}

					itemBatchDtlService.itemBatchMstUpdation(salesDtl.getBarcode(), salesDtl.getBatch(),
							salesTransHdr.getBranchCode(), salesDtl.getExpiryDate(), salesDtl.getItemId(),
							salesDtl.getMrp(), processInstanceId, qty, taskId, salesTransHdr.getCompanyMst(), store);

					String particular = "SALES " + salesTransHdr.getAccountHeads().getAccountName();
					Double QtyIn = 0.0;
					final VoucherNumber voucherNo = voucherService.generateInvoice("STV",
							salesTransHdr.getCompanyMst().getId());

					itemBatchDtlService.itemBatchDtlInsertionNew(salesDtl.getBarcode(), salesDtl.getBatch(),
							salesTransHdr.getBranchCode(), salesDtl.getExpiryDate(), salesDtl.getItemId(),
							salesDtl.getMrp(), particular, processInstanceId, QtyIn, salesDtl.getQty(),
							salesDtl.getId(), salesTransHdr.getId(), voucherDate, voucherNumber, store, taskId,
							voucherDate, voucherNo.getCode(), salesTransHdr.getCompanyMst());
				}
			}

		}

	}

	public void executebatchwiserowmaterialupdate(String voucherNumber, Date voucherDate, CompanyMst companyMst,
			SalesDtl salesDtl) {

//		Optional<ActualProductionHdr> actualProductionHdrOpt = actualProductionHdrRepository.findById(actualProductionHdrId);
//		
//		
//		List<ActualProductionDtl> actualProductionDtlList = actualProductionDtlRepository.findByActualProductionHdrId(actualProductionHdrOpt.get().getId());
//		
//		Iterator iter = actualProductionDtlList.iterator();
//		while (iter.hasNext()) {
//			ActualProductionDtl actualProductionDtl = (ActualProductionDtl) iter.next();
//			
//			Double qty = actualProductionDtl.getActualQty() ;
//			String itemId = actualProductionDtl.getItemId();
//			
//			/*
//			 * Find Kit Header
//			 */

		KitDefinitionMst kitDefinitionMst = null;
		List<KitDefinitionMst> kitDefinitionMstList = kitDefinitionMstRepo.findByItemId(salesDtl.getItemId());
		if (kitDefinitionMstList.size() > 0) {
			kitDefinitionMst = kitDefinitionMstList.get(0);
		}
		Double defenitionQty = kitDefinitionMst.getMinimumQty();

		/*
		 * Find Kit Dtl - Raw Materil
		 */

		List<KitDefenitionDtl> kitDefenitionDtlList = kitDefenitionDtlRepo
				.findBykitDefenitionmstId(kitDefinitionMst.getId());

		Iterator iterRM = kitDefenitionDtlList.iterator();

		while (iterRM.hasNext()) {

			KitDefenitionDtl kitDefenitionDtl = (KitDefenitionDtl) iterRM.next();

			String rawMaterialId = kitDefenitionDtl.getItemId();
			Double rawMaterialUnitQty = kitDefenitionDtl.getQty();
			if (null != rawMaterialId) {
				Double requiredRawMaterial = rawMaterialUnitQty * salesDtl.getQty() / defenitionQty;

				System.out.println(rawMaterialId);

				Optional<ItemMst> itemopt = itemMstRepository.findById(rawMaterialId);

				if (!itemopt.isPresent()) {
					System.out.println(rawMaterialId + " Not Present");
					continue;
				}

				ItemMst rmItem = itemopt.get();

				List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
						.findByItemIdAndBatchAndBarcode(rawMaterialId, MapleConstants.Nobatch, rmItem.getBarCode());

				if (itembatchmst.size() == 1) {
					ItemBatchMst itemBatchMst = itembatchmst.get(0);
					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
					itemBatchMst.setQty(itemBatchMst.getQty() - requiredRawMaterial);
					itemBatchMst.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());
					itemBatchMstRepo.saveAndFlush(itemBatchMst);
				} else {
					ItemBatchMst itemBatchMst = new ItemBatchMst();
					itemBatchMst.setBarcode(itemopt.get().getBarCode());
					itemBatchMst.setBatch(MapleConstants.Nobatch);
					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
					itemBatchMst.setQty(-1 * requiredRawMaterial);
					itemBatchMst.setItemId(rawMaterialId);
					itemBatchMst.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());
					itemBatchMst.setCompanyMst(companyMst);
					itemBatchMstRepo.saveAndFlush(itemBatchMst);

				}

				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
				final VoucherNumber voucherNo = voucherService.generateInvoice("STV", companyMst.getId());
				itemBatchDtl.setBarcode(itemopt.get().getBarCode());
				itemBatchDtl.setBatch(MapleConstants.Nobatch);
				itemBatchDtl.setItemId(rawMaterialId);
				itemBatchDtl.setMrp(itemopt.get().getStandardPrice());
				itemBatchDtl.setQtyIn(0.0);
				itemBatchDtl.setParticulars("SALES KOT-" + salesDtl.getItemName());
				itemBatchDtl.setStore("MAIN");
				itemBatchDtl.setQtyOut(requiredRawMaterial);
				itemBatchDtl.setVoucherNumber(voucherNo.getCode());
				itemBatchDtl.setVoucherDate(salesDtl.getSalesTransHdr().getVoucherDate());
				itemBatchDtl.setItemId(rawMaterialId);
				itemBatchDtl.setSourceVoucherNumber(salesDtl.getSalesTransHdr().getVoucherNumber());
				itemBatchDtl.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());
				itemBatchDtl.setCompanyMst(companyMst);
				itemBatchDtl.setSourceParentId(salesDtl.getSalesTransHdr().getId());
				itemBatchDtl.setSourceDtlId(salesDtl.getId());
				itemBatchDtl = itemBatchDtlRepository.saveAndFlush(itemBatchDtl);
				Map<String, Object> variables = new HashMap<String, Object>();

				variables.put("voucherNumber", itemBatchDtl.getId());
				variables.put("id", itemBatchDtl.getId());
				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("inet", 0);
				variables.put("REST", 1);
				variables.put("companyid", itemBatchDtl.getCompanyMst());
				variables.put("WF", "forwardItemBatchDtl");

				LmsQueueMst lmsQueueMst = new LmsQueueMst();

				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

				java.util.Date uDate = (Date) variables.get("voucherDate");
				java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(uDate);

				uDate = SystemSetting.SqlDateToUtilDate(sqlDate);
				lmsQueueMst.setVoucherDate(sqlDate);

				// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
				lmsQueueMst.setVoucherType("forwardItemBatchDtl");
				lmsQueueMst.setPostedToServer("NO");
				lmsQueueMst.setJobClass("forwardItemBatchDtl");
				lmsQueueMst.setCronJob(true);
				lmsQueueMst.setJobName("forwardItemBatchDtl" + itemBatchDtl.getId());
				lmsQueueMst.setJobGroup("forwardItemBatchDtl");
				lmsQueueMst.setRepeatTime(60000L);
				lmsQueueMst.setSourceObjectId(itemBatchDtl.getId());

				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
				variables.put("lmsqid", lmsQueueMst.getId());

				eventBus.post(variables);

			}

		}
	}

	private void runTimeProduction(SalesDtl salesDtl, Double diffQty) {
		KitDefinitionMst kitDefinitionMst = null;
		List<KitDefinitionMst> kitDefinitionMstList = kitDefinitionMstRepo.findByItemId(salesDtl.getItemId());
		if (kitDefinitionMstList.size() > 0) {
			kitDefinitionMst = kitDefinitionMstList.get(0);
		}
		Optional<ItemMst> itemopt = itemMstRepository.findById(salesDtl.getItemId());
		{
			if (null != kitDefinitionMst) {
				List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo.findByItemIdAndBatchAndBarcode(
						salesDtl.getItemId(), salesDtl.getBatch(), salesDtl.getBarcode());

				if (itembatchmst.size() == 1) {
					ItemBatchMst itemBatchMst = itembatchmst.get(0);
					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
					itemBatchMst.setQty(itemBatchMst.getQty() + diffQty);
					itemBatchMstRepo.saveAndFlush(itemBatchMst);

				} else {
					ItemBatchMst itemBatchMst = new ItemBatchMst();
					itemBatchMst.setBarcode(itemopt.get().getBarCode());
					if (salesDtl.getBatch() != null) {
						itemBatchMst.setBatch(salesDtl.getBatch());
					} else {
						itemBatchMst.setBatch(MapleConstants.Nobatch);
					}

					itemBatchMst.setMrp(itemopt.get().getStandardPrice());
					itemBatchMst.setQty(diffQty);
					itemBatchMst.setItemId(salesDtl.getItemId());
					itemBatchMst.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());
					itemBatchMst.setCompanyMst(salesDtl.getSalesTransHdr().getCompanyMst());
					itemBatchMstRepo.saveAndFlush(itemBatchMst);

				}

				ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
				final VoucherNumber voucherNo = voucherService.generateInvoice("STV",
						salesDtl.getSalesTransHdr().getCompanyMst().getId());
				itemBatchDtl.setBarcode(itemopt.get().getBarCode());
				if (salesDtl.getBatch() != null) {
					itemBatchDtl.setBatch(salesDtl.getBatch());
				} else {
					itemBatchDtl.setBatch(MapleConstants.Nobatch);
				}
				itemBatchDtl.setItemId(salesDtl.getItemId());
				itemBatchDtl.setMrp(itemopt.get().getStandardPrice());
				itemBatchDtl.setQtyIn(diffQty);
				itemBatchDtl.setQtyOut(0.0);
				itemBatchDtl.setStore("MAIN");
				itemBatchDtl.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());
				itemBatchDtl.setVoucherNumber(voucherNo.getCode());
				itemBatchDtl.setVoucherDate(salesDtl.getSalesTransHdr().getVoucherDate());
				itemBatchDtl.setItemId(salesDtl.getItemId());
				itemBatchDtl.setSourceVoucherNumber(salesDtl.getSalesTransHdr().getVoucherNumber());
				itemBatchDtl.setParticulars("SALES PRODUCTION");
				itemBatchDtl.setStore("MAIN");
				itemBatchDtl.setSourceParentId(salesDtl.getSalesTransHdr().getId());
				itemBatchDtl.setSourceDtlId(salesDtl.getId());
				itemBatchDtl.setCompanyMst(salesDtl.getSalesTransHdr().getCompanyMst());
				itemBatchDtl = itemBatchDtlRepository.saveAndFlush(itemBatchDtl);
				Map<String, Object> variables = new HashMap<String, Object>();

				variables.put("voucherNumber", itemBatchDtl.getId());
				variables.put("id", itemBatchDtl.getId());
				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("inet", 0);
				variables.put("REST", 1);
				variables.put("companyid", itemBatchDtl.getCompanyMst());
				variables.put("WF", "forwardItemBatchDtl");

				LmsQueueMst lmsQueueMst = new LmsQueueMst();

				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

				java.util.Date uDate = (Date) variables.get("voucherDate");
				java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(uDate);

				uDate = SystemSetting.SqlDateToUtilDate(sqlDate);
				lmsQueueMst.setVoucherDate(sqlDate);

				// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
				lmsQueueMst.setVoucherType("forwardItemBatchDtl");
				lmsQueueMst.setPostedToServer("NO");
				lmsQueueMst.setJobClass("forwardItemBatchDtl");
				lmsQueueMst.setCronJob(true);
				lmsQueueMst.setJobName("forwardItemBatchDtl" + itemBatchDtl.getId());
				lmsQueueMst.setJobGroup("forwardItemBatchDtl");
				lmsQueueMst.setRepeatTime(60000L);
				lmsQueueMst.setSourceObjectId(itemBatchDtl.getId());

				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
				variables.put("lmsqid", lmsQueueMst.getId());

				eventBus.post(variables);

			}

		}
	}

	private double getConvertionQty(String companyMstId, String itemId, String sourceUnit, String targetUnit,
			double sourceQty) {
		if (recurssionOff) {
			return sourceQty;
		}
		MultiUnitMst multiUnitMstList = multiUnitMstRepository.findByCompanyMstIdAndItemIdAndUnit1(companyMstId, itemId,
				sourceUnit);

		while (!multiUnitMstList.getUnit2().equalsIgnoreCase(targetUnit)) {

			if (recurssionOff) {
				break;
			}
			sourceUnit = multiUnitMstList.getUnit2();

			sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();

			getConvertionQty(companyMstId, itemId, sourceUnit, targetUnit, sourceQty);

		}
		sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();
		recurssionOff = true;
		// sourceQty = sourceQty *
		// multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
		return sourceQty;
	}

	@GetMapping("{companymstid}/getvouchernumber/{voucherType}/salestype")
	public List<Object> retriveVoucherNumber(@PathVariable(value = "voucherType") String voucherType,
			@RequestParam("voucherdate") String voucherdate,
			@PathVariable(value = "companymstid") String companymstid) {

		java.util.Date date = SystemSetting.StringToUtilDate(voucherdate, "dd-MM-yyyy");
		return salesTransHdrService.getVoucherNumber(companymstid, date, voucherType);

	}

	@GetMapping("{companymstid}/saleshdrByvouchernumber/{vouchernumber}/saleshdr")
	public List<SalesTransHdr> getSalesHdrByVoucherNumber(@PathVariable(value = "vouchernumber") String vouchernumber,
			@RequestParam("date") String date, @PathVariable(value = "companymstid") String companymstid) {
		java.util.Date vdate = SystemSetting.StringToUtilDate(date, "dd-MM-yyyy");
		return salesTransHdrService.findByVoucherNumberAndVoucherDate(companymstid, vouchernumber, vdate);

	}

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/{salesman}/{tablename}/printkot")
	public List<SalesTransHdr> printKotByTablenameAndSalesman(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("salesman") String salesman,
			@PathVariable("tablename") String tablename) {
		return salesTransHdrService.printKotByTablenameAndSalesman(salesman, tablename);

	}

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/takeorder/printkotinvoice/{hdrid}")
	public List<SalesTransHdr> printKotByHdrId(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("hdrid") String hdrid) {
		return salesTransHdrService.printKotBySalesTransHdr(hdrid);

	}

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/{salestranshdrid}/printkot")
	public List<SalesTransHdr> printKotBySalesTransHdr(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("salestranshdrid") String tablename) {
		return salesTransHdrService.printKotBySalesTransHdr(tablename);

	}

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/{salesman}/{tablename}/printkotinvoice")
	public List<SalesTransHdr> printKotInvoiceByTablenameAndSalesman(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("salesman") String salesman,
			@PathVariable("tablename") String tablename) {
		return salesTransHdrService.printKotInvoiceByTablenameAndSalesman(salesman, tablename);

	}

	@GetMapping("{companymstid}/{branchcode}/getplacedtable/{customiseSalesMode}")
	public List<TableWaiterReport> retrivePalcedTable(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode,
			@PathVariable("customiseSalesMode") String customiseSalesMode) {
		return salesTransHdrService.retrivePalcedTable(companymstid, branchcode, customiseSalesMode);
	}

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/getcustomerandvouchernumber")
	public List<Object> getCustomerAndVouchernumber(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("vDate") String vDate) {

		Date date = SystemSetting.StringToUtilDate(vDate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		return salesTransHdrRepo.getSalesCustomerAndVouchernumber(branchcode, companyMst.get(), date);

	}

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/getholdedsales")
	public List<SalesTransHdr> getHoldedSales(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode) {

		return salesTransHdrRepo.getHoldedSales(companymstid, branchcode);

	}

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/getsalestranshdrbyvoucher/{voucherno}")
	public SalesTransHdr getSalesTransByVoucherNo(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("voucherno") String voucherno) {

		return salesTransHdrRepo.findByVoucherNumberAndBranchCode(voucherno, branchcode);

	}

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/getholdedpossales")
	public List<SalesTransHdr> getHoldedPOSSales(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode) {

		return salesTransHdrRepo.getHoldedPOSSales(companymstid, branchcode);

	}

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/getholdedpossalesbyuser/{userid}")
	public List<SalesTransHdr> getHoldedPOSSalesByUser(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("userid") String userid) {

		return salesTransHdrRepo.getHoldedPOSSalesByUser(companymstid, branchcode, userid);

	}

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/getholdedtakeordersales")
	public List<SalesTransHdr> getHoldedTakeoderSales(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode) {

		return salesTransHdrRepo.getHoldedTakeOrderSales(companymstid, branchcode);

	}

//---------------------------------- OFFER ----------------------------------------------------------------

	/*
	 * Parameter Store is introduced on Dec 20, 2020, by Regy This is to support
	 * Ambrosia additional counter at partking slot, with Store management.
	 * 
	 * This code is no more used . Dec 9 , 2021 
	 * @remove
	 */
	@PutMapping("{companymstid}/salestranshdrfinalsaveoffer/{salestranshdrid}/{store}")
	public SalesTransHdr SalesFinalSaveOffer(

			@PathVariable("salestranshdrid") String salestranshdrid, @PathVariable("companymstid") String companymstid,
			@PathVariable("store") String store, @RequestParam("logdate") String logdate,
			@Valid @RequestBody SalesTransHdr salesTransHdrRequest) {

		Date logDate = SystemSetting.StringToUtilDate(logdate, "dd-MM-yyyy");

		return salesTransHdrRepo.findById(salestranshdrid).map(salestranshdr -> {

			salesTransHdrRequest.setCompanyMst(salestranshdr.getCompanyMst());

			if (null == salesTransHdrRequest.getVoucherNumber()) {

				if (null != salesTransHdrRequest.getInvoiceNumberPrefix()) {
					String voucherNoPreFix = null;
					voucherNoPreFix = SystemSetting.getFinancialYear() + salesTransHdrRequest.getInvoiceNumberPrefix();
					String vcNo = "";

					VoucherNumber voucherNo = voucherService.generateInvoice(voucherNoPreFix,
							salestranshdr.getCompanyMst().getId());
					
					String invoice[] = voucherNo.getCode().split(voucherNoPreFix);

					salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));
					salestranshdr.setNumericVoucherNumber(Long.parseLong(invoice[1]));


					if (voucherNoYearPrefix.equalsIgnoreCase("NO")) {
						vcNo = voucherNo.getCode().substring(9);
					} else {
						vcNo = voucherNo.getCode();
					}
					
					


					if (salesTransHdrRequest.getInvoiceNumberPrefix().equalsIgnoreCase("dd-MM")) {

						vcNo = vcNo.replace(SystemSetting.getFinancialYear(), "");

						vcNo = vcNo.replace(salesTransHdrRequest.getInvoiceNumberPrefix(), "");
						vcNo = vcNo.replace(salestranshdr.getCompanyMst().getId(), "");

						System.out.println("vcNo =" + vcNo);

						vcNo = SystemSetting.SqlDateTostring(salesTransHdrRequest.getVoucherDate(), "dd-MM") + "-"
								+ vcNo;
						System.out.println("vcNo =" + vcNo);
					} else if (salesTransHdrRequest.getInvoiceNumberPrefix().equalsIgnoreCase("MM-dd")) {
						vcNo = vcNo.replace(SystemSetting.getFinancialYear(), "");

						vcNo = vcNo.replace(salesTransHdrRequest.getInvoiceNumberPrefix(), "");
						vcNo = vcNo.replace(salestranshdr.getCompanyMst().getId(), "");

						System.out.println("vcNo =" + vcNo);

						vcNo = SystemSetting.SqlDateTostring(salesTransHdrRequest.getVoucherDate(), "MM-dd") + "-"
								+ vcNo;
						System.out.println("vcNo =" + vcNo);
					}
					salesTransHdrRequest.setVoucherNumber(vcNo);
				} else {
					String voucherNoPreFixPos = null;
					String vNo = null;

					String finYear = SystemSetting.getFinancialYear();

					VoucherNumber voucherNo = voucherService.generateInvoice(
							SystemSetting.getFinancialYear() + POS_SALES_PREFIX, salestranshdr.getCompanyMst().getId());

					if (voucherNoYearPrefix.equalsIgnoreCase("NO")) {
						vNo = voucherNo.getCode().substring(9);
					} else {
						vNo = voucherNo.getCode();
					}
					salesTransHdrRequest.setVoucherNumber(vNo);

					String invoice[] = salesTransHdrRequest.getVoucherNumber().split(POS_SALES_PREFIX);
					salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));

				}

			}
			if (null != salesTransHdrRequest.getEditedStatus()) {
				salestranshdr.setEditedStatus(salesTransHdrRequest.getEditedStatus());
			}
			salestranshdr.setLocalCustomerMst(salesTransHdrRequest.getLocalCustomerMst());

			salestranshdr.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			// salestranshdr.setNumericVoucherNumber(salesTransHdrRequest.getNumericVoucherNumber());
			salestranshdr.setCustomerId(salesTransHdrRequest.getCustomerId());
			salestranshdr.setCardamount(salesTransHdrRequest.getCardamount());
			salestranshdr.setCardNo(salesTransHdrRequest.getCardNo());
			salestranshdr.setCashPay(salesTransHdrRequest.getCashPay());
			salestranshdr.setCreditOrCash(salesTransHdrRequest.getCreditOrCash());
			salestranshdr.setDeliveryBoyId(salesTransHdrRequest.getDeliveryBoyId());
			salestranshdr.setInvoiceAmount(salesTransHdrRequest.getInvoiceAmount());
			salestranshdr.setIsBranchSales(salesTransHdrRequest.getBranchCode());
			salestranshdr.setMachineId(salesTransHdrRequest.getMachineId());
			salestranshdr.setPaidAmount(salesTransHdrRequest.getPaidAmount());
			salestranshdr.setSalesManId(salesTransHdrRequest.getSalesManId());
			salestranshdr.setSalesMode(salesTransHdrRequest.getSalesMode());
			salestranshdr.setCurrencyId(salesTransHdrRequest.getCurrencyId());
			salestranshdr.setServingTableName(salesTransHdrRequest.getServingTableName());
			// salestranshdr.setBranchCode(salesTransHdrRequest.getBranchCode());

			salestranshdr.setAccountHeads(salesTransHdrRequest.getAccountHeads());

			Calendar calendar = Calendar.getInstance();
			java.util.Date currentDate = calendar.getTime();
			java.sql.Date date = new java.sql.Date(currentDate.getTime());

			String strDate = SystemSetting.SqlDateTostring(date);

			date = SystemSetting.StringToSqlDate(strDate, "dd/MM/yyyy");

			if (null == salesTransHdrRequest.getVoucherDate()) {
				salesTransHdrRequest.setVoucherDate(date);
			}

			// salesTransHdrRequest.setVoucherDate(salesTransHdrRequest.getVoucherDate());
			salesTransHdrRequest.setCompanyMst(salestranshdr.getCompanyMst());

			salesTransHdrRequest.setSourceIP(salestranshdr.getSourceIP());
			salesTransHdrRequest.setSourcePort(salestranshdr.getSourcePort());

			if (null != salestranshdr.getBranchCode()) {
				salesTransHdrRequest.setBranchCode(salestranshdr.getBranchCode());
			}

			if (null != salesTransHdrRequest.getInvoiceNumberPrefix()) {
				String invoice[] = salesTransHdrRequest.getVoucherNumber()
						.split(salesTransHdrRequest.getInvoiceNumberPrefix());
				if (invoice.length < 1) {
					salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));
				}
			}

			salestranshdr.setInvoicePrintType(salesTransHdrRequest.getInvoicePrintType());

			salestranshdr.setFcInvoiceAmount(salesTransHdrRequest.getFcInvoiceAmount());
			salestranshdr.setCurrencyType(salesTransHdrRequest.getCurrencyType());
			salestranshdr.setDoctorId(salesTransHdrRequest.getDoctorId());
			salestranshdr.setCurrencyConversionRate(salesTransHdrRequest.getCurrencyConversionRate());
//====================================================================================================================	

			salestranshdr.setPoNumber(salesTransHdrRequest.getPoNumber());
//====================================================================================================================	
			//salestranshdr = salesTransHdrRepo.saveAndFlush(salesTransHdrRequest);
			
			salestranshdr = saveAndPublishService.saveSalesTransHdr(salesTransHdrRequest,salesTransHdrRequest.getBranchCode());
			
//			if(null!=salestranshdr)
//			{
//				saveAndPublishService.salesFinalSaveToKafkaEvent(salestranshdr, salestranshdr.getBranchCode(), KafkaMapleEventType.SERVER);
//				
//			List<SalesReceipts> salesReceiptsList = salesReceiptsRepo.findBySalesTransHdr(salestranshdr);
//			for (SalesReceipts salesReceipts:salesReceiptsList)
//			{
//			saveAndPublishService.publishObjectToKafkaEvent(salesReceipts,  salestranshdr.getBranchCode(), 
//			KafkaMapleEventType.SALESRECEIPTS, KafkaMapleEventType.SERVER);
//			}
//			List<AccountReceivable> accountReceivableList = accountReceivableRepository.findByCompanyMstAndSalesTransHdr(salestranshdr.getCompanyMst(), salestranshdr);
//			for (AccountReceivable accountReceivable:accountReceivableList)
//			{
//			saveAndPublishService.publishObjectToKafkaEvent(accountReceivable, salestranshdr.getBranchCode(), KafkaMapleEventType.ACCOUNTRECEIVABLE, KafkaMapleEventType.SERVER);
//			}
//			}

			// ===============updating sales hdr to cloud==============by
			// anandu===============05-08-2021=======
//			externalApi.salesTransHdrUpdatedToCloud(companymstid, salestranshdr);

			// Boolean offer = CheckOfferWhileFinalSave(salestranshdr, logDate);

			Boolean offer = schemeService.CheckOfferWhileFinalSave(salesTransHdrRequest, logDate);

			System.out.print(
					"inside the offer isssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");

			/*
			 * mange Sales receipts
			 */
			String SalesMode = salestranshdr.getSalesMode();

			Double invoiceAmount = salestranshdr.getInvoiceAmount();
			Double cashPaidAmount = salestranshdr.getCashPay();
			Double cardAmount = salestranshdr.getCardamount();
			Double sodexoAmount = salestranshdr.getSodexoAmount();
			Double creditAmount = salestranshdr.getCreditAmount();

			List<SalesReceipts> salesReceiptsList = salesReceiptsRepo.findByCompanyMstIdAndSalesTransHdrId(companymstid,
					salestranshdr.getId());

			Iterator iter = salesReceiptsList.iterator();
			while (iter.hasNext()) {
				SalesReceipts salesReceipts = (SalesReceipts) iter.next();
				salesReceipts.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
				salesReceipts.setCompanyMst(salesTransHdrRequest.getCompanyMst());
				salesReceiptsRepo.saveAndFlush(salesReceipts);

			}

			List<AccountReceivable> accountReceivableList = accountReceivableRepository
					.findByCompanyMstAndSalesTransHdr(salestranshdr.getCompanyMst(), salestranshdr);

			if (accountReceivableList.size() > 0) {
				accountReceivable = accountReceivableList.get(0);
				if (null != accountReceivable) {
					accountReceivable.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
					java.sql.Date ldate = SystemSetting.UtilDateToSQLDate(salesTransHdrRequest.getVoucherDate());
					accountReceivable.setVoucherDate(ldate);
					accountReceivable = accountReceivableRepository.saveAndFlush(accountReceivable);

				}
			}
			/*
			 * Manage Stock
			 * 
			 */

			try {
				salesAccounting.execute(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(),
						salestranshdr.getId(), salestranshdr.getCompanyMst());
				if (null != salestranshdr.getCompanyMst().getCurrencyName()
						&& !salestranshdr.getCompanyMst().getCurrencyName().equalsIgnoreCase(null)) {
					CurrencyMst comCurrencyop = currencyMstRepo
							.findByCurrencyName(salestranshdr.getCompanyMst().getCurrencyName());
					if (null != salestranshdr.getAccountHeads().getCurrencyId()) {
						Optional<CurrencyMst> custCurrencyOp = currencyMstRepo
								.findById(salestranshdr.getAccountHeads().getCurrencyId());
						if (custCurrencyOp.isPresent()) {
							if (null != comCurrencyop) {
								if (!comCurrencyop.getId().equalsIgnoreCase(custCurrencyOp.get().getId())) {
									salesAccountingfc.execute(salestranshdr.getVoucherNumber(),
											salestranshdr.getVoucherDate(), salestranshdr.getId(),
											salestranshdr.getCompanyMst());
								}
							}
						}
					}
				}

			} catch (PartialAccountingException e) {

				logger.error(e.getMessage());
				return null;
			}

			salesDayEndReporting.execute(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(),
					salestranshdr.getCompanyMst(), salestranshdr.getId());

			/*
			 * Parameter store is introduced on Dec 20, 2020 by Regy
			 */
			SalesStockUpdate(salesTransHdrRequest.getVoucherNumber(), salesTransHdrRequest.getVoucherDate(),
					salestranshdr, store);

			/*
			 * Manage Accounting
			 * 
			 */
 
			return salestranshdr;
		}).orElseThrow(() -> new ResourceNotFoundException("salestranshdrid " + salestranshdrid + " not found"));

	}

	/*
	 * Dec 9 , 2021 Final Save url for sales
	 * 
	 */
	
	@PutMapping("{companymstid}/salestranshdrresource/salesfinalsavereturningbillentity/{salestranshdrid}/{store}")
	public SalesBillModelClass  SalesFinalSaveReturningBillEntity(

			@PathVariable("salestranshdrid") String salestranshdrid, @PathVariable("companymstid") String companymstid,
			@PathVariable("store") String store, @RequestParam("logdate") String logdate,
			@Valid @RequestBody SalesTransHdr salesTransHdrRequest) {

		
		if(null==store||store.length()==0) {
			store = MapleConstants.Store;
			
			
		}
		Date logDate = SystemSetting.StringToUtilDate(logdate, "dd-MM-yyyy");

		Optional<SalesTransHdr> salestranshdridOptional =  salesTransHdrRepo.findById(salestranshdrid)  ;
		/*
		 * Check for valid record and return if no record found
		 */
		SalesTransHdr salestranshdr = salestranshdridOptional.get();		
		salesTransHdrRequest.setCompanyMst(salestranshdr.getCompanyMst());
		     /*
			 * Check and set vouhcer number
			 */
			if (null == salesTransHdrRequest.getVoucherNumber()) {
			
				setVoucherNumber(salesTransHdrRequest);
			}
			/*
			 * Set Object fields
			 */
			setObjectFields(salestranshdr,salesTransHdrRequest);
			salestranshdr = saveAndPublishService.saveSalesTransHdr(salesTransHdrRequest,salesTransHdrRequest.getBranchCode());
			
			if(null!=salestranshdr)
			{
				saveAndPublishService.salesFinalSaveToKafkaEvent(salestranshdr, salestranshdr.getBranchCode(), KafkaMapleEventType.SERVER);
				
//				SalesReceipts salesReceipts = salesReceiptsRepo.findBySalesTransHdr(salestranshdr);
//				saveAndPublishService.publishObjectToKafkaEvent(saved,  salestranshdr.getBranchCode(), 
//						KafkaMapleEventType.SALESRECEIPTS, KafkaMapleEventType.SERVER);
			}
			
			
			Boolean offer = schemeService.CheckOfferWhileFinalSave(salesTransHdrRequest, logDate);
		 	/*
			 * mange Sales receipts
			 */
			manageSalesReceipts(salestranshdr,salesTransHdrRequest);
			/*
			 * Manage Accounting
			 */
			manageAccounting(salestranshdr);
			salesDayEndReporting.execute(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(),
					salestranshdr.getCompanyMst(), salestranshdr.getId());
			/*
			 * Parameter store is introduced on Dec 20, 2020 by Regy
			 */
			/*
			 * Manage Stock
			 */
			SalesStockUpdate(salesTransHdrRequest.getVoucherNumber(), salesTransHdrRequest.getVoucherDate(),
					salestranshdr, store);
			
			/*
			 * function for setting values on the entity "SalesBillModelClass"
			 */
			return salesTransHdrService.setValuesOnSalesBillModalClass(salestranshdr);
			
			 
	}
	 
	
	
	private void manageSalesReceipts(SalesTransHdr salestranshdr, @Valid SalesTransHdr salesTransHdrRequest) {
		
		String SalesMode = salestranshdr.getSalesMode();

		Double invoiceAmount = salestranshdr.getInvoiceAmount();
		Double cashPaidAmount = salestranshdr.getCashPay();
		Double cardAmount = salestranshdr.getCardamount();
		Double sodexoAmount = salestranshdr.getSodexoAmount();
		Double creditAmount = salestranshdr.getCreditAmount();

		List<SalesReceipts> salesReceiptsList = salesReceiptsRepo.findByCompanyMstIdAndSalesTransHdrId(
				salestranshdr.getCompanyMst().getId(),
				salestranshdr.getId());

		Iterator iter = salesReceiptsList.iterator();
		while (iter.hasNext()) {
			SalesReceipts salesReceipts = (SalesReceipts) iter.next();
			salesReceipts.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			salesReceipts.setCompanyMst(salesTransHdrRequest.getCompanyMst());
			salesReceiptsRepo.saveAndFlush(salesReceipts);

		}

		List<AccountReceivable> accountReceivableList = accountReceivableRepository
				.findByCompanyMstAndSalesTransHdr(salestranshdr.getCompanyMst(), salestranshdr);

		if (accountReceivableList.size() > 0) {
			accountReceivable = accountReceivableList.get(0);
			if (null != accountReceivable) {
				accountReceivable.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
				java.sql.Date ldate = SystemSetting.UtilDateToSQLDate(salesTransHdrRequest.getVoucherDate());
				accountReceivable.setVoucherDate(ldate);
				accountReceivable = accountReceivableRepository.saveAndFlush(accountReceivable);

			}
		}
		
	}

	private void manageAccounting(SalesTransHdr salestranshdr) {
		try {
			salesAccounting.execute(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(),
					salestranshdr.getId(), salestranshdr.getCompanyMst());
			if (null != salestranshdr.getCompanyMst().getCurrencyName()
					&& !salestranshdr.getCompanyMst().getCurrencyName().equalsIgnoreCase(null)) {
				CurrencyMst comCurrencyop = currencyMstRepo
						.findByCurrencyName(salestranshdr.getCompanyMst().getCurrencyName());
				if (null != salestranshdr.getAccountHeads().getCurrencyId()) {
					Optional<CurrencyMst> custCurrencyOp = currencyMstRepo
							.findById(salestranshdr.getAccountHeads().getCurrencyId());
					if (custCurrencyOp.isPresent()) {
						if (null != comCurrencyop) {
							if (!comCurrencyop.getId().equalsIgnoreCase(custCurrencyOp.get().getId())) {
								salesAccountingfc.execute(salestranshdr.getVoucherNumber(),
										salestranshdr.getVoucherDate(), salestranshdr.getId(),
										salestranshdr.getCompanyMst());
							}
						}
					}
				}
			}

		} catch (PartialAccountingException e) {

			logger.error(e.getMessage());
			 
		}

		
	}

	private void setObjectFields(SalesTransHdr salestranshdr,SalesTransHdr salesTransHdrRequest) {
		if (null != salesTransHdrRequest.getEditedStatus()) {
			salestranshdr.setEditedStatus(salesTransHdrRequest.getEditedStatus());
		}
		salestranshdr.setLocalCustomerMst(salesTransHdrRequest.getLocalCustomerMst());

		salestranshdr.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
		// salestranshdr.setNumericVoucherNumber(salesTransHdrRequest.getNumericVoucherNumber());
		salestranshdr.setCustomerId(salesTransHdrRequest.getCustomerId());
		salestranshdr.setCardamount(salesTransHdrRequest.getCardamount());
		salestranshdr.setCardNo(salesTransHdrRequest.getCardNo());
		salestranshdr.setCashPay(salesTransHdrRequest.getCashPay());
		salestranshdr.setCreditOrCash(salesTransHdrRequest.getCreditOrCash());
		salestranshdr.setDeliveryBoyId(salesTransHdrRequest.getDeliveryBoyId());
		salestranshdr.setInvoiceAmount(salesTransHdrRequest.getInvoiceAmount());
		salestranshdr.setIsBranchSales(salesTransHdrRequest.getBranchCode());
		salestranshdr.setMachineId(salesTransHdrRequest.getMachineId());
		salestranshdr.setPaidAmount(salesTransHdrRequest.getPaidAmount());
		salestranshdr.setSalesManId(salesTransHdrRequest.getSalesManId());
		salestranshdr.setSalesMode(salesTransHdrRequest.getSalesMode());
		salestranshdr.setCurrencyId(salesTransHdrRequest.getCurrencyId());
		salestranshdr.setServingTableName(salesTransHdrRequest.getServingTableName());
		// salestranshdr.setBranchCode(salesTransHdrRequest.getBranchCode());

		salestranshdr.setAccountHeads(salesTransHdrRequest.getAccountHeads());

		Calendar calendar = Calendar.getInstance();
		java.util.Date currentDate = calendar.getTime();
		java.sql.Date date = new java.sql.Date(currentDate.getTime());

		String strDate = SystemSetting.SqlDateTostring(date);

		date = SystemSetting.StringToSqlDate(strDate, "dd/MM/yyyy");

		if (null == salesTransHdrRequest.getVoucherDate()) {
			salesTransHdrRequest.setVoucherDate(date);
		}

		// salesTransHdrRequest.setVoucherDate(salesTransHdrRequest.getVoucherDate());
		salesTransHdrRequest.setCompanyMst(salestranshdr.getCompanyMst());

		salesTransHdrRequest.setSourceIP(salestranshdr.getSourceIP());
		salesTransHdrRequest.setSourcePort(salestranshdr.getSourcePort());

		if (null != salestranshdr.getBranchCode()) {
			salesTransHdrRequest.setBranchCode(salestranshdr.getBranchCode());
		}

		if (null != salesTransHdrRequest.getInvoiceNumberPrefix()) {
			String invoice[] = salesTransHdrRequest.getVoucherNumber()
					.split(salesTransHdrRequest.getInvoiceNumberPrefix());
			if (invoice.length < 1) {
				salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));
			}
		}

		salestranshdr.setInvoicePrintType(salesTransHdrRequest.getInvoicePrintType());

		salestranshdr.setFcInvoiceAmount(salesTransHdrRequest.getFcInvoiceAmount());
		salestranshdr.setCurrencyType(salesTransHdrRequest.getCurrencyType());
		salestranshdr.setDoctorId(salesTransHdrRequest.getDoctorId());
		salestranshdr.setCurrencyConversionRate(salesTransHdrRequest.getCurrencyConversionRate());
//====================================================================================================================	

		salestranshdr.setPoNumber(salesTransHdrRequest.getPoNumber());
//====================================================================================================================	
		//salestranshdr = salesTransHdrRepo.saveAndFlush(salesTransHdrRequest);
		
		
		
	}

	private void setVoucherNumber(@Valid SalesTransHdr salesTransHdrRequest) {

		if (null != salesTransHdrRequest.getInvoiceNumberPrefix()) {
			String voucherNoPreFix = null;
			voucherNoPreFix = SystemSetting.getFinancialYear() + salesTransHdrRequest.getInvoiceNumberPrefix();
			String vcNo = "";

			VoucherNumber voucherNo = voucherService.generateInvoice(voucherNoPreFix,
					salesTransHdrRequest.getCompanyMst().getId());
			
			String invoice[] = voucherNo.getCode().split(voucherNoPreFix);

			salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));
			salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));


			if (voucherNoYearPrefix.equalsIgnoreCase("NO")) {
				vcNo = voucherNo.getCode().substring(9);
			} else {
				vcNo = voucherNo.getCode();
			}
			
			


			if (salesTransHdrRequest.getInvoiceNumberPrefix().equalsIgnoreCase("dd-MM")) {

				vcNo = vcNo.replace(SystemSetting.getFinancialYear(), "");

				vcNo = vcNo.replace(salesTransHdrRequest.getInvoiceNumberPrefix(), "");
				vcNo = vcNo.replace(salesTransHdrRequest.getCompanyMst().getId(), "");

				System.out.println("vcNo =" + vcNo);

				vcNo = SystemSetting.SqlDateTostring(salesTransHdrRequest.getVoucherDate(), "dd-MM") + "-"
						+ vcNo;
				System.out.println("vcNo =" + vcNo);
			} else if (salesTransHdrRequest.getInvoiceNumberPrefix().equalsIgnoreCase("MM-dd")) {
				vcNo = vcNo.replace(SystemSetting.getFinancialYear(), "");

				vcNo = vcNo.replace(salesTransHdrRequest.getInvoiceNumberPrefix(), "");
				vcNo = vcNo.replace(salesTransHdrRequest.getCompanyMst().getId(), "");

				System.out.println("vcNo =" + vcNo);

				vcNo = SystemSetting.SqlDateTostring(salesTransHdrRequest.getVoucherDate(), "MM-dd") + "-"
						+ vcNo;
				System.out.println("vcNo =" + vcNo);
			}
			salesTransHdrRequest.setVoucherNumber(vcNo);
		} else {
			String voucherNoPreFixPos = null;
			String vNo = null;

			String finYear = SystemSetting.getFinancialYear();

			VoucherNumber voucherNo = voucherService.generateInvoice(
					SystemSetting.getFinancialYear() + POS_SALES_PREFIX, salesTransHdrRequest.getCompanyMst().getId());

			if (voucherNoYearPrefix.equalsIgnoreCase("NO")) {
				vNo = voucherNo.getCode().substring(9);
			} else {
				vNo = voucherNo.getCode();
			}
			salesTransHdrRequest.setVoucherNumber(vNo);

			String invoice[] = salesTransHdrRequest.getVoucherNumber().split(POS_SALES_PREFIX);
			salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));

		}

		
	}

	private Boolean CheckOfferWhileFinalSave(SalesTransHdr salesTransHdr, Date loginDate) {

		String salesMode = null;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("POS")) {

			salesMode = "Retail";

		}
		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2B")
				|| salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			salesMode = "WholeSale";
		}
		if (null == salesMode) {
			return false;
		}

		List<SchemeInstance> schemeInstanceList = schemeInstanceRepository
				.findByCompanyMstAndSchemeWholesaleRetailAndIsActive(salesTransHdr.getCompanyMst().getId(), salesMode,
						"YES");

		if (schemeInstanceList.size() > 0) {

			for (SchemeInstance scheme : schemeInstanceList) {
				Boolean validityPeriod = CheckValidityPeriod(scheme, loginDate, salesTransHdr);
				if (validityPeriod) {

					Boolean eligible = eligibilityCheck(scheme, salesTransHdr);
					if (eligible) {
						return true;
					}

				}
			}
		}
		return false;
	}

	private Boolean eligibilityCheck(SchemeInstance scheme, SalesTransHdr salesTransHdr) {

		CompanyMst companyMst = salesTransHdr.getCompanyMst();
		List<SchEligibilityAttribInst> schEligibilityAttrbInstList = schEligibilityAttribInstRepository
				.findByCompanyMstAndSchemeIdAndEligibilityId(companyMst, scheme.getId(), scheme.getEligibilityId());

		for (SchEligibilityAttribInst inst : schEligibilityAttrbInstList) {

			if (inst.getEligibilityId().equals("7")) {

				Boolean validInvoice = ChecknvoiceAmount(inst, scheme, salesTransHdr);
				if (!validInvoice) {
					return false;
				} else {
					return true;
				}

			}

		}

		return true;

	}

	private Boolean CheckValidityPeriod(SchemeInstance scheme, Date loginDate, SalesTransHdr salesTransHdr) {

		List<SchSelectionAttribInst> schSlectionAttrbInstList = schSelectionAttribInstRepository
				.findByCompanyMstAndSchemeId(salesTransHdr.getCompanyMst(), scheme.getId());

		for (SchSelectionAttribInst select : schSlectionAttrbInstList) {

			if (select.getAttributeType().equalsIgnoreCase("DATE")) {

				if (select.getAttributeName().equalsIgnoreCase("START DATE")) {
					Boolean validStartDate = validStartDate(select, loginDate);
					if (!validStartDate) {
						return false;
					}
				}

				if (select.getAttributeName().equalsIgnoreCase("END DATE")) {
					Boolean validEndDate = validEndDate(select, loginDate);
					if (!validEndDate) {
						return false;
					}
				}

			}

			if (select.getAttributeType().equalsIgnoreCase("DAY")) {

//				java.util.Date udate = SystemSetting.systemDate;
				java.util.Calendar calender = java.util.Calendar.getInstance();
				calender.setTime(loginDate);

				int day = calender.get(java.util.Calendar.DAY_OF_WEEK);
				String dayofweek = checkValidDay(day);

				if (!dayofweek.equalsIgnoreCase(select.getAttributeValue())) {
					return false;
				}

			}

			if (select.getAttributeType().equalsIgnoreCase("MONTH")) {

//				java.util.Date udate = SystemSetting.systemDate;
				java.util.Calendar calender = java.util.Calendar.getInstance();
				calender.setTime(loginDate);

				int month = loginDate.getMonth();
				String monthname = MonthName(month);

				if (!monthname.equalsIgnoreCase(select.getAttributeValue())) {
					return false;
				}

			}

		}

		return true;
	}

	private String MonthName(int month) {
		if (month == 0) {
			return "JANUARY";
		}
		if (month == 1) {
			return "FEBRUARY";
		}
		if (month == 2) {
			return "MARCH";
		}
		if (month == 3) {
			return "APRIL";
		}
		if (month == 4) {

			return "MAY";
		}
		if (month == 5) {
			return "JUNE";
		}
		if (month == 6) {
			return "JULY";
		}
		if (month == 7) {
			return "AUGUST";
		}
		if (month == 8) {
			return "SEPTEMBER";
		} else if (month == 9) {
			return "OCTOBER";
		}
		if (month == 10) {
			return "NOVEMBER";
		}
		if (month == 11) {
			return "DECEMBER";
		}

		return null;
	}

	private String checkValidDay(int day) {
		if (day == 1) {
			return "SUNDAY";
		}
		if (day == 2) {
			return "MONDAY";
		}
		if (day == 3) {
			return "TUESDAY";
		}
		if (day == 4) {
			return "WEDNESDAY";
		}
		if (day == 5) {
			return "THURSDAY";
		}
		if (day == 6) {
			return "FRIDAY";
		}
		if (day == 7) {
			return "SATURDAY";
		}

		return null;
	}

	private Boolean validEndDate(SchSelectionAttribInst select, Date nowDate) {
		System.out.println("Noooooooooooooooo" + nowDate);
		java.util.Date endDate = SystemSetting.StringToUtilDate(select.getAttributeValue(), "dd-MM-yyyy");
		Integer intDate = endDate.compareTo(nowDate);

		if (intDate >= 0) {
			return true;
		}
		return false;
	}

	private Boolean validStartDate(SchSelectionAttribInst select, Date nowDate) {
		System.out.println("Noooooooooooooooo" + nowDate);
		java.util.Date startDate = SystemSetting.StringToUtilDate(select.getAttributeValue(), "dd-MM-yyyy");
//			String schemeDate = select.getAttributeValue();
		Integer intDate = nowDate.compareTo(startDate);

		if (intDate >= 0) {
			return true;
		}

		return false;
	}

	private Boolean ChecknvoiceAmount(SchEligibilityAttribInst inst, SchemeInstance scheme,
			SalesTransHdr salesTransHdr) {
		Double eligibAmount = 0.0;

		SummarySalesDtl summary = salessummary(salesTransHdr.getId());

		SchEligibilityAttribInst eligib = schEligibilityAttribInstRepository
				.findByCompanyMstAndAttributeNameAndSchemeId(salesTransHdr.getCompanyMst(), "AMOUNT", scheme.getId());

		eligibAmount = Double.parseDouble(eligib.getAttributeValue());

		SalesDtl salesDtl = new SalesDtl();
		salesDtl.setAmount(summary.getTotalAmount());
		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setCompanyMst(salesTransHdr.getCompanyMst());

		if (eligibAmount <= summary.getTotalAmount()) {
			if (scheme.getOfferId().equals("2")) {

				AddOfferQtyCheck(scheme, salesTransHdr, salesDtl, eligibAmount, "ITEM_AND_QTY");
				return true;
			}

			if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

				AddAmountDiscount(scheme, salesTransHdr, eligibAmount, "ITEM_AND_QTY");
				return true;
			}

		}
		return false;
	}

	private void AddOfferQtyCheck(SchemeInstance scheme, SalesTransHdr salesTransHdr, SalesDtl salesDtl, Double eligQty,
			String eligibilityType) {

		String itemName = "";
		String batch = "";
		String mutipleAllowed = "";
		Double offerQty = 0.0;

		List<SchOfferAttrInst> offerInstList = schOfferAttrInstRepository
				.findByCompanyMstAndSchemeId(salesTransHdr.getCompanyMst(), scheme.getId());

		if (offerInstList.size() > 0) {
			for (SchOfferAttrInst offer : offerInstList) {
				if (offer.getAttributeName().equalsIgnoreCase("ITEMNAME")) {
					itemName = offer.getAttributeValue();
				}

				if (offer.getAttributeName().equalsIgnoreCase("QTY")) {
					offerQty = Double.parseDouble(offer.getAttributeValue());
				}

				if (offer.getAttributeName().equalsIgnoreCase("BATCH_CODE")) {
					batch = offer.getAttributeValue();
				}

				if (offer.getAttributeName().equalsIgnoreCase("MULTIPLE ALLOWED")) {
					mutipleAllowed = offer.getAttributeValue();
				}
			}
		}

		addOfferItem(itemName, offerQty, batch, mutipleAllowed, salesDtl, eligQty, eligibilityType, scheme);

	}

	private void AddAmountDiscount(SchemeInstance scheme, SalesTransHdr salesTransHdr, Double instQty, String string) {

		Double discount = 0.0;

		SummarySalesDtl summary = salessummary(salesTransHdr.getId());

		List<SchOfferAttrInst> offerInstList = schOfferAttrInstRepository
				.findByCompanyMstAndSchemeId(salesTransHdr.getCompanyMst(), scheme.getId());
		for (SchOfferAttrInst offer : offerInstList) {
			if (offer.getAttributeName().equalsIgnoreCase("DISCOUNT")) {
				discount = Double.parseDouble(offer.getAttributeValue());
			}
			if (offer.getAttributeName().equalsIgnoreCase("PERCENTAGE DISCOUNT")) {
				discount = Double.parseDouble(offer.getAttributeValue());
			}
		}

		if (scheme.getOfferId().equals("1")) {

			salesTransHdr.setInvoiceDiscount(discount);

			discount = (discount / summary.getTotalAmount()) * 100;
			String discountper = discount + "%";

			salesTransHdr.setDiscount(discountper);
		}
		if (scheme.getOfferId().equals("4")) {

			salesTransHdr.setDiscount(discount + "%");

			discount = summary.getTotalAmount() * discount / 100;
			salesTransHdr.setInvoiceDiscount(discount);

		}

		return;

	}

	private SummarySalesDtl salessummary(String salesTransHdrId) {
		List<Object> objList = salesDetailsRepo.findSumSalesDtl(salesTransHdrId);

		Object[] objAray = (Object[]) objList.get(0);

		SummarySalesDtl summary = new SummarySalesDtl();
		summary.setTotalQty((Double) objAray[0]);
		summary.setTotalAmount((Double) objAray[1]);
		summary.setTotalTax((Double) objAray[2]);
		summary.setTotalCessAmt((Double) objAray[3]);

		return summary;

	}

	private void addOfferItem(String itemName, Double offerQty, String batch, String mutipleAllowed,
			SalesDtl salesDtlref, Double eligQty, String eligibilityType, SchemeInstance scheme) {

		Optional<ItemMst> itemMstOpt = itemMstRepository.findByItemNameAndCompanyMst(itemName,
				salesDtlref.getCompanyMst());
		ItemMst itemMst = itemMstOpt.get();

		SalesTransHdr salesTransHdr = salesDtlref.getSalesTransHdr();

		int itemqty = 0;

		if (mutipleAllowed.equalsIgnoreCase("YES")) {

			SchEligibilityAttribInst schEligibilityAttribInstAmt = schEligibilityAttribInstRepository
					.findByCompanyMstAndAttributeNameAndSchemeId(salesDtlref.getCompanyMst(), "AMOUNT", scheme.getId());

			if (null != schEligibilityAttribInstAmt) {
				itemqty = (int) (salesDtlref.getAmount() / eligQty);

				itemqty = (int) (itemqty * offerQty);
			}

			System.out.println("--mutipleAllowed---Qty-------" + itemqty);
		}

		Optional<CompanyMst> companyMst = companyMstRepository.findById(salesDtlref.getCompanyMst().getId());

		SalesDtl salesDtl = new SalesDtl();
		salesDtl.setCompanyMst(companyMst.get());
		salesDtl.setItemId(itemMst.getId());
		salesDtl.setItemName(itemMst.getItemName());
		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setBarcode(itemMst.getBarCode());
		salesDtl.setBatch(batch);
		salesDtl.setSchemeId(scheme.getId());
		if (itemqty > 0) {
			salesDtl.setQty((double) itemqty);
		} else {
			salesDtl.setQty((double) offerQty);
		}
		salesDtl.setUnitId(itemMst.getUnitId());

		Optional<UnitMst> unitMstOpt = unitMstRepository.findById(itemMst.getUnitId());
		UnitMst unitMst = unitMstOpt.get();

		if (null != unitMst) {
			salesDtl.setUnitName(unitMst.getUnitName());
		}

		salesDtl.setMrp(0.0);
		salesDtl.setTaxRate(0.0);
		salesDtl.setRate(0.0);
		salesDtl.setCessAmount(0.0);
		salesDtl.setCessRate(0.0);
		salesDtl.setCgstAmount(0.0);
		salesDtl.setCgstTaxRate(0.0);
		salesDtl.setSgstAmount(0.0);
		salesDtl.setSgstTaxRate(0.0);
		salesDtl.setAmount(0.0);
		salesDtl.setAddCessRate(0.0);
		salesDtl.setDiscount(0.0);
		salesDtl.setIgstAmount(0.0);
		salesDtl.setIgstTaxRate(0.0);

		//salesDtl = salesDetailsRepo.saveAndFlush(salesDtl);
		salesDtl=saveAndPublishService.saveSalesDtl(salesDtl, salesDtl.getSalesTransHdr().getBranchCode());
		/*if(null!=salesDtl)
		{
			saveAndPublishService.publishObjectToKafkaEvent(salesDtl, 
					salesDtl.getSalesTransHdr().getBranchCode(), 
					KafkaMapleEventType.SALESDTL,KafkaMapleEventType.SERVER);
		}*/
		

	}
	
	@DeleteMapping("{companymstid}/salestranshdr/deletenullvouchernumber")
	public void salestransHdrDelete(@PathVariable String companymstid) {

		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepo.fetchSalesTanHdr();

		for (int i = 0; i < salesTransHdrList.size(); i++) {
			List<SalesDtl> SalesDtlList = salesDetailsRepository
					.findBySalesTransHdrId(salesTransHdrList.get(i).getId());
			for (int j = 0; j < SalesDtlList.size(); j++) {

				SalesDltdDtl salesDltdDtl = new SalesDltdDtl();
				salesDltdDtl.setBarcode(SalesDtlList.get(j).getBarcode());

				salesDltdDtl.setBatch(SalesDtlList.get(j).getBatch());
				if (null == SalesDtlList.get(j).getAddCessRate()) {
					salesDltdDtl.setAddCessRate(0.0);
				} else {
					salesDltdDtl.setAddCessRate(SalesDtlList.get(j).getAddCessRate());
				}

				if (null == SalesDtlList.get(j).getAmount()) {
					salesDltdDtl.setAmount(0.0);
				} else {
					salesDltdDtl.setAmount(SalesDtlList.get(j).getAmount());
				}
				if (null == SalesDtlList.get(j).getCessAmount()) {
					salesDltdDtl.setCessAmount(0.0);
				} else {
					salesDltdDtl.setCessAmount(SalesDtlList.get(j).getCessAmount());
				}
				if (null == SalesDtlList.get(j).getAddCessAmount()) {
					salesDltdDtl.setAddCessAmount(0.0);
				} else {
					salesDltdDtl.setAddCessAmount(SalesDtlList.get(j).getAddCessAmount());
				}

				if (null == SalesDtlList.get(j).getCgstAmount()) {
					salesDltdDtl.setCgstAmount(0.0);
				} else {

					salesDltdDtl.setCgstAmount(SalesDtlList.get(j).getCgstAmount());
				}
				if (null == SalesDtlList.get(j).getCgstTaxRate()) {
					salesDltdDtl.setCgstTaxRate(0.0);
				} else {
					salesDltdDtl.setCgstTaxRate(SalesDtlList.get(j).getCgstTaxRate());
				}
				if (null == SalesDtlList.get(j).getCostPrice()) {
					salesDltdDtl.setCostPrice(0.0);
				} else {
					salesDltdDtl.setCostPrice(SalesDtlList.get(j).getCostPrice());
				}
				salesDltdDtl.setCompanyMst(SalesDtlList.get(j).getCompanyMst());

				salesDltdDtl.setExpiryDate(SalesDtlList.get(j).getExpiryDate());
				if (null == SalesDtlList.get(j).getDiscount()) {
					salesDltdDtl.setDiscount(0.0);
				} else {
					salesDltdDtl.setDiscount(SalesDtlList.get(j).getDiscount());
				}
				if (null == SalesDtlList.get(j).getCessRate()) {
					salesDltdDtl.setCessRate(0.0);
				} else {
					salesDltdDtl.setCessRate(SalesDtlList.get(j).getCessRate());
				}
				if (null == SalesDtlList.get(j).getSgstAmount()) {
					salesDltdDtl.setSgstAmount(0.0);
				} else {
					salesDltdDtl.setSgstAmount(SalesDtlList.get(j).getSgstAmount());
				}
				salesDltdDtl.setIgstAmount(SalesDtlList.get(j).getIgstAmount());
				salesDltdDtl.setItemId(SalesDtlList.get(j).getItemId());
				salesDltdDtl.setMrp(SalesDtlList.get(j).getMrp());
				salesDltdDtl.setOfferReferenceId(SalesDtlList.get(j).getOfferReferenceId());
				salesDltdDtl.setItemTaxaxId(SalesDtlList.get(j).getItemTaxaxId());
				salesDltdDtl.setSchemeId(SalesDtlList.get(j).getSchemeId());
				salesDltdDtl.setQty(SalesDtlList.get(j).getQty());
				salesDltdDtl.setReturnedQty(SalesDtlList.get(j).getReturnedQty());
				if (null == SalesDtlList.get(j).getTaxRate()) {
					salesDltdDtl.setTaxRate(0.0);
				} else {
					salesDltdDtl.setTaxRate(SalesDtlList.get(j).getTaxRate());
				}
				if (null == SalesDtlList.get(j).getSgstTaxRate()) {

					salesDltdDtl.setSgstTaxRate(0.0);
				} else {
					salesDltdDtl.setSgstTaxRate(SalesDtlList.get(j).getSgstTaxRate());
				}

				if (null == SalesDtlList.get(j).getUnitId()) {
					salesDltdDtl.setUnitId("");
				} else {
					salesDltdDtl.setUnitId(SalesDtlList.get(j).getUnitId());
				}
				if (null == SalesDtlList.get(j).getIgstTaxRate()) {
					salesDltdDtl.setIgstTaxRate(0.0);
				} else {
					salesDltdDtl.setIgstTaxRate(SalesDtlList.get(j).getIgstTaxRate());
				}
				if (null == SalesDtlList.get(j).getRate()) {
					salesDltdDtl.setRate(0.0);
				} else {
					salesDltdDtl.setRate(SalesDtlList.get(j).getRate());
				}

				salesDltdDtl.setUnitName(SalesDtlList.get(j).getUnitName());
				salesDltdDtl.setFbKey(SalesDtlList.get(j).getFbKey());
				if (null == SalesDtlList.get(j).getStandardPrice()) {
					salesDltdDtl.setStandardPrice(0.0);
				} else {
					salesDltdDtl.setStandardPrice(SalesDtlList.get(j).getStandardPrice());
				}

				
				salesDeletedDtlRepository.save(salesDltdDtl);

			}
			
			salesTransHdrRepo.delete(salesTransHdrList.get(i));

		}

		
		

	}

	@GetMapping("{companymstid}/salestranshdrbyvoucherdate/{branchcode}")
	public List<SalesTransHdr> getSalesTransHdrByVoucherDate(@PathVariable("companymstid") String companymstid,
			@RequestParam("date") String voucherdate, @PathVariable("branchcode") String branchcode) {

		Date date = SystemSetting.StringToUtilDate(voucherdate, "yyyy-MM-dd");

		return salesTransHdrRepo.findByVoucherDate(date, branchcode);

	}

	@GetMapping("{companymstid}/salestranshdr/salestranshdrbyvoucherdateandmode")
	public List<SalesTransHdr> getSalesTransHdrByVoucherDateNDMode(@PathVariable("companymstid") String companymstid,
			@RequestParam("date") String voucherdate) {

		Date date = SystemSetting.StringToUtilDate(voucherdate, "yyyy-MM-dd");

		return salesTransHdrRepo.findByVoucherDateWithSalesMode(date);

	}

	@GetMapping("{companymstid}/allsalesnumericbyvoucherdate")
	public List<SalesModeWiseBillReport> getallSalesNumericVoucherNumber(
			@PathVariable("companymstid") String companymstid, @RequestParam("date") String voucherdate) {

		Date date = SystemSetting.StringToUtilDate(voucherdate, "yyyy-MM-dd");

		return salesTransHdrService.findNumericVoucherNoBySalesModeAndDate(date, companymstid);

	}

	// function for getting the item batches
//	@GetMapping("{companymstid}/salestranshdr/sample/{itemId}/{reqQty}")
//	public Map<String, Double> getAllBatches(@PathVariable("itemId") String itemId,
//			@PathVariable("reqQty")Double reqQty)
//	{
//		return productionFinishedGoodStock.getBatchStock(itemId, reqQty);
//	}

	// url to generate invoice

//	@GetMapping("{companymstid}/generatesalesinvoice/{customer}/{salesman}")
//	public List<SalesDtl> GenerateSalesInvoice(
//			@PathVariable("companymstid") String companymstid,
//			@PathVariable("customer") String customer,
//			@PathVariable("salesman") String salesman) {
//		
//		return salesTransHdrService.GenerateSalesInvoice(companymstid,customer,salesman);
//
//	}

	@GetMapping("{companymstid}/allbillseriesbyvoucherdate")
	public List<SalesModeWiseBillReport> getAllBillseriesByVoucherDate(
			@PathVariable("companymstid") String companymstid, @RequestParam("date") String voucherdate) {
		System.out.println(voucherdate
				+ "################################################################ voucher date isssssssssssssssssssss");
		Date date = SystemSetting.StringToUtilDate(voucherdate, "yyyy-MM-dd");

		System.out.println(date
				+ "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ the date isssssssssssssssssssss");
		return salesTransHdrService.findBillseriesByVoucherDate(date, companymstid);

	}

	@PutMapping("{companymstid}/spencersalestranshdrfinalsave/{salestranshdrid}")
	public SalesTransHdr SpenserSalesFinalSaveOffer(

			@PathVariable("salestranshdrid") String salestranshdrid, @PathVariable("companymstid") String companymstid,
			@RequestParam("logdate") String logdate,

			@Valid @RequestBody SalesTransHdr salesTransHdrRequest) {

		Date logDate = SystemSetting.StringToUtilDate(logdate, "dd-MM-yyyy");

		return salesTransHdrRepo.findById(salestranshdrid).map(salestranshdr -> {

			salesTransHdrRequest.setCompanyMst(salestranshdr.getCompanyMst());

			if (null == salesTransHdrRequest.getVoucherNumber()) {

				if (null != salesTransHdrRequest.getInvoiceNumberPrefix()) {
					String voucherNoPreFix = null;
					voucherNoPreFix = SystemSetting.getFinancialYear() + salesTransHdrRequest.getInvoiceNumberPrefix();
					String vcNo = "";

					VoucherNumber voucherNo = voucherService.generateInvoice(voucherNoPreFix,
							salestranshdr.getCompanyMst().getId());

					if (voucherNoYearPrefix.equalsIgnoreCase("NO")) {
						vcNo = voucherNo.getCode().substring(9);
					} else {
						vcNo = voucherNo.getCode();
					}

					if (salesTransHdrRequest.getInvoiceNumberPrefix().equalsIgnoreCase("dd-MM")) {

						vcNo = vcNo.replace(SystemSetting.getFinancialYear(), "");

						vcNo = vcNo.replace(salesTransHdrRequest.getInvoiceNumberPrefix(), "");
						vcNo = vcNo.replace(salestranshdr.getCompanyMst().getId(), "");

						System.out.println("vcNo =" + vcNo);

						vcNo = SystemSetting.SqlDateTostring(salesTransHdrRequest.getVoucherDate(), "dd-MM") + "-"
								+ vcNo;
						System.out.println("vcNo =" + vcNo);
					} else if (salesTransHdrRequest.getInvoiceNumberPrefix().equalsIgnoreCase("MM-dd")) {
						vcNo = vcNo.replace(SystemSetting.getFinancialYear(), "");

						vcNo = vcNo.replace(salesTransHdrRequest.getInvoiceNumberPrefix(), "");
						vcNo = vcNo.replace(salestranshdr.getCompanyMst().getId(), "");

						System.out.println("vcNo =" + vcNo);

						vcNo = SystemSetting.SqlDateTostring(salesTransHdrRequest.getVoucherDate(), "MM-dd") + "-"
								+ vcNo;
						System.out.println("vcNo =" + vcNo);
					}
					salesTransHdrRequest.setVoucherNumber(vcNo);
				} else {
					String voucherNoPreFixPos = null;
					String vNo = null;

					String finYear = SystemSetting.getFinancialYear();

					VoucherNumber voucherNo = voucherService.generateInvoice(
							SystemSetting.getFinancialYear() + POS_SALES_PREFIX, salestranshdr.getCompanyMst().getId());

					if (voucherNoYearPrefix.equalsIgnoreCase("NO")) {
						vNo = voucherNo.getCode().substring(9);
					} else {
						vNo = voucherNo.getCode();
					}
					salesTransHdrRequest.setVoucherNumber(vNo);

					String invoice[] = salesTransHdrRequest.getVoucherNumber().split(POS_SALES_PREFIX);
					salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));

				}

			}

			salestranshdr.setLocalCustomerMst(salesTransHdrRequest.getLocalCustomerMst());

			salestranshdr.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
			salestranshdr.setCustomerId(salesTransHdrRequest.getCustomerId());
			salestranshdr.setCardamount(salesTransHdrRequest.getCardamount());
			salestranshdr.setCardNo(salesTransHdrRequest.getCardNo());
			salestranshdr.setCashPay(salesTransHdrRequest.getCashPay());
			salestranshdr.setCreditOrCash(salesTransHdrRequest.getCreditOrCash());
			salestranshdr.setDeliveryBoyId(salesTransHdrRequest.getDeliveryBoyId());
			salestranshdr.setInvoiceAmount(salesTransHdrRequest.getInvoiceAmount());
			salestranshdr.setIsBranchSales(salesTransHdrRequest.getBranchCode());
			salestranshdr.setMachineId(salesTransHdrRequest.getMachineId());
			salestranshdr.setPaidAmount(salesTransHdrRequest.getPaidAmount());
			salestranshdr.setSalesManId(salesTransHdrRequest.getSalesManId());
			salestranshdr.setSalesMode(salesTransHdrRequest.getSalesMode());
			salestranshdr.setServingTableName(salesTransHdrRequest.getServingTableName());
			salestranshdr.setAccountHeads(salesTransHdrRequest.getAccountHeads());

			Calendar calendar = Calendar.getInstance();
			java.util.Date currentDate = calendar.getTime();
			java.sql.Date date = new java.sql.Date(currentDate.getTime());

			String strDate = SystemSetting.SqlDateTostring(date);

			date = SystemSetting.StringToSqlDate(strDate, "dd/MM/yyyy");

			if (null == salesTransHdrRequest.getVoucherDate()) {
				salesTransHdrRequest.setVoucherDate(date);
			}

			salesTransHdrRequest.setCompanyMst(salestranshdr.getCompanyMst());
			salesTransHdrRequest.setSourceIP(salestranshdr.getSourceIP());
			salesTransHdrRequest.setSourcePort(salestranshdr.getSourcePort());

			if (null != salestranshdr.getBranchCode()) {
				salesTransHdrRequest.setBranchCode(salestranshdr.getBranchCode());
			}

			if (null != salesTransHdrRequest.getInvoiceNumberPrefix()) {
				String invoice[] = salesTransHdrRequest.getVoucherNumber()
						.split(salesTransHdrRequest.getInvoiceNumberPrefix());
				salesTransHdrRequest.setNumericVoucherNumber(Long.parseLong(invoice[1]));

			}

			//salestranshdr = salesTransHdrRepo.saveAndFlush(salesTransHdrRequest);
			
			salestranshdr = saveAndPublishService.saveSalesTransHdr(salesTransHdrRequest, salesTransHdrRequest.getBranchCode());
					
			Boolean offer = CheckOfferWhileFinalSave(salestranshdr, logDate);
			/*
			 * mange Sales receipts
			 */

			String SalesMode = salestranshdr.getSalesMode();

			Double invoiceAmount = salestranshdr.getInvoiceAmount();
			Double cashPaidAmount = salestranshdr.getCashPay();
			Double cardAmount = salestranshdr.getCardamount();
			Double sodexoAmount = salestranshdr.getSodexoAmount();
			Double creditAmount = salestranshdr.getCreditAmount();

			List<SalesReceipts> salesReceiptsList = salesReceiptsRepo.findByCompanyMstIdAndSalesTransHdrId(companymstid,
					salestranshdr.getId());

			Iterator iter = salesReceiptsList.iterator();
			while (iter.hasNext()) {
				SalesReceipts salesReceipts = (SalesReceipts) iter.next();
				salesReceipts.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
				salesReceipts.setCompanyMst(salesTransHdrRequest.getCompanyMst());
				salesReceiptsRepo.saveAndFlush(salesReceipts);

			}

			List<AccountReceivable> accountReceivableList = accountReceivableRepository
					.findByCompanyMstAndSalesTransHdr(salestranshdr.getCompanyMst(), salestranshdr);

			if (accountReceivableList.size() > 0) {
				accountReceivable = accountReceivableList.get(0);
				if (null != accountReceivable) {
					accountReceivable.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());
					java.sql.Date ldate = SystemSetting.UtilDateToSQLDate(salesTransHdrRequest.getVoucherDate());
					accountReceivable.setVoucherDate(ldate);
					accountReceivable = accountReceivableRepository.saveAndFlush(accountReceivable);

					Map<String, Object> variables = new HashMap<String, Object>();

					variables.put("voucherNumber", accountReceivable.getVoucherNumber());
					variables.put("voucherDate", accountReceivable.getVoucherDate());
					variables.put("inet", 0);
					variables.put("id", accountReceivable.getId());
					variables.put("branchcode", accountReceivable.getSalesTransHdr().getBranchCode());

					variables.put("companyid", accountReceivable.getCompanyMst());
					if (serverorclient.equalsIgnoreCase("REST")) {
						variables.put("REST", 1);
					} else {
						variables.put("REST", 0);
					}

					variables.put("WF", "forwardAccountReceivable");

					String workflow = (String) variables.get("WF");
					String voucherNumber = (String) variables.get("voucherNumber");
					String sourceID = (String) variables.get("id");

					LmsQueueMst lmsQueueMst = new LmsQueueMst();

					lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

					java.util.Date uDate = (Date) variables.get("voucherDate");
					java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(uDate);

					uDate = SystemSetting.SqlDateToUtilDate(sqlDate);
					lmsQueueMst.setVoucherDate(sqlDate);

					lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
					lmsQueueMst.setVoucherType(workflow);
					lmsQueueMst.setPostedToServer("NO");
					lmsQueueMst.setJobClass("forwardAccountReceivable");
					lmsQueueMst.setCronJob(true);
					lmsQueueMst.setJobName(workflow + sourceID);
					lmsQueueMst.setJobGroup(workflow);
					lmsQueueMst.setRepeatTime(60000L);
					lmsQueueMst.setSourceObjectId(sourceID);

					lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

					lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
					variables.put("lmsqid", lmsQueueMst.getId());
					eventBus.post(variables);
				}
			}
			/*
			 * Manage Stock
			 * 
			 */

			try {
				salesAccounting.execute(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(),
						salestranshdr.getId(), salestranshdr.getCompanyMst());
			} catch (PartialAccountingException e) {

				logger.error(e.getMessage());
				return null;
			}

			salesDayEndReporting.execute(salestranshdr.getVoucherNumber(), salestranshdr.getVoucherDate(),
					salestranshdr.getCompanyMst(), salestranshdr.getId());

//			SalesStockUpdate(salesTransHdrRequest.getVoucherNumber(), salesTransHdrRequest.getVoucherDate(),
//					salestranshdr);

			/*
			 * Manage Accounting
			 */

			Map<String, Object> variables = new HashMap<String, Object>();

			String creditOrCash = salestranshdr.getCreditOrCash();
			if (null == creditOrCash) {
				creditOrCash = "CASH";
			}

			if (null == salestranshdr.getIsBranchSales()) {
				salestranshdr.setIsBranchSales("N");
			}

			String isBranchSales = salestranshdr.getIsBranchSales();
			int int_isBranchSales = 0;
			if (isBranchSales.equalsIgnoreCase("Y")) {
				int_isBranchSales = 1;

			} else {
				int_isBranchSales = 0;
			}

			variables.put("voucherNumber", salestranshdr.getVoucherNumber());
			variables.put("voucherDate", salestranshdr.getVoucherDate());
			variables.put("inet", 0);
			variables.put("id", salestranshdr.getId());
			variables.put("isbranchsales", int_isBranchSales);
			variables.put("companyid", salestranshdr.getCompanyMst());

			if (serverorclient.equalsIgnoreCase("REST")) {
				variables.put("REST", 1);
			} else {
				variables.put("REST", 0);
			}

			if (creditOrCash.equalsIgnoreCase("CREDIT") || creditOrCash.equalsIgnoreCase("B2B")) {
				variables.put("CREDIT", 1);
			} else {
				variables.put("CREDIT", 0);
			}

			variables.put("branchcode", salestranshdr.getBranchCode());

			variables.put("WF", "forwardSales");

			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");

			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(uDate);

			lmsQueueMst.setVoucherDate(sqlDate);

			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardSales");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);

			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);

			/*
			 * LmsQueueTallyMst lmsQueueTallyMst = new LmsQueueTallyMst();
			 * lmsQueueTallyMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			 * lmsQueueTallyMst.setVoucherDate(uDate);
			 * 
			 * lmsQueueTallyMst.setVoucherNumber((String) variables.get("voucherNumber"));
			 * lmsQueueTallyMst.setVoucherType(workflow);
			 * lmsQueueTallyMst.setPostedToTally("NO");
			 * lmsQueueTallyMst.setJobClass("forwardSales");
			 * lmsQueueTallyMst.setCronJob(true); lmsQueueTallyMst.setJobName(workflow +
			 * sourceID); lmsQueueTallyMst.setJobGroup(workflow);
			 * lmsQueueTallyMst.setRepeatTime(60000L);
			 * lmsQueueTallyMst.setSourceObjectId(sourceID);
			 * lmsQueueTallyMst.setBranchCode((String) variables.get("branchcode"));
			 * lmsQueueTallyMst = lmsQueueTallyMstRepository.saveAndFlush(lmsQueueTallyMst);
			 * variables.put("lmsqid", lmsQueueMst.getId()); eventBus.post(variables);
			 */

			/*
			 * If KOT update kot
			 */

			/*
			 * 
			 * String sourceIP = salestranshdr.getSourceIP(); String sourcePort =
			 * salestranshdr.getSourcePort(); if (null != sourceIP && null != sourcePort) {
			 * SocketConnection socketConnection = new SocketConnection();
			 * 
			 * socketConnection.sendToSocket(sourceIP, sourcePort, salestranshdr.getId() +
			 * ":" + "POS_SAVED" + ":" + salestranshdr.getServingTableName()); //
			 * socketConnection.sendToSocket(sourceIP, sourcePort, "OVER"); }
			 * 
			 */

			return salestranshdr;
		}).orElseThrow(() -> new ResourceNotFoundException("salestranshdrid " + salestranshdrid + " not found"));

	}

	// ----------------version 12 end

	@PutMapping("{companymstid}/salestranshdrresource/{hdrid}/updateperformainvoice")
	public SalesTransHdr updateSalesTransHdrPerformaInvoice(@PathVariable(value = "hdrid") String hdrid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SalesTransHdr salesTransRequest) {

		Optional<SalesTransHdr> salesTransHdrOpt = salesTransHdrRepository.findById(hdrid);
		SalesTransHdr salesTransHdr = salesTransHdrOpt.get();

		salesTransHdr.setPerformaInvoicePrinted(salesTransRequest.getPerformaInvoicePrinted());
		salesTransHdr = salesTransHdrRepo.save(salesTransHdr);

		return salesTransHdr;

	}

	@PutMapping("{companymstid}/salestranshdrresource/updatelocalcustomer/{hdrid}")
	public SalesTransHdr updateSalesTransHdrWithLocalCustomer(@PathVariable(value = "hdrid") String hdrid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SalesTransHdr salesTransRequest) {

		Optional<SalesTransHdr> salesTransHdrOpt = salesTransHdrRepository.findById(hdrid);
		SalesTransHdr salesTransHdr = salesTransHdrOpt.get();

		salesTransHdr.setLocalCustomerMst(salesTransRequest.getLocalCustomerMst());
		//salesTransHdr = salesTransHdrRepo.saveAndFlush(salesTransHdr);
		
		salesTransHdr = saveAndPublishService.saveSalesTransHdr(salesTransHdr,salesTransHdr.getBranchCode());
		

		return salesTransHdr;

	}

	@GetMapping("{companymstid}/salestranshdr/getnullvoucherbbycustomerid/{customerid}/{customsalesmode}")
	public SalesTransHdr getSalesTransHdrWithVoucherNullAndCustomerId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "customerid") String customerid,
			@PathVariable(value = "customsalesmode") String customsalesmode,
			@RequestParam(value = "sdate") String sdate)

	{
		Date udate = SystemSetting.StringToUtilDate(sdate, "yyyy-MM-dd");
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return salesTransHdrRepo.getSalesTransHdrBycustomerAndVoucherNull(customerid, udate, companyMst,
				customsalesmode);

	}

	@GetMapping("{companymstid}/savesaletranshdr/web")
	public @ResponseBody SalesTransHdr saveWebCustomer(@RequestParam("customer") String customer,
			@RequestParam("branchcode") String branchcode, @PathVariable(value = "companymstid") String companymstid,
			@RequestParam("date") String date) {

		// Find Customer Mst using mobile

		java.util.Date edate = SystemSetting.StringToUtilDate(date, "yyyy-MM-dd");

		customer = customer.replaceAll("%20", " ");
//		customer = customer.replaceAll("%21", "\\-");
		customer = customer.replaceAll("%22", "\\[");
		customer = customer.replaceAll("%23", "\\]");

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		AccountHeads accountHeadsOpt = accountHeadsRepository.findByAccountNameAndCompanyMst(customer, companyMst);

		SalesTransHdr salesTransHdr = new SalesTransHdr();

		if (null == accountHeadsOpt.getPartyGst() || accountHeadsOpt.getPartyGst().length() < 13) {
			salesTransHdr.setSalesMode("B2C");
		} else {
			salesTransHdr.setSalesMode("B2B");
		}

		salesTransHdr.setBranchCode(branchcode);
		salesTransHdr.setVoucherDate(edate);
		salesTransHdr.setCompanyMst(companyMst);
		salesTransHdr.setAccountHeads(accountHeadsOpt);
		salesTransHdr.setCustomerId(accountHeadsOpt.getId());
		//SalesTransHdr saved = salesTransHdrRepository.save(salesTransHdr);
		SalesTransHdr saved  =  saveAndPublishService.saveSalesTransHdr(salesTransHdr, salesTransHdr.getBranchCode());
		

		return saved;
	}

//	@GetMapping("{companymstid}/salestranshdr/web/finalsave")
//	public SalesTransHdr SalesTransHdrFinalSaveWeb(
//			@RequestParam("hdrid") String hdrid,
//			@RequestParam("vouchernumber") String vouchernumber,
//			@PathVariable(value = "companymstid") String companymstid)
//	{
//
//		
//				return salesTransHdrRepository.findById(hdrid).map(salesTransHdr -> {
//					
//					salesTransHdr.setVoucherNumber(vouchernumber); 
//
//					SalesTransHdr saved = salesTransHdrRepository.saveAndFlush(salesTransHdr);
//		
//		            return saved;
//		        }).orElseThrow(() -> new ResourceNotFoundException("hdrid " + hdrid + " not found"));
//				
//				
//			
//	}

	@GetMapping("{companymstid}/salestranshdr/web/finalsave")
	public SalesTransHdr SalesTransHdrFinalSaveWeb(@RequestParam("hdrid") String hdrid,
			@RequestParam("vouchernumber") String vouchernumber,
			@PathVariable(value = "companymstid") String companymstid) {

		return salesTransHdrRepo.findById(hdrid).map(salesTransHdr -> {

			/*
			 * Sales from web is by default using MAIN store. Done by Regy on Dec 20, 2020.
			 */

			String store = "MAIN";

			salesTransHdr.setVoucherNumber(vouchernumber);
			Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
			CompanyMst companyMst = companyMstOpt.get();

			List<AccountReceivable> accountReceivableList = (List<AccountReceivable>) accountReceivableRepository
					.findByCompanyMstAndSalesTransHdr(salesTransHdr.getCompanyMst(), salesTransHdr);

			accountReceivable = new AccountReceivable();

			if (accountReceivableList.size() > 0) {

				accountReceivable = accountReceivableList.get(0);
			}

			if (null != accountReceivable) {
				accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
				accountReceivable.setCompanyMst(companyMst);
				// accountReceivable.setVoucherDate(salesTransHdrRequest.getVoucherDate());

				accountReceivableRepository.saveAndFlush(accountReceivable);
			}

			if (null != accountReceivable) {
				accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
				java.sql.Date ldate = SystemSetting.UtilDateToSQLDate(salesTransHdr.getVoucherDate());
				accountReceivable.setVoucherDate(ldate);
				accountReceivable.setCompanyMst(companyMst);

				accountReceivableRepository.saveAndFlush(accountReceivable);
			}

			try {
				salesAccounting.execute(salesTransHdr.getVoucherNumber(), salesTransHdr.getVoucherDate(),
						salesTransHdr.getId(), salesTransHdr.getCompanyMst());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
				return null;
			}

			salesDayEndReporting.execute(salesTransHdr.getVoucherNumber(), salesTransHdr.getVoucherDate(),
					salesTransHdr.getCompanyMst(), salesTransHdr.getId());

			SalesStockUpdate(salesTransHdr.getVoucherNumber(), salesTransHdr.getVoucherDate(), salesTransHdr, store);

			//SalesTransHdr saved = salesTransHdrRepo.saveAndFlush(salesTransHdr);
			SalesTransHdr saved  = saveAndPublishService.saveSalesTransHdr(salesTransHdr, salesTransHdr.getBranchCode());

			return saved;
		}).orElseThrow(() -> new ResourceNotFoundException("hdrid " + hdrid + " not found"));

	}

	@GetMapping("{companymstid}/salestranshdr/getsalesmode")
	public List<String> getSalesMode(@PathVariable(value = "companymstid") String companymstid)

	{
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		List<String> salesMode = salesTransHdrRepo.findAllSalesMode(companymstid);
		return salesMode;

	}

	@GetMapping("{companymstid}/salestranshdr/getcustomerwisesalesreportbycategory/{customername}/{branchcode}")
	public List<SalesInvoiceReport> getCustomerwiseSalesReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "customername") String customername,
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam(value = "categoryname") String categoryname, @RequestParam(value = "fdate") String fdate,
			@RequestParam(value = "tdate") String tdate)

	{
		Date fudate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		Date tudate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();

		String[] array = categoryname.split(";");
		return salesDetailsService.getCustomerWiseSalesReportWithCategoryList(fudate, tudate, companyMst, branchcode,
				array, customername);

	}

	@GetMapping("{companymstid}/salestranshdr/getcustomerwisesalesreport/{customername}/{branchcode}")
	public List<SalesInvoiceReport> getCustomerwiseSalesReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "customername") String customername,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam(value = "fdate") String fdate,
			@RequestParam(value = "tdate") String tdate)

	{
		Date fudate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		Date tudate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return salesDetailsService.getCustomerWiseSalesReport(fudate, tudate, companyMst, branchcode, customername);

	}

	// Get Sales with insurance

	@GetMapping("{companymstid}/salestranshdr/getsaleswithinsurance/{branchcode}")
	public List<InsuranceCompanySalesSummaryReport> getSalesSummaryWithInsurance(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam(value = "fdate") String fdate,
			@RequestParam(value = "tdate") String tdate

	)

	{

		Date fudate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		Date tudate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return salesDetailsService.getSalesSummaryWithInsurance(fudate, tudate, branchcode);

	}

	@GetMapping("{companymstid}/salestranshdr/getsaleswithinsuranceid/{branchcode}")
	public List<InsuranceCompanySalesSummaryReport> getSalesSummaryWithInsuranceId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam(value = "fdate") String fdate,
			@RequestParam(value = "tdate") String tdate, @RequestParam(value = "insurancelist") String insurancelist)

	{
		String[] array = insurancelist.split(";");
		Date fudate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		Date tudate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return salesDetailsService.getSalesSummaryWithInsuranceName(fudate, tudate, branchcode, array);

	}

	@GetMapping("{companymstid}/salestranshdr/resendbranchsalesbetweendate")
	public String resendFailedOtherBranchBetweenDate(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "fromdate") String fromdate, @RequestParam(value = "todate") String todate) {
		Date fuDate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		Date tudate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		List<SalesTransHdr> salesList = salesTransHdrRepo.findSalesBetweenVoucherDate(fuDate, tudate);
		for (int i = 0; i < salesList.size(); i++) {
			salesDetailsService.resendBranchSales(salesList.get(i).getId());
		}
		return "success";
	}

	// ---------------------new version 2.5 surya

	@GetMapping("{companymstid}/salestranshdr/savesalessummary/{branchcode}")
	public String saveSalesSummary(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam(value = "logdate") String logdate) {

		Date date = SystemSetting.StringToUtilDate(logdate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);

		String salesSavingStatus = salesTransHdrService.saveSalesSummary(date, companyMstOpt.get(), branchcode);
		return salesSavingStatus;
	}

	// ---------------------new version 2.5 surya end

	@DeleteMapping("{companymstid}/salestranshdr/salestranshdrdeletebyorderid/{orderid}")
	public void DeleteSalesTransHdrByOrderId(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "orderid") String orderid) {
		salesTransHdrRepo.deleteBySaleOrderHrdId(orderid);
		salesTransHdrRepo.flush();

	}

	@GetMapping("{companymstid}/salestranshdr/getkotnumberbytablename/{tablename}")
	public SalesTransHdr getKotNumber(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "tablename") String tablename, @RequestParam(value = "rdate") String rdate) {
		Date udate = SystemSetting.StringToUtilDate(rdate, "yyyy-MM-dd");
		List<SalesTransHdr> salesList = salesTransHdrRepo.getKotNumberBytableName(tablename, udate);
		if (salesList.size() > 0)
			return salesList.get(0);
		else
			return null;

	}

//	mobile Sales written by sathyajith

	@PostMapping("{companymstid}/salestranshdrresource/{salestranshdrId}/{vouchernumber}/{branchcode}/addsalesdtl")
	public SalesDtl createSalesDtlForMobile(@PathVariable(value = "salestranshdrId") String salestranshdrId,
			@PathVariable(value = "vouchernumber") String vouchernumber,
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam(value = "rdate") String rdate,
			@RequestParam(value = "custid") String custid, @Valid @RequestBody List<SalesDtl> salesDtlList) {

		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		// Optional<SalesTransHdr> salesTransHdrOpt=
		// salesTransHdrRepository.findById(salestranshdrId);

		Optional<AccountHeads> accountHeadsOptional = accountHeadsRepository.findById(custid);
		Date udate = SystemSetting.StringToUtilDate(rdate, "yyyy-MM-dd");
		SalesDtl salesDtl = new SalesDtl();
		// if(salesTransHdrOpt.isPresent()) {

		// return null;

		// }else {

		SalesTransHdr salesTransHdr = new SalesTransHdr();
		salesTransHdr.setVoucherNumber(vouchernumber);
		salesTransHdr.setVoucherDate(udate);
		// salesTransHdr.setId(salestranshdrId);
		salesTransHdr.setBranchCode(branchcode);
		salesTransHdr.setCompanyMst(companyMst.get());
		salesTransHdr.setAccountHeads(accountHeadsOptional.get());

		 //salesTransHdr = salesTransHdrRepository.save(salesTransHdr);
		
		salesTransHdr = saveAndPublishService.saveSalesTransHdr(salesTransHdr, salesTransHdr.getBranchCode());
	 

		for (int i = 0; i < salesDtlList.size(); i++) {

			salesDtl.setCompanyMst(salesDtlList.get(i).getCompanyMst());
			salesDtl.setSalesTransHdr(salesTransHdr);
			salesDtl.setAmount(salesDtlList.get(i).getAmount());
			salesDtl.setAddCessAmount(salesDtlList.get(i).getAddCessAmount());
			salesDtl.setAddCessRate(salesDtlList.get(i).getAddCessRate());
			salesDtl.setBatch(salesDtlList.get(i).getBatch());
			salesDtl.setCessAmount(salesDtlList.get(i).getCessAmount());
			salesDtl.setCessRate(salesDtlList.get(i).getCessRate());
			salesDtl.setCgstAmount(salesDtlList.get(i).getCgstAmount());
			salesDtl.setCgstTaxRate(salesDtlList.get(i).getCgstTaxRate());
			salesDtl.setCompanyMst(salesDtlList.get(i).getCompanyMst());
			salesDtl.setCostPrice(salesDtlList.get(i).getCostPrice());
			salesDtl.setDiscount(salesDtlList.get(i).getDiscount());
			salesDtl.setExpiryDate(salesDtlList.get(i).getExpiryDate());
			salesDtl.setFbKey(salesDtlList.get(i).getFbKey());
			salesDtl.setFcAmount(salesDtlList.get(i).getFcAmount());
			salesDtl.setFcCessAmount(salesDtlList.get(i).getFcCessAmount());
			salesDtl.setFcCessRate(salesDtlList.get(i).getFcCessRate());
			salesDtl.setFcCgst(salesDtlList.get(i).getFcCgst());
			salesDtl.setFcDiscount(salesDtlList.get(i).getFcDiscount());
			salesDtl.setFcIgstAmount(salesDtlList.get(i).getFcIgstAmount());
			salesDtl.setFcMrp(salesDtlList.get(i).getFcMrp());
			salesDtl.setFcRate(salesDtlList.get(i).getFcRate());
			salesDtl.setFcSgst(salesDtlList.get(i).getFcSgst());
			salesDtl.setFcStandardPrice(salesDtlList.get(i).getFcStandardPrice());
			salesDtl.setFcTaxAmount(salesDtlList.get(i).getFcTaxAmount());
			salesDtl.setFcTaxRate(salesDtlList.get(i).getFcTaxRate());

			// salesDtl.setId(salesDtlList.get(i).getId());

			salesDtl.setIgstAmount(salesDtlList.get(i).getIgstAmount());
			salesDtl.setIgstTaxRate(salesDtlList.get(i).getIgstTaxRate());
			salesDtl.setItemId(salesDtlList.get(i).getItemId());
			salesDtl.setItemName(salesDtlList.get(i).getItemName());
			salesDtl.setItemTaxaxId(salesDtlList.get(i).getItemTaxaxId());
			salesDtl.setKotDescription(salesDtlList.get(i).getKotDescription());
			salesDtl.setListPrice(salesDtlList.get(i).getListPrice());
			salesDtl.setMrp(salesDtlList.get(i).getMrp());
			salesDtl.setOfferReferenceId(salesDtlList.get(i).getOfferReferenceId());
			salesDtl.setOldId(salesDtlList.get(i).getOldId());
			salesDtl.setPrintKotStatus(salesDtlList.get(i).getPrintKotStatus());
			salesDtl.setProcessInstanceId(salesDtlList.get(i).getProcessInstanceId());
			salesDtl.setQty(salesDtlList.get(i).getQty());
			salesDtl.setRate(salesDtlList.get(i).getRate());
			salesDtl.setRateBeforeDiscount(salesDtlList.get(i).getRateBeforeDiscount());
			salesDtl.setReturnedQty(salesDtlList.get(i).getReturnedQty());
			salesDtl.setSalesTransHdr(salesTransHdr);
			salesDtl.setSchemeId(salesDtlList.get(i).getSchemeId());
			salesDtl.setSgstAmount(salesDtlList.get(i).getSgstAmount());
			salesDtl.setSgstTaxRate(salesDtlList.get(i).getSgstTaxRate());
			salesDtl.setStandardPrice(salesDtlList.get(i).getStandardPrice());
			salesDtl.setStatus(salesDtlList.get(i).getStatus());
			salesDtl.setTaskId(salesDtlList.get(i).getTaskId());
			salesDtl.setTaxRate(salesDtlList.get(i).getTaxRate());
			salesDtl.setUnitId(salesDtlList.get(i).getUnitId());
			salesDtl.setUnitName(salesDtlList.get(i).getUnitName());
			salesDtl.setUpdatedTime(salesDtlList.get(i).getUpdatedTime());
			salesDtl.setWarrantySerial(salesDtlList.get(i).getWarrantySerial());
			salesDtl.setBarcode(salesDtlList.get(i).getBarcode());
			salesDtl.setAddCessAmount(salesDtlList.get(i).getAddCessAmount());

			return salesDtl = salesDetailsRepository.save(salesDtl);

		}
		return salesDtl;

	}

//}

	// -----------Sibi 22-04-2021-----------------//

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/{salesman}/{tablename}/{kotnumber}/printkotmobile")
	public List<SalesTransHdr> printKotBySalesmanAndKotNumber(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("salesman") String salesman,
			@PathVariable("kotnumber") String kotnumber, @PathVariable("tablename") String tablename,
			@RequestParam("date") String fdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");

		return salesTransHdrService.printKotBySalesmanAndKotNumber(salesman, tablename, kotnumber, date);

	}

	@GetMapping("{companymstid}/{branchcode}/salestranshdr/{salesman}/{tablename}/{kotnumber}/runningkotmobile")
	public List<SalesTransHdr> printRunningKot(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("salesman") String salesman,
			@PathVariable("kotnumber") String kotnumber, @PathVariable("tablename") String tablename) {

		return salesTransHdrService.printRunningKot(salesman, tablename, kotnumber);

	}

	// Print Invoice -----Call API to print KOT. passing Table Name and KOT number
	// -------
	@GetMapping("{companymstid}/{branchcode}/salestranshdr/printkotinvoice/{tablename}/{kotnumber}")
	public List<SalesTransHdr> printKotByTableNameAndKotNumber(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("tablename") String tablename,
			@PathVariable("kotnumber") String kotnumber, @RequestParam("date") String date) {

		java.util.Date vdate = SystemSetting.StringToUtilDate(date, "dd-MM-yyyy");
		return salesTransHdrService.printKotInvoiceByTableNameAndKotNumber(kotnumber, tablename, vdate);

	}

	// -----------------------------------------API---------------------
	// Print Performa Invoice ---Call API to print KOT. passing Table Name and KOT
	// number -------
	@GetMapping("{companymstid}/{branchcode}/salestranshdr/printperformainvoice/{tablename}/{kotnumber}")
	public List<SalesTransHdr> printPerformaByTableNameAndKotNumber(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @PathVariable("tablename") String tablename,
			@PathVariable("kotnumber") String kotnumber, @RequestParam("date") String date) {

		java.util.Date vdate = SystemSetting.StringToUtilDate(date, "dd-MM-yyyy");
		return salesTransHdrService.printPerformaInvoiceByTableNameAndKotNumber(kotnumber, tablename, vdate);

	}
	// -----------------------------------------API---------------------

	@GetMapping("{companymstid}/salestranshdr/getsalestranshdrbyorderid/{orderid}")
	public List<SalesTransHdr> getSalesTransHdrByOrderId(@PathVariable("companymstid") String companymstid,
			@PathVariable("orderid") String orderid) {

		return salesTransHdrRepository.findBySaleOrderHrdId(orderid);

	}

	@GetMapping("{companymstid}/salestranshdr/savesalestranshdridfromsalesorderid/{orderid}")

	public String SaveSalesTransHdrFromSalesOrderId(@PathVariable("orderid") String orderid) {
		SalesTransHdr salesTransHdr = salesTransHdrService.saveSalesTransHdrFromSalesOrderId(orderid);

		return salesTransHdr.getId();

	}

	// .........................sibi....23-06-2021........////

	@GetMapping("{companymstid}/salestranshdr/getpharmacydayendreport/{branchCash}")
	public List<PharmacyDayEndReport> getpharmacydayendreport(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "fromdate") String fromdate, @PathVariable(value = "branchCash") String branchCash)

	{

		Date fudate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");

		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return salesTransHdrService.getPharmacyDayEndreport(fudate, branchCash, companyMst);

	}

	@GetMapping("{companymstid}/salestranshdr/getpharmacycashsales/{branchCash}")
	public Double getPharmacyCashSales(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "fromdate") String fromdate, @PathVariable(value = "branchCash") String branchCash) {

		Date fudate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");

		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return salesTransHdrService.getPharmacyCashSales(fudate, branchCash, companyMst);

	}

	@GetMapping("{companymstid}/salestranshdr/getpharmacycashrevceived/{branchCash}")
	public Double getPharmacyCashReceived(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "fromdate") String fromdate, @PathVariable(value = "branchCash") String branchCash) {

		Date fudate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");

		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return salesTransHdrService.getPharmacyCashReceived(fudate, branchCash, companyMst);

	}

	@GetMapping("{companymstid}/salestranshdr/getpharmacycashpaid/{branchCash}")
	public Double getPharmacyCashPaid(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "fromdate") String fromdate, @PathVariable(value = "branchCash") String branchCash) {

		Date fudate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");

		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return salesTransHdrService.getPharmacyCashPaid(fudate, branchCash, companyMst);

	}

	@GetMapping("{companymstid}/salestranshdr/getpharmacyopeningcash/{branchCash}")
	public Double getPharmacyOpeningCashBalance(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "fromdate") String fromdate, @PathVariable(value = "branchCash") String branchCash) {

		Date fudate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");

		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return salesTransHdrService.getPharmacyCashBalance(fudate, branchCash, companyMst);

	}

	@GetMapping("{companymstid}/salestranshdr/getpharmacyclosingcash/{branchCash}")
	public Double getPharmacyClosingCash(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "fromdate") String fromdate, @PathVariable(value = "branchCash") String branchCash) {

		Date fudate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");

		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return salesTransHdrService.getPharmacyClosingCashBalance(fudate, branchCash, companyMst);

	}

	@GetMapping("{companymstid}/salestranshdr/getaccountidbyaccountname/{branchCash}")
	public String getAccountIdbyAccountName(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchCash") String branchCash) {
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return salesTransHdrService.getAccountIdByAccountName(branchCash);

	}

	
	//=============MAP-131-api-for-fetch-kot-details===============anandu==================
	@GetMapping("{companymstid}/salestranshdrresource/getkotdetail")
	public List<KOTItems> getKotDetail(@PathVariable(value = "companymstid") String companymstid) {
		return salesTransHdrService.getKotDetail();

	}
	
	
	
	//=============MAP-203-api-for-sale-in-web-using-angular=================anandu=========12-10-2021
	@PostMapping("{companymstid}/salestranshdrresource/savesalesinweb")
	public SalesInWeb saveSalesInWeb(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SalesInWeb salesInWeb) {
		return salesTransHdrService.saveSalesInWeb(salesInWeb,companymstid);

		
	}
	
	//=============MAP-493-api-for-holded-pos-details===============anandu=============07-12-2021=====
		@GetMapping("{companymstid}/salestranshdrresource/getposdetail/{branch}")
		public ArrayList getPosDetail(@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "branch") String branch) {
			return salesTransHdrRepo.getPosDetail(companymstid,branch);

		}
		
		//============online sales============//
		
		@PostMapping("{companymstid}/salestranshdrresource/saveonlinesales/{branch}/"
				+ "{userid}/{salesmode}/{salestype}/{customerid}/{localcustomerid}/{salesman}")
		public List<SalesDtl> saveOnlineSales(@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "userid") String userid,@PathVariable(value = "branch") String branch,
				@PathVariable(value = "salesmode") String salesmode,
				@PathVariable(value = "salestype") String salestype,
				@PathVariable(value = "customerid") String customerid,
				@PathVariable(value = "localcustomerid") String localCustomerId,
				@PathVariable(value = "salesman") String salesMan,
				@Valid @RequestBody SalesDtl salesDtl) {
			
			CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
			
			
			
			salesDtl.setCompanyMst(companyMst);
			UnitMst unit = unitMstRepository.findByUnitName(salesDtl.getUnitName());
			if(unit.getId()!=null) {
			salesDtl.setUnitId(unit.getId());
			}
			
				if (salesDtl.getSalesTransHdr().getId()==null)
				{
					SalesTransHdr salesTransHdrResp =salesTransHdrService.createSalesTransHdr(companyMst, branch, 
							userid,salestype, customerid,salesmode, localCustomerId, salesMan);
					salesDtl.setSalesTransHdr(salesTransHdrResp);
					salesDtl = taxMstService.calculateAndSetTaxToSalesDtl(salesDtl);
				
					salesDtl = salesDetailsService.saveSalesDtlForOnlinePos(salesDtl,branch);
				}
			
				else 
				{
					salesDtl = taxMstService.calculateAndSetTaxToSalesDtl(salesDtl);
					salesDtl = salesDetailsService.saveSalesDtlForOnlinePos(salesDtl,branch);
				}
			
			
			List<SalesDtl> salesDtlList = salesDetailsRepo.findBySalesTransHdr(salesDtl.getSalesTransHdr());
			
			return salesDtlList;
		}
		
		
		
		
		/*
		 * new single url for common final save of sales including 
		 * sales_trans_hdr, sales_receipts, account_receivable
		 * jan 14 2022
		 */
		@PutMapping("{companymstid}/salestranshdrresource/salesfinalsave/{store}")
		public void FinalSaveForSales(
				 @PathVariable("companymstid") String companymstid,
				 @PathVariable("store") String store, 
				 @RequestParam("logdate") String logdate,
				@Valid @RequestBody SalesFinalSave salesFinalSave) {
			
			Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
			
			salesFinalSaveService.finalSaveForSales(salesFinalSave,companyMst.get(),store,logdate);
			
			
			
		}
}
