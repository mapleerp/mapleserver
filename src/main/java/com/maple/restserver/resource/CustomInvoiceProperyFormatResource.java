package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CustomInvoicePropertyFormat;
import com.maple.restserver.entity.DamageHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CustomInvoicePropertyFormatRepository;

@RestController
public class CustomInvoiceProperyFormatResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	CustomInvoicePropertyFormatRepository customInvoiceProperyFormatRepo;
	
	@PostMapping("{companymstid}/custominvoicepropertyformat/savecustominvoicepropertyformat")
	public CustomInvoicePropertyFormat createCustomInvoicePropertyFormat(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 CustomInvoicePropertyFormat  customInvoicePropertyFormat)
	{
		
		
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		
		CompanyMst companyMst = companyMstOpt.get();
		customInvoicePropertyFormat.setCompanyMst(companyMst);
		
	   return customInvoiceProperyFormatRepo.save(customInvoicePropertyFormat);
	}
	@GetMapping("{companymstid}/custominvoicepropertyformat/fetchallcustominvoicepropertyformat")
	public List<CustomInvoicePropertyFormat> retrieveAllCustomInvoicePropertyFormat(
			@PathVariable(value="companymstid") String companymstid){
		return customInvoiceProperyFormatRepo.findAll();
	}


	@DeleteMapping("{companymstid}/custominvoicepropertyformat/deletbyid/{id}")
	public void deleteCustomInvoiceProperty(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="id") String id)
	{
		customInvoiceProperyFormatRepo.deleteById(id);
	}
	@GetMapping("{companymstid}/custominvoicepropertyformat/findbycustomsalesmode/{customsalesmode}")
	public List<CustomInvoicePropertyFormat> CustomInvoicePropertyFormatByCustomSalemode(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="customsalesmode") String customsalesmode){
		return customInvoiceProperyFormatRepo.findByCustomSalesMode(customsalesmode);
	}
	
}
