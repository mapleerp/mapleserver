package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemNutrition;
import com.maple.restserver.entity.TaxLedger;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.TaxLedgerRepository;

@RestController
public class TaxLedgerResource {
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	
	@Autowired
	TaxLedgerRepository taxLedgerRepository;
	
	//save 
		@PostMapping("{companymstid}/taxledgerresource/savetaxledger")
		public TaxLedger createTaxLedger(
				@PathVariable(value="companymstid")	String commpanymstid,
				@Valid @RequestBody TaxLedger taxLedger)
		{
			CompanyMst companymst=CompanyMstRepository.findById(commpanymstid).get();
			taxLedger.setCompanyMst(companymst);
			taxLedger=taxLedgerRepository.save(taxLedger);
			return taxLedger;
		}
		
		
		// delete
		@DeleteMapping("{companymstid}/taxledgerresource/deletetaxledger/{id}")
		public void DeleteTaxLedger(@PathVariable(value = "id") String Id) {
			taxLedgerRepository.deleteById(Id);
		}
		
		
		//showing all 
		@GetMapping("{companymstid}/taxledgerresource/showalltaxledger")
		public List<TaxLedger> getTaxLedger(
				
				@PathVariable(value = "companymstid") String companymstid) {

			Optional<CompanyMst> companyMstOpt = CompanyMstRepository.findById(companymstid);
			
			return taxLedgerRepository.findByCompanyMst(companyMstOpt.get());
		}
		
}
