package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.OwnAccountSettlementDtl;
import com.maple.restserver.entity.ParamValueConfig;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ParamValueConfigRepository;

@RestController
@Transactional
public class ParamValueConfigResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	ParamValueConfigRepository paramValueConfigRepo;
	@PostMapping("{companymstid}/paramvalueconfig")
	public ParamValueConfig createParamValueConfig(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 ParamValueConfig paramValueConfig)
    {
		
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		paramValueConfig.setCompanyMst(companyMst.get());
       return paramValueConfigRepo.saveAndFlush(paramValueConfig);
		 
	}
	
	@GetMapping("{companymstid}/paramvalueconfig/{param}/{branchcode}")
	public ParamValueConfig fetchParamValueConfigByparam(@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="param") String param,
			@PathVariable(value ="branchcode")String branchcode){
		
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);

		return paramValueConfigRepo.findByParamAndCompanyMst(param,companyMst.get());
	}


	@GetMapping("{companymstid}/paramvalueconfig/getallparamvalueconfig")
	public List<ParamValueConfig> fetchParamValueConfigByCompany(
			@PathVariable(value="companymstid") String companymstid
			){
		
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);

		return paramValueConfigRepo.findByCompanyMst(companyMst.get());
	}
	
	@DeleteMapping("{companymstid}/paramvalueconfig/deletebyid/{id}")
	public void DeletepurchaseDtl(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable (value="id") String id) {
		paramValueConfigRepo.deleteById(id);

	}
		
}
