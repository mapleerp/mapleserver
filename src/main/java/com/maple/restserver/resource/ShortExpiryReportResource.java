package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ShortExpiryReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.ShortExpiryService;
import com.maple.restserver.utils.SystemSetting;


@RestController
public class ShortExpiryReportResource {

	
	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	ShortExpiryService shortExpiryService;
	
	
	
	@GetMapping("{companymstid}/shortexpiryreportresource/shortexpiryreport/{branchcode}/{currentdate}")
	public List<ShortExpiryReport> retrieveShortExpiryReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "currentdate") String currentdate
		) {
		
		java.sql.Date currentDate = SystemSetting.StringToSqlDate(currentdate, "yyyy-MM-dd");
		
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return shortExpiryService.getShortExpiry(companyMst,branchcode,currentDate);
	}
	
	
	@GetMapping("{companymstid}/shortexpiryreportresource/catogarywiseshortexpiryreport/{branchcode}/")
	public List<ShortExpiryReport> retrieveCatogaryWiseShortExpiryReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,@RequestParam(value="categoryname")String categorynames
		) {
		
		String categoryNames=categorynames;
		
		String[]array=categoryNames.split(";");
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return shortExpiryService.getCatogarywiseShortExpiry(companyMst,branchcode,array);
	}
	
	
	@GetMapping("{companymstid}/shortexpiryreportresource/itemwiseshortexpiryreport/{branchcode}")
	public List<ShortExpiryReport> retrieveItemwiseShortExpiryReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,@RequestParam(value="itemlist") String itemlist
		) {
		
String itemName=itemlist;
		
		String[]array=itemName.split(";");
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return shortExpiryService.getItemWiseShortExpiry(companyMst,branchcode,array);
	}
	
}
