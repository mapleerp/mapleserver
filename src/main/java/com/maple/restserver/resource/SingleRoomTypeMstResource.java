
package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.SingleRoomTypeMst;
import com.maple.restserver.repository.SingleRoomTypeMstRepository;



@RestController
@Transactional
public class SingleRoomTypeMstResource {
	@Autowired
	private SingleRoomTypeMstRepository singleroommsttypes;
	@GetMapping("{companymstid}/singleroomtype")
	public List<SingleRoomTypeMst> retrieveAlldepartments()
	{
		return singleroommsttypes.findAll();
		
	}
	@PostMapping("{companymstid}/singleroomtype")
	public ResponseEntity<Object>createUser(@Valid @RequestBody SingleRoomTypeMst singleroommsttypes1)
	{
		SingleRoomTypeMst saved=singleroommsttypes.saveAndFlush(singleroommsttypes1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}

}
