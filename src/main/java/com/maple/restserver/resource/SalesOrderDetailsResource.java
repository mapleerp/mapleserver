package com.maple.restserver.resource;


import java.math.BigDecimal;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.SaleOrderEdit;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesOrderDtl;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SchEligibilityAttribInst;
import com.maple.restserver.entity.SchSelectionAttribInst;
import com.maple.restserver.entity.SchemeInstance;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.entity.SummarySalesOderDtl;
import com.maple.restserver.entity.TaxMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SalesOrderDtlRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.SchEligibilityAttribInstRepository;
import com.maple.restserver.repository.SchSelectionAttribInstRepository;
import com.maple.restserver.repository.SchemeInstanceRepository;
import com.maple.restserver.repository.TaxMstRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.repository.UserMstRepository;
import com.maple.restserver.service.task.ProductionFinishedGoodsStock;
import com.maple.restserver.utils.SystemSetting;




@RestController
@Transactional
public class SalesOrderDetailsResource {
	@Autowired
	private SchSelectionAttribInstRepository schSelectionAttribInstRepository;
	@Autowired
	private SalesOrderDtlRepository salesOrderdtlRepo;
	
	@Autowired
	private SalesOrderTransHdrRepository salesOrderhdrRepo;
	
	
	@Autowired
	private ItemMstRepository itemMstRepository;
	
	@Autowired
	private UnitMstRepository unitMstRepository;
	
	@Autowired
	private SchEligibilityAttribInstRepository schEligibilityAttribInstRepository;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	TaxMstRepository taxMstRepository;

	@Autowired
	UserMstRepository userMstRepository;

	@Autowired
	TaxMstResource taxMstResource;
	
	
	@Autowired
	MultiUnitMstRepository multyMstRepository;
	
	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepo;

	@Autowired
	BatchPriceDefinitionResource batchPriceDefinitionResource;
	
	@Autowired
	private PriceDefinitionRepository priceDefinitionRepo;
	@Autowired
	private SchemeInstanceRepository schemeInstanceRepository;

	
	@Autowired
	private ProductionFinishedGoodsStock productionFinishedGoodsStock;

	
	@GetMapping("{companymstid}/salesordertranshdr/{salestranshdrId}/salesorderdtl")
	public List<SalesOrderDtl> retrieveAllSalesDtlBySalesTransHdrId(@PathVariable (value = "salestranshdrId") String salesTransHdrID,
            Pageable pageable) {
		return salesOrderdtlRepo.findBySalesOrderTransHdrId(salesTransHdrID,pageable);
	}
	
	
	@PostMapping("{companymstid}/salesordertranshdr/{salesordertranshdrId}/salesorderdtl")
	public SalesOrderDtl createSalesDtl(@PathVariable (value = "salesordertranshdrId") String salesordertranshdrId,
            @Valid @RequestBody SalesOrderDtl salesDtlRequest) 
	{
		 
		
		 return salesOrderhdrRepo.findById(salesordertranshdrId).map(salesordertranshdr -> {
			 salesDtlRequest.setSalesOrderTransHdr(salesordertranshdr);
	            return salesOrderdtlRepo.saveAndFlush(salesDtlRequest);
	        }).orElseThrow(() -> new ResourceNotFoundException("salesordertranshdrId " + salesordertranshdrId + " not found"));

		 
	}
	
	@DeleteMapping("{companymstid}/deletesalesorderdtls/{salesdtlId}")
	public void salesDtlDelete(@PathVariable String salesdtlId) {
		salesOrderdtlRepo.deleteById(salesdtlId);

	}

	@GetMapping("{companymstid}/salesorderdetail/{salesordertranshdr}/{itemid}/{batch}/getsalesorderdetailsbyitem")
	public List<SalesOrderDtl> retrieveAllSalesOrderDtlByItemIdAndBatch(
			@PathVariable(value = "salesordertranshdr") SalesOrderTransHdr salesOrderTransHdr,
			@PathVariable(value = "itemid") String itemId, @PathVariable(value = "batch") String batch,
			@PathVariable(value = "companymstid") String companymstid) {

		// return
		// salesDetailsService.getSalesDetailItemQty(companymstid,salesTransHdrID,itemId,batch);
		return salesOrderdtlRepo.findBySalesOrderTransHdrAndItemIdAndBatch(salesOrderTransHdr, itemId, batch);
	}

 
	 @PutMapping("{companymstid}/salesordertranshdr/{salestransHdrId}/salesorderdtl/{salesdtlId}")
	    public SalesOrderDtl updateSalesDtl(@PathVariable (value = "salestransHdrId") String salestransHdrId,
	                                 @PathVariable (value = "salesdtlId") String salesdtlId,
	                                 @Valid @RequestBody SalesOrderDtl salesDtlRequest) {
	        if(!salesOrderhdrRepo.existsById(salestransHdrId)) {
	            throw new ResourceNotFoundException("salestransHdrId " + salestransHdrId + " not found");
	        }

	        return salesOrderdtlRepo.findById(salesdtlId).map(salesDtl -> {
	        	salesDtl.setQty(salesDtlRequest.getQty());
	        	 
	        	
	            return salesOrderdtlRepo.saveAndFlush(salesDtlRequest);
	        }).orElseThrow(() -> new ResourceNotFoundException("salestransHdrId " + salestransHdrId + "not found"));
	    }
	
	 //--------------------new version 2.2 surya
	//--------------------------------now we are not using this url, please use 'saleorderdtl/{saleOrdrTransHdrId}/saleorderwindowsummaryfordiscount'
	  @GetMapping("{companymstid}/salesordertranshdr/{salesOrderTransHdrId}/saleorderwindowsummary")
	  public SummarySalesOderDtl getSalesWindowSummaryByHdrId(
	  
	  @PathVariable(value ="salesOrderTransHdrId") String salesOrderTransHdrId) {
	  List<Object> objList = salesOrderdtlRepo.findSumSalesOrderDtl(salesOrderTransHdrId);
	  
	  Object[] objAray = (Object[]) objList.get(0);
	  
	  SummarySalesOderDtl summary = new  SummarySalesOderDtl(); 
	  summary.setTotalQty((Double) objAray[0]);
	  
	  summary.setTotalAmount((Double) objAray[0]);
	  
	  summary.setTotalTax((Double)objAray[1]);
	  
	  summary.setTotalCessAmt((Double)objAray[2]);


	


	  
	  return summary;
	  
	  }
	  
	  @GetMapping("{companymstid}/saleorderdtl/{saleOrdrTransHdrId}/saleorderwindowsummaryfordiscount")
		public SummarySalesOderDtl getSaleOrderWindowDiscountSummaryByHdrId(
				@PathVariable(value = "saleOrdrTransHdrId") String saleOrdrTransHdrId) {

			return saleOrderSummaryDiscount(saleOrdrTransHdrId);

		}
	  

		private SummarySalesOderDtl saleOrderSummaryDiscount(String saleOrdrTransHdrId) {
			
			Optional<SalesOrderTransHdr> salesOrderTransHdrOpt = salesOrderhdrRepo.findById(saleOrdrTransHdrId);
			SalesOrderTransHdr salesOrderTransHdr = salesOrderTransHdrOpt.get();
			if(null == salesOrderTransHdr)
			{
				return null;
			}
			
			SummarySalesOderDtl summary = new SummarySalesOderDtl();

			
			if(salesOrderTransHdr.getAccountHeads().getPartyGst().length() > 13)
			{
				List<Object> objList = salesOrderdtlRepo.findSumOfSaleOrderDtlforDiscount(saleOrdrTransHdrId);

				Object[] objAray = (Object[]) objList.get(0);

				summary.setTotalQty((Double) objAray[0]);
				summary.setTotalAmount((Double) objAray[1]);
				summary.setTotalTax((Double) objAray[2]);
				summary.setTotalCessAmt((Double) objAray[3]);
			} else {
				
				List<Object> objList = salesOrderdtlRepo.findSumOfB2CSaleOrderDtlforDiscount(saleOrdrTransHdrId);

				Object[] objAray = (Object[]) objList.get(0);

				summary.setTotalQty((Double) objAray[0]);
				summary.setTotalAmount((Double) objAray[1]);
				summary.setTotalTax((Double) objAray[2]);
				summary.setTotalCessAmt((Double) objAray[3]);
			}
			

			return summary;

		}
		
		@GetMapping("{companymstid}/saleorderdtl/{saleOrdrTransHdrId}/saleorderwindowsummary")
		public SummarySalesOderDtl getSalesOrderWindowSummaryByHdrId(
				@PathVariable(value = "saleOrdrTransHdrId") String saleOrdrTransHdrId) {

			return salessummary(saleOrdrTransHdrId);

		}
		
		private SummarySalesOderDtl salessummary(String saleOrdrTransHdrId) {
			List<Object> objList = salesOrderdtlRepo.findSumSaleOrderDtl(saleOrdrTransHdrId);

			Object[] objAray = (Object[]) objList.get(0);

			SummarySalesOderDtl summary = new SummarySalesOderDtl();
			summary.setTotalQty((Double) objAray[0]);
			summary.setTotalAmount((Double) objAray[1]);
			summary.setTotalTax((Double) objAray[2]);
			summary.setTotalCessAmt((Double) objAray[3]);

			return summary;

		}
	 
		 //--------------------new version 2.2 surya end

	  @GetMapping("{companymstid}/salesorderdetailsresource/web/{itemname}/{qty}/{transhdrid}")
		public SalesOrderDtl addSalesOrderItem(@PathVariable (value = "itemname") String itemname,
				@PathVariable (value = "qty") String qty,
				@PathVariable (value = "transhdrid") String transhdrid) 
		{
			 
			// Find Item Based On Item Name
		  itemname = itemname.replaceAll("%20"," ");
			ItemMst item = itemMstRepository.findByItemName(itemname);
			
			UnitMst unit = unitMstRepository.findById(item.getUnitId()).get();
			
			//find salesOrderTransHdr;
			
			Double dQty = Double.parseDouble(qty);
			
			
			SalesOrderTransHdr salesOrderTransHdr = salesOrderhdrRepo.findById(transhdrid).get();
			SalesOrderDtl salesOrderDtl = new SalesOrderDtl();
			salesOrderDtl.setItemId(item.getId());
			salesOrderDtl.setItemName(item.getItemName());
			salesOrderDtl.setSalesOrderTransHdr(salesOrderTransHdr);
			salesOrderDtl.setUnitId(item.getUnitId());
			
			salesOrderDtl.setAddCessRate(item.getCess());
			salesOrderDtl.setBarode(item.getBarCode());
			salesOrderDtl.setBatch(MapleConstants.Nobatch);
			salesOrderDtl.setQty(dQty);
			salesOrderDtl.setRate(item.getStandardPrice()*100/(100+item.getTaxRate()));
			salesOrderDtl.setMrp(item.getStandardPrice());
			salesOrderDtl.setTaxRate(item.getTaxRate());
			salesOrderDtl.setCgstTaxRate(item.getTaxRate()/2);
			salesOrderDtl.setSgstTaxRate(item.getTaxRate()/2);
			salesOrderDtl.setAmount(dQty*item.getStandardPrice());
			salesOrderDtl.setUnitId(item.getUnitId());
			salesOrderDtl.setUnitName(unit.getUnitName());
			salesOrderDtl = salesOrderdtlRepo.saveAndFlush(salesOrderDtl);
			 return  salesOrderDtl;
			 
		}
	  
	  @GetMapping("{companymstid}/salesorderdetailsresource/savesaleorderdetailweb")
		public SalesOrderDtl addSalesOrderItemWeb(
				@RequestParam("itemname") String itemname,
				@RequestParam("ordermsg") String ordermsg,
				@RequestParam("orderadvice") String orderadvice,
				@RequestParam("qty") String qty,
				@RequestParam("transhdrid") String transhdrid) {

			itemname = itemname.replaceAll("%20", " ");
			itemname = itemname.replaceAll("%21", "\\[");
			itemname = itemname.replaceAll("%22", "\\]");
			
			
			ordermsg = ordermsg.replaceAll("%20", " ");
			orderadvice = orderadvice.replaceAll("%20", " ");

			ItemMst item = itemMstRepository.findByItemName(itemname);
			if(null == item)
			{
				return null;
			}
			UnitMst unit = unitMstRepository.findById(item.getUnitId()).get();
			Double dQty = Double.parseDouble(qty);

			SalesOrderTransHdr salesOrderTransHdr = salesOrderhdrRepo.findById(transhdrid).get();
			SalesOrderDtl salesOrderDtl = new SalesOrderDtl();
			salesOrderDtl.setItemId(item.getId());
			salesOrderDtl.setItemName(item.getItemName());
			salesOrderDtl.setSalesOrderTransHdr(salesOrderTransHdr);
			salesOrderDtl.setUnitId(item.getUnitId());
			salesOrderDtl.setOrderAdvice(orderadvice);
			salesOrderDtl.setOrderMsg(ordermsg);
			salesOrderDtl.setAddCessRate(item.getCess());
			salesOrderDtl.setBarode(item.getBarCode());
			salesOrderDtl.setBatch(MapleConstants.Nobatch);
			salesOrderDtl.setQty(dQty);
			salesOrderDtl.setRate(item.getStandardPrice() * 100 / (100 + item.getTaxRate()));
			salesOrderDtl.setMrp(item.getStandardPrice());
			salesOrderDtl.setTaxRate(item.getTaxRate());
			salesOrderDtl.setCgstTaxRate(item.getTaxRate() / 2);
			salesOrderDtl.setSgstTaxRate(item.getTaxRate() / 2);
			salesOrderDtl.setAmount(dQty * item.getStandardPrice());
			salesOrderDtl.setUnitId(item.getUnitId());
			salesOrderDtl.setUnitName(unit.getUnitName());
			salesOrderDtl = salesOrderdtlRepo.saveAndFlush(salesOrderDtl);
			return salesOrderDtl;

		}
	  
	  
	  
	// ------------------new version 1.10 surya ---------------------
		@PostMapping("{companymstid}/saleordedtl/{saleordertranshdrid}/savesaleordedtlwithlogdate")
		public SalesOrderDtl createSalesDtl(@PathVariable(value = "saleordertranshdrid") String saleordertranshdrid,
				@PathVariable(value = "companymstid") String companymstid,
				@RequestParam("logdate") String logdate,
				@Valid @RequestBody SalesOrderDtl salesDtlRequest) {
			
			Date udate = SystemSetting.StringToUtilDate(logdate, "yyyy-MM-dd");

			Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
			return salesOrderhdrRepo.findById(saleordertranshdrid).map(saleordertranshdr -> {
				
				salesDtlRequest.getSalesOrderTransHdr().setCompanyMst(companyMst.get());
				

				
				SalesOrderDtl saleOrderEdit = setSaleOrderEditCalculation(salesDtlRequest,udate);
				
				saleOrderEdit.setCompanyMst(companyMst.get());
				saleOrderEdit.setOrderMsg(salesDtlRequest.getOrderMsg());
				
				SalesOrderDtl addedItem = salesOrderdtlRepo
						.findBySalesOrderTransHdrAndItemIdAndBatchAndBarodeAndUnitIdAndRate(saleordertranshdr,
								saleOrderEdit.getItemId(), saleOrderEdit.getBatch(), saleOrderEdit.getBarode(),
								saleOrderEdit.getUnitId(), saleOrderEdit.getRate());
				if (null != addedItem && null != addedItem.getId()
						&& saleOrderEdit.getUnitId().equalsIgnoreCase(addedItem.getUnitId())) {

					addedItem.setQty(saleOrderEdit.getQty());
					addedItem.setCgstAmount(saleOrderEdit.getCgstAmount());
					addedItem.setSgstAmount(saleOrderEdit.getSgstAmount());
					addedItem.setCessAmount(saleOrderEdit.getCessAmount());
					// if(salestranshdr.getCustomerMst().getCustomerDiscount() >= 0)
					// {
					addedItem.setAmount(addedItem.getQty() * (saleOrderEdit.getRate() + saleOrderEdit.getCgstAmount()
							+ saleOrderEdit.getSgstAmount()));
					// }
//					else
//					{
//					addedItem.setAmount(addedItem.getQty() * salesDtlRequest.getMrp());
//					}
					addedItem.setRate(saleOrderEdit.getRate());
					addedItem.setMrp(saleOrderEdit.getMrp());

					addedItem = salesOrderdtlRepo.saveAndFlush(addedItem);

//					offerChecking(addedItem, companyMst.get());
					return addedItem;

				} else {
					salesDtlRequest.setSalesOrderTransHdr(saleordertranshdr);

					SalesOrderDtl salesDtl = salesOrderdtlRepo.saveAndFlush(salesDtlRequest);
//					offerChecking(salesDtl, companyMst.get());
					return salesDtl;
				}

			}).orElseThrow(() -> new ResourceNotFoundException("salestranshdrId " + saleordertranshdrid + " not found"));

		}

		
		
		private SalesOrderDtl setSaleOrderEditCalculation(SalesOrderDtl salesDtlRequest ,Date udate ) {

			List<TaxMst> getTaxMst = taxMstRepository.getTaxByItemId(salesDtlRequest.getItemId());

			if (getTaxMst.size() > 0) {

				salesDtlRequest = calculationByTaxMst(salesDtlRequest);

			} else {

				salesDtlRequest = calculationWithOutTaxMst(salesDtlRequest);
			}
			
			
			Double taxRate = 00.0;
			Double mrpRateIncludingTax = 0.0;
			Optional<ItemMst> itemMst = itemMstRepository.findById(salesDtlRequest.getItemId());
			ItemMst item = itemMst.get();
			taxRate = item.getTaxRate();
			mrpRateIncludingTax = salesDtlRequest.getMrp();
			
			String companyState = salesDtlRequest.getSalesOrderTransHdr().getCompanyMst().getState();
			String customerState = "KERALA";
			try {
				customerState = salesDtlRequest.getSalesOrderTransHdr().getAccountHeads().getCustomerState();
			} catch (Exception e) {

			}

			if (null == customerState) {
				customerState = "KERALA";
			}

			if (null == companyState) {
				companyState = "KERALA";
			}

			if (customerState.equalsIgnoreCase(companyState)) {
				
				salesDtlRequest.setSgstTaxRate(taxRate / 2);
				salesDtlRequest.setCgstTaxRate(taxRate / 2);
				Double cgstAmt = 0.0, sgstAmt = 0.0;
				cgstAmt = salesDtlRequest.getCgstTaxRate() * salesDtlRequest.getQty() * salesDtlRequest.getRate() / 100;
				BigDecimal bdcgstAmt = new BigDecimal(cgstAmt);
				bdcgstAmt = bdcgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				salesDtlRequest.setCgstAmount(bdcgstAmt.doubleValue());
				sgstAmt = salesDtlRequest.getSgstTaxRate() * salesDtlRequest.getQty() * salesDtlRequest.getRate() / 100;
				BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
				bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				salesDtlRequest.setSgstAmount(bdsgstAmt.doubleValue());
				salesDtlRequest.setIgstTaxRate(0.0);
				salesDtlRequest.setIgstAmount(0.0);

			} else {
				
				salesDtlRequest.setSgstTaxRate(0.0);
				salesDtlRequest.setCgstTaxRate(0.0);
				salesDtlRequest.setCgstAmount(0.0);
				salesDtlRequest.setSgstAmount(0.0);
				salesDtlRequest.setIgstTaxRate(taxRate);
				salesDtlRequest.setIgstAmount(salesDtlRequest.getIgstTaxRate() * salesDtlRequest.getQty() * salesDtlRequest.getRate() / 100);

			}

			BigDecimal settoamount = new BigDecimal(salesDtlRequest.getQty() * salesDtlRequest.getRate());
			double includingTax = (settoamount.doubleValue() * salesDtlRequest.getTaxRate()) / 100;
			double amount = settoamount.doubleValue() + includingTax + salesDtlRequest.getCessAmount();
			BigDecimal setamount = new BigDecimal(amount);
			setamount = setamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtlRequest.setAmount(setamount.doubleValue());
			salesDtlRequest.setAddCessRate(0.0);
			
//			PriceDefenitionMst priceDefenitionMst1 = priceDefinitionMstRepo.
//					findByCompanyMstAndPriceLevelName(salesDtlRequest.getCompanyMst(),"MRP");
//			if (null != priceDefenitionMst1) {
//				
//				BatchPriceDefinition batchpriceDef = batchPriceDefinitionResource.PriceDefinitionByItemIdAndUnitAndBatch(
//						salesDtlRequest.getItemId(), priceDefenitionMst1.getId(), salesDtlRequest.getUnitId(),
//						salesDtlRequest.getBatch(), udate);
//				if(null != batchpriceDef)
//				{
//					salesDtlRequest.setMrp(batchpriceDef.getAmount());
//				}
//				else
//				{
//					
//				PriceDefinition priceDefinition = priceDefinitionRepo.
//						findByItemIdAndStartDateCostprice(salesDtlRequest.getItemId(), priceDefenitionMst1.getId(),udate,salesDtlRequest.getUnitId());
//				if (null != priceDefinition) {
//					salesDtlRequest.setMrp(priceDefinition.getAmount());
//				}
//				}
//			}
	//
//			
//			PriceDefenitionMst priceDefenitionMst = priceDefinitionMstRepo.findByCompanyMstAndPriceLevelName(
//					salesDtlRequest.getCompanyMst(),"COST PRICE");
//			
//			if (null != priceDefenitionMst) {
//				
//				PriceDefinition batchpriceDef = priceDefinitionRepo.
//						findByItemIdAndStartDateCostprice(salesDtlRequest.getItemId(), priceDefenitionMst1.getId(),udate,salesDtlRequest.getUnitId());
//				
//				if(null != batchpriceDef)
//				{
//					salesDtlRequest.setCostPrice(batchpriceDef.getAmount());
//				}
//				else
//				{
//					PriceDefinition priceDefinition = priceDefinitionRepo.
//							findByItemIdAndStartDateCostprice(salesDtlRequest.getItemId(), priceDefenitionMst1.getId(),udate,salesDtlRequest.getUnitId());
//				if (null != priceDefinition) {
//					salesDtlRequest.setCostPrice(priceDefinition.getAmount());
//				}
//				}
//			}

			return salesDtlRequest;
		}

		
		private SalesOrderDtl calculationWithOutTaxMst(SalesOrderDtl salesDtlRequest) {
			
			Double taxRate = 00.0;
			Double mrpRateIncludingTax = 0.0;

			
			Optional<ItemMst> itemMst = itemMstRepository.findById(salesDtlRequest.getItemId());
			ItemMst item = itemMst.get();
			taxRate = item.getTaxRate();
			mrpRateIncludingTax = salesDtlRequest.getMrp();
			
			SalesOrderTransHdr salesOrderTransHdr = salesDtlRequest.getSalesOrderTransHdr();

			if (salesDtlRequest.getUnitId().equalsIgnoreCase(item.getUnitId())) {
				salesDtlRequest.setStandardPrice(item.getStandardPrice());
			} else {

				MultiUnitMst multiUnitMst = multyMstRepository.
						findByCompanyMstIdAndItemIdAndUnit1(salesDtlRequest.getSalesOrderTransHdr().getCompanyMst().getId(), 
								salesDtlRequest.getItemId(), salesDtlRequest.getUnitId());
				
				salesDtlRequest.setStandardPrice(multiUnitMst.getPrice());

			}
			if (null != item.getTaxRate()) {
				salesDtlRequest.setTaxRate(item.getTaxRate());
				taxRate = item.getTaxRate();

			} else {
				salesDtlRequest.setTaxRate(0.0);
				taxRate = 0.0;

			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
			salesDtlRequest.setRateBeforeDiscount(rateBeforeTax);
			// if Discount

			// Calculate discount on base price
			if (null != salesOrderTransHdr.getAccountHeads().getDiscountProperty()) {

				if (salesOrderTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF BASE PRICE")) {
					salesDtlRequest = calcDiscountOnBasePrice(salesOrderTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate,salesDtlRequest);

				}
				if (salesOrderTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF MRP")) {
//							salesDtl.setTaxRate(0.0);
					salesDtlRequest = calcDiscountOnMRP(salesOrderTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate,salesDtlRequest);
				}
				if (salesOrderTransHdr.getAccountHeads().getDiscountProperty()
						.equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX")) {
					ambrossiaDiscount(salesOrderTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate,salesDtlRequest);
				}
			} else {

				double cessAmount = 0.0;
				double cessRate = 0.0;
				salesDtlRequest.setRate(rateBeforeTax);
				
				String salesMode = null; 
				if (null == salesOrderTransHdr.getAccountHeads().getPartyGst() || salesOrderTransHdr.getAccountHeads().getPartyGst().length() < 13) {
					salesMode = "B2C";
				} else {
					salesMode = "B2B";
				}
				
				if (salesMode.equalsIgnoreCase("B2C")) {
					if (item.getCess() > 0) {
						cessRate = item.getCess();
						rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

						salesDtlRequest.setRate(rateBeforeTax);
						cessAmount = salesDtlRequest.getQty() * salesDtlRequest.getRate() * item.getCess() / 100;
					} else {
						cessAmount = 0.0;
						cessRate = 0.0;
					}
					salesDtlRequest.setRate(rateBeforeTax);
					salesDtlRequest.setCessRate(cessRate);
					salesDtlRequest.setCessAmount(cessAmount);
				}

				// salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
				double sgstTaxRate = taxRate / 2;
				double cgstTaxRate = taxRate / 2;
				salesDtlRequest.setCgstTaxRate(cgstTaxRate);

				salesDtlRequest.setSgstTaxRate(sgstTaxRate);
				String companyState = salesDtlRequest.getSalesOrderTransHdr().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesOrderTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					salesDtlRequest.setSgstTaxRate(taxRate / 2);

					salesDtlRequest.setCgstTaxRate(taxRate / 2);

					Double cgstAmt = 0.0, sgstAmt = 0.0;
					cgstAmt = salesDtlRequest.getCgstTaxRate() * salesDtlRequest.getQty() * salesDtlRequest.getRate() / 100;
					BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
					bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					salesDtlRequest.setCgstAmount(bdCgstAmt.doubleValue());
					sgstAmt = salesDtlRequest.getSgstTaxRate() * salesDtlRequest.getQty() * salesDtlRequest.getRate() / 100;
					BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
					bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					salesDtlRequest.setSgstAmount(bdsgstAmt.doubleValue());

					salesDtlRequest.setIgstTaxRate(0.0);
					salesDtlRequest.setIgstAmount(0.0);

				} else {
					salesDtlRequest.setSgstTaxRate(0.0);

					salesDtlRequest.setCgstTaxRate(0.0);

					salesDtlRequest.setCgstAmount(0.0);

					salesDtlRequest.setSgstAmount(0.0);

					salesDtlRequest.setIgstTaxRate(taxRate);
					salesDtlRequest.setIgstAmount(
							salesDtlRequest.getIgstTaxRate() * salesDtlRequest.getQty() * salesDtlRequest.getRate() / 100);

				}
			}
			
			return salesDtlRequest;

		}
		
		
		private SalesOrderDtl ambrossiaDiscount(SalesOrderTransHdr saleOrderTransHdr, Double rateBeforeTax, ItemMst item,
				Double mrpRateIncludingTax, double taxRate,SalesOrderDtl salesDtlRequest) {

			if (saleOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
				double discoutAmount = (mrpRateIncludingTax * saleOrderTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
				BigDecimal BrateAfterDiscount = new BigDecimal(discoutAmount);

				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				double newRate = rateBeforeTax - discoutAmount;
				salesDtlRequest.setDiscount(discoutAmount);
				BigDecimal BnewRate = new BigDecimal(newRate);
				BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				salesDtlRequest.setRate(BnewRate.doubleValue());
				BigDecimal BrateBeforeTax = new BigDecimal(rateBeforeTax);
				BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				// salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
//			salesOrderDtl.sets(BrateBeforeTax.doubleValue());
			} else {
				salesDtlRequest.setRate(rateBeforeTax);
			}
			double cessAmount = 0.0;
			double cessRate = 0.0;
			String salesMode = null; 
			if (null == saleOrderTransHdr.getAccountHeads().getPartyGst() || saleOrderTransHdr.getAccountHeads().getPartyGst().length() < 13) {
				salesMode = "B2C";
			} else {
				salesMode = "B2B";
			}
			
			if (salesMode.equalsIgnoreCase("B2C")) {
				if (item.getCess() > 0) {
					cessRate = item.getCess();

					
					
					double discountAmount = (mrpRateIncludingTax * saleOrderTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
					BigDecimal BrateAfterDiscount = new BigDecimal(mrpRateIncludingTax - discountAmount);

					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					
					
//				
					salesDtlRequest.setDiscount(discountAmount);
					
					
					double newRate = BrateAfterDiscount.doubleValue();
					BigDecimal BnewRate = new BigDecimal(newRate);
					BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					BigDecimal BrateBeforeTax = new BigDecimal((newRate * 100) / (100 + taxRate+cessRate));

					BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					salesDtlRequest.setRate(BrateBeforeTax.doubleValue());

					// salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
					// salesDtl.setStandardPrice(BrateBeforeTax.doubleValue());
					
					

					System.out.println("rateBeforeTax---------" + BrateBeforeTax);

//					if (salesTransHdr.getCustomerMst().getCustomerDiscount() > 0) {
//						Double rateAfterDiscount = (100 * rateBeforeTax)
//								/ (100 + salesTransHdr.getCustomerMst().getCustomerDiscount());
//						BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
//						BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//						
//						
//						salesDtl.setRate(BrateAfterDiscount.doubleValue());
//						BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
//						rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//						// salesDtl.setMrp(rateBefrTax.doubleValue());
//						salesDtl.setStandardPrice(rateBefrTax.doubleValue());
//					} else {
//						salesDtl.setRate(rateBeforeTax);
//					}
				

					cessAmount = salesDtlRequest.getQty() * salesDtlRequest.getRate() * item.getCess() / 100;

					/*
					 * Recalculate RateBefore Tax if Cess is applied
					 */

				}
			} else {
				cessAmount = 0.0;
				cessRate = 0.0;
			}

			salesDtlRequest.setCessRate(cessRate);
			salesDtlRequest.setCessAmount(cessAmount);
			
			return salesDtlRequest;

		}
		
		private SalesOrderDtl calcDiscountOnMRP(SalesOrderTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
				Double mrpRateIncludingTax, double taxRate, SalesOrderDtl salesDtlRequest) {
			if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
				Double rateAfterDiscount = (100 * mrpRateIncludingTax)
						/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
				Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

				BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
				BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);

				BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				salesDtlRequest.setRate(BnewrateBeforeTax.doubleValue());
				// BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
				// rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
				// salesDtl.setMrp(BrateAfterDiscount.doubleValue());
//				salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
			} else {
				salesDtlRequest.setRate(rateBeforeTax);
			}
			double cessAmount = 0.0;
			double cessRate = 0.0;

			String salesMode = null; 
			if (null == salesTransHdr.getAccountHeads().getPartyGst() || salesTransHdr.getAccountHeads().getPartyGst().length() < 13) {
				salesMode = "B2C";
			} else {
				salesMode = "B2B";
			}
			
			if (salesMode.equalsIgnoreCase("B2C")) {
				if (item.getCess() > 0) {
					cessRate = item.getCess();

					rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());
					salesDtlRequest.setRateBeforeDiscount(rateBeforeTax);

					System.out.println("rateBeforeTax---------" + rateBeforeTax);

					if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
						Double rateAfterDiscount = (100 * mrpRateIncludingTax)
								/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
						Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

						BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
						BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
						salesDtlRequest.setRate(BrateAfterDiscount.doubleValue());
						BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
						rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
						// salesDtl.setMrp(mrpRateIncludingTax);
//						salesDtl.setStandardPrice(rateBefrTax.doubleValue());
					} else {
						salesDtlRequest.setRate(rateBeforeTax);
					}
					// salesDtl.setRate(rateBeforeTax);

					cessAmount = salesDtlRequest.getQty() * salesDtlRequest.getRate() * item.getCess() / 100;

					/*
					 * Recalculate RateBefore Tax if Cess is applied
					 */

				}
			} else {
				cessAmount = 0.0;
				cessRate = 0.0;
			}

			salesDtlRequest.setCessRate(cessRate);
			salesDtlRequest.setCessAmount(cessAmount);
			
			return salesDtlRequest;

		}
		
		private SalesOrderDtl calcDiscountOnBasePrice(SalesOrderTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
				Double mrpRateIncludingTax, double taxRate,SalesOrderDtl salesDtlRequest) {
			if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
				Double rateAfterDiscount = (100 * rateBeforeTax)
						/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
				BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				salesDtlRequest.setRate(BrateAfterDiscount.doubleValue());
				BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
				rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				// salesDtl.setMrp(rateBefrTax.doubleValue());
//				salesOrderDtl.setStandardPrice(rateBefrTax.doubleValue());
			} else {
				salesDtlRequest.setRate(rateBeforeTax);
			}
			double cessAmount = 0.0;
			double cessRate = 0.0;

			String salesMode = null; 
			if (null == salesTransHdr.getAccountHeads().getPartyGst() || salesTransHdr.getAccountHeads().getPartyGst().length() < 13) {
				salesMode = "B2C";
			} else {
				salesMode = "B2B";
			}
			
			if (salesMode.equalsIgnoreCase("B2C")) {
				if (item.getCess() > 0) {
					cessRate = item.getCess();

					rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());
					salesDtlRequest.setRateBeforeDiscount(rateBeforeTax);
					System.out.println("rateBeforeTax---------" + rateBeforeTax);

					if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
						Double rateAfterDiscount = (100 * rateBeforeTax)
								/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
						BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
						BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
						salesDtlRequest.setRate(BrateAfterDiscount.doubleValue());
						BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
						rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
						// salesDtl.setMrp(rateBefrTax.doubleValue());
//						salesOrder/Dtl.setStandardPrice(rateBefrTax.doubleValue());
					} else {
						salesDtlRequest.setRate(rateBeforeTax);
					}
					// salesDtl.setRate(rateBeforeTax);

					cessAmount = salesDtlRequest.getQty() * salesDtlRequest.getRate() * item.getCess() / 100;

					/*
					 * Recalculate RateBefore Tax if Cess is applied
					 */

				}
			} else {
				cessAmount = 0.0;
				cessRate = 0.0;
			}

			salesDtlRequest.setCessRate(cessRate);
			salesDtlRequest.setCessAmount(cessAmount);
			
			return salesDtlRequest;

		}

		private SalesOrderDtl calculationByTaxMst(SalesOrderDtl salesDtlRequest) {

			List<TaxMst> getTaxMst = taxMstRepository.getTaxByItemId(salesDtlRequest.getItemId());
			SalesOrderTransHdr salesOrderTransHdr = salesDtlRequest.getSalesOrderTransHdr();
			Double mrpRateIncludingTax = salesDtlRequest.getMrp();

			for (TaxMst taxMst : getTaxMst) {

				String companyState = salesDtlRequest.getSalesOrderTransHdr().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesOrderTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
						salesDtlRequest.setCgstTaxRate(taxMst.getTaxRate());
//						BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCgstAmount(CgstAmount.doubleValue());
					}
					if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
						salesDtlRequest.setSgstTaxRate(taxMst.getTaxRate());
//						BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setSgstAmount(SgstAmount.doubleValue());
					}
					salesDtlRequest.setIgstTaxRate(0.0);
					salesDtlRequest.setIgstAmount(0.0);
					TaxMst taxMst1 = taxMstRepository.getTaxByItemIdAndTaxId(salesDtlRequest.getItemId(), "IGST");
					if (null != taxMst1) {

						salesDtlRequest.setTaxRate(taxMst1.getTaxRate());
					}
				} else {
					if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
						salesDtlRequest.setCgstTaxRate(0.0);
						salesDtlRequest.setCgstAmount(0.0);
						salesDtlRequest.setSgstTaxRate(0.0);
						salesDtlRequest.setSgstAmount(0.0);

						salesDtlRequest.setTaxRate(taxMst.getTaxRate());
						salesDtlRequest.setIgstTaxRate(taxMst.getTaxRate());
//							BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//							salesDtl.setIgstAmount(igstAmount.doubleValue());
					}
				}
				String salesMode = null; 
				if (null == salesOrderTransHdr.getAccountHeads().getPartyGst() || salesOrderTransHdr.getAccountHeads().getPartyGst().length() < 13) {
					salesMode = "B2C";
				} else {
					salesMode = "B2B";
				}
				
				if (salesMode.equalsIgnoreCase("B2C")) {
					if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
						salesDtlRequest.setCessRate(taxMst.getTaxRate());
//						BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCessAmount(cessAmount.doubleValue());
					}
				}
				if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
					salesDtlRequest.setAddCessRate(taxMst.getTaxRate());
//					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(),
//							Double.valueOf(txtRate.getText()));
//					salesDtl.setAddCessAmount(cessAmount.doubleValue());
				}

			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + salesDtlRequest.getIgstTaxRate()
					+ salesDtlRequest.getCessRate() + salesDtlRequest.getAddCessRate() + salesDtlRequest.getSgstTaxRate()
					+ salesDtlRequest.getCgstTaxRate());
			salesDtlRequest.setRate(rateBeforeTax);
			BigDecimal igstAmount = taxMstResource.taxCalculator(salesDtlRequest.getIgstTaxRate(), rateBeforeTax);
			salesDtlRequest.setIgstAmount(igstAmount.doubleValue() * salesDtlRequest.getQty());
			BigDecimal SgstAmount = taxMstResource.taxCalculator(salesDtlRequest.getSgstTaxRate(), rateBeforeTax);
			salesDtlRequest.setSgstAmount(SgstAmount.doubleValue() * salesDtlRequest.getQty());
			BigDecimal CgstAmount = taxMstResource.taxCalculator(salesDtlRequest.getCgstTaxRate(), rateBeforeTax);
			salesDtlRequest.setCgstAmount(CgstAmount.doubleValue() * salesDtlRequest.getQty());
			BigDecimal cessAmount = taxMstResource.taxCalculator(salesDtlRequest.getCessRate(), rateBeforeTax);
			salesDtlRequest.setCessAmount(cessAmount.doubleValue() * salesDtlRequest.getQty());
			BigDecimal addcessAmount = taxMstResource.taxCalculator(salesDtlRequest.getAddCessRate(), rateBeforeTax);
			salesDtlRequest.setAddCessAmount(addcessAmount.doubleValue() * salesDtlRequest.getQty());

			return salesDtlRequest;
		}
		
		@GetMapping("{companymstid}/saleorderdetail/{salestranshdrId}/{itemId}/getsaleorderdetailsbyitem")
		public List<SalesOrderDtl> retrieveAllSalesOrderDtlByItemIdAndBatch(
				@PathVariable(value = "salestranshdrId") String salesTransHdrID,
				@PathVariable(value = "itemId") String itemId, 
				@PathVariable(value = "companymstid") String companymstid) {

			Optional<SalesOrderTransHdr> salesOrderTransHdr = salesOrderhdrRepo.findById(salesTransHdrID);
			return salesOrderdtlRepo.findBySalesOrderTransHdrAndItemId(salesOrderTransHdr.get(), itemId);
		}
		
		

		// ------------------new version 1.10 surya ---------------------
		
		
		@GetMapping("{companymstid}/saleorderdetail/getsaleorderdetailbyid/{id}")
		public Optional<SalesOrderDtl> retrieveSalesOrderDtlByID(
				@PathVariable(value = "id") String id,
				@PathVariable(value = "companymstid") String companymstid) {
			

			return salesOrderdtlRepo.findById(id);
		}
//
//		@GetMapping("{companymstid}/salesorderdetail/{salesordertranshdrId}/{itemId}/getsalesorderdetailqty")
//		public Double retrieveAllSalesOrderDtlQtyById(@PathVariable(value = "salesordertranshdrId") String salesOrderTransHdrID,
//				@PathVariable(value = "itemId") String itemId,
//
//				@PathVariable(value = "companymstid") String companymstid) {
//
//			// return
//			// salesDetailsService.getSalesDetailItemQty(companymstid,salesTransHdrID,itemId,batch);
//			return salesOrderdtlRepo.getSalesOrderDetailItemQty(salesOrderTransHdrID, itemId);
//		}
//		
		@GetMapping("{companymstid}/salesorderdetail/bybatch/{salesordertranshdrId}/{itemId}/getsalesorderdetailqty")
		public Double retrieveAllSalesOrderDtlQtyByIdAndBatch(@PathVariable(value = "salesordertranshdrId") String salesOrderTransHdrID,
				@PathVariable(value = "itemId") String itemId,

				@PathVariable(value = "companymstid") String companymstid, @RequestParam(value = "batch") String batch) {

			// return
			// salesDetailsService.getSalesDetailItemQty(companymstid,salesTransHdrID,itemId,batch);
			return salesOrderdtlRepo.getSalesOrderDetailItemQtyByBatch(salesOrderTransHdrID, itemId, batch);
		}

	


}