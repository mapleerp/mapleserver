package com.maple.restserver.resource;

import java.util.Date;

/**
 *@GetMapping("{companymstid}/acceptstocks")
 * 
 *@PostMapping("{companymstid}/acceptstock")
 *@Valid @RequestBody AcceptStock acceptStock
 * 
 */

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseOrderHdr;
import com.maple.restserver.entity.PurchasePriceDefinitionDtl;
import com.maple.restserver.entity.PurchasePriceDefinitionMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.PurchasePriceDefinitionDtlRepository;
import com.maple.restserver.service.PurchasePriceDefinitionService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class PurchasePriceDefinitionDtlResource {

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	PurchasePriceDefinitionDtlRepository purchasePriceDefinitionDtlRepository;

	
	@Autowired
	PurchasePriceDefinitionService purchasePriceDefinitionService;
	
	
	@GetMapping("{companymstid}/purchasepricedefinitiondtl/purchasepricedefinitionmstbyid/{hdrid}")
	public List<PurchasePriceDefinitionDtl> retrievePurchasePriceDefinitionMstByHdrId(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "hdrid") String hdrid) {

		return purchasePriceDefinitionDtlRepository.findByPurchaseHdrIdAndCompanyMstId(hdrid, companymstid);
	}

	@PostMapping("{companymstid}/purchasepricedefinitiondtl/savepurchasepricedefinitiondtl")
	public PurchasePriceDefinitionDtl createPurchasePriceDefinitionDtl(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody PurchasePriceDefinitionDtl purchasePriceDefinitionDtl) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		purchasePriceDefinitionDtl.setCompanyMst(companyMst);

		PurchasePriceDefinitionDtl saved = purchasePriceDefinitionDtlRepository
				.saveAndFlush(purchasePriceDefinitionDtl);

		return saved;

	}
	
	@GetMapping("{companymstid}/purchasepricedefinitiondtl/pricedefinitionupdationfrompurchasebatch/{hdrid}")
	public String PriceDefinitionUpdationFromPurchaseBatch(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "hdrid") String hdrid) {

		return purchasePriceDefinitionService.getPurchasePriceDefinitionMstByHdrIdFromLocalDB(companymstid,hdrid);
	}
	
	
	@DeleteMapping("{companymstid}/purchasepricedefinitiondtl/deletebyid/{id}")
	public void DeletePurchasePriceDefinitionDtlById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {

		 purchasePriceDefinitionDtlRepository.deleteById(id);
	}

}
