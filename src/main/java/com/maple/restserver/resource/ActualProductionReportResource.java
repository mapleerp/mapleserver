package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.ActualProductionReport;
import com.maple.restserver.report.entity.FinishedProduct;
import com.maple.restserver.repository.ActualProductionHdrRepository;
import com.maple.restserver.service.ActualProductionService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ActualProductionReportResource{

@Autowired
ActualProductionService actualProductionService;


	@GetMapping("/{companymstid}/actualproductionreportwithid/{branchcode}")
	public List<ActualProductionReport> actualproductionreport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, 
			@RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	java.util.Date TDate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		return actualProductionService.actualProductionReport(companymstid,branchcode,fDate,TDate);
	}
	@GetMapping("/{companymstid}/actualproductionreport/{branchcode}")
	public List<ActualProductionReport> actualproductionreportPrint(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, 
			@RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	java.util.Date TDate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		return actualProductionService.actualProductionPrintReport(companymstid,branchcode,fDate,TDate);
	}
	
	
	@GetMapping("/{companymstid}/actualproductionreportresource/actualproductionbetweendate/{branchcode}")
	public List<FinishedProduct> actualproductionbetweendate(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, 
			@RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	java.util.Date TDate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		return actualProductionService.actualproductionbetweendate(companymstid,branchcode,fDate,TDate);
	}
	
}
