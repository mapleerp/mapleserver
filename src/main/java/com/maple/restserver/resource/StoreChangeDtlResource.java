package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.StoreChangeDtl;
import com.maple.restserver.entity.StoreChangeMst;
import com.maple.restserver.entity.StoreMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.StoreChangeDtlRepository;
import com.maple.restserver.repository.StoreChangeMstRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
public class StoreChangeDtlResource {

	
	private static final Logger logger = LoggerFactory.getLogger(PurchaseHdrResource.class);
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	

	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	StoreChangeDtlRepository storeChangeDtlRepository;
	
	@Autowired
	StoreChangeMstRepository storeChangeMstRepository;
	
	

	@PostMapping("{companymstid}/storechangedtlresource/storechangedtl/{storechangemstid}")
	public StoreChangeDtl createStoreChangeDtl(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "storechangemstid") String
			  storechangemstid,@Valid @RequestBody 
			  StoreChangeDtl  storeChangeDtl)
	{
	Optional<CompanyMst> companyMst= companyMstRepository.findById(companymstid);
	     storeChangeDtl.setCompanyMst(companyMst.get());
	    Optional< StoreChangeMst> storechangeMstOpt=storeChangeMstRepository.findById(storechangemstid);
	    storeChangeDtl.setStoreChangeMst(storechangeMstOpt.get());
//		return  storeChangeDtlRepository.saveAndFlush(storeChangeDtl);
	    return  saveAndPublishService.saveStoreChangeDtl(storeChangeDtl,mybranch);
		
	}
	
	@GetMapping("{companymstid}/storechangedtlresource/storechangedtlbyhdrid/{storechangehdrid}")
	public List<StoreChangeDtl> storeChangeDtlByHdrId(
			@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "storechangehdrid") String storechangehdrid)
	{
		 Optional<CompanyMst> companyMstOpt =  companyMstRepository.findById(companymstid) ;
		 
		 Optional<StoreChangeMst> storeChangeMstOPt=storeChangeMstRepository.findById(storechangehdrid);
		return storeChangeDtlRepository.findByStoreChangeMstAndCompanyMst(storeChangeMstOPt.get(),companyMstOpt.get());
		
	}
	
	@DeleteMapping("{companymstid}/storechangedtlresource/storechangedtldelete/{id}")
	public void StoreMstDelete(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value = "id") String Id) {
		storeChangeDtlRepository.deleteById(Id);

	}
	
}
