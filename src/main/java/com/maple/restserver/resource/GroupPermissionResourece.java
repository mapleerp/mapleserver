package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.GroupPermissionMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.GroupPermissionRepository;

@RestController
@Transactional
public class GroupPermissionResourece {
	
	@Autowired
	GroupPermissionRepository groupPermisssionRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@PostMapping("{companymstid}/grouppermission")
	public GroupPermissionMst createGroupPermission(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			GroupPermissionMst permissionMst)
	{
	 return companyMstRepo.findById(companymstid).map(companyMst-> {
		 permissionMst.setCompanyMst(companyMst);
		return groupPermisssionRepo.saveAndFlush(permissionMst);
		
	 }).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found")); 
	 
	/*
	 * Work flow
	 * forwardGroupPermission
	 */
	}
	
	
	@GetMapping("{companymstid}/allgrouppermission")
	public List<GroupPermissionMst> retrieveAllGroupPermission(@PathVariable(value = "companymstid") String
			  companymstid)
	{
		return groupPermisssionRepo.findByCompanyMstId(companymstid);
	};

	
	@DeleteMapping("{companymstid}/grouppermissionmst/{id}")
	public void groupPermissionMstDelete(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "id") String Id) {
		groupPermisssionRepo.deleteByIdAndCompanyMstId(Id,companymstid);

	}

	

	@GetMapping("{companymstid}/grouppermissionbyprocessid/{id}")
	public List<GroupPermissionMst> retrieveAllGroupPermissionMst(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "id") String Id){
		return groupPermisssionRepo.findByProcessIdAndCompanyMstId(Id,companymstid);
		
	}
	
	@GetMapping("{companymstid}/grouppermissionbygroupid/{groupid}")
	public List<GroupPermissionMst> getGroupPermissionMst(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "groupid") String groupid){
		return groupPermisssionRepo.findByGroupIdAndCompanyMstId(groupid,companymstid);
		
	}
}
