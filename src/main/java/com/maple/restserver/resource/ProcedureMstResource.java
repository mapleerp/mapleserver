package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.maple.restserver.entity.ProcedureMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.ProcedureMstRepository;

@RestController
@Transactional
public class ProcedureMstResource {
	
	@Autowired
	private ProcedureMstRepository procedureMstRepository;
	
	@GetMapping("{companymstid}/proceduremst")
	public List<ProcedureMst> retrieveAllItemProcedureMst(){
		return procedureMstRepository.findAll();
		
		
	}
	
	@PostMapping("{companymstid}/proceduremst")
	public ResponseEntity<Object> createItemPriceMst(@Valid @RequestBody 
			ProcedureMst procedureMst)
	{
		ProcedureMst saved = procedureMstRepository.saveAndFlush(procedureMst);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/(id)").buildAndExpand(saved.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}

	
	
}
