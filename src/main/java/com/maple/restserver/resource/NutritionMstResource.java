package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.NutritionMst;
import com.maple.restserver.entity.NutritionValueMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.NutrintionMstRepository;

@RestController
public class NutritionMstResource {
	
	@Autowired
	NutrintionMstRepository nutrintionMstRepo;
	
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	@GetMapping("{companymstid}/allnutritionmsts")
	public List<NutritionMst> retrieveAllMulyUnitMst()
	{
		return nutrintionMstRepo.findAll();
	};



	@PostMapping("{companymstid}/nutritionmst")
	public NutritionMst createNutritionMst(@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody  NutritionMst nutritionMst)
	{
		Optional<CompanyMst> companyMst= companyMstRepository.findById(companymstid);
		nutritionMst.setCompanyMst(companyMst.get());
			return nutrintionMstRepo.saveAndFlush(nutritionMst);

	}
	
	

	@GetMapping("{companymstid}/nutritionmsts/findbyname/{nutritionfacts}")
	public NutritionMst retrieveNUtritionFacts(	@PathVariable(value = "nutritionfacts")String nutritionfacts)
	{
		return nutrintionMstRepo.findByNutrition(nutritionfacts);
	};

	
	
	@DeleteMapping("{companymstid}/nutritionfacts/{nutritionfactsid}")
	public void DeleteNutritionMst(@PathVariable(value="companymstid") CompanyMst companymstid,
			@PathVariable (value="nutritionfactsid") String nutritionfactsid) {
		nutrintionMstRepo.deleteById(nutritionfactsid);

	}
	
}
