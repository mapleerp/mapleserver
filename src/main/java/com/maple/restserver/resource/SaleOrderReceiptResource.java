package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.SaleOrderReceipt;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.SaleOrderReceiptReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.SaleOrderReceiptRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.service.SaleOrderReportService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
@RestController
@Transactional
public class SaleOrderReceiptResource {
	
private static final Logger logger = LoggerFactory.getLogger(SaleOrderReceiptResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	
	@Value("${serverorclient}")
	private String serverorclient;
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	SaleOrderReportService saleorderreportService; 
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SaleOrderReceiptRepository saleOrderReceiptRepo;
	
	@Autowired
	private SalesOrderTransHdrRepository salesOrderTransHdrRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@PostMapping("{companymstid}/saleorderreceipts")
	public SaleOrderReceipt createSalesOredrReceipts(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  SaleOrderReceipt saleOrderReceipts)
	{
		
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();		
		saleOrderReceipts.setCompanyMst(companyMst);
			
		SaleOrderReceipt saved = null;
		SaleOrderReceipt saleOrderReceipt = saleOrderReceiptRepo.findBySalesOrderTransHdrAndReceiptModeAndReceiptAmount(saleOrderReceipts.getSalesOrderTransHdr(),saleOrderReceipts.getReceiptMode(),saleOrderReceipts.getReceiptAmount());
		if(null == saleOrderReceipt)
		{
			//saved= saleOrderReceiptRepo.save(saleOrderReceipts);
			
			saved=saveAndPublishService.saveSaleOrderReceipt(saleOrderReceipts, mybranch);
			logger.info("saveSaleOrderReceipt send to KafkaEvent: {}", saved);
		}
		else
		{
			saved = saleOrderReceipt;
		}
		
     	
		return saved;
		}

	
	
	@GetMapping("{companymstid}/retrivesalesorderreceipt/{salesordertrandhdrid}")
	public List<SaleOrderReceipt> retrieveSalesOrderReceipts(
			@PathVariable(value = "companymstid") String  companymstid,@PathVariable(value = "salesordertrandhdrid") String  salesordertrandhdrid
			){
		
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		SalesOrderTransHdr salesOrderTrandHdr=salesOrderTransHdrRepo.findByIdAndCompanyMst(salesordertrandhdrid,companyMst.get());
		return saleOrderReceiptRepo.findByCompanyMstAndSalesOrderTransHdr(companyMst.get(),salesOrderTrandHdr);
	}
	

	@DeleteMapping("{companymstid}/deletesaleorderreceipt/{id}")
	public void SaleOrderReceiptDelete(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value = "id") String Id) {
		saleOrderReceiptRepo.deleteById(Id);

	}
	

	@GetMapping("{companymstid}/salesorderreceiptamount/{salesordertranshdrid}")
	Double retrieveSalesOrderReceiptsAmount(
			@PathVariable(value = "companymstid") String  companymstid,
		
			@PathVariable(value ="salesordertranshdrid") String  salesordertrandhdrid
			 ){
		return saleOrderReceiptRepo.retrieveSalesorderReceiptsAmount(salesordertrandhdrid);
	}
	@GetMapping("{companymstid}/saleorderreceipt/getsaleorderreceiptbymodeanddate/{mode}")
	public List<SaleOrderReceiptReport> retrieveSalesOrderReceiptsByMode(
			@PathVariable(value = "companymstid") String  companymstid,
			@PathVariable(value = "mode") String  mode,
			@RequestParam("rdate")String strDate
			){
		Date vdate = SystemSetting.StringToUtilDate(strDate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return saleorderreportService.getSaleOrderReceiptByDateAndMode(vdate, mode);
	}
	
	
	@GetMapping("{companymstid}/saleorderreceipt/getsaleorderreceiptcard")
	public List<SaleOrderReceiptReport> retrieveSalesOrderReceiptsByCard(
			@PathVariable(value = "companymstid") String  companymstid,
			
			@RequestParam("rdate")String strDate
			){
		Date vdate = SystemSetting.StringToUtilDate(strDate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return saleorderreportService.getSaleOrderReceiptByDateAndCard(vdate);
	}
	
	@GetMapping("{companymstid}/saleorderreceipt/getsaleorderreceiptpreviouscard")
	public List<SaleOrderReceiptReport> retrieveSalesOrderReceiptsPreviousCard(
			@PathVariable(value = "companymstid") String  companymstid,
			
			@RequestParam("rdate")String strDate
			){
		Date vdate = SystemSetting.StringToUtilDate(strDate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return saleorderreportService.getSaleOrderReceiptPreviousCard(vdate);
	}
	
	//----------------------version 6.4 surya 
	@GetMapping("{companymstid}/salesorderreceiptbymode/{salesordertrandhdrid}/{receiptmode}")
	public List<SaleOrderReceipt> retrieveSalesOrderReceipts(
			@PathVariable(value = "companymstid") String  companymstid,
			@PathVariable(value = "receiptmode") String  receiptmode,

			@PathVariable(value = "salesordertrandhdrid") String  salesordertrandhdrid
			){
		
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		SalesOrderTransHdr salesOrderTrandHdr=salesOrderTransHdrRepo.findByIdAndCompanyMst(salesordertrandhdrid,companyMst.get());
		return saleOrderReceiptRepo.findByCompanyMstAndSalesOrderTransHdrAndReceiptMode(companyMst.get(),salesOrderTrandHdr,receiptmode);
	}
	//---------------------version 6.4 surya end
	
	
	
// Regy April 7th
	@GetMapping("{companymstid}/saleorderreceipt/getsoreceiptbymode/{mode}")
	Double getSOReceiptsbyMode(
			@PathVariable(value = "companymstid") String  companymstid,
		
			@PathVariable(value ="mode") String  mode,
			@RequestParam("rdate")String strDate
			 ){
		Date vdate = SystemSetting.StringToUtilDate(strDate, "yyyy-MM-dd");
		//Double  getSaleOrderReceiptSummaryByDatendMode(Date vdate,String mode);
		return saleOrderReceiptRepo.getSaleOrderReceiptSummaryByDatendMode(vdate,mode);
	}
	
	
	// Regy April 7th
		@GetMapping("{companymstid}/saleorderreceipt/getalladvancefordate")
		Double getTotalAdvance(
				@PathVariable(value = "companymstid") String  companymstid,
		 		@RequestParam("rdate")String strDate
				 ){
			Date vdate = SystemSetting.StringToUtilDate(strDate, "yyyy-MM-dd");
			//Double  getSaleOrderReceiptSummaryByDatendMode(Date vdate,String mode);
			return saleOrderReceiptRepo.getTotalAdvanceForDate(vdate);
		}
		
}
