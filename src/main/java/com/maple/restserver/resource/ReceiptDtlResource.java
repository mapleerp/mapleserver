
package com.maple.restserver.resource;


import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.DailyReceiptsSummaryReport;
import com.maple.restserver.repository.ReceiptDtlRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.service.ReceiptEditService;
import com.maple.restserver.service.ReceiptEditServiceImpl;
import com.maple.restserver.service.ReciptDtlService;
import com.maple.restserver.service.ReciptDtlServiceImpl;
import com.maple.restserver.utils.SystemSetting;
import com.maple.restserver.repository.ReceiptDtlRepository;



@RestController
@Transactional
public class ReceiptDtlResource {
	private static final Logger logger = LoggerFactory.getLogger(ReceiptDtlResource.class);
	
	//----------------version 5.1 surya
	@Autowired
	ReciptDtlService reciptDtlService;
	//----------------version 5.1 surya end
	@Autowired
	ReceiptEditServiceImpl receiptEditServiceImpl;
	
	@Autowired
	ReceiptEditService receiptEditService;
	
	
	@Autowired
	private ReceiptHdrRepository receiptHdrRepo;
	
	@Autowired
	private ReceiptDtlRepository receiptDtlRepo;
	
	
	@Autowired
	ReciptDtlServiceImpl reciptDtlServiceImpl;
	@GetMapping("{companymstid}/receipthdr/{receiptId}/receiptdtl")
	
	 public List<ReceiptDtl> getAllPaymentDtlByPaymentHdrId(@PathVariable (value = "receiptId") String receipthdrId,
             Pageable pageable) {
		return receiptDtlRepo.findByReceiptHdrId(receipthdrId);
	}

	 @PostMapping("{companymstid}/receipthdr/{receiptId}/receiptdtl")
	    public ReceiptDtl createReceiotDtl(@PathVariable (value = "receiptId") String receiptId,
	                                 @Valid @RequestBody ReceiptDtl receiptdtlRequest) {
	        return receiptHdrRepo.findById(receiptId).map(receiptHdr -> {
	        	receiptdtlRequest.setReceiptHdr(receiptHdr); 
	            return receiptDtlRepo.saveAndFlush(receiptdtlRequest);
	        }).orElseThrow(() -> new ResourceNotFoundException("receiptId 1" + receiptId + " not found"));
	    }
	
	 
	 @PutMapping("{companymstid}/receipthdr/{receiptHdrId}/receiptdtl/{receiptDtlId}")
	    public ReceiptDtl updatePaymentDtl(@PathVariable (value = "receiptHdrId") String receiptHdrId,
	                                 @PathVariable (value = "receiptDtlId") String receiptDtlId,
	                                 @Valid @RequestBody ReceiptDtl receiptDtlRequest) {
	        if(!receiptHdrRepo.existsById(receiptHdrId)) {
	            throw new ResourceNotFoundException("receiptHdrId " + receiptHdrId + " not found");
	        }

	        return receiptDtlRepo.findById(receiptDtlId).map(receiptdtl -> {
	        	receiptdtl.setAccount(receiptDtlRequest.getAccount());
	        	receiptdtl.setAmount(receiptDtlRequest.getAmount());
	        	
	            return receiptDtlRepo.save(receiptdtl);
	        }).orElseThrow(() -> new ResourceNotFoundException("PaymentDtlId " + receiptDtlId + "not found"));
	    }

	 @PutMapping("{companymstid}/receipthdr/{receiptdtl}/updatereceiptdtl")
	 public ReceiptDtl updateReceiptDtl(@PathVariable (value = "receiptdtl") String receiptdtl,
             @PathVariable (value = "companymstid") String companymstid,
             @Valid @RequestBody ReceiptDtl receiptDtlRequest) {
		 	ReceiptDtl receiptDtl = receiptDtlRepo.findById(receiptdtl).get();
//		 	receiptDtl.setAccount(receiptDtlRequest.getAccount());
//		 	receiptDtl.setAmount(receiptDtlRequest.getAmount());
//		 	receiptDtl.setBankAccountNumber(receiptDtlRequest.getBankAccountNumber());
//		 	receiptDtl.setBranchCode(receiptDtlRequest.getBranchCode());
//		 	receiptDtl.setDebitAccountId(receiptDtlRequest.getDebitAccountId());
//		 	receiptDtl.setDepositBank(receiptDtlRequest.getDepositBank());
//		 	receiptDtl.setInstrumentDate(receiptDtlRequest.getInstrumentDate());
//		 	receiptDtl.setInstnumber(receiptDtlRequest.getInstnumber());
//		 	receiptDtl.setInvoiceNumber(receiptDtlRequest.getInvoiceNumber());
//		 	receiptDtl.setModeOfpayment(receiptDtlRequest.getModeOfpayment());
//		 	receiptDtl.setTransDate(receiptDtlRequest.getTransDate());
//		 	receiptDtl.setRemark(receiptDtlRequest.getRemark());
//		 	receiptDtl = receiptDtlRepo.save(receiptDtl);
		 	receiptEditServiceImpl.deleteReceiptAndOwnAccount(companymstid, receiptDtl.getReceiptHdr().getId(), receiptDtl);
		 	return receiptDtl;
		 
	 }
	 
	 @DeleteMapping("{companymstid}/posts/{receiptHdrId}/receiptdtl/{receiptDtlId}")
	    public ResponseEntity<?> deletePaymentDtl(@PathVariable (value = "receiptHdrId") String receiptHdrId,
	                              @PathVariable (value = "receiptDtlId") String receiptDtlId) {
	        return receiptDtlRepo.findByIdAndId(receiptDtlId, receiptHdrId).map(receiptdtl -> {
	        	receiptDtlRepo.delete(receiptdtl);
	            return ResponseEntity.ok().build();
	        }).orElseThrow(() -> new ResourceNotFoundException("Comment not found with id " + receiptDtlId + " and postId " + receiptHdrId));
	    }
	 
	 @DeleteMapping("{companymstid}/receiptdtl/{receiptdetailId}")
		public void DeleteReceiptDtl(@PathVariable (value="receiptdetailId") String receiptdetailId) {
		 receiptDtlRepo.deleteById(receiptdetailId);

		}
		

	 

		@GetMapping("{companymstid}/receiptdtl/receiptdtlreport/{accountid}")
		public List<ReceiptDtl>  getReciptDtlReport(@PathVariable(value = "companymstid") String
				  companymstid,
				  @PathVariable(value = "accountid") String
				  accountid,
				  @RequestParam("rdate") String reportdate){
			java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		
		
			return reciptDtlServiceImpl.reciptDtlServiceImpl(accountid,date);
		
		}	
		
		@GetMapping("{companymstid}/receiptdtl/receiptsummaryreport")
		public List<DailyReceiptsSummaryReport>  getDailyReciptSummaryReport(
				@PathVariable(value = "companymstid") String companymstid,
				  @RequestParam("rdate") String reportdate){
			java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		
			return receiptEditService.receiptSummaryReport(companymstid,date);
		
		}
		
		//------------------version 4.14
		
				@GetMapping("{companymstid}/receiptdtl/receiptsummaryreportbyreceiptmode/{receiptmode}")
				public DailyReceiptsSummaryReport  getDailyReciptSummaryReportByReceiptMode(
						@PathVariable(value = "companymstid") String companymstid,
						@PathVariable(value = "receiptmode") String receiptmode,
						  @RequestParam("rdate") String reportdate){
					java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
				
					return receiptEditService.receiptSummaryReportByReceiptMode(companymstid,date,receiptmode);
				
				}
				//-------------------version 4.14 end
				
				//-------------------version 5.1 surya
				
				@GetMapping("{companymstid}/receiptdtl/accountwisereceiptsummaryreport/{branchcode}")
				public List<DailyReceiptsSummaryReport>  getDailyReciptSummaryByDateAndAccount(
						@PathVariable(value = "companymstid") String companymstid,
						@PathVariable(value = "branchcode") String branchcode,
						  @RequestParam("rdate") String reportdate){
					
					java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
					return reciptDtlService.getDailyReciptSummaryByDateAndAccount(companymstid,date,branchcode);
				
				}
				//------------------version 5.1 surya end
}

