package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PurchaseSchemeMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PurchaseSchemeMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class PurchaseSchemeMstResource {
	private static final Logger logger = LoggerFactory.getLogger(PurchaseSchemeMstResource.class);
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	PurchaseSchemeMstRepository purchaseSchemeMstRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@PostMapping("{companymstid}/purchaseschememstresource/purchaseschememstsave")
	public PurchaseSchemeMst createPurchaseSchemeMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody PurchaseSchemeMst purchaseSchemeMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			purchaseSchemeMst.setCompanyMst(companyMst);
//			PurchaseSchemeMst saved =  purchaseSchemeMstRepo.save(purchaseSchemeMst);
			PurchaseSchemeMst saved =saveAndPublishService.savePurchaseSchemeMst(purchaseSchemeMst, purchaseSchemeMst.getBranchCode());
			logger.info("PurchaseSchemeMst send to KafkaEvent: {}", saved);
			Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", saved.getId());

			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", saved.getId());

			variables.put("companyid", saved.getCompanyMst());
			variables.put("branchcode", saved.getBranchCode());

			variables.put("REST", 1);
			
			
			variables.put("WF", "forwardPurchaseSchemeMst");
			
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardPurchaseSchemeMst");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			
			variables.put("lmsqid", lmsQueueMst.getId());
			
			eventBus.post(variables);
		 
			return saved;
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}
	
	
	
	@GetMapping("{companymstid}/purchaseschememstresource/purchaseschememstfindall")
	public List<PurchaseSchemeMst> findAllPurchaseSchemeMst(@PathVariable(value="companymstid")String companymstid)
	{
		return purchaseSchemeMstRepo.findAll();
	}
	
	@DeleteMapping("{companymstid}/purchaseschememstresource/purchaseschememstdeletebyid/{id}")
	public void deletePurchaseSchemeMst(@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "id")String id)
	{
		purchaseSchemeMstRepo.deleteById(id);
	}
}
