package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SessionEndClosureMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SessionEndClosureMstRepository;
import com.maple.restserver.service.SessionEndClosureMstService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class SessionEndClosureMstResource {

	
	@Autowired
	SessionEndClosureMstRepository sessionendclosuremstrepo;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	SessionEndClosureMstService sessionEndClosureMstService;
	
	@PostMapping("{companymstid}/sessionendclosuremst")
	public SessionEndClosureMst createSessionEndClosure(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SessionEndClosureMst  sessionEndClosure)
	{
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		sessionEndClosure.setCompanyMst(companyMst);
		SessionEndClosureMst saved = sessionendclosuremstrepo.saveAndFlush(sessionEndClosure);
		
		
		return saved;
	}
	

	@GetMapping("{companymstid}/allsessionendclosuremst")
	public List<SessionEndClosureMst> retrieveSessionEndClosureMstGroup()
	{
		return sessionendclosuremstrepo.findAll();
	};
	
	
	@PutMapping("{companymstid}/sessionendclosurehdr/finalsave/{sessionendclosurehdrid}")
	public SessionEndClosureMst SessionEndClosureFinalSave(@PathVariable String sessionendclosurehdrid,@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody SessionEndClosureMst sessionEndClosureMstRequest)
	{
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		
		
				return sessionendclosuremstrepo.findById(sessionendclosurehdrid).map(sessionEndClosureHdr -> {
					sessionEndClosureHdr.setPhysicalCash(sessionEndClosureMstRequest.getPhysicalCash());
					sessionEndClosureHdr.setCardSale(sessionEndClosureMstRequest.getCardSale());
					sessionEndClosureHdr.setCardSale2(sessionEndClosureMstRequest.getCardSale2());
					sessionEndClosureHdr.setCashSale(sessionEndClosureMstRequest.getCashSale());
					sessionEndClosureHdr.setCompanyMst(comapnyMstOpt.get());
					
					
					SessionEndClosureMst saved = sessionendclosuremstrepo.saveAndFlush(sessionEndClosureHdr);
		            return saved;
		        }).orElseThrow(() -> new ResourceNotFoundException("sessionendclosurehdrid " + sessionendclosurehdrid + " not found"));
				/*
				 * forwardSessionEnd
				 */
				
			
	}

	

	@GetMapping("{companymstid}/getsessionendclosuremst/{userid}/sessionendclosuremst")
	public SessionEndClosureMst retriveSessionEndClosureMst(@PathVariable(value = "userid") String userId,
			@RequestParam("processdate") String processdate ) {
		
	    Date date = SystemSetting.StringToUtilDate(processdate,"dd-MM-yyyy");
		return sessionendclosuremstrepo.findByUserIdAndSessionDate(userId,date);

	}
	
	
    @GetMapping("{companymstid}/getsessionendclosuremstbyuserid/{userid}/date") public
    SessionEndClosureMst retrieveSessionEndClosure(
    		@PathVariable(value = "userid") String userId, 
    		@RequestParam("processdate") String processdate  ) { 
	    	
	    	
	    System.out.println(processdate+"voucher date isssssssssssssssssssssssssssssssssssssss");
			java.util.Date date = SystemSetting.StringToUtilDate(processdate,"dd-MM-yyyy");
		  return  sessionEndClosureMstService.retrieveSessionEndClosure(date,userId);
		  
		  
			  }
	 
    
	
    @GetMapping("{companymstid}/sessionendclosuremstresource/maxofsessionslno") public
   Integer  fetchSessionEndClosure(@PathVariable(value = "companymstid") String companymstid, 
   		@RequestParam("processdate") String date  ) { 
	    	
		java.util.Date udate = SystemSetting.StringToUtilDate(date,"yyyy-MM-dd");

		  return  sessionendclosuremstrepo.findMaxOfSlNo(udate,companymstid);
		  
		  
			  }
	

	@GetMapping("{companymstid}/sessionendclosuremstbyid/{id}")
	public SessionEndClosureMst retriveSessionEndClosureMstById(
			@PathVariable(value = "id") String id,
			@PathVariable(value = "companymstid") String companymstid
			) {
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		
		return sessionendclosuremstrepo.findByIdAndCompanyMst(id,companyMst);

	}
	
	
    @GetMapping("{companymstid}/sessionendclosuremstbyslnoanddate/{slno}") public
    SessionEndClosureMst retrieveSessionEndClosureBy(
    		@PathVariable(value = "slno") String slno, 
    		@PathVariable(value = "companymstid") String companymstid,
    		@RequestParam("date") String processdate  ) { 
	    	
	    java.util.Date date = SystemSetting.StringToUtilDate(processdate,"yyyy-MM-dd");
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		  return  sessionendclosuremstrepo.retrieveSessionEndClosureByDateAndSlno(date,slno,companyMst);
		  
		  
			  }
}
