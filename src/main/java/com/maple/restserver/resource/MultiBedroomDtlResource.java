
package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.MultiBedroomDtl;
import com.maple.restserver.repository.MultiBedroomDtlRepository;





@RestController
@Transactional
public class MultiBedroomDtlResource {
	@Autowired
	private MultiBedroomDtlRepository multibedroomdetail;
	@GetMapping("{companymstid}/multibedroomdtl")
	public List<MultiBedroomDtl> retrieveAlldepartments()
	{
		return multibedroomdetail.findAll();
		
	}
	@PostMapping("{companymstid}/multibedroomdtl")
	public ResponseEntity<Object>createUser(@Valid @RequestBody MultiBedroomDtl multibedroomdtl1)
	{
		MultiBedroomDtl saved=multibedroomdetail.saveAndFlush(multibedroomdtl1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}

}
