package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SchEligibilityAttribInst;
import com.maple.restserver.entity.SchSelectionAttribInst;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SchEligibilityAttribInstRepository;
import com.maple.restserver.service.SaveAndPublishService;
@RestController
@Transactional
public class SchEligibilityAttribInstResource {
	
private static final Logger logger = LoggerFactory.getLogger(PurchaseHdrResource.class);
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	SchEligibilityAttribInstRepository schEligibilityAttribInstRepo;

	@PostMapping("{companymstid}/scheligibilityattribinst")
	public SchEligibilityAttribInst createSchEligibilityAttribInst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  SchEligibilityAttribInst  schEligibilityAttribInst)
	{
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		
		schEligibilityAttribInst.setCompanyMst(companyMst.get());
		
			return  schEligibilityAttribInstRepo.save(schEligibilityAttribInst);
//			return saveAndPublishService.saveSchEligibilityAttribInst(schEligibilityAttribInst, mybranch);
			
		}
	
	@GetMapping("{companymstid}/allscheligibilityattribinst")
	public  @ResponseBody List<SchEligibilityAttribInst> findAllSchEligibilityAttribInst(@PathVariable(value = "companymstid") String
			 companymstid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schEligibilityAttribInstRepo.findByCompanyMst(companyMst.get());
	}
	
	@GetMapping("{companymstid}/eligibilityattribinstbyattributename/{attributename}/{schemeid}")
	public  @ResponseBody SchEligibilityAttribInst findAllSchEligibilityAttribInstByAttrName(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "attributename") String attributename ,
			@PathVariable(value = "schemeid") String schemeid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schEligibilityAttribInstRepo.findByCompanyMstAndAttributeNameAndSchemeId(companyMst.get(), attributename, schemeid);
	}
	
	@GetMapping("{companymstid}/allscheligibilityattribinstbyschemeid/{schemeid}")
	public  @ResponseBody List<SchEligibilityAttribInst> findAllSchEligibilityAttribInstBySchemeId(@PathVariable(value = "companymstid") String
			 companymstid,  @PathVariable(value = "schemeid") String schemeid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schEligibilityAttribInstRepo.findByCompanyMstAndSchemeId(companyMst.get(), schemeid);
	}


	
	@DeleteMapping("{companymstid}/deletescheligibilityattribinst/{id}")
	public void SchEligibilityAttribInst(@PathVariable(value = "id") String id) {
		
	Optional<SchEligibilityAttribInst> schEligibilityAttribInst=null ;
		
		if(null!=id)
		{
		 schEligibilityAttribInst=schEligibilityAttribInstRepo.findById(id);
		}
	
		schEligibilityAttribInstRepo.delete(schEligibilityAttribInst.get());
		
		saveAndPublishService.publishObjectDeletion(schEligibilityAttribInst.get(), 
				mybranch, KafkaMapleEventType.SCHELIGIATTRIBINSTDELETE, schEligibilityAttribInst.get().getId());
		 

	}
	
	
	@GetMapping("{companymstid}/allscheligibilityattribinstbyschemeidandeligibilityid/{eligibilityid}/{schemeid}")
	public  @ResponseBody List<SchEligibilityAttribInst> findAllSchEligibilityAttribInstBySchemeIdAndEligibilityId(@PathVariable(value = "companymstid") String
			 companymstid,  @PathVariable(value = "schemeid") String schemeid , 
			 @PathVariable(value = "eligibilityid") String eligibilityid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schEligibilityAttribInstRepo.findByCompanyMstAndSchemeIdAndEligibilityId(companyMst.get(), schemeid,eligibilityid);
	}
	
	@GetMapping("{companymstid}/eligibilityattribinstbyattributenameandeligibilityid/{attributename}/{schemeid}/{eligibilityid}")
	public  @ResponseBody SchEligibilityAttribInst findAllSchEligibilityAttribInstByAttrNameAndEligibilityid(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "attributename") String attributename ,
			@PathVariable(value = "schemeid") String schemeid,
			@PathVariable(value = "eligibilityid") String eligibilityid 
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schEligibilityAttribInstRepo.findByCompanyMstAndAttributeNameAndSchemeIdAndEligibilityId(companyMst.get(), attributename, schemeid,eligibilityid);
	}
	
}
