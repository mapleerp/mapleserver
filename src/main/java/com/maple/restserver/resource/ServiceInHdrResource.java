package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ServiceInHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ServiceInHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class ServiceInHdrResource {

	private static final Logger logger = LoggerFactory.getLogger(PurchaseHdrResource.class);
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository; 
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	@Autowired
	ServiceInHdrRepository serviceInHdrRepo;
	
	@PostMapping("{companymstid}/serviceinhdr")
	public ServiceInHdr createServiceInHdr(@PathVariable(value = "companymstid") String companymstid,@Valid @RequestBody 
			ServiceInHdr serviceInHdr)
	{
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			serviceInHdr.setCompanyMst(companyMst);
//			ServiceInHdr serviceInHdrSaved = serviceInHdrRepo.saveAndFlush(serviceInHdr);
			ServiceInHdr serviceInHdrSaved = saveAndPublishService.saveServiceInHdr(serviceInHdr,mybranch);
			return serviceInHdrSaved;
	}
	
	@GetMapping("{companymstid}/getserviceinhdrbyid/{hdrid}")
	public ServiceInHdr gerServiceInHdrById(@PathVariable ("hdrid") String hdrid,
			@PathVariable("companymstid") String companymstid)
	
	{
		return serviceInHdrRepo.findById(hdrid).get();
	}
		
	@PutMapping("{companymstid}/serviceinhdr/{servicehdrid}/updateserviceinhdr")
	public ServiceInHdr updateServiceInHdr(
			@PathVariable(value="servicehdrid") String servicehdrid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody ServiceInHdr  serviceInHdrRequest)
	{
			
		ServiceInHdr serviceInHdr = serviceInHdrRepo.findById(servicehdrid).get();
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		serviceInHdr.setCompanyMst(companyMst);
		serviceInHdr.setVoucherDate(serviceInHdrRequest.getVoucherDate());
		serviceInHdr.setVoucherNumber(serviceInHdrRequest.getVoucherNumber());
		serviceInHdr.setAdvanceAmount(serviceInHdrRequest.getAdvanceAmount());
		serviceInHdr.setEstimatedCharge(serviceInHdrRequest.getEstimatedCharge());
		serviceInHdr.setEstimatedDeliveryDate(serviceInHdrRequest.getEstimatedDeliveryDate());
//		serviceInHdr = serviceInHdrRepo.saveAndFlush(serviceInHdr);
		serviceInHdr = saveAndPublishService.saveServiceInHdr(serviceInHdr,mybranch);
		
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", serviceInHdr.getVoucherNumber());
		variables.put("voucherDate", serviceInHdr.getVoucherDate());
		variables.put("id", serviceInHdr.getId());
		variables.put("companyid", serviceInHdr.getCompanyMst());
		variables.put("branchcode", serviceInHdr.getBranchCode());

		variables.put("inet", 0);
		variables.put("REST", 1);

		variables.put("WF", "forwardServiceIn");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));

		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardServiceIn");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);

		return serviceInHdr;
	}
	     
}
