package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.StockTransferInExceptionHdr;
import com.maple.restserver.report.entity.StockTransferInExceptionReportDtl;
import com.maple.restserver.repository.StockTransferInExceptionHdrRepository;
import com.maple.restserver.service.StockTransferInExceptionDtlService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class StockTransferInExceptionReportResource {

	
	
	@Autowired
	StockTransferInExceptionHdrRepository stockTransferInExceptionHdrRepository;
	
	
	@Autowired
	StockTransferInExceptionDtlService stockTransferInExceptionDtlService;
	
	
	@GetMapping("{companymstid}/stocktransferinexceptionhdrresource/stocktransferinhdrexceptionreport{branchcode}")
	public List<StockTransferInExceptionHdr> getStockTransferIntockTransferInExceptionReport(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate){
		java.util.Date Fdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");

		java.util.Date tDate=SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		return stockTransferInExceptionHdrRepository.stockTransferExceptionReport(Fdate,tDate,branchcode, companymstid );

		
	}
	
	
	@GetMapping("{companymstid}/stocktransferinexceptionhdrresource/stocktransferindtlexceptionreport{voucherNumber}")
	public List<StockTransferInExceptionReportDtl> getStockTransferIntockTransferInDtlExceptionReport(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("vouchernumber") String voucherNumber,@RequestParam("fromdate") String fromdate){
		java.util.Date Fdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");

		java.util.Date tDate=SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		return stockTransferInExceptionDtlService. stockTransferInExceptionReort( voucherNumber, tDate);


	}
	
	
	
	
}
