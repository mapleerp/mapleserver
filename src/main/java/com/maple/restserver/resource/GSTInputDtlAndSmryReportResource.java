package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.GSTInputDtlAndSmryReport;
import com.maple.restserver.service.GSTInputDtlAndSmryReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class GSTInputDtlAndSmryReportResource {

	@Autowired
	GSTInputDtlAndSmryReportService gstInputDtlAndSmryReportService;
	
	
	
	//---------------Get GST input details report-----Anandu-------------
			@GetMapping("{companymstid}/gstinputdtlandsmryreportresource/getgstinputdtlandsmry")		
			public List<GSTInputDtlAndSmryReport> getGstInputDtlAndSmry(
					@PathVariable(value = "companymstid") String companymstid,
					@RequestParam("fromdate") String fromDate,
					@RequestParam("todate") String toDate) {
				
				java.util.Date fdate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
				java.util.Date tdate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");

				return gstInputDtlAndSmryReportService.findGstInputDtlAndSmry(fdate,tdate);
			}
}
