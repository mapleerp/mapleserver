package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.maple.restserver.entity.ItemPriceMst;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemPriceMstRepository;

@RestController
@Transactional
public class ItemPriceMstResource {
	
	@Autowired
	private ItemPriceMstRepository itemPriceMstRepository;
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("{companymstid}/itempricemst")
	public List<ItemPriceMst> retrieveAllItemPriceMst(){
		return itemPriceMstRepository.findAll();
		
		
	}
	

	
	@PostMapping("{companymstid}/itempricemst")
	public ItemPriceMst createItemPriceMst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			ItemPriceMst itemPriceMst)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			itemPriceMst.setCompanyMst(companyMst);
       return itemPriceMstRepository.saveAndFlush(itemPriceMst);
		
		
		 }).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
	
	}

	


