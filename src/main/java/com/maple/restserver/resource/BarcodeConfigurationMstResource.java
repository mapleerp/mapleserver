package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BarcodeConfigurationMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.repository.BarcodeConfigurationMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
public class BarcodeConfigurationMstResource {

	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	BarcodeConfigurationMstRepository barcodeConfigurationMstRepository;
	
	
	
	@PostMapping("/{companymstid}/barcodeconfigurationmstresource/addbarcodeconfiguration")
	public BarcodeConfigurationMst createBarcodeConfigurationMst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  BarcodeConfigurationMst  barcodeConfigurationMst)
	{
		Optional<CompanyMst>companyMstOpt=companyMstRepository.findById(companymstid);
	
		barcodeConfigurationMst.setCompanyMst(companyMstOpt.get());
		return  barcodeConfigurationMstRepository.saveAndFlush(barcodeConfigurationMst);
	}

	@PutMapping("{companymstid}/barcodeconfigurationmstresource/{configid}/updatebranchmst")
	public BarcodeConfigurationMst updateBarcodeConfigurationMst(
			@PathVariable(value="configid") String configid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody BarcodeConfigurationMst  barcodeConfigurationMstRequest)
	{
		
		Optional<BarcodeConfigurationMst> barcodeConfigurationMstOpt = barcodeConfigurationMstRepository.findById(configid);
		BarcodeConfigurationMst barcodeConfigurationMst=barcodeConfigurationMstOpt.get();
		barcodeConfigurationMst.setBarcodeX(barcodeConfigurationMstRequest.getBarcodeX());
		barcodeConfigurationMst.setBarcodeY(barcodeConfigurationMstRequest.getBarcodeY());
		barcodeConfigurationMst.setExpiryDateX(barcodeConfigurationMstRequest.getExpiryDateX());
		barcodeConfigurationMst.setExpiryDateY(barcodeConfigurationMstRequest.getExpiryDateY());
		barcodeConfigurationMst.setIngLine1X(barcodeConfigurationMstRequest.getIngLine1X());
		barcodeConfigurationMst.setIngLine1Y(barcodeConfigurationMstRequest.getIngLine1Y());
		barcodeConfigurationMst.setIngLine2X(barcodeConfigurationMstRequest.getIngLine2X());
		barcodeConfigurationMst.setIngLine2Y(barcodeConfigurationMstRequest.getIngLine2Y());
		barcodeConfigurationMst.setItemNameX(barcodeConfigurationMstRequest.getItemNameX());
		barcodeConfigurationMst.setItemNameY(barcodeConfigurationMstRequest.getItemNameY());
		barcodeConfigurationMst.setManufactureDateX(barcodeConfigurationMstRequest.getManufactureDateX());
		barcodeConfigurationMst.setManufactureDateY(barcodeConfigurationMstRequest.getManufactureDateY());
	
	    return barcodeConfigurationMstRepository.save(barcodeConfigurationMst);
	
	}
	
	@GetMapping("{companymstid}/barcodeconfigurationmstresource/{branchcode}/fetchbarcodeconfigurationmst")
	public BarcodeConfigurationMst retrieveBarcodeConfigurationMst(
			@PathVariable("companymstid") String companymstid,@PathVariable(value="branchcode") String branchcode )
	{
		Optional<CompanyMst> companymst = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		return  barcodeConfigurationMstRepository.findByBranchCode(branchcode);
	}
	
	
	

	
	@GetMapping("{companymstid}/barcodeconfigurationmstresource/showallbarcodeconfigurationmst/{branchcode}")
	public BarcodeConfigurationMst showAllBarcodeConfigurationMst(
			@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode)
	{
		Optional<CompanyMst> companymst = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		return  barcodeConfigurationMstRepository.fetchBarcodeConfigMst(branchcode);
	}
	

	
	@GetMapping("{companymstid}/barcodeconfigurationmstresource/showallbarcodeconfigurationmstbyid/{id}")
	public Optional<BarcodeConfigurationMst> getBarcodeConfigurationMst(
			@PathVariable("companymstid") String companymstid,@PathVariable(value="id") String id )
	{
		Optional<CompanyMst> companymst = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		return  barcodeConfigurationMstRepository.findById(id);
	}
	}

