package com.maple.restserver.resource;

import java.net.URI;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AddKotTable;
import com.maple.restserver.entity.AddKotWaiter;
import com.maple.restserver.entity.AdditionalExpense;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AddKotWaiterRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class AddKotWaiterResource {
private static final Logger logger = LoggerFactory.getLogger(AddKotWaiterResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	@Autowired
	private AddKotWaiterRepository addKotWaiterRepository;
	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@GetMapping("/{companymstid}/addkotwaiters")
	public List<AddKotWaiter> retrieveAllAddKotWaiter(@PathVariable(value = "companymstid") String companymstid){
		return addKotWaiterRepository.findByCompanyMstId(companymstid);
	}
	@GetMapping("/{companymstid}/getkotwaiterbystatus/{branchcode}")
	public List<AddKotWaiter> retrievekottablesbystatus(@PathVariable(value = "companymstid") String companymstid){
		return addKotWaiterRepository.getKotWaiterbyStatus(companymstid);
	}
		
	@GetMapping("{companymstid}/getkotwaiterbyname/{waitername}/{branchcode}")

	public AddKotWaiter retrievKotwaiterbyname(@PathVariable(value="companymstid") String companymstid,@PathVariable(value="waitername") String waitername){
		
		
		return addKotWaiterRepository.findByCompanyMstIdAndWaiterName(companymstid, waitername);
	}

	@PostMapping("/{companymstid}/addkotwaiter")
	public AddKotWaiter createDailyStock(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			AddKotWaiter addKotWaiter)
	{
		
		 	return companyMstRepo.findById(companymstid).map(companyMst-> {
		 		addKotWaiter.setCompanyMst(companyMst);
//		 		AddKotWaiter savedAddKotWaiter = addKotWaiterRepository.saveAndFlush(addKotWaiter);
		 		AddKotWaiter savedAddKotWaiter =saveAndPublishService.saveAddKotWaiter(addKotWaiter, addKotWaiter.getBranchCode());
		 		logger.info("AddKotWaiter send to KafkaEvent: {}", savedAddKotWaiter);
		 		 Map<String, Object> variables = new HashMap<String, Object>();

					variables.put("voucherNumber", savedAddKotWaiter.getId());

					variables.put("voucherDate", SystemSetting.getSystemDate());
					variables.put("inet", 0);
					variables.put("id", savedAddKotWaiter.getId());

					variables.put("companyid", savedAddKotWaiter.getCompanyMst());

					variables.put("REST", 1);
					
					variables.put("branchcode", savedAddKotWaiter.getBranchCode());
					variables.put("WF", "forwardKotWaiter");
					
					
					
					
					String workflow = (String) variables.get("WF");
					String voucherNumber = (String) variables.get("voucherNumber");
					String sourceID = (String) variables.get("id");


					LmsQueueMst lmsQueueMst = new LmsQueueMst();

					lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
					//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
					
					java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
					java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
					lmsQueueMst.setVoucherDate(sqlVDate);
					
					lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
					lmsQueueMst.setVoucherType(workflow);
					lmsQueueMst.setPostedToServer("NO");
					lmsQueueMst.setJobClass("forwardKotWaiter");
					lmsQueueMst.setCronJob(true);
					lmsQueueMst.setJobName(workflow + sourceID);
					lmsQueueMst.setJobGroup(workflow);
					lmsQueueMst.setRepeatTime(60000L);
					lmsQueueMst.setSourceObjectId(sourceID);
					
					lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

					lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
					variables.put("lmsqid", lmsQueueMst.getId());
					eventBus.post(variables);
					return savedAddKotWaiter;
		 		
		 	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
					  companymstid + " not found"));
		 	
		/*
		 * Work Flow
		 * 
		 * forwardKotWaiter
		 */
	}
		 

	}
	
	


