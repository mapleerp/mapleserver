package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.maple.restserver.entity.BranchPriceDefinitionMst;
import com.maple.restserver.entity.CompanyMst;


import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.BranchPriceDefinitionRepository;
import com.maple.restserver.repository.CompanyMstRepository;


@RestController
public class BranchPriceDefinitionResource {

	@Autowired
	private BranchPriceDefinitionRepository branchPriceDefinitionRepository;
	@Autowired
	CompanyMstRepository companyMstRepo;

	@GetMapping("{companymstid}/branchpricedefinitionresource/savebranchpricedefitionbyid/{id}")
	public BranchPriceDefinitionMst getBranchPriceDefinitionMstById(
			@PathVariable(value = "companymstid") String companymstid, 
			@PathVariable(value = "id") String id) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}

		return branchPriceDefinitionRepository.findByCompanyMstAndId(companyOpt.get(), id);
	}

	
	@GetMapping("{companymstid}/branchpricedefinitionresource/showallbranchpricedefinition")
	public List<BranchPriceDefinitionMst> getAllBranchpricedefinition(
			@PathVariable(value = "companymstid") String companymstid) {
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}
		
		return branchPriceDefinitionRepository.findByCompanyMst(companyOpt.get());
	}

	
	@DeleteMapping("{companymstid}/branchpricedefinition/deletebranchpricedefinition/{id}")
	public void branchPriceDefinition(
			@PathVariable(value = "id") String id) {
		branchPriceDefinitionRepository.deleteById(id);
	}

	
	@PostMapping("{companymstid}/branchpricedefinition/savebranchpricedefinition")
	public BranchPriceDefinitionMst createBranchPriceDefinitionMst(@PathVariable(value = "companymstid") String
			  companymstid ,@Valid @RequestBody 
	BranchPriceDefinitionMst branchPriceDefinitionMst)
	             {
		 	return companyMstRepo.findById(companymstid).map(companyMst-> {
		 		branchPriceDefinitionMst.setCompanyMst(companyMst);
   		return branchPriceDefinitionRepository.save(branchPriceDefinitionMst);
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
		
}
//Sibi............12.03.2021
