package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.AMDCShortExpiryReport;
import com.maple.restserver.report.entity.BranchStockReport;
import com.maple.restserver.service.AMDCShortExpiryReportService;
import com.maple.restserver.service.BranchStockReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class AMDCShortExpiryReportResource {
	@Autowired
	AMDCShortExpiryReportService aMDCShortExpiryReportService;
	
	@GetMapping("{companymstid}/amdcshortexpiryreportresource/getamdcshortexpiryreport")		
	public List<AMDCShortExpiryReport> getAMDCShortExpiryReport(
			@PathVariable(value = "companymstid") String companymstid,
			
			@RequestParam("date") String ddate) {
		
		java.util.Date date = SystemSetting.StringToUtilDate(ddate, "yyyy-MM-dd");
		

		return aMDCShortExpiryReportService.findAMDCShortExpiryReport(date);
	}
}
