package com.maple.restserver.resource;

import java.net.UnknownHostException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.repository.CompanyMstRepository;




@RestController

public class AMDCDataTransferResource   {
	
	
	@Autowired
	CompanyMstRepository companyMstRepository;
//	@Autowired
//	AMDCDataTransferService aMDCDataTransferService;
	
	@GetMapping("{companymstid}/amdcdatatransferresource/amdcitemtransfer/{branchcode}")
	public String  doAMDCItemTransfer(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.doItemTransfer(comapnyMstOpt.get(),branchcode);
		return null;
	}
	
	@GetMapping("{companymstid}/amdcdatatransferresource/amdcbranchmsttransfer/{branchcode}")
	public String  BranchMstTransferAmdc(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.branchMstTransferAmdc(comapnyMstOpt.get(),branchcode);
		return null;
	}
	
	
	@GetMapping("{companymstid}/amdcdatatransferresource/amdcaccountclasstransfer/{branchcode}")
	public String  AccountClassTransferamdc(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.accountClassTransferamdc(comapnyMstOpt.get(),branchcode);
		return null;
	}
	
	
	@GetMapping("{companymstid}/amdcdatatransferresource/amdcdosalestranshdrtransfer/{branchcode}")
	public String  DoSalesTransHdrTransfer(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.doSalesTransHdrTransfer(comapnyMstOpt.get(),branchcode);
		return null;
	}
	
	
	

	@GetMapping("{companymstid}/amdcdatatransferresource/amdcdopurchasetransfer/{branchcode}")
	public String  DoPurchaseTransfer(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.doPurchaseTransfer(comapnyMstOpt.get(),branchcode);
		return null;
	}
	
	
	
	
	

	@GetMapping("{companymstid}/amdcdatatransferresource/amdcdoitembatchdtltransfer/{branchcode}")
	public String  DoItemBatchDtlTransfer(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.doItemBatchDtlTransfer(comapnyMstOpt.get(),branchcode);
		return null;
	}
	
	

	

	@GetMapping("{companymstid}/amdcdatatransferresource/amdctransfercustomer/{branchcode}")
	public String  TransferCustomer(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.transferCustomer(comapnyMstOpt.get(),branchcode);
		return null;
	}
	
	@GetMapping("{companymstid}/amdcdatatransferresource/amdctransferaccountheads/{branchcode}")
	public String  TransferAccountHeads(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.transferAccountHeads(comapnyMstOpt.get(),branchcode);
		return null;
	}
	
	
	@GetMapping("{companymstid}/amdcdatatransferresource/amdcpatientdtltransferambc/{branchcode}")
	public String  PatientDtlTransferambc(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.patientDtlTransferambc(comapnyMstOpt.get(),branchcode);
		return null;
	}
	
	@GetMapping("{companymstid}/amdcdatatransferresource/amdcinvcategorymsttransferamdc/{branchcode}")
	public String  InvCategoryMstTransferAmdc(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.invCategoryMstTransferAmdc(comapnyMstOpt.get(),branchcode);
		return null;
	}
	
	@GetMapping("{companymstid}/amdcdatatransferresource/amdcdodatatransferusermst/{branchcode}")
	public String  DoDataTransferUserMst(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.doDataTransferUserMst(comapnyMstOpt.get());
		return null;
	}
	
	
	
	
	@GetMapping("{companymstid}/amdcdatatransferresource/amdcdconsumptionhdrtransferamdc/{branchcode}")
	public String  ConsumptionHdrTransferamdc(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.consumptionHdrTransferamdc(comapnyMstOpt.get());
		return null;
	}
	@GetMapping("{companymstid}/amdcdatatransferresource/amdcconsumptiondtltransferamdc/{branchcode}")
	public String  ConsumptionDtlTransferamdc(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchcode) throws UnknownHostException, Exception{
		Optional<CompanyMst> comapnyMstOpt=companyMstRepository.findById(companymstid);
		
//		return aMDCDataTransferService.consumptionDtlTransferamdc(comapnyMstOpt.get());
		return null;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
