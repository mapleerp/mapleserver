package com.maple.restserver.resource;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CashBook;
import com.maple.restserver.entity.CategoryManagementMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CategoryManagementMstRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemStockPopUpRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
@RestController
@Transactional
@CrossOrigin("http://localhost:4200")
public class CategoryMstResource {
	private static final Logger logger = LoggerFactory.getLogger(CategoryMstResource.class);
	EventBus eventBus = EventBusFactory.getEventBus();
	
	@Value("${mybranch}")
	private String myBranch;
	
	@Autowired
	CategoryManagementMstRepository categoryManagementMstRepo;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	private CategoryMstRepository catgegoryMstRepo;

	@Autowired
	CompanyMstRepository companyMstRepo;
	 @Autowired
     SaveAndPublishService saveAndPublishService;
	
	
	
	@Autowired
	private ExternalApi externalApi;
	Pageable topFifty =   PageRequest.of(0, 50);
	
	
	
	
	@Autowired
	private ItemStockPopUpRepository  itemStockPopUpRepository;
	@GetMapping("{companymstid}/categorymsts")
	public List<CategoryMst> retrieveAllCategory(@PathVariable(value = "companymstid") String companymstid) {

		return catgegoryMstRepo.findByCompanyMstId(companymstid);
	}
	
	@GetMapping("{companymstid}/{categoryname}/categorymstbyname")
	public  CategoryMst getcategorymstbyName(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "categoryname") String categoryname) {

		return catgegoryMstRepo.findByCompanyMstIdAndCategoryName(companymstid,categoryname);
	}
	@GetMapping("{companymstid}/categorymst/getparentcategory")
	public  @ResponseBody List<CategoryMst> getParentcategory(	
			@PathVariable(value = "companymstid") String companymstid){
		return catgegoryMstRepo.getParentCategory() ;
	}
	
	

	@PostMapping("{companymstid}/categorymst")
	public CategoryMst createCategoryMst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody CategoryMst categoryMstReq) {
		System.out.println(companymstid);
	 	return companyMstRepo.findById(companymstid).map(companyMst-> {
	 		categoryMstReq.setCompanyMst(companyMst);
	 		
	 		
	 		/*
	 		 * Workflow to post category to be added
	 		 * 
	 		 */
	 		CategoryMst	categoryMst= catgegoryMstRepo.save(categoryMstReq);
	 		
			saveAndPublishService.saveCategoryMst(categoryMstReq,myBranch);
			logger.info("categoryMst send to KafkaEvent: {}", categoryMst);
	 		if(null != categoryMst.getParentId())
	 		{
	 			CategoryManagementMst categorymgmntMst = new CategoryManagementMst();
	 			categorymgmntMst.setCategoryId(categoryMst.getId());
	 			categorymgmntMst.setSubCategoryId(categoryMst.getParentId());
	 			categorymgmntMst.setCompanyMst(categoryMst.getCompanyMst());
     			categoryManagementMstRepo.save(categorymgmntMst);
	 	
	 		
	 		}
		  Map<String, Object> variables = new HashMap<String, Object>();

				variables.put("voucherNumber", categoryMst.getId());

				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("inet", 0);
				variables.put("id", categoryMst.getId());

				variables.put("companyid", categoryMst.getCompanyMst());
				variables.put("branchcode", categoryMst.getBranchCode());

				variables.put("REST", 1);
				
				
				variables.put("WF", "forwardCategory");
				
				
				String workflow = (String) variables.get("WF");
				String voucherNumber = (String) variables.get("voucherNumber");
				String sourceID = (String) variables.get("id");


				LmsQueueMst lmsQueueMst = new LmsQueueMst();

				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
				//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
				
				java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
				java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
				lmsQueueMst.setVoucherDate(sqlVDate);
				
				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
				lmsQueueMst.setVoucherType(workflow);
				lmsQueueMst.setPostedToServer("NO");
				lmsQueueMst.setJobClass("forwardCategory");
				lmsQueueMst.setCronJob(true);
				lmsQueueMst.setJobName(workflow + sourceID);
				lmsQueueMst.setJobGroup(workflow);
				lmsQueueMst.setRepeatTime(60000L);
				lmsQueueMst.setSourceObjectId(sourceID);
				
				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
				
				variables.put("lmsqid", lmsQueueMst.getId());
				
				eventBus.post(variables);
				return categoryMst;
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found")); }
	
	
	@GetMapping("{companymstid}/categorysearch")
	public  @ResponseBody List<CategoryMst> searchCustomer(	
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("data") String searchstring){
		return catgegoryMstRepo.findSearch("%"+searchstring+"%",companymstid) ;
	}
	

	@GetMapping("{companymstid}/categorymst/stockitemsearchbycatname")
	public  @ResponseBody List<Object> searchItemStockWithCom(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid){
		
		Optional<CompanyMst> companyMst1 = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMst1.get();
		searchstring = searchstring.replaceAll("20%", " ");
		
		List<Object> objByItemCode = itemStockPopUpRepository.findItemStockSearchByCategoryName(searchstring, companyMst) ;
		return objByItemCode;
	}
	
	@GetMapping("{companymstid}/categorymst/getchildcategoryusingparentid/{catName}")
	public  @ResponseBody List<CategoryMst> getchildCategoryUsingParentId(	
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "catName")String catName){
		return catgegoryMstRepo.getChildCategory(catName) ;
	}


	@DeleteMapping("{companymstid}/categorymst/deletecategorybyid/{id}")
	public void categoryDelete(@PathVariable(value = "id") String Id) {
		catgegoryMstRepo.deleteById(Id);

	}
	
	@GetMapping("{companymstid}/categorymstbyid/{categoryid}")
	public  CategoryMst getcategorymstbyid(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "categoryid") String categoryid) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return catgegoryMstRepo.findByCompanyMstAndId(companyOpt.get(),categoryid);
	}
	
	@PutMapping("{companymstid}/categorymst/{catid}/updatecategory")
	public CategoryMst updateCategoryMst(
			@PathVariable(value="catid") String catid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody CategoryMst  categoryMstRequest)
	{
			
		CategoryMst categoryMst = catgegoryMstRepo.findById(catid).get();
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		categoryMst.setCompanyMst(companyMst);
		categoryMst.setCategoryName(categoryMstRequest.getCategoryName());
		categoryMst.setId(catid);
		categoryMst.setParentId(categoryMstRequest.getParentId());
	
		categoryMst = catgegoryMstRepo.save(categoryMst);
		
		if(null != categoryMst.getParentId())
 		{
 			CategoryManagementMst categorymgmntMst = new CategoryManagementMst();
 			categorymgmntMst.setCategoryId(categoryMst.getId());
 			categorymgmntMst.setSubCategoryId(categoryMst.getParentId());
 			categorymgmntMst.setCompanyMst(categoryMst.getCompanyMst());
 			categoryManagementMstRepo.save(categorymgmntMst);
 		}
		  Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", categoryMst.getId());

			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", categoryMst.getId());

			variables.put("companyid", categoryMst.getCompanyMst());
			variables.put("branchcode", categoryMst.getBranchCode());

			variables.put("REST", 1);
			
			
			variables.put("WF", "forwardCategory");
			
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardCategory");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			
			variables.put("lmsqid", lmsQueueMst.getId());
			
			eventBus.post(variables);
	
		return categoryMst;
	}

	
	// ------------------new url ----publish offline button

		@PostMapping("{companymstid}/categorymst/{status}")
		public CategoryMst sendOffLineMessageToCloud(@Valid @RequestBody CategoryMst categoryMstReq,
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable("status") String status) {
			 ResponseEntity<CategoryMst> saved=	externalApi.sendcategoryMstOffLineMessageToCloud(companymstid, categoryMstReq);
				
			return saved.getBody();

		}

}

