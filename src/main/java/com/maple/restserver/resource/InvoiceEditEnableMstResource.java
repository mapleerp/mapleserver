package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.InvoiceEditEnableMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.TenantyMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.InvoiceEditEnableRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.TenantyMstRepository;
import com.maple.restserver.repository.UserMstRepository;

import com.maple.restserver.service.InvoiceEditEnableService;

import com.maple.restserver.service.TenantyMstService;

import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class InvoiceEditEnableMstResource {
	@Autowired
	private InvoiceEditEnableRepository invoiceEditEnableRepository;

	@Autowired
	PaymentHdrRepository paymentHdrRepo;

	@Autowired
	ReceiptHdrRepository receiptHdrRepo;

	@Autowired
	PurchaseHdrRepository purchaseHdrRepo;
	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	UserMstRepository userMstRepository;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;

	@Autowired
	BranchMstRepository branchMstRepository;

	@Autowired
	InvoiceEditEnableService invoiceEditEnableService;


	@Autowired
	TenantyMstRepository tenantyMstRepository;


	// -------------------version 6.2 surya
	@Autowired
	SalesOrderTransHdrRepository salesOrderTransHdrRepository;

	// -------------------version 6.2 surya end
	
	@Autowired
	TenantyMstService tenantyMstService;

	@GetMapping("{companymstid}/saveinvoiceeditenable/{username}/{invoiceno}/{voucherType}/{branchcode}")
	public InvoiceEditEnableMst createInvoiceEditEnable(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "username") String username, @PathVariable(value = "voucherType") String voucherType,
			@PathVariable(value = "invoiceno") String invoiceno, @PathVariable(value = "branchcode") String branchcode,
			@RequestParam("rdate") String vouchedate) {

		username = username.replaceAll("%20", " ");
		invoiceno = invoiceno.replaceAll("%20", " ");
		branchcode = branchcode.replaceAll("%20", " ");
		voucherType = voucherType.replaceAll("%20", " ");

		Date date = SystemSetting.StringToUtilDate(vouchedate, "yyyy-MM-dd");
		// -------------------version 6.5 surya
		List<InvoiceEditEnableMst> invoiceEditEnableMstList0 = invoiceEditEnableRepository
				.findByVoucherNumberAndStatusAndVoucherDateAndBranchCode(invoiceno, "N", date, branchcode);
		// -------------------version 6.5 surya end

		if (invoiceEditEnableMstList0.size() > 0) {
			return null;
		}

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		if (null == companyMst) {
			return null;
		}

		UserMst userMst = userMstRepository.findByUserNameAndCompanyMstId(username, companymstid);
		if (null == userMst) {
			return null;
		}

		Optional<BranchMst> branch = branchMstRepository.findByBranchCodeAndCompanyMstId(branchcode, companymstid);
		BranchMst branchMst = branch.get();
		if (null == branch) {
			return null;
		}
		InvoiceEditEnableMst invoiceEditEnableMst = new InvoiceEditEnableMst();
		if (voucherType.equalsIgnoreCase("SALES")) {
			// -------------------version 6.5 surya

//			SalesTransHdr salesTransHdr = salesTransHdrRepository
//					.findByVoucherNumberAndCompanyMstAndBranchCode(invoiceno, companyMst, branchcode);

			SalesTransHdr salesTransHdr = salesTransHdrRepository
					.findByVoucherNumberAndCompanyMstAndBranchCodeAndVoucherDate(invoiceno, companyMst, branchcode,
							date);

			// -------------------version 6.5 surya end

			if (null == salesTransHdr) {
				return null;
			}

			invoiceEditEnableMst.setVoucherDate(salesTransHdr.getVoucherDate());

		} else if (voucherType.equalsIgnoreCase("PURCHASE")) {
			// -------------------version 6.5 surya

//			PurchaseHdr puchaseHdr = purchaseHdrRepo.findByVoucherNumberAndCompanyMstAndBranchCode(invoiceno,
//					companyMst, branchcode);

			PurchaseHdr puchaseHdr = purchaseHdrRepo.findByVoucherNumberAndCompanyMstAndBranchCodeAndVoucherDate(
					invoiceno, companyMst, branchcode, date);
			// -------------------version 6.5 surya end

			if (null == puchaseHdr) {
				return null;
			}
			invoiceEditEnableMst.setVoucherDate(puchaseHdr.getVoucherDate());

		} else if (voucherType.equalsIgnoreCase("RECEIPT")) {
			// -------------------version 6.5 surya

//			ReceiptHdr receiptHdr = receiptHdrRepo.findByVoucherNumberAndCompanyMstAndBranchCode(invoiceno, companyMst,
//					branchcode);

			ReceiptHdr receiptHdr = receiptHdrRepo.findByVoucherNumberAndCompanyMstAndBranchCodeAndVoucherDate(
					invoiceno, companyMst, branchcode, date);
			// -------------------version 6.5 surya end

			if (null == receiptHdr) {
				return null;
			}
			invoiceEditEnableMst.setVoucherDate(receiptHdr.getVoucherDate());

		} else if (voucherType.equalsIgnoreCase("PAYMENT")) {
			// -------------------version 6.5 surya

//			PaymentHdr paymentHdr = paymentHdrRepo.findByVoucherNumberAndCompanyMstAndBranchCode(invoiceno, companyMst,
//					branchcode);

			PaymentHdr paymentHdr = paymentHdrRepo.findByVoucherNumberAndCompanyMstAndBranchCodeAndVoucherDate(
					invoiceno, companyMst, branchcode, date);
			// -------------------version 6.5 surya end

			if (null == paymentHdr) {
				return null;
			}
			invoiceEditEnableMst.setVoucherDate(paymentHdr.getVoucherDate());

		}

		// -----------------------version 6.4 surya
		else if (voucherType.equalsIgnoreCase("SALE ORDER")) {
			// -------------------version 6.5 surya

//			SalesOrderTransHdr salesOrderTransHdr = salesOrderTransHdrRepository
//					.findByVoucherNumberAndCompanyMstAndBranchCode(invoiceno, companyMst, branchcode);

			SalesOrderTransHdr salesOrderTransHdr = salesOrderTransHdrRepository
					.findByVoucherNumberAndCompanyMstAndBranchCodeAndVoucherDate(invoiceno, companyMst, branchcode,
							date);
			// -------------------version 6.5 surya end

			if (null == salesOrderTransHdr) {
				return null;
			}
			invoiceEditEnableMst.setVoucherDate(salesOrderTransHdr.getVoucherDate());

		}
		// -----------------------version 6.4 surya end

		else if (voucherType.equalsIgnoreCase("STOCK TRANSFER")) {
			// -------------------version 6.5 surya

			invoiceEditEnableMst.setVoucherDate(date);

		}
		invoiceEditEnableMst.setBranchCode(branchcode);
		invoiceEditEnableMst.setCompanyMst(companyMst);
		invoiceEditEnableMst.setUserMst(userMst);
		invoiceEditEnableMst.setUserName(userMst.getUserName());

		invoiceEditEnableMst.setVoucherNumber(invoiceno);

		invoiceEditEnableMst.setVoucherType(voucherType);
		invoiceEditEnableMst = invoiceEditEnableRepository.saveAndFlush(invoiceEditEnableMst);

//		List<InvoiceEditEnableMst> invoiceEditEnableMstList = invoiceEditEnableRepository.findAll();
//		Iterator iter = invoiceEditEnableMstList.iterator();
//		while (iter.hasNext()) {
//			InvoiceEditEnableMst invoiceEditEnableMstObj = (InvoiceEditEnableMst) iter.next();
//			List<BranchMst> BranchList = branchMstRepository.findAll();
//
//			Iterator iter01 = BranchList.iterator();
//			while (iter01.hasNext()) {
//				BranchMst branchMsttoSend = (BranchMst) iter01.next();
//
//				if (invoiceEditEnableMstObj.getBranchCode().equals(branchMsttoSend.getBranchCode())) {
//					jmsTemplate.convertAndSend(branchMsttoSend.getBranchCode() + ".invoiceeditenablemst",
//							invoiceEditEnableMstObj);
//
//				}
//
//			}
//		}

		/*
		 * Delete previous transactions for this voucher
		 */
//		if (invoiceEditEnableMst.getVoucherType().equalsIgnoreCase("SALES")) {
//			// Fetch previous sales for this invoice
//			SalesTransHdr salesTransHdr = salesTransHdrRepository
//					.findByVoucherNumberAndCompanyMstAndBranchCode(invoiceno, companyMst, branchcode);
//			/*
//			 * Delete salestransHdr and Save SalesTransHdrHst Delete itemBatchDtl and save
//			 * to ItemBatchDtlHst
//			 */
//			List<ItemBatchDtl> itembatchDtlList = itemBatchDtlRepository.findBySourceParentId(salesTransHdr.getId());
//			if (itembatchDtlList.size() > 0) {
//				for (int i = 0; i < itembatchDtlList.size(); i++) {
//					saveItemBatchDtlHst(itembatchDtlList.get(i));
//				}
//				itemBatchDtlRepository.deleteBySourceParentId(salesTransHdr.getId());
//			}
//			saveSalesTransHdrHst(salesTransHdr);
//			/*
//			 * Done by Sari on (08/12/21) Delete Accounting Accounting history is not saved
//			 * to any of the hst table As per instruction from Regy George
//			 */
//			AccountClass accClass = accountClassRepo.findBySourceVoucherNumberAndTransDateAndBrachCode(
//					salesTransHdr.getVoucherNumber(), salesTransHdr.getVoucherDate(), salesTransHdr.getBranchCode());
//			if (null != accClass) {
//				accountClassRepo.deleteById(accClass.getId());
//			}
//			// Finally delete salesTrasHdr By Using this id
//			salesTransHdrRepository.deleteById(salesTransHdr.getId());
//		}
		return invoiceEditEnableMst;
	}

	@GetMapping("{companymstid}/web/invoicenumbersales/{branchcode}/{voucherType}")
	public List<SalesTransHdr> getInvoiceNumber(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "voucherType") String voucherType, @RequestParam("rdate") String vouchedate) {

		branchcode = branchcode.replaceAll("%20", " ");
		voucherType = voucherType.replaceAll("%20", " ");
		Date date = SystemSetting.StringToUtilDate(vouchedate, "yyyy-MM-dd");

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		if (null == companyMst) {
			return null;
		}

		return salesTransHdrRepository.findByVoucherDateAndCompanyMstAndBranchCode(date, companyMst, branchcode);

	}

	@GetMapping("{companymstid}/web/invoicenumberpuchase/{branchcode}/{voucherType}")
	public List<PurchaseHdr> getInvoiceNumberPurchase(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "voucherType") String voucherType, @RequestParam("rdate") String vouchedate) {

		branchcode = branchcode.replaceAll("%20", " ");
		voucherType = voucherType.replaceAll("%20", " ");
		Date date = SystemSetting.StringToUtilDate(vouchedate, "yyyy-MM-dd");

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		if (null == companyMst) {
			return null;
		}

		return purchaseHdrRepo.findByVoucherDateAndCompanyMstAndBranchCode(date, companyMst, branchcode);

	}

	@GetMapping("{companymstid}/web/invoicenumberreceipt/{branchcode}/{voucherType}")
	public List<ReceiptHdr> getInvoiceNumberReceipt(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "voucherType") String voucherType, @RequestParam("rdate") String vouchedate) {

		branchcode = branchcode.replaceAll("%20", " ");
		voucherType = voucherType.replaceAll("%20", " ");
		Date date = SystemSetting.StringToUtilDate(vouchedate, "yyyy-MM-dd");

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		if (null == companyMst) {
			return null;
		}

		return receiptHdrRepo.findByVoucherDateAndCompanyMstAndBranchCode(date, companyMst, branchcode);

	}

	@GetMapping("{companymstid}/web/invoicenumberpayment/{branchcode}/{voucherType}")
	public List<PaymentHdr> getInvoiceNumberPayment(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "voucherType") String voucherType, @RequestParam("rdate") String vouchedate) {

		branchcode = branchcode.replaceAll("%20", " ");
		voucherType = voucherType.replaceAll("%20", " ");
		Date date = SystemSetting.StringToUtilDate(vouchedate, "yyyy-MM-dd");

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		if (null == companyMst) {
			return null;
		}

		return paymentHdrRepo.findByVoucherDateAndCompanyMstAndBranchCode(date, companyMst, branchcode);

	}

	@GetMapping("{companymstid}/web/invoiceeditenablemst")
	public List<InvoiceEditEnableMst> getInvoiceEditEnableMst(
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		if (null == companyMst) {
			return null;
		}

		return invoiceEditEnableRepository.findByCompanyMst(companyMst);
	}
	@GetMapping("{companymstid}/web/invoiceeditenablemst/getstocktransfervoucher/{userid}")
	public List<Object> getStockTransferVoucherByDate(
			@PathVariable(value="companymstid")String companymstid,
			@RequestParam(value="vdate")String vdate,
			@PathVariable(value="userid")String userid)
	{
		Date date = SystemSetting.StringToUtilDate(vdate, "yyyy-MM-dd");
		
		return invoiceEditEnableRepository.getstockTransferVoucherNumber(date,companymstid,userid);

	}

	@GetMapping("{companymstid}/tenant")
	public TenantyMst tenantSave(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		TenantyMst tenantyMst = new TenantyMst();
		tenantyMst.setCompanyMst(companyMst);
		tenantyMst.setTenantyKey("001");
		tenantyMst.setTenantyRoll("SINGLE");
		tenantyMst = tenantyMstRepository.save(tenantyMst);

		return tenantyMst;
	}

	@GetMapping("{companymstid}/invoiceeditenablemstbyuser/{userid}/{branchcode}")
	public List<Object> getInvoiceEditEnableMstByUserId(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "userid") String userid, @PathVariable(value = "branchcode") String branchcode,
			@RequestParam("rdate") String vouchedate) {

		Date date = SystemSetting.StringToUtilDate(vouchedate, "yyyy-MM-dd");

		List<Object> objList = new ArrayList<Object>();

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		if (null == companyMst) {
			return null;
		}

		Optional<UserMst> userMstOpt = userMstRepository.findById(userid);
		UserMst userMst = userMstOpt.get();

		if (null == userMst) {
			return null;
		}

//		boolean tenantyValue = tenantyMstService.checkTenantyMst(companyMst);
//
//		if (tenantyValue) {
//			objList = invoiceEditEnableRepository.findByCompanyMstAndVoucherDate(companyMst, date);
//		}
//
//		else {
			objList = invoiceEditEnableRepository.findByCompanyMstAndUserMstAndBranchCodeAndVoucherDate(companyMst,
					userMst, branchcode, date);
//		}

		return objList;

	}



	

	@GetMapping("{companymstid}/{branchcode}/invoiceeditenable/getsupplierandvouchernumber")
	public List<Object> getSupplierAndVouchernumber(@PathVariable("companymstid") String companymstid,

			@PathVariable("branchcode") String branchcode,

			@RequestParam("vDate") String vDate) {

		List<Object> objList = new ArrayList<Object>();

		Date date = SystemSetting.StringToSqlDate(vDate, "yyyy-MM-dd");

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

		boolean tenantyValue = tenantyMstService.checkTenantyMst(companyMst.get());

		if (tenantyValue) {
			objList = invoiceEditEnableRepository.getAllSupplierAndVouchernumber(companyMst.get(), date);
		} else {
			objList =  invoiceEditEnableRepository.getSupplierAndVouchernumber(branchcode, companyMst.get(), date);

		}
		
		return objList;

	}

	@GetMapping("{companymstid}/invoiceeditenable/getreceiptbydate/{branchcode}/{userid}")
	public List<Object> getReceiptHdrByDate(@RequestParam("voucherDate") String voucherDate,
			@PathVariable("companymstid") String companymstid, @PathVariable("userid") String userid,
			@PathVariable("branchcode") String branchcode) {
		
		List<Object> objList = new ArrayList<Object>();

		java.util.Date fdate = SystemSetting.StringToUtilDate(voucherDate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

		
		boolean tenantyValue = tenantyMstService.checkTenantyMst(companyMst.get());

		if (tenantyValue) {
			objList = invoiceEditEnableRepository.getAllReceiptsVoucherNumbers(companyMst.get(), fdate);
		} else {
			objList =  invoiceEditEnableRepository.findByCompanyMstIdAndVoucherDate(companymstid, branchcode, fdate, userid);

		}

		return objList;
	}

	@GetMapping("{companymstid}/invoiceeditenable/getpaymentbydate/{branchcode}/{userid}")
	public List<Object> getPaymenttHdrByDate(@RequestParam("voucherDate") String voucherDate,
			@PathVariable("companymstid") String companymstid, @PathVariable("userid") String userid,
			@PathVariable("branchcode") String branchcode) {
		
	

		java.util.Date fdate = SystemSetting.StringToUtilDate(voucherDate, "yyyy-MM-dd");
		
		

		
		
		return invoiceEditEnableService.findPaymentByCompanyMstIdAndVoucherDate(companymstid,branchcode, fdate,userid);

	}

//}
	@PutMapping("{companymstid}/invoiceeditenablemst/{vouchernumber}/{voucherdate}/updatestatus")
	public void updateInvoiceEditEnableMstStaus(@PathVariable(value = "vouchernumber") String vouchernumber,
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "voucherdate") String voucherdate) {
		java.util.Date fdate = SystemSetting.StringToUtilDate(voucherdate, "yyyy-MM-dd");
		List<InvoiceEditEnableMst> invoiceEditEnableMstList = invoiceEditEnableRepository
				.findByVoucherNumberAndStatusAndVoucherDate(vouchernumber, "N", fdate);
		// Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		// CompanyMst companyMst = comapnyMstOpt.get();
		for (int i = 0; i < invoiceEditEnableMstList.size(); i++) {
			invoiceEditEnableMstList.get(i).setStatus("Y");
			invoiceEditEnableRepository.saveAndFlush(invoiceEditEnableMstList.get(i));
		}

	}

	// ----------------------version 6.2 surya

	@GetMapping("{companymstid}/saleorderinvoicenumber/{branchcode}")
	public List<SalesOrderTransHdr> saleOrderInvoiceNumber(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam("date") String date) {
		java.util.Date fdate = SystemSetting.StringToUtilDate(date, "yyyy-MM-dd");

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		if (null == companyMst) {
			return null;
		}

		return salesOrderTransHdrRepository.findByCompanyMstAndBranchCodeAndVoucherDate(companyMst, branchcode, fdate);
	}
	// ----------------------version 6.2 surya end

	// -------------------version 6.4 surya

	@GetMapping("{companymstid}/saleorderinvoiceeditbyuser/{userid}/{invoicenumber}")
	public List<InvoiceEditEnableMst> saleOrderInvoiceNumber(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "userid") String userid, @PathVariable(value = "invoicenumber") String invoicenumber,
			@RequestParam("date") String date) {
		java.util.Date fdate = SystemSetting.StringToUtilDate(date, "yyyy-MM-dd");

		Optional<UserMst> userMstOpt = userMstRepository.findById(userid);
		UserMst userMst = userMstOpt.get();

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		if (null == companyMst) {
			return null;
		}

		return invoiceEditEnableRepository
				.findByUserMstAndCompanyMstAndVoucherNumberAndVoucherDateAndVoucherTypeAndStatus(userMst, companyMst,
						invoicenumber, fdate, "SALE ORDER", "N");
	}
	
	@GetMapping("{companymstid}/invoiceditenablemst/{userid}/{customername}/getcustomerfrominvoiceeditenablemst")
	public List<InvoiceEditEnableMst> getCustomer(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "userid") String userid, @PathVariable(value = "customername") String customername) {

		Optional<UserMst> userMstOpt = userMstRepository.findById(userid);
		UserMst userMst = userMstOpt.get();

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		if (null == companyMst) {
			return null;
		}

		return invoiceEditEnableRepository.getInvoiceEditEnableByCustomer(userMst.getUserName(),customername);
	}

	// -------------------version 6.4 surya end
	
	
	//------------------version new 1.02 surya (get other branch sales edit voucher number)
//	
//	@GetMapping("{companymstid}/invoiceeditenablemstbyuser/{userid}/{branchcode}")
//	public List<Object> getOtherBranchInvoiceEditEnableMstByUserId(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "userid") String userid, @PathVariable(value = "branchcode") String branchcode,
//			@RequestParam("rdate") String vouchedate) {
//
//		Date date = SystemSetting.StringToUtilDate(vouchedate, "yyyy-MM-dd");
//
//		List<Object> objList = new ArrayList<Object>();
//
//		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
//		CompanyMst companyMst = companyOpt.get();
//
//		if (null == companyMst) {
//			return null;
//		}
//
//		Optional<UserMst> userMstOpt = userMstRepository.findById(userid);
//		UserMst userMst = userMstOpt.get();
//
//		if (null == userMst) {
//			return null;
//		}
//
//		boolean tenantyValue = tenantyMstService.checkTenantyMst(companyMst);
//
//		if (tenantyValue) {
//			objList = invoiceEditEnableRepository.findByCompanyMstAndVoucherDate(companyMst, date);
//		}
//
//		else {
//			objList = invoiceEditEnableRepository.findByCompanyMstAndUserMstAndBranchCodeAndVoucherDate(companyMst,
//					userMst, branchcode, date);
//		}
//
//		return objList;
//
//	}

}
