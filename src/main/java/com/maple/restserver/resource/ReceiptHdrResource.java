
package com.maple.restserver.resource;


import java.net.URI;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.LmsQueueTallyMst;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.ReceiptHdrAndDtlMessage;
import com.maple.restserver.report.entity.ReceiptInvoice;
import com.maple.restserver.report.entity.ReceiptReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.LmsQueueTallyMstRepository;
import com.maple.restserver.repository.ReceiptDtlRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.service.ReciptDtlServiceImpl;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.SaveAndPublishServiceImpl;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.service.accounting.task.ReceiptAccounting;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;




@RestController
@Transactional
public class ReceiptHdrResource {
	private static final Logger logger = LoggerFactory.getLogger(ReceiptHdrResource.class);
	
	EventBus eventBus = EventBusFactory.getEventBus();
	@Value("${serverorclient}")
	private String serverorclient;
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	private ReceiptHdrRepository receiptHdrrepo;
	@Autowired
	ReciptDtlServiceImpl reciptDtlServiceImpl;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	ReceiptAccounting receiptAccounting;
	@Autowired
	LmsQueueTallyMstRepository lmsQueueTallyMstRepository;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@Autowired
	SaveAndPublishServiceImpl saveAndPublishServiceImpl;
	
	@GetMapping("{companymstid}/receipthdrs")
	public List<ReceiptHdr> getAllReceipts()
	{
		return receiptHdrrepo.findAll();
	}
	
	
	
	@GetMapping("{companymstid}/receipthdrs/receipthdrbyid/{hdrid}")
	public Optional<ReceiptHdr> getAllReceiptByHdrId(@PathVariable  (value = "companymstid") String companymstid,
			@PathVariable  (value = "hdrid") String hdrid)
	{
		return receiptHdrrepo.findById(hdrid);
	}
	@Autowired
	private ReceiptDtlRepository receiptDtlRepo;
	@PostMapping("{companymstid}/receipthdr")
	public ReceiptHdr createReceiptsHdr(@Valid @RequestBody ReceiptHdr receipthdr1,
			@PathVariable (value = "companymstid") String companymstid)
	{
		
		
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			receipthdr1.setCompanyMst(companyMst);
			 
			
			
//		ReceiptHdr saved=receiptHdrrepo.saveAndFlush(receipthdr1);
			
//			ReceiptHdr saved=saveAndPublishService.saveReceiptHdr(receipthdr1, receipthdr1.getBranchCode());
			ReceiptHdr saved=receiptHdrrepo.save(receipthdr1);
			logger.info("ReceiptHdr send to KafkaEvent: {}", saved);
			
	 	return saved;
	}

	@PutMapping("{companymstid}/receipthdr/{receiptId}")
	public ReceiptHdr receiptFinalSave(
			@PathVariable (value = "companymstid") String companymstid,
			@PathVariable (value = "receiptId") String receiptId,
			 @Valid @RequestBody ReceiptHdr receiptRequest)
	{

				
				return receiptHdrrepo.findById(receiptId).map(receipt -> {
					receipt.setVoucherNumber(receiptRequest.getVoucherNumber()); 
					receipt.setBranchCode(receiptRequest.getBranchCode());
					receipt.setVoucherDate(receiptRequest.getVoucherDate());
	 					
					
					
					receipt = receiptHdrrepo.save(receipt);
//					receipt=saveAndPublishService.saveReceiptHdr(receipt, receipt.getBranchCode());
					logger.info("ReceiptHdr send to KafkaEvent: {}", receipt);
					
					List<ReceiptDtl> receiptDtlList = receiptDtlRepo.findByReceiptHdr(receipt);
					ReceiptHdrAndDtlMessage ReceiptHdrAndDtlMessage = new ReceiptHdrAndDtlMessage();
					ReceiptHdrAndDtlMessage.setReceiptHdr(receipt);
					ReceiptHdrAndDtlMessage.getReceiptDtlList().addAll(receiptDtlList);
					
					saveAndPublishServiceImpl.publishObjectToKafkaEvent(ReceiptHdrAndDtlMessage, 
							receipt.getBranchCode(),
								KafkaMapleEventType.RECEIPTHDRANDDTL, 
								KafkaMapleEventType.SERVER,receipt.getId());
					
					
					
					try {
						receiptAccounting.execute(receipt.getVoucherNumber(),  receipt.getVoucherDate(), receipt.getId(), receipt.getCompanyMst());
					} catch (PartialAccountingException e) {
						// TODO Auto-generated catch block
						logger.error(e.getMessage());
					}
					eventBus.post(receipt);
					
					Map<String, Object> variables = new HashMap<String, Object>();
	 				variables.put("voucherNumber", receipt.getVoucherNumber());
					variables.put("voucherDate", receipt.getVoucherDate());
					variables.put("inet", 0);
					variables.put("id", receipt.getId());
			 		variables.put("companyid", receipt.getCompanyMst());
					variables.put("branchcode", receipt.getBranchCode());

			 		
			 		if(serverorclient.equalsIgnoreCase("REST")) {
						variables.put("REST",1);
					}else {
						variables.put("REST",0);
					}
			 				
					variables.put("WF", "forwardReceipt");
					
					
					String workflow = (String) variables.get("WF");
					String voucherNumber = (String) variables.get("voucherNumber");
					String sourceID = (String) variables.get("id");


					LmsQueueMst lmsQueueMst = new LmsQueueMst();

					lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
					
					//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
					
					java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
					java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
					lmsQueueMst.setVoucherDate(sqlVDate);
					
					
					lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
					lmsQueueMst.setVoucherType(workflow);
					lmsQueueMst.setPostedToServer("NO");
					lmsQueueMst.setJobClass("forwardReceipt");
					lmsQueueMst.setCronJob(true);
					lmsQueueMst.setJobName(workflow + sourceID);
					lmsQueueMst.setJobGroup(workflow);
					lmsQueueMst.setRepeatTime(60000L);
					lmsQueueMst.setSourceObjectId(sourceID);
					
					lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

					lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
					variables.put("lmsqid", lmsQueueMst.getId());
					eventBus.post(variables);
	 					
					LmsQueueTallyMst lmsQueueTallyMst=new LmsQueueTallyMst();
					
					lmsQueueTallyMst.setCompanyMst((CompanyMst) variables.get("companyid"));
					lmsQueueTallyMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
					lmsQueueTallyMst.setVoucherNumber((String) variables.get("voucherNumber"));
					lmsQueueTallyMst.setVoucherType(workflow);
					lmsQueueTallyMst.setPostedToTally("NO");
					lmsQueueTallyMst.setJobClass("forwardReceipt");
					lmsQueueTallyMst.setCronJob(true);
					lmsQueueTallyMst.setJobName(workflow + sourceID);
					lmsQueueTallyMst.setJobGroup(workflow);
					lmsQueueTallyMst.setRepeatTime(60000L);
					lmsQueueTallyMst.setSourceObjectId(sourceID);
					lmsQueueTallyMst.setBranchCode((String) variables.get("branchcode"));
					lmsQueueTallyMst=lmsQueueTallyMstRepository.saveAndFlush(lmsQueueTallyMst);
					variables.put("lmsqid", lmsQueueMst.getId());
					eventBus.post(variables);
		            return receipt;
		        }).orElseThrow(() -> new ResourceNotFoundException("receiptID " + receiptId + " not found"));
				
				//Posting receipttranshdr to save tallyRetryMst
				

	}

	

    @DeleteMapping("{companymstid}/receipthdr/{receiptId}")
    public ResponseEntity<?> deletePost(@PathVariable String receiptID) {
        return receiptHdrrepo.findById(receiptID).map(receipt -> {
        	receiptHdrrepo.delete(receipt);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("receiptID " + receiptID + " not found"));
    }
    
	

	@GetMapping("{companymstid}/reciptHdr/{startDate}{endDate}/reciptHdrReport")
	public List<ReceiptHdr> retrivedailyReciptSummary(
			@PathVariable(value = "startDate") Date startDate,@PathVariable(value = "endDate)") Date endDate) {
		return receiptHdrrepo.reciptSummaryReport(startDate,endDate);

	}
	
	@GetMapping("{companymstid}/receiptdtl/{hdrid}/receipttotalamount")
	public Double getReceiptDtlAmount(
			 @PathVariable("hdrid") String hdrId){
	
		return receiptDtlRepo.getSumOfReceiptDtlAmount(hdrId);

	

	}
	@GetMapping("{companymstid}/receipthdr/getreceiptbydate{voucherDate}")
	public List<ReceiptHdr> getReceiptHdrByDate(@RequestParam("voucherDate") String voucherDate,
			@PathVariable("companymstid") String companymstid)
	{
		java.util.Date fdate = SystemSetting.StringToUtilDate(voucherDate,"yyyy-MM-dd");
		
		return receiptHdrrepo.findByCompanyMstIdAndVoucherDate(companymstid, fdate);
	}
	
	@GetMapping("{companymstid}/receipthdr/getreceiptbydateandvoucher/{vouchernumber}")
	public ReceiptHdr getReceiptHdrByDateAndVoucher(@RequestParam("voucherDate") String voucherDate,
			@PathVariable("vouchernumber") String vouchernumber,
			@PathVariable("companymstid") String companymstid)
	{
		java.util.Date fdate = SystemSetting.StringToUtilDate(voucherDate,"yyyy-MM-dd");
		return receiptHdrrepo.getReceiptByVdateAndVno(fdate, vouchernumber);
	}
	@GetMapping("{companymstid}/receipthdr/receiptdtlsum/{branchCode}/{acid}")
	public Double getReceiptDtlAmountByDate(
			 @PathVariable("companymstid") String companymstid,
			 @PathVariable("branchCode") String branchcode,
			 @PathVariable("acid") String acid,
			 @RequestParam("rdate") String reportfdate
			 ){
		java.util.Date fdate = SystemSetting.StringToUtilDate(reportfdate,"yyyy-MM-dd");

		return receiptDtlRepo.getSumOfReceiptDtlAmountByDate(companymstid,fdate,branchcode,acid);

	

	}

	@GetMapping("{companymstid}/receipthdr/receiptdtlreportbetweendate/{accountid}")
	public List<ReceiptInvoice>  getReciptDtlReportWithId(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "accountid") String
			  accountid,
			  @RequestParam("rdate") String reportfdate,
			  @RequestParam("tdate") String reporttdate){
		java.util.Date fdate = SystemSetting.StringToUtilDate(reportfdate,"yyyy-MM-dd");
		java.util.Date tdate = SystemSetting.StringToUtilDate(reporttdate,"yyyy-MM-dd");
	
	
		return reciptDtlServiceImpl.reciptDtlBetweenDate(accountid,fdate,tdate);
	
	}	
	@GetMapping("{companymstid}/receipthdr/receiptdtlreport")
	public List<ReceiptInvoice>  getReciptDtlReport(@PathVariable(value = "companymstid") String
			  companymstid,
			  @RequestParam("rdate") String reportfdate,
			  @RequestParam("tdate") String reporttdate){
		java.util.Date fdate = SystemSetting.StringToUtilDate(reportfdate,"yyyy-MM-dd");
		java.util.Date tdate = SystemSetting.StringToUtilDate(reporttdate,"yyyy-MM-dd");
	
	
		return reciptDtlServiceImpl.reciptDtlBetweenDate(fdate,tdate);
	
	}	
	
	
    
}

