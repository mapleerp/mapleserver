package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.StockVerificationDtl;
import com.maple.restserver.entity.StockVerificationMst;
import com.maple.restserver.report.entity.ShortExpiryReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.StockVerificationDtlRepository;
import com.maple.restserver.repository.StockVerificationMstRepository;
import com.maple.restserver.service.StockVerificationServiceImpl;

@RestController
@Transactional
public class StockVerificationMstResource {
	@Value("${serverorclient}")
	private String serverorclient;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	StockVerificationDtlRepository stockVerificationDtlRepo;
	
	@Autowired
	ItemMstRepository itemMstRepo;
	
	@Autowired
	StockVerificationServiceImpl stockVerificationServiceImpl;
	
	@Autowired
	StockVerificationMstRepository stockVerificationMstRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("{companymstid}/stcokverificationmst/getpendingitems/{branchcode}")
	public StockVerificationMst retrieveStockVerificationMst(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam(value= "rdate")String rdate
		) {
		java.sql.Date fdate = ClientSystemSetting.StringToSqlDate(rdate, "yyyy-MM-dd");
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return stockVerificationMstRepo.getStockVerification(companyMst,fdate);
	}
	@GetMapping("{companymstid}/stcokverificationmst/getstockverificationbydate")
	public StockVerificationMst retrieveStockVerificationMstByDate(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value="rdate") String rdate) 
	{
		java.sql.Date fdate = ClientSystemSetting.StringToSqlDate(rdate, "yyyy-MM-dd");

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return stockVerificationMstRepo.findByCompanyMstAndVoucherDate(companyMst, fdate);
	}
	@PutMapping("{companymstid}/stcokverificationmst/{hdrId}/finalsave")
	public StockVerificationMst updateStockVerificationMst(
			@PathVariable(value="hdrId") String hdrId, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody StockVerificationMst  stockVerificationMstRequest)
	{
			
		StockVerificationMst stockVerificationMst = stockVerificationMstRepo.findById(hdrId).get();
		stockVerificationMst.setVoucherNumber(stockVerificationMstRequest.getVoucherNumber());
		stockVerificationMst.setUserId(stockVerificationMstRequest.getUserId());
		stockVerificationMst.setBranchCode(stockVerificationMstRequest.getBranchCode());
		stockVerificationMst.setVoucherDate(stockVerificationMstRequest.getVoucherDate());
		stockVerificationMst = stockVerificationMstRepo.save(stockVerificationMst);
		
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", stockVerificationMst.getVoucherNumber());
		variables.put("voucherDate", stockVerificationMst.getVoucherDate());
		variables.put("inet", 0);
		variables.put("id", stockVerificationMst.getId());
		variables.put("branchcode", stockVerificationMst.getBranchCode());

		variables.put("companyid", stockVerificationMst.getCompanyMst());
		if (serverorclient.equalsIgnoreCase("REST")) {
			variables.put("REST", 1);
		} else {
			variables.put("REST", 0);
		}

		variables.put("WF", "forwardStockVerification");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		
		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
			
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardStockVerification");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		
		return stockVerificationMst;
	}
		
	@GetMapping("{companymstid}/stcokverificationmst/getitems/{stockcount}")
	public String getRandomStockVerificationMst(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "stockcount")int stockcount,
			@RequestParam(value = "fdate") String fdate
		
		) 
	{
		ItemMst itemMst = null;
		StockVerificationMst stockVerificationMst = null;
		StockVerificationDtl stockVerificationDtl = null;
		int c =1;
		int count = itemMstRepo.getitemCount();
		if(count > 0)
		{
		do
		{
		String random = "0";
		
	
		random = stockVerificationServiceImpl.getOtp(count);
		for(int i = count; i<=  Integer.parseInt(random );)
		{
			random = stockVerificationServiceImpl.getOtp(count);
			
		}
		count = Integer.parseInt(random);
		List<ItemMst> items = itemMstRepo.findAll();
		
		for(int i= count; i<= items.size();)
		{
			 itemMst = items.get(i);
		
			 c++;
			 break;
			
		}
		if(null ==stockVerificationMst)
		{
			java.sql.Date rdate = ClientSystemSetting.StringToSqlDate(fdate, "yyyy-MM-dd");

			stockVerificationMst = new StockVerificationMst();
			Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = companyOpt.get();
			stockVerificationMst.setCompanyMst(companyMst);
			stockVerificationMst.setVoucherDate(rdate);
			stockVerificationMst = 	stockVerificationMstRepo.save(stockVerificationMst);
		
		}

		 stockVerificationDtl = new StockVerificationDtl();
		stockVerificationDtl.setItemId(itemMst.getId());
		stockVerificationDtl.setQty(0.0);
		stockVerificationDtl.setStockVerificationMst(stockVerificationMst);
		stockVerificationDtlRepo.save(stockVerificationDtl);
		}while(c <= stockcount);
		}
		
		
		return "nthng";
	}
	
}
