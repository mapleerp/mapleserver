package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBranchHdr;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBranchMstRepository;

@RestController
@Transactional
public class ItemBranchMstResource {

	@Autowired
	ItemBranchMstRepository 	itemBranchMstRepository;
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@PostMapping("{companymstid}/itembranchmst/itembranchmst")
	public ItemBranchHdr createItemBranchHdr(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 ItemBranchHdr  itemBranchMst)
	{
		
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		
		CompanyMst companyMst = companyMstOpt.get();
		itemBranchMst.setCompanyMst(companyMst);
		
       return itemBranchMstRepository.saveAndFlush(itemBranchMst);
		
		} 
	
	
	@GetMapping("{companymstid}/itembranchhdr/itembranchdtls")
	public List<ItemBranchHdr> retrieveAllitemBranchHdr(@PathVariable(value="companymstid") String companymstid){
		return itemBranchMstRepository.findByCompanyMstId(companymstid);
	}

	

	@PutMapping("{companymstid}/itembranchmst/itembranchmstupdate/{itembranchhdrid}")
	public ItemBranchHdr itembranchFinalSave(
			@PathVariable String itembranchhdrid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody ItemBranchHdr itembranchhdrRequest)
	{
				
		
		
		
		
		
		Optional<ItemBranchHdr> itemBranchhdrOpt = itemBranchMstRepository.findById(itembranchhdrid);
		ItemBranchHdr ItemBranchHdr=itemBranchhdrOpt.get();
					Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		
				
				//	ItemBranchHdr.setCompanyMst(comapnyMstOpt.get());
					
			
					ItemBranchHdr.setVoucherDate(itembranchhdrRequest.getVoucherDate());
					ItemBranchHdr.setVoucherNumber(itembranchhdrRequest.getVoucherNumber());
					itembranchhdrRequest.setCompanyMst(ItemBranchHdr.getCompanyMst());
					ItemBranchHdr.setCompanyMst(itembranchhdrRequest.getCompanyMst());
					ItemBranchHdr =itemBranchMstRepository.saveAndFlush(itembranchhdrRequest);
					 
				
		            return ItemBranchHdr;
		        
				
				
			
	}
	
	
	
	
}
