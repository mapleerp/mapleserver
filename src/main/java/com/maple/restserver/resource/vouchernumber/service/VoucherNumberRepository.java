package   com.maple.restserver.resource.vouchernumber.service;

import java.util.Optional;

/**
 * Contract for data access operations on {@link Invoice}.
 */
public interface VoucherNumberRepository extends ModelRepository<VoucherNumber>
{
	Optional<VoucherNumber> findByCode(String code);
}
