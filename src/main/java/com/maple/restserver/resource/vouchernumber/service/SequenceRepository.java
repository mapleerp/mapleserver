package com.maple.restserver.resource.vouchernumber.service;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * Contract for data access operations on {@link Sequence}.
 */
public interface SequenceRepository extends ModelRepository<Sequence> {
	/**
	 * Finds a sequence with a specified name.
	 *
	 * @param name The sequence name to find.
	 * @return A {@link Sequence} if one with the specified name is found,
	 *         {@code null} otherwise.
	 */
	Sequence findByName(String name);

	@Modifying(flushAutomatically = true)
	@Query(nativeQuery = true, value = "update sequence set id=:id, name=:name, value=:value")
	void updateSequence(String name, Long value, String id);
}
