package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.OtherBranchPurchaseDtl;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.HsnCodeSaleReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.OtherBranchPurchaseDtlRepository;
import com.maple.restserver.repository.OtherBranchPurchaseHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Component

public class OtherBranchPurchaseDtlResource {
	
private static final Logger logger = LoggerFactory.getLogger(OtherBranchPurchaseDtlResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	
	@Autowired
	OtherBranchPurchaseDtlRepository otherBranchPurchaseDtlRepo;
	
	@Autowired
	OtherBranchPurchaseHdrRepository otherBranchPurchaseHdrRepo;
	
	@Autowired
	private  CompanyMstRepository  companyMstRepository;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@GetMapping("{companymstid}/otherbranchpurchasedtl/getall")
	public List<OtherBranchPurchaseDtl> retrieveAllPurchaseDtl() {
		return otherBranchPurchaseDtlRepo.findAll();

	}
//	@GetMapping("{companymstid}/purchasedtl/hsncodepuchasereport/{branchcode}")
//	public List<HsnCodeSaleReport> retrieveHsnCodeSales(
//			@PathVariable(value = "branchcode") String branchcode,
//			@RequestParam(value="fdate")String fdate,@RequestParam(value="tdate")String tdate) {
//		java.util.Date fudate = ClientSystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
//
//		java.util.Date ftdate = ClientSystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
//
//		return purchaseReportService.getHSNCodeWiseSalesDtl(fudate, ftdate, branchcode);
//	}
	@Transactional
	@PostMapping("{companymstid}/otherbranchpurchasedtl/{otherbranchpurchaseId}/otherbranchpurchasedtl")
	public OtherBranchPurchaseDtl createOtherBranchPurchaseDtl(
			@PathVariable(value = "otherbranchpurchaseId") String otherbranchpurchaseId,
			@PathVariable (value = "companymstid") String companymstid,
			@Valid @RequestBody OtherBranchPurchaseDtl otherBranchPurchaseDtlRequest) {
		return otherBranchPurchaseHdrRepo.findById(otherbranchpurchaseId).map(otherbranchpurchaseHdr -> {
			
			Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			otherBranchPurchaseDtlRequest.setOtherBranchPurchaseHdr(otherbranchpurchaseHdr);
		//	OtherBranchPurchaseDtl saved =  otherBranchPurchaseDtlRepo.save(otherBranchPurchaseDtlRequest);
			OtherBranchPurchaseDtl saved =saveAndPublishService.saveOtherBranchPurchaseDtl(otherBranchPurchaseDtlRequest, mybranch, otherBranchPurchaseDtlRequest.getOtherBranchPurchaseHdr().getBranchCode());
			logger.info("OtherBranchPurchaseDtl send to KafkaEvent: {}", saved);
			return saved;
		}).orElseThrow(() -> new ResourceNotFoundException("purchaseId " + otherbranchpurchaseId + " not found"));

	}
	
	@DeleteMapping("{companymstid}/otherbranchpurchasedtl/{purchasedetailId}")
	public void DeleteOtherBranchpurchaseDtl(@PathVariable (value="purchasedetailId") String purchasedetailId) {
		otherBranchPurchaseDtlRepo.deleteById(purchasedetailId);

	}
	
	@GetMapping("{companymstid}/otherbranchpurchasedtl/{otherbranchpurchasehdrId}/getotherbranchpurchasedtl")
	public List<OtherBranchPurchaseDtl> findAllOtherBranchByOtherPurchaseHdrID(
			@PathVariable(value = "otherbranchpurchasehdrId") String otherbranchpurchasehdrId) {
		return otherBranchPurchaseDtlRepo.findByOtherBranchPurchaseHdrId(otherbranchpurchasehdrId);

	}
	
	@GetMapping("{companymstid}/otherbranchpurchasedtl/{otherbranchpurchasehdrId}/otherbranchpurchasedtlbyid")
	public OtherBranchPurchaseDtl findAllOtherBranchPurchaseDtlByOtherBranchPurchaseID(
			@PathVariable(value = "otherbranchpurchasehdrId") String otherbranchpurchasehdrId) {
		return otherBranchPurchaseDtlRepo.findById(otherbranchpurchasehdrId).get();

	}
	@GetMapping("{companymstid}/otherbranchpurchasedtl/{hdrid}/otherbranchpurchasedtlsummary")
	public SummarySalesDtl retrieveAllPurchaseSummaryByOtherBranchPurchaseHdrID(
			@PathVariable(value ="hdrid") String hdrid,
			@PathVariable(value ="companymstid") String companymstid
			
			) {
		List<Object> objList =  otherBranchPurchaseDtlRepo.findSumPurchaseDetail(hdrid);
		 
		Object[] objAray = (Object[]) objList.get(0);
 
		SummarySalesDtl summary = new SummarySalesDtl();
		summary.setTotalAmount((Double) objAray[0]);
		summary.setTotalQty((Double) objAray[1]);
		summary.setTotalDiscount((Double)objAray[2]);
		summary.setTotalTax((Double)objAray[3]);
		summary.setTotalCessAmt((Double)objAray[4]);
		
		 
		return summary;

	}
}
