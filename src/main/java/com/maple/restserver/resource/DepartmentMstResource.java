package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.DepartmentMst;
import com.maple.restserver.repository.DepartmentMstRepository;


@RestController
@Transactional
public class DepartmentMstResource {
	@Autowired
	private DepartmentMstRepository depmst;
	@GetMapping("{companymstid}/departmentmst")
	public List<DepartmentMst> retrieveAlldepartments()
	{
		return depmst.findAll();
		
	}
	@PostMapping("{companymstid}/departmentmst")
	public ResponseEntity<Object>createUser(@Valid @RequestBody DepartmentMst departmentmst1)
	{
		DepartmentMst saved=depmst.saveAndFlush(departmentmst1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	/*
	 * 
	 * Work flow forwardDepartMent
	 */
	}
	
	@GetMapping("{companymstid}/departmentmstresource/getalldepartment")
	public List<DepartmentMst> getAllDepartment()
	{	
	return depmst.findAll();
	
	}
	
	@GetMapping("{companymstid}/departmentmstresource/getdepartmentbyname/{departmentname}")
	public List<DepartmentMst> getDepartmentByName(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "departmentname") String departmentname)
	{	
		
	return depmst.getDepartmentByName(departmentname);
	
	}
	

}
