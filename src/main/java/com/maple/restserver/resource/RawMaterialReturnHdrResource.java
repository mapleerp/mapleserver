package com.maple.restserver.resource;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.RawMaterialIssueHdr;
import com.maple.restserver.entity.RawMaterialReturnHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.RawMaterialReturnHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Transactional
public class RawMaterialReturnHdrResource {
private static final Logger logger = LoggerFactory.getLogger(RawMaterialReturnHdrResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	RawMaterialReturnHdrRepository rawMaterialReturnHdrRepo;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	
	@PostMapping("{companymstid}/rawmaterialireturnhdr")
	public RawMaterialReturnHdr createRawMaterialReturnHdr(@Valid @RequestBody RawMaterialReturnHdr rawMaterialReturnHdr,
			@PathVariable (value = "companymstid") String companymstid)
	{
		
		
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			rawMaterialReturnHdr.setCompanyMst(companyMst);
			 
			
			
			//RawMaterialReturnHdr saved=rawMaterialReturnHdrRepo.saveAndFlush(rawMaterialReturnHdr);
			RawMaterialReturnHdr saved=saveAndPublishService.saveRawMaterialReturnHdr(rawMaterialReturnHdr,mybranch);
			logger.info("RawMaterialReturnHdr send to KafkaEvent: {}", saved);
	 	return saved;
	}
	@PutMapping("{companymstid}/rawmaterialreturnhdr/{MstId}/updaterawmaterialreturnhdr")
	public RawMaterialReturnHdr updateRawMaterialReturnHdr(
			@PathVariable(value="MstId") String MstId, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody RawMaterialReturnHdr  rawMaterialReturnHdrRequest)
	{
			
		RawMaterialReturnHdr rawMaterialReturnHdr = rawMaterialReturnHdrRepo.findById(MstId).get();
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		rawMaterialReturnHdr.setCompanyMst(companyMst);
		rawMaterialReturnHdr.setVoucherNumber(rawMaterialReturnHdrRequest.getVoucherNumber());
		rawMaterialReturnHdr = rawMaterialReturnHdrRepo.save(rawMaterialReturnHdr);
		
        return rawMaterialReturnHdr;
		 			
	}
 
}
