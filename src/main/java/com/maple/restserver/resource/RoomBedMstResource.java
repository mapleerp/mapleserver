
package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.RoomBedMst;
import com.maple.restserver.repository.RoomBedMstRepository;

@RestController
public class RoomBedMstResource {
	@Autowired
	private RoomBedMstRepository roombedmaster;
	@GetMapping("{companymstid}/roombedmst")
	public List<RoomBedMst> retrieveAlldepartments()
	{
		return roombedmaster.findAll();
		
	}
	@PostMapping("{companymstid}/roombedmst")
	public ResponseEntity<Object>createUser(@Valid @RequestBody RoomBedMst roombedmaster1)
	{
		RoomBedMst saved=roombedmaster.saveAndFlush(roombedmaster1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}

}
