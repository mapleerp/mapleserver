package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.SalesPropertiesConfigMst;
import com.maple.restserver.entity.SalesPropertyTransHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesPropertyTransHdrRepository;
import com.maple.restserver.service.SalesPropertiesService;

@RestController
public class SalesPropertyTransHdrResource {

	@Autowired
	SalesPropertyTransHdrRepository salesPropertyTransHdrRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SalesPropertiesService salesPropertiesService;
	
	
	@PostMapping("/{companymstid}/salespropertytranshdr/savesalespropertytranshdr")
	public SalesPropertyTransHdr createSalesPropertyTransHdr(@PathVariable(value = "companymstid") String companymstid,@Valid @RequestBody 
			SalesPropertyTransHdr salesPropertyTransHdr)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			salesPropertyTransHdr.setCompanyMst(companyMst);
			SalesPropertyTransHdr saved = salesPropertyTransHdrRepo.save(salesPropertyTransHdr);
			return saved;
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found"));


	}
	
	@GetMapping("/{companymstid}/salespropertytranshdr/getallsalespropertytranshdrbyhdrid/{hdrid}")	
	public List<SalesPropertyTransHdr> getAllSalesConfig(
			@PathVariable(value = "hdrid")String hdrid)
	{
		return salesPropertiesService.findBySalesTransHdrId(hdrid);
	}
	
	@DeleteMapping("/{companymstid}/salespropertytranshdr/deletebyid/{id}")
	public void deleteSalesProperties(@PathVariable (value = "id")String id)
	{
		salesPropertyTransHdrRepo.deleteById(id);
	}
	@DeleteMapping("/{companymstid}/salespropertytranshdr/deletebyhdrid/{id}")
	public void deleteSalesPropertiesByHdrID(@PathVariable (value = "id")String id)
	{
		salesPropertyTransHdrRepo.deleteBySalesTransHdrId(id);
	}
}
