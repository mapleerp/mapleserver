package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.KotItemMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;

@RestController

public class KotMobileResource {
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SalesDetailsRepository salesDetailsRepository;
	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	
//	@PostMapping("{companymstid}/kotmobileresource/kotplaceorder")
//	public List<SalesDtl> kotPlaceOrder(
//			@PathVariable(value = "companymstid") String companymstid,
//			@RequestParam(value ="branchcode")String branchcode,
//			@Valid @RequestBody List<SalesDtl> salesDtls )
//	{
//		
//		SalesTransHdr salesTransHdr = new SalesTransHdr();
//		
//		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
//		if(!companyOpt.isPresent()) {
//			return null;
//		}
//		
//		CompanyMst companyMst = companyOpt.get();
//		
//		salesTransHdr.setCompanyMst(companyMst);
//		salesTransHdr.setBranchCode(branchcode);
//		salesTransHdr.setCustomerId(customerId);
//		salesTransHdr.setCustomerMst(customerMst);
//		salesTransHdr.setKotNumber(kotNumber);
//		salesTransHdr.setVoucherType(voucherType);
//		salesTransHdr.setSalesMode(salesMode);
//		salesTransHdr.setSalesManId(salesManId);
//		salesTransHdr.setServingTableName(servingTableName);
//		
//			 
//			 
//			 return null;
//	}

}
