package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductMst;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProductMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ProductMstResource {
private static final Logger logger = LoggerFactory.getLogger(ProductMstResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	Pageable topFifty =   PageRequest.of(0, 50);
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	private ProductMstRepository productMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	

	@GetMapping("{companymstid}/productmstresource/productmstbyname")
	public ProductMst retrieveProductMstByName(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("productname") String productname) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}

		return productMstRepository.findByCompanyMstAndProductName(companyOpt.get(), productname);
	}

	@PostMapping("{companymstid}/productmstresource/saveproductmst")
	public ProductMst createProductMst(@Valid @RequestBody ProductMst productMst,
			@PathVariable(value = "companymstid") String companymstid) {
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}
		
		productMst.setCompanyMst(companyOpt.get());

		//productMst = productMstRepository.saveAndFlush(productMst);
		
		productMst =saveAndPublishService.saveProductMst(productMst, productMst.getBranchCode());
		logger.info("productMst send to KafkaEvent: {}", productMst);
		
		   Map<String, Object> variables = new HashMap<String, Object>();

					variables.put("voucherNumber", productMst.getId());

					variables.put("voucherDate", SystemSetting.getSystemDate());
					variables.put("inet", 0);
					variables.put("id", productMst.getId());

					variables.put("companyid", productMst.getCompanyMst());
					variables.put("branchcode", productMst.getBranchCode());

					variables.put("REST", 1);
					
					
					variables.put("WF", "forwardProductMst");
					
					
					String workflow = (String) variables.get("WF");
					String voucherNumber = (String) variables.get("voucherNumber");
					String sourceID = (String) variables.get("id");


					LmsQueueMst lmsQueueMst = new LmsQueueMst();

					lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
					
					//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
					
					
					java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
					java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
					lmsQueueMst.setVoucherDate(sqlVDate);
					
					
					
					lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
					lmsQueueMst.setVoucherType(workflow);
					lmsQueueMst.setPostedToServer("NO");
					lmsQueueMst.setJobClass("forwardProductMst");
					lmsQueueMst.setCronJob(true);
					lmsQueueMst.setJobName(workflow + sourceID);
					lmsQueueMst.setJobGroup(workflow);
					lmsQueueMst.setRepeatTime(60000L);
					lmsQueueMst.setSourceObjectId(sourceID);
					
					lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

					lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
					variables.put("lmsqid", lmsQueueMst.getId());
					eventBus.post(variables);
					
					return productMst;

	}
	
	@GetMapping("{companymstid}/productmstresource/productmstbyid/{productid}")
	public ProductMst retrieveProductMstById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "productid") String productid) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}

		return productMstRepository.findByCompanyMstAndId(companyOpt.get(), productid);
	}

	@GetMapping("{companymstid}/productmst")
	public List< Object> retrieveAllProduct(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid)
	{
		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		return  productMstRepository.findSearch("%"+searchstring.toLowerCase()+"%", topFifty);
	}
}


