package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.maple.restserver.entity.MapleUserGroup;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.MapleUserGroupRepository;

@RestController

public class MapleUserGroupResource {
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	MapleUserGroupRepository mapleUserGroupRepository;
	
	@GetMapping("{companymstid}/mapleuserGroupresource/getmapleuserGroup/{id}")
	public List<MapleUserGroup>getmapleUserGroupsByUserId(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "userid") String id)
	
	
{
	return mapleUserGroupRepository.findByUserIdAndCompanyMst(companymstid, id);
}
	

	@PostMapping("{companymstid}/mapleuserGroupresource/savemapleuserGroup")
	public MapleUserGroup createmapleUserGroup(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody MapleUserGroup mapleUserGroup)  {

		return companyMstRepository.findById(companymstid).map(companyMst -> {
			mapleUserGroup.setCompanyMst(companyMst);
			return mapleUserGroupRepository.saveAndFlush(mapleUserGroup);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}
	
	@DeleteMapping("{companymstid}/mapleuserGroupresource/deletemapleuserGroup/{id}")
	public void MapleUserGroupDeleteById(@PathVariable(value = "id") String Id) {
		mapleUserGroupRepository.deleteById(Id);

	}

}
