

package com.maple.restserver.resource;


import java.net.URI;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

 
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.PayableHdr;
import com.maple.restserver.repository.PayableHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
 

@RestController
@Transactional
public class PayableHdrResource {
	@Autowired
	private PayableHdrRepository payable;
	
	  
	
	@GetMapping("{companymstid}/payablehdr")
	public List<PayableHdr> retrieveAllcash()
	{
		return payable.findAll();
	}
	@PostMapping("{companymstid}/payablehdr")
	public ResponseEntity<Object> createUser(@Valid @RequestBody PayableHdr pay)
	{
	PayableHdr saved=payable.saveAndFlush(pay);
	URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	}
	
	
	  

}


