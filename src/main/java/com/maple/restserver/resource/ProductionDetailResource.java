package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionDtlDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.ProductionDtlsReport;
import com.maple.restserver.report.entity.RawMaterialReport;
import com.maple.restserver.repository.ProductionDetailRepository;
import com.maple.restserver.repository.ProductionDtlDtlRepository;
import com.maple.restserver.repository.ProductionMstRepository;
import com.maple.restserver.service.ProductionService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ProductionDetailResource {
	@Autowired
	private ProductionMstRepository productionMstRepository;
	
	@Autowired
	private ProductionDtlDtlRepository productionDtlDtlRepo;
	@Autowired
	private ProductionDetailRepository productionDetailRepository;
	
	@Autowired
	ProductionService productionService;

	@PostMapping("{companymstid}/productiondtl/{productionId}/productiondtldtl")
	public ProductionDtlDtl createProductionDtlDtl(@PathVariable(value = "productionId") String productionId,
			@Valid @RequestBody ProductionDtlDtl productionDtlRequest) {

		return productionDetailRepository.findById(productionId).map(productionDtl -> {
			
			productionDtlRequest.setProductionDtl(productionDtl);
			return  productionDtlDtlRepo.saveAndFlush(productionDtlRequest);
			
		}).orElseThrow(() -> new ResourceNotFoundException("productionId " + productionId + " not found"));

	}
	
	@GetMapping("{companymstid}/productiondtl/{productionDtlId}/productiondtldtl")
	public List<ProductionDtlDtl> findAllProductionDtlDtByProductioDtlID(
			@PathVariable(value = "productionDtlId") String productionDtlId) {
		return productionDtlDtlRepo.findByproductionDtlId(productionDtlId);

	}
	
	 
	
	
	@GetMapping("{companymstid}/productionplanningrmbydate/{branchcode}")
	public List<ProductionDtlDtl> retrieveAllProductionDtlByDate(
			@PathVariable (value = "companymstid") String companymstid,
			@PathVariable (value = "branchcode") String branchcode,
			 @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		
		return productionService.getRawMaterialsByDate(date);
	}
	@GetMapping("{companymstid}/productionplanningsumrmbydate/{branchcode}")
	public List<ProductionDtlsReport> retrieveAllProductionDtlSumByDateAndBranchcode(
			@PathVariable (value = "companymstid") String companymstid,
			@PathVariable (value = "branchcode") String branchcode,
			 @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		
		return productionService.getRawMaterialsSumByDate(date);
	}
	@GetMapping("{companymstid}/productionplanningsumrmbybetweendate")
	public List<ProductionDtlsReport> retrieveAllProductionDtlSumByDate(
			@PathVariable (value = "companymstid") String companymstid,
			 @RequestParam("fdate") String reportfdate,
			 @RequestParam("tdate") String reporttdate){
		java.util.Date fdate = SystemSetting.StringToUtilDate(reportfdate,"yyyy-MM-dd");
		java.util.Date tdate = SystemSetting.StringToUtilDate(reporttdate,"yyyy-MM-dd");
		
		return productionService.getRawMaterialsSumByBetweenDate(fdate,tdate);
	}

	@GetMapping("{companymstid}/productionplanningkitbydate/{branchcode}")
	public List<ProductionDtl> retrieveAllProductionKitByDate(
			@PathVariable (value = "companymstid") String companymstid,
			@PathVariable (value = "branchcode") String branchcode,
			 @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		
		return productionService.getKitByDate(date);
	}
	
	
	@GetMapping("{companymstid}/productiondtl/{startDate}/productiondtsummary")
	public List<ProductionDtl> findAllProductionDtlByDate(
			@PathVariable(value = "startDate") Date startDate)
	
	{
	
		
		return productionDetailRepository.productionDtlSummary(startDate);

	}
	
	
	@GetMapping("{companymstid}/productiondtlresource/productionplanningkitbydateandstore/{branchcode}/{store}")
	public List<ProductionDtl> retrieveAllProductionKitByDateAndStore(
			@PathVariable (value = "companymstid") String companymstid,
			@PathVariable (value = "branchcode") String branchcode,
			@PathVariable (value = "store") String store,
			 @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		
		return productionService.getKitByDateByStore(date,store);
	}
}
