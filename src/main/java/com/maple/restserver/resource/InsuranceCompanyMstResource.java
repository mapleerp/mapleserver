package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.entity.InsuranceCompanyDtl;
import com.maple.restserver.entity.InsuranceCompanyMst;
import com.maple.restserver.entity.ItemNutrition;
import com.maple.restserver.entity.MenuConfigMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;
import com.maple.restserver.repository.InsuranceCompanyRepository;
import com.maple.restserver.service.InsuranceTypeinitializeService;

@RestController
@Transactional
public class InsuranceCompanyMstResource {
	
	
	@Autowired
	InsuranceTypeinitializeService insuranceTypeinitializeService;
	

	@Autowired
	AccountHeadsRepository accRepo;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	InsuranceCompanyRepository insuranceCompanyMstrepo;
	
	@Autowired
	CurrencyMstRepository currencyMstRepo;

	//-----------------use this fort save
	@PostMapping("{companymstid}/insurancecompanymst/saveinsurancecompany")
	public InsuranceCompanyMst createInsuranceCompanyMst(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 InsuranceCompanyMst  insuranceCompanyMst)
		{
			
			
				Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
				
				CompanyMst companyMst = companyMstOpt.get();
				insuranceCompanyMst.setCompanyMst(companyMst);
				
				InsuranceCompanyMst saved=insuranceCompanyMstrepo.save(insuranceCompanyMst);
				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setAccountName(saved.getInsuranceCompanyName());
				accountHeads.setCompanyMst(saved.getCompanyMst());
				CurrencyMst currencyMst = currencyMstRepo.findByCurrencyName(saved.getCompanyMst().getCurrencyName());
				if(null != currencyMst)
				{
					accountHeads.setCurrencyId(currencyMst.getId());
				}
				accountHeads.setId(saved.getId());
				accRepo.save(accountHeads);
				return saved;
			} 

	@GetMapping("{companymstid}/insurancecompanymst/findall")
	public List<InsuranceCompanyMst> findAllInsuranceComp(
			@PathVariable(value = "companymstid")String companymstid)
	{
		return insuranceCompanyMstrepo.findAll();
	}
	@GetMapping("{companymstid}/insurancecompanymst/getinsurancecompbyid/{insurancemstid}")
	public InsuranceCompanyMst getInsuranceCompMstById(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "insurancemstid")String insurancemstid)
	{
		return insuranceCompanyMstrepo.findByCompanyMstIdAndId(companymstid,insurancemstid);
	}
	
	@PutMapping("{companymstid}/insurancecompanymst/updateinsurancecompbyid/{insurancemstid}")
	public InsuranceCompanyMst updateInsuranceCompById(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "insurancemstid")String insurancemstid,
			@Valid @RequestBody InsuranceCompanyMst  insuranceCompanyMstRequest)
	{
		InsuranceCompanyMst insuranceComp = insuranceCompanyMstrepo.findByCompanyMstIdAndId(companymstid, insurancemstid);
		insuranceComp.setContactPerson(insuranceCompanyMstRequest.getContactPerson());
		insuranceComp.setInsuranceCompanyName(insuranceCompanyMstRequest.getInsuranceCompanyName());
		insuranceComp.setPhoneNumber(insuranceCompanyMstRequest.getPhoneNumber());
		insuranceComp = insuranceCompanyMstrepo.save(insuranceComp);
		Optional<AccountHeads> accByIdop = accRepo.findById(insuranceComp.getId());
		if(accByIdop.isPresent())
		{
			AccountHeads accountHeads = accByIdop.get();
			accountHeads.setAccountName(insuranceComp.getInsuranceCompanyName());
			accountHeads.setCompanyMst(insuranceComp.getCompanyMst());
			CurrencyMst currencyMst = currencyMstRepo.findByCurrencyName(insuranceComp.getCompanyMst().getCurrencyName());
			if(null != currencyMst)
			{
				accountHeads.setCurrencyId(currencyMst.getId());
			}
			accountHeads.setId(insuranceComp.getId());
			accRepo.save(accountHeads);
		}
		else
		{
			AccountHeads accountHeads = new AccountHeads();
			accountHeads.setAccountName(insuranceComp.getInsuranceCompanyName());
			accountHeads.setCompanyMst(insuranceComp.getCompanyMst());
			CurrencyMst currencyMst = currencyMstRepo.findByCurrencyName(insuranceComp.getCompanyMst().getCurrencyName());
			if(null != currencyMst)
			{
				accountHeads.setCurrencyId(currencyMst.getId());
			}
			accountHeads.setId(insuranceComp.getId());
			accRepo.save(accountHeads);
		}
		
		return insuranceComp;
	}
	@GetMapping("{companymstid}/insurancecompanymst/getinsurancecompbyname")
	public List<InsuranceCompanyMst> getInsuranceCompMstByname(
			@PathVariable(value = "companymstid")String companymstid,
			@RequestParam(value = "insurancecompname")String insurancecompname)
	{
		return insuranceCompanyMstrepo.findByCompanyMstIdAndInsuranceCompanyName(companymstid,insurancecompname);
	}
	@DeleteMapping("{companymstid}/insurancecompanymst/deleteinsurancecompbyid/{insurancemstid}")
	public void deleteInsuranceCompById(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "insurancemstid")String insurancemstid)
	{
		 insuranceCompanyMstrepo.deleteById(insurancemstid);
	}

	//save 
//	@PostMapping("{companymstid}/insurancecompanymst/saveinsurancecompanymst")
//	public InsuranceCompanyMst createInsuranceCompanyMst(
//			@PathVariable(value="companymstid")	String commpanymstid,
//			@Valid @RequestBody InsuranceCompanyMst insuranceCompanyMst)
//	{
//		CompanyMst companymst=companyMstRepo.findById(commpanymstid).get();
//		insuranceCompanyMst.setCompanyMst(companymst);
//		insuranceCompanyMst=insuranceCompanyMstrepo.save(insuranceCompanyMst);
//		return insuranceCompanyMst;
//	}
	
	
	// delete
	@DeleteMapping("{companymstid}/insurancecompanymst/deleteinsurancecompanymst/{id}")
	public void DeleteInsuranceCompanyMst(@PathVariable(value = "id") String Id) {
		insuranceCompanyMstrepo.deleteById(Id);
	}
	
	
	//showing all 
	@GetMapping("{companymstid}/insurancecompanymst/showallinsurancecompanymst")
	public List<InsuranceCompanyMst> showallInsuranceCompanyMst(
			
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		
		return insuranceCompanyMstrepo.findByCompanyMst(companyMstOpt.get());
	}
//	@GetMapping("{companymstid}/insurancecompanymst/getinsurancecompanymstbyname")
//	public List<InsuranceCompanyMst> getPolicyByInsuranceCompMst(
//			@PathVariable(value = "companymstid")String companymstid,
//			@RequestParam(value = "insurancecompany")String insurancecompany) {
//	
//		return insuranceCompanyMstrepo.findByInsuranceCompanyName(insurancecompany);
//	}
	
	

	
	/*@GetMapping("{companymstid}/insurancecompanymst/getinitializeinsurancecompanymst/{branchcode}")
	
	public String getinitializeinsurancecompanymst
	   (@PathVariable("companymstid") String companymstid,
				@PathVariable("branchcode") String branchcode)
	
	{
		
		return insuranceTypeinitializeService.initializeInsuranceType(companymstid, branchcode);

	
	}*/

}
