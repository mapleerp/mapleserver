package com.maple.restserver.resource;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DashBoardReportType;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DashBoardReportTypeRepository;

@RestController
@Transactional
public class DashBoardReporttypeResource {
	
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	@Autowired
	DashBoardReportTypeRepository dashBoardReportTypeRepository;
	
	@PostMapping("/{companymstid}/dashboardreporttyperesource/savedashboardreporttype")
	public DashBoardReportType saveDashBoardReportType(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody DashBoardReportType dashBoardReportType)
	{
		CompanyMst companymst=CompanyMstRepository.findById(companymstid).get();
		dashBoardReportType.setCompanyMst(companymst);
		return dashBoardReportTypeRepository.save(dashBoardReportType);
		
		
	}
	
	
	@GetMapping("{companymstid}/{dashboardname}/dashboardreporttyperesource/getreportnames")
	public List<DashBoardReportType> retrieveReportNameByDashboardname(
			@PathVariable(value = "dashboardname") String dashBoardName,
			@PathVariable(value = "companymstid") String companymstid) {
		
		return dashBoardReportTypeRepository.findByDashBoardNameAndCompanyMstId(dashBoardName,companymstid);

	}
}
