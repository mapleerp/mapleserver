package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MixCatItemLinkMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ResourceCatItemLinkRepository;
import com.maple.restserver.service.MachineResourceService;

@RestController
public class ResourceCatItemMstResource {

	
	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	ResourceCatItemLinkRepository resourceCatItemLinkRepository;
	@Autowired
	MachineResourceService machineResourceService;

@PostMapping("{companymstid}/resourcecatitemmstresource/createresourcecatitemmst")
public MixCatItemLinkMst createResourceCatItemLinkMst(@PathVariable(value = "companymstid") String
		  companymstid,@Valid @RequestBody 
		  MixCatItemLinkMst resourceCatItemLinkMst)
{
	return companyMstRepository.findById(companymstid).map(companyMst-> {
		resourceCatItemLinkMst.setCompanyMst(companyMst);
		
	return resourceCatItemLinkRepository.save(resourceCatItemLinkMst);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found")); }
	


@GetMapping("{companymstid}/resourcecatitemmstresource/findallresourcecatitemmst")
public List<MixCatItemLinkMst> retrieveResourceCatItemLinkMst(@PathVariable(value = "companymstid") String
		 companymstid)
{
	return machineResourceService.retrieveResourceCatItemLinkMst();
}
	


@GetMapping("{companymstid}/resourcecatitemmstresource/findallresourcecatitemmst/{itemid}")
public List<MixCatItemLinkMst> retrieveResourceCatItemLinkMsbyItemAndCat(@PathVariable(value = "companymstid") String
		 companymstid,@PathVariable(value="itemid")String itemid,@RequestParam (value="resourcecatid")String resourcecatid)
{
	return resourceCatItemLinkRepository.findByItemIdAndResourceCatId(itemid,resourcecatid);
}
	


@GetMapping("{companymstid}/resourcecatitemmstresource/findbyresourcecatitemmstbyitemid/{itemid}")
public List<MixCatItemLinkMst> retrieveResourceCatItemLinkMsbyItem(@PathVariable(value = "companymstid") String
		 companymstid,@PathVariable(value="itemid")String itemid)
{
	return resourceCatItemLinkRepository.findByItemId(itemid);
}



@GetMapping("{companymstid}/resourcecatitemmstresource/fetchresourcecatitemmstbyitemid/{itemid}")
public MixCatItemLinkMst retrieveResourceCatItemLinkMstbyItem(@PathVariable(value = "companymstid") String
		 companymstid,@PathVariable(value="itemid")String itemid)
{
	return resourceCatItemLinkRepository.fetchByItemId(itemid);
}


@PutMapping("{companymstid}/resourcecatitemmstresource/updateresourcecatitemmst/{itemid}/{subcatid}")
public void updateResourceCatItemLinkMst(@PathVariable(value = "companymstid") String
		  companymstid,@PathVariable(value="itemid") String itemid,@PathVariable(value="subcatid") String subcatid
		 )
{

		
	 resourceCatItemLinkRepository.updateMixCategoryMst(itemid,subcatid);
}
	

}
