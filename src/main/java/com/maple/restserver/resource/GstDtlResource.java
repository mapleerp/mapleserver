package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.GstDtl;
import com.maple.restserver.entity.TaxDtl;
import com.maple.restserver.repository.GstDtlRepository;
import com.maple.restserver.repository.TaxDtlRepository;


@RestController

public class GstDtlResource {
	
	@Autowired
	private GstDtlRepository gstRepository;
	
	@GetMapping("{companymstid}/gstdtls")
	public List< GstDtl> retrieveAllSupplier(@PathVariable(value = "companymstid") String
			  companymstid){
		return gstRepository.findByCompanyMstId(companymstid);
	}
	
	@PostMapping("{companymstid}/gstdtl")
	public GstDtl createsGstdtl(@Valid @RequestBody 
			GstDtl  gstDtl)
	{
		GstDtl saved = gstRepository.saveAndFlush(gstDtl);
	 
		
		return saved;
	}

}
