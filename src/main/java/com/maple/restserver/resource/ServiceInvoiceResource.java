package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ServiceInvoice;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.ServiceInvoiceServiceImpl;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class ServiceInvoiceResource {
	
	@Autowired
	ServiceInvoiceServiceImpl serviceInvoiceServiceImpl;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("/{companymstid}/serviceinvoice/{branchcode}/{voucherNumber}")
	public List<ServiceInvoice> serviceInvoiceReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "voucherNumber") String voucherNumber,
			@PathVariable(value = "branchcode") String branchcode, 
			@RequestParam("fdate") String fdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		return serviceInvoiceServiceImpl.getserviceInvoice(companyMstOpt.get(),branchcode,fDate,voucherNumber);
	}
	
	

}
