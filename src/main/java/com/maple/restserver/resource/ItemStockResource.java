package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.SummaryReportItemStock;
import com.maple.restserver.repository.ItemStockRepository;

@RestController
@Transactional
public class ItemStockResource {
	
	@Autowired
	ItemStockRepository itemstockRepo;
	@GetMapping("{companymstid}/itemstockreport")
	public List<SummaryReportItemStock> retrieveAllSalesOrders()
	{
		
		
		  List<Object> objList= itemstockRepo.finditemstockreport();
		  List<SummaryReportItemStock> reportsummary =new ArrayList<SummaryReportItemStock>();
		  for(int i=0;i<objList.size();i++) {
		  Object[] objAray = (Object[]) objList.get(0);
		  SummaryReportItemStock summary = new SummaryReportItemStock ();
		  summary.setItemName((String) objAray[0]);
		  summary.setBarCode((String) objAray[1]);
		  summary.setUnitName((String) objAray[2]);
		  summary.setMrp((Double) objAray[3]);
		  summary.setQty((Double)objAray[4]);
		  summary.setCess((Double)objAray[5]);
		  summary.setTaxRate((Double)objAray[6]);
		  reportsummary.add(summary);
		  }
		 
		  return reportsummary;
		 
		
	}
}
