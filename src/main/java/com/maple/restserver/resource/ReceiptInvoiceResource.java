package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AccountPayable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.OrgReceiptInvoice;
import com.maple.restserver.report.entity.ReceiptInvoice;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.ReceiptInvoiceService;
import com.maple.restserver.service.ReceiptInvoiceServiceImpl;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ReceiptInvoiceResource {

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	ReceiptInvoiceService receiptInvoiceService;

	@GetMapping("{companymstid}/receiptinvoice/{voucherNumber}")

	public List<ReceiptInvoice> getStockTransferOutDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("voucherNumber") String voucherNumber,

			@RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		// return
		// receiptInvoiceService.findByVoucherDateAndVoucherNumbergetReceiptByVoucherAndDate(voucherNumber,
		// reportdate, companymstid);
		return receiptInvoiceService.getReceiptByVoucherAndDate(voucherNumber, date, companymstid);

	}

	@GetMapping("{companymstid}/orgreceiptinvoice/{voucherNumber}/{memberId}")

	public List<OrgReceiptInvoice> getReceiptOrgReport(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("voucherNumber") String voucherNumber, 
			@PathVariable("memberId") String memberId,
			@RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		// return
		// receiptInvoiceService.findByVoucherDateAndVoucherNumbergetReceiptByVoucherAndDate(voucherNumber,
		// reportdate, companymstid);
		return receiptInvoiceService.getReceiptByVoucherAndDateAndmember(voucherNumber, date, companymstid, memberId);

	}

	@GetMapping("{companymstid}/receiptinvoiceresource/receiptinvoicereport/{branchcode}")
	public List<ReceiptInvoice> getReceiptInvoiceReport(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchCode, @RequestParam("fdate") String fromdate,
			@RequestParam("tdate") String todate) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

		return receiptInvoiceService.getReceiptInvoiceReport(sdate, edate, companyMst.get(), branchCode);

	}
}
