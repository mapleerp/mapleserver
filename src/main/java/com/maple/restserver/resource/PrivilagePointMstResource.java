package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PrivilagePointMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PrivilagePointMstRepository;




@RestController
@Transactional
public class PrivilagePointMstResource {
	@Autowired
	private PrivilagePointMstRepository privilagePointMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("{companymstid}/privilagepoints")
	public List<PrivilagePointMst> retrieveAllPrivilagePointMst(
			@PathVariable(value = "companymstid") String
			  companymstid){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		
		return privilagePointMstRepository.findByCompanyMst(companyOpt.get());
	}
	
	
	@PostMapping("{companymstid}/privilagepoint")
	public PrivilagePointMst savePrivlagePoint(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			PrivilagePointMst privilagePointMst)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			privilagePointMst.setCompanyMst(companyMst);
		
		
		return privilagePointMstRepository.saveAndFlush(privilagePointMst);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));
		
		
	
	}
	
	
	
	

}
