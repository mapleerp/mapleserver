package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProcessMst;
import com.maple.restserver.entity.SalesManMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesManMstRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class SalesManMstResource {

	
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	
	@Autowired
	SalesManMstRepository salesManMstRepository;
	
	
	

	@PostMapping("{companymstid}/salesmanmstresource/createsalesman")
	public SalesManMst createSalesManMst(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody 
			SalesManMst salesManMst)
	{
		
		Optional<CompanyMst> companyMst = CompanyMstRepository.findById(companymstid);
				
	//	processMst.setCompanyMst(companyMst.get());
				
		SalesManMst saved = salesManMstRepository.saveAndFlush(salesManMst);
		
		return saved;
	}
	
	@DeleteMapping("{companymstid}/salesmanmstresource/{id}/deletesalesman")
	public void salesManMstDelete(@PathVariable(value = "id") String Id) {
		salesManMstRepository.deleteById(Id);

	}
	
	@GetMapping("{companymstid}/salesmanmstresource/findallsalesman")
	public List<SalesManMst> retrieveAllUser()
	{
		return salesManMstRepository.findAll();
	}
	
	@GetMapping("{companymstid}/salesmanmstresource/findsalesmanbyname/{salesman}")
	public List<SalesManMst> retrieveAllSalesManByName(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "salesman") String salesman)
	{
		return salesManMstRepository.findBySalesManName(salesman);
	}
}
