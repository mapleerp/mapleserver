package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchExpiryDtl;
import com.maple.restserver.entity.ItemPropertyConfig;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.report.entity.HsnCodeSaleReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchExpiryDtlRepository;
import com.maple.restserver.repository.ItemPropertyConfigRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.service.PurchaseDtlService;
import com.maple.restserver.service.PurchaseReportService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
@CrossOrigin("http://localhost:4200")
@RestController
public class PurchaseDtlResource {

	private static final Logger logger = LoggerFactory.getLogger(PurchaseDtlResource.class);
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@Autowired
	ItemPropertyConfigRepository itemPropertyConfigRepo;
	@Autowired
	private PurchaseDtlRepository purchaseDtlRepository;

	@Autowired
	PurchaseReportService purchaseReportService;
	@Autowired
	private PurchaseHdrRepository purchaseHdrRepository;

	@Autowired
	private ItemBatchExpiryDtlRepository itemBatchExpiryDtlRepo;
	
	@Autowired
	private  CompanyMstRepository  companyMstRepository;
	
	@Autowired
	private  PurchaseDtlService  purchaseDtlService;
	
	
	EventBus eventBus = EventBusFactory.getEventBus();

	@GetMapping("{companymstid}/purchasedtls")
	public List<PurchaseDtl> retrieveAllPurchaseDtl() {
		return purchaseDtlRepository.findAll();

	}
	@GetMapping("{companymstid}/purchasedtl/hsncodepuchasereport/{branchcode}")
	public List<HsnCodeSaleReport> retrieveHsnCodeSales(
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam(value="fdate")String fdate,@RequestParam(value="tdate")String tdate) {
		java.util.Date fudate = ClientSystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");

		java.util.Date ftdate = ClientSystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");

		return purchaseReportService.getHSNCodeWiseSalesDtl(fudate, ftdate, branchcode);
	}
	

	@PostMapping("{companymstid}/purchasedtlresource/purchasehdr/{purchaseId}/purchasedtl")
	public PurchaseDtl createPurchaseDtl(
			@PathVariable(value = "purchaseId") String purchaseId,
			@PathVariable (value = "companymstid") String companymstid,
			@Valid @RequestBody PurchaseDtl purchaseDtlRequest) {
		
		
		
		return purchaseHdrRepository.findById(purchaseId).map(purchasehdr -> {
			
			Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			purchaseDtlRequest.setPurchaseHdr(purchasehdr);
			PurchaseDtl saved =  purchaseDtlRepository.save(purchaseDtlRequest);
			
			/*
			 * PurchaseDtl saved
			 * =saveAndPublishService.savePurchaseDtl(purchaseDtlRequest,mybranch);
			 * if(null!=saved) { saveAndPublishService.publishObjectToKafkaEvent(saved,
			 * mybranch, KafkaMapleEventType.PURCHASEDTL, KafkaMapleEventType.SERVER); }else
			 * { return null; }
			 */
			
			logger.info("PurchaseDtl send to KafkaEvent: {}", saved);
			purchaseDtlService.updatePurchaseDtlDisplaySerial(purchasehdr.getId());
			
			if(!purchaseDtlRequest.getBatch().equalsIgnoreCase(MapleConstants.Nobatch))
			{
				 ItemBatchExpiryDtl itemBatchExpiryDtl = new ItemBatchExpiryDtl();
				List<ItemBatchExpiryDtl> items=  itemBatchExpiryDtlRepo.findByCompanyMstAndItemIdAndBatch(saved.getPurchaseHdr().getCompanyMst(), saved.getItemId(), saved.getBatch());
				 if(items.isEmpty())
				 {
						itemBatchExpiryDtl.setBatch(saved.getBatch());
						itemBatchExpiryDtl.setCompanyMst(companyMst);
						itemBatchExpiryDtl.setExpiryDate(saved.getExpiryDate());
						itemBatchExpiryDtl.setItemId(saved.getItemId());
						itemBatchExpiryDtl.setPurchaseDtl(saved);
						itemBatchExpiryDtl.setUpdatedDate(ClientSystemSetting.getSystemDate());
//						itemBatchExpiryDtlRepo.save(itemBatchExpiryDtl);
						itemBatchExpiryDtl=saveAndPublishService.saveItemBatchExpiryDtl(itemBatchExpiryDtl, mybranch);
						if(null != itemBatchExpiryDtl) {
							saveAndPublishService.publishObjectToKafkaEvent(itemBatchExpiryDtl,
									mybranch, KafkaMapleEventType.ITEMBATCHEXPIRYDTL, 
									KafkaMapleEventType.SERVER, itemBatchExpiryDtl.getId());
						}else {
							return null;
						}
						eventBus.post(itemBatchExpiryDtl);
				 }
				 else
				 {
					 	itemBatchExpiryDtl.setBatch(saved.getBatch());
						itemBatchExpiryDtl.setCompanyMst(companyMst);
						itemBatchExpiryDtl.setExpiryDate(saved.getExpiryDate());
						itemBatchExpiryDtl.setItemId(saved.getItemId());
						itemBatchExpiryDtl.setPurchaseDtl(saved);
						itemBatchExpiryDtl.setUpdatedDate(ClientSystemSetting.getSystemDate());
					 itemBatchExpiryDtlRepo.UpdateItemBatchExpiryDtl(saved.getItemId(),saved.getExpiryDate(),saved.getBatch());
						List<ItemBatchExpiryDtl> itemsList=  itemBatchExpiryDtlRepo.
								findByCompanyMstAndItemIdAndBatch(itemBatchExpiryDtl.getCompanyMst(), itemBatchExpiryDtl.getItemId(), itemBatchExpiryDtl.getBatch());
					 eventBus.post(itemsList.get(0));
				 }
			 }
			return saved;
		}).orElseThrow(() -> new ResourceNotFoundException("purchaseId " + purchaseId + " not found"));

	}
	
	//here we are calculating total of purchasedtl summary

	@GetMapping("{companymstid}/purchasehdr/{purchaseId}/purchasedtlsummary")
	public SummarySalesDtl retrieveAllPurchaseSummaryByPurchaseHdrID(
			@PathVariable(value ="purchaseId") String purchaseId,
			@PathVariable(value ="companymstid") String companymstid
			
			) {
		List<Object> objList =  purchaseDtlRepository.findSumPurchaseDetail(purchaseId);
		 
		Object[] objAray = (Object[]) objList.get(0);
 
		SummarySalesDtl summary = new SummarySalesDtl();
		summary.setTotalAmount((Double) objAray[0]);
		summary.setTotalQty((Double) objAray[1]);
		summary.setTotalDiscount((Double)objAray[2]);
		summary.setTotalTax((Double)objAray[3]);
		summary.setTotalCessAmt((Double)objAray[4]);
		
		 
		return summary;

	}
	//here we deleting purchase detail by using its id
	
	@DeleteMapping("{companymstid}/purchasedtl/{purchasedetailId}")
	public void DeletepurchaseDtl(@PathVariable (value="purchasedetailId") String purchasedetailId) {
		
		PurchaseDtl purchaseDtl = purchaseDtlRepository.getById(purchasedetailId);
		PurchaseHdr purchaseHdr = purchaseDtl.getPurchaseHdr();
 		purchaseDtlRepository.deleteById(purchasedetailId);
		purchaseDtlService.updatePurchaseDtlDisplaySerial(purchaseHdr.getId());


	}
	
	
	@GetMapping("{companymstid}/purchasehdr/{purchaseHdrId}/purchasedtl")
	public List<PurchaseDtl> findAllPurchaseDtlByPurchaseHdrID(
			@PathVariable(value = "purchaseHdrId") String purchaseHdrId) {
		return purchaseDtlRepository.findByPurchaseHdrId(purchaseHdrId);

	}
	
	//version1.7

	@GetMapping("{companymstid}/purchasedtl/{purchaseHdrId}/purchasedtlbyid")
	public PurchaseDtl findAllPurchaseDtlByPurchaseID(
			@PathVariable(value = "purchaseHdrId") String purchaseHdrId) {
		return purchaseDtlRepository.findById(purchaseHdrId).get();

	}
	//version1.7ends
	

	/*
	 * @GetMapping("/purchasehdr/{purchaseId}/purchasedtl") public List<PurchaseDtl>
	 * retrieveAllPurchaseDtlByPurchaseHdrID(
	 * 
	 * @PathVariable(value = "purchaseId") String purchaseId, Pageable pageable) {
	 * return purchaseDtlRepository.findByPurchaseHdrId(purchaseId);
	 * 
	 * }
	 */
	
	@PutMapping("{companymstid}/purchasedtl/updatenetcost")
	public void updatePurchaseDtlNetCost(@PathVariable(value = "companymstid")String companymstid,
			@RequestBody PurchaseDtl purchaseDtlReq)
	{
		Optional<PurchaseDtl> purchaseDtlop = purchaseDtlRepository.findById(purchaseDtlReq.getId());
		if(purchaseDtlop.isPresent())
		{
			PurchaseDtl purchaseDtl = purchaseDtlop.get();
			purchaseDtl.setNetCost(purchaseDtlReq.getNetCost());
			if(null != purchaseDtlReq.getFcNetCost())
			{
			purchaseDtl.setFcNetCost(purchaseDtlReq.getFcNetCost());
			}
//			purchaseDtlRepository.save(purchaseDtl);
			purchaseDtl = saveAndPublishService.savePurchaseDtl(purchaseDtl,mybranch);
			/*if(null!=purchaseDtl) {
				saveAndPublishService.publishObjectToKafkaEvent(purchaseDtl, 
						mybranch, KafkaMapleEventType.PURCHASEDTL,
						KafkaMapleEventType.SERVER);
			}else {
				return;
			}*/
			
		}
	}
	
	@PutMapping("{companymstid}/purchasedtl/updatefcfields")
	public void updatePurchaseDtlFCFields(@PathVariable(value = "companymstid")String companymstid,
			@RequestBody PurchaseDtl purchaseDtlReq)
	{
		Optional<PurchaseDtl> purchaseDtlop = purchaseDtlRepository.findById(purchaseDtlReq.getId());
		if(purchaseDtlop.isPresent())
		{
			PurchaseDtl purchaseDtl = purchaseDtlop.get();
			purchaseDtl.setFcNetCost(purchaseDtlReq.getFcNetCost());
			purchaseDtl.setFcAmount(purchaseDtlReq.getFcAmount());
			if(null != purchaseDtlReq.getDiscount())
			{
			purchaseDtl.setFcDiscount(purchaseDtlReq.getDiscount());
			}
			purchaseDtl.setFcPurchaseRate(purchaseDtlReq.getFcPurchaseRate());
			purchaseDtl.setFcTaxAmt(purchaseDtlReq.getFcTaxAmt());
			purchaseDtl.setFcTaxRate(purchaseDtlReq.getFcTaxRate());
//			purchaseDtlRepository.save(purchaseDtl);
			purchaseDtl = saveAndPublishService.savePurchaseDtl(purchaseDtl,mybranch);
				/*if(null!=purchaseDtl) {
					saveAndPublishService.publishObjectToKafkaEvent(purchaseDtl,
							mybranch, KafkaMapleEventType.PURCHASEDTL, KafkaMapleEventType.SERVER);
				}else {
					return;
				}*/
			
		}
	}
	
	@GetMapping("{companymstid}/purchasedtl/autogeneratebatch/{itemid}")
	public String autoGenerateBatch(@PathVariable(value ="itemid")String itemid)
	{
		String batch=null;
		List<ItemPropertyConfig> itemPropertyConfig = itemPropertyConfigRepo.findByItemId(itemid);
		if(itemPropertyConfig.size()>0)
		{
			 batch =purchaseReportService.autoGenerateBatch(itemid);
		}
		else
		{
			batch = MapleConstants.Nobatch;
		}
		return batch;
	}
}
