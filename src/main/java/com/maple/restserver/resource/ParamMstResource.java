package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.NutritionMst;
import com.maple.restserver.entity.ParamMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ParamMstRepository;
import com.maple.restserver.service.ParamService;
import com.maple.restserver.utils.EventBusFactory;

@RestController
public class ParamMstResource {
	EventBus eventBus = EventBusFactory.getEventBus();
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	private ParamService paramService;
	@Autowired
	ParamMstRepository paramMstRepo;
	
	@PostMapping("{companymstid}/parammstresource/saveparammst")
	public ParamMst createParamMst(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 ParamMst paramMst)
    {
		
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		paramMst.setCompanyMst(companyMst.get());
       return paramMstRepo.saveAndFlush(paramMst);
		 
	}
	
	@GetMapping("{companymstid}/parammstresource/findbyname/{string}")
	public ParamMst ParamMstByName(	@PathVariable(value = "string")String string)
	{
		return paramMstRepo.findByParamName(string);
	}
	@GetMapping("{companymstid}/paramsearch")
	public @ResponseBody List<ParamMst> searchParam(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("data") String searchstring) {
		searchstring = searchstring.replaceAll("%20", " ");
		return paramService.searchLikeParamByName("%" + searchstring + "%", companymstid);
	}


}
