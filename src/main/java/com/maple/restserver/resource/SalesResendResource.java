package com.maple.restserver.resource;

 
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BankMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.report.entity.ItemBatchDtlReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.service.CallForResendBulkTransactionService;
import com.maple.restserver.service.SalesFinalSaveServiceImpl;
import com.maple.restserver.service.YearEndService;
import com.maple.restserver.utils.SystemSetting;
@RestController
public class SalesResendResource {

	@Autowired
	SalesFinalSaveServiceImpl salesFinalSaveServiceImpl;
	
	@Autowired
 SalesTransHdrRepository salesTransHdrRepository;
 
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	CallForResendBulkTransactionService callForResendBulkTransactionService;
	
	
	
	@Transactional
	@GetMapping("{companymstid}/salesresend")
	public  void resendSalesTransaction(
			@PathVariable(value = "companymstid") String companymstid ,
			@RequestParam("fromDate") String  fromDate,
			@RequestParam("toDate") String  toDate
			)
	{
		
		Date fDate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
		Date tDate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
		 
		List<SalesTransHdr> result = salesTransHdrRepository.salesSummaryReport(fDate,tDate);
		
		for (SalesTransHdr salesTransHdr : result) {
			salesFinalSaveServiceImpl.commonPublishForSales(salesTransHdr);
			
		}
		/*
		 * 
		 */
	}
	
	
	
	
	
	@Transactional
	@GetMapping("{companymstid}/salesresendresource/callforresendbulktransaction/{branchCode}/{vouchertype}")
	public  void callForResendBulkTransaction(
			@PathVariable(value = "companymstid") String companymstid ,
			@PathVariable(value = "branchCode") String branchCode,
			@PathVariable(value = "vouchertype") String vouchertype,
			@RequestParam("fromDate") String  fromDate,
			@RequestParam("toDate") String  toDate
			)
	{
		
		Date fDate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
		Date tDate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
		
		
		
		
		callForResendBulkTransactionService.publishforbulktransaction(branchCode,fDate,tDate,vouchertype);
		
		
	}
	
	
	
	
	
	
	
	
}
