package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.BatchwisePerformancereport;
import com.maple.restserver.service.BatchwisePerformanceReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class BatchWisePerformanceReportResource {

	
	
	@Autowired
	BatchwisePerformanceReportService batchwisePerformanceReportService;
	
	
	@GetMapping("/{companymstid}/batchwiseperformancereport/{branchcode}/{itemgroup}")
	public List<BatchwisePerformancereport> BatchwisePerformancereport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,	@PathVariable(value = "itemgroup") List<String> itemgroup,  
			@RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	java.util.Date TDate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		return batchwisePerformanceReportService.batchwisePerformanceReport(companymstid,branchcode,fDate,TDate,itemgroup);
	}
}
