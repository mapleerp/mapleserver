
package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.MultiBedroomMst;
import com.maple.restserver.repository.MultiBedroomMstRepository;



@RestController
@Transactional
public class MultiBedroomMstResource {
	@Autowired
	private MultiBedroomMstRepository multibedroommaster;
	@GetMapping("{companymstid}/multibedroommst")
	public List<MultiBedroomMst> retrieveAlldepartments()
	{
		return multibedroommaster.findAll();
		
	}
	@PostMapping("{companymstid}/multibedroommst")
	public ResponseEntity<Object>createUser(@Valid @RequestBody MultiBedroomMst MultiBedroomMst1)
	{
		MultiBedroomMst saved=multibedroommaster.saveAndFlush(MultiBedroomMst1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}

}

