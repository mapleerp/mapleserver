package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.ProductConversionReport;
import com.maple.restserver.service.ProductConversionReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class ProductConversionReportResource {

	@Autowired
	ProductConversionReportService productConversionReportService;
	
	

	@GetMapping("/{companymstid}/productconversionreportresource/productconversionreport/{branchcode}")
	public 	List<ProductConversionReport> fetchProductConversionReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,	  
			@RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	java.util.Date TDate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		return productConversionReportService.fetchProductConversionReport(companymstid,branchcode,fDate,TDate);
		
		
	}
	
	
}
