package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.maple.restserver.entity.ProcedurePriceDtl;
import com.maple.restserver.repository.ProcedurePriceDtlRepository;

@RestController
@Transactional
public class ProcedurePriceDtlResource {
	
	@Autowired
	private ProcedurePriceDtlRepository procedurePriceDtlRepository;
	
	@GetMapping("{companymstid}/procedurepricedtl")
	public List<ProcedurePriceDtl> retrieveAllProcedureProcedurePriceDtl(){
		return procedurePriceDtlRepository.findAll();
	}
	
	@PostMapping("{companymstid}/procedurepricedtl")
	public ResponseEntity<Object> createProcedurePriceDtl(@Valid @RequestBody 
			ProcedurePriceDtl procedurePriceDtl)
	{
		ProcedurePriceDtl saved = procedurePriceDtlRepository.saveAndFlush(procedurePriceDtl);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/(id)").buildAndExpand(saved.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	

}
