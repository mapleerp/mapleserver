//package com.maple.restserver.resource;
//
//import java.sql.Date;
//import java.time.LocalDate;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Optional;
//
//import javax.transaction.Transactional;
//import javax.validation.Valid;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
////import org.camunda.bpm.engine.RuntimeService;
////import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.maple.restserver.entity.CustomerMst;
//import com.maple.restserver.entity.LmsQueueMst;
//import com.maple.restserver.entity.SalesDtl;
//import com.maple.restserver.entity.Supplier;
//import com.maple.restserver.entity.UnitMst;
//import com.maple.restserver.repository.CustomerMstRepository;
//import com.maple.restserver.repository.LmsQueueMstRepository;
//import com.maple.restserver.repository.SupplierRepository;
//import com.google.common.eventbus.EventBus;
//import com.maple.maple.util.MapleConstants;
//import com.maple.restserver.cloud.api.ExternalApi;
//import com.maple.restserver.entity.AccountHeads;
//import com.maple.restserver.entity.AccountPayable;
//import com.maple.restserver.entity.AddKotTable;
//import com.maple.restserver.entity.CategoryMst;
//import com.maple.restserver.entity.CompanyMst;
//import com.maple.restserver.entity.Supplier;
//import com.maple.restserver.entity.UnitMst;
//import com.maple.restserver.exception.ResourceNotFoundException;
//import com.maple.restserver.repository.AccountHeadsRepository;
//import com.maple.restserver.repository.CompanyMstRepository;
//import com.maple.restserver.service.AccountHeadsService;
//import com.maple.restserver.service.CustomerRegistrationServiceImp;
//import com.maple.restserver.service.SaveAndPublishService;
//import com.maple.restserver.utils.EventBusFactory;
//import com.maple.restserver.utils.SystemSetting;
//
///**
// * @author user
// *
// */
//@RestController
//public class CustomerRegistrationResourse {
//	private static final Logger logger = LoggerFactory.getLogger(PurchaseHdrResource.class);
//	
//	@Value("${mybranch}")
//	private String mybranch;
//
//	@Autowired
//	private CustomerMstRepository customerRegRepo;
//	@Autowired
//	private CustomerRegistrationServiceImp customerRegervice;
//
//	@Autowired
//	private AccountHeadsRepository accountHeadsRepository;
//
//	@Autowired
//	SupplierRepository supplierRepository;
//	@Autowired
//	LmsQueueMstRepository lmsQueueMstRepository;
//
//	EventBus eventBus = EventBusFactory.getEventBus();
//
//	@Autowired
//	CompanyMstRepository companyMstRepo;
//	
//	@Autowired
//	SaveAndPublishService saveAndPublishService;
//	
//	
//	
//	@Autowired
//	private ExternalApi externalApi;
//	@Autowired
//	private AccountHeadsService accountHeadsService;
//
//	// @Autowired
//	// private RuntimeService runtimeService;
//	Pageable topFifty =   PageRequest.of(0, 50);
//
//	@GetMapping("/{companymstid}/customerregistrations")
//	public List<CustomerMst> retrieveAllCustomer(@PathVariable(value = "companymstid") String companymstid) {
//
//		List<CustomerMst> alist = customerRegRepo.findByCompanyMstId(companymstid);
//
//		return alist;
//	}
//
//	// version3.5-sari
//	@Transactional
//	@GetMapping("{companymstid}/customerregistration/setdiscount")
//	public String updateCustomerDiscount() {
//
//		customerRegRepo.updateCustDiscount();
//		return "success";
//	}
//
//	// version3.5end
//	@PutMapping("{companymstid}/customerregistration/{custid}/updatecustomerrank")
//	public CustomerMst updateCustomerRank(@PathVariable(value = "custid") String custid,
//			@PathVariable(value = "companymstid") String companymstid,
//			@Valid @RequestBody CustomerMst customerMstRequest) {
//
//		CustomerMst customermst = customerRegRepo.findById(custid).get();
//
//		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
//		CompanyMst companyMst = comapnyMstOpt.get();
//		customermst.setCompanyMst(companyMst);
//		customermst.setRank(customerMstRequest.getRank());
////		customermst = customerRegRepo.save(customermst);
//		customermst =saveAndPublishService.saveCustomerMst(customermst,mybranch);
//		logger.info("customermst send to KafkaEvent: {}", customermst);
//		
//		return customermst;
//	}
//
//	@PutMapping("{companymstid}/customerregistration/{custid}/updatecustomer")
//	public CustomerMst updateCustomerReg(@PathVariable(value = "custid") String custid,
//			@PathVariable(value = "companymstid") String companymstid,
//			@Valid @RequestBody CustomerMst customerMstRequest) {
//
//		CustomerMst customermst = customerRegRepo.findById(custid).get();
//
//		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
//		CompanyMst companyMst = comapnyMstOpt.get();
//		customermst.setCompanyMst(companyMst);
//		customermst.setCreditPeriod(customerMstRequest.getCreditPeriod());
//		customermst.setCustomerAddress(customerMstRequest.getCustomerAddress());
//		customermst.setCustomerContact(customerMstRequest.getCustomerContact());
//		customermst.setDrugLicenseNumber(customerMstRequest.getDrugLicenseNumber());
//		customermst.setCustomerGroup(customerMstRequest.getCustomerGroup());
//		customermst.setCustomerGst(customerMstRequest.getCustomerGst());
//		customermst.setCustomerMail(customerMstRequest.getCustomerMail());
//		customermst.setCustomerName(customerMstRequest.getCustomerName());
//		customermst.setCustomerState(customerMstRequest.getCustomerState());
//		customermst.setCustomerDiscount(customerMstRequest.getCustomerDiscount());
//		customermst.setDiscountProperty(customerMstRequest.getDiscountProperty());
//		customermst.setPriceTypeId(customerMstRequest.getPriceTypeId());
//		customermst.setCustomerName(customerMstRequest.getCustomerName());
//		customermst.setBankName(customerMstRequest.getBankName());
//		customermst.setBankIfsc(customerMstRequest.getBankIfsc());
//		customermst.setTaxInvoiceFormat(customerMstRequest.getTaxInvoiceFormat());
//		customermst.setBankAccountName(customerMstRequest.getBankAccountName());
//		customermst.setCustomerType(customerMstRequest.getCustomerType());
//		customermst.setCurrencyId(customerMstRequest.getCurrencyId());
//		customermst.setCustomerCountry(customerMstRequest.getCustomerCountry());
////		customermst = customerRegRepo.save(customermst);
//		customermst =saveAndPublishService.saveCustomerMst(customermst,mybranch);
//		logger.info("customermst send to KafkaEvent: {}", customermst);
//		
//
//		Optional<AccountHeads> accountHeadsOpt = accountHeadsRepository.findById(customermst.getId());
//		AccountHeads accountheads = accountHeadsOpt.get();
//		AccountHeads accounHeadByAsset = accountHeadsRepository.findByAccountName("ASSETS");
//		AccountHeads accountByGroup = accountHeadsRepository.findByAccountName(customermst.getCustomerGroup());
//
//		accountheads.setAccountName(accountheads.getAccountName());
//		accountheads.setRootParentId(accounHeadByAsset.getId());
//		accountheads.setParentId(accountByGroup.getId());
//		accountheads = accountHeadsRepository.save(accountheads);
//		accountheads.setAccountName(customerMstRequest.getCustomerName());
//		accountheads.setCurrencyId(customerMstRequest.getCurrencyId());
//		Supplier supplier = null;
//		Optional<Supplier> supplierOptional = supplierRepository.findById(custid);
//		if (supplierOptional.isPresent()) {
//			supplier = supplierOptional.get();
//			supplier.setCompanyMst(customermst.getCompanyMst());
//			supplier.setPhoneNo(customermst.getCustomerContact());
//			supplier.setAddress(customermst.getCustomerAddress());
//			supplier.setState(customermst.getCustomerState());
//			supplier.setSupGST(customermst.getCustomerGst());
//			supplier.setEmailid(customermst.getCustomerMail());
//			supplier.setAccount_id(customermst.getId());
//			supplier.setCerditPeriod(customermst.getCreditPeriod());
//			supplier.setCompany(customermst.getCustomerName());
//			supplier.setSupplierName(customermst.getCustomerName());
//			supplier.setSupplierId(customermst.getId());
//			supplier.setCerditPeriod(customermst.getCreditPeriod());
//			supplier.setCurrencyId(customermst.getCurrencyId());
//			supplier = supplierRepository.save(supplier);
//		} else {
//			supplier = new Supplier();
//			supplier.setCompanyMst(customermst.getCompanyMst());
//			supplier.setPhoneNo(customermst.getCustomerContact());
//			supplier.setAddress(customermst.getCustomerAddress());
//			supplier.setState(customermst.getCustomerState());
//			supplier.setSupGST(customermst.getCustomerGst());
//			supplier.setEmailid(customermst.getCustomerMail());
//			supplier.setAccount_id(customermst.getId());
//			supplier.setCerditPeriod(customermst.getCreditPeriod());
//			supplier.setCompany(customermst.getCustomerName());
//			supplier.setSupplierName(customermst.getCustomerName());
//			supplier.setSupplierId(customermst.getId());
//			supplier.setCerditPeriod(customermst.getCreditPeriod());
//			supplier.setId(customermst.getId());
//			supplier.setCurrencyId(customermst.getCurrencyId());
////			supplier = supplierRepository.save(supplier);
//			supplier=saveAndPublishService.saveSupplier(supplier, mybranch);
//			logger.info("supplierMSt send to KafkaEvent: {}", supplier);
//		}
//		
//		
//		//==============commented for block the msg===========by anandu======02-08-2021=========
//		
//		
//		Map<String, Object> variables = new HashMap<String, Object>();
//		variables.put("voucherNumber", customermst.getId());
//		variables.put("voucherDate", SystemSetting.getSystemDate());
//		variables.put("inet", 0);
//		variables.put("id", customermst.getId());
//		variables.put("branchcode", customermst.getCompanyMst().getId());
//		variables.put("companyid", customermst.getCompanyMst());
//		variables.put("REST", 1);
//
//		variables.put("WF", "forwardCustomer");
//
//		String workflow = (String) variables.get("WF");
//		String voucherNumber = (String) variables.get("voucherNumber");
//		String sourceID = (String) variables.get("id");
//
//		LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//
//		// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
//		Object obj = variables.get("voucherDate");
//		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
//		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//		lmsQueueMst.setVoucherDate(sqlVDate);
//
//		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardCustomer");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow + sourceID);
//		lmsQueueMst.setJobGroup(workflow);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID);
//
//		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//		variables.put("lmsqid", lmsQueueMst.getId());
//		eventBus.post(variables);
//
//		Map<String, Object> variables1 = new HashMap<String, Object>();
//
//		variables1.put("voucherNumber", supplier.getId());
//
//		variables1.put("voucherDate", SystemSetting.getSystemDate());
//		variables1.put("inet", 0);
//		variables.put("id", supplier.getId());
//		variables1.put("branchcode", supplier.getCompanyMst().getId());
//		variables1.put("companyid", supplier.getCompanyMst());
//
//		variables1.put("REST", 1);
//
//		variables1.put("WF", "forwardSupplier");
//
//		String workflow1 = (String) variables1.get("WF");
//		String voucherNumber1 = (String) variables1.get("voucherNumber");
//		String sourceID1 = (String) variables1.get("id");
//
//		lmsQueueMst = new LmsQueueMst();
//
//		lmsQueueMst.setCompanyMst((CompanyMst) variables1.get("companyid"));
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//		java.util.Date uDate1 = (java.util.Date) variables1.get("voucherDate");
//		java.sql.Date sqlVDate1 = SystemSetting.UtilDateToSQLDate(uDate1);
//		lmsQueueMst.setVoucherDate(sqlVDate1);
//
//		lmsQueueMst.setVoucherNumber((String) variables1.get("voucherNumber"));
//		lmsQueueMst.setVoucherType(workflow1);
//		lmsQueueMst.setPostedToServer("NO");
//		lmsQueueMst.setJobClass("forwardSupplier");
//		lmsQueueMst.setCronJob(true);
//		lmsQueueMst.setJobName(workflow1 + sourceID1);
//		lmsQueueMst.setJobGroup(workflow1);
//		lmsQueueMst.setRepeatTime(60000L);
//		lmsQueueMst.setSourceObjectId(sourceID1);
//
//		lmsQueueMst.setBranchCode((String) variables1.get("branchcode"));
//
//		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//
//		variables.put("lmsqid", lmsQueueMst.getId());
//
//		eventBus.post(variables);
//
//		Map<String, Object> variables2 = new HashMap<String, Object>();
//
//		variables2.put("voucherNumber", accountheads.getId());
//		variables2.put("voucherDate", SystemSetting.getSystemDate());
//
//		variables2.put("id", accountheads.getId());
//		variables2.put("inet", 0);
//		variables2.put("REST", 1);
//		variables2.put("companyid", accountheads.getCompanyMst());
//		variables2.put("branchcode", accountheads.getCompanyMst().getId());
//
//		variables2.put("WF", "forwardAccountHeads");
//
//		String workflow2 = (String) variables2.get("WF");
//		String voucherNumber2 = (String) variables2.get("voucherNumber");
//		String sourceID2 = (String) variables2.get("id");
//
//		LmsQueueMst lmsQueueMst1 = new LmsQueueMst();
//
//		lmsQueueMst1.setCompanyMst((CompanyMst) variables1.get("companyid"));
//
//		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//		java.util.Date uDate2 = SystemSetting.localToUtilDate(LocalDate.now());
//		java.sql.Date sqlVDate2 = SystemSetting.UtilDateToSQLDate(uDate2);
//		lmsQueueMst1.setVoucherDate(sqlVDate2);
//
//		lmsQueueMst1.setVoucherNumber((String) variables2.get("voucherNumber"));
//		lmsQueueMst1.setVoucherType(workflow2);
//		lmsQueueMst1.setPostedToServer("NO");
//
////	 			String jobClass = getJobClass(workflow, lmsQueueMst);
////	 			if (null == jobClass) {
////	 				logger.info("jobClass not found for " + workflow);
////	 				return;
////	 			}
//		lmsQueueMst1.setJobClass("forwardAccountHeads");
//		lmsQueueMst1.setCronJob(true);
//		lmsQueueMst1.setJobName(workflow2 + sourceID2);
//		lmsQueueMst1.setJobGroup(workflow2);
//		lmsQueueMst1.setRepeatTime(60000L);
//		lmsQueueMst1.setSourceObjectId(sourceID2);
//
//		lmsQueueMst1.setBranchCode((String) variables1.get("branchcode"));
//
//		lmsQueueMst1 = lmsQueueMstRepository.saveAndFlush(lmsQueueMst1);
//		variables1.put("lmsqid", lmsQueueMst1.getId());
//
//		eventBus.post(variables1);
//		
//		//===================comment ended ============anandu =====================
//		
//		return customermst;
//
//	}
//
//	@PostMapping("{companymstid}/customerregistration")
//	public CustomerMst createcustomer(@PathVariable(value = "companymstid") String companymstid,
//			@Valid @RequestBody CustomerMst customerReg) {
//		return companyMstRepo.findById(companymstid).map(companyMst -> {
//			List<CustomerMst> custList = customerRegRepo.findByCustomerName(customerReg.getCustomerName());
//			if (custList.size() == 0) {
//				customerReg.setCompanyMst(companyMst);
//
//				//CustomerMst savedCustomer = customerRegRepo.saveAndFlush(customerReg);
//				
//				CustomerMst savedCustomer = saveAndPublishService.saveCustomerMst(customerReg, mybranch);
//				
//
//				Supplier supplier = supplierRepository
//						.findBySupplierNameAndCompanyMst(savedCustomer.getCustomerName(), companyMst);
//
//				if (null == supplier) {
//					supplier= new Supplier();
//					
//					supplier.setId(savedCustomer.getId());
//					supplier.setCompanyMst(savedCustomer.getCompanyMst());
//					supplier.setPhoneNo(savedCustomer.getCustomerContact());
//					supplier.setAddress(savedCustomer.getCustomerAddress());
//					supplier.setState(savedCustomer.getCustomerState());
//					supplier.setSupGST(savedCustomer.getCustomerGst());
//					supplier.setEmailid(savedCustomer.getCustomerMail());
//					supplier.setAccount_id(savedCustomer.getId());
//					supplier.setCerditPeriod(savedCustomer.getCreditPeriod());
//					supplier.setCompany(savedCustomer.getCustomerName());
//					supplier.setSupplierName(savedCustomer.getCustomerName());
//					supplier.setSupplierId(savedCustomer.getId());
//					supplier.setCurrencyId(savedCustomer.getCurrencyId());
//					supplier.setCerditPeriod(savedCustomer.getCreditPeriod());
//
////					supplierRepository.save(supplier);
//					saveAndPublishService.saveSupplier(supplier, mybranch);
//					 logger.info("SaveSupplier send to KafkaEvent: {}", supplier);
//				}
//
//				AccountHeads accountHeads = accountHeadsRepository
//						.findByAccountNameAndCompanyMstId(savedCustomer.getCustomerName(), companyMst.getId());
//
//				if (null == accountHeads) {
//
//					AccountHeads accounHeadByAsset = accountHeadsRepository.findByAccountName("ASSETS");
//					
//						AccountHeads accountByGroup = accountHeadsRepository
//								.findByAccountName(savedCustomer.getCustomerGroup());
//						accountHeads = new AccountHeads();
//						
//						accountHeads.setAccountName(savedCustomer.getCustomerName());
//						accountHeads.setGroupOnly("N");
//						accountHeads.setId(savedCustomer.getId());
//						accountHeads.setCompanyMst(savedCustomer.getCompanyMst());
//						accountHeads.setCurrencyId(savedCustomer.getCurrencyId());
//						if (null != accountByGroup) {
//							accountHeads.setParentId(accountByGroup.getId());
//						}
//						if (null != accounHeadByAsset) {
//							accountHeads.setRootParentId(accounHeadByAsset.getId());
//						}
//						accountHeads = accountHeadsRepository.save(accountHeads);
//
//				}
//				
//				//==============commented for block the msg===========by anandu======02-08-2021=========
//				
//				Map<String, Object> variables = new HashMap<String, Object>();
//				variables.put("voucherNumber", savedCustomer.getId());
//				variables.put("voucherDate", SystemSetting.getSystemDate());
//				variables.put("inet", 0);
//				variables.put("id", savedCustomer.getId());
//				variables.put("branchcode", savedCustomer.getCompanyMst().getId());
//				variables.put("companyid", savedCustomer.getCompanyMst());
//				variables.put("REST", 1);
//
//				variables.put("WF", "forwardCustomer");
//
//				String workflow = (String) variables.get("WF");
//				String voucherNumber = (String) variables.get("voucherNumber");
//				String sourceID = (String) variables.get("id");
//
//				LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//				// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
//
//				java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
//				java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//				lmsQueueMst.setVoucherDate(sqlVDate);
//
//				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//				lmsQueueMst.setVoucherType(workflow);
//				lmsQueueMst.setPostedToServer("NO");
//				lmsQueueMst.setJobClass("forwardCustomer");
//				lmsQueueMst.setCronJob(true);
//				lmsQueueMst.setJobName(workflow + sourceID);
//				lmsQueueMst.setJobGroup(workflow);
//				lmsQueueMst.setRepeatTime(60000L);
//				lmsQueueMst.setSourceObjectId(sourceID);
//
//				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//				variables.put("lmsqid", lmsQueueMst.getId());
//				eventBus.post(variables);
//				
//				//==========================comment ended============anandu======================
//				
//
//				/*
//				 * ProcessInstanceWithVariables pVariablesInReturn =
//				 * runtimeService.createProcessInstanceByKey("forwardCustomer")
//				 * .setVariables(variables)
//				 * 
//				 * .executeWithVariablesInReturn();
//				 */
//
////	 	  
////	 		Map<String, Object> variables1 = new HashMap<String, Object>();
//// 			variables1.put("voucherNumber", savedCustomer.getId());
//// 			variables1.put("voucherDate",SystemSetting.getSystemDate());
////			variables1.put("inet", 0);
////			variables1.put("id", savedCustomer.getId());		 
////			variables1.put("companyid", savedCustomer.getCompanyMst());
////		 			variables1.put("REST",1);		 
////	 	
////		 			variables1.put("WF", "forwardAccountHeads");
////		 			eventBus.post(variables1);
//
//				
//				//==============commented for block the msg===========by anandu======02-08-2021=========
//				
//				Map<String, Object> variables1 = new HashMap<String, Object>();
//
//				variables1.put("voucherNumber", accountHeads.getId());
//				variables1.put("voucherDate", SystemSetting.getSystemDate());
//
//				variables1.put("id", accountHeads.getId());
//				variables1.put("inet", 0);
//				variables1.put("REST", 1);
//				variables1.put("companyid", accountHeads.getCompanyMst());
//				variables1.put("branchcode", accountHeads.getCompanyMst().getId());
//
//				variables1.put("WF", "forwardAccountHeads");
//
//				String workflow1 = (String) variables1.get("WF");
//				String voucherNumber1 = (String) variables1.get("voucherNumber");
//				String sourceID1 = (String) variables1.get("id");
//
//				LmsQueueMst lmsQueueMst1 = new LmsQueueMst();
//
//				lmsQueueMst1.setCompanyMst((CompanyMst) variables1.get("companyid"));
//
//				// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//				java.util.Date uDate1 = SystemSetting.localToUtilDate(LocalDate.now());
//				java.sql.Date sqlVDate1 = SystemSetting.UtilDateToSQLDate(uDate1);
//				lmsQueueMst1.setVoucherDate(sqlVDate1);
//
//				lmsQueueMst1.setVoucherNumber((String) variables1.get("voucherNumber"));
//				lmsQueueMst1.setVoucherType(workflow1);
//				lmsQueueMst1.setPostedToServer("NO");
//
////		 			String jobClass = getJobClass(workflow, lmsQueueMst);
////		 			if (null == jobClass) {
////		 				logger.info("jobClass not found for " + workflow);
////		 				return;
////		 			}
//				lmsQueueMst1.setJobClass("forwardAccountHeads");
//				lmsQueueMst1.setCronJob(true);
//				lmsQueueMst1.setJobName(workflow1 + sourceID1);
//				lmsQueueMst1.setJobGroup(workflow1);
//				lmsQueueMst1.setRepeatTime(60000L);
//				lmsQueueMst1.setSourceObjectId(sourceID1);
//
//				lmsQueueMst1.setBranchCode((String) variables1.get("branchcode"));
//
//				lmsQueueMst1 = lmsQueueMstRepository.saveAndFlush(lmsQueueMst1);
//				variables1.put("lmsqid", lmsQueueMst1.getId());
//
//				eventBus.post(variables1);
//
//				Map<String, Object> variables2 = new HashMap<String, Object>();
//
//				variables2.put("voucherNumber", supplier.getId());
//
//				variables2.put("voucherDate", SystemSetting.getSystemDate());
//				variables2.put("inet", 0);
//				variables2.put("id", supplier.getId());
//				variables2.put("branchcode", supplier.getCompanyMst().getId());
//				variables2.put("companyid", supplier.getCompanyMst());
//
//				variables2.put("REST", 1);
//
//				variables2.put("WF", "forwardSupplier");
//
//				String workflow2 = (String) variables2.get("WF");
//				String voucherNumber2 = (String) variables2.get("voucherNumber");
//				String sourceID2 = (String) variables2.get("id");
//
//				lmsQueueMst = new LmsQueueMst();
//
//				lmsQueueMst.setCompanyMst((CompanyMst) variables2.get("companyid"));
//				// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//				java.util.Date uDate2 = (java.util.Date) variables2.get("voucherDate");
//				java.sql.Date sqlVDate2 = SystemSetting.UtilDateToSQLDate(uDate2);
//				lmsQueueMst.setVoucherDate(sqlVDate2);
//
//				lmsQueueMst.setVoucherNumber((String) variables2.get("voucherNumber"));
//				lmsQueueMst.setVoucherType(workflow2);
//				lmsQueueMst.setPostedToServer("NO");
//				lmsQueueMst.setJobClass("forwardSupplier");
//				lmsQueueMst.setCronJob(true);
//				lmsQueueMst.setJobName(workflow2 + sourceID2);
//				lmsQueueMst.setJobGroup(workflow2);
//				lmsQueueMst.setRepeatTime(60000L);
//				lmsQueueMst.setSourceObjectId(sourceID2);
//
//				lmsQueueMst.setBranchCode((String) variables2.get("branchcode"));
//
//				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//
//				variables.put("lmsqid", lmsQueueMst.getId());
//
//				eventBus.post(variables);
//				
//				//================comment ended ==========anandu=======================
//				
//				
//				/*
//				 * ProcessInstanceWithVariables pVariablesInReturn1 =
//				 * runtimeService.createProcessInstanceByKey("forwardAccountHeads")
//				 * .setVariables(variables1)
//				 * 
//				 * .executeWithVariablesInReturn();
//				 * 
//				 */
////				ResponseEntity<CustomerMst> saved=	externalApi.customerDtlSavedToCloud(companymstid, savedCustomer);
//				
//				return savedCustomer;
//
//			}
//			return customerReg;
//		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
//	}
//
//	@GetMapping("{companymstid}/customermst/{custid}")
//	public Optional<CustomerMst> retrievCustomerMstById(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable String custid) {
//		return customerRegRepo.findByIdAndCompanyMstId(custid, companymstid);
//	}
//
//	@GetMapping("{companymstid}/customermstbyname/{customername}")
//	public CustomerMst retrievCustomerMstByName(
//
//			@PathVariable(value = "customername") String customername,
//			@PathVariable(value = "companymstid") String companymstid) {
//
//		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
//
//		List<CustomerMst> customerList = customerRegRepo.findByCustomerNameAndCompanyMst(customername,
//				companyOpt.get());
//
//		if (customerList.size() == 0) {
//			return null;
//		}
//
//		return customerList.get(0);
//	}
//
//	@GetMapping("{companymstid}/customermstbynamelist/{customername}")
//	public List<CustomerMst> retrievCustomerMstByNameList(
//
//			@PathVariable(value = "customername") String customername,
//			@PathVariable(value = "companymstid") String companymstid) {
//
//		List<CustomerMst> custList = customerRegRepo.findByCustomerNameAndCompanyMstIdList(customername, companymstid);
//		return custList;
//	}
//
//	@GetMapping("{companymstid}/customermstbynameandbrachcode/{customername}/{branchcode}")
//	public Optional<CustomerMst> retrievCustomerMstByNameAndBrachCode(
//
//			@PathVariable(value = "customername") String customername,
//			@PathVariable(value = "branchcode") String branchcode,
//			@PathVariable(value = "companymstid") String companymstid) {
//
//		return customerRegRepo.findByCustomerNameAndCompanyMstIdAndBranchCode(customername, companymstid,
//				"%" + companymstid + branchcode + "%");
//	}
//
//	@GetMapping("{companymstid}/customersearch")
//	public @ResponseBody List<CustomerMst> searchCustomer(@PathVariable(value = "companymstid") String companymstid,
//			@RequestParam("data") String searchstring) {
//		return customerRegervice.searchLikeCustomerByName("%" + searchstring + "%", companymstid, topFifty);
//	}
//
//	@GetMapping("/{companymstid}/customerbyphonenumber/{customerphone}")
//	public CustomerMst retrieveCustomer(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "customerphone") String customerphone) {
//
//		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
//
//		return customerRegRepo.findByCompanyMstAndCustomerContact(companyMst.get(), customerphone);
//	}
//
//	@Transactional
//	@GetMapping("/{companymstid}/customerregistration/updateallcustoerbyreportname/{reportname}")
//	public String updateAllCustomerByReportName(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "reportname") String reportname) {
//
//		customerRegRepo.updateAllCustomerByReportName(reportname);
//		return "Success";
//	}
//
//	@Transactional
//	@GetMapping("/{companymstid}/customerregistration/updateallcustoerbyreportnameandpriceid/{priceid}/{reportname}")
//	public String updateAllCustomerByReportName(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "reportname") String reportname, @PathVariable(value = "priceid") String priceid) {
//
//		customerRegRepo.updateAllCustomerByReportNameAndPriceId(reportname, priceid);
//		return "Success";
//	}
//
//	@GetMapping("/{companymstid}/customerregistration/getgsttype/{state}")
//	public String getCustomerGstType(@PathVariable(value = "state") String state) {
//		return customerGstType(state);
//	}
//
//	public String customerGstType(String state) {
//		if (state.equalsIgnoreCase("KERALA")) {
//			return "GST";
//		} else {
//			return "IGST";
//		}
//	}
//
//	@GetMapping("/{companymstid}/customerregistration/customerbygst/{gst}")
//	public List<CustomerMst> retrieveCustomerMstByGst(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "gst") String gst) {
//
//		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
//
//		return customerRegRepo.findByCompanyMstAndCustomerGst(companyMst.get(), gst);
//	}
//
//	
//
//	
//	// ------------------new url ----publish offline button
//
//			@PostMapping("{companymstid}/customerregistration/{status}")
//			public CustomerMst sendOffLineMessageToCloud(@Valid @RequestBody CustomerMst customerReg,
//					@PathVariable(value = "companymstid") String companymstid,
//					@PathVariable("status") String status) {
//				 ResponseEntity<CustomerMst> saved=	externalApi.sendCustomerMstOffLineMessageToCloud(companymstid, customerReg);
//					
//				return saved.getBody();
//
//			}
//
//			/* Data is present in Customer  but not in Account heads. 
//			 *
//			 * Common entity for account heads, customer. 
//			 * Extra properties of customer is entered 
//			 * in seperate entity and link it with common 
//			 * entity using  Id.
//			 * 
//			 * By  Sharon
//			 *
//			 * */
//	
//	
//			@GetMapping("{companymstid}/customerregistrationresourse/retrievecustomermst")
//			public String retrieveCustomerMst(@PathVariable(value = "companymstid") String companymstid) {
//
//				List<CustomerMst> customerList = customerRegRepo.findByCompanyMstId(companymstid);
//				if (customerList.size() == 0) {
//					return null;
//				}
//
//				for (int i = 0; i < customerList.size(); i++) {
//
//					accountHeadsService.accountHeadsManagement(customerList.get(i).getId(),
//							customerList.get(i).getCustomerName(), MapleConstants.CUSTOMER, companymstid);
//
//				}
//
//				return "Success";
//			}
//	
//	
//	
//	
//	
//}
