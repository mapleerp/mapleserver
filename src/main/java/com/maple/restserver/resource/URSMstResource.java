package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PurchaseSchemeMst;
import com.maple.restserver.entity.UrsMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.URSMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class URSMstResource {
	
	private static final Logger logger = LoggerFactory.getLogger(PurchaseHdrResource.class);
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	URSMstRepository ursMstRepo;
	
	@PostMapping("{companymstid}/ursmstresource/ursmstresourcesave")
	public UrsMst createUrsMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody UrsMst ursMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			ursMst.setCompanyMst(companyMst);
//			UrsMst saved= ursMstRepo.save(ursMst);
			UrsMst saved= saveAndPublishService.saveUrsMst(ursMst, mybranch);
			Map<String,Object> variables = new HashMap<String, Object>();
			variables.put("voucherNumber", saved.getId());
			variables.put("voucherDate",SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", saved.getId());		 
			variables.put("companyid", saved.getCompanyMst());
			variables.put("branchcode", saved.getBranchCode());

				variables.put("REST",1);
			 
				variables.put("WF", "forwardUrsMst");
				
				String workflow = (String) variables.get("WF");
				String voucherNumber = (String) variables.get("voucherNumber");
				String sourceID = (String) variables.get("id");

				LmsQueueMst lmsQueueMst = new LmsQueueMst();

				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
				
				//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
				
				java.util.Date uDate = (Date) variables.get("voucherDate");
				java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
				lmsQueueMst.setVoucherDate(sqlVDate);
				
				
				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
				lmsQueueMst.setVoucherType(workflow);
				lmsQueueMst.setPostedToServer("NO");
				lmsQueueMst.setJobClass("forwardUrsMst");
				lmsQueueMst.setCronJob(true);
				lmsQueueMst.setJobName(workflow + sourceID);
				lmsQueueMst.setJobGroup(workflow);
				lmsQueueMst.setRepeatTime(60000L);
				lmsQueueMst.setSourceObjectId(sourceID);
				
				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
				variables.put("lmsqid", lmsQueueMst.getId());
				eventBus.post(variables);
				return saved;
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}
	
	@DeleteMapping("{companymstid}/ursmstresource/ursmstdelete/{id}")
	public void deleteUrsMst(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id	)
	{
		ursMstRepo.deleteById(id);
	}
	
	@PutMapping("{companymstid}/ursmstresource/ursmstupdate/{id}")
	public UrsMst updateUrsMst(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id,@RequestBody UrsMst ursMstReq)
	{
		Optional<UrsMst> ursMstOPt = ursMstRepo.findById(id);
		UrsMst ursMst =ursMstOPt.get();
		ursMst.setEnteredStock(ursMstReq.getEnteredStock());
		ursMst.setItemId(ursMstReq.getItemId());
		ursMst.setSystemStock(ursMstReq.getSystemStock());
		ursMst.setUserId(ursMstReq.getUserId());
//		UrsMst saved =  ursMstRepo.save(ursMst);
		UrsMst saved = saveAndPublishService.saveUrsMst(ursMst, mybranch);
		Map<String,Object> variables = new HashMap<String, Object>();
		variables.put("voucherNumber", saved.getId());
		variables.put("voucherDate",SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", saved.getId());		 
		variables.put("companyid", saved.getCompanyMst());
		variables.put("branchcode", saved.getBranchCode());

			variables.put("REST",1);
		 
			variables.put("WF", "forwardUrsMst");
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");

			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			
			//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardUrsMst");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);
			return saved;
	}
	
	@GetMapping("{companymstid}/ursmstresource/ursshowbydate")
	public List<UrsMst> getUrsMstByDate(@PathVariable(value="companymstid")String companymstid,
			@RequestParam(value="rdate")String rdate)
	{
		Date vdate = SystemSetting.StringToUtilDate(rdate, "yyyy-MM-dd");
		return ursMstRepo.getAllByDate(vdate);
	}
	
	@GetMapping("{companymstid}/ursmstresource/ursshowbyid/{id}")
	public UrsMst getUrsMstById(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="id")String id)
	{
		UrsMst ursMst =null;
		Optional<UrsMst> ursMstOpt = ursMstRepo.findById(id);
		if(ursMstOpt.isPresent())
		{
			ursMst = ursMstOpt.get();
		}
		return ursMst;
	}
	
	@GetMapping("{companymstid}/ursmstresource/ursshowbydateanditemid/{itemid}/{branchcode}")
	public UrsMst getUrsMstByDateAndItemId(@PathVariable(value="companymstid")String companymstid,
			@RequestParam(value="rdate")String rdate,@PathVariable(value="itemid")String itemid,
			@PathVariable(value="branchcode")String branchcode)
	{
		Date vdate = SystemSetting.StringToUtilDate(rdate, "yyyy-MM-dd");
		return ursMstRepo.getAllByDateAndItemId(vdate,itemid);
	}
}
