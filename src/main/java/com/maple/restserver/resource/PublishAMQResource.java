package com.maple.restserver.resource;

import java.util.List;





import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;


/*
 * This class used to publish Item Mst Or Customer Mst from client to mobile device.
 * Individual Item/customer as well as all publish is possible.
 */




@RestController
@Transactional
public class PublishAMQResource {
 
	@Autowired
	CompanyMstRepository companyMstRepo;

	@GetMapping("{companymstid}/publishamqresource/itemmst/{itemid}")
	public String publishSingleItemToMobileDevice(
			@PathVariable(value = "itemid") String itemid){
		
		
		/*
		 * Publish Item to TAB01
		 */
		
		//jmsTemplate.convertAndSend("TAB01", "OK:");
		 
		return  "OK" ;
	}
	
 
	@GetMapping("{companymstid}/publishamqresource/itemmst/allitem")
	public String publishAlItemToMobileDevice(
			@PathVariable(value = "itemid") String itemid){
		
		
		/*
		 * Publish all item to TAB01
		 */
		
		return  "OK" ;
	}
	
	
	@GetMapping("{companymstid}/publishamqresource/customermst/{custoemrid}")
	public String publishSingleCustomerToMobileDevice(
			@PathVariable(value = "itemid") String itemid){
		
		
		/*
		 * Publish customer  to TAB01
		 */
		
		return  "OK" ;
	}
	
 
	@GetMapping("{companymstid}/publishamqresource/itemmst/allcustomer")
	public String publishAlCustomerToMobileDevice(
			@PathVariable(value = "itemid") String itemid){
		
		
		/*
		 * Publish all custoemr  to TAB01
		 */
		
		return  "OK" ;
	}
	

}
