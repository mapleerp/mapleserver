package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KotCategoryMst;
import com.maple.restserver.entity.KotItemMst;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KotCategoryMstRepository;
import com.maple.restserver.repository.KotItemMstRepository;

@RestController
public class KotCategoryMstResource {

	@Autowired
	KotItemMstRepository kotitemMstRepo;
	@Autowired
	ItemMstRepository itemmstRepo;
	@Autowired
CompanyMstRepository companyMstRepository;

	@Autowired
	CategoryMstRepository categoryMstrepo;
	@Autowired
	KotCategoryMstRepository kotCategoryMstRepository;
	@PostMapping("{companymstid}/kotcategorymstresource/kotcategorymst")
	public KotCategoryMst creatKotCategoryMst(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody KotCategoryMst kotCategoryMst )
	{
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		
		kotCategoryMst.setCompanyMst(companyMst);
			 return kotCategoryMstRepository.save(kotCategoryMst);
	}
	
	@DeleteMapping("{companymstid}/deletekotcategorymst/{id}")
	public void KotCategoryMst(@PathVariable(value = "id") String id,
			@PathVariable(value = "companymstid") String companymstid) 
	{
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
//		/*
//		 * Delete Items from kotItemMst with corresponding category id
//		 */
//		KotCategoryMst kotcategory = kotCategoryMstRepository.findById(id).get();
//		List<ItemMst> itemmst = itemmstRepo.findByCompanyMstAndCategoryId(companyMst, kotcategory.getCategoryId());
//		for(int i =0;i<itemmst.size();i++)
//		{
//			List<KotItemMst> kotitemmstlist =kotitemMstRepo.findByItemId(itemmst.get(i).getId());
//			for(int j=0;j<kotitemmstlist.size();j++)
//			{
//				kotitemMstRepo.deleteById(kotitemmstlist.get(j).getId());
//			}
//		}
		kotCategoryMstRepository.deleteById(id);
		kotCategoryMstRepository.flush();
	}
	@GetMapping("{companyid}/kotcategorymstresource/fetchbycategoryid/{categoryid}")
	public  KotCategoryMst getKotCategoryMstByCategoryId(
			@PathVariable (value = "companyid") String companyid , 
			@PathVariable (value = "categoryid") String categoryid) 
	{
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return  kotCategoryMstRepository.findByCompanyMstAndCategoryId(companyMst,categoryid);
		
	}
	
	@GetMapping("{companyid}/kotcategorymstresource/fetchbycategoryidandprinterid/{categoryid}/{printrId}")
	public  KotCategoryMst getKotCategoryMstByCategoryIdAndPrinterID(
			@PathVariable (value = "companyid") String companyid , 
			@PathVariable (value = "categoryid") String categoryid,
			@PathVariable (value = "printrId") String printrId) 
	{
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return  kotCategoryMstRepository.findByCompanyMstAndCategoryIdAndPrinterId(companyMst,categoryid,printrId);
		
	}

	@GetMapping("{companyid}/kotcategorymstresource/findallkotcategorymst")
	public List< KotCategoryMst> getKotCategoryMst(
			@PathVariable (value = "companyid") String companyid  
			)
			
	{
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return  kotCategoryMstRepository.findByCompanyMst(companyMst);
		
	}
	/*
	 * This url is used to get category name not in kot category printer but items 
	 * in kot items
	 */
	@GetMapping("{companyid}/kotcategorymstresource/getcategorynotinkot")
	public List< String> getKotCategoryMstNotInKotCategory(
			@PathVariable (value = "companyid") String companyid  
			)
			
	{
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return kotCategoryMstRepository.getCategoryNotInKotCategory();
		
		
	}
	
}
