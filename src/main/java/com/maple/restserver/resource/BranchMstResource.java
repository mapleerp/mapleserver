package com.maple.restserver.resource;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.service.BranchMstService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@CrossOrigin("http://localhost:4200")
@RestController
public class BranchMstResource {
	
	private static final Logger logger = LoggerFactory.getLogger(BranchMstResource.class);
	EventBus eventBus = EventBusFactory.getEventBus();
	// @Autowired
//	  private RuntimeService runtimeService;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired

	private BranchMstRepository brnchMstRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	BranchMstService branchMstService;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@GetMapping("{companymstid}/allbranches")
	public List<BranchMst> retrieveAllbranch(@PathVariable(value = "companymstid") String companymstid)
	{
		return brnchMstRepo.findByCompanyMstId(companymstid);
	};
	
	@GetMapping("{companymstid}/otherbranches")
	public List<BranchMst> getOtherBranches()
	{
		return brnchMstRepo.findByMyBranch("N");
	};
	
	@GetMapping("{companymstid}/mybranch")
	public BranchMst getMyBranch()
	{
		return brnchMstRepo.getMyBranch();
	};
	
	
	@PostMapping("/{companymstid}/branch")
	public BranchMst createBranch(
			 
			@PathVariable(value = "companymstid") String companymstid,
			  @Valid @RequestBody BranchMst branchMst)
	{
		
	 	 
		Optional<CompanyMst> companyMstOpt =   companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		System.out.print(companyMst.getCompanyName());
		branchMst.setCompanyMst(companyMst);
//		branchMst = 	brnchMstRepo.saveAndFlush(branchMst);
		branchMst= saveAndPublishService.saveBranchMst(branchMst, branchMst.getBranchCode());
		logger.info("BranchDtl send to KafkaEvent: {}", branchMst);
		
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("voucherNumber", branchMst.getBranchCode());
		variables.put("voucherDate",SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", branchMst.getId());		 
		variables.put("companyid", branchMst.getCompanyMst());
		variables.put("branchcode", branchMst.getCompanyMst().getId());
	 		variables.put("REST",1);
		 
	 		variables.put("WF", "forwardBranch");
	 		
	 		
	 		String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
 
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
 
 
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardBranch");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			
			variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);

	 		
 /* ProcessInstanceWithVariables pVariablesInReturn =
  runtimeService.createProcessInstanceByKey("forwardBranch")
  .setVariables(variables)
  
  .executeWithVariablesInReturn();
*/
			
			
			
		
	 	return branchMst;
		
	 } 
	
	
	@DeleteMapping("/{companymstid}/branchmst/{id}")
	public void groupMstDelete(@PathVariable(value = "id") String Id) {
		brnchMstRepo.deleteById(Id);

	}
	
	@GetMapping("/{companymstid}/branchmstbybranchcode/{branchcode}")
	public  @ResponseBody Optional<BranchMst> branchMstbyBranchCode(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchCode){
		return brnchMstRepo.findByBranchCodeAndCompanyMstId(branchCode,companymstid);
	}
	
	

	@GetMapping("/{companymstid}/branchmstbyid/{id}")
	public  @ResponseBody Optional<BranchMst> branchMstbyId(@PathVariable(value = "id") String id){
		return brnchMstRepo.findById(id);
	}
	
	@GetMapping("/{companymstid}/branchcodebyname/{name}")
	public  @ResponseBody String branchCodebyName(@PathVariable(value = "name") String name){
		return branchMstService.findByBranchName(name);
	}
	
	

	@GetMapping("/{companymstid}/branchmstbybranchname/{branchname}")
	public  @ResponseBody BranchMst branchMstbyBranchName(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchname") String branchname){
		
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
				
		
		return brnchMstRepo.findByBranchNameAndCompanyMst(branchname,companyMstOpt);
	}
	
	//For web

	@GetMapping("{companymstid}/web/savebranch/{branchName}/{branchCode}/"
			+ "{branchGST}/{branchEmail}/{phno}/{branchWebsite}/{address1}/{address2}/{bankaccNo}/"
			+ "{bankName}/{bankBranch}/{IFSC}/{state}/{myBranch}/{branchId}")
	public String createBranchWeb(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchName") String branchName,
			@PathVariable(value = "branchCode") String branchCode,
			@PathVariable(value = "branchGST") String branchGST, 
			@PathVariable(value = "branchEmail") String branchEmail,
			@PathVariable(value = "phno") String phno, 
			@PathVariable(value = "branchWebsite") String branchWebsite,
			@PathVariable(value = "address1") String address1, 
			@PathVariable(value = "address2") String address2,
			@PathVariable(value = "bankaccNo") String bankaccNo,
			@PathVariable(value = "bankName") String bankName,
			@PathVariable(value = "bankBranch") String bankBranch,
			@PathVariable(value = "IFSC") String IFSC,
			@PathVariable(value = "state") String state,
			@PathVariable(value = "myBranch") String myBranch,
			@PathVariable(value = "branchId") String branchId) {

		BranchMst branchMst = new BranchMst();
		branchName = branchName.replaceAll("%20", " ");
		branchCode = branchCode.replaceAll("%20", " ");
		branchGST = branchGST.replaceAll("%20", " ");
		branchEmail = branchEmail.replaceAll("%20", " ");
		phno = phno.replaceAll("%20", " ");
		state = state.replaceAll("%20", " ");
		branchWebsite = branchWebsite.replaceAll("%20", " ");
		address1 = address1.replaceAll("%20", " ");
		address2 = address2.replaceAll("%20", " ");
		bankaccNo = bankaccNo.replaceAll("%20", " ");
		bankName = bankName.replaceAll("%20", " ");
		bankBranch = bankBranch.replaceAll("%20", " ");
		IFSC = IFSC.replaceAll("%20", " ");
		myBranch = myBranch.replaceAll("%20", " ");
		branchId = branchId.replaceAll("%20", " ");

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		branchMst.setCompanyMst(companyOpt.get());
		branchMst.setId(branchId);
		branchMst.setAccountNumber(bankaccNo);
		branchMst.setBankBranch(bankBranch);
		
		branchMst.setBankName(bankName);;
		branchMst.setBranchAddress1(address1);
		branchMst.setBranchAddress2(address2);
		branchMst.setBranchCode(branchCode);
		branchMst.setBranchEmail(branchEmail);
		branchMst.setBranchGst(branchGST);
		branchMst.setBranchName(branchName);
		branchMst.setBranchState(state);
		branchMst.setBranchTelNo(phno);
		branchMst.setMyBranch(myBranch);
		branchMst.setIfsc(IFSC);
		branchMst.setBranchWebsite(branchWebsite);
		//branchMst = brnchMstRepo.saveAndFlush(branchMst);
		
		branchMst= saveAndPublishService.saveBranchMst(branchMst, branchMst.getBranchCode());
		logger.info("BranchDtl send to KafkaEvent: {}", branchMst);
		

		return "success";

	}
	@PutMapping("{companymstid}/branchmst/{branchid}/updatebranchmst")
	public BranchMst updateBranch(
			@PathVariable(value="branchid") String branchid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody BranchMst  branchMstRequest)
	{
			
		 BranchMst branchMst = brnchMstRepo.findById(branchid).get();
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		branchMst.setCompanyMst(companyMst);
		branchMst.setAccountNumber(branchMstRequest.getAccountNumber());
		branchMst.setBankBranch(branchMstRequest.getBankBranch());
		branchMst.setBankName(branchMstRequest.getBankName());
		branchMst.setBranchAddress1(branchMstRequest.getBranchAddress1());
		branchMst.setBranchAddress2(branchMstRequest.getBranchAddress2());
		branchMst.setBranchCode(branchMstRequest.getBranchCode());
		branchMst.setBranchEmail(branchMstRequest.getBranchEmail());
		branchMst.setBranchGst(branchMstRequest.getBranchGst());
		branchMst.setBranchName(branchMstRequest.getBranchName());
		branchMst.setBranchPlace(branchMstRequest.getBranchPlace());
		branchMst.setBranchState(branchMstRequest.getBranchState());
		branchMst.setBranchTelNo(branchMstRequest.getBranchTelNo());
		branchMst.setBranchWebsite(branchMstRequest.getBranchWebsite());
		branchMst.setIfsc(branchMstRequest.getIfsc());
		branchMst.setMyBranch(branchMstRequest.getMyBranch());
		//branchMst= brnchMstRepo.saveAndFlush(branchMst);
		branchMst= saveAndPublishService.saveBranchMst(branchMst, branchMst.getBranchCode());
		logger.info("BranchDtl send to KafkaEvent: {}", branchMst);
		

		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("voucherNumber", branchMst.getBranchCode());
		variables.put("voucherDate",SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", branchMst.getId());		 
		variables.put("companyid", branchMst.getCompanyMst());
		variables.put("branchcode", branchMst.getCompanyMst().getId());
	 		variables.put("REST",1);
		 
	 		variables.put("WF", "forwardBranch");
	 		
	 		
	 		String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
 
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
 
 
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardBranch");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			
			variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);
	
			return branchMst;
	
	}
}
