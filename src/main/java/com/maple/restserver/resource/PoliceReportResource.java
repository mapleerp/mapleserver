package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.PoliceReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.PoliceReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class PoliceReportResource {
	@Autowired
	PoliceReportService  policeReportService;
	@Autowired
	CompanyMstRepository companyMstRepository;
	
                          //  (0_0)       SharonN
	
	
	
	@GetMapping("{companymstid}/policereportresource/getpolicereport")		
	public List<PoliceReport> getPoliceReport(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("customer") String customer,
			@RequestParam("fromdate") String fDate,
			@RequestParam("todate") String tDate) {
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fDate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(tDate, "yyyy-MM-dd");
		
		String customerNames = customer;

		String[] array = customerNames.split(";");
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		return policeReportService.findPoliceReport(companyMst,fromDate,toDate,array);
	}
}
