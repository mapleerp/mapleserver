package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.DailyStock;
import com.maple.restserver.repository.DailyStockRepository;


@RestController
@Transactional
public class DailyStockResource {
	
	@Autowired
	private DailyStockRepository dailyStockRepository;
	
	@GetMapping("{companymstid}/dailystocks")
	public List<DailyStock> retrieveAllDailyStock(){
		return dailyStockRepository.findAll();
	}
	
	@PostMapping("{companymstid}/dailystock")
	public ResponseEntity<Object> createDailyStock(@Valid @RequestBody 
			DailyStock dailyStock)
	{
		DailyStock saved = dailyStockRepository.saveAndFlush(dailyStock);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/(id)").buildAndExpand(saved.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}

}
