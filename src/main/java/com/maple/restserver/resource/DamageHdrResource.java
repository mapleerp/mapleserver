package com.maple.restserver.resource;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.DamageHdr;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.IntentInHdr;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.DamageHdrAndDtlMessage;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DamageDtlRepository;
import com.maple.restserver.repository.DamageHdrRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.service.SaveAndPublishServiceImpl;
import com.maple.restserver.service.task.DamageEntryStock;
import com.maple.restserver.utils.EventBusFactory;

@RestController
@Transactional
public class DamageHdrResource {

	
	 @Value("${serverorclient}")
		private String serverorclient;
	 
	 EventBus eventBus = EventBusFactory.getEventBus();

	 //@Autowired
	//  private RuntimeService runtimeService;
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	 
@Autowired
DamageHdrRepository damageHdrRpo;
@Autowired
CompanyMstRepository companyMstRepo;
	
@Autowired
DamageEntryStock damageEntryStock;

@Autowired
DamageDtlRepository damageDtlRepository;
@Autowired
SaveAndPublishServiceImpl saveAndPublishServiceImpl;

@PostMapping("{companymstid}/damagehdr")
public DamageHdr createDamageHdr(@PathVariable(value = "companymstid") String
		 companymstid,@Valid @RequestBody 
		 DamageHdr  damagehdr)
{
	
	
	Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
	
	CompanyMst companyMst = companyMstOpt.get();
	damagehdr.setCompanyMst(companyMst);
	
   return damageHdrRpo.save(damagehdr);
	} 


@GetMapping("{companymstid}/damagehdrs")
public List<DamageHdr> retrieveAllDamageHdr(@PathVariable(value="companymstid") String companymstid){
	return damageHdrRpo.findByCompanyMstId(companymstid);
}

@GetMapping("{companymstid}/{id}/damagehdrs")
public DamageHdr retrieveAllDamageHdrAndId(@PathVariable(value="companymstid") String companymstid,
		@PathVariable(value="id") String id){
	return damageHdrRpo.findByIdAndCompanyMstId(id,companymstid);
}
	


@PutMapping("{companymstid}/damagehdr/{damagehdrId}")
public DamageHdr damageHdrFinalSave(@PathVariable(value="companymstid") String companymstid,
		@PathVariable String damagehdrId, @Valid @RequestBody DamageHdr damageHdrRequest)
{
		 
	   		 
	DamageHdr damageHdr=damageHdrRpo.findByIdAndCompanyMstId(damagehdrId,companymstid);
				
	damageHdr.setVoucheNumber(damageHdrRequest.getVoucheNumber()); 
				 
	DamageHdr saved = damageHdrRpo.saveAndFlush(damageHdr);
	
	List<DamageDtl> damageDtlList = damageDtlRepository.findByDamageHdr(saved);
	DamageHdrAndDtlMessage damageHdrAndDtlMessage = new DamageHdrAndDtlMessage();
	damageHdrAndDtlMessage.setDamageHdr(saved);
	damageHdrAndDtlMessage.getDamageDtlList().addAll(damageDtlList);
	
	saveAndPublishServiceImpl.publishObjectToKafkaEvent(damageHdrAndDtlMessage, 
			saved.getBranchCode(),
				KafkaMapleEventType.DAMAGEHDRANDDTL, 
				KafkaMapleEventType.SERVER,saved.getId());

	/*
	 * Init WF
	 */

	
	damageEntryStock.executebatchwise(saved.getVoucheNumber(), saved.getVoucherDate(), saved.getId(), saved.getCompanyMst(),
			saved.getBranchCode());
	Map<String, Object> variables = new HashMap<String, Object>();

	variables.put("voucherNumber", saved.getVoucheNumber());
	variables.put("voucherDate", saved.getVoucherDate());
	variables.put("inet", 0);
	variables.put("id", saved.getId());
	variables.put("branchcode", saved.getBranchCode());

	variables.put("companyid", saved.getCompanyMst());
	if (serverorclient.equalsIgnoreCase("REST")) {
		variables.put("REST", 1);
	} else {
		variables.put("REST", 0);
	}

	
	variables.put("WF", "forwardDamageEntry");
	
	String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardDamageEntry");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);
		
		
	/*ProcessInstanceWithVariables pVariablesInReturn = runtimeService
			.createProcessInstanceByKey("damageentryprocess").setVariables(variables)

			.executeWithVariablesInReturn();
			*/
		

	return saved;

	
	
	
	         
		  
}
@PutMapping("{companymstid}/damagehdr/batchwisefinasave/{damagehdrId}")
public DamageHdr damageHdrBatchWiseFinalSave(@PathVariable(value="companymstid") String companymstid,
		@PathVariable String damagehdrId, @Valid @RequestBody DamageHdr damageHdrRequest)
{
		 
	   		 
	DamageHdr damageHdr=damageHdrRpo.findByIdAndCompanyMstId(damagehdrId,companymstid);
				
	damageHdr.setVoucheNumber(damageHdrRequest.getVoucheNumber()); 
				 
	DamageHdr saved = damageHdrRpo.saveAndFlush(damageHdr);
	
	
	List<DamageDtl> damageDtlList = damageDtlRepository.findByDamageHdr(saved);
	DamageHdrAndDtlMessage damageHdrAndDtlMessage = new DamageHdrAndDtlMessage();
	damageHdrAndDtlMessage.setDamageHdr(saved);
	damageHdrAndDtlMessage.getDamageDtlList().addAll(damageDtlList);
	
	saveAndPublishServiceImpl.publishObjectToKafkaEvent(damageHdrAndDtlMessage, 
			saved.getBranchCode(),
				KafkaMapleEventType.DAMAGEHDRANDDTL, 
				KafkaMapleEventType.SERVER,saved.getId()+"BATCHWISE");


	/*
	 * Init WF
	 */

	
	damageEntryStock.executebatchwise(saved.getVoucheNumber(), saved.getVoucherDate(), saved.getId(), saved.getCompanyMst(),
			saved.getBranchCode());
	Map<String, Object> variables = new HashMap<String, Object>();

	variables.put("voucherNumber", saved.getVoucheNumber());
	variables.put("voucherDate", saved.getVoucherDate());
	variables.put("inet", 0);
	variables.put("id", saved.getId());
	variables.put("branchcode", saved.getBranchCode());

	variables.put("companyid", saved.getCompanyMst());
	if (serverorclient.equalsIgnoreCase("REST")) {
		variables.put("REST", 1);
	} else {
		variables.put("REST", 0);
	}

	
	variables.put("WF", "forwardDamageEntry");
	
	String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		
		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
		
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardDamageEntry");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);
		
		
	/*ProcessInstanceWithVariables pVariablesInReturn = runtimeService
			.createProcessInstanceByKey("damageentryprocess").setVariables(variables)

			.executeWithVariablesInReturn();
			*/
		

	return saved;

	
	
	
	         
		  
}
	
}
