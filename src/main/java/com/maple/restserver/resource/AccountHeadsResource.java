
package com.maple.restserver.resource;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.accounting.entity.AccountMergeMst;
import com.maple.restserver.accounting.resource.OpeningBalanceMstResource;
import com.maple.restserver.accounting.service.AccountingService;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.AccountBalanceReport;
import com.maple.restserver.report.entity.SaleOrderinoiceReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.AccountMergeRepository;
import com.maple.restserver.repository.CompanyMstRepository;
 
import com.maple.restserver.service.AccountHeadsService;
import com.maple.restserver.service.AccountHeadsServiceImpl;
import com.maple.restserver.service.AccountMergeService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
@CrossOrigin("http://localhost:4200")
@RestController
@Transactional
public class AccountHeadsResource {

	private static final Logger logger = LoggerFactory.getLogger(AccountHeadsResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	EventBus eventBus = EventBusFactory.getEventBus();
	Pageable topFifty =   PageRequest.of(0, 50);

	// @Autowired
	// private RuntimeService runtimeService;

	 
	@Autowired
	AccountHeadsService accountHeadsService;
	@Autowired
	AccountMergeService accountMergeService;
	@Autowired
	AccountHeadsServiceImpl accountHeadsServiceImpl;

	@Autowired
	private AccountHeadsRepository accountHeadsRepo;

	@Autowired
	AccountingService aaccountingService;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	AccountMergeRepository accountMergeRepository;
	

	@GetMapping("{companymstid}/accountheads")
	public List<AccountHeads> retrieveAllbalance(
			@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsRepo.findByCompanyMstId(companymstid);
	}

	@GetMapping("{companymstid}/accountheadssearchbyname")
	public List<AccountHeads> getAccountByAccountName(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "data") String accountName) {
		List<AccountHeads>  result = accountHeadsRepo.findByAccountNameAndCompanyMstIdLike("%" + accountName + "%", companymstid, topFifty);
		return result;

	}

	@GetMapping("{companymstid}/accountheadsbyid/{accountId}")

	public AccountHeads getAccountByAccountId(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "accountId") String accountId) {

		return accountHeadsRepo.findByIdAndCompanyMstId(accountId, companymstid);

	}

	@GetMapping("{companymstid}/accountheadsbyname/{accountName}")

	public AccountHeads fetchAccountByAccountName(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "accountName") String accountName) {
		return accountHeadsRepo.findByAccountNameAndCompanyMstId(accountName, companymstid);
	}

	@PostMapping("{companymstid}/accountheads")
	public AccountHeads createaAccountHeads(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody AccountHeads accountHdr) {

		System.out.println("Account =  " + accountHdr);

		Optional<CompanyMst> companyMsList = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMsList.get();

		accountHdr.setCompanyMst(companyMst);
//		AccountHeads savedAccountJeads = accountHeadsRepo.save(accountHdr);
		
		AccountHeads savedAccountJeads =saveAndPublishService.saveAccountHeads(accountHdr,mybranch);
		logger.info("AccountHeads send to KafkaEvent: {}", savedAccountJeads);
		
		

		 

		/*
		 * ProcessInstanceWithVariables pVariablesInReturn =
		 * runtimeService.createProcessInstanceByKey("forwardAccountHeads")
		 * .setVariables(variables)
		 * 
		 * .executeWithVariablesInReturn();
		 */

		return savedAccountJeads;

	}

	@GetMapping("{companymstid}/accountheadsbystatus")
	public List<AccountHeads> retrievebystatus(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsRepo.getAccountHeads(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/fetchaccountheadsbybankaccount")

	public List<AccountHeads> fetchAccountHeadsByAccountName(
			@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.findByBankAccount(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/fetchaccountheadsbyassetaccount")
	public List<AccountHeads> fetchAccountHeadsByAssetAccnt(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.findByAssetAccount(companymstid);
	}

	@GetMapping("{companymstid}/{branchcode}/accountheads/initilize")
	public String initializeAccountHeads(

			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		// To be cahnged
		return accountHeadsService.addAccountHeadInitialize(companymstid,branchcode);
	}

	@GetMapping("{companymstid}/accountheads/addheadsalesreturn")
	public String addAccountHeadSalesReturn(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadSalesReturn(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadassets")
	public String addAccountHeadAssets(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadAsset(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadliability")
	public String addAccountHeadLialibility(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadLiability(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadincome")
	public String addAccountHeadIncome(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadIncome(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadexpance")
	public String addAccountHeadExpance(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadExpance(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadcurrentassets")
	public String addAccountHeadCurrentAssets(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadCurrentAssets(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadcash")
	public String addAccountHeadCash(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadCash(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadbankaccount")
	public String addAccountHeadBankAccount(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadBankAccount(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadsodexo")
	public String addAccountHeadSodexo(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadSodexo(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadsalesaccount")
	public String addAccountHeadSalesAccount(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadSalesAccount(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadpurchaseaccount")
	public String addAccountHeadPurchaseAccount(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadPurchaseAccount(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheaddutiesandtaxes")
	public String addAccountHeadDutiesandtaxes(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadDutiesandtaxes(companymstid);
	}

	@PostMapping("{companymstid}/accountheads/addheadcgstaccount")
	public String addAccountHeadCgstAccount(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadCgstAccount(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadsgstaccount")
	public String addAccountHeadSgstAccount(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadsgstAccount(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadscessaccount")
	public String addAccountHeadCessAccount(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadCessAccount(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadkeralafloodcess")
	public String addAccountHeadKrealaFloodCess(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addAccountHeadKeralaFloodCess(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/addheadpettycash/{branchCode}")
	public String addAccountHeadPettyCash(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		return accountHeadsService.addAccountHeadPettyCash(companymstid, branchcode);
	}

	@GetMapping("{companymstid}/accountheads/addheadroundoff/{branchCode}")
	public String addAccountHeadRoundOff(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		return accountHeadsService.addAccountHeadroundoff(companymstid, branchcode);
	}

	@GetMapping("{companymstid}/accountheads/unitcreation")
	public String UnitCreation(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.unitCreation(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionreports")
	public String addProcessPermissionReports(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessPermissionReports(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionkotpos")
	public String addProcessPermissionKotPos(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessPermissionKotPos(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissiondamageentry")
	public String addProcessDamageEntry(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessDamageEntry(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionwholesale")
	public String addProcessWholeSale(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessWholeSale(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionposwindow")
	public String addProcessPosWindow(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessPosWindow(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissiononlinesales")
	public String addProcessOnlineSales(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessOnlineSales(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissiononreorder")
	public String addProcessReorder(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessReorder(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionsaleorder")
	public String addProcessSaleOrder(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessSaleOrder(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionitemmaster")
	public String addProcessItemMaster(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessItemMaster(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionaccountheads")
	public String addProcessAccountHeads(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessAccountHeads(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionproductconversion")
	public String addProcessProductConversion(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessProductConversion(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionreceiptwindow")
	public String addProcessReceiptWindow(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessReceiptWindow(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissiondayendclossure")
	public String addProcessDayEndClossure(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessDayEndClossure(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionpurchase")
	public String addProcessPurchase(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessPurchase(companymstid);

	}

	@GetMapping("{companymstid}/accountheads/processpemissioncustomer")
	public String addProcessCustomer(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessCustomer(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionacceptstock")
	public String addProcessAcceptStock(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessAcceptStock(companymstid);

	}

	@GetMapping("{companymstid}/accountheads/processpemissionaddsupplier")
	public String addProcessAddSupplier(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessAddSupplier(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionprocesspayment")
	public String addProcessPayment(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessPayment(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissiontasklist")
	public String addProcessTaskList(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessTaskList(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionmultyunit")
	public String addProcessMultyUnit(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessMultyUnit(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionstocktransfer")
	public String addProcessStockTransfer(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessStockTransfer(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionintentent")
	public String addProcessIntentent(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessIntentent(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionproductionplanning")
	public String addProcessProductionPlanning(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessProductionPlanning(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionjournal")
	public String addProcessJournal(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessJournal(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionsessionend")
	public String addProcessSessionEnd(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessSessionEnd(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionphysicalstock")
	public String addProcessPhysicalStock(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessPhysicalStock(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissiontaxcreation")
	public String addProcessTaxCreation(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessTaxCreation(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionkitdefinition")
	public String addProcessKitDefinition(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessKitDefinition(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionpricedefinition")
	public String addProcessPriceDefinition(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessPriceDefinition(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionkotmanager")
	public String addProcessKotManger(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessKotManger(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionuserregistration")
	public String addProcessUserRegistration(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessUserRegistration(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionuserprocesscreation")
	public String addProcessProcessCreation(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessProcessCreation(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionchangepassword")
	public String addProcessChangePassword(@PathVariable(value = "companymstid") String companymstid) {

		return accountHeadsService.addProcessChangePasswordf(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionbranchcreation")
	public String addProcessBranchCreation(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessBranchCreation(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissioncompanycreation")
	public String addProcessCompanyCreation(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessCompanyCreation(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionvoucherreprint")
	public String addProcessVoucherReprint(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessVoucherReprint(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionscheme")
	public String addProcessScheme(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessScheme(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionschemedetails")
	public String addProcessSchemeDetails(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessSchemeDetails(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemission")
	public String addProcessPermission(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermission(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissiongrouppermisssion")
	public String addProcessPermissionGroupPermission(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionGroupPermission(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissiongroupcreation")
	public String addProcessPermissionGroupCreation(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionGroupCreation(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionscustomer")
	public String addProcessPermissionCustomer(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionCustomer(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionsalesreports")
	public String addProcessPermissionSalesReport(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionSalesReport(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionmonthlysummary")
	public String addProcessPermissionMopnthalySummaryReport(
			@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionMopnthalySummaryReport(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionsaleorderreport")
	public String addProcessPermissionSaleOrderReport(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionSaleOrderReport(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionaccountbalance")
	public String addProcessPermissionAccountBalance(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionAccountBalance(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionmonthlystockreport")
	public String addProcessPermissionMonthlyStockReport(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionMonthlyStockReport(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionsalesreport")
	public String addProcessPermissionsSalesReport(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionsSalesReport(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionaccounts")
	public String addProcessPermissionsAccounts(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionsAccounts(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionsupplierpayment")
	public String addProcessPermissionsSupplierPayment(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionsSupplierPayment(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionownaccountsettlement")
	public String addProcessPermissionsOwnAccountSettlement(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionsOwnAccountSettlement(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionactualproduction")
	public String addProcessPermissionsActualProduction(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionsActualProduction(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionsalesreturn")
	public String addProcessPermissionsSalesReturn(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionsSalesReturn(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionsalesfromorder")
	public String addProcessPermissionSalesFromOrder(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionSalesFromOrder(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionsaleorderedit")
	public String addProcessPermissionSaleorderEdit(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionSaleorderEdit(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionconfigurations")
	public String addProcessPermissionConfigurations(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionConfigurations(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionreceiptmode")
	public String addProcessPermissionReceiptMode(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionReceiptMode(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissionvouchertype")
	public String addProcessPermissionVoucherType(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissionVouchertype(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/processpemissions")
	public String addProcessPermissions(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsService.addProcessPermissions(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/getaccountheadsunderexpense")
	public List<AccountHeads> getAccountHeadByExpense(@PathVariable(value = "companymstid") String companymstid) {
		AccountHeads accountHeadsOp = accountHeadsRepo.findByAccountNameAndCompanyMstId("EXPENSE", companymstid);
		
		// List<AccountHeads>   accloundHeadList = aaccountingService.getallChildAccounts("EXPENSE", companymstid);
		 //accountHeadsRepo.getAccountHeadsUnderExpense(accountHeadsOp.getId());
		//return  accloundHeadList ;
		return aaccountingService.getallChildAccounts("EXPENSE", companymstid);
	}

	@PutMapping("{companymstid}/updateaccountheads/{id}")
	public AccountHeads updateAccountHeads(@PathVariable(value = "id") String id,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody AccountHeads accountHeadsReq) {

		AccountHeads accountHeads = accountHeadsRepo.findById(id).get();

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		accountHeads.setCompanyMst(companyMst);
		accountHeads.setParentId(accountHeadsReq.getParentId());
		accountHeads.setSerialNumber(accountHeadsReq.getSerialNumber());
//		accountHeads = accountHeadsRepo.saveAndFlush(accountHeads);
		 accountHeads =saveAndPublishService.saveAccountHeads(accountHeads,mybranch);
		logger.info("AccountHeads send to KafkaEvent: {}", accountHeads);
		return accountHeads;
	}

	@GetMapping("{companymstid}/accountheadresource/mergeaccounthead/{fromaccountid}/{toaccountid}")
	public String mergeAccountHeads(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "fromaccountid") String fromaccountid,
			@PathVariable(value = "toaccountid") String toaccountid)

	{
		return accountMergeService.accountMerge(fromaccountid, toaccountid);
	}

	@GetMapping("{companymstid}/accountheadsresource/accountheadsbyparentid/{parentid}")
	public List<AccountHeads> getAccountByAccountNameAndParentId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "parentid") String parentid, @RequestParam("accountname") String accountName) {

		return accountHeadsRepo.findByParentIdAccountNameAndCompanyMstIdLike(parentid, "%" + accountName + "%",
				companymstid, topFifty);

	}

	@GetMapping("{companymstid}/accountheads/getaccountheadbyserialnumbers")
	public List<AccountHeads> getAccountHeadBySerialNumber(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsRepo.getAccountHeadsBySerialNumber(companymstid);
	}

	@GetMapping("{companymstid}/accountheads/getroot")
	public String getrootaccount(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "account") String account) {
		return aaccountingService.getRootParentOfAccount(account);
	}

	@GetMapping("{companymstid}/accountheads/getaccountheadswithnullparent")
	public List<AccountHeads> getAccountHeadsWithNullParent(@PathVariable(value = "companymstid") String companymstid) {
		return accountHeadsRepo.getaccountHeadsWithNullParent();
	}

	// =============sharon 18-3-2021
	@GetMapping("{companymstid}/accountheadsresource/branchwiseaccountclosingbalance/{accountid}")
	public List<AccountBalanceReport> getBranchWiseAccountClosingBalance(
			@PathVariable(value = "companymstid") String companymstid, 
			@PathVariable(value = "accountid") String accountid,
			@RequestParam("rdate") String reportdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return aaccountingService.getBranchWiseAccountClosingBalance(companymstid, date,accountid);

	}
	
	
	//================get account heads by parent id====anandu==== 
	@GetMapping("{companymstid}/accountheadsresource/getallaccountheadsbyparentid/{assetId}")
	public List<AccountHeads> getAccountHeadsByParentId(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "assetId") String assetId) {
		
		return accountHeadsRepo.getAccountHeadsByParentId(assetId);
	}
	
	//-get all accountMerge Dtls....//
	@GetMapping("{companymstid}/accountheads/getaccountmergedtl")
	public List<AccountMergeMst> getAllAccountMergeDtl(@PathVariable(value = "companymstid") String companymstid)
	{
		return accountMergeRepository.findAllByCompanyMst(companymstid);
	}
	
	@GetMapping("{companymstid}/accountheads/getaccountmergemstbyid/{fromAccountId}/{toAccountId}")
	public AccountMergeMst getAccountMergeMstById(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "fromAccountId") String fromAccountId,@PathVariable(value = "toAccountId") String toAccountId)
	{
		return accountMergeRepository.getAccountMergeMstById(companymstid,fromAccountId,toAccountId);
	}
	
	
	@GetMapping("{companymstid}/accountheads/searchaccountbysupplierandboth")
	public  @ResponseBody List<AccountHeads> searchSupplier(
			@PathVariable(value = "companymstid") String companymstid,
			  @RequestParam("data") String searchData)
			 {
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		List<AccountHeads> result = accountHeadsService.searchLikeSupplierByNamefromAccountHeads("%"+searchData+"%",comapnyMstOpt.get()) ;
		return result;
	}
	
	@GetMapping("/{companymstid}/accountheadsresource/partycreationbygst/{gst}")
	public List<AccountHeads> retrieveAccountHeadsByGst(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "gst") String gst) {

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

		return accountHeadsRepo.findByCompanyMstAndPartyGst(companyMst.get(), gst);
	}

	
//	@GetMapping("{companymstid}/customermst/{partyid}")
//	public Optional<AccountHeads> retrievePartyCreationById(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable String partyid) {
//		return accountHeadsRepo.findByIdAndCompanyMstId(partyid, companymstid);
//	}

	
	// ------------------new url ----publish offline button
//
//	@PostMapping("{companymstid}/accountheadsresource/{status}")
//	public AccountHeads sendOffLineMessageToCloud(@Valid @RequestBody CustomerMst customerReg,
//			@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable("status") String status) {
//		 ResponseEntity<AccountHeads> saved=	externalApi.sendCustomerMstOffLineMessageToCloud(companymstid, customerReg);
//			
//		return saved.getBody();
//
//	}
	
	@PutMapping("{companymstid}/accountheadsresource/{partyid}/updatepartycreation")
	public AccountHeads updatePartyCreation(@PathVariable(value = "partyid") String partyid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody AccountHeads AccountHeadsRequest) {

		AccountHeads AccountHeads = accountHeadsRepo.findById(partyid).get();

		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		AccountHeads.setCompanyMst(companyMst);
		AccountHeads.setCreditPeriod(AccountHeadsRequest.getCreditPeriod());
		AccountHeads.setPartyAddress1(AccountHeadsRequest.getPartyAddress1());
		AccountHeads.setCustomerContact(AccountHeadsRequest.getCustomerContact());
		AccountHeads.setDrugLicenseNumber(AccountHeadsRequest.getDrugLicenseNumber());
		AccountHeads.setCustomerGroup(AccountHeadsRequest.getCustomerGroup());
		AccountHeads.setPartyGst(AccountHeadsRequest.getPartyGst());
		AccountHeads.setPartyMail(AccountHeadsRequest.getPartyMail());
		AccountHeads.setAccountName(AccountHeadsRequest.getAccountName());
		AccountHeads.setCustomerState(AccountHeadsRequest.getCustomerState());
		AccountHeads.setCustomerDiscount(AccountHeadsRequest.getCustomerDiscount());
		AccountHeads.setDiscountProperty(AccountHeadsRequest.getDiscountProperty());
		AccountHeads.setPriceTypeId(AccountHeadsRequest.getPriceTypeId());
//		AccountHeads.setCustomerName(AccountHeadsRequest.getCustomerName());
		AccountHeads.setBankName(AccountHeadsRequest.getBankName());
		AccountHeads.setBankIfsc(AccountHeadsRequest.getBankIfsc());
		AccountHeads.setTaxInvoiceFormat(AccountHeadsRequest.getTaxInvoiceFormat());
		AccountHeads.setBankAccountName(AccountHeadsRequest.getBankAccountName());
		AccountHeads.setCustomerType(AccountHeadsRequest.getCustomerType());
		AccountHeads.setCurrencyId(AccountHeadsRequest.getCurrencyId());
		AccountHeads.setCustomerCountry(AccountHeadsRequest.getCustomerCountry());
		AccountHeads.setCustomerOrSupplier(AccountHeadsRequest.getCustomerOrSupplier());
		
		AccountHeads accounHeadByAsset = accountHeadsRepo.findByAccountName("ASSETS");
//		AccountHeads accountByGroup = accountHeadsRepo.findByAccountName(AccountHeads.getCustomerGroup());

		AccountHeads.setAccountName(AccountHeads.getAccountName());
		AccountHeads.setRootParentId(accounHeadByAsset.getId());
		AccountHeads.setParentId(AccountHeads.getId());
		AccountHeads = accountHeadsRepo.save(AccountHeads);
		AccountHeads.setAccountName(AccountHeads.getAccountName());
		AccountHeads.setCurrencyId(AccountHeads.getCurrencyId());
		
		AccountHeads =saveAndPublishService.saveAccountHeads(AccountHeads,mybranch);
		//===================comment ended ============anandu =====================
		
		return AccountHeads;

	}
	 

//	@PostMapping("{companymstid}/partyregistration/{status}")
//	public CustomerMst sendOffLineMessageToCloud(@Valid @RequestBody CustomerMst customerReg,
//			@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable("status") String status) {
//		 ResponseEntity<CustomerMst> saved=	externalApi.sendCustomerMstOffLineMessageToCloud(companymstid, customerReg);
//			
//		return saved.getBody();
//
//	}
	

	@PostMapping("{companymstid}/accountheadsresource/partyregistration")
	public AccountHeads createParty(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody AccountHeads partycreation) {
          Optional<CompanyMst>	companyMst=	 companyMstRepo.findById(companymstid);
			AccountHeads custList = accountHeadsRepo.findByAccountName(partycreation.getAccountName());
			if (null == custList) {
				
				
				AccountHeads accounHeadByAsset = accountHeadsRepo.findByAccountName("ASSETS");
				
				AccountHeads accountByGroup = accountHeadsRepo
						.findByAccountName(partycreation.getCustomerGroup());
				
				partycreation.setCompanyMst(companyMst.get());
				
				partycreation.setGroupOnly("N");
				
				if (null != accountByGroup) {
					partycreation.setParentId(accountByGroup.getId());
				}
				if (null != accounHeadByAsset) {
					partycreation.setRootParentId(accounHeadByAsset.getId());
				}
				
				
				
				 custList = saveAndPublishService.saveAccountHeads(partycreation, mybranch);
				
			}
			return custList;

	}
	 
	@GetMapping("{companymstid}/partysearch")
	public @ResponseBody List<AccountHeads> searchParty(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("data") String searchstring) {
		return accountHeadsService.searchLikeAccountName("%" + searchstring + "%", companymstid, topFifty);
	}

	
	
	
	
	
	
	
	
	@GetMapping("{companymstid}/accountheadsresource/accountheadsbynameandbrachcode/{accountname}/{branchcode}")
	public Optional<AccountHeads> retrievAccountHeadsByNameAndBrachCode(
			@PathVariable(value = "accountname") String accountname,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "companymstid") String companymstid) {
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		return accountHeadsRepo.findByAccountNameAndCompanyMstIdAndBranchCode(accountname, companyMst.get().getId(),
				"%" + companyMst.get().getCompanyName() + branchcode + "%");
	}
	
	
	/*
	 * new url for getting customer from account heads by Account name====05-01-2022
	 */
	@GetMapping("{companymstid}/accountheadsresource/accountheadsbyname/{accountname}")
	public AccountHeads retrieveAccountHeadsByName(

			@PathVariable(value = "accountname") String accountname,
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

		AccountHeads accountHeads = accountHeadsRepo.findByAccountNameAndCompanyMst(accountname,
				companyMst.get());

		if (null != accountHeads) {
			return null;
		}

		return accountHeads;
	}
	
	@GetMapping("/{companymstid}/customerregistration/getgsttype/{state}")
	public String getCustomerGstType(@PathVariable(value = "state") String state) {
		return customerGstType(state);
	}

	public String customerGstType(String state) {
		if (state.equalsIgnoreCase("KERALA")) {
			return "GST";
		} else {
			return "IGST";
		}
	}
	
	@Transactional
	@GetMapping("{companymstid}/accountheadsresource/setdiscount")
	public String updateCustomerDiscount() {

		accountHeadsRepo.updateCustDiscount();
		return "success";
	}
	
	
	
	/*
	 * fetch account by account name. If it is not present, then create a new account and sent back.
	 */
	@GetMapping("{companymstid}/accountheadsresource/accountheadsbynamewithstatus/{accountcreatestatus}")

	public AccountHeads getAccountByAccountNameWithStatus(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "accountcreatestatus") Boolean accountCreateStatus,
			@RequestParam("selectedItem") String selectedItem) {
		
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		
		return accountHeadsService.getAccountByAccountNameWithStatus(selectedItem,accountCreateStatus,companyMst.get());
		
	}
	
	
	@GetMapping("{companyid}/accountheadsresource/retrieveaccountheadsbynamerequestparam")
	public AccountHeads retrieveAccountHeadsByNamerequestparam(@RequestParam(value = "accountdata") String accountdata) {
		return accountHeadsRepo.findByAccountName(accountdata.trim());

	}
	
	
	@GetMapping("{companymstid}/accountheadsresource/accountheads")
	public List< Object> retrieveAllaccountName(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid)
	{
		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		return  accountHeadsRepo.findSearch("%"+searchstring.toLowerCase()+"%", topFifty);
	}
}
