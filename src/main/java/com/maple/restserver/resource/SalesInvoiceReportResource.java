package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.BranchSaleReport;
import com.maple.restserver.report.entity.DailySalesReportDtl;
import com.maple.restserver.report.entity.ItemSaleReport;
import com.maple.restserver.report.entity.SaleOrderinoiceReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.SalesReturnInvoiceReport;
import com.maple.restserver.report.entity.TaxSummaryMst;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.service.SalesInvoiceService;
import com.maple.restserver.service.SalesInvoiceServiceImpl;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class SalesInvoiceReportResource {
	
	@Autowired
	SalesInvoiceService salesInvoiceService;

	@Autowired
	SalesInvoiceServiceImpl salesInvoiceServiceImpl;

	
	@Autowired
	SalesTransHdrRepository SalesTransHdrRepo;
	
	@GetMapping("{companymstid}/salesinvoice/{voucherNumber}/{branchcode}")
	public List<SalesInvoiceReport> getSalesInvoice(@PathVariable("voucherNumber") String voucherNumber,
			@RequestParam("rdate") String reportdate,
			@PathVariable("branchcode")String branchcode,
			@PathVariable(value = "companymstid") String  companymstid) {
		java.sql.Date date = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");
	
		return salesInvoiceServiceImpl.getSalesInvoice(companymstid,voucherNumber, date);

	}
	
	@GetMapping("{companymstid}/salesinvoicetax/{vouchernumber}/{branchcode}")
	public List<TaxSummaryMst> getSalesInvoiceTax(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable("vouchernumber") String vouchernumber, @RequestParam("rdate") String rdate) {
		java.sql.Date date = SystemSetting.StringToSqlDate(rdate, "yyyy-MM-dd");

		return salesInvoiceServiceImpl.getSalesInvoiceTax(companymstid, vouchernumber, date);

	}
	
	
	

	@GetMapping("{companymstid}/salesinvoicereport/gettaxinvoicecustomer/{vouchernumber}/{branchcode}")
	public List<SalesInvoiceReport> getTaxinvoiceCustomer(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("vouchernumber") String vouchernumber,
			@PathVariable("branchcode") String branchcode,@RequestParam("rdate") String rdate) {
		
		java.sql.Date date = SystemSetting.StringToSqlDate(rdate, "yyyy-MM-dd");

		return salesInvoiceServiceImpl.getTaxinvoiceCustomer(companymstid, vouchernumber, date);

	}
	@GetMapping("{companymstid}/salesinvoice")
	public List<SalesInvoiceReport> getAllSalesInvoice(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("rdate") String reportdate) {

		return salesInvoiceServiceImpl.getAllSalesInvoice(companymstid);

	}

	@GetMapping("{companymstid}/saleorderinvoicereport/{voucherNumber}")
	public SaleOrderinoiceReport getSaleOrderInvoiceReport(@PathVariable(value = "companymstid") String
			  companymstid,
			 @PathVariable("voucherNumber") String voucherNumber,  @RequestParam("rdate") String reportdate){
		
		
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
	


		return salesInvoiceServiceImpl.getSaleOrderInvoiceReport(voucherNumber, date );

	}

		@GetMapping("{companymstid}/saleorderinvoicereport/{itemid}")
		public List<ItemSaleReport> getItemSaleReport(@PathVariable(value = "companymstid") String
				  companymstid,
				 @PathVariable("itemid") String itemid,  @RequestParam("rdate") String reportdate){
			java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		


			return salesInvoiceServiceImpl.getItemSaleReport(itemid, date );

	
}
		

		@GetMapping("{companymstid}/salesinvoice/cessamount/{vouchernumber}/{branchcode}")
		public Double getCessAmount(@PathVariable(value = "companymstid") String
				  companymstid,
				 @PathVariable("vouchernumber") String vouchernumber, 
				 @PathVariable("branchcode") String branchcode,@RequestParam("rdate") String reportdate){
			java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		


			return salesInvoiceServiceImpl.getCessAmount(vouchernumber, date );

	
}
		@GetMapping("{companymstid}/sumoftaxes/{vouchernumber}/{branchcode}")
		public List<TaxSummaryMst> getSumOfTaxAmounts(@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "branchcode") String branchcode,
				@PathVariable("vouchernumber") String vouchernumber, @RequestParam("rdate") String rdate) {
			java.sql.Date date = SystemSetting.StringToSqlDate(rdate, "yyyy-MM-dd");

			return salesInvoiceServiceImpl.getSumOfTaxAmounts(companymstid, vouchernumber, date);

		}
		
		


		@GetMapping("{companymstid}/salesinvoice/nontaxableamount/{vouchernumber}/{branchcode}")
		public Double getNonTaxableAmount(@PathVariable(value = "companymstid") String
				  companymstid,
				 @PathVariable("vouchernumber") String vouchernumber,  
				 @PathVariable("branchcode") String branchcode,  @RequestParam("rdate") String reportdate){
			java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

			return salesInvoiceServiceImpl.getNonTaxableAmount(vouchernumber, date );
	
		}
		
		
		@GetMapping("{companymstid}/getbranchsalehdrreport/{branchcode}")
		public List<BranchSaleReport> getBranchSaleHdrReport(
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable("branchcode") String branchcode, 
				@RequestParam("fromdate") String fromdate,
				@RequestParam("todate") String todate) {
			
			Date sdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
			Date edate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");


			return salesInvoiceServiceImpl.getBranchSaleHdrReport(companymstid, branchcode,sdate, edate);

		}
		
		
		@GetMapping("{companymstid}/salesinvoicereportresource/salesreportbypricetype/{branchcode}/{pricetype}")
		public List<DailySalesReportDtl> getSalesReportByPriceTypeReport(
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable("branchcode") String branchcode, 
				@PathVariable(value = "pricetype") String pricetype,
				@RequestParam("fromdate") String fromdate,
				@RequestParam("todate") String todate) {
			
			Date sdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
			Date edate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");


			return salesInvoiceServiceImpl.getSalesReportByPriceTypeReport(companymstid, branchcode,pricetype,sdate, edate);

		}
		
		
		//=============sharon 17-3-2021
		@GetMapping("{companymstid}/saleorderinvoicereportresource/totalbranchwisesales")
		public List< SaleOrderinoiceReport> getTotalBranchWiseSales
		       (@PathVariable(value = "companymstid") String companymstid,
				@RequestParam("rdate") String reportdate){
			
			
			java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		


			return salesInvoiceServiceImpl.getTotalBranchWiseSales( companymstid ,date );

		}
		
		
		
		@GetMapping("{companymstid}/salesinvoicereport/getbranchsalehdrreport/{branchcode}")
		public List<BranchSaleReport> getBranchSaleHdrReportSummary(
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable("branchcode") String branchcode, 
				@RequestParam("fromdate") String fromdate,
				@RequestParam("todate") String todate) {
			
			Date sdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
			Date edate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");


			return salesInvoiceService.getBranchSaleHdrReportSummary(companymstid, branchcode,sdate, edate);

		}
		
		
		@GetMapping("{companymstid}/salesinvoicereport/gettopsellingproducts")
		public List<BranchSaleReport> getTopSellingProducts(
				@PathVariable(value = "companymstid") String companymstid,
				@RequestParam("date") String date) {
			
			Date udate = SystemSetting.StringToUtilDate(date, "yyyy-MM-dd");


			return salesInvoiceService.getTopSellingProducts(companymstid,udate);

		}
		

}
