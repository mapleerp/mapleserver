package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PDCPayment;
import com.maple.restserver.entity.PDCReceipts;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.PurchaseReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PDCReceiptRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class PDCReceiptsResource {
	
private static final Logger logger = LoggerFactory.getLogger(PDCReceiptsResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	PDCReceiptRepository pDCReceiptRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	EventBus eventBus = EventBusFactory.getEventBus();

	@PostMapping("{companymstid}/pdcreceiptresource/pdcreceipts")
	public PDCReceipts createPDCReceipts(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody PDCReceipts pDCReceipts) {
		return companyMstRepository.findById(companymstid).map(companyMst -> {
			pDCReceipts.setCompanyMst(companyMst);

//			PDCReceipts pdcReceiptsSaved =  pDCReceiptRepository.saveAndFlush(pDCReceipts);
			PDCReceipts pdcReceiptsSaved = saveAndPublishService.savePDCReceipts(pDCReceipts, pDCReceipts.getBranchCode());
			logger.info("pdcReceiptsSaved send to KafkaEvent: {}", pdcReceiptsSaved);
			Map<String, Object> variables = new HashMap<String, Object>();
			variables.put("voucherNumber", pdcReceiptsSaved.getId());
			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", pdcReceiptsSaved.getId());
			variables.put("companyid", pdcReceiptsSaved.getCompanyMst());
			variables.put("branchcode", pdcReceiptsSaved.getBranchCode());
			variables.put("REST", 1);
			variables.put("WF", "forwardPdcReceipts");
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");
			LmsQueueMst lmsQueueMst = new LmsQueueMst();
			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardPdcReceipts");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());

			eventBus.post(variables);
			
			return pdcReceiptsSaved;
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	@DeleteMapping("{companymstid}/pdcreceipts/{pdcreceiptsid}")
	public ResponseEntity<?> deletePost(@PathVariable(value = "pdcreceiptsid") String pdcreceiptsid) {
		return pDCReceiptRepository.findById(pdcreceiptsid).map(pcdreceipts -> {
			pDCReceiptRepository.delete(pcdreceipts);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("pcdreceiptsid " + pdcreceiptsid + " not found"));
	}

	@GetMapping("{companymstid}/pdcreceiptsbydate/{branchcode}")
	public List<PDCReceipts> retrievePDCReceipts(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam("rdate") String reportdate) {
		
		java.sql.Date date = SystemSetting.StringToSqlDate(reportdate, "dd-MM-yyyy");
		
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		return pDCReceiptRepository.findByTransDateAndCompanyMstAndBranchCode(date,companyMst,branchcode);
	}
//	@GetMapping("{companymstid}/pdcreceiptsbydateandvoucherno/{branchcode}/{vouchernumber}")
//	public List<PDCReceipts> retrievePDCReceiptsByDateAndVoucherNo(
//			@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "branchcode") String branchcode,
//			@PathVariable(value = "vouchernumber") String vouchernumber,
//			@RequestParam("rdate") String reportdate) {
//		
//		java.sql.Date date = SystemSetting.StringToSqlDate(reportdate, "dd-MM-yyyy");
//		
//		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
//		CompanyMst companyMst = companyOpt.get();
//
//		return pDCReceiptRepository.findByTransDateAndCompanyMstAndBranchCode(date,companyMst,branchcode);
//	}

	


	@GetMapping("{companymstid}/pdcreceiptbystatus/{bankname}")
	public List<PDCReceipts> retrievePDCReceiptsByBankNameAndStatus(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "bankname") String bankname) {
		
		
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		return pDCReceiptRepository.findByStatusAndDepositBankAndCompanyMst("PENDING",bankname,companyMst);
	}
	@GetMapping("{companymstid}/pdcreceipt/getbystatusandaccountidanddate/{bankname}/{accountid}")
	public List<PDCReceipts> retrievePDCReceiptsByBankNameAndStatusAndAccount(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "bankname") String bankname,
			@PathVariable(value = "accountid") String accountid,
			@RequestParam(value = "rdate")String rdate) {
		
		java.sql.Date trasDate = SystemSetting.StringToSqlDate(rdate, "yyyy-MM-dd");
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		return pDCReceiptRepository.findByStatusAndTransDateAndDepositBankAndAccount("PENDING",trasDate,bankname,accountid);
	}
	
	
	
	
	@GetMapping("{companymstid}/pdcreceiptsbyid/{id}")
	public Optional<PDCReceipts> retrievePDCPaymentById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {
		
		return pDCReceiptRepository.findById(id);
	
	
	
	}
	
	


	@PutMapping("{companymstid}/pdcreceiptresource/updatepdcreceipt/{pdcreceiptid}")
	public PDCReceipts updatePDCPayment(
			@PathVariable(value = "pdcreceiptid") String pdcreceiptid ,
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody PDCReceipts pdcreceiptsRequest)
	{
		PDCReceipts pDCReceipts = pDCReceiptRepository.findById(pdcreceiptid).get();
		
		pDCReceipts.setStatus(pdcreceiptsRequest.getStatus());
		
		
//		PDCReceipts pdcReceiptsSaved =  pDCReceiptRepository.saveAndFlush(pDCReceipts);
		PDCReceipts pdcReceiptsSaved =saveAndPublishService.savePDCReceipts(pDCReceipts, pDCReceipts.getBranchCode());
		logger.info("pdcReceiptsSaved send to KafkaEvent: {}", pdcReceiptsSaved);
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("voucherNumber", pdcReceiptsSaved.getId());
		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", pdcReceiptsSaved.getId());
		variables.put("companyid", pdcReceiptsSaved.getCompanyMst());
		variables.put("branchcode", pdcReceiptsSaved.getBranchCode());
		variables.put("REST", 1);
		variables.put("WF", "forwardPdcReceipts");
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");
		LmsQueueMst lmsQueueMst = new LmsQueueMst();
		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardPdcReceipts");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());

		eventBus.post(variables);
		
		return pdcReceiptsSaved;
		
		
	}
	
	//...sibi..12.03.2021.....
	
	
	@GetMapping("{companymstid}/pdcreceipts/pdcreceiptsbyaccountidandstatus/{accountid}/{status}")
	public List<PDCReceipts> retrievePDCPaymentByAccountid(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "accountid") String accountid, @PathVariable(value = "status") String status) {
		
		return pDCReceiptRepository.findByAccountAndStatus(accountid, status);
	
	
	
	}


}
