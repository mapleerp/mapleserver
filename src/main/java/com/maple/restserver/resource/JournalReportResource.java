package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.JournalReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class JournalReportResource {

	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	JournalReportService journalReportService;
	
	
	

	@GetMapping("{companymstid}/journalreportresource/journalhdrreport/{branchcode}")
	
	 public List<JournalHdr> getJournalHdr(@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable (value = "branchcode") String branchcode ,
				@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate) {
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		
		
		return journalReportService.getJournalHdr(companyMstOpt.get(),fromDate,toDate,branchcode);
	}
	
	
	

	@GetMapping("{companymstid}/journalreportresource/journaldtlreport/{journalhdrid}")
	
	 public List<JournalDtl> getJournalDtl(@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable (value = "journalhdrid") String journalhdrid 
	) {
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		
		
		
		return journalReportService.getJournalDtl(journalhdrid);
	}
	
	
	

	@GetMapping("{companymstid}/journalreportresource/journaljasperreport/{branchcode}")
	
	 public List<JournalDtl> getJournalJasperReportr(@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable (value = "branchcode") String branchcode ,
				@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate) {
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		
		
		return journalReportService.getJournalJasperReportr(companyMstOpt.get(),fromDate,toDate,branchcode);
	}
	
	
}
