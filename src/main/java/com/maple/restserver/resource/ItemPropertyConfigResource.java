package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemPropertyConfig;
import com.maple.restserver.entity.ItemPropertyInstance;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemPropertyConfigRepository;
import com.maple.restserver.repository.ItemPropertyInstanceRepository;

@RestController
public class ItemPropertyConfigResource {
	
	@Autowired
	ItemPropertyInstanceRepository itemPropertyInstanceRepo;
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	ItemPropertyConfigRepository  itemPropertyConfigRepository;
	
	
	@PostMapping("/{companymstid}/itemconfiginstanceresource/createitemconfiginstance")
	public ItemPropertyConfig createItemPropertyConfig(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  ItemPropertyConfig itemPropertyConfig)
	{
		
		 	return companyMstRepo.findById(companymstid).map(companyMst-> {
		 		itemPropertyConfig.setCompanyMst(companyMst);
		 		ItemPropertyConfig savedItemPropertyConfig = itemPropertyConfigRepository.saveAndFlush(itemPropertyConfig);
		 		
		 		
					return savedItemPropertyConfig;
		 		
		 	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
					  companymstid + " not found"));
		 	
		
	}

	
	@GetMapping("/{companymstid}/itempropertyconfigresource/findallitempropertyconfig")
	public List<ItemPropertyConfig> retrieveAllItemPropertyConfig(@PathVariable(value = "companymstid") String companymstid){
		
		Optional<CompanyMst> companyMstOpt=companyMstRepo.findById(companymstid);
		return itemPropertyConfigRepository.findByCompanyMst(companyMstOpt.get());
	}
	
	@GetMapping("/{companymstid}/itempropertyconfigresource/getitempropertyconfigbyitemidandpropertyname/{itemid}")
	public ItemPropertyConfig retrieveAllItemPropertyConfig(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value ="itemid")String itemid,
			@RequestParam(value ="propertyname")String propertyname){
		
		Optional<CompanyMst> companyMstOpt=companyMstRepo.findById(companymstid);
		return itemPropertyConfigRepository.findByItemIdAndPropertyName(itemid,propertyname);
	}
	
	
	
	
	
	@PutMapping("{companymstid}/itempropertyconfigresource/{id}/updateitempropertyconfig")
	public ItemPropertyConfig updateItemPropertyConfig(
			@PathVariable(value="id") String id, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody ItemPropertyConfig  itemPropertyConfigRequest)
	{
			
		ItemPropertyConfig itemPropertyConfig = itemPropertyConfigRepository.findById(id).get();
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		itemPropertyConfig.setCompanyMst(companyMst);
		itemPropertyConfig.setItemId(itemPropertyConfigRequest.getItemId());
		itemPropertyConfig.setBranchCode(itemPropertyConfigRequest.getBranchCode());
		itemPropertyConfig.setPropertyName(itemPropertyConfigRequest.getPropertyName());
		itemPropertyConfig.setPropertyType(itemPropertyConfigRequest.getPropertyType());
		itemPropertyConfig = itemPropertyConfigRepository.save(itemPropertyConfig);
		return itemPropertyConfig;
	}
	
	
	
	
	
	@DeleteMapping("{companymstid}/itempropertyconfigresource/itempropertyconfigdelete/{id}")
	public void deleteItemPropertyConfig(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "id") String
			  id)
	{
		ItemPropertyConfig itemproconfig = itemPropertyConfigRepository.findById(id).get();
		List<ItemPropertyInstance> itemPropertyInstance = itemPropertyInstanceRepo.findByItemIdAndPropertyName(itemproconfig.getItemId(),itemproconfig.getPropertyName());
		if(itemPropertyInstance.size()==0)
		{
			itemPropertyConfigRepository.deleteById(id);
		}
		else
		{
			throw new ResourceNotFoundException("Cannot Delete Item");
		}
	}
		
	@GetMapping("{companymstid}/itempropertyconfig/itempropertyconfigbyitemid/{itemid}")
	public List<ItemPropertyConfig> getAllItemPropertyConfigByItemId(@PathVariable(value = "itemid")
	String itemid)
	{
		return itemPropertyConfigRepository.findByItemId(itemid);
	}
	
}




