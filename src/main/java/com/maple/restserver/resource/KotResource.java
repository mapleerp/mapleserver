package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AccountPayable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.GoodReceiveNoteDtl;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.KotTableDtl;
import com.maple.restserver.entity.KotTableHdr;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.KotTableDtlRepository;
import com.maple.restserver.repository.KotTableHdrRepository;
import com.maple.restserver.service.KotService;
@CrossOrigin("http://localhost:4200")
@RestController
public class KotResource {
	
	@Autowired
	KotService kotService;
	@Autowired 
	KotTableHdrRepository kotTableHdrRepository;
	@Autowired
	KotTableDtlRepository kotTableDtlRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	KotService kotPosService;
	
	
	
	@Autowired
	KotTableHdrRepository kotTableHdrRepo;
	
	@PostMapping("{companymstid}/kotresource/savekothdr")
	public KotTableHdr saveKotPosHdr(
			@PathVariable(value = "companymstid") String companymstid,
			 @RequestBody KotTableHdr kotTableHdr
			)
	{
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		kotTableHdr.setCompanyMst(companyMstOpt.get());
		return kotPosService.saveKotPosHdr(kotTableHdr);
	}
	
	/*
	 * save kot-pos-dtl
	 */
	@PostMapping("{companymstid}/kotresource/savekotdtl")
	public KotTableDtl saveKotPosDtl(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestBody KotTableDtl kotTableDtlList
			)
	{
		System.out.println(kotTableDtlList);
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		kotTableDtlList.setCompanyMst(companyMstOpt.get());
		return kotPosService.saveKotPosDtl(kotTableDtlList);
	}
	/*
	 * print kot
	 */
	@PutMapping("{companymstid}/kotresource/printkot/{hdrid}")
	public KotTableHdr KotPosHdrPrint(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "hdrid") String hdrid,
			 @RequestBody KotTableHdr kotTableHdr
			)
	{
		Optional<KotTableHdr> kotHdr = kotTableHdrRepo.findById(hdrid);
		KotTableHdr kottableHdrs = kotHdr.get();
		return kotPosService.printKOT(kottableHdrs);
	}
	
	
	
	/*
	 * finalsave kot
	 */
	@PutMapping("{companymstid}/kotresource/kotfinalsave/{hdrid}")
	public KotTableHdr KotPosHdrFinalSave(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "hdrid") String hdrid,
			 @RequestBody KotTableHdr kotTableHdr
			)
	{
		Optional<KotTableHdr> kotHdr = kotTableHdrRepo.findById(hdrid);
		KotTableHdr kottableHdrs = kotHdr.get();
		return kotPosService.finalSaveKot(kottableHdrs);
	}
	
	
	
	
	
	/* 
	 * display kotdtls in show table
	 * 
	 */
	@GetMapping("{companymstid}/kotresource/getkottabledtlbyhdr/{kottablehdrid}")
	public List<KotTableDtl> findAllKotTableDtlByKotTableHdr(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "kottablehdrid") String kottablehdrid ){
		Optional<KotTableHdr> kotTableHdr = kotTableHdrRepo.findById(kottablehdrid);
		
		return  kotPosService.findByKotHdr(kotTableHdr.get());

	}
	
	/*
	 * get kot table dtl by table name
	 */
	@GetMapping("{companymstid}/kotresource/getkottabledtlbytablename/{tablename}")
	public List<KotTableDtl> findAllKotTableDtlByTableName(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "tablename") String tablename ){
		List<KotTableDtl> kotTableDtlList1 = new ArrayList<KotTableDtl>();
		List<KotTableDtl> kotTableDtlList2 = new ArrayList<KotTableDtl>();
		List<KotTableHdr> kotTableHdrList = kotTableHdrRepo.findByRunningTable(tablename);
		if(kotTableHdrList.size()>0) {
		for(int i=0;i<kotTableHdrList.size();i++) {
			List<Object> kotDtlList = kotTableDtlRepo.findByKotHdr(kotTableHdrList.get(i));
			for (int j = 0; j < kotDtlList.size(); j++) {
				Object[] objAray = (Object[]) kotDtlList.get(j);
				KotTableDtl kotTableDtl = new KotTableDtl();
				
				kotTableDtl.setItemId((String) objAray[2]);
				kotTableDtl.setQty((Double) objAray[0]);
				kotTableDtl.setRate((Double) objAray[1]);
				kotTableDtl.setId((String) objAray[3]);
				kotTableDtl.setKotNumber((String) objAray[4]);
				Optional<KotTableHdr> kothdr = kotTableHdrRepository.findById((String) objAray[5]);
				kotTableDtl.setKotTableHdr(kothdr.get());
				
				kotTableDtlList2.add(kotTableDtl);
				

			}
			kotTableDtlList1.addAll(kotTableDtlList2);
		}
		}
		return  kotTableDtlList1;

	}
	
	
	// delete
			@DeleteMapping("{companymstid}/kotresource/deleteitem/{id}")
			public void DeleteKotItem(@PathVariable(value = "id") String Id) {
				kotTableDtlRepo.deleteById(Id);
			}
}
