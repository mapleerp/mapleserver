package com.maple.restserver.resource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyConversionMst;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CurrencyConversionMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;
import com.maple.restserver.service.CurrencyExchnageService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class CurrencyConversionMstResource {

	@Autowired
	CurrencyMstRepository currencyMstRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	CurrencyConversionMstRepository currencyConversionMstRepo;
	
	@Autowired
	CurrencyExchnageService currencyExchnageService;
	
	@PostMapping("{companymstid}/currencyconversionmst/savecompanycurrency")
	public CurrencyConversionMst createCurrencyConversionMst(
			@PathVariable(value="companymstid")
			String companymstid,
			@Valid @RequestBody CurrencyConversionMst currencyConversionMst)
	{    
		       CompanyMst companyMst=companyMstRepo.findById(companymstid).get();
		       currencyConversionMst.setCompanyMst(companyMst);
		       
		       if(null==currencyConversionMst.getUpdatedTime()) {
		    	   LocalDateTime date = LocalDateTime.now(); 
		    	   currencyConversionMst.setUpdatedTime(date);
		       }
		      
		       currencyConversionMst=currencyConversionMstRepo.save(currencyConversionMst);
		       return currencyConversionMst;
		       
	}
	@GetMapping("{companymstid}/currencyconversionmst/getallcurrencyconversionmst")
	public List<CurrencyConversionMst> retrieveAllbranch(@PathVariable(value = "companymstid") String companymstid)
	{
		return currencyConversionMstRepo.findByCompanyMstId(companymstid);
	};

	
	@GetMapping("{companymstid}/currencyconversionmst/currencyconversionmstbycurrency/{currencyname}")
	public CurrencyConversionMst getCurrencyConversionMstByCurrencyName(
			@PathVariable(value = "currencyname")String currencyname,
			@PathVariable(value = "companymstid")String companymstid)
	{
		return currencyConversionMstRepo.getCurrencyConversionByCurrencyName(currencyname);
	}
	@GetMapping("{companymstid}/currencyconversionmst/currencyconversionmstbycurrencyid/{currencyid}")
	public CurrencyConversionMst getCurrencyConversionMstByCurrencyId(
			@PathVariable(value = "currencyid")String currencyid,
			@PathVariable(value = "companymstid")String companymstid)
	{
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		CurrencyMst currencyMst = currencyMstRepo.findByCurrencyName(companyMst.get().getCurrencyName());
		
		return currencyConversionMstRepo.findByToCurrencyIdAndFromCurrencyId(currencyMst.getId(),currencyid);
	}
	
	
	@GetMapping("{companymstid}/currencyconversionmst/findfcrate/{rate}/{currencyName}")
	public Double getFCRate(
			@PathVariable(value = "rate")Double rate,
			@PathVariable(value = "currencyName")String currencyName,
			@PathVariable(value = "companymstid")String companymstid)
	{
		Double fcrate=0.0;
		CurrencyMst currencyMstByid = currencyMstRepo.findByCurrencyName(currencyName);
		CurrencyConversionMst currencyConversionMst = currencyConversionMstRepo.findByToCurrencyId(currencyMstByid.getId());
		if(null != currencyConversionMst)
		{
		Double fcRate = rate/currencyConversionMst.getConversionRate();
		BigDecimal bdfcRate = new BigDecimal(fcRate);
		bdfcRate = bdfcRate.setScale(2,BigDecimal.ROUND_HALF_EVEN);
		fcrate =  bdfcRate.doubleValue();
		}
		return fcrate;
		
	}
	
	@GetMapping("{companymstid}/currencyconversionmst/findcompanyCurrency/{rate}/{currencyName}")
	public Double getCompanyCurrencyRate(
			@PathVariable(value = "rate")Double rate,
			@PathVariable(value = "currencyName")String currencyName,
			@PathVariable(value = "companymstid")String companymstid)
	{
		Double fcrate=0.0;
		CurrencyMst currencyMstByid = currencyMstRepo.findByCurrencyName(currencyName);
		CurrencyConversionMst currencyConversionMst = currencyConversionMstRepo.findByToCurrencyId(currencyMstByid.getId());
		if(null != currencyConversionMst)
		{
		Double fcRate = rate*currencyConversionMst.getConversionRate();
		BigDecimal bdfcRate = new BigDecimal(fcRate);
		bdfcRate = bdfcRate.setScale(2,BigDecimal.ROUND_HALF_EVEN);
		fcrate =  bdfcRate.doubleValue();
		}
		return fcrate;
	
	}

	@DeleteMapping("{companymstid}/currencyconversionmstresource/deletecurrencyconversionmst/{id}")
	public void CurrencyConversionMstDelete(@PathVariable(value = "id") String Id) {
		currencyConversionMstRepo.deleteById(Id);

	}
	
	
	@GetMapping("{companymstid}/currencyconversionmst/getconvertedcurrencyamount/{fromcurrency}/{tocurrency}/{amount}")
	public Double cuurencyConversion(
			@PathVariable(value = "amount")Double amount,
			@PathVariable(value = "fromcurrency")String fromcurrency,
			@PathVariable(value = "tocurrency")String tocurrency,
			@PathVariable(value = "companymstid")String companymstid)
	{
		return currencyExchnageService.getCurrencyExchange(fromcurrency, tocurrency, amount, companymstid);
	
	}
}

