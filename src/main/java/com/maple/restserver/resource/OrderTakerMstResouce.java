package com.maple.restserver.resource;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DeliveryBoyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.OrderTakerMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.OrderTakerMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class OrderTakerMstResouce {
private static final Logger logger = LoggerFactory.getLogger(OrderTakerMstResouce.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	EventBus eventBus = EventBusFactory.getEventBus();
	
	@Autowired
	OrderTakerMstRepository 	orderTakerMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	 
		@Autowired
		SaveAndPublishService saveAndPublishService;

	@PostMapping("{companymstid}/ordertakermst")
	public OrderTakerMst createDeliveryBoy(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 OrderTakerMst  ordertakerMst)
	{
		
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		
		CompanyMst companyMst = companyMstOpt.get();
		ordertakerMst.setCompanyMst(companyMst);
		
		
		
//		OrderTakerMst savedOrderTakerMst = orderTakerMstRepository.saveAndFlush(ordertakerMst);
       
		OrderTakerMst savedOrderTakerMst =saveAndPublishService.saveOrderTakerMst(ordertakerMst, ordertakerMst.getBranchCode());
		logger.info("savedOrderTakerMst send to KafkaEvent: {}", savedOrderTakerMst);
       Map<String, Object> variables = new HashMap<String, Object>();

     		variables.put("voucherNumber", savedOrderTakerMst.getId());

     		variables.put("voucherDate", SystemSetting.getSystemDate());
     		variables.put("inet", 0);
     		variables.put("id", savedOrderTakerMst.getId());
			variables.put("branchcode", savedOrderTakerMst.getBranchCode());

     		variables.put("companyid", savedOrderTakerMst.getCompanyMst());

     		variables.put("REST", 1);
     		
     		
     		variables.put("WF", "forwardOrderTakerMst");
     		
     		
     		String workflow = (String) variables.get("WF");
    		String voucherNumber = (String) variables.get("voucherNumber");
    		String sourceID = (String) variables.get("id");


    		LmsQueueMst lmsQueueMst = new LmsQueueMst();

    		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
    		
    		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
    		
    		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
    		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
    		lmsQueueMst.setVoucherDate(sqlVDate);
    		
    		
    		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
    		lmsQueueMst.setVoucherType(workflow);
    		lmsQueueMst.setPostedToServer("NO");
    		lmsQueueMst.setJobClass("forwardOrderTakerMst");
    		lmsQueueMst.setCronJob(true);
    		lmsQueueMst.setJobName(workflow + sourceID);
    		lmsQueueMst.setJobGroup(workflow);
    		lmsQueueMst.setRepeatTime(60000L);
    		lmsQueueMst.setSourceObjectId(sourceID);
    		
    		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

    		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
    		variables.put("lmsqid", lmsQueueMst.getId());
     		eventBus.post(variables);
     		return savedOrderTakerMst;
       
		/*
		 * Work Flow
		 * 
		 * forwardOrderTakerMst
		 */
		
	} 
	

	@DeleteMapping("/{companymstid}/deleteordertaker/{id}")
	public void orderTakerDelete(@PathVariable(value = "id") String Id) {
		orderTakerMstRepository.deleteById(Id);

	}
	
	
	@GetMapping("{companymstid}/getordertaker/{branchcode}")
	public List<OrderTakerMst> retrievOrderTaker(@PathVariable(value="companymstid") String companymstid){
		return orderTakerMstRepository.findByCompanyMstId(companymstid);
	}
	@GetMapping("{companymstid}/getordertakerbystatus/{branchcode}")
	public List<OrderTakerMst> retrievOrderTakerByStatus(@PathVariable(value="companymstid") String companymstid){
		return orderTakerMstRepository.getOrderTakerbyStatus(companymstid);
	}
	
	
	@PutMapping("{companyid}/updateordertaker/{ordertakerid}/ordertaker")
	public OrderTakerMst orderTakerMstUpdate(
			@PathVariable String ordertakerid, 
			@PathVariable (value = "companyid") String companyid,
			@Valid @RequestBody OrderTakerMst ordertakerMstRequest)
	{
 		  
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();
	
		ordertakerMstRequest.setCompanyMst(companyMst);
		 
				return orderTakerMstRepository.findById(ordertakerid).map(ordertakermst -> {
					 
					ordertakermst.setStatus(ordertakerMstRequest.getStatus());
					
//					orderTakerMstRepository.saveAndFlush(ordertakermst);
					saveAndPublishService.saveOrderTakerMst(ordertakermst, ordertakermst.getBranchCode());
					logger.info("savedOrderTakerMst send to KafkaEvent: {}", ordertakermst);
					
		            return ordertakerMstRequest;
		            
		        }).orElseThrow(() -> new ResourceNotFoundException("ordertakermst " + ordertakerMstRequest + " not found"));
				
				
			
	}
	
	@GetMapping("{companymstid}/getordertakerbyname/{ordertakername}/{branchcode}")
	public OrderTakerMst getOrderTakerByName(@PathVariable(value="companymstid") String companymstid,@PathVariable(value="ordertakername") String ordertakername){
		return orderTakerMstRepository.findByCompanyMstIdAndOrderTakerName(companymstid,ordertakername);
	}
	

	@GetMapping("{companymstid}/ordertakermst/ordertakerbyid/{id}")
	public OrderTakerMst getOrderTakerById(@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="id") String id){
		return orderTakerMstRepository.findByCompanyMstIdAndId(companymstid,id);
	}
	
}
