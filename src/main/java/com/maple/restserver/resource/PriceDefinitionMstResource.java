package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PhysicalStockHdr;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class PriceDefinitionMstResource {
private static final Logger logger = LoggerFactory.getLogger(PriceDefinitionMstResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	EventBus eventBus = EventBusFactory.getEventBus();
	 
	//@Autowired
	//private RuntimeService runtimeService;
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepo;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@PostMapping("{companymstid}/pricedefinitionmst")
	public PriceDefenitionMst createPricedefinitionmst(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody PriceDefenitionMst priceDefinitionMst) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);

		priceDefinitionMst.setCompanyMst(companyMstOpt.get());

//		PriceDefenitionMst saved = priceDefinitionMstRepo.saveAndFlush(priceDefinitionMst);
		PriceDefenitionMst saved =saveAndPublishService.savePriceDefenitionMst(priceDefinitionMst,mybranch);
		logger.info("PriceDefenitionMst send to KafkaEvent: {}", saved);
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("voucherNumber", saved.getId());
		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", saved.getId());
		variables.put("companyid", saved.getCompanyMst());
		variables.put("branchcode", saved.getCompanyMst().getId());

		variables.put("REST", 1);

		
		variables.put("WF", "forwardPriceDefinitionMst");
		
		
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
	//
		//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardPriceDefinitionMst");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		
		variables.put("lmsqid", lmsQueueMst.getId());
		
		eventBus.post(variables);
		
		/*
		ProcessInstanceWithVariables pVariablesInReturn = runtimeService.createProcessInstanceByKey("Forwardpricedef")
				.setVariables(variables)
				

				.executeWithVariablesInReturn();
				*/
		

		return saved;

	}

	@DeleteMapping("{companymstid}/pricedefinitionmstdelete/{id}")
	public void priceDefinitionMstDelete(@PathVariable(value = "id") String Id) {
		priceDefinitionMstRepo.deleteById(Id);

	}

	@GetMapping("{companymstid}/findallpricedefinitionmst")
	public List<PriceDefenitionMst> retrieveAlPriceDefinition(
			@PathVariable(value = "companymstid") String companymstid) {
		return priceDefinitionMstRepo.findAll();
	}

	@GetMapping("{companymstid}/pricedefinitionmstbyname/{name}")
	public @ResponseBody List<PriceDefenitionMst> pricedefinitionMstbyName(@PathVariable(value = "name") String name) {
		return priceDefinitionMstRepo.findByPriceLevelName(name);
	}

	@GetMapping("{companymstid}/pricedefinitionmstbypricecalculator/{pricecalculator}")
	public @ResponseBody List<PriceDefenitionMst> pricedefinitionMstbyPriceCalculator(
			@PathVariable(value = "pricecalculator") String pricecalculator) {
		return priceDefinitionMstRepo.findByPriceCalculator(pricecalculator);
	}

	@GetMapping("{companymstid}/pricedefinitionmstbyid/{id}")
	public @ResponseBody Optional<PriceDefenitionMst> pricedefinitionMstbyid(@PathVariable(value = "id") String id) {
		return priceDefinitionMstRepo.findById(id);
	}
	
	@GetMapping("{companymstid}/pricedefinitionmstbyname")
	public PriceDefenitionMst retrieveAlPriceDefinitionByName(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("pricename") String pricename) {
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return priceDefinitionMstRepo.findByCompanyMstAndPriceLevelName(companyOpt.get(),pricename);
	}

}
