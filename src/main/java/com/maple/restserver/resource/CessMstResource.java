
package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AddKotWaiter;
import com.maple.restserver.entity.CessMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CessMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
 

@RestController
@Transactional
public class CessMstResource {
	
	@Autowired
	private CessMstRepository cessMstRepository;
	
	@GetMapping("{companymstid}/cessmst")
	public List< CessMst> retrieveAllSupplier(@PathVariable(value = "companymstid") String
			  companymstid){
		return cessMstRepository.findByCompanyMstId(companymstid);
	}
	@Autowired
	CompanyMstRepository companyMstRepo;


	
	@PostMapping("/{companymstid}/cessmst")
	public CessMst createsupplier(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			CessMst  cessMst)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			cessMst.setCompanyMst(companyMst);
			
			
			
			/*
			 * Post Cess to be done
			 * 
			 */
		return  cessMstRepository.saveAndFlush(cessMst);
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
		
	
	}


