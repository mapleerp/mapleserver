package com.maple.restserver.resource;

 
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.derby.tools.sysinfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.service.CompanyMstService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;

@RestController
@Transactional
@CrossOrigin("http://localhost:4200")
public class CompanyMstResource {
	
	private static final Logger logger = LoggerFactory.getLogger(CompanyMstResource.class);

	@Autowired
	CompanyMstService companyMstService;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	@Value("${mybranch}")
	private String myBranch;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	// @Autowired
	//  private RuntimeService runtimeService;
	 
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	 
	 @Autowired
	 BranchMstRepository branchMstRepository;
	 
	 
	 @Autowired
	 SaveAndPublishService saveAndPublishService;
	 
	 
	
	
	@PostMapping("/companymst")
	public CompanyMst createaCompanyMst( @RequestBody CompanyMst companyMst)
	{
		System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
		System.out.println(companyMst);
		
		
	//CompanyMst savedCompany = 	companyMstRepo.saveAndFlush(companyMst);
		CompanyMst savedCompany=saveAndPublishService.saveCompanyMst(companyMst,myBranch);
		logger.info("CompanyCreation send to KafkaEvent: {}", savedCompany);
		
	Map<String, Object> variables = new HashMap<String, Object>();
		
		variables.put("voucherNumber", savedCompany.getId());
		variables.put("id", savedCompany.getId());
		variables.put("voucherDate", ClientSystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("REST", 1);
		variables.put("companyid",savedCompany);
		
		
		variables.put("WF", "forwardCompany");
		
		

 		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardCompany");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);

		
	/*	ProcessInstanceWithVariables pVariablesInReturn  = runtimeService.createProcessInstanceByKey("forwardCompany")
				.setVariables(variables)
				 
				.executeWithVariablesInReturn();
				*/
		
			
		return savedCompany;
 
		}

	@GetMapping("/companymst/{companymstid}/companymst")
	public Optional<CompanyMst> retrievCompanyMstById(@PathVariable String companymstid){
		return companyMstRepo.findById(companymstid);
	}
	@GetMapping("/getcompanymst/{companymstid}")
	public Optional<CompanyMst> getCompanyMstById(@PathVariable String companymstid){
		return companyMstRepo.findById(companymstid);
	}
 
 
	@GetMapping("/companymst")
	public List<CompanyMst> getAllCompanyMst(){
		return companyMstRepo.findAll();
	}
 

	@GetMapping("/{companymstid}/companymst")
	public List<CompanyMst> getAllCompanyMstForSystem(){
		return companyMstRepo.findAll();
	}
	
	@GetMapping("/{companymstid}/companymst/getversion")
	public String getVersion(){
		return ClientSystemSetting.version;
	}
	@GetMapping("/{companymstid}/companymst/getcountry")
	public List<String> getAllCountries()
	{
		
		ArrayList<String> strList = new ArrayList();
		strList.add("INDIA");
		strList.add("MALDIVES");
		return strList;
		
	}
	
	@GetMapping("/{companymstid}/companymst/getstatebycountry")
	public List<String> getAllStateByCountry(
			@RequestParam (value ="country")String country)
	{
			ArrayList<String> strList = new ArrayList();
			strList =	companyMstService.getStateByCountry(country);
			return strList;

		
	}
	
	@PostMapping("/companymstforwebapp")
	public CompanyMst createCompanyMst( @RequestBody CompanyMst companyMst)
	{
		System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
		System.out.println(companyMst);
		
		
	CompanyMst savedCompany = 	companyMstRepo.saveAndFlush(companyMst);
	
	BranchMst branchMst = new BranchMst();
	
	branchMst.setId(savedCompany.getId());
	String branchName="BRCH"+savedCompany.getCompanyName();
	branchMst.setBranchName(branchName);
	branchMst.setBranchCode(branchName);
	branchMst.setBranchState(savedCompany.getState());
	branchMst.setCompanyMst(savedCompany);
	//branchMst.setMyBranch("Y");
	
	branchMstRepository.save(branchMst);
	return savedCompany;
	
}
	
	@PostMapping("/companymstforcloudweb")
	public CompanyMst saveCompanyMst( @RequestBody CompanyMst companyMst)
	{
		System.out.println("---^^^---^^^---^^^---^^^---^^^---^^^---^^^---^^^---");
		System.out.println(companyMst);		
		CompanyMst savedCompany = 	companyMstRepo.save(companyMst);
		return savedCompany;		
	}
}