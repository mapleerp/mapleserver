package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.OwnAccountSettlementDtl;
import com.maple.restserver.entity.OwnAccountSettlementMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.OwnAccountSettlementDtlRepository;
import com.maple.restserver.repository.OwnAccountSettlementMstRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Transactional
public class OwnAccountSettlementDtlResource {
	
private static final Logger logger = LoggerFactory.getLogger(OwnAccountSettlementDtlResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	OwnAccountSettlementMstRepository 	ownAccountSettlementMstRepository;
	
	@Autowired
	OwnAccountSettlementDtlRepository ownAccountSettlementDtlRepository;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@PostMapping("{companymstid}/ownaccountsettlementdtl")
	public OwnAccountSettlementDtl createOwnAccountSettlementDtl(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 OwnAccountSettlementDtl ownaccountsettlementdtl)
    {
		
		Optional<CompanyMst> companyMst= companyMstRepository.findById(companymstid);
			ownaccountsettlementdtl.setCompanyMst(companyMst.get());
      // return ownAccountSettlementDtlRepository.saveAndFlush(ownaccountsettlementdtl);
			return saveAndPublishService.saveOwnAccountSettlementDtl(ownaccountsettlementdtl, mybranch);
		 
		}
		
	
		@DeleteMapping("{companymstid}/deleteownaccountsettlementdtl/{ownaccountsettlementdtlid}")
		public void deleteOwnAccountSettleMentDtls(@PathVariable(value = "ownaccountsettlementdtlid") String ownaccountsettlementdtlid) {
			ownAccountSettlementDtlRepository.deleteById(ownaccountsettlementdtlid);

		}
	  
@GetMapping("{companymstid}/ownaccountsettlementdtls/{ownaccountsettlementmstid}")
public List<OwnAccountSettlementDtl> retrieveOwnAccountSettlementDtl(@PathVariable(value="companymstid") String companymstid,
		@PathVariable(value="ownaccountsettlementmstid") String ownaccountsettlementmstid){
	
	Optional<CompanyMst> companyMst= companyMstRepository.findById(companymstid);

	Optional<OwnAccountSettlementMst> OwnAccountSettlementMst=ownAccountSettlementMstRepository.findById(ownaccountsettlementmstid);
	return ownAccountSettlementDtlRepository.findByOwnAccountSettlementMstAndCompanyMst(OwnAccountSettlementMst.get(),companyMst.get());
}
	
@GetMapping("{companymstid}/getownaccountsettlementdtls/{invoicenumber}")
public OwnAccountSettlementDtl fetchOwnAccountSettlementDtlbyInvoiceNumber(@PathVariable(value="companymstid") String companymstid,
		@PathVariable(value="invoicenumber") String invoicenumber){
	
	Optional<CompanyMst> companyMst= companyMstRepository.findById(companymstid);

	return ownAccountSettlementDtlRepository.findByInvoiceNumberAndCompanyMst(invoicenumber,companyMst.get());
}



   @PutMapping("{companymstid}/ownaccountsettlementdtl/{invoicenumber}")
   public OwnAccountSettlementDtl updateOwnAccountSettlementDtl(@PathVariable (value = "companymstid") String companymstid,
                                @PathVariable (value = "invoicenumber") String invoicenumber,
                                @Valid @RequestBody OwnAccountSettlementDtl ownAccountSettlementDtlRequest) {
      

       return ownAccountSettlementDtlRepository.findByInvoiceNumber(invoicenumber).map(ownacccountSettlmentdtl -> {
     
       	ownacccountSettlmentdtl.setPaidAmount(ownAccountSettlementDtlRequest.getPaidAmount());
       	
         //  return ownAccountSettlementDtlRepository.saveAndFlush(ownacccountSettlmentdtl);
       	return saveAndPublishService.saveOwnAccountSettlementDtl(ownacccountSettlmentdtl, mybranch);
       }).orElseThrow(() -> new ResourceNotFoundException("invoicenumber " + invoicenumber + "not found"));
   }


}
