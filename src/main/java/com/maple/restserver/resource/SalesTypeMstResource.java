package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.SalesTypeMst;

import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.SalesTypeMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.SalesTypeMstService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
@CrossOrigin("http://localhost:4200")
@RestController
public class SalesTypeMstResource {
	EventBus eventBus = EventBusFactory.getEventBus();
	@Autowired
	SalesTypeMstRepository 	salesTypeMstRepository; 
	@Autowired
	private VoucherNumberService voucherService;
	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	SalesTypeMstService salesTypeMstService;

	
	@PostMapping("{companymstid}/salestypemst")
	public SalesTypeMst createSalesTypeMst(@PathVariable(value = "companymstid") String companymstid
           , @Valid @RequestBody SalesTypeMst salesTypeMst) 
	{
		 
		Optional<CompanyMst> companyMstOPt=companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOPt.get();
			 
			 salesTypeMst.setCompanyMst(companyMst);
			 
			
			 salesTypeMst=salesTypeMstRepository.saveAndFlush(salesTypeMst);
	             
//	 	        Map<String, Object> variables = new HashMap<String, Object>();
//
//	 			variables.put("voucherNumber", salesTypeMst.getId());
//
//	 			variables.put("voucherDate", SystemSetting.getSystemDate());
//	 			variables.put("inet", 0);
//	 			variables.put("id", salesTypeMst.getId());
//
//	 			variables.put("companyid", salesTypeMst.getCompanyMst());
//	 			variables.put("branchcode", salesTypeMst.getBranchCode());
//
//	 			variables.put("REST", 1);
//	 			
//	 			
//	 			variables.put("WF", "forwardSalesTypeMst");
//	 			
//	 			
//	 			String workflow = (String) variables.get("WF");
//	 			String voucherNumber = (String) variables.get("voucherNumber");
//	 			String sourceID = (String) variables.get("id");
//
//
//	 			LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//	 			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//	 			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
//	 			
//	 			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
//	 			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//	 			lmsQueueMst.setVoucherDate(sqlVDate);
//	 			
//	 			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//	 			lmsQueueMst.setVoucherType(workflow);
//	 			lmsQueueMst.setPostedToServer("NO");
//	 			lmsQueueMst.setJobClass("forwardSalesTypeMst");
//	 			lmsQueueMst.setCronJob(true);
//	 			lmsQueueMst.setJobName(workflow + sourceID);
//	 			lmsQueueMst.setJobGroup(workflow);
//	 			lmsQueueMst.setRepeatTime(60000L);
//	 			lmsQueueMst.setSourceObjectId(sourceID);
//	 			
//	 			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//	 			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
//	 			
//	 			variables.put("lmsqid", lmsQueueMst.getId());
//	 			
//	 			eventBus.post(variables);

	 			return salesTypeMst;
	}
	
	
	@GetMapping("{companymstid}/salestypemstrepository/salestypemsts/{branchcode}")
	public List<SalesTypeMst> findallSalesTypeMst(
			@PathVariable(value = "companymstid") String companymstid ,
			@PathVariable(value = "branchcode") String branchcode)
	{
		
		 Optional<CompanyMst> companyMstOpt =  companyMstRepository.findById(companymstid) ;
		return salesTypeMstRepository.findByCompanyMst(companyMstOpt.get());
		
	}
	
	@GetMapping("{companymstid}/salestypemstrepository/salestypemstsbysaletype/{salestype}/{branchcode}")
	public SalesTypeMst findallSalesTypeMstBySalesType(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "salestype") String salestype)
	{
		
		 Optional<CompanyMst> companyMstOpt =  companyMstRepository.findById(companymstid) ;
		 if(companyMstOpt.isPresent()) {
			 return salesTypeMstRepository.findByCompanyMstAndSalesType(companyMstOpt.get(),salestype);
		 }else {
			 return null;
		 }
		
		
	}
	
	
	

	@DeleteMapping("{companymstid}/salestypemstrepository/salestypemstdelete/{Id}")
	public void SalesTypeMstDelete(@PathVariable String Id) {
		salesTypeMstRepository.deleteById(Id);
		salesTypeMstRepository.flush();
		 

	}
	
	
	
	//=====================initializeCorporateSale===============================
		@GetMapping("{companymstid}/salestypemstresource/initializecorporatesale/{branchcode}")
		public String initializeCorporateSale(
				@PathVariable(value = "companymstid") String companymstid,
		@PathVariable(value = "branchcode") String branchcode)
		
				{
			
			return salesTypeMstService.initializeCorporateSale(companymstid,branchcode);
		}
		
}
