package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductMst;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.entity.WatchStrapMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProductMstRepository;
import com.maple.restserver.repository.WatchStrapMstRepository;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class WatchStrapMstResource {
	Pageable topFifty =   PageRequest.of(0, 50);
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	private WatchStrapMstRepository watchStrapMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;

	@GetMapping("{companymstid}/watchstrapmstresource/watchmstbyname")
	public WatchStrapMst retrieveWatchStrapMstByName(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("strapname") String strapname) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}

		return watchStrapMstRepository.findByCompanyMstAndStrapName(companyOpt.get(), strapname);
	}

	@PostMapping("{companymstid}/watchstrapmstresource/savewatchsatrapmst")
	public WatchStrapMst createProductMst(@Valid @RequestBody WatchStrapMst watchStrapMst,
			@PathVariable(value = "companymstid") String companymstid) {
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}
		
		watchStrapMst.setCompanyMst(companyOpt.get());

		watchStrapMst = watchStrapMstRepository.saveAndFlush(watchStrapMst);
		
		   Map<String, Object> variables = new HashMap<String, Object>();

					variables.put("voucherNumber", watchStrapMst.getId());

					variables.put("voucherDate", SystemSetting.getSystemDate());
					variables.put("inet", 0);
					variables.put("id", watchStrapMst.getId());

					variables.put("companyid", watchStrapMst.getCompanyMst());
					variables.put("branchcode", watchStrapMst.getBranchCode());

					variables.put("REST", 1);
					
					
					variables.put("WF", "forwardWatchStrapMst");
					
					
					String workflow = (String) variables.get("WF");
					String voucherNumber = (String) variables.get("voucherNumber");
					String sourceID = (String) variables.get("id");


					LmsQueueMst lmsQueueMst = new LmsQueueMst();

					lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
					
					//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
					
					
					java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
					java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
					lmsQueueMst.setVoucherDate(sqlVDate);
					
					
					
					lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
					lmsQueueMst.setVoucherType(workflow);
					lmsQueueMst.setPostedToServer("NO");
					lmsQueueMst.setJobClass("forwardWatchStrapMst");
					lmsQueueMst.setCronJob(true);
					lmsQueueMst.setJobName(workflow + sourceID);
					lmsQueueMst.setJobGroup(workflow);
					lmsQueueMst.setRepeatTime(60000L);
					lmsQueueMst.setSourceObjectId(sourceID);
					
					lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

					lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
					variables.put("lmsqid", lmsQueueMst.getId());
					eventBus.post(variables);
					
					return watchStrapMst;

	}
	
	@GetMapping("{companymstid}/watchstrapmstresource/watchstrapmstbyid/{id}")
	public WatchStrapMst retrieveWatchStrapMstById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}

		return watchStrapMstRepository.findByCompanyMstAndId(companyOpt.get(), id);
	}

	@GetMapping("{companymstid}/watchstrapmstresource/watchstrapmstbycompanymst")
	public List< Object> retrieveAllProduct(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid)
	{
		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		return  watchStrapMstRepository.findSearch("%"+searchstring.toLowerCase()+"%", topFifty);
	}
}


