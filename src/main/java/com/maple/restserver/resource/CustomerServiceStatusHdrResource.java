package com.maple.restserver.resource;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CustomerServiceStatusHdr;
import com.maple.restserver.entity.ServiceItemMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CustomerServiceStatusHdrRepository;

@RestController
public class CustomerServiceStatusHdrResource {

	

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	CustomerServiceStatusHdrRepository customerServiceStatusHdrRepo;
	
	@PostMapping("{companymstid}/customerservicestatushdr")
	public CustomerServiceStatusHdr createCustomerServiceStatusHdr(@PathVariable(value = "companymstid") String companymstid,@Valid @RequestBody 
			CustomerServiceStatusHdr customerServiceStatusHdr)
	{
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			customerServiceStatusHdr.setCompanyMst(companyMst);
			CustomerServiceStatusHdr customerServiceStatusHdrSaved = customerServiceStatusHdrRepo.saveAndFlush(customerServiceStatusHdr);
		
			return customerServiceStatusHdrSaved;
	}
	
	
	@PutMapping("{companymstid}/customerservicestatushdr/{custserviceid}/updatecustomerservicestatushdr")
	public CustomerServiceStatusHdr updateCustomerServiceStatusHdr(
			@PathVariable(value="custserviceid") String custserviceid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody CustomerServiceStatusHdr  customerServiceStatusHdrRequest)
	{
			
		CustomerServiceStatusHdr customerServiceStatusHdr = customerServiceStatusHdrRepo.findById(custserviceid).get();
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		customerServiceStatusHdr.setCompanyMst(companyMst);
		customerServiceStatusHdr.setJobCardNumber(customerServiceStatusHdrRequest.getJobCardNumber());
		customerServiceStatusHdr.setLocationId(customerServiceStatusHdrRequest.getLocationId());
		customerServiceStatusHdr = customerServiceStatusHdrRepo.saveAndFlush(customerServiceStatusHdr);
		
		return customerServiceStatusHdr;
	}
}
