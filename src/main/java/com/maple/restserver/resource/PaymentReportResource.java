package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.accounting.service.AccountingService;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.report.entity.DailyPaymentSummaryReport;
import com.maple.restserver.report.entity.OrgPaymentVoucher;
import com.maple.restserver.report.entity.PaymentReports;
import com.maple.restserver.report.entity.PaymentVoucher;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.service.PaymentReportService;
import com.maple.restserver.service.PaymentReportServiceImpl;
import com.maple.restserver.utils.SystemSetting;
 


@RestController
@Transactional
public class PaymentReportResource {
	

	@Autowired
	AccountingService accountingService;
	@Autowired
	private PaymentReportServiceImpl paymentReportServiceImpl;
	
	@Autowired
	private PaymentReportService paymentReportService;
	@Autowired
	CompanyMstRepository 	companyMstRepository; 

	@Autowired
	PaymentDtlRepository paymentDtlRepo;
	
	@Autowired
	PaymentHdrRepository paymentHdrRepo;
	
	@GetMapping("{companymstid}/paymentvoucher")
	public List<PaymentVoucher> retrieveAllPayment(){
		return paymentReportServiceImpl.getPayments();
	}
	
	

	
	@GetMapping("{companymstid}/paymentreport/{vouchernumber}/{branchcode}")
	public List<PaymentVoucher>  getPaymentReport(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "vouchernumber") String
			  vouchernumber,
			  @PathVariable(value = "branchcode") String
			  branchCode,
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
	
	
	

		return paymentReportServiceImpl.getPaymentReport(vouchernumber,date,branchCode);
	
	}	
	@GetMapping("{companymstid}/paymentreport/paymentbetweendate/{branchcode}")
	public List<PaymentVoucher>  getPaymentReportBetweenDate(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "branchcode") String
			  branchCode,
			  @RequestParam("rdate") String reportFromdate,
			  @RequestParam("tdate") String reportTodate){
		java.util.Date fdate = SystemSetting.StringToUtilDate(reportFromdate,"yyyy-MM-dd");
		java.util.Date tdate = SystemSetting.StringToUtilDate(reportTodate,"yyyy-MM-dd");
	
	
	

		return paymentReportServiceImpl.getPaymentReportBetweenDate(fdate,tdate,branchCode,companymstid);
	
	}	
	@GetMapping("{companymstid}/paymentreport/paymentfordayend/{branchcode}/{acid}")
	public Double  getPaymentReportForDayEnd(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "branchcode") String
			  branchCode,@PathVariable(value = "acid") String
			  acid,
			  @RequestParam("rdate") String reportFromdate){
		
		java.util.Date fdate = SystemSetting.StringToUtilDate(reportFromdate,"yyyy-MM-dd");
		return paymentHdrRepo.getPaymentReportbyDate(fdate,branchCode,companymstid,acid);
	
	}	
	@GetMapping("{companymstid}/paymentreport/paymentbetweendatebyaccount/{accountid}/{branchcode}")
	public List<PaymentVoucher>  getPaymentReportBetweenDateByAccount(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "branchcode") String
			  branchCode,
			  @PathVariable(value = "accountid") String
			  accountid,
			  @RequestParam("rdate") String reportFromdate,
			  @RequestParam("tdate") String reportTodate){
		java.util.Date fdate = SystemSetting.StringToUtilDate(reportFromdate,"yyyy-MM-dd");
		java.util.Date tdate = SystemSetting.StringToUtilDate(reportTodate,"yyyy-MM-dd");
	
	
	

		return paymentReportServiceImpl.getPaymentReportBetweenDateByAccount(accountid,fdate,tdate,branchCode,companymstid);
	
	}	
	
	
	

	@GetMapping("{companymstid}/paymentdtlreport/{accountid}")
	public List<PaymentDtl>  getPaymentDtlReport(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "accountid") String
			  accountid,
			 
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
	
	
		return paymentReportServiceImpl.findByAccountIdAndInstrumentDate(accountid,date);
	
	}	
	
	@GetMapping("{companymstid}/paymentreport/{vouchernumber}/{branchcode}/{memberid}")
	public List<OrgPaymentVoucher>  getOrgPaymentReport(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable(value = "vouchernumber") String
			  vouchernumber,
			  @PathVariable(value = "branchcode") String
			  branchCode,
			  @PathVariable(value = "memberid") String
			  memberid,
			  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return paymentReportServiceImpl.getOrgPaymentReport(vouchernumber,date,branchCode,memberid);
	
	}	
	
	
	
	@GetMapping("{companymstid}/paymentreport/dailypaymentsummary")
	public List<DailyPaymentSummaryReport>  getDailyPaymentSummaryReport(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
	
	
		return paymentReportService.getDailyPaymentSummaryReport(companymstid,date);
	
	}	
	
	

	@GetMapping("{companymstid}/paymentreportresource/paymentdtlreport/{branchcode}")
	public List<PaymentVoucher> getPaymentDtlReport(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchCode, @RequestParam("fdate") String fromdate,
			@RequestParam("tdate") String todate) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);

		return paymentReportService.getPaymentDtlReport(sdate, edate, companyMst.get(), branchCode);
	}
	

	@GetMapping("{companymstid}/paymentreport/dailypaymentsummarybyreceiptmode/{paymentMode}")
	public DailyPaymentSummaryReport  getDailyPaymentSummaryReportByRecieptMode(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "paymentMode") String paymentMode,
			@RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
	
		return paymentReportService.getDailyPaymentSummaryReportByRecieptMode(companymstid,date,paymentMode);
	
	}
	//-----------------version 4.14 end

	
	
	
	@GetMapping("{companymstid}/paymentreport/paymentreports/{branchcode}")
	public List<PaymentHdr> paymentHdrReportHdrs(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable (value = "branchcode") String branchcode ,
			@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate){
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
	
		
		return paymentReportService.fetchPaymnetReportHdr(companyMstOpt.get(),branchcode,fromDate,toDate);
	}
	
	
	
	@GetMapping("{companymstid}/paymentreport/{paymentId}/paymentdtl")
	
	 public List<PaymentDtl> getAllPaymentDtlByPaymentHdrId(@PathVariable (value = "paymentId") String paymenthdrId,
             Pageable pageable) {
		return paymentReportService.fetchBypaymenthdrId(paymenthdrId);
	}
	
	
	

	@GetMapping("{companymstid}/paymentreport/exportpaymentreports/{branchcode}")
	
	 public List<PaymentReports> exportPaymentReport(@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable (value = "branchcode") String branchcode ,
				@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate) {
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		
		return paymentReportService.exportPaymentReport(companyMstOpt.get(),fromDate,toDate,branchcode);
	}
	
	

	@GetMapping("{companymstid}/paymentreport/paymentreportprinting/{branchcode}")
	
	 public List<PaymentReports> paymentReportReportPrinting(@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable (value = "branchcode") String branchcode ,
				@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate) {
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		
		return paymentReportService.paymentReportReportPrinting(companyMstOpt.get(),fromDate,toDate,branchcode);
	}

	@GetMapping("{companymstid}/paymentreport/accountwisepaymentreport/{branchcode}")
	
	 public List<PaymentReports> paymentReport(@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable (value = "branchcode") String branchcode ,
				@RequestParam("fromdate") String fromdate) {
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
	
		return paymentReportService.paymentReport(companyMstOpt.get(),fromDate,branchcode);
	}
	
	
	@GetMapping("{companymstid}/paymentreport/pettycashpayment/{branchcode}")
	
	 public List<PaymentReports> getPettyCashPayment(@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable (value = "branchcode") String branchcode ,
				@RequestParam("fromdate") String fromdate) {
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
	
		return paymentReportService.pettCashpaymentReport(companyMstOpt.get(),fromDate,branchcode);
	}
	
	@Transactional
	@DeleteMapping("{companymstid}/paymentreport/deletepaymentreport/{vouchernumber}")
	public void receiptHdrDelete(@PathVariable(value = "vouchernumber") String vouchernumber) {
		
		accountingService.deleteBySourceVoucherNumber(vouchernumber);
		
		paymentHdrRepo.deleteByVoucherNumber(vouchernumber);

	}
	
	
}
