package com.maple.restserver.resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageHdr;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductConversionDtl;
import com.maple.restserver.entity.ProductConversionMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProductConversionMstRepository;
import com.maple.restserver.service.task.ProductConversionStock;
import com.maple.restserver.utils.EventBusFactory;

@RestController
@Transactional
public class ProductConversionMstResource {
@Autowired
ProductConversionMstRepository productConversionMstRepo;

@Autowired
private CompanyMstRepository companyMstRepository;
@Autowired
ProductConversionStock productConversionStock;

@Value("${serverorclient}")
private String serverorclient;

EventBus eventBus = EventBusFactory.getEventBus();

@Autowired
LmsQueueMstRepository lmsQueueMstRepository;


@PostMapping("{companymstid}/productconversionmst")
public ProductConversionMst createProduction(@Valid @RequestBody ProductConversionMst productconversionMst,
		@PathVariable(value = "companymstid") String
		  companymstid )
{
	Optional<CompanyMst> companyMstOpt = 	companyMstRepository.findById(companymstid);
	CompanyMst companyMst = companyMstOpt.get();
	productconversionMst.setCompanyMst(companyMst);

	
  ProductConversionMst saved=productConversionMstRepo.saveAndFlush(productconversionMst);
  return saved;
   
}

@GetMapping("{companymstid}/getallproductconversionmst")
public List<ProductConversionMst> retrieveAllProductionMst(){
	return productConversionMstRepo.findAll();
}

@DeleteMapping("{companymstid}/getallproductconversionmst/{id}")
public void productConversionMstDelete(@PathVariable(value = "id") String Id) {
	productConversionMstRepo.deleteById(Id);

}

@PutMapping("{companymstid}/productconversionhdr/{productconversionhdrId}/batchwiseporoductconversiondtl")
public ProductConversionMst createBatchwiseProductConversionDtl(@PathVariable(value="companymstid") String companymstid,
		@PathVariable(value = "productconversionhdrId") String productconversionhdrId,
		@Valid @RequestBody ProductConversionMst productConversionMstRequest) {

		 
	ProductConversionMst productConversionMst=productConversionMstRepo.findByIdAndCompanyMstId(productconversionhdrId,companymstid);
			
	productConversionMst.setVoucherNo(productConversionMstRequest.getVoucherNo()); 
			 
	ProductConversionMst saved = productConversionMstRepo.saveAndFlush(productConversionMst);
	
		
		productConversionStock.executebatchwise( productConversionMst.getVoucherNo(),  productConversionMst.getConversionDate(),
				productConversionMst.getCompanyMst(), productConversionMst.getId());

		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", productConversionMst.getVoucherNo());
		variables.put("voucherDate", productConversionMst.getConversionDate());
		variables.put("inet", 0);
		variables.put("id", productConversionMst.getId());
		variables.put("branchcode", productConversionMst.getBranchCode());

		variables.put("companyid", productConversionMst.getCompanyMst());
		if (serverorclient.equalsIgnoreCase("REST")) {
			variables.put("REST", 1);
		} else {
			variables.put("REST", 0);
		}

		
		variables.put("WF", "productcoversion");
		
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		
		//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("productcoversion");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		eventBus.post(variables);
		
		
		/*
		ProcessInstanceWithVariables pVariablesInReturn = runtimeService
				.createProcessInstanceByKey("productcoversion").setVariables(variables)

				.executeWithVariablesInReturn();
				*/
		

		return saved;


	}

}
