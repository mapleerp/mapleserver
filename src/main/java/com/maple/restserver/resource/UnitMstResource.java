
package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.UnitMstService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
@CrossOrigin("http://localhost:4200")
@RestController
@Transactional
public class UnitMstResource {
	private static final Logger logger = LoggerFactory.getLogger(UnitMstResource.class);

	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	private UnitMstRepository unitRepository;

	@Autowired
	private UnitMstService unitMstService;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	@Autowired
	private ExternalApi externalApi;
	Pageable topFifty =   PageRequest.of(0, 50);

	@GetMapping("{companymstid}/unitmst")
	public List<UnitMst> retrieveAllSupplier(@PathVariable(value = "companymstid") String companymstid) {
		return unitRepository.findByCompanyMstId(companymstid);
	}

	@GetMapping("{companymstid}/unitmst/{unitid}/unitmst")
	public Optional<UnitMst> retrievUnitMstById(@PathVariable String unitid,
			@PathVariable(value = "companymstid") String companymstid) {
		return unitRepository.findById(unitid);
	}

	@Autowired
	CompanyMstRepository companyMstRepo;

	@GetMapping("{companymstid}/unitmst/{unitname}")
	public UnitMst retrievUnitMstByUnitName(@PathVariable(value = "unitname") String unitname,
			@PathVariable(value = "companymstid") String companymstid) {
		return unitRepository.findByUnitNameAndCompanyMstId(unitname, companymstid);
	}

	@PostMapping("{companymstid}/unitmst")
	public UnitMst createsupplier(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody UnitMst unitMst1) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);

		CompanyMst companyMst = companyMstOpt.get();
		unitMst1.setCompanyMst(companyMst);
//		unitMst1 = unitRepository.saveAndFlush(unitMst1);
		unitMst1=saveAndPublishService.saveUnitMst(unitMst1, unitMst1.getBranchCode());
		logger.info("unitMst1 send to KafkaEvent: {}", unitMst1);
		
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", unitMst1.getId());

		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", unitMst1.getId());

		variables.put("companyid", unitMst1.getCompanyMst());
		variables.put("branchcode", unitMst1.getBranchCode());

		variables.put("REST", 1);

		variables.put("WF", "forwardUnitMst");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));

		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardUnitMst");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);

		variables.put("lmsqid", lmsQueueMst.getId());

		eventBus.post(variables);

		return unitMst1;

		/*
		 * wf . forwardUnit
		 */
	}

	@GetMapping("{companymstid}/unitmst/initializeunitmst/{branchcode}")
	public String initializeUnitMst(@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "companymstid") String companymstid) {
		return unitMstService.initializeUnitMst(branchcode, companymstid);
	}

	

	// ------------------new url ----publish offline button

	@PostMapping("{companymstid}/unitmst/{status}")
	public UnitMst sendOffLineMessageToCloud(@Valid @RequestBody UnitMst unitMst1,
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("status") String status) {
		 ResponseEntity<UnitMst> saved=	externalApi.sendunitMstOffLineMessageToCloud(companymstid, unitMst1);
			
		return saved.getBody();

	}
	
}
