package com.maple.restserver.resource;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.DynamicProductionDtl;
import com.maple.restserver.entity.DynamicProductionHdr;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DynamicProductionDtlRepository;
import com.maple.restserver.repository.DynamicProductionHdrRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.ItemStockPopUpRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.service.DynamicProductionService;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.service.MultiUnitConversionServiceImpl;
import com.maple.restserver.service.kitdefinionservice;

@RestController
@Transactional
public class DynamicProductionHdrResource {
	boolean recurssionOff = false;
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	@Autowired
	MultiUnitConversionServiceImpl multiUnitConversionServiceImpl;
	@Autowired
	ItemBatchMstRepository itemBatchMstRepo;
	@Autowired
	kitdefinionservice kitDefinitionService;
	@Autowired
	ItemMstRepository itemMstrepo;
	@Autowired
	private ItemStockPopUpRepository itemStockPopUpRepository;
	@Autowired
	DynamicProductionService dynamicProductionService;
	@Autowired
	DynamicProductionDtlRepository dynamicProductioDtlrepo;
	@Autowired
	DynamicProductionHdrRepository dynamicProductionHdrrepo;
	
	@Autowired
	KitDefinitionMstRepository kitDefinitionMstRepo;
	@Autowired
	CompanyMstRepository companyMstRepo;
	Pageable topFifty =   PageRequest.of(0, 50);
	@PostMapping("{companymstid}/dynamicproductionhdr/savedynamicproduction")
	public DynamicProductionHdr saveDynamicProduction(@PathVariable(value = "companymstid")String companymstid,
			@RequestBody DynamicProductionHdr dynamicProductionHdr)
	{
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			dynamicProductionHdr.setCompanyMst(companyMst);
			return dynamicProductionHdrrepo.save(dynamicProductionHdr);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}
	
	@PutMapping("{companymstid}/dynamicproductionhdr/updatedynamicproduction")
	public void updateDynamicProduction(@PathVariable(value = "companymstid")String companymstid,
			@RequestBody DynamicProductionHdr dynamicProductionreq)
	{
		Optional<DynamicProductionHdr> dynamicProductionHdrop = dynamicProductionHdrrepo.findById(dynamicProductionreq.getId());
		if(dynamicProductionHdrop.isPresent())
		{
			DynamicProductionHdr dynamicProductionHdr = dynamicProductionHdrop.get();
			dynamicProductionHdr.setVoucherNumber(dynamicProductionreq.getVoucherNumber());
			dynamicProductionHdr.setVoucherDate(dynamicProductionreq.getVoucherDate());
			dynamicProductionHdr = dynamicProductionHdrrepo.save(dynamicProductionHdr);
			stockUpdation(dynamicProductionHdr);
			
		}
		
	}

	private void stockUpdation(DynamicProductionHdr dynamicProductionHdr) {
		List<DynamicProductionDtl> dynamicProdDtllist = dynamicProductioDtlrepo.findByDynamicProductionHdrId(dynamicProductionHdr.getId());
		Iterator iter = dynamicProdDtllist.iterator();
		while (iter.hasNext()) {

			DynamicProductionDtl dynamicProductionDtl = (DynamicProductionDtl) iter.next();
			// Check stock from itembatchDtl

			ItemMst itemmst = itemMstrepo.findById(dynamicProductionDtl.getItemId()).get();
//			
			KitDefinitionMst kitDefinitionMst = null;
			List<KitDefinitionMst> kitDefinitionMstList = kitDefinitionMstRepo.findByItemId(dynamicProductionDtl.getItemId());
			if (kitDefinitionMstList.size() > 0) {
				kitDefinitionMst = kitDefinitionMstList.get(0);
			}

			if (null != kitDefinitionMst) {

				List<Object> searchItemStockByName = itemStockPopUpRepository
						.findSearchbyItemName(itemmst.getItemName(), dynamicProductionDtl.getBatch(), topFifty);
				Double chkQty = 0.0;
				for (int i = 0; i < searchItemStockByName.size(); i++) {

					Object[] objArray = (Object[]) searchItemStockByName.get(i);
					chkQty = (Double) objArray[4];

				}
				if (chkQty < 1 || chkQty < dynamicProductionDtl.getQty()) {
					Double diffQty = null;
					if (chkQty < dynamicProductionDtl.getQty()) {
						diffQty = dynamicProductionDtl.getQty() + chkQty;
					} else {
						diffQty = dynamicProductionDtl.getQty();
					}
					kitDefinitionService.runTimeProduction(dynamicProductionDtl.getItemId(), dynamicProductionDtl.getBatch(), itemmst.getBarCode(), dynamicProductionHdr.getCompanyMst(), dynamicProductionDtl.getId(), dynamicProductionHdr.getId(), dynamicProductionHdr.getVoucherNumber(), dynamicProductionHdr.getVoucherDate(), dynamicProductionHdr.getBranchCode(), diffQty);
					kitDefinitionService.executebatchwiserowmaterialupdate(dynamicProductionHdr.getVoucherNumber(), dynamicProductionHdr.getVoucherDate(), dynamicProductionHdr.getCompanyMst(), itemmst.getItemName(),dynamicProductionDtl.getItemId(), dynamicProductionDtl.getBatch(), dynamicProductionDtl.getQty(),  dynamicProductionHdr.getBranchCode(), dynamicProductionHdr.getId(), dynamicProductionDtl.getId());
				}
			}
			else
			{
				Optional<ItemMst> itemOpt = itemMstrepo.findById(dynamicProductionDtl.getItemId());
				if (!itemOpt.isPresent()) {
					return;
				}

				ItemMst item = itemOpt.get();
				if (null != item.getServiceOrGoods()) {
					if (!item.getServiceOrGoods().equalsIgnoreCase("SERVICE ITEM")) {
						// ----this code execute for service item
						List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
								.findByItemIdAndBatchAndBarcode(dynamicProductionDtl.getItemId(), dynamicProductionDtl.getBatch(),
										item.getBarCode());
						double conversionQty = 0.0;
						ItemBatchMst itemBatchMst = null;
						if (itembatchmst.size() == 1) {
							itemBatchMst = itembatchmst.get(0);
							if (!item.getUnitId().equalsIgnoreCase(dynamicProductionDtl.getUnitId())) {

								String itemId = item.getId();
								String sourceUnit = dynamicProductionDtl.getUnitId();
								String targetUnit = item.getUnitId();

								recurssionOff = false;
								conversionQty = multiUnitConversionServiceImpl.getConvertionQty(dynamicProductionHdr.getCompanyMst().getId(), item.getId(),
										sourceUnit, targetUnit, dynamicProductionDtl.getQty());
								itemBatchMst.setQty(itemBatchMst.getQty() + conversionQty);
							} else {
								itemBatchMst.setQty(itemBatchMst.getQty() - dynamicProductionDtl.getQty());
							}

							// itemBatchMst.setMrp(salesDtl.getMrp());
							// itemBatchMst.setQty(itemBatchMst.getQty() - salesDtl.getQty());
							itemBatchMst.setBranchCode(dynamicProductionHdr.getBranchCode());
							itemBatchMstRepo.saveAndFlush(itemBatchMst);
						} else {
							itemBatchMst = new ItemBatchMst();
							itemBatchMst.setBarcode(item.getBarCode());
							itemBatchMst.setBatch(dynamicProductionDtl.getBatch());
							itemBatchMst.setMrp(item.getStandardPrice());
							itemBatchMst.setQty(dynamicProductionDtl.getQty());
							itemBatchMst.setItemId(dynamicProductionDtl.getItemId());
							itemBatchMst.setBranchCode(dynamicProductionHdr.getBranchCode());
							itemBatchMst.setCompanyMst(dynamicProductionHdr.getCompanyMst());
							itemBatchMstRepo.saveAndFlush(itemBatchMst);

						}
						// ----------------version 5.0 surya

//						itemBatchDtlService.SalesStockUpdation(item,salesDtl,salesTransHdr,itemBatchMst);
						String particular = "DYNAMIC PRODUCTION";
						itemBatchDtlService.ItemBatchDtlUpdation(item.getId(), dynamicProductionHdr.getId(), dynamicProductionDtl.getId(), 0.0,
								dynamicProductionDtl.getQty(),item.getStandardPrice(), dynamicProductionDtl.getUnitId(), itemBatchMst,
								dynamicProductionHdr.getVoucherDate(), dynamicProductionHdr.getVoucherNumber(), particular,
								dynamicProductionHdr.getCompanyMst(), dynamicProductionDtl.getBatch(), dynamicProductionHdr.getBranchCode(), "MAIN");
					}
				}
			}
		}
	}
		
		
}
