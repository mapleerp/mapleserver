 package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductConversionDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProductConversionDtlRepository;
import com.maple.restserver.repository.ProductConversionMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.task.ProductConversionStock;
import com.maple.restserver.utils.EventBusFactory;

@RestController
@Transactional
public class ProductConversionDtlResource {
	
private static final Logger logger = LoggerFactory.getLogger(ProductConversionDtlResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	@Value("${serverorclient}")
	private String serverorclient;

	//@Autowired
	//private RuntimeService runtimeService;

	EventBus eventBus = EventBusFactory.getEventBus();
	 
	
	@Autowired
	ProductConversionDtlRepository productConversionDtlRepo;

	@Autowired
	ProductConversionMstRepository productconversionMstRepo;

	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	ProductConversionStock productConversionStock;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	/*
	 * @PostMapping(
	 * "/productconversionmst/{productconversionId}/productconversiondtl") public
	 * ProductConversionMst makeProductConversion(@PathVariable (value =
	 * "productconversionId") String productconversionId,
	 * 
	 * @Valid @RequestBody ProductConversionDtl productconversiondtlRequest) {
	 * return paymentHdrRepo.findById(productconversionId).map(paymenthdr -> {
	 * paymentdtlRequest.setPaymenthdr(paymenthdr); return
	 * paymentDtlRepo.save(paymentdtlRequest); }).orElseThrow(() -> new
	 * ResourceNotFoundException("paymentId 1" + paymentId + " not found")); }
	 */

	@PostMapping("{companymstid}/productconversionhdr/{productconversionhdrId}/poroductconversiondtl")
	public ProductConversionDtl createProductConversionDtl(
			@PathVariable(value = "productconversionhdrId") String productconversionhdrId,
			@Valid @RequestBody ProductConversionDtl productconversionDtlRequest) {

		return productconversionMstRepo.findById(productconversionhdrId).map(productConversionMst -> {
			productconversionDtlRequest.setProductConversionMst(productConversionMst);
		//	ProductConversionDtl saved =productConversionDtlRepo.saveAndFlush(productconversionDtlRequest);
			ProductConversionDtl saved =saveAndPublishService.saveProductConversionDtl(productconversionDtlRequest, mybranch);
			logger.info("ProductConversionDtl send to KafkaEvent: {}", saved);

			return saved;

		}).orElseThrow(
				() -> new ResourceNotFoundException("productcoversion " + productconversionhdrId + " not found"));

	}
	@GetMapping("{companymstid}/productconversiondtl/{producthdrid}/{fromitemId}/{batchcode}/getproductconversiondetailqty")
	public Double retrieveAllProductConversionQtyById(@PathVariable(value = "producthdrid") String producthdrid,
			@PathVariable(value = "fromitemId") String fromitemId,@PathVariable(value = "batchcode") String batchcode,

			@PathVariable(value = "companymstid") String companymstid) {

		// return
		// salesDetailsService.getSalesDetailItemQty(companymstid,salesTransHdrID,itemId,batch);
		return productConversionDtlRepo.getProductConversionDetailItemQty(producthdrid, fromitemId,batchcode);
	}

	

	@DeleteMapping("{companymstid}/productconversiondtl/{id}")
	public void productConversionDelete(@PathVariable(value = "id") String Id) {
		productConversionDtlRepo.deleteById(Id);

	}

	@GetMapping("{companymstid}/productconversionbyid/{id}")
	public @ResponseBody List<ProductConversionDtl> productConversionDtlbyId(@PathVariable(value = "id") String id) {
		return productConversionDtlRepo.findByProductConversionMstId(id);
	}

	
	
//	
//	@PostMapping("{companymstid}/productconversionhdr/{productconversionhdrId}/batchwiseporoductconversiondtl")
//	public ProductConversionDtl createBatchwiseProductConversionDtl(
//			@PathVariable(value = "productconversionhdrId") String productconversionhdrId,
//			@Valid @RequestBody ProductConversionDtl productconversionDtlRequest) {
//
//		return productconversionMstRepo.findById(productconversionhdrId).map(productConversionMst -> {
//			productconversionDtlRequest.setProductConversionMst(productConversionMst);
//
//			ProductConversionDtl saved = productConversionDtlRepo.saveAndFlush(productconversionDtlRequest);
//
//			/*
//			 * Init W
//			 */
//			
//			productConversionStock.executebatchwise( productConversionMst.getVoucherNo(),  productConversionMst.getConversionDate(),
//					productConversionMst.getCompanyMst(), productConversionMst.getId());
//
//			Map<String, Object> variables = new HashMap<String, Object>();
//
//			variables.put("voucherNumber", productConversionMst.getVoucherNo());
//			variables.put("voucherDate", productConversionMst.getConversionDate());
//			variables.put("inet", 0);
//			variables.put("id", productConversionMst.getId());
//			variables.put("branchcode", productConversionMst.getCompanyMst().getId());
//
//			variables.put("companyid", productConversionMst.getCompanyMst());
//			if (serverorclient.equalsIgnoreCase("REST")) {
//				variables.put("REST", 1);
//			} else {
//				variables.put("REST", 0);
//			}
//
//			
//			variables.put("WF", "productcoversion");
//			
//			String workflow = (String) variables.get("WF");
//			String voucherNumber = (String) variables.get("voucherNumber");
//			String sourceID = (String) variables.get("id");
//
//
//			LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//			lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//			lmsQueueMst.setVoucherType(workflow);
//			lmsQueueMst.setPostedToServer("NO");
//			lmsQueueMst.setJobClass("productcoversion");
//			lmsQueueMst.setCronJob(true);
//			lmsQueueMst.setJobName(workflow + sourceID);
//			lmsQueueMst.setJobGroup(workflow);
//			lmsQueueMst.setRepeatTime(60000L);
//			lmsQueueMst.setSourceObjectId(sourceID);
//			
//			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//			lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//			eventBus.post(variables);
//			
//			
//			/*
//			ProcessInstanceWithVariables pVariablesInReturn = runtimeService
//					.createProcessInstanceByKey("productcoversion").setVariables(variables)
//
//					.executeWithVariablesInReturn();
//					*/
//			
//
//			return saved;
//
//		}).orElseThrow(
//				() -> new ResourceNotFoundException("productcoversion " + productconversionhdrId + " not found"));
//
//	}
}
