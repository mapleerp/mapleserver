package com.maple.restserver.resource;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ConsumptionDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.ConsumptionDtlRepository;
import com.maple.restserver.repository.ConsumptionHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Transactional
public class ConsumptionDtlResource {
	private static final Logger logger = LoggerFactory.getLogger(ConsumptionDtlResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	ConsumptionHdrRepository consumptionHdrRepo;
	
	@Autowired
	ConsumptionDtlRepository consumptionDtlRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@PostMapping("{companymstid}/consumptiondtl/{hdrid}/consumptiondtlsave")
	public ConsumptionDtl createConsumptionDtl(@PathVariable (value = "hdrid") String hdrid,
            @Valid @RequestBody ConsumptionDtl consumptionDtlRequest) 
	{
		 
		
		 return consumptionHdrRepo.findById(hdrid).map(consumptionhdr -> {
			 
			 
			 consumptionDtlRequest.setConsumptionHdr(consumptionhdr);
			 
//			 ConsumptionDtl saved = consumptionDtlRepo.saveAndFlush(consumptionDtlRequest);
			 ConsumptionDtl saved =saveAndPublishService.saveConsumptionDtl(consumptionDtlRequest,mybranch);
			 logger.info("ConsumptionDtl send to KafkaEvent: {}", saved);
	
	
		 return saved;
    }).orElseThrow(() -> new ResourceNotFoundException("salestranshdrId " + hdrid + " not found"));
	}
	
	@GetMapping("{companymstid}/consumptiondtl/getconsumptiondtlbyhdr/{hdrid}")
	public List<ConsumptionDtl> getConsumptionDtlById(@PathVariable (value = "hdrid") String hdrid)
	{
		return consumptionDtlRepo.findByConsumptionHdrId(hdrid);
	}
	
	@DeleteMapping("{companymstid}/consumptiondtl/deletebydtlid/{dtlid}")
	public void deleteConsumptionDtl(@PathVariable (value = "dtlid") String dtlid)
	{
		consumptionDtlRepo.deleteById(dtlid);
	}
}
