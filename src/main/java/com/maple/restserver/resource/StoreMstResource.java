package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.SiteMst;
import com.maple.restserver.entity.StoreMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.StoreMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class StoreMstResource {

	private static final Logger logger = LoggerFactory.getLogger(PurchaseHdrResource.class);
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	StoreMstRepository storeMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	private VoucherNumberService voucherService;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@PostMapping("{companymstid}/storemstresource/storemst")
	public StoreMst createStoreMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody StoreMst storeMst) {
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		storeMst.setCompanyMst(companyMst.get());
//		storeMst = storeMstRepository.saveAndFlush(storeMst);
		storeMst=saveAndPublishService.saveStoreMst(storeMst, storeMst.getBranchCode());
		logger.info("storeMst send to KafkaEvent: {}", storeMst);

		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", storeMst.getId());

		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", storeMst.getId());

		variables.put("companyid", storeMst.getCompanyMst());
		variables.put("branchcode", storeMst.getBranchCode());

		variables.put("REST", 1);

		variables.put("WF", "forwardStoreMst");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

		// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));

		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardStoreMst");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);

		return storeMst;

	}

	@GetMapping("{companymstid}/storemstresource/storemstbycompanymst")
	public List<StoreMst> storeMstByCompanyMst(@PathVariable(value = "companymstid") String companymstid) {
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		return storeMstRepository.findByCompanyMst(companyMstOpt.get());

	}

	@DeleteMapping("{companymstid}/storemstresource/storemstdelete/{id}")
	public void StoreMstDelete(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String Id) {
		storeMstRepository.deleteById(Id);

	}

	@GetMapping("{companymstid}/storemstresource/storemstbycompanymstandname/{name}")
	public List<StoreMst> storeMstByCompanyMstAndName(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "name") String name) {
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		return storeMstRepository.findByCompanyMstAndName(companyMstOpt.get(), name);

	}

	@GetMapping("{companymstid}/storemstresource/initializestoremst/{branchcode}")
	public String storeMstInitialization(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);

		List<StoreMst> storeMstList = storeMstRepository.findByCompanyMstAndName(companyMstOpt.get(), "MAIN");
		if (storeMstList.size() == 0) {
			StoreMst storeMst = new StoreMst();

			VoucherNumber voucherNo = voucherService.generateInvoice(branchcode, "STR");
			storeMst.setId(voucherNo.getCode());

			storeMst.setBranchCode(branchcode);
			storeMst.setCompanyMst(companyMstOpt.get());
			storeMst.setMobile("NO");
			storeMst.setName("MAIN");
			storeMst.setShortCode("MAIN");
			storeMst = storeMstRepository.saveAndFlush(storeMst);

		}

		return "Success";
	}
}
