package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.DailySalesReport;
import com.maple.restserver.report.entity.VoucherReprintDtl;
import com.maple.restserver.service.VoucherReprintService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class VoucherReprintResource {

	@Autowired
	VoucherReprintService voucherReprintService;

	@GetMapping("{companymstid}/voucherreprint/{vouchernumber}/{branchcode}")
	public List<VoucherReprintDtl> retriveVoucherReprint(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "vouchernumber") String vouchernumber,
			@PathVariable(value = "branchcode") String branchcode) {
		// java.util.Date date =
		// SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return voucherReprintService.getVoucherReprint(companymstid, vouchernumber, branchcode);

	}

	@GetMapping("{companymstid}/voucherreprint/salesreturn/{vouchernumber}/{branchcode}")
	public List<VoucherReprintDtl> retriveSalesReturnVoucherReprint(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "vouchernumber") String vouchernumber,
			@PathVariable(value = "branchcode") String branchcode) {
		// java.util.Date date =
		// SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return voucherReprintService.getSalesReturnVoucherReprint(companymstid, vouchernumber, branchcode);

	}
	
	
	
	@GetMapping("{companymstid}/voucherreprintbydate/{vouchernumber}/{branchcode}")
	public List<VoucherReprintDtl> retriveVoucherReprintByDate(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "vouchernumber") String vouchernumber,
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam("date") String fdate

			) {
		 java.util.Date date = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");

		return voucherReprintService.getVoucherReprint(companymstid, vouchernumber, branchcode,date);

	}

}
