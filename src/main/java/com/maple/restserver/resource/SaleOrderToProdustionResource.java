package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesOrderDtl;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesOrderDtlRepository;
import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.service.SaleOrderToProductionService;

@RestController
public class SaleOrderToProdustionResource {

	@Autowired
	CompanyMstRepository companyMstRepository;
	
	
	@Autowired
	SaleOrderToProductionService saleOrderToProductionService;
	
	@Autowired
	SalesOrderTransHdrRepository SalesOrderTransHdrRepository;
	
	@Autowired
	SalesOrderDtlRepository salesOrderDtlRepository;
	
	

	
	@GetMapping("{companymstid}/saleordertoproductionresource/{branchCode}/saleordertoproduction")
	public List<SalesOrderTransHdr> fetchSalesOrderTransHdr(@PathVariable(value = "companymstid") String
			  companymstid,
			 @PathVariable("branchCode") String branchCode,  @RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate){
		java.util.Date fromDate = ClientSystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
       java.util.Date toDate=ClientSystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		Optional<CompanyMst> companyMst=companyMstRepository.findById(companymstid);
		
		
		
		return SalesOrderTransHdrRepository.fetchSalesOrderTransHdr(companymstid,branchCode,fromDate,toDate);
	

	}
	
	

	@GetMapping("{companymstid}/saleordertoproductionresource/{salesordertranshdrId}/fetchsalesorderdtl")
	public List<SalesOrderDtl> retrieveAllSalesDtlBySalesOredrTransHdrId(@PathVariable (value = "salestranshdrId") String salesTransHdrID,
            Pageable pageable) {
		return salesOrderDtlRepository.findBySalesOrderTransHdrId(salesTransHdrID,pageable);
	}
	
	
	
	
	
	@GetMapping("{companymstid}/saleordertoproductionresource/{branchCode}/convertsaleordertoproduction")
	public String convertSaleOrderToProduction(@PathVariable (value = "companymstid") String companymstid,@PathVariable (value = "branchCode") String branchCode,
            Pageable pageable) {
		
		
		
		return saleOrderToProductionService.convertSaleOrderToProduction( companymstid,branchCode);
	}
	
	
}
