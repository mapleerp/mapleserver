package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.MachineResourceMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.message.entity.SalesTransHdrMessage;
import com.maple.restserver.report.entity.KitValidationReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.MachineResourceMstRepository;
import com.maple.restserver.service.MachineResourceService;
import com.maple.restserver.service.kitdefinionservice;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class MobileTabKotClearResource {
 
	@Autowired
	CompanyMstRepository companyMstRepository;

 

 
	@GetMapping("{companymstid}/{branchcode}/mobiletabkotclear/{tabid}/{tableid}")
	public String fetchSaleOrderProductQty(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, 
			@PathVariable(value = "tabid") String tabid, 
			@PathVariable(value = "tableid") String tableid 
			 )throws  ResourceNotFoundException

	{
	 
		SalesTransHdrMessage salesTransHdrMessage = new SalesTransHdrMessage();
		salesTransHdrMessage.setServingTableName(tableid);
		 /*
		  * This method is not implemeted fully
		  */
		
		
	
	return "Success";
	}
	
	
 

}
