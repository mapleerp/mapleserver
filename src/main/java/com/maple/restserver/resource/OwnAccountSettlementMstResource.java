package com.maple.restserver.resource;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.OwnAccountSettlementMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.OwnAccountSettlementMstRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Transactional
public class OwnAccountSettlementMstResource {
	private static final Logger logger = LoggerFactory.getLogger(OwnAccountSettlementMstResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	
	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	OwnAccountSettlementMstRepository 	ownAccountSettlementMstRepository;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@PostMapping("{companymstid}/ownaccountsettlementmst")
	
	public OwnAccountSettlementMst createOwnAccountSettlementMst(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 OwnAccountSettlementMst ownaccountsettlementMst)
	{
		
		Optional<CompanyMst> companyMst= companyMstRepository.findById(companymstid);
			ownaccountsettlementMst.setCompanyMst(companyMst.get());
      // return ownAccountSettlementMstRepository.saveAndFlush(ownaccountsettlementMst);
			return saveAndPublishService.saveOwnAccountSettlementMst(ownaccountsettlementMst, mybranch);
		 
		}
		
	
	
}
