
package com.maple.restserver.resource;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.StockValueApr;
import com.maple.restserver.entity.UserGroupMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.StockValueAprRepository;



@RestController
@Transactional
public class StockValueAprResource {
	@Autowired
	private StockValueAprRepository stockValueApr;
	@Autowired
	CompanyMstRepository companyMstRepo;
	@GetMapping("{companymstid}/stockvalue")
	public List<StockValueApr> retrieveAllcash(@PathVariable(value = "companymstid") String
			  companymstid)
	{
		return stockValueApr.findByCompanyMst(companymstid);
	}
	


	
	
	
	@PostMapping("/{companymstid}/stockvalue")
	public StockValueApr createUser(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody StockValueApr receivablehdr1)
	{

		 return companyMstRepo.findById(companymstid).map(companyMst-> {
			 receivablehdr1.setCompanyMst(companyMst);
					 return	stockValueApr.saveAndFlush(receivablehdr1);

 
			 }).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
					  companymstid + " not found")); } 


	

}

