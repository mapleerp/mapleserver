package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ProfitAndLossExpense;
import com.maple.restserver.entity.ProfitAndLossIncome;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ProfitAndLossIncomeRepository;

@RestController
@Transactional
public class ProfitAndLossIncomeResource {
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	ProfitAndLossIncomeRepository  profitAndLossIncomeRepository;
	
	//save
		@PostMapping("{companymstid}/profitandlossincomeresource/saveprofitandlossincome")
		public ProfitAndLossIncome createProfitAndLossIncome(@PathVariable(value = "companymstid") String
				 companymstid,@Valid @RequestBody 
				 ProfitAndLossIncome profitAndLossIncome)
	    {
			
			Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
			profitAndLossIncome.setCompanyMst(companyMst.get());
	       return profitAndLossIncomeRepository.saveAndFlush(profitAndLossIncome);
			 
		}
		//showing all 
		@GetMapping("{companymstid}/profitandlossincomeresource/getallprofitandlossincome")
		public List<ProfitAndLossIncome> getAllProfitAndLossIncome(
				@PathVariable(value = "companymstid") String companymstid) {
			Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
			
			return profitAndLossIncomeRepository.findByCompanyMst(companyMst.get());
		}
		
		@DeleteMapping("{companymstid}/profitandlossincomeresource/deleteprofitandlossincome/{id}")
		public void DeleteProfitAndLossIncome(@PathVariable (value="id") String id) {
			profitAndLossIncomeRepository.deleteById(id);

		}
	
	
	
}
