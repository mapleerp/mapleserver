package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.entity.InsuranceCompanyDtl;
import com.maple.restserver.entity.InsuranceCompanyMst;
import com.maple.restserver.entity.ItemNutrition;
import com.maple.restserver.entity.NutritionValueMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;
import com.maple.restserver.repository.InsuranceCompanyDtlRepository;
import com.maple.restserver.repository.InsuranceCompanyRepository;

@RestController
@Transactional
public class InsuranceCompanyDtlResource {
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	InsuranceCompanyDtlRepository insuranceCompanyDtlRepository;
	@Autowired
	AccountHeadsRepository accRepo;
	@Autowired
	CurrencyMstRepository currencyMstRepo;
	
	@GetMapping("{companymstid}/insurancecompanydtl/getinsurancecompanydtlbypolicytype")
	public InsuranceCompanyDtl getInsuranceCompMstByPolicyType(
			@PathVariable(value = "companymstid")String companymstid,
			@RequestParam(value = "policytype")String policytype)
	{
		return insuranceCompanyDtlRepository.findByCompanyMstIdAndPolicyType(companymstid,policytype);
	}
	@GetMapping("{companymstid}/insurancecompanydtl/findall")
	public List<InsuranceCompanyDtl> findAllInsuranceCompdtl(
			@PathVariable(value = "companymstid")String companymstid)
	{
		return insuranceCompanyDtlRepository.findAll();
	}
	
	@GetMapping("{companymstid}/insurancecompanydtl/getinsurancecompdtlbyid/{insurancecompanydtlid}")
	public InsuranceCompanyDtl getInsuranceCompanyDtlById(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "insurancecompanydtlid")String insurancecompanydtlid)
	{
		return insuranceCompanyDtlRepository.findByCompanyMstIdAndId(companymstid,insurancecompanydtlid);
	}
	@PutMapping("{companymstid}/insurancecompanydtl/updateinsurancecompdtlbyid/{insurancedtlid}")
	public InsuranceCompanyDtl updateInsuranceCompDtlById(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "insurancedtlid")String insurancedtlid,
			@Valid @RequestBody InsuranceCompanyDtl  insuranceCompanyDtlRequest)
	{
		InsuranceCompanyDtl insuranceCompdtl = insuranceCompanyDtlRepository.findByCompanyMstIdAndId(companymstid, insurancedtlid);
		
	insuranceCompdtl.setPercentageCoverage(insuranceCompanyDtlRequest.getPercentageCoverage());
	
	insuranceCompdtl.setPolicyType(insuranceCompanyDtlRequest.getPolicyType());
	insuranceCompdtl = insuranceCompanyDtlRepository.save(insuranceCompdtl);
		Optional<AccountHeads> accByIdop = accRepo.findById(insuranceCompdtl.getId());
		if(accByIdop.isPresent())
		{
			AccountHeads accountHeads = accByIdop.get();
			accountHeads.setAccountName(insuranceCompdtl.getPolicyType());
			accountHeads.setCompanyMst(insuranceCompdtl.getCompanyMst());
			CurrencyMst currencyMst = currencyMstRepo.findByCurrencyName(insuranceCompdtl.getCompanyMst().getCurrencyName());
			if(null != currencyMst)
			{
				accountHeads.setCurrencyId(currencyMst.getId());
			}
			accountHeads.setId(insuranceCompdtl.getId());
			accRepo.save(accountHeads);
		}
		else
		{
			AccountHeads accountHeads = new AccountHeads();
			accountHeads.setAccountName(insuranceCompdtl.getPolicyType());
			accountHeads.setCompanyMst(insuranceCompdtl.getCompanyMst());
			CurrencyMst currencyMst = currencyMstRepo.findByCurrencyName(insuranceCompdtl.getCompanyMst().getCurrencyName());
			if(null != currencyMst)
			{
				accountHeads.setCurrencyId(currencyMst.getId());
			}
			accountHeads.setId(insuranceCompdtl.getId());
			accRepo.save(accountHeads);
		}
		
		return insuranceCompdtl;
	}
	
	///sharon (this url not working properly)===========
//	@PostMapping("{companymstid}/insurancecompanydtl/saveinsurancecompanydtl")
//	public InsuranceCompanyDtl createInsuranceCompanyDtl(@PathVariable(value = "companymstid") String
//			 companymstid,@Valid @RequestBody 
//			 InsuranceCompanyDtl  insuranceCompanyDtl)
//		{
//			
//			
//				Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
//				
//				CompanyMst companyMst = companyMstOpt.get();
//				insuranceCompanyDtl.setCompanyMst(companyMst);
//				
//				InsuranceCompanyDtl saved=insuranceCompanyDtlRepository.save(insuranceCompanyDtl);
//				AccountHeads accountHeads = new AccountHeads();
//				accountHeads.setAccountName(saved.getPolicyType());
//				accountHeads.setCompanyMst(saved.getCompanyMst());
//				CurrencyMst currencyMst = currencyMstRepo.findByCurrencyName(saved.getCompanyMst().getCurrencyName());
//				if(null != currencyMst)
//				{
//					accountHeads.setCurrencyId(currencyMst.getId());
//				}
//				accountHeads.setId(saved.getId());
//				accRepo.save(accountHeads);
//				return saved;
//			} 
//	
	
	@DeleteMapping("{companymstid}/insurancecompanydtl/deleteinsurancecompdtlbyid/{insurancedtlid}")
	public void deleteInsuranceCompDtlById(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "insurancedtlid")String insurancedtlid)
	{
		insuranceCompanyDtlRepository.deleteById(insurancedtlid);
	}
	
	// delete
		@DeleteMapping("{companymstid}/insurancecompanydtl/deleteinsurancecompanydtl/{id}")
		public void DeleteInsuranceCompDtl(@PathVariable(value = "id") String Id) {
			insuranceCompanyDtlRepository.deleteById(Id);
		}
	
	
	
	
	

	//save 
	@PostMapping("{companymstid}/insurancecompanydtlresource/saveinsurancecompanydtl")
	public InsuranceCompanyDtl createInsuranceCompanyDtl(
			@PathVariable(value="companymstid")	String commpanymstid,
			@Valid @RequestBody InsuranceCompanyDtl insuranceCompanydtl)
	{
		CompanyMst companymst=companyMstRepo.findById(commpanymstid).get();
		insuranceCompanydtl.setCompanyMst(companymst);
		insuranceCompanydtl=insuranceCompanyDtlRepository.save(insuranceCompanydtl);
		return insuranceCompanydtl;
	}
	
	
	
	

	//showing all 
	@GetMapping("{companymstid}/insurancecompanydtlresource/showallinsurancecompanydtl")
	public List<InsuranceCompanyDtl> showallInsuranceCompanyDtl(
			
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		
		return insuranceCompanyDtlRepository.findByCompanyMst(companyMstOpt.get());
	}
	
	
	
	
	
	
	
	
	@GetMapping("{companymstid}/insurancecompanydtlresource/{insurancecompanymstid}/getinsurancecompanydtl")
	public InsuranceCompanyDtl retrieveInsuranceCompanyDtlByInsuranceCompanyMst(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "insurancecompanymstid") String insurancecompanymstid) {
		return insuranceCompanyDtlRepository.findByCompanyMstIdAndInsuranceCompanyMst(companymstid,insurancecompanymstid );

	}
	
	@GetMapping("{companymstid}/insurancecompanydtlresource/insurancecompanydtlbyid/{id}")
	public InsuranceCompanyDtl retrieveAllInsuranceCompanyDtlId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		if(!companyOpt.isPresent())
		{
			return null;
		}
		return insuranceCompanyDtlRepository.findByCompanyMstAndId(companyOpt.get(),id);
	}
	
	
	@GetMapping("{companymstid}/insurancecompanydtlresource/insurancedtlbycompanymst/{hdrid}")
	public List<InsuranceCompanyDtl> findAllInsuranceCompdtlByCompanyId(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "hdrid")String hdrid)
	{
		return insuranceCompanyDtlRepository.findByInsuranceCompanyMstId(hdrid);
	}
	
	
	
}
