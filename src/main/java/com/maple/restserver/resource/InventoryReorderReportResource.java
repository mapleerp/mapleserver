package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.DailySalesSummaryReport;
import com.maple.restserver.report.entity.InventoryReorderStatusReport;
import com.maple.restserver.service.InventoryReorderstatusReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class InventoryReorderReportResource {

	@Autowired
	InventoryReorderstatusReportService inventoryReorderstatusReportService;

	@GetMapping("{companymstid}/dainuteused/{branchCode}")
	public List<InventoryReorderStatusReport> dailySalesDtlnotUsed(@PathVariable(value = "companymstid") String companymstid,

			@PathVariable("branchCode") String branchCode, @RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate) {
		java.util.Date date = SystemSetting.StringToUtilDate(startDate, "dd/MM/yyyy");

		return inventoryReorderstatusReportService.getInventoryReorderStatusReport(startDate, endDate, branchCode,
				companymstid);

	}

}
