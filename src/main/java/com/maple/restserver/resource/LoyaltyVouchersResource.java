package com.maple.restserver.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ParamValueConfig;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ParamValueConfigRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class LoyaltyVouchersResource {

	@Autowired
	ExternalApi externalApi;
	
	@Autowired
	ParamValueConfigRepository paramValueConfigRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("{companymstid}/loyaltyvoucher/callloyalty")
	public String callLoyalty(@RequestParam(value = "phno")String phno,
			@PathVariable(value = "companymstid")String companymstid)
	{
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		ParamValueConfig paramValueConfigApi = paramValueConfigRepo.findByParamAndCompanyMst("API-Key", companyMst);
		ParamValueConfig paramValueConfigHashKey = paramValueConfigRepo.findByParamAndCompanyMst("Hash-Key", companyMst);

 
		//SystemSetting.getLoyaltyResult(phno,paramValueConfigApi.getValue(),paramValueConfigHashKey.getValue());
		
	ResponseEntity<String> response=	externalApi.getLoyaltyResult("9744555253","a6a75a6f457e2bb74c829f0eb81c13357dd1bf7d","9f8489e13685c19ff3dde686106496d1");
		return response.getBody();
 
		//externalApi.getLoyaltyResult(phno,paramValueConfigApi.getValue(),paramValueConfigHashKey.getValue());
	//	return "Success";
 
	}
	
	
	
}
