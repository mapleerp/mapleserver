package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.DamageHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DamageDtlRepository;
import com.maple.restserver.repository.DamageHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Transactional
public class DamageDtlResource {
private static final Logger logger = LoggerFactory.getLogger(DamageDtlResource.class);
	
	@Value("${mybranch}")
	private String mybranch;


	@Autowired
	DamageHdrRepository damageHdrRpo;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	DamageDtlRepository damageDtlRepo;
	 @Autowired
	 SaveAndPublishService saveAndPublishService;


	@PostMapping("{companymstid}/damagehdr/{damagehdrId}/damagedtl")
	public DamageDtl createDamageDtl(
			@PathVariable(value = "damagehdrId") String damagehdrId,
			@PathVariable (value = "companymstid") String companymstid,
			@Valid @RequestBody DamageDtl damageDtlRequest) {
		
		    DamageHdr damageHdr= damageHdrRpo.findByIdAndCompanyMstId(damagehdrId,companymstid);
			
		    
		    Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			damageDtlRequest.setCompanyMst(companyMst);
			damageDtlRequest.setDamageHdr(damageHdr);
			
			logger.info("saveDamageDtl send to KafkaEvent: {}", damageDtlRequest);
//			return damageDtlRepo.saveAndFlush(damageDtlRequest);
			return saveAndPublishService.saveDamageDtl(damageDtlRequest, mybranch);
		
	}
	
	
	@GetMapping("{companymstid}/{id}/damagedtl")
	public  DamageDtl retrieveAllDamageHdrAndId(@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="id") String id){
		return damageDtlRepo.findByIdAndCompanyMstId(id,companymstid);
	}
	
	
	@GetMapping("{companymstid}/damage/{damagehdrid}/{itemId}/{batch}/getdamagedetailqty")
	public Double retrieveAllDamageDtlQtyById(@PathVariable(value = "damagehdrid") String damagehdrid,
			@PathVariable(value = "itemId") String itemId,@PathVariable(value = "batch") String batch,

			@PathVariable(value = "companymstid") String companymstid) {

		// return
		// salesDetailsService.getSalesDetailItemQty(companymstid,salesTransHdrID,itemId,batch);
		return damageDtlRepo.getSalesDetailItemQty(damagehdrid, itemId,batch);
	}

	

	@GetMapping("{companymstid}/{hdrid}/damagedtls")
	public  List<DamageDtl>  retrieveAllDamageDtl(@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="hdrid") String hdrid){
		return damageDtlRepo.findByDamageHdrIdAndCompanyMstId(hdrid,companymstid);
	}
	
	
	@DeleteMapping("{companymstid}/damagedtl/{damagedetailId}")
	public void DeleteDamageHdrDtls(@PathVariable(value="companymstid") CompanyMst companymstid,
			@PathVariable (value="damagedetailId") String damagedetailId) {
		damageDtlRepo.deleteById(damagedetailId);

	}
	

	
	
	
	
	
	
}
