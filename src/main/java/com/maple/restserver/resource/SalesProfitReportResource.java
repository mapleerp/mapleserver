package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.SalesProfitHdrReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.SalesProfitReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class SalesProfitReportResource {

	
	@Autowired
	CompanyMstRepository  companyMstRepository;
	
	
	

	@Autowired
	SalesProfitReportService  salesProfitReportService;
	
	
	
	
	
	
	

	
	@GetMapping("{companymstid}/salesprofitreportresource/salesprofitreport/{branchcode}")
	public List<SalesProfitHdrReport> getSalesProfitHdrReport(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate
			 ){
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
	
		return salesProfitReportService.getSalesProfitHdrReport(branchcode, fromdate,todate,companymstid );

	

	}
}
