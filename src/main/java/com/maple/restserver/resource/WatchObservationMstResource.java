package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductMst;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.entity.WatchComplaintMst;
import com.maple.restserver.entity.WatchObservationMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProductMstRepository;
import com.maple.restserver.repository.WatchComplaintMstRepository;
import com.maple.restserver.repository.WatchObservationMstRepository;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class WatchObservationMstResource {
	Pageable topFifty =   PageRequest.of(0, 50);
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	private WatchObservationMstRepository watchObservationMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;
	

	@GetMapping("{companymstid}/watchobservationmst/observationbyname")
	public WatchObservationMst WatchObservationMstByName(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("name") String name) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}

		return watchObservationMstRepository.findByCompanyMstAndObservation(companyOpt.get(), name);
	}

	@PostMapping("{companymstid}/watchobservationmst/savewatchobservationmst")
	public WatchObservationMst createWatchObservationMst(
			@Valid @RequestBody WatchObservationMst watchObservationMst,
			@PathVariable(value = "companymstid") String companymstid) {
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}
		
		watchObservationMst.setCompanyMst(companyOpt.get());

		watchObservationMst = watchObservationMstRepository.saveAndFlush(watchObservationMst);
		
		   Map<String, Object> variables = new HashMap<String, Object>();

					variables.put("voucherNumber", watchObservationMst.getId());

					variables.put("voucherDate", SystemSetting.getSystemDate());
					variables.put("inet", 0);
					variables.put("id", watchObservationMst.getId());

					variables.put("companyid", watchObservationMst.getCompanyMst());
					variables.put("branchcode", watchObservationMst.getBranchCode());

					variables.put("REST", 1);
					
					
					variables.put("WF", "forwardWatchObservationMst");
					
					
					String workflow = (String) variables.get("WF");
					String voucherNumber = (String) variables.get("voucherNumber");
					String sourceID = (String) variables.get("id");


					LmsQueueMst lmsQueueMst = new LmsQueueMst();

					lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
					
					//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
					
					
					java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
					java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
					lmsQueueMst.setVoucherDate(sqlVDate);
					
					
					
					lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
					lmsQueueMst.setVoucherType(workflow);
					lmsQueueMst.setPostedToServer("NO");
					lmsQueueMst.setJobClass("forwardWatchObservationMst");
					lmsQueueMst.setCronJob(true);
					lmsQueueMst.setJobName(workflow + sourceID);
					lmsQueueMst.setJobGroup(workflow);
					lmsQueueMst.setRepeatTime(60000L);
					lmsQueueMst.setSourceObjectId(sourceID);
					
					lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

					lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
					variables.put("lmsqid", lmsQueueMst.getId());
					eventBus.post(variables);
					
					return watchObservationMst;

	}
	
	@GetMapping("{companymstid}/watchobservationmst/observationbyid/{id}")
	public WatchObservationMst retrieveWatchObservationById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}

		return watchObservationMstRepository.findByCompanyMstAndId(companyOpt.get(), id);
	}

	@GetMapping("{companymstid}/watchobservationmst")
	public List< Object> retrieveAllProduct(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid)
	{
		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		return  watchObservationMstRepository.findSearch("%"+searchstring.toLowerCase()+"%", topFifty,companyMst);
	}
}


