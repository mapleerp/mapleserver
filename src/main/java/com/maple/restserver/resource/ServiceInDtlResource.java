package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.RawMaterialReturnDtl;
import com.maple.restserver.entity.ServiceInDtl;
import com.maple.restserver.entity.ServiceInHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.ServiceInReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ServiceInDtlRepository;
import com.maple.restserver.repository.ServiceInHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.ServiceInService;

@RestController
public class ServiceInDtlResource {
	
	private static final Logger logger = LoggerFactory.getLogger(PurchaseHdrResource.class);
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	ServiceInHdrRepository serviceInHdrRepo;
	
	@Autowired
	ServiceInService serviceInService;
	
	
	@Autowired
	ServiceInDtlRepository serviceInDtlRepo;
	

	 @PostMapping("{companymstid}/serviceindtl/{servicehdrid}")
	    public ServiceInDtl createServiceInDtl(@PathVariable (value = "servicehdrid") String servicehdrid,
	                                 @Valid @RequestBody ServiceInDtl serviceInDtl) {
	        return serviceInHdrRepo.findById(servicehdrid).map(ServiceInHdr -> {
	        	serviceInDtl.setServiceInHdr(ServiceInHdr); 
//	            return serviceInDtlRepo.saveAndFlush(serviceInDtl);
	        	return saveAndPublishService.saveServiceInDtl(serviceInDtl, mybranch);
	        }).orElseThrow(() -> new ResourceNotFoundException("raw Material" + servicehdrid + " not found"));
	    }
	 
	 
	 @GetMapping("{companymstid}/serviceindtl/{servicehdrid}/serviceindtls")
		public List<ServiceInDtl> retriveAllServiceInDtlByHdrId(
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "servicehdrid") String servicehdrid) {

		 Optional<CompanyMst> companyMstOpt =companyMstRepo. findById(companymstid);
			return serviceInDtlRepo.findByServiceInHdrId(servicehdrid);


	}
	 @DeleteMapping("{companymstid}/servicedtl/serviceindtldelete/{id}")
		public void serviceDetailDelete(@PathVariable(value = "id") String Id) {
		 serviceInDtlRepo.deleteById(Id);

		}
	 
	 
	 
	 @GetMapping("{companymstid}/serviceindtl/serviceindtlbydate")
		public List<ServiceInReport> retriveAllServiceInByDate(
				@PathVariable(value = "companymstid") String companymstid,
				@RequestParam("fdate") String fdate,
				@RequestParam("tdate") String tdate) {
		 
		 Date fromDate = ClientSystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		 Date toDate = ClientSystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");


		 Optional<CompanyMst> companyMstOpt =companyMstRepo. findById(companymstid);
			return serviceInService.findByCompanyMstAndBetweenDate(companyMstOpt.get(),fromDate,toDate);


	}
	  
}
