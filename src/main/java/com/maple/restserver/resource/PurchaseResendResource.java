package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class PurchaseResendResource {
	
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@Transactional
	@GetMapping("{companymstid}/purchaseresend")
	public  void resendPurchaseTransaction(
			@PathVariable(value = "companymstid") String companymstid ,
			@RequestParam("fromDate") String  fromDate,
			@RequestParam("toDate") String  toDate
			)
	{
		
		Date fDate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
		Date tDate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
		 
		List<PurchaseHdr> result = purchaseHdrRepository.getPurchaseHdrBetweenDate(fDate,tDate);
		
		for (PurchaseHdr purchaseHdr : result) {
			 saveAndPublishService.purchaseFinalSaveToKafkaEvent(purchaseHdr, mybranch, KafkaMapleEventType.SERVER);
			
		}
		
	}

}
