package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.StockTransferOutDltdDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.StockTransferOutDltdDtlRepository;
import com.maple.restserver.repository.StockTransferOutHdrRepository;
import com.maple.restserver.service.StockTransferDltdDtlService;
import com.maple.restserver.utils.SystemSetting;
@RestController
@Transactional
public class StockTransferDltdDtlResource {
	@Autowired
	private StockTransferOutDltdDtlRepository stockTransferOutDltdDtlRepository;
	
	@Autowired
	StockTransferDltdDtlService stockTransferDltdDtlService;
	
	@Autowired
	StockTransferOutHdrRepository stockTransferOutHdrRepository;
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired 
	ItemBatchDtlRepository itemBatchDtlRepository;
	
	@GetMapping("{companymstid}/stocktransferoutdltddtlbydate")
	public List<StockTransferOutDltdDtl> retrieveAllStockTransferOutDltdDtlByDate(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("date") String fdate){
		
		java.util.Date edate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");

		return stockTransferDltdDtlService.findByCompanyMstAndDate(companymstid,edate);
	}
	
	
	@GetMapping("{companymstid}/stocktransferoutdltddtlbyhdr/{hdrid}")
	public List<StockTransferOutDltdDtl> retrieveAllStockTransferOutDltdDtlByStockTransferOutDltdDtl(
			@PathVariable(value = "hdrid") String hdrid,
			@PathVariable(value = "companymstid") String companymstid){
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		
		Optional<StockTransferOutHdr> stockTransferOutHdr = stockTransferOutHdrRepository.findById(hdrid);

		return stockTransferOutDltdDtlRepository.findByCompanyMstAndStockTransferOutHdr(companyOpt.get(),stockTransferOutHdr.get());
	}  
	
	
	
	
	

}
