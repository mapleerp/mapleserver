package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JobCardInDtl;
import com.maple.restserver.entity.JobCardInHdr;
import com.maple.restserver.repository.JobCardInDtlRepository;

@RestController
@Transactional
public class JobCardInDtlResource {
	
	@Autowired
	JobCardInDtlRepository jobCardInDtlRepo;
	
	
	@GetMapping("{companymstid}/jobcardindtl/getjobcardindtlbyhdrid/{hdrid}")
	public List<JobCardInDtl> getJobCardInHdrByVoucher(
			@PathVariable("companymstid") String companymstid,
			@PathVariable("hdrid") String hdrid)

	{
		
		return jobCardInDtlRepo.findByJobCardInHdrId(hdrid);
	}

}
