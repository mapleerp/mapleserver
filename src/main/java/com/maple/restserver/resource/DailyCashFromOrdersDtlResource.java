package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.DailyCashFromOrdersDtl;
import com.maple.restserver.entity.DailyCreditSales;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.DailyCashFromOrdersDtlRepository;

@RestController
public class DailyCashFromOrdersDtlResource {
	

	@Autowired
	private DailyCashFromOrdersDtlRepository dailyCashFromOrdersDtlRepo;
	
	@GetMapping("{companymstid}/dailycashfromordersdtl")
	public List<DailyCashFromOrdersDtl> retrieveAllDailyCreditSales(){
		return dailyCashFromOrdersDtlRepo.findAll();
	}
	
	@PostMapping("{companymstid}/dailycashfromordersdtl")
	public DailyCashFromOrdersDtl createDailyStock(@Valid @RequestBody 
			DailyCashFromOrdersDtl dailyCreditSales)
	{
		DailyCashFromOrdersDtl  saved = dailyCashFromOrdersDtlRepo.saveAndFlush(dailyCreditSales);
		
		 
		
		return saved;
	}
	
	

}
