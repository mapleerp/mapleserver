package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseAdditionalExpenseDtl;
import com.maple.restserver.entity.PurchaseAdditionalExpenseHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PurchaseAdditionalExpenseDtlRepository;
import com.maple.restserver.repository.PurchaseAdditionalExpenseHdrRepository;

@RestController
@Controller
public class PurchaseAdditionalExpenseDtlResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	PurchaseAdditionalExpenseDtlRepository purchaseAdditionalExpenseDtlRepo;
	
	@Autowired
	PurchaseAdditionalExpenseHdrRepository purchaseAddtionalExpenseHdrRepo;
	
	@PostMapping("{companymstid}/purchaseadditionalexpensedtl/savepurchasedtladditionalexpense/{hdrid}")
	public PurchaseAdditionalExpenseDtl createPurchaseAdditionalExpenseDtl(@Valid @RequestBody PurchaseAdditionalExpenseDtl purchaseAdditionalExpenseDtl,
			@PathVariable (value = "companymstid") String companymstid,
			@PathVariable(value = "hdrid")String hdrid)
	{ 
			Optional<PurchaseAdditionalExpenseHdr> purchaseAdditionExpenseHdrop = purchaseAddtionalExpenseHdrRepo.findById(hdrid);
			purchaseAdditionalExpenseDtl.setPurchaseAdditionalExpenseHdr(purchaseAdditionExpenseHdrop.get());
			PurchaseAdditionalExpenseDtl saved=purchaseAdditionalExpenseDtlRepo.save(purchaseAdditionalExpenseDtl);
	 	    return saved;
	}
	
	@GetMapping("{companymstid}/purchaseaddtionalexpensedtl/getpurchaseaddtionaldtlbyhdrid/{hdrid}")
	public List<PurchaseAdditionalExpenseDtl> getbyHdrId(@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "hdrid")String hdrid)
	{
		return purchaseAdditionalExpenseDtlRepo.findByPurchaseAdditionalExpenseHdrId(hdrid);
	}
	
	@GetMapping("{companymstid}/purchaseaddtionalexpensedtl/getpurchaseaddtionaldtlbyhdridnotcalculated/{hdrid}")
	public List<PurchaseAdditionalExpenseDtl> getbyHdrIdNotCalculated(@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "hdrid")String hdrid)
	{
		return purchaseAdditionalExpenseDtlRepo.findByPurchaseAdditionalExpenseHdrIdNotCalculated(hdrid);
	}
	
	@DeleteMapping("{companymstid}/purchaseaddtionalexpensedtl/deletebyid/{dtlid}")
	public void deletePurchaseAdditionalExpenseDtlById(@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "dtlid")String dtlid)
	{
		purchaseAdditionalExpenseDtlRepo.deleteById(dtlid);
	}
	
	@PutMapping("{companymstid}/purchaseaddtionalexpensedtl/updatepurchasedtlstatus")
	public void updatePurchaseDtlStatus(@PathVariable(value = "companymstid")String companymstid,
			@RequestBody PurchaseAdditionalExpenseDtl purreq)
	{
		PurchaseAdditionalExpenseDtl pur = purchaseAdditionalExpenseDtlRepo.findById(purreq.getId()).get();
		pur.setCalculated(purreq.getCalculated());
		purchaseAdditionalExpenseDtlRepo.save(pur);
	}
}
