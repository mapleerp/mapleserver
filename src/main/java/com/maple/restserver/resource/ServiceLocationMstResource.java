package com.maple.restserver.resource;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ServiceInHdr;
import com.maple.restserver.entity.ServiceItemMst;
import com.maple.restserver.entity.ServiceLocationMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ServiceLocationMstRepository;

@RestController
public class ServiceLocationMstResource {


	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	ServiceLocationMstRepository serviceLocationMstRepo;
	
	@PostMapping("{companymstid}/servicelocationmst")
	public ServiceLocationMst createServiceLocationMst(@PathVariable(value = "companymstid") String companymstid,@Valid @RequestBody 
			ServiceLocationMst serviceLocationMst)
	{
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			serviceLocationMst.setCompanyMst(companyMst);
			ServiceLocationMst serviceLocationMstSaved = serviceLocationMstRepo.saveAndFlush(serviceLocationMst);
		
			return serviceLocationMstSaved;
	}
	@GetMapping("{companymstid}/servicelocationmst/getservicelocationmstbyid/{hdrid}")
	public ServiceLocationMst getServiceLocationMstById(@PathVariable ("hdrid") String hdrid,
			@PathVariable("companymstid") String companymstid)
	
	{
		
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
		return serviceLocationMstRepo.findByCompanyMstAndId(companyMst, hdrid);
	}
	
	@DeleteMapping("{companymstid}/servicelocationmst/deletebyid/{hdrid}")
	public void serviceLocationMstDelete(@PathVariable String hdrid) {
		serviceLocationMstRepo.deleteById(hdrid);

	}
	@PutMapping("{companymstid}/servicelocationmst/{servicelocationid}/updateservicelocationmst")
	public ServiceLocationMst updateServiceLocationMst(
			@PathVariable(value="servicelocationid") String servicelocationid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody ServiceLocationMst  serviceLocationMstRequest)
	{
			
		ServiceLocationMst serviceLocationMst = serviceLocationMstRepo.findById(servicelocationid).get();
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		serviceLocationMst.setCompanyMst(companyMst);
		serviceLocationMst.setLocationName(serviceLocationMstRequest.getLocationName());
		serviceLocationMst = serviceLocationMstRepo.saveAndFlush(serviceLocationMst);
		return serviceLocationMst;
	}
	
}
