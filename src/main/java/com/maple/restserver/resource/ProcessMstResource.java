package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.GroupMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProcessMst;
import com.maple.restserver.entity.ProcessPermissionMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProcessMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ProcessMstResource {
private static final Logger logger = LoggerFactory.getLogger(ProcessMstResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	EventBus eventBus = EventBusFactory.getEventBus();

	// @Autowired
	// private RuntimeService runtimeService;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	ProcessMstRepository processMstRepo;

	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	SaveAndPublishService saveAndPublishService;


	@PostMapping("{companymstid}/processcreation")
	public ProcessMst createProcess(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody ProcessMst processMst) {

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

		processMst.setCompanyMst(companyMst.get());

//		ProcessMst saved = processMstRepo.saveAndFlush(processMst);
		ProcessMst saved =saveAndPublishService.saveProcessMst(processMst, processMst.getBranchCode());
		logger.info("ProcessMst send to KafkaEvent: {}", saved);

		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("voucherNumber", saved.getId());
		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", saved.getId());
		variables.put("companyid", saved.getCompanyMst());
		variables.put("branchcode", saved.getBranchCode());

		variables.put("REST", 1);

		variables.put("WF", "forwardProcessMst");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));

		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardProcessMst");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);

		/*
		 * ProcessInstanceWithVariables pVariablesInReturn =
		 * runtimeService.createProcessInstanceByKey("forwardProcess")
		 * .setVariables(variables)
		 * 
		 * .executeWithVariablesInReturn();
		 */

		return saved;
	}

	@DeleteMapping("{companymstid}/processmst/{id}")
	public void journalMstDelete(@PathVariable(value = "id") String Id) {
		processMstRepo.deleteById(Id);

	}

	@GetMapping("{companymstid}/processmsts")
	public List<ProcessMst> retrieveAllUser() {
		return processMstRepo.findAll();
	}

	@GetMapping("{companymstid}/processmstbycompany")
	public List<ProcessMst> retrieveProcessMstByCompanyMst(@PathVariable(value = "companymstid") String companymstid) {
		Optional<CompanyMst> companyMstResp = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstResp.get();

		return processMstRepo.findAll();
	}

	@GetMapping("{companymstid}/processmstbyname/{processName}")
	public ProcessMst retrieveProcessMst(@PathVariable(value = "processName") String processName) {
		return processMstRepo.findByProcessName(processName);
	}

	@GetMapping("{companymstid}/processmstbyid/{id}")
	public @ResponseBody Optional<ProcessMst> processDetailbyId(@PathVariable(value = "id") String id) {
		return processMstRepo.findById(id);
	}

	@GetMapping("{companymstid}/processmstbyprocesstype/{processtype}")
	public List<ProcessMst> retrieveProcessMstByProcessType(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "processtype") String processtype) {
		Optional<CompanyMst> companyMstResp = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstResp.get();

		return processMstRepo.findByCompanyMstAndProcessType(companyMst, processtype);
	}

}
