package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SysDateMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SysDateMstRepository;

@RestController
@Transactional
public class SysDateMstResource {
@Autowired

SysDateMstRepository sysDateMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;
	
	
	
	@PostMapping("{companymstid}/systemdate")
	public SysDateMst createSysDate(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 SysDateMst  sysDateMst)
	{
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		
		CompanyMst companyMst = companyMstOpt.get();
		sysDateMst.setCompanyMst(companyMst);
		
       return sysDateMstRepository.saveAndFlush(sysDateMst);
		
		} 
	
	
	
	@GetMapping("/{companymstid}/getsysdate/{branchCode}")
	public SysDateMst retrieveSysDate(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchCode") String branchCode){
  Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
  	CompanyMst companyMst	= null;
		if(companyMstOpt.isPresent())
		{
		 companyMst = companyMstOpt.get();
		
		}
		return sysDateMstRepository.findByCompanyMstAndBranchCode(companyMst,branchCode);
		
	}
	
	
	
	
	@PutMapping("{companymstid}/updatesysdate/{sysdatemstid}")
	public SysDateMst updateSysDate(
			@PathVariable(value="sysdatemstid") String sysdatemstid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody SysDateMst sysDateMstRequest)
	{
		
				return sysDateMstRepository.findById(sysdatemstid).map(sysdatemst -> {
					
					Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
					CompanyMst companyMst = comapnyMstOpt.get();
					sysdatemst.setAplicationDate(sysDateMstRequest.getAplicationDate());
					sysdatemst.setSystemDate(sysDateMstRequest.getSystemDate());
					sysdatemst.setCompanyMst(companyMst);
					sysdatemst.setDayEndDone(sysDateMstRequest.getDayEndDone());
		
					sysdatemst =sysDateMstRepository.saveAndFlush(sysdatemst);
					
		            return sysdatemst;
		        }).orElseThrow(() -> new ResourceNotFoundException("sysdatemstid " + sysdatemstid + " not found"));
				
				
			
	}
}
