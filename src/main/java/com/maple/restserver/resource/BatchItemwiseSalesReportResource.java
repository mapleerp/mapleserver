package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.BatchItemWiseSalesReport;
import com.maple.restserver.report.entity.BatchWiseCustomerSalesReport;
import com.maple.restserver.service.BatchItemWiseSalesReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class BatchItemwiseSalesReportResource {

	@Autowired
	BatchItemWiseSalesReportService batchItemWiseSalesReportService;
	
	
	@GetMapping("/{companymstid}/batchitemwisesales/{branchcode}/{itemList}")
	public List<BatchItemWiseSalesReport> batchwisItemWiseSalesReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,	@PathVariable(value = "itemList") List<String> itemList,  
			@RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	java.util.Date TDate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		return batchItemWiseSalesReportService.itemWiseSalesReport(companymstid,branchcode,fDate,TDate,itemList);
	}
	
}
