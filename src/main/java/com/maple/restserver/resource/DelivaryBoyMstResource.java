package com.maple.restserver.resource;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DeliveryBoyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DelivaryBoyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class DelivaryBoyMstResource {
private static final Logger logger = LoggerFactory.getLogger(DelivaryBoyMstResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	DelivaryBoyMstRepository 	delivaryBoyMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	 @Autowired
	 SaveAndPublishService saveAndPublishService;
	
	
	@PostMapping("{companymstid}/delivaryboymst")
	public DeliveryBoyMst createDeliveryBoy(
			@PathVariable(value = "companymstid") String companymstid, 
			@Valid @RequestBody   DeliveryBoyMst  delivaryBoyMst)
	{
		
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		
		CompanyMst companyMst = companyMstOpt.get();
		delivaryBoyMst.setCompanyMst(companyMst);
		
//		DeliveryBoyMst deliveryBoyMst = delivaryBoyMstRepository.saveAndFlush(delivaryBoyMst);
		DeliveryBoyMst deliveryBoyMst =saveAndPublishService.saveDeliveryBoyMst(delivaryBoyMst, delivaryBoyMst.getBranchCode());
		 logger.info("DeliveryBoyMst send to KafkaEvent: {}", deliveryBoyMst);
		
       Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", deliveryBoyMst.getId());

		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", deliveryBoyMst.getId());

		variables.put("companyid", deliveryBoyMst.getCompanyMst());
		variables.put("branchcode", deliveryBoyMst.getBranchCode());

		variables.put("REST", 1);
		
		
		variables.put("WF", "forwardDelivaryBoyMst");
		
		
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		
		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		
		
		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardDelivaryBoyMst");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);
		return deliveryBoyMst;
		
	} 
	
	
	
	
	@DeleteMapping("/{companymstid}/deletedelivaryboy/{id}")
	public void deliveryBoyDelete(@PathVariable(value = "id") String Id) {
		delivaryBoyMstRepository.deleteById(Id);

	}
	
	@GetMapping("{companymstid}/getdeliveryboy/{branchcode}")
	public List<DeliveryBoyMst> retrievDelivaryBoy(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="branchcode") String branchcode){
		return delivaryBoyMstRepository.findByCompanyMstIdAndBranchCode(companymstid,branchcode);
	}
	
	@GetMapping("{companymstid}/getdeliveryboybystatus/{branchcode}")
	public List<DeliveryBoyMst> retrievDelivaryBoyByStatus(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="branchcode") String branchcode){
		return delivaryBoyMstRepository.getDeliveryBoyByStatus(companymstid,branchcode);
	}
	
	
	@PutMapping("{companyid}/updatedelivaryboy/{deliveryboyid}/deliveryboy")
	public DeliveryBoyMst delivaryBoyMstUpdate(
			@PathVariable String deliveryboyid, 
			@PathVariable (value = "companyid") String companyid,
			@Valid @RequestBody DeliveryBoyMst deliveryBoyMstRequest)
	{
 		  
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();
	
		deliveryBoyMstRequest.setCompanyMst(companyMst);
		 
				return delivaryBoyMstRepository.findById(deliveryboyid).map(deliveryboymst -> {
					 
					deliveryboymst.setStatus(deliveryBoyMstRequest.getStatus());
					
				//	delivaryBoyMstRepository.saveAndFlush(deliveryboymst);
					saveAndPublishService.saveDeliveryBoyMst(deliveryboymst, deliveryboymst.getBranchCode());
					 logger.info("DeliveryBoyMst send to KafkaEvent: {}", deliveryboymst);
					
					
		            return deliveryBoyMstRequest;
		            
		        }).orElseThrow(() -> new ResourceNotFoundException("deliveryboymst " + deliveryBoyMstRequest + " not found"));
				
				/*
				 * 
				 * work flow forwardDeliveryBoyMst
				 */
	}
	

	@GetMapping("{companymstid}/getdeliveryboybyname/{delivaryboyname}/{branchcode}")

	public DeliveryBoyMst retrievDelivaryBoyByName(@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="branchcode") String branchcode,
			@PathVariable(value="delivaryboyname") String delivaryboyname){
		
		System.out.println(delivaryboyname+"delivary boy name"+companymstid+"company mst id");
		
		return delivaryBoyMstRepository.findByCompanyMstIdAndDeliveryBoyNameAndBranchCode(companymstid,delivaryboyname,branchcode);
	}
	
	@GetMapping("{companymstid}/deliveryboymst/deliveryboymstbyid/{id}")
	public DeliveryBoyMst retrievDelivaryBoyById(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="id") String id){
		
		
		return delivaryBoyMstRepository.findByCompanyMstIdAndId(companymstid,id);
	}
	
}
