package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.StockMovementView;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.service.StockService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
@CrossOrigin("http://localhost:4200")
public class StockResource {
	

	@Autowired
	private StockService stockService;
	
//	@GetMapping("{companymstid}/stocklessminqty")
//	public List<ItemBatchMst> retrieveAllAcceptStock(@PathVariable(value = "companymstid") String
//			  companymstid){
//		return stockService.getAllStockLessThanMinQty(companymstid);
//	}
	
	@GetMapping("{companymstid}/stockresource/stockifitem/{itemid}")
	public double retrieveStock(@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value ="itemid" )String itemid)
	{
		return stockService.getStockOfItem(companymstid, itemid);
	}
	
	@GetMapping("{companymstid}/{branchcode}/stockresource/getstockmovementallitem")
	public List<StockMovementView> getstockMovmentAllItem(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate){
		
		java.sql.Date startDate = SystemSetting.StringToSqlDate(fromdate, "yyy-MM-dd");
		java.sql.Date endDate = SystemSetting.StringToSqlDate(todate, "yyy-MM-dd");
		
		
		return stockService.getAllItemMovement(companymstid, startDate, endDate , branchcode);
	}
}
