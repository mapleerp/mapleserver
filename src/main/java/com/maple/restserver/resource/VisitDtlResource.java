package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.VisitDtl;
import com.maple.restserver.repository.VisitDtlRepository;

@RestController
@Transactional
public class VisitDtlResource {
	@Autowired
	private VisitDtlRepository visitdtl1;
	@GetMapping("{companymstid}/visitdtl")
	public List<VisitDtl> retrieveAlldepartments()
	{
		return visitdtl1.findAll();
		
	}
	@PostMapping("{companymstid}/visitdtl")
	public VisitDtl createUser(@Valid @RequestBody VisitDtl visitdetail1)
	{
		VisitDtl saved=visitdtl1.saveAndFlush(visitdetail1);
		
	return saved;
	
	}

	
}
