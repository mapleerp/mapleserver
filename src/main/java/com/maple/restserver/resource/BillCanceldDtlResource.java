package com.maple.restserver.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BillCanceldDtl;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.BillCanceldDtlRepository;
import com.maple.restserver.repository.BillCanceldHdrRepository;

@RestController
@Transactional
public class BillCanceldDtlResource {
@Autowired
BillCanceldDtlRepository billCanceldDtlRepository;


@Autowired
BillCanceldHdrRepository billCanceldHdrRepository;
	/*
	 * @PostMapping("{companymstid}/salestranshdr/{salestranshdrId}/salesdtl")
	 * public BillCanceldDtl createBillCanceldDtl(@PathVariable (value =
	 * "billCanceldHdrId") String billCanceldHdrId,
	 * 
	 * @Valid @RequestBody BillCanceldDtl billCanceldDtlRequest) {
	 * 
	 * 
	 * return billCanceldHdrRepository.findById(billCanceldHdrId).map(BillCanceldHdr
	 * -> {
	 * 
	 * billCanceldDtlRequest.setCompanyMst(salestranshdr.getCompanyMst());
	 * 
	 * BillCanceldDtl addedItem =
	 * salesdtlRepo.findBySalesTransHdrIdAndItemIdAndBatchAndBarcode(salestranshdr.
	 * getId(),salesDtlRequest.getItemId(), salesDtlRequest.getBatch(),
	 * salesDtlRequest.getBarcode());
	 * 
	 * if(null!=addedItem && null!= addedItem.getId()) {
	 * addedItem.setQty(addedItem.getQty() + salesDtlRequest.getQty());
	 * addedItem.setCgstAmount(addedItem.getCgstAmount()+salesDtlRequest.
	 * getCgstAmount());
	 * addedItem.setSgstAmount(addedItem.getSgstAmount()+salesDtlRequest.
	 * getSgstAmount());
	 * addedItem.setCessAmount(addedItem.getCessAmount()+salesDtlRequest.
	 * getCessAmount()); addedItem.setAmount(addedItem.getAmount()
	 * +salesDtlRequest.getAmount() );
	 * 
	 * return salesdtlRepo.save(addedItem); }else {
	 * billCanceldDtlRequest.setSalesTransHdr(salestranshdr); return
	 * salesdtlRepo.save(billCanceldDtlRequest); } }).orElseThrow(() -> new
	 * ResourceNotFoundException("salestranshdrId " + salestranshdrId +
	 * " not found"));
	 * 
	 * 
	 * }
	 */


@DeleteMapping("{companymstid}/billcancelddtls/{bilcancelddtlId}")
public void billCanceldDtlDelete(@PathVariable String salesdtlId) {
	billCanceldDtlRepository.deleteById(salesdtlId);

}
}


