package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SchEligibilityDef;
import com.maple.restserver.entity.SiteMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SiteMstRepository;
@CrossOrigin("http://localhost:4200")

@RestController
@Transactional
public class SiteMstResource {

	@Autowired
	SiteMstRepository siteMstRepository;
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	
	@PostMapping("{companymstid}/sitemst")
	public SiteMst createSite(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			SiteMst  siteMst)
	{
	Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
//	Optional<CustomerMst> customerMst= customerRegRepo.findById(custid);
			siteMst.setCompanyMst(companyMst.get());
	//		siteMst.setCustomerMst(customerMst.get());
		return  siteMstRepository.saveAndFlush(siteMst);
		
		
	}
	
	
	@GetMapping("{companymstid}/sitemstbycustomerid/{customerid}")
	public List<SiteMst> siteMstbyCustomerId(
			@PathVariable(value = "companymstid") String companymstid,
			  @PathVariable("customerid") String customerid )
	{
		 Optional<AccountHeads> customerMstOpt =  accountHeadsRepository.findById(customerid) ;
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		return siteMstRepository.findByAccountHeadsAndCompanyMst(customerMstOpt.get(),companyMstOpt.get());
		
	}
	

	@GetMapping("{companymstid}/showsites")
	public  @ResponseBody List<SiteMst> findAllSite(@PathVariable(value = "companymstid") String
			 companymstid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return siteMstRepository.findByCompanyMst(companyMst.get());
	}
	

	@DeleteMapping("{companymstid}/sitemstdelete/{id}")
	public void SiteMstDelete(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value = "id") String Id) {
		siteMstRepository.deleteById(Id);

	}
	}
	

