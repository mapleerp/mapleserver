package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
 
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.entity.DeliveryBoyMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;

@RestController
public class CurrencyMstResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	CurrencyMstRepository currencyMstRepo;
	
	
	@PostMapping("{companymstid}/currencymst/savecurrencymst")
	public CurrencyMst createCurrencyMst(
			@PathVariable(value = "companymstid") String companymstid, 
			@Valid @RequestBody   CurrencyMst  currencyMst)
	{
		
		
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		
		CompanyMst companyMst = companyMstOpt.get();
		currencyMst.setCompanyMst(companyMst);
		
		CurrencyMst saved = currencyMstRepo.save(currencyMst);
       return saved;
	}
	
	@GetMapping("{companymstid}/currencymst/getcurrencybyname/{currencyname}")
	public CurrencyMst getcurrencyByName(
			@PathVariable(value ="companymstid")String companymstid,
			@PathVariable(value = "currencyname")String currencyname)
	{
		return currencyMstRepo.findByCurrencyName(currencyname);
	}
	
	@GetMapping("{companymstid}/currencymst/getcurrencybyid/{currencyid}")
	public Optional<CurrencyMst> getcurrencyById(
			@PathVariable(value ="companymstid")String companymstid,
			@PathVariable(value = "currencyid")String currencyid)
	{
		return currencyMstRepo.findById(currencyid);
	}
	@GetMapping("{companymstid}/currencymst/getallcurrency")
	public List<CurrencyMst> getAllCurrency(
			@PathVariable(value ="companymstid")String companymstid)
	{
		return currencyMstRepo.findAll();
	}
	
	
}
