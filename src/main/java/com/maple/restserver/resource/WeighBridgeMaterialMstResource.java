package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.WeighBridgeMaterialMst;
import com.maple.restserver.entity.WeighBridgeTareWeight;
import com.maple.restserver.entity.WeighBridgeWeights;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.WeighBridgeMaterialMstRepository;
import com.maple.restserver.repository.WeighBridgeWeightsRepository;
@RestController
@Transactional
public class WeighBridgeMaterialMstResource {
	@Autowired
	private WeighBridgeMaterialMstRepository weighBridgeMaterialMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	  
	@GetMapping("{companymstid}/findallweighbridgematerialmst")
	public List<WeighBridgeMaterialMst> retrieveAllWeighBridgeMaterialMst(
			@PathVariable(value = "companymstid") String
			  companymstid){
		return weighBridgeMaterialMstRepository.findAll();
	}
	
	@GetMapping("{companymstid}/findweighbridgematerialmstbymaterial/{material}")
	public  WeighBridgeMaterialMst  retrieveWeighBridgeMaterialMstByMaterial(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "material") String material){
		return weighBridgeMaterialMstRepository.findByMaterial(material);
	}
	
	@GetMapping("{companymstid}/findweighbridgematerialmstbyid/{id}")
	public  Optional<WeighBridgeMaterialMst>  retrieveWeighBridgeMaterialMstById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id){
		return weighBridgeMaterialMstRepository.findById(id);
	}
	
	
	@PostMapping("{companymstid}/weighbridgematerialmst")
	public WeighBridgeMaterialMst createWeighBridgeMaterialMst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  WeighBridgeMaterialMst weighBridgeMaterialMst)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			weighBridgeMaterialMst.setCompanyMst(companyMst);
		
		
			return weighBridgeMaterialMstRepository.save(weighBridgeMaterialMst);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));
		
		
		
	
	}
	
	
	
	

}
