package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.AppointmentDtl;
import com.maple.restserver.repository.AppointmentDtlRepository;


@RestController
@Transactional
public class AppointmentDtlResource {
	@Autowired
	private AppointmentDtlRepository appointdtl;
	@GetMapping("{companymstid}/appointmentdtl")
	public List<AppointmentDtl> retrieveAlldepartments()
	{
		return appointdtl.findAll();
		
	}
	@PostMapping("{companymstid}/appointmentdtl")
	public ResponseEntity<Object>createUser(@Valid @RequestBody AppointmentDtl appointmentdtl1)
	{
		AppointmentDtl saved=appointdtl.saveAndFlush(appointmentdtl1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}

}
