
package com.maple.restserver.resource;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.OpeningStock;
import com.maple.restserver.repository.OpeningStockRepository;



@RestController
@Transactional
public class OpeningStockResource {
	@Autowired
	private OpeningStockRepository openingStock;
	@GetMapping("{companymstid}/openingstock")
	public List<OpeningStock> retrieveAllcash()
	{
		return openingStock.findAll();
	}
	@PostMapping("{companymstid}/openingstock")
	public ResponseEntity<Object> createUser(@Valid @RequestBody OpeningStock openingStock1)
	{
		OpeningStock saved=openingStock.saveAndFlush(openingStock1);
	URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	}

}

