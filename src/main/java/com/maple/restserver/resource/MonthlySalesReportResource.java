package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.MonthlySalesDtl;
import com.maple.restserver.entity.MonthlySalesTransHdr;
import com.maple.restserver.report.entity.DailySalesReportDtl;
import com.maple.restserver.repository.MonthlySalesDtlRepository;
import com.maple.restserver.repository.MonthlySalesTransHdrRepository;
import com.maple.restserver.service.MonthlySalesReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class MonthlySalesReportResource {

	
	@Autowired
	MonthlySalesReportService monthlySalesReportService;
	
	@Autowired
	MonthlySalesDtlRepository  monthlySalesDtlRepository;
	
	MonthlySalesTransHdrRepository monthlySalesTransHdrRepository;

	

	@GetMapping("{companymstid}/monthlysalesreportresource/{branchcode}/monthlysalesreport")
	public List<MonthlySalesTransHdr> getMonthlySalesReporthdr(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate
			 ){
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
	
		return monthlySalesReportService.getMonthlySalesReporthdr(branchcode, fromdate,todate,companymstid );


	}
	
	
	
	@GetMapping("{companymstid}/monthlysalesreportresource/monthlysalesreportdtl/{vouchernumber}")
	public List<MonthlySalesDtl> getMonthlySalesReportDtl(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("vouchernumber") String vouchernumber
	
			 ){
	//	java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
	//	java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
	
		return monthlySalesReportService.findByMonthlySalesTransHdr(vouchernumber);


	}
	
	

    @GetMapping("{companymstid}/monthlysalesreportresource/salestomonthlysales")
	public String moveSalesToMonthlySalesTransHdr(
			@PathVariable(value = "companymstid") String companymstid,
                  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate
			 ){
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
	
		System.out.print("inside from date issssssssssss"+fromdate);
		System.out.print("inside to date issssssssssss"+todate);
		monthlySalesTransHdrRepository.insertingIntoMonthlySalesTransHdr(fromdate, todate);
		monthlySalesDtlRepository.insertingIntoMonthlySalesDtl(fromdate, todate);
		
		return "sucesses";
	}
	
	
	
}
