package com.maple.restserver.resource.hr;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.hr.EmployeeDtl;
import com.maple.restserver.entity.hr.EmployeeHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.hr.EmployeeDtlRepository;
import com.maple.restserver.repository.hr.EmployeeHdrRepository;

@RestController
public class EmployeeDtlResource {
	@Autowired
	EmployeeHdrRepository employeeHdrRepository;
	
	@Autowired
	EmployeeDtlRepository employeeDtlRepository;
	
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@PostMapping("{companymstid}/employeehdrresource/saveemployeedetails/{employeeid}")
	public EmployeeDtl CreateEmployeeDtl(
			@PathVariable(value="employeehdrid") String employeehdrid,
			@Valid @RequestBody EmployeeDtl employeeDtl,
			@PathVariable(value="companymstid") String companymstid ) {
	 Optional<EmployeeHdr> employeeHdrOpt=employeeHdrRepository.findById(employeehdrid);
	 Optional<CompanyMst> companyMstOpt=companyMstRepo.findById(companymstid);
	 employeeDtl.setCompanyMst(companyMstOpt.get());
			 employeeDtl.setEmployeeHdr(employeeHdrOpt.get());
			return employeeDtlRepository.save(employeeDtl);
				
	
	}	
	
	@DeleteMapping("{companymstid}/employeehdrresource/deleteemployeedetails/{employeedtl}")
	public void deleteEmployeedetails(
			@PathVariable(value = "employeedtl") String employeedtl) {
		employeeDtlRepository.deleteById(employeedtl);
	}
	@GetMapping("{companymstid}/employeedtl/showallemployeedtlbyhdrid/{hdrid}")
	public List<EmployeeDtl> showAllEmployeeDtlByCompanyMstAndHdrId(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "hdrid")String hdrid) {

		return employeeDtlRepository.findByCompanyMstIdAndEmployeeHdrId(companymstid,hdrid);

	}
	
	
}
