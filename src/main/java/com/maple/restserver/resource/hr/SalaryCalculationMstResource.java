package com.maple.restserver.resource.hr;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.hr.GradeMst;
import com.maple.restserver.entity.hr.SalaryCalculationMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.hr.GradeMstRepository;
import com.maple.restserver.repository.hr.SalaryCalculationMstRepository;

@RestController
public class SalaryCalculationMstResource {
@Autowired
	
	CompanyMstRepository companyMstRepo;
	@Autowired
	SalaryCalculationMstRepository salaryCalculationMstRepository;
	
	@PostMapping("{companymstid}/salarycalculationmstresource/savesalarycalculationmst")
	public SalaryCalculationMst createSalaryCalculationMst(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SalaryCalculationMst salaryCalculationMst) {

			CompanyMst  companyMst = companyMstRepo.findById(companymstid).get();
			salaryCalculationMst.setCompanyMst(companyMst);
			salaryCalculationMst= salaryCalculationMstRepository.save(salaryCalculationMst);
			return salaryCalculationMst;
		}

	@DeleteMapping("{companymstid}/salarycalculationmstresource/deletesalarycalculationmstresource/byid/{id}")
	public void deleteSalaryCalculationMst(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {
		salaryCalculationMstRepository.deleteById(id);
	}
	
	@GetMapping("{companymstid}/salarycalculationmstresource/showallsalarycalculationmstbycompanymst")
	public List<SalaryCalculationMst> showAllSalaryCalculationMstByCompanyMst(
			@PathVariable(value = "companymstid") String companymstid) 
	{

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		if (!companyMstOpt.isPresent()) {
			return null;
		}
		return salaryCalculationMstRepository.findByCompanyMst(companyMstOpt.get());

	}
	
	@GetMapping("{companymstid}/salarycalculationmst/showallsalarycalculationmstbyid/{id}")
	public Optional<SalaryCalculationMst> getSalaryCalculationMstById(@PathVariable(value = "id") String id) {

		return salaryCalculationMstRepository.findById(id);

	}
}



