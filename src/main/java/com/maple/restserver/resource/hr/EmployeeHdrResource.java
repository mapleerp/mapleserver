package com.maple.restserver.resource.hr;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.hr.EmployeeHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.hr.EmployeeHdrRepository;

@RestController
public class EmployeeHdrResource {

	@Autowired

	CompanyMstRepository companyMstRepo;
	@Autowired
	EmployeeHdrRepository employeeMstRepository;

	@PostMapping("{companyMstid}/employeemstresource/addemployeemst")
	public EmployeeHdr 
	      createEmployeeMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody EmployeeHdr employeeMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			employeeMst.setCompanyMst(companyMst);
			return employeeMstRepository.save(employeeMst);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid" + companymstid + "not found"));

	}

	@DeleteMapping("{companymstid}/employeeMst/deleteemployeemstbyid/{id}")
	public void deleteEmployeeMst(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {
		employeeMstRepository.deleteById(id);
	}

	@GetMapping("{companymstid}/employeemst/showallemployeemstbycompanymst")
	public List<EmployeeHdr> showAllEmployeeMstByCompanyMst(@PathVariable(value = "companymstid")
	String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		if (!companyMstOpt.isPresent()) {
			return null;
		}
		return employeeMstRepository.findByCompanyMst(companyMstOpt.get());

	}

	@GetMapping("{companymstid}/employeemst/showallemployeemstbyid/{id}")
	public Optional<EmployeeHdr> getEmployeeMstById(@PathVariable(value = "id") String id) {

		return employeeMstRepository.findById(id);

	}
	
//	@GetMapping("{comapnymstid}/employeehdr/getbyage/{age}")
//	public List<EmployeeHdr> getEmployeeByAge(@PathVariable(value = "comapnymstid")String comapnymstid,
//			@PathVariable(value = "age")Integer age)
//	{
//		return employeeMstRepository.findByAge(age);
//	}
//	

}
