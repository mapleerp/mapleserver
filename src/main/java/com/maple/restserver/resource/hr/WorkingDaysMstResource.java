package com.maple.restserver.resource.hr;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.hr.GradeMst;
import com.maple.restserver.entity.hr.WorkingDaysMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.hr.GradeMstRepository;
import com.maple.restserver.repository.hr.WorkingDaysMstRepository;

@RestController
public class WorkingDaysMstResource {
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	WorkingDaysMstRepository workingDaysMstRepository;
	

	@PostMapping("{companyMstid}/workingdaysmstresource/saveworkingdaysmst")
	public @Valid WorkingDaysMst 
	      createGradeMst (@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody WorkingDaysMst workingDaysMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			workingDaysMst.setCompanyMst(companyMst);
			return workingDaysMstRepository.save(workingDaysMst);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid" + companymstid + "not found"));

	}
	
	@DeleteMapping("{companymstid}/workingdaysmst/deleteworkingdaysmstbyid/{id}")
	public void deleteWorkingdaysmstt(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {
		workingDaysMstRepository.deleteById(id);
	}
	@GetMapping("{companymstid}/workingdaysmst/showallworkingdaysmstbycompanymst")
	public List<WorkingDaysMst> showAllworkingDaysMstByCompanyMst(@PathVariable(value = "companymstid")
	String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		if (!companyMstOpt.isPresent()) {
			return null;
		}
		return workingDaysMstRepository.findByCompanyMst(companyMstOpt.get());

	}
	
	@GetMapping("{companymstid}/workingdaysmst/showallworkingdaysmstbyid/{id}")
	public Optional<WorkingDaysMst> getWorkingDaysMstById(@PathVariable(value = "id") String id) {

		return workingDaysMstRepository.findById(id);

	}

}
