package com.maple.restserver.resource.hr;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.hr.AttendenceMst;
import com.maple.restserver.entity.hr.SalaryCalculationMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.hr.AttendenceMstRepository;

@RestController

public class AttendenceMstResource {
	@Autowired
	AttendenceMstRepository attendenceMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@PostMapping("{companymstid}/attendencemst/saveattendencemst")
	public AttendenceMst createAttendenceMst(
			@PathVariable (value="companymstid")String companymstid,
			@Valid @RequestBody AttendenceMst attendenceMst)
	{

		CompanyMst  companyMst = companyMstRepository.findById(companymstid).get();
		attendenceMst.setCompanyMst(companyMst);
		attendenceMst= attendenceMstRepository.save(attendenceMst);
		return attendenceMst;
	}
	
	@DeleteMapping("{companymstid}/attendencemstresource/deleteattendencemst/byid/{id}")
	public void deleteAttendenceMst(@PathVariable(value="companymstid")String companymstid,
	@PathVariable(value="id") String id)
	{
		attendenceMstRepository.deleteById(id);
	}
	
	@GetMapping("{companymstid}/attendencemst/showallattendencemstbycompanymst")
	public List<AttendenceMst> showAllAttendenceMstByCompanyMst(
			@PathVariable(value = "companymstid") String companymstid) 
	{

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		if (!companyMstOpt.isPresent()) {
			return null;
		}
		return attendenceMstRepository.findByCompanyMst(companyMstOpt.get());

	}
	
	@GetMapping("{companymstid}/attendencemst/showallattendencemstbyid/{id}")
	public Optional<AttendenceMst> getAttendenceMstById(@PathVariable(value = "id") String id) {

		return attendenceMstRepository.findById(id);

	}
	

}
