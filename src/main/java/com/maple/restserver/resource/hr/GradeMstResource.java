package com.maple.restserver.resource.hr;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.hr.EmployeeHdr;
import com.maple.restserver.entity.hr.GradeMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.hr.GradeMstRepository;

@RestController
public class GradeMstResource {
	@Autowired
	
	CompanyMstRepository companyMstRepo;
	@Autowired
	GradeMstRepository gradeMstRepository;
	
	@PostMapping("{companyMstid}/grademstresource/savegrademst")
	public GradeMst 
	      createGradeMst (@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody GradeMst gradeMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			gradeMst.setCompanyMst(companyMst);
			return gradeMstRepository.save(gradeMst);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid" + companymstid + "not found"));

	}
	
	@DeleteMapping("{companymstid}/grademst/deletegrademstbyid/{id}")
	public void deleteEmployeeMst(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {
		gradeMstRepository.deleteById(id);
	}
	@GetMapping("{companymstid}/grademst/showallgrademstbycompanymst")
	public List<GradeMst> showAllGradeMstByCompanyMst(@PathVariable(value = "companymstid")
	String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		if (!companyMstOpt.isPresent()) {
			return null;
		}
		return gradeMstRepository.findByCompanyMst(companyMstOpt.get());

	}
	
	@GetMapping("{companymstid}/gradeemst/showallgradeemstbyid/{id}")
	public Optional<GradeMst> getGradeMstById(@PathVariable(value = "id") String id) {

		return gradeMstRepository.findById(id);

	}
}
