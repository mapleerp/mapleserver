package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemNutrition;
import com.maple.restserver.entity.UserGroupMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.UserGroupMstReository;

@RestController
@Transactional
public class UserGroupMstResource {

	@Autowired
	UserGroupMstReository userGroupMst;
	
	@Autowired
	CompanyMstRepository companyMstRepo;


	@PostMapping("{companymstid}/usergroup")
	public UserGroupMst createUserGroup(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody 
			UserGroupMst usergroupMst)
	{
		 return
				 companyMstRepo.findById(companymstid).map(companyMst-> {
					 usergroupMst.setCompanyMst(companyMst);
			
	           return userGroupMst.saveAndFlush(usergroupMst);
				 }).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
						  companymstid + " not found")); } 
	
	
	
	
	@GetMapping("{companymstid}/allusergroup")
	public List<UserGroupMst> retrieveAllUserGroup(@PathVariable(value="companymstid") String companymstid)
	{
		
		Optional<CompanyMst>companyMatOpt=companyMstRepo.findById(companymstid);
		return userGroupMst.findByCompanyMst(companyMatOpt.get());
	};
	
	
	@DeleteMapping("{companymstid}/usergroupmst/{id}")
	public void UserGroupMstDelete(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value = "id") String Id) {
		userGroupMst.deleteById(Id);

	}
	
	
	@GetMapping("{companymstid}/usergroupbyuserid/{userid}")
	public  @ResponseBody List<UserGroupMst> userMstDetail(@PathVariable(value="companymstid") String companymstid,@PathVariable(value = "userid") String userid){
		return userGroupMst.findByUserIdAndCompanyMst(userid,companymstid);
	}
	

	@GetMapping("{companymstid}/findusergroup/{groupid}")
	public  @ResponseBody List<UserGroupMst> getuserMstDetail(@PathVariable(value = "groupid") String groupid,@PathVariable(value="companymstid") String companymstid){
		return userGroupMst.findByGroupIdAndCompanyMst(groupid,companymstid);
	}
	

	
}


