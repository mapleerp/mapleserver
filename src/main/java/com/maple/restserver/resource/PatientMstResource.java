package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CurrencyMst;
import com.maple.restserver.entity.InsuranceCompanyMst;
import com.maple.restserver.entity.PatientMst;
import com.maple.restserver.entity.ReceiptModeMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CurrencyMstRepository;
import com.maple.restserver.repository.PatientMstRepository;
import com.maple.restserver.service.PatientMstService;
import com.maple.restserver.service.ReceiptModeMstService;


@RestController
@Transactional
public class PatientMstResource {
	@Autowired
	AccountHeadsRepository accountHeadsRepo;
	@Autowired
	CurrencyMstRepository currencyMstRepo;
	@Autowired
	private PatientMstRepository patientMstRepo;
	
	
	@Autowired
	PatientMstService patientMstService;
	
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@GetMapping("{companymstid}/patientmst")
	public List<PatientMst> retrieveAlldepartments()
	{
		return patientMstRepo.findAll();
		
	}
	@PostMapping("{companymstid}/patientmst")
	public ResponseEntity<Object>createUser(@Valid @RequestBody PatientMst patientmst1)
	{
		PatientMst saved=patientMstRepo.saveAndFlush(patientmst1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}
	@PostMapping("{companymstid}/patientmst/savepatientmst")
	public PatientMst createPatientMst(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody 
			 PatientMst  PatientMst)
		{
			
			
				Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
				
				CompanyMst companyMst = companyMstOpt.get();
				PatientMst.setCompanyMst(companyMst);
				
				PatientMst saved= patientMstRepo.save(PatientMst);
		
				return saved;
			} 

	@GetMapping("{companymstid}/patientmst/getpatientmstbyid/{patientmstid}")
	public PatientMst getPatientMstById(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "patientmstid")String patientmstid)
	{
		return patientMstRepo.findByCompanyMstIdAndId(companymstid,patientmstid);
	}
	
	@PutMapping("{companymstid}/patientmst/updatepatientmstbyid/{patientmstid}")
	public PatientMst updatePatientMstById(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "patientmstid")String patientmstid,
			@Valid @RequestBody PatientMst  patientMstRequest)
	{
		PatientMst patientMst = patientMstRepo.findByCompanyMstIdAndId(companymstid, patientmstid);
		patientMst.setAddress1(patientMstRequest.getAddress1());
		patientMst.setAddress2(patientMstRequest.getAddress2());
		patientMst.setContactPerson(patientMstRequest.getContactPerson());
		patientMst.setDateOfBirth(patientMstRequest.getDateOfBirth());
		patientMst.setGender(patientMstRequest.getGender());
		patientMst.setHospitalId(patientMstRequest.getHospitalId());
		patientMst.setInsuranceCard(patientMstRequest.getInsuranceCard());
		patientMst.setInsuranceCardExpiry(patientMstRequest.getInsuranceCardExpiry());
		patientMst.setNationalId(patientMstRequest.getNationalId());
		patientMst.setNationality(patientMstRequest.getNationality());
		patientMst.setPercentageCoverage(patientMstRequest.getPercentageCoverage());
		patientMst.setPhoneNumber(patientMstRequest.getPhoneNumber());
		patientMst.setPolicyType(patientMstRequest.getPolicyType());
		
		
		//=================added by anandu============
		patientMst.setAccountHeads(patientMstRequest.getAccountHeads());
		//patientMst.setPatientType(patientMstRequest.getPatientType());
		//==============================================
		patientMst = patientMstRepo.save(patientMst);
		
		/*
		 * modified due to change the customer mst into account heads========07/01/2022
		 */
//		CustomerMst customermst =null;
//		Optional<CustomerMst> customerMstop = customerMstRepo.findById(patientMst.getId());
//		if(customerMstop.isPresent())
//		{
//			 customermst = customerMstop.get();
//			customermst.setCustomerAddress(patientMst.getAddress1());
//			customermst.setCustomerContact(patientMst.getPhoneNumber());
//			customermst.setCustomerCountry(patientMst.getNationality());
//			customermst.setCustomerName(patientMst.getPatientName());
//			customermst = customerMstRepo.save(customermst);
//		}
		Optional<AccountHeads> accop = accountHeadsRepo.findById(patientMst.getId());
		if(accop.isPresent())
		{
			AccountHeads accountHeads = accop.get();
			accountHeads.setPartyAddress1(patientMst.getAddress1());
			accountHeads.setCustomerContact(patientMst.getPhoneNumber());
			accountHeads.setAccountName(patientMst.getPatientName());
			accountHeads.setCompanyMst(patientMst.getCompanyMst());
//			if(null != patientMst.getCompanyMst().getCurrencyName())
//			{
//			accountHeads.setCurrencyId(customermst.getCurrencyId());
//			}
			accountHeads = accountHeadsRepo.save(accountHeads);
		}
		return patientMst;
	}
	@GetMapping("{companymstid}/patientmst/getpatientmstbyname")
	public PatientMst getPatientMstByname(
			@PathVariable(value = "companymstid")String companymstid,
			@RequestParam(value = "patientname")String patientname)
	{
		return patientMstRepo.findByCompanyMstIdAndPatientName(companymstid,patientname);
	}
	@DeleteMapping("{companymstid}/patientmst/deletepatientmst/{patientmstid}")
	public void deletePatientMstById(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "patientmstid")String patientmstid)
	{
		patientMstRepo.deleteById(patientmstid);
	}
	
	@GetMapping("{companymstid}/patientmst/searchpatient")
	public @ResponseBody List<PatientMst> searchPatient(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("data") String searchstring) {
		searchstring = searchstring.replaceAll("%20", " ");
		return patientMstRepo.searchLikePatientByName("%" + searchstring + "%", companymstid);
	}
	
	
	
	//================new customer and account heads from patientmst=========anandu===============
	
	@PostMapping("{companymstid}/patientmstresource/cusandaccountfrompatient")
		public String cusAndAccountFromPatient(@PathVariable(value = "companymstid") String
				 companymstid,@Valid @RequestBody 
				 PatientMst  patientMst) {
		
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);	
		CompanyMst companyMst = companyMstOpt.get();
			
			return patientMstService.cusAndAccountFromPatient(patientMst,companyMst);
		}
	
	//===========================end new customer and account heads from patientmst===========
	
	
	// ==============================getPatientWithCustomer======================

		@GetMapping("{companymstid}/patientmstresource/findallpatientwithcustomer")
		public @ResponseBody List<PatientMst> findAllPatientWithCustomer(
				@PathVariable(value = "companymstid") String companymstid,
				@RequestParam("data") String customerid) {

			Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
			return patientMstRepo.findByNewPatientWithCustomer(companyMst.get(),customerid);

		}

	
}
