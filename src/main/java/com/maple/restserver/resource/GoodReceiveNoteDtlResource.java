package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.GoodReceiveNoteDtl;
import com.maple.restserver.entity.GoodReceiveNoteHdr;
import com.maple.restserver.entity.ItemBatchExpiryDtl;
import com.maple.restserver.entity.ItemPropertyConfig;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.PurchaseOrderDtl;
import com.maple.restserver.entity.PurchaseOrderHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.GoodReceiveNoteDtlRepository;
import com.maple.restserver.repository.GoodReceiveNoteHdrRepository;
import com.maple.restserver.repository.ItemBatchExpiryDtlRepository;
import com.maple.restserver.repository.ItemPropertyConfigRepository;
import com.maple.restserver.service.GoodReceiveNoteDtlService;
import com.maple.restserver.service.PurchaseDtlService;
import com.maple.restserver.service.PurchaseReportService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;

@RestController
@Transactional
public class GoodReceiveNoteDtlResource {
	
private static final Logger logger = LoggerFactory.getLogger(GoodReceiveNoteDtlResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	@Autowired
	ItemPropertyConfigRepository itemPropertyConfigRepo;
	@Autowired
	private GoodReceiveNoteDtlRepository  goodReceiveNoteDtlRepository;
	@Autowired
	private  GoodReceiveNoteDtlService  goodReceiveNoteDtlService;
	@Autowired
	PurchaseReportService purchaseReportService;
	@Autowired
	private GoodReceiveNoteHdrRepository    goodReceiveNoteHdrRepository;

	@Autowired
	private ItemBatchExpiryDtlRepository itemBatchExpiryDtlRepo;
	
	@Autowired
	private  CompanyMstRepository  companyMstRepository;
	
	 @Autowired
	 SaveAndPublishService saveAndPublishService;

	
	
	EventBus eventBus = EventBusFactory.getEventBus();

	@GetMapping("{companymstid}/goodreceivenotedtlresource/goodreceivenotedtl")
	public List<GoodReceiveNoteDtl> retrieveAllGoodReceiveNoteDtl() {
		return goodReceiveNoteDtlRepository.findAll();

	}

	
	

	/*
	 * // delete
	 * 
	 * @DeleteMapping(
	 * "{companymstid}/goodreceivenotedtlresource/deletegoodreceivenotedtl/{id}")
	 * public void DeleteGoodReceiveNoteDtl(@PathVariable(value = "id") String Id) {
	 * goodReceiveNoteDtlRepository.deleteById(Id); }
	 */
	
			
			@PostMapping("{companymstid}/goodreceivenotedtlresource/goodreceivenotedtls/{goodreceivenotehdrid}")
			public GoodReceiveNoteDtl createGoodReceiveNoteDtl(@PathVariable(value = "companymstid") String
					  companymstid,@PathVariable(value = "goodreceivenotehdrid") String
					  goodreceivenotehdrid,@Valid @RequestBody 
					  GoodReceiveNoteDtl  goodReceiveNoteDtl)


			{
				Optional<CompanyMst> companyMstOpt =companyMstRepository.findById(companymstid);
				Optional<GoodReceiveNoteHdr> goodReceiveNoteHdrOPt=goodReceiveNoteHdrRepository.findById(goodreceivenotehdrid);	
				goodReceiveNoteDtl.setCompanyMst(companyMstOpt.get());
				
				
				GoodReceiveNoteDtl addedItem = goodReceiveNoteDtlRepository.findByGoodReceiveNoteHdrIdAndItemIdAndBatchAndBarcodeAndUnitId(
						goodReceiveNoteHdrOPt.get().getId(), goodReceiveNoteDtl.getItemId(), goodReceiveNoteDtl.getBatch(),
						goodReceiveNoteDtl.getBarcode(), goodReceiveNoteDtl.getUnitId());
				if (null != addedItem && null != addedItem.getId()
						&& goodReceiveNoteDtl.getUnitId().equalsIgnoreCase(addedItem.getUnitId())) {

					addedItem.setQty(addedItem.getQty() + goodReceiveNoteDtl.getQty());
					
					addedItem.setMrp(goodReceiveNoteDtl.getMrp());
//					addedItem = goodReceiveNoteDtlRepository.saveAndFlush(addedItem);
					addedItem=saveAndPublishService.saveGoodReceiveNoteDtl(addedItem,mybranch);
					 logger.info("saveGoodReceiveNoteDtl send to KafkaEvent: {}", addedItem);

					return addedItem;

				}
				
				
				goodReceiveNoteDtl.setGoodReceiveNoteHdr(goodReceiveNoteHdrOPt.get());
				return goodReceiveNoteDtlRepository.saveAndFlush(goodReceiveNoteDtl);
				
				
			}	
			//here we deleting goodreceivenote detail by using its id
			
			
			  @DeleteMapping("{companymstid}/goodreceivenotedtlresource/goodreceivenotedelete/{goodreceivenotedetailId}")
			  public void DeletegoodReceiveNoteDtl(
			  
			  @PathVariable (value="goodreceivenotedetailId") String
			  goodreceivenotedetailId) {
			  
			  GoodReceiveNoteDtl goodReceiveNoteDtl =
			  goodReceiveNoteDtlRepository.getById(goodreceivenotedetailId);
			  GoodReceiveNoteHdr goodReceiveNoteHdr =
			  goodReceiveNoteDtl.getGoodReceiveNoteHdr();
			  goodReceiveNoteDtlRepository.deleteById(goodreceivenotedetailId);
			  goodReceiveNoteDtlService.updateGoodReceiveNoteDtlDisplaySerial(
			  goodReceiveNoteHdr.getId());
			  
			  
			  }
			  
			 
	

			@GetMapping("{companymstid}/goodreceivenotedtlresource/goodreceivenote/{goodReceiveNoteHdrId}/goodreceivenotedtlbyid")
			public List<GoodReceiveNoteDtl> findAllGoodReceiveNoteDtlByGoodReceiveNoteID(
					@PathVariable(value = "goodReceiveNoteHdrId") String goodReceiveNoteHdrId) {
				
				Optional<GoodReceiveNoteHdr> goodReceiveNoteHdr = goodReceiveNoteHdrRepository.findById(goodReceiveNoteHdrId);
				return goodReceiveNoteDtlRepository.findByGoodReceiveNoteHdr(goodReceiveNoteHdr.get());

			}
	
	
			

			@GetMapping("{companymstid}/goodreceivenotedtlresource/{goodreceivenotehdrid}/goodreceivenotedtlsummary")
			public SummarySalesDtl retrieveAllGoodReceiveNoteSummaryByGoodReceiveNoteHdrID(
					@PathVariable(value ="goodreceivenotehdrid") String goodreceivenotehdrid,
					@PathVariable(value ="companymstid") String companymstid
					
					) {
				Double objList =  goodReceiveNoteDtlRepository.findSumGoodReceiveNoteDetail(goodreceivenotehdrid);
				

				SummarySalesDtl summary = new SummarySalesDtl();
				summary.setTotalQty(objList);
				
				 
				return summary;

			}
			/*
			 * @DeleteMapping(
			 * "{companymstid}/goodreceivenotedtlresource/goodreceivenotedtl/{goodreceivenotedetailId}")
			 * public void DeleteGoodReceiveNoteDtl(@PathVariable
			 * (value="goodreceivenotedetailId") String goodreceivenotedetailId) {
			 * goodReceiveNoteDtlRepository.deleteById(goodreceivenotedetailId);
			 * 
			 * }
			 */
			
			
			@GetMapping("{companymstid}/goodreceivenotedtlresource/goodreceivenotehdr/{goodreceivenoteHdrId}/goodreceivenotedtl")
			public List<GoodReceiveNoteDtl> findAllGoodReceiveNoteDtlByGoodReceiveNoteHdrID(
					@PathVariable(value = "goodreceivenoteHdrId") String goodreceivenoteHdrId) {
				return goodReceiveNoteDtlRepository.findByGoodReceiveNoteHdrId(goodreceivenoteHdrId);

			}
			
			
			
			
			
			
			
			
			/*
			 * @GetMapping(
			 * "{companymstid}/purchaseorderhdr/{purchaseorderHdrId}/purchaseorderdtl")
			 * public List<GoodReceiveNoteDtl>
			 * findAllGoodReceiveNoteDtlByGoodReceiveNoteHdrID(
			 * 
			 * @PathVariable(value = "purchaseorderHdrId") String purchaseorderHdrId) {
			 * return
			 * goodReceiveNoteDtlRepository.findByPurchaseOrderHdrId(purchaseorderHdrId);
			 * 
			 * }
			 */
			
			
}
