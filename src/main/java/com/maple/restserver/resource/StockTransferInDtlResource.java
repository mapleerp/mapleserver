package com.maple.restserver.resource;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchExpiryDtlRepository;
import com.maple.restserver.repository.StockTransferInDtlRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.StockTransferInDtlService;
import com.maple.restserver.utils.SystemSetting;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.entity.ItemBatchExpiryDtl;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.StockTransferInDtl;
import com.maple.restserver.entity.StockTransferInHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.report.entity.StockTransferInReport;

@RestController

public class StockTransferInDtlResource {
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	private ItemBatchExpiryDtlRepository itemBatchExpiryDtlRepo;
	@Autowired
	private StockTransferInDtlRepository stockTransferInDtlRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	StockTransferInDtlService stockTransferInDtlService;
	
	@Autowired 
	SaveAndPublishService saveAndPublishService;



	@GetMapping("{companymstid}/stocktransferindtl")
	public List<StockTransferInDtl> retrieveAllStockTransferInDtl(
			@PathVariable(value = "companymstid") String companymstid) {

		return stockTransferInDtlRepository.findByCompanyMst(companymstid);

	}

	@PostMapping("{companymstid}/stocktransferindtl")
	public StockTransferInDtl createSaleTransferHdr(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody StockTransferInDtl stockTransferInDtl) {

		return companyMstRepo.findById(companymstid).map(companyMst -> {
			stockTransferInDtl.setCompanyMst(companyMst);
//			StockTransferInDtl saved = stockTransferInDtlRepository.saveAndFlush(stockTransferInDtl);
			StockTransferInDtl saved=saveAndPublishService.saveStockTransferInDtl(stockTransferInDtl,mybranch,stockTransferInDtl.getStockTransferInHdr().getFromBranch());
			if (null!=saved)
			{
				saveAndPublishService.publishStockTransferToKafkaEvent(saved, mybranch, KafkaMapleEventType.STOCKTRANSFERINDTL, stockTransferInDtl.getStockTransferInHdr().getFromBranch(),stockTransferInDtl.getId());
			}
			else {
				return null;
			}
			if (stockTransferInDtl.getBatch().equalsIgnoreCase(MapleConstants.Nobatch)) {
				ItemBatchExpiryDtl itemBatchExpiryDtl = new ItemBatchExpiryDtl();
				itemBatchExpiryDtl.setBatch(saved.getBatch());
				itemBatchExpiryDtl.setCompanyMst(companyMst);
				itemBatchExpiryDtl.setExpiryDate(saved.getExpiryDate());
				itemBatchExpiryDtl.setItemId(saved.getItemId().getId());
				itemBatchExpiryDtl.setStockTransferInDtl(saved);
				itemBatchExpiryDtl.setUpdatedDate(ClientSystemSetting.getSystemDate());
				//itemBatchExpiryDtlRepo.save(itemBatchExpiryDtl);
				itemBatchExpiryDtl=saveAndPublishService.saveItemBatchExpiryDtl(itemBatchExpiryDtl, mybranch);
				if (null!=itemBatchExpiryDtl)
				{
					saveAndPublishService.publishObjectToKafkaEvent(itemBatchExpiryDtl,
							mybranch, KafkaMapleEventType.ITEMBATCHEXPIRYDTL,
							KafkaMapleEventType.SERVER,itemBatchExpiryDtl.getId());
				}
				else
				{
					return null;
				}
			}

			return saved;

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	@GetMapping("{companymstid}/stocktransferinhdr/{stocktransferinhdrId}/stocktransferindtl")
	public List<StockTransferInDtl> retrieveAllSockTransferInDtlByHdrID(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "stocktransferinhdrId") String stocktransferinhdrId) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		return stockTransferInDtlRepository.findBystockTransferInHdrIdAndCompanyMst(stocktransferinhdrId, companyMst);

	}

	@GetMapping("{companymstid}/stocktransfeindtlresource/getstocktransferindtlreport/{vouchernumber}/{branchcode}")
	public List<StockTransferInDtl> getStockTransferInDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("vouchernumber") String vouchernumber, @PathVariable("branchcode") String branchcode) {
		// java.sql.Date fdate = SystemSetting.StringToSqlDate(fromdate,"yyyy-MM-dd");
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);

		return stockTransferInDtlRepository
				.findByCompanyMstAndStockTransferInHdrVoucherNumberAndStockTransferInHdrBranchCode(companyMstOpt.get(),
						vouchernumber, branchcode);

	}

	@GetMapping("{companymstid}/stocktransfeindtlresource/stocktransferindetailreport/{branchcode}")
	public List<StockTransferInReport> getTransferInDetailReport(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate) {

		Date fdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		Date tdate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		return stockTransferInDtlService.getTransferInDetailReport(companyMst, branchcode, fdate, tdate);

	}
	
	@GetMapping("{companymstid}/stocktransfeindtlresource/stocktransferindetailreportbyfrombranch/"
			+ "{branchcode}/{frombranch}")
	public List<StockTransferInReport> getTransferInDetailReportByFromBranch(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, 
			@PathVariable("frombranch") String frombranch,
			@RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate) {

		Date fdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		Date tdate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		return stockTransferInDtlService.getTransferInDetailReportByFromBranch(companyMst, branchcode,frombranch, fdate, tdate);

	}
	
	
	@GetMapping("{companymstid}/stocktransfeindtlresource/stocktransferindetailreportbyfrombranchandcategory/{branchcode}/{frombranch}")
	public List<StockTransferInReport> getTransferInDetailReportByFromBranchAndCategory(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, 
			@PathVariable("frombranch") String frombranch,
			@RequestParam("categoryids") String categoryids,

			@RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate) {
		
		String[] array = categoryids.split(";");


		Date fdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		Date tdate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		return stockTransferInDtlService.getTransferInDetailReportByFromBranch(companyMst, branchcode,frombranch, array,fdate, tdate);

	}
	
	
	@GetMapping("{companymstid}/stocktransfeindtlresource/stocktransferindetailreportbycategory/{branchcode}")
	public List<StockTransferInReport> getTransferInDetailReportByCategory(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, 
			@RequestParam("categoryids") String categoryids,

			@RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate) {
		
		String[] array = categoryids.split(";");


		Date fdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		Date tdate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		return stockTransferInDtlService.getTransferInDetailReportByCategory(companyMst, branchcode,array,fdate, tdate);

	}
}
