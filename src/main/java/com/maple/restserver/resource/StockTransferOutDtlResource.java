package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.StockTransferOutDltdDtl;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.StockTransferOutDltdDtlRepository;
import com.maple.restserver.repository.StockTransferOutDtlRepository;
import com.maple.restserver.repository.StockTransferOutHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.StockTransferOutDtlService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class StockTransferOutDtlResource {
	
	@Value("${mybranch}")
	private String mybranch;
	

	@Autowired 
	SaveAndPublishService saveAndPublishService;

	@Autowired
	private StockTransferOutDtlRepository stockTransferOutDtlRepo;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	private StockTransferOutHdrRepository stockTransferOutHdrRepo;
	
	//-----------------version 6.8 surya
	@Autowired
	private StockTransferOutDltdDtlRepository stockTransferOutDltdDtlRepository;
	
	@Autowired
	ItemMstRepository itemMstRepository;
	
	//-----------------version 6.8 surya end


	@GetMapping("{companymstid}/stocktransferoutdtl")
	public List<StockTransferOutDtl> retrieveAllStockTransferOutDtl(@PathVariable(value = "companymstid") String
			  companymstid) {
		
	 
		return stockTransferOutDtlRepo.findByCompanyMst(companymstid);
	}

	

	@PostMapping("{companymstid}/stocktransferoutdtl")
	public StockTransferOutDtl createstockTransferOutDtl(@PathVariable(value = "companymstid") String
			  companymstid,
			  @Valid @RequestBody StockTransferOutDtl stockTransferOutDtl) {
		StockTransferOutHdr stockTransferOutHdr=stockTransferOutDtl.getStockTransferOutHdr();
//		return saveAndPublishService.saveStockTransferOutDtl(stockTransferOutDtl,mybranch,stockTransferOutHdr.getToBranch());
		return stockTransferOutDtlRepo.save(stockTransferOutDtl);

	}

	

	@DeleteMapping("{companymstid}/stocktransferoutdtl/{stocktransferoutdtlid}")
	public void stocktransferDtlDelete(@PathVariable String stocktransferoutdtlid,
			//-----------------version 6.8 surya

			@PathVariable(value="companymstid")String companymstid
			//-----------------version 6.8 surya end
			) {
		//-----------------version 6.8 surya
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);

		
		Optional<StockTransferOutDtl> stockTransferOutDtlOpt = stockTransferOutDtlRepo.findById(stocktransferoutdtlid);
		StockTransferOutDtl stockTransferOutDtl = stockTransferOutDtlOpt.get();
		if(null != stockTransferOutDtl)
		{
			StockTransferOutDltdDtl stockTransferOutDltdDtl = new StockTransferOutDltdDtl();
			stockTransferOutDltdDtl.setBranchCode(stockTransferOutDtl.getStockTransferOutHdr().getFromBranch());
			stockTransferOutDltdDtl.setCompanyMst(companyMst.get());
			stockTransferOutDltdDtl.setItemId(stockTransferOutDtl.getItemId().getId());
			stockTransferOutDltdDtl.setMrp(stockTransferOutDtl.getMrp());
			stockTransferOutDltdDtl.setQty(stockTransferOutDtl.getQty());
			stockTransferOutDltdDtl.setUnitId(stockTransferOutDtl.getUnitId());
			stockTransferOutDltdDtl.setStockTransferOutHdr(stockTransferOutDtl.getStockTransferOutHdr());
			
			stockTransferOutDltdDtlRepository.saveAndFlush(stockTransferOutDltdDtl);
			
		}
		//-----------------version 6.8 surya end

		stockTransferOutDtlRepo.deleteById(stocktransferoutdtlid);
		

	}
	
	
/*Commented 	by regy - May 9th
 * @GetMapping("{companymstid}/stocktransferoutdtl/updatestocktransfer/{hdrid}")
	public void updateSerialNumber(
			@PathVariable (value="hdrid")String hdrid) {
		List<StockTransferOutDtl> stkList = stockTransferOutDtlRepo.findByStockTransferOutHdrId(hdrid);
		for(int i = 0;i <stkList.size();i++)
		{
			StockTransferOutDtl stockTransferOutDtl = stkList.get(i);
			stockTransferOutDtl.setSlNo(i+1);
			stockTransferOutDtlRepo.save(stockTransferOutDtl);
			
		}
		

	}
	*/
	
	//here we calculating the sum of amount in stocktransferOutdetail

	@GetMapping("{companymstid}/stocktransferoutdtl/{stocktransferouthdrId}/sumTotalStockTranOut")
	public double retriveSumOfTotalAmount(
			@PathVariable(value ="stocktransferouthdrId") String stocktransferouthdrId) {
		double totalAmount =  stockTransferOutDtlRepo.findSumAmount(stocktransferouthdrId);
		return totalAmount;

	}
	
	@GetMapping("{companymstid}/stocktransferoutdtl/stocktransferoutdtlbyvoucher/{voucherno}/{branchcode}")
	public List<StockTransferOutDtl> getStockTransferOutDtlByVoucherNumber(
			@PathVariable(value="voucherno")String voucherno,
			@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="branchcode")String branchcode) {
		
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		return stockTransferOutDtlRepo.findByCompanyMstAndStockTransferOutHdrVoucherNumberAndStockTransferOutHdrFromBranch(companyMst.get(),voucherno,branchcode);

	}
	
	@GetMapping("{companymstid}/stocktransferoutdtl/totalqtybyitemidandbatch/{itemid}/{batch}/{hdrid}")
	public List<StockTransferOutDtl> getStockTransferOutDtlByItemIdAndBatch(
			@PathVariable(value="itemid")String itemid,
			@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="hdrid")String hdrid,
			@PathVariable(value="batch")String batch) {
		
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		
		Optional<ItemMst> itemmst=itemMstRepository.findById(itemid);
		
		return stockTransferOutDtlRepo.findByStockTransferOutHdrIdAndItemIdAndBatch(hdrid, itemmst.get(), batch);

	}
	

}
