package com.maple.restserver.resource;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.entity.IntentInDtl;
import com.maple.restserver.entity.IntentInHdr;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.report.entity.IntentInReport;
import com.maple.restserver.repository.IntentInDtlRepository;
import com.maple.restserver.repository.IntentInHdrRepository;
import com.maple.restserver.service.IntentInReportService;
import com.maple.restserver.service.IntentInServiceImpl;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class IntentInDtlResource {
	
private static final Logger logger = LoggerFactory.getLogger(AccountReceivableResource.class);
	
	@Value("${mybranch}")
	private String mybranch;

	
	@Autowired
	IntentInDtlRepository intentInDtlRepo;
	
	@Autowired
	IntentInServiceImpl intentInServiceImpl;
	
	@Autowired
	IntentInReportService intentInReportService;
	
	 @Autowired
	 SaveAndPublishService saveAndPublishService;

	
	
	

@Autowired
IntentInHdrRepository intentInHdrRepository;

	@GetMapping("/{companymstid}/intentindtl/{id}/intentindtls")
	public List<IntentInDtl> getintentInDtlByHdrId(
			@PathVariable(value = "id") String id
			) {
		return intentInDtlRepo.findByIntentInHdrId(id);

	}
	
	@GetMapping("/{companymstid}/intentindtl/getpendingintent")
	public List<Object> getPendingIntent(@RequestParam("rdate") String rdate,@RequestParam("tdate") String tdate		
			) {
		java.util.Date fdate = SystemSetting.StringToUtilDate(rdate,"dd-MM-yyyy");
		java.util.Date todate = SystemSetting.StringToUtilDate(tdate,"dd-MM-yyyy");

		return intentInServiceImpl.getPendingIntent(fdate,todate);

	}
	@GetMapping("/{companymstid}/intentindtl/getpendingintentbybranch/{branchCode}")
	public List<Object> getPendingIntentByBranch(		
			@PathVariable(value = "branchCode") String branchCode
			) {
		return intentInHdrRepository.getPendingIntentByBranch(branchCode);

	}
	
	@GetMapping("{companymstid}/intentindtl/updateallintent")
	public String updateAllIntentInDtl(
			
			@PathVariable(value = "companymstid") String companymstid )
		
	{
			
		List<IntentInDtl> itemList = intentInDtlRepo.findAll();
		Iterator iter = itemList.iterator();
		while (iter.hasNext()) 
		{
			IntentInDtl intentDtl = (IntentInDtl) iter.next();
			intentDtl.setStatus("COMPLETED");
			intentDtl = intentInDtlRepo.save(intentDtl);
		}
		List<IntentInHdr> intenHdrList = intentInHdrRepository.findAll();
		Iterator iter1 = intenHdrList.iterator();
		while (iter1.hasNext()) 
		{
			IntentInHdr intentInHdr = (IntentInHdr) iter1.next();
			intentInHdr.setIntentStatus("COMPLETED");
			intentInHdr = intentInHdrRepository.save(intentInHdr);
		}
		return "Success"; 
	}
	
	
	@PutMapping("{companymstid}/intentindtl/{intentid}/updateintent")
	public IntentInDtl updateIntentInDtl(
			@PathVariable(value="intentid") String intentid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody IntentInDtl  intentInDtlRequest)
	{
			
		IntentInDtl intentInDtl = intentInDtlRepo.findById(intentid).get();
		
		intentInDtl.setStatus(intentInDtlRequest.getStatus());
		
		intentInDtl.setAllocatedQty(intentInDtlRequest.getAllocatedQty());
//		intentInDtl = intentInDtlRepo.saveAndFlush(intentInDtl);
		intentInDtl =saveAndPublishService.saveIntentInDtl(intentInDtlRequest,mybranch);
		 logger.info("saveIntentInDtl send to KafkaEvent: {}", intentInDtl);
		return intentInDtl; 
	}
	
	
	@GetMapping("{companymstid}/intentdtl/itemwiseintentsummary/{branchcode}")
	public List<IntentInReport> getItemWiseIntentSummary(
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "companymstid") String companymstid ,

			@RequestParam("date") String date) {
		
		java.util.Date fdate = SystemSetting.StringToUtilDate(date,"yyyy-MM-dd");

		return intentInReportService.getItemWiseIntentSummary(branchcode,companymstid,fdate );

	}
	
}
