package com.maple.restserver.resource;

import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CategoryManagementMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CategoryManagementMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
@Resource
@Transactional
public class CategoryManagementMstResource {

	@Autowired
	CategoryManagementMstRepository categoryManagementMstRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@PostMapping("{companymstid}/categorymanagementmst/savecategorymanagementmst")
	public CategoryManagementMst createCategoryManagementMst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody CategoryManagementMst categoryManagementMst) {
		
	 	return companyMstRepo.findById(companymstid).map(companyMst-> {
	 		categoryManagementMst.setCompanyMst(companyMst);
		return categoryManagementMstRepo.save(categoryManagementMst);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found")); }
	
	
	@GetMapping("{companymstid}/categorymanagementmst/findallcategorymanagement")
	public  List<CategoryManagementMst> findallCategoryManagementMst(
			@PathVariable(value = "companymstid") String companymstid) {

		return categoryManagementMstRepo.findAll();
	}
	
	@GetMapping("{companymstid}/categorymanagementmst/getchildCategory/{catName}")
	public  List<CategoryManagementMst> getAllCategoryManagementMstWithParent(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value ="catName")String catName) {

		return categoryManagementMstRepo.getCategoryWithParent(catName);
	}
	@GetMapping("{companymstid}/categorymstresource/getparentcategory")
	public  @ResponseBody List<CategoryManagementMst> getParentcategory(	
			@PathVariable(value = "companymstid") String companymstid){
		return categoryManagementMstRepo.getParentCategory() ;
	}
	
	@GetMapping("{companymstid}/categorymanagementmst/getbycatidandsubcatid/{catid}/{subcatid}")
	public  CategoryManagementMst getByCategoryAndSubCategory(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value ="catid")String catid,
			@PathVariable(value = "subcatid")String subcatid) {

		return categoryManagementMstRepo.findByCategoryIdAndSubCategoryId(catid,subcatid);
	}
	@DeleteMapping("{companymstid}/categorymanagementmst/deletecategorymanagementmstbyid/{id}")
	public void categoryDelete(@PathVariable(value = "id") String Id) {
		categoryManagementMstRepo.deleteById(Id);

	}
	
	
}
