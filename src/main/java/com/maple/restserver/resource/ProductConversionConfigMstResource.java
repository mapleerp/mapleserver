package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductConversionConfigMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProductConfigurationMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class ProductConversionConfigMstResource {
private static final Logger logger = LoggerFactory.getLogger(ProductConversionConfigMstResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	
	 @Value("${serverorclient}")
		private String serverorclient;
	 EventBus eventBus = EventBusFactory.getEventBus();
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@Autowired
	ProductConfigurationMstRepository productConfigurationMstRepository;
	

	@PostMapping("{companymstid}/productconversionconfigurationmstresource/productconversionconfiguration")
	public ProductConversionConfigMst createProductConversionMstRepository(@Valid @RequestBody ProductConversionConfigMst productConversionConfigMst,
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		productConversionConfigMst.setCompanyMst(companyMst);
		//ProductConversionConfigMst saved = productConfigurationMstRepository.save(productConversionConfigMst);
		ProductConversionConfigMst saved =saveAndPublishService.saveProductConversionConfigMst(productConversionConfigMst, productConversionConfigMst.getBranchCode());
		logger.info("ProductConversionConfigMst send to KafkaEvent: {}", saved);
		Map<String, Object> variables = new HashMap<String, Object>();
		
		variables.put("companyid",companyMst);
		
		variables.put("voucherNumber",saved.getId());
		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("id",saved.getId());
		
		variables.put("branchcode", saved.getBranchCode());

		if(serverorclient.equalsIgnoreCase("REST")) {
			variables.put("REST",1);
		}else {
			variables.put("REST",0);
		}
		
		
		//variables.put("voucherDate", purchase.getVoucherDate());
		//variables.put("id",purchase.getId());
		variables.put("inet", 0);
		
		
		
		variables.put("WF", "forwardProductConfigMst");
		
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardProductConfigMst");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		
		
		variables.put("lmsqid", lmsQueueMst.getId());
		
		
		eventBus.post(variables);
		return saved;
	}
	
	
	
	@PutMapping("{companymstid}/productconversionconfigurationmstresource/productconversionconfigurationmstfinalsave/{productconversionconfigmstid}")
	public ProductConversionConfigMst ProductConversionMstUpdate(@PathVariable String productconversionconfigmstid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody ProductConversionConfigMst productconversionconfigurationRequest) {
		ProductConversionConfigMst productconversiononfigurationMst = productConfigurationMstRepository.findById(productconversionconfigmstid).get();

		productconversiononfigurationMst.setFromItemId(productconversionconfigurationRequest.getFromItemId());
		productconversiononfigurationMst.setToItemId(productconversionconfigurationRequest.getToItemId());
		
		//return productConfigurationMstRepository.save(productconversiononfigurationMst);
		return	saveAndPublishService.saveProductConversionConfigMst(productconversiononfigurationMst, productconversiononfigurationMst.getBranchCode());
	}
	

	@DeleteMapping("{companymstid}/productconversionconfigurationmstresource/productconversionconfigurationmsdelete/{id}")
	public void ProductConversionConfigMstDelete(
			@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value = "id") String Id) {
		productConfigurationMstRepository.deleteById(Id);

	}
	
	
	
	@GetMapping("{companymstid}/productconversionconfigurationmstresource/productconversionconfigurationmsshowall/{branchcode}")
	public List<ProductConversionConfigMst>getProductConversionConfigMst(
			@PathVariable(value ="branchcode")String branchcode)
	{
		return productConfigurationMstRepository.findAll();
	}
	

	@GetMapping("{companymstid}/productconversionconfigurationmstresource/makeproduction/{fromitemid}/{toitemid}/{branchcode}")
	public ProductConversionConfigMst  findByFromItemIdAndToItemId(@PathVariable(value = "fromitemid") String fromitemid ,
			@PathVariable(value = "branchcode") String branchcode ,
			@PathVariable(value = "toitemid") String  toitemid)
	{
	ProductConversionConfigMst productconversionconfigMst = productConfigurationMstRepository.findByFromItemIdAndToItemId(fromitemid,toitemid);
	
	return productconversionconfigMst;
	
	
	}
}
