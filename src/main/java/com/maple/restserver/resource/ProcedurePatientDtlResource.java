package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.ProcedurePatientDtl;
import com.maple.restserver.repository.ProcedurePatientDtlRepository;

@RestController
@Transactional
public class ProcedurePatientDtlResource {
	
	@Autowired
	private ProcedurePatientDtlRepository procedurePatientDtlRepository;
	
	@GetMapping("{companymstid}/procedurePatientDtl")
	public List<ProcedurePatientDtl> retrieveAllProcedurePatientDtl(){
		return procedurePatientDtlRepository.findAll();
	}
	
	@PostMapping("{companymstid}/procedurePatientDtl")
	public ResponseEntity<Object> createProcedurePatientDtlResource(@Valid @RequestBody 
			ProcedurePatientDtl procedurePatientDtl)
	{
		ProcedurePatientDtl saved = procedurePatientDtlRepository.saveAndFlush(procedurePatientDtl);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/(id)").buildAndExpand(saved.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}

}
