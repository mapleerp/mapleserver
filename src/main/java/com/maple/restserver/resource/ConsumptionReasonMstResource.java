package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ConsumptionReasonMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ConsumptionReasonMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;

@RestController
public class ConsumptionReasonMstResource {
	
private static final Logger logger = LoggerFactory.getLogger(ConsumptionReasonMstResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	
	

	EventBus eventBus = EventBusFactory.getEventBus();

	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
    @Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	ConsumptionReasonMstRepository  consumptionReasonMstRepository;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	
	
	@PostMapping("{companymstid}/consumptionreasonmstresource/addconsumptionreasonmst")
	public ConsumptionReasonMst createConsumptionReasonMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody ConsumptionReasonMst consumptionReasonMst) {
		return companyMstRepository.findById(companymstid).map(companyMst -> {
			consumptionReasonMst.setCompanyMst(companyMst);
//			ConsumptionReasonMst saved = consumptionReasonMstRepository.saveAndFlush(consumptionReasonMst);
			ConsumptionReasonMst saved =saveAndPublishService.saveConsumptionReasonMst(consumptionReasonMst,mybranch);
			 logger.info("ConsumptionReasonMst send to KafkaEvent: {}", saved);
			/*
			 * forward to cloud
			 */

			Map<String, Object> variables = new HashMap<String, Object>();
			
			variables.put("voucherNumber", saved.getId());
			variables.put("voucherDate", ClientSystemSetting.getSystemDate());
			
			variables.put("id", saved.getId());
			variables.put("inet", 0);
			variables.put("REST", 1);
			variables.put("companyid", saved.getCompanyMst());
			variables.put("branchcode", saved.getCompanyMst().getId());
			
			variables.put("WF", "forwardConsumptionReasonMst");
			
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			
			//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (java.util.Date)variables.get("voucherDate");
			java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			
//			String jobClass = getJobClass(workflow, lmsQueueMst);
//			if (null == jobClass) {
//				logger.info("jobClass not found for " + workflow);
//				return;
//			}
			lmsQueueMst.setJobClass("forwardConsumptionReasonMst");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());
			
			eventBus.post(variables);
			return saved;
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	
	@GetMapping("{companymstid}/consumptionreasonmstresource/findallconsumptionreasonmst")
	public List<ConsumptionReasonMst> retrieveAllConsumptionReasonMst(){
		return consumptionReasonMstRepository.findAll();
	}
	@GetMapping("{companymstid}/consumptionreasonmstresource/findallconsumptionreasonmstbyreason/{reason}")
	public ConsumptionReasonMst retrieveAllConsumptionReasonMstbyReason(
			@PathVariable(value = "reason") String reason
			){
		return consumptionReasonMstRepository.findByReason(reason);
	}
	@GetMapping("{companymstid}/consumptionreasonmstresource/findconsumptionreasonmst/{id}")
	public Optional<ConsumptionReasonMst>  retrieveConsumptionReasonMst(@PathVariable(value = "id") String Id){
		return consumptionReasonMstRepository.findById(Id);
	}
	

	@DeleteMapping("/{companymstid}/consumptionreasonmstresource/deleteconsumptionreasonmst/{id}")
	public void consumptionReasonMstDelete(@PathVariable(value = "id") String Id) {
		consumptionReasonMstRepository.deleteById(Id);

	}
	
}
