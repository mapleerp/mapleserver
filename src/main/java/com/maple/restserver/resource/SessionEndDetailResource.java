package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SessionEndDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SessionEndClosureMstRepository;
import com.maple.restserver.repository.SessionEndDetailRepository;

@RestController
@Transactional
public class SessionEndDetailResource {

	@Autowired
	SessionEndDetailRepository sessionEndDetailRepo;
	@Autowired
	SessionEndClosureMstRepository sessionendclosuremst;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	
	
	

	@PostMapping("{companymstid}/sessionendclosurehdr/{sessionendclosurehdrId}/sessionendclosuredtl")
	public SessionEndDtl createSessionEndClosureDtl(
			@PathVariable(value = "sessionendclosurehdrId") String sessionendclosurehdrId,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SessionEndDtl sessionendclosureDtlRequest) {

		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		return sessionendclosuremst.findById(sessionendclosurehdrId).map(sessionEndClosure -> {
			sessionendclosureDtlRequest.setSessionEndClosure(sessionEndClosure);
			sessionendclosureDtlRequest.setCompanyMst(companyOpt.get());
			return sessionEndDetailRepo.saveAndFlush(sessionendclosureDtlRequest);
		}).orElseThrow(() -> new ResourceNotFoundException("sessionendclosurehdrId " + sessionendclosurehdrId + " not found"));

	}

	@DeleteMapping("{companymstid}/sessionenddtldelete/{id}")
	public void SessionEndDtlDelete(@PathVariable(value = "id") String Id) {
		sessionEndDetailRepo.deleteById(Id);

	}


	@GetMapping("{companymstid}/sessiondtl/{hdrid}")
	public  @ResponseBody List<SessionEndDtl> sessionEndDtlbyHdrId(@PathVariable(value = "hdrid") String hdrid){
		return sessionEndDetailRepo.findBySessionEndClosureId(hdrid);
	}
	
	@GetMapping("{companymstid}/totalsessionendphysicalcash/{hdrid}")
	public Double totalsessionendphysicalcash(@PathVariable String hdrid){
		Double total =  sessionEndDetailRepo.gettotal(hdrid);
		if(null == total)
		{
			total= 0.0;
		}
		return total;
	}
}
