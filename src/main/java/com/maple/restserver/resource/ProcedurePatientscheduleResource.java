package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.ProcedurePatientschedule;
import com.maple.restserver.repository.ProcedurePatientscheduleRepository;

@RestController
@Transactional
public class ProcedurePatientscheduleResource {
	
	@Autowired
	private ProcedurePatientscheduleRepository procedurePatientscheduleRepository;
	
	@GetMapping("{companymstid}/procedurepatientschedule")
	public List<ProcedurePatientschedule> retrieveAllProcedurePatientschedule(){
		return procedurePatientscheduleRepository.findAll();
	}
	
	@PostMapping("{companymstid}/procedurepatientschedule")
	public ResponseEntity<Object> createProcedurePatientschedule(@Valid @RequestBody 
			ProcedurePatientschedule procedurePatientschedule)
	{
		ProcedurePatientschedule saved = procedurePatientscheduleRepository.saveAndFlush(procedurePatientschedule);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/(id)").buildAndExpand(saved.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}

}
