package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.GroupMst;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class MultiUnitMstResource {
private static final Logger logger = LoggerFactory.getLogger(MultiUnitMstResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	MultiUnitMstRepository multyMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	

	
	@PostMapping("{companymstid}/multiunitmst")
	public MultiUnitMst createMultyUnit(@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody MultiUnitMst multyUnitMst)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			multyUnitMst.setCompanyMst(companyMst);
		
//			MultiUnitMst multyUnitMstSaves = multyMstRepository.saveAndFlush(multyUnitMst);
			MultiUnitMst multyUnitMstSaves =saveAndPublishService.saveMultiUnitMst(multyUnitMst, mybranch);
			logger.info("multyUnitMstSaves send to KafkaEvent: {}", multyUnitMstSaves);
			 Map<String, Object> variables = new HashMap<String, Object>();

				variables.put("voucherNumber", multyUnitMstSaves.getId());

				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("inet", 0);
				variables.put("id", multyUnitMstSaves.getId());

				variables.put("companyid", multyUnitMstSaves.getCompanyMst());
				variables.put("branchcode", multyUnitMstSaves.getBranchCode());

				variables.put("REST", 1);
				
				
				variables.put("WF", "forwardMultiUnitMst");
				
				
				String workflow = (String) variables.get("WF");
				String voucherNumber = (String) variables.get("voucherNumber");
				String sourceID = (String) variables.get("id");


				LmsQueueMst lmsQueueMst = new LmsQueueMst();

				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
				
				//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
				
				
				java.util.Date uDate = (Date) variables.get("voucherDate");
				java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
				lmsQueueMst.setVoucherDate(sqlVDate);
				
				
				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
				lmsQueueMst.setVoucherType(workflow);
				lmsQueueMst.setPostedToServer("NO");
				lmsQueueMst.setJobClass("forwardMultiUnitMst");
				lmsQueueMst.setCronJob(true);
				lmsQueueMst.setJobName(workflow + sourceID);
				lmsQueueMst.setJobGroup(workflow);
				lmsQueueMst.setRepeatTime(60000L);
				lmsQueueMst.setSourceObjectId(sourceID);
				
				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
				
				variables.put("lmsqid", lmsQueueMst.getId());
				
				eventBus.post(variables);
			
			return  multyUnitMstSaves;
			
			
		

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	
		/*
		 * Work Flow
		 * 
		 * forwardMultiUnitMst
		 */
	
		

		
	}
	
	@GetMapping("{companymstid}/allmultyunitmst")
	public List<MultiUnitMst> retrieveAllMulyUnitMst()
	{
		return multyMstRepository.findAll();
	};
	
	@GetMapping("{companymstid}/multiunitmst/{itemid}/multiunititemdtls")
	public List<MultiUnitMst> retrieveAllMultiUnitByItemId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid) {
		return multyMstRepository.findByCompanyMstIdAndItemId(companymstid,itemid );

	}
	@GetMapping("{companymstid}/multiunitmst/{itemid}/{unitName}/multiunititemdtls")
	public MultiUnitMst retrieveAllMultiUnitByItemIdAndUnit(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "unitName") String unitName) {
		return multyMstRepository.findByCompanyMstIdAndItemIdAndUnit2(companymstid, itemid, unitName);

	}
	@GetMapping("{companymstid}/multiunitmst/{itemid}/{unitName}/getbyprimaryunit")
	public MultiUnitMst retrieveAllMultiUnitByItemIdAndUnit1(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "unitName") String unitName) {
		return multyMstRepository.findByCompanyMstIdAndItemIdAndUnit1(companymstid, itemid, unitName);

	}

	@DeleteMapping("{companymstid}/multyunitmst/{id}")
	public void MultiUnitMstDelete(@PathVariable(value = "id") String Id) {
		multyMstRepository.deleteById(Id);

	}
	
	
	@PutMapping("{companymstid}/multiunitmstresource/updatepriceinmultiunitmst/{multiunitmstid}")
	public MultiUnitMst updatePriceInMultiUnitMst(
			@PathVariable(value = "multiunitmstid") String multiunitmstid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody MultiUnitMst multiUnitMstRequest) {

		MultiUnitMst multiUnitMst = multyMstRepository.findById(multiunitmstid).get();

			Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			
			multiUnitMst.setPrice(multiUnitMstRequest.getPrice());
			
			multiUnitMst = multyMstRepository.save(multiUnitMst);
			return multiUnitMst;
	}

}
