package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.accounting.repository.AccountClassRepository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.report.entity.ReceiptReports;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ReceiptDtlRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.service.ReceiptReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class ReceiptReportResource {

	
	@Autowired
	ReceiptHdrRepository receiptHdrRepository;
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
  AccountClassRepository acountClassRepository;
	@Autowired 
	ReceiptReportService receiptReportService;
	
	@Autowired
	ReceiptDtlRepository receiptDtlRepo;

	
	@GetMapping("{companymstid}/receiptreport/receiptreporthdr/{branchcode}")
	public List<ReceiptHdr> receiptReportHdrs(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable (value = "branchcode") String branchcode ,
			@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate){
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
	
		
		return receiptReportService.fetchReceiptReportHdr(companyMstOpt.get(),branchcode,fromDate,toDate);
	}
	
	
	@GetMapping("{companymstid}/receipthdr/{receiptId}/receiptdtlreports")
	
	 public List<ReceiptDtl> getAllPaymentDtlByPaymentHdrId(@PathVariable (value = "receiptId") String receipthdrId,
            Pageable pageable) {
		return receiptReportService.fetchByReceiptHdrId(receipthdrId);
	}
	
	

	@GetMapping("{companymstid}/receipthdr/exportreceiptdtlreports/{branchcode}")
	
	 public List<ReceiptReports> exportReceiptReport(@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable (value = "branchcode") String branchcode ,
				@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate) {
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		
		
		return receiptReportService.exportReceiptReport(companyMstOpt.get(),fromDate,toDate,branchcode);
	}
	
	
	
	@GetMapping("{companymstid}/receipthdr/receiptreportprinting/{branchcode}")
	 public List<ReceiptReports> receiptReportPrinting(@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable (value = "branchcode") String branchcode ,
				@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate) {
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		
		
		return receiptReportService.receiptReportPrinting(companyMstOpt.get(),fromDate,toDate,branchcode);
	}
	
	
 
	
 
	
	@Transactional
	@DeleteMapping("{companymstid}/receiptreport/deletereceiptreport/{vouchernumber}")
	public void receiptHdrDelete(@PathVariable(value = "vouchernumber") String vouchernumber) {
		
		acountClassRepository.deleteBySourceVoucherNumber(vouchernumber);
		
		receiptHdrRepository.deleteByVoucherNumber(vouchernumber);

	}
}
