package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseOrderHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.TableOccupiedMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.TableOccupiedMstRepository;

@RestController
@Transactional
public class TableOccupiedMstResource {
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepo;
	@Autowired
	TableOccupiedMstRepository tableOccupiedMstRepo;

	@PostMapping("{companymstid}/tableoccuiedmst")
	public TableOccupiedMst createTableOccupiedMst(@Valid @RequestBody TableOccupiedMst tableOccupiedMst,
			@PathVariable (value = "companymstid") String companymstid)
	{
		
		  Optional<CompanyMst> comapnyMstOpt = CompanyMstRepository.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			tableOccupiedMst.setCompanyMst(companyMst);
			 
			TableOccupiedMst saved=tableOccupiedMstRepo.saveAndFlush(tableOccupiedMst);
	 	    return saved;
	}
	@PutMapping("{companymstid}/tableoccuiedmst/updatetableoccupiedmst")
	public TableOccupiedMst updateTableOccupiedMst(@PathVariable(value ="companymstid")String companymstid,
			@Valid @RequestBody TableOccupiedMst tableOccupiedMstReq)
	{
		TableOccupiedMst tableOccupiedMst = tableOccupiedMstRepo.findById(tableOccupiedMstReq.getId()).get();
		tableOccupiedMst.setWaiterId(tableOccupiedMstReq.getWaiterId());
		tableOccupiedMst.setOrderedTime(tableOccupiedMstReq.getOrderedTime());
		tableOccupiedMst.setSalesTransHdr(tableOccupiedMstReq.getSalesTransHdr());
		tableOccupiedMst.setBillingTime(tableOccupiedMstReq.getBillingTime());
		tableOccupiedMst.setInvoiceAmount(tableOccupiedMstReq.getInvoiceAmount());
		
		return tableOccupiedMstRepo.saveAndFlush(tableOccupiedMst);
				
	}
	
	
	@GetMapping("{companymstid}/tableoccupiedmst/gettableoccupiedmstbysaleshdrid/{hdrid}")
	public TableOccupiedMst getTableBySalesHdrId(@PathVariable(value ="companymstid")String companymstid,
			@PathVariable(value="hdrid")String hdrid)
	{

		SalesTransHdr salesTransHdr = salesTransHdrRepo.findById(hdrid).get();
		
			return tableOccupiedMstRepo.findBySalesTransHdr(salesTransHdr);
		
	}
	@GetMapping("{companymstid}/tableoccupiedmst/gettableoccupiedmstbytable/{tableid}")
	public TableOccupiedMst getTable(@PathVariable(value ="companymstid")String companymstid,
			@PathVariable(value="tableid")String tableid)
	{
		List <TableOccupiedMst> tableList = tableOccupiedMstRepo.findByTableId(tableid);
		int count = tableList.size();
		if(count>0)
		{
		return tableList.get(count-1);
		}
		else
			return null;
		
	}
	
}
