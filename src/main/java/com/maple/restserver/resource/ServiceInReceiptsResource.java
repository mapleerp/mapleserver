package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ServiceInHdr;
import com.maple.restserver.entity.ServiceInReceipt;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ServiceInHdrRepository;
import com.maple.restserver.repository.ServiceInReceiptsRepository;
@RestController
@Transactional
public class ServiceInReceiptsResource {

	@Autowired
	private ServiceInReceiptsRepository serviceInReceiptsRepository;
	
	@Autowired
	ServiceInHdrRepository serviceInHdrRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("{companymstid}/serviceinreceiptresource/serviceinreceiptbyserviceinhdr/{serviceinhdrid}")
	public List<ServiceInReceipt> retrieveServiceInReceiptByHdrId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "serviceinhdrid") String serviceinhdrid){
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		if(!companyOpt.isPresent())
		{
			return null;
		}
		
		Optional<ServiceInHdr> serviceInHdr = serviceInHdrRepository.findById(serviceinhdrid);
		if(!serviceInHdr.isPresent())
		{
			return null;
		}	
		
		return serviceInReceiptsRepository.findByServiceInHdrAndCompanyMst(serviceInHdr.get(),companyOpt.get());
	}
	
	
	@PostMapping("{companymstid}/serviceinreceiptresource/saveserviceinreceipt/{serviceinhdrid}")
	public ServiceInReceipt createServiceInReceipt(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "serviceinhdrid") String serviceinhdrid,
			@Valid @RequestBody ServiceInReceipt serviceInReceipt)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			
			Optional<ServiceInHdr> serviceInHdr = serviceInHdrRepository.findById(serviceinhdrid);
			if(!serviceInHdr.isPresent())
			{
				return null;
			}
			
			serviceInReceipt.setCompanyMst(companyMst);
			serviceInReceipt.setServiceInHdr(serviceInHdr.get());
		
		
		return serviceInReceiptsRepository.saveAndFlush(serviceInReceipt);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));

	}
	
	
	@DeleteMapping("{companymstid}/serviceinreceiptresource/deleteserviceinreceipt/{id}")
	public void ServiceInReceiptDelete(@PathVariable(value = "id") String Id) {
		serviceInReceiptsRepository.deleteById(Id);
		serviceInReceiptsRepository.flush();

	}
	
	@GetMapping("{companymstid}/serviceinreceiptresource/saveserviceinreceipt/{serviceinhdrid}")
	Double retrieveServiceInReceiptAmount(
			@PathVariable(value = "companymstid") String  companymstid,
			@PathVariable(value ="serviceinhdrid") String  serviceinhdrid
			 ){
		return serviceInReceiptsRepository.retrieveServiceInReceiptAmount(serviceinhdrid,companymstid);
	}
	

}
