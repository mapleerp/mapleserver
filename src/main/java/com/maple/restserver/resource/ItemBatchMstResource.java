package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemBranchHdr;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ItemNutrition;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SchOfferAttrInst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.PharmacyItemWiseSalesReport;
import com.maple.restserver.report.entity.PoliceReport;
import com.maple.restserver.report.entity.StockMigrationReport;
import com.maple.restserver.report.entity.StockReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.service.ItemBatchDtlDailyService;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.service.ItemBatchMstService;
import com.maple.restserver.service.task.ProductionFinishedGoodsStock;
import com.maple.restserver.utils.SystemSetting;

@CrossOrigin("http://localhost:4200")
@RestController
@Transactional
public class ItemBatchMstResource {
	private static final Logger logger = LoggerFactory.getLogger(ItemBatchMstResource.class);
	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepo;
	@Autowired
	ProductionFinishedGoodsStock productionFinishedGoodsStock;
	@Autowired
	private ItemBatchMstRepository itemBatchMstRepository;
	
	@Autowired
	private ItemMstRepository itemMstRepository;
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	@Value("${ENABLENEGATIVEBILLING}")
	private String ENABLENEGATIVEBILLING;
	
	
	@Autowired
	ItemBatchMstService itemBatchMstService;
	
	@GetMapping("{companymstid}/itembatchmst")
	public List<ItemBatchMst> retrieveAllDailyStock(@PathVariable(value = "companymstid") String
			  companymstid){
		return itemBatchMstRepository.findByCompanyMstId(companymstid);
	}
	
	@GetMapping("{companymstid}/itembatchmst/{itemid}/{reqqty}")
	public JSONArray retrieveAllItemBatchMSt(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "itemid") String
			  itemid,@PathVariable(value = "reqqty") Double
			  reqqty){
		Map<String, Double> map = new HashMap<String, Double>();
		//map =	
		JSONArray jsonArray = itemBatchDtlService.getBatchWiseQty(itemid, reqqty);
		
//		for(int i=0; i < jsonArray.length(); i++)
//		{
//			JSONObject jsonObj = jsonArray.getJSONObject(i);
//		}
//		
//		map.put("", value)
		return jsonArray;
	}
//	
	
	 @GetMapping("{companymstid}/itembatchmst/updatestockfromdtl")
	 public  String updateItemBatchMstStockFromDtlfromDate(@PathVariable(value="companymstid") String companymstid,
			
			 @RequestParam(value="fdate")String fdate){
		 
		 Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		 CompanyMst companyMst =companyMstOpt.get();
		 
			java.sql.Date date = SystemSetting.StringToSqlDate(fdate, "yyyy-MM-dd");

		 try {
			 itemBatchDtlService.updateItemBatchMstStockFromDtl(companyMst , date);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		 
	 	return "Stock update done";
	 	
	 }
	
		
	@PostMapping("{companymstid}/itembatchmst")
	public ItemBatchMst createItemBatchMst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			ItemBatchMst itemBatchMst)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			itemBatchMst.setCompanyMst(companyMst);
		return itemBatchMstRepository.saveAndFlush(itemBatchMst);
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				 companymstid + " not found")); }
		
		
	

	
	   @GetMapping("{companymstid}/itembatchmst/{batch}/itembatchmstdtls")
		public List<ItemBatchMst> retrivexpDate(@PathVariable(value = "companymstid") String
				  companymstid,
				@PathVariable(value = "batch") String batch) {
			return (List<ItemBatchMst>) itemBatchMstRepository.findByBatchAndCompanyMstId(batch,companymstid);

	   }
	   @DeleteMapping("{comanymstid}/itembatchmst/deleteitembatcdtl/{hdrid}")
	   public void itemBatchDtlDelete(@PathVariable(value = "hdrid") String hdrid)
	   {
		   		
		   itemBatchDtlRepo.deleteBySourceParentId(hdrid);
	   }
	   @GetMapping("{companymstid}/itembatchmst/itembatchdtlbyitemid/{itemid}/{batch}")
		public  @ResponseBody List<ItemBatchDtl> findAllItemBatchDtlByItemId(@PathVariable(value = "companymstid") String
				companymstid ,@PathVariable(value = "itemid") String itemid,
				@PathVariable(value = "batch") String batch
				 
				){
			Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
			return itemBatchDtlRepo.findByItemIdAndBatchAndCompanyMst(itemid,batch,companyMst.get());
		}
	   
	   @GetMapping("{companymstid}/itembatchmstbyitemid/{itemid}")
		public  @ResponseBody List<ItemBatchMst> findAllItemBatchMStByItemId(@PathVariable(value = "companymstid") String
				companymstid ,@PathVariable(value = "itemid") String itemid
				 
				){
			Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
			return itemBatchMstRepository.findByCompanyMstAndItemId(companyMst.get(),itemid);
		}
	   
	   
	   

	   @GetMapping("{companymstid}/batchsearch")
			public  @ResponseBody List<ItemBatchMst> findAllItemBatchMStByItemName(@PathVariable(value = "companymstid") String
					companymstid ,@RequestParam(value = "data") String itemName
					 
					){
		   List<ItemBatchMst> itemBatchMstList = null;
				Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
				itemName = itemName.replaceAll("20%", " ");
				System.out.println(itemName+"   item name isssssssssssssssssssssssssssssssssssssss");
				String[]array=itemName.split(";");
				
				for(String itr:array) { 
					System.out.print(itr+"batch name isssssssssssssssssssssssssssssssssssssssssssss");
				ItemMst itemMst=itemMstRepository.findByItemName(itr);
//				System.out.print(itemMst.getId()+"item id isssssssssssssssssssssssssssssssssssssss");
				
				
				if(null != itemMst)
				{
					itemBatchMstList=itemBatchMstRepository.searchByItemName(itemMst.getId());
				}
//				System.out.print(itemBatchMstList.size()+"size of item batch mst issssssssssssssssssssssssss");
				}
				return itemBatchMstList;
			}
	   @GetMapping("{companymstid}/itembatchmstbyitemidandbatch/{itemid}/{batch}")
	 		public  @ResponseBody ItemBatchMst findAllItemBatchMStByItemIdAndBatch(@PathVariable(value = "companymstid") String
	 				companymstid ,@PathVariable(value = "itemid") String itemid,
	 				@PathVariable(value = "batch") String batch
	 				 
	 				){
	 			Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
	 			return itemBatchMstRepository.findByCompanyMstAndItemIdAndBatch(companyMst.get(),itemid,batch);
	 		}
	   @GetMapping("{companymstid}/itembatchmstresource/fastmovingitems/{reportname}/{branchcode}")
		public  @ResponseBody List<StockReport> fastMovingItems(
				@PathVariable(value = "companymstid") String companymstid ,
				@PathVariable(value = "reportname") String reportname ,
				@RequestParam(value="fdate")String fdate,
				@RequestParam(value="tdate")String tdate,
				@PathVariable(value="branchcode")String branchcode)
		{
		   Date fuDate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		   Date tuDate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
		   return itemBatchDtlService.getFastMovingItem(fuDate,tuDate,companymstid,branchcode,reportname);
		   
		}
	   
	   @GetMapping("{companymstid}/itembatchmstresource/fastmovingitemsbysalesmode/{salesmode}/{reportname}/{branchcode}")
		public  @ResponseBody List<StockReport> fastMovingItemsBySalesMode(
				@PathVariable(value = "companymstid") String companymstid ,
				@PathVariable(value = "salesmode") String salesmode ,
				@PathVariable(value = "reportname") String reportname ,
				@RequestParam(value="fdate")String fdate,
				@RequestParam(value="tdate")String tdate,
				@PathVariable(value="branchcode")String branchcode)
		{
		   Date fuDate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		   Date tuDate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
		   return itemBatchDtlService.fastMovingItemsBySalesMode(fuDate,tuDate,companymstid,branchcode,salesmode,reportname);
		   
		}
	   
	   @GetMapping("{companymstid}/itembatchmstresource/fastmovingitemsbysalesmodeandcategory/{salesmode}/{category}/{reportname}/{branchcode}")
		public  @ResponseBody List<StockReport> fastMovingItemsBySalesModeAndCategory(
				@PathVariable(value = "companymstid") String companymstid ,
				@PathVariable(value = "salesmode") String salesmode ,
				@PathVariable(value = "category") String category ,
				@PathVariable(value = "reportname") String reportname ,
				@RequestParam(value="fdate")String fdate,
				@RequestParam(value="tdate")String tdate,
				@PathVariable(value="branchcode")String branchcode)
		{
		   Date fuDate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		   Date tuDate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
		   return itemBatchDtlService.fastMovingItemsBySalesModeAndCategory(fuDate,tuDate,companymstid,branchcode,salesmode,category,reportname);
		   
		}
	   
	   
	   @GetMapping("{companymstid}/itembatchmstresource/itembatchdtlinsertion")
	 		public  void itemBatchDtlInsertion(
	 				@PathVariable(value = "companymstid") String companymstid,
	 				@RequestParam(value="date")String tdate)
	 		{
				Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);

	 		   Date tuDate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
	 		   itemBatchDtlService.itemBatchDtlInsertion(tuDate,companyMst.get());
	 		   
	 		}
	    //showing all 
		@GetMapping("{companymstid}/itembatchmstresource/showallitembatchdtl")
		public List<StockMigrationReport> showallItemBatchDtl(
				
				@PathVariable(value = "companymstid") String companymstid) {

			Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
			
			return itemBatchDtlService.findAllItemBatchDtl(companyMstOpt.get());
		}
		
		@DeleteMapping("{companymstid}/itembatchmstresource/deleteitembatchdtl/{id}")
		public void deleteitembatchdtl(@PathVariable(value = "id") String Id) {
			itemBatchDtlRepo.deleteById(Id);
	   
	   
	   }
		
		
//		//=================stock qty  from item batch mst ===========anandu ==============28-06-2021========
//		
//		 @GetMapping("{companymstid}/itembatchmstresource/itembatchdtlqty/{itemid}/{batch}")
//	 		public  Double findQtyFromItemBatchMst(@PathVariable(value = "companymstid") String
//	 				companymstid ,@PathVariable(value = "itemid") String itemid,
//	 				@PathVariable(value = "batch") String batch
//	 				 
//	 				){
////	 			Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
//	 			return itemBatchMstRepository.findQtyFromItemBatchMst(itemid,batch);
//	 		}
	             
		
		
	     //  (0_0)       SharonN  [18/06/2021]
		@GetMapping("{companymstid}/itembatchmstresource/getitemwisesalesreport")		
		public List<PharmacyItemWiseSalesReport> getPharmacyItemWiseSalesReport(
				@PathVariable(value = "companymstid") String companymstid,
				@RequestParam("fromdate") String fDate,
				@RequestParam("todate") String tDate) {
			
			java.util.Date fromDate = SystemSetting.StringToUtilDate(fDate, "yyyy-MM-dd");
			java.util.Date toDate = SystemSetting.StringToUtilDate(tDate, "yyyy-MM-dd");

			return itemBatchDtlService.findPharmacyItemWiseSalesReport(fromDate,toDate);
		}
		@GetMapping("{companymstid}/itembatchmstresource/getitemwisesalesreportbyitemname")		
		public List<PharmacyItemWiseSalesReport> getPharmacyItemWiseSalesReportByItemName(
				@PathVariable(value = "companymstid") String companymstid,
				@RequestParam("fromdate") String fDate, @RequestParam("itename") String itename,
				@RequestParam("todate") String tDate) {
			

	     	String itemNameNames=itename;
			
			String[]array=itemNameNames.split(";");
			
			java.util.Date fromDate = SystemSetting.StringToUtilDate(fDate, "yyyy-MM-dd");
			java.util.Date toDate = SystemSetting.StringToUtilDate(tDate, "yyyy-MM-dd");

			//return itemBatchDtlService.findPharmacyItemWiseSalesReport(fromDate,toDate);
			return itemBatchDtlService.findPharmacyItemWiseSalesReportByItemName(fromDate,toDate,array);
		}
		
		
		//------------------------sharon [19/06/2021]------------------------------------
		@GetMapping("{companymstid}/itembatchmstresource/itembatchdtlqty/{batch}/{itemid}/{store}")
		public Double itemBatchMstStock(
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "itemid") String itemid,
				@PathVariable(value = "store") String store,
				@PathVariable(value = "batch") String batch){
		
			{
				return itemBatchMstRepository.findQtyByItemAndBatch(itemid, batch,store);
			}
		}	
		
		
		
		
		/* 
		 * 
		 * use of item wise sales with branch 
		 * 
		 */
		@GetMapping("{companymstid}/itembatchmstresource/getitemwisesalesreportbyitemnamewithbranch/{branchcode}")		
		public List<PharmacyItemWiseSalesReport> getItemWiseSalesReportByItemName(
				@PathVariable(value = "companymstid") String companymstid,
				@RequestParam("fromdate") String fDate, 
				@RequestParam("todate") String tDate,
				@PathVariable(value="branchcode")String branchcode) {
			

			
			java.util.Date fromDate = SystemSetting.StringToUtilDate(fDate, "yyyy-MM-dd");
			java.util.Date toDate = SystemSetting.StringToUtilDate(tDate, "yyyy-MM-dd");

			return itemBatchDtlService.findItemWiseSalesReportByItemName(fromDate,toDate,branchcode);
		}
		
		
		
		//***********************Stock verification at final save***(26-04-2022)*************************///
		@GetMapping("{companymstid}/stockverification/{salestranshdrid}")
		public String retrieveAllItemBatchDtlDaily(@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "salestranshdrid") String salestranshdrid) {
			logger.info(".........Stock checking in restserver started...........");
			if (ENABLENEGATIVEBILLING.equalsIgnoreCase("true")) {
				logger.info(".........enable-negative-billing is true...........");
				return MapleConstants.STOCKOK;
			}
			logger.info(".........enable-negative-billing is false...........");
			String result = itemBatchMstService.stockVerification(companymstid, salestranshdrid);
			logger.info(".........result of stock verification in item-batch-dtl..........." + result);
			return result;
		}
		
		
		
}
