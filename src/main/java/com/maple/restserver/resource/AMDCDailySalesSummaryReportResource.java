package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.AMDCDailySalesSummaryReport;
import com.maple.restserver.report.entity.PharmacyDailySalesTransactionReport;
import com.maple.restserver.report.entity.PoliceReport;
import com.maple.restserver.service.AMDCDailySalesSummaryReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class AMDCDailySalesSummaryReportResource {
	
	@Autowired
	AMDCDailySalesSummaryReportService amdcDailySalesSummaryReportService;
	
	//---------------Get AMDC Daily Sales Summary report-----Anandu-------------
	@GetMapping("{companymstid}/amdcdailysalessummaryreportresource/getamdcdailysalessummaryreport")		
	public List<AMDCDailySalesSummaryReport> getAMDCDailySalesSummaryReport(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("currentDate") String currentDate) {
		
		java.util.Date cDate = SystemSetting.StringToUtilDate(currentDate, "yyyy-MM-dd");

		return amdcDailySalesSummaryReportService.findAMDCDailySalesSummaryReport(cDate);
	}
	
	
	//---------------Get PharmacyDailySalesTransactionReport-----sharon-------------
	@GetMapping("{companymstid}/amdcdailysalessummaryreportresource/getpharmacydailysalestransactionreport")		
	public List<PharmacyDailySalesTransactionReport> getPharmacyDailySalesTransactionReport(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fromdate") String fDate,
			@RequestParam("todate") String tDate) {
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fDate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(tDate, "yyyy-MM-dd");

		return amdcDailySalesSummaryReportService.findPharmacyDailySalesTransactionReport(fromDate,toDate);
	}
}
