package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CreditNote;
import com.maple.restserver.entity.DebitNote;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CreditNoteRepository;

@RestController
public class CreditNoteResource {
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	CreditNoteRepository creditNoteRepository;
	
	@GetMapping("{companymstid}/creditnoteresource/getcreditnotedetails")
	public List<CreditNote>getcreditDetailsByUserId(@PathVariable(value = "companymstid") 
	String companymstid
			)
	{
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		
		return creditNoteRepository.findByCompanyMst(companyMstOpt.get());
	}
	
	@PostMapping("{companymstid}/creditnoteresource/savecreditnotedetails")
	public CreditNote createcreditNote(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody CreditNote creditNote)  {

		return companyMstRepository.findById(companymstid).map(companyMst -> {
			creditNote.setCompanyMst(companyMst);
			return creditNoteRepository.saveAndFlush(creditNote);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	@DeleteMapping("{companymstid}/creditnoteresource/deletecreditnotedetails/{id}")
	public void CreditNoteDeleteById(@PathVariable(value = "id") String Id) {
		creditNoteRepository.deleteById(Id);

	}

}

	

