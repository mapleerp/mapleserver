package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.IntentInDtl;
import com.maple.restserver.entity.IntentInHdr;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.StockTransferInHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.IntentInDtlRepository;
import com.maple.restserver.repository.IntentInHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class IntentInHdrResource {
	
private static final Logger logger = LoggerFactory.getLogger(IntentInHdrResource.class);
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	IntentInHdrRepository intentInHdrRepo;
	
	@Autowired
	 IntentInDtlRepository intentInDtlRepo;
	
	 @Autowired
	 SaveAndPublishService saveAndPublishService;

	    
	@GetMapping("{companymstid}/intenthdrinsearch")
	public  @ResponseBody List<IntentInHdr> searchIntentHdrIn(@RequestParam("data") String searchstring){
		return intentInHdrRepo.findSearch("%"+searchstring+"%") ;
	}
	     
//
//	@GetMapping("{companymstid}/intenindtl/{hdrid}")
//	public  @ResponseBody List<IntentInDtl> intentInByHdrId(@PathVariable(value = "hdrid") String hdrid){
//		
//		System.out.println(hdrid+"hdr id isssssssssssssssssssssssssssss");
//		
//		return intentInDtlRepo.findByIntentInHdrId(hdrid);
//	}
//	
	
	
	
	@PutMapping("{companymstid}/intentinhdr/{intentinhdrId}/updateintenthdr")
	public IntentInHdr intentFinalSave(@PathVariable String intentinhdrId, @Valid @RequestBody IntentInHdr intentinRequest)
	{
 		 
		   		 
				return intentInHdrRepo.findById(intentinhdrId).map(intentinhdr -> {
					intentinhdr.setInVoucherNumber(intentinRequest.getInVoucherNumber()); 
					intentinhdr.setBranchCode(intentinRequest.getBranchCode());
					intentinhdr.setIntentStatus("COMPLETE");
//					IntentInHdr intenthdr = intentInHdrRepo.saveAndFlush(intentinhdr);
					IntentInHdr intenthdr=saveAndPublishService.saveIntentInHdr(intentinhdr,intentinhdr.getBranchCode());
					 logger.info("IntentInHdr send to KafkaEvent: {}", intenthdr);
			
		            return intenthdr;
		            
		        }).orElseThrow(() -> new ResourceNotFoundException("intentinhdrId " + intentinhdrId + " not found"));
				
				
			  
	}
	

	@GetMapping("/{companymstid}/intentinhdr/{branch}/intenthdrs")
	public List<IntentInHdr> getintentHdrByDateAndBranch(
			@PathVariable(value = "branch") String branch , @RequestParam("rdate") String rdate
			) {
		java.util.Date vdate = SystemSetting.StringToUtilDate(rdate,"yyyy-MM-dd");
		return intentInHdrRepo.findByInVoucherDateAndFromBranch(vdate,branch);

	}
	
	
	
	@GetMapping("/{companymstid}/intenthdrByvouchernumber/{vouchernumber}/intenthdr")
	public List<IntentInHdr> getintentHdrByVoucherNumber(
			@PathVariable(value = "vouchernumber") String vouchernumber , @RequestParam("date") String date
			) {
		java.util.Date vdate = SystemSetting.StringToUtilDate(date,"dd-MM-yyyy");
		return intentInHdrRepo.findByInVoucherNumberAndInVoucherDate(vouchernumber,vdate);

	}
	
	
	@GetMapping("{companymstid}/intentinhdr/searchpendingvoucher")
	public @ResponseBody List<IntentInHdr> searchIntentInVoucher(@RequestParam("data") String searchstring) {
		return intentInHdrRepo.getPendingVoucherNumber("%" + searchstring + "%");
	}

}
