package com.maple.restserver.resource;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PharmacyPurchaseReport;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.report.entity.HsnWisePurchaseDtlReport;
import com.maple.restserver.report.entity.MonthlySupplierSummary;
import com.maple.restserver.report.entity.PurchaseReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.service.PurchaseReportService;
import com.maple.restserver.service.PurchaseReportServiceImpl;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class PurchaseReportResource {
	

	@Autowired
	private PurchaseReportServiceImpl purchaseReportServiceImpl;
	
	
	@Autowired
	PurchaseReportService purchaseReportService;
	
	@Autowired
	private  CompanyMstRepository  companyMstRepository;
	
	@Autowired
	private PurchaseHdrRepository purchaseHdrRepository;
	@GetMapping("{companymstid}/purchasereport")
	public List<PurchaseReport> retrieveAllPurchase(){
		return purchaseReportServiceImpl.getAllPurchase();
	}
	
	
	 
	
	@GetMapping("{companymstid}/purchasereport/{vouchernumber}/{branchcode}")
	public List<PurchaseReport> retrieveReportPurchase(@PathVariable (value = "vouchernumber") String vouchernumber,
			@PathVariable (value = "branchcode") String branchcode ,
			@RequestParam("rdate") String reportdate){
		
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		return purchaseReportServiceImpl.retrieveReportPurchase(vouchernumber,branchcode,date);
	}
	

	@GetMapping("{companymstid}/retrivetaxrate/{vouchernumber}/{branchcode}")
	public List<PurchaseReport> retrieveTaxRate(@PathVariable (value = "vouchernumber") String vouchernumber,
			@PathVariable (value = "branchcode") String branchcode ,
			@RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
	
		return purchaseReportServiceImpl.retrieveTaxRate(vouchernumber,branchcode,date);
	}
	 

	@GetMapping("{companymstid}/purchasereport/monthlysuppliersummary/{vouchernumber}/{branchcode}")
	public List<MonthlySupplierSummary> retrievePurchaseReport(@PathVariable (value = "vouchernumber") String vouchernumber,
			@PathVariable (value = "branchcode") String branchcode ,
			@RequestParam("startDate") String startDate,	@RequestParam("endDate") String endDate){
		
		
		return purchaseReportServiceImpl.retrievePurchaseReport(vouchernumber,branchcode,startDate,endDate);
	}
	 
	

	@GetMapping("{companymstid}/purchasereport1/{branchcode}/purchasereport")
	public List<PurchaseHdr> getPurchaseHdr(
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "branchcode") String branchCode  ,
				@RequestParam("fromdate") String fromdate,
				@RequestParam("todate") String todate) {
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
		return purchaseHdrRepository.purchaseReport(fromDate,toDate,branchCode,companyMst);

	}
	
	
	
//version 3.1
	@GetMapping("{companymstid}/purchasereportresource/{branchcode}/purchasedtlreport")
	public List<PurchaseReport> getPurchaseDtlReport(
				@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "branchcode") String branchCode  , @RequestParam("fromdate") String fromdate,@RequestParam("tomdate") String todate) {
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		return purchaseReportService.getPurchaseDtlReport(fromDate,toDate,branchCode,companyMst);

	}
	//version 3.1 end

	@GetMapping("{companymstid}/purchasereport/exportpurchasereporttoexcel/{branchcode}")
	public List<PurchaseReport> purchaseExportToExcel(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable (value = "branchcode") String branchcode ,
			@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate){
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
	
		
		return purchaseReportService.purchaseExportToExcel(companyMstOpt.get(),branchcode,fromDate,toDate);
	}
	//for amdc
	@GetMapping("{companymstid}/purchasereport/supplierwisepurchasereportwithcategory/{supname}/{branchCode}")
	public List<PurchaseReport> SupplierwisePurhaseReportWithCategory(@PathVariable(value = "companymstid")
	String companymstid,@PathVariable(value = "supname")String supname,
	@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate,@RequestParam
	(value = "categoryName")String categoryNames)
	{
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		
		String[]array=categoryNames.split(";");
		return purchaseReportService.getSupplierWisePurchaseReport(fromDate,toDate,array,supname,companyMstOpt.get());
	}
	
	

	@GetMapping("/{companymstid}/purchasereport/{category}/getpharmacypurchasereport/{branchCode}")
	public List<PharmacyPurchaseReport>getPharmacyPurchaseReport(@PathVariable(value = "companymstid") String
			  companymstid,
			 @PathVariable("branchCode") String branchCode, @PathVariable("category") String category,  @RequestParam("fdate") String fromdate,  @RequestParam("tdate") String todate){
		java.util.Date fdate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date  tdate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		
		
	String categoryNames=category;
		
		String[]array=categoryNames.split(";");
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return purchaseReportService.getPharmacyPurchaseReport(companyMst ,fdate,tdate,branchCode,array);

	
	

	}
	
	

	@GetMapping("{companymstid}/purchasereport/supplierwisepurchasereport/{supname}/{branchcode}")
	public List<PurchaseReport> SupplierwisePurhaseReport(@PathVariable(value = "companymstid")
	String companymstid,@PathVariable(value = "supname")String supname,
	@RequestParam("fromdate") String fromdate,@RequestParam("todate") String todate)
	{
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		
		return purchaseReportService.getSupplierWisePurchaseReportWithDate(fromDate,toDate,supname,companyMstOpt.get());
	}
	@GetMapping("{companymstid}/purchasereport/hsnpurchasedtlreport/{branchcode}")
	public List<HsnWisePurchaseDtlReport> hsnCodePurchaseDtlReport(@PathVariable(value = "companymstid")
	String companymstid,@PathVariable(value = "branchcode")String branchcode,
	@RequestParam("fromdate") String fromdate,
	@RequestParam("todate") String todate,
	@RequestParam(value = "categoryName")String categoryNames)
	{
		Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		if(categoryNames.equalsIgnoreCase(""))
		{
			return purchaseReportService.getHsnWisePurchaseDtlReportWithoutGroup(fromDate,toDate,branchcode,companyMstOpt.get());

		}
		else
		{
		String[]array=categoryNames.split(";");
		return purchaseReportService.getHsnWisePurchaseDtlReport(fromDate,toDate,array,branchcode,companyMstOpt.get());
		}
		}
	
	
	
	

	@GetMapping("{companymstid}/purchasereportresource/getimportpurchaseinvoice/{phdrid}")	                          
	public List<PurchaseReport> getImportPurchaseInvoice(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable (value = "phdrid") String phdrid){
		
		
		return purchaseReportService.getImportPurchaseInvoiceDetail(phdrid);
	}
	
	
	@GetMapping("{companymstid}/purchasereportresource/gettotaladditionalexpensebyhdrid/{id}")	                          
	public Double getTotalAdditionalExpenseByHdrId(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable (value = "id") String phdrid){
		
		
		return purchaseReportService.getTotalAdditionalExpenseByHdrId(phdrid);
	}
	
	@GetMapping("{companymstid}/purchasereportresource/gettotalimportexpense//{id}")	                          
	public Double getTotalImportExpense(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable (value = "id") String phdrid){
		
		
		return purchaseReportService.getTotalImportExpense(phdrid);
	}

	@GetMapping("{companymstid}/purchasereportresource/getimportpurchaseinvoicebyhdrid/{phdrid}")
	public List<PurchaseReport> getImportPurchaseInvoiceReportByHdrId(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable (value = "phdrid") String phdrid){
		
		
		return purchaseReportService.getImportPurchaseInvoiceReportByHdrId(phdrid);
	}
	
}
