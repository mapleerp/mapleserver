package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.StockReport;
import com.maple.restserver.service.BatchPurchaseReportService;
import com.maple.restserver.service.BatchStockReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class BatchWiseStockReportResource {

	@Autowired
	BatchStockReportService batchStockReportService;
	
	@GetMapping("{companymstid}/batchstockreport/{branchcode}/{categoryid}")
	public List<StockReport> getStockReportWithBatch(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,@PathVariable("categoryid") String categoryid ,@RequestParam("fromdate") String fromdate){
		java.sql.Date date = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");

		return batchStockReportService.getStockByBranchCodeAndDateAndCompanyMstIdAndBatch(branchcode, companymstid, date, categoryid);



	}
	
}
