package com.maple.restserver.resource;
import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.BalanceSheet;
import com.maple.restserver.repository.BalanceSheetRepository;

 
@RestController
public class BalanceSheetResource 
{
@Autowired
private BalanceSheetRepository balanceSheetRepo;

@GetMapping("{companymstid}/balance")
public List<BalanceSheet> retrieveAllbalance(@PathVariable(value = "companymstid") String
		  companymstid)
{
	return balanceSheetRepo.findByCompanyMstId(companymstid);
}
@PostMapping("{companymstid}/balances")
public BalanceSheet createUser(@Valid @RequestBody BalanceSheet balanceSheet)
{
BalanceSheet saved=balanceSheetRepo.saveAndFlush(balanceSheet);
 
return saved;
}

}
