package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.maple.restserver.entity.UserCreatedTaskIn;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.UseCreatedTaskInRepository;


public class UseCreatedTaskInResource {
	
	@Autowired
	UseCreatedTaskInRepository userCreatedTaskInRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("{companymstid}/usercreatetaskresource/getusertaskbyusername/{userid}")
	public List<UserCreatedTaskIn> getusertaskByUserId(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "userid") String userid) {
		return userCreatedTaskInRepository.findByUserIdAndCompanyMst(companymstid, userid);
	}

	


}
