
package com.maple.restserver.resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.AppointmentDtl;
import com.maple.restserver.entity.BalanceSheet;
import com.maple.restserver.entity.BankBook;
import com.maple.restserver.entity.BankMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.MapleObject;
import com.maple.restserver.repository.AppointmentDtlRepository;
import com.maple.restserver.repository.BalanceSheetRepository;
import com.maple.restserver.repository.BankBookRepository;
import com.maple.restserver.repository.BankMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;


@RestController
@Transactional
public class BankBookResource {
	
	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	private BankBookRepository bankBookRepository;
	
	
	@Autowired
	private BankMstRepository bankMstRepository ;
	

	
	@Autowired
	private  AppointmentDtlRepository appointmentDtlRepository;
	
	
	@Autowired
	private BalanceSheetRepository balanceSheetRepository;
	
	
	@GetMapping("/{companymstid}/bankbook")
	public List<BankBook> retrieveAllcash(@PathVariable(value = "companymstid") String companymstid)
	{
		return bankBookRepository.findByCompanyMstId(companymstid);
	}
	
	
	@PostMapping("{companymstid}/bankbook")
	public ResponseEntity<Object> createUser(@Valid @RequestBody BankBook bankBook1)
	{
		BankBook saved=bankBookRepository.saveAndFlush(bankBook1);
		
		
		
	URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	}

	@GetMapping("/{companymstid}/bankbooktree")
	public List<BankBook> manageBankBookTree(@PathVariable(value = "companymstid") String companymstid)
	{
		
		Optional<CompanyMst>  companyMstOpt = companyMstRepo.findById(companymstid);
		
		CompanyMst companyMst =  companyMstOpt.get();
		
		
		BankBook bankBook = new BankBook();
		bankBook.setId("001");
		bankBook.setCompanyMst(companyMst);
		
		BankMst bankMst = new BankMst();
		bankMst.setId("002");
		
		bankMst.setParentId(bankBook.getId());
		bankMst.setCompanyMst(companyMst);
		 
		
		
		AppointmentDtl appointmentDtl = new AppointmentDtl();
		appointmentDtl.setId("003");
		appointmentDtl.setCompanyMst(companyMst);
		 
		appointmentDtl.setParentId(bankMst.getId());
		
		
		
	 
		
		BalanceSheet balanceSheet = new BalanceSheet();
		 
		balanceSheet.setParentId(bankMst.getId());
		balanceSheet.setCompanyMst(companyMst);
		
		ArrayList<MapleObject> mapleArrayObject = new ArrayList();
		
	 
		
		bankBook = bankBookRepository.save(bankBook);
		
		bankMst.setParentId(bankBook.getId());
		bankMst = bankMstRepository.save(bankMst);
		
		appointmentDtl.setParentId(bankMst.getId());
		appointmentDtl = appointmentDtlRepository.save(appointmentDtl);
		
		balanceSheet.setParentId(appointmentDtl.getId());
		balanceSheet = balanceSheetRepository.save(balanceSheet);
		
		
		
		return bankBookRepository.findByCompanyMstId(companymstid);
	}
	
	
}
