package com.maple.restserver.resource;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.RawMaterialIssueDtl;
import com.maple.restserver.entity.RawMaterialIssueHdr;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.ActualProductionReport;
import com.maple.restserver.report.entity.RawMaterialIssueReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.RawMaterialMstRepository;
import com.maple.restserver.service.RawMaterialIssueService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class RawMaterialIssuMstResource {
	EventBus eventBus = EventBusFactory.getEventBus();
	 
	 @Value("${serverorclient}")
		private String serverorclient;
	 
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	RawMaterialMstRepository rawMaterialMstRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired 
	RawMaterialIssueService rawMaterialIssueService;

	
	@PostMapping("{companymstid}/rawmaterialissuemst")
	public RawMaterialIssueHdr createRawMaterialIssueMst(@Valid @RequestBody RawMaterialIssueHdr rawMaterialIssueMst,
			@PathVariable (value = "companymstid") String companymstid)
	{
		
		
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			rawMaterialIssueMst.setCompanyMst(companyMst);
			 
			
			
			RawMaterialIssueHdr saved=rawMaterialMstRepo.saveAndFlush(rawMaterialIssueMst);
	 	return saved;
	}
	@PutMapping("{companymstid}/rawmaterialissuemst/{MstId}/updaterawmaterialissuemst")
	public RawMaterialIssueHdr updateRawmaterialissuemst(
			@PathVariable(value="MstId") String MstId, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody RawMaterialIssueHdr  rawMaterialIssueHdrRequest)
	{
			
		RawMaterialIssueHdr rawMaterialIssueHdr = rawMaterialMstRepo.findById(MstId).get();
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		rawMaterialIssueHdr.setCompanyMst(companyMst);
		rawMaterialIssueHdr.setVoucherNumber(rawMaterialIssueHdrRequest.getVoucherNumber());
		rawMaterialIssueHdr = rawMaterialMstRepo.save(rawMaterialIssueHdr);
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", rawMaterialIssueHdr.getVoucherNumber());
		variables.put("voucherDate", rawMaterialIssueHdr.getVoucherDate());
		variables.put("inet", 0);
		variables.put("id", rawMaterialIssueHdr.getId());
		variables.put("branchcode", rawMaterialIssueHdr.getBranchCode());

		variables.put("companyid", rawMaterialIssueHdr.getCompanyMst());
		if (serverorclient.equalsIgnoreCase("REST")) {
			variables.put("REST", 1);
		} else {
			variables.put("REST", 0);
		}

		
		variables.put("WF", "forwardRawMaterialIssue");
		
		String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardRawMaterialIssue");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);
        return rawMaterialIssueHdr;
		 			
	}
 
	@GetMapping("/{companymstid}/rawmaterialissuereport/{branchcode}")
	public List<RawMaterialIssueReport> rawMaterialIssueReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, 
			@RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	java.util.Date TDate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		return rawMaterialIssueService.rawMaterialIssueReportbetweenDate(companymstid,branchcode,fDate,TDate);
	}
	
	
}
