package com.maple.restserver.resource;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.IntentHdrRepository;
import com.maple.restserver.repository.JournalDtlRepository;
import com.maple.restserver.repository.JournalHdrRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.service.IntentService;
import com.maple.restserver.service.IntentServiceImpl;
import com.maple.restserver.service.JournalHdrServiceImpl;

@RestController
@Transactional
public class JournalDtlResource {
	
	
	@Autowired
	JournalHdrServiceImpl journalHdrServiceImpl;
	
	@Autowired
	private JournalDtlRepository journalDtlRepository;
	
	@Autowired
	private JournalHdrRepository journalHdrRepository;
	
	
	@GetMapping("{companymstid}/journalhdr/{journalhdrid}/journaldtls")
	public List<JournalDtl> retrieveAllJournalDtlByJournalHdrID(
			@PathVariable(value = "journalhdrid") String journalhdrid) {
		return journalDtlRepository.findByJournalHdrId(journalhdrid );

	}
	
	@PostMapping("{companymstid}/journalhdr/{journalhdrid}/journaldtl")
	public JournalDtl createJournalDtl(@PathVariable(value = "journalhdrid") String journalhdrid,
			@Valid @RequestBody JournalDtl journalDtlRequest) {

		return journalHdrRepository.findById(journalhdrid).map(journalhdr -> {
			journalDtlRequest.setJournalHdr(journalhdr);
			return journalDtlRepository.saveAndFlush(journalDtlRequest);
		}).orElseThrow(() -> new ResourceNotFoundException("journalhdrid " + journalhdrid + " not found"));

	}

	@DeleteMapping("{companymstid}/journaldtl/{id}")
	public void IntentDetailDelete(@PathVariable(value = "id") String Id) {
		journalDtlRepository.deleteById(Id);

	}
	
	@GetMapping("{companymstid}/journaldtl/{journalhdrid}/sumdebitamount")
	public double retriveSumOfDebitAmount(
			@PathVariable(value ="journalhdrid") String journalhdrid) {
		double totalAmount =  journalDtlRepository.findSumDebitAmount(journalhdrid);
		return totalAmount;

	}

	@GetMapping("{companymstid}/journaldtl/{journalhdrid}/sumcreditamount")
	public double retriveSumOfCreditAmount(
			@PathVariable(value ="journalhdrid") String journalhdrid) {
		double totalAmount =  journalDtlRepository.findSumCreditAmount(journalhdrid);
		return totalAmount;

	}

	
}
