package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PDCReceipts;
import com.maple.restserver.entity.PDCReconcileHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PDCReconcileHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class PDCReconcileHdrResource {
	
private static final Logger logger = LoggerFactory.getLogger(PDCReconcileHdrResource.class);
	
	
	
	
	@Autowired
	PDCReconcileHdrRepository pDCReconcileRepository;
	
	@Autowired
	CompanyMstRepository cmpanyMstRepository;
	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	

	@PostMapping("{companymstid}/pdcreconcilehdrresource/pdcreconcilehdr")
	public PDCReconcileHdr  createPDCReconcileHdr(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody PDCReconcileHdr PDCReconcileHdr) {
		return cmpanyMstRepository.findById(companymstid).map(companyMst -> {
			PDCReconcileHdr.setCompanyMst(companyMst);

//			return pDCReconcileRepository.saveAndFlush(PDCReconcileHdr);
			return saveAndPublishService.savePDCReconcileHdr(PDCReconcileHdr, PDCReconcileHdr.getBranchCode());
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	
	@GetMapping("{companymstid}/pdcreconncilebydate/branchcode")
	public List<PDCReconcileHdr> retrievePDCReconcileHdr(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam("rdate") String reportdate) {
		
		java.sql.Date date = SystemSetting.StringToSqlDate(reportdate, "dd-MM-yyyy");
		
		Optional<CompanyMst> companyOpt = cmpanyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return pDCReconcileRepository.findByTransDateAndCompanyMstAndBranchCode(date,companyMst,branchcode);
	}
	
	/*
	@GetMapping("{companymstid}/pdcreconcilehdrs")
	public List<PDCReconcileHdr> retrievePDCReconcileHdrByCompanyMst(
			@PathVariable(value = "companymstid") String companymstid) {
		
		
		Optional<CompanyMst> companyOpt = cmpanyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return pDCReconcileRepository.findByCompanyMst(companyMst);
	}
	*/
	@PutMapping("{companymstid}/pdcreconcilehdrresource/updatepdcreconcile/{pdcreceiptid}")
	public PDCReconcileHdr updatePDCReconcileHdr(
			@PathVariable(value = "pdcreceiptid") String pdcreceiptid ,
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody PDCReconcileHdr pdcrecreconcilRequest)
	{
		PDCReconcileHdr pDCReconcileHdr = pDCReconcileRepository.findById(pdcreceiptid).get();
		
		pDCReconcileHdr.setVoucherNumber(pdcrecreconcilRequest.getVoucherNumber());
		
		pDCReconcileHdr =  pDCReconcileRepository.saveAndFlush(pDCReconcileHdr);
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("voucherNumber", pDCReconcileHdr.getVoucherNumber());
		variables.put("voucherDate", pDCReconcileHdr.getTransDate());
		variables.put("inet", 0);
		variables.put("id", pDCReconcileHdr.getId());
		variables.put("companyid", pDCReconcileHdr.getCompanyMst());
		variables.put("branchcode", pDCReconcileHdr.getBranchCode());
		variables.put("REST", 1);
		variables.put("WF", "forwardPDCReconcile");
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");
		LmsQueueMst lmsQueueMst = new LmsQueueMst();
		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardPdcReceipts");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());

		eventBus.post(variables);
		
		return pDCReconcileHdr;
		
		
	}
	
}
