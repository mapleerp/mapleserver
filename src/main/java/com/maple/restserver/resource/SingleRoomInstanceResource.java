
package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.his.entity.SingleRoomInstance;
import com.maple.restserver.repository.SingleRoomInstanceRepository;



@RestController
@Transactional
public class SingleRoomInstanceResource {
	@Autowired
	private SingleRoomInstanceRepository singleroominst;
	@GetMapping("{companymstid}/singlerooms")
	public List<SingleRoomInstance> retrieveAlldepartments()
	{
		return singleroominst.findAll();
		
	}
	@PostMapping("{companymstid}/singlerooms")
	public ResponseEntity<Object>createUser(@Valid @RequestBody SingleRoomInstance singlerooms1)
	{
		SingleRoomInstance saved=singleroominst.saveAndFlush(singlerooms1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}

}
