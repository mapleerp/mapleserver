package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProcedureMst;
import com.maple.restserver.entity.ProcessMst;
import com.maple.restserver.entity.ProcessPermissionMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.ProcessPermissionWebEntity;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProcessMstRepository;
import com.maple.restserver.repository.ProcessPermissionRepository;
import com.maple.restserver.repository.UserMstRepository;
import com.maple.restserver.service.ProcessPermissionService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ProcessPermissionResource {
private static final Logger logger = LoggerFactory.getLogger(ProcessPermissionResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	//@Autowired
	//private RuntimeService runtimeService;
	EventBus eventBus = EventBusFactory.getEventBus();
	 
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	UserMstRepository userMstRepository;
	
	@Autowired
	ProcessMstRepository processMstRepository;

	@Autowired
	ProcessPermissionRepository processpermissionRepo;
	
	@Autowired
	ProcessPermissionService processPermissionService;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@PostMapping("{companymstid}/processpermission")
	public ProcessPermissionMst createProcessPermission(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody ProcessPermissionMst processPermissionMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			processPermissionMst.setCompanyMst(companyMst);

		//	ProcessPermissionMst savedprocessPermissionMst = processpermissionRepo.saveAndFlush(processPermissionMst);
			ProcessPermissionMst savedprocessPermissionMst =saveAndPublishService.saveProcessPermissionMst(processPermissionMst,mybranch);
			logger.info("PurchaseHdr send to KafkaEvent: {}", savedprocessPermissionMst);
			Map<String, Object> variables = new HashMap<String, Object>();
			variables.put("voucherNumber", savedprocessPermissionMst.getId());
			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", savedprocessPermissionMst.getId());
			variables.put("companyid", companyMst);

			variables.put("REST", 1);

			
			variables.put("WF", "forwardProcessPermission");
			
			

	 		String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			
			//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardProcessPermission");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);
			
			/*ProcessInstanceWithVariables pVariablesInReturn = runtimeService
					.createProcessInstanceByKey("forwardProcessPermission").setVariables(variables)

					.executeWithVariablesInReturn();*/
			

			//return processpermissionRepo.saveAndFlush(processPermissionMst);
//			return saveAndPublishService.saveProcessPermissionMst(processPermissionMst,mybranch);
			return savedprocessPermissionMst;
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	@DeleteMapping("{companymstid}/processpermissiondelete/{id}")
	public void processPermissionMstDelete(@PathVariable(value = "id") String Id) {
		processpermissionRepo.deleteById(Id);

	}

	@GetMapping("{companymstid}/findallprocesspermission")
	public List<ProcessPermissionMst> retrieveAProcessPermissionMst() {
		return processpermissionRepo.findAll();

	}

	@GetMapping("{companymstid}/processpermissionbyprocessid/{id}")
	public List<ProcessPermissionMst> retrieveAllProcessPermissionMst(@PathVariable(value = "id") String Id) {
		return processpermissionRepo.findByProcessId(Id);

	}

	@GetMapping("{companymstid}/processpermissionbyuserid/{userid}")
	public List<ProcessPermissionMst> getProcessPermissionMst(@PathVariable(value = "userid") String userId) {
		return processpermissionRepo.findByUserId(userId);

	}
	

	@GetMapping("{companymstid}/processpermission/processpermissionbyname/{processname}")
	public List<ProcessPermissionMst> getProcessPermissionMstByProcessName(
			@PathVariable(value = "processname") String processname) {
		return processpermissionRepo.getprocessPermissionMstByProcessName(processname);

	}
	
	@GetMapping("{companymstid}/web/processpermission/{username}/{processname}/{id}")
	public ProcessPermissionMst createProcessPermissionMst(
			@PathVariable(value = "username") String username,
			@PathVariable(value = "processname") String processname,
			@PathVariable(value = "id") String id,
			@PathVariable(value = "companymstid") String companymstid) {
		
		username = username.replaceAll("%20", " ");
		processname = processname.replaceAll("%20", " ");
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		
		if(null == companyMst)
		{
			return null;
		}
		
		UserMst userMst = userMstRepository.findByUserNameAndCompanyMstId(username, companymstid);
		if(null == userMst)
		{
			return null;
		}
			
		ProcessMst processMst = processMstRepository.findByProcessNameAndCompanyMst(processname,companyMst);
		if(null == processMst) 
		{
			return null;
		}
		
		ProcessPermissionMst processPermissionMst = new ProcessPermissionMst();
		processPermissionMst.setCompanyMst(companyMst);
		processPermissionMst.setProcessId(processMst.getId());
		processPermissionMst.setUserId(userMst.getId());
		processPermissionMst.setId(id);
		
//		return processpermissionRepo.saveAndFlush(processPermissionMst);
		return saveAndPublishService.saveProcessPermissionMst(processPermissionMst,mybranch);

	}
	
	@GetMapping("{companymstid}/web/allprocesspermission")
	public List<ProcessPermissionWebEntity> getProcessPermissionMstByCompany(
			@PathVariable(value = "companymstid") String companymstid) {
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		
		if(null == companyMst)
		{
			return null;
		}

	return processPermissionService.findByCompanyMst(companyMst);
	}
	
	

	@GetMapping("{companymstid}/processmstresource/processsearch")
	public  @ResponseBody List<ProcessMst> searchProcessMst(	
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("data") String searchstring){
		return processMstRepository.findSearch("%"+searchstring+"%",companymstid) ;
	}
	

}
