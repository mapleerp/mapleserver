package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BranchCategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.repository.BranchCategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.BranchCategoryMstService;

@RestController
@Transactional
public class BranchCategoryMstResource {
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	BranchCategoryMstRepository branchCategoryMstRepository;

	@Autowired
	BranchCategoryMstService branchCategoryMstService;

	@PostMapping("{companymstid}/branchcategorymstresource/createbranchcategorymst")
	public BranchCategoryMst createBranchCategoryMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody BranchCategoryMst branchCategoryMst) {

		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		branchCategoryMst.setCompanyMst(companyMst);
		branchCategoryMst = branchCategoryMstRepository.save(branchCategoryMst);
		return branchCategoryMst;
	}

	@DeleteMapping("{companymstid}/branchcategorymstresource/deletebranchcategorymst/{id}")
	public void DeleteBranchCategoryMst(@PathVariable(value = "id") String Id) {
		branchCategoryMstRepository.deleteById(Id);
	}

	@GetMapping("{companymstid}/branchcategorymstresource/branchcatergorymstbybranchcode/{branchcode}")
	public List<BranchCategoryMst> retrieveBranchCategoryMst(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		if (!companyMstOpt.isPresent()) {
			return null;
		}
		return branchCategoryMstRepository.findByCompanyMstAndBranchCode(companyMstOpt.get(), branchcode);
	}

	@GetMapping("{companymstid}/branchcategorymstresource/showallbranchcategorymst")
	public List<BranchCategoryMst> showallbranchcategorymst(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		if (!companyMstOpt.isPresent()) {
			return null;
		}
		return branchCategoryMstRepository.findByCompanyMst(companyMstOpt.get());
	}

	@GetMapping("{companymstid}/branchcategorymstresource/branchcatergorymstbycategoryid/{categoryid}")
	public List<BranchCategoryMst> retrieveBranchCategoryidMst(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "categoryid") String categoryid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		if (!companyMstOpt.isPresent()) {
			return null;
		}
		return branchCategoryMstRepository.findByCompanyMstAndCategoryId(companyMstOpt.get(), categoryid);
	}

	@GetMapping("{companymstid}/branchcategorymstresource/showallbranchcategory")
	public List<BranchCategoryMst> retrieveBranchCategory(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		if (!companyMstOpt.isPresent()) {
			return null;
		}
		return branchCategoryMstService.retrieveBranchCategory(companyMstOpt.get());
	}

}
