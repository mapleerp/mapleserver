package com.maple.restserver.resource;

import java.net.URI;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.kafka.common.protocol.types.Field.Str;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.AccountPayable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.LmsQueueTallyMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.PurchaseHdrMessageEntity;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.report.entity.ImportPurchaseSummaryReport;
import com.maple.restserver.report.entity.LocalPurchaseDetailReport;
import com.maple.restserver.report.entity.LocalPurchaseSummaryReport;
import com.maple.restserver.report.entity.PharmacyLocalPurchaseDetailsReport;
import com.maple.restserver.report.entity.PurchaseReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.AccountPayableRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.LmsQueueTallyMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ImportPurchaseSummaryRptService;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.service.ItemBatchDtlServiceimpl;
import com.maple.restserver.service.LocalPurchaseSummaryDtlReportService;
import com.maple.restserver.service.MultiUnitConversionServiceImpl;
import com.maple.restserver.service.PharmacyLocalPurchaseDetailsReportService;
import com.maple.restserver.service.PurchaseDtlService;
import com.maple.restserver.service.PurchaseExpenseAccountService;
import com.maple.restserver.service.PurchaseService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.service.accounting.task.PurchaseAccounting;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
@CrossOrigin("http://localhost:4200")
@RestController

public class PurchaseHdrResource {
	private static final Logger logger = LoggerFactory.getLogger(PurchaseHdrResource.class);
	
	//----------------version 5.0 surya
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	@Autowired
	ItemBatchDtlServiceimpl itemBatchDtlServiceimpl;
	//----------------version 5.0 surya end
	@Autowired
	MultiUnitMstRepository multiUnitMstRepository;

	@Autowired
	private PurchaseHdrRepository purchaseHdrRepository;

	@Autowired
	private PurchaseDtlRepository purchaseDtlRepository;

	@Autowired
	private CompanyMstRepository companyMstRepository;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	private ItemMstRepository itemMstRepository;

	@Autowired
	private ItemBatchMstRepository itemBatchMstRepository;

	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	LmsQueueTallyMstRepository lmsQueueTallyMstRepository;

	@Autowired
	private AccountPayableRepository accountPayableRepository;

	@Autowired
	private AccountHeadsRepository accountHeadsRepository;

	@Autowired
	PurchaseAccounting purchaseAccounting;
	
	@Autowired
	MultiUnitConversionServiceImpl multiUnitConversionServiceImpl;
	
	@Autowired
	PurchaseDtlService purchaseDtlService;
	
	@Autowired
	PurchaseService purchaseService;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	// @Autowired
	// private RuntimeService runtimeService;

	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	private VoucherNumberService voucherService;
	
	@Autowired
	ImportPurchaseSummaryRptService importPurchaseSummaryRptService;
	
	@Autowired
	LocalPurchaseSummaryDtlReportService localPurchaseSummaryDtlReportService;
	
	@Autowired
	PurchaseExpenseAccountService purchaseExpenseAccountService;
	
	@Autowired
	PharmacyLocalPurchaseDetailsReportService pharmacyLocalPurchaseDetailsReportService;

	//boolean recurssionOff = false;
	
	@Value("${ENABLEACCOUNTING}")
	private String ENABLEACCOUNTING;

	@GetMapping("{companymstid}/purchasehdr/{branchcode}")
	public List<PurchaseHdr> retrieveAllPurchaseHdr(@PathVariable(value = "companymstid") String companymstid) {

		return purchaseHdrRepository.findByCompanyMstId(companymstid);

	}

//	@PostMapping("{companymstid}/purchasehdr/{id}/purchasedtl")
//	public PurchaseHdr createPurchaseHdr(@Valid @RequestBody PurchaseHdr purchaseHdr,
//			@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "id") String id) {
//
//		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
//		CompanyMst companyMst = comapnyMstOpt.get();
//		purchaseHdr.setCompanyMst(companyMst);
//		PurchaseHdr saved = purchaseHdrRepository.save(purchaseHdr);
//		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/(id)").buildAndExpand(saved.getId())
//				.toUri();
//		return saved;
//	}
	
	@PostMapping("{companymstid}/purchasehdr")
	public PurchaseHdr createPurchaseHdr(@Valid @RequestBody PurchaseHdr purchaseHdr,
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		purchaseHdr.setCompanyMst(companyMst);
	    PurchaseHdr saved = purchaseHdrRepository.save(purchaseHdr);
		
//		PurchaseHdr saved=saveAndPublishService.savePurchaseHdr(purchaseHdr, mybranch);
		/*
		 * if(null!=saved) { saveAndPublishService.publishObjectToKafkaEvent(saved,
		 * mybranch, KafkaMapleEventType.PURCHASEHDR, KafkaMapleEventType.SERVER); }else
		 * { return null; }
		 * 
		 * logger.info("PurchaseHdr send to KafkaEvent: {}", saved);
		 * 
		 */
//		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/(id)").buildAndExpand(saved.getId())
//				.toUri();
		return saved;
	}
	@Transactional
	@PutMapping("{companymstid}/purchasehdr/changetopartialsave/{purchaseid}")
	public PurchaseHdr purchaseToPArtialSave(@PathVariable String purchaseid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody PurchaseHdr purchasehdrRequest) {
		PurchaseHdr purchaseHdr = purchaseHdrRepository.findById(purchaseid).get();

		purchaseHdr.setFinalSavedStatus("N");

		purchaseAccounting.removePurchaseAccounting(purchaseHdr.getVoucherNumber(), purchaseHdr.getVoucherDate(),
				purchaseHdr.getId(), purchaseHdr.getCompanyMst(), purchaseHdr);
		removePurchaseStock(purchaseHdr, purchaseHdr.getCompanyMst());
		// accountPayableRepository.deleteByVoucherNumber(purchaseHdr.getVoucherNumber());
		return purchaseHdr;

	}

	@PutMapping("{companymstid}/purchasehdr/{purchaseid}")
	public PurchaseHdr purchaseFinalSave(@PathVariable String purchaseid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody PurchaseHdr purchasehdrRequest) {

		PurchaseHdr 	purchase = purchaseHdrRepository.findById(purchaseid).get();

			Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();

			purchase.setCompanyMst(companyMst);

			purchase.setSupplierId(purchasehdrRequest.getSupplierId());
			purchase.setPurchaseType(purchasehdrRequest.getPurchaseType());
			purchase.setCurrency(purchasehdrRequest.getCurrency());

			purchase.setPurchaseType(purchasehdrRequest.getPurchaseType());
			purchase.setUserId(purchasehdrRequest.getUserId());
			
			purchase.setEnableBatchStatus(purchasehdrRequest.getEnableBatchStatus());
			purchase.setInvoiceTotal(purchasehdrRequest.getInvoiceTotal());
			if(null != purchasehdrRequest.getFcInvoiceTotal())
			{
			purchase.setFcInvoiceTotal(purchasehdrRequest.getFcInvoiceTotal());
			}
			purchase.setVoucherNumber(purchasehdrRequest.getVoucherNumber());

			purchase.setVoucherDate(purchasehdrRequest.getVoucherDate());
			purchase.setCurrency(purchasehdrRequest.getCurrency());

			purchase.setDiscount(purchasehdrRequest.getDiscount());
//			     	purchase.setVoucherDate(purchasehdrRequest.getVoucherDate());
			purchase.setFinalSavedStatus(purchasehdrRequest.getFinalSavedStatus());

			
			 
			
			/*
			 * Update Account Payable Voucher Number
			 */
			
			/*
			 * Iniitate process
			 */
			 purchase=saveAndPublishService.savePurchaseHdr(purchase, mybranch);
			  if(null!=purchase) {
				  saveAndPublishService.purchaseFinalSaveToKafkaEvent(purchase, mybranch, KafkaMapleEventType.SERVER);
			  }else {
				  return null;
			  }
			
			logger.info("PurchaseHdr send to KafkaEvent: {}", purchase);
		//	purchase = purchaseHdrRepository.saveAndFlush(purchase);
 
			

			if (purchasehdrRequest.getFinalSavedStatus().equals("Y")) {
				AccountPayable accountPayable = new AccountPayable();

				Optional<AccountPayable> accountPaybleSaved = accountPayableRepository
						.findByVoucherNumberAndVoucherDate(purchasehdrRequest.getVoucherNumber(),
								purchasehdrRequest.getVoucherDate());

				if (accountPaybleSaved.isPresent()) {
					if (null != accountPaybleSaved.get()) {
						accountPayable = accountPaybleSaved.get();
					}
				}

				accountPayable.setCompanyMst(companyMst);
				accountPayable.setAccountId(purchasehdrRequest.getSupplierId());
				accountPayable.setDueAmount(purchasehdrRequest.getInvoiceTotal());

				Optional<AccountHeads> accountHeadsOpt = accountHeadsRepository.findById(purchasehdrRequest.getSupplierId());
				AccountHeads accountHeads = accountHeadsOpt.get();
				int creditPeriod = 0;
				if (null != accountHeads.getCreditPeriod()) {
					creditPeriod = accountHeads.getCreditPeriod();

				}
				////////////////////////////////////////////////

				LocalDate dueDate = LocalDate.now().plusDays(creditPeriod);

				accountPayable.setDueDate(SystemSetting.localToUtilDate(dueDate));
				accountPayable.setPaidAmount(0.0);
				accountPayable.setRemark("PURCHASE");
				accountPayable.setAccountHeads(accountHeads);
				accountPayable.setVoucherDate(purchasehdrRequest.getVoucherDate());
				accountPayable.setVoucherNumber(purchasehdrRequest.getVoucherNumber());
				accountPayable.setPurchaseHdr(purchasehdrRequest);
			//	accountPayable = accountPayableRepository.save(accountPayable);
				accountPayable= saveAndPublishService.saveAccountPayable(accountPayable, mybranch);
			/*
			 * if(null!=accountPayable) {
			 * saveAndPublishService.publishObjectToKafkaEvent(accountPayable, mybranch,
			 * KafkaMapleEventType.ACCOUNTPAYABLE, KafkaMapleEventType.SERVER,
			 * accountPayable.getVoucherNumber()); } else { return null; }
			 */


				/*
				 * Update accounting
				 */
				if (ENABLEACCOUNTING.equalsIgnoreCase("true")) {
					
					logger.info(".........enable-accounting is true...........");
				try {
					purchaseAccounting.execute(purchase.getVoucherNumber(), purchase.getVoucherDate(), purchase.getId(),
							companyMst);
				} catch (PartialAccountingException e) {
					// TODO Auto-generated catch block
					logger.error(e.getMessage());
				}
			}
				/*
				 * Update Stock
				 */
				updatePurchaseStock(purchase, companyMst);
				

				try {
					purchaseExpenseAccountService.doImportExpenseAccounting(purchase);
					purchaseExpenseAccountService.doAdditionalExpenseAccounting(purchase);
				} catch (PartialAccountingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//Posting purchasetranshdr to save tallyRetryMst
				//eventBus.post(purchase);

				//PurchaseHdrMessageEntity purchaseHdrMsgEntity = purchaseService.createPurchaseMsgEntity(purchase.getVoucherNumber(),purchase.getVoucherDate());
			
				//eventBus.post(purchaseHdrMsgEntity);

			}
 

		
			return purchase;
		
		 

	}

	@GetMapping("{companymstid}/purchasehdr/getconversion/{itemid}/{sourceunit}/{targetunit}/{sourceqty}")
	public double getConvertionQtyUrl(@PathVariable(value = "itemid") String itemId,
			@PathVariable(value = "companymstid") String companyMstId,
			@PathVariable(value = "sourceunit") String sourceUnit,
			@PathVariable(value = "targetunit") String targetUnit,
			@PathVariable(value = "sourceqty") Double sourceQty) {
		
		
		return multiUnitConversionServiceImpl.getConvertionQty(companyMstId, itemId, sourceUnit, targetUnit, sourceQty);
		
	/*	recurssionOff = false;
		if (recurssionOff) {
			return sourceQty;
		}
		MultiUnitMst multiUnitMstList = multiUnitMstRepository.findByCompanyMstIdAndItemIdAndUnit1(companyMstId, itemId,
				sourceUnit);
		while (!multiUnitMstList.getUnit2().equalsIgnoreCase(targetUnit)) {
			if (recurssionOff) {
				break;
			}
			sourceUnit = multiUnitMstList.getUnit2();
			sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();
			getConvertionQty(companyMstId, itemId, sourceUnit, targetUnit, sourceQty);
		}
		
		sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();
		recurssionOff = true;
		 
		return sourceQty;*/
		
	}
	
	

	private void updatePurchaseStock(PurchaseHdr purchaseHdr, CompanyMst companyMst) {
		List<ItemBatchDtl> itemBatchDtlList = itemBatchDtlRepository.findBySourceParentId(purchaseHdr.getId());
		if(itemBatchDtlList.size()>0)
		{
		
		
//		itemBatchDtlRepository.deleteBySourceParentId(purchaseHdr.getId());
		}
		List<PurchaseDtl> puchaseDtlList = purchaseDtlRepository.findByPurchaseHdrId(purchaseHdr.getId());
		Iterator iter = puchaseDtlList.iterator();
		while (iter.hasNext()) {
			PurchaseDtl purchaseDtl = (PurchaseDtl) iter.next();

			ItemMst item = itemMstRepository.findById(purchaseDtl.getItemId()).get();



			double conversionQty = 0.0;

				Double qty=0.0;
				if (!item.getUnitId().equalsIgnoreCase(purchaseDtl.getUnitId())) {

					String itemId = item.getId();
					String sourceUnit = purchaseDtl.getUnitId();
					String targetUnit = item.getUnitId();

					/*recurssionOff = false;
					conversionQty = getConvertionQty(purchaseHdr.getCompanyMst().getId(), item.getId(), sourceUnit,
							targetUnit, purchaseDtl.getQty());
							*/
					
					
					
					
					conversionQty =  multiUnitConversionServiceImpl.getConvertionQty(purchaseHdr.getCompanyMst().getId(),
							item.getId(), sourceUnit, targetUnit, purchaseDtl.getQty());
					
					
					qty = conversionQty;
				} else {
					qty = purchaseDtl.getQty();
				}
				String processInstanceId=null;
				String taskId=null;
				
				if(null == purchaseDtl.getStore() || purchaseDtl.getStore().trim().isEmpty())
				{
					purchaseDtl.setStore("MAIN");
				}
				
				itemBatchDtlService.itemBatchMstUpdation(purchaseDtl.getBarcode(),purchaseDtl.getBatch(),purchaseHdr.getBranchCode(),
						purchaseDtl.getExpiryDate(),purchaseDtl.getItemId(),purchaseDtl.getMrp(),processInstanceId,qty,taskId,companyMst,purchaseDtl.getStore());

				
				
				AccountHeads accountHeads = accountHeadsRepository.findById(purchaseHdr.getSupplierId()).get();

				String particulars = "Purchase  from " + accountHeads.getAccountName() + "by " + purchaseHdr.getVoucherNumber();
				Double qtyOut = 0.0;
				final VoucherNumber voucherNo = voucherService.generateInvoice("STV", purchaseHdr.getCompanyMst().getId());
				
				itemBatchDtlService.itemBatchDtlInsertionNew(purchaseDtl.getBarcode(),purchaseDtl.getBatch(),purchaseHdr.getBranchCode(),
						purchaseDtl.getExpiryDate(),purchaseDtl.getItemId(),purchaseDtl.getMrp(),particulars,processInstanceId,qty,qtyOut,purchaseDtl.getId(),
						purchaseHdr.getId(),purchaseHdr.getVoucherDate(),purchaseHdr.getVoucherNumber(),purchaseDtl.getStore(),taskId,purchaseHdr.getVoucherDate(),
						voucherNo.getCode(),companyMst);

			}
	


	}
	


	/*
	private double getConvertionQty(String companyMstId, String itemId, String sourceUnit, String targetUnit,
			double sourceQty) {
		if (recurssionOff) {
			return sourceQty;
		}
		MultiUnitMst multiUnitMstList = multiUnitMstRepository.findByCompanyMstIdAndItemIdAndUnit1(companyMstId, itemId,
				sourceUnit);

		while (!multiUnitMstList.getUnit2().equalsIgnoreCase(targetUnit)) {

			if (recurssionOff) {
				break;
			}
			sourceUnit = multiUnitMstList.getUnit2();

			sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();

			getConvertionQty(companyMstId, itemId, sourceUnit, targetUnit, sourceQty);

		}
		sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();
		recurssionOff = true;
		// sourceQty = sourceQty *
		// multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
		return sourceQty;
	}
*/
	
	private void removePurchaseStock(PurchaseHdr purchaseHdr, CompanyMst companyMst) {

		List<PurchaseDtl> puchaseDtlList = purchaseDtlRepository.findByPurchaseHdrId(purchaseHdr.getId());
		Iterator iter = puchaseDtlList.iterator();
		while (iter.hasNext()) {
			PurchaseDtl purchaseDtl = (PurchaseDtl) iter.next();

			// find if batch mast has an entry
			List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepository
					.findByItemIdAndBatchAndBarcode(purchaseDtl.getItemId(), purchaseDtl.getBatch(),
							purchaseDtl.getBarcode());

			if (itembatchmst.size() == 1) {
				ItemBatchMst itemBatchMst = itembatchmst.get(0);
				itemBatchMst.setMrp(purchaseDtl.getMrp());
				itemBatchMst.setQty(itemBatchMst.getQty() - purchaseDtl.getQty());
				itemBatchMst.setCompanyMst(companyMst);

				itemBatchMst.setBranchCode(purchaseHdr.getBranchCode());

				itemBatchMstRepository.saveAndFlush(itemBatchMst);
			}

			itemBatchDtlRepository.deleteBySourceVoucherNumber(purchaseHdr.getVoucherNumber());

		}

	}

	/*
	 * @GetMapping("/purchaseHdr/{startDate}{endDate}/dailyPurchaseReport") public
	 * List<PurchaseHdr> retrivedailyPurchaseSummary2(
	 * 
	 * @PathVariable(value = "startDate") Date startDate,@PathVariable(value =
	 * "endDate)") Date endDate) { return
	 * purchaseHdrRepository.purchaseSummaryReport(startDate,endDate);
	 * 
	 * }
	 */

	@GetMapping("{companymstid}/purchasehdr/{id}/purchasehdr")
	public Optional<PurchaseHdr> getPurchaseHdr(@PathVariable(value = "id") String id) {
		return purchaseHdrRepository.findById(id);

	}

	@GetMapping("{companymstid}/purchasehdrbystatus/{branchcode}")
	public List<PurchaseHdr> retrieveAllPurchaseHdrByStatus() {

		return purchaseHdrRepository.fetchPurchaseHdr();

	}

	@GetMapping("{companymstid}/purchasehdrbyvouchernumber/{vouchernumber}/purchasehdr")
	public List<PurchaseHdr> getPurchaseHdrByVoucherNumber(@PathVariable(value = "vouchernumber") String vouchernumber,
			@RequestParam("date") String date) {
		java.sql.Date vdate = SystemSetting.StringToSqlDate(date, "yyyy-MM-dd");
		return purchaseHdrRepository.findByVoucherNumberAndVoucherDate(vouchernumber, vdate);

	}

	
	@GetMapping("{companymstid}/purchasehdr/{vouchernumber}/purchasehdrbyvoucher/{branchcode}")
	public PurchaseHdr getPurchaseHdrByVoucherNumberAndBranch(
			@PathVariable(value = "vouchernumber") String vouchernumber,
			@PathVariable(value = "branchcode") String branchcode
			) {
		return purchaseHdrRepository.findByVoucherNumberAndBranchCode(vouchernumber, branchcode);

	}

	
	@GetMapping("{companymstid}/purchasehdr/{supplierid}/{branchcode}/purchasehdrbysupplier")
	public List<PurchaseHdr> getPurchaseHdr(@PathVariable(value = "supplierid") String suppplierid,
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchCode") String branchCode, @RequestParam("fromdate") String fromdate,
			@RequestParam("tomdate") String todate) {
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return purchaseHdrRepository.findBySupplier(suppplierid, fromDate, toDate, branchCode, companyMst);

	}

	@GetMapping("{companymstid}/{branchcode}/purchasehdr/getsupplierandvouchernumber")
	public List<Object> getSupplierAndVouchernumber(@PathVariable("companymstid") String companymstid,

			@PathVariable("branchcode") String branchcode,

			@RequestParam("vDate") String vDate) {

		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		return purchaseHdrRepository.getSupplierAndVouchernumber(branchcode, companyMst.get(), vDate);

	}

	
	

	@DeleteMapping("{companymstid}/purchasehdr/{id}/purchasedelete")
	public void DeleteReceiptDtl(@PathVariable(value = "id") String id) {
		purchaseHdrRepository.deleteById(id);

	}
	
	
	
	
//	@PostMapping("{companymstid}/purchasehdrresource/{purchaseHdrSaved}")
//	public PurchaseHdr createPurchaseHdr(@Valid @RequestBody PurchaseHdr purchasehdrresource,
//			@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "purchaseHdrSaved") String purchaseHdrSaved) {
//
//		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
//		CompanyMst companyMst = comapnyMstOpt.get();
//		purchaseHdrSaved.setCompanyMst(companyMst);
//		PurchaseHdr saved = purchaseHdrRepository.save(purchaseHdrSaved);
//		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/(id)").buildAndExpand(saved.getId())
//				.toUri();
//		return saved;
//	}
	
	@GetMapping("{companymstid}/purchasehdrresource/getimportpurchasesummary")
	public List<ImportPurchaseSummaryReport> getPurchaseHdr(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate) {
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		return importPurchaseSummaryRptService.getImportpurchaseReport(fromDate, toDate);

	}

	
	@GetMapping("{companymstid}/purchasehdrresource/getlocalPurchaseSummaryDtlReport")
	public List<LocalPurchaseSummaryReport> getLocalPurchaseSummarydtlReport(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate) {
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		return localPurchaseSummaryDtlReportService.getLocalPurchaseSummaryDtlReport(fromDate, toDate);

	}
	
	@GetMapping("{companymstid}/purchasehdrresource/getPharmacyLocalPurchaseDetailsReport")
	public List<PharmacyLocalPurchaseDetailsReport> getPharmacyLocalPurchaseDetailsReport(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate) {
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		return pharmacyLocalPurchaseDetailsReportService.getPharmacyLocalPurchaseDetailsReport(fromDate, toDate);

	}
	
	@GetMapping("{companymstid}/purchasehdrresource/getpurchaseconsolidatedreport/{branch}")
	public List<PurchaseReport> getPurchaseConsolidatedReport(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branch") String branch,
			@RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate) {
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		return purchaseDtlService.getPurchaseConsolidatedReport(fromDate, toDate,branch);

	}
	
	

}
