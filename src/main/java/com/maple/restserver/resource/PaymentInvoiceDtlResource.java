package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PaymentInvoiceDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.PaymentInvoiceDtlRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Transactional
public class PaymentInvoiceDtlResource {
private static final Logger logger = LoggerFactory.getLogger(PaymentInvoiceDtlResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
@Autowired
PaymentInvoiceDtlRepository paymentInvoiceDtlRepository;

@Autowired
CompanyMstRepository companyMstRepo;
@Autowired
SalesTransHdrRepository salesTransHdrRepository;
@Autowired 
PaymentHdrRepository paymentHdrRepository ;

@Autowired
SaveAndPublishService saveAndPublishService;

@Autowired
AccountHeadsRepository accountHeadsRepository;
@PostMapping("/{companymstid}/paymentInvoiceDtl")
public PaymentInvoiceDtl createPaymentInvoiceDtl(@PathVariable(value = "companymstid") String
		 companymstid,@Valid @RequestBody PaymentInvoiceDtl paymentInvoiceDtl)
{
	   
	
	return companyMstRepo.findById(companymstid).map(companyMst-> {
		
		paymentInvoiceDtl.setCompanyMst(companyMst);
		
// return paymentInvoiceDtlRepository.saveAndFlush(paymentInvoiceDtl);
		return saveAndPublishService.savePaymentInvoiceDtl(paymentInvoiceDtl, mybranch);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found")); }



@DeleteMapping("{companymstid}/paymentInvoiceDtl/{paymentdtlid}/deletepaymentInvoiceDtl")
public void deletePaymentInvoiceDtlById(
		@PathVariable(value = "paymentdtlid") String paymentdtlid) {

	paymentInvoiceDtlRepository.deleteById(paymentdtlid);

}
@GetMapping("{companymstid}/paymentInvoiceDtl/{paymenthdrid}/getreceiptinvoicedtlbyid")
public List<PaymentInvoiceDtl> retrievePaymentInvoiceDtlByPaymentId(
		@PathVariable(value = "paymenthdrid") String paymenthdrid) {
	Optional<PaymentHdr> paymentHdr  =paymentHdrRepository.findById(paymenthdrid);
	return paymentInvoiceDtlRepository.findByPaymentHdr(paymentHdr.get());

}

@GetMapping("{companymstid}/paymentInvoiceDtl/{paymenthdrid}/sumofpaidamount")
public Double sumOfPaidAmount(
		@PathVariable(value = "paymenthdrid") String paymenthdrid) {
	Optional<PaymentHdr> paymentHdr  =paymentHdrRepository.findById(paymenthdrid);
	return paymentInvoiceDtlRepository.sumOfPaidAmount(paymentHdr.get());

}



@GetMapping("{companymstid}/paymentInvoiceDtl/{supplierid}/sumofpaidamountbysupplier")
public Double sumOfPaymentInvoiceDtlPaidAmount(
		@PathVariable(value = "supplierid") String supplierid) {
	Optional<AccountHeads> accountHeadsOPt  =accountHeadsRepository.findById(supplierid);
	
	return paymentInvoiceDtlRepository.sumOfPaymentInvoiceDtlPaidAmount(accountHeadsOPt.get());

}
}
