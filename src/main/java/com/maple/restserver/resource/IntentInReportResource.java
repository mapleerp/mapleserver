package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.IntentInReport;
import com.maple.restserver.report.entity.IntentVoucherReport;
import com.maple.restserver.service.IntentInReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class IntentInReportResource {

	@Autowired
	IntentInReportService intentInReportService; 

@GetMapping("{companymstid}/intentinvoucherreport/{voucherNumber}")
public List<IntentInReport> getIntentInReport(@PathVariable(value = "companymstid") String
		  companymstid,
		 @PathVariable("voucherNumber") String voucherNumber, @RequestParam("rdate") String reportdate){
	java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"dd-MM-yyyy");

	return intentInReportService.getIntentInReport(voucherNumber, date);

	
}
	
	
}
