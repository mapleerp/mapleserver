package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AdditionalExpense;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AdditionalExpenseRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Transactional
public class AdditionalExpenseResource {
	
private static final Logger logger = LoggerFactory.getLogger(AccountReceivableResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
private AdditionalExpenseRepository additionalExpenseRepository;
	
	 @Autowired
	 SaveAndPublishService saveAndPublishService;

	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@GetMapping("/{companymstid}/additionalexpenses")
	public List<AdditionalExpense> retrieveAllAdditionalExpense(@PathVariable(value = "companymstid")
	String
			  companymstid){
		return additionalExpenseRepository.findByCompanyMst(companymstid);
	}

	@PostMapping("/{companymstid}/additionalexpense")
	public AdditionalExpense createAdditionalExpense(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			AdditionalExpense additionalExpense)
	{
		         logger.info("additionalExpense send to KafkaEvent: {}", additionalExpense);
		 		return saveAndPublishService.saveAdditionalExpense(additionalExpense,mybranch);
	 }
		
	

	@GetMapping("{companymstid}/additionalexpense/sumofamount/{hdrid}")
	public Double getSumOfAmountByPurchaseHdr(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "hdrid")String hdrid)
	{
		return additionalExpenseRepository.findSumOfAmountByPurchaseHdrId(hdrid);
	}

	
	@GetMapping("{companymstid}/additionalexpense/sumoffcamount/{hdrid}")
	public Double getSumOfFCAmountByPurchaseHdr(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "hdrid")String hdrid)
	{
		return additionalExpenseRepository.findSumOfFCAmountByPurchaseHdrId(hdrid);
	}
	@GetMapping("{companymstid}/additionalexpense/sumoffcamountinlcudedinbill/{hdrid}/{expensetype}")
	public Double getSumOfFCAmountIncludedInBIllByPurchaseHdr(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "hdrid")String hdrid,
			@PathVariable(value = "expensetype")String expensetype)
	{
		return additionalExpenseRepository.findSumOfAmountBYExpenseTyepe(hdrid,expensetype);
	}
	
//	@GetMapping("{companymstid}/additionalexpense/sumoffcamountwithstatus/{hdrid}")
//	public Double getSumOfFCAmountWithStatusByPurchaseHdr(
//			@PathVariable(value = "companymstid")String companymstid,
//			@PathVariable(value = "hdrid")String hdrid)
//	{
//		return additionalExpenseRepository.findSumOfAmountBYStatus(hdrid);
//	}

	@DeleteMapping("{companymstid}/additionalexpense/deleteadditionalexpense/{id}")
	public void deleteAdditionalExpense(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value = "id")String id)
	{
		additionalExpenseRepository.deleteById(id);
	}
	@GetMapping("{companymstid}/additionalexpense/getalladditonalexpensebypurchasehdr/{purchasehdrid}")
	public List<AdditionalExpense> getAllAdditionalExpense(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "purchasehdrid")String purchasehdrid)
	{
		return additionalExpenseRepository.findByPurchaseHdrId(purchasehdrid);
	}
	
//	@GetMapping("{companymstid}/additionalexpense/updateadditionalexpensestatus/{purchasehdrid}")
//	public String updateAdditionalExpenseStatus(@PathVariable(value = "companymstid")String companymstid,
//			@PathVariable(value="purchasehdrid")String puchasehdrid)
//	{
//		additionalExpenseRepository.updateAdditionalExpenseStatus(puchasehdrid);
//		return "Success";
//	}
	
	
	@GetMapping("{companymstid}/additionalexpense/sumofamountinfcbyexpencetype/{hdrid}/{expensetype}")
	public Double getSumOfFCAmountByExpenseType(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "hdrid")String hdrid,
			@PathVariable(value = "expensetype")String expensetype)
	{
		return additionalExpenseRepository.getSumOfFCAmountByExpenseType(hdrid,expensetype);
	}
	
	
	@GetMapping("{companymstid}/additionalexpense/sumoffcamountinlcudedinbillbycurrencyid/{hdrid}/{expensetype}/{currencyid}")
	public Double getSumOfFCAmountIncludedInBIllByPurchaseHdr(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "hdrid")String hdrid,
			@PathVariable(value = "currencyid")String currencyid,
			@PathVariable(value = "expensetype")String expensetype)
	{
		return additionalExpenseRepository.findSumOfAmountBYExpenseTyepeByCurrency(hdrid,expensetype,currencyid);
	}
}



