package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchExpiryDtl;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.report.entity.InventoryReorderStatusReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchExpiryDtlRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ItemBatchExpiryDtlResource {
	
	@Autowired
	ItemMstRepository itemmstrepo;
	@Autowired
	ItemBatchExpiryDtlRepository itemBatchExpiryDtlRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("{companymstid}/itembatchexpirydtl/getitemexpiry/{itemid}/{batch}")
	public List<ItemBatchExpiryDtl> getItemBatchExpiry(@PathVariable(value = "companymstid") String companymstid,

			@PathVariable("itemid") String itemid,
			@PathVariable("batch")String batch) {
	
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		return itemBatchExpiryDtlRepo.findByCompanyMstAndItemIdAndBatch(companyMst.get(), itemid, batch);
	}

	@GetMapping("{companymstid}/itembatchexpirydtl/searchbyitemnameandbatch")
	public List<Object> searchItemBatchExpDtl(
			@PathVariable(value = "companymstid")String companymstid,
			@RequestParam(value = "data")String data)
	{
		data = data.replaceAll("20%", " ");
		List<Object> objByItemCode=new ArrayList<Object>();
		objByItemCode = itemBatchExpiryDtlRepo.searchItemBatchExpDtlByItemName("%"+data.toLowerCase()+"%");
		if(objByItemCode.size()==0)
		{
			objByItemCode  = itemBatchExpiryDtlRepo.searchItemBatchExpDtlByBatch("%"+data.toLowerCase()+"%");
			
		}
		return objByItemCode;
	}

}
