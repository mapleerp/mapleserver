package com.maple.restserver.resource;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
//import org.camunda.bpm.spring.boot.starter.event.PostDeployEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.LmsQueueTallyMst;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.PaymentHdrAndDtlMessage;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.LmsQueueTallyMstRepository;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.service.PaymentEditService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.SaveAndPublishServiceImpl;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.service.accounting.task.PaymentAccounting;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
 
  


@RestController
@Transactional
public class PaymentHdrResource{
	 private static final Logger logger = LoggerFactory.getLogger(PaymentHdrResource.class);

	// @Autowired
	 // private RuntimeService runtimeService;
	 
	 EventBus eventBus = EventBusFactory.getEventBus();
	 
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	 
	@Autowired
	private PaymentHdrRepository paymentHdrRepo;
 
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	PaymentDtlRepository paymentDtlRepo;
	
	@Autowired
	PaymentAccounting paymentAccounting;
	
	@Autowired
	PaymentEditService paymentEditService;
	@Autowired
	LmsQueueTallyMstRepository lmsQueueTallyMstRepository;
	
	 @Autowired
     SaveAndPublishService saveAndPublishService;
	 
	 @Autowired
		SaveAndPublishServiceImpl saveAndPublishServiceImpl;
	 
    @GetMapping("{companymstid}/paymenthdr")
    public Page<PaymentHdr> getAllPayments(Pageable pageable) {
        return paymentHdrRepo.findAll(pageable);
    }
     



    
	@PostMapping("{companymstid}/paymenthdr")
	public PaymentHdr createPayment(@PathVariable(value = "companymstid") String companymstid,@Valid @RequestBody PaymentHdr payment)
	{		 
		 return  companyMstRepo.findById(companymstid).map(companyMst-> {
			 payment.setCompanyMst(companyMst);
			 
			 
			 
			 
			 
//	 	return paymentHdrRepo.saveAndFlush(payment);
			 
//			 return saveAndPublishService.savePaymentHdr(payment, payment.getBranchCode());
			 return paymentHdrRepo.save(payment);
				
		 }).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }

	
	
	@PutMapping("{companymstid}/paymenthdr/{paymentId}")
	public PaymentHdr paymentFinalSave(@PathVariable String paymentId,
			@Valid @RequestBody PaymentHdr paymentRequest)
	{
 		 
		/*
		 * ProcessInstanceWithVariables pVariablesInReturn = runtimeService.createProcessInstanceByKey(processID)
				.setVariable("hello", hello)
				.executeWithVariablesInReturn();
		 */
		   		 
				return paymentHdrRepo.findById(paymentId).map(payment -> {
					payment.setVoucherNumber(paymentRequest.getVoucherNumber()); 
					payment.setBranchCode(paymentRequest.getBranchCode());
					payment.setVoucherDate(paymentRequest.getVoucherDate());
					//PaymentHdr payhdr = paymentHdrRepo.saveAndFlush(payment);
//					PaymentHdr payhdr = 	saveAndPublishService.savePaymentHdr(payment, payment.getBranchCode());
					PaymentHdr payhdr = paymentHdrRepo.save(payment);
					logger.info("payhdr send to KafkaEvent: {}", payhdr);
					List<PaymentDtl> paymentDtlList = paymentDtlRepo.findBypaymenthdrId(payhdr.getId());
					
					PaymentHdrAndDtlMessage paymentHdrAndDtlMessage = new PaymentHdrAndDtlMessage();
					paymentHdrAndDtlMessage.setPaymentHdr(payhdr);
					paymentHdrAndDtlMessage.getPaymentDtlList().addAll(paymentDtlList);
					
					saveAndPublishServiceImpl.publishObjectToKafkaEvent(paymentHdrAndDtlMessage, 
							payhdr.getBranchCode(),
								KafkaMapleEventType.PAYMENTHDRANDDTL, 
								KafkaMapleEventType.SERVER,payhdr.getId());
					
					try {
						paymentAccounting.execute(payhdr.getVoucherNumber(), payhdr.getVoucherDate(),
								payhdr.getId(), payment.getCompanyMst());
					} catch (PartialAccountingException e) {
						// TODO Auto-generated catch block
						logger.error(e.getMessage());
					}
					//
					eventBus.post(payhdr);	
					
					Map<String, Object> variables = new HashMap<String, Object>();
					
					variables.put("voucherNumber", payhdr.getVoucherNumber());
					variables.put("voucherDate", payhdr.getVoucherDate());
					variables.put("inet", 0);
					variables.put("REST", 1);
					variables.put("id", payhdr.getId());
					variables.put("companyid", payment.getCompanyMst());
					
					variables.put("branchcode", payment.getBranchCode());

					variables.put("WF", "forwardPayment");
					
					
					String workflow = (String) variables.get("WF");
		    		String voucherNumber = (String) variables.get("voucherNumber");
		    		String sourceID = (String) variables.get("id");


		    		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		    		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		    		
		    		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		    	
		    		
		    		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		    		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		    		lmsQueueMst.setVoucherDate(sqlVDate);
		    		
		    		
		    		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		    		lmsQueueMst.setVoucherType(workflow);
		    		lmsQueueMst.setPostedToServer("NO");
		    		lmsQueueMst.setJobClass("forwardPayment");
		    		lmsQueueMst.setCronJob(true);
		    		lmsQueueMst.setJobName(workflow + sourceID);
		    		lmsQueueMst.setJobGroup(workflow);
		    		lmsQueueMst.setRepeatTime(60000L);
		    		lmsQueueMst.setSourceObjectId(sourceID);
		    		
		    		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		    		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		    		variables.put("lmsqid", lmsQueueMst.getId());
					eventBus.post(variables);
					LmsQueueTallyMst lmsQueueTallyMst=new LmsQueueTallyMst();
					lmsQueueTallyMst.setCompanyMst((CompanyMst) variables.get("companyid"));
					lmsQueueTallyMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
					lmsQueueTallyMst.setVoucherNumber((String) variables.get("voucherNumber"));
					lmsQueueTallyMst.setVoucherType(workflow);
					lmsQueueTallyMst.setPostedToTally("NO");
					lmsQueueTallyMst.setJobClass("forwardPayment");
					lmsQueueTallyMst.setCronJob(true);
					lmsQueueTallyMst.setJobGroup(workflow);
					lmsQueueTallyMst.setRepeatTime(60000L);
					lmsQueueTallyMst.setSourceObjectId(sourceID);
					lmsQueueTallyMst.setBranchCode((String) variables.get("branchcode"));
					lmsQueueTallyMst=lmsQueueTallyMstRepository.saveAndFlush(lmsQueueTallyMst);
					variables.put("lmsqid", lmsQueueMst.getId());
					eventBus.post(variables);
					
				/*	ProcessInstanceWithVariables pVariablesInReturn  = runtimeService.createProcessInstanceByKey("Forwardpayment")
							.setVariables(variables)
							 
							.executeWithVariablesInReturn();*/
					
		            return payhdr;
		            
		        }).orElseThrow(() -> new ResourceNotFoundException("paymentId " + paymentId + " not found"));
				
			
			
	}
	 

    @DeleteMapping("{companymstid}/paymenthdr/{paymentId}")
    public ResponseEntity<?> deletePost(@PathVariable String paymentId) {
        return paymentHdrRepo.findById(paymentId).map(payment -> {
        	paymentHdrRepo.delete(payment);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("paymentId " + paymentId + " not found"));
    }

// here we are fetching the payment summary between two dates
    
    @GetMapping("/paqymentHdr/{startDate}{endDate}/paymentSummaryReport")
	public List<PaymentHdr> retrivedailyPaymentSummary(
			@PathVariable(value = "startDate") Date startDate,@PathVariable(value = "endDate)") Date endDate) {
		return paymentHdrRepo.paymentSummaryReport(startDate,endDate);

}
    
    

	@GetMapping("{companymstid}/paymentsummary/paymentsummary")
	public List<Object> getDailyPaymentSummary( @RequestParam("reportdate") 
    String reportdate){
		
		
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd" );
		 
		
		return  paymentHdrRepo.PaymentDetails(date) ;
	}
	
	@GetMapping("{companymstid}/paymenttotal/{hdrid}/paymenttotalamount")
	public Double getReceiptDtlAmount(
			 @PathVariable("hdrid") String hdrId){
	
		return paymentDtlRepo.getSumOfPaymentDtlAmount(hdrId);

	

	}
    
	@GetMapping("{companymstid}/paymenthdrresouce/paymenthdr/{hdrid}")
	public Optional<PaymentHdr> getPaymentHdr(
			 @PathVariable("hdrid") String hdrId){
	
		return paymentHdrRepo.findById(hdrId);

	

	}
    

	@GetMapping("{companymstid}/paymenthdrresouce/findbyvouchernumber/{vouchernumber}")
	public PaymentHdr  getPaymentHdrByVoucherNumber(
			 @PathVariable("vouchernumber") String vouchernumber){
	
		return paymentHdrRepo.fetchPaymentHdrByVoucherNumber(vouchernumber);

	

	}
	 @PutMapping("{companymstid}/paymenthdr/{paymentDtl}/updatepaymentdtl")
	 public PaymentDtl updateReceiptDtl(@PathVariable (value = "paymentDtl") String paymentDtl,
            @PathVariable (value = "companymstid") String companymstid,
            @Valid @RequestBody PaymentDtl paymentDtlRequest) {
		 PaymentDtl paymentDtls = paymentDtlRepo.findById(paymentDtl).get();
//		 	receiptDtl.setAccount(receiptDtlRequest.getAccount());
//		 	receiptDtl.setAmount(receiptDtlRequest.getAmount());
//		 	receiptDtl.setBankAccountNumber(receiptDtlRequest.getBankAccountNumber());
//		 	receiptDtl.setBranchCode(receiptDtlRequest.getBranchCode());
//		 	receiptDtl.setDebitAccountId(receiptDtlRequest.getDebitAccountId());
//		 	receiptDtl.setDepositBank(receiptDtlRequest.getDepositBank());
//		 	receiptDtl.setInstrumentDate(receiptDtlRequest.getInstrumentDate());
//		 	receiptDtl.setInstnumber(receiptDtlRequest.getInstnumber());
//		 	receiptDtl.setInvoiceNumber(receiptDtlRequest.getInvoiceNumber());
//		 	receiptDtl.setModeOfpayment(receiptDtlRequest.getModeOfpayment());
//		 	receiptDtl.setTransDate(receiptDtlRequest.getTransDate());
//		 	receiptDtl.setRemark(receiptDtlRequest.getRemark());
//		 	receiptDtl = receiptDtlRepo.save(receiptDtl);
		 paymentEditService.deletePaymentAndOwnAccount(companymstid, paymentDtls.getPaymenthdr().getId(), paymentDtls);
		 	return paymentDtls;
		 
	 }
	 
    
    

	 @PostMapping("{companymstid}/paymenthdrresource/{paymentId}/paymentdtl")
	    public PaymentDtl createPaymentDtls(@PathVariable (value = "paymentId") String paymentId,
	                                 @Valid @RequestBody PaymentDtl paymentdtlRequest) {
	        return paymentHdrRepo.findById(paymentId).map(paymenthdr -> {
	        	paymentdtlRequest.setPaymenthdr(paymenthdr); 
	            return paymentDtlRepo.saveAndFlush(paymentdtlRequest);
	        }).orElseThrow(() -> new ResourceNotFoundException("receiptId 1" + paymentId + " not found"));
	    }
	
    
    }
