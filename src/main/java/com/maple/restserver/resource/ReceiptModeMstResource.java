package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ParamMst;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ReceiptModeMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.ReceiptModeReport;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ReciptModeMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ReceiptModeMstService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ReceiptModeMstResource {

	private static final Logger logger = LoggerFactory.getLogger(ReceiptModeMstResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	// @Autowired
	// private RuntimeService runtimeService;
	EventBus eventBus = EventBusFactory.getEventBus();
	Pageable topFifty =   PageRequest.of(0, 50);
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	ReciptModeMstRepository reciptModeMstRepository;

	@Autowired
	CompanyMstRepository CompanyMstRepository;

	// ------------new version 1.8 surya
	@Autowired
	ReceiptModeMstService receiptModeMstService;

	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	// ------------new version 1.8 surya end

	@PostMapping("{companymstid}/receiptmode/{branchcode}")
	public ReceiptModeMst createReceiptMode(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@Valid @RequestBody ReceiptModeMst receiptMst) {

		
		
		 Optional<CompanyMst> companyMstOpt = CompanyMstRepository.findById(companymstid);
		 
		 CompanyMst companyMst = companyMstOpt.get();
		 

		receiptMst.setCompanyMst(companyMst);

		VoucherNumber voucherNo = voucherService.generateInvoice(branchcode, "RM");

		receiptMst.setId(voucherNo.getCode());
//		ReceiptModeMst saved = reciptModeMstRepository.save(receiptMst);
		
		ReceiptModeMst saved =saveAndPublishService.saveReceiptModeMst(receiptMst,mybranch);
		logger.info("ReceiptModeMst send to KafkaEvent: {}", saved);
		

		if(null == saved)
		{
			return null;
		}
		AccountHeads accountHeads = accountHeadsRepository.findByAccountName(saved.getReceiptMode());
			if (null == accountHeads) {
				
				accountHeads = new AccountHeads();


			
			 accountHeads = new AccountHeads();

			accountHeads.setAccountName(saved.getReceiptMode());
			accountHeads.setGroupOnly("N");
			accountHeads.setId(saved.getId());
			accountHeads.setCompanyMst(companyMst);
			accountHeadsRepository.save(accountHeads);



		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("voucherNumber", saved.getId());
		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", saved.getId());
		variables.put("companyid", saved.getCompanyMst());
		variables.put("branchcode", saved.getCompanyMst().getId());

		variables.put("REST", 1);

		variables.put("WF", "forwardRecieptMode");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));

		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardRecieptMode");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());

		//eventBus.post(variables);

		/*
		 * ProcessInstanceWithVariables pVariablesInReturn =
		 * runtimeService.createProcessInstanceByKey("forwardReceiptMode")
		 * .setVariables(variables)
		 * 
		 * .executeWithVariablesInReturn();
		 */
			}

		return saved;

	 	 
	}

//====================edited on 07-05======by anandu============
	@GetMapping("{companymstid}/findallreceipt")
	public @ResponseBody List<ReceiptModeMst> findAllreceiptMode(
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMst = CompanyMstRepository.findById(companymstid);
		return reciptModeMstRepository.findByNewReceiptMode(companyMst.get());
	}

	// =============================new url for receiptmode except
	// insurance======================

	@GetMapping("{companymstid}/findallreceiptexceptinsurance")
	public @ResponseBody List<ReceiptModeMst> findAllreceiptModeExceptInsurance(
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMst = CompanyMstRepository.findById(companymstid);
		return reciptModeMstRepository.findByNewReceiptModeExceptInsurance(companyMst.get());

	}

//=========================end===================================
	@GetMapping("{companymstid}/findallreceiptmodes")
	public @ResponseBody List<ReceiptModeMst> findAllreceiptModes(
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMst = CompanyMstRepository.findById(companymstid);
		return reciptModeMstRepository.findByCompanyMst(companyMst.get());
	}

	@PutMapping("{companymstid}/updatereceiptmode/{receiptid}")
	public ReceiptModeMst ReceiptModeMstUpdate(@PathVariable(value = "receiptid") String receiptid,
			@Valid @RequestBody ReceiptModeMst receiptRequest) {

		return reciptModeMstRepository.findById(receiptid).map(receiptmst -> {
			receiptmst.setStatus(receiptRequest.getStatus());
			receiptmst.setCreditCardStatus(receiptRequest.getCreditCardStatus());
//			ReceiptModeMst receiptSaved = reciptModeMstRepository.saveAndFlush(receiptmst);
			ReceiptModeMst receiptSaved =saveAndPublishService.saveReceiptModeMst(receiptmst, mybranch);
			logger.info("ReceiptModeMst send to KafkaEvent: {}", receiptSaved);

			return receiptSaved;
		}).orElseThrow(() -> new ResourceNotFoundException(" Error saving receipt mode "));

	}

	@DeleteMapping("{companymstid}/receiptmodedelete/{id}")
	public void receiptModenMstDelete(@PathVariable(value = "id") String Id) {
		reciptModeMstRepository.deleteById(Id);

	}

	@GetMapping("{companymstid}/findreceiptmode/{receiptmode}")
	public @ResponseBody ReceiptModeMst findByreceiptMode(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "receiptmode") String receiptmode) {
		Optional<CompanyMst> companyMst = CompanyMstRepository.findById(companymstid);
		return reciptModeMstRepository.findByCompanyMstAndReceiptMode(companyMst.get(), receiptmode);
	}

	// ---------------nwe version 1.8 surya

	@GetMapping("{companymstid}/receiptmodemst/findtotalcardamount/{branchcode}")
	public List<ReceiptModeReport> findTotalCardAmount(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam(value = "date") String date) {

		Date sdate = SystemSetting.StringToUtilDate(date, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst = CompanyMstRepository.findById(companymstid);
		return receiptModeMstService.findTotalCardAmount(companyMst.get(), branchcode, sdate);
	}

	// ---------------nwe version 1.8 surya end

	// ---------------nwe version 1.9 surya

	@GetMapping("{companymstid}/findreceiptmode/getrecieptmodebyname/{receiptmode}")
	public List<ReceiptModeMst> findByreceiptModeByName(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "receiptmode") String receiptmode) {

		Optional<CompanyMst> companyMst = CompanyMstRepository.findById(companymstid);
		return reciptModeMstRepository.findByReceiptModeAndCompanyMst(receiptmode, companyMst.get());
	}
	// ---------------nwe version 1.9 surya end

	// ==================ANANDU NEW RECEIPTMODE===========

	@GetMapping("{companymstid}/receiptmodemstresource/findnewreceiptmode/getnewrecieptmodebyname/{branchcode}")
	public String initializeNewReceiptMode(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		return receiptModeMstService.initializeNewReceiptMode(companymstid, branchcode);
	}

	// ==========END NEW RECEIPTMODE=========

///******************************************************************************////
	@GetMapping("{companymstid}/receiptmodemst/receiptmodesearch")
	public @ResponseBody List<ReceiptModeMst> searchReceiptMode(
			@PathVariable(value = "companymstid") String companymstid, @RequestParam("data") String searchstring) {
		return reciptModeMstRepository.searchLikeReceiptModeByName("%" + searchstring + "%", companymstid, topFifty);
	}

}
