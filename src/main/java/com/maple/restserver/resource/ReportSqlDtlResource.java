package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReportSqlDtl;
import com.maple.restserver.entity.ReportSqlHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ReportSqlDtlRepository;
import com.maple.restserver.repository.ReportSqlHdrRepository;


@RestController
@Transactional
public class ReportSqlDtlResource {
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	ReportSqlDtlRepository reportSqlDtlRepository;
	
	@Autowired
	ReportSqlHdrRepository reportSqlHdrRepository;
	
	
	//save 
	@PostMapping("{companymstid}/reportsqldtlresource/savereportsqldtl")
	public ReportSqlDtl createReportSqlDtl(
			@PathVariable(value="companymstid")	String commpanymstid,
			@Valid @RequestBody ReportSqlDtl reportSqlDtl)
	{
		CompanyMst companymst=companyMstRepo.findById(commpanymstid).get();
		reportSqlDtl.setCompanyMst(companymst);
		reportSqlDtl=reportSqlDtlRepository.save(reportSqlDtl);
		return reportSqlDtl;
	}
	
	@DeleteMapping("{companymstid}/reportsqldtlresource/deletereportsqldtl/{id}")
	public void DeleteReportSqlDtl(@PathVariable(value = "id") String id) {
		
		Optional<ReportSqlDtl> reportSqlDtl = reportSqlDtlRepository.findById(id);
		String HdrId = reportSqlDtl.get().getReportSqlHdrId();
		
		reportSqlDtlRepository.deleteById(id);
		
		List<ReportSqlDtl> reportSqlList = reportSqlDtlRepository.findByReportSqlHdrId(HdrId);
		if(reportSqlList.size() == 0)
		{
			reportSqlHdrRepository.deleteById(HdrId);

		}
	}	
	//showing all 
	@GetMapping("{companymstid}/reportsqldtlresource/showallreportsqldtl")
	public List<ReportSqlDtl> showallReportSqlDtl(
			
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		
		return reportSqlDtlRepository.findByCompanyMst(companyMstOpt.get());
	}	
	@GetMapping("{companymstid}/reportsqldtlresource/reportsqldtlbyhdrid/{id}")
	public List<ReportSqlDtl> retrieveAllReportSqlByReportName(
	@PathVariable(value ="companymstid")String companymstid,
	@PathVariable(value ="id")String id)
	{
		return reportSqlDtlRepository.findByCompanyMstIdAndReportSqlHdrId(companymstid,id);
	};
	

}
