package com.maple.restserver.resource;

import java.math.BigDecimal;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
 
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.AccountClassfcRepository;
import com.maple.restserver.accounting.service.InventoryLedgerMstService;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CommonReceiveMsgEntity;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.FinancialYearMst;
import com.maple.restserver.entity.IntentInHdr;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.ProcessCompleteMessage;
import com.maple.restserver.entity.StockTransferMessageEntity;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.entity.StockValueApr;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.StockTransferOutDtlRepository;
import com.maple.restserver.repository.StockTransferOutHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.FinancialYearService;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.StockTransferOutDtlService;
import com.maple.restserver.service.StockTransferOutService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController

public class StockTransferOutHdrResource {
	private final Logger logger = LoggerFactory.getLogger(StockTransferOutHdr.class);
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	StockTransferOutService stockTransferOutService;
	@Autowired
	PriceDefinitionRepository priceDefinitionRepository;
	@Autowired
	FinancialYearService financialYearService;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepo;
	@Autowired
	CategoryMstRepository categoryMstRepository;
	@Autowired
	private ItemBatchMstRepository itemBatchMstRepo;

	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	private StockTransferOutDtlRepository stockTransferOutDtlRepo;

	@Autowired
	private StockTransferOutHdrRepository stockTransferOutHdrRepo;

	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepository;

	@Autowired
	ItemMstRepository itemMstRepository;

	// ----------------version 5.0 surya

	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	// ----------------version 5.0 surya end
	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired 
	SaveAndPublishService saveAndPublishService;

	EventBus eventBus = EventBusFactory.getEventBus();

	// @Autowired
	// private RuntimeService runtimeService;

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	private StockTransferOutHdrRepository stockTransferOutHdrRepository;

	@Autowired
	ProcessCompleteMessage processCompleteMessage;

	@Autowired
	StockTransferOutDtlService stockTransferOutDtlService;
	
	@Autowired
	private ExternalApi externalApi;
	
	
	@Autowired
	private AccountClassRepository accountClassRepository;
	
	
	 @Autowired
		InventoryLedgerMstService inventoryledgerMstService;

	@GetMapping("{companymstid}/stocktransferouthdr")
	public List<StockTransferOutHdr> retrieveAllStockTransferOutHdr(
			@PathVariable(value = "companymstid") String companymstid) {
		return stockTransferOutHdrRepository.findByCompanyMstId(companymstid);
	}

	@PostMapping("{companymstid}/stocktransferouthdr/{stocktransferouthdrId}/stocktransferoutdtl")
	public StockTransferOutDtl createStockTransferOutDtl(
			@PathVariable(value = "stocktransferouthdrId") String stocktransferouthdrId,
			@Valid @RequestBody StockTransferOutDtl stockTransferOutDtlRequest) {

		/*
		 * 1. Find Hdr. 2. find by Item Id and rate 3. If found add qty else insert qty
		 */

		Optional<StockTransferOutHdr> stocktransferOutHdrOpt = stockTransferOutHdrRepo.findById(stocktransferouthdrId);

		if (!stocktransferOutHdrOpt.isPresent()) {
			return null;
		}

		StockTransferOutHdr stocktransferOutHdr = stocktransferOutHdrOpt.get();
		List<StockTransferOutDtl> stockTransferOutDtlList = stockTransferOutDtlRepo
				.findByItemIdAndUnitIdAndStockTransferOutHdr(stockTransferOutDtlRequest.getItemId(),
						stockTransferOutDtlRequest.getUnitId(), stocktransferOutHdr);
		if (stockTransferOutDtlList.size() > 0) {
			StockTransferOutDtl stockTransferOutDtl = stockTransferOutDtlList.get(0);
			stockTransferOutDtl.setQty(stockTransferOutDtl.getQty() + stockTransferOutDtlRequest.getQty());
			//==============================surya============================
			stockTransferOutDtl.setAmount(stockTransferOutDtl.getAmount() + stockTransferOutDtlRequest.getAmount());
			//==========================surya============
			stockTransferOutDtlRequest = stockTransferOutDtlRepo.save(stockTransferOutDtl);
		} else {
			stockTransferOutDtlRequest.setStockTransferOutHdr(stocktransferOutHdr);
			stockTransferOutDtlRequest.setCompanyMst(stocktransferOutHdr.getCompanyMst());
			stockTransferOutDtlRequest = stockTransferOutDtlRepo.save(stockTransferOutDtlRequest);

		}
		
		stockTransferOutDtlService.updateStocktransferOutDisplaySerial(stocktransferouthdrId);

		return stockTransferOutDtlRequest;

	}

	@GetMapping("{companymstid}/stocktransferouthdr/{stocktransferouthdrid}/stocktransferoutdtl")
	public List<StockTransferOutDtl> retrieveAllStockTransferDtlByStockTransferHdrId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "stocktransferouthdrid") String stocktransferouthdrid) {

		stockTransferOutDtlService.updateStocktransferOutDisplaySerial(stocktransferouthdrid);

		return stockTransferOutDtlRepo.findByStockTransferOutHdrIdOrderByDisplaySrl(stocktransferouthdrid);

	}

	@GetMapping("{companymstid}/stocktransferouthdr/resendfailedstocktransfer/{stkid}")
	public String resendFailedStockTransfer(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "stkid") String stkid) {
		Optional<StockTransferOutHdr> hdrOpt = stockTransferOutHdrRepository.findById(stkid);

		StockTransferOutHdr stockTransferOutHdr = hdrOpt.get();
		
		
		
		stockTransferOutHdr=saveAndPublishService.saveStockTransferOutHdr(stockTransferOutHdr,mybranch,stockTransferOutHdr.getToBranch());
		
 
/*
		ArrayList<ItemMst> itemList = new ArrayList();
		ArrayList<CategoryMst> catList = new ArrayList();
		ArrayList<PriceDefinition> priceList = new ArrayList();
		ArrayList<PriceDefenitionMst> priceDefMstList = new ArrayList();

		StockTransferMessageEntity stockTransMessageEntity = new StockTransferMessageEntity();
		stockTransMessageEntity.setStockTransferOutHdr(stockTransferOutHdr);
		ArrayList<StockTransferOutDtl> stockDtl = stockTransferOutDtlRepo.findByStockTransferOutHdrIdAndCompanyMstId(
				stockTransferOutHdr.getId(), stockTransferOutHdr.getCompanyMst().getId());
		stockTransMessageEntity.getStockTransferOutDtlList().addAll(stockDtl);
		stockTransMessageEntity.setItemCount(stockDtl.size());
		try {

			for (StockTransferOutDtl stockTran : stockDtl) {
				Optional<ItemMst> item = itemMstRepository.findById(stockTran.getItemId());
 
				itemList.add(item.get());
				Optional<CategoryMst> category = categoryMstRepository.findById(item.get().getCategoryId());
 
				catList.add(category.get());
				List<PriceDefinition> priceDefList = priceDefinitionRepository.findByItemId(stockTran.getItemId());

				for (PriceDefinition priceDef : priceDefList) {

					Optional<PriceDefenitionMst> priceDefMst = priceDefinitionMstRepository
							.findById(priceDef.getPriceId());
					if (priceDefMst.isPresent()) {
						PriceDefenitionMst priceDefenitionMst = priceDefMst.get();
						priceDefMstList.add(priceDefenitionMst);
						priceList.add(priceDef);
					}
 

				}

			}
			stockTransMessageEntity.getCategoryList().addAll(catList);
			stockTransMessageEntity.getItemmstList().addAll(itemList);
			stockTransMessageEntity.getPriceDefinitionList().addAll(priceList);
			stockTransMessageEntity.getPriceDefinitionMstList().addAll(priceDefMstList);
			CommonReceiveMsgEntity commonReceiveMsgEntity = new CommonReceiveMsgEntity();
			commonReceiveMsgEntity.setStockTransferMessageEntity(stockTransMessageEntity);
			commonReceiveMsgEntity.setMessageKey("STOCKTRANSFERIN");
			if(null != commonReceiveMsgEntity.getStockTransferMessageEntity().getStockTransferOutHdr().getCompanyMst()) {
				companymstid = commonReceiveMsgEntity.getStockTransferMessageEntity().getStockTransferOutHdr().getCompanyMst().getId();
			}
			ResponseEntity<String> message = externalApi.commonReceiveMsgToCloud(commonReceiveMsgEntity,companymstid);
		} catch (Throwable e) {

			System.out.print(e.toString());
		} */
		return "Done";
	}

	@PostMapping("{companymstid}/stocktransferouthdr")
	public StockTransferOutHdr createstockTransferOutHdr(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody StockTransferOutHdr stockTransferOutHdr) {
	Optional<CompanyMst>	companyMst=	companyMstRepo.findById(companymstid);
	if(!companyMst.isPresent()) {
		logger.error("company not found {}",companymstid);
		
		
		return null;
	}
		stockTransferOutHdr.setCompanyMst(companyMst.get());
		
//		stockTransferOutHdr=saveAndPublishService.saveStockTransferOutHdr(stockTransferOutHdr,mybranch,stockTransferOutHdr.getToBranch());
	
		stockTransferOutHdr = stockTransferOutHdrRepo.save(stockTransferOutHdr);
		return stockTransferOutHdr;
		
	
	}

	@PutMapping("{companymstid}/stocktransferouthdr/{stocktransferouthdrid}/updatestocktransfer")
	public StockTransferOutHdr stockTransferBranchUpdate(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable String stocktransferouthdrid,
			@Valid @RequestBody StockTransferOutHdr StockTransferouthdrRequest) {

		StockTransferOutHdr stockTransferOutHdr = stockTransferOutHdrRepository
				.findByIdAndCompanyMstId(stocktransferouthdrid, companymstid);

		stockTransferOutHdr.setToBranch(StockTransferouthdrRequest.getToBranch());

		stockTransferOutHdr.setIntentNumber(StockTransferouthdrRequest.getIntentNumber());

		stockTransferOutHdr = stockTransferOutHdrRepo.save(stockTransferOutHdr);
		return stockTransferOutHdr;

	}

	@PutMapping("{companymstid}/stocktransferouthdr/{stocktransferouthdrid}")
	public StockTransferOutHdr paymentFinalSave(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable String stocktransferouthdrid,
			@Valid @RequestBody StockTransferOutHdr StockTransferouthdrRequest) {

		StockTransferOutHdr stockTransferOutHdr = stockTransferOutHdrRepository
				.findByIdAndCompanyMstId(stocktransferouthdrid, companymstid);
		
		
		
		
		
		//formatting year in yy format like 21,22 etc
		
		String financialYear = SystemSetting.getFinancialYear();
		String formattingyear=financialYearService.formattingYear(financialYear);
	
		
		if (null == StockTransferouthdrRequest.getVoucherNumber()) {
			 VoucherNumber voucherNo = voucherService.generateInvoice(financialYear+StockTransferouthdrRequest.getFromBranch(),companymstid);
			
			
				String[] voucherNoSplitList=voucherNo.getCode().split(financialYear);
				
	
				String voucherNoStr ;
				if(voucherNoSplitList.length >= 2 )
				{
				
				String voucherNoSplit = voucherNoSplitList[1];

				voucherNoStr= formattingyear + voucherNoSplit;
				} else {
					voucherNoStr = voucherNo.getCode();
				}
			 
			 stockTransferOutHdr.setVoucherNumber(voucherNoStr);
			 
			 System.out.println("Voucher number generated = "+voucherNo);
			 System.out.println("Voucher number generated = "+voucherNo);
		}
		else {
			stockTransferOutHdr.setVoucherNumber(StockTransferouthdrRequest.getVoucherNumber());
		}
		if(null!=StockTransferouthdrRequest.getVoucherDate()){
			stockTransferOutHdr.setVoucherDate(StockTransferouthdrRequest.getVoucherDate());
			
		}
		stockTransferOutHdr.setToBranch(StockTransferouthdrRequest.getToBranch());

//		stockTransferOutHdr = stockTransferOutHdrRepository.saveAndFlush(stockTransferOutHdr);
		stockTransferOutHdr=saveAndPublishService.saveStockTransferOutHdrRep(stockTransferOutHdr,mybranch,stockTransferOutHdr.getToBranch());
		if (null!=stockTransferOutHdr)
		{
			saveAndPublishService.saveStockTransferOutHdr(stockTransferOutHdr,mybranch,stockTransferOutHdr.getToBranch());
		}
		else {
			return  null;
		}
		updateStockTransferOutStock(stockTransferOutHdr, stockTransferOutHdr.getCompanyMst());
        
		
		
//		StockTransferMessageEntity stockTransferMsgEntity = stockTransferOutService
//				.createMessageEntity(stockTransferOutHdr.getId());
//
//		eventBus.post(stockTransferMsgEntity);

		// processCompleteMessage.setProcessType("STOCKTRANSFER");
		// processCompleteMessage.setProcessStatus("COMPLETED");

		// eventBus.post(processCompleteMessage);

		/*
		 * ProcessInstanceWithVariables pVariablesInReturn =
		 * runtimeService.createProcessInstanceByKey("ForwardStockTransferOut")
		 * .setVariables(variables) .executeWithVariablesInReturn();
		 * 
		 */

		return stockTransferOutHdr;

	}

	private void updateStockTransferOutStock(StockTransferOutHdr stockTransferOutHdr, CompanyMst comapnyMst) {

		List<StockTransferOutDtl> stockTransferOutDtlList = stockTransferOutDtlRepo
				.findByStockTransferOutHdrIdAndCompanyMstId(stockTransferOutHdr.getId(), comapnyMst.getId());

		Iterator iter = stockTransferOutDtlList.iterator();
		while (iter.hasNext()) {
			StockTransferOutDtl stockTransferOutDtl = (StockTransferOutDtl) iter.next();

			CompanyMst compantMst = stockTransferOutDtl.getStockTransferOutHdr().getCompanyMst();

			//=======apply a common function for item_batch_mst updation and 
			// ======== item_batch_dtl insertion ====== by anandu === 25-06-2021 
			String store = null;
			if (null != stockTransferOutDtl.getStoreName()) {
				store = stockTransferOutDtl.getStoreName();
			} else {
				store = "MAIN";
			}
			Double qty = stockTransferOutDtl.getQty() * -1;
			Date ExpiryDate = null;
			String processInstanceId = null;
			String taskId = null;
			
			itemBatchDtlService.itemBatchMstUpdation(stockTransferOutDtl.getBarcode(),stockTransferOutDtl.getBatch(),stockTransferOutHdr.getFromBranch(),
					ExpiryDate,stockTransferOutDtl.getItemId().getId(),stockTransferOutDtl.getMrp(),processInstanceId,qty,taskId,stockTransferOutHdr.getCompanyMst(),store);
			
			
			
			String particular = "STOCK TRANSFER TO " + stockTransferOutHdr.getToBranch();
			Double QtyIn = 0.0;
			final VoucherNumber voucherNo = voucherService.generateInvoice("STV", stockTransferOutHdr.getCompanyMst().getId());
			

			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
			itemBatchDtl.setBarcode(stockTransferOutDtl.getBarcode());
			itemBatchDtl.setBatch(stockTransferOutDtl.getBatch());
			itemBatchDtl.setBranchCode(stockTransferOutHdr.getFromBranch());
			itemBatchDtl.setExpDate(ExpiryDate);
			itemBatchDtl.setItemId(stockTransferOutDtl.getItemId().getId());
			itemBatchDtl.setMrp(stockTransferOutDtl.getMrp());
			itemBatchDtl.setParticulars(particular);
			itemBatchDtl.setProcessInstanceId(processInstanceId);
			itemBatchDtl.setQtyIn(QtyIn);
			itemBatchDtl.setQtyOut(stockTransferOutDtl.getQty());
			itemBatchDtl.setSourceDtlId(stockTransferOutDtl.getId());
			itemBatchDtl.setSourceParentId(stockTransferOutHdr.getId());
			itemBatchDtl.setSourceVoucherDate(stockTransferOutHdr.getVoucherDate());
			itemBatchDtl.setSourceVoucherNumber(stockTransferOutHdr.getVoucherNumber());
			itemBatchDtl.setStore(store);
			itemBatchDtl.setTaskId(taskId);
			itemBatchDtl.setVoucherDate(stockTransferOutHdr.getVoucherDate());
			itemBatchDtl.setVoucherNumber(voucherNo.getCode());
			itemBatchDtl.setCompanyMst(stockTransferOutHdr.getCompanyMst());

//			itemBatchDtl = itemBatchDtlRepo.saveAndFlush(itemBatchDtl);
			itemBatchDtl =saveAndPublishService.saveItemBatchDtl(itemBatchDtl, itemBatchDtl.getBranchCode());
			
			if(null!=itemBatchDtl)
			{
				saveAndPublishService.publishObjectToKafkaEvent(itemBatchDtl,
						itemBatchDtl.getBranchCode(), KafkaMapleEventType.ITEMBATCHDTL, 
						KafkaMapleEventType.SERVER,itemBatchDtl.getVoucherNumber());
			}
			else
			{
				return ;
			}
			
			
			
//			itemBatchDtlService.itemBatchDtlInsertionNew(stockTransferOutDtl.getBarcode(),stockTransferOutDtl.getBatch(),stockTransferOutHdr.getFromBranch(),
//					ExpiryDate,stockTransferOutDtl.getItemId(),stockTransferOutDtl.getMrp(),particular,processInstanceId,
//					QtyIn,stockTransferOutDtl.getQty(),stockTransferOutDtl.getId(),stockTransferOutHdr.getId(),stockTransferOutHdr.getVoucherDate(),stockTransferOutHdr.getVoucherNumber(),store,taskId,
//					stockTransferOutHdr.getVoucherDate(),voucherNo.getCode(),stockTransferOutHdr.getCompanyMst());
			
			//================= end ======================== 25-06-2021 ===========
			
			
			
			
			/*
			 * inventory Accounting
			 * 
			 */
			AccountClass  accountClass =new AccountClass();
			
			accountClass.setCompanyMst(stockTransferOutHdr.getCompanyMst());
			
			accountClass.setTotalCredit(new BigDecimal(0.0));
			accountClass.setTotalDebit(new BigDecimal(0.0));
			
		
			//accountClassRepository.save(accountClass);
			accountClass=saveAndPublishService.saveAccountClass(accountClass, mybranch);
			if (null!=accountClass)
			{
				saveAndPublishService.publishObjectToKafkaEvent(accountClass,
						mybranch, KafkaMapleEventType.ACCOUNTCLASS,
						KafkaMapleEventType.SERVER, accountClass.getId());
			}
			else
			{
				return ;
			}
			
			
			
			inventoryledgerMstService.stockTransferOutInventoryAccounting(stockTransferOutHdr.getVoucherNumber(), 
					stockTransferOutHdr.getVoucherDate(), stockTransferOutHdr.getId(),stockTransferOutHdr.getCompanyMst(),accountClass);

			

			
			

//			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
//			final VoucherNumber voucherNo = voucherService.generateInvoice("STV", comapnyMst.getId());
//
//			itemBatchDtl.setBarcode(stockTransferOutDtl.getBarcode());
//			itemBatchDtl.setBatch(stockTransferOutDtl.getBatch());
//			itemBatchDtl.setItemId(stockTransferOutDtl.getItemId());
//			itemBatchDtl.setMrp(stockTransferOutDtl.getMrp());
//			itemBatchDtl.setQtyIn(0.0);
//			itemBatchDtl.setQtyOut(stockTransferOutDtl.getQty());
//			itemBatchDtl.setVoucherNumber(voucherNo.getCode());
//			itemBatchDtl.setVoucherDate(stockTransferOutHdr.getVoucherDate());
//			itemBatchDtl.setItemId(stockTransferOutDtl.getItemId());
//
//			itemBatchDtl.setSourceVoucherNumber(stockTransferOutHdr.getVoucherNumber());
//			itemBatchDtl.setSourceVoucherDate(stockTransferOutHdr.getVoucherDate());
//
//			itemBatchDtl.setParticulars("STOCK TRANSFER TO " + stockTransferOutHdr.getToBranch());
//			itemBatchDtl.setBranchCode(stockTransferOutHdr.getFromBranch());
//			itemBatchDtl.setCompanyMst(compantMst);
//			itemBatchDtl.setSourceParentId(stockTransferOutHdr.getId());
//			itemBatchDtl.setSourceDtlId(stockTransferOutDtl.getId());
//			itemBatchDtl.setStore("MAIN");
//			itemBatchDtl = itemBatchDtlRepository.saveAndFlush(itemBatchDtl);
//
//			Map<String, Object> variables = new HashMap<String, Object>();
//
//			variables.put("voucherNumber", itemBatchDtl.getId());
//			variables.put("id", itemBatchDtl.getId());
//			variables.put("voucherDate", SystemSetting.getSystemDate());
//			variables.put("inet", 0);
//			variables.put("REST", 1);
//			variables.put("branchcode", itemBatchDtl.getBranchCode());
//			variables.put("companyid", itemBatchDtl.getCompanyMst());
//
//			variables.put("WF", "forwardItemBatchDtl");
//
//			LmsQueueMst lmsQueueMst = new LmsQueueMst();
//
//			String workflow = (String) variables.get("WF");
//			String sourceID = itemBatchDtl.getId();
//
//			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//			// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
//
//			java.util.Date uDate = (Date) variables.get("voucherDate");
//			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
//			lmsQueueMst.setVoucherDate(sqlVDate);
//
//			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
//			lmsQueueMst.setVoucherType(workflow);
//			lmsQueueMst.setPostedToServer("NO");
//			lmsQueueMst.setJobClass("forwardItemBatchDtl");
//			lmsQueueMst.setCronJob(true);
//			lmsQueueMst.setJobName(workflow + sourceID);
//			lmsQueueMst.setJobGroup(workflow);
//			lmsQueueMst.setRepeatTime(60000L);
//			lmsQueueMst.setSourceObjectId(sourceID);
//
//			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));
//
//			lmsQueueMst = lmsQueueMstRepository.save(lmsQueueMst);
//			variables.put("lmsqid", lmsQueueMst.getId());
//
//			eventBus.post(variables);

		}

	}

//	@GetMapping("{companymstid}/stocktransferouthdrbyvouchernumber/{vouchernumber}/stocktransferouthdr")
//	public List<StockTransferOutHdr> getintentHdrByVoucherNumber(
//			@PathVariable(value = "vouchernumber") String vouchernumber, @RequestParam("date") String date) {
//		java.util.Date vdate = SystemSetting.StringToUtilDate(date, "dd-MM-yyyy");
//		return stockTransferOutHdrRepository.findByVoucherNumberAndVoucherDate(vouchernumber, vdate);
//
//	}

	@GetMapping("{companymstid}/stocktransferouthdrbyvouchernumber/{vouchernumber}/stocktransferouthdr")
	public StockTransferOutHdr getStockTransferOutHdr(@PathVariable(value = "vouchernumber") String vouchernumber,
			@RequestParam("date") String date) {
		Date vdate = SystemSetting.StringToSqlDate(date, "yyyy-MM-dd");
		return stockTransferOutHdrRepository.findByVoucherNumberAndVoucherDate(vouchernumber, vdate).get(0);

	}

	@GetMapping("{companymstid}/stocktransferouthdr/stocktransferouthdrbetweendate/{branchcode}")
	public List<StockTransferOutHdr> getStockTransferBetweenDate(@RequestParam(value = "fdate") String fdate,
			@RequestParam(value = "tdate") String tdate, @PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "companymstid") String companymstid) {
		java.sql.Date vfdate = SystemSetting.StringToSqlDate(fdate, "yyyy-MM-dd");
		java.sql.Date vtdate = SystemSetting.StringToSqlDate(tdate, "yyyy-MM-dd");

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		return stockTransferOutHdrRepository.getStockTransferHdrBetweenDate(vfdate, vtdate, companyMst.get().getId(),
				branchcode);

	}

	// forweb
	@GetMapping("{companymstid}/stocktransferouthdr/{tobranch}/getvouchernobytobranchanddate")
	public List<StockTransferOutHdr> getVoucherNoByFromBranchAndDate(@PathVariable(value = "tobranch") String tobranch,
			@RequestParam("date") String date) {
		java.sql.Date vdate = SystemSetting.StringToSqlDate(date, "yyyy-MM-dd");
		return stockTransferOutHdrRepository.getVoucherNoByToBranchAndDate(tobranch, vdate);

	}

// for web
	@GetMapping("{companymstid}/stocktransferouthdr/{frombranch}/getvouchernobyfrombranchanddate")
	public List<StockTransferOutHdr> getVoucherNoByToBranchAndDate(
			@PathVariable(value = "frombranch") String frombranch, @RequestParam("date") String date) {
		java.sql.Date vdate = SystemSetting.StringToSqlDate(date, "yyyy-MM-dd");
		return stockTransferOutHdrRepository.getVoucherNoByFromBranchAndDate(frombranch, vdate);

	}

	// -------------------new version 1.4 surya

	@GetMapping("{companymstid}/stocktransferouthdr/holdedstocktransferouthdrs")
	public List<StockTransferOutHdr> getHoldedStockTransferOutHdr(
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		return stockTransferOutHdrRepository.getHoldedStockTransferOutHdr(companyOpt.get());

	}

	@GetMapping("{companymstid}/stocktransferouthdr/stocktransferouthdrbyid/{id}")
	public StockTransferOutHdr getStockTransferOutHdrById(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		return stockTransferOutHdrRepository.findByCompanyMstAndId(companyOpt.get(), id);

	}

	// -------------------new version 1.4 surya end

}
