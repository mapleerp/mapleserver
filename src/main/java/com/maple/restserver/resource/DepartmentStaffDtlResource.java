package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.DepartmentStaffDtl;
import com.maple.restserver.repository.DepartmentStaffDtlRepository;




@RestController
@Transactional
public class DepartmentStaffDtlResource {
	@Autowired
	private DepartmentStaffDtlRepository depstaff;
	@GetMapping("{companymstid}/departmentstaffdtl")
	public List<DepartmentStaffDtl> retrieveAlldepartments()
	{
		return depstaff.findAll();
		
	}
	@PostMapping("{companymstid}/departmentstaffdtl")
	public ResponseEntity<Object>createUser(@Valid @RequestBody DepartmentStaffDtl departmentstaff)
	{
		DepartmentStaffDtl saved=depstaff.saveAndFlush(departmentstaff);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	
	}

}
