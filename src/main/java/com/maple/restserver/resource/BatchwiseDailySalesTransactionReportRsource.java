package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.BatchwiseSalesTransactionReport;
import com.maple.restserver.service.BatchwiseDailySalesTransactionReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class BatchwiseDailySalesTransactionReportRsource {

	@Autowired
	BatchwiseDailySalesTransactionReportService batchwiseDailySalesTransactionReportService;

	@GetMapping("/{companymstid}/batchwisedailysalestransactionreport/{branchcode}")
	public List<BatchwiseSalesTransactionReport> batchwiseDailySalesTransactionReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,	@PathVariable(value = "groupList") List<String> groupList,  
			@RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
	java.util.Date TDate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		return batchwiseDailySalesTransactionReportService.batchwiseSalesTransactionReport(companymstid,branchcode,fDate,TDate,groupList);
	}
	
	
}
