package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.SalesAnalysis;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.SalesAnalysisRepository;
import com.maple.restserver.utils.EventBusFactory;

@RestController
@Transactional
public class SalesAnalysisResource {

	@Autowired
	private SalesAnalysisRepository salesAnalysisRepository;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	CompanyMstRepository companyMstRepo;

	@GetMapping("{companymstid}/salessnalysiss")
	public List<SalesAnalysis> retrieveAllSalesAnalysis(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return salesAnalysisRepository.findByCompanyMst(companyOpt.get());
	}

	@PostMapping("{companymstid}/salesanalysis")
	public SalesAnalysis createSalesAnalysis(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SalesAnalysis salesAnalysis) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			salesAnalysis.setCompanyMst(companyMst);

			SalesAnalysis salesAnalysisSaved = salesAnalysisRepository.saveAndFlush(salesAnalysis);

			Map<String, Object> variables = new HashMap<String, Object>();
			variables.put("voucherNumber", salesAnalysisSaved.getId());
			variables.put("voucherDate", ClientSystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", salesAnalysisSaved.getId());
			variables.put("branchcode", salesAnalysisSaved.getBranchCode());
			variables.put("companyid", salesAnalysisSaved.getCompanyMst());
			variables.put("REST", 1);

			variables.put("WF", "forwardSalesAnalysis");

			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");

			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

			// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			Object obj = variables.get("voucherDate");
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);

			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardSalesAnalysis");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);

			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);

			return salesAnalysisSaved;
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));

	}

}
