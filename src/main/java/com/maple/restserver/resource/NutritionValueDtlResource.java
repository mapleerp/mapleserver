package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.NutritionValueDtl;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.NutrintionValueDtlRepository;

@RestController
@Transactional
public class NutritionValueDtlResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	NutrintionValueDtlRepository nutrintionValueDtlRepo;
	
	@PostMapping("{companymstid}/nutritionvaluedtl")
	public NutritionValueDtl createNutritionValueDtl(@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody  NutritionValueDtl nutritionValueDtl)
	{
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
			nutritionValueDtl.setCompanyMst(companyMst.get());
			return nutrintionValueDtlRepo.saveAndFlush(nutritionValueDtl);

	}
	@PutMapping("{companymstid}/nutritionvaluedtl/{dtlid}/updatenutritionvaluedtl")
	public NutritionValueDtl updateNutritionvaluedtl(
			@PathVariable(value="dtlid") String dtlid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody NutritionValueDtl  nutritionValueDtlRequest)
	{
			
		NutritionValueDtl nutritionValueDtl = nutrintionValueDtlRepo.findById(dtlid).get();
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		nutritionValueDtl.setCompanyMst(companyMst);
		nutritionValueDtl.setNutrition(nutritionValueDtlRequest.getNutrition());
		nutritionValueDtl.setPercentage(nutritionValueDtlRequest.getPercentage());
		nutritionValueDtl.setSerial(nutritionValueDtlRequest.getSerial());
		nutritionValueDtl.setValue(nutritionValueDtlRequest.getValue());
		nutritionValueDtl.setNutritionValueMst(nutritionValueDtlRequest.getNutritionValueMst());
		nutritionValueDtl = nutrintionValueDtlRepo.save(nutritionValueDtl);
		
        return nutritionValueDtl;

	}
	@GetMapping("{companymstid}/nutrinvaluedtl/{mstid}/getnutritionvaluedtl")
	public List<NutritionValueDtl> retrieveNutritionValueDtlByItemMst(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "mstid") String mstid) {
		return nutrintionValueDtlRepo.findByCompanyMstIdAndNutritionValueMstId(companymstid,mstid );

	}
	@GetMapping("{companymstid}/nutrinvaluedtl/{mstid}/{nutrition}/getnutritionvaluedtl")
	public NutritionValueDtl retrieveNutritionValueDtlByItemName(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "nutrition") String nutrition,
			@PathVariable(value = "mstid") String mstid) {
		return nutrintionValueDtlRepo.findByCompanyMstIdAndNutritionValueMstIdAndNutrition(companymstid,mstid,nutrition );

	}
	@DeleteMapping("{companymstid}/nutritionvaluedtl/{id}/deletenutritionvaluedtl")
	public void NutritionValueDelete(@PathVariable(value = "id") String Id) {
		nutrintionValueDtlRepo.deleteById(Id);

	}
}
