
package com.maple.restserver.resource;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.ProfitLoss;
import com.maple.restserver.repository.ProfitLossRepository;

@RestController
@Transactional
public class ProfitLossResource {
	@Autowired
	private ProfitLossRepository profitlossdtl;
	@GetMapping("{companymstid}/profitloss")
	public List<ProfitLoss> retrieveAllcash()
	{
		return profitlossdtl.findAll();
	}
	@PostMapping("{companymstid}/profitloss")
	public ResponseEntity<Object> createUser(@Valid @RequestBody ProfitLoss ledger)
	{
		ProfitLoss saved=profitlossdtl.saveAndFlush(ledger);
	URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	}

}

