package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JobCardHdr;
import com.maple.restserver.entity.ServiceInHdr;
import com.maple.restserver.entity.ServiceItemMst;
import com.maple.restserver.report.entity.StockValueReports;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ServiceItemMstRepository;
import com.maple.restserver.service.ServiceInService;

@RestController
public class ServiceItemMstResource {
	
	Pageable topFifty =   PageRequest.of(0, 50);

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	ServiceItemMstRepository serviceItemMstRepo;
	
	@Autowired
	ServiceInService serviceInService;
	
	
	@PostMapping("{companymstid}/serviceitemmst")
	public ServiceItemMst createServiceItemMst(@PathVariable(value = "companymstid") String companymstid,@Valid @RequestBody 
			ServiceItemMst serviceItemMst)
	{
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			serviceItemMst.setCompanyMst(companyMst);
			ServiceItemMst serviceItemMstSaved = serviceItemMstRepo.saveAndFlush(serviceItemMst);
		
			return serviceItemMstSaved;
	}
	@PutMapping("{companymstid}/serviceitemmst/{serviceitemid}/updateserviceitemmst")
	public ServiceItemMst updateServiceItemMst(
			@PathVariable(value="serviceitemid") String serviceitemid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody ServiceItemMst  serviceItemMstRequest)
	{
			
		ServiceItemMst serviceItemMst = serviceItemMstRepo.findById(serviceitemid).get();
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		serviceItemMst.setCompanyMst(companyMst);
		serviceItemMst.setItemName(serviceItemMstRequest.getItemName());
		serviceItemMst.setStandardPrice(serviceItemMstRequest.getStandardPrice());
		serviceItemMst.setTaxRate(serviceItemMstRequest.getTaxRate());
		serviceItemMst.setCess(serviceItemMstRequest.getCess());
		serviceItemMst.setCategoryId(serviceItemMstRequest.getCategoryId());
		serviceItemMst.setIsDeleted(serviceItemMstRequest.getIsDeleted());
		serviceItemMst = serviceItemMstRepo.saveAndFlush(serviceItemMst);
		return serviceItemMst;
	}
	
	@GetMapping("{companymstid}/serviceitemmst/getserviceitemmstbyid/{hdrid}")
	public ServiceItemMst getServiceItemMstById(@PathVariable ("hdrid") String hdrid,
			@PathVariable("companymstid") String companymstid)
	
	{
		
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
		return serviceItemMstRepo.findByCompanyMstAndId(companyMst, hdrid);
	}
		
	
	@DeleteMapping("{companymstid}/serviceitemmst/deletebyid/{hdrid}")
	public void serviceItemMstDelete(@PathVariable String hdrid) {
		serviceItemMstRepo.deleteById(hdrid);

	}
	@GetMapping("{companymstid}/serviceitemmstsold")
	public List<ServiceItemMst> getServiceItemMstByCompany(
			@PathVariable("companymstid") String companymstid)
	
	{
		
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
		return serviceItemMstRepo.findByCompanyMst(companyMst);
	}
	
	@GetMapping("{companymstid}/serchforserviceitem")
	public  @ResponseBody List<Object> searchServiceItemWithCom(
			@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid){
		
		 
		Optional<CompanyMst> companyMst1 = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMst1.get();
		searchstring = searchstring.replaceAll("20%", " ");
		
		return serviceItemMstRepo.searchByLocationWithCompany("%"+searchstring.toLowerCase()+"%", topFifty,companyMst) ; 
		
		
	}
	
	@GetMapping("{companymstid}/serviceitemmstbyname")
	public ServiceItemMst findByServiceItemNameWithCom(
			@RequestParam("data") String itemname,
			@PathVariable("companymstid") String companymstid){
		
		 
		Optional<CompanyMst> companyMst1 = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMst1.get();
		itemname = itemname.replaceAll("20%", " ");
		
		return serviceItemMstRepo.findByCompanyMstAndItemName(companyMst,itemname) ; 
		
		
	}
	
	@GetMapping("{companymstid}/serviceitemmst/serviceoritemprofit/{type}")
	public List<StockValueReports> findByServiceOrItemProfit(
			@RequestParam("fdate") String fdate,
			@RequestParam("tdate") String tdate,
			@PathVariable("companymstid") String companymstid,
			@PathVariable("type") String type){
		
		Date sdate = ClientSystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		Date edate = ClientSystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");

		
		Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		
		return serviceInService.findByServiceOrItemProfit(companyMst,sdate,edate,type) ; 
		
		
	}
}
