package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.ChequePrintMst;
import com.maple.restserver.entity.ConsumptionHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.ChequePrintMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
public class ChequePrintMstResource {
	
	@Autowired
	ChequePrintMstRepository chequePrintMstRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;

	@PostMapping("{companymstid}/chequeprintmst/savechequeprintmst")
	public ChequePrintMst createChequePrintMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody ChequePrintMst chequePrintMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			chequePrintMst.setCompanyMst(companyMst);
			return chequePrintMstRepo.save(chequePrintMst);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}
	
	@DeleteMapping("{companymstid}/chequeprintmst/deletechequeprintmstbyid/{id}")
	public void deleteChequePrintMst(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id)
	{
		chequePrintMstRepo.deleteById(id);
	}
	
	@GetMapping("{companymstid}/chequeprintmst/showallchequeprintbybankname/{accid}")
	public List<ChequePrintMst> showAllChequePrintByBankName(@PathVariable(value = "accid") String accid)
	{
		return chequePrintMstRepo.findByAccountId(accid);
	}
	

	@GetMapping("{companymstid}/chequeprintmst/showallchequeprintmst")
	public List<ChequePrintMst> showAllChequePrintMst()
	{
		return chequePrintMstRepo.findAll();
	}
}
