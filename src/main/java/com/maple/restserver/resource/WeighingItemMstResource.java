package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.RawMaterialIssueHdr;
import com.maple.restserver.entity.SalesReturnHdr;
import com.maple.restserver.entity.WeighingItemMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.WeighingItemMstRepository;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class WeighingItemMstResource {

	@Autowired
	WeighingItemMstRepository weighingItemMstRepo;
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@PostMapping("{companymstid}/weighingitemmst")
	public WeighingItemMst createWeighingItemMst(@Valid
			@RequestBody WeighingItemMst weighingItemMst,
			@PathVariable (value = "companymstid") String companymstid)
	{
		
		
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			weighingItemMst.setCompanyMst(companyMst);
			 
			

			WeighingItemMst saved=weighingItemMstRepo.saveAndFlush(weighingItemMst);
			Map<String,Object> variables = new HashMap<String, Object>();
			variables.put("voucherNumber", saved.getId());
			variables.put("voucherDate",SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", saved.getId());		 
			variables.put("companyid", saved.getCompanyMst());
			variables.put("branchcode", saved.getBranchCode());

				variables.put("REST",1);
			 
				variables.put("WF", "forwardWeighingItemMst");
				
				String workflow = (String) variables.get("WF");
				String voucherNumber = (String) variables.get("voucherNumber");
				String sourceID = (String) variables.get("id");

				LmsQueueMst lmsQueueMst = new LmsQueueMst();

				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
				
				//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
				
				java.util.Date uDate = (Date) variables.get("voucherDate");
				java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
				lmsQueueMst.setVoucherDate(sqlVDate);
				
				
				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
				lmsQueueMst.setVoucherType(workflow);
				lmsQueueMst.setPostedToServer("NO");
				lmsQueueMst.setJobClass("forwardWeighingItemMst");
				lmsQueueMst.setCronJob(true);
				lmsQueueMst.setJobName(workflow + sourceID);
				lmsQueueMst.setJobGroup(workflow);
				lmsQueueMst.setRepeatTime(60000L);
				lmsQueueMst.setSourceObjectId(sourceID);
				
				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
				variables.put("lmsqid", lmsQueueMst.getId());
				eventBus.post(variables);
	 	return saved;
	}
	@PutMapping("{companymstid}/weighingitemmst/{itemid}/updateitem")
	public WeighingItemMst updateItem(
			@PathVariable(value="itemid") String itemid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody WeighingItemMst  weighingItemMstRequest)
	{
			
		WeighingItemMst weighingItemMst = weighingItemMstRepo.findByCompanyMstIdAndItemId(companymstid, itemid);
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		weighingItemMst.setCompanyMst(companyMst);
		weighingItemMst.setBarcode(weighingItemMstRequest.getBarcode());
		weighingItemMst.setId(weighingItemMstRequest.getId());
		weighingItemMst.setItemId(itemid);
		return weighingItemMst;
	}
	@GetMapping("{companymstid}/weighingitemmst/{barcode}/weighingitembybarcode/{branchcode}")
	public WeighingItemMst getSalesReturnHdrById(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable("barcode") String barcode,
			  @PathVariable("branchcode") String branchcode)
	{
		return weighingItemMstRepo.findByCompanyMstIdAndBarcode(companymstid, barcode);
		
	}
	@GetMapping("{companymstid}/weighingitemmst/{itemcode}/weighingitembyitemid")
	public WeighingItemMst getItemByItemId(@PathVariable(value = "companymstid") String
			  companymstid,
			  @PathVariable("itemcode") String itemcode )
	{
		return weighingItemMstRepo.findByCompanyMstIdAndItemId(companymstid, itemcode);
		
	}
}
