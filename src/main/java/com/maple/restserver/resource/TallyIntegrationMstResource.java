package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JournalDtl;
import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.ReceiptDtl;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.TallyIntegrationMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.JournalHdrAndDtlMessage;
import com.maple.restserver.message.entity.PaymentHdrAndDtlMessage;
import com.maple.restserver.message.entity.ReceiptHdrAndDtlMessage;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.JournalDtlRepository;
import com.maple.restserver.repository.JournalHdrRepository;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.ReceiptDtlRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.TallyIntegrationMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.SaveAndPublishServiceImpl;
import com.maple.restserver.utils.SystemSetting;
@RestController
@Transactional

public class TallyIntegrationMstResource {
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	
	@Autowired
	private TallyIntegrationMstRepository tallyIntegrationMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	 SalesTransHdrRepository salesTransHdrRepository;
	
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;
	
	@Autowired
	PaymentHdrRepository paymentHdrRepository;
	
	@Autowired
	PaymentDtlRepository paymentDtlRepo;
	
	@Autowired
	SaveAndPublishServiceImpl saveAndPublishServiceImpl;
	
	@Autowired
	 JournalHdrRepository journalHdrRepository;
	
	@Autowired
	JournalDtlRepository journalDtlRepository;
	
	@Autowired
	private ReceiptHdrRepository receiptHdrrepo;
	
	
	@Autowired
	private ReceiptDtlRepository receiptDtlRepo;
	
	
	@GetMapping("{companymstid}/tallyintegrationmst/gettallyintegrationmst")
	public List<TallyIntegrationMst> retrieveAllTallyIntegrationMst(
			@PathVariable(value = "companymstid") String
			  companymstid){
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return tallyIntegrationMstRepository.findByCompanyMst(companyOpt.get());
	}
	
	
	
	
	@PostMapping("{companymstid}/tallyintegrationmst/savetallyintegrationmst")
	public TallyIntegrationMst createTallyIntegrationMst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  TallyIntegrationMst tallyIntegrationMst)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			tallyIntegrationMst.setCompanyMst(companyMst);
		
		
		return tallyIntegrationMstRepository.saveAndFlush(tallyIntegrationMst);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));
		
		
		
	
	}
	
	
	@DeleteMapping("{companymstid}/tallyintegrationmst/deletetallyintegrationmst")
	public void DeletepurchaseDtl(
			@PathVariable (value="companymstid") String companymstid) {
		tallyIntegrationMstRepository.deleteAll();

	}
	
	
	
	
	
	@Transactional
	@GetMapping("{companymstid}/tallyintegrationmst/getsales")
	public  void retrieveAllSales(
			@PathVariable(value = "companymstid") String companymstid ,
			@RequestParam("fromDate") String  fromDate,
			@RequestParam("toDate") String  toDate
			)
	{
		
		Date fDate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
		Date tDate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
		 
		List<SalesTransHdr> result = salesTransHdrRepository.salesSummaryReport(fDate,tDate);
		
		for (SalesTransHdr salestranshdr : result) {
			
			
			saveAndPublishService.salesFinalSaveToKafkaEvent(salestranshdr, 
					salestranshdr.getBranchCode(), KafkaMapleEventType.SERVER);
				 
			
		}
		
	}

	@Transactional
	@GetMapping("{companymstid}/tallyintegrationmst/getpurchase")
	public  void retrieveAllPurchase(
			@PathVariable(value = "companymstid") String companymstid ,
			@RequestParam("fromDate") String  fromDate,
			@RequestParam("toDate") String  toDate
			)
	{
		
		Date fDate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
		Date tDate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
		 
         List<PurchaseHdr> result = purchaseHdrRepository.getPurchaseHdrBetweenDate(fDate,tDate);
		
		for (PurchaseHdr purchaseHdr : result) {
			 saveAndPublishService.purchaseFinalSaveToKafkaEvent(purchaseHdr, mybranch, KafkaMapleEventType.SERVER);
			
		}
		
	}
	
	@Transactional
	@GetMapping("{companymstid}/tallyintegrationmst/getpayments")
	public  void retrieveAllPayments(
			@PathVariable(value = "companymstid") String companymstid ,
			@RequestParam("fromDate") String  fromDate,
			@RequestParam("toDate") String  toDate
			)
	{
		
		Date fDate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
		Date tDate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
		 
		List<PaymentHdr> result=paymentHdrRepository.getPaymentHdrBetweenDate(fDate, tDate);
		
		for(PaymentHdr paymentHdr :result) {
			
			List<PaymentDtl> paymentDtlList = paymentDtlRepo.findBypaymenthdrId(paymentHdr.getId());
			
			PaymentHdrAndDtlMessage paymentHdrAndDtlMessage = new PaymentHdrAndDtlMessage();
			paymentHdrAndDtlMessage.setPaymentHdr(paymentHdr);
			paymentHdrAndDtlMessage.getPaymentDtlList().addAll(paymentDtlList);
			
			saveAndPublishServiceImpl.publishObjectToKafkaEvent(paymentHdrAndDtlMessage, 
					paymentHdr.getBranchCode(),
						KafkaMapleEventType.PAYMENTHDRANDDTL, 
						KafkaMapleEventType.SERVER,paymentHdr.getId());	
		}
		
		
	}
	
	@Transactional
	@GetMapping("{companymstid}/tallyintegrationmst/getjournal")
	public  void retrieveAllJournal(
			@PathVariable(value = "companymstid") String companymstid ,
			@RequestParam("fromDate") String  fromDate,
			@RequestParam("toDate") String  toDate
			)
	{
		
		Date fDate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
		Date tDate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
		 
		
		List<JournalHdr> result=journalHdrRepository.getJournalHdrBetweenDate(fDate, tDate);
		
		for(JournalHdr journalHdr :result) {
		
		List<JournalDtl> journalDtlList = journalDtlRepository.findByJournalHdr(journalHdr);
		
		JournalHdrAndDtlMessage journalHdrAndDtlMessage = new JournalHdrAndDtlMessage();
		journalHdrAndDtlMessage.setJournalHdr(journalHdr);
		journalHdrAndDtlMessage.getJournalDtlList().addAll(journalDtlList);
		
	
		saveAndPublishServiceImpl.publishObjectToKafkaEvent(journalHdrAndDtlMessage, 
				journalHdr.getBranchCode(),
					KafkaMapleEventType.JOURNALHDRANDDTL, 
					KafkaMapleEventType.SERVER,journalHdr.getId());
		}
		
	}
	
	@Transactional
	@GetMapping("{companymstid}/tallyintegrationmst/getreceipts")
	public  void retrieveAllReceipt(
			@PathVariable(value = "companymstid") String companymstid ,
			@RequestParam("fromDate") String  fromDate,
			@RequestParam("toDate") String  toDate
			)
	{
		
		Date fDate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
		Date tDate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
		
		List<ReceiptHdr> result=receiptHdrrepo.getReceiptHdrBetweenDate(fDate, tDate);
		
		for(ReceiptHdr receiptHdr :result) {
			
		List<ReceiptDtl> receiptDtlList = receiptDtlRepo.findByReceiptHdr(receiptHdr);
		ReceiptHdrAndDtlMessage ReceiptHdrAndDtlMessage = new ReceiptHdrAndDtlMessage();
		ReceiptHdrAndDtlMessage.setReceiptHdr(receiptHdr);
		ReceiptHdrAndDtlMessage.getReceiptDtlList().addAll(receiptDtlList);
		
		saveAndPublishServiceImpl.publishObjectToKafkaEvent(ReceiptHdrAndDtlMessage, 
				receiptHdr.getBranchCode(),
					KafkaMapleEventType.RECEIPTHDRANDDTL, 
					KafkaMapleEventType.SERVER,receiptHdr.getId());
		
		
	}
	
	}
	
	
}
