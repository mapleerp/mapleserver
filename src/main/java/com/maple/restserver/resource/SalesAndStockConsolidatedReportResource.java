package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.SalesAndStockConsolidatedReport;
import com.maple.restserver.service.SalesAndStockConsolidatedReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class SalesAndStockConsolidatedReportResource {
	
	@Autowired
	SalesAndStockConsolidatedReportService salesAndStockConsolidatedReportService;
	
	
	@GetMapping("{companymstid}/salesandstockconsolidatedreportresource/getsalesandstockconsolidatedreport/{frombranch}")
	public List<SalesAndStockConsolidatedReport> getSalesAndStockConsolidatedReport(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "frombranch")String frombranch,
			@RequestParam("fromdate")String fromdate,
			@RequestParam("todate")String todate,
			@RequestParam("itemlist")String itemlist
			)
	{
		String []itemArray = itemlist.split(";");
		java.util.Date fudate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date tudate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		return salesAndStockConsolidatedReportService.getSalesAndStockConsolidatedReport(fudate,tudate,frombranch,companymstid,itemArray);
	}

}
