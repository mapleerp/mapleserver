package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.TestService;

@RestController
public class TestResource {

	@Autowired
	TestService testService;
	@Autowired
	CompanyMstRepository cmpanyMstRepository;
	
	@Autowired
	SalesTransHdrRepository salesTransHdrRepo;

@Autowired
ItemMstRepository itemMstRepository;

@Autowired
BranchMstRepository branchMstRepository;
@Autowired
SalesDetailsRepository salesDetailsRepository;
@Autowired
VoucherNumberService voucherNumberService;

@Autowired
PurchaseHdrRepository purchaseHdrRepository;

@Autowired
PurchaseDtlRepository purchaseDtlRepository;
@Autowired
AccountHeadsRepository accountHeadsRepository;

	@GetMapping("{companymstid}/testresource/testsales/{noofsalesbill}")
	public String  createSalesBill(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "noofsalesbill") int noofsalesbill
			) {
		Optional<CompanyMst> companyMstOpt= cmpanyMstRepository.findById(companymstid);
			for(int z=1; z<=noofsalesbill; z++) {
		Double invoiceAmount=0.0;
		SalesTransHdr salesTransHdr =new SalesTransHdr();
			salesTransHdr.setCompanyMst(companyMstOpt.get());
			BranchMst  branch =branchMstRepository.getMyBranch();
			salesTransHdr.setBranchCode(branch.getBranchCode());
			salesTransHdr.setCardamount(0.0);
			salesTransHdr.setUserId("");
			Random rand=new Random();
			int customerCount=accountHeadsRepository.accountHeadsCount();
			int n=rand.nextInt(customerCount);
			AccountHeads customterMst=accountHeadsRepository.randomAccountHeads(n);
			n+=1;
			
			salesTransHdr.setCustomerId(customterMst.getId());
			salesTransHdr.setAccountHeads(customterMst);
			salesTransHdr.setCardamount(5.0);
			salesTransHdr.setCreditAmount(25.5);
			salesTransHdr.setCardNo("");
			salesTransHdr.setCardType("");
			salesTransHdr.setInvoiceAmount(50.25);
			salesTransHdr.setCreditOrCash("");
			salesTransHdr.setSalesManId("");
			salesTransHdr=	salesTransHdrRepo.saveAndFlush(salesTransHdr);
			
			for(int j=1; j<=10; j++) {

			Random randItem=new Random();
			int itemCount=itemMstRepository.itemCount();
			int k=randItem.nextInt(itemCount);
			ItemMst itemMst=itemMstRepository.fetchRandomItem(k);
			k+=1;
		    SalesDtl salesDtl=new SalesDtl();
			salesDtl.setItemId(itemMst.getId());
			salesDtl.setBatch(MapleConstants.Nobatch);
			salesDtl.setAddCessRate(itemMst.getCess());
			salesDtl.setCompanyMst(companyMstOpt.get());
			salesDtl.setBarcode(itemMst.getBarCode());
			Random randQty=new Random();
			int q=randQty.nextInt(itemCount);
			q+=1;
			salesDtl.setQty(2.0);
			salesDtl.setRate(itemMst.getStandardPrice());
			salesDtl.setAmount(salesDtl.getQty()*salesDtl.getRate());
			
			invoiceAmount+=salesDtl.getAmount();
			salesDtl.setUnitId("");
			salesDtl.setCessRate(itemMst.getCess());
			salesDtl.setAddCessAmount(0.0);
			salesDtl.setMrp(itemMst.getStandardPrice());
			salesDtl.setCessAmount(0.0);
			salesDtl.setCgstAmount(0.0);
			salesDtl.setCgstTaxRate(0.0);
			salesDtl.setDiscount(0.0);
			salesDtl.setIgstAmount(0.0);
			salesDtl.setIgstTaxRate(0.0);
			salesDtl.setSgstAmount(0.0);
			salesDtl.setSgstTaxRate(0.0);
			salesDtl.setTaxRate(0.0);
				/*
				 * String salesTransHdrId=salesTransHdrRepo.testInputProgressWholesalebill();
				 * Optional<SalesTransHdr>
				 * salesTransOpt=salesTransHdrRepo.findById(salesTransHdrId);
				 */
			salesDtl.setSalesTransHdr(salesTransHdr);
			salesDetailsRepository.saveAndFlush(salesDtl);
			}
			// updating voucher number and amount in salestrans hdr
			String salesTransHdrId=salesTransHdrRepo.testInputProgressWholesalebill();
			Optional<SalesTransHdr> salesTransOpt=salesTransHdrRepo.findById(salesTransHdrId);
			SalesTransHdr salesTrans=salesTransOpt.get();
			String uCode="TEST";
			VoucherNumber voucherNumber=voucherNumberService.generateInvoice(uCode, companymstid);
			salesTrans.setVoucherNumber(voucherNumber.getCode());
			salesTrans.setInvoiceAmount(invoiceAmount);
			salesTrans.setVoucherDate(ClientSystemSetting.getSystemDate());
			salesTransHdrRepo.saveAndFlush(salesTrans);
			}
			return "sucessful";
	}
		
	
	@GetMapping("{companymstid}/testresource/testpurchase/{noofpurchasebill}")
	public String  createPurchase(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "noofpurchasebill") int noofpurchasebill
			) {
		  for(int p=1;p<=noofpurchasebill; p++) {
		   PurchaseHdr purchaseHdr=new PurchaseHdr();
		   Optional <CompanyMst> compannyMstOpt=cmpanyMstRepository.findById(companymstid);
		   purchaseHdr.setCompanyMst(compannyMstOpt.get());
		   BranchMst  branch =branchMstRepository.getMyBranch();
		   purchaseHdr.setBranchCode(branch.getBranchCode());
		   Random randSupplier=new Random();
		   int accountHeadsCount=accountHeadsRepository.accountHeadsCount();
		   int k=randSupplier.nextInt(accountHeadsCount);
		   AccountHeads accountHeads=accountHeadsRepository.randomAccountHeads(k);
		   System.out.print(accountHeads.getAccountName()+"supplier name issssssss");
		   k+=1;
		   purchaseHdr.setSupplierId(accountHeads.getId());
		   purchaseHdr=	purchaseHdrRepository.save(purchaseHdr);
			for(int s=1;s<=10;s++) {
			PurchaseDtl purchasedtl=new PurchaseDtl();
			int itemCount=itemMstRepository.itemCount();
			Random randItem=new Random();
			int x =randItem.nextInt(itemCount);
			ItemMst itemMst=testService.fetchRandomItem(x);
			System.out.print(itemMst.getItemName()+"item name isssssssssssssssssssssss");
			x+=1;
			  purchasedtl.setItemId(itemMst.getId());
			  purchasedtl.setBarcode(itemMst.getBarCode()); purchasedtl.setQty(2.0);
			  purchasedtl.setMrp(itemMst.getStandardPrice());
			  purchasedtl.setPurchaseHdr(purchaseHdr);
				/*
				 * String purchaseHdrId=purchaseHdrRepository.fetchPrograssingPurchaseHdrId();
				 * Optional<PurchaseHdr>pHdrOpt=purchaseHdrRepository.findById(purchaseHdrId);
				 * purchasedtl.setPurchaseHdr(pHdrOpt.get());
				 */
			purchaseDtlRepository.save(purchasedtl);
			}
			String purchaseHdrId=purchaseHdrRepository.fetchPrograssingPurchaseHdrId();
		    Optional<PurchaseHdr>pHdrOpt=purchaseHdrRepository.findById(purchaseHdrId);
			PurchaseHdr pHdr=pHdrOpt.get();
			pHdr.setVoucherDate(ClientSystemSetting.systemDate);
			String uCode="TEST";
			VoucherNumber purchaseInvoiceNumber=voucherNumberService.generateInvoice(uCode, companymstid);
			pHdr.setVoucherNumber(purchaseInvoiceNumber.getCode());
			purchaseHdrRepository.save(pHdr);
	//	}
		
		
	}

		  return "sucessful";
	
}
}
