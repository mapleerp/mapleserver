package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.KitDefenitionDtl;
import com.maple.restserver.entity.KitDefinitionMst;
import com.maple.restserver.entity.LocalCustomerMst;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.KitDefenitionDtlRepository;
import com.maple.restserver.repository.KitDefinitionMstRepository;
import com.maple.restserver.service.KitDefinitionImpl;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.kitdefinionservice;
import com.maple.restserver.utils.EventBusFactory;

@RestController
@Transactional
public class KitdefinitionDetailResource {
private static final Logger logger = LoggerFactory.getLogger(KitdefinitionDetailResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
    @Autowired
	private KitDefenitionDtlRepository kitdefnitionDetailRepo;
	@Autowired
	private KitDefinitionMstRepository kitdefinitionMstRepo;
	
	@Autowired
	kitdefinionservice kitdefinionservice;
	
	 @Autowired
	 SaveAndPublishService saveAndPublishService;

	//@Autowired
	//private RuntimeService runtimeService;
	EventBus eventBus = EventBusFactory.getEventBus();
	 
	@Autowired
	private ExternalApi externalApi;
	Pageable topFifty =   PageRequest.of(0, 50);
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@PostMapping("{companymstid}/kitdefinitionmsts/{kitdefinitionmstId}/kitdefinitondtl")
	public KitDefenitionDtl createKitDefenitionDtl(
			@PathVariable(value = "kitdefinitionmstId") String kitdefinitionmstId,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody KitDefenitionDtl kitdefinitionDtlRequest) {

		
		Optional<CompanyMst> companyMstOpt = 	companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		kitdefinitionDtlRequest.setCompanyMst(companyMst);

		
		
		
		return kitdefinitionMstRepo.findById(kitdefinitionmstId).map(kitDefenitionmst -> {
			kitdefinitionDtlRequest.setKitDefenitionmst(kitDefenitionmst);
			return kitdefnitionDetailRepo.save(kitdefinitionDtlRequest);
//			return saveAndPublishService.saveKitDefenitionDtl(kitdefinitionDtlRequest,mybranch);
		}).orElseThrow(
				() -> new ResourceNotFoundException("kitdefinitionmstId" + kitdefinitionmstId + " not found"));

		
		
	}
	
	
	
	@DeleteMapping("{companymstid}/kitdefinitiondtl/{kitdefinitiondtlId}")
	public void DeleteKitdefinitonDetail(@PathVariable(value="kitdefinitiondtlId") String kitdefinitiondetailId) {
		kitdefnitionDetailRepo.deleteById(kitdefinitiondetailId);

	}

	@GetMapping("{companymstid}/kitdefinitionmst/{kitdefinitionMstId}/kitdefinitiondtl")
	public List<KitDefenitionDtl> retrieveAllIntentDtlByIntentHdrID(
			@PathVariable(value = "kitdefinitionMstId") String kitdefinitionMstId) {
 

		List <KitDefenitionDtl> a = kitdefnitionDetailRepo.findBykitDefenitionmstId(kitdefinitionMstId);
		
		return  a;

	}
	
	@GetMapping("{companymstid}/kitdefinitiondtlbyid/{kitdefinitiondtl}")
	public KitDefenitionDtl retrieveKItDefById(
			@PathVariable(value = "kitdefinitiondtl") String kitdefinitiondtl) {

		Optional<KitDefenitionDtl> kitDef= kitdefnitionDetailRepo.findById(kitdefinitiondtl);
		
		return  kitDef.get();

	}
	
 @GetMapping("{companymstid}/kitdefinitiondtl/{kitdefinitionMstId}/kitdefinitiondtls")
	 
	public List<KitDefenitionDtl> retrieveAllKitDefById(
			@PathVariable(value = "kitdefinitionMstId") String kitdefinitionMstId) {
 
		return kitdefnitionDetailRepo.findBykitDefenitionmstId( kitdefinitionMstId);
		
		  
 

	}
	

 //-------------------------------new url by using publish offline----------------
 
 @PostMapping("{companymstid}/kitdefinitionmsts/{kitdefinitionmstId}/kitdefinitondtl/{status}")
	public KitDefenitionDtl sendOffLineMessageToCloud(@PathVariable(value = "companymstid") String companymstid,
	      @PathVariable(value = "kitdefinitionmstId") String kitdefinitionmstId,
			@Valid @RequestBody KitDefenitionDtl kitdefinitionDtlRequest,@PathVariable("status") String status) {
	 
	 ResponseEntity<KitDefenitionDtl> saved=	externalApi.sendKitDefenitionDtlOffLineMessageToCloud(companymstid, kitdefinitionDtlRequest,kitdefinitionmstId);
		
		return saved.getBody();
 }
}
