package com.maple.restserver.resource;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AccountPayable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.LmsQueueTallyMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.StockTransferOutDtl;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.entity.StoreChangeDtl;
import com.maple.restserver.entity.StoreChangeMst;
import com.maple.restserver.entity.StoreMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.StoreChangeDtlRepository;
import com.maple.restserver.repository.StoreChangeMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@Transactional
@RestController
public class StoreChangeMstResource {
	
	private static final Logger logger = LoggerFactory.getLogger(PurchaseHdrResource.class);
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@Autowired
	  ItemBatchDtlService itemBatchDtlService ;
	
	@Autowired
	private ItemBatchMstRepository itemBatchMstRepo;
	
	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;
	
	@Autowired
	CompanyMstRepository cmpanyMstRepository;
	
	@Autowired
	StoreChangeDtlRepository storeChangeDtlRepository;
	
	@Autowired
	StoreChangeMstRepository storeChangeMstRepository;
	

	@Autowired
	private VoucherNumberService voucherService;
	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository; 
	
	EventBus eventBus = EventBusFactory.getEventBus();
	

	@PostMapping("{companymstid}/storechangemstresource/storechangemst")
	public StoreChangeMst createStoreChangeMst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  StoreChangeMst  storeChangeMst)
	{
	Optional<CompanyMst> companyMst= cmpanyMstRepository.findById(companymstid);
	storeChangeMst.setCompanyMst(companyMst.get());
//		return  storeChangeMstRepository.saveAndFlush(storeChangeMst);
	return  saveAndPublishService.saveStoreChangeMst(storeChangeMst, mybranch);
		
	}
	
	@GetMapping("{companymstid}/storechangemstresource/storechangemstbetweendate")
	public List<StoreChangeMst> getStoreChangeMstBtwnDate(
			@RequestParam (value = "fromdate")String fromdate,
			@RequestParam(value ="todate")String todate)
	{
		Date fudate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		Date tudate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		return storeChangeMstRepository.getStoreChangeMstBtwnDate(fudate,tudate);
	}
	

	@PutMapping("{companymstid}/storechangemstresource/storechangemstupdate/{storechangemstid}")
	public StoreChangeMst StoreChangeMstFinalSave(@PathVariable String storechangemstid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody StoreChangeMst storechangemstRequest) {
		
		Optional<CompanyMst> companyMst= cmpanyMstRepository.findById(companymstid);

		Optional<StoreChangeMst> storechangeMstOpt=storeChangeMstRepository.findById(storechangemstid);
		StoreChangeMst storechangeMst =storechangeMstOpt.get();
		storechangeMst.setVoucherNumber(storechangemstRequest.getVoucherNumber());
		storechangeMst.setStore(storechangemstRequest.getStore());
		storechangeMst.setToStore(storechangemstRequest.getToStore());
		storechangeMst.setCompanyMst(companyMst.get());
		storechangeMst.setVoucherDate(storechangemstRequest.getVoucherDate());
		storechangeMst.setFromBranch(storechangemstRequest.getFromBranch());
//		storechangeMst = storeChangeMstRepository.saveAndFlush(storechangeMst);
		storechangeMst = saveAndPublishService.saveStoreChangeMst(storechangeMst, mybranch);
		updateStoreChangeStock(storechangeMst,companyMst.get());
		
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", storechangeMst.getVoucherNumber());
		variables.put("voucherDate", storechangeMst.getVoucherDate());
		variables.put("id", storechangeMst.getId());
		variables.put("companyid", storechangeMst.getCompanyMst());
		variables.put("branchcode", storechangeMst.getFromBranch());

		variables.put("inet", 0);
		variables.put("REST", 1);

		variables.put("WF", "forwardStoreChange");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));

		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardStoreChange");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);

		
		return storechangeMst;
	}
	
	
	private void updateStoreChangeStock(StoreChangeMst storeChangeMst, CompanyMst comapnyMst) {

		List<StoreChangeDtl> storeChangeDtlList = storeChangeDtlRepository
				.findByStoreChangeMstAndCompanyMst(storeChangeMst,comapnyMst);

		Iterator iter = storeChangeDtlList.iterator();
		while (iter.hasNext()) {
			StoreChangeDtl storeChangeDtl = (StoreChangeDtl) iter.next();

			//=======apply a common function for item_batch_mst updation and 
			// ======== item_batch_dtl insertion ====== by anandu === 25-06-2021
			
			
			//===========from store updation===================
			Double Qty = storeChangeDtl.getQty() * -1;
			String processInstanceId = null;
			String taskId = null;
			Date ExpiryDate = null;
			
			itemBatchDtlService.itemBatchMstUpdation(storeChangeDtl.getBarcode(),storeChangeDtl.getBatch(),storeChangeMst.getFromBranch(),
					ExpiryDate,storeChangeDtl.getItemId(),storeChangeDtl.getMrp(),processInstanceId,Qty,taskId,comapnyMst,storeChangeMst.getStore());
			
			
			final VoucherNumber voucherNo = voucherService.generateInvoice("STV", comapnyMst.getId());
			String particulars = "STORE CHANGE FROM " + storeChangeMst.getStore();
			Double qtyIn = 0.0;
			
			
			logger.info("Inside itemBatchDtlInsertionNew");
			
			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
			itemBatchDtl.setBarcode(storeChangeDtl.getBarcode());
			itemBatchDtl.setBatch(storeChangeDtl.getBatch());
			itemBatchDtl.setBranchCode(storeChangeMst.getFromBranch());
			itemBatchDtl.setExpDate(ExpiryDate);
			itemBatchDtl.setItemId(storeChangeDtl.getItemId());
			itemBatchDtl.setMrp(storeChangeDtl.getMrp());
			itemBatchDtl.setParticulars(particulars);
			itemBatchDtl.setProcessInstanceId(processInstanceId);
			itemBatchDtl.setQtyIn(qtyIn);
			itemBatchDtl.setQtyOut(storeChangeDtl.getQty());
			itemBatchDtl.setSourceDtlId(storeChangeDtl.getId());
			itemBatchDtl.setSourceParentId(storeChangeMst.getId());
			itemBatchDtl.setSourceVoucherDate(storeChangeMst.getVoucherDate());
			itemBatchDtl.setSourceVoucherNumber(storeChangeMst.getVoucherNumber());
			itemBatchDtl.setStore(storeChangeMst.getStore());
			itemBatchDtl.setTaskId(taskId);
			itemBatchDtl.setVoucherDate(storeChangeMst.getVoucherDate());
			itemBatchDtl.setVoucherNumber(voucherNo.getCode());
			itemBatchDtl.setCompanyMst(comapnyMst);

//			itemBatchDtl = itemBatchDtlRepo.saveAndFlush(itemBatchDtl);
			saveAndPublishService.saveItemBatchDtl(itemBatchDtl, mybranch);
			
//			itemBatchDtlService.itemBatchDtlInsertionNew(storeChangeDtl.getBarcode(),storeChangeDtl.getBatch(),storeChangeMst.getFromBranch(),
//					ExpiryDate,storeChangeDtl.getItemId(),storeChangeDtl.getMrp(),particulars,processInstanceId,
//					qtyIn,storeChangeDtl.getQty(),storeChangeDtl.getId(),storeChangeMst.getId(),storeChangeMst.getVoucherDate(),
//					storeChangeMst.getVoucherNumber(),storeChangeMst.getStore(),taskId,
//					storeChangeMst.getVoucherDate(),voucherNo.getCode(),comapnyMst);

			
			//===========to store updation===================
			
			itemBatchDtlService.itemBatchMstUpdation(storeChangeDtl.getBarcode(),storeChangeDtl.getBatch(),storeChangeMst.getFromBranch(),
					ExpiryDate,storeChangeDtl.getItemId(),storeChangeDtl.getMrp(),processInstanceId,storeChangeDtl.getQty(),taskId,comapnyMst,storeChangeMst.getToStore());
			
			Double qtyOut = 0.0;
			String particularsTo = "STORE CHANGE TO " + storeChangeMst.getStore();
			
			logger.info("Inside itemBatchDtlInsertionNew");
			
			ItemBatchDtl itemBatchDtlToStore = new ItemBatchDtl();
			itemBatchDtlToStore.setBarcode(storeChangeDtl.getBarcode());
			itemBatchDtlToStore.setBatch(storeChangeDtl.getBatch());
			itemBatchDtlToStore.setBranchCode(storeChangeMst.getFromBranch());
			itemBatchDtlToStore.setExpDate(ExpiryDate);
			itemBatchDtlToStore.setItemId(storeChangeDtl.getItemId());
			itemBatchDtlToStore.setMrp(storeChangeDtl.getMrp());
			itemBatchDtlToStore.setParticulars(particularsTo);
			itemBatchDtlToStore.setProcessInstanceId(processInstanceId);
			itemBatchDtlToStore.setQtyIn(storeChangeDtl.getQty());
			itemBatchDtlToStore.setQtyOut(qtyOut);
			itemBatchDtlToStore.setSourceDtlId(storeChangeDtl.getId());
			itemBatchDtlToStore.setSourceParentId(storeChangeMst.getId());
			itemBatchDtlToStore.setSourceVoucherDate(storeChangeMst.getVoucherDate());
			itemBatchDtlToStore.setSourceVoucherNumber(storeChangeMst.getVoucherNumber());
			itemBatchDtlToStore.setStore(storeChangeMst.getToStore());
			itemBatchDtlToStore.setTaskId(taskId);
			itemBatchDtlToStore.setVoucherDate(storeChangeMst.getVoucherDate());
			itemBatchDtlToStore.setVoucherNumber(voucherNo.getCode());
			itemBatchDtlToStore.setCompanyMst(comapnyMst);
			
			saveAndPublishService.saveItemBatchDtl(itemBatchDtlToStore, mybranch);
//			itemBatchDtlService.itemBatchDtlInsertionNew(storeChangeDtl.getBarcode(),storeChangeDtl.getBatch(),storeChangeMst.getFromBranch(),
//					ExpiryDate,storeChangeDtl.getItemId(),storeChangeDtl.getMrp(),particularsTo,processInstanceId,
//					storeChangeDtl.getQty(),qtyOut,storeChangeDtl.getId(),storeChangeMst.getId(),storeChangeMst.getVoucherDate(),
//					storeChangeMst.getVoucherNumber(),storeChangeMst.getToStore(),taskId,
//					storeChangeMst.getVoucherDate(),voucherNo.getCode(),comapnyMst);
			
			//======================end =================25-06-2021===============
		}

	}
	
}
