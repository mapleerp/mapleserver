package com.maple.restserver.resource;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.TermsAndConditionsMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.TermsAndConditionRepository;
@RestController
@Transactional
public class TermsAndConditionResource {
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired 
	TermsAndConditionRepository termsAndConditionRepository;
	
	
	@GetMapping("{companymstid}/termsandconditionmst/gettermsandconditionmst")
	public List<TermsAndConditionsMst> retrieveAllTermsAndConditionMst(
			@PathVariable(value = "companymstid") String
			  companymstid){
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return termsAndConditionRepository.findByCompanyMst(companyOpt.get());
	}
	
	
	@GetMapping("{companymstid}/termsandconditionmst/gettermsandconditionmstbyinvoiceformat/{invoiceformat}")
	public List<TermsAndConditionsMst> retrieveAllTermsAndConditionMstByInvoiceFormat(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "invoiceformat") String invoiceformat){
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return termsAndConditionRepository.findByCompanyMstAndInvoiceFormat(companyOpt.get(),invoiceformat);
	}  
	
	
	@PostMapping("{companymstid}/termsandconditionmst/savetermsandconditionmst")
	public TermsAndConditionsMst createTermsAndConditionMst(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  TermsAndConditionsMst termsAndConditionMst)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			termsAndConditionMst.setCompanyMst(companyMst);
		
		
		return termsAndConditionRepository.saveAndFlush(termsAndConditionMst);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));
		
		
		
	
	}
	
	
	@DeleteMapping("{companymstid}/termsandconditionmst/deletetermsandconditionmst/{id}")
	public void salesDtlDelete(
			@PathVariable(value = "companymstid") String  companymstid,
			@PathVariable(value = "id") String  id) {
		
		termsAndConditionRepository.deleteById(id);
		
	}
	

}
