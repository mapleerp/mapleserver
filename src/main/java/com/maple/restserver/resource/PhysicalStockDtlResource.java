package com.maple.restserver.resource;

import java.net.URI;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchExpiryDtl;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PhysicalStockDtl;
import com.maple.restserver.entity.PhysicalStockHdr;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.entity.VisitDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.PharmacyPhysicalStockVarianceReport;
import com.maple.restserver.report.entity.PhysicalStockReport;
import com.maple.restserver.report.entity.PoliceReport;
import com.maple.restserver.report.entity.StockMigrationReport;
import com.maple.restserver.repository.ItemBatchExpiryDtlRepository;
import com.maple.restserver.repository.PhysicalStockDtlRepository;
import com.maple.restserver.repository.PhysicalStockHdrRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.VisitDtlRepository;
import com.maple.restserver.service.PhysicalStockService;
import com.maple.restserver.service.PhysicalStockServiceImpl;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class PhysicalStockDtlResource {
	@Autowired
	PhysicalStockServiceImpl physicalStockServiceImpl;
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	private PhysicalStockDtlRepository physicalstockDelRepo;

	
	@Autowired
	PhysicalStockService physicalStockService;
	@Autowired
	private PhysicalStockHdrRepository physicalstockHdrRepo;

	@Autowired
	private ItemBatchExpiryDtlRepository itemBatchExpiryDtlRepo;

	@GetMapping("{companymstid}/physicalstockdtltest/{physicalstockhdrid}/physicalstockdtl")
	public List<PhysicalStockDtl> retrievePhysicalstock(@PathVariable("physicalstockhdrid") String physicalstockhdrid,
			@PathVariable("companymstid") String companymstid, Pageable pageable) {

		Optional<PhysicalStockHdr> phdrOpt = physicalstockHdrRepo.findByIdAndCompanyMstId(physicalstockhdrid,
				companymstid);
		PhysicalStockHdr ph = phdrOpt.get();

		return physicalstockDelRepo.findByPhysicalStockHdrId(ph);
	}

	@Transactional
	@PostMapping("{companymstid}/physicalstockhdr/{physicalstockhdrid}/physicalstockdtl")
	public PhysicalStockDtl createSalesDtl(@PathVariable(value = "physicalstockhdrid") String physicalstockhdrid,
			@Valid @RequestBody PhysicalStockDtl physicalstockDtlRequest) {

		return physicalstockHdrRepo.findById(physicalstockhdrid).map(physicalstockhdr -> {
			physicalstockDtlRequest.setPhysicalStockHdrId(physicalstockhdr);
			physicalstockDtlRequest.setCompanyMst(physicalstockhdr.getCompanyMst());

			PhysicalStockDtl saved = physicalstockDelRepo.saveAndFlush(physicalstockDtlRequest);
			if (!physicalstockDtlRequest.getBatch().equalsIgnoreCase(MapleConstants.Nobatch)) {
				ItemBatchExpiryDtl itemBatchExpiryDtl = new ItemBatchExpiryDtl();
				List<ItemBatchExpiryDtl> items = itemBatchExpiryDtlRepo.findByCompanyMstAndItemIdAndBatch(
						physicalstockhdr.getCompanyMst(), saved.getItemId(), saved.getBatch());
				if (items.isEmpty()) {
					itemBatchExpiryDtl.setBatch(saved.getBatch());
					itemBatchExpiryDtl.setCompanyMst(physicalstockhdr.getCompanyMst());
					itemBatchExpiryDtl.setExpiryDate(saved.getExpiryDate());
					itemBatchExpiryDtl.setItemId(saved.getItemId());
					itemBatchExpiryDtl.setPhysicalStockDtl(saved);
					itemBatchExpiryDtl.setUpdatedDate(ClientSystemSetting.getSystemDate());
					itemBatchExpiryDtlRepo.save(itemBatchExpiryDtl);
					eventBus.post(itemBatchExpiryDtl);
				} else {
					itemBatchExpiryDtl.setBatch(saved.getBatch());
					itemBatchExpiryDtl.setCompanyMst(physicalstockhdr.getCompanyMst());
					itemBatchExpiryDtl.setExpiryDate(saved.getExpiryDate());
					itemBatchExpiryDtl.setItemId(saved.getItemId());
					itemBatchExpiryDtl.setPhysicalStockDtl(saved);
					itemBatchExpiryDtl.setUpdatedDate(ClientSystemSetting.getSystemDate());
					if (null != saved.getExpiryDate())
						itemBatchExpiryDtlRepo.UpdateItemBatchExpiryDtl(saved.getItemId(), saved.getExpiryDate(),
								saved.getBatch());
					
					List<ItemBatchExpiryDtl> itemsList = itemBatchExpiryDtlRepo.findByCompanyMstAndItemIdAndBatch(
							physicalstockhdr.getCompanyMst(), saved.getItemId(), saved.getBatch());

					eventBus.post(itemsList.get(0));
				}
			}
			return saved;
		}).orElseThrow(() -> new ResourceNotFoundException("physicalstockhdrid " + physicalstockhdrid + " not found"));

	}

	@DeleteMapping("{companymstid}/physicalstockdtl/{physicalstockdtlid}")
	public void salesDtlDelete(@PathVariable String physicalstockdtlid) {
		physicalstockDelRepo.deleteById(physicalstockdtlid);

	}

	@PutMapping("{companymstid}/physicalstockhdr/{physicalstockHdrId}/physicalstockdtl/{physicalstockdtlid}")
	public PhysicalStockDtl updateSalesDtl(@PathVariable(value = "physicalstockHdrId") String physicalstockHdrId,
			@PathVariable(value = "physicalstockdtlid") String physicalstockdtlid,
			@Valid @RequestBody PhysicalStockDtl physicalstockDtlRequest) {
		if (!physicalstockHdrRepo.existsById(physicalstockHdrId)) {
			throw new ResourceNotFoundException("salestransHdrId " + physicalstockHdrId + " not found");
		}

		return physicalstockDelRepo.findById(physicalstockdtlid).map(salesDtl -> {
			salesDtl.setQtyIn(physicalstockDtlRequest.getQtyIn());

			// salesDtl.setQtyOut(physicalstockDtlRequest.getQtyOut());
			return physicalstockDelRepo.saveAndFlush(physicalstockDtlRequest);
		}).orElseThrow(() -> new ResourceNotFoundException("salestransHdrId " + physicalstockHdrId + "not found"));
	}

	@GetMapping("{companymstid}/physicalstockdtl")
	public List<PhysicalStockDtl> retrieveAllreceivabledtls(@PathVariable(value = "companymstid") String companymstid) {
		return physicalstockDelRepo.findByCompanyMstId(companymstid);
	}

	@GetMapping("{companymstid}/physicalstockreport")
	public List<PhysicalStockReport> getPhysicalStockDtlReport(
			@PathVariable(value = "companymstid") String companymstid, @RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate) {
		java.util.Date sdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");

		List<PhysicalStockReport>  result =  physicalStockServiceImpl.getPhysicalStockDtlReport(companymstid, sdate, edate) ; 
		
		return result;
	}

	// ===============sibi========================//
	
	
	
	//------------------------------PhysicalStockVarianceReport  for pharmacy use-----------------
	
	@GetMapping("{companymstid}/physicalstockdtlresource/getpharmacyphysicalstockvariancereport")		
	public List<PharmacyPhysicalStockVarianceReport> getPharmacyPhysicalStockVarianceReport(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fromdate") String fDate,
			@RequestParam("todate") String tDate) {
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fDate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(tDate, "yyyy-MM-dd");

		return physicalStockService.findPhysicalStockVarianceReport(fromDate,toDate);
	}

}
