package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BankMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.BankMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;


@RestController
@Transactional
public class BankMstResource {
	


	
	@Autowired
	private BankMstRepository bankMst;
	
	
	@PostMapping("/{companymstid}/bankmst")
	public BankMst createUser(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody BankMst bankMst1)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			bankMst1.setCompanyMst(companyMst);
	return bankMst.saveAndFlush(bankMst1);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
	
	
	
	@GetMapping("{companymstid}/bankmst")
	public List<BankMst> retrieveAllcash(@PathVariable(value = "companymstid") String
			 companymstid)
	{
		return bankMst.findByCompanyMstId(companymstid);
	}
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	

	@GetMapping("/{companymstid}/findallbankmst")
	public  @ResponseBody List<BankMst> userDetailbyId(@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "id") String id){
		return bankMst.findByCompanyMstId(companymstid);
	}
	}

	



