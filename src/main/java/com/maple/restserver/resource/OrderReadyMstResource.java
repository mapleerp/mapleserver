package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.OrderReadyMst;
import com.maple.restserver.entity.OtherBranchSalesDtl;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.OrderReadyMstRepository;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class OrderReadyMstResource {
	@Autowired
	CompanyMstRepository companyMstRepository;
@Autowired
OrderReadyMstRepository orderReadyRepo;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	EventBus eventBus = EventBusFactory.getEventBus();
	
	@GetMapping("{companymstid}/orderreadymstresource/changestatus/{branchcode}/{tablenumber}")
	public String createChefWindowMst(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "tablenumber") String tablenumber,
			@PathVariable(value = "branchcode") String branchcode )
	{
		
		
		CompanyMst companyMst = companyMstRepository.findById(companymstid).get();
		
		
		  Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", tablenumber);

			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", tablenumber);

			variables.put("companyid",companyMst);
			variables.put("branchcode", branchcode);

			variables.put("REST", 1);
			
			
			variables.put("WF", "forwardChefstatus");
			
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardChefstatus");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			
			variables.put("lmsqid", lmsQueueMst.getId());
			
			eventBus.post(variables);
		
		return "success";
		
	}
	
	@PostMapping("{companymstid}/orderreadymstresource/saveorderreadymst")
	public OrderReadyMst createOrderReadyMst(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody OrderReadyMst orderreadymst) 
	
	{
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		orderreadymst.setCompanyMst(companyMst.get());
		//Order ready and save
		orderReadyRepo.save(orderreadymst);
		return orderreadymst;
	}
	
	}
	
