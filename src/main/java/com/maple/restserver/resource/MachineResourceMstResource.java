package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.MachineResourceMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.KitValidationReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.MachineResourceMstRepository;
import com.maple.restserver.service.MachineResourceService;
import com.maple.restserver.service.kitdefinionservice;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class MachineResourceMstResource {

	@Autowired
	MachineResourceService machineResourceService;
	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	MachineResourceMstRepository machineResourceMstRepository;
	@Autowired
	kitdefinionservice  kitdefinionservice;
	
	@Autowired
	ItemMstRepository itemMstRepository;

	@PostMapping("{companymstid}/machineresourcemstresource/createmachineresourcemst")
	public MachineResourceMst createMachineResourceMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody MachineResourceMst machineResourceMst) {
		return companyMstRepository.findById(companymstid).map(companyMst -> {
			machineResourceMst.setCompanyMst(companyMst);

			return machineResourceMstRepository.save(machineResourceMst);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	@GetMapping("{companymstid}/machineresourcemstresource/findallmachineresourcemst")
	public List<MachineResourceMst> retrieveMachineResourceMst(
			@PathVariable(value = "companymstid") String companymstid) {
		return machineResourceService.machineDetails();
	}

	@GetMapping("{companymstid}/machineresourcemstresource/findallmachineresourcemsts")
	public ArrayList<MachineResourceMst> retrieveMachineResourceMsts(
			@PathVariable(value = "companymstid") String companymstid) {
		return machineResourceMstRepository.fetchMachineDetais();
	}

	@GetMapping("{companymstid}/machineresourcemstresource/findallmachineresourcebyname/{resourcename}")
	public MachineResourceMst retrieveMachineResourceMstByName(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "resourcename") String machineresourcename) {
		return machineResourceMstRepository.findByMachineResourceName(machineresourcename);
	}

	@GetMapping("{companymstid}/machineresourcemstresource/findbymachineresourcebyid/{id}")
	public Optional<MachineResourceMst> retrieveMachineResourceMstById(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "id") String id) {

		return machineResourceMstRepository.findById(id);
	}
	/*
	 * @GetMapping(
	 * "{companymstid}/machineresourcemstresource/{branchcode}/findproductmachineratio")
	 * public ProductionMst productMachineRatio(@PathVariable(value =
	 * "companymstid") String companymstid,
	 * 
	 * @PathVariable(value = "branchcode") String
	 * branchcode, @RequestParam("fromdate") String fromdate)
	 * 
	 * { // @RequestParam Double machineCapacity java.util.Date fromDate =
	 * SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
	 * List<MachineResourceMst> machineResourceMstList =
	 * machineResourceMstRepository.findAll();
	 * 
	 * Double machineCapacity = machineResourceMstList.get(0).getCapcity();
	 * 
	 * System.out.print(machineCapacity + "machine capacity "); return
	 * machineResourceService.productMachineRatio(companymstid, fromDate,
	 * machineCapacity, branchcode); }
	 */

	/*
	 * @GetMapping(
	 * "{companymstid}/machineresourcemstresource/fetchsaleorderitemqty/{branchcode}")
	 * public List<SaleOrderItemQty> fetchSaleOrderItemQty(@PathVariable(value =
	 * "companymstid") String companymstid,
	 * 
	 * @PathVariable(value = "item") String item, @PathVariable(value =
	 * "branchcode") String branchcode,
	 * 
	 * @RequestParam("fromdate") String fromdate, @RequestParam Double
	 * machineCapacity)
	 * 
	 * { java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,
	 * "yyyy-MM-dd");
	 * 
	 * return machineResourceService.fetchSaleOrderItemQty(branchcode, companyfetchSaleOrderProductionPrePlanningmstid,
	 * fromDate); }
	 */

	/*
	 * @GetMapping(
	 * "{companymstid}/machineresourcemstresource/productionpreplaningtoproduction")
	 * public String productionPrePlaningToProduction(@PathVariable(value =
	 * "companymstid") String companymstid)
	 * 
	 * { // java.util.Date //
	 * fromDate=SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
	 * 
	 * return machineResourceService.productionPrePlaningToProduction(); }
	 */
	/*
	 * @GetMapping(
	 * "{companymstid}/machineresourcemstresource/fetchproductionpreplaningtoproduction")
	 * public List<ProductionDtl> fetchProductionPrePlaningToProduction(
	 * 
	 * @PathVariable(value = "companymstid") String companymstid)
	 * 
	 * { // java.util.Date //
	 * fromDate=SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
	 * 
	 * return machineResourceService.fetchProductionPrePlaningToProduction(); }
	 */
	

	@GetMapping("{companymstid}/machineresourcemstresource/{branchcode}/fetchsaleorderproductqty")
	public String fetchSaleOrderProductQty(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, 
			@RequestParam("fromdate") String fromdate,@RequestParam("tdate") String toDate)throws  ResourceNotFoundException

	{
		// @RequestParam Double machineCapacity
		java.util.Date fdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");

		java.util.Date tdate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
		
	

	try {
		 machineResourceService.preplanningBatchInitializeStage2( branchcode, companymstid,  fdate,tdate);
}catch (Exception e) {
			System.out.println("***********************************ERROR"+e.getMessage());
			System.out.println("***********************************ERROR"+e.toString());

			return e.getMessage().toString();

		}
	
	return "Sucesses";
	}
	
	
	
	@DeleteMapping("{companymstid}/machineresourcemstresource/deleteproductionpreplanning")
	public void  DeleteProductionPrePlanninG(@RequestParam("fromdate") String fromdate,@RequestParam("tdate") String toDate) {
		java.util.Date fdate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");

		java.util.Date tdate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
		
		
		
		machineResourceService.deleteProductionPrePlanning(fdate,tdate);
		
	}
	
	
	
	@GetMapping("{companymstid}/machineresourcemstresource/validatekit/{itemname}")
	public  KitValidationReport  rawMaterialForSingleItemInMachineUnit(@PathVariable (value = "companymstid")String companyMstId,
			@PathVariable(value ="itemname") String itemName) throws  ResourceNotFoundException
	{
		CompanyMst companyMst = companyMstRepository.findById(companyMstId).get(); 
		
	ItemMst ItemMst=itemMstRepository.findByItemName(itemName);
		String responseMessage="";
	
	System.out.print(itemName+"item name isssssssss@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");

		return  kitdefinionservice.findRawMaterialForSingleItemInMachineUnit(companyMstId,ItemMst.getId());
	
	
	}

}
