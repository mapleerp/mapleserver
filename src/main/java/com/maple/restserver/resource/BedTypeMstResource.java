
package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.his.entity.BedTypeMst;
import com.maple.restserver.repository.BedTypeMstRepository;



@RestController
@Transactional
public class BedTypeMstResource {
	@Autowired
	private BedTypeMstRepository bedtypemaster;
	@GetMapping("{companymstid}/bedtypemst")
	public List<BedTypeMst> retrieveAlldepartments()
	{
		return bedtypemaster.findAll();
		
	}
	@PostMapping("{companymstid}/bedtypemst")
	public ResponseEntity<Object>createUser(@Valid @RequestBody BedTypeMst bedtypemaster1)
	{
		BedTypeMst saved=bedtypemaster.saveAndFlush(bedtypemaster1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}

}

