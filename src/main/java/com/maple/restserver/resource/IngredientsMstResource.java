package com.maple.restserver.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.IngredientsMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.repository.IngredientMstRepository;

@RestController
public class IngredientsMstResource {

	@Autowired
	IngredientMstRepository ingredientMstRepository;
	
	
	@GetMapping("{companymstid}/IngredientsMstResource/{id}/findbyitemmst")

	public IngredientsMst retrievIngredient(@PathVariable(value="companymstid") String companymstid,@PathVariable(value="id") String id
			){
		
		
		return ingredientMstRepository.findByItemMstId(id);
	}
}
