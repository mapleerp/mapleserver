

package com.maple.restserver.resource;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ReorderMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ReorderMstRepository;
import com.maple.restserver.service.ReorderService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ReorderMstResource {
private static final Logger logger = LoggerFactory.getLogger(ReorderMstResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	private  ReorderMstRepository reorderMstRepository;
	
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	@Autowired
	private ReorderService reorderService;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	@GetMapping("{companymstid}/reordermsts")
	public List< ReorderMst> retrieveAllSupplier(){
		return reorderMstRepository.findAll();
	}
	
	@PostMapping("{companymstid}/reordermst")
	public ReorderMst createReorder(@Valid @RequestBody 
			ReorderMst  reorderMst,
			@PathVariable (value = "companymstid") String companymstid)
	{
		
	 
	    Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		reorderMst.setCompanyMst(companyMst);
		 
		
		
		// ReorderMst saved = reorderMstRepository.save(reorderMst);
		ReorderMst saved =saveAndPublishService.saveReorderMst(reorderMst, reorderMst.getBranchCode());
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", saved.getId());

		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", saved.getId());

		variables.put("companyid", saved.getCompanyMst());
		variables.put("branchcode", saved.getBranchCode());

		variables.put("REST", 1);
		
		
		variables.put("WF", "forwardReorderMst");
		
		
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardReorderMst");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		
		variables.put("lmsqid", lmsQueueMst.getId());
		
		eventBus.post(variables);
	 
		
		return saved;
	
	}

	@GetMapping("{companymstid}/reordermst/{itemName}/{supliername}/reoders")
	public  @ResponseBody List<ReorderMst> 
	getReorderDetail(@PathVariable("itemName") String itemName,
			@PathVariable("supliername")String SuplierName)
	{
		return reorderMstRepository.fetchreorderbyItemNameAndSupplierName(itemName,SuplierName);
	}
	@GetMapping("{companymstid}/reordermst/{itemName}{minqty}")
	public  @ResponseBody List<ReorderMst> 
	getReorderDetails(@RequestParam("itemName") String itemName,@RequestParam("minqty")String minqty){
		return reorderMstRepository.fetchreorderbyItemNameAndSupplierName(itemName,minqty);
	}
	@DeleteMapping("{companymstid}/reordermst/{reordermstId}")
	public void DeletereorderMst(@PathVariable (value="reordermstId") String reordermstId) {
		reorderMstRepository.deleteById(reordermstId);

	}

	
	@GetMapping("{companymstid}/reordersearch")
	public  @ResponseBody List<ReorderMst> searchReorder(@RequestParam("data") String searchstring){
		return reorderService.searchLikeReorderByName("%"+searchstring+"%") ;
	}
	
	
	
}
