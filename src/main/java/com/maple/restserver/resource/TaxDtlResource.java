package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.TaxDtl;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.TaxDtlRepository;


@RestController
@Transactional
public class TaxDtlResource {
	
	@Autowired
	private TaxDtlRepository taxRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("{companymstid}/taxdtls")
	public List< TaxDtl> retrieveAllSupplier(@PathVariable(value = "companymstid") String
			  companymstid){
		return taxRepository.findByCompanyMst(companymstid);
	}
	

	
	
	
	
	@PostMapping("{companymstid}/taxdtls")
	public TaxDtl createsupplier(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			TaxDtl  taxdtl1)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			taxdtl1.setCompanyMst(companyMst);
		return taxRepository.saveAndFlush(taxdtl1);
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
			
	
	}


