package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.ReceiptInvoiceDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.repository.ReceiptInvoiceDtlRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.service.ReceiptInvoiceDtlService;
import com.maple.restserver.service.SaveAndPublishService;


@RestController
@Transactional
public class ReceiptInvoiceDtlResource {
	
private static final Logger logger = LoggerFactory.getLogger(ReceiptInvoiceDtlResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	@Autowired
	ReceiptHdrRepository 	receiptHdrRepository;
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	ReceiptInvoiceDtlRepository receiptInvoiceDtlRepository;
	@Autowired
	
	SalesTransHdrRepository salesTransHdrRepository;
	
	@Autowired
	ReceiptInvoiceDtlService receiptInvoiceDtlService;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@PostMapping("/{companymstid}/receiptinvoicedtl")
	public ReceiptInvoiceDtl createReceiptInvoiceDtl(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody ReceiptInvoiceDtl receiptinvoicedtl)
	{
		   
		List<SalesTransHdr>  transHdr=salesTransHdrRepository.findByVoucherNumberAndVoucherDate(receiptinvoicedtl.getInvoiceNumber(),receiptinvoicedtl.getInvoiceDate());
		
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			
			receiptinvoicedtl.setCompanyMst(companyMst);
			receiptinvoicedtl.setSalesTransHdr(transHdr.get(0));
	// return receiptInvoiceDtlRepository.saveAndFlush(receiptinvoicedtl);
			return saveAndPublishService.saveReceiptInvoiceDtl(receiptinvoicedtl,mybranch);
			
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
	

	
	@GetMapping("{companymstid}/receiptinvoicedtl/getreceivedamount/{receiptHdr}")
	 
	public Double getReceivedAmount(
			@PathVariable(value = "companymstid") String companymstid,@PathVariable(value = "receiptHdr") String receipthdr
			){
	
 
		//return receiptInvoiceService.findByVoucherDateAndVoucherNumbergetReceiptByVoucherAndDate(voucherNumber, reportdate, companymstid);
		return receiptInvoiceDtlRepository.getReceivedAmount(receipthdr);
 
	
 
	}
	 

	@GetMapping("{companymstid}/receiptinvoicedtl/{receipthdrid}/getreceiptinvdtlbyid")
	public List<ReceiptInvoiceDtl> retrieveReceiptInVDtlByReceiptId(
			@PathVariable(value = "receipthdrid") String receipthdrid) {
		Optional<ReceiptHdr> receiptHdr  =receiptHdrRepository.findById(receipthdrid);
		return receiptInvoiceDtlRepository.findByReceiptHdr(receiptHdr.get());

	}
	
	
	@DeleteMapping("{companymstid}/receiptinvoicedtl/{receiptinvoicedtlid}/deletereceiptinvoicedtl")
	public void deleteReceiptInvoiceDtlByReceiptId(
			@PathVariable(value = "receiptinvoicedtlid") String receiptinvoicedtlid) {

		 receiptInvoiceDtlRepository.deleteById(receiptinvoicedtlid);

	}
	@DeleteMapping("{companymstid}/receiptinvoicedtl/{receiptHdr}/deletereceiptinvoicedtlbyhdr")
	public void deleteReceiptInvoiceDtlByReceiptHdrId(
			@PathVariable(value = "receiptHdr") String receiptHdr) {

		 receiptInvoiceDtlRepository.deleteByReceiptHdrId(receiptHdr);

	}
	
	@GetMapping("{companymstid}/receiptinvoicedtl/{customerid}/retrivereceiptinvoicedtlbycustomer")
	public List<ReceiptInvoiceDtl> retrieveReceiptInvoiceDtlByCustomer(
			@PathVariable(value = "customerid") String customerid) {
	
		return receiptInvoiceDtlService.findByCustomerMstIdAndOwnAccount(customerid);

	}
	
	
				
			
	
}
