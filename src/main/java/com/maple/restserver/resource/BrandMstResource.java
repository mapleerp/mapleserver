package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.BrandMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.ProductMst;
import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.BrandMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.ProductMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class BrandMstResource {
	
private static final Logger logger = LoggerFactory.getLogger(BrandMstResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	Pageable topFifty =   PageRequest.of(0, 50);

	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	private BrandMstRepository brandMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	

	@GetMapping("{companymstid}/brandmstrepository/brandmstbyname")
	public BrandMst retrieveBrandMstByName(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("brandname") String brandname) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}

		return brandMstRepository.findByCompanyMstAndBrandName(companyOpt.get(), brandname);
	}

	@PostMapping("{companymstid}/brandmstrepository/savebrandmst")
	public BrandMst createBrandMst(@Valid @RequestBody BrandMst brandMst,
			@PathVariable(value = "companymstid") String companymstid) {
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}

		brandMst.setCompanyMst(companyOpt.get());

//		brandMst = brandMstRepository.saveAndFlush(brandMst);
		brandMst =saveAndPublishService.saveBrandMst(brandMst,brandMst.getBranchCode());
		 logger.info("saveBrandMst send to KafkaEvent: {}", brandMst);
		

		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", brandMst.getId());

		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", brandMst.getId());

		variables.put("companyid", brandMst.getCompanyMst());
		variables.put("branchcode", brandMst.getBranchCode());

		variables.put("REST", 1);

		variables.put("WF", "forwardBrandMst");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

		// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));

		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardBrandMst");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);

		return brandMst;
	}

	@GetMapping("{companymstid}/brandmstrepository/brandmstbyid/{brandid}")
	public BrandMst retrieveBrandMstById(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "brandid") String brandid) {

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);

		if (!companyOpt.isPresent()) {
			return null;
		}

		return brandMstRepository.findByCompanyMstAndId(companyOpt.get(), brandid);
	}

	@GetMapping("{companymstid}/brandmst/brandbyname")
	public List<Object> retrieveAllProduct(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid) {
		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		return brandMstRepository.findSearch("%" + searchstring.toLowerCase() + "%", topFifty);
	}
	
	/*
	 * url to find the brandmst by brandname
	 */
	@GetMapping("{companymstid}/brandmstresource/{brandname}")
	public BrandMst getBrandMstByBrandName(@PathVariable(value = "brandname")String brandname)
	{
		return brandMstRepository.findByBrandName(brandname);
	};
	
	/*
	 * url to save brandmst
	 */
	@PostMapping("{companymstid}/brandmstresource")
	public BrandMst createBrandMst(@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody  BrandMst brandMst)
	{
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		brandMst.setCompanyMst(companyMst.get());
			return brandMstRepository.save(brandMst);
	}
	
	/*
	 * url to retrieve all brandMsts
	 */
	@GetMapping("{companymstid}/brandmstresource")
	public List<BrandMst> retrieveAllBrandMsts(@PathVariable(value = "companymstid")String companymstid)
	{
		return brandMstRepository.findAll();
	};
	
	/*
	 * url to delete a brandmst by id
	 */
	@DeleteMapping("{companymstid}/brandmstresource/{brandid}")
	public void deleteBrandMst(@PathVariable(value="companymstid") CompanyMst companymstid,
			@PathVariable (value="brandid") String brandid) {
		brandMstRepository.deleteById(brandid);

	}
}
