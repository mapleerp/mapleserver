package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.BatchExpiryItemReport;
import com.maple.restserver.report.entity.BatchItemWiseSalesReport;
import com.maple.restserver.service.BatchExpiryItemService;
import com.maple.restserver.service.BatchItemWiseSalesReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class BatchExpiryItemReportResource {


	@Autowired
	BatchExpiryItemService batchExpiryItemService;
	
	
	@GetMapping("/{companymstid}/batchexpiryitemwise/{branchcode}/{itemList}")
	public List<BatchExpiryItemReport> batcItemWiseExpirySalesReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,	@PathVariable(value = "itemList") List<String> itemList,  
			@RequestParam("fdate") String fdate, @RequestParam("tdate") String tdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
		return batchExpiryItemService.itemWiseSortExpiry(companymstid,branchcode,fDate,itemList);
	}
	
}
