package com.maple.restserver.resource;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.websocket.server.PathParam;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.entity.LmsQueueTallyMst;
import com.maple.restserver.entity.MonthlyJournalHdr;
import com.maple.restserver.entity.MonthlySalesTransHdr;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.TallyFailedVoucherMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.JournalHdrRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.LmsQueueTallyMstRepository;
import com.maple.restserver.repository.MonthlyJournalDtlRepository;
import com.maple.restserver.repository.MonthlyJournalHdrRepository;
import com.maple.restserver.repository.MonthlySalesDtlRepository;
import com.maple.restserver.repository.MonthlySalesReceiptsRepository;
import com.maple.restserver.repository.MonthlySalesTransHdrRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.repository.SalesDtlTempRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesReceiptsTempRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.TallyFailedVoucherMstRepository;
import com.maple.restserver.repository.TaxMstRepository;
import com.maple.restserver.service.MonthlyJournalHdrService;
import com.maple.restserver.service.MonthlyJournalHdrServiceImpl;
import com.maple.restserver.service.TallyService;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.service.accounting.task.ReceiptAccounting;
import com.maple.restserver.service.accounting.task.SalesAccounting;
import com.maple.restserver.tally.integration.task.CPaymentsToTally;
import com.maple.restserver.tally.integration.task.CReceiptsToTally;
import com.maple.restserver.tally.integration.task.JournalIntegrationTally;
import com.maple.restserver.tally.integration.task.PurchaseIntegrationTally;
import com.maple.restserver.tally.integration.task.SaleIntegrationTally;
import com.maple.restserver.utils.SystemSetting;

@RestController

public class TallyResource {

	
	@Autowired
	MonthlyJournalHdrService monthlyJournalHdrService;
	private static final Logger logger = LoggerFactory.getLogger(SalesAccounting.class);
	@Autowired
	private CreditClassRepository creditClassRepo;

	@Value("${tallyServer}")
	private String tallyServer;
	@Autowired
	TallyService tallyService;
	@Autowired
	private
	PurchaseHdrRepository purchaseHdrRepository;
	
	@Autowired
	private DebitClassRepository debitClassRepo;

	@Autowired
	JournalIntegrationTally journalIntegrationTally;
	@Autowired
	private LedgerClassRepository ledgerClassRepo;

	@Autowired
	private ApplicationContext context;

	@Autowired
	TaxMstRepository taxMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	SalesTransHdrRepository salesTransHdrRepository;
	
	@Autowired
	LmsQueueTallyMstRepository lmsQueueTallyMstRepository;

	@Autowired
	MonthlyJournalDtlRepository monthlyJournalDtlRepository;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	SalesReceiptsRepository salesReceiptsRepo;

	@Autowired
	private AccountClassRepository accountClassRepo;

	@Autowired
	SalesAccounting salesAccounting;

	@Autowired
	SaleIntegrationTally saleIntegrationTally;
	


	@Autowired
	PurchaseIntegrationTally purchaseIntegrationTally;
	
	@Autowired
	 SalesDtlTempRepository salesDtlTempRepository;
	
	@Autowired
	SalesReceiptsTempRepository salesReceiptsTempRepository;
	@Autowired
	ReceiptAccounting receiptAccounting;
	
	@Autowired
	CReceiptsToTally cReceiptsToTally;

	
	@Autowired
	CPaymentsToTally cPaymentsToTally;
	
	
	@Autowired
	ReceiptHdrRepository receiptHdrRepository;
	
	
	@Autowired
	PaymentHdrRepository paymentHdrRepository;
	
	
	@Autowired
	TallyFailedVoucherMstRepository tallyFailedVoucherMstRepository;
	
	@Autowired
	MonthlySalesReceiptsRepository  monthlySalesReceiptsRepository;

	@Autowired 
	MonthlySalesDtlRepository 	monthlySalesDtlRepository;

	@Autowired
	MonthlyJournalHdrServiceImpl monthlyJournalHdrServiceImpl;
	
	@Autowired
	MonthlyJournalHdrRepository monthlyJournalHdrRepository;
	
	
	@Autowired
	JournalHdrRepository journalHdrRepository;
	

	@GetMapping("{companymstid}/restsalestotally")
	public String resetsalesToTally(@PathVariable(value = "companymstid") String companymstid) {
		lmsQueueMstRepository.deleteAll();
		return "Rest Done";
	}
	
	
	@Autowired
	MonthlySalesTransHdrRepository monthlySalesTransHdrRepository;
	

	@GetMapping("{companymstid}/salestotallydirect")
	public String salesToTallyDirect(@PathVariable(value = "companymstid") String companymstid) {

		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findAll();
		
		
		java.sql.Date date = ClientSystemSetting.StringToSqlDate("2020-12-01", "yyy-MM-dd");
		
		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findByVoucherDateGreaterEqual(date);
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findByVoucherNumberLike("%1448%");
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findFailedVouchers();
		
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = salesTransHdrList.iterator();

		while (iterSales.hasNext()) {
			SalesTransHdr salesTransHdr = (SalesTransHdr) iterSales.next();

			if (null == salesTransHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = salesTransHdr.getVoucherNumber();
			Date voucherDate = salesTransHdr.getVoucherDate();
			
			
			
			System.out.println("VOUCHER NUBMER =  " + voucherNumber);
			System.out.println("VOUCHER DATE =  " + voucherDate);
			
			

			String sourvceId = salesTransHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = salesTransHdr.getId();

			  
			 

			String companyMst = salesTransHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = salesTransHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					logger.info("Found previous records.. " + accountClass.getSourceVoucherNumber()
							+ accountClass.getBrachCode());

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							boolean result = saleIntegrationTally.SaveCashSale(accountClass.getCompanyMst(),
									salesTransHdr.getBranchCode(), "GODOWN", salesTransHdr.getSalesMode(),
									voucherNumber, strVoucherDate, salesTransHdr.getAccountHeads(), debitClassList,
									creditClassList,tallyServer);

						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
							System.out.println(e.toString());
							System.out.println("FAILED VOUCHER =  " + voucherNumber);
							System.out.println("VOUCHER NUBMER =  " + voucherNumber);
							System.out.println("VOUCHER DATE =  " + voucherDate);
							TallyFailedVoucherMst tallyFailedVoucherMst = new TallyFailedVoucherMst();
							tallyFailedVoucherMst.setVoucherNumber(voucherNumber);
							
							tallyFailedVoucherMstRepository.save(tallyFailedVoucherMst);
							
							
							
						}
					}

				}
			}

		}

		return "Sales Posted to Tally";
	}
	
	
	
	

	@GetMapping("{companymstid}/salestotallydirectdate/{startdate}")
	public String salesToTallyDirectStartDate(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "startdate") String startdate) {
 
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findAll();
		
		
		java.sql.Date date = ClientSystemSetting.StringToSqlDate(startdate, "yyy-MM-dd");
		
		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findByVoucherDateGreaterEqual(date);
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findByVoucherNumberLike("%1448%");
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findFailedVouchers();
		
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = salesTransHdrList.iterator();

		while (iterSales.hasNext()) {
			SalesTransHdr salesTransHdr = (SalesTransHdr) iterSales.next();

			if (null == salesTransHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = salesTransHdr.getVoucherNumber();
			Date voucherDate = salesTransHdr.getVoucherDate();
			
			
			
			System.out.println("VOUCHER NUBMER =  " + voucherNumber);
			System.out.println("VOUCHER DATE =  " + voucherDate);
			
			

			String sourvceId = salesTransHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = salesTransHdr.getId();

			  
			 

			String companyMst = salesTransHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = salesTransHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					logger.info("Found previous records.. " + accountClass.getSourceVoucherNumber()
							+ accountClass.getBrachCode());

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							boolean result = saleIntegrationTally.SaveCashSale(accountClass.getCompanyMst(),
									salesTransHdr.getBranchCode(), "GODOWN", salesTransHdr.getSalesMode(),
									voucherNumber, strVoucherDate, salesTransHdr.getAccountHeads(), debitClassList,
									creditClassList,tallyServer);

						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
							System.out.println(e.toString());
							System.out.println("FAILED VOUCHER =  " + voucherNumber);
							System.out.println("VOUCHER NUBMER =  " + voucherNumber);
							System.out.println("VOUCHER DATE =  " + voucherDate);
							TallyFailedVoucherMst tallyFailedVoucherMst = new TallyFailedVoucherMst();
							tallyFailedVoucherMst.setVoucherNumber(voucherNumber);
							
							tallyFailedVoucherMstRepository.save(tallyFailedVoucherMst);
							
							
							
						}
					}

				}
			}

		}

		return "Sales Posted to Tally";
	}
	
	
	
	
	
	

	@GetMapping("{companymstid}/monthlysalestotallydirect")
	public String monthlySalesToTallyDirect(@PathVariable(value = "companymstid") String companymstid) {

		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findAll();
		
		
		java.sql.Date date = ClientSystemSetting.StringToSqlDate("2020-03-01", "yyy-MM-dd");
		
		List<MonthlySalesTransHdr> monthlySalesTransHdrList = monthlySalesTransHdrRepository.findAll();
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findByVoucherNumberLike("%2019-2020RT002209%");
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findFailedVouchers();
		
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = monthlySalesTransHdrList.iterator();

		while (iterSales.hasNext()) {
			MonthlySalesTransHdr salesTransHdr = (MonthlySalesTransHdr) iterSales.next();

			if (null == salesTransHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = salesTransHdr.getVoucherNumber();
			Date voucherDate = salesTransHdr.getVoucherDate();
			
			
			
			System.out.println("VOUCHER NUBMER =  " + voucherNumber);
			System.out.println("VOUCHER DATE =  " + voucherDate);
			
			

			String sourvceId = salesTransHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = salesTransHdr.getId();

			  
			 

			String companyMst = salesTransHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = salesTransHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					logger.info("Found previous records.. " + accountClass.getSourceVoucherNumber()
							+ accountClass.getBrachCode());

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							boolean result = saleIntegrationTally.SaveCashSale(accountClass.getCompanyMst(),
									salesTransHdr.getBranchCode(), "GODOWN", salesTransHdr.getSalesMode(),
									voucherNumber, strVoucherDate, salesTransHdr.getAccountHeads(), debitClassList,
									creditClassList,tallyServer);

						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
							System.out.println(e.toString());
							System.out.println("FAILED VOUCHER =  " + voucherNumber);
							System.out.println("VOUCHER NUBMER =  " + voucherNumber);
							System.out.println("VOUCHER DATE =  " + voucherDate);
							TallyFailedVoucherMst tallyFailedVoucherMst = new TallyFailedVoucherMst();
							tallyFailedVoucherMst.setVoucherNumber(voucherNumber);
							
							tallyFailedVoucherMstRepository.save(tallyFailedVoucherMst);
							
							
							
						}
					}

				}
			}

		}

		return "Sales Posted to Tally";
	}
	
	
	
	
	
	

	@GetMapping("{companymstid}/monthlyjournaltotallydirect")
	public String monthlyJournalToTallyDirect(@PathVariable(value = "companymstid") String companymstid) {

		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findAll();
		
		
		//java.sql.Date date = ClientSystemSetting.StringToSqlDate("2020-03-01", "yyy-MM-dd");
		
		List<MonthlyJournalHdr> monthlyJournalHdrList = monthlyJournalHdrRepository.findAll();
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findByVoucherNumberLike("%2019-2020RT002209%");
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findFailedVouchers();
		
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = monthlyJournalHdrList.iterator();

		while (iterSales.hasNext()) {
			MonthlyJournalHdr journalTransHdr = (MonthlyJournalHdr) iterSales.next();

			if (null == journalTransHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = journalTransHdr.getVoucherNumber();
			Date voucherDate = journalTransHdr.getVoucherDate();
			
			
			
			System.out.println("VOUCHER NUBMER =  " + voucherNumber);
			System.out.println("VOUCHER DATE =  " + voucherDate);
			
			

			String sourvceId = journalTransHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = journalTransHdr.getId();

			  
			 

			String companyMst = journalTransHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = journalTransHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					logger.info("Found previous records.. " + accountClass.getSourceVoucherNumber()
							+ accountClass.getBrachCode());

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							boolean result = journalIntegrationTally.SaveJournal(accountClass.getCompanyMst(),
									journalTransHdr.getBranchCode(), "GODOWN", "JNL",
									voucherNumber, strVoucherDate,  debitClassList,
									creditClassList,tallyServer);

						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
							System.out.println(e.toString());
							System.out.println("FAILED VOUCHER =  " + voucherNumber);
							System.out.println("VOUCHER NUBMER =  " + voucherNumber);
							System.out.println("VOUCHER DATE =  " + voucherDate);
							TallyFailedVoucherMst tallyFailedVoucherMst = new TallyFailedVoucherMst();
							tallyFailedVoucherMst.setVoucherNumber(voucherNumber);
							
							tallyFailedVoucherMstRepository.save(tallyFailedVoucherMst);
							
							
							
						}
					}

				}
			}

		}

		return "Sales Posted to Tally";
	}
	
	
	

	@GetMapping("{companymstid}/journaltotallydirect")
	public String JournalToTallyDirect(@PathVariable(value = "companymstid") String companymstid) {

		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findAll();
		
		
		//java.sql.Date date = ClientSystemSetting.StringToSqlDate("2020-03-01", "yyy-MM-dd");
		
		List<JournalHdr> journalHdrList = journalHdrRepository.findAll();
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findByVoucherNumberLike("%2019-2020RT002209%");
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findFailedVouchers();
		
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = journalHdrList.iterator();

		while (iterSales.hasNext()) {
			JournalHdr journalTransHdr = (JournalHdr) iterSales.next();

			if (null == journalTransHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = journalTransHdr.getVoucherNumber();
			Date voucherDate = journalTransHdr.getVoucherDate();
			
			
			
			System.out.println("VOUCHER NUBMER =  " + voucherNumber);
			System.out.println("VOUCHER DATE =  " + voucherDate);
			
			

			String sourvceId = journalTransHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = journalTransHdr.getId();

			  
			 

			String companyMst = journalTransHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = journalTransHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					logger.info("Found previous records.. " + accountClass.getSourceVoucherNumber()
							+ accountClass.getBrachCode());

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							boolean result = journalIntegrationTally.SaveJournal(accountClass.getCompanyMst(),
									journalTransHdr.getBranchCode(), "GODOWN", "JNL",
									voucherNumber, strVoucherDate,  debitClassList,
									creditClassList,tallyServer);

						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
							System.out.println(e.toString());
							System.out.println("FAILED VOUCHER =  " + voucherNumber);
							System.out.println("VOUCHER NUBMER =  " + voucherNumber);
							System.out.println("VOUCHER DATE =  " + voucherDate);
							TallyFailedVoucherMst tallyFailedVoucherMst = new TallyFailedVoucherMst();
							tallyFailedVoucherMst.setVoucherNumber(voucherNumber);
							
							tallyFailedVoucherMstRepository.save(tallyFailedVoucherMst);
							
							
							
						}
					}

				}
			}

		}

		return "Sales Posted to Tally";
	}
	
	
	
	
	
	
	
	
	@GetMapping("{companymstid}/purchasetotallydirect")
	public String purchaseToTallyDirect(@PathVariable(value = "companymstid") String companymstid) {

		List<PurchaseHdr> salesTransHdrList = purchaseHdrRepository.findAll();
		
		
		//List<PurchaseHdr> salesTransHdrList = purchaseHdrRepository.findByVoucherNumberLike("%190%");
		
		
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = salesTransHdrList.iterator();

		while (iterSales.hasNext()) {
			PurchaseHdr salesTransHdr = (PurchaseHdr) iterSales.next();

			if (null == salesTransHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = salesTransHdr.getVoucherNumber();
			Date voucherDate = salesTransHdr.getVoucherDate();

			String sourvceId = salesTransHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = salesTransHdr.getId();

			  

			String companyMst = salesTransHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = salesTransHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					logger.info("Found previous records.. Deleting" + accountClass.getSourceVoucherNumber()
							+ accountClass.getBrachCode());

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							boolean result = purchaseIntegrationTally.SavePurchase(salesTransHdr.getCompanyMst(),
									tallyServer, salesTransHdr.getBranchCode(), voucherNumber, strVoucherDate, 
									salesTransHdr.getSupplierInvDate(), salesTransHdr.getSupplierInvNo(), 
									salesTransHdr.getSupplierId(), debitClassList, creditClassList,
									"PURCHASE");
									
									 
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
						}
					}

				}
			}

		}

		return "purchase Posted to Tally";
	}

	
	
	@GetMapping("{companymstid}/journaltotallydirectdate/{startdate}")
	public String JournalToTallyDirectDate(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "startdate") String startdate) {
java.sql.Date date = ClientSystemSetting.StringToSqlDate(startdate, "yyy-MM-dd");
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findAll();
		
		
		//java.sql.Date date = ClientSystemSetting.StringToSqlDate("2020-03-01", "yyy-MM-dd");
		
		List<JournalHdr> journalHdrList = journalHdrRepository.journalVoucherGreateAndEquel(date);
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findByVoucherNumberLike("%2019-2020RT002209%");
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findFailedVouchers();
		
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = journalHdrList.iterator();

		while (iterSales.hasNext()) {
			JournalHdr journalTransHdr = (JournalHdr) iterSales.next();

			if (null == journalTransHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = journalTransHdr.getVoucherNumber();
			Date voucherDate = journalTransHdr.getVoucherDate();
			
			
			
			System.out.println("VOUCHER NUBMER =  " + voucherNumber);
			System.out.println("VOUCHER DATE =  " + voucherDate);
			
			

			String sourvceId = journalTransHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = journalTransHdr.getId();

			  
			 

			String companyMst = journalTransHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = journalTransHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					logger.info("Found previous records.. " + accountClass.getSourceVoucherNumber()
							+ accountClass.getBrachCode());

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							boolean result = journalIntegrationTally.SaveJournal(accountClass.getCompanyMst(),
									journalTransHdr.getBranchCode(), "GODOWN", "JNL",
									voucherNumber, strVoucherDate,  debitClassList,
									creditClassList,tallyServer);

						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
							System.out.println(e.toString());
							System.out.println("FAILED VOUCHER =  " + voucherNumber);
							System.out.println("VOUCHER NUBMER =  " + voucherNumber);
							System.out.println("VOUCHER DATE =  " + voucherDate);
							TallyFailedVoucherMst tallyFailedVoucherMst = new TallyFailedVoucherMst();
							tallyFailedVoucherMst.setVoucherNumber(voucherNumber);
							
							tallyFailedVoucherMstRepository.save(tallyFailedVoucherMst);
							
							
							
						}
					}

				}
			}

		}

		return "Sales Posted to Tally";
	}
	
	
	
	
	
	@GetMapping("{companymstid}/purchasetotallydirectdate/{startdate}")
	public String purchaseToTallyDirectWithDate(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "startdate") String startdate) {

		java.sql.Date date = ClientSystemSetting.StringToSqlDate(startdate, "yyy-MM-dd");
		List<PurchaseHdr> salesTransHdrList = purchaseHdrRepository.purchaseVoucherGreateAndEquel(date);
		
		
		//List<PurchaseHdr> salesTransHdrList = purchaseHdrRepository.findByVoucherNumberLike("%190%");
		
		
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = salesTransHdrList.iterator();

		while (iterSales.hasNext()) {
			PurchaseHdr salesTransHdr = (PurchaseHdr) iterSales.next();

			if (null == salesTransHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = salesTransHdr.getVoucherNumber();
			Date voucherDate = salesTransHdr.getVoucherDate();

			String sourvceId = salesTransHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = salesTransHdr.getId();

			  

			String companyMst = salesTransHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = salesTransHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					logger.info("Found previous records.. Deleting" + accountClass.getSourceVoucherNumber()
							+ accountClass.getBrachCode());

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							boolean result = purchaseIntegrationTally.SavePurchase(salesTransHdr.getCompanyMst(),
									tallyServer, salesTransHdr.getBranchCode(), voucherNumber, strVoucherDate, 
									salesTransHdr.getSupplierInvDate(), salesTransHdr.getSupplierInvNo(), 
									salesTransHdr.getSupplierId(), debitClassList, creditClassList,
									"PURCHASE");
									
									 
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
						}
					}

				}
			}

		}

		return "purchase Posted to Tally";
	}

	

	@GetMapping("{companymstid}/receiptstally/{startdate}")
	public String receiptsToTallyDirect(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "startdate") String startdate) {
		
		java.sql.Date date = ClientSystemSetting.StringToSqlDate(startdate, "yyy-MM-dd");
		List<ReceiptHdr> receiptHdrList = receiptHdrRepository.receiptVoucherGreateAndEquel(date);
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = receiptHdrList.iterator();

		while (iterSales.hasNext()) {
			ReceiptHdr receiptHdr = (ReceiptHdr) iterSales.next();

			if (null == receiptHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = receiptHdr.getVoucherNumber();
			Date voucherDate = receiptHdr.getVoucherDate();

			String sourvceId = receiptHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = receiptHdr.getId();

			 
			 

			String companyMst = receiptHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = receiptHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					logger.info("Found previous records.. Deleting" + accountClass.getSourceVoucherNumber()
							+ accountClass.getBrachCode());

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							 
							
							
							
							boolean result =  cReceiptsToTally.SaveReceipts(accountClass.getCompanyMst(),
									 branchMst, "RECEIPT",
									 branchMst, 
										  voucherNumber,   strVoucherDate,
										  debitClassList,
										  creditClassList,tallyServer);
										
										
										
						} catch (    TransformerException | IOException | ParserConfigurationException | FactoryConfigurationError e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
						}
					}

				}
			}

		}

		return "receiptstotallydirect  Posted to Tally";
	}
	
	@GetMapping("{companymstid}/paymentstally/{startdate}")
	public String paymentsToTallyDirect(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "startdate") String startdate) {
		
		java.sql.Date date = ClientSystemSetting.StringToSqlDate(startdate, "yyy-MM-dd");
		List<PaymentHdr> paymentHdrList = paymentHdrRepository.paymentVoucherGreateAndEquel(date);
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = paymentHdrList.iterator();

		while (iterSales.hasNext()) {
			PaymentHdr paymentHdr = (PaymentHdr) iterSales.next();

			if (null == paymentHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = paymentHdr.getVoucherNumber();
			Date voucherDate = paymentHdr.getVoucherDate();

			String sourvceId = paymentHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = paymentHdr.getId();

			 
			 

			String companyMst = paymentHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = paymentHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					logger.info("Found previous records.. Deleting" + accountClass.getSourceVoucherNumber()
							+ accountClass.getBrachCode());

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							 
							
							

							boolean result =  cPaymentsToTally.SavePayments
									(accountClass.getCompanyMst(),
									 branchMst, "PAYMENTS",
									 branchMst, 
										  voucherNumber,   strVoucherDate,
										  debitClassList,
										  creditClassList,tallyServer);
										
							
							/*boolean result =  cPaymentsToTally..(accountClass.getCompanyMst(),
									 branchMst, "RECEIPT",
									 branchMst, 
										  voucherNumber,   strVoucherDate,
										  debitClassList,
										  creditClassList,tallyServer);
										
										*/
										
						} catch (    TransformerException | IOException | ParserConfigurationException | FactoryConfigurationError e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
						}
					}

				}
			}

		}

		return "receiptstotallydirect  Posted to Tally";
	}

	/*
	 * @GetMapping("{companymstid}/salestotally") public String
	 * salesToTally(@PathVariable(value = "companymstid") String companymstid) { //
	 * Find list of sales trans hdr // create job for each sales.
	 * 
	 * List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findAll(); //
	 * List<SalesTransHdr> salesTransHdrList = //
	 * salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
	 * Iterator iter = salesTransHdrList.iterator();
	 * 
	 * while (iter.hasNext()) { SalesTransHdr salesTransHdr = (SalesTransHdr)
	 * iter.next();
	 * 
	 * if (null == salesTransHdr.getVoucherNumber()) { continue; } LmsQueueMst
	 * lmsQueueMst = new LmsQueueMst();
	 * 
	 * lmsQueueMst.setCompanyMst(salesTransHdr.getCompanyMst());
	 * lmsQueueMst.setVoucherDate(salesTransHdr.getVoucherDate());
	 * lmsQueueMst.setVoucherNumber(salesTransHdr.getVoucherNumber());
	 * lmsQueueMst.setVoucherType("SALES"); lmsQueueMst.setPostedToServer("NO");
	 * 
	 * String jobClass =
	 * "com.maple.restserver.tally.integration.task.SaleIntegrationTally";
	 * 
	 * lmsQueueMst.setJobClass(jobClass); lmsQueueMst.setCronJob(false);
	 * lmsQueueMst.setJobName(salesTransHdr.getVoucherNumber());
	 * lmsQueueMst.setJobGroup("SALES"); lmsQueueMst.setRepeatTime(60000L);
	 * lmsQueueMst.setSourceObjectId(salesTransHdr.getId()); lmsQueueMst =
	 * lmsQueueMstRepository.save(lmsQueueMst);
	 * 
	 * //break;
	 * 
	 * }
	 * 
	 * try { schedulerRepository.deleteAll(); //
	 * logger.info("Schedule all new scheduler jobs at app startup - complete" + //
	 * voucherNumber); } catch (Exception ex) { //
	 * logger.error("Schedule all new scheduler jobs at app startup - error", ex); }
	 * 
	 * try { schedulerService.startAllSchedulers(); //
	 * logger.info("Schedule all new scheduler jobs at app startup - complete" + //
	 * voucherNumber); } catch (Exception ex) { //
	 * logger.error("Schedule all new scheduler jobs at app startup - error", ex); }
	 * 
	 * Scheduler scheduler = schedulerFactoryBean.getScheduler();
	 * 
	 * try { JobDetail jobDetail = JobBuilder .newJob((Class<? extends
	 * QuartzJobBean>) Class
	 * .forName("com.maple.restserver.tally.integration.task.SaleIntegrationTally"))
	 * .withIdentity("SALESTOTALLY", "TALLY").build(); if
	 * (!scheduler.checkExists(jobDetail.getKey())) { Trigger trigger; jobDetail =
	 * scheduleCreator.createJob( (Class<? extends QuartzJobBean>) Class
	 * .forName("com.maple.restserver.tally.integration.task.SaleIntegrationTally"),
	 * false, context, "SALESTOTALLY", "TALLY");
	 * 
	 * trigger = scheduleCreator.createSimpleTrigger("SALESTOTALLY", new Date(),
	 * 6000L, SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
	 * 
	 * scheduler.scheduleJob(jobDetail, trigger);
	 * 
	 * } } catch (ClassNotFoundException e) { System.out.print(e.toString()); }
	 * catch (SchedulerException e) { System.out.print(e.toString()); }
	 * 
	 * return "Sales Posted to Tally"; };
	 */

	@GetMapping("{companymstid}/reprocesssalesaccounting")
	public String rreprocessSalesAccounting(@PathVariable(value = "companymstid") String companymstid) {
		// Find list of sales trans hdr
		// create job for each sales.

		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findAll();
		Iterator iter = salesTransHdrList.iterator();
		int record = 0;
		while (iter.hasNext()) {
			SalesTransHdr salesTransHdr = (SalesTransHdr) iter.next();

			if (null == salesTransHdr.getVoucherNumber()) {
				continue;
			}
			record++;
			// Correct Sales Hdr.

			/*
			 * SalesReceipts aSalesReceipts = new SalesReceipts();
			 * aSalesReceipts.setAccountId(salesTransHdr.getCustomerId());
			 * aSalesReceipts.setBranchCode(salesTransHdr.getBranchCode());
			 * aSalesReceipts.setCompanyMst(salesTransHdr.getCompanyMst());
			 * aSalesReceipts.setReceiptAmount(salesTransHdr.getInvoiceAmount());
			 * aSalesReceipts.setReceiptMode("CREDIT");
			 * aSalesReceipts.setSalesTransHdr(salesTransHdr);
			 * aSalesReceipts.setUserId(salesTransHdr.getUserId());
			 * aSalesReceipts.setVoucherNumber(salesTransHdr.getVoucherNumber());
			 * 
			 * aSalesReceipts = salesReceiptsRepo.saveAndFlush(aSalesReceipts);
			 */

			try {
				salesAccounting.execute(salesTransHdr.getVoucherNumber(), salesTransHdr.getVoucherDate(),
						salesTransHdr.getId(), salesTransHdr.getCompanyMst());
			} catch (PartialAccountingException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			}

			System.out.println("Record " + record);

		}

		return "Acounting rep[rocessd";
	};

	
	@GetMapping("{companymstid}/reprocessreceiptsaccounting")
	public String reporocessReceiptAccounting(@PathVariable(value = "companymstid") String companymstid) {
		// Find list of sales trans hdr
		// create job for each sales.

		 
		
		List<ReceiptHdr> receiptHdrList = receiptHdrRepository.findAll();
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = receiptHdrList.iterator();
		int record = 0;
		while (iterSales.hasNext()) {
			ReceiptHdr receiptHdr = (ReceiptHdr) iterSales.next();
			
			
	 

			if (null == receiptHdr.getVoucherNumber()) {
				continue;
			}
			record++;
			

			try {
				receiptAccounting.execute(receiptHdr.getVoucherNumber(), receiptHdr.getVoucherDate(),
						receiptHdr.getId(), receiptHdr.getCompanyMst());
			} catch (PartialAccountingException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			}

			System.out.println("Record " + record);

		}

		return "Acounting reprocessd";
	};
	
	@GetMapping("{companymstid}/tallyrsource/tallymstbydateandvouchertype/{vouchertype}")
	public List<LmsQueueTallyMst> LmsQueueTallyMstByDateAndType(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "vouchertype") String vouchertype,
			@PathParam("date") String date) {
		
		Date udate = ClientSystemSetting.StringToUtilDate(date, "yyyy-MM-dd");
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		
		return lmsQueueTallyMstRepository.findByVoucherDateAndVoucherTypeAndCompanyMstAndPostedToTally(udate,vouchertype,companyOpt.get(),"NO");
		
	};
	
	
	

	@GetMapping("{companymstid}/salestotallybyvouchernoanddate/{voucherno}")
	public String salesToTallyByVoucherNoAndDate(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "voucherno") String voucherno,
			@RequestParam ("date") String sdate ) {

		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findAll();
		
		
		java.sql.Date date = ClientSystemSetting.StringToSqlDate(sdate, "yyy-MM-dd");
		
		List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findByVoucherNumberAndVoucherDate(voucherno, date);
		//List<SalesTransHdr> salesTransHdrList = salesTransHdrRepository.findByVoucherNumberLike("%2587%");
		
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		
		
		System.out.println("voucherno : " + voucherno);
		System.out.println("sdate : " + sdate);
		
		
		Iterator iterSales = salesTransHdrList.iterator();

		while (iterSales.hasNext()) {
			
			System.out.println("Found Voucher : " + voucherno);
			 
			SalesTransHdr salesTransHdr = (SalesTransHdr) iterSales.next();

			if (null == salesTransHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = salesTransHdr.getVoucherNumber();
			Date voucherDate = salesTransHdr.getVoucherDate();

			String sourvceId = salesTransHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = salesTransHdr.getId();

			  			 

			String companyMst = salesTransHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = salesTransHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					logger.info("Found previous records.. " + accountClass.getSourceVoucherNumber()
							+ accountClass.getBrachCode());

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							
							System.out.println("Calling Tally : " + voucherno);
							boolean result = saleIntegrationTally.SaveCashSale(accountClass.getCompanyMst(),
									salesTransHdr.getBranchCode(), "GODOWN", salesTransHdr.getSalesMode(),
									voucherNumber, strVoucherDate, salesTransHdr.getAccountHeads(), debitClassList,
									creditClassList,tallyServer);

						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
							System.out.println(e.toString());
						}
					}

				}
			}

		}

		return "Sales Posted to Tally";
	}
	

	
	@GetMapping("{companymstid}/purchasetotallybyvouchernoanddate/{voucherno}")
	public String purchaseToTallyByVoucherNoAndDate(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "voucherno") String voucherno,
			@RequestParam ("date") String sdate ) {
		
		
		Date date = ClientSystemSetting.StringToUtilDate(sdate, "yyy-MM-dd");
		
		List<PurchaseHdr> salesTransHdrList = purchaseHdrRepository.findByVoucherNumberAndVoucherDate(voucherno, date);
		
		
		//List<PurchaseHdr> salesTransHdrList = purchaseHdrRepository.findByVoucherNumberLike("%122%");
		
		
		// List<SalesTransHdr> salesTransHdrList =
		// salesTransHdrRepository.findById("f62ff71c-b0ee-4e9c-8543-cbc9a738c90b");
		Iterator iterSales = salesTransHdrList.iterator();

		while (iterSales.hasNext()) {
			PurchaseHdr salesTransHdr = (PurchaseHdr) iterSales.next();

			if (null == salesTransHdr.getVoucherNumber()) {
				continue;
			}

			BigDecimal zero = new BigDecimal("0");

			String voucherNumber = salesTransHdr.getVoucherNumber();
			Date voucherDate = salesTransHdr.getVoucherDate();

			String sourvceId = salesTransHdr.getId();
			// String lmsqueueId = jobInfo.getId();

			String id = salesTransHdr.getId();

			  

			String companyMst = salesTransHdr.getCompanyMst().getId();
			// (String) context.getJobDetail().getJobDataMap().get("companyid");

			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			String branchMst = salesTransHdr.getBranchCode();

			String goDownName = branchMst;

			String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

			System.out.println("Company ID " + companyMst);

			List<AccountClass> accountClassPreviousList = accountClassRepo
					.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

			if (accountClassPreviousList.size() > 0) {
				logger.info("Found previous records.. Deleting");

				Iterator iter = accountClassPreviousList.iterator();
				while (iter.hasNext()) {
					AccountClass accountClass = (AccountClass) iter.next();
					logger.info("Found previous records.. Deleting" + accountClass.getSourceVoucherNumber()
							+ accountClass.getBrachCode());

					List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
					List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

					List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

					if (creditClassList.size() > 0) {

						try {
							boolean result = purchaseIntegrationTally.SavePurchase(salesTransHdr.getCompanyMst(),
									tallyServer, salesTransHdr.getBranchCode(), voucherNumber, strVoucherDate, 
									salesTransHdr.getSupplierInvDate(), salesTransHdr.getSupplierInvNo(), 
									salesTransHdr.getSupplierId(), debitClassList, creditClassList,
									"PURCHASE");
									
									 
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
						}
					}

				}
			}

		}

		return "purchase Posted to Tally";
	}

	

	@GetMapping("/{companymstid}/copytomonthlyjournal/{branchcode}")
	public void copyToMonthlyJournalTables(@PathVariable(value = "companymstid") String
			  companymstid,	@PathVariable(value = "branchcode") String branchcode,
			  @RequestParam("rdate")String rdate,@RequestParam("tdate")String tdate
			){
Optional<CompanyMst> companyMstOpt=companyMstRepo.findById(companymstid);
    	
    	CompanyMst companyMst=companyMstOpt.get();
    	Date fromDate = null,toDate = null;
    	 
    	java.util.Date fromdate = SystemSetting.StringToUtilDate(rdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
    	
    	monthlyJournalHdrService.copyToMonthlyJournalHdr(fromdate,todate,branchcode,companyMst);
    	
    }
	
	

	@GetMapping("/{companymstid}/credit/{branchcode}")
	public List<Object>  creditTables(@PathVariable(value = "companymstid") String
			  companymstid,	@PathVariable(value = "branchcode") String branchcode,
			  @RequestParam("rdate")String rdate,@RequestParam("tdate")String tdate
			){
       Optional<CompanyMst> companyMstOpt=companyMstRepo.findById(companymstid);
    	
    	CompanyMst companyMst=companyMstOpt.get();
    	Date fromDate = null,toDate = null;
    	 
    	java.util.Date fromdate = SystemSetting.StringToUtilDate(rdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
    	
		return	salesTransHdrRepository.sumOfCreditAmount(fromdate,todate);
//	return	monthlyJournalHdrServiceImpl.copyToMonthlyJournalHdr(fromdate,todate,branchcode,companyMst);
    	
    }
	
	@GetMapping("/{companymstid}/crdr/{branchcode}")
	public List<Double>  CrDr(@PathVariable(value = "companymstid") String
			  companymstid,	@PathVariable(value = "branchcode") String branchcode,
			  @RequestParam("rdate")String rdate,@RequestParam("tdate")String tdate
			){
       Optional<CompanyMst> companyMstOpt=companyMstRepo.findById(companymstid);
    	
    	CompanyMst companyMst=companyMstOpt.get();
    	Date fromDate = null,toDate = null;
    	 
    	java.util.Date fromdate = SystemSetting.StringToUtilDate(rdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
    	String voucherNumber="01/01/2018-13/07/2020";
		return	monthlyJournalDtlRepository.sumOfDebitAmountCrAmount(voucherNumber);
//	return	monthlyJournalHdrServiceImpl.copyToMonthlyJournalHdr(fromdate,todate,branchcode,companyMst);
    	
    }
	
	
	
    @Transactional
	@GetMapping("/{companymstid}/copytotables/{branchcode}")
	public void copyToTables(@PathVariable(value = "companymstid") String
			  companymstid,	@PathVariable(value = "branchcode") String branchcode,
			  @RequestParam("rdate")String rdate,@RequestParam("tdate")String tdate){
	
    	
    	
		  Calendar c = Calendar.getInstance();
		  
		  
		  int month = c.get(Calendar.MONTH); 
		  int year =c.get(Calendar.YEAR);
		  
		  int prevMonth = month; if(prevMonth==0) 
		  { 
			  prevMonth = 12; 
			  year=year-1; 
		  }
		  String prvMonth = Integer.toString(prevMonth); if(prvMonth.length()==1) 
		  {
		  prvMonth = "0"+prevMonth; 
		  }
		  
		  String date =Integer.toString(year)+"-"+prvMonth+"-01"; LocalDate
		  convertedDate = LocalDate.parse(date,
		  DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		  convertedDate =convertedDate.withDayOfMonth(convertedDate.getMonth().length(convertedDate.
		  isLeapYear())); 
		  Date fromdate = ClientSystemSetting.StringToUtilDate(date,"yyyy-MM-dd");
		  Date todate = ClientSystemSetting.localToUtilDate(convertedDate);
////		 
    ///	java.util.Date fromdate = SystemSetting.StringToUtilDate(rdate,"yyyy-MM-dd");
	//	java.util.Date todate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		/*
		 * 
		 * Calendar c = Calendar.getInstance();
		 * 
		 * 
		 * int month = c.get(Calendar.MONTH); int year =c.get(Calendar.YEAR);
		 * 
		 * int prevMonth = month; if(prevMonth==0) { prevMonth = 12; year=year-1; }
		 * String prvMonth = Integer.toString(prevMonth); if(prvMonth.length()==1) {
		 * prvMonth = "0"+prevMonth; }
		 * 
		 * String date =Integer.toString(year)+"-"+prvMonth+"-01"; LocalDate
		 * convertedDate = LocalDate.parse(date,
		 * DateTimeFormatter.ofPattern("yyyy-MM-dd")); convertedDate =
		 * convertedDate.withDayOfMonth(convertedDate.getMonth().length(convertedDate.
		 * isLeapYear())); Date fromdate = ClientSystemSetting.StringToUtilDate(date,
		 * "yyyy-MM-dd"); Date todate =
		 * ClientSystemSetting.localToUtilDate(convertedDate);
		 */
    	
		 monthlySalesTransHdrRepository.deleteMonthlySalesTransHdrInitially();
		 salesDtlTempRepository.deleteSalesDtlTempInitially();
         salesReceiptsTempRepository.deleteSalesReceiptsInitially();
		 monthlySalesTransHdrRepository.insertingIntoMonthlySalesTransHdr(fromdate,todate);
		 monthlySalesDtlRepository.insertingIntoMonthlySalesDtl(fromdate, todate);
		 monthlySalesReceiptsRepository.insertingIntoMonthlySalesReceipts(fromdate, todate);
		 tallyService.monthlySalesSummaryToTally(fromdate, todate);
		 salesDtlTempRepository.insertingIntoSalesDtlTem ( );
		 salesReceiptsTempRepository.insertingIntoSalesReceiptsTemp();
		 monthlySalesReceiptsRepository.deleteMonthlySalesReceipts();
		 monthlySalesDtlRepository.deleteMonthlySalesDtl();
		 salesDtlTempRepository.insertingIntoMonthlySalesDtlFromSalesDtlTemp();
		 salesReceiptsTempRepository. insertingIntoMonthlySalesReceiptsFromSalesReceiptsTemp();


    }
    
    
    //http://localhost:8181/MFP/salestotallydirectwithvouchernumberanddate?vdate=2020-11-23
    //http://localhost:8181/AMB/salestotallydirectwithvouchernumberanddate/MFP002494?vdate=2020-11-23
    
    //new url 
	@GetMapping("{companymstid}/salestotallydirectwithvouchernumberanddate/{voucherno}")
	public String salesToTallyDirectWithVoucherNoAndDate(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value="vdate")String vdate,
			@PathVariable(value = "voucherno")String voucherno) {

			
	
		java.sql.Date date = ClientSystemSetting.StringToSqlDate(vdate, "yyy-MM-dd");
		
		return tallyService.salesToTallyByVoucherNoAndVoucherDate(voucherno,date);
	}
	
	
	
	
	
}
