package com.maple.restserver.resource;

import java.util.List;
import java.util.Date;
import com.maple.maple.util.ClientSystemSetting;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.PurchaseOrderDtl;
import com.maple.restserver.entity.PurchaseOrderHdr;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.PurchaseOrderDtlRepository;
import com.maple.restserver.repository.PurchaseOrderHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
public class PurchaseOrderDtlResource {
private static final Logger logger = LoggerFactory.getLogger(PurchaseOrderDtlResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	
	
@Autowired
PurchaseOrderDtlRepository purchaseOrderDtlRepository;

@Autowired
CompanyMstRepository companyMstRepository;

@Autowired
PurchaseOrderHdrRepository purchaseOrderHdrRepository;


@Autowired
SaveAndPublishService saveAndPublishService;


@PostMapping("{companymstid}/purchaseorderdtls/{purchaseorderhdrid}")
public PurchaseOrderDtl createPurchaseOrderDtl(@PathVariable(value = "companymstid") String
		  companymstid,@PathVariable(value = "purchaseorderhdrid") String
		  purchaseorderhdrid,@Valid @RequestBody 
		PurchaseOrderDtl  purchaseOrderDtl)


{
	Optional<CompanyMst> companyMstOpt =companyMstRepository.findById(companymstid);
	Optional<PurchaseOrderHdr> purchaseOrderHdrOPt=purchaseOrderHdrRepository.findById(purchaseorderhdrid);	
	purchaseOrderDtl.setCompanyMst(companyMstOpt.get());
	purchaseOrderDtl.setPurchaseOrderHdr(purchaseOrderHdrOPt.get());
	//return purchaseOrderDtlRepository.saveAndFlush(purchaseOrderDtl);
	return saveAndPublishService.savePurchaseOrderDtl(purchaseOrderDtl,mybranch);
	
	
}


@DeleteMapping("{companymstid}/purchaseorderdtl/{purchaseorderdetailId}")
public void DeletePurchaseOrderDtl(@PathVariable (value="purchaseorderdetailId") String purchaseorderdetailId) {
	purchaseOrderDtlRepository.deleteById(purchaseorderdetailId);

}


@GetMapping("{companymstid}/purchaseorderhdr/{purchaseorderHdrId}/purchaseorderdtl")
public List<PurchaseOrderDtl> findAllPurchaseDtlByPurchaseHdrID(
		@PathVariable(value = "purchaseorderHdrId") String purchaseorderHdrId) {
	return purchaseOrderDtlRepository.findByPurchaseOrderHdrId(purchaseorderHdrId);

}

@GetMapping("{companymstid}/purchaseorderhdr/{voucherNumber}/purchaseorderdtlbyvoucher")
public List<PurchaseOrderDtl> findAllPurchaseDtlByVoucherNumber(
		@PathVariable(value = "voucherNumber") String voucherNumber,
		@PathVariable(value ="companymstid") String companymstid) {
	return purchaseOrderDtlRepository.findByPurchaseOrderHdrVoucherNumber(voucherNumber);

}


//version1.7


@GetMapping("{companymstid}/purchaseorderhdr/{voucherNumber}/purchasedtlbyvouchernoanddateandstatus")
public List<PurchaseOrderDtl> findAllPurchaseDtlByVoucherNumberAndDate(
		@PathVariable(value = "voucherNumber") String voucherNumber,
		@PathVariable(value ="companymstid") String companymstid,
		@RequestParam(value="voucherDate")String voucherDate) {
	Date vdate = ClientSystemSetting.StringToUtilDate(voucherDate, "yyyy-MM-dd");
	return purchaseOrderDtlRepository.findByVoucherDateVoucherNoAndStatus(vdate, voucherNumber);

}
//version1.7ends
//version 1.4
@GetMapping("{companymstid}/purchaseorderhdr/{dtlid}/purchaseorderdtlbyid")
public PurchaseOrderDtl findPurchaseDtlById(
		@PathVariable(value = "dtlid") String dtlid,
		@PathVariable(value ="companymstid") String companymstid) {
	return purchaseOrderDtlRepository.findById(dtlid).get();
}


@DeleteMapping("{companymstid}/purchaseorderdtl/deletepurchaseorder/{purchaseorderdetailId}")
public void DeletepurchaseDtl(@PathVariable (value="purchaseorderdetailId") String purchaseorderdetailId) {
	purchaseOrderDtlRepository.deleteById(purchaseorderdetailId);

}


//1.4 ends


@GetMapping("{companymstid}/purchaseorderhdr/{purchaseorderhdrId}/purchaseorderdtlsummary")
public SummarySalesDtl retrieveAllPurchaseOrderSummaryByPurchaseOrderHdrID(
		@PathVariable(value ="purchaseorderhdrId") String purchaseorderhdridId,
		@PathVariable(value ="companymstid") String companymstid
		
		) {
	List<Object> objList =  purchaseOrderDtlRepository.findSumPurchaseOrderDetail(purchaseorderhdridId);
	 
	Object[] objAray = (Object[]) objList.get(0);

	SummarySalesDtl summary = new SummarySalesDtl();
	summary.setTotalAmount((Double) objAray[0]);
	summary.setTotalQty((Double) objAray[1]);
	summary.setTotalTax((Double) objAray[2]);
	 
	return summary;

}
@PutMapping("{companymstid}/purchaseorderdtlupdate/{purordrDtl}")
public PurchaseOrderDtl PurchaseOrderDtlStatusUpdate(@PathVariable String purordrDtl, 
		@Valid @RequestBody PurchaseOrderDtl purchaseOrderDtlRequest)

{
			    Optional<PurchaseOrderDtl> purchaseOrderDtlOpt = purchaseOrderDtlRepository.findById(purchaseOrderDtlRequest.getId());
			    PurchaseOrderDtl purchaseOrderDtl = purchaseOrderDtlOpt.get();
			    purchaseOrderDtl.setStatus(purchaseOrderDtlRequest.getStatus());
			    purchaseOrderDtl.setReceivedQty(purchaseOrderDtlRequest.getReceivedQty());

			    //PurchaseOrderDtl saved = purchaseOrderDtlRepository.saveAndFlush(purchaseOrderDtl);
			    PurchaseOrderDtl saved =saveAndPublishService.savePurchaseOrderDtl(purchaseOrderDtl,mybranch);
				logger.info("PurchaseOrderDtl send to KafkaEvent: {}", saved);
	            return saved;

}

}

