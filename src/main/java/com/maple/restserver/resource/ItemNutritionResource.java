package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemNutrition;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemNutritionRepository;

@RestController
public class ItemNutritionResource {
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	
	@Autowired
	ItemNutritionRepository itemNutritionRepository;
	
	

	//save 
	@PostMapping("{companymstid}/itemnutritionresource/saveitemnutrition")
	public ItemNutrition createItemNutrition(
			@PathVariable(value="companymstid")	String commpanymstid,
			@Valid @RequestBody ItemNutrition itemNutrition)
	{
		CompanyMst companymst=CompanyMstRepository.findById(commpanymstid).get();
		itemNutrition.setCompanyMst(companymst);
		itemNutrition=itemNutritionRepository.save(itemNutrition);
		return itemNutrition;
	}
	
	
	// delete
	@DeleteMapping("{companymstid}/itemnutritionresource/deleteitemnutrition/{id}")
	public void DeleteItemNutrition(@PathVariable(value = "id") String Id) {
		itemNutritionRepository.deleteById(Id);
	}
	
	
	//showing all 
	@GetMapping("{companymstid}/itemnutritionresource/showallitemnutrition")
	public List<ItemNutrition> showallitemNutrition(
			
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = CompanyMstRepository.findById(companymstid);
		
		return itemNutritionRepository.findByCompanyMst(companyMstOpt.get());
	}
	
	
}
