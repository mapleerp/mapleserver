package com.maple.restserver.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemLocationMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.report.entity.ItemLocationReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemLocationMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.service.ItemLocationService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ItemLocationMstResource {
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	ItemLocationService itemLocationService;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	ItemLocationMstRepository itemLocationMstRepo;
	EventBus eventBus = EventBusFactory.getEventBus();

	@PostMapping("{companymstid}/itemlocationmstresource/saveitemlocationmst")
	public ItemLocationMst createItemLocationMst(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestBody ItemLocationMst itemLocationMstreq)
	{
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		itemLocationMstreq.setCompanyMst(companyMst);
		ItemLocationMst saved =  itemLocationMstRepo.save(itemLocationMstreq);
		
		
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", saved.getId());

		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("id", saved.getId());

		variables.put("companyid", saved.getCompanyMst());
		variables.put("branchcode", saved.getBranchCode());

		variables.put("REST", 1);
		
		
		variables.put("WF", "forwardItemLocationMst");
		
		
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardItemLocationMst");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		
		variables.put("lmsqid", lmsQueueMst.getId());
		
		eventBus.post(variables);
		return saved;
	}
			
	@DeleteMapping("{companymstid}/itemlocationmstresource/deleteitemlocationmst/{id}")
	public void deleteItemLocationMst(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value="id")String id)
	{
		itemLocationMstRepo.deleteById(id);
	}
	@GetMapping("{companymstid}/itemlocationmstresource/getitembylocation/{itemid}/{floor}/{shelf}/{rack}")
	public ItemLocationMst getItemByLocation(@PathVariable(value="itemid")String itemid,
			@PathVariable(value="floor")String floor,@PathVariable(value="shelf")String shelf,
			@PathVariable(value="rack")String rack)
	{
		return itemLocationMstRepo.findByItemIdAndFloorAndShelfAndRack(itemid, floor, shelf, rack);
	}
	@GetMapping("{companymstid}/itemlocationmstresource/getallitemlocation/{branchcode}")
	public List<ItemLocationMst> getAllItemLocation(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="branchcode")String branchcode)
	{
		return itemLocationMstRepo.findAll();
	}
	
	@GetMapping("{companymstid}/itemlocationmstresource/getfloordistinct/{branchcode}")
	public List<String> getDisctinctFloor(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="branchcode")String branchcode)
	{
		return itemLocationMstRepo.getDisctinctFloor();
	}
	
	@GetMapping("{companymstid}/itemlocationmstresource/getshelfdistinct/{branchcode}")
	public List<String> getDisctinctShelf(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="branchcode")String branchcode)
	{
		return itemLocationMstRepo.getDisctinctShelf();
	}
	
	@GetMapping("{companymstid}/itemlocationmstresource/getrackdistinct/{branchcode}")
	public List<String> getDisctinctRack(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="branchcode")String branchcode)
	{
		return itemLocationMstRepo.getDisctinctRack();
	}
	
	@GetMapping("{companymstid}/itemlocationmstresource/getlocationbyitemid/{itemid}")
	public List<ItemLocationMst> getLocationBYItemId(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="itemid")String itemid)
	{
		return itemLocationMstRepo.findByItemId(itemid);
	}
	@GetMapping("{companymstid}/itemlocationmstresource/getlocationbyfloor/{floor}")
	public List<ItemLocationMst> getLocationBYFloor(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="floor")String floor)
	{
		return itemLocationMstRepo.findByFloor(floor);
	}
	@GetMapping("{companymstid}/itemlocationmstresource/getlocationreportbyitemid/{itemid}")
	public List<ItemLocationReport> getLocationReportByItemId(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="itemid")String itemid)
	{
		return itemLocationService.getItemLocationByItemId(itemid);
	}
	
	@GetMapping("{companymstid}/itemlocationmstresource/getlocationreportbyfloor/{floor}")
	public List<ItemLocationReport> getLocationReportByFloor(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="floor")String floor)
	{
		return itemLocationService.getItemLocationByFloor(floor);
	}
	
	@GetMapping("{companymstid}/itemlocationmstresource/getlocationbyfloorandshelf/{floor}/{shelf}")
	public List<ItemLocationMst> getLocationByFloorAndShelf(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="floor")String floor,
			@PathVariable(value="shelf")String shelf)
	{
		return itemLocationMstRepo.findByFloorAndShelf(floor, shelf);
	}
	
	
	@GetMapping("{companymstid}/itemlocationmstresource/getlocationbyfloorandshelfandrack/{floor}/{shelf}/{rack}")
	public List<ItemLocationMst> getLocationByFloorAndShelfAndRack(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="floor")String floor,
			@PathVariable(value="shelf")String shelf,
			@PathVariable(value="rack")String rack)
	{
		return itemLocationMstRepo.findByFloorAndShelfAndRack(floor, shelf,rack);
	}
	
	@GetMapping("{companymstid}/itemlocationmstresource/getlocationreportbyfloorandshelf/{floor}/"
			+ "{shelf}")
	public List<ItemLocationReport> getLocationReportByFloorAndShelf(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="floor")String floor,
			@PathVariable(value="shelf")String shelf)
	{
		return itemLocationService.getItemLocationByFloorAndShelf(floor,shelf);
	}
	

	@GetMapping("{companymstid}/itemlocationmstresource/getlocationreportbyfloorandshelfandrack/{floor}/"
			+ "{shelf}/{rack}")
	public List<ItemLocationReport> getLocationReportByFloorAndShelfAndRack(@PathVariable(value="companymstid")String companymstid,
			@PathVariable(value="floor")String floor,
			@PathVariable(value="shelf")String shelf,
			@PathVariable(value="rack")String rack)
	{
		return itemLocationService.getItemLocationByFloorAndShelfAndRack(floor,shelf,rack);
	}
}
