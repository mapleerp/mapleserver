package com.maple.restserver.resource;



import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.MenuWindowMst;
import com.maple.restserver.repository.MenuWindowMstRepository;

@RestController
@Transactional
public class MenuWindowMstResource {

	@Autowired
	MenuWindowMstRepository menuWindowMstRepo;
	
	@GetMapping("{companymstid}/menuwindowmst/getmenuwindowbymenu/{menuname}")
	public MenuWindowMst menuWindowMstByMenuName(@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "menuname")String menuname)
	{
		return menuWindowMstRepo.findByMenuName(menuname);
	}
}
