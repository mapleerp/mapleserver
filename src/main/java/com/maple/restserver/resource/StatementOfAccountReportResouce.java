package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.SummaryReportSalesOrder;
import com.maple.restserver.entity.SummaryStatementOfAccountReport;
import com.maple.restserver.repository.StatementOfAccountReportRepository;
import com.maple.restserver.service.StatementOfAccountReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class StatementOfAccountReportResouce {

	@Autowired
	private StatementOfAccountReportRepository statmentofAccRepo;
	
	@Autowired
	private StatementOfAccountReportService statementOfAccountReportService;
	
	
		 
	
	
	@GetMapping("{companymstid}/statementofaccountreport/{acconuntId}")
	public List<SummaryStatementOfAccountReport> retrieveAllStatementOfAccount(@RequestParam("startDate") String startDate,
			@RequestParam("endDate") String endDate,
			@PathVariable(value = "companymstid") String  companymstid,@PathVariable(value = "acconuntId")String acconuntId)
	{
		java.util.Date eDate = SystemSetting.StringToUtilDate(endDate,"yyyy-MM-dd");
		java.util.Date SDate = SystemSetting.StringToUtilDate(startDate,"yyyy-MM-dd");

		 return statementOfAccountReportService.StatementOfAccountReport(companymstid,SDate,eDate,acconuntId);
		
		  
		 
	}	
}
