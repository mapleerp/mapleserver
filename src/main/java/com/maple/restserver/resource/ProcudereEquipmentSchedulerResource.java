package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.maple.restserver.his.entity.ProcudereEquipmentScheduler;
import com.maple.restserver.repository.ProcudereEquipmentSchedulerRepository;

@RestController
@Transactional
public class ProcudereEquipmentSchedulerResource {
	
	
	@Autowired
	private ProcudereEquipmentSchedulerRepository procudereEquipmentSchedulerRepository;
	
	@GetMapping("{companymstid}/procudereequipmentscheduler")
	public List<ProcudereEquipmentScheduler> retrieveAllProcudereEquipmentScheduler(){
		return procudereEquipmentSchedulerRepository.findAll();
		
		
	}
	
	@PostMapping("{companymstid}/procudereequipmentscheduler")
	public ResponseEntity<Object> createProcudereEquipmentScheduler(@Valid @RequestBody 
			ProcudereEquipmentScheduler procudereEquipmentScheduler)
	{
		ProcudereEquipmentScheduler saved = procudereEquipmentSchedulerRepository.saveAndFlush(procudereEquipmentScheduler);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/(id)").buildAndExpand(saved.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}


}
