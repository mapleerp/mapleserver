package com.maple.restserver.resource;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DashBoardConfigMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DashBoardConfigMstRepository;

@RestController
@Transactional
public class DashBoardConfigMstResource {
	
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	
	@Autowired
	DashBoardConfigMstRepository dashBoardConfigMstRepository;

	@PostMapping("/{companymstid}/dashboardconfigmstresource/savedashboardconfiguration")
	public DashBoardConfigMst saveDashBoardConfigurationMst(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody DashBoardConfigMst dashBoardConfigMst)
	{
		CompanyMst companymst=CompanyMstRepository.findById(companymstid).get();
		dashBoardConfigMst.setCompanyMst(companymst);
		return dashBoardConfigMstRepository.save(dashBoardConfigMst);	
	}
	
	@GetMapping("{companymstid}/dashboardconfigmstresource/getalldashboardconfiguration")
	public List<DashBoardConfigMst> retrieveAllDashBoardConfiguration(
			@PathVariable(value = "companymstid") String companymstid) {
		
		return dashBoardConfigMstRepository.findByCompanyMstId(companymstid);

	}
}
