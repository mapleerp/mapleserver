package com.maple.restserver.resource;

import java.net.URI;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.ReorderReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.IntentHdrRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.service.IntentService;
import com.maple.restserver.service.IntentServiceImpl;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.testService.TestService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class IntentHdrResource {
	
	private static final Logger logger = LoggerFactory.getLogger(IntentHdrResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	
	 @Value("${serverorclient}")
		private String serverorclient;

	
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	// @Autowired
	  //private RuntimeService runtimeService;
	 @Autowired
		IntentService intentService;
	 
	 EventBus eventBus = EventBusFactory.getEventBus();
	 
	 
	@Autowired
	private IntentHdrRepository intentHdrRepo;
	
	@Autowired
	IntentServiceImpl intentServiceImpl;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
//	@Autowired
//	TestService testClass;
	
	@GetMapping("{companymstid}/intenthdrs")
	public List<IntentHdr> retrieveAllPurchaseHdr(@PathVariable(value = "companymstid") String
			  companymstid){
		
		return intentHdrRepo.findByCompanyMstId(companymstid);
		
		
	}
	
	@GetMapping("{companymstid}/intenthdrs/intenthdrbyid/{id}")
	public IntentHdr retrieveIntentHdrById(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "id") String
			  id){
		
		return intentHdrRepo.findByIdAndCompanyMstId(id,companymstid);
		
		
	}
	
	

	@PostMapping("{companymstid}/intenthdr")
	public IntentHdr createIntentHdr(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			IntentHdr intentHdr)
	{
		
return companyMstRepo.findById(companymstid).map(companyMst-> {
	intentHdr.setCompanyMst(companyMst);
//	return intentHdrRepo.saveAndFlush(intentHdr);
	return saveAndPublishService.saveIntentHdr(intentHdr, mybranch, intentHdr.getToBranch());
		
}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
		  companymstid + " not found")); }
		
	
	


	@PutMapping("{companymstid}/intenthdr/{intenthdrid}")
	public IntentHdr intentFinalSave(@PathVariable String intenthdrid, 
			@Valid @RequestBody IntentHdr intentHdrRequest)
	{

				
				return intentHdrRepo.findById(intenthdrid).map(intent -> {


					intent.setVoucherNumber(intentHdrRequest.getVoucherNumber()); 
					
					
					
					//IntentHdr saved = intentHdrRepo.saveAndFlush(intent);
					IntentHdr saved =saveAndPublishService.saveIntentHdr(intent, mybranch, intent.getToBranch());
					logger.info("IntentHdr send to KafkaEvent: {}", saved);
					//------------for test only
//					TestService testService = new TestService();
//					testService.SaveIntentInDtls(saved);
					
					
					  Map<String, Object> variables = new HashMap<String, Object>();
					  variables.put("companyid", saved.getCompanyMst());
					  variables.put("voucherNumber", saved.getVoucherNumber());
					  variables.put("voucherDate", saved.getVoucherDate());
					  variables.put("id",saved.getId()); variables.put("inet", 0);
						variables.put("branchcode", saved.getBranchCode());

						if(serverorclient.equalsIgnoreCase("REST")) {
							variables.put("REST",1);
						}else {
							variables.put("REST",0);
						}
						
						
					  
					 /* ProcessInstanceWithVariables pVariablesInReturn =
					  runtimeService.createProcessInstanceByKey("intentout")
					  .setVariables(variables)
					  
					  .executeWithVariablesInReturn();
					  
					  */
						variables.put("WF", "forwardIntentOut");
						
						String workflow = (String) variables.get("WF");
						String voucherNumber = (String) variables.get("voucherNumber");
						String sourceID = (String) variables.get("id");


						LmsQueueMst lmsQueueMst = new LmsQueueMst();

						lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
						
						//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
						
						java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
						java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
						lmsQueueMst.setVoucherDate(sqlVDate);
						
						
						
						lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
						lmsQueueMst.setVoucherType(workflow);
						lmsQueueMst.setPostedToServer("NO");
						lmsQueueMst.setJobClass("forwardIntentOut");
						lmsQueueMst.setCronJob(true);
						lmsQueueMst.setJobName(workflow + sourceID);
						lmsQueueMst.setJobGroup(workflow);
						lmsQueueMst.setRepeatTime(60000L);
						lmsQueueMst.setSourceObjectId(sourceID);
						
						lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

						lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
						variables.put("lmsqid", lmsQueueMst.getId());
						eventBus.post(variables);
					
					  

		            return saved;
		            
		            
		        }).orElseThrow(() -> new ResourceNotFoundException("Intent " + intenthdrid + " not found"));

	}
	 
	@GetMapping("{companymstid}/{branchCode}/generateintenthdr")
	public Map<String, Object>retrieveGeneratedIntent(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "branchCode") String
			  branchCode){
		
		
		//Optional<CompanyMst> companyMstOpt = companyMstRepo.findById(companymstid);
		//CompanyMst companyMst = companyMstOpt.get();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map = intentServiceImpl.getGeneratedIntent(companymstid,branchCode);
	
		return map;
		
		
	}
	@GetMapping("/{companymstid}/intenthdrresource/intentdtlbydate/{branchcode}")
	public List<ReorderReport> tallyImportReportReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, 
			@RequestParam("date") String fdate){
	java.util.Date fDate = SystemSetting.StringToUtilDate(fdate,"yyyy-MM-dd");
//	java.util.Date TDate = SystemSetting.StringToUtilDate(tdate,"yyyy-MM-dd");
		return intentService.IntentDtlReport(companymstid,branchcode,fDate);
		
	}
	
}
