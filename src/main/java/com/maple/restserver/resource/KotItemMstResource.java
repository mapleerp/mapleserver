package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.KotCategoryMst;
import com.maple.restserver.entity.KotItemMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.KotItemMstRepository;

@RestController
public class KotItemMstResource {

	@Autowired
	ItemMstRepository itemmstRepo;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	KotItemMstRepository kotItemMstRepo;
	@PostMapping("{companymstid}/kotitemmst/savekotitemmst")
	public KotItemMst creatKotItemMst(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody KotItemMst kotItemMst )
	{
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		
		kotItemMst.setCompanyMst(companyMst);
			 return kotItemMstRepo.save(kotItemMst);
	}
	@DeleteMapping("{companymstid}/kotitemmst/deletekotitemmst/{id}")
	public void deleteKotItemMst(@PathVariable(value = "id") String id) {
		kotItemMstRepo.deleteById(id);
		 
	}
	
	@GetMapping("{companyid}/kotitemmst/findallkotitemmst")
	public List< KotItemMst> getKotItemMst(
			@PathVariable (value = "companyid") String companyid  )
	{
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();
		return  kotItemMstRepo.findByCompanyMst(companyMst);
	}
	
	
	@GetMapping("{companyid}/kotitemmst/findallkotitemmstbyitemname")
	public List< KotItemMst> getKotItemMstByItemName(
			@PathVariable (value = "companyid") String companyid ,
			@RequestParam(value ="itemname")String itemname)
	{
		ItemMst itemmst = itemmstRepo.findByItemName(itemname);
		if(null != itemmst)
		{
		return  kotItemMstRepo.findByItemId(itemmst.getId());
		}
		else
		{
			return null;
		}
	}
	
	
	@GetMapping("{companyid}/kotitemmst/kotitemmstbyitemname")
	public List< KotItemMst> getKotItemMstByItemname(
			@PathVariable (value = "companyid") String companyid,
			@RequestParam(value ="itemname")String itemname
			)
	{
		ItemMst itemmstByItem = itemmstRepo.findByItemName(itemname);
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();
		return  kotItemMstRepo.findByCompanyMstAndItemId(companyMst,itemmstByItem.getId());
	}
	
	
	@GetMapping("{companyid}/kotitemmst/kotitemmstbyitemid/{itemid}")
	public List<KotItemMst> getKotItemMstByItemId(
			@PathVariable (value = "companyid") String companyid,
			@PathVariable (value = "itemid") String itemid
			)
	{
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
		if(null == companyMst)
		{
			return null;
		}
		return  kotItemMstRepo.findByCompanyMstAndItemId(companyMst,itemid);
	}
}
