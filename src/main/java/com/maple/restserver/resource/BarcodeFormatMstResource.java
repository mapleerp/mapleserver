package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BarcodeFormatMst;
import com.maple.restserver.entity.ChequePrintMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.BarcodeFormatMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
public class BarcodeFormatMstResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	BarcodeFormatMstRepository barcodeFormatMstRepo;
	
	@PostMapping("{companymstid}/barcodeformatmst/savebarcodeformatmst")
	public BarcodeFormatMst createBarcodeFormatMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody BarcodeFormatMst barcodeFormatMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			barcodeFormatMst.setCompanyMst(companyMst);
			return barcodeFormatMstRepo.save(barcodeFormatMst);

		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}
	@DeleteMapping("{companymstid}/barcodeformatmst/deletebarcodeformatmstbyid/{id}")
	public void deleteBarcodeFormatMst(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id)
	{
		barcodeFormatMstRepo.deleteById(id);
	}
	@GetMapping("{companymstid}/barcodeformatmst/showallbarcodeformatmstbyname/{formatname}")
	public List<BarcodeFormatMst> showAllBarcodeFormatMstByFormatName(
			@PathVariable(value = "formatname") String formatname)
	{
		return barcodeFormatMstRepo.findByBarcodeFormatName(formatname);
	}
	@GetMapping("{companymstid}/barcodeformatmst/showallbarcodeformatmst")
	public List<BarcodeFormatMst> showAllBarcodeFormatMst()
	{
		return barcodeFormatMstRepo.findAll();
	}
	
}
