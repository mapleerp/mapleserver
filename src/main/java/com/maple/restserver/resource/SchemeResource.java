package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SchEligiAttrListDef;
import com.maple.restserver.entity.SchEligibilityAttribInst;
import com.maple.restserver.entity.SchEligibilityDef;
import com.maple.restserver.entity.SchOfferAttrInst;
import com.maple.restserver.entity.SchOfferAttrListDef;
import com.maple.restserver.entity.SchOfferDef;
import com.maple.restserver.entity.SchSelectAttrListDef;
import com.maple.restserver.entity.SchSelectDef;
import com.maple.restserver.entity.SchSelectionAttribInst;
import com.maple.restserver.entity.SchemeInstance;
import com.maple.restserver.entity.SessionEndClosureMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.SchemeMessage;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.SchEligiAttrListDefRepository;
import com.maple.restserver.repository.SchEligibilityAttribInstRepository;
import com.maple.restserver.repository.SchEligibilityDefRepository;
import com.maple.restserver.repository.SchOfferAttrInstRepository;
import com.maple.restserver.repository.SchOfferAttrListDefRepository;
import com.maple.restserver.repository.SchOfferDefRepository;
import com.maple.restserver.repository.SchSelectAttrListDefReoository;
import com.maple.restserver.repository.SchSelectDefRepository;
import com.maple.restserver.repository.SchSelectionAttribInstRepository;
import com.maple.restserver.repository.SchemeInstanceRepository;
import com.maple.restserver.repository.SessionEndClosureMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.SaveAndPublishServiceImpl;
import com.maple.restserver.service.SchemeImplementationService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class SchemeResource {
	
	

	@Value("${mybranch}")
	private String mybranch;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	SchemeInstanceRepository schemeInstanceRepo;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
 
	@Autowired
	SchOfferDefRepository schOfferDefRepository;
	
	@Autowired
	SchOfferAttrListDefRepository schOfferAttrListDefRepository;
	
	@Autowired
	SchEligibilityDefRepository  schEligibilityDefRepository;
	
	@Autowired
	SchSelectDefRepository schSelectDefRepository;
	
	
	@Autowired
	SchEligiAttrListDefRepository schEligiAttrListDefRepository;
	
	@Autowired
	SchSelectAttrListDefReoository schSelectAttrListDefReoository;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@Autowired
	SchemeImplementationService schemeImplementationService;
	
	@Autowired
	SchSelectionAttribInstRepository schSelectionAttribInstRepository;
	@Autowired
	SchOfferAttrInstRepository schOfferAttrInstRepository;
	@Autowired
	SchEligibilityAttribInstRepository schEligibilityAttribInstRepository;
	
	@Autowired
	SaveAndPublishServiceImpl saveAndPublishServiceImpl;
	
	@PostMapping("{companymstid}/schemeinstance")
	public   SchemeInstance setSchemeInstance(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SchemeInstance  schemeInstance)
	{
		
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
			CompanyMst companyMst = companyMstOpt.get();
			schemeInstance.setCompanyMst(companyMst);
		
			
		 schemeInstance =	schemeInstanceRepo.save(schemeInstance);
		
		return schemeInstance;
		
	}
	
	@GetMapping("{companymstid}/schemeinstances")
	public List<SchemeInstance> getSchemeInstance(
			@PathVariable(value = "companymstid") String companymstid )
	{
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		
		return schemeInstanceRepo.findByCompanyMst(companyMstOpt.get());
		
	}
	
	@GetMapping("{companymstid}/activeschemeinstances/{retailorwholesale}")
	public List<SchemeInstance> getSchemeInstanceByWholesaleOrRetails(
			@PathVariable(value = "companymstid") String companymstid ,
			@PathVariable(value = "retailorwholesale") String retailorwholesale)
	{
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		
		return schemeInstanceRepo.findByCompanyMstAndSchemeWholesaleRetailAndIsActive(companyMstOpt.get().getId(),retailorwholesale,"YES");
		
	}
	

	@DeleteMapping("{companymstid}/deleteschemeinstance/{schemeinstancesid}")
	public void SchemeInstance(@PathVariable(value = "schemeinstancesid") String schemeinstancesid) {
		
		Optional<SchemeInstance> SchemeInstance=schemeInstanceRepo.findById(schemeinstancesid);
	    schemeInstanceRepo.deleteById(schemeinstancesid);
		
		saveAndPublishService.publishObjectDeletion(SchemeInstance.get(), 
				mybranch, KafkaMapleEventType.SCHEMEHDRDELETE,SchemeInstance.get().getId());
		schemeInstanceRepo.flush();
		 

	}
	
	@GetMapping("{companymstid}/schemebyname/{shemename}")
	public SchemeInstance getSchemeByName(
			@PathVariable(value = "companymstid") String companymstid,
			  @PathVariable("shemename") String shemename )
	{
		return schemeInstanceRepo.findBySchemeNameAndCompanyMstId(shemename,companymstid);
		
	}
	
	
	
	@PostMapping("{companymstid}/{schemeofferdef}")
	public  SchOfferDef getSchemeByName(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SchOfferDef  schOfferDef)
	{
		

		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
			CompanyMst companyMst = companyMstOpt.get();
			schOfferDef.setCompanyMst(companyMst);
			
			schOfferDef = schOfferDefRepository.saveAndFlush(schOfferDef);
			
			
			return schOfferDef;
		
		
	}
	
	

	@PostMapping("{companymstid}/scheligibilitydef")
	public   SchEligibilityDef getSaveSchEligibDef(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SchEligibilityDef  schEligibilityDef)
	{
		
		
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
			CompanyMst companyMst = companyMstOpt.get();
			schEligibilityDef.setCompanyMst(companyMst);
			
			
			schEligibilityDef = schEligibilityDefRepository.saveAndFlush(schEligibilityDef);
		
			 Map<String, Object> variables = new HashMap<String, Object>();

				variables.put("voucherNumber", schEligibilityDef.getId());

				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("inet", 0);
				variables.put("id", schEligibilityDef.getId());

				variables.put("companyid", schEligibilityDef.getCompanyMst());
				variables.put("branchcode", schEligibilityDef.getBranchCode());

				variables.put("REST", 1);
				
				
				variables.put("WF", "forwardSchEligibilityDef");
				
				
				String workflow = (String) variables.get("WF");
				String voucherNumber = (String) variables.get("voucherNumber");
				String sourceID = (String) variables.get("id");


				LmsQueueMst lmsQueueMst = new LmsQueueMst();

				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
				
				//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
				
				java.util.Date uDate = (Date) variables.get("voucherDate");
				java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
				lmsQueueMst.setVoucherDate(sqlVDate);
				
				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
				lmsQueueMst.setVoucherType(workflow);
				lmsQueueMst.setPostedToServer("NO");
				lmsQueueMst.setJobClass("forwardSchEligibilityDef");
				lmsQueueMst.setCronJob(true);
				lmsQueueMst.setJobName(workflow + sourceID);
				lmsQueueMst.setJobGroup(workflow);
				lmsQueueMst.setRepeatTime(60000L);
				lmsQueueMst.setSourceObjectId(sourceID);
				
				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
				
				variables.put("lmsqid", lmsQueueMst.getId());
				
				eventBus.post(variables);
	  
			
			
			
			return schEligibilityDef;
		
	}
	
	

	@GetMapping("{companymstid}/scheligibilitydefbyname/{scheligibilitydefname}")
	public SchEligibilityDef schEligibilitydefbyName(
			@PathVariable(value = "companymstid") String companymstid,
			  @PathVariable("scheligibilitydefname") String scheligibilitydefname )
	{
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		return schEligibilityDefRepository.findByEligibilityNameAndCompanyMst(scheligibilitydefname,companyMstOpt.get());
		
	}
	
	
	@GetMapping("{companymstid}/scheligibilitydefbyid/{scheligibilitydefid}")
	public SchEligibilityDef schEligibilitydefbyId(
			@PathVariable(value = "companymstid") String companymstid,
			  @PathVariable("scheligibilitydefid") String scheligibilitydefid )
	{
		
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		return schEligibilityDefRepository.findByIdAndCompanyMst(scheligibilitydefid,companyMstOpt.get());
		
	}
	
	@GetMapping("{companymstid}/scheligibilitydefbyall")
	public List<SchEligibilityDef> schEligibilitydefbyAll(
			@PathVariable(value = "companymstid") String companymstid )
	{
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		
		return schEligibilityDefRepository.findByCompanyMst(companyMstOpt.get());
		
	}
	
	
	//-------Offer ----------
	
	@PostMapping("{companymstid}/schofferdef")
	public   SchOfferDef setSchofferdef(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SchOfferDef  schOfferDef)
	{
		
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
			CompanyMst companyMst = companyMstOpt.get();
			schOfferDef.setCompanyMst(companyMst);
		
			
			
			schOfferDef =  schOfferDefRepository.saveAndFlush(schOfferDef);
		
		
		 Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", schOfferDef.getId());

			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", schOfferDef.getId());

			variables.put("companyid", schOfferDef.getCompanyMst());
			variables.put("branchcode", schOfferDef.getBranchCode());

			variables.put("REST", 1);
			
			
			variables.put("WF", "forwardSchemeOfferDef");
			
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardSchemeOfferDef");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			
			variables.put("lmsqid", lmsQueueMst.getId());
			
			eventBus.post(variables);
		
		
		return schOfferDef;
	}
	
	@PostMapping("{companymstid}/schofferattrlistdef")
	public   SchOfferAttrListDef setSchofferdefattrlist(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SchOfferAttrListDef  schOfferAttrListDef)
	{
		
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
			CompanyMst companyMst = companyMstOpt.get();
			schOfferAttrListDef.setCompanyMst(companyMst);
		
			
			
			schOfferAttrListDef = schOfferAttrListDefRepository.saveAndFlush(schOfferAttrListDef);
		
			
			  Map<String, Object> variables = new HashMap<String, Object>();

				variables.put("voucherNumber", schOfferAttrListDef.getId());

				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("inet", 0);
				variables.put("id", schOfferAttrListDef.getId());

				variables.put("companyid", schOfferAttrListDef.getCompanyMst());
				variables.put("branchcode", schOfferAttrListDef.getBranchCode());

				variables.put("REST", 1);
				
				
				variables.put("WF", "forwardSchOfferAttribListDef");
				
				
				String workflow = (String) variables.get("WF");
				String voucherNumber = (String) variables.get("voucherNumber");
				String sourceID = (String) variables.get("id");


				LmsQueueMst lmsQueueMst = new LmsQueueMst();

				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
				
				//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
				
				java.util.Date uDate = (Date) variables.get("voucherDate");
				java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
				lmsQueueMst.setVoucherDate(sqlVDate);
				
				
				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
				lmsQueueMst.setVoucherType(workflow);
				lmsQueueMst.setPostedToServer("NO");
				lmsQueueMst.setJobClass("forwardSchOfferAttribListDef");
				lmsQueueMst.setCronJob(true);
				lmsQueueMst.setJobName(workflow + sourceID);
				lmsQueueMst.setJobGroup(workflow);
				lmsQueueMst.setRepeatTime(60000L);
				lmsQueueMst.setSourceObjectId(sourceID);
				
				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
				
				variables.put("lmsqid", lmsQueueMst.getId());
				
				eventBus.post(variables);
	  
		return schOfferAttrListDef;
	}
	
	
	  @GetMapping("{companymstid}/schofferattrlistdefbyofferid/{offerid}")
		public   List<SchOfferAttrListDef> schOfferAttrListDefByOfferId(
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable("offerid") String offerid )
		{
		  Optional<CompanyMst>  companyMst=companyMstRepo.findById(companymstid);
			return schOfferAttrListDefRepository.findByOfferIdAndCompanyMst(offerid,companyMst.get());
			
			
		}
		
	
	@GetMapping("{companymstid}/schofferattributelistdefbyname/{schoffeattname}")
	public SchOfferAttrListDef schofferattlistdefName(
			@PathVariable(value = "companymstid") String companymstid,
			  @PathVariable("schoffeattname") String schoffeattname )
	{
		
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		return schOfferAttrListDefRepository.findByAttribNameAndCompanyMst(schoffeattname,companyMstOpt.get());
		
	}
	

	@GetMapping("{companymstid}/schofferdefbyname/{schofferdeftype}")
	public SchOfferDef schofferdefName(
			@PathVariable(value = "companymstid") String companymstid,
			  @PathVariable("schofferdeftype") String schofferdeftype )
	
	
	{
		
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		return schOfferDefRepository.findByOfferTypeAndCompanyMst(schofferdeftype,companyMstOpt.get());
		
	}
	
	
	@GetMapping("{companymstid}/schofferdefbyid/{schofferdefid}")
	public SchOfferDef OfferDefbyId(
			@PathVariable(value = "companymstid") String companymstid,
			  @PathVariable("schofferdefid") String schofferdefid )
	{
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		return schOfferDefRepository.findByIdAndCompanyMst(schofferdefid,companyMstOpt.get());
		
	}
	
	@GetMapping("{companymstid}/schofferdefdefbyall")
	public List<SchOfferDef> schOfferDefbyAll(
			@PathVariable(value = "companymstid") String companymstid )
	{
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		return schOfferDefRepository.findByCompanyMst(companyMstOpt.get());
		
	}
	
	//------------------------Selection ---
	
	@PostMapping("{companymstid}/schselectdef")
	public   SchSelectDef schSelectDef(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SchSelectDef  schSelectDef)
	{
		Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		 CompanyMst companyMst = companyMstOpt.get();
		 schSelectDef.setCompanyMst(companyMst);
		 schSelectDef = schSelectDefRepository.saveAndFlush(schSelectDef);
		 
		 Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", schSelectDef.getId());

			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", schSelectDef.getId());

			variables.put("companyid", schSelectDef.getCompanyMst());
			variables.put("branchcode", schSelectDef.getBranchCode());

			variables.put("REST", 1);
			
			
			variables.put("WF", "forwardSchSelectDef");
			
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardSchSelectDef");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			
			variables.put("lmsqid", lmsQueueMst.getId());
			
			eventBus.post(variables);
		 
		 return schSelectDef;
		
		
	}
	
	@GetMapping("{companymstid}/schselectdef/{schselectdefname}")
	public SchSelectDef schselectdefName(
			@PathVariable(value = "companymstid") String companymstid,
			  @PathVariable("schselectdefname") String schselectdefname )
	{
		
		Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		 CompanyMst companyMst = companyMstOpt.get();
		return schSelectDefRepository.findBySelectionNameAndCompanyMst(schselectdefname,companyMst);
		
	}
	//----------------------scheme attribute
	
	@PostMapping("{companymstid}/schselectattrlistdef")
	public   SchSelectAttrListDef schSelectAttrListDef(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SchSelectAttrListDef  schSelectAttrListDef)
	{
		
		 Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		 CompanyMst companyMst = companyMstOpt.get();
		 schSelectAttrListDef.setCompanyMst(companyMst);
		 
		 schSelectAttrListDef = schSelectAttrListDefReoository.saveAndFlush(schSelectAttrListDef);
		
		 Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", schSelectAttrListDef.getId());

			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", schSelectAttrListDef.getId());

			variables.put("companyid", schSelectAttrListDef.getCompanyMst());
			variables.put("branchcode", schSelectAttrListDef.getBranchCode());

			variables.put("REST", 1);
			
			
			variables.put("WF", "forwardSchSelectAttrListDef");
			
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardSchSelectAttrListDef");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			
			variables.put("lmsqid", lmsQueueMst.getId());
			
			eventBus.post(variables);
		 
		return schSelectAttrListDef;
		
	}
	
	@GetMapping("{companymstid}/schselectsttrlistdefbyname/{schselectdefname}")
	public SchSelectAttrListDef schselectattrlistdefName(
			@PathVariable(value = "companymstid") String companymstid,
			  @PathVariable("schselectdefname") String schselectdefname )
	{
		Optional<CompanyMst>  companyMstOpt=companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		return schSelectAttrListDefReoository.findByAttribNameAndCompanyMst(schselectdefname,companyMst);
		
	}
	
	@GetMapping("{companymstid}/schselectsttrlistdefbyselectionid/{selctiondefid}")
	public List<SchSelectAttrListDef> schselectattrlistdefbyselectionid(
			@PathVariable(value = "companymstid") String companymstid,
			  @PathVariable("selctiondefid") String selctiondefid )
	{
		
		Optional<CompanyMst>  companyMstOpt=companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		return schSelectAttrListDefReoository.findBySelectionIdAndCompanyMst(selctiondefid,companyMst);
		
	}
	@GetMapping("{companymstid}/schselectdefbyid/{schselectdefid}")
	public SchSelectDef schselectdefId(
			@PathVariable(value = "companymstid") String companymstid,
			  @PathVariable("schselectdefid") String schselectdefid )
	{
		Optional<CompanyMst>  companyMst=companyMstRepo.findById(companymstid);
		return schSelectDefRepository.findByIdAndCompanyMst(schselectdefid,companyMst.get());
		
	}
	
	
	@GetMapping("{companymstid}/schselectdefall")
	public List<SchSelectDef> schselectdefId(
			@PathVariable(value = "companymstid") String companymstid
			  )
	{
		  Optional<CompanyMst>  companyMst=companyMstRepo.findById(companymstid);
		return schSelectDefRepository.findByCompanyMst(companyMst.get());
		
	}
	
	@PostMapping("{companymstid}/scheligiattrlistdef")
	public   SchEligiAttrListDef schEligiAttrListDefFun(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SchEligiAttrListDef  schEligiAttrListDef)
	{
		Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid) ;
		 CompanyMst companyMst = companyMstOpt.get();
		 schEligiAttrListDef.setCompanyMst(companyMst);
		 schEligiAttrListDef = schEligiAttrListDefRepository.saveAndFlush(schEligiAttrListDef);
		 
		 
		 Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", schEligiAttrListDef.getId());

			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("inet", 0);
			variables.put("id", schEligiAttrListDef.getId());

			variables.put("companyid", schEligiAttrListDef.getCompanyMst());
			variables.put("branchcode", schEligiAttrListDef.getBranchCode());

			variables.put("REST", 1);
			
			
			variables.put("WF", "forwardSchEligiAttrListDef");
			
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardSchEligiAttrListDef");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			
			variables.put("lmsqid", lmsQueueMst.getId());
			
			eventBus.post(variables);
		 
		 return schEligiAttrListDef;
		
		
	}
	
	

    @GetMapping("{companymstid}/scheligibilityattribinstbyid/{id}")
	public   SchEligiAttrListDef schEligiAttibByid(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("id") String id )
	{
	  
	  Optional<CompanyMst>  companyMst=companyMstRepo.findById(companymstid);
	  
		return schEligiAttrListDefRepository.findByIdAndCompanyMst(id,companyMst.get());
		
		
	}
	

  @GetMapping("{companymstid}/scheligiattrlistdefbyeligibilityid/{eligibilityid}")
	public   List<SchEligiAttrListDef> eligibilityId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("eligibilityid") String eligibilityid )
	{
	  Optional<CompanyMst>  companyMst=companyMstRepo.findById(companymstid);
		return schEligiAttrListDefRepository.findByEligibilityIdAndCompanyMst(eligibilityid,companyMst.get());
		
		
	}
	
  
  @GetMapping("{companymstid}/scheligiattrlistdefbyname/{attribname}/{eligibilityid}")
	public SchEligiAttrListDef scheligiattrListdefbyname(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("attribname") String attribname ,@PathVariable("eligibilityid") String eligibilityid)
	
	{
	  
	 Optional<CompanyMst>  companyMst=companyMstRepo.findById(companymstid);
		return schEligiAttrListDefRepository.findByAttribNameAndCompanyMstAndEligibilityId(attribname,companyMst.get(),eligibilityid);
		
		
	}
	
  @PutMapping("{companyid}/updateschemeinstance/{id}")
	public SchemeInstance schemeInstanceUpdate(
			@PathVariable (value = "id") String id, 
			@PathVariable (value = "companyid") String companyid,
			@Valid @RequestBody SchemeInstance schemeInstanceRequest)
	{
	  	Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companyid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
		schemeInstanceRequest.setCompanyMst(companyMst);
		
		return schemeInstanceRepo.findById(id).map(schemeInstance -> {
			 
			schemeInstance.setCompanyMst(schemeInstanceRequest.getCompanyMst());
			schemeInstance.setEligibilityId(schemeInstanceRequest.getEligibilityId());
			schemeInstance.setIsActive(schemeInstanceRequest.getIsActive());
			schemeInstance.setOfferId(schemeInstanceRequest.getOfferId());
			schemeInstance.setSchemeName(schemeInstanceRequest.getSchemeName());
			schemeInstance.setSchemeWholesaleRetail(schemeInstanceRequest.getSchemeWholesaleRetail());
			schemeInstance.setSelectionId(schemeInstanceRequest.getSelectionId());
	
			
			schemeInstanceRepo.saveAndFlush(schemeInstance);
			
            return schemeInstanceRequest;
            
        }).orElseThrow(() -> new ResourceNotFoundException("schemeInstance " + schemeInstanceRequest + " not found"));
		
	
	}
  
  @GetMapping("{companymstid}/schemeinstbyid/{id}")
	public SchemeInstance SchemeById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("id") String id)
	
	{
	  
	  Optional<CompanyMst>  companyMst=companyMstRepo.findById(companymstid);
		return schemeInstanceRepo.findByIdAndCompanyMst(id,companyMst.get());
		
		
	}
  
  
  @PutMapping("{companymstid}/finalupdateschemeinstance/{id}")
 	public SchemeInstance schemeInstanceUpdate(
 			@PathVariable (value = "id") String id, 
 			@PathVariable (value = "companymstid") String companymstid)
 	{
	  
	  Optional<CompanyMst>  companyMst=companyMstRepo.findById(companymstid);
	  
	  SchemeInstance scheme = schemeInstanceRepo.findByIdAndCompanyMst(id,companyMst.get());
	  
	   Map<String, Object> variables = new HashMap<String, Object>();

				variables.put("voucherNumber", scheme.getId());

				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("inet", 0);
				variables.put("id", scheme.getId());

				variables.put("companyid", scheme.getCompanyMst());
				variables.put("branchcode", scheme.getBranchCode());

				variables.put("REST", 1);
				
				
				variables.put("WF", "forwardSchemeInstance");
				
				
				String workflow = (String) variables.get("WF");
				String voucherNumber = (String) variables.get("voucherNumber");
				String sourceID = (String) variables.get("id");


				LmsQueueMst lmsQueueMst = new LmsQueueMst();

				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
				
				//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
				
				java.util.Date uDate = (Date) variables.get("voucherDate");
				java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
				lmsQueueMst.setVoucherDate(sqlVDate);
				
				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
				lmsQueueMst.setVoucherType(workflow);
				lmsQueueMst.setPostedToServer("NO");
				lmsQueueMst.setJobClass("forwardSchemeInstance");
				lmsQueueMst.setCronJob(true);
				lmsQueueMst.setJobName(workflow + sourceID);
				lmsQueueMst.setJobGroup(workflow);
				lmsQueueMst.setRepeatTime(60000L);
				lmsQueueMst.setSourceObjectId(sourceID);
				
				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
				
				variables.put("lmsqid", lmsQueueMst.getId());
				
				eventBus.post(variables);
	  
//				schemeImplementationService.finalSchemePublish(id,companyMst.get(),mybranch);
				
				
				
		return null;
	  
 	}
  
  
  //------------------version 6.13 surya
  
  @GetMapping("{companymstid}/deleteallschemetables")
	public String DltSchemeInst(@PathVariable (value = "companymstid") String companymstid) {
		
	  
	  schOfferDefRepository.deleteAll();
	  
	  schOfferAttrListDefRepository.deleteAll();
	  
	  
	  
	  schSelectDefRepository.deleteAll();
	  
	  schSelectAttrListDefReoository.deleteAll();
	  
	  
	  schEligibilityDefRepository.deleteAll();
	  schEligiAttrListDefRepository.deleteAll();
	  

	  return "Success";
	}
  
  //---------------------version 6.13 surya end
  
  
  /*
   * scheme publishing to all branches..............17-03-2022.........
   */
  @GetMapping("{companymstid}/schemeresource/publishschemetoallbranches/{schemename}")
 	public String publishSchemeToAllBranches(
 			@PathVariable(value = "companymstid") String companymstid,
 			@PathVariable("schemename") String schemename)
 	
 	{
 	  
 		return schemeImplementationService.publishSchemeToAllBranches(schemename,companymstid);
 		
 	}

}
