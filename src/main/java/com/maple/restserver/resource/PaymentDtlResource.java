
package com.maple.restserver.resource;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.PayableDtl;
import com.maple.restserver.entity.PaymentDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.PayableDtlRepository;
import com.maple.restserver.repository.PaymentDtlRepository;
import com.maple.restserver.repository.PaymentHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;


@RestController
@Transactional
public class PaymentDtlResource {
	
	private static final Logger logger = LoggerFactory.getLogger(PaymentDtlResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	private PaymentHdrRepository paymentHdrRepo;
	
	@Autowired
	private PaymentDtlRepository paymentDtlRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@GetMapping("{companymstid}/paymenthdr/{paymentId}/paymentdtl")
	
	 public List<PaymentDtl> getAllPaymentDtlByPaymentHdrId(@PathVariable (value = "paymentId") String paymenthdrId,
             Pageable pageable) {
		return paymentDtlRepo.findBypaymenthdrId(paymenthdrId);
	}
	
 
	 @PostMapping("{companymstid}/paymenthdr/{paymentId}/paymentdtl")
	    public PaymentDtl createPaymentDtl(@PathVariable (value = "paymentId") String paymentId,
	                                 @Valid @RequestBody PaymentDtl paymentdtlRequest) {
	        return paymentHdrRepo.findById(paymentId).map(paymenthdr -> {
	        	paymentdtlRequest.setPaymenthdr(paymenthdr);
	            //return paymentDtlRepo.saveAndFlush(paymentdtlRequest);
	        	return saveAndPublishService.savePaymentDtl(paymentdtlRequest,mybranch);
	        }).orElseThrow(() -> new ResourceNotFoundException("paymentId 1" + paymentId + " not found"));
	    }
	
	 
	 @PutMapping("{companymstid}/paymenthdr/{paymentHdrId}/paymentdtl/{paymentDtlId}")
	    public PaymentDtl updatePaymentDtl(@PathVariable (value = "paymentHdrId") String paymentHdrId,
	                                 @PathVariable (value = "paymentDtlId") String paymentDtlId,
	                                 @Valid @RequestBody PaymentDtl paymentDtlRequest) {
	        if(!paymentHdrRepo.existsById(paymentHdrId)) {
	            throw new ResourceNotFoundException("paymentHdrId " + paymentHdrId + " not found");
	        }

	        return paymentDtlRepo.findById(paymentDtlId).map(paymentdtl -> {
	        	paymentdtl.setAccountId(paymentDtlRequest.getAccountId());
	        	paymentdtl.setAmount(paymentDtlRequest.getAmount());
	        	
//	            return paymentDtlRepo.saveAndFlush(paymentdtl);
	        	return saveAndPublishService.savePaymentDtl(paymentdtl,mybranch);
	        }).orElseThrow(() -> new ResourceNotFoundException("PaymentDtlId " + paymentDtlId + "not found"));
	    }

		@DeleteMapping("{companymstid}/paymentdtl/{paymentDtlId}")
		public void paymentDtlDelete(@PathVariable String paymentDtlId) {
			paymentDtlRepo.deleteById(paymentDtlId);

		}
	 

}

