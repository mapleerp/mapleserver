package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.RawMaterialIssueDtl;
import com.maple.restserver.entity.RawMaterialReturnDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.RawMaterialIssueReport;
import com.maple.restserver.report.entity.RawMaterialReturnReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.RawMaterialReturnDtlRepository;
import com.maple.restserver.repository.RawMaterialReturnHdrRepository;
import com.maple.restserver.service.RawMaterialReturnService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class RawMaterialReturnDtlResource {
	private static final Logger logger = LoggerFactory.getLogger(RawMaterialReturnDtlResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	RawMaterialReturnHdrRepository rawMaterialReturnHdrRepo;
	
	@Autowired
	RawMaterialReturnDtlRepository rawMaterialReturnDtlRepo;
	
	@Autowired
	RawMaterialReturnService rawMaterialReturnService;
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	
	 @PostMapping("{companymstid}/rawmaterialreturndtl/{rawmaterialreturnhdrId}")
	    public RawMaterialReturnDtl createRawMaterialReturnDtl(@PathVariable (value = "rawmaterialreturnhdrId") String rawmaterialreturnhdrId,
	                                 @Valid @RequestBody RawMaterialReturnDtl rawMaterialReturnDtl) {
	        return rawMaterialReturnHdrRepo.findById(rawmaterialreturnhdrId).map(rawMaterialReturnHdr -> {
	        	rawMaterialReturnDtl.setRawMaterialReturnHdr(rawMaterialReturnHdr); 
	           // return rawMaterialReturnDtlRepo.saveAndFlush(rawMaterialReturnDtl);
	        	return saveAndPublishService.saveRawMaterialReturnDtl(rawMaterialReturnDtl,mybranch);
	        }).orElseThrow(() -> new ResourceNotFoundException("raw Material" + rawmaterialreturnhdrId + " not found"));
	    }
		
		@DeleteMapping("{companymstid}/rawmaterialreturn/{rawmaterialhdrId}/deleterawmaterialretunrdtl")
		public void DeleteRawMaterialDtl(@PathVariable (value="rawmaterialhdrId") String rawmaterialhdrId) {
			rawMaterialReturnDtlRepo.deleteById(rawmaterialhdrId);

		}
		  @GetMapping("{companymstid}/rawmaterialretunrdtl/{retunrmstid}/rawmaterialreturndtls")
			public List<RawMaterialReturnDtl> retriveAllRawMaterialReturnByHdrId(
					@PathVariable(value = "companymstid") String companymstid,
					@PathVariable(value = "retunrmstid") String retunrmstid) {

				return rawMaterialReturnDtlRepo.findByRawMaterialReturnHdrId(retunrmstid);


		}
		  
		  @GetMapping("{companymstid}/rawmaterialreturndtl/{vocuhernumber}/rawmaterialreturnreport")
			public List<RawMaterialReturnReport> retriveAllRawMaterialByVoucher(
					@PathVariable(value = "companymstid") String companymstid,
					@PathVariable(value = "vocuhernumber") String vocuhernumber,
					 @RequestParam("rdate") String reportdate){
				java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd"); {

				return rawMaterialReturnService.findByRawMaterialVoucher(vocuhernumber,date,companymstid);


				}
		   }
}
