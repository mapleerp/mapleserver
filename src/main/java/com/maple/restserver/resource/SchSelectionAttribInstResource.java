package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SchSelectionAttribInst;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SchSelectionAttribInstRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
public class SchSelectionAttribInstResource {

	private static final Logger logger = LoggerFactory.getLogger(PurchaseHdrResource.class);
	@Value("${mybranch}")
	private String mybranch;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	SchSelectionAttribInstRepository schSelectionAttribInstRepo;
	
	

	
	
	@PostMapping("{companymstid}/schselectionattributeinst")
	public SchSelectionAttribInst createschSelectionAttr(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  SchSelectionAttribInst  schSelectionAttribInst)
	{
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		
		schSelectionAttribInst.setCompanyMst(companyMst.get());
		
			return  schSelectionAttribInstRepo.save(schSelectionAttribInst);
//			return saveAndPublishService.saveSchSelectionAttribInst(schSelectionAttribInst, mybranch);
			
		}
	
	@GetMapping("{companymstid}/allschselectionattributeinst")
	public  @ResponseBody List<SchSelectionAttribInst> findAllSchSelectionAttribInst(@PathVariable(value = "companymstid") String
			 companymstid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schSelectionAttribInstRepo.findByCompanyMst(companyMst.get());
	}
	
	@GetMapping("{companymstid}/schselectionattributeinstbyattribname/{attribname}/{schemeid}")
	public  @ResponseBody SchSelectionAttribInst findAllSchSelectionAttribInstByAttrName(@PathVariable(value = "companymstid") String
			 companymstid, @PathVariable(value = "attribname") String attribname, 
			 @PathVariable(value = "schemeid") String schemeid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schSelectionAttribInstRepo.findByCompanyMstAndAttributeNameAndSchemeId(companyMst.get(),attribname,schemeid);
	}
	
	@GetMapping("{companymstid}/allschselectionattributeinstbyschemeid/{schemeid}")
	public  @ResponseBody List<SchSelectionAttribInst> findAllSchSelectionAttribInstBySchemeId(@PathVariable(value = "companymstid") String
			 companymstid,  @PathVariable(value = "schemeid") String schemeid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schSelectionAttribInstRepo.findByCompanyMstAndSchemeId(companyMst.get(), schemeid);
	}
	
	@DeleteMapping("{companymstid}/deleteschselectionattribinst/{id}")
	public void SchSelectionAttribInst(@PathVariable(value = "id") String id) {
		
		Optional<SchSelectionAttribInst> schSelectionAttribInst=schSelectionAttribInstRepo.findById(id);
		
		schSelectionAttribInstRepo.deleteById(id);
		//schSelectionAttribInstRepo.flush();
		
		saveAndPublishService.publishObjectDeletion(schSelectionAttribInst.get(), 
				mybranch, KafkaMapleEventType.SCHSELECTIONATTRIBINSTDELETE,schSelectionAttribInst.get().getId());
		 
	}
	
	
	@GetMapping("{companymstid}/allschselectionattributeinstbyschemeidandselectionid/{schemeid}/{selectionid}")
	public  @ResponseBody List<SchSelectionAttribInst> findAllSchSelectionAttribInstBySchemeIdAndSelectionId
	(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "schemeid") String schemeid,
			@PathVariable(value = "selectionid") String selectionid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schSelectionAttribInstRepo.findByCompanyMstAndSchemeIdAndSelectionId(companyMst.get(), schemeid,selectionid);
	}
	
	
	@GetMapping("{companymstid}/schselectionattributeinstbyattribnameandselectionid/{attribname}/{schemeid}/{selectionid}")
	public  @ResponseBody SchSelectionAttribInst findAllSchSelectionAttribInstByAttrNameAndSelectionId(@PathVariable(value = "companymstid") String
			 companymstid, @PathVariable(value = "attribname") String attribname, 
			 @PathVariable(value = "schemeid") String schemeid,
			 @PathVariable(value = "selectionid") String selectionid
			){
		Optional<CompanyMst> companyMst=companyMstRepo.findById(companymstid);
		return schSelectionAttribInstRepo.findByCompanyMstAndAttributeNameAndSchemeIdAndSelectionId(companyMst.get(),attribname,schemeid,selectionid);
	}


}
