package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.CreditCardReconcileHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.ConsumptionReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.CreditCardReconcileRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class CreditCardReconcileResource {

	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	CreditCardReconcileRepository creditCardReconcileRepository;
	
	
	
	@PostMapping("{companymstid}/creditcardreconcileresource/createcreditcardreconcile")
	public CreditCardReconcileHdr createCreditCardReconcileHdr(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody CreditCardReconcileHdr creditCardReconcileHdr) {
		
Optional<CompanyMst> companyMstOpt=companyMstRepository.findById(companymstid);
	 		creditCardReconcileHdr.setCompanyMst(companyMstOpt.get());
	 		
	 		
	 		
	 		creditCardReconcileHdr= creditCardReconcileRepository.save(creditCardReconcileHdr);
	 		
				return creditCardReconcileHdr;
	
	 	
	
	}
	
	

	@GetMapping("{companymstid}/creditcardreconcileresource/getcretitcardreconcilereport/{branchcode}")
	public List<CreditCardReconcileHdr> fetchCreditCardReconcile(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,  @RequestParam("rdate") String strfromdate
			
			 ){
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		
	
//		return creditCardReconcileRepository.findByBranchCodeAndTransDate(branchcode, fromdate );

		System.out.print(fromdate+"from date isssssssssssssssssssssssssssssssssssssssssssssss");
		return creditCardReconcileRepository.fetchCreditCardReconcile(branchcode, fromdate);

	}

	

	@GetMapping("{companymstid}/creditcardreconcileresource/creditcardreconcilereportbetweendate/{branchcode}")
	public List<CreditCardReconcileHdr> getCreditCardReconcileHdrReport(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate
			 ){
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
	
		return creditCardReconcileRepository.getCreditCardReconcileHdrReport(branchcode, fromdate,todate );

	

	}

	
	
}
