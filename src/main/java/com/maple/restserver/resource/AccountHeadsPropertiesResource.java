package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AccountHeadsProperties;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AccountHeadsPropertiesRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
public class AccountHeadsPropertiesResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
		
	@Autowired
	AccountHeadsPropertiesRepository accountHeadsPropertiesRepository;
	
	@PostMapping("/{companymstid}/accountheadspropertiesresource/createaccountheadsproperty")
	public AccountHeadsProperties createAccountHeadsProperties(
			@PathVariable(value = "companymstid") String companymstid,
			  @Valid @RequestBody AccountHeadsProperties accountHeadsProperties)
	{
		
		 	return companyMstRepo.findById(companymstid).map(companyMst-> {
		 		accountHeadsProperties.setCompanyMst(companyMst);
		 		AccountHeadsProperties savedAccountHeadsProperties = accountHeadsPropertiesRepository.save(accountHeadsProperties);
		 		
		 		
					return savedAccountHeadsProperties;
		 		
		 	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
					  companymstid + " not found"));
		 	
		
	}

	
	@GetMapping("/{companymstid}/accountheadspropertiesresource/findallaccountheadsproperties")
	public List<AccountHeadsProperties> retrieveAllAccountHeadsProperties(
			@PathVariable(value = "companymstid") String companymstid){
		
		Optional<CompanyMst> companyMstOpt=companyMstRepo.findById(companymstid);
		return accountHeadsPropertiesRepository.findByCompanyMst(companyMstOpt.get());
	}
	
	
	@GetMapping("/{companymstid}/accountheadspropertiesresource/getaccountheadspropertiesbyaccountidandpropertyname/{accountid}")
	public AccountHeadsProperties retrieveAllAccountHeadsPropertiesByAccountIdAndPropertyName(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value ="accountid")String accountid,
			@RequestParam(value ="propertyname")String propertyname){
		
		Optional<CompanyMst> companyMstOpt=companyMstRepo.findById(companymstid);
		return accountHeadsPropertiesRepository.findByAccountIdAndPropertyName(accountid,propertyname);
	}
	
	
	
	// delete
		@DeleteMapping("{companymstid}/accountheadspropertiesresource/deleteaccountheadsproperties/{id}")
		public void DeleteAccountHeadsProperties(@PathVariable(value = "id") String Id) {
			accountHeadsPropertiesRepository.deleteById(Id);
		}
	
	
	
	
	
	
	
	
	
}
