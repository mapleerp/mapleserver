package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JobCardHdr;
import com.maple.restserver.entity.JobCardInHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.JobCardInHdrRepository;

@RestController
@Transactional
public class JobCardInHdrResource {

	@Autowired
	JobCardInHdrRepository jobCardInHdrRepo;
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@GetMapping("{companymstid}/jobcardinhdr/getjobcardinbystatus")
	public List<JobCardInHdr> getJobCardInHdrByStatus(
			@PathVariable("companymstid") String companymstid)

	{
		return jobCardInHdrRepo.getJobCardVoucher();
	}
	
	@GetMapping("{companymstid}/jobcardinhdr/getjobcardinbyvoucherno/{voucherno}")
	public JobCardInHdr getJobCardInHdrByVoucher(
			@PathVariable("companymstid") String companymstid,
			@PathVariable("voucherno") String voucherno)

	{
		
		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		return jobCardInHdrRepo.findByVoucherNumberAndCompanyMst(voucherno,companyMst.get());
	}
	
	
}
