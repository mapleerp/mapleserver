package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemImageMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.exception.ResourceNotFoundException;
 
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemImageMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
@RestController
@Transactional
public class ItemImageMstResource {
	@Value("${serverorclient}")
	private String serverorclient;
	
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	 EventBus eventBus = EventBusFactory.getEventBus();
	@Autowired
	private ItemMstRepository itemMstRepository;
	
	@Autowired
	private ItemImageMstRepository itemImageMstRepository;
	
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@GetMapping("{companymstid}/getimagebyitem/{itemmstid}")
	public List<ItemImageMst> getImageByItemId(
	@PathVariable(value = "companymstid") String companymstid,
	@PathVariable(value = "itemmstid") String itemmstid)
	
	{
		
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		
		
		Optional<ItemMst> itemMstOpt = itemMstRepository.findById(itemmstid);
		ItemMst itemMst = itemMstOpt.get();
		List<ItemImageMst> itemImageMstList = itemImageMstRepository.findByCompanyMstAndItemMst(companyMst,itemMst );
		
		return itemImageMstList;
	}
	
	
	
	@GetMapping("{companymstid}/getimagebyitem/mobile/{itemmstid}")
	public JSONArray getImageByItemIdForMobile(
	@PathVariable(value = "companymstid") String companymstid,
	@PathVariable(value = "itemmstid") String itemmstid)
	
	{
		
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		
		
		Optional<ItemMst> itemMstOpt = itemMstRepository.findById(itemmstid);
		ItemMst itemMst = itemMstOpt.get();
		List<ItemImageMst> itemImageMstList = itemImageMstRepository.findByCompanyMstAndItemMst(companyMst,itemMst );
		
		JSONArray jsArray = new JSONArray(itemImageMstList);
		
		return jsArray;
	}
	
	
	@PostMapping("{companymstid}/itemimagemst")
	public ItemImageMst createItemImage(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  ItemImageMst itemImageMst)
	{
		return companyMstRepository.findById(companymstid).map(companyMst-> {
			itemImageMst.setCompanyMst(companyMst);
		
		
			  itemImageMstRepository.saveAndFlush(itemImageMst);
			  Map<String, Object> variables = new HashMap<String, Object>();
				
				variables.put("companyid",companyMst);
				
				variables.put("voucherNumber",itemImageMst.getId());
				variables.put("voucherDate", SystemSetting.getSystemDate());
				variables.put("id",itemImageMst.getId());
				
				variables.put("branchcode", itemImageMst.getBranchCode());

				if(serverorclient.equalsIgnoreCase("REST")) {
					variables.put("REST",1);
				}else {
					variables.put("REST",0);
				}
				
				
				//variables.put("voucherDate", purchase.getVoucherDate());
				//variables.put("id",purchase.getId());
				variables.put("inet", 0);
				
				
				
				variables.put("WF", "forwardItemImage");
				
				String workflow = (String) variables.get("WF");
				String voucherNumber = (String) variables.get("voucherNumber");
				String sourceID = (String) variables.get("id");


				LmsQueueMst lmsQueueMst = new LmsQueueMst();

				lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
				//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
				
				java.util.Date uDate = (Date) variables.get("voucherDate");
				java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
				lmsQueueMst.setVoucherDate(sqlVDate);
				
				lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
				lmsQueueMst.setVoucherType(workflow);
				lmsQueueMst.setPostedToServer("NO");
				lmsQueueMst.setJobClass("forwardItemImage");
				lmsQueueMst.setCronJob(true);
				lmsQueueMst.setJobName(workflow + sourceID);
				lmsQueueMst.setJobGroup(workflow);
				lmsQueueMst.setRepeatTime(60000L);
				lmsQueueMst.setSourceObjectId(sourceID);
				
				lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

				lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
				
				
				variables.put("lmsqid", lmsQueueMst.getId());
				
				
				eventBus.post(variables);
		
		return itemImageMst;
		
		
		
		
		
		
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));
		
		
		
	
	}
	
	
	
	

}
