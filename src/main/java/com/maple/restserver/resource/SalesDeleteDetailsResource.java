package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.AddKotWaiter;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.SalesDltdDtl;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesDeletedDtlRepository;

@RestController
public class SalesDeleteDetailsResource {

	
	@Autowired
	CompanyMstRepository companyMstRepository;
	

	@Autowired
	SalesDeletedDtlRepository salesDeletedDtlRepository;
	@PostMapping("{companymstid}/salesdeleteddetailsresource/salesdeleteddtl")
	public SalesDltdDtl createSalesDeletedDtl(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody SalesDltdDtl salesDeletedDtl) {
		
	 	return companyMstRepository.findById(companymstid).map(companyMst-> {
	 		salesDeletedDtl.setCompanyMst(companyMst);
	 		
		return salesDeletedDtlRepository.saveAndFlush(salesDeletedDtl);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found")); }
	
	
	@GetMapping("{companymstid}/salesdeleteddetailsresource/getsalesdeletebydate/{branchCode}")

	public List<SalesDltdDtl> getSalesDeleteDtlByDate(@PathVariable(value="companymstid") String companymstid,
			@PathVariable(value="branchCode") String branchCode,
			@RequestParam(value="rdate")String rdate){
		
		Date urDate = ClientSystemSetting.StringToUtilDate(rdate, "yyyy-MM-dd");
		
		return salesDeletedDtlRepository.findByBranchCodeAndVoucherDateAndCompanyMstId(branchCode,urDate,companymstid);
	}
	
	
	
}
