package com.maple.restserver.resource;

import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;

import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.ProductionDtl;
import com.maple.restserver.entity.ProductionDtlDtl;

import com.maple.restserver.entity.ProductionMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.ReqRawMaterial;
import com.maple.restserver.repository.ActualProductionDtlRepository;
import com.maple.restserver.repository.ActualProductionHdrRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.ProductionDetailRepository;
import com.maple.restserver.repository.ProductionDtlDtlRepository;
import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;

import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.repository.ProductionMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ProductionService;
import com.maple.restserver.service.ProductionServiceImpl;
import com.maple.restserver.service.task.ProductionFinishedGoodsStock;
import com.maple.restserver.service.task.ProductionRawMaterialStock;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class ProductionMstResource {
	@Value("${PRODUCTIONWITHOUTPLANNING}")
	private String PRODUCTIONWITHOUTPLANNING;

	@Autowired
	ActualProductionDtlRepository actualProdDtlRepo;
	@Value("${serverorclient}")
	private String serverorclient;
	@Autowired
	ItemMstRepository itemMstRepo;
	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepo;
	@Autowired
	ProductionFinishedGoodsStock productionFinishedGoodsStock;
	@Autowired
	ProductionRawMaterialStock productionRawMaterialStock;
	@Autowired
	private PriceDefinitionRepository priceDefinitionRepo;

	@Autowired
	ProductionDtlDtlRepository productionDtlDtlRepo;
	@Autowired
	ProductionDetailRepository productionDtlRepo;
	@Autowired
	ActualProductionHdrRepository actualProductionHdrRepo;
	@Autowired
	VoucherNumberService voucherService;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	ProductionServiceImpl productionService;

	EventBus eventBus = EventBusFactory.getEventBus();
	@Autowired
	private ProductionMstRepository productionMstRepo;

	@Autowired
	private CompanyMstRepository companyMstRepository;

	@GetMapping("{companymstid}/productionmsts")
	public List<ProductionMst> retrieveAllProduction() {
		return productionMstRepo.findAll();
	}

	@GetMapping("{companymstid}/productionmst/getpartiallysavedproductionbydate/{branchcode}")
	public ProductionMst getPartiallySaved(@RequestParam(value = "rdate") String rdate,
			@PathVariable(value = "branchcode") String branchcode) {
		Date vdate = SystemSetting.StringToUtilDate(rdate, "yyyy-MM-dd");
		return productionMstRepo.getPartiallySavedByDate(vdate);
	}

	@PutMapping("{companymstid}/productionmst/updateproductionmst")
	public ProductionMst productionMstUpdate(@Valid @RequestBody ProductionMst productionMstRequest,
			@PathVariable(value = "companymstid") String companymstid) {
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		ProductionMst productionMst = productionMstRepo.findById(productionMstRequest.getId()).get();
		productionMst.setCompanyMst(companyMst);
		productionMst.setVoucherNumber(productionMstRequest.getVoucherNumber());
		ProductionMst saved = productionMstRepo.saveAndFlush(productionMst);
		if (PRODUCTIONWITHOUTPLANNING.equalsIgnoreCase("YES")) {

			saveActualProduction(saved);
		}
		/*
		 * Now forward to server
		 */

		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", saved.getVoucherNumber());
		variables.put("voucherDate", saved.getVoucherDate());
		variables.put("inet", 0);
		variables.put("id", saved.getId());
		variables.put("branchcode", saved.getBranchCode());

		variables.put("companyid", saved.getCompanyMst());

		variables.put("WF", "forwardProduction");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));

		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardProduction");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());

		System.out.print("Calling EVENT BUS");
		eventBus.post(variables);

		return saved;
	}

	@PostMapping("{companymstid}/productionmst")
	public ProductionMst createProduction(@Valid @RequestBody ProductionMst productionMst,
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		productionMst.setCompanyMst(companyMst);

		ProductionMst saved = productionMstRepo.saveAndFlush(productionMst);

		return saved;
	}

	@GetMapping("{companymstid}/isketitem/{kitname}/kitname")
	public String getIsKitKitem(@PathVariable(value = "kitname") String kitname) {
		return productionService.itemIsKitByName(kitname);
	}

	@GetMapping("{companymstid}/isketitem/{kitid}/kitid")
	public String getIsKitKitembyId(@PathVariable(value = "kitid") String kitid) {
		return productionService.itemIsKitById(kitid);
	}

	@GetMapping("{companymstid}/getproductionmstbyhdrid/{hdrid}")
	public Optional<ProductionMst> getProductionMstbyHdrId(@PathVariable(value = "hdrid") String hrdid) {
		return productionMstRepo.findById(hrdid);
	}

	@GetMapping("{companymstid}/getkitproductioncost/{productiondtlid}/detailid")
	public Double getProductionCostbyDetailId(@PathVariable(value = "productiondtlid") String productiondtlid) {
		return productionService.getProductionCostByProductionDetailId(productiondtlid);
	}

	@PostMapping("{companymstid}/makeproduction/{productiondtlid}")
	public Double makeProductionBydtlId(@PathVariable(value = "productiondtlid") String productiondtlid,
			@PathVariable(value = "companymstid") String companymstid) {

		Double production = productionService.makeProductionByProductionDtlId(productiondtlid, companymstid);
		return production;

	}

	@PostMapping("{companymstid}/makeproduction/{productiondtlid}/{qty}")
	public Double makeProductionByKitId(@PathVariable(value = "kitid") String kitid,
			@PathVariable(value = "qty") double qty) {
		Double production = productionService.makeProductionByKitId(kitid, qty);

		return production;
	}

	@GetMapping("{companymstid}/productionmstresource/getplanedproduction")
	public List<ProductionDtl> getPlanedProduction(@PathVariable(value = "companymstid") String companymstid) {
		return productionService.getPlanedProduction();
	}

	@Transactional
	private void saveActualProduction(ProductionMst productionMst) throws ResourceNotFoundException {

		ActualProductionHdr actualProductionHdr = new ActualProductionHdr();
		actualProductionHdr.setBranchCode(productionMst.getBranchCode());
		actualProductionHdr.setCompanyMst(productionMst.getCompanyMst());
		actualProductionHdr.setVoucherDate(productionMst.getVoucherDate());
		String VnoPrefix = null;
		VnoPrefix = SystemSetting.getFinancialYear() + "ACTPROD";
		VoucherNumber voucherNo = voucherService.generateInvoice(VnoPrefix,
				actualProductionHdr.getCompanyMst().getId());
		actualProductionHdr.setVoucherNumber(voucherNo.getCode());
		actualProductionHdr = actualProductionHdrRepo.save(actualProductionHdr);

		List<ProductionDtl> prodList = productionDtlRepo.findByProductionMst(productionMst);
		for (int i = 0; i < prodList.size(); i++) {
			double prodCost = 0.0;
			ActualProductionDtl actualProdctionDtl = new ActualProductionDtl();
			actualProdctionDtl.setActualProductionHdr(actualProductionHdr);
			actualProdctionDtl.setActualQty(prodList.get(i).getQty());
			actualProdctionDtl.setItemId(prodList.get(i).getItemId());
			actualProdctionDtl.setBatch(prodList.get(i).getBatch());
			actualProdctionDtl.setProductionDtl(prodList.get(i));
			prodCost = calculateProdCost(prodList.get(i), actualProdctionDtl.getActualQty());
			actualProdctionDtl.setProductionCost(prodCost);
			actualProdctionDtl.setCompanyMst(actualProductionHdr.getCompanyMst());
			
			String store = "MAIN";
			if(null != prodList.get(i).getStore())
			{
				actualProdctionDtl.setStore(prodList.get(i).getStore());

			}
			
			actualProdctionDtl.setStore(store);
			

			actualProdctionDtl = actualProdDtlRepo.save(actualProdctionDtl);
			ProductionDtl productionDtl = prodList.get(i);
			productionDtl.setStatus("Y");
			productionDtlRepo.save(productionDtl);
		}

		List<ReqRawMaterial> rawmaterialList = productionService.getRawMaterialQty(actualProductionHdr.getId());
		if (rawmaterialList.size() == 0) {
			productionRawMaterialStock.executebatchwiserowmaterialupdate(actualProductionHdr.getVoucherNumber(),
					actualProductionHdr.getVoucherDate(), actualProductionHdr.getCompanyMst(),
					actualProductionHdr.getId());

			productionFinishedGoodsStock.executebatchwisestockupdate(actualProductionHdr.getVoucherNumber(),
					actualProductionHdr.getVoucherDate(), actualProductionHdr.getCompanyMst(),
					actualProductionHdr.getId());

			Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("voucherNumber", actualProductionHdr.getVoucherNumber());
			variables.put("voucherDate", actualProductionHdr.getVoucherDate());
			variables.put("inet", 0);
			variables.put("id", actualProductionHdr.getId());

			variables.put("companyid", actualProductionHdr.getCompanyMst());
			variables.put("branchcode", actualProductionHdr.getBranchCode());

			if (serverorclient.equalsIgnoreCase("REST")) {
				variables.put("REST", 1);
			} else {
				variables.put("REST", 0);
			}

			variables.put("WF", "productionprocess");

			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");

			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

			// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));

			java.util.Date uDate = (Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);

			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("productionprocess");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);

			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			System.out.print("EVENT CALLING ACTUAL PRODUCTION");
			variables.put("lmsqid", lmsQueueMst.getId());

			eventBus.post(variables);
		} else {
			throw new ResourceNotFoundException();
		}
	}

	public double calculateProdCost(ProductionDtl prodDtl, Double qty) {

		List<ProductionDtlDtl> getProdDtlDtlList = productionDtlDtlRepo.findByproductionDtl(prodDtl);

		PriceDefenitionMst priceDefenitionMst = priceDefinitionMstRepo
				.findByCompanyMstAndPriceLevelName(prodDtl.getProductionMst().getCompanyMst(), "COST PRICE");
		double prodCost = 0.0;
		for (int i = 0; i < getProdDtlDtlList.size(); i++) {

			PriceDefinition priceDefinition = priceDefinitionRepo.findByItemIdAndStartDateCostprice(
					getProdDtlDtlList.get(i).getRawMaterialItemId(), priceDefenitionMst.getId(),
					prodDtl.getProductionMst().getVoucherDate(), getProdDtlDtlList.get(i).getUnitId());
			if (null != priceDefinition) {
				// prodCost = prodCost +
				// (getPrice.getBody().getAmount()*(productionDtlDtl.getQty()/prodDtl.getQty())*actualProductionDtl.getProductionDtl().getQty());
				prodCost = prodCost
						+ (priceDefinition.getAmount() * getProdDtlDtlList.get(i).getQty() / prodDtl.getQty());

				// prodCost = prodCost+
				// (getPrice.getBody().getAmount()*productionDtlDtl.getQty()/prodDtl.getQty()*actualProductionDtl.getActualQty());

			} else {
				ItemMst getItem = itemMstRepo.findById(getProdDtlDtlList.get(i).getRawMaterialItemId()).get();
				prodCost = prodCost + (getItem.getStandardPrice() * getProdDtlDtlList.get(i).getQty());
				// prodCost = prodCost +
				// (getItem.getBody().getStandardPrice()*(productionDtlDtl.getQty()/prodDtl.getQty())*actualProductionDtl.getActualQty()
				// );

			}

		}
		return prodCost / qty;
	}

}
