package com.maple.restserver.resource;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.maple.util.MapleConstants;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BatchPriceDefinition;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.FCSummarySalesDtl;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.SalesDltdDtl;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesDtlDeleted;
import com.maple.restserver.entity.SalesOrderDtl;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SchEligibilityAttribInst;
import com.maple.restserver.entity.SchOfferAttrInst;
import com.maple.restserver.entity.SchSelectionAttribInst;
import com.maple.restserver.entity.SchemeInstance;
import com.maple.restserver.entity.SummarySalesDtl;
import com.maple.restserver.entity.TaxMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.HsnCodeSaleReport;
import com.maple.restserver.report.entity.RetailSalesDetailReport;
import com.maple.restserver.report.entity.RetailSalesSummaryReport;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.ItemStockPopUpRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.SalesDeletedDtlRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesDtlDeletedRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.SchEligibilityAttribInstRepository;
import com.maple.restserver.repository.SchOfferAttrInstRepository;
import com.maple.restserver.repository.SchSelectionAttribInstRepository;
import com.maple.restserver.repository.SchemeInstanceRepository;
import com.maple.restserver.repository.TaxMstRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.BatchPriceDefinitionService;
import com.maple.restserver.service.InsuranceService;
import com.maple.restserver.service.MultiUnitConversionServiceImpl;
import com.maple.restserver.service.SalesDetailsService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.SchemeImplementationService;
import com.maple.restserver.service.SchemeService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@CrossOrigin("http://localhost:4200")

public class SalesDetailsResource {
	
private static final Logger logger = LoggerFactory.getLogger(SalesDetailsResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;

	@Value("${serverorclient}")
	private String serverorclient;
	boolean recurssionOff = false;

	@Autowired
	MultiUnitConversionServiceImpl multiUnitConversionServiceImpl;
	@Autowired
	private ItemBatchMstRepository itemBatchMstRepo;
	@Autowired
	private VoucherNumberService voucherService;
	@Autowired
	SalesDtlDeletedRepository salesDtlDeletedRepository;
	EventBus eventBus = EventBusFactory.getEventBus();
	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;
	@Autowired
	MultiUnitMstRepository multiUnitMstRepository;

	@Autowired
	SalesDeletedDtlRepository salesDeletedDtlRepository;
	@Autowired
	private SchOfferAttrInstRepository schOfferAttrInstRepository;
	@Autowired
	SchemeService schemeService;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	private UnitMstRepository unitMstRepository;

	@Autowired
	private SchemeInstanceRepository schemeInstanceRepository;

	@Autowired
	private SchEligibilityAttribInstRepository schEligibilityAttribInstRepository;

	@Autowired
	private CategoryMstRepository categoryMstRepository;

	@Autowired
	private ItemMstRepository itemMstRepository;

	@Autowired
	private SchSelectionAttribInstRepository schSelectionAttribInstRepository;

	@Autowired
	private CompanyMstRepository companyMstRepository;

	@Autowired
	private SalesDetailsRepository salesdtlRepo;

	@Autowired
	private SalesTransHdrRepository saleshdrRepo;

	@Autowired
	private ItemStockPopUpRepository itemStockPopUpRepository;

	@Autowired
	SalesDetailsRepository salesDetailsRepository;
	@Autowired
	private SalesDetailsService salesDetailsService;

	@Autowired
	TaxMstRepository taxMstRepository;

	@Autowired
	TaxMstResource taxMstResource;

	@Autowired
	MultiUnitMstRepository multyMstRepository;


	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepo;

	@Autowired
	private PriceDefinitionRepository priceDefinitionRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;

	//@Autowired
	//BatchPriceDefinitionResource batchPriceDefinitionResource;
	
	
	@Autowired
	BatchPriceDefinitionService batchPriceDefinitionService;

	BatchPriceDefinitionResource batchPriceDefinitionResource;
	
	@Autowired
	InsuranceService insuranceService;
	
	@Autowired
	SchemeImplementationService schemeImplementationService;
	
	@Autowired
	AccountHeadsResource accountHeadsResource;
	
	@Autowired
	private ExternalApi externalApi;
	Pageable topFifty = PageRequest.of(0, 50);
	

	@GetMapping("{companymstid}/salestranshdr/{salestranshdrId}/salesdtl")
	public List<SalesDtl> retrieveAllSalesDtlBySalesTransHdrId(
			@PathVariable(value = "salestranshdrId") String salesTransHdrID, Pageable pageable) {
		return salesdtlRepo.findBySalesTransHdrIdOrderByUpdatedTimeDesc(salesTransHdrID, pageable);
	}

	@GetMapping("{companymstid}/salesdtl/{salestranshdrId}/countofsalesdtl")
	public Integer countofSalesDtl(@PathVariable(value = "salestranshdrId") String salesTransHdrID) {
		return salesdtlRepo.countOfSalesDtl(salesTransHdrID);
	}

	@GetMapping("{companymstid}/salesdetails/hsncodesalereport/{branchcode}")
	public List<HsnCodeSaleReport> retrieveHsnCodeSales(@PathVariable(value = "branchcode") String branchcode,
			@RequestParam(value = "fdate") String fdate, @RequestParam(value = "tdate") String tdate) {
		java.util.Date fudate = ClientSystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");

		java.util.Date ftdate = ClientSystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");

		return null;// salesDetailsService.getHSNCodeWiseSalesDtl(fudate, ftdate, branchcode);
	}

	// ---------------------------this url doesn't use
	// salesdtlandoffer---------------------------------------
	@PostMapping("{companymstid}/salestranshdr/{salestranshdrId}/salesdtl")
	public SalesDtl createSalesDtl(@PathVariable(value = "salestranshdrId") String salestranshdrId,
			@PathVariable(value = "companymstid") String companymstid, @Valid @RequestBody SalesDtl salesDtlRequest) {

		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		return saleshdrRepo.findById(salestranshdrId).map(salestranshdr -> {
			salesDtlRequest.setCompanyMst(companyMst.get());

			SalesDtl addedItem = salesdtlRepo.findBySalesTransHdrIdAndItemIdAndBatchAndBarcodeAndUnitIdAndRate(
					salestranshdr.getId(), salesDtlRequest.getItemId(), salesDtlRequest.getBatch(),
					salesDtlRequest.getBarcode(), salesDtlRequest.getUnitId(), salesDtlRequest.getRate());
			if (null != addedItem && null != addedItem.getId()
					&& salesDtlRequest.getUnitId().equalsIgnoreCase(addedItem.getUnitId())) {

				addedItem.setQty(addedItem.getQty() + salesDtlRequest.getQty());
				addedItem.setCgstAmount(salesDtlRequest.getCgstAmount());
				addedItem.setSgstAmount(salesDtlRequest.getSgstAmount());
				addedItem.setCessAmount(salesDtlRequest.getCessAmount());

				addedItem.setAmount(addedItem.getQty() * (salesDtlRequest.getRate() + salesDtlRequest.getCgstAmount()
						+ salesDtlRequest.getSgstAmount()));

				addedItem.setRate(salesDtlRequest.getRate());
				addedItem.setMrp(salesDtlRequest.getMrp());
			//	addedItem = salesdtlRepo.saveAndFlush(addedItem);
				
				
				addedItem=saveAndPublishService.saveSalesDtl(salesDtlRequest, mybranch);
				
				if (null!=addedItem)
				{
			
				saveAndPublishService.publishSalesDtl( addedItem ,  mybranch);
				
				logger.info("saveSalesDtl send to KafkaEvent: {}", addedItem);
				}
				return addedItem;
				

			} else {
				salesDtlRequest.setSalesTransHdr(salestranshdr);

				//SalesDtl salesDtl = salesdtlRepo.saveAndFlush(salesDtlRequest);
				SalesDtl salesDtl=saveAndPublishService.saveSalesDtl(salesDtlRequest, mybranch);
				logger.info("saveSalesDtl send to KafkaEvent: {}", salesDtl);
				return salesDtl;
			}

		}).orElseThrow(() -> new ResourceNotFoundException("salestranshdrId " + salestranshdrId + " not found"));

	}
//add item from pos
	@PostMapping("{companymstid}/salestranshdr/{salestranshdrId}/salesdtlandoffer")
	public SalesDtl createSalesDtlAndOffer(@PathVariable(value = "salestranshdrId") String salestranshdrId,
			@PathVariable(value = "companymstid") String companymstid, @RequestParam("logdate") String logdate,
			@Valid @RequestBody SalesDtl salesDtlRequest) {

		Date loginDate = ClientSystemSetting.StringToUtilDate(logdate, "dd-MM-yyyy");

		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		Optional<SalesTransHdr> salesTransHdrOpt = saleshdrRepo.findById(salestranshdrId);
		
		
		SalesTransHdr salestranshdr = salesTransHdrOpt.get();
		salesDtlRequest.setCompanyMst(companyMst.get());

		String permormaPrinted = "NO";

		if (null != salestranshdr.getPerformaInvoicePrinted()) {
			permormaPrinted = salestranshdr.getPerformaInvoicePrinted();
		} else {
			permormaPrinted = "NO";
		}
		/*
		 * No more add item allwed after performainvoice is printed
		 */
		if (permormaPrinted.equalsIgnoreCase("NO")) {
			 
		}else {
			return null;
		}
		
		
		
			SalesDtl addedItem = salesdtlRepo.findBySalesTransHdrIdAndItemIdAndBatchAndBarcodeAndUnitIdAndRate(
					salestranshdr.getId(), salesDtlRequest.getItemId(), salesDtlRequest.getBatch(),
					salesDtlRequest.getBarcode(), salesDtlRequest.getUnitId(), salesDtlRequest.getRate());

			if (null != addedItem && null != addedItem.getId()
					&& salesDtlRequest.getUnitId().equalsIgnoreCase(addedItem.getUnitId())
					&& !salesDtlRequest.getSalesTransHdr().getSalesMode().equalsIgnoreCase("KOT")) {

				addedItem.setQty(addedItem.getQty() + salesDtlRequest.getQty());
				addedItem.setCgstAmount(addedItem.getCgstAmount() + salesDtlRequest.getCgstAmount());
				addedItem.setSgstAmount(addedItem.getSgstAmount() + salesDtlRequest.getSgstAmount());
				addedItem.setCessAmount(addedItem.getCessAmount() + salesDtlRequest.getCessAmount());
				addedItem.setAmount(addedItem.getQty() * (salesDtlRequest.getRate()));
				double includingTax = (addedItem.getAmount() * salesDtlRequest.getTaxRate()) / 100;
				BigDecimal BrateAfterDiscount = new BigDecimal(
						includingTax + addedItem.getAmount() + addedItem.getCessAmount());
				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_CEILING);
				addedItem.setAmount(BrateAfterDiscount.doubleValue());
				addedItem.setRate(salesDtlRequest.getRate());
				addedItem.setMrp(salesDtlRequest.getMrp());

				//addedItem = salesdtlRepo.saveAndFlush(addedItem);
				addedItem=saveAndPublishService.saveSalesDtl(addedItem, mybranch);
//				addedItem=saveAndPublishService.saveSalesDtl(salesDtlRequest, mybranch);
				
//				if(null!=addedItem) {
//				saveAndPublishService.publishSalesDtl( addedItem ,  mybranch);
//				}

				//===============saving sales dtl to cloud==============by anandu===============04-08-2021=======
//				ResponseEntity<SalesDtl> saved=	externalApi.salesDtlsSavedToCloud(companymstid, addedItem);
				
				
				//new code for offerchecking from 17-12-2021
				if(null == salesDtlRequest.getSalesTransHdr().getKotNumber()) {
				schemeImplementationService.offerCheckingAndOfferApply(addedItem,companyMst.get(),loginDate);
				}
				
				//no need from 17-12-2021
			//	offerChecking(addedItem, companyMst.get(), loginDate);
				// Boolean offer=schemeService.CheckOfferWhileFinalSave(salestranshdr,
				// loginDate);

				// System.out.print(offer+"offer status
				// izzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
				
				return addedItem;

			} else {
				salesDtlRequest.setSalesTransHdr(salestranshdr);

			//	SalesDtl salesDtl = salesdtlRepo.saveAndFlush(salesDtlRequest);
				
				SalesDtl salesDtl = saveAndPublishService.saveSalesDtl(salesDtlRequest, mybranch);
//				
//			     if(null!=salesDtl) {
//				saveAndPublishService.publishSalesDtl( salesDtl ,  mybranch);
//			     }

				
				
				
				if(null != salesDtl)
				{
					insuranceService.parmacyItemAddInsuranceCalculate(salesDtl.getSalesTransHdr());
				}
			//	offerChecking(salesDtl, companyMst.get(), loginDate);
				if(null == salesDtlRequest.getSalesTransHdr().getKotNumber()) {
				String schemeResult=schemeImplementationService.offerCheckingAndOfferApply(salesDtl,companyMst.get(),loginDate);
				
				}
				
				//===============saving sales dtl to cloud==============by anandu===============04-08-2021=======
//				ResponseEntity<SalesDtl> saved=	externalApi.salesDtlsSavedToCloud(companymstid, salesDtlRequest);
				
				return salesDtl;
			}
		
	//	return null;

	}

	@DeleteMapping("{companymstid}/salesdtls/{salesdtlId}")
	public void salesDtlDelete(@PathVariable String salesdtlId) {
		Optional<SalesDtl> SalesDtlOpt = salesdtlRepo.findById(salesdtlId);
		SalesTransHdr salesTransHdr = SalesDtlOpt.get().getSalesTransHdr();

		String permormaPrinted = "NO";

		if (null != salesTransHdr.getPerformaInvoicePrinted()) {
			permormaPrinted = salesTransHdr.getPerformaInvoicePrinted();
		} else {
			permormaPrinted = "NO";
		}

		if (permormaPrinted.equalsIgnoreCase("NO")) {
			SalesDltdDtl salesDltdDtl = new SalesDltdDtl();
			salesDltdDtl.setBarcode(SalesDtlOpt.get().getBarcode());
			salesDltdDtl.setAddCessAmount(SalesDtlOpt.get().getAddCessAmount());
			salesDltdDtl.setBatch(SalesDtlOpt.get().getBatch());
			salesDltdDtl.setCessRate(SalesDtlOpt.get().getCessAmount());
			salesDltdDtl.setCessRate(SalesDtlOpt.get().getCessAmount());
			salesDltdDtl.setCgstAmount(SalesDtlOpt.get().getCgstAmount());
			salesDltdDtl.setCgstTaxRate(SalesDtlOpt.get().getCgstTaxRate());
			salesDltdDtl.setCompanyMst(SalesDtlOpt.get().getCompanyMst());
			salesDltdDtl.setCostPrice(SalesDtlOpt.get().getCostPrice());
			salesDltdDtl.setExpiryDate(SalesDtlOpt.get().getExpiryDate());
			salesDltdDtl.setDiscount(SalesDtlOpt.get().getDiscount());
			salesDltdDtl.setIgstAmount(SalesDtlOpt.get().getIgstAmount());
			salesDltdDtl.setItemId(SalesDtlOpt.get().getItemId());
			salesDltdDtl.setMrp(SalesDtlOpt.get().getMrp());
			salesDltdDtl.setOfferReferenceId(SalesDtlOpt.get().getOfferReferenceId());
			salesDltdDtl.setItemTaxaxId(SalesDtlOpt.get().getItemTaxaxId());
			salesDltdDtl.setSchemeId(SalesDtlOpt.get().getSchemeId());
			salesDltdDtl.setQty(SalesDtlOpt.get().getQty());
			salesDltdDtl.setReturnedQty(SalesDtlOpt.get().getReturnedQty());
			salesDltdDtl.setUnitId(SalesDtlOpt.get().getUnitId());
			salesDltdDtl.setUnitName(SalesDtlOpt.get().getUnitName());
			salesDltdDtl.setFbKey(SalesDtlOpt.get().getFbKey());
			salesDltdDtl.setStandardPrice(SalesDtlOpt.get().getStandardPrice());
			java.sql.Date sqlVoucherDate = SystemSetting.UtilDateToSQLDate(salesTransHdr.getVoucherDate());
			salesDltdDtl.setVoucherDate(sqlVoucherDate);
		    salesDltdDtl = salesDeletedDtlRepository.saveAndFlush(salesDltdDtl);
			
			salesdtlRepo.deleteById(salesdtlId);

		}
		salesdtlRepo.flush();

	}

	private void salesDtlStockUpdate(SalesDtl salesDtl) {
		ItemMst item = itemMstRepository.findById(salesDtl.getItemId()).get();
		List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
				.findByItemIdAndBatchAndBarcode(salesDtl.getItemId(), salesDtl.getBatch(), salesDtl.getBarcode());
		double conversionQty = 0.0;
		if (itembatchmst.size() == 1) {
			ItemBatchMst itemBatchMst = itembatchmst.get(0);
			if (!item.getUnitId().equalsIgnoreCase(salesDtl.getUnitId())) {

				String itemId = item.getId();
				String sourceUnit = salesDtl.getUnitId();
				String targetUnit = item.getUnitId();

				recurssionOff = false;
				conversionQty = multiUnitConversionServiceImpl.getConvertionQty(
						salesDtl.getSalesTransHdr().getCompanyMst().getId(), item.getId(), sourceUnit, targetUnit,
						salesDtl.getQty());
//				conversionQty = getConvertionQty(salesDtl.getSalesTransHdr().getCompanyMst().getId(), item.getId(), sourceUnit,
//						targetUnit, salesDtl.getQty());
				itemBatchMst.setQty(itemBatchMst.getQty() + conversionQty);
			} else {
				itemBatchMst.setQty(itemBatchMst.getQty() + salesDtl.getQty());
			}

			// itemBatchMst.setMrp(salesDtl.getMrp());
			// itemBatchMst.setQty(itemBatchMst.getQty() - salesDtl.getQty());
			itemBatchMst.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());
		    itemBatchMstRepo.saveAndFlush(itemBatchMst);
			
		
		} else {
			ItemBatchMst itemBatchMst = new ItemBatchMst();
			itemBatchMst.setBarcode(salesDtl.getBarcode());
			itemBatchMst.setBatch(salesDtl.getBatch());
			itemBatchMst.setMrp(salesDtl.getMrp());
			itemBatchMst.setQty(salesDtl.getQty());
			itemBatchMst.setItemId(salesDtl.getItemId());
			itemBatchMst.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());
			itemBatchMst.setCompanyMst(salesDtl.getCompanyMst());
			itemBatchMstRepo.saveAndFlush(itemBatchMst);

		}
		ItemBatchMst itemBatchMst = itembatchmst.get(0);
		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		final VoucherNumber voucherNo = voucherService.generateInvoice("STV",
				salesDtl.getSalesTransHdr().getCompanyMst().getId());
		if (!item.getUnitId().equalsIgnoreCase(salesDtl.getUnitId())) {

			String itemId = item.getId();
			String sourceUnit = salesDtl.getUnitId();
			String targetUnit = item.getUnitId();

			recurssionOff = false;
			conversionQty = multiUnitConversionServiceImpl.getConvertionQty(
					salesDtl.getSalesTransHdr().getCompanyMst().getId(), item.getId(), sourceUnit, targetUnit,
					salesDtl.getQty());

//			conversionQty = getConvertionQty(salesDtl.getSalesTransHdr().getCompanyMst().getId(), item.getId(), sourceUnit,
//					targetUnit, salesDtl.getQty());
//			
			itemBatchDtl.setQtyIn(conversionQty);
		} else {

			itemBatchDtl.setQtyIn(salesDtl.getQty());
		}
		itemBatchDtl.setBarcode(salesDtl.getBarcode());
		itemBatchDtl.setBatch(salesDtl.getBatch());
		itemBatchDtl.setItemId(salesDtl.getItemId());
		itemBatchDtl.setMrp(salesDtl.getMrp());
		itemBatchDtl.setQtyOut(0.0);
		itemBatchDtl.setExpDate(itemBatchMst.getExpDate());
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(salesDtl.getSalesTransHdr().getVoucherDate());
		itemBatchDtl.setItemId(salesDtl.getItemId());
		itemBatchDtl.setSourceVoucherNumber(salesDtl.getSalesTransHdr().getVoucherNumber());
		itemBatchDtl.setSourceVoucherDate(salesDtl.getSalesTransHdr().getVoucherDate());
		itemBatchDtl.setCompanyMst(salesDtl.getSalesTransHdr().getCompanyMst());
		itemBatchDtl.setBranchCode(salesDtl.getSalesTransHdr().getBranchCode());

		itemBatchDtl.setStore("MAIN");
		itemBatchDtl.setSourceParentId(salesDtl.getSalesTransHdr().getId());
		itemBatchDtl.setSourceDtlId(salesDtl.getId());
		itemBatchDtl.setParticulars("SALES " + salesDtl.getSalesTransHdr().getAccountHeads().getAccountName());

	//	itemBatchDtl = itemBatchDtlRepository.saveAndFlush(itemBatchDtl);
		itemBatchDtl =saveAndPublishService.saveItemBatchDtl(itemBatchDtl, mybranch);
		logger.info("PurchaseHdr send to KafkaEvent: {}", itemBatchDtl);
		

		/*
		 * Forward Item Batch Dtl.
		 *
		 */

		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", itemBatchDtl.getId());
		variables.put("id", itemBatchDtl.getId());
		variables.put("voucherDate", ClientSystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("REST", 1);
		variables.put("companyid", salesDtl.getSalesTransHdr().getCompanyMst());
		variables.put("WF", "forwardItemBatchDtl");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));

		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlDate = ClientSystemSetting.UtilDateToSQLDate(uDate);

		// java.util.Date uVDate = SystemSetting.SqlDateToUtilDate(sqlDate);
		lmsQueueMst.setVoucherDate(sqlDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType("forwardItemBatchDtl");
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardItemBatchDtl");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName("forwardItemBatchDtl" + itemBatchDtl.getId());
		lmsQueueMst.setJobGroup("forwardItemBatchDtl");
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(itemBatchDtl.getId());

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());

		eventBus.post(variables);

	}

	// Invoice edit
	@DeleteMapping("{companymstid}/salesdtl/salesdtleditdelete/{salesdtlId}")
	public void salesDtlDeleteInvoiceEdit(@PathVariable String salesdtlId) {
		Optional<SalesDtl> salesDtlOpt = salesdtlRepo.findById(salesdtlId);
		if (null == salesDtlOpt.get()) {
			return;
		}
		SalesDtl salesDtl = salesDtlOpt.get();
		salesDtlStockUpdate(salesDtl);
		String salesMode = null;
		if (null != salesDtl.getSalesTransHdr()) {
			if (null != salesDtl.getSalesTransHdr().getSalesMode()) {
				salesMode = salesDtl.getSalesTransHdr().getSalesMode();
			}
		}

		if (null != salesDtl.getOfferReferenceId()) {
			List<SalesDtl> salesDtlList = salesdtlRepo.findByIdAndSalesTransHdr(salesDtl.getOfferReferenceId(),
					salesDtl.getSalesTransHdr());

			if (null != salesMode && salesMode.equalsIgnoreCase("POS")) {
				saveSalesDtlDeleted(salesDtl);
			}

			salesdtlRepo.deleteById(salesdtlId);

			for (SalesDtl sales : salesDtlList) {
				SchemeInstance schemeInstance = schemeInstanceRepository.findByIdAndCompanyMst(sales.getSchemeId(),
						sales.getCompanyMst());

				if (schemeInstance.getEligibilityId().equalsIgnoreCase("2")
						|| schemeInstance.getEligibilityId().equalsIgnoreCase("5")) {

					SchEligibilityAttribInst schEligibilityAttribInst = schEligibilityAttribInstRepository
							.findByCompanyMstAndAttributeNameAndSchemeId(sales.getCompanyMst(), "CATEGORY",
									schemeInstance.getId());

					if (null != schEligibilityAttribInst) {
						Optional<ItemMst> itemOpt = itemMstRepository.findById(sales.getItemId());
						ItemMst itemMst = itemOpt.get();
						if (null != itemMst) {
							Optional<CategoryMst> categOptional = categoryMstRepository
									.findById(itemMst.getCategoryId());
							CategoryMst categoryMst = categOptional.get();

							if (null != categoryMst) {
								Boolean checkCat = false;
								if (schemeInstance.getEligibilityId().equalsIgnoreCase("2")) {
									checkCat = checkCategoryQty(categoryMst, schEligibilityAttribInst, schemeInstance,
											sales);
								}
								if (schemeInstance.getEligibilityId().equalsIgnoreCase("5")) {
									checkCat = checkCategoryAmount(schEligibilityAttribInst, categoryMst, sales,
											schemeInstance);
								}

								if (!checkCat) {
									if (null != salesMode && salesMode.equalsIgnoreCase("POS")) {
										saveSalesDtlDeleted(salesDtl);
									}

									salesdtlRepo.deleteById(sales.getId());
								}
							}
						}

					} else {
						if (null != salesMode && salesMode.equalsIgnoreCase("POS")) {
							saveSalesDtlDeleted(salesDtl);
						}
						salesdtlRepo.deleteById(sales.getId());
					}
				} else {
					if (null != salesMode && salesMode.equalsIgnoreCase("POS")) {
						saveSalesDtlDeleted(salesDtl);
					}
					salesdtlRepo.deleteById(sales.getId());
				}
			}

		} else {
			if (null != salesMode && salesMode.equalsIgnoreCase("POS")) {
				saveSalesDtlDeleted(salesDtl);
			}
			salesdtlRepo.deleteById(salesDtl.getId());
		}
		salesdtlRepo.flush();
	}

	@DeleteMapping("{companymstid}/salesdtlsandoffer/{salesdtlId}")
	public void salesDtlDeleteAndOffer(@PathVariable String salesdtlId) {

		Optional<SalesDtl> salesDtlOpt = salesdtlRepo.findById(salesdtlId);

		if (null == salesDtlOpt.get()) {
			return;
		}
		SalesTransHdr salesTransHdr = salesDtlOpt.get().getSalesTransHdr();
		// cheack weather the salesTransHdr performanceInvoice Status is NO

		String permormaPrinted = "NO";

		if (null != salesTransHdr.getPerformaInvoicePrinted()) {
			permormaPrinted = salesTransHdr.getPerformaInvoicePrinted();
		} else {
			permormaPrinted = "NO";
		}

		if (permormaPrinted.equalsIgnoreCase("NO")) {

			SalesDtl salesDtl = salesDtlOpt.get();
			String salesMode = null;
			if (null != salesDtl.getSalesTransHdr()) {
				if (null != salesDtl.getSalesTransHdr().getSalesMode()) {
					salesMode = salesDtl.getSalesTransHdr().getSalesMode();
				}
			}

			if (null != salesDtl.getOfferReferenceId()) {
				List<SalesDtl> salesDtlList = salesdtlRepo.findByIdAndSalesTransHdr(salesDtl.getOfferReferenceId(),
						salesDtl.getSalesTransHdr());

				if (null != salesMode && salesMode.equalsIgnoreCase("POS")) {
					saveSalesDtlDeleted(salesDtl);
				}

				salesdtlRepo.deleteById(salesdtlId);

				for (SalesDtl sales : salesDtlList) {
					SchemeInstance schemeInstance = schemeInstanceRepository.findByIdAndCompanyMst(sales.getSchemeId(),
							sales.getCompanyMst());

					if (schemeInstance.getEligibilityId().equalsIgnoreCase("2")
							|| schemeInstance.getEligibilityId().equalsIgnoreCase("5")) {

						SchEligibilityAttribInst schEligibilityAttribInst = schEligibilityAttribInstRepository
								.findByCompanyMstAndAttributeNameAndSchemeId(sales.getCompanyMst(), "CATEGORY",
										schemeInstance.getId());

						if (null != schEligibilityAttribInst) {
							Optional<ItemMst> itemOpt = itemMstRepository.findById(sales.getItemId());
							ItemMst itemMst = itemOpt.get();
							if (null != itemMst) {
								Optional<CategoryMst> categOptional = categoryMstRepository
										.findById(itemMst.getCategoryId());
								CategoryMst categoryMst = categOptional.get();

								if (null != categoryMst) {
									Boolean checkCat = false;
									if (schemeInstance.getEligibilityId().equalsIgnoreCase("2")) {
										checkCat = checkCategoryQty(categoryMst, schEligibilityAttribInst,
												schemeInstance, sales);
									}
									if (schemeInstance.getEligibilityId().equalsIgnoreCase("5")) {
										checkCat = checkCategoryAmount(schEligibilityAttribInst, categoryMst, sales,
												schemeInstance);
									}

									if (!checkCat) {
										if (null != salesMode && salesMode.equalsIgnoreCase("POS")) {
											saveSalesDtlDeleted(salesDtl);
										}

										salesdtlRepo.deleteById(sales.getId());
									}
								}
							}

						} else {
							if (null != salesMode && salesMode.equalsIgnoreCase("POS")) {
								saveSalesDtlDeleted(salesDtl);
							}
							salesdtlRepo.deleteById(sales.getId());
						}
					} else {
						if (null != salesMode && salesMode.equalsIgnoreCase("POS")) {
							saveSalesDtlDeleted(salesDtl);
						}
						salesdtlRepo.deleteById(sales.getId());
					}
				}

			} else {
				if (null != salesMode && salesMode.equalsIgnoreCase("POS")) {
					saveSalesDtlDeleted(salesDtl);
				}
				salesdtlRepo.deleteById(salesDtl.getId());
				
				
					insuranceService.parmacyItemAddInsuranceCalculate(salesTransHdr);
				
				saveSalesDtlDeleted(salesDtl);
			}
			salesdtlRepo.flush();

		}
	}

	private void saveSalesDtlDeleted(SalesDtl sales) {

		SalesDtlDeleted salesDtlDeleted = new SalesDtlDeleted();
		salesDtlDeleted.setAddCessRate(sales.getAddCessRate());
		salesDtlDeleted.setRate(sales.getRate());
		salesDtlDeleted.setAmount(sales.getAmount());
		salesDtlDeleted.setBarcode(sales.getBarcode());
		salesDtlDeleted.setBatch(sales.getBatch());
		salesDtlDeleted.setCessAmount(sales.getCessAmount());
		salesDtlDeleted.setCessRate(sales.getCessRate());
		salesDtlDeleted.setCgstAmount(sales.getCgstAmount());
		salesDtlDeleted.setCgstTaxRate(sales.getCgstTaxRate());
		salesDtlDeleted.setCompanyMst(sales.getCompanyMst());
		salesDtlDeleted.setDiscount(sales.getDiscount());
		salesDtlDeleted.setExpiryDate(sales.getExpiryDate());
		salesDtlDeleted.setIgstAmount(sales.getIgstAmount());
		salesDtlDeleted.setIgstTaxRate(sales.getIgstTaxRate());
		salesDtlDeleted.setItemId(sales.getItemId());
		salesDtlDeleted.setItemName(sales.getItemName());
		salesDtlDeleted.setItemTaxaxId(sales.getItemTaxaxId());
		salesDtlDeleted.setMrp(sales.getMrp());
		salesDtlDeleted.setOfferReferenceId(sales.getOfferReferenceId());
		salesDtlDeleted.setQty(sales.getQty());
		salesDtlDeleted.setReturnedQty(sales.getReturnedQty());
		salesDtlDeleted.setSalesTransHdr(sales.getSalesTransHdr());
		salesDtlDeleted.setSchemeId(sales.getSchemeId());
		salesDtlDeleted.setSgstAmount(sales.getSgstAmount());
		salesDtlDeleted.setSgstTaxRate(sales.getSgstTaxRate());
		salesDtlDeleted.setStandardPrice(sales.getStandardPrice());
		salesDtlDeleted.setStatus(sales.getStatus());
		salesDtlDeleted.setTaxRate(sales.getTaxRate());
		salesDtlDeleted.setUnitId(sales.getUnitId());
		salesDtlDeleted.setUnitName(sales.getUnitName());
		salesDtlDeleted.setUpdatedTime(sales.getUpdatedTime());
		salesDtlDeleted.setBranchCode(sales.getSalesTransHdr().getBranchCode());
		salesDtlDeletedRepository.saveAndFlush(salesDtlDeleted);
		/*
		 * forward sales deleted details
		 */
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("companyid", salesDtlDeleted.getCompanyMst());

		variables.put("voucherNumber", salesDtlDeleted.getId());
		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("id", salesDtlDeleted.getId());

		variables.put("branchcode", salesDtlDeleted.getBranchCode());

		if (serverorclient.equalsIgnoreCase("REST")) {
			variables.put("REST", 1);
		} else {
			variables.put("REST", 0);
		}

		// variables.put("voucherDate", purchase.getVoucherDate());
		// variables.put("id",purchase.getId());
		variables.put("inet", 0);

		variables.put("WF", "forwardSalesDeletedDtl");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));

		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardSalesDeletedDtl");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);

		variables.put("lmsqid", lmsQueueMst.getId());

		eventBus.post(variables);
	}

	@PutMapping("{companymstid}/salesdetail/{dtlid}/updatesalesdeatilreturn")
	public SalesDtl updateSalesDtlByReturn(@PathVariable(value = "dtlid") String dtlid,
			@PathVariable(value = "companymstid") String companymstid, @Valid @RequestBody SalesDtl salesDtlRequest) {

		SalesDtl salesDtl = salesdtlRepo.findById(dtlid).get();

		salesDtl.setStatus("RETURNED");
		salesDtl.setReturnedQty(salesDtlRequest.getReturnedQty());

		//salesDtl = salesdtlRepo.save(salesDtl);
		salesDtl =saveAndPublishService.saveSalesDtl(salesDtl, mybranch);
		logger.info("PurchaseHdr send to KafkaEvent: {}", salesDtl);
		return salesDtl;

	}

	@PutMapping("{companymstid}/salesdetail/{dtlid}/updatefcfields")
	public SalesDtl updateSalesFCFields(@PathVariable(value = "dtlid") String dtlid,
			@PathVariable(value = "companymstid") String companymstid, @Valid @RequestBody SalesDtl salesDtlRequest) {

		SalesDtl salesDtl = salesdtlRepo.findById(dtlid).get();

		salesDtl.setFcAmount(salesDtlRequest.getFcAmount());
		salesDtl.setFcDiscount(salesDtlRequest.getFcDiscount());
		salesDtl.setFcIgstAmount(salesDtlRequest.getFcIgstAmount());
		salesDtl.setFcIgstRate(salesDtlRequest.getFcIgstRate());
		salesDtl.setFcMrp(salesDtlRequest.getFcMrp());
		salesDtl.setFcRate(salesDtlRequest.getFcRate());
		salesDtl.setFcStandardPrice(salesDtlRequest.getFcStandardPrice());
		salesDtl.setFcTaxRate(salesDtlRequest.getFcTaxRate());
		salesDtl.setFcTaxAmount(salesDtlRequest.getFcTaxAmount());
		//salesDtl = salesdtlRepo.save(salesDtl);
		salesDtl =saveAndPublishService.saveSalesDtl(salesDtl, mybranch);
		logger.info("PurchaseHdr send to KafkaEvent: {}", salesDtl);

		return salesDtl;

	}

	@PutMapping("{companymstid}/salestranshdr/{salestransHdrId}/salesdtl/{salesdtlId}")
	public SalesDtl updateSalesDtl(@PathVariable(value = "salestransHdrId") String salestransHdrId,
			@PathVariable(value = "salesdtlId") String salesdtlId, @Valid @RequestBody SalesDtl salesDtlRequest) {
		if (!saleshdrRepo.existsById(salestransHdrId)) {
			throw new ResourceNotFoundException("salestransHdrId " + salestransHdrId + " not found");
		}

		return salesdtlRepo.findById(salesdtlId).map(salesDtl -> {
			salesDtl.setQty(salesDtlRequest.getQty());

			//return salesdtlRepo.saveAndFlush(salesDtlRequest);
			return salesDtl =saveAndPublishService.saveSalesDtl(salesDtl, mybranch);
		
		}).orElseThrow(() -> new ResourceNotFoundException("salestransHdrId " + salestransHdrId + "not found"));
	}

	@GetMapping("{companymstid}/salestranshdr/{salesTransHdrId}/saleswindowsummary")
	public SummarySalesDtl getSalesWindowSummaryByHdrId(
			@PathVariable(value = "salesTransHdrId") String salesTransHdrId) {

		return salessummary(salesTransHdrId);

	}

	@GetMapping("{companymstid}/salestranshdr/{salesTransHdrId}/fcsaleswindowsummary")
	public FCSummarySalesDtl getFCSalesWindowSummaryByHdrId(
			@PathVariable(value = "salesTransHdrId") String salesTransHdrId) {

		return fcsalessummary(salesTransHdrId);

	}

	private FCSummarySalesDtl fcsalessummary(String salesTransHdrId) {
		List<Object> objList = salesdtlRepo.findFCSumSalesDtl(salesTransHdrId);

		Object[] objAray = (Object[]) objList.get(0);

		FCSummarySalesDtl summary = new FCSummarySalesDtl();
		summary.setTotalQty((Double) objAray[0]);
		summary.setFctotalAmount((Double) objAray[1]);
		summary.setFctotalTax((Double) objAray[2]);
		summary.setFctotalCessAmt((Double) objAray[3]);

		return summary;

	}

	// for customer discount new url for summary

	@GetMapping("{companymstid}/salestranshdr/{salesTransHdrId}/saleswindowsummaryfordiscount")
	public SummarySalesDtl getSalesWindowDiscountSummaryByHdrId(
			@PathVariable(value = "salesTransHdrId") String salesTransHdrId) {

		return salessummaryDiscount(salesTransHdrId);

	}

	@GetMapping("{companymstid}/salestranshdr/{salesTransHdrId}/fcsaleswindowsummaryfordiscount")
	public FCSummarySalesDtl getFCSalesWindowDiscountSummaryByHdrId(
			@PathVariable(value = "salesTransHdrId") String salesTransHdrId) {

		return fcsalessummaryDiscount(salesTransHdrId);

	}

	private FCSummarySalesDtl fcsalessummaryDiscount(String salesTransHdrId) {
		List<Object> objList = salesdtlRepo.findFCSumSalesDtlforDiscount(salesTransHdrId);

		Object[] objAray = (Object[]) objList.get(0);

		FCSummarySalesDtl summary = new FCSummarySalesDtl();
		summary.setTotalQty((Double) objAray[0]);
		summary.setFctotalAmount((Double) objAray[1]);
		summary.setFctotalTax((Double) objAray[2]);
		summary.setFctotalCessAmt((Double) objAray[3]);

		return summary;

	}

	private SummarySalesDtl salessummary(String salesTransHdrId) {
		List<Object> objList = salesdtlRepo.findSumSalesDtl(salesTransHdrId);

		Object[] objAray = (Object[]) objList.get(0);

		SummarySalesDtl summary = new SummarySalesDtl();
		summary.setTotalQty((Double) objAray[0]);
		summary.setTotalAmount((Double) objAray[1]);
		summary.setTotalTax((Double) objAray[2]);
		summary.setTotalCessAmt((Double) objAray[3]);

		return summary;

	}

	private SummarySalesDtl salessummaryDiscount(String salesTransHdrId) {

		Optional<SalesTransHdr> salesTransHdrOpt = saleshdrRepo.findById(salesTransHdrId);
		SalesTransHdr salesTransHdr = salesTransHdrOpt.get();

		if (null == salesTransHdr) {
			return null;
		}

		AccountHeads accountHeads = salesTransHdr.getAccountHeads();

		SummarySalesDtl summary = new SummarySalesDtl();

		if (accountHeads.getPartyGst().length() > 13) {
			List<Object> objList = salesdtlRepo.findSumSalesDtlforDiscount(salesTransHdrId);

			Object[] objAray = (Object[]) objList.get(0);

			summary.setTotalQty((Double) objAray[0]);
			summary.setTotalAmount((Double) objAray[1]);
			summary.setTotalTax((Double) objAray[2]);
			summary.setTotalCessAmt((Double) objAray[3]);
		} else {

			List<Object> objList = salesdtlRepo.findSumSalesDtlforB2CDiscount(salesTransHdrId);

			Object[] objAray = (Object[]) objList.get(0);

			summary.setTotalQty((Double) objAray[0]);
			summary.setTotalAmount((Double) objAray[1]);
			summary.setTotalTax((Double) objAray[2]);
			summary.setTotalCessAmt((Double) objAray[3]);
		}

		return summary;

	}

	@GetMapping("/{companymstid}/salesdetail")
	public List<SalesDtl> retrieveAllreceivabledtls() {
		return salesdtlRepo.findAll();
	}

	@GetMapping("{companymstid}/salesdetail/{salestranshdrId}/{itemId}/getsalesdetailqty")
	public Double retrieveAllSalesDtlQtyById(@PathVariable(value = "salestranshdrId") String salesTransHdrID,
			@PathVariable(value = "itemId") String itemId,

			@PathVariable(value = "companymstid") String companymstid) {

		// return
		// salesDetailsService.getSalesDetailItemQty(companymstid,salesTransHdrID,itemId,batch);
		return salesdtlRepo.getSalesDetailItemQty(salesTransHdrID, itemId);
	}

	@GetMapping("{companymstid}/salesdetail/bybatch/{salestranshdrId}/{itemId}/getsalesdetailqty")
	public Double retrieveAllSalesDtlQtyByIdAndBatch(@PathVariable(value = "salestranshdrId") String salesTransHdrID,
			@PathVariable(value = "itemId") String itemId,

			@PathVariable(value = "companymstid") String companymstid, @RequestParam(value = "batch") String batch) {

		// return
		// salesDetailsService.getSalesDetailItemQty(companymstid,salesTransHdrID,itemId,batch);
		return salesdtlRepo.getSalesDetailItemQtyByBatch(salesTransHdrID, itemId, batch);
	}

	@GetMapping("{companymstid}/salesdetail/{salestranshdrId}/{itemId}/{batch}/getsalesdetailsbyitem")
	public List<SalesDtl> retrieveAllSalesDtlByItemIdAndBatch(
			@PathVariable(value = "salestranshdrId") String salesTransHdrID,
			@PathVariable(value = "itemId") String itemId, @PathVariable(value = "batch") String batch,
			@PathVariable(value = "companymstid") String companymstid) {

		// return
		// salesDetailsService.getSalesDetailItemQty(companymstid,salesTransHdrID,itemId,batch);
		return salesdtlRepo.findBySalesTransHdrIdAndItemIdAndBatch(salesTransHdrID, itemId, batch);
	}

	@GetMapping("{companymstid}/taxrate/{salestranshdrId}")
	public List<Object> retrieveAllTaxRate(@PathVariable(value = "salestranshdrId") String salesTransHdrID) {
		return salesdtlRepo.getTaxRate(salesTransHdrID);
	}

	@GetMapping("{companymstid}/itemtaxrate/{salestranshdrId}/{itemid}")
	public List<Double> retrieveItemWiseTaxRate(@PathVariable(value = "salestranshdrId") String salesTransHdrId,
			@PathVariable(value = "itemid") String itemid) {
		return salesdtlRepo.getItemWiseTaxRate(salesTransHdrId, itemid);
	}

	@GetMapping("{companymstid}/taxamount/{salestranshdrId}/{taxrate}")
	public Double retrieveTaxAmount(@PathVariable(value = "salestranshdrId") String salesTransHdrId,
			@PathVariable(value = "taxrate") Double taxrate) {
		return salesdtlRepo.retrieveTaxAmount(salesTransHdrId, taxrate).doubleValue();
	}

	@GetMapping("{companymstid}/salesdtl/{dtlid}/getsalesdtlbyid")
	public Optional<SalesDtl> retrieveSalesDtlbyId(@PathVariable(value = "dtlid") String dtlid) {
		return salesdtlRepo.findById(dtlid);
	}

	@GetMapping("{companymstid}/salesdetails/gettotalcesssummary/{salestranshdrId}")
	public Double retrieveAllCessAmount(@PathVariable(value = "salestranshdrId") String salesTransHdrId) {
		return salesdtlRepo.retrieveAllCessAmount(salesTransHdrId).doubleValue();
	}

	@GetMapping("{companymstid}/gettotaltaxsummary/{salestranshdrId}")
	public Double retrieveAllTaxAmount(@PathVariable(value = "salestranshdrId") String salesTransHdrId) {
		return salesdtlRepo.retrieveAllTaxAmount(salesTransHdrId).doubleValue();
	}

	@GetMapping("{companymstid}/getgrandtotalincludingtax/{salestranshdrId}")
	public Double retrieveGrandTotalincludingTax(@PathVariable(value = "salestranshdrId") String salesTransHdrId) {
		return salesdtlRepo.retrieveGrandTotalincludingTax(salesTransHdrId).doubleValue();
	}
	// this url is used when pos window having discount
//	@GetMapping("{companymstid}/salesdtl/grandtotlaincludingtax/{salestranshdrId}")
//	public Double getGrandTotalIncTaz(@PathVariable(value = "salestranshdrId") String salesTransHdrId) {
//		return salesdtlRepo.retrieveGrandTotalincludingTaxForPOsDiscount(salesTransHdrId).doubleValue();
//	}

	@GetMapping("{companymstid}/getsalestranshdr/{salestranshdrId}")
	public List<Object> retrieveSalesTransHdr(@PathVariable(value = "salestranshdrId") String salesTransHdrId) {
		return salesdtlRepo.getsalesHdrDtl(salesTransHdrId);
	}

	@GetMapping("{companymstid}/getitemdetals/{salestranshdrId}")
	public List<Object> retrieveItemDetails(@PathVariable(value = "salestranshdrId") String salesTransHdrId) {
		return salesdtlRepo.getItemDetails(salesTransHdrId);
	}

	@GetMapping("{companymstid}/salesdtlsbyitemid/{salestranshdrId}/{itemid}")
	public List<SalesDtl> retrieveItemWiseSalesDtl(@PathVariable(value = "salestranshdrId") String salesTransHdrId,
			@PathVariable(value = "itemid") String itemid, @PathVariable(value = "companymstid") String companymstid) {

		Optional<SalesTransHdr> salesTransHdrOpt = saleshdrRepo.findById(salesTransHdrId);

		return salesdtlRepo.findBySalesTransHdrAndItemId(salesTransHdrOpt.get(), itemid);
	}

	@GetMapping("{companymstid}/salesdtlbycategoryid/{salestranshdrId}/{categoryid}")
	public List<SalesDtl> getSalesDtlByCategoryIdAndSalesTransHsr(
			@PathVariable(value = "salestranshdrId") String salestranshdrId,
			@PathVariable(value = "categoryid") String categoryid,
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<SalesTransHdr> salesTransHdrOpt = saleshdrRepo.findById(salestranshdrId);

		return salesDetailsService.findBySalesTransHdrAndCategoryIdd(salestranshdrId, categoryid);
//			return salesdtlRepo.findBySalesTransHdrAndCategoryIdd(salestranshdrId,categoryid);
	}

	@GetMapping("{companymstid}/salesdtlbyitemidandbatchandsalestranshdrid/{itemid}/{batch}/{salestranshdrid}")
	public List<SalesDtl> getSalesDtlByItemIdAndBatchAndSalesTransHdr(@PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "batch") String batch,
			@PathVariable(value = "salestranshdrid") String salestranshdrid,
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<SalesTransHdr> salesTransHdrOpt = saleshdrRepo.findById(salestranshdrid);

//			return salesDetailsService.findBySalesTransHdrAndCategoryIdd(salestranshdrId,categoryid);
		return salesdtlRepo.findByItemIdAndBatchAndSalesTransHdr(itemid, batch, salesTransHdrOpt.get());
	}

	@GetMapping("{companymstid}/salesdtlbyofferreferenceid/{offerreferenceid}/{salestranshdrid}")
	public List<SalesDtl> getSalesDtlByOfferReferenceId(
			@PathVariable(value = "offerreferenceid") String offerreferenceid,
			@PathVariable(value = "salestranshdrid") String salestranshdrid,
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<SalesTransHdr> salesTransHdrOpt = saleshdrRepo.findById(salestranshdrid);

//			return salesDetailsService.findBySalesTransHdrAndCategoryIdd(salestranshdrId,categoryid);
		return salesdtlRepo.findByIdAndSalesTransHdr(offerreferenceid, salesTransHdrOpt.get());
	}

	@PutMapping("{companyid}/updatesalesdtl/{id}")
	public SalesDtl salesDtlUpdate(@PathVariable(value = "id") String id,
			@PathVariable(value = "companyid") String companyid, @Valid @RequestBody SalesDtl salesDtlRequest) {

		return salesdtlRepo.findById(id).map(salesDtl -> {

			salesDtl.setOfferReferenceId(salesDtlRequest.getOfferReferenceId());
			;

			//salesDtl = salesdtlRepo.save(salesDtl);
			 salesDtl =saveAndPublishService.saveSalesDtl(salesDtl, mybranch);

			return salesDtl;

		}).orElseThrow(() -> new ResourceNotFoundException("salesDtls " + salesDtlRequest + " not found"));

	}

//----------------------------------------OFFER CHECKING--------------------------------------------------------		

	private void offerChecking(SalesDtl addedItem, CompanyMst companyMst, Date loginDate) {

		String salesMode = null;

		SalesTransHdr salesTransHdr = addedItem.getSalesTransHdr();
		if (null != salesTransHdr) {
			if (null == salesTransHdr.getSalesMode()) {
				return;
			}
		}

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("POS")) {

			salesMode = "Retail";

		}
		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2B")
				|| salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			salesMode = "WholeSale";
		}

		if (null == salesMode) {
			return;
		}

		List<SchemeInstance> schemeInstanceList = schemeInstanceRepository
				.findByCompanyMstAndSchemeWholesaleRetailAndIsActive(companyMst.getCompanyName(), salesMode, "YES");

		for (SchemeInstance scheme : schemeInstanceList) {
			Boolean validityPeriod = CheckValidityPeriod(scheme, companyMst, addedItem, loginDate);

			if (validityPeriod) {
				if (scheme.getOfferId().equalsIgnoreCase("2") && !scheme.getEligibilityId().equalsIgnoreCase("7")) {

					Boolean eligible = eligibilityCheck(companyMst, scheme, addedItem);
					if (!eligible) {
//						if (null != addedItem.getOfferReferenceId()) {
//							Optional<SalesDtl> salesDtlOpt = salesdtlRepo.findById(addedItem.getOfferReferenceId());
//							if (!salesDtlOpt.equals(null)) {
//								if (null != salesDtlOpt.get())
//									salesdtlRepo.deleteById(addedItem.getOfferReferenceId());
//							}
//						}

						return;
					}

				}

			}
		}
	}

	private Boolean eligibilityCheck(CompanyMst companyMst, SchemeInstance scheme, SalesDtl salesDtl) {

		Optional<ItemMst> itemMstOpt = itemMstRepository.findById(salesDtl.getItemId());
		ItemMst itemMst = itemMstOpt.get();

		List<SchEligibilityAttribInst> schEligibilityAttrbInstList = schEligibilityAttribInstRepository
				.findByCompanyMstAndSchemeIdAndEligibilityId(companyMst, scheme.getId(), scheme.getEligibilityId());

		for (SchEligibilityAttribInst inst : schEligibilityAttrbInstList) {

			if (inst.getEligibilityId().equals("1")) {

				SchEligibilityAttribInst schEligibilityAttribInst = schEligibilityAttribInstRepository
						.findByCompanyMstAndAttributeNameAndSchemeId(companyMst, "ITEMNAME", scheme.getId());

				if (schEligibilityAttribInst.getAttributeValue().equals(itemMst.getItemName())) {
					Boolean validQty = checkItemAndQty(inst, itemMst, scheme, salesDtl);
					if (!validQty) {

						return false;

					} else {
						return true;
					}
				}

			}

			if (inst.getEligibilityId().equals("2")) {

				SchEligibilityAttribInst schEligibilityAttribInst = schEligibilityAttribInstRepository
						.findByCompanyMstAndAttributeNameAndSchemeId(companyMst, "CATEGORY", scheme.getId());

				Optional<CategoryMst> categoryMstResp = categoryMstRepository.findById(itemMst.getCategoryId());
				CategoryMst categoryMst = categoryMstResp.get();

				if (schEligibilityAttribInst.getAttributeValue().equals(categoryMst.getCategoryName())) {
					Boolean validCat = checkCategoryQty(categoryMst, inst, scheme, salesDtl);
					if (!validCat) {
						return false;
					} else {
						return true;
					}
				}

			}

			if (inst.getEligibilityId().equals("4")) {

				SchEligibilityAttribInst schEligibilityAttribInst = schEligibilityAttribInstRepository
						.findByCompanyMstAndAttributeNameAndSchemeId(companyMst, "ITEMNAME", scheme.getId());

				if (schEligibilityAttribInst.getAttributeValue().equals(itemMst.getItemName())) {
					Boolean validAmount = checkItemAndAmount(inst, itemMst, scheme, salesDtl);
					if (!validAmount) {
						return false;
					} else {
						return true;
					}
				}

			}

			if (inst.getEligibilityId().equals("5")) {

				SchEligibilityAttribInst schEligibilityAttribInst = schEligibilityAttribInstRepository
						.findByCompanyMstAndAttributeNameAndSchemeId(companyMst, "CATEGORY", scheme.getId());

				Optional<CategoryMst> categoryMstResp = categoryMstRepository.findById(itemMst.getCategoryId());
				CategoryMst categoryMst = categoryMstResp.get();

				if (schEligibilityAttribInst.getAttributeValue().equals(categoryMst.getCategoryName())) {
					Boolean validCatAmount = checkCategoryAmount(inst, categoryMst, salesDtl, scheme);
					if (!validCatAmount) {
						return false;
					} else {
						return true;
					}
				}

			}

			if (inst.getEligibilityId().equals("8")) {

				Boolean validItemBtach = CheckItemBatchValidity(inst, salesDtl, itemMst, scheme);
				if (!validItemBtach) {
					return false;
				}

			}

		}

		return true;

	}

	private Boolean CheckItemBatchValidity(SchEligibilityAttribInst inst, SalesDtl salesDtl, ItemMst itemMst,
			SchemeInstance scheme) {

		List<SchEligibilityAttribInst> eligiblrList = schEligibilityAttribInstRepository
				.findByCompanyMstAndSchemeIdAndEligibilityId(salesDtl.getCompanyMst(), scheme.getId(),
						scheme.getEligibilityId());

		if (eligiblrList.size() <= 0) {
			return false;
		}
		String itemName = "";
		String batchCode = "";
		Double qty = 0.0;

		for (SchEligibilityAttribInst eligible : eligiblrList) {
			if (eligible.getAttributeName().equalsIgnoreCase("ITEMNAME")) {
				if (null == eligible.getAttributeValue()) {
					return false;
				}
				itemName = eligible.getAttributeValue();
			}

			if (eligible.getAttributeName().equalsIgnoreCase("QTY")) {
				if (null == eligible.getAttributeValue()) {
					return false;
				}
				qty = Double.parseDouble(eligible.getAttributeValue());
			}

			if (eligible.getAttributeName().equalsIgnoreCase("BATCH_CODE")) {

				if (null == eligible.getAttributeValue()) {
					return false;
				}
				batchCode = eligible.getAttributeValue();
			}

		}

		if (itemName.equals(itemMst.getItemName()) && batchCode.equals(salesDtl.getBatch())) {

			List<SalesDtl> salesDtlList = salesdtlRepo.findBySalesTransHdrIdAndItemIdAndBatch(
					salesDtl.getSalesTransHdr().getId(), salesDtl.getItemId(), salesDtl.getBatch());

			if (salesDtlList.size() <= 0) {
				return false;
			}
			Double salesQty = 0.0, conversionQty = 0.0;
			for (SalesDtl sales : salesDtlList) {
				Optional<ItemMst> itemMstOpt = itemMstRepository.findById(sales.getItemId());
				itemMst = itemMstOpt.get();

				if (null != itemMst) {
					if (!itemMst.getUnitId().equals(sales.getUnitId())) {
						conversionQty = multiUnitConversionServiceImpl.getConvertionQty(sales.getCompanyMst().getId(),
								itemMst.getId(), sales.getUnitId(), itemMst.getUnitId(), sales.getQty());

						salesQty = salesQty + conversionQty;

					} else {
						salesQty = salesQty + sales.getQty();
					}

				}
			}

			if (qty <= salesQty) {

				if (scheme.getOfferId().equals("2")) {

					AddOfferQtyCheck(scheme, salesDtl, salesQty, "ITEM_AND_BATCH", 0.0);
					return true;
				}

				if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

					AddAmountDiscount(scheme, salesDtl, salesQty, "ITEM_AND_BATCH");
					return true;
				}

			}

		}
		return false;

	}

	private Boolean checkCategoryAmount(SchEligibilityAttribInst inst, CategoryMst categoryMst, SalesDtl salesDtl,
			SchemeInstance scheme) {

		Double amount = 0.0;

		List<SalesDtl> salesDtlList = salesDetailsService
				.findBySalesTransHdrAndCategoryIdd(salesDtl.getSalesTransHdr().getId(), categoryMst.getId());

		if (salesDtlList.size() > 0) {
			for (SalesDtl sales : salesDtlList) {
				amount = amount + sales.getAmount();
			}
		} else {
			return false;
		}

		String offerRefId = salesDtlList.get(0).getOfferReferenceId();
		salesDtl.setOfferReferenceId(offerRefId);

		SchEligibilityAttribInst schEligibilityAttribInst = schEligibilityAttribInstRepository
				.findByCompanyMstAndAttributeNameAndSchemeId(salesDtl.getCompanyMst(), "AMOUNT", scheme.getId());

		Double offerAmount = Double.parseDouble(schEligibilityAttribInst.getAttributeValue());

		if (amount >= offerAmount) {

			if (scheme.getOfferId().equals("2")) {

				AddOfferQtyCheck(scheme, salesDtl, offerAmount, "CATEGORY_AND_QTY", amount);
				return true;
			}
			if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

				AddAmountDiscount(scheme, salesDtl, offerAmount, "ITEM_AND_QTY");
				return true;
			}
		}

		return false;
	}

	private Boolean checkItemAndAmount(SchEligibilityAttribInst inst, ItemMst itemMst, SchemeInstance scheme,
			SalesDtl salesDtl) {

		Double amount = 0.0;

		List<SalesDtl> salesDtlList = salesdtlRepo.findBySalesTransHdrAndItemId(salesDtl.getSalesTransHdr(),
				itemMst.getId());

		if (salesDtlList.size() <= 0) {

			return false;

		}

		String offerRefId = salesDtlList.get(0).getOfferReferenceId();
		salesDtl.setOfferReferenceId(offerRefId);

		for (SalesDtl salesdtl : salesDtlList) {

			amount = amount + salesdtl.getAmount();

		}

		SchEligibilityAttribInst schEligibilityAttribInst = schEligibilityAttribInstRepository
				.findByCompanyMstAndAttributeNameAndSchemeId(salesDtl.getCompanyMst(), "AMOUNT", scheme.getId());

		Double offerAmount = Double.parseDouble(schEligibilityAttribInst.getAttributeValue());
		if (amount >= offerAmount) {

			if (scheme.getOfferId().equals("2")) {

				AddOfferQtyCheck(scheme, salesDtl, offerAmount, "ITEM_AND_QTY", amount);
				return true;
			}

			if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

				AddAmountDiscount(scheme, salesDtl, offerAmount, "ITEM_AND_QTY");
				return true;
			}

		}

		return false;
	}

	private Boolean checkCategoryQty(CategoryMst categoryMst, SchEligibilityAttribInst inst, SchemeInstance scheme,
			SalesDtl salesDtl) {

		Double qty = 0.0, conversionQty = 0.0;
		String offerRefId = null;

		List<SalesDtl> salesDtlList = salesDetailsService
				.findBySalesTransHdrAndCategoryIdd(salesDtl.getSalesTransHdr().getId(), categoryMst.getId());

		if (salesDtlList.size() > 0) {
			offerRefId = salesDtlList.get(0).getOfferReferenceId();

			for (SalesDtl sales : salesDtlList) {
				Optional<ItemMst> itemOpt = itemMstRepository.findById(sales.getItemId());
				ItemMst itemMst = itemOpt.get();
				if (null != itemMst) {

					if (!itemMst.getUnitId().equals(sales.getUnitId())) {
						conversionQty = multiUnitConversionServiceImpl.getConvertionQty(sales.getCompanyMst().getId(),
								itemMst.getId(), sales.getUnitId(), itemMst.getUnitId(), sales.getQty());

						qty = qty + conversionQty;
					} else {
						qty = qty + sales.getQty();
					}

				}

			}
		}

		SchEligibilityAttribInst schEligibilityAttribInst = schEligibilityAttribInstRepository
				.findByCompanyMstAndAttributeNameAndSchemeId(salesDtl.getCompanyMst(), "QTY", scheme.getId());

		Double instQty = Double.parseDouble(schEligibilityAttribInst.getAttributeValue());
		salesDtl.setOfferReferenceId(offerRefId);

		salesDtl.setQty(qty);

		if (qty >= instQty) {

			if (scheme.getOfferId().equals("2")) {

				AddOfferQtyCheck(scheme, salesDtl, instQty, "CATEGORY_AND_QTY", 0.0);
				return true;
			}

			if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

				AddAmountDiscount(scheme, salesDtl, instQty, "ITEM_AND_QTY");
				return true;
			}

		}

		return false;
	}

	private void AddAmountDiscount(SchemeInstance scheme, SalesDtl salesDtl, Double instQty, String string) {

		Double discount = 0.0;

		SalesTransHdr salesTransHdr = salesDtl.getSalesTransHdr();
		SummarySalesDtl summary = salessummary(salesTransHdr.getId());

		List<SchOfferAttrInst> offerInstList = schOfferAttrInstRepository
				.findByCompanyMstAndSchemeId(salesDtl.getCompanyMst(), scheme.getId());
		for (SchOfferAttrInst offer : offerInstList) {
			if (offer.getAttributeName().equalsIgnoreCase("DISCOUNT")) {
				discount = Double.parseDouble(offer.getAttributeValue());
			}
			if (offer.getAttributeName().equalsIgnoreCase("PERCENTAGE DISCOUNT")) {
				discount = Double.parseDouble(offer.getAttributeValue());
			}
		}

		if (scheme.getOfferId().equals("1")) {

			salesTransHdr.setInvoiceDiscount(discount);

			discount = (discount / summary.getTotalAmount()) * 100;
			String discountper = discount + "%";

			salesTransHdr.setDiscount(discountper);
		}
		if (scheme.getOfferId().equals("4")) {

			salesTransHdr.setDiscount(discount + "%");

			discount = summary.getTotalAmount() * discount / 100;
			salesTransHdr.setInvoiceDiscount(discount);

		}

		return;

	}

	private Boolean checkItemAndQty(SchEligibilityAttribInst inst, ItemMst itemMst, SchemeInstance scheme,
			SalesDtl salesDtl) {

		SchEligibilityAttribInst schEligibilityAttribInst = schEligibilityAttribInstRepository
				.findByCompanyMstAndAttributeNameAndSchemeId(salesDtl.getCompanyMst(), "QTY", scheme.getId());

		Double eligQty = Double.parseDouble(schEligibilityAttribInst.getAttributeValue());

		if (eligQty <= salesDtl.getQty()) {

			if (scheme.getOfferId().equals("2")) {

				AddOfferQtyCheck(scheme, salesDtl, eligQty, "ITEM_AND_QTY", 0.0);
				return true;
			}

			if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

				AddAmountDiscount(scheme, salesDtl, eligQty, "ITEM_AND_QTY");
				return true;
			}
			// privilage points remains---- done by client requirement customer

		}

		return false;
	}

	private void AddOfferQtyCheck(SchemeInstance scheme, SalesDtl salesDtl, Double eligQty, String eligibilityType,
			Double amount) {

		String itemName = "";
		String batch = "";
		String mutipleAllowed = "";
		Double offerQty = 0.0;

		List<SchOfferAttrInst> offerInstList = schOfferAttrInstRepository
				.findByCompanyMstAndSchemeId(salesDtl.getCompanyMst(), scheme.getId());

		if (offerInstList.size() > 0) {
			for (SchOfferAttrInst offer : offerInstList) {
				if (offer.getAttributeName().equalsIgnoreCase("ITEMNAME")) {
					itemName = offer.getAttributeValue();
				}

				if (offer.getAttributeName().equalsIgnoreCase("QTY")) {
					offerQty = Double.parseDouble(offer.getAttributeValue());
				}

				if (offer.getAttributeName().equalsIgnoreCase("BATCH_CODE")) {
					batch = offer.getAttributeValue();
				}

				if (offer.getAttributeName().equalsIgnoreCase("MULTIPLE ALLOWED")) {
					mutipleAllowed = offer.getAttributeValue();
				}
			}
		}

		addOfferItem(itemName, offerQty, batch, mutipleAllowed, salesDtl, eligQty, eligibilityType, scheme, amount);

	}

	private void addOfferItem(String itemName, Double offerQty, String batch, String mutipleAllowed,
			SalesDtl salesDtlref, Double eligQty, String eligibilityType, SchemeInstance scheme, Double amount) {

		Optional<ItemMst> itemMstOpt = itemMstRepository.findByItemNameAndCompanyMst(itemName,
				salesDtlref.getCompanyMst());
		ItemMst itemMst = itemMstOpt.get();

		SalesTransHdr salesTransHdr = salesDtlref.getSalesTransHdr();

		int itemqty = 0;

		if (mutipleAllowed.equalsIgnoreCase("YES")) {

			SchEligibilityAttribInst schEligibilityAttribInst = schEligibilityAttribInstRepository
					.findByCompanyMstAndAttributeNameAndSchemeId(salesDtlref.getCompanyMst(), "QTY", scheme.getId());

			if (null != schEligibilityAttribInst) {
				itemqty = (int) (salesDtlref.getQty() / eligQty);
				itemqty = (int) (itemqty * offerQty);
			}

			SchEligibilityAttribInst schEligibilityAttribInstAmt = schEligibilityAttribInstRepository
					.findByCompanyMstAndAttributeNameAndSchemeId(salesDtlref.getCompanyMst(), "AMOUNT", scheme.getId());

			if (null != schEligibilityAttribInstAmt) {
				itemqty = (int) (amount / eligQty);

				itemqty = (int) (itemqty * offerQty);
			}

			System.out.println("--mutipleAllowed---Qty-------" + itemqty);
		}

		if (null != salesDtlref.getOfferReferenceId()) {

			List<SalesDtl> salesDtlOfferList = salesdtlRepo.findByIdAndSalesTransHdr(salesDtlref.getOfferReferenceId(),
					salesDtlref.getSalesTransHdr());

			for (SalesDtl sales : salesDtlOfferList) {

				salesdtlRepo.deleteById(sales.getId());
			}
		}

		Optional<CompanyMst> companyMst = companyMstRepository.findById(salesDtlref.getCompanyMst().getId());

		SalesDtl salesDtl = new SalesDtl();
		salesDtl.setCompanyMst(companyMst.get());
		salesDtl.setItemId(itemMst.getId());
		salesDtl.setItemName(itemMst.getItemName());
		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setBarcode(itemMst.getBarCode());
		salesDtl.setBatch(batch);
		salesDtl.setSchemeId(scheme.getId());
		if (itemqty > 0) {
			salesDtl.setQty((double) itemqty);
		} else {
			salesDtl.setQty((double) offerQty);
		}
		salesDtl.setUnitId(itemMst.getUnitId());

		Optional<UnitMst> unitMstOpt = unitMstRepository.findById(itemMst.getUnitId());
		UnitMst unitMst = unitMstOpt.get();

		if (null != unitMst) {
			salesDtl.setUnitName(unitMst.getUnitName());
		}

		salesDtl.setMrp(0.0);
		salesDtl.setTaxRate(0.0);
		salesDtl.setRate(0.0);
		salesDtl.setCessAmount(0.0);
		salesDtl.setCessRate(0.0);
		salesDtl.setCgstAmount(0.0);
		salesDtl.setCgstTaxRate(0.0);
		salesDtl.setSgstAmount(0.0);
		salesDtl.setSgstTaxRate(0.0);
		salesDtl.setAmount(0.0);
		salesDtl.setAddCessRate(0.0);
		salesDtl.setDiscount(0.0);
		salesDtl.setIgstAmount(0.0);
		salesDtl.setIgstTaxRate(0.0);

	//	salesDtl = salesdtlRepo.saveAndFlush(salesDtl);
		salesDtl = saveAndPublishService.saveSalesDtl(salesDtl, mybranch);
		
		

		if (eligibilityType.equalsIgnoreCase("CATEGORY_AND_QTY")) {
			updateSalesDtlsByCategory(salesDtlref, salesDtl);

		}
		if (eligibilityType.equalsIgnoreCase("ITEM_AND_QTY")) {
			udateSalesDtlsbyItem(salesDtlref, salesDtl);
		}

		if (eligibilityType.equalsIgnoreCase("ITEM_AND_BATCH")) {
			udateSalesDtlsbyItemAndBatch(salesDtlref, salesDtl);
		}
	}

	private void udateSalesDtlsbyItemAndBatch(SalesDtl salesDtlref, SalesDtl salesDtl) {
		List<SalesDtl> salesDtlByItemId = salesdtlRepo.findBySalesTransHdrAndItemIdAndBatch(
				salesDtlref.getSalesTransHdr(), salesDtlref.getItemId(), salesDtlref.getBatch());

		for (SalesDtl sales : salesDtlByItemId) {
			if (!salesDtl.getId().equalsIgnoreCase(sales.getId())) {
				sales.setOfferReferenceId(salesDtl.getId());
				salesdtlRepo.saveAndFlush(sales);
			}
		}

	}

	private void udateSalesDtlsbyItem(SalesDtl salesDtlref, SalesDtl salesDtl) {

		List<SalesDtl> salesDtlByItemId = salesdtlRepo.findBySalesTransHdrAndItemId(salesDtlref.getSalesTransHdr(),
				salesDtlref.getItemId());

		for (SalesDtl sales : salesDtlByItemId) {
			if (!salesDtl.getId().equalsIgnoreCase(sales.getId())) {
				sales.setOfferReferenceId(salesDtl.getId());
				salesdtlRepo.saveAndFlush(sales);
			}
		}

	}

	private void updateSalesDtlsByCategory(SalesDtl salesDtlref, SalesDtl salesDtl) {

		Optional<ItemMst> itemMstOpt = itemMstRepository.findById(salesDtlref.getItemId());
		ItemMst itemMst = itemMstOpt.get();

		List<SalesDtl> salesDtlByCategory = salesDetailsService
				.findBySalesTransHdrAndCategoryIdd(salesDtlref.getSalesTransHdr().getId(), itemMst.getCategoryId());

		for (SalesDtl sales : salesDtlByCategory) {
			if (!salesDtl.getId().equalsIgnoreCase(sales.getId())) {
				sales.setOfferReferenceId(salesDtl.getId());
				salesdtlRepo.saveAndFlush(sales);
			}
		}
	}

	private Boolean CheckValidityPeriod(SchemeInstance scheme, CompanyMst companyMst, SalesDtl addedItem,
			Date loginDate) {

		List<SchSelectionAttribInst> schSlectionAttrbInstList = schSelectionAttribInstRepository
				.findByCompanyMstAndSchemeId(companyMst, scheme.getId());

		for (SchSelectionAttribInst select : schSlectionAttrbInstList) {

			if (select.getAttributeType().equalsIgnoreCase("DATE")) {

				if (select.getAttributeName().equalsIgnoreCase("START DATE")) {
					Boolean validStartDate = validStartDate(select, loginDate);
					if (!validStartDate) {
						return false;
					}
				}

				if (select.getAttributeName().equalsIgnoreCase("END DATE")) {
					Boolean validEndDate = validEndDate(select, loginDate);
					if (!validEndDate) {
						return false;
					}
				}

			}

			if (select.getAttributeType().equalsIgnoreCase("DAY")) {

//				java.util.Date udate = SystemSetting.systemDate;
				java.util.Calendar calender = java.util.Calendar.getInstance();
				calender.setTime(loginDate);

				int day = calender.get(java.util.Calendar.DAY_OF_WEEK);
				String dayofweek = checkValidDay(day);

				if (!dayofweek.equalsIgnoreCase(select.getAttributeValue())) {
					return false;
				}

			}

			if (select.getAttributeType().equalsIgnoreCase("MONTH")) {

//				java.util.Date udate = SystemSetting.systemDate;
				java.util.Calendar calender = java.util.Calendar.getInstance();
				calender.setTime(loginDate);

				int month = loginDate.getMonth();
				String monthname = MonthName(month);

				if (!monthname.equalsIgnoreCase(select.getAttributeValue())) {
					return false;
				}

			}

		}

		return true;
	}

	private String MonthName(int month) {
		if (month == 0) {
			return "JANUARY";
		}
		if (month == 1) {
			return "FEBRUARY";
		}
		if (month == 2) {
			return "MARCH";
		}
		if (month == 3) {
			return "APRIL";
		}
		if (month == 4) {

			return "MAY";
		}
		if (month == 5) {
			return "JUNE";
		}
		if (month == 6) {
			return "JULY";
		}
		if (month == 7) {
			return "AUGUST";
		}
		if (month == 8) {
			return "SEPTEMBER";
		} else if (month == 9) {
			return "OCTOBER";
		}
		if (month == 10) {
			return "NOVEMBER";
		}
		if (month == 11) {
			return "DECEMBER";
		}

		return null;
	}

	private String checkValidDay(int day) {
		if (day == 1) {
			return "SUNDAY";
		}
		if (day == 2) {
			return "MONDAY";
		}
		if (day == 3) {
			return "TUESDAY";
		}
		if (day == 4) {
			return "WEDNESDAY";
		}
		if (day == 5) {
			return "THURSDAY";
		}
		if (day == 6) {
			return "FRIDAY";
		}
		if (day == 7) {
			return "SATURDAY";
		}

		return null;
	}

	private Boolean validEndDate(SchSelectionAttribInst select, Date nowDate) {
		System.out.println("Noooooooooooooooo" + nowDate);
		java.util.Date endDate = ClientSystemSetting.StringToUtilDate(select.getAttributeValue(), "dd-MM-yyyy");
		Integer intDate = endDate.compareTo(nowDate);

		if (intDate >= 0) {
			return true;
		}
		return false;
	}

	private Boolean validStartDate(SchSelectionAttribInst select, Date nowDate) {
		System.out.println("Noooooooooooooooo" + nowDate);
		java.util.Date startDate = ClientSystemSetting.StringToUtilDate(select.getAttributeValue(), "dd-MM-yyyy");
//			String schemeDate = select.getAttributeValue();
		Integer intDate = nowDate.compareTo(startDate);

		if (intDate >= 0) {
			return true;
		}

		return false;
	}

//	private double getConvertionQty(String companyMstId, String itemId, String sourceUnit, String targetUnit,
//			double sourceQty) {
//		if (recurssionOff) {
//			return sourceQty;
//		}
//		MultiUnitMst multiUnitMstList = multiUnitMstRepository.findByCompanyMstIdAndItemIdAndUnit1(companyMstId, itemId,
//				sourceUnit);
//
//		while (!multiUnitMstList.getUnit2().equalsIgnoreCase(targetUnit)) {
//
//			if (recurssionOff) {
//				break;
//			}
//			sourceUnit = multiUnitMstList.getUnit2();
//
//			sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();
//
//			getConvertionQty(companyMstId, itemId, sourceUnit, targetUnit, sourceQty);
//
//		}
//		sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();
//		recurssionOff = true;
//		// sourceQty = sourceQty *
//		// multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
//		return sourceQty;
//	}

	@GetMapping("{companymstid}/allsalesdtldeleted")
	public List<SalesDtlDeleted> retrieveAllSalesDtlDeleted() {
		return salesDtlDeletedRepository.findAll();
	}

	@GetMapping("{companymstid}/addsalesitem/web")
	public SalesDtl addWebSalesItem(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("itemname") String itemname, @RequestParam("qty") String qty,
			@RequestParam("rate") String rate, @RequestParam("transhdrid") String transhdrid) {

		// Find Item Based On Item Name

		itemname = itemname.replaceAll("%20", " ");
		itemname = itemname.replaceAll("%21", "\\[");
		itemname = itemname.replaceAll("%22", "\\]");
		ItemMst item = itemMstRepository.findByItemName(itemname);

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		UnitMst unit = unitMstRepository.findById(item.getUnitId()).get();

		// find salesOrderTransHdr;

		Double dQty = Double.parseDouble(qty);
		double mrpRateIncludingTax = Double.parseDouble(rate);

		SalesTransHdr salesTransHdr = saleshdrRepo.findById(transhdrid).get();
		SalesDtl salesDtl = new SalesDtl();

		salesDtl.setCompanyMst(companyMst);
		salesDtl.setItemId(item.getId());
		salesDtl.setItemName(item.getItemName());
		salesDtl.setSalesTransHdr(salesTransHdr);

		salesDtl.setAddCessRate(item.getCess());
		salesDtl.setBarcode(item.getBarCode());
		salesDtl.setBatch(MapleConstants.Nobatch);
		salesDtl.setQty(dQty);
		salesDtl.setMrp(mrpRateIncludingTax);
		salesDtl.setTaxRate(item.getTaxRate());
		salesDtl.setCgstTaxRate(item.getTaxRate() / 2);
		salesDtl.setSgstTaxRate(item.getTaxRate() / 2);
		salesDtl.setUnitId(item.getUnitId());
		salesDtl.setUnitName(unit.getUnitName());

		// ----------------------------------------

		double taxRate = item.getTaxRate();

		double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
		double cessAmount = 0.0;
		double cessRate = 0.0;
		salesDtl.setRate(rateBeforeTax);
		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();
				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				salesDtl.setRate(rateBeforeTax);
				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;
			} else {
				cessAmount = 0.0;
				cessRate = 0.0;
			}
			salesDtl.setRate(rateBeforeTax);
			salesDtl.setCessRate(cessRate);
			salesDtl.setCessAmount(cessAmount);
		}

		// salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
		double sgstTaxRate = taxRate / 2;
		double cgstTaxRate = taxRate / 2;
		salesDtl.setCgstTaxRate(cgstTaxRate);

		salesDtl.setSgstTaxRate(sgstTaxRate);
		String companyState = companyMst.getState();
		String customerState = "KERALA";
		try {
			customerState = salesTransHdr.getAccountHeads().getCustomerState();
		} catch (Exception e) {

		}

		if (null == customerState) {
			customerState = "KERALA";
		}

		if (null == companyState) {
			companyState = "KERALA";
		}

		salesDtl.setCessAmount(0.0);
		salesDtl.setCessRate(0.0);
		if (customerState.equalsIgnoreCase(companyState)) {
			salesDtl.setSgstTaxRate(taxRate / 2);

			salesDtl.setCgstTaxRate(taxRate / 2);

			Double cgstAmt = 0.0, sgstAmt = 0.0;
			cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
			BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
			bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			salesDtl.setCgstAmount(bdCgstAmt.doubleValue());
			sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
			BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
			bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

			salesDtl.setIgstTaxRate(0.0);
			salesDtl.setIgstAmount(0.0);

		} else {
			salesDtl.setSgstTaxRate(0.0);

			salesDtl.setCgstTaxRate(0.0);

			salesDtl.setCgstAmount(0.0);

			salesDtl.setSgstAmount(0.0);

			salesDtl.setIgstTaxRate(taxRate);
			salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		}

		salesDtl.setAmount(
				salesDtl.getQty() * (salesDtl.getRate() + salesDtl.getCgstAmount() + salesDtl.getSgstAmount()));

		// ----------------------------------------

		salesDtl.setDiscount(0.0);
		//salesDtl = salesDetailsRepository.saveAndFlush(salesDtl);
		salesDtl=saveAndPublishService.saveSalesDtl(salesDtl, mybranch);

		return salesDtl;

	}

	@GetMapping("{companymstid}/salesdtlbyhdrid/{transhdrid}")
	public List<SalesDtl> getSalesDtlListByHdrId(@PathVariable(value = "transhdrid") String transhdrid) {

		SalesTransHdr salesTransHdr = saleshdrRepo.findById(transhdrid).get();

		return salesDetailsRepository.findBySalesTransHdr(salesTransHdr);

	}

	// ----------------------------new version 2.0 surya

	@PostMapping("{companymstid}/salestranshdr/{salestranshdrId}/savesalesdtlwithcalculation")
	public SalesDtl createSalesDtlWithCalculation(@PathVariable(value = "salestranshdrId") String salestranshdrId,
			@PathVariable(value = "companymstid") String companymstid, @RequestParam("logdate") String logdate,

			@Valid @RequestBody SalesDtl salesDtlRequest) {

		Date date = SystemSetting.StringToUtilDate(logdate, "yyyy-MM-dd");

		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		return saleshdrRepo.findById(salestranshdrId).map(salestranshdr -> {
			salesDtlRequest.setCompanyMst(companyMst.get());

			SalesDtl calculatedSalesDtl = SalesDtlCalculation(salesDtlRequest, salestranshdr, date);

			SalesDtl addedItem = salesdtlRepo.findBySalesTransHdrIdAndItemIdAndBatchAndBarcodeAndUnitIdAndRate(
					salestranshdr.getId(), salesDtlRequest.getItemId(), salesDtlRequest.getBatch(),
					salesDtlRequest.getBarcode(), salesDtlRequest.getUnitId(), salesDtlRequest.getRate());
			if (null != addedItem && null != addedItem.getId()
					&& salesDtlRequest.getUnitId().equalsIgnoreCase(addedItem.getUnitId())) {

				addedItem.setQty(addedItem.getQty() + salesDtlRequest.getQty());
				addedItem.setCgstAmount(salesDtlRequest.getCgstAmount());
				addedItem.setSgstAmount(salesDtlRequest.getSgstAmount());
				addedItem.setCessAmount(salesDtlRequest.getCessAmount());

				addedItem.setAmount(addedItem.getQty() * (salesDtlRequest.getRate() + salesDtlRequest.getCgstAmount()
						+ salesDtlRequest.getSgstAmount()));

				addedItem.setRate(salesDtlRequest.getRate());
				addedItem.setMrp(salesDtlRequest.getMrp());
				//addedItem = salesdtlRepo.saveAndFlush(addedItem);
				addedItem=saveAndPublishService.saveSalesDtl(addedItem, mybranch);
				return addedItem;

			} else {
				salesDtlRequest.setSalesTransHdr(salestranshdr);

				//SalesDtl salesDtl = salesdtlRepo.saveAndFlush(salesDtlRequest);
				SalesDtl salesDtl =saveAndPublishService.saveSalesDtl(salesDtlRequest, mybranch);
				return salesDtl;
			}

		}).orElseThrow(() -> new ResourceNotFoundException("salestranshdrId " + salestranshdrId + " not found"));

	}

	private SalesDtl SalesDtlCalculation(SalesDtl salesDtl, SalesTransHdr salesTransHdr, Date date) {

		Optional<ItemMst> itemMstOpt = itemMstRepository.findById(salesDtl.getItemId());
		ItemMst itemMst = itemMstOpt.get();

		Double taxRate = 00.0;
		Double mrpRateIncludingTax = 0.0;

		taxRate = itemMst.getTaxRate();
		mrpRateIncludingTax = salesDtl.getMrp();

		List<TaxMst> getTaxMst = taxMstRepository.getTaxByItemId(salesDtl.getItemId());
		if (getTaxMst.size() > 0) {
			for (TaxMst taxMst : getTaxMst) {

				String companyState = salesTransHdr.getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
						salesDtl.setCgstTaxRate(taxMst.getTaxRate());
//						BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCgstAmount(CgstAmount.doubleValue());
					}
					if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
						salesDtl.setSgstTaxRate(taxMst.getTaxRate());
//						BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setSgstAmount(SgstAmount.doubleValue());
					}
					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);
					TaxMst taxMst1 = taxMstRepository.getTaxByItemIdAndTaxId(salesDtl.getItemId(), "IGST");
					if (null != taxMst1) {

						salesDtl.setTaxRate(taxMst1.getTaxRate());
					}
				} else {
					if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
						salesDtl.setCgstTaxRate(0.0);
						salesDtl.setCgstAmount(0.0);
						salesDtl.setSgstTaxRate(0.0);
						salesDtl.setSgstAmount(0.0);

						salesDtl.setTaxRate(taxMst.getTaxRate());
						salesDtl.setIgstTaxRate(taxMst.getTaxRate());

					}
				}
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
						salesDtl.setCessRate(taxMst.getTaxRate());

					}
				}
				if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
					salesDtl.setAddCessRate(taxMst.getTaxRate());
				}

			}
			Double rateBeforeTax = (100 * salesDtl.getMrp()) / (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate()
					+ salesDtl.getAddCessRate() + salesDtl.getSgstTaxRate() + salesDtl.getCgstTaxRate());
			salesDtl.setRate(rateBeforeTax);
			BigDecimal igstAmount = taxMstResource.taxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
			salesDtl.setIgstAmount(igstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal SgstAmount = taxMstResource.taxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
			salesDtl.setSgstAmount(SgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal CgstAmount = taxMstResource.taxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
			salesDtl.setCgstAmount(CgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal cessAmount = taxMstResource.taxCalculator(salesDtl.getCessRate(), rateBeforeTax);
			salesDtl.setCessAmount(cessAmount.doubleValue() * salesDtl.getQty());
			BigDecimal addcessAmount = taxMstResource.taxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
			salesDtl.setAddCessAmount(addcessAmount.doubleValue() * salesDtl.getQty());
		}

		else {

// verificed

			if (salesDtl.getUnitId().equalsIgnoreCase(itemMst.getUnitId())) {
				salesDtl.setStandardPrice(itemMst.getStandardPrice());
			} else {

				MultiUnitMst multiUnitMst = multyMstRepository.findByCompanyMstIdAndItemIdAndUnit1(
						salesTransHdr.getCompanyMst().getId(), salesDtl.getItemId(), salesDtl.getUnitId());

				salesDtl.setStandardPrice(multiUnitMst.getPrice());

			}
			if (null != itemMst.getTaxRate()) {
				salesDtl.setTaxRate(itemMst.getTaxRate());
				taxRate = itemMst.getTaxRate();

			} else {
				salesDtl.setTaxRate(0.0);
			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);

			// if Discount

			// Calculate discount on base price
			if (null != salesTransHdr.getAccountHeads().getDiscountProperty()) {

				if (salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF BASE PRICE")) {
					salesDtl = calcDiscountOnBasePrice(salesTransHdr, rateBeforeTax, itemMst, mrpRateIncludingTax,
							taxRate, salesDtl);

				}
				if (salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF MRP")) {
//				salesDtl.setTaxRate(0.0);
					salesDtl = calcDiscountOnMRP(salesTransHdr, rateBeforeTax, itemMst, mrpRateIncludingTax, taxRate,
							salesDtl);
				}
				if (salesTransHdr.getAccountHeads().getDiscountProperty()
						.equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX")) {
					salesDtl = ambrossiaDiscount(salesTransHdr, rateBeforeTax, itemMst, mrpRateIncludingTax, taxRate,
							salesDtl);
				}
			} else {

				double cessAmount = 0.0;
				double cessRate = 0.0;
				salesDtl.setRate(rateBeforeTax);
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (itemMst.getCess() > 0) {
						cessRate = itemMst.getCess();
						rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + itemMst.getCess());

						salesDtl.setRate(rateBeforeTax);
						cessAmount = salesDtl.getQty() * salesDtl.getRate() * itemMst.getCess() / 100;
					} else {
						cessAmount = 0.0;
						cessRate = 0.0;
					}
					salesDtl.setRate(rateBeforeTax);
					salesDtl.setCessRate(cessRate);
					salesDtl.setCessAmount(cessAmount);
				}

				// salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
				double sgstTaxRate = taxRate / 2;
				double cgstTaxRate = taxRate / 2;
				salesDtl.setCgstTaxRate(cgstTaxRate);

				salesDtl.setSgstTaxRate(sgstTaxRate);
				String companyState = salesTransHdr.getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					salesDtl.setSgstTaxRate(taxRate / 2);

					salesDtl.setCgstTaxRate(taxRate / 2);

					Double cgstAmt = 0.0, sgstAmt = 0.0;
					cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
					BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
					bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					salesDtl.setCgstAmount(bdCgstAmt.doubleValue());
					sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
					BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
					bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);

				} else {
					salesDtl.setSgstTaxRate(0.0);

					salesDtl.setCgstTaxRate(0.0);

					salesDtl.setCgstAmount(0.0);

					salesDtl.setSgstAmount(0.0);

					salesDtl.setIgstTaxRate(taxRate);
					salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

				}
			}
		}

		String companyState = salesTransHdr.getCompanyMst().getState();
		String customerState = "KERALA";
		try {
			customerState = salesTransHdr.getAccountHeads().getCustomerState();
		} catch (Exception e) {

		}

		if (null == customerState) {
			customerState = "KERALA";
		}

		if (null == companyState) {
			companyState = "KERALA";
		}
//		String gstType = customerRegistrationResourse.customerGstType(customerState);

		String gstType = "";
		if (customerState.equalsIgnoreCase("KERALA")) {
			gstType = "GST";
		}else {
			gstType = "IGST";
		}
		if (gstType.equalsIgnoreCase("GST")) {
			salesDtl.setSgstTaxRate(taxRate / 2);

			salesDtl.setCgstTaxRate(taxRate / 2);

			Double cgstAmt = 0.0, sgstAmt = 0.0;
			cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
			BigDecimal bdcgstAmt = new BigDecimal(cgstAmt);
			bdcgstAmt = bdcgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setCgstAmount(bdcgstAmt.doubleValue());
			sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
			BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
			bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

			salesDtl.setIgstTaxRate(0.0);
			salesDtl.setIgstAmount(0.0);

		} else {
			salesDtl.setSgstTaxRate(0.0);

			salesDtl.setCgstTaxRate(0.0);

			salesDtl.setCgstAmount(0.0);

			salesDtl.setSgstAmount(0.0);

			salesDtl.setIgstTaxRate(taxRate);
			salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		}

		BigDecimal settoamount = new BigDecimal((salesDtl.getQty() * salesDtl.getRate()));

		double includingTax = (settoamount.doubleValue() * salesDtl.getTaxRate()) / 100;
		double amount = settoamount.doubleValue() + includingTax + salesDtl.getCessAmount();
		BigDecimal setamount = new BigDecimal(amount);
		setamount = setamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		salesDtl.setAmount(setamount.doubleValue());

		salesDtl.setAddCessRate(0.0);
		PriceDefenitionMst priceDefenitionMst1 = priceDefinitionMstRepo
				.findByCompanyMstAndPriceLevelName(salesTransHdr.getCompanyMst(), "MRP");
		if (null != priceDefenitionMst1) {
			/*
			 * fetch for batchprice definition
			 */
			BatchPriceDefinition batchpriceDef = batchPriceDefinitionService.priceDefinitionByItemIdAndUnitAndBatch(
					salesDtl.getItemId(), priceDefenitionMst1.getId(), salesDtl.getUnitId(), salesDtl.getBatch(), date);

			if (null != batchpriceDef) {
				salesDtl.setMrp(batchpriceDef.getAmount());
			} else {
				PriceDefinition priceDefinition = priceDefinitionRepo.findByItemIdAndStartDateCostprice(
						salesDtl.getItemId(), priceDefenitionMst1.getId(), date, salesDtl.getUnitId());

				if (null != priceDefinition) {
					salesDtl.setMrp(priceDefinition.getAmount());
				}
			}
		}

		PriceDefenitionMst priceDefenitionMst = priceDefinitionMstRepo
				.findByCompanyMstAndPriceLevelName(salesTransHdr.getCompanyMst(), "COST PRICE");

		if (null != priceDefenitionMst) {

			BatchPriceDefinition batchpriceDef = batchPriceDefinitionService.priceDefinitionByItemIdAndUnitAndBatch(
					salesDtl.getItemId(), priceDefenitionMst1.getId(), salesDtl.getUnitId(), salesDtl.getBatch(), date);

			if (null != batchpriceDef) {
				salesDtl.setCostPrice(batchpriceDef.getAmount());
			} else {

				PriceDefinition priceDefinition = priceDefinitionRepo.findByItemIdAndStartDateCostprice(
						salesDtl.getItemId(), priceDefenitionMst.getId(), date, salesDtl.getUnitId());
				if (null != priceDefinition) {
					salesDtl.setCostPrice(priceDefinition.getAmount());
				}
			}
		}

		return salesDtl;
	}

	private SalesDtl ambrossiaDiscount(SalesTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, Double taxRate, SalesDtl salesDtl) {

		System.out.print("mrpRateIncludingTax" + mrpRateIncludingTax);

		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			double discountAmount = (mrpRateIncludingTax * salesTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
			BigDecimal BrateAfterDiscount = new BigDecimal(mrpRateIncludingTax - discountAmount);

			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

//		
			salesDtl.setDiscount(discountAmount);

			double newRate = BrateAfterDiscount.doubleValue();
			BigDecimal BnewRate = new BigDecimal(newRate);
			BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal BrateBeforeTax = new BigDecimal((newRate * 100) / (100 + taxRate));

			BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			salesDtl.setRate(BrateBeforeTax.doubleValue());

			// salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
			// salesDtl.setStandardPrice(BrateBeforeTax.doubleValue());
		} else {
			salesDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				double discountAmount = (mrpRateIncludingTax * salesTransHdr.getAccountHeads().getCustomerDiscount())
						/ 100;
				BigDecimal BrateAfterDiscount = new BigDecimal(mrpRateIncludingTax - discountAmount);

				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

//			
				salesDtl.setDiscount(discountAmount);

				double newRate = BrateAfterDiscount.doubleValue();
				BigDecimal BnewRate = new BigDecimal(newRate);
				BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				BigDecimal BrateBeforeTax = new BigDecimal((newRate * 100) / (100 + taxRate + cessRate));

				BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				salesDtl.setRate(BrateBeforeTax.doubleValue());

				// salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
				// salesDtl.setStandardPrice(BrateBeforeTax.doubleValue());

				System.out.println("rateBeforeTax---------" + BrateBeforeTax);

//				if (salesTransHdr.getCustomerMst().getCustomerDiscount() > 0) {
//					Double rateAfterDiscount = (100 * rateBeforeTax)
//							/ (100 + salesTransHdr.getCustomerMst().getCustomerDiscount());
//					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
//					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//					
//					
//					salesDtl.setRate(BrateAfterDiscount.doubleValue());
//					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
//					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//					// salesDtl.setMrp(rateBefrTax.doubleValue());
//					salesDtl.setStandardPrice(rateBefrTax.doubleValue());
//				} else {
//					salesDtl.setRate(rateBeforeTax);
//				}

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);

		return salesDtl;
	}

	private SalesDtl calcDiscountOnMRP(SalesTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, Double taxRate, SalesDtl salesDtl) {

		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {

			Double rateAfterDiscount = (100 * mrpRateIncludingTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);

			BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BnewrateBeforeTax.doubleValue());
			// BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			// rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
			// salesDtl.setMrp(BrateAfterDiscount.doubleValue());
			// salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
		} else {
			salesDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * mrpRateIncludingTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

					BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(mrpRateIncludingTax);
					// salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesDtl.setRate(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);

		return salesDtl;

	}

	private SalesDtl calcDiscountOnBasePrice(SalesTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate, SalesDtl salesDtl) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * rateBeforeTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BrateAfterDiscount.doubleValue());
			BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			// salesDtl.setMrp(rateBefrTax.doubleValue());
			// salesDtl.setStandardPrice(rateBefrTax.doubleValue());
		} else {
			salesDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(rateBefrTax.doubleValue());
					// salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesDtl.setRate(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);

		return salesDtl;

	}

	// ----------------------------new version 1.7 surya end

	@GetMapping("{companymstid}/findallsalesdtls")
	public List<SalesDtl> getSalesDtl(@PathVariable(value = "companymstid") String companymstid) {

		return salesDetailsRepository.findAll();

	}

	@PostMapping("{companymstid}/salesdetails/savekotordersalesdtl/{branchcode}")
	public String createKOTSales(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, 
			@Valid @RequestBody List<SalesDtl> salesDtlReqList) {

		if (salesDtlReqList.size() == 0) {
			return null;
		}

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);

		if (!companyMstOpt.isPresent()) {
			return null;
		}
		CompanyMst companyMst = companyMstOpt.get();

		SalesTransHdr salesTransHdr = new SalesTransHdr();
		SalesTransHdr salesTransHdrTemp = salesDtlReqList.get(0).getSalesTransHdr();

		for (SalesDtl salesDtlToSave : salesDtlReqList) {

			if (null == salesTransHdr.getId()) {
				salesTransHdr = new SalesTransHdr();

				Optional<AccountHeads> accountHeadsOpt = accountHeadsResource
						.retrievAccountHeadsByNameAndBrachCode("KOT", branchcode, companymstid);

				AccountHeads accountHeads = accountHeadsOpt.get();

				if (null == accountHeads) {

					accountHeads = accountHeadsResource.retrieveAccountHeadsByName("KOT", companymstid);
				}

				salesTransHdr.setAccountHeads(accountHeads);
				salesTransHdr.setUserId(salesTransHdrTemp.getUserId());
				salesTransHdr.setSalesMode("KOT");
				salesTransHdr.setInvoiceAmount(0.0);
				salesTransHdr.setBranchCode(branchcode);
				salesTransHdr.setVoucherDate(SystemSetting.getSystemDate());
				salesTransHdr.setCustomerId(accountHeads.getId());
				salesTransHdr.setCreditOrCash("CASH");
				salesTransHdr.setIsBranchSales("N");
				salesTransHdr.setKotNumber(salesTransHdrTemp.getKotNumber());
				salesTransHdr.setServingTableName(salesTransHdrTemp.getServingTableName());
				salesTransHdr.setCompanyMst(companyMst);
				salesTransHdr.setPerformaInvoicePrinted("NO");

				salesTransHdr.setSalesManId(salesTransHdrTemp.getSalesManId());

				//salesTransHdr = saleshdrRepo.saveAndFlush(salesTransHdr);
				salesTransHdr=saveAndPublishService.saveSalesTransHdr(salesTransHdrTemp, mybranch);
				logger.info("saveSalesTransHdr send to KafkaEvent: {}", salesTransHdr);
			}
			
			if(null == salesTransHdr)
			{
				return null;
			}

			Optional<ItemMst> itemMstResp = itemMstRepository.findById(salesDtlToSave.getItemId());
			if (!itemMstResp.isPresent()) {
				continue;
			}
			ItemMst itemMst = itemMstResp.get();

			SalesDtl salesDtl = new SalesDtl();

			salesDtl.setAddCessAmount(0.0);
			salesDtl.setAddCessRate(0.0);
			salesDtl.setAmount(salesDtlToSave.getQty() * itemMst.getStandardPrice());
			salesDtl.setBarcode(itemMst.getBarCode());
			salesDtl.setBatch(MapleConstants.Nobatch);
			salesDtl.setCessRate(0.0);
			salesDtl.setCessAmount(0.0);
			salesDtl.setCgstAmount(0.0);
			salesDtl.setCgstTaxRate(0.0);
			salesDtl.setCompanyMst(companyMst);
			salesDtl.setCostPrice(0.0);
			salesDtl.setDiscount(0.0);
			salesDtl.setExpiryDate(null);
//			salesDtl.setId(id);
			salesDtl.setIgstAmount(0.0);
			salesDtl.setIgstTaxRate(0.0);
			salesDtl.setItemId(itemMst.getId());
			salesDtl.setItemName(itemMst.getItemName());
			salesDtl.setKotDescription(salesDtlToSave.getKotDescription());
			salesDtl.setMrp(itemMst.getStandardPrice());
			salesDtl.setPrintKotStatus("N");
			salesDtl.setQty(salesDtlToSave.getQty());
			salesDtl.setRate(0.0);
			salesDtl.setRateBeforeDiscount(0.0);
			salesDtl.setReturnedQty(0.0);
			salesDtl.setSalesTransHdr(salesTransHdr);
			salesDtl.setSgstAmount(0.0);
			salesDtl.setSgstTaxRate(0.0);
			salesDtl.setStandardPrice(itemMst.getStandardPrice());
			salesDtl.setTaxRate(itemMst.getTaxRate());
			salesDtl.setUnitId(itemMst.getUnitId());

			//salesdtlRepo.save(salesDtl);
			saveAndPublishService.saveSalesDtl(salesDtl, mybranch);
			logger.info("saveSalesTransHdr send to KafkaEvent: {}", salesDtl);

		}

		List<SalesDtl> salesDtlList = salesDetailsRepository.findBySalesTransHdr(salesTransHdr);

		return salesTransHdr.getId();

	}
	
	
	
	@PostMapping("{companymstid}/salesdetails/savekotordertorunningorder/{hdrid}/{branchcode}")
	public List<SalesDtl> updateKOTSalesToRunningOrder(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, 
			@PathVariable(value = "hdrid") String hdrid, 

			@Valid @RequestBody List<SalesDtl> salesDtlReqList) {

		if (salesDtlReqList.size() == 0) {
			return null;
		}

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);

		if (!companyMstOpt.isPresent()) {
			return null;
		}
		CompanyMst companyMst = companyMstOpt.get();
		

		Optional<SalesTransHdr> salesTransHdrOpt = saleshdrRepo.findById(hdrid);
		SalesTransHdr salesTransHdr = salesTransHdrOpt.get();
		

		if(null == salesTransHdr)
		{
			return null;
		}

		for (SalesDtl salesDtlToSave : salesDtlReqList) {

			SalesDtl salesDtlSaved = salesDetailsRepository.
					findByItemIdAndSalesTransHdr(salesDtlToSave.getItemId(),salesTransHdr);
			
			
			

			Optional<ItemMst> itemMstResp = itemMstRepository.findById(salesDtlToSave.getItemId());
			if (!itemMstResp.isPresent()) {
				continue;
			}
			
			ItemMst itemMst = itemMstResp.get();

			SalesDtl salesDtl = new SalesDtl();
			salesDtl.setQty(salesDtlToSave.getQty());

			
			if(null != salesDtlSaved)
			{
				salesDtl.setId(salesDtlSaved.getId());
				salesDtl.setQty(salesDtlSaved.getQty()+salesDtlToSave.getQty());
				
				salesDetailsRepository.deleteById(salesDtlSaved.getId());

			}

			salesDtl.setAddCessAmount(0.0);
			salesDtl.setAddCessRate(0.0);
			salesDtl.setBarcode(itemMst.getBarCode());
			salesDtl.setBatch(MapleConstants.Nobatch);
			salesDtl.setCessRate(0.0);
			salesDtl.setCessAmount(0.0);
			salesDtl.setCgstAmount(0.0);
			salesDtl.setCgstTaxRate(0.0);
			salesDtl.setCompanyMst(companyMst);
			salesDtl.setCostPrice(0.0);
			salesDtl.setDiscount(0.0);
			salesDtl.setExpiryDate(null);
//			salesDtl.setId(id);
			salesDtl.setIgstAmount(0.0);
			salesDtl.setIgstTaxRate(0.0);
			salesDtl.setItemId(itemMst.getId());
			salesDtl.setItemName(itemMst.getItemName());
			salesDtl.setKotDescription(salesDtlToSave.getKotDescription());
			salesDtl.setMrp(itemMst.getStandardPrice());
			salesDtl.setPrintKotStatus("N");
			salesDtl.setRate(0.0);
			salesDtl.setRateBeforeDiscount(0.0);
			salesDtl.setReturnedQty(0.0);
			salesDtl.setSalesTransHdr(salesTransHdr);
			salesDtl.setSgstAmount(0.0);
			salesDtl.setSgstTaxRate(0.0);
			salesDtl.setStandardPrice(itemMst.getStandardPrice());
			salesDtl.setTaxRate(itemMst.getTaxRate());
			salesDtl.setUnitId(itemMst.getUnitId());
			salesDtl.setAmount(salesDtlToSave.getQty() * itemMst.getStandardPrice());


			//salesdtlRepo.save(salesDtl);
			saveAndPublishService.saveSalesDtl(salesDtl, mybranch);
			logger.info("salesDtl send to KafkaEvent: {}", salesDtl);

		}

		List<SalesDtl> salesDtlList = salesDetailsRepository.findBySalesTransHdr(salesTransHdr);

		return salesDtlList;

	}
	
	@GetMapping("{companymstid}/salesdetailresource/getretailsalesdetailreport")
	public List<RetailSalesDetailReport> getRetailSalesDetailReport(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate) {
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		
		
		return salesDetailsService.getRetailSalesDetailReport (fromDate, toDate);
	}
	
	
	@GetMapping("{companymstid}/salesdetailresource/getretailsalessummaryreport")
	public List<RetailSalesSummaryReport> getRetailSalesSummaryReport(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("fromdate") String fromdate,
			@RequestParam("todate") String todate) {
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		return salesDetailsService.getRetailSalesSummaryReport (fromDate, toDate);


}
	
}
