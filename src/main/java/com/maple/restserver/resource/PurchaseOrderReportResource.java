package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.PurchaseReport;
import com.maple.restserver.service.PurchaseOrderReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class PurchaseOrderReportResource {

	@Autowired
	PurchaseOrderReportService purchaseOrderReportService;
	
	@GetMapping("{companymstid}/purchaseorderreport/{vouchernumber}/{branchcode}")
	public List<PurchaseReport> retrieveReportPurchaseOrder(@PathVariable (value = "vouchernumber") String vouchernumber,
			@PathVariable (value = "branchcode") String branchcode ,
			@RequestParam("rdate") String reportdate){
		
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
		return purchaseOrderReportService.retrieveReportPurchaseOrder(vouchernumber,branchcode,date);
	}
	
}
