
package com.maple.restserver.resource;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.StockTransferIn;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.StockTransferInRepository;


@RestController
@Transactional
public class StockTransferInResource {
	@Autowired
	private StockTransferInRepository stocktransferin;
	
	CompanyMstRepository companyMstRepo;
	@GetMapping("{companymstid}/stocktransferins")
	public List<StockTransferIn> retrieveAll(@PathVariable(value = "companymstid") String companymstid)
	{
		return stocktransferin.findByCompanyMst(companymstid);
	}
	
	




	@PostMapping("{companymstid}/stocktransferin")
	public StockTransferIn createUser(@PathVariable(value = "companymstid") String companymstid,@Valid @RequestBody StockTransferIn stocktransferin1)
	{
		
		return  companyMstRepo.findById(companymstid).map(companyMst-> {
			stocktransferin1.setCompanyMst(companyMst);
	     return stocktransferin.saveAndFlush(stocktransferin1);
		 }).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }

	}

	
	


