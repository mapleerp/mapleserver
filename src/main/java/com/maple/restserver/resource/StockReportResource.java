package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.ItemStockEnquiryReport;
import com.maple.restserver.report.entity.StockReport;
import com.maple.restserver.service.StockReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
@CrossOrigin("http://localhost:4200")
public class StockReportResource {

	@Autowired
	StockReportService stockReportService;
	
	@GetMapping("{companymstid}/stockreport/{branchcode}/{categoryid}")
	public List<StockReport> getStockReport(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,
			 @PathVariable("categoryid") String categoryid ,
			 @RequestParam("fromdate") String fromdate){
		java.sql.Date date = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");

		return stockReportService.getStockByBranchCodeAndDateAndCompanyMstId(branchcode, companymstid ,date,categoryid);



		
	}
	
	@GetMapping("{companymstid}/stockreport/dailystockreport/{branchcode}")
	public List<StockReport> getDailyStockReport(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode ,@RequestParam("fromdate") String fromDate){
		
		System.out.print(fromDate+"date issssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		java.sql.Date date = SystemSetting.StringToSqlDate(fromDate, "yyyy-MM-dd");

		return stockReportService.getDailyStockReport(branchcode, companymstid ,date);


		
	}
	
	
	@GetMapping("{companymstid}/stockreportresurce/itemwisestock")
	public List<StockReport> getItemWiseStockReport(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("branchcode") String branchcode ,@RequestParam("fromdate") String fromDate,@RequestParam("itemid") String itemid){
		
		
		java.util.Date date = SystemSetting.StringToSqlDate(fromDate, "yyyy-MM-dd");

		return stockReportService.getItemWiseStockReport(branchcode, companymstid ,date,itemid);
	
	}
	
	@GetMapping("{companymstid}/stockreportresurce/itemstockenquiryreport/{branchcode}/{itemid}/{vouchertype}")
	public List<ItemStockEnquiryReport> getItemStockEnquiryReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "vouchertype") String vouchertype){
		
		return stockReportService.getItemStockReport(companymstid ,branchcode,itemid,vouchertype);
	
	}
	
	@GetMapping("{companymstid}/stockreport/dailymobilestockreport/{branchcode}")
	public List<StockReport> getDailyMobileStockReport(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode ,@RequestParam("fromdate") String fromDate){
		
		System.out.print(fromDate+"date issssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		java.sql.Date date = SystemSetting.StringToSqlDate(fromDate, "yyyy-MM-dd");

		return stockReportService.getDailyMobileStockReport(branchcode, companymstid ,date);


		
	}
	
	
	@GetMapping("{companymstid}/stockreport/dailymobilestockreportwithitemorcategory/{branchcode}")
	public List<StockReport> getDailyMobileStockReportByItemOrCategory(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode ,
			 @RequestParam("fromdate") String fromDate,
			 @RequestParam(value = "itemname")String itemname,
			 @RequestParam(value = "categoryname")String categoryname){
		
		java.sql.Date date = SystemSetting.StringToSqlDate(fromDate, "yyyy-MM-dd");
		categoryname = categoryname.replace("AND", "&");
		String[] array = categoryname.split(";");
		if(itemname.equalsIgnoreCase("") && !categoryname.equalsIgnoreCase(""))
		{
			return stockReportService.getDailyMobileStockReportByCategory(branchcode, companymstid ,date,array);
		}
		else if(!itemname.equalsIgnoreCase("") &&categoryname.equalsIgnoreCase("") )
		{
			return stockReportService.getDailyMobileStockReportByItemName(branchcode, companymstid ,date,itemname);

		}
		else
			
		return stockReportService.getDailyMobileStockReport(branchcode, companymstid ,date);


		
	}
	
	
	//===========================new version 1.5 surya
	
	@GetMapping("{companymstid}/stockreport/storewisestockreport/{store}/{branchcode}")
	public List<StockReport> getStoreWiseStockReport(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,

			 @PathVariable("store") String store){
		
		return stockReportService.getStoreWiseStockReport(branchcode, companymstid ,store);

	}
	
	//===========================new version 1.5 surya end

	
	//******************PriceType Wise Stock Summary Report*******anandu*****
	
	@GetMapping("{companymstid}/stockreportresource/getstocksummarydetails/{selectedItem}/{selectedItem2}")
	public List<StockReport> getStockSummaryDetails(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "selectedItem") String pricetype,
			@PathVariable(value = "selectedItem2") String branchcode,
			@RequestParam("fromdate") String fromDate,
			@RequestParam("todate") String toDate
			 ){

		java.util.Date fdate = SystemSetting.StringToUtilDate(fromDate, "yyyy-MM-dd");
		java.util.Date tdate = SystemSetting.StringToUtilDate(toDate, "yyyy-MM-dd");
		return stockReportService.getPriceTypeWiseStockSummaryReport(fdate, tdate ,pricetype,branchcode);

	}
	
	
	
}
