package com.maple.restserver.resource;

 
import java.math.BigDecimal;
 
import java.io.IOException;
 
import java.net.URI;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.service.InventoryLedgerMstService;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.StockTransferIn;
import com.maple.restserver.entity.StockTransferInDtl;
import com.maple.restserver.entity.StockTransferInHdr;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.entity.SysDateMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.jms.send.KafkaMapleEvent;
import com.maple.restserver.jms.send.KafkaMapleEventType;
import com.maple.restserver.message.entity.AcceptStockMessage;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.StockTransferInDtlRepository;
import com.maple.restserver.repository.StockTransferInHdrRepository;
import com.maple.restserver.repository.SysDateMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController

public class StockTransferInHdrResource {
	private static final Logger logger = LoggerFactory.getLogger(StockTransferInHdrResource.class);

	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	SysDateMstRepository sysDateMstRepository;
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	@Autowired
	private VoucherNumberService voucherService;
	 
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	private ItemBatchMstRepository itemBatchMstRepo;

	@Autowired
	private ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	private StockTransferInHdrRepository stockTransferInHdrRepository;
	
	
	@Autowired
	private StockTransferInDtlRepository stockTransferInDtlRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	private ExternalApi externalApi;
	
	@Autowired 
	SaveAndPublishService saveAndPublishService;
	
	
	 EventBus eventBus = EventBusFactory.getEventBus();
	 
	 
	 @Autowired
	 InventoryLedgerMstService inventoryledgerMstService;
	 
	 @Autowired
	 AccountClassRepository accountClassRepository;
	 

	@GetMapping("{companymstid}/stocktransferinhdr")
	public List<StockTransferInHdr> retrieveAllStockTransferInHdr() {

		return stockTransferInHdrRepository.findAll();

	}

	@PostMapping("{companymstid}/stocktransferinhdr")
	public StockTransferInHdr createStockTransferInHdr(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody StockTransferInHdr stockTransferInHdr) {

		StockTransferInHdr saved= saveAndPublishService.saveStockTransferInHdr(stockTransferInHdr,mybranch,stockTransferInHdr.getFromBranch());
		
		if(null!=saved)
		{
//			saveAndPublishService.publishStockTransferToKafkaEvent(saved, mybranch, KafkaMapleEventType.STOCKTRANSFERINHDR,stockTransferInHdr.getFromBranch());
		}
		else {
			return null;
		}
		return saved;
	
	}

	@GetMapping("{companymstid}/stocktransferinbystatus")
	public List<StockTransferInHdr> retrieveAllStockTranHdrByStatus(
			@PathVariable(value = "companymstid") String companymstid) {

		return stockTransferInHdrRepository.fetchStockTransferInHdr(companymstid);

	}

	@GetMapping("{companymstid}/getvouchernosearch")
	public @ResponseBody List<StockTransferInHdr> searchCustomer(@RequestParam("data") String searchstring) {
		return stockTransferInHdrRepository.getPendingVoucherNumber("%" + searchstring + "%");
	}

	@PostMapping("{companymstid}/stocktransferinhdr/acceptstock")
	public StockTransferInHdr createStockTransferInAcceptStock(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody StockTransferInHdr stockTransferInHdr) {
		
		

		Optional<CompanyMst> compasnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst compasnyMst = compasnyMstOpt.get();

		String fromBranch = stockTransferInHdr.getFromBranch();  
		Date transDate = stockTransferInHdr.getVoucherDate();
		String voucherNumber = stockTransferInHdr.getVoucherNumber();
		String branchCode = stockTransferInHdr.getBranchCode();
		String vNo = stockTransferInHdr.getVoucherNumber();
		Optional<StockTransferInHdr> stockTransferInHdrOpt = stockTransferInHdrRepository.findById(stockTransferInHdr.getId());
		 
		stockTransferInHdr = stockTransferInHdrOpt.get();
		stockTransferInHdr.setStatusAcceptStock("COMPLETED");
		stockTransferInHdr.setBranchCode(branchCode);
		stockTransferInHdr.setCompanyMst(compasnyMst);
		stockTransferInHdr.setVoucherNumber(vNo);
		List<StockTransferInDtl> stockTransferInDtlList = stockTransferInDtlRepository
				.findBystockTransferInHdrIdAndCompanyMst(stockTransferInHdr.getId(), compasnyMst);
		Iterator itr = stockTransferInDtlList.iterator();
		while (itr.hasNext()) {
			StockTransferInDtl stockTransferInDtl = (StockTransferInDtl) itr.next();
		
			String itemId= stockTransferInDtl.getItemId().getId();
			
			double qty = stockTransferInDtl.getQty();
			
		
		

			String batchCode = stockTransferInDtl.getBatch();

			String barcode = stockTransferInDtl.getBarcode();
			double mrp = stockTransferInDtl.getMrp();

			
			//=======apply a common function for item_batch_mst updation and 
			// ======== item_batch_dtl insertion ====== by anandu === 25-06-2021 
			
			String processInstanceId = null;
			String taskId = null;
			Date ExpiryDate = null;
			
			itemBatchDtlService.itemBatchMstUpdation(barcode,batchCode,branchCode,
					ExpiryDate,itemId,mrp,processInstanceId,qty,taskId,compasnyMst,"MAIN");
			
			Double qtyOut = 0.0;
			String particulars = "STOCK TRANSFER IN FROM " + stockTransferInHdr.getFromBranch();
			final VoucherNumber voucherNo = voucherService.generateInvoice("STV", compasnyMst.getId());
			ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
			itemBatchDtl.setBarcode(barcode);
			itemBatchDtl.setBatch(batchCode);
			itemBatchDtl.setBranchCode(branchCode);
			itemBatchDtl.setExpDate(ExpiryDate);
			itemBatchDtl.setItemId(itemId);
			itemBatchDtl.setMrp(mrp);
			itemBatchDtl.setParticulars(particulars);
			itemBatchDtl.setProcessInstanceId(processInstanceId);
			itemBatchDtl.setQtyIn(qty);
			itemBatchDtl.setQtyOut(qtyOut);
			itemBatchDtl.setSourceDtlId(stockTransferInDtl.getId());
			itemBatchDtl.setSourceParentId(stockTransferInHdr.getId());
			itemBatchDtl.setSourceVoucherDate(transDate);
			itemBatchDtl.setSourceVoucherNumber(voucherNumber);
			itemBatchDtl.setStore("MAIN");
			itemBatchDtl.setTaskId(taskId);
			itemBatchDtl.setVoucherDate(transDate);
			itemBatchDtl.setVoucherNumber(voucherNo.getCode());
			itemBatchDtl.setCompanyMst(compasnyMst);

//			itemBatchDtl = itemBatchDtlRepo.saveAndFlush(itemBatchDtl);
			itemBatchDtl =saveAndPublishService.saveItemBatchDtl(itemBatchDtl, itemBatchDtl.getBranchCode());
			
			if (null!=itemBatchDtl)
			{
				saveAndPublishService.publishObjectToKafkaEvent(itemBatchDtl,
						itemBatchDtl.getBranchCode(), 
						KafkaMapleEventType.ITEMBATCHDTL,
						KafkaMapleEventType.SERVER,itemBatchDtl.getVoucherNumber());
			}
			
			else 
			{
				return null;
			}
//			
//			itemBatchDtlService.itemBatchDtlInsertionNew(barcode,batchCode,branchCode,
//					ExpiryDate,itemId,mrp,particulars,processInstanceId,
//					qty,qtyOut,stockTransferInDtl.getId(),stockTransferInHdr.getId(),transDate,voucherNumber,"MAIN",taskId,
//					transDate,voucherNo.getCode(),compasnyMst);
//		
			//================= end ======================== 25-06-2021 ===========
			
			
			AccountClass  accountClass = new AccountClass();
			
			accountClass.setCompanyMst(stockTransferInHdr.getCompanyMst());
			accountClass.setBrachCode(stockTransferInHdr.getBranchCode());
			accountClass.setFinancialYear(SystemSetting.getFinancialYear());
			accountClass.setTotalCredit(new BigDecimal(0.0));
			accountClass.setTotalDebit(new BigDecimal(0.0));
			
			//accountClassRepository.save(accountClass);
			accountClass=saveAndPublishService.saveAccountClass(accountClass, mybranch);
			if (null!=accountClass)
			{
				saveAndPublishService.publishObjectToKafkaEvent(accountClass, 
						mybranch, KafkaMapleEventType.ACCOUNTCLASS,
						KafkaMapleEventType.SERVER, accountClass.getId());
			}
			else {
				return null;
			}
			inventoryledgerMstService.acceptstockInventoryAccounting(stockTransferInHdr.getVoucherNumber(), 
					stockTransferInHdr.getVoucherDate(), stockTransferInHdr.getId(),stockTransferInHdr.getCompanyMst(), accountClass);

		
			

		}
 
//		stockTransferInHdr = stockTransferInHdrRepository.save(stockTransferInHdr);
		stockTransferInHdr=  saveAndPublishService.saveStockTransferInHdr(stockTransferInHdr,mybranch,stockTransferInHdr.getFromBranch());
		  
			if(null!=stockTransferInHdr)
			{
				saveAndPublishService.publishStockTransferToKafkaEvent(stockTransferInHdr, mybranch, KafkaMapleEventType.STOCKTRANSFERINHDR,stockTransferInHdr.getFromBranch(),stockTransferInHdr.getId());
			}
			else {
				return null;
			}
 

//		stockTransferInHdr = stockTransferInHdrRepository.save(stockTransferInHdr);
		stockTransferInHdr = saveAndPublishService.saveStockTransferInHdr(stockTransferInHdr,mybranch,stockTransferInHdr.getFromBranch());
		if(null!=stockTransferInHdr)
		{
			saveAndPublishService.publishStockTransferToKafkaEvent(stockTransferInHdr, mybranch, KafkaMapleEventType.STOCKTRANSFERINHDR,stockTransferInHdr.getFromBranch(),stockTransferInHdr.getId());
		}
		else {
			return null;
		}

		 
		

		 //stockTransferInHdr = stockTransferInHdrRepository.save(stockTransferInHdr);
//		 saveAndPublishService.saveStockTransferInHdr(stockTransferInHdr,mybranch,stockTransferInHdr.getFromBranch());
		 AcceptStockMessage acceptStockMessage= new AcceptStockMessage();
		 acceptStockMessage.setCompanyMst(compasnyMst);
		 acceptStockMessage.setStockTransferFrom(fromBranch);
		 acceptStockMessage.setVoucherNumber(stockTransferInHdr.getVoucherNumber());
		 acceptStockMessage.setStockTransferOutHdrId(stockTransferInHdr.getStockTransferOutHdrId());
		 acceptStockMessage=saveAndPublishService.saveAcceptStockMessage(acceptStockMessage,mybranch,stockTransferInHdr.getFromBranch());
		 

		
		 
		
		/*
		 * inventory Accounting
		 * 
		 */
		
		
		AccountClass  accountClass = new AccountClass();
		accountClass.setCompanyMst(stockTransferInHdr.getCompanyMst());
		accountClass.setBrachCode(stockTransferInHdr.getBranchCode());
		accountClass.setFinancialYear(SystemSetting.getFinancialYear());
		accountClass.setTotalCredit(new BigDecimal(0.0));
		accountClass.setTotalDebit(new BigDecimal(0.0));
		
		//accountClassRepository.save(accountClass);
		accountClass=saveAndPublishService.saveAccountClass(accountClass, mybranch);
		if (null!=accountClass)
		{
			saveAndPublishService.publishObjectToKafkaEvent(accountClass,
					mybranch, KafkaMapleEventType.ACCOUNTCLASS,
					KafkaMapleEventType.SERVER, accountClass.getId());
		}
		else {
			return null;
		}
		inventoryledgerMstService.acceptstockInventoryAccounting(stockTransferInHdr.getVoucherNumber(), 
				stockTransferInHdr.getVoucherDate(), stockTransferInHdr.getId(),stockTransferInHdr.getCompanyMst(), accountClass);

	
		
		
		
		
		return stockTransferInHdr;
 
		
 
	}
	
	
	

	@GetMapping("{companymstid}/stocktransfeinhdrresource/getstocktransferinhdrreport/{branchcode}")
	public List<StockTransferInHdr> getStockTransferInHdrl(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode, @RequestParam("fromdate") String fromdate,	@RequestParam("todate") String todate){
		java.util.Date fdate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		
		java.util.Date tdate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
 		return stockTransferInHdrRepository.getStockTransferInHdr(branchcode, fdate,tdate,companymstid );

	
 		
 		
	}
	

	@DeleteMapping("{companymstid}/stocktransfeinhdrresource/{hdrid}/stocktransferinhdrdelete")
	public void DeletepurchaseDtl(@PathVariable (value="hdrid") String hdrid,
			@PathVariable (value="companymstid") String companymstid) {
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		itemBatchDtlRepository.deleteBySourceParentId(hdrid);
		stockTransferInHdrRepository.deleteById(hdrid);
		List<SysDateMst> sysDateMstList = sysDateMstRepository.findAll();

		SysDateMst sysDateMst = sysDateMstList.get(0);

		try {
			itemBatchDtlService.updateItemBatchMstStockFromDtl(companyMst, sysDateMst.getAplicationDate());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

	}
	
	
	

	@GetMapping("{companymstid}/stocktransfeinhdrresource/getstocktransferinhdrbyid/{hdrid}")
	public StockTransferInHdr getStockTransferInHdrById(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("hdrid") String hdrid){
		
		StockTransferInHdr stockTransferInHdr =  stockTransferInHdrRepository.findByStockTransferOutHdrId(hdrid);
		
		if(null == stockTransferInHdr)
		{
			externalApi.getStocktransferFromServer(companymstid,hdrid);
			stockTransferInHdr =  stockTransferInHdrRepository.findByStockTransferOutHdrId(hdrid);

		}
	
 		return stockTransferInHdr;
 		
	}
	
	

	@GetMapping("{companymstid}/stocktransfeinhdrresource/getstocktransferinhdrbyvouchernumberin/{vouchernumberin}")
	public StockTransferInHdr  getStockTransferInHdrByVoucherNumberIn(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("vouchernumberin") String vouchernumberin){
		
	StockTransferInHdr  stockTransferInHdr =  stockTransferInHdrRepository.findByInVoucherNumber(vouchernumberin);
		
 	return stockTransferInHdr;
 		
	}
	
}
