package com.maple.restserver.resource;

import java.net.UnknownHostException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
 
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageHdr;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemBranchDtl;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.MenuMst;
import com.maple.restserver.entity.OtherBranchSalesTransHdr;
import com.maple.restserver.entity.PrinterMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesDltdDtl;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.StoreMst;
import com.maple.restserver.entity.SysDateMst;
import com.maple.restserver.entity.UserMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemBranchDtlRepository;
import com.maple.restserver.repository.OtherBranchSalesTransHdrRepository;
import com.maple.restserver.repository.SysDateMstRepository;
import com.maple.restserver.service.DataTransferService;
import com.maple.restserver.service.ItemBatchDtlDailyService;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.service.MessagingService;
import com.maple.restserver.service.MysqlDataTransferService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class DataTransferResource {
	
	private static final Logger logger = LoggerFactory.getLogger(DataTransferResource.class);

	@Autowired
	ItemBatchDtlDailyService itemBatchDtlDailyService;

	@Value("${serverorclient}")
	private String serverorclient;

	@Autowired
	SysDateMstRepository sysDateMstRepository;

	@Autowired
	ItemBatchDtlService itemBatchDtlService;
 

 

	@Autowired
	DataTransferService dataTransfer;

	@Autowired
	MysqlDataTransferService mysqlDataTransferService;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;

	@Autowired
	private ItemBatchMstRepository itemBatchMstRepository;



	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	/*
	 * @GetMapping("{companymstid}/{branchcode}/datatransfermysql") public String
	 * doDataTransfermysql(@PathVariable(value = "companymstid") String
	 * companymstid,
	 * 
	 * @PathVariable(value = "branchcode") String branchcode) {
	 * 
	 * Optional<CompanyMst> companyMstOpt =
	 * companyMstRepository.findById(companymstid); CompanyMst companyMst =
	 * companyMstOpt.get();
	 * 
	 * try { dataTransfer.doItemTransfermysql(companyMst, branchcode); } catch
	 * (Exception e) { // TODO Auto-generated catch block logger.error(e.getMessage()); }
	 * 
	 * return "Item done";
	 * 
	 * }
	 */

	// http://localhost:8182/CGN/datatransfer_excluding_tax

	@GetMapping("{companymstid}/{branchcode}/datatransfer_excluding_tax")
	public String doDataTransferExTax(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.doItemTransferExTax(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "Item done";

	}

	/*
	 * This is for transfering stock from old db to mysql. Can be used in Hotcakes
	 */
	@GetMapping("{companymstid}/{branchcode}/datatransferstock")
	public String transferStock(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferStock(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "stock done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransfer")
	public String doDataTransfer(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.doItemTransfer(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "Item done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransfersalestranshdranddtl")
	public String doSalesTransHdrTransfer(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.doSalesTransHdrTransfer(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "Sales transhdr done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransferpurchasehdr")
	public String doPurchaseHdr(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.doPurchaseTransfer(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "Purchase transhdr done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransferpurchasedtl")
	public String doPurchaseDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.doPurchaseDtl(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "PurchaseDtl  done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransferkit")
	public String transferKit(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferKit(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "kit done";

	}

	@GetMapping("{companymstid}/datatransfercompanymst")
	public String transferCompanyMst(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferCompanyMst(companyMst);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "companyMst done";

	}

//	@GetMapping("{companymstid}/datatransfer/updatesupplieridasaccountid")
//	public String updateSupplierId(@PathVariable(value = "companymstid") String companymstid) {
//		dataTransfer.updateSupplierIdAsAccountId();
//		return "Done";
//	}

	// This is used when a duplicate entry in salestranshdr during bulk otherbranch
	// fetch
	@Transactional
	@GetMapping("{companymstid}/datatransfer/removeduplicatevoucherinsalestranshdr")
	public String removeDuplication(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "rdate") String rdate) {
		Date udate = SystemSetting.StringToUtilDate(rdate, "yyyy-MM-dd");
		dataTransfer.removeDuplicateVOucherinSalesTransHdr(udate);
		return "DOne";
	}

	@GetMapping("{companymstid}/{branchcode}/datatransferminstock")
	public String transferMinStock(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {

			System.out.println("Starit Min Stock");
			dataTransfer.transferMinStock(companyMst, branchcode);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "stock done";

	}

	@GetMapping("{companymstid}/{branchcode}/stockmsttoserver")
	public String pushstocktoserver(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		// JmsTemplate jmsTemplate =
		// RestserverApplication.applicationContext.getBean(JmsTemplate.class);

		List<ItemBatchMst> ItemBatchMstList = itemBatchMstRepository.findAll();

		String msgto = "server.itembatchmstbulk";
		List dataList = ItemBatchMstList;
		//messagingService.sendMessage(msgto, dataList);
		// jmsTemplate.convertAndSend("server.itembatchmstbulk", ItemBatchMstList);

		return "stock to server done";

	}

	@GetMapping("{companymstid}/{branchcode}/stockdtltoserver")
	public String pushstockdtltoserver(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		// JmsTemplate jmsTemplate =
		// RestserverApplication.applicationContext.getBean(JmsTemplate.class);

		List<ItemBatchDtl> ItemBatchDtlList = itemBatchDtlRepository.findAll();
		String msgto = "server.itembatchmstbulk";
		List dataList = ItemBatchDtlList;
		//messagingService.sendMessage(msgto, dataList);
		// jmsTemplate.convertAndSend("server.itembatchdtlbulk", ItemBatchDtlList);

		return "stock to server done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransfercustomer")
	public String transferCustomer(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferCustomer(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "customer done";

	}

	@GetMapping("{companymstid}/{branchcode}/savecustomertoserver")
	public String saleCustomerToServer(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		// JmsTemplate jmsTemplate =
		// RestserverApplication.applicationContext.getBean(JmsTemplate.class);
		
		/*
		 * modified due to change the customer mst into account heads========07/01/2022
		 */

//		List<CustomerMst> customerMstList = customerMstRepository.findAll();
		List<AccountHeads> accountHeadsList = accountHeadsRepository.findAll();

		String msgto = "server.customerbulk";
		List dataList = accountHeadsList;
		//messagingService.sendMessage(msgto, dataList);

		// jmsTemplate.convertAndSend("server.customerbulk", customerMstList);

		return "Customer saved to server";

	}

	@GetMapping("{companymstid}/{branchcode}/saveaccountheadstoserver")
	public String saleAccountHeadsToServer(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

//	 JmsTemplate jmsTemplate = RestserverApplication.applicationContext.getBean(JmsTemplate.class);

		List<AccountHeads> accountHeadsList = accountHeadsRepository.findAll();

		String msgto = "server.accountheadsbulk";
		List dataList = accountHeadsList;
		//messagingService.sendMessage(msgto, dataList);

		// jmsTemplate.convertAndSend("server.accountheadsbulk", accountHeadsList);

		return "AccountHeads saved to server";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransferaccountheads")
	public String transferAccountHeads(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferAccountHeads(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "account heads done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransfersupplier")
	public String transferSupplier(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferSupplier(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "Supplier done";

	}

//	@GetMapping("{companymstid}/{branchcode}/savesuppliertoserver")
//	public String saleSupplierToServer(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "branchcode") String branchcode) {
//
//		// JmsTemplate jmsTemplate =
//		// RestserverApplication.applicationContext.getBean(JmsTemplate.class);
//
//		List<Supplier> supplierList = supplierRepository.findAll();
//
//		String msgto = "server.supplierbulk";
//		List dataList = supplierList;
//		//messagingService.sendMessage(msgto, dataList);
//
//		// jmsTemplate.convertAndSend("server.supplierbulk", supplierList);
//
//		return "Supplier saved to server";
//
//	}

	@GetMapping("{companymstid}/{branchcode}/datatransferpurchase")
	public String transferPurchase(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferPurchase(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "purchase done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransfersales")
	public String transferSales(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferSales(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "Sales done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransferotherbranchsales")
	public String transferOtherBranchSales(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferOtherBranchSales(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "Sales done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransferreceipts")
	public String transferReceipts(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferReceipts(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "datatransferreceipts done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransferpayments")
	public String transferPayments(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferPayments(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "datatransferpayments done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransferjournal")
	public String transferJournal(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferJournal(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "datatransferjournal done";

	}
	/*
	 * String transferSupplier(CompanyMst companyMst, String branchCode ) throws
	 * UnknownHostException, Exception ; String transferAccountHeads(CompanyMst
	 * companyMst, String branchCode ) throws UnknownHostException, Exception ;
	 * String transferAccountBalance(CompanyMst companyMst, String branchCode )
	 * throws UnknownHostException, Exception ; String transferPurchase(CompanyMst
	 * companyMst, String branchCode) throws UnknownHostException, Exception ;
	 * String transferSales(CompanyMst companyMst, String branchCode) throws
	 * UnknownHostException, Exception ; String transferReceipts(CompanyMst
	 * companyMst, String branchCode) throws UnknownHostException, Exception ;
	 * String transferPayments(CompanyMst companyMst, String branchCode) throws
	 * UnknownHostException, Exception ; String transferJournal(CompanyMst
	 * companyMst, String branchCode) throws UnknownHostException, Exception ;
	 * 
	 */

	/*
	 * Data Correction select * from item_batch_dtl where source_voucher_number
	 * ='2019-2020G000021'; select * from sales_dtl where sales_trans_hdr_id =
	 * (select id from sales_trans_hdr where voucher_number ='2019-2020G000021') ;
	 * select * from item_batch_dtl where source_voucher_number ='2019-2020G000021'
	 * AND item_id not in (select item_id from sales_dtl where sales_trans_hdr_id =
	 * (select id from sales_trans_hdr where voucher_number ='2019-2020G000021') );
	 */

	@GetMapping("{companymstid}/{branchcode}/salesdatacorrection")
	public String salesdatafromitembatchdtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.salesDataCorrectionFromItemBatchDtl(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "data correction done";

	}

	@GetMapping("{companymstid}/{branchcode}/changeseries/{prefix}/{newprefix}")
	public String changeSeries(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @PathVariable(value = "prefix") String prefix,
			@PathVariable(value = "newprefix") String newprefix) {
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		dataTransfer.updateBillSeries(companyMst, branchcode, prefix, newprefix);

		return "Done";
	}

	@Transactional
	@GetMapping("{companymstid}/{branchcode}/datatransfer/updateexpdate")
	public String updateExpDate(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		Date uDate = ClientSystemSetting.StringToUtilDate("2080-12-01", "yyyy-MM-dd");
		itemBatchDtlRepository.updateExpdate(uDate);

		return "Done";
	}

	@Transactional
	@GetMapping("{companymstid}/{branchcode}/salesdatacorrection2")
	public String removeitembatchdtlduplicate(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.removeItemBatchDuplicate(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "data correction 2 done";

	}

	@Transactional
	@GetMapping("{companymstid}/{branchcode}/updatestockbranch")
	public String updatestockbranch(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.updateItemBatchMstStockBranch(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "Stock update  branch done";

	}

	@Transactional
	@GetMapping("{companymstid}/{branchcode}/updatestockfromdtl")
	public String updateItemBatchMstStockFromDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		List<SysDateMst> sysDateMstList = sysDateMstRepository.findAll();

		SysDateMst sysDateMst = sysDateMstList.get(0);

		try {
			itemBatchDtlService.updateItemBatchMstStockFromDtl(companyMst, sysDateMst.getAplicationDate());
			itemBatchDtlDailyService.updateItemBatchDtlDailyFromItemBatchDtl(companyMst,
					sysDateMst.getAplicationDate());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "Stock update done";

	}

	@Transactional
	@GetMapping("{companymstid}/{branchcode}/updatepurchasedtl")
	public String updateUnitFromPurchaseDtl(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.updateUnitFromPurchaseDtl(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "Purchase update done";

	}

	@Transactional
	@GetMapping("{companymstid}/{branchcode}/updatepricedefinition")
	public String updateUnitFromPriceDefinition(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.updateUnitFromPriceDefenition(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "PriceDefinition update done";

	}

	/*
	 * Below url is written on may 18 th to correct voucher date . based on starting
	 * and ending voucher number
	 */

	// http://localhost:8082/AMB/AMB/updatevoucherdate?startvoucher=3248&endvoucher=3250&olddate=2020-05-08&newdate=2020-05-08
	@Transactional
	@GetMapping("{companymstid}/updatevoucherdate/{branchcode}")
	public String updatenewdate(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam(value = "startvoucher") String startnumber,
			@RequestParam(value = "endvoucher") String endnumber, @RequestParam(value = "olddate") String olddate,
			@RequestParam(value = "newdate") String newdate,
			@RequestParam(value = "vouchersuffix") String vouchersuffix) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		java.sql.Date newDate = SystemSetting.StringToSqlDate(newdate, "yyyy-MM-dd");
		java.sql.Date oldDate = SystemSetting.StringToSqlDate(olddate, "yyyy-MM-dd");
		int sffixLength = vouchersuffix.length();
		String voucherStartngNumber = startnumber.replace(vouchersuffix, "");
		String sVoucher = voucherStartngNumber.trim();
		int startVoucherNumber = Integer.valueOf(sVoucher);

		String entVoucherNumber = endnumber.replace(vouchersuffix, "");
		String eVoucher = entVoucherNumber.trim();
		int endVoucherNumber = Integer.valueOf(eVoucher);
		try {

			dataTransfer.updateUDateCorrection(companyMst, branchcode, startVoucherNumber, endVoucherNumber, oldDate,
					newDate, vouchersuffix);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "PriceDefinition update done";

	}

	@GetMapping("{companymstid}/datatransfermembermst")
	public String transferMemberMst(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferMemberMst(companyMst);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "familymst transfer done";

	}

	@GetMapping("{companymstid}/datatransferfamilymst")
	public String transferFamilyMst(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferFamilyMst(companyMst);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "familymst transfer done";

	}

	@GetMapping("{companymstid}/datatransferorganisation")
	public String transferOrganisation(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferOrganisation(companyMst);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "organisation transfer done";

	}

	@GetMapping("{companymstid}/datatransferorgaccountheads")
	public String transferOrgAccount(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferOrgAccountHeads(companyMst);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "Account heads transfer completed";

	}
	// update sales_trans_hdr set voucher_date = '2020-05-18:00.000+0000' where
	// numeric_voucher_number >= 1111

	@GetMapping("{companymstid}/{branchcode}/datatransferitembatchdtl")
	public String doItemBatchDtlTransfer(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.doItemBatchDtlTransfer(companyMst, branchcode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "Itembatchdtl done";

	}

	@GetMapping("{companymstid}/datatransferbranch")
	public String doDataTransferBranch(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.doDataTransferBranch(companyMst);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "BranchMst done";

	}

	@GetMapping("{companymstid}/datatransferusermst")
	public String doDataTransferUserMst(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.doDataTransferUserMst(companyMst);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "UserMst done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransferunitmst")
	public String doDataTransferUnitMst(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.doDataTransferUnitMst(companyMst, branchcode);
		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		return "Unitdone";

	}

	// http://localhost:8182HOTCAKES/datatransferstockfromfile/kdrm.txt

	@GetMapping("{companymstid}/datatransferstockfromfile/{filename}")
	public String doDataTransferStockFromFile(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "filename") String filename) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			dataTransfer.transferStockFromFile(companyMst, filename);
		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		return "stock From File Done";

	}

	// http://localhost:8185/AMB/WBH/transfermastermysql
	@GetMapping("{companymstid}/{branchcode}/transfermastermysql")
	public String transfermastermysql(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchCode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			System.out.println("branchMstTransfermysql Starting");

			mysqlDataTransferService.branchMstTransfermysql(companyMst, branchCode);
		} catch (Exception e) {

			logger.error(e.getMessage());
		}
		System.out.println("transferUserMstMysql Starting");
		mysqlDataTransferService.transferUserMstMysql(companyMst, branchCode);

		System.out.println("transferAccountHeads Starting");
		try {
			mysqlDataTransferService.transferAccountHeads(companyMst, branchCode);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		System.out.println("transferCategoryMst Starting");
		mysqlDataTransferService.transferCategoryMst(companyMst, branchCode);

		System.out.println("unitMstTransfermysql Starting");
		try {
			mysqlDataTransferService.unitMstTransfermysql(companyMst, branchCode);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		System.out.println("doItemTransfermysql Starting");

		try {
			mysqlDataTransferService.doItemTransfermysql(companyMst, branchCode);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		System.out.println("priceDefenitionTransfermysql Starting");
		try {
			mysqlDataTransferService.priceDefenitionTransfermysql(companyMst, branchCode);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		System.out.println("customerMstTransfermysql Starting");
		mysqlDataTransferService.customerMstTransfermysql(companyMst, branchCode);

		System.out.println("localCustomerMstTransfermysql Starting");
		mysqlDataTransferService.localCustomerMstTransfermysql(companyMst, branchCode);

		System.out.println("supplierTransfermysql Starting");
		mysqlDataTransferService.supplierTransfermysql(companyMst, branchCode);

		System.out.println("salesManMstTransfermysql Starting");
		mysqlDataTransferService.salesManMstTransfermysql(companyMst, branchCode);

		System.out.println("salesPropertiesConfigMstTransfermysql Starting");
		mysqlDataTransferService.salesPropertiesConfigMstTransfermysql(companyMst, branchCode);

		System.out.println("salesTypeMstTransfermysql Starting");
		mysqlDataTransferService.salesTypeMstTransfermysql(companyMst, branchCode);

		System.out.println("addKotWaiterTransfermysql Starting");
		try {
			mysqlDataTransferService.addKotWaiterTransfermysql(companyMst, branchCode);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		System.out.println("transferKotTable Starting");
		try {
			mysqlDataTransferService.transferKotTable(companyMst, branchCode);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println("processPermissionMstTransfermysql Starting");
		try {
			mysqlDataTransferService.processPermissionMstTransfermysql(companyMst, branchCode);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		System.out.println("deliveryBoyMstTransfermysql Starting");
		try {
			mysqlDataTransferService.deliveryBoyMstTransfermysql(companyMst, branchCode);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		System.out.println("paramValueConfigTransfermysql Starting");
		mysqlDataTransferService.paramValueConfigTransfermysql(companyMst, branchCode);

		System.out.println("orderTakerMstTransfermysql Starting");
		mysqlDataTransferService.orderTakerMstTransfermysql(companyMst, branchCode);

		System.out.println("receiptModeMstTransfermysql Starting");
		mysqlDataTransferService.receiptModeMstTransfermysql(companyMst, branchCode);

		System.out.println("invoiceFormatMstTransfermysql Starting");
		mysqlDataTransferService.invoiceFormatMstTransfermysql(companyMst, branchCode);

		System.out.println("transferAccountHeads Starting");
		mysqlDataTransferService.transferMenuMst(companyMst, branchCode);

		System.out.println("priceDefenitionTransfermysql Starting");
		try {
			mysqlDataTransferService.priceDefenitionTransfermysql(companyMst, branchCode);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		System.out.println("kotCategoryMstTransfermysql Starting");
		try {
			mysqlDataTransferService.kotCategoryMstTransfermysql(companyMst, branchCode);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		try {
			System.out.println("kotItemMstTransfermysql Starting");
			mysqlDataTransferService.kotItemMstTransfermysql(companyMst, branchCode);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		try {
			System.out.println("itemDeviceMstTransfermysql Starting");
			mysqlDataTransferService.itemDeviceMstTransfermysql(companyMst, branchCode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		// System.out.println("vouchernumbermysql Starting");
		// mysqlDataTransferService.vouchernumbermysql(companyMst, branchCode);

		try {
			System.out.println("sequencemysql Starting");
			mysqlDataTransferService.sequencemysql(companyMst, branchCode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		System.out.println("financialYearMstTransfermysql Starting");
		try {
			mysqlDataTransferService.financialYearMstTransfermysql(companyMst, branchCode);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		try {
			System.out.println("itemBatchDtlFromDerbyToMysql Starting");
			mysqlDataTransferService.itemBatchDtlFromDerbyToMysql(companyMst, branchCode);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		try {
			System.out.println("kitDefinitionMstFromDerbyToMysql Starting");
			mysqlDataTransferService.kitDefinitionMstFromDerbyToMysql(companyMst, branchCode);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		try {
			System.out.println("transferMenuMst Starting");
			mysqlDataTransferService.transferMenuMst(companyMst, branchCode);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		try {
			System.out.println("printerMstTransfermysql Starting");
			mysqlDataTransferService.printerMstTransfermysql(companyMst, branchCode);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		try {
			System.out.println("menuWindowMstTransfermysql Starting");
			mysqlDataTransferService.menuWindowMstTransfermysql(companyMst, branchCode);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		System.out.println("processMstTransfermysql Starting");
		mysqlDataTransferService.processMstTransfermysql(companyMst, branchCode);

		System.out.println("menuConfigMstTransfermysql Starting");
		mysqlDataTransferService.menuConfigMstTransfermysql(companyMst, branchCode);

		System.out.println("menuTransfermysql Starting");
		// mysqlDataTransferService.menuTransfermysql(companyMst, branchCode);

		// voucher_number
		// sequence

		return "stock From File Done";

	}

//	@GetMapping("{companymstid}/{branchcode}/transfertransactionmysql")
//	public String transfertransactionmysql(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "branchcode") String branchcode) {
//
//		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
//		CompanyMst companyMst = companyMstOpt.get();
//
//		try {
//
//			mysqlDataTransferService.transferSalesFromDerbyToMysql(companyMst, branchcode);
//			mysqlDataTransferService.transferPurchaseHdrMysql(companyMst, branchcode);
//			mysqlDataTransferService.paymentTransferToMysql(companyMst, branchcode);
//			mysqlDataTransferService.salesReceiptsTransfermysql(companyMst, branchcode);
//
//			mysqlDataTransferService.accountClassDataTransferFromDerbyToMysql(companyMst, branchcode);
//			mysqlDataTransferService.ledgerClassDataTransferFromDerbyToMysql(companyMst, branchcode);
//			mysqlDataTransferService.debitClassDataTransferFromDerbyToMysql(companyMst, branchcode);
//			mysqlDataTransferService.creditClassDataTransferFromDerbyToMysql(companyMst, branchcode);
//
//		} catch (Exception e) {
//
//			logger.error(e.getMessage());
//		}
//
//		return "stock From File Done";
//
//	}

	@GetMapping("{companymstid}/{branchcode}/transfertransactionmysqltestsalestest")
	public String transfertransactionmysqltestsales(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {

			mysqlDataTransferService.transferSalesFromDerbyToMysql(companyMst, branchcode);

		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		return "stock From File Done";

	}

	@GetMapping("{companymstid}/{branchcode}/transfertransactionmysql")
	public String transfertransactionmysql(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {

			mysqlDataTransferService.transferSalesFromDerbyToMysql(companyMst, branchcode);
			mysqlDataTransferService.transferPurchaseHdrMysql(companyMst, branchcode);
			mysqlDataTransferService.paymentTransferToMysql(companyMst, branchcode);

			mysqlDataTransferService.accountClassDataTransferFromDerbyToMysql(companyMst, branchcode);

			// mysqlDataTransferService.debitClassDataTransferFromDerbyToMysql(companyMst,
			// branchcode);
			// mysqlDataTransferService.creditClassDataTransferFromDerbyToMysql(companyMst,
			// branchcode);
			mysqlDataTransferService.itemBatchDtlFromDerbyToMysql(companyMst, branchcode);

		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		return "stock From File Done";

	}

	@GetMapping("{companymstid}/{branchcode}/datatransferitemromfile/{filename}")
	public String datatransferitemromfile(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @PathVariable(value = "filename") String filename) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {
			System.out.println("starting process");
			dataTransfer.transferItemFromFileCarPalace(companyMst, filename, branchcode);
		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		return "Item From File Done";

	}

//	@GetMapping("{companymstid}/{branchcode}/datatransferitemromfile/{filename}")
//	public String datatransferitemromfile(@PathVariable(value = "companymstid") String companymstid,
//			@PathVariable(value = "branchcode") String branchcode, @PathVariable(value = "filename") String filename) {
//
//		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
//		CompanyMst companyMst = companyMstOpt.get();
//
//		try {
//			System.out.println("starting process");
//			dataTransfer.transferItemFromFileCarPalace(companyMst, "data.csv", branchcode);
//		} catch (Exception e) {
//
//			logger.error(e.getMessage());
//		}
//
//		return "stock From File Done";
//
//	}

//		@GetMapping("{companymstid}/{branchcode}/transfermastermysql")
//		public String transfermastermysql(@PathVariable(value = "companymstid") String companymstid,
//				@PathVariable(value = "branchcode") String branchCode) {
//
//			Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
//			CompanyMst companyMst = companyMstOpt.get();
//
//			try {
//
//				mysqlDataTransferService.branchMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.transferUserMstMysql(companyMst, branchCode);
//				mysqlDataTransferService.transferAccountHeads(companyMst, branchCode);
//				mysqlDataTransferService.transferCategoryMst(companyMst, branchCode);
//				mysqlDataTransferService.unitMstTransfermysql(companyMst, branchCode);
//
//				mysqlDataTransferService.doItemTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.priceDefenitionTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.customerMstTransfermysql(companyMst, branchCode);
//
//				mysqlDataTransferService.localCustomerMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.supplierTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.salesManMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.salesPropertiesConfigMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.salesTypeMstTransfermysql(companyMst, branchCode);
//
//				mysqlDataTransferService.addKotWaiterTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.transferKotTable(companyMst, branchCode);
//				mysqlDataTransferService.processPermissionMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.deliveryBoyMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.paramValueConfigTransfermysql(companyMst, branchCode);
//
//				mysqlDataTransferService.orderTakerMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.receiptModeMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.invoiceFormatMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.transferMenuMst(companyMst, branchCode);
//				mysqlDataTransferService.menuConfigMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.transferMenuMst(companyMst, branchCode);
//
//				mysqlDataTransferService.priceDefenitionTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.kotCategoryMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.kotItemMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.itemDeviceMstTransfermysql(companyMst, branchCode);
//				mysqlDataTransferService.vouchernumbermysql(companyMst, branchCode);
//				mysqlDataTransferService.sequencemysql(companyMst, branchCode);
//
//				// voucher_number
//				// sequence
//
//			} catch (Exception e) {
//
//				logger.error(e.getMessage());
//			}
//
//			return "stock From File Done";
//
//		}

	@GetMapping("{companymstid}/{branchcode}/transfertransactionmysqltestpricedefinitiontest")
	public String transfertransactionmysqltestpricedefinition(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {

			mysqlDataTransferService.priceDefenitionTransfermysql(companyMst, branchcode);

		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		return "Price definition Done";

	}
	
	
	@GetMapping("{companymstid}/{branchcode}/transfertransactionmysqltestso")
	public String transfertransactionmysqltestSo(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {

			mysqlDataTransferService.soTransfermysql(companyMst, branchcode);

		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		return "Price definition Done";

	}
	
	@GetMapping("{companymstid}/datatransferresource/stockmigration/{branchcode}")
	public String stockMigration(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {

			
			mysqlDataTransferService.openingStockDtlStockMigration(companyMst, branchcode);

		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		return "stock From File Done";

	}

	

	@GetMapping("{companymstid}/datatransferresource/itemmstmigration/{branchcode}")
	public String itemmysqlMigration(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		mysqlDataTransferService.transferCategoryMst(companyMst, branchcode);

		System.out.println("unitMstTransfermysql Starting");
		try {
			mysqlDataTransferService.unitMstTransfermysql(companyMst, branchcode);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		System.out.println("doItemTransfermysql Starting");

		try {
			mysqlDataTransferService.doItemTransfermysql(companyMst, branchcode);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		return "stock From File Done";

	}
	@GetMapping("{companymstid}/datatransferresource/vouchernumbersequencemigration/{branchcode}")
	public String sequencemysqlMigration(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();

		try {

//			mysqlDataTransferService.itemBatchDtlFromDerbyToMysql(companyMst, branchcode);

			mysqlDataTransferService.sequencemysql(companyMst, branchcode);

		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		return "stock From File Done";

	}

	
	
	


}
