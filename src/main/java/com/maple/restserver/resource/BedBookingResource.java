
package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.his.entity.BedBooking;
import com.maple.restserver.repository.BedBookingRepository;



@RestController
@Transactional
public class BedBookingResource {
	@Autowired
	private BedBookingRepository bedbooking;
	@GetMapping("/{companymstid}/bedbook")
	public List<BedBooking> retrieveAlldepartments(@PathVariable(value = "companymstid") String companymstid)
	{
		return bedbooking.findByCompanyMstId(companymstid);
		
	}
	@PostMapping("{companymstid}/bedbook")
	public ResponseEntity<Object>createUser(@Valid @RequestBody BedBooking bedbooking1)
	{
		BedBooking saved=bedbooking.saveAndFlush(bedbooking1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}

}
