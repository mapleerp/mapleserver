package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayBook;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.RawMaterialReturnHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DayBookRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;

@RestController
@Transactional
public class DayBookReportResource {
	
private static final Logger logger = LoggerFactory.getLogger(DamageDtlResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	 EventBus eventBus = EventBusFactory.getEventBus();

	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	DayBookRepository dayBookRepository;
	@Autowired
	CompanyMstRepository 	companyMstRepository;
	
	 @Autowired
	 SaveAndPublishService saveAndPublishService;
	
	@PostMapping("{companymstid}/daybook")
	public DayBook createDayBookEntry(@Valid @RequestBody DayBook dayBook,
			@PathVariable (value = "companymstid") String companymstid)
	{
		
		
		  Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();
			dayBook.setCompanyMst(companyMst);
//			DayBook saved=dayBookRepository.saveAndFlush(dayBook);
			DayBook saved=saveAndPublishService.saveDayBook(dayBook, dayBook.getBranchCode());
			logger.info("saveDayBook send to KafkaEvent: {}", saved);
			
			Map<String, Object> variables = new HashMap<String, Object>();
			
			variables.put("voucherNumber", saved.getSourceVoucheNumber());
			variables.put("voucherDate", saved.getSourceVoucherDate());
			variables.put("inet", 0);
			variables.put("REST", 1);
			variables.put("id", saved.getId());
			variables.put("companyid", saved.getCompanyMst());
			
			variables.put("branchcode", saved.getBranchCode());

			variables.put("WF", "forwardDayBook");
			
			
			String workflow = (String) variables.get("WF");
    		String voucherNumber = (String) variables.get("voucherNumber");
    		String sourceID = (String) variables.get("id");


    		LmsQueueMst lmsQueueMst = new LmsQueueMst();

    		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
    		
    		//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
    		
    		
    		java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
    		java.sql.Date sqlVDate = ClientSystemSetting.UtilDateToSQLDate(uDate);
    		lmsQueueMst.setVoucherDate(sqlVDate);
    		
    		
    		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
    		lmsQueueMst.setVoucherType(workflow);
    		lmsQueueMst.setPostedToServer("NO");
    		lmsQueueMst.setJobClass("forwardDayBook");
    		lmsQueueMst.setCronJob(true);
    		lmsQueueMst.setJobName(workflow + sourceID);
    		lmsQueueMst.setJobGroup(workflow);
    		lmsQueueMst.setRepeatTime(60000L);
    		lmsQueueMst.setSourceObjectId(sourceID);
    		
    		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

    		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
    		variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);
			
			return saved;
	}

	@GetMapping("{companymstid}/daybookreport/{branchCode}")
	public List<DayBook> dayBookReport(@PathVariable(value = "companymstid") String
			  companymstid,
			 @PathVariable("branchCode") String branchCode,  @RequestParam("rdate") String reportdate){
		java.util.Date date = ClientSystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		Optional<CompanyMst> companyMst=companyMstRepository.findById(companymstid);
		
		
		
		return dayBookRepository.findBySourceVoucherDateAndBranchCodeAndCompanyMst(date,branchCode,companyMst.get());

	}
	
	@DeleteMapping("{companymstid}/daybook/daybookdelete/{voucher}")
	public void deleteDaybook(@PathVariable(value = "companymstid") String
			  companymstid,@PathVariable(value = "voucher") String
			  voucher)
	{
		dayBookRepository.deleteBySourceVoucheNumber(voucher);
	}
	
}
