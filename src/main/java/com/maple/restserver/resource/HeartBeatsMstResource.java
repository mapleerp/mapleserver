package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
 
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.HeartBeatsMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.HeartBeatsMstRepository;
import com.maple.restserver.service.HeartBeatsService;
import com.maple.restserver.service.MessagingService;

@RestController
@Transactional
public class HeartBeatsMstResource {
	
 
 
	@Autowired
	private HeartBeatsMstRepository heartBeatsMstRepository;
	
	@Autowired
	private HeartBeatsService heartBeatsService;

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	BranchMstRepository branchMstRepo;

	@GetMapping("{companymstid}/heartbeatsmst")
	public List<HeartBeatsMst> retrieveAllAcceptStock(@PathVariable(value = "companymstid") String companymstid) {
	
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		
		List<HeartBeatsMst> heartBeatList = heartBeatsMstRepository.findByCompanyMst(companyOpt.get());

		return heartBeatList;
	}

	@GetMapping("{companymstid}/sendheartbeatmsg/{setheartbeatsid}")
	public String sendHeartBeatMsg(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "setheartbeatsid") String setheartbeatsid) {
		List<HeartBeatsMst> heartBeatsMstList = new ArrayList<HeartBeatsMst>();

		heartBeatsMstRepository.updateOffLineStatus(companymstid);
		
		List<BranchMst> BranchList = branchMstRepo.findAll();
		
		HeartBeatsMst heartBeatsMst = new HeartBeatsMst();
		heartBeatsMst.setHeartBeatsId(setheartbeatsid);
		
		heartBeatsMstList.add(heartBeatsMst);

		Iterator iter01 = BranchList.iterator();
		while (iter01.hasNext()) {
			BranchMst branchMsttoSend = (BranchMst) iter01.next();

			String msgto = branchMsttoSend.getBranchCode() + ".heartbeats";
		
			//messageService.sendMessage(msgto, heartBeatsMstList);
			
			//jmsTemplate.convertAndSend(branchMsttoSend.getBranchCode() + ".heartbeats", heartBeatsMstList);

		}
		return "success";
	}

}
