package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JobCardOutHdr;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.JobCardOutHdrRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
@RestController
@Transactional
public class JobCardOutHdrResource {
	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository; 
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	@Autowired
	private JobCardOutHdrRepository jobCardOutHdrRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	@PostMapping("{companymstid}/jobcardouthdr")
	public JobCardOutHdr createJobCardOutHdr(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody JobCardOutHdr jobCardOutHdr)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			jobCardOutHdr.setCompanyMst(companyMst);
		
		
		return jobCardOutHdrRepository.saveAndFlush(jobCardOutHdr);
	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));
		
		
		
	
	}
	
	
	@PutMapping("{companymstid}/jobcardouthdr/{hdrid}/updatejobcardouthdr")
	public JobCardOutHdr updateJobCardHdr(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "hdrid") String hdrid,
			@Valid @RequestBody JobCardOutHdr jobCardOutHdrRequest)
	{
		return jobCardOutHdrRepository.findById(hdrid).map(jobCardOutHdr-> {
			
			Optional<CompanyMst> optCompany = companyMstRepo.findById(companymstid);
			jobCardOutHdr.setCompanyMst(optCompany.get());
			jobCardOutHdr.setVoucherDate(jobCardOutHdrRequest.getVoucherDate());
			jobCardOutHdr.setVoucherNumber(jobCardOutHdrRequest.getVoucherNumber());
			jobCardOutHdr.setJobCardVoucherDate(jobCardOutHdrRequest.getJobCardVoucherDate());
			jobCardOutHdr.setJobCardVoucherNumber(jobCardOutHdrRequest.getJobCardVoucherNumber());
		
		
			jobCardOutHdr = jobCardOutHdrRepository.saveAndFlush(jobCardOutHdr);
		
		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", jobCardOutHdr.getVoucherNumber());
		variables.put("voucherDate", jobCardOutHdr.getVoucherDate());
		variables.put("id", jobCardOutHdr.getId());
		variables.put("companyid", jobCardOutHdr.getCompanyMst());
		variables.put("branchcode", jobCardOutHdr.getFromBranch());

		variables.put("inet", 0);
		variables.put("REST", 1);

		variables.put("WF", "forwardJobCardOut");

		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));

		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);

		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardJobCardOut");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());
		eventBus.post(variables);

		return jobCardOutHdr;
		
	}).orElseThrow(() -> new ResourceNotFoundException("jobcardhdrid " +
			hdrid + " not found"));

	
	}
	
	
	@DeleteMapping("{companymstid}/deletejobcardouthdrbyid/{id}")
	public void JobCardOutDtlDelete(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String Id) {
		jobCardOutHdrRepository.deleteById(Id);
		jobCardOutHdrRepository.flush();

	}

}
