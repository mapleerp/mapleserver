package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.SaleOrderRealizedMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.SaleOrderRealizedMstRepository;

@RestController
@Transactional
public class SaleOrderRealizedMstResource {
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	SaleOrderRealizedMstRepository saleOrderRealizedMstRepository;

	@GetMapping("{companymstid}/saleorderrealizedmst/getallsaleorderrealizedmst")
	public List<SaleOrderRealizedMst> retrieveAllSaleOrderRealizedMst(
			@PathVariable(value = "companymstid") String companymstid) {
		return saleOrderRealizedMstRepository.findByCompanyMst(companymstid);
	}

	
	@PostMapping("{companymstid}/saleorderrealizedmst/savesaleorderrealizedmst")
	public SaleOrderRealizedMst createSaleOrderRealizedMst(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SaleOrderRealizedMst saleOrderRealizedMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			saleOrderRealizedMst.setCompanyMst(companyMst);

			return saleOrderRealizedMstRepository.saveAndFlush(saleOrderRealizedMst);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));

	}

}
