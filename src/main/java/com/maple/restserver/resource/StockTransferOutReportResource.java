package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.service.StockTransferReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class StockTransferOutReportResource {
	
	@Autowired
	StockTransferReportService  StockTransferReportService; 

	@GetMapping("{companymstid}/stocktransferoutreport/{voucherNumber}")
	public List<StockTransferOutReport> getStockTransferOutDtl(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("voucherNumber") String voucherNumber,  @RequestParam("rdate") String reportdate){
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
 		return StockTransferReportService.getStockTransferByVoucerAndDate(voucherNumber, date,companymstid );
	}
	
	
	@GetMapping("{companymstid}/stocktransferoutreport/getreportbyhdrid/{hdrid}")
	public List<StockTransferOutReport> getStockTransferOutDtl(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("hdrid") String hdrid
			 ){
		 
 		return StockTransferReportService.getStockTransferByHdrId(hdrid );
	}
	
	
	
	
	@GetMapping("{companymstid}/stocktransferoutreport/stocktransferoutsummaryreportbetweendate")
	public List<StockTransferOutReport> getStockTransferOutSummaryBetweenDate(
			@PathVariable(value = "companymstid") String companymstid,
			   @RequestParam("fromdate") String fromdate,
			   @RequestParam("todate") String todate  ){
		java.util.Date fdate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date tdate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
 		return StockTransferReportService.getStockTransferOutSummaryBetweenDate(fdate,tdate);
	}
	
	@GetMapping("{companymstid}/stocktransferoutreport/stockreportbetweendate/{branchcode}")
	public List<StockTransferOutReport> getStockTransferBetweenDate(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "branchcode")String branchcode,
			@RequestParam("categorylist")String categorylist,
			@RequestParam(value = "tobranch")String tobranch,
			@RequestParam("fromdate")String fromdate,
			@RequestParam("todate")String todate
			)
	{
		
		java.util.Date fudate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date tudate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		String []array = categorylist.split(";");
		if(array[0].equalsIgnoreCase("") && tobranch.equalsIgnoreCase(""))
		{
			return StockTransferReportService.getStockTransferOutDtlBetweenDate(fudate,tudate,branchcode,companymstid);

		}
		else if(array[0].equalsIgnoreCase("") && !tobranch.equalsIgnoreCase(""))
		{
			return StockTransferReportService.getStockTransferOutDtlBetweenDateAndToBranch(fudate,tudate,branchcode,companymstid,tobranch);

		}
		else if(tobranch.equalsIgnoreCase("") && !array[0].equalsIgnoreCase(""))
		{
			return StockTransferReportService.getStockTransferOutDtlBetweenDateAndCategoryList(fudate,tudate,branchcode,companymstid,array);

		}
		else
		{
			return StockTransferReportService.getStockTransferOutDtlBetweenDateAndCategoryListAndTobranch(fudate,tudate,branchcode,companymstid,array,tobranch);

		}
	}
	
	@GetMapping("{companymstid}/stocktransferoutreport/stockreportbetweendateandtobranch/{branchcode}/{tobranch}")
	public List<StockTransferOutReport> getStockTransferBetweenDateAndToBranch(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "branchcode")String branchcode,
			@PathVariable(value = "tobranch")String tobranch,
			@RequestParam("fromdate")String fromdate,
			@RequestParam("todate")String todate
			)
	{
		java.util.Date fudate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date tudate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		return StockTransferReportService.getStockTransferOutDtlBetweenDateAndToBranch(fudate,tudate,branchcode,companymstid,tobranch);
	}
	
	@GetMapping("{companymstid}/stocktransferoutreport/stockreportbetweendatecategory/{branchcode}")
	public List<StockTransferOutReport> getStockTransferBetweenDateAndCategoryList(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "branchcode")String branchcode,
			@RequestParam("fromdate")String fromdate,
			@RequestParam("todate")String todate,
			@RequestParam("categorylist")String categorylist
			)
	{
		String []array = categorylist.split(";");
		java.util.Date fudate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date tudate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		return StockTransferReportService.getStockTransferOutDtlBetweenDateAndCategoryList(fudate,tudate,branchcode,companymstid,array);
	}
	@GetMapping("{companymstid}/stocktransferoutreport/stockreportbetweendatecategory/{branchcode}/{tobranch}")
	public List<StockTransferOutReport> getStockTransferBetweenDateAndCategoryListAndToBranch(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "branchcode")String branchcode,
			@RequestParam("fromdate")String fromdate,
			@RequestParam("todate")String todate,
			@RequestParam("categorylist")String categorylist,
			@PathVariable(value = "tobranch")String tobranch
			)
	{
		String []array = categorylist.split(";");
		java.util.Date fudate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date tudate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		return StockTransferReportService.getStockTransferOutDtlBetweenDateAndCategoryListAndTobranch(fudate,tudate,branchcode,companymstid,array,tobranch);
	}
	
	
	
	 
}
