package com.maple.restserver.resource;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DailySalesSummary;
import com.maple.restserver.report.entity.CompanyDailySaleSummary;
import com.maple.restserver.report.entity.ReceiptModeReport;
import com.maple.restserver.report.entity.SaleModeWiseSummaryReport;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DailySalesSummaryRepository;
import com.maple.restserver.repository.SaleOrderReceiptRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.service.DailySaleSummaryService;
import com.maple.restserver.service.DayEndService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class DailySalesSummaryResource {

	@Autowired
	SaleOrderReceiptRepository saleOrderReceiptRepo;
	@Autowired
	SalesReceiptsRepository salesReceiptsRepo;
	@Autowired
	SalesTransHdrRepository salesTransHdrRepo;
	@Autowired
	private DayEndService dayEndService;
	@Autowired
	DailySaleSummaryService dailySaleSummaryService;

	@Autowired
	CompanyMstRepository companyMstRepository;

	@Autowired
	DailySalesSummaryRepository dailySalesSummaryRepo;

	@GetMapping("{companymstid}/dailysalessummary")
	public List<DailySalesSummary> getDailySalesSummaryNoDate() {

		return dailySalesSummaryRepo.findAll();
	}

	@GetMapping("{companymstid}/dailysalessummary/{branch}/dailysalessummary")
	public DailySalesSummary getDailySalesSummary(@PathVariable("branch") String branch,
			@RequestParam("reportdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dayEndService.getDailySalesSumary(branch, date);
	}

	@GetMapping("{companymstid}/dailysalessummary/{branch}/companydailysalessummary")
	public List<CompanyDailySaleSummary> getCompanyDailySalesSummary(@PathVariable("branch") String branch,
			@PathVariable("companymstid") String companymstid, @RequestParam("reportdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);

		return dayEndService.getCompanyDailySalesSumary(date, comapnyMstOpt.get());
	}

	@GetMapping("{companymstid}/dailybussinessummary/{branch}/dailybussinessummary")
	public List<Object> getDailyBussinessSummary(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branch") String branch, @RequestParam("reportdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "dd/MM/yyyy");

		return dailySaleSummaryService.getDailySaleSummary(branch, date, companymstid /* ,cashaccountid */ );

	}

	@GetMapping("{companymstid}/dailysalessummary/{branch}/dailypaymentmodewisereport")
	public List<SaleModeWiseSummaryReport> getPaymentModeWiseReport(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branch") String branch,
			@RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);

		return dailySaleSummaryService.getPaymentModeWiseReport(branch, date, companyMst.get());

	}

	@GetMapping("{companymstid}/dailybussinessummary/{branch}/{customerid}/sumofonlinesales")
	public Double getSumOfOnlineSale(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branch") String branch, @PathVariable(value = "customerid") String customerid,
			@RequestParam("reportdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySaleSummaryService.getSumOfOnlineSale(branch, date, companymstid, customerid);

	}

	@GetMapping("{companymstid}/dailysalessummary/{branch}/sumofreceiptmode")
	public List<ReceiptModeReport> getSumOfreceiptMode(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branch") String branch, @RequestParam("reportdate") String reportdate) {

		java.sql.Date sqlDate = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");
		// java.util.Date date =
		// SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return dailySaleSummaryService.getsalesReceipts(branch, sqlDate, companymstid);

	}

	@GetMapping("{companymstid}/dailysalessummary/{branch}/sumofreceiptmodebymode/{receiptmode}")
	public List<ReceiptModeReport> getSumOfreceiptMode(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("branch") String branch, @PathVariable("receiptmode") String receiptmode,
			@RequestParam("reportdate") String reportdate) {

		java.sql.Date sqlDate = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");
		// java.util.Date date =
		// SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return dailySaleSummaryService.getSalesReceiptsByMode(branch, sqlDate, companymstid, receiptmode);

	}

	@GetMapping("{companymstid}/dailysalessummary/{branch}/sumofreceiptmodebyaccountid/{accountid}")
	public List<ReceiptModeReport> getSumOfreceiptModeByAccountId(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branch") String branch,
			@PathVariable("accountid") String accountid, @RequestParam("reportdate") String reportdate) {

		java.sql.Date sqlDate = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");
		// java.util.Date date =
		// SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		return dailySaleSummaryService.getSalesReceiptsByModeByAccountId(branch, sqlDate, companymstid, accountid);

	}

	@GetMapping("{companymstid}/dailysalessummary/web/sumofreceiptmode")
	public List<ReceiptModeReport> getSumOfreceiptModeWeb(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("reportdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return dailySaleSummaryService.getsalesReceiptsWeb(date, companymstid);

	}

	@GetMapping("{companymstid}/dailysalessummary/sumofsaleorderreceiptmodebydate")
	public List<ReceiptModeReport> getSumOfSaleOrderreceiptMode(
			@PathVariable(value = "companymstid") String companymstid, @RequestParam("reportdate") String reportdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		return dailySaleSummaryService.getSaleOrdersalesReceipts(date, companymstid);

	}

	@GetMapping("{companymstid}/dailysalessummary/sumofsaleorderreceiptmodebydatebymode/{mode}")
	public List<ReceiptModeReport> getSumOfSaleOrderreceiptModeByMode(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "mode") String mode,
			@RequestParam("reportdate") String reportdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		return dailySaleSummaryService.getSaleOrdersalesReceiptsByMode(date, companymstid, mode);

	}

	@GetMapping("{companymstid}/dailysalessummary/gettotalcardpayment/{branchcode}")
	public Double getTotalCardPayment(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, 
			@RequestParam("reportdate") String reportdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		String mode = branchcode + "-CASH";
		Double total = 0.0;
		total = dailySaleSummaryService.getTotalCard(date, companymstid, mode);
		
		if(null == total)
		{
			total = 0.0;
		}

		return total;

	}

	@GetMapping("{companymstid}/dailysalessummary/gettotalcardpaymentso/{branchcode}")
	public Double getTotalCardPaymentSO(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam("reportdate") String reportdate) {

		java.sql.Date Sdate = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");
		System.out.println(Sdate + "date izzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");

		Double total = 0.0;
		total = dailySaleSummaryService.getTotalCardSO(companymstid, Sdate);

		return total;

	}

	@GetMapping("{companymstid}/dailysalessummary/sumofsales/{branchcode}")
	public Double getSumOfSales(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode, @RequestParam("reportdate") String reportdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		return salesTransHdrRepo.sumofTotalSale(date, branchcode);

	}
	
	
	@GetMapping("{companymstid}/dailysalessummary/branchwisesalessummary")
	public List<SalesInvoiceReport> getBranchWiseSalesSummary(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("reportdate") String reportdate) {

		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		return dailySaleSummaryService.getBranchWiseSalesSummary(date,companymstid);

	}
}
