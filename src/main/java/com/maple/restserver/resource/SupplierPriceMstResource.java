package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SupplierPriceMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SupplierPriceMstRepository;
import com.maple.restserver.service.SupplierPriceMstService;

@RestController
public class SupplierPriceMstResource {
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	
	@Autowired
	SupplierPriceMstRepository  supplierPriceMstRepository;
	
	@Autowired
	SupplierPriceMstService SupplierPriceMstService;
	
	
	//save 
	@PostMapping("{companymstid}/supplierpricemstresource/savesupplierpricemst")
	public SupplierPriceMst createSupplierPriceMst(
			@PathVariable(value="companymstid")	String commpanymstid,
			@Valid @RequestBody SupplierPriceMst supplierPriceMst)
	{
		CompanyMst companymst=CompanyMstRepository.findById(commpanymstid).get();
		supplierPriceMst.setCompanyMst(companymst);
		supplierPriceMst=supplierPriceMstRepository.save(supplierPriceMst);
		return supplierPriceMst;
	}
	
	
	// delete
	@DeleteMapping("{companymstid}/supplierpricemstresource/deletesupplierpricemst/{id}")
	public void DeleteSupplierPriceMst(@PathVariable(value = "id") String Id) {
	supplierPriceMstRepository.deleteById(Id);
	}
	
	
	//showing all 
	@GetMapping("{companymstid}/supplierpricemstresource/showallsupplierpricemst")
	public List<SupplierPriceMst> showallsupplierpricemst(
			
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = CompanyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		if (!companyMstOpt.isPresent()) {
			return null;
		}
		return SupplierPriceMstService.findAllSupplierPriceMst(companyMstOpt.get());
	}
	
	
	
}
