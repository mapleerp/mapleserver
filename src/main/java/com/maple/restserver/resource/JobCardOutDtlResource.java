package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.JobCardOutDtl;
import com.maple.restserver.entity.JobCardOutHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.JobCardDtlRepository;
import com.maple.restserver.repository.JobCardOutDtlRepository;
import com.maple.restserver.repository.JobCardOutHdrRepository;
@RestController
@Transactional
public class JobCardOutDtlResource {
	@Autowired
	private JobCardOutDtlRepository jobCardOutDtlRepository;
	
	@Autowired
	private JobCardOutHdrRepository jobCardOutHdrRepository;
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("{companymstid}/jobcardoutdtls/{hdrid}")
	public List<JobCardOutDtl> retrieveAllJobCardOutDtl(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "hdrid") String hdrid
			){
		Optional<JobCardOutHdr> jobCardOutHdr = jobCardOutHdrRepository.findById(hdrid);
		return jobCardOutDtlRepository.findByJobCardOutHdr(jobCardOutHdr.get());
	}
	
	
	@PostMapping("{companymstid}/jobcardoutdtl/{hdrid}")
	public JobCardOutDtl createDailyStock(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "hdrid") String hdrid,
			@Valid @RequestBody JobCardOutDtl requestJobCardOutDtl)
	{
		
		return jobCardOutHdrRepository.findById(hdrid).map(jobCardOutHdr-> {
			requestJobCardOutDtl.setJobCardOutHdr(jobCardOutHdr);
		
		
		return jobCardOutDtlRepository.saveAndFlush(requestJobCardOutDtl);
	}).orElseThrow(() -> new ResourceNotFoundException("jobcardhdrid " +
			hdrid + " not found"));
		
	
	}
	
	
	@DeleteMapping("{companymstid}/deletejobcardoutdtlbyid/{id}")
	public void JobCardOutDtlDelete(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String Id) {
		jobCardOutDtlRepository.deleteById(Id);
		jobCardOutDtlRepository.flush();

	}
	
	
	

}
