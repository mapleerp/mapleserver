package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.UserMst;
import com.maple.restserver.his.entity.BedMst;
import com.maple.restserver.repository.BedMstRepository;


@RestController
@Transactional
public class BedMstResource {
	@Autowired
	private BedMstRepository bedmaster;
	@GetMapping("/{companymstid}/bedmst")
	public List<BedMst> retrieveAlldepartments(@PathVariable(value = "companymstid") String companymstid)
	{
		return bedmaster.findByCompanyMstId(companymstid);
	
	}
	@PostMapping("{companymstid}/bedmst")
	public ResponseEntity<Object>createUser(@Valid @RequestBody BedMst BedMst1)
	{
		BedMst saved=bedmaster.saveAndFlush(BedMst1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}
	@GetMapping("/{companymstid}/test")
	public String test()
	{
		return "login";
		
	
	}

}
