package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.transaction.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DayEndReportStore;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DayEndReportStoreRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.service.DayEndReportStoreService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;
@RestController
@Transactional
public class DayEndReportStoreResource {
	
private static final Logger logger = LoggerFactory.getLogger(DayEndReportStoreResource.class);
	
	@Value("${mybranch}")
	private String mybranch;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	 
	 @Value("${serverorclient}")
		private String serverorclient;
	 
	 @Autowired
	 SaveAndPublishService saveAndPublishService;

	 
	
	@Autowired
	private DayEndReportStoreRepository dayEndReportStoreRepository;
	
	//-------------version 4.7

	@Autowired
	private DayEndReportStoreService dayEndReportStoreService;
	
	//-------------version 4.7 end

	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	@GetMapping("{companymstid}/dayendreportstorebydate/{branchcode}")
	public DayEndReportStore retrieveDayEndReportStoreByDate(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchcode") String branchcode,
			@RequestParam("reportdate") String reportdate){
		
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		List<DayEndReportStore> dayEndReportStore = dayEndReportStoreRepository.findByCompanyMstAndDate(companyMst.get(),date);
		if(dayEndReportStore.size()>0)
		{
			return dayEndReportStore.get(0);
		}
		else
		{
			return null;
		}
		
	}
	
	//-------------version 4.7
	
	@GetMapping("{companymstid}/dayendreportstore")
	public List<DayEndReportStore> DayEndReportStoreList(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("reportdate") String reportdate){
		
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		return dayEndReportStoreService.findDayEndReportByDate(companyMst.get(),date);
	}
	
	//-------------version 4.7 end
	

	//-------------version 6.7 surya
	
	@GetMapping("{companymstid}/dayendreportstoreandcard")
	public List<DayEndReportStore> DayEndReportWithCard2(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("reportdate") String reportdate){
		
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");

		Optional<CompanyMst> companyMst = companyMstRepo.findById(companymstid);
		return dayEndReportStoreService.findDayEndReportByDateAndCard(companyMst.get(),date);
	}
	
	//-------------version 6.7 surya end


	
	@PostMapping("{companymstid}/savedayendreportstore")
	public DayEndReportStore createDayEndReportStore(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			  DayEndReportStore dayEndReportStore)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			
			List<DayEndReportStore> dayEndReportStoreSaved = dayEndReportStoreRepository.findByCompanyMstAndDate(companyMst,dayEndReportStore.getDate());
		
			if(dayEndReportStoreSaved.size() > 0)
			{
				return null;
			}
			dayEndReportStore.setCompanyMst(companyMst);
		
		
//			DayEndReportStore dayEndReportStoreMst = dayEndReportStoreRepository.saveAndFlush(dayEndReportStore);
			DayEndReportStore dayEndReportStoreMst = saveAndPublishService.saveDayEndReportStore(dayEndReportStore, dayEndReportStore.getBranchCode());
			logger.info("DayEndReportStore send to KafkaEvent: {}", dayEndReportStoreMst);
			Map<String, Object> variables = new HashMap<String, Object>();
			
			variables.put("companyid",companyMst);
			
			variables.put("voucherNumber",dayEndReportStoreMst.getId());
			variables.put("voucherDate",dayEndReportStoreMst.getDate());
			variables.put("id",dayEndReportStoreMst.getId());
			
			variables.put("branchcode", dayEndReportStoreMst.getBranchCode());

			if(serverorclient.equalsIgnoreCase("REST")) {
				variables.put("REST",1);
			}else {
				variables.put("REST",0);
			}
			
			
			//variables.put("voucherDate", purchase.getVoucherDate());
			//variables.put("id",purchase.getId());
			variables.put("inet", 0);
			
			
			
			variables.put("WF", "forwardDayEndReportStore");
			
			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");


			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardDayEndReportStore");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);
			
			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			
			
			variables.put("lmsqid", lmsQueueMst.getId());
			
			
			eventBus.post(variables);
			
			return dayEndReportStoreMst;

	}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
			  companymstid + " not found"));
		
		
		
	
	}
	
	
	
	

}
