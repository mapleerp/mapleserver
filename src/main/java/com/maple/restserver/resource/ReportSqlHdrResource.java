package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.InsuranceCompanyMst;
import com.maple.restserver.entity.ItemNutrition;
import com.maple.restserver.entity.PurchaseOrderDtl;
import com.maple.restserver.entity.ReportSqlDtl;
import com.maple.restserver.entity.ReportSqlHdr;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ReportSqlDtlRepository;
import com.maple.restserver.repository.ReportSqlHdrRepository;

@RestController
@Transactional
public class ReportSqlHdrResource {
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	ReportSqlHdrRepository reportSqlHdrRepository;
	
	@Autowired
	ReportSqlDtlRepository reportSqlDtlRepository;
	
	
	
//	@GetMapping("{companymstid}/reportsqlhdrresource/allreportsqlhdr")
//	public List<ReportSqlHdr> retrieveAllReportSql(
//	@PathVariable(value ="companymstid")String companymstid)
//	{
//		return reportSqlHdrRepository.findByCompanyMstId(companymstid);
//	};
//	@PostMapping("{companymstid}/reportsqlhdrresource/reportsqlhdr")
//	public ReportSqlHdr createReportSqlHdr(@PathVariable(value = "companymstid") String companymstid ,
//			@Valid @RequestBody  ReportSqlHdr reportSqlHdr)
//	{
//		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
//		reportSqlHdr.setCompanyMst(companyMst.get());
//			return reportSqlHdrRepository.save(reportSqlHdr);
//
//	}
	@GetMapping("{companymstid}/reportsqlhdrresource/allreportsqlhdr")
	public List<ReportSqlHdr> retrieveAllReportSql(
	@PathVariable(value ="companymstid")String companymstid)
	{
		return reportSqlHdrRepository.findByCompanyMstId(companymstid);
	};

	//save 
			@PostMapping("{companymstid}/reportsqlhdrresource/savereportsqlhdr")
			public ReportSqlHdr createReportSqlHdr(
					@PathVariable(value="companymstid")	String commpanymstid,
					@Valid @RequestBody ReportSqlHdr reportSqlHdr)
			{
				CompanyMst companymst=companyMstRepo.findById(commpanymstid).get();
				reportSqlHdr.setCompanyMst(companymst);
				reportSqlHdr=reportSqlHdrRepository.save(reportSqlHdr);
				return reportSqlHdr;
			}
			
			// delete
			@DeleteMapping("{companymstid}/reportsqlhdrresource/deletereportsqlhdr/{id}")
			public void DeleteReportSqlHdr(@PathVariable(value = "id") String Id) {
				
				
				reportSqlHdrRepository.deleteById(Id);
			}	
			
			@GetMapping("{companymstid}/reportsqlhdrresource/getreportsqlhdrbyid/{reportsqlhdrid}")
			public ReportSqlHdr getReportSqlHdrById(
					@PathVariable(value = "companymstid")String companymstid,
					@PathVariable(value = "reportsqlhdrid")String reportsqlhdrid)
			{
				return reportSqlHdrRepository.findByCompanyMstIdAndId(companymstid,reportsqlhdrid);
			}
			
			@GetMapping("{companymstid}/reportsqlhdrresource/reportsqlhdrbyreportname/{reportName}")
			public List<ReportSqlHdr> getReportSqlDtlByHdrId(
					@PathVariable(value = "companymstid")String companymstid,
					@PathVariable(value = "reportName")String reportsqlhdrname)
			{
				return reportSqlHdrRepository.findByCompanyMstIdAndReportName(companymstid,reportsqlhdrname);
			}
			
			@PutMapping("{companymstid}/reportsqlhdrresource/reportsqlhdrupdate/{reportsqlhdr}")
			public ReportSqlHdr ReportSqlHdrUpdate(@PathVariable String reportsqlhdr, 
					@Valid @RequestBody ReportSqlHdr reportSqlHdrRequest)

			{
						    Optional<ReportSqlHdr> reportsqlhdrOpt = reportSqlHdrRepository.findById(reportSqlHdrRequest.getId());
						    ReportSqlHdr reportSqlHdr = reportsqlhdrOpt.get();
						    reportSqlHdr.setReportName(reportSqlHdrRequest.getReportName());
						    reportSqlHdr.setSqlString(reportSqlHdrRequest.getSqlString());

						    ReportSqlHdr saved = reportSqlHdrRepository.saveAndFlush(reportSqlHdr);
				            return saved;

			}
			
			@GetMapping("{companymstid}/reportsqlhdrresource/allreportsqlhdrbyuser/{userid}")
			public List<ReportSqlHdr> retrieveAllReportSql(@PathVariable(value = "userid") String userid,
					@PathVariable(value = "companymstid") String companymstid) {
				return reportSqlHdrRepository.findByReportSqlHdrByCompanyAndUser(companymstid, userid);

			};
}
