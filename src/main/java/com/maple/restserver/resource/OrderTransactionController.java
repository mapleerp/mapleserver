package com.maple.restserver.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.OrderTransaction;

@RestController

@RequestMapping("{companymstid}/transaction")

public class OrderTransactionController {

 

  @PostMapping("{companymstid}/send")

  public void send(@RequestBody OrderTransaction transaction) {

    System.out.println("Sending a transaction.");

    // Post message to the message queue named "OrderTransactionQueue"

  //  jmsTemplate.convertAndSend("OrderTransactionQueue", transaction);

  }

}