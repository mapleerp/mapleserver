package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.CategoryWiseStockMovement;
import com.maple.restserver.report.entity.DailySalesReportDtl;
import com.maple.restserver.report.entity.StockReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.utils.SystemSetting;
@RestController
@Transactional
public class CategoryWiseStockMovementReportResource {

	
	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@GetMapping("{companymstid}/categorywisestockmovement1/{branchcode}")
	public List<CategoryWiseStockMovement> getStockReport(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,
			 @RequestParam("fromdate") String fromdate,
			 @RequestParam("todate") String todate){
		java.sql.Date fdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.sql.Date tdate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");
		return itemBatchDtlService.getCategoryWiseStockMovement(branchcode, companymstid ,fdate,tdate);

	}
	
	
	@GetMapping("{companymstid}/categorywisestockmovementreportresource/categorywisestockmovementreport/{branchcode}/{categoryid}")
	public List<CategoryWiseStockMovement> getCategoryWiseStockMovementReport(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,@PathVariable("categoryid") String categoryid,
			 @RequestParam("fromdate") String fromdate,
			 @RequestParam("todate") String todate){
		java.sql.Date fdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.sql.Date tdate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");
		return itemBatchDtlService.getCategoryWiseStockMovementReport(branchcode, companymstid ,fdate,tdate,categoryid);

	}
	
	// -------------------new version 2.1 surya 

	
	@GetMapping("{companymstid}/categorywisestockmovementreportresource/itemwisestockmovementreport/{branchcode}")
	public List<CategoryWiseStockMovement> getItemWiseStockMovementReport(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,
			 @RequestParam("fromdate") String fromdate,
			 @RequestParam("todate") String todate){
		java.sql.Date fdate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.sql.Date tdate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");
		return itemBatchDtlService.getItemWiseStockMovementReport(branchcode, companymstid ,fdate,tdate);

	}
	
	// -------------------new version 2.1 surya end
	
	//------------------sibi..................14/07/2021....//
	
	@GetMapping("{companymstid}/categorywisestockmovementreportresource/getpharmacystockmovementreportbycategorywise/{branchcode}")
	public List<CategoryWiseStockMovement> getStockReportByCategoryWise(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable("branchcode") String branchcode,
			 @RequestParam("strtDate") String strtDate,@RequestParam("cat") String cat,
			 @RequestParam("strfDate") String strfDate){
		java.sql.Date fdate = SystemSetting.StringToSqlDate(strfDate, "yyyy-MM-dd");
		java.sql.Date tdate = SystemSetting.StringToSqlDate(strtDate, "yyyy-MM-dd");
		
		String categoryNames=cat;
		
		String[]array=categoryNames.split(";");
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return itemBatchDtlService.getStockMovementbyCategoryWise(branchcode,companyMst ,fdate,tdate,array);

	}

}
