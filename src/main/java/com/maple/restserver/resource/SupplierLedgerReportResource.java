package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.SupplierBillwiseDueDayReport;
import com.maple.restserver.report.entity.SupplierMonthlySummary;
import com.maple.restserver.report.entity.supplierLedger;
import com.maple.restserver.service.SupplierLedgerReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class SupplierLedgerReportResource {

	@Autowired
	SupplierLedgerReportService supplierLedgerReportService;
	

	@GetMapping("{companymstid}/getsupplierledgerrepot/{supplierid}/{branchcode}")
	public List<supplierLedger> getSupplierLedgerReport(@PathVariable("supplierid") String supplierid,
			@RequestParam("rdate") String reportdate,
			@PathVariable(value = "companymstid") String  companymstid,@PathVariable(value = "branchcode") String  branchcode) {
		java.sql.Date date = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");


		return supplierLedgerReportService.getSupplierLedgerReport(companymstid,supplierid, reportdate,branchcode);

	}
	
	
	@GetMapping("{companymstid}/getmonthlysupplierledgerrepot/{supplierid}/{branchcode}")
	public List<SupplierMonthlySummary> getMonthlySupplierLedgerReport(@PathVariable("supplierid") String supplierid,
			@RequestParam("fromdate") String fromdate,	@RequestParam("todate") String todate,
			@PathVariable(value = "companymstid") String  companymstid,@PathVariable(value = "branchcode") String  branchcode) {
		java.util.Date fromDate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");

		return supplierLedgerReportService.getMonthlySupplierLedgerReport(companymstid,supplierid, fromDate,toDate,branchcode);

	}
	
	@GetMapping("{companymstid}/supplierledgerrepot/supplierduedayreport/{supplierid}")
	public List<SupplierBillwiseDueDayReport> getSupplierDueDayReport(@PathVariable("supplierid") String supplierid,
			@RequestParam("fromdate") String fromdate,	@RequestParam("todate") String todate,
			@PathVariable(value = "companymstid") String  companymstid) {
		java.util.Date fromDate = SystemSetting.StringToSqlDate(fromdate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToSqlDate(todate, "yyyy-MM-dd");

		return supplierLedgerReportService.getSupplierDueDayReport(companymstid,supplierid, fromDate,toDate);

	}
	
}
