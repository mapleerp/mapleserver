package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBranchHdr;
import com.maple.restserver.entity.ItemMergeMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.RawMaterialReturnHdr;
import com.maple.restserver.report.entity.PoliceReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMergeMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.service.ItemMergeService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class ItemMergeResource {

	private static final Logger logger = LoggerFactory.getLogger(ItemMergeResource.class);
	 
	 EventBus eventBus = EventBusFactory.getEventBus();
	 @Value("${serverorclient}")
		private String serverorclient;
	@Autowired
	ItemMergeService itemMergeService;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	 @Autowired
	 LmsQueueMstRepository lmsQueueMstRepository;
	
	@Autowired
	ItemMergeMstRepository itemMergeMstRepository;
	
	 @Autowired
     SaveAndPublishService saveAndPublishService;
	
	@PostMapping("{companymstid}/itemmergeresource/itemmergemst")
	public ItemMergeMst createItemMergeMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody ItemMergeMst itemMergeMst) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		itemMergeMst.setCompanyMst(companyMst);
		
		itemMergeService.mergeitem(itemMergeMst.getFromItemId(), itemMergeMst.getToItemId(), 
				itemMergeMst.getToItemBarCode(),itemMergeMst.getToItemName());
//		ItemMergeMst saved = itemMergeMstRepository.saveAndFlush(itemMergeMst);
		
		ItemMergeMst saved = saveAndPublishService.saveItemMergeMst(itemMergeMst, itemMergeMst.getBranchCode());
		logger.info("itemMergeMst send to KafkaEvent: {}", saved);
		Map<String, Object> variables = new HashMap<String, Object>();
		
		variables.put("companyid",companyMst);
		
		variables.put("voucherNumber",saved.getId());
		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("id",saved.getId());
		
		variables.put("branchcode", saved.getBranchCode());

		if(serverorclient.equalsIgnoreCase("REST")) {
			variables.put("REST",1);
		}else {
			variables.put("REST",0);
		}
		
		
		//variables.put("voucherDate", purchase.getVoucherDate());
		//variables.put("id",purchase.getId());
		variables.put("inet", 0);
		
		
		
		variables.put("WF", "forwardItemMerge");
		
		String workflow = (String) variables.get("WF");
		String voucherNumber = (String) variables.get("voucherNumber");
		String sourceID = (String) variables.get("id");


		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
		lmsQueueMst.setVoucherDate(sqlVDate);
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType(workflow);
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardItemMerge");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName(workflow + sourceID);
		lmsQueueMst.setJobGroup(workflow);
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(sourceID);
		
		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		
		
		variables.put("lmsqid", lmsQueueMst.getId());
		
		
		eventBus.post(variables);
        return saved;
			

	}
//	@PutMapping("{companymstid}/itemmstmergeresource/{fromItemId}/{toItemId}/{fromItemBarCode}/updateitems")
//	public void ItemMerge(
//			
//		
//			@PathVariable(value="fromItemId") String fromItemId,@PathVariable(value="toItemId") String toItemId,  
//			@PathVariable(value="fromItemBarCode") String fromItemBarCode,@PathVariable(value = "companymstid") String companymstid 	)
//	
//	{
//		System.out.print("inside item merge meathod");
//		
//	itemMergeService.mergeitem(fromItemId,toItemId,fromItemBarCode);
//
//		
//	}
//	
	@GetMapping("{companymstid}/itemmstresource/getallitemmergemst")
	public List<ItemMergeMst> getAllItemMergeMst(@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyMstOpt.get();
		return itemMergeMstRepository.findByCompanyMst(companyMst);

	}
	
	
	//--------------use for itemMerge Report with date or without date-------------
	
	@GetMapping("{companymstid}/itemmstresource/getitemmergereportwithdate")		
	public List<ItemMergeMst>  getItemMergeReport(
			@PathVariable(value = "companymstid") String companymstid,
			@RequestParam("todate") String tDate) {
		
		
		java.util.Date toDate = SystemSetting.StringToUtilDate(tDate, "yyyy-MM-dd");
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		return itemMergeMstRepository.getItemMergeReportwithDate(companyMst,toDate);
	}
	
	
	@GetMapping("{companymstid}/itemmstresource/getitemmergereportwithoutdate")		
	public List<ItemMergeMst>  getItemMergeReportwithoutDate(
			@PathVariable(value = "companymstid") String companymstid) {
		
		
		
		Optional<CompanyMst> companyOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();

		return itemMergeMstRepository.findAll();
	}
	
	
	
}
