package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.DayEndClosureDtl;
import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.DayEndClosureDtlRepository;
import com.maple.restserver.repository.DayEndClosureRepository;
import com.maple.restserver.service.SaveAndPublishService;

@RestController
@Transactional
public class DayEndClosureDtlResource {
	
private static final Logger logger = LoggerFactory.getLogger(DayEndClosureDtlResource.class);
	
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	DayEndClosureDtlRepository dayEndClosureDtlRepo;
	
	@Autowired
	DayEndClosureRepository dayendclosurerepo;
	
	 @Autowired
	 SaveAndPublishService saveAndPublishService;

	
	@PostMapping("{companymstid}/dayendclosurehdr/{dayendclosurehdrId}/dayendclosuredtl")
	public DayEndClosureDtl createDayEndClosureDtl(@PathVariable(value = "dayendclosurehdrId") String dayendclosurehdrId,
			@Valid @RequestBody DayEndClosureDtl dayendclosureDtlRequest) {
 
		return dayendclosurerepo.findById(dayendclosurehdrId).map(dayEndClosureHdr -> {
			
			dayendclosureDtlRequest.setDayEndClosureHdr(dayEndClosureHdr);
//			return dayEndClosureDtlRepo.saveAndFlush(dayendclosureDtlRequest);
//			return saveAndPublishService.saveDayEndClosureDtl(dayendclosureDtlRequest, mybranch);
			return dayEndClosureDtlRepo.save(dayendclosureDtlRequest);
		}).orElseThrow(() -> new ResourceNotFoundException("dayendclosurehdrId" + dayendclosurehdrId + " not found"));

		
	}
	
	

	
	@DeleteMapping("{companymstid}/dayendclosuredtl/{id}")
	public void groupMstDelete(@PathVariable(value = "id") String Id) {
		dayEndClosureDtlRepo.deleteById(Id);

	}
	@GetMapping("{companymstid}/dayendclosurehdr/{hdrid}/dayendclosuredtl")
	public List<DayEndClosureDtl> retrievDayEndClosureDtlId(@PathVariable String hdrid){
		return dayEndClosureDtlRepo.findByDayEndClosureHdrId(hdrid);
	}
	
	@GetMapping("{companymstid}/findtotaldayendclosurehdr/{hdrid}")
	public Double findtotalofdayendclosuredtl(@PathVariable String hdrid){
		Double total =  dayEndClosureDtlRepo.gettotal(hdrid);
		if(null == total)
		{
			total = 0.0;
		}
		return total;
	}
}
