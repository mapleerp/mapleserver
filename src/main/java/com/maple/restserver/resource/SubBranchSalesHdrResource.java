package com.maple.restserver.resource;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SubBranchSalesDtl;
import com.maple.restserver.entity.SubBranchSalesTransHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SubBranchSalesDtlRepository;
import com.maple.restserver.repository.SubBranchSalesHdrRepository;
@RestController
@Transactional
public class SubBranchSalesHdrResource {
	
	@Autowired
	SubBranchSalesHdrRepository spencerSalesHdrRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	
	@PostMapping("{companymstid}/savespencersalestranshdr")
	public SubBranchSalesTransHdr createSpencerSalesTransHdr(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SubBranchSalesTransHdr spencerSalesTransHdr) {
		return companyMstRepository.findById(companymstid).map(companyMst -> {
			spencerSalesTransHdr.setCompanyMst(companyMst);

			return spencerSalesHdrRepository.saveAndFlush(spencerSalesTransHdr);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

}
