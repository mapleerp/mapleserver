package com.maple.restserver.resource;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.maple.util.TaxManager;
import com.maple.restserver.entity.TaxMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.TaxMstRepository;
import com.maple.restserver.service.IGSTCheckService;

@RestController
@Transactional
public class TaxMstResource {

	@Autowired
	TaxMstRepository taxMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;

	TaxManager taxManager;
	
	@Autowired
	IGSTCheckService igstTCheckService;

	@PostMapping("{companymstid}/taxmst")
	public TaxMst createTax(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody TaxMst taxMst) {
		return companyMstRepo.findById(companymstid).map(companyMst -> {
			taxMst.setCompanyMst(companyMst);
			taxMstRepository.UpdateTaxMstByItemId(taxMst.getItemId(), taxMst.getStartDate(), taxMst.getTaxId());

			return taxMstRepository.saveAndFlush(taxMst);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	/*
	 * forwardTax
	 */

	@GetMapping("{companymstid}/alltaxmst")
	public List<TaxMst> retrieveTaxMst(@PathVariable(value = "companymstid") String companymstid) {
		return taxMstRepository.findByCompanyMst(companymstid);
	};

	@GetMapping("{companymstid}/taxmst/taxmstbyitemidandenddate/{itemid}")
	public List<TaxMst> retrieveTaxMst(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid) {
		return taxMstRepository.getTaxByItemId(itemid);
	};

	@GetMapping("{companymstid}/taxmst/taxbyitemidtaxid/{itemid}/{taxid}")
	public TaxMst retrieveTaxMstByItemIdAndTaxIdAndEndDate(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid, @PathVariable(value = "taxid") String taxid) {
		return taxMstRepository.getTaxByItemIdAndTaxId(itemid, taxid);
	};

	@GetMapping("{comapnymstid}/getalltax")
	public List<Object> retrieveAllTaxTypes(@PathVariable(value = "comapnymstid") String comapnymstid) {
		return taxManager.getTaxIds();

	}

	@GetMapping("{comapnymstid}/getalltaxmstbyitemid/{itemid}")
	public List<TaxMst> retrieveAllTaxMstsByItemId(@PathVariable(value = "comapnymstid") String comapnymstid,
			@PathVariable(value = "itemid") String itemid) {
		return taxMstRepository.findByCompanyMstIdAndItemId(comapnymstid, itemid);

	}

	@GetMapping("{comapnymstid}/getalltaxmstbyitemidandtaxid/{itemid}/{taxid}")
	public List<TaxMst> retrieveAllTaxMstsByItemIdAndTaxId(@PathVariable(value = "comapnymstid") String comapnymstid,
			@PathVariable(value = "itemid") String itemid, @PathVariable(value = "taxid") String taxid) {
		return taxMstRepository.findByCompanyMstIdAndItemIdAndTaxId(comapnymstid, itemid, taxid);

	}

	@GetMapping("{comapnymstid}/getalltaxmstbyitemidandtaxrate/{itemid}/{taxrate}")
	public List<TaxMst> retrieveAllTaxMstsByItemIdAndTaxRate(@PathVariable(value = "comapnymstid") String comapnymstid,
			@PathVariable(value = "itemid") String itemid, @PathVariable(value = "taxrate") Double taxrate) {
		return taxMstRepository.findByCompanyMstIdAndItemIdAndTaxRate(comapnymstid, itemid, taxrate);

	}

	@GetMapping("{comapnymstid}/getalltaxmstbyitemidandtaxidandtaxrate/{itemid}/{taxid}/{taxrate}")
	public List<TaxMst> retrieveAllTaxMstsByItemIdAndTaxIdAndTaxRate(
			@PathVariable(value = "comapnymstid") String comapnymstid, @PathVariable(value = "itemid") String itemid,
			@PathVariable(value = "taxid") String taxid, @PathVariable(value = "taxrate") Double taxrate) {
		return taxMstRepository.findByCompanyMstIdAndItemIdAndTaxIdAndTaxRate(comapnymstid, itemid, taxid, taxrate);

	}

	@GetMapping("{comapnymstid}/getalltaxmsts")
	public List<TaxMst> retrieveAllTaxMsts(@PathVariable(value = "comapnymstid") String comapnymstid) {
		return taxMstRepository.findAll();

	}

	@GetMapping("{comapnymstid}/taxmst/taxcalculator/{taxpercent}/{mrp}")
	public BigDecimal retrieveTaxCalculation(@PathVariable(value = "comapnymstid") String comapnymstid,
			@PathVariable(value = "taxpercent") Double taxpercent, @PathVariable(value = "mrp") Double mrp) {
		
		//-------------------new version 1.10 surya 
		
		BigDecimal bigDecimal = taxCalculator(taxpercent,mrp);
		
		return bigDecimal;
		
		//-------------------new version 1.10 surya end


		

	}

	//-------------------new version 1.10 surya 

	public BigDecimal taxCalculator(Double taxpercent, Double mrp) {
		
		String taxId = "ASV";
		HashMap hm = new HashMap();
		TaxManager tm = new TaxManager();
		hm.put("TAXPERCENT", taxpercent);
		hm.put("MRP", mrp);

		return tm.getTax(taxId, hm);
		
	}
	
	//-------------------new version 1.10 surya end


	@GetMapping("{companymstid}/taxmst/{taxrate}/{itemId}/{taxid}")
	public List<TaxMst> retrieveTaxMstByTaxRateAndTaxType(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "taxrate") Double taxRate, @PathVariable(value = "taxid") String taxId,
			@PathVariable(value = "itemId") String itemId, @RequestParam(value = "rdate") String rdate) {
		java.sql.Date modifiedDate = ClientSystemSetting.StringToSqlDate(rdate, "dd-MM-yyyy");
		return taxMstRepository.findByTaxIdandTaxRate(itemId, taxRate, taxId, companymstid, modifiedDate);
	};
	
	
	@GetMapping("{companymstid}/taxmstresource/gettaxtype/{customerstate}/{companystate}/{companycountry}")
	public String retrieveCUstomerTaxType(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "customerstate") String customerstate,
			@PathVariable(value = "companystate") String companystate,
			@PathVariable(value = "companycountry") String companycountry) {
		return igstTCheckService.checkIgst(companystate,customerstate,companycountry);
	};

	
	
}
