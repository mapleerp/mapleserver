
package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AccountHeads;
//import com.maple.mapleclient.restService.RestCaller;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.LocalCustomerMst;
import com.maple.restserver.entity.SaleOrderReceipt;
import com.maple.restserver.entity.SalesOrderDtl;
import com.maple.restserver.entity.SalesOrderReceipts;
import com.maple.restserver.entity.SalesOrderTransHdr;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SummaryReportSalesOrder;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.LocalCustomerRepository;
import com.maple.restserver.repository.SaleOrderReceiptRepository;
import com.maple.restserver.repository.SalesOrderDtlRepository;

import com.maple.restserver.repository.SalesOrderTransHdrRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.service.SalesFromOrderService;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class SalesOrderTransHdrResource {
	

	
	
	@Value("${mybranch}")
	private String mybranch;
	
	private static final Logger logger = LoggerFactory.getLogger(SalesOrderTransHdrResource.class);

	Pageable topFifty =   PageRequest.of(0, 50);
	@Autowired
	private SalesOrderTransHdrRepository salesOrderTransHdrRepo;

	@Autowired
	private SalesOrderDtlRepository salesOrderDtlRepository;

	@Autowired
	private SalesReceiptsRepository salesReceiptsRepo;

	@Autowired
	private CompanyMstRepository companyMstRepository;


	@Autowired
	SaleOrderReceiptRepository saleOrderReceiptRepo;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	private LocalCustomerRepository localCustomerRepository;

	@Autowired
	SalesFromOrderService salesFromOrderService;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	// @Autowired
	// private RuntimeService runtimeService;
	EventBus eventBus = EventBusFactory.getEventBus();

	@GetMapping("{companymstid}/salesordertranshdr")
	public List<SalesOrderTransHdr> retrieveAllSalesOrderTransHdr(
			@PathVariable(value = "companymstid") String companymstid) {
		return salesOrderTransHdrRepo.findByCompanyMst(companymstid);

	}

	@GetMapping("{companymstid}/salesorderbydate")
	public List<SummaryReportSalesOrder> retrieveAllSalesOrders(
			@PathVariable(value = "companymstid") String companymstid) {

		// return salesOrderTransHdrRepo.getdailySalesOrder();

		List<Object> objList = salesOrderTransHdrRepo.getdailySalesOrder(companymstid);
		List<SummaryReportSalesOrder> reportsummary = new ArrayList<SummaryReportSalesOrder>();
		for (int i = 0; i < objList.size(); i++) {
			Object[] objAray = (Object[]) objList.get(0);
			SummaryReportSalesOrder summary = new SummaryReportSalesOrder();
			summary.setItemName((String) objAray[0]);
			summary.setMrp((Double) objAray[1]);
			summary.setOrderAdvice((String) objAray[2]);
			summary.setOrderMessageToPrint((String) objAray[3]);
			summary.setQty((Double) objAray[4]);
			summary.setVoucherDate((Date) objAray[5]);
			summary.setVoucherNumber((String) objAray[6]);
			reportsummary.add(summary);
		}

		return reportsummary;

	}

	@PostMapping("{companymstid}/salesordertranshdr/sales_receipts/delete")
	public SalesTransHdr deleteSalesReceiptsByTransHdr(@Valid @RequestBody SalesTransHdr salesTransHdr) {

		List<SalesReceipts> salesReceiptsList = salesReceiptsRepo.findBySalesTransHdr(salesTransHdr);
		salesReceiptsRepo.deleteAll(salesReceiptsList);
		salesReceiptsRepo.flush();

		return salesTransHdr;

	}

	@GetMapping("{companymstid}/salesordertranshdr/{salesordertranshdrid}")
	public Optional<SalesOrderTransHdr> getSalesOrderTransHdrById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("salesordertranshdrid") String salesordertranshdrid) {
		return salesOrderTransHdrRepo.findByIdAndCompanyMstId(salesordertranshdrid, companymstid);

	}

	@PostMapping("{companymstid}/salesordertranshdr")
	public SalesOrderTransHdr createSalesTransHdr(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SalesOrderTransHdr salesTransHdr) {

		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);

		salesTransHdr.setCompanyMst(companyMstOpt.get());

		//salesTransHdr.setCustomerId(salesTransHdr.getCustomerId());
		//salesTransHdr.setLocalCustomerId(salesTransHdr.getLocalCustomerId());
//	java.util.Date date = SystemSetting.StringToUtilDate(salesTransHdr.getVoucherDate(),"yyyy-MM-dd" );
		
		if(null == salesTransHdr.getLocalCustomerId())
		{
			LocalCustomerMst localCustomerMst = localCustomerRepository.findByLocalcustomerName("LOCALCUSTOMER");
			System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk"+localCustomerMst);
			if (null != localCustomerMst) {
				salesTransHdr.setLocalCustomerId(localCustomerMst);
			}  
		}

		

		//SalesOrderTransHdr saved = salesOrderTransHdrRepo.saveAndFlush(salesTransHdr);
		
		SalesOrderTransHdr saved=saveAndPublishService.saveSalesOrderTransHdr(salesTransHdr, mybranch);
		logger.info("SalesOrderTransHdr send to KafkaEvent: {}", saved);
		return saved;

	}

	@PutMapping("{companymstid}/salesordertranshdr/{salesordertranshdrid}")
	public SalesOrderTransHdr SalesFinalSave(@PathVariable(value = "salesordertranshdrid") String salesordertranshdrid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody SalesOrderTransHdr salesTransHdrRequest) {

		return salesOrderTransHdrRepo.findById(salesordertranshdrid).map(salesOrdertranshdr -> {
			salesOrdertranshdr.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());

//					Calendar calendar = Calendar.getInstance();
//					java.util.Date currentDate = calendar.getTime();
//					java.sql.Date date = new java.sql.Date(currentDate.getTime());

			salesOrdertranshdr.setVoucherDate(salesTransHdrRequest.getVoucherDate());
//					salesTransHdrRequest.setVoucherDate(date);
			salesOrdertranshdr.setInvoiceAmount(salesTransHdrRequest.getInvoiceAmount());
			Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
			salesOrdertranshdr.setCompanyMst(companyMst.get());

			salesOrdertranshdr.setOrderStatus(salesTransHdrRequest.getOrderStatus());

			salesOrdertranshdr.setOrderAdvice(salesTransHdrRequest.getOrderAdvice());
			salesOrdertranshdr.setOrderDue(salesTransHdrRequest.getOrderDue());
			salesOrdertranshdr.setOrderMessageToPrint(salesTransHdrRequest.getOrderMessageToPrint());
			salesOrdertranshdr
					.setSaleOrderReceiptVoucherNumber(salesTransHdrRequest.getSaleOrderReceiptVoucherNumber());
			salesOrdertranshdr.setPaymentMode(salesTransHdrRequest.getPaymentMode());

			salesOrdertranshdr.setPaidAmount(salesTransHdrRequest.getPaidAmount());
			salesOrdertranshdr.setAccountHeads(salesTransHdrRequest.getAccountHeads());

			// Optional<CustomerMst>
			// customerMst=customerMstRepository.findById(salesTransHdrRequest.getCustomerId().getId());
			// salesOrdertranshdr.setCustomerId(customerId);
			salesOrdertranshdr.setLocalCustomerId(salesTransHdrRequest.getLocalCustomerId());

			// --------------------version 6.1 surya
			salesOrdertranshdr.setOrderDue(salesTransHdrRequest.getOrderDue());
			salesOrdertranshdr.setOrderDueTime(salesTransHdrRequest.getOrderDueTime());
			salesOrdertranshdr.setOrderTimeMode(salesTransHdrRequest.getOrderTimeMode());
			// --------------------version 6.1 surya end

			salesOrdertranshdr.setCancelationReason(salesTransHdrRequest.getCancelationReason());
			if (null != salesTransHdrRequest.getDeliveryBoyId()) {
				salesOrdertranshdr.setDeliveryBoyId(salesTransHdrRequest.getDeliveryBoyId());

			}

			//SalesOrderTransHdr saved = salesOrderTransHdrRepo.saveAndFlush(salesOrdertranshdr);
			SalesOrderTransHdr saved =saveAndPublishService.saveSalesOrderTransHdr(salesOrdertranshdr, mybranch);
			logger.info("SalesOrderTransHdr send to KafkaEvent: {}", saved);		
			/*
			 * mange Sales receipts
			 */

			Double invoiceAmount = salesOrdertranshdr.getInvoiceAmount();
			Double cashPaidAmount = salesOrdertranshdr.getCashPay();
			Double cardAmount = salesOrdertranshdr.getCardamount();

//						
//						if(null!=cashPaidAmount && cashPaidAmount >0) {
//							SalesOrderReceipts salesOrderReceipts = new SalesOrderReceipts();
//							salesOrderReceipts.setReceiptAmount(cashPaidAmount);
//							
//							 
//							salesOrderReceipts.setReceiptMode("CASH");
//							salesOrderReceipts.setSalesOrderTransHdr(saved);
//							salesOrderReceipts.setCompanyMst(salesOrdertranshdr.getCompanyMst());
//							salesOrderReceiptsRepo.saveAndFlush(salesOrderReceipts);
//						}
//			

			Map<String, Object> variables = new HashMap<String, Object>();
			variables.put("voucherNumber", saved.getVoucherNumber());
			variables.put("voucherDate", saved.getVoucherDate());
			variables.put("id", saved.getId());
			variables.put("companyid", saved.getCompanyMst());

			variables.put("branchcode", saved.getBranchCode());

			variables.put("WF", "forwardSaleOrder");

			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");

			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));

			// lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));

			java.util.Date uDate = (Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);

			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardSaleOrder");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);

			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);

			/*
			 * ProcessInstanceWithVariables pVariablesInReturn =
			 * runtimeService.createProcessInstanceByKey("forwardOrderSales")
			 * .setVariables(variables)
			 * 
			 * .executeWithVariablesInReturn();
			 */

			return saved;
		}).orElseThrow(() -> new ResourceNotFoundException("salestranshdrid " + salesordertranshdrid + " not found"));

	}

	@GetMapping("{companymstid}/dailyordersummar/dailyordersummary")
	public List<Object> getDailyOrderSummary(@RequestParam("reportdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		return salesOrderTransHdrRepo.DailyCashFromOrder(date);
	}

	@GetMapping("{companymstid}/getsaleorder")
	public List<SalesOrderTransHdr> getSaleOrder() {
		// java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd"
		// );
		return salesOrderTransHdrRepo.getsSaleOrder();
	}

	@GetMapping("{companymstid}/getsaleorderbyid/{id}")
	public @ResponseBody Optional<SalesOrderTransHdr> getSaleOrderbyId(@PathVariable(value = "id") String id) {
		return salesOrderTransHdrRepo.findById(id);
	}

	@GetMapping("{companymstid}/getsaleorderbyvoucherno/{voucherNumber}/{branchcode}")
	public @ResponseBody Optional<SalesOrderTransHdr> getAllSalesTransHdrByVoucherNo(
			@PathVariable(value = "voucherNumber") String voucherNumber) {
		return salesOrderTransHdrRepo.findByVoucherNumber(voucherNumber);
	}

	@GetMapping("{companymstid}/saleordertranshdr/getsaleorderbyduedate/{branchcode}")
	public @ResponseBody List<SalesOrderTransHdr> getPendingSaleOrderByDate(@RequestParam(value = "fdate") String fdate,
			@RequestParam(value = "tdate") String tdate, @PathVariable(value = "branchcode") String branchCode) {
		Date rufDate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		Date rutDate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");

		return salesOrderTransHdrRepo.getPendingSaleOrder(rufDate, rutDate, branchCode);
	}

	@GetMapping("{companymstid}/salesordertranshdr/web/{phoneno}")
	public @ResponseBody SalesOrderTransHdr saveSalesOrderTransHdrByPhone(
			@PathVariable(value = "phoneno") String phoneno, @RequestParam("orderdate") String orderdate) {

		// Find Customer Mst using mobile

//		CustomerMst customerMst = customerMstRepository.findByCustomerContact(phoneno);
		AccountHeads accountHeads = accountHeadsRepository.findByCustomerContact(phoneno);

		LocalCustomerMst localCustomerMst = localCustomerRepository
				.findByLocalcustomerName(accountHeads.getAccountName());

		if (null == localCustomerMst || null == localCustomerMst.getId()) {
			localCustomerMst = new LocalCustomerMst();
			localCustomerMst.setAddress(accountHeads.getPartyAddress1());
			localCustomerMst.setLocalcustomerName(accountHeads.getAccountName());
			localCustomerMst.setPhoneNo(accountHeads.getCustomerContact());
			localCustomerMst.setPhoneNO2(accountHeads.getCustomerState());
			localCustomerMst = localCustomerRepository.save(localCustomerMst);

		}

		CompanyMst companyMst = accountHeads.getCompanyMst();

		SalesOrderTransHdr salesOrderTransHdr = new SalesOrderTransHdr();

		salesOrderTransHdr.setCompanyMst(companyMst);

		salesOrderTransHdr.setAccountHeads(accountHeads);
		salesOrderTransHdr.setLocalCustomerId(localCustomerMst);

		java.util.Date date = SystemSetting.StringToUtilDate(orderdate, "yyyy-MM-dd");
		salesOrderTransHdr.setVoucherDate(date);

	//	SalesOrderTransHdr saved = salesOrderTransHdrRepo.saveAndFlush(salesOrderTransHdr);
		SalesOrderTransHdr saved =saveAndPublishService.saveSalesOrderTransHdr(salesOrderTransHdr, mybranch);
		logger.info("SalesOrderTransHdr send to KafkaEvent: {}", saved);
		return saved;
	}

	@GetMapping("{companymstid}/salesordertranshdr/web/{salesordertranshdrid}/{vouchernumber}")
	public SalesOrderTransHdr SalesOrderFinalSaveFromWeb(
			@PathVariable(value = "salesordertranshdrid") String salesordertranshdrid,
			@PathVariable(value = "vouchernumber") String vouchernumber) {

		return salesOrderTransHdrRepo.findById(salesordertranshdrid).map(salesOrdertranshdr -> {
			salesOrdertranshdr.setVoucherNumber(vouchernumber);

			salesOrdertranshdr.setOrderStatus("Orderd");

			SalesOrderTransHdr saved = salesOrderTransHdrRepo.saveAndFlush(salesOrdertranshdr);

			return saved;
		}).orElseThrow(
				() -> new ResourceNotFoundException("salesordertranshdrid " + salesordertranshdrid + " not found"));

	}

	@GetMapping("{companymstid}/searchsaleorderbyvoucherno")
	public List<Object> SearchForSaleOrder(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid) {
		Optional<CompanyMst> companymst = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		return salesOrderTransHdrRepo.SearchSaleOrder("%" + searchstring.toLowerCase() + "%", topFifty, companyMst);
	}

	@GetMapping("{companymstid}/searchsaleorderbyvouchernocname")
	public List<Object> SearchForSaleOrderReturnCname(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid) {
		Optional<CompanyMst> companymst = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		return salesOrderTransHdrRepo.SearchSaleOrderRetunLcustomerName("%" + searchstring.toLowerCase() + "%",
				topFifty, companyMst);
	}

	@GetMapping("{companymstid}/salesordertranshdr/savesaleorderweb")
	public @ResponseBody SalesOrderTransHdr saveWebSalesOrderTransHdr(
			@RequestParam("localcustomerid") String localcustomerid, @RequestParam("customerid") String customerid,
			@RequestParam("deliveryboyid") String deliveryboyid, @RequestParam("ordertakerid") String ordertakerid,
			@RequestParam("branchcode") String branchcode, @RequestParam("orderduetime") String orderDueTime,
			@RequestParam("amorpm") String amorpm, @RequestParam("orderby") String orderBy,

			@RequestParam("orderdate") String orderdate) {

		// Find Customer Mst using mobile

		Optional<AccountHeads> accountHeadsResp = accountHeadsRepository.findById(customerid);
		AccountHeads accountHeads = accountHeadsResp.get();

		Optional<LocalCustomerMst> localCustomerMstResp = localCustomerRepository.findById(localcustomerid);
		LocalCustomerMst localCustomerMst = localCustomerMstResp.get();

		CompanyMst companyMst = accountHeads.getCompanyMst();

		SalesOrderTransHdr salesOrderTransHdr = new SalesOrderTransHdr();
		salesOrderTransHdr.setDeliveryBoyId(deliveryboyid);
		salesOrderTransHdr.setSalesManId(ordertakerid);
		salesOrderTransHdr.setBranchCode(branchcode);
		salesOrderTransHdr.setCompanyMst(companyMst);
		salesOrderTransHdr.setOrderDueTime(orderDueTime);
		salesOrderTransHdr.setOrderTimeMode(amorpm);
		salesOrderTransHdr.setOrdedBy(orderBy);

		salesOrderTransHdr.setAccountHeads(accountHeads);
		salesOrderTransHdr.setLocalCustomerId(localCustomerMst);

		java.util.Date date = SystemSetting.StringToUtilDate(orderdate, "yyyy-MM-dd");
		salesOrderTransHdr.setVoucherDate(date);

		//SalesOrderTransHdr saved = salesOrderTransHdrRepo.saveAndFlush(salesOrderTransHdr);

		SalesOrderTransHdr saved =saveAndPublishService.saveSalesOrderTransHdr(salesOrderTransHdr, mybranch);
		logger.info("SalesOrderTransHdr send to KafkaEvent: {}", saved);
		return saved;
	}

	@GetMapping("{companymstid}/salesordertranshdr/saleorderfinalsaveweb/{salesordertranshdrid}/{vouchernumber}")
	public SalesOrderTransHdr SalesOrderFinalSaveWeb(
			@PathVariable(value = "salesordertranshdrid") String salesordertranshdrid,
			@PathVariable(value = "vouchernumber") String vouchernumber) {

		return salesOrderTransHdrRepo.findById(salesordertranshdrid).map(salesOrdertranshdr -> {
			salesOrdertranshdr.setVoucherNumber(vouchernumber);

			salesOrdertranshdr.setOrderStatus("Orderd");

		//	SalesOrderTransHdr saved = salesOrderTransHdrRepo.saveAndFlush(salesOrdertranshdr);
			SalesOrderTransHdr saved =saveAndPublishService.saveSalesOrderTransHdr(salesOrdertranshdr, mybranch);
			logger.info("SalesOrderTransHdr send to KafkaEvent: {}", saved);

//			Map<String, Object> variables = new HashMap<String, Object>();
//			variables.put("voucherNumber", saved.getVoucherNumber());
//			variables.put("voucherDate", saved.getVoucherDate());
//			variables.put("id", saved.getId());
//			variables.put("companyid", saved.getCompanyMst());
//
//			ProcessInstanceWithVariables pVariablesInReturn = runtimeService
//					.createProcessInstanceByKey("processWebSalesOrder").setVariables(variables)
//
//					.executeWithVariablesInReturn();

			return saved;
		}).orElseThrow(
				() -> new ResourceNotFoundException("salesordertranshdrid " + salesordertranshdrid + " not found"));

	}

	@GetMapping("{companymstid}/salesordertranshdr/holdedsalesordertranshdr/{branchcode}")
	public List<SalesOrderTransHdr> getHoldedSalesOrderTransHdr(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode) {
		Optional<CompanyMst> companymst = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		return salesOrderTransHdrRepo.getHoldedSalesOrderTransHdr(companyMst);
	}

	// -------------new version 2.0 surya

	@GetMapping("{companymstid}/salesordertranshdr/canceledsalesordertranshdr/{branchcode}")
	public List<SalesOrderTransHdr> getCanceledSalesOrderTransHdr(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode, @RequestParam("fdate") String fdate,
			@RequestParam("tdate") String tdate) {

		Optional<CompanyMst> companymst = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companymst.get();

		Date sdate = SystemSetting.StringToUtilDate(fdate, "yyyy-MM-dd");
		Date edate = SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");

		return salesOrderTransHdrRepo.getCanceledSalesOrderTransHdr(companyMst, branchcode, sdate, edate);
	}

	// -------------new version 2.0 surya end

	// -------------new version 2.3 surya end

	@DeleteMapping("{companymstid}/salesorderhdr/deletesalesorderhdr/{saleorderhdrid}")
	public void salesDtlDelete(@PathVariable String saleorderhdrid) {
		salesOrderTransHdrRepo.deleteById(saleorderhdrid);

	}
	// -------------new version 2.3 surya end

	@GetMapping("{companymstid}/salesorderhdr/getnullvoucherbbycustomerid/{customerid}/{customsalesmode}")
	public SalesOrderTransHdr getSalesOrderTransHdrWithVoucherNullAndCustomerId(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "customerid") String customerid,
			@PathVariable(value = "customsalesmode") String customsalesmode,
			@RequestParam(value = "sdate") String sdate)

	{
		Date udate = SystemSetting.StringToUtilDate(sdate, "yyyy-MM-dd");
		CompanyMst companyMst = companyMstRepository.findById(companymstid).get();
		return salesOrderTransHdrRepo.getSalesOrderTransHdrBycustomerAndVoucherNull(customerid, udate, companyMst,
				customsalesmode);

	}

	@PostMapping("{companymstid}/salesordertranshdr/salesordertosales/{salesordertanshdrid}/{userid}")
	public String conversalesordertosales(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "salesordertanshdrid") String salesordertanshdrid,
			@PathVariable(value = "userid") String userid) {

		Optional<SalesOrderTransHdr> salesOrdertranshdrOpt = salesOrderTransHdrRepo.findById(salesordertanshdrid);

		if (!salesOrdertranshdrOpt.isPresent()) {
			return null;
		}
		try {
			salesFromOrderService.convertSaleOrderToSales(salesOrdertranshdrOpt.get(), userid);
		} catch (PartialAccountingException e) {

			logger.error(e.getMessage());
			return "FAILED " + e.toString();
		}

		return "OK";

	}
	
	
	@GetMapping("{companymstid}/salesorderteanshdrresource/salesordertranshdrbyvoucheranddate/{vouchernumber}/{branchcode}")
	public SalesOrderTransHdr getHoldedSalesOrderTransHdr(@PathVariable("companymstid") String companymstid,
			@PathVariable("branchcode") String branchcode,
			@PathVariable("vouchernumber") String vouchernumber,
			@RequestParam("rdate") String reportdate) {
		Optional<CompanyMst> companymst = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		return salesOrderTransHdrRepo.findByVoucherNumberAndVoucherDateAndCompanyMstId(vouchernumber, date, companymstid);
		
	}
	
	
	@GetMapping("{companymstid}/salesordertranshdrresource/sales_receipts/{id}")
	public List<SalesOrderDtl> getSalesReceiptsByTransHdr(
			@PathVariable(value = "id") String salesOrderTransHdrId) {

		SalesOrderTransHdr salesOrderTransHdr = salesOrderTransHdrRepo.findById(salesOrderTransHdrId).get();

		return salesOrderDtlRepository.findBySalesOrderTransHdr(salesOrderTransHdr);
	}

	

}
