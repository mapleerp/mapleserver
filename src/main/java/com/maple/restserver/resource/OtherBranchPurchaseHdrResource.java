package com.maple.restserver.resource;

import java.net.URI;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AccountPayable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.LmsQueueTallyMst;
import com.maple.restserver.entity.OtherBranchPurchaseHdr;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.OtherBranchPurchaseHdrRepository;
import com.maple.restserver.service.SaveAndPublishService;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Component
public class OtherBranchPurchaseHdrResource {
	private static final Logger logger = LoggerFactory.getLogger(OtherBranchPurchaseHdrResource.class);
	
	
	@Value("${mybranch}")
	private String mybranch;
	EventBus eventBus = EventBusFactory.getEventBus();
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	OtherBranchPurchaseHdrRepository otherBranchPurchaseHdrRepo;
	
	@Autowired
	SaveAndPublishService saveAndPublishService;
	
	@GetMapping("{companymstid}/otherbranchpurchasehdr/getallotherbranchpurchasehdr")
	public List<OtherBranchPurchaseHdr> retrieveAllotherPurchaseHdr(@PathVariable(value = "companymstid") String companymstid) {

		return otherBranchPurchaseHdrRepo.findByCompanyMstId(companymstid);

	}

	@PostMapping("{companymstid}/otherbranchpurchasehdr/saveotherbranchpurchasehdr")
	public OtherBranchPurchaseHdr createOtherBranchPurchaseHdr(@Valid @RequestBody OtherBranchPurchaseHdr
			otherBranchPurchaseHdr,
			@PathVariable(value = "companymstid") String companymstid) {

		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		otherBranchPurchaseHdr.setCompanyMst(companyMst);
	//	OtherBranchPurchaseHdr saved = otherBranchPurchaseHdrRepo.save(otherBranchPurchaseHdr);
		OtherBranchPurchaseHdr saved =saveAndPublishService.saveOtherBranchPurchaseHdr(otherBranchPurchaseHdr, mybranch, otherBranchPurchaseHdr.getBranchCode());
		logger.info("OtherBranchPurchaseHdr send to KafkaEvent: {}", saved);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/(id)").buildAndExpand(saved.getId())
				.toUri();
		return saved;
	}
	@PutMapping("{companymstid}/otherbranchpurchasehdr/finalsave/{otherbranchpurchaseid}")
	public OtherBranchPurchaseHdr finalSave(@PathVariable String otherbranchpurchaseid,
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody OtherBranchPurchaseHdr otherBranchPurchaseHdrRequest) {

		return otherBranchPurchaseHdrRepo.findById(otherbranchpurchaseid).map(otherbranchpurchase -> {

			Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
			CompanyMst companyMst = comapnyMstOpt.get();

			otherbranchpurchase.setCompanyMst(companyMst);

			otherbranchpurchase.setSupplierId(otherBranchPurchaseHdrRequest.getSupplierId());
			otherbranchpurchase.setPurchaseType(otherBranchPurchaseHdrRequest.getPurchaseType());
			otherbranchpurchase.setCurrency(otherBranchPurchaseHdrRequest.getCurrency());

			otherbranchpurchase.setEnableBatchStatus(otherBranchPurchaseHdrRequest.getEnableBatchStatus());
			otherbranchpurchase.setInvoiceTotal(otherBranchPurchaseHdrRequest.getInvoiceTotal());
			if(null != otherBranchPurchaseHdrRequest.getFcInvoiceTotal())
			{
				otherbranchpurchase.setFcInvoiceTotal(otherBranchPurchaseHdrRequest.getFcInvoiceTotal());
			}
			otherbranchpurchase.setVoucherNumber(otherBranchPurchaseHdrRequest.getVoucherNumber());

			otherbranchpurchase.setVoucherDate(otherBranchPurchaseHdrRequest.getVoucherDate());

//			     	purchase.setVoucherDate(purchasehdrRequest.getVoucherDate());
			otherbranchpurchase.setFinalSavedStatus(otherBranchPurchaseHdrRequest.getFinalSavedStatus());


			/*
			 * Iniitate process
			 */
			//otherbranchpurchase = otherBranchPurchaseHdrRepo.saveAndFlush(otherbranchpurchase);
			
			otherbranchpurchase=saveAndPublishService.saveOtherBranchPurchaseHdr(otherbranchpurchase, mybranch, otherbranchpurchase.getBranchCode());
			logger.info("OtherBranchPurchaseHdr send to KafkaEvent: {}", otherbranchpurchase);

			Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("companyid", companyMst);

			variables.put("voucherNumber", otherbranchpurchase.getVoucherNumber());
			variables.put("voucherDate", otherbranchpurchase.getVoucherDate());
			variables.put("id", otherbranchpurchase.getId());
			variables.put("inet", 0);

			variables.put("WF", "forwardOtherBranchPurchase");

			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");

			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			
			//lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
			
			java.util.Date uDate = (java.util.Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);
			
			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardOtherBranchPurchase");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);

			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
			variables.put("lmsqid", lmsQueueMst.getId());
			eventBus.post(variables);

//			LmsQueueTallyMst lmsQueueTallyMst = new LmsQueueTallyMst();
//			lmsQueueTallyMst.setCompanyMst((CompanyMst) variables.get("companyid"));
//			lmsQueueTallyMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));
//			lmsQueueTallyMst.setVoucherNumber((String) variables.get("voucherNumber"));
//			lmsQueueTallyMst.setVoucherType(workflow);
//			lmsQueueTallyMst.setPostedToTally("NO");
//			lmsQueueTallyMst.setJobClass("forwardPurchase");
//			lmsQueueTallyMst.setCronJob(true);
//			lmsQueueTallyMst.setJobName(workflow + sourceID);
//			lmsQueueTallyMst.setJobGroup(workflow);
//			lmsQueueTallyMst.setRepeatTime(60000L);
//			lmsQueueTallyMst.setSourceObjectId(sourceID);
//			lmsQueueTallyMst.setBranchCode((String) variables.get("branchcode"));
//			lmsQueueTallyMst = lmsQueueTallyMstRepository.save(lmsQueueTallyMst);
//			variables.put("lmsqid", lmsQueueTallyMst.getId());
//			eventBus.post(variables);

			/*
			 * ProcessInstanceWithVariables pVariablesInReturn =
			 * runtimeService.createProcessInstanceByKey("forwardPurchase")
			 * .setVariables(variables)
			 * 
			 * .executeWithVariablesInReturn();
			 */

			return otherbranchpurchase;
		}).orElseThrow(() -> new ResourceNotFoundException("purchaseid " + otherbranchpurchaseid + " not found"));

	}
	
	@GetMapping("{companymstid}/otherbranchpurchasehdr/{id}/getotherbranchbyhdrid")
	public Optional<OtherBranchPurchaseHdr> getOtherBranchPurchaseHdrbyId(@PathVariable(value = "id") String id) {
		return otherBranchPurchaseHdrRepo.findById(id);

	}
	@GetMapping("{companymstid}/otherbranchpurchasehdr/fetchbystatus")
	public List<OtherBranchPurchaseHdr> retrieveAllOtherBranchPurchaseHdrByStatus() {

		return otherBranchPurchaseHdrRepo.fetchPurchaseHdr();

	}

	@GetMapping("{companymstid}/otherbranchpurchasehdr/{vouchernumber}/purchasehdrbyvoucher")
	public List<OtherBranchPurchaseHdr> getOtherBranchPurchaseHdrByVoucherNumber(@PathVariable(value = "vouchernumber") String vouchernumber,
			@RequestParam("date") String date) {
		java.sql.Date vdate = SystemSetting.StringToSqlDate(date, "yyyy-MM-dd");
		return otherBranchPurchaseHdrRepo.findByVoucherNumberAndVoucherDate(vouchernumber, vdate);

	}

	
	@GetMapping("{companymstid}/otherbranchpurchasehdr/{vouchernumber}/otherbranchpurchasehdrbyvoucher/{branchcode}")
	public OtherBranchPurchaseHdr getOtherBranchPurchaseHdrByVoucherNumberAndBranch(
			@PathVariable(value = "vouchernumber") String vouchernumber,
			@PathVariable(value = "branchcode") String branchcode
			) {
		return otherBranchPurchaseHdrRepo.findByVoucherNumberAndBranchCode(vouchernumber, branchcode);

	}

	
	@GetMapping("{companymstid}/otherbranchpurchasehdr/{supplierid}/{branchcode}/otherbranchpurchasehdrbysupplier")
	public List<OtherBranchPurchaseHdr> getOtherBranchPurchaseHdr(@PathVariable(value = "supplierid") String suppplierid,
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "branchCode") String branchCode, @RequestParam("fromdate") String fromdate,
			@RequestParam("tomdate") String todate) {
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();

		return otherBranchPurchaseHdrRepo.findBySupplier(suppplierid, fromDate, toDate, branchCode, companyMst);

	}

	@GetMapping("{companymstid}/{branchcode}/otherbranchpurchasehdr/getsupplierandvouchernumber")
	public List<Object> getOtherBranchSupplierAndVouchernumber(@PathVariable("companymstid") String companymstid,

			@PathVariable("branchcode") String branchcode,

			@RequestParam("vDate") String vDate) {

		Optional<CompanyMst> companyMst = companyMstRepository.findById(companymstid);
		return otherBranchPurchaseHdrRepo.getSupplierAndVouchernumber(branchcode, companyMst.get(), vDate);

	}

	
	

	@DeleteMapping("{companymstid}/otherbranchpurchasehdr/{id}/otherbranchpurchasehdrdelete")
	public void DeleteReceiptDtl(@PathVariable(value = "id") String id) {
		otherBranchPurchaseHdrRepo.deleteById(id);

	}
	
	//-------------------------------new version 1.13 surya 
	
	@GetMapping("{companymstid}/otherbranchpurchasehdr/{branchcode}/otherbranchpurchasesummary")
	public List<OtherBranchPurchaseHdr> getOtherBranchPurchaseSummary(
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "branchcode") String branchCode  ,
				@RequestParam("fromdate") String fromdate,
				@RequestParam("todate") String todate) {
		
		java.util.Date fromDate = SystemSetting.StringToUtilDate(fromdate,"yyyy-MM-dd");
		java.util.Date toDate = SystemSetting.StringToUtilDate(todate,"yyyy-MM-dd");
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepository.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		
		return otherBranchPurchaseHdrRepo.getOtherBranchPurchaseSummary(fromDate,toDate,branchCode,companyMst);

	}
	
	@GetMapping("{companymstid}/otherbranchpurchasehdr/{id}/otherbranchpurchasehdrbyid")
	public Optional<OtherBranchPurchaseHdr> getOtherBranchPurchaseHdrById(
				@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "id") String id) {
		
		
		return otherBranchPurchaseHdrRepo.findById(id);
	}
	
	//-------------------------------new version 1.13 surya end
	


}
