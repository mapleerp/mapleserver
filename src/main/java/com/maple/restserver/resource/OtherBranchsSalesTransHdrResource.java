package com.maple.restserver.resource;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SocketClient;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AccountReceivable;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.LmsQueueTallyMst;
import com.maple.restserver.entity.OtherBranchPurchaseHdr;
import com.maple.restserver.entity.OtherBranchSalesTransHdr;
import com.maple.restserver.entity.SalesReceipts;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.StockTransferInHdr;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.report.entity.SalesInvoiceReport;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.report.entity.TaxSummaryMst;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.OtherBranchSalesTransHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.service.OtherBranchSalesService;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class OtherBranchsSalesTransHdrResource {
	@Autowired
	private OtherBranchSalesTransHdrRepository otherBranchSalesTransHdrRepository;
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	@Value("${serverorclient}")
	private String serverorclient;
	@Autowired
	OtherBranchSalesService otherBranchSalesService;
	EventBus eventBus = EventBusFactory.getEventBus();

	@Autowired
	private ExternalApi externalApi;

	@Autowired
	CompanyMstRepository companyMstRepo;

	@PostMapping("{companymstid}/saveotherbranchsalestranshdr")
	public OtherBranchSalesTransHdr createOtherBranchSalesTransHdr(
			@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody OtherBranchSalesTransHdr otherBranchSalesTransHdr) {

		return companyMstRepo.findById(companymstid).map(companyMst -> {
			otherBranchSalesTransHdr.setCompanyMst(companyMst);

			return otherBranchSalesTransHdrRepository.saveAndFlush(otherBranchSalesTransHdr);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " + companymstid + " not found"));
	}

	@PutMapping("{companymstid}/otherbranchsalestranshdrfinalsave/{hdrid}")
	public OtherBranchSalesTransHdr OtherBranchSalesTransHdrFinalSave(

			@PathVariable("hdrid") String hdrid, @PathVariable("companymstid") String companymstid,
			@Valid @RequestBody OtherBranchSalesTransHdr salesTransHdrRequest) {

		return otherBranchSalesTransHdrRepository.findById(hdrid).map(salestranshdr -> {

			salestranshdr.setInvoiceAmount(salesTransHdrRequest.getInvoiceAmount());
			salestranshdr.setCashPay(salesTransHdrRequest.getCashPay());
			salestranshdr.setCardamount(salesTransHdrRequest.getCardamount());
			salestranshdr.setPaidAmount(salesTransHdrRequest.getPaidAmount());
			salestranshdr.setVoucherDate(salesTransHdrRequest.getVoucherDate());
			salestranshdr.setVoucherNumber(salesTransHdrRequest.getVoucherNumber());

			salestranshdr = otherBranchSalesTransHdrRepository.saveAndFlush(salestranshdr);
			Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("companyid", salestranshdr.getCompanyMst());

			variables.put("voucherNumber", salestranshdr.getVoucherNumber());
			variables.put("voucherDate", salestranshdr.getVoucherDate());
			variables.put("id", salestranshdr.getId());

			variables.put("branchcode", salestranshdr.getBranchCode());

			if (serverorclient.equalsIgnoreCase("REST")) {
				variables.put("REST", 1);
			} else {
				variables.put("REST", 0);
			}

			// variables.put("voucherDate", purchase.getVoucherDate());
			// variables.put("id",purchase.getId());
			variables.put("inet", 0);

			variables.put("WF", "forwardOtherBranchSales");

			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");

			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
			// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));

			java.util.Date uDate = (Date) variables.get("voucherDate");
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);

			lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
			lmsQueueMst.setVoucherType(workflow);
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("forwardOtherBranchSales");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName(workflow + sourceID);
			lmsQueueMst.setJobGroup(workflow);
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(sourceID);

			lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);

			variables.put("lmsqid", lmsQueueMst.getId());

			eventBus.post(variables);
			return salestranshdr;
		}).orElseThrow(() -> new ResourceNotFoundException("salestranshdrid " + hdrid + " not found"));

	}

	@GetMapping("{companymstid}/otherbranchsalesinvoice/{voucherNumber}")
	public List<SalesInvoiceReport> getOtherSalesInvoice(@PathVariable("voucherNumber") String voucherNumber,
			@RequestParam("rdate") String reportdate, @PathVariable(value = "companymstid") String companymstid) {
		java.sql.Date date = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");

		return otherBranchSalesService.getSalesInvoice(companymstid, voucherNumber, date);

	}

	@GetMapping("{companymstid}/otherbranchsalesinvoicetax/{vouchernumber}")
	public List<TaxSummaryMst> getOtherBranchSalesInvoiceTax(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("vouchernumber") String vouchernumber, @RequestParam("rdate") String rdate) {
		java.sql.Date date = SystemSetting.StringToSqlDate(rdate, "yyyy-MM-dd");

		return otherBranchSalesService.getSalesInvoiceTax(companymstid, vouchernumber, date);

	}

	@GetMapping("{companymstid}/otherbranchsumoftaxes/{vouchernumber}")
	public List<TaxSummaryMst> getOtherBranchSumOfTaxAmounts(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("vouchernumber") String vouchernumber, @RequestParam("rdate") String rdate) {
		java.sql.Date date = SystemSetting.StringToSqlDate(rdate, "yyyy-MM-dd");

		return otherBranchSalesService.getSumOfTaxAmounts(companymstid, vouchernumber, date);

	}

	@GetMapping("{companymstid}/otherbranchsalesinvoicereport/gettaxinvoicecustomer/{vouchernumber}")
	public List<SalesInvoiceReport> getOtherBranchTaxinvoiceCustomer(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("vouchernumber") String vouchernumber, @RequestParam("rdate") String rdate) {
		java.sql.Date date = SystemSetting.StringToSqlDate(rdate, "yyyy-MM-dd");

		return otherBranchSalesService.getTaxinvoiceCustomer(companymstid, vouchernumber, date);

	}

	@GetMapping("{companymstid}/otherbranchsalesinvoice/cessamount/{vouchernumber}")
	public Double getCessAmount(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("vouchernumber") String vouchernumber, @RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return otherBranchSalesService.getCessAmount(vouchernumber, date);

	}

	@GetMapping("{companymstid}/otherbranchsalesinvoice/nontaxableamount/{vouchernumber}")
	public Double getOtherBranchSalesNonTaxableAmount(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("vouchernumber") String vouchernumber, @RequestParam("rdate") String reportdate) {
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");

		return otherBranchSalesService.getNonTaxableAmount(vouchernumber, date);

	}

	@GetMapping("{companymstid}/otherbranchsalestranshdrbyvoucheranddate/{vouchernumber}")
	public OtherBranchSalesTransHdr getSalesTransHdrByVoucherNumberAndDate(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("vouchernumber") String vouchernumber, @RequestParam("rdate") String reportdate)

	{
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		return otherBranchSalesTransHdrRepository.findByVoucherNumberAndVoucherDateAndCompanyMstId(vouchernumber, date,
				companymstid);

	}

	@GetMapping("{companymstid}/otherbranchsalestranshdr/getstocktransfervoucher/{vouchernumber}")
	public List<StockTransferOutReport> getStockTransferReportOtherBranch(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("vouchernumber") String vouchernumber, @RequestParam("rdate") String reportdate)

	{
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "yyyy-MM-dd");
		return otherBranchSalesService.getOtherBranchStockTransfer(vouchernumber, date);

	}

	@GetMapping("{companymstid}/otherbranchsalestranshdr/resendotherbranchsales/{hdrid}")
	public String resendFailedOtherBranch(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "hdrid") String hdrid) {
		otherBranchSalesService.resendOtherBranchSales(hdrid);
		return "success";
	}

	@GetMapping("{companymstid}/otherbranchsalestranshdr/resendotherbranchsalesbetweendate")
	public String resendFailedOtherBranchBetweenDate(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "fromdate") String fromdate, @RequestParam(value = "todate") String todate) {
		Date fuDate = SystemSetting.StringToUtilDate(fromdate, "yyyy-MM-dd");
		Date tudate = SystemSetting.StringToUtilDate(todate, "yyyy-MM-dd");
		List<OtherBranchSalesTransHdr> otherbranchSalesList = otherBranchSalesTransHdrRepository
				.findBetweenVoucherDate(fuDate, tudate);
		for (int i = 0; i < otherbranchSalesList.size(); i++) {
			otherBranchSalesService.resendOtherBranchSales(otherbranchSalesList.get(i).getId());
		}
		return "success";
	}

	@GetMapping("{companymstid}/otherbranchsalestranshdrbydate")
	public List<OtherBranchSalesTransHdr> getOtherSalesTransHdrByVoucherDate(
			@PathVariable(value = "companymstid") String companymstid, @RequestParam("date") String reportdate)

	{
		java.util.Date date = SystemSetting.StringToUtilDate(reportdate, "dd-MM-yyyy");

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return otherBranchSalesTransHdrRepository.findByVoucherDateAndCompanyMst(date, companyOpt.get());

	}

	@GetMapping("{companymstid}/otherbranchsalestranshdr/otherbranchsalestranshdrbysalesid/{hdrid}")
	public OtherBranchSalesTransHdr getOtherSalesTransHdrBySalesHdrId(
			@PathVariable(value = "companymstid") String companymstid, @PathVariable(value = "hdrid") String hdrid)

	{

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return otherBranchSalesTransHdrRepository.findBySalesTransHdrIdAndCompanyMst(hdrid, companyOpt.get());

	}

	@GetMapping("{companymstid}/otherbranchsalestranshdr/otherbranchsalestranshdrbyid/{id}")
	public OtherBranchSalesTransHdr getOtherSalesTransHdrById(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id)

	{

		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		return otherBranchSalesTransHdrRepository.findByIdAndCompanyMst(id, companyOpt.get());

	}

	// ===============================2021-3-21 sharon=========================
	@GetMapping("{companymstid}/otherbranchssalestranshdrresource/getotherbranchpopup")
	public @ResponseBody List<OtherBranchSalesTransHdr> searchOtherBranch(@RequestParam("data") String searchstring) {
		return otherBranchSalesTransHdrRepository.getOtherBranchVoucherNumber("%" + searchstring + "%");
	}
	// ===============================2021-3-21 sharon end========================

	@GetMapping("{companymstid}/otherbranchsalestranshdr/{hdrid}/getotherbranchsalesbyid")
	public String getOtherBranchSalesInHdrById(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("hdrid") String hdrid) {

		try {
			
			System.out.println("Inside External API");
			
			externalApi.getOtherBranchSalesFromServer(companymstid, hdrid);

			return "Success";
			
		} catch (Exception e) {

		 return "Failed";
		}
		

	}

	

}
