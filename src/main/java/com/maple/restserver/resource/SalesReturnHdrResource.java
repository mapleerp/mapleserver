package com.maple.restserver.resource;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.MultiUnitMst;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesReturnDtl;
import com.maple.restserver.entity.SalesReturnHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.SysDateMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.MultiUnitMstRepository;
import com.maple.restserver.repository.SalesRetunDtlRepository;
import com.maple.restserver.repository.SalesReturnHdrRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.service.accounting.task.PartialAccountingException;
import com.maple.restserver.service.accounting.task.SalesReturnAccounting;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

@RestController
@Transactional
public class SalesReturnHdrResource {
	
	private static final Logger logger = LoggerFactory.getLogger(SalesReturnHdrResource.class);
	
	boolean recurssionOff = false;
	EventBus eventBus = EventBusFactory.getEventBus();
	
	@Autowired
	private VoucherNumberService voucherService;
	
@Autowired
SalesReturnHdrRepository salesReturnHdrRepository;

@Autowired
CompanyMstRepository companyMstRepository;

@Autowired
SalesReturnAccounting salesReturnAccounting;

@Autowired
private SalesRetunDtlRepository salesRetunDtlRepository;

@Autowired
private ItemMstRepository itemMstRepository;

@Autowired
private ItemBatchMstRepository itemBatchMstRepo;


@Autowired
private ItemBatchDtlRepository itemBatchDtlRepository;


@Autowired
MultiUnitMstRepository multiUnitMstRepository;


@Autowired
LmsQueueMstRepository lmsQueueMstRepository;

@PostMapping("{companymstid}/salesreturnhdr")
public SalesReturnHdr createSalesReturnHdr(@PathVariable(value = "companymstid") String
		 companymstid,@Valid @RequestBody 
		 SalesReturnHdr  salesReturnHdr)
{

	
	Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
	
	CompanyMst companyMst = companyMstOpt.get();
	salesReturnHdr.setCompanyMst(companyMst);
	
   return salesReturnHdrRepository.saveAndFlush(salesReturnHdr);
	
	} 
@PutMapping("{companymstid}/salesreturnhdr/{hdrId}/updatesalesreturn")
public SalesReturnHdr updateSalesReturnHdr(
		@PathVariable(value="hdrId") String hdrId, 
		@PathVariable(value = "companymstid") String companymstid ,
		@Valid @RequestBody SalesReturnHdr  salesReturnHdrRequest)
{
		
	SalesReturnHdr salesReturnHdr = salesReturnHdrRepository.findById(hdrId).get();
	
	salesReturnHdr.setVoucherNumber(salesReturnHdrRequest.getVoucherNumber());
	salesReturnHdr.setVoucherDate(salesReturnHdrRequest.getVoucherDate());
	salesReturnHdr.setInvoiceAmount(salesReturnHdrRequest.getInvoiceAmount());
	salesReturnHdr = salesReturnHdrRepository.save(salesReturnHdr);
	
	try {
		salesReturnAccounting.execute(salesReturnHdr.getVoucherNumber(), salesReturnHdr.getVoucherDate(),
				salesReturnHdr.getId(), salesReturnHdr.getCompanyMst());
	} catch (PartialAccountingException e) {
		// TODO Auto-generated catch block
		logger.error(e.getMessage());
	}
	
   
	SalesReturnStockUpdate(  salesReturnHdr.getVoucherNumber(),   salesReturnHdr.getVoucherDate(), 
			salesReturnHdr);
	
	
	
	return salesReturnHdr;
	 			
}

@GetMapping("{companymstid}/salesreturnhdrbyvoucheranddate/{vouchernumber}")
public SalesReturnHdr getSalesReturnHdrVoucherNumberAndDate(@PathVariable(value = "companymstid") String
		  companymstid, @PathVariable("vouchernumber") String vouchernumber ,
		  @RequestParam("rdate") String reportdate )


{
	java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"yyyy-MM-dd");
	return salesReturnHdrRepository.findByVoucherNumberAndVoucherDateAndCompanyMstId(vouchernumber,date,companymstid);
	
}


@GetMapping("{companymstid}/salesreturnhdr/{salesreturnhdrid}")
public SalesReturnHdr getSalesReturnHdrById(@PathVariable(value = "companymstid") String
		  companymstid,
		  @PathVariable("salesreturnhdrid") String salesreturnhdrid )
{
	return salesReturnHdrRepository.findByIdAndCompanyMstId(salesreturnhdrid,companymstid);
	
}


private void SalesReturnStockUpdate(String voucherNumber, Date voucherDate, SalesReturnHdr salesReturnHdr) {

	// Optional<SalesTransHdr> salesTransHdr =
	// salesTransHdrRepo.findById(salesTransHdrId);

	// List<PurchaseHdr> puchaseList =
	// purchaseHdrRepo.findByVoucherNumberAndVoucherDate( voucherNumber,
	// voucherDate);
	// PurchaseHdr puchaseHdr = puchaseList.get(0);

	List<SalesReturnDtl> salesReturnDtlList = salesRetunDtlRepository.findBySalesReturnHdrId(salesReturnHdr.getId());

	Iterator iter = salesReturnDtlList.iterator();
	while (iter.hasNext()) {
		SalesReturnDtl salesReturnDtl = (SalesReturnDtl) iter.next();
		ItemMst item = itemMstRepository.findById(salesReturnDtl.getItemId()).get();
		// find if batch mast has an entry
		List<ItemBatchMst> itembatchmst = (List<ItemBatchMst>) itemBatchMstRepo
				.findByItemIdAndBatchAndBarcode(salesReturnDtl.getItemId(), salesReturnDtl.getBatch(), salesReturnDtl.getBarcode());
		double conversionQty = 0.0;
		if (itembatchmst.size() == 1) {
			ItemBatchMst itemBatchMst = itembatchmst.get(0);
			if (!item.getUnitId().equalsIgnoreCase(salesReturnDtl.getUnitId())) {

				String itemId = item.getId();
				String sourceUnit = salesReturnDtl.getUnitId();
				String targetUnit = item.getUnitId();

				recurssionOff = false;
				conversionQty = getConvertionQty(salesReturnHdr.getCompanyMst().getId(), item.getId(), sourceUnit,
						targetUnit, salesReturnDtl.getQty());
				itemBatchMst.setQty(itemBatchMst.getQty() + conversionQty);
			} else {
				itemBatchMst.setQty(itemBatchMst.getQty() + salesReturnDtl.getQty());
			}

			// itemBatchMst.setMrp(salesDtl.getMrp());
			// itemBatchMst.setQty(itemBatchMst.getQty() - salesDtl.getQty());
			itemBatchMst.setBranchCode(salesReturnHdr.getBranchCode());
			itemBatchMstRepo.saveAndFlush(itemBatchMst);
		} else {
			ItemBatchMst itemBatchMst = new ItemBatchMst();
			itemBatchMst.setBarcode(salesReturnDtl.getBarcode());
			itemBatchMst.setBatch(salesReturnDtl.getBatch());
			itemBatchMst.setMrp(salesReturnDtl.getMrp());
			itemBatchMst.setQty(salesReturnDtl.getQty());
			itemBatchMst.setItemId(salesReturnDtl.getItemId());
			itemBatchMst.setBranchCode(salesReturnHdr.getBranchCode());
			itemBatchMst.setCompanyMst(salesReturnHdr.getCompanyMst());
			itemBatchMstRepo.saveAndFlush(itemBatchMst);

		}
		ItemBatchMst itemBatchMst = itembatchmst.get(0);
		ItemBatchDtl itemBatchDtl = new ItemBatchDtl();
		final VoucherNumber voucherNo = voucherService.generateInvoice("STV",
				salesReturnHdr.getCompanyMst().getId());
		if (!item.getUnitId().equalsIgnoreCase(salesReturnDtl.getUnitId())) {

			String itemId = item.getId();
			String sourceUnit = salesReturnDtl.getUnitId();
			String targetUnit = item.getUnitId();

			recurssionOff = false;
			conversionQty = getConvertionQty(salesReturnHdr.getCompanyMst().getId(), item.getId(), sourceUnit,
					targetUnit, salesReturnDtl.getQty());
			
			itemBatchDtl.setQtyIn(conversionQty);
		} else {
			
			itemBatchDtl.setQtyIn(salesReturnDtl.getQty());
		}
		itemBatchDtl.setBarcode(salesReturnDtl.getBarcode());
		itemBatchDtl.setBatch(salesReturnDtl.getBatch());
		itemBatchDtl.setItemId(salesReturnDtl.getItemId());
		itemBatchDtl.setMrp(salesReturnDtl.getMrp());
		itemBatchDtl.setQtyOut(0.0);
		itemBatchDtl.setExpDate(itemBatchMst.getExpDate());
		itemBatchDtl.setVoucherNumber(voucherNo.getCode());
		itemBatchDtl.setVoucherDate(salesReturnHdr.getVoucherDate());
		itemBatchDtl.setItemId(salesReturnDtl.getItemId());
		itemBatchDtl.setSourceVoucherNumber(salesReturnHdr.getVoucherNumber());
		itemBatchDtl.setSourceVoucherDate(salesReturnHdr.getVoucherDate());
		itemBatchDtl.setCompanyMst(salesReturnHdr.getCompanyMst());
		itemBatchDtl.setBranchCode(salesReturnHdr.getBranchCode());

		itemBatchDtl.setStore("MAIN");
		itemBatchDtl.setSourceParentId(salesReturnHdr.getId());
		itemBatchDtl.setSourceDtlId(salesReturnDtl.getId());
		itemBatchDtl.setParticulars("SALES " + salesReturnHdr.getAccountHeads().getAccountName());

		itemBatchDtl = itemBatchDtlRepository.saveAndFlush(itemBatchDtl);

		/*
		 * Forward Item Batch Dtl.
		 *
		 */

		Map<String, Object> variables = new HashMap<String, Object>();

		variables.put("voucherNumber", itemBatchDtl.getId());
		variables.put("id", itemBatchDtl.getId());
		variables.put("voucherDate", SystemSetting.getSystemDate());
		variables.put("inet", 0);
		variables.put("REST", 1);
		variables.put("companyid", salesReturnHdr.getCompanyMst());
		variables.put("WF", "forwardItemBatchDtl");

		LmsQueueMst lmsQueueMst = new LmsQueueMst();

		lmsQueueMst.setCompanyMst((CompanyMst) variables.get("companyid"));
		
		
		//lmsQueueMst.setVoucherDate((Date) variables.get("voucherDate"));
		
		java.util.Date uDate = (Date) variables.get("voucherDate");
		java.sql.Date sqlDate = SystemSetting.UtilDateToSQLDate(uDate);
		 
		java.util.Date   uVDate = SystemSetting.SqlDateToUtilDate(sqlDate);		
		lmsQueueMst.setVoucherDate(sqlDate);
		
		
		lmsQueueMst.setVoucherNumber((String) variables.get("voucherNumber"));
		lmsQueueMst.setVoucherType("forwardItemBatchDtl");
		lmsQueueMst.setPostedToServer("NO");
		lmsQueueMst.setJobClass("forwardItemBatchDtl");
		lmsQueueMst.setCronJob(true);
		lmsQueueMst.setJobName("forwardItemBatchDtl" + itemBatchDtl.getId());
		lmsQueueMst.setJobGroup("forwardItemBatchDtl");
		lmsQueueMst.setRepeatTime(60000L);
		lmsQueueMst.setSourceObjectId(itemBatchDtl.getId());

		lmsQueueMst.setBranchCode((String) variables.get("branchcode"));

		lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);
		variables.put("lmsqid", lmsQueueMst.getId());

		eventBus.post(variables);

	}

}
private double getConvertionQty(String companyMstId, String itemId, String sourceUnit, String targetUnit,
		double sourceQty) {
	if (recurssionOff) {
		return sourceQty;
	}
	MultiUnitMst multiUnitMstList = multiUnitMstRepository.findByCompanyMstIdAndItemIdAndUnit1(companyMstId, itemId,
			sourceUnit);

	while (!multiUnitMstList.getUnit2().equalsIgnoreCase(targetUnit)) {

		if (recurssionOff) {
			break;
		}
		sourceUnit = multiUnitMstList.getUnit2();

		sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();

		getConvertionQty(companyMstId, itemId, sourceUnit, targetUnit, sourceQty);

	}
	sourceQty = sourceQty * multiUnitMstList.getQty2() * multiUnitMstList.getQty1();
	recurssionOff = true;
	// sourceQty = sourceQty *
	// multiUnitMstList.getQty2()*multiUnitMstList.getQty1();
	return sourceQty;
}
}
