package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.NutritionValueDtl;
import com.maple.restserver.entity.NutritionValueMst;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.NutritionValueMstRepository;

@RestController
@Transactional
public class NutritionValueMstResource {

	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@Autowired
	NutritionValueMstRepository nutrintionMstRepo;
	
	@PostMapping("{companymstid}/nutritionvaluemst")
	public NutritionValueMst createNutritionValueMst(@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody  NutritionValueMst nutritionValueMst)
	{
		Optional<CompanyMst> companyMst= companyMstRepo.findById(companymstid);
		nutritionValueMst.setCompanyMst(companyMst.get());
			return nutrintionMstRepo.saveAndFlush(nutritionValueMst);

	}
	@GetMapping("{companymstid}/nutritionnvaluemst/{itemid}/getnutritionvaluemst")
	public NutritionValueMst retrieveNutritionValueMstByItemMst(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "itemid") String itemid) {
		return nutrintionMstRepo.findByCompanyMstIdAndItemMstId(companymstid,itemid );

	}
	@PutMapping("{companymstid}/nutritionvaluemst/{mstid}/updatenutritionvaluemst")
	public NutritionValueMst updateNutritionValueMst(
			@PathVariable(value="mstid") String mstid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@Valid @RequestBody NutritionValueMst  nutritionValueMstRequest)
	{
			
		NutritionValueMst nutritionValueMst = nutrintionMstRepo.findById(mstid).get();
		
		
		Optional<CompanyMst> comapnyMstOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = comapnyMstOpt.get();
		nutritionValueMst.setCompanyMst(companyMst);
		nutritionValueMst.setCalories(nutritionValueMstRequest.getCalories());
		nutritionValueMst.setFat(nutritionValueMstRequest.getFat());
		nutritionValueMst.setItemMst(nutritionValueMstRequest.getItemMst());
		nutritionValueMst.setNutrition(nutritionValueMstRequest.getNutrition());
		nutritionValueMst.setServingSize(nutritionValueMstRequest.getServingSize());
		nutritionValueMst = nutrintionMstRepo.save(nutritionValueMst);
		
        return nutritionValueMst;
		 			
	}
}
