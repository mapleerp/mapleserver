
package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.GstMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.GstMstRepository;


@RestController

public class GstMstResource {
	
	@Autowired
	private GstMstRepository gstMstRepository;
	
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@GetMapping("{companymstid}/gstmst")
	public List< GstMst> retrieveAllGst(@PathVariable(value = "companymstid") String
			  companymstid){
		return gstMstRepository.findByCompanyMstId(companymstid);
	}
	
	@PostMapping("{companymstid}/gstmst")
	public GstMst createsupplier(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody 
			GstMst  gstMst)
	{
		
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			gstMst.setCompanyMst(companyMst);
	return gstMstRepository.saveAndFlush(gstMst);
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
		
	/*
	 * Work flow forwardGst
	 */

	}


