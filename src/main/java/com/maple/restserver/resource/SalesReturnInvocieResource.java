package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.SalesReturnInvoiceReport;
import com.maple.restserver.report.entity.TaxSummaryMst;
import com.maple.restserver.service.SalesReturnInvoiceServoice;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class SalesReturnInvocieResource {

	@Autowired
	SalesReturnInvoiceServoice salesReturnInvoiceServoice;
	
	
	
	@GetMapping("{companymstid}/salesreturninvoiceresource/salesreturninvoice/{voucherNumber}")
	public List<SalesReturnInvoiceReport> getSalesReturnInvoice(@PathVariable("voucherNumber") String voucherNumber,
			@RequestParam("rdate") String reportdate,
			@PathVariable(value = "companymstid") String  companymstid) {
		java.sql.Date date = SystemSetting.StringToSqlDate(reportdate, "yyyy-MM-dd");
	
		return salesReturnInvoiceServoice.getSalesReturnInvoice(companymstid,voucherNumber, date);

	}
	
	
	
	
	@GetMapping("{companymstid}/salesreturninvoiceresource/salesreturninvoicetax/{vouchernumber}")
	public List<TaxSummaryMst> getSalesReturnInvoiceTax(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("vouchernumber") String vouchernumber, @RequestParam("rdate") String rdate) {
		java.sql.Date date = SystemSetting.StringToSqlDate(rdate, "yyyy-MM-dd");

		return salesReturnInvoiceServoice.getSalesReturnInvoiceTax(companymstid, vouchernumber, date);

	}
	
	
	

	@GetMapping("{companymstid}/salesreturninvoiceresource/sumoftaxes/{vouchernumber}")
	public List<TaxSummaryMst> getSumOfSlaesReturnTaxAmounts(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("vouchernumber") String vouchernumber, @RequestParam("rdate") String rdate) {
		java.sql.Date date = SystemSetting.StringToSqlDate(rdate, "yyyy-MM-dd");

		return salesReturnInvoiceServoice.getSumOfSalesReturnTaxAmounts(companymstid, vouchernumber, date);

	}
	
	

	@GetMapping("{companymstid}/salesreturninvoiceresource/gettaxinvoicecustomer/{vouchernumber}")
	public List<SalesReturnInvoiceReport> getTaxinvoiceCustomer(@PathVariable(value = "companymstid") String companymstid,
			@PathVariable("vouchernumber") String vouchernumber, @RequestParam("rdate") String rdate) {
		java.sql.Date date = SystemSetting.StringToSqlDate(rdate, "yyyy-MM-dd");

		return salesReturnInvoiceServoice.getTaxinvoiceCustomer(companymstid, vouchernumber, date);

	}
	
	

	@GetMapping("{companymstid}/salesreturninvoiceresource/cessamount/{vouchernumber}")
	public Double getCessAmount(@PathVariable(value = "companymstid") String
			  companymstid,
			 @PathVariable("vouchernumber") String vouchernumber,  @RequestParam("rdate") String reportdate){
		java.sql.Date date = SystemSetting.StringToSqlDate(reportdate,"yyyy-MM-dd");
	


		return salesReturnInvoiceServoice.getCessAmount(vouchernumber, date );
		
	}
	

	@GetMapping("{companymstid}/salesreturninvoiceresource/nontaxableamount/{vouchernumber}")
	public Double getNonTaxableAmount(@PathVariable(value = "companymstid") String
			  companymstid,
			 @PathVariable("vouchernumber") String vouchernumber,  @RequestParam("rdate") String reportdate){
		java.sql.Date date = SystemSetting.StringToSqlDate(reportdate,"yyyy-MM-dd");

		return salesReturnInvoiceServoice.getNonTaxableAmount(vouchernumber, date );

		
	}
}
