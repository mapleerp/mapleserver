
package com.maple.restserver.resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ItemPopUp;
import com.maple.restserver.report.entity.ItemDetailPopUp;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.ItemPopUpRepository;
import com.maple.restserver.repository.ItemStockPopUpRepository;
import com.maple.restserver.service.ItemDetailPopUpService;
import com.maple.restserver.utils.SystemSetting;
@CrossOrigin("http://localhost:4200")
@RestController
@Transactional
public class PopUpResource {

	Pageable topFifty = PageRequest.of(0, 50);

	private static final Logger logger = LoggerFactory.getLogger(PaymentHdrResource.class);

	@Value("${spring.datasource.url}")
	private String dbType;

	@Autowired
	private ItemPopUpRepository itemPopUpRepo;
	
	@Autowired
	ItemDetailPopUpService itemDetailPopUpService;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Autowired
	CategoryMstRepository categoryMstRepository;

	@Autowired
	private ItemStockPopUpRepository itemStockPopUpRepository;

	@GetMapping("{companymstid}/itempopup/{searchstring}")
	public List<Object> retrieveAllItem(@PathVariable String searchstring,
			@PathVariable("companymstid") String companymstid) {
		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companymst.get();
		return itemPopUpRepo.findSearch("%" + searchstring.toLowerCase() + "%", topFifty);
	}

	@GetMapping("{companymstid}/itempopup")
	public List<ItemPopUp> retrieveAllSupplier() {
		return itemPopUpRepo.findAll();
	}

	@GetMapping("{companymstid}/itempopupsearch")
	public @ResponseBody List<Object> searchItems(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid) {
		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companymst.get();

		return itemPopUpRepo.findSearch("%" + searchstring.toLowerCase() + "%", topFifty);

	}

	@GetMapping("{companymstid}/itempopupsearchwithcomp")
	public synchronized @ResponseBody List<Object> searchItemsWithComp(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid) {
		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companymst.get();

		// itemPopUpRepo.findSearchWithComp("%"+searchstring.toLowerCase()+"%",topFifty,companyMst)
		// ;

		searchstring = searchstring.replaceAll("20%", " ");

		List<Object> objByItemCode = itemPopUpRepo.findSearchByItemCode(searchstring.toLowerCase(), topFifty,
				companyMst);

		if (objByItemCode.size() == 0) {
			objByItemCode = itemPopUpRepo.findSearchByBarcodeWithComp(searchstring.toLowerCase(), topFifty, companyMst);
		}

		if (objByItemCode.size() == 0) {
			objByItemCode = itemPopUpRepo.findSearchWithComp("%" + searchstring.toLowerCase() + "%", topFifty,
					companyMst);
		}

		return objByItemCode;

	}

	@GetMapping("{companymstid}/popupresource/itemforinmemdb")
	public synchronized @ResponseBody List<Object> ItemsForInMem() {

		List<Object> objByItemCode = itemPopUpRepo.itemsForInMemDb();
		return objByItemCode;

	}

	@GetMapping("{companymstid}/itempopupsearchwithcatregoryname")
	public synchronized @ResponseBody List<Object> searchItemsWithCategory(@RequestParam("data") String searchstring,
			@RequestParam("categoryname") String categoryname, @PathVariable("companymstid") String companymstid) {
		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companymst.get();

		// itemPopUpRepo.findSearchWithComp("%"+searchstring.toLowerCase()+"%",topFifty,companyMst)
		// ;

		searchstring = searchstring.replaceAll("20%", " ");
		String categoryNames = categoryname;
		List<Object> objByItemCode = new ArrayList<Object>();
		String[] array = categoryNames.split(";");

		for (String itr : array) {
			CategoryMst categoryMst = categoryMstRepository.findByCategoryNameAndCompanyMst(itr, companyMst);
			if (categoryname.isEmpty() || null == categoryMst) {

				objByItemCode = itemPopUpRepo.findSearchByItemCode(searchstring.toLowerCase(), topFifty, companyMst);

				if (objByItemCode.size() == 0) {
					objByItemCode = itemPopUpRepo.findSearchByBarcodeWithComp(searchstring.toLowerCase(), topFifty,
							companyMst);
				}

				if (objByItemCode.size() == 0) {
					objByItemCode = itemPopUpRepo.findSearchWithComp("%" + searchstring.toLowerCase() + "%", topFifty,
							companyMst);
				}

			} else {
				for (String categoryName : array) {

					categoryMst = categoryMstRepository.findByCategoryNameAndCompanyMst(categoryName, companyMst);
					objByItemCode = itemPopUpRepo.findSearchByItemCode(searchstring.toLowerCase(), topFifty,
							companyMst);
					if (objByItemCode.size() == 0 && null != categoryMst) {
						objByItemCode = itemPopUpRepo.findSearchByBarcodeWithCategory(searchstring.toLowerCase(),
								topFifty, companyMst, categoryMst.getId());
					}

					if (objByItemCode.size() == 0 && null != categoryMst) {
						objByItemCode = itemPopUpRepo.findSearchWithCategory("%" + searchstring.toLowerCase() + "%",
								topFifty, companyMst, categoryMst.getId());
					}

				}
			}
		}
		return objByItemCode;

	}

	@GetMapping("{companymstid}/popupresource/itempopupbybarcode")
	public synchronized @ResponseBody List<Object> searchItemByBarcodeWithComp(
			@RequestParam("data") String searchstring, @PathVariable("companymstid") String companymstid) {
		Optional<CompanyMst> companymst = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companymst.get();

		// itemPopUpRepo.findSearchWithComp("%"+searchstring.toLowerCase()+"%",topFifty,companyMst)
		// ;

		searchstring = searchstring.replaceAll("20%", " ");

		List<Object> objByItemCode = itemPopUpRepo.findSearchByBarcodeDummyStockWithComp(searchstring.toLowerCase(),
				topFifty, companyMst);

		/*
		 * List<Object> objByItemCode =
		 * itemPopUpRepo.findSearchByItemCode(searchstring.toLowerCase(), topFifty,
		 * companyMst) ;
		 * 
		 * 
		 * if(objByItemCode.size()==0) { objByItemCode =
		 * itemPopUpRepo.findSearchByBarcodeWithComp(searchstring.toLowerCase(),
		 * topFifty,companyMst) ; }
		 * 
		 * if(objByItemCode.size()==0) { objByItemCode
		 * =itemPopUpRepo.findSearchWithComp("%"+searchstring.toLowerCase()+"%",
		 * topFifty,companyMst) ; }
		 */

		return objByItemCode;

	}

	//

	@GetMapping("{companymstid}/popup/itempopupsearchlimit")
	public @ResponseBody List<ItemMst> findItemBySearch(@RequestParam("data") String searchstring) {

		searchstring = searchstring.replaceAll("%20", " ");
		return itemPopUpRepo.findItemBySearch("%" + searchstring.toLowerCase() + "%", topFifty);

	}

	@GetMapping("{companymstid}/popup/itempopupsearchbybarcode")
	public @ResponseBody List<ItemMst> findItemByBarcodeSearch(@RequestParam("data") String searchstring) {

		return itemPopUpRepo.findItemBySearch("%" + searchstring.toLowerCase() + "%", topFifty);

	}

	@GetMapping("{companymstid}/stockitempopupsearch")
	public @ResponseBody List<Object> searchItemStock(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid) {

		Optional<CompanyMst> companyMst1 = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMst1.get();
		searchstring = searchstring.replaceAll("20%", " ");

		List<Object> objByItemCode = itemStockPopUpRepository.findSearchByItemCode(searchstring.toLowerCase(),
				topFifty);

		if (objByItemCode.size() == 0) {
			objByItemCode = itemStockPopUpRepository.findSearch("%" + searchstring.toLowerCase() + "%", topFifty);
		}
		return objByItemCode;
	}

	@GetMapping("{companymstid}/stockitempopupsearchwithcom/{branchcode}")
	public @ResponseBody List<Object> searchItemStockWithCom(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid, @PathVariable("branchcode") String branchcode) {

		Optional<CompanyMst> companyMst1 = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMst1.get();
		searchstring = searchstring.replaceAll("20%", " ");

		List<Object> objByItemCode = itemStockPopUpRepository.findSearchByItemCodeWithComp(searchstring.toLowerCase(),
				topFifty, companyMst);

		if (objByItemCode.size() == 0) {
			objByItemCode = itemStockPopUpRepository.findSearchByBarcodeWithComp(searchstring.toLowerCase(), topFifty,
					companyMst);
		}

		if (objByItemCode.size() == 0) {
			objByItemCode = itemStockPopUpRepository.findSearchWithComp("%" + searchstring.toLowerCase() + "%",
					topFifty, companyMst);
		}
		return objByItemCode;
	}

	@GetMapping("{companymstid}/popupresource/stokcpopupwithouzerostock/{branchcode}")
	public @ResponseBody List<Object> searchItemWithoutZeroStock(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid, @PathVariable("branchcode") String branchcode) {

		Optional<CompanyMst> companyMst1 = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMst1.get();
		searchstring = searchstring.replaceAll("20%", " ");

		List<Object> objByItemCode = itemStockPopUpRepository.findSearchByBarcodeWithComp(searchstring.toLowerCase(),
				topFifty, companyMst);

		if (objByItemCode.size() == 0) {
			objByItemCode = itemStockPopUpRepository.findSearchByBarcodeWithComp(searchstring.toLowerCase(), topFifty,
					companyMst);
		}

		if (objByItemCode.size() == 0) {
			objByItemCode = itemStockPopUpRepository.findSearchWithComp("%" + searchstring.toLowerCase() + "%",
					topFifty, companyMst);
		}
		return objByItemCode;
	}

	@GetMapping("{companymstid}/stockitemsearch/{data}/{batch}/{branchcode}")
	public @ResponseBody List<Object> searchItemStockByName(@PathVariable("data") String searchstring,
			@PathVariable("batch") String batch) {

		return itemStockPopUpRepository.findSearchbyItemName(searchstring, batch, topFifty);
	}

	// ===============stock checking using store name
	// ===========anandu================
	@GetMapping("{companymstid}/popupresource/stockitemsearch/{data}/{batch}/{branchcode}/{storename}")
	public @ResponseBody List<Object> searchItemStockByStoreName(@PathVariable("data") String searchstring,
			@PathVariable("batch") String batch, @PathVariable("branchcode") String branchcode,
			@PathVariable("storename") String storename) {

		return itemStockPopUpRepository.findSearchbyItemNameAndStore(searchstring, batch, topFifty, storename);
	}
	// =============================end stock checking==========================

	@GetMapping("{companymstid}/stockitemsearch/{data}/{num}/lastfivetransactions")
	public @ResponseBody List<Object> findSearchbyItemNameTop(@PathVariable("data") String searchstring,
			@PathVariable("num") String num) {

		Integer topNum = Integer.parseInt(num);
		Pageable topValue = PageRequest.of(0, topNum);
		// topValue =
		return itemStockPopUpRepository.findSearchbyItemNameTop(searchstring, topValue);
	}

	@GetMapping("{companymstid}/stockitemsearch/{data}/{supId}/{num}/lastfivetransactionswithcustomer")
	public @ResponseBody List<Object> findSearchbyItemNameByCustomerTop(@PathVariable("data") String searchstring,
			@PathVariable("supId") String supId, @PathVariable("num") String num) {
		Integer topNum = Integer.parseInt(num);
		Pageable topValue = PageRequest.of(0, topNum);

		return itemStockPopUpRepository.findSearchbyItemNameByCustomerTop(searchstring, supId, topValue);
	}

	@GetMapping("{companymstid}/popup/stockitempopupsearchbybarcode/{branchcode}")
	public @ResponseBody List<Object> searchItemStockByBarcodeAndBatch(@PathVariable("branchcode") String branchcode,
			@RequestParam("data") String searchstring, @RequestParam("batch") String batch) {

		// List<Object> itemBarcodeList =
		// itemStockPopUpRepository.findSearchBarcode(searchstring.toLowerCase(),
		// topFifty) ;

		/*
		 * Iterator iter = itemBarcodeList.iterator(); while(iter.hasNext()) { List
		 * element = (List) iter.next(); String itemName = (String) element.get(0);
		 * String barCode = (String) element.get(1); String unitname = (String)
		 * element.get(2); Double mrp = (Double) element.get(3); Double qty = (Double)
		 * element.get(4); Double cess = (Double) element.get(4); Double taxRate =
		 * (Double) element.get(5); String itemId = (String) element.get(6); String
		 * unitId = (String) element.get(7); String batch = (String) element.get(8); }
		 */

		return itemStockPopUpRepository.findSearchBarcodeAndBatch(searchstring.toLowerCase(), batch.toLowerCase(),
				topFifty);
	}

	@GetMapping("{companymstid}/stockitempopupsearchbybarcode/{branchcode}")
	public @ResponseBody List<Object> searchItemStockByBarcode(@PathVariable("branchcode") String branchcode,
			@RequestParam("data") String searchstring) {

		// List<Object> itemBarcodeList =
		// itemStockPopUpRepository.findSearchBarcode(searchstring.toLowerCase(),
		// topFifty) ;

		/*
		 * Iterator iter = itemBarcodeList.iterator(); while(iter.hasNext()) { List
		 * element = (List) iter.next(); String itemName = (String) element.get(0);
		 * String barCode = (String) element.get(1); String unitname = (String)
		 * element.get(2); Double mrp = (Double) element.get(3); Double qty = (Double)
		 * element.get(4); Double cess = (Double) element.get(4); Double taxRate =
		 * (Double) element.get(5); String itemId = (String) element.get(6); String
		 * unitId = (String) element.get(7); String batch = (String) element.get(8); } 1
		 */

		return itemStockPopUpRepository.findSearchBarcode(searchstring.toLowerCase(), topFifty);
	}

//	@GetMapping("{companymstid}/stockitemsearchbycustomer/{data}/{customerphone}")
//	public  @ResponseBody List<Object> searchItemStockByNameCustomer(@PathVariable("data") String searchstring){
//		return itemStockPopUpRepository.findSearchbyItemName(searchstring) ;
//	}

	// -------------------new version 1.6 surya
	// stockitempopupsearchwithbatch
	@GetMapping("{companymstid}/stockitempopupsearchwithbatch")
	public @ResponseBody List<Object> searchItemStockWithBatch(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid, @RequestParam("date") String date) {

		Date sdate = SystemSetting.StringToUtilDate(date, "yyyy-MM-dd");

		Optional<CompanyMst> companyMst1 = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMst1.get();
		searchstring = searchstring.replaceAll("20%", " ");

		// =============================================
		List<Object> objByItemCode = new ArrayList<>();
		if (dbType.contains("mysql")) {
			 objByItemCode = itemStockPopUpRepository
					.findSearchByItemCodeWithBatch(searchstring.toLowerCase(), topFifty, companyMst, sdate);
			System.out.println("findSearchByItemCodeWithBatch");
			if (objByItemCode.size() == 0) {
				objByItemCode = itemStockPopUpRepository.findSearchByBarcodeWithBatch(searchstring.toLowerCase(),
						topFifty, companyMst, sdate);
				System.out.println("findSearchByBarcodeWithBatch");

			}

			if (objByItemCode.size() == 0) {
				objByItemCode = itemStockPopUpRepository.findSearchWithBatch("%" + searchstring.toLowerCase() + "%",
						topFifty, companyMst, sdate);
				System.out.println("findSearchWithBatch");

			}
		} else if (dbType.contains("derby")) {
			
			 objByItemCode = itemStockPopUpRepository
					.findSearchByItemCodeWithBatchDerby(searchstring.toLowerCase(), topFifty, companyMst, sdate);

			if (objByItemCode.size() == 0) {
				objByItemCode = itemStockPopUpRepository.findSearchByBarcodeWithBatchDerby(searchstring.toLowerCase(),
						topFifty, companyMst, sdate);
				System.out.println("findSearchByBarcodeWithBatch");

			}

			if (objByItemCode.size() == 0) {
				objByItemCode = itemStockPopUpRepository.findSearchWithBatchDerby("%" + searchstring.toLowerCase() + "%",
						topFifty, companyMst, sdate);
				System.out.println("findSearchWithBatch");

			}
		}

		// =============================================

		return objByItemCode;
	}

	// -------------------new version 1.6 surya end

	@GetMapping("{companymstid}/stockitempopupsearchwithbatchanditemid")
	public @ResponseBody List<Object> searchItemStockWithBatchAndItemId(@RequestParam("data") String itemid,
			@PathVariable("companymstid") String companymstid, @RequestParam("date") String date) {

		Date sdate = SystemSetting.StringToUtilDate(date, "yyyy-MM-dd");

		Optional<CompanyMst> companyMst1 = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMst1.get();

		List<Object> objByItemCode = itemStockPopUpRepository.findSearchWithBatchAndItemId(itemid, topFifty, companyMst,
				sdate);
		System.out.println("findSearchWithBatch");

		return objByItemCode;
	}

	// -----------------------------------sharon-----------------------jun 23 by
	// using kit publish window-------
	@GetMapping("{companymstid}/popupresource/kititempopupsearch")
	public @ResponseBody List<Object> searchKitItems(@RequestParam("data") String searchstring) {

		return itemPopUpRepo.findKitNameSearch("%" + searchstring.toLowerCase() + "%", topFifty);

	}

	// -----------------------------------sharon-----------------------jun 25 by
	// using itemstock popup -------

	@GetMapping("{companymstid}/popupresource/storeitempopupsearchbybarcode/{branchcode}")
	public @ResponseBody List<Object> SearchStoreItemByBarcode(@PathVariable("branchcode") String branchcode,
			@RequestParam("data") String searchstring) {
		return itemStockPopUpRepository.findSearchStoreBarcode(searchstring.toLowerCase(), topFifty);
	}
	
	//=============MAP-508======new url for item detail pop up in web ===========anandu=====09-12-2021===
	@GetMapping("{companymstid}/popupresource/stockitempopupsearchwithbatch")
	public @ResponseBody List<ItemDetailPopUp> getItemDetailPopUp(@RequestParam("data") String searchstring,
			@PathVariable("companymstid") String companymstid, @RequestParam("date") String date) {

		Date sdate = SystemSetting.StringToUtilDate(date, "yyyy-MM-dd");

		Optional<CompanyMst> companyMst1 = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyMst1.get();
		searchstring = searchstring.replaceAll("20%", " ");

			return itemDetailPopUpService.getItemDetailPopUp(searchstring.toLowerCase(), topFifty, companyMst, sdate);
		
	}
}
