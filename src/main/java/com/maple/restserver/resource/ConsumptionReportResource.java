package com.maple.restserver.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.report.entity.ConsumptionHdrReport;
import com.maple.restserver.report.entity.ConsumptionReport;
import com.maple.restserver.report.entity.ConsumptionReportDtl;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.service.ConsumptionReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class ConsumptionReportResource {

	
	@Autowired
	ConsumptionReportService consumptionReportService;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	
	@GetMapping("{companymstid}/consumptionreportresourec/dailyconsumption/{branchcode}")
	public List<ConsumptionReport> getConsumptionReport(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate
			 ){
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
	
		return consumptionReportService.getConsumptionReport(branchcode, fromdate,todate,companymstid );

	

	}

	
	
	@GetMapping("{companymstid}/consumptionreportresourec/consumptionhdrreport/{branchcode}/{reasonid}")
	public List<ConsumptionHdrReport> getConsumptionHdrReport(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate, @PathVariable("reasonid") String reasonid
			 ){
	
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
	
		return consumptionReportService.getConsumptionHdrReport(branchcode, fromdate,todate,companymstid,reasonid );


	}
	
	@GetMapping("{companymstid}/consumptionreportresourec/distinctconsumptionreason/{branchcode}")
	public List<ConsumptionHdrReport> getConsumptionReason(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate
			 ){
	
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
	
		return consumptionReportService.getConsumptionReason(branchcode, fromdate,todate,companymstid );


	}
	

	@GetMapping("{companymstid}/consumptionreportresourec/consumptiondtlreport/{reasonid}/{branchcode}")
	public List<ConsumptionReportDtl> getConsumptionDtlReport(
			@PathVariable(value = "companymstid") String companymstid,
			  @PathVariable("reasonid") String reasonid,@RequestParam("rdate") String strfromdate,
				 @RequestParam("tdate") String strtodate
			 
			 ){
	
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
	
	
		return consumptionReportService.getConsumptionDtlReport(reasonid,fromdate,todate );


	}
	
	@GetMapping("{companymstid}/consumptionreportresourec/consumptionamount/{branchcode}")
	public Double getConsumptionAmount(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate
			 ){
	
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
	
		return consumptionReportService.getConsumptionAmount(branchcode, fromdate,todate,companymstid );


	}
	//for amdc
	@GetMapping("{companymstid}/consumptionreportresource/getdepatmentwiseconsumption/{branchcode}/{department}")
	public List<ConsumptionReport> getDepartmentWiseConsumption(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate,
			 @RequestParam("category")String category,
			 @PathVariable(value="department")String department)
			 {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
		String[]array=category.split(";");
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		return consumptionReportService.getdepartmentWiseConsumption(department,array,branchcode,fromdate,todate,companyMst);

	

	}
	
	@GetMapping("{companymstid}/consumptionreportresource/itemwiseconsumptionreport/{branchcode}/{department}")
	public List<ConsumptionReport> getItemWiseConsumption(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode, 
			 @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate,
			 @RequestParam("items")String items,
			 @PathVariable(value="department")String department)
			 {
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
		String[]array=items.split(";");
		CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
		if(null == companyMst)
			
		{
			return null;
		}
		return consumptionReportService.getItemWiseConsumption(department,array,branchcode,fromdate,todate,companyMst);

	

	}
	
	@GetMapping("{companymstid}/consumptionreportresourec/consumptionreportsummary/{branchcode}")
	public List<ConsumptionReport>getConsumptionreportsummary(
			@PathVariable(value = "companymstid") String companymstid,
			 @PathVariable("branchcode") String branchcode,  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate
			 ){
	
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
	
		return consumptionReportService.getConsumptionreportsummary(branchcode, fromdate,todate);


	}
	
	//==================consumptiom detail report with category===================
	@GetMapping("{companymstid}/consumptionreportresource/consumptiondetail/{username}")
	public List<ConsumptionReport>getConsumptionDetail(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "username") String username,
			  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate,
			 @RequestParam("category") String category
			 ){
	
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
		
		String categoryNames = category;

		String[] array = categoryNames.split(";");
	
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return consumptionReportService.getConsumptionDetail(companyMst,fromdate,todate,array,username);


	}
	
	//==================consumptiom detail report with out category===================
	@GetMapping("{companymstid}/consumptionreportresource/consumptiondetailwithoutcategory/{username}")
	public List<ConsumptionReport>getConsumptionDetailWithoutCategory(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "username") String username,
			  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate
			 ){
	
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return consumptionReportService.getConsumptionDetailWithoutCategory(companyMst,fromdate,todate,username);


	}
	
	//==================consumptiom summary report with category===================
	@GetMapping("{companymstid}/consumptionreportresource/consumptionsummary/{username}")
	public List<ConsumptionReport>getConsumptionSummary(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "username") String username,
			  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate,
			 @RequestParam("category") String category
			 ){
	
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
		
		String categoryNames = category;

		String[] array = categoryNames.split(";");
	
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return consumptionReportService.getConsumptionSummary(companyMst,fromdate,todate,array,username);


	}

	//==================consumptiom summary report with out category===================
	@GetMapping("{companymstid}/consumptionreportresource/consumptionsummarywithoutcategory/{username}")
	public List<ConsumptionReport>getConsumptionSummaryWithoutCategory(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "username") String username,
			  @RequestParam("rdate") String strfromdate,
			 @RequestParam("tdate") String strtodate
			
			 ){
	
		java.util.Date fromdate = SystemSetting.StringToUtilDate(strfromdate,"yyyy-MM-dd");
		java.util.Date todate = SystemSetting.StringToUtilDate(strtodate,"yyyy-MM-dd");
		
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		CompanyMst companyMst = companyOpt.get();
		return consumptionReportService.getConsumptionSummaryWithoutCategory(companyMst,fromdate,todate,username);


	}

	
	
}
