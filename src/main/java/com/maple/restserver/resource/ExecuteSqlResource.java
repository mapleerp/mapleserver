package com.maple.restserver.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

 
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.AccountPayable;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ExecuteSql;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ExecuteSqlRepository;

@RestController
@Transactional

public class ExecuteSqlResource {
	

	@Autowired
	private ExecuteSqlRepository executeSqlRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	 
	@Autowired
	BranchMstRepository branchMstRepository;

	
	@PersistenceContext
	    private EntityManager em;
	
	@GetMapping("{companymstid}/executesql")
	public  List<Map<String,Object>>  executesqlget(@PathVariable(value = "companymstid") String companymstid,
			@RequestParam(value = "executeSql") String executesqlparam   )
	{
		Optional<CompanyMst> companyMstOpt=  companyMstRepo.findById(companymstid) ;
		CompanyMst companyMst = companyMstOpt.get();
		
		
		List<BranchMst> branchMstList =  branchMstRepository.getMyBranchList();
		
		executesqlparam = executesqlparam.replaceAll("%20", " ");
		executesqlparam = executesqlparam.replaceAll("%21", "/");
		
  
		
		
		ExecuteSql executeSql = new ExecuteSql();
		executeSql.setBrachCode(branchMstList.get(0).getBranchCode());
		executeSql.setCompanyMst(companyMst);
		executeSql.setExecuteInServer(false);
		executeSql.setSqlToExecute(executesqlparam);
		executeSqlRepository.save(executeSql);
		
  	 
		  Session session = em.unwrap(Session.class);
		 
	 
		  if(executesqlparam.toUpperCase().contains("UPDATE ")||
 				  executesqlparam.toUpperCase().contains("DELETE ") || 
				   executesqlparam.toUpperCase().contains("CREATE ") || 
				   executesqlparam.toUpperCase().contains("ALTER ") ||
				    executesqlparam.toUpperCase().contains("INSERT ") ||
				   executesqlparam.toUpperCase().contains("DROP ")) {
			  Query query=session.createSQLQuery(executesqlparam);
			  
			  int result = query.executeUpdate();
			 
			  
		  }else {
			  executesqlparam = executesqlparam +" fetch first 100 rows only " ;
			  Query query=session.createSQLQuery(executesqlparam);
			  ((org.hibernate.query.Query) query).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			  List<Map<String,Object>> aliasToValueMapList=((org.hibernate.query.Query) query).list();
			
			  session.close();
			  
			  return aliasToValueMapList ;
	     
		  }
		
		 //http://localhost:8185/AMB/executesql/select r.id from sales_receipts r,sales_trans_hdr where h.id=r.sales_trans_hdr_id
		  
		  session.close();
		return null ;
		
	}
	
	
	
	@PostMapping("{companymstid}/executesqlpost")
	public  List<Map<String,Object>>  executeSqlPost(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody ExecuteSql sqlToExecute
		   )
	{
		Optional<CompanyMst> companyMstOpt=  companyMstRepo.findById(companymstid) ;
		CompanyMst companyMst = companyMstOpt.get();
		
		String executesqlparam = sqlToExecute.getSqlToExecute();
		
		/*
		 * change below code to pass branch code from client.
		 * Surya told not to do it now.  she will do it later - Date 4th march 2021
		 * 
		 */
		List<BranchMst> branchMstList =  branchMstRepository.getMyBranchList();
		
		executesqlparam = executesqlparam.replaceAll("%20", " ");
		executesqlparam = executesqlparam.replaceAll("%21", "/");
		
  
		
		
		ExecuteSql executeSql = new ExecuteSql();
		executeSql.setBrachCode(branchMstList.get(0).getBranchCode());
		executeSql.setCompanyMst(companyMst);
		executeSql.setExecuteInServer(false);
		executeSql.setSqlToExecute(executesqlparam);
//		executeSqlRepository.save(executeSql);
		
  	 
		  Session session = em.unwrap(Session.class);
		 
	 
		  if(executesqlparam.toUpperCase().contains("UPDATE ")||
 				  executesqlparam.toUpperCase().contains("DELETE ") || 
				   executesqlparam.toUpperCase().contains("CREATE ") || 
				   executesqlparam.toUpperCase().contains("ALTER ") ||
				    executesqlparam.toUpperCase().contains("INSERT ") ||
				   executesqlparam.toUpperCase().contains("DROP ")) {
			  Query query=session.createSQLQuery(executesqlparam);
			  
			  int result = query.executeUpdate();
			 
			  
		  }else {
			  executesqlparam = executesqlparam  ;
			  Query query=session.createSQLQuery(executesqlparam);
			  ((org.hibernate.query.Query) query).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			  List<Map<String,Object>> aliasToValueMapList=((org.hibernate.query.Query) query).list();
			 
			  session.close();
			  
			  return aliasToValueMapList ;
	     
		  }
		
		 //http://localhost:8185/AMB/executesql/select r.id from sales_receipts r,sales_trans_hdr where h.id=r.sales_trans_hdr_id
		  
		  session.close();
		return null ;
		
	}
	


	
	

}
