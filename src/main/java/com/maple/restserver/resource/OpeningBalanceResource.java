package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.OpeningBalance;
import com.maple.restserver.repository.OpeningBalanceRepository;



@RestController
@Transactional
public class OpeningBalanceResource {
	@Autowired
	private OpeningBalanceRepository openingblnc;
	@GetMapping("{companymstid}/openingbalance")
	public List<OpeningBalance> retrieveAllcash()
	{
		return openingblnc.findAll();
	}
	@PostMapping("{companymstid}/openingbalance")
	public ResponseEntity<Object> createUser(@Valid @RequestBody OpeningBalance openingbalance1)
	{
		OpeningBalance saved=openingblnc.saveAndFlush(openingbalance1);
	URI location=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	}

}

