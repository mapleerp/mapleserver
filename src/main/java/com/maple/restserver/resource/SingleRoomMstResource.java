
package com.maple.restserver.resource;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.his.entity.SingleRoomMst;
import com.maple.restserver.repository.SingleRoomMstRepository;



@RestController
@Transactional
public class SingleRoomMstResource {
	@Autowired
	private SingleRoomMstRepository singleroommstr;
	@GetMapping("{companymstid}/singleroommst")
	public List<SingleRoomMst> retrieveAlldepartments()
	{
		return singleroommstr.findAll();
		
	}
	@PostMapping("{companymstid}/singleroommst")
	public ResponseEntity<Object>createUser(@Valid @RequestBody SingleRoomMst singleroommstr1)
	{
		SingleRoomMst saved=singleroommstr.saveAndFlush(singleroommstr1);
		URI location =ServletUriComponentsBuilder.fromCurrentRequest().path("/[{id}").
				buildAndExpand(saved.getId()).toUri();
	return ResponseEntity.created(location).build();
	
	}

}
