package com.maple.restserver.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.report.entity.IntentVoucherReport;
import com.maple.restserver.report.entity.StockTransferOutReport;
import com.maple.restserver.service.IntentReportService;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class IntentReportResource {
@Autowired
IntentReportService intentReportService;


@GetMapping("{companymstid}/intentvoucherreport/{voucherNumber}/{branchcode}")
public List<IntentVoucherReport> getIntentVoucherReport(@PathVariable(value = "companymstid") String
		  companymstid,
		 @PathVariable("voucherNumber") String voucherNumber,  @RequestParam("rdate") String reportdate,@PathVariable(value = "branchcode") String
		 branchcode){
	java.util.Date date = SystemSetting.StringToUtilDate(reportdate,"dd/MM/yyyy");


	String cashaccountid="";
	return intentReportService.getIntentVoucherReport(voucherNumber, date,companymstid,branchcode );

	
}

}
