package com.maple.restserver.resource;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.SubscriptionMst;

import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SubscriptionMstRepository;

@RestController
@Transactional
public class SubscriptionMstResource {
	
	@Autowired
	SubscriptionMstRepository subscriptionMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	@PostMapping("{companymstid}/subscriptionmstresource/checksubscriptions")
	
	public List<SubscriptionMst> checkSubscriptions(
			@PathVariable(value = "companymstid") String companymstid, @RequestBody SubscriptionMst  subscriptionMst){
		
		List<SubscriptionMst> subList =subscriptionMstRepository.findAll();
		
		if(subList.size()==0)
		{
			subscriptionMstRepository.save(subscriptionMst);
		}
		
		else {
			
			SubscriptionMst subcription =subList.get(0);
			subcription.setSubscriptionEnds(subscriptionMst.getSubscriptionEnds());
			subcription.setSubscriptionKey(subscriptionMst.getSubscriptionKey());
			subscriptionMstRepository.save(subcription);
			
		}
		return subscriptionMstRepository.findAll();
	
	
	}
	
	}
	
	


