package com.maple.restserver.resource;


import java.net.URI;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.maple.restserver.entity.CessMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.FinancialYearMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.FinancialYearMstRepository;
import com.maple.restserver.utils.SystemSetting;
import com.maple.maple.util.ClientSystemSetting;

@RestController
@Transactional
public class FinancialYearMstResource {

	
	@Autowired
	private FinancialYearMstRepository financialyearmaster;
	@GetMapping("/{companymstid}/financialyearmst")
	public List<FinancialYearMst> retrieveAllcash(@PathVariable(value = "companymstid") String
			  companymstid)
	{
		return financialyearmaster.findByCompanyMstId(companymstid);
	}
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	


	@PostMapping("/{companymstid}/financialyearmst")
	public FinancialYearMst createUser(@PathVariable(value = "companymstid") String
			  companymstid,@Valid @RequestBody FinancialYearMst financialyearmaster1)
	{
		return companyMstRepo.findById(companymstid).map(companyMst-> {
			financialyearmaster1.setCompanyMst(companyMst);
			financialyearmaster.updateFinancialYearMst();
	return financialyearmaster.saveAndFlush(financialyearmaster1);
		
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); 
		}
	




//-----------------------version 1.10
	
	//version2.0
@Transactional
@GetMapping("/{companymstid}/financialyearmst/getfinanicialyear")
public FinancialYearMst retrieveFinancialYearMstByDate(
		@PathVariable(value = "companymstid") String companymstid,
		@RequestParam("date") String date)
{
	
	java.sql.Date udate = ClientSystemSetting.StringToSqlDate(date, "yyyy-MM-dd");
	FinancialYearMst financialYearMst = financialyearmaster.findByLoginDate(companymstid,udate);
	if(null==financialYearMst) {
		// Find last financial year
		FinancialYearMst lastFinancialYearMst = financialyearmaster.findLastFY(companymstid);
		
		// Find starting year
		 
		String startDateString = SystemSetting.SqlDateTostring(lastFinancialYearMst.getStartDate(),
				 "dd/MM/yyyy");
		String[] startDateArray = startDateString.split("/");
		
		String ddStr = startDateArray[0];
		String mmStr = startDateArray[1];
		String yyyyStr = startDateArray[2];
		
		 int startYear = Integer.parseInt(yyyyStr);
		 startYear++;
		 
		//Find Ending Year
		
 
		 
			String endDateString = SystemSetting.SqlDateTostring(lastFinancialYearMst.getEndDate(),
					 "dd/MM/yyyy");
			String[] endDateArray = endDateString.split("/");
			
			String ddStrE = endDateArray[0];
			String mmStrE = endDateArray[1];
			String yyyyStrE = endDateArray[2];
			
			 int endYear = Integer.parseInt(yyyyStrE);
			 endYear++;
		 		 
		 //Build New starting date and Ending Date
		 
			 String newStartDateStr =startYear +"/" + mmStr + "/" + ddStr;
			java.sql.Date  newstartDate = SystemSetting.StringToSqlDate(newStartDateStr, "yyyy/MM/dd");
			 
			 
			 String newEndDateStr =endYear +"/" + mmStrE + "/" + ddStrE;
			 java.sql.Date newEndDate = SystemSetting.StringToSqlDate(newEndDateStr, "yyyy/MM/dd");
			
				// check if login date falls with new financial year
			 if (newstartDate.compareTo(udate)<=0) {
				 
				 if(newEndDate.compareTo(udate)>=0){
					// if yes , close previous fy and add new fy with open status

					 financialyearmaster.updateStatusAndCurrentYearOfFYMst();

					 CompanyMst companyMst = companyMstRepo.findById(companymstid).get();
					 
					 financialYearMst = new FinancialYearMst();
					 financialYearMst.setStartDate(newstartDate);
					 financialYearMst.setEndDate(newEndDate);
					 financialYearMst.setCurrentFinancialYear(1);
					 financialYearMst.setOpenCloseStatus("OPEN");
					 financialYearMst.setFinancialYear(startYear + "-" + endYear);
					 financialYearMst.setCompanyMst(companyMst);
					 financialYearMst.setUserName("System");
					 financialYearMst = financialyearmaster.save(financialYearMst);
					 
					 
					 // Before returning, check if current date from system falls in new financial year
					 
				 }
			 }
	
		
		 
		
		
	}
	
	
	if(null==SystemSetting.getFinancialYear() || SystemSetting.getFinancialYear().equalsIgnoreCase("")) {
		SystemSetting.setFinancialYear(financialYearMst);
		ClientSystemSetting.setFinancialYear(financialYearMst);
	}
	
	 
	
	return financialYearMst;
}


@GetMapping("/{companymstid}/financialyearmst/getfinanicialyearcount")
public Integer retrieveFinancialYearMstCount(
		@PathVariable(value = "companymstid") String companymstid
		)
{
	return financialyearmaster.getCurrentFinancialYearCount();
}




@GetMapping("/{companymstid}/financialyearmst/cheackfinancialyearduplication")
public List<FinancialYearMst> cheackFinancialYearDuplication(
		@PathVariable(value = "companymstid") String companymstid,@RequestParam("startdate") String startDate,@RequestParam("enddate") String endDate
		)
{
	java.sql.Date sdate = SystemSetting.StringToSqlDate(startDate, "yyyy-MM-dd");
	java.sql.Date edate = SystemSetting.StringToSqlDate(endDate, "yyyy-MM-dd");
	
	return financialyearmaster.findByStartDateAndEndDate(sdate,edate);
}

@PutMapping("/{companymstid}/financialyearmst/updateopenigclosingstatus")
public void updateOpeningClosingStatus(
		@PathVariable(value = "companymstid") String companymstid,
		@RequestBody FinancialYearMst financialYrmstreq)
	{
		financialyearmaster.updateFinancialYearMst();
		FinancialYearMst financialYrMst = financialyearmaster.findById(financialYrmstreq.getId()).get();
		financialYrMst.setOpenCloseStatus(financialYrmstreq.getOpenCloseStatus());
		financialYrMst.setCurrentFinancialYear(1);
		financialyearmaster.save(financialYrMst);
	}
}




