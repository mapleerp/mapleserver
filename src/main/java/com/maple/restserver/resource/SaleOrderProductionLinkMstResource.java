package com.maple.restserver.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.BankMst;
import com.maple.restserver.entity.SaleOrderProductionLinkMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SaleOrderProductionLinkMstRepository;

@RestController
public class SaleOrderProductionLinkMstResource {

	@Autowired
	SaleOrderProductionLinkMstRepository 
	saleOrderProductionLinkMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	

	@PostMapping("/{companymstid}/saleorderproductionlinkmstresource/createsaleorderproductionlinkmst")
	public SaleOrderProductionLinkMst createSaleOrderProductionLinkMst(@PathVariable(value = "companymstid") String
			 companymstid,@Valid @RequestBody SaleOrderProductionLinkMst  saleOrderProductionLinkMst )
	{
		return companyMstRepository.findById(companymstid).map(companyMst-> {
			saleOrderProductionLinkMst.setCompanyMst(companyMst);
	return saleOrderProductionLinkMstRepository.saveAndFlush(saleOrderProductionLinkMst);
		}).orElseThrow(() -> new ResourceNotFoundException("companymstid " +
				  companymstid + " not found")); }
	
	
	

	@GetMapping("{companymstid}/saleorderproductionlinkmstresource/fetchallsaleorderproductionlinkmst")
	public List<SaleOrderProductionLinkMst> retrieveAllSaleOrderProductionLinkMst(@PathVariable(value = "companymstid") String
			 companymstid)
	{
		return saleOrderProductionLinkMstRepository.findAll();
	}
	
	
}
