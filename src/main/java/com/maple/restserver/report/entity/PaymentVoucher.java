package com.maple.restserver.report.entity;

import java.util.Date;

public class PaymentVoucher {
	
	String id;
	String branchName;
	String voucherNO;
	Date voucherDate;
	String remark;
	Double amount;
	String companyName;
	String accountName;
	String memberId;
	Double totalAmount;
	String instrumentNumber;
	Date instrumentDate;
	String modeOfPayment;
	String branchEmail;
	String branchWebsite;
	String familyName;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getVoucherNO() {
		return voucherNO;
	}
	public void setVoucherNO(String voucherNO) {
		this.voucherNO = voucherNO;
	}
	
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getInstrumentNumber() {
		return instrumentNumber;
	}
	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}
	public Date getInstrumentDate() {
		return instrumentDate;
	}
	public void setInstrumentDate(java.util.Date instrumentDate) {
		this.instrumentDate = (Date) instrumentDate;
	}
	public String getModeOfPayment() {
		return modeOfPayment;
	}
	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	@Override
	public String toString() {
		return "PaymentVoucher [branchName=" + branchName + ", voucherNO=" + voucherNO + ", voucherDate=" + voucherDate
				+ ", remark=" + remark + ", amount=" + amount + ", companyName=" + companyName + ", accountName="
				+ accountName + ", memberId=" + memberId + ", totalAmount=" + totalAmount + ", instrumentNumber="
				+ instrumentNumber + ", instrumentDate=" + instrumentDate + ", modeOfPayment=" + modeOfPayment
				+ ", branchEmail=" + branchEmail + ", branchWebsite=" + branchWebsite + "]";
	}
	
	
	

}
