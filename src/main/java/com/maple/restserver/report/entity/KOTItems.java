package com.maple.restserver.report.entity;

import java.util.ArrayList;

import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;


public class KOTItems {
	String id;
	String tables;
	String waiter;
	ArrayList<SalesDtl> arrayofsalesdtl;
	SalesTransHdr salesTransHdr;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTables() {
		return tables;
	}
	public void setTables(String tables) {
		this.tables = tables;
	}
	public String getWaiter() {
		return waiter;
	}
	public void setWaiter(String waiter) {
		this.waiter = waiter;
	}
	public ArrayList<SalesDtl> getArrayofsalesdtl() {
		return arrayofsalesdtl;
	}
	public void setArrayofsalesdtl(ArrayList<SalesDtl> arrayofsalesdtl) {
		this.arrayofsalesdtl = arrayofsalesdtl;
	}
	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}
	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}
	@Override
	public String toString() {
		return "KOTItems [id=" + id + ", tables=" + tables + ", waiter=" + waiter + ", arrayofsalesdtl="
				+ arrayofsalesdtl + ", salesTransHdr=" + salesTransHdr + "]";
	}
	
	
}
