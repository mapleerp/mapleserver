package com.maple.restserver.report.entity;

import java.sql.Date;

import org.springframework.stereotype.Component;

@Component
public class ConsumptionReport {

	String itemName;
	String reason;
	Double qty;
	String categoryName;
	String unit;
	Double amount;
	
	Double mrp;
	

	String voucherNumber;
	String date;
	String department;
	String userName;
	
	String batch;
	String expiry;
	Double rate;
	
	
	java.util.Date voucherDate;
	
	public java.util.Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(java.util.Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	
	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}



	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	@Override
	public String toString() {
		return "ConsumptionReport [itemName=" + itemName + ", reason=" + reason + ", qty=" + qty + ", categoryName="
				+ categoryName + ", unit=" + unit + ", amount=" + amount + ", mrp=" + mrp + ", voucherNumber="
				+ voucherNumber + ", date=" + date + ", department=" + department + ", userName=" + userName
				+ ", batch=" + batch + ", expiry=" + expiry + ", rate=" + rate + ", voucherDate=" + voucherDate + "]";
	}


	
	
	
}
