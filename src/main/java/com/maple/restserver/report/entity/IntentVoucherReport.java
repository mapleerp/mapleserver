package com.maple.restserver.report.entity;

import java.io.Serializable;
import java.util.Date;

public class IntentVoucherReport implements Serializable {
   private String id;
	String companyName;
	String companyaddress1;
	String companyaddress2;
	String voucherNumber;
	Date voucherDate;
	String itemId;
    String UnitName;
    Double Qty;
    Double rate;
    Date intentDate;
	String toBranch;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyaddress1() {
		return companyaddress1;
	}
	public void setCompanyaddress1(String companyaddress1) {
		this.companyaddress1 = companyaddress1;
	}
	public String getCompanyaddress2() {
		return companyaddress2;
	}
	public void setCompanyaddress2(String companyaddress2) {
		this.companyaddress2 = companyaddress2;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUnitName() {
		return UnitName;
	}
	public void setUnitName(String unitName) {
		UnitName = unitName;
	}
	public Double getQty() {
		return Qty;
	}
	public void setQty(Double qty) {
		Qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Date getIntentDate() {
		return intentDate;
	}
	public void setIntentDate(Date intentDate) {
		this.intentDate = intentDate;
	}
	public String getToBranch() {
		return toBranch;
	}
	public void setToBranch(String toBranch) {
		this.toBranch = toBranch;
	}

	
}
