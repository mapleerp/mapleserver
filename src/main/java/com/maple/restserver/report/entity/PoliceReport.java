package com.maple.restserver.report.entity;

import org.springframework.stereotype.Component;

@Component
public class PoliceReport {
	
	
	String id;
	String invoiceDate;
	String voucherNumber;
	String customerName;
	String mpsType;
	String patientID;
	private Double cashPaid;
	private Double cardPaid;
	private Double insuranceAmount;
	private Double invoiceAmount;
	private Double creditAmount;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getMpsType() {
		return mpsType;
	}
	public void setMpsType(String mpsType) {
		this.mpsType = mpsType;
	}
	public String getPatientID() {
		return patientID;
	}
	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}
	public Double getCashPaid() {
		return cashPaid;
	}
	public void setCashPaid(Double cashPaid) {
		this.cashPaid = cashPaid;
	}
	public Double getCardPaid() {
		return cardPaid;
	}
	public void setCardPaid(Double cardPaid) {
		this.cardPaid = cardPaid;
	}
	public Double getInsuranceAmount() {
		return insuranceAmount;
	}
	public void setInsuranceAmount(Double insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	@Override
	public String toString() {
		return "PoliceReport [id=" + id + ", invoiceDate=" + invoiceDate + ", voucherNumber=" + voucherNumber
				+ ", customerName=" + customerName + ", mpsType=" + mpsType + ", patientID=" + patientID + ", cashPaid="
				+ cashPaid + ", cardPaid=" + cardPaid + ", insuranceAmount=" + insuranceAmount + ", invoiceAmount="
				+ invoiceAmount + ", creditAmount=" + creditAmount + "]";
	}
	
	
	
}
