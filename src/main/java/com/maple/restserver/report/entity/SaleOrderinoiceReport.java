package com.maple.restserver.report.entity;

import java.util.Date;

public class SaleOrderinoiceReport {
	String companyName;
	String companyAddress;
	String companyPhoneNo;
	String companyPlace;
	String companyGst;
	String orderNo;
	Date orderDate;
	String orderBy;
	String orderDueTime;
	String orderTimeMode;

	String deliveryMode;
	String customerName;
	String customerAddress;
	String customerPhoneNo;
	String customerPhoneNo2;

	Double totalSale;
	Integer slNo;
	String itemName;
	Double rate;
	Double qty;
	Double gst;
	String unitName;
	Double amount;
	Double total;
	String localCustomer;
	String orderTakerName;
	String branchWebsite;
	String branchEmail;
	String gstCustomerName;
	
	String orderMsg;

	String branchName;
	
	
	Double taxRate;
	String batch;
	String itemCode;
	String expiryDate;
	String voucherDate;
	String voucherNumber;
	
	
	
	public String getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public Double getTotalSale() {
		return totalSale;
	}
	public void setTotalSale(Double totalSale) {
		this.totalSale = totalSale;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public String getCompanyPhoneNo() {
		return companyPhoneNo;
	}
	public void setCompanyPhoneNo(String companyPhoneNo) {
		this.companyPhoneNo = companyPhoneNo;
	}
	public String getCompanyPlace() {
		return companyPlace;
	}
	public void setCompanyPlace(String companyPlace) {
		this.companyPlace = companyPlace;
	}
	public String getCompanyGst() {
		return companyGst;
	}
	public void setCompanyGst(String companyGst) {
		this.companyGst = companyGst;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getDeliveryMode() {
		return deliveryMode;
	}
	public void setDeliveryMode(String deliveryMode) {
		this.deliveryMode = deliveryMode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCustomerPhoneNo() {
		return customerPhoneNo;
	}
	public void setCustomerPhoneNo(String customerPhoneNo) {
		this.customerPhoneNo = customerPhoneNo;
	}
	public Integer getSlNo() {
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getGst() {
		return gst;
	}
	public void setGst(Double gst) {
		this.gst = gst;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public String getLocalCustomer() {
		return localCustomer;
	}
	public void setLocalCustomer(String localCustomer) {
		this.localCustomer = localCustomer;
	}
	public String getOrderDueTime() {
		return orderDueTime;
	}
	public void setOrderDueTime(String orderDueTime) {
		this.orderDueTime = orderDueTime;
	}
	public String getOrderTimeMode() {
		return orderTimeMode;
	}
	public void setOrderTimeMode(String orderTimeMode) {
		this.orderTimeMode = orderTimeMode;
	}
	public String getOrderTakerName() {
		return orderTakerName;
	}
	public void setOrderTakerName(String orderTakerName) {
		this.orderTakerName = orderTakerName;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getGstCustomerName() {
		return gstCustomerName;
	}
	public void setGstCustomerName(String gstCustomerName) {
		this.gstCustomerName = gstCustomerName;
	}
	public String getOrderMsg() {
		return orderMsg;
	}
	public void setOrderMsg(String orderMsg) {
		this.orderMsg = orderMsg;
	}
	
	
	public String getCustomerPhoneNo2() {
		return customerPhoneNo2;
	}
	public void setCustomerPhoneNo2(String customerPhoneNo2) {
		this.customerPhoneNo2 = customerPhoneNo2;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	@Override
	public String toString() {
		return "SaleOrderinoiceReport [companyName=" + companyName + ", companyAddress=" + companyAddress
				+ ", companyPhoneNo=" + companyPhoneNo + ", companyPlace=" + companyPlace + ", companyGst=" + companyGst
				+ ", orderNo=" + orderNo + ", orderDate=" + orderDate + ", orderBy=" + orderBy + ", orderDueTime="
				+ orderDueTime + ", orderTimeMode=" + orderTimeMode + ", deliveryMode=" + deliveryMode
				+ ", customerName=" + customerName + ", customerAddress=" + customerAddress + ", customerPhoneNo="
				+ customerPhoneNo + ", customerPhoneNo2=" + customerPhoneNo2 + ", totalSale=" + totalSale + ", slNo="
				+ slNo + ", itemName=" + itemName + ", rate=" + rate + ", qty=" + qty + ", gst=" + gst + ", unitName="
				+ unitName + ", amount=" + amount + ", total=" + total + ", localCustomer=" + localCustomer
				+ ", orderTakerName=" + orderTakerName + ", branchWebsite=" + branchWebsite + ", branchEmail="
				+ branchEmail + ", gstCustomerName=" + gstCustomerName + ", orderMsg=" + orderMsg + ", branchName="
				+ branchName + ", taxRate=" + taxRate + ", batch=" + batch + ", itemCode=" + itemCode + "]";
	}
	
	

	
	
}
