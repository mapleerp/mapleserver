package com.maple.restserver.report.entity;

import java.util.Date;

public class SalesOrderAdvanceBalanceReport {
	

	 String companyName;
	 Date voucherDate;
	 String VoucherNumber;
	 String branchAddress1;
	 String branchAddress2;
	 String branchEmail;
	 String branchGst;
	 String branchPhNo;	 
	Date currentDate;
	String orderNo;
	String invoiceNo;
	String customerName;
	Double orderInvoiceTotal;
	Double advance;
	Double Cash;
	Double credit;
	Double creditCard;
	Double debitCard;
	Double online;
	Double paytm;
	Double sodexo;
	Double balance;
	Double netTotal;
	String branchName;

	String receiptMode;
	
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Double getOrderInvoiceTotal() {
		return orderInvoiceTotal;
	}
	public void setOrderInvoiceTotal(Double orderInvoiceTotal) {
		this.orderInvoiceTotal = orderInvoiceTotal;
	}
	public Double getAdvance() {
		return advance;
	}
	public void setAdvance(Double advance) {
		this.advance = advance;
	}
	public Double getCash() {
		return Cash;
	}
	public void setCash(Double cash) {
		Cash = cash;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	public Double getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(Double creditCard) {
		this.creditCard = creditCard;
	}
	public Double getDebitCard() {
		return debitCard;
	}
	public void setDebitCard(Double debitCard) {
		this.debitCard = debitCard;
	}
	public Double getOnline() {
		return online;
	}
	public void setOnline(Double online) {
		this.online = online;
	}
	public Double getPaytm() {
		return paytm;
	}
	public void setPaytm(Double paytm) {
		this.paytm = paytm;
	}
	public Double getSodexo() {
		return sodexo;
	}
	public void setSodexo(Double sodexo) {
		this.sodexo = sodexo;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public Double getNetTotal() {
		return netTotal;
	}
	public void setNetTotal(Double netTotal) {
		this.netTotal = netTotal;
	}
	
	
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchAddress1() {
		return branchAddress1;
	}
	public void setBranchAddress1(String branchAddress1) {
		this.branchAddress1 = branchAddress1;
	}
	public String getBranchAddress2() {
		return branchAddress2;
	}
	public void setBranchAddress2(String branchAddress2) {
		this.branchAddress2 = branchAddress2;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchGst() {
		return branchGst;
	}
	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}
	public String getBranchPhNo() {
		return branchPhNo;
	}
	public void setBranchPhNo(String branchPhNo) {
		this.branchPhNo = branchPhNo;
	}
	public Date getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return VoucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		VoucherNumber = voucherNumber;
	}
	@Override
	public String toString() {
		return "SalesOrderAdvanceBalanceReport [companyName=" + companyName + ", voucherDate=" + voucherDate
				+ ", VoucherNumber=" + VoucherNumber + ", branchAddress1=" + branchAddress1 + ", branchAddress2="
				+ branchAddress2 + ", branchEmail=" + branchEmail + ", branchGst=" + branchGst + ", branchPhNo="
				+ branchPhNo + ", currentDate=" + currentDate + ", orderNo=" + orderNo + ", invoiceNo=" + invoiceNo
				+ ", customerName=" + customerName + ", orderInvoiceTotal=" + orderInvoiceTotal + ", advance=" + advance
				+ ", Cash=" + Cash + ", credit=" + credit + ", creditCard=" + creditCard + ", debitCard=" + debitCard
				+ ", online=" + online + ", paytm=" + paytm + ", sodexo=" + sodexo + ", balance=" + balance
				+ ", netTotal=" + netTotal + ", branchName=" + branchName + "]";
	}
	

	

}
