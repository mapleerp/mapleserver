package com.maple.restserver.report.entity;

public class ProcessPermissionWebEntity {
	
	String processName;
	String userName;
	
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	

}
