package com.maple.restserver.report.entity;

import java.util.Date;

public class GSTOutputDetailReport{
	
	String id;
	String invoiceDate;
	String invoiceNumber;
	String customerName;
	String customerTin;
	String branch;
	String branchActivity;
	Double totalSalesExcludingGst;
	//Double totalSalesExcludingGst
	Double gstAmount;
	Double excemptedSales;
	Double totalSales;
	String gstPercent;
	
	
	
	
	
	
	public String getGstPercent() {
		return gstPercent;
	}
	public void setGstPercent(String gstPercent) {
		this.gstPercent = gstPercent;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerTin() {
		return customerTin;
	}
	public void setCustomerTin(String customerTin) {
		this.customerTin = customerTin;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getBranchActivity() {
		return branchActivity;
	}
	public void setBranchActivity(String branchActivity) {
		this.branchActivity = branchActivity;
	}
	public Double getTotalSalesExcludingGst() {
		return totalSalesExcludingGst;
	}
	public void setTotalSalesExcludingGst(Double totalSalesExcludingGst) {
		this.totalSalesExcludingGst = totalSalesExcludingGst;
	}
	public Double getGstAmount() {
		return gstAmount;
	}
	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}
	public Double getExcemptedSales() {
		return excemptedSales;
	}
	public void setExcemptedSales(Double excemptedSales) {
		this.excemptedSales = excemptedSales;
	}
	public Double getTotalSales() {
		return totalSales;
	}
	public void setTotalSales(Double totalSales) {
		this.totalSales = totalSales;
	}
	

}
