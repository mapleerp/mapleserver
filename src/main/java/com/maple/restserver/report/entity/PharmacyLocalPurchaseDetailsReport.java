package com.maple.restserver.report.entity;

import java.util.Date;

public class PharmacyLocalPurchaseDetailsReport {

	private Date transEntryDate;
	private Date invoiceDate;
	private String voucherNumber;
	private String supplierInvoice;
	private String supplier;
	private String description;
	private String itemCode;
	private String group;
	private String batch;
	private Date expiryDate;
	private String poNumber;
	private Double qty;
	private Double purchaseRate;
	private Double gst;
	private Double amount;
	private Double netCost;
	private String user;
	public Date getTransEntryDate() {
		return transEntryDate;
	}
	public void setTransEntryDate(Date transEntryDate) {
		this.transEntryDate = transEntryDate;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getSupplierInvoice() {
		return supplierInvoice;
	}
	public void setSupplierInvoice(String supplierInvoice) {
		this.supplierInvoice = supplierInvoice;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public Double getPurchaseRate() {
		return purchaseRate;
	}
	public void setPurchaseRate(Double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}
	public Double getGst() {
		return gst;
	}
	public void setGst(Double gst) {
		this.gst = gst;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getNetCost() {
		return netCost;
	}
	public void setNetCost(Double netCost) {
		this.netCost = netCost;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	@Override
	public String toString() {
		return "PharmacyLocalPurchaseDetailsReport [transEntryDate=" + transEntryDate + ", invoiceDate=" + invoiceDate
				+ ", voucherNumber=" + voucherNumber + ", supplierInvoice=" + supplierInvoice + ", supplier=" + supplier
				+ ", description=" + description + ", itemCode=" + itemCode + ", group=" + group + ", batch=" + batch
				+ ", expiryDate=" + expiryDate + ", poNumber=" + poNumber + ", qty=" + qty + ", purchaseRate="
				+ purchaseRate + ", gst=" + gst + ", amount=" + amount + ", netCost=" + netCost + ", user=" + user
				+ "]";
	}


}
