package com.maple.restserver.report.entity;

import java.util.Date;
import java.util.List;

 
public class SalesInvoiceReport {

	String id;
	String branchName;
	String branchAddress1;
	String branchAddress2;
	String branchPlace;
	String branchTelNo;
	String branchGst;
	String branchState;
	String branchEmail;
	String bankName;
	String accountNumber;
	String bankBranch;
	String ifsc;
	Date voucherDate;
	String voucherNumber;
	String customerName;
	
	

	String itemName;
	String hsnCode;
	Double rate;
	Double mrp;
	Double qty;
	String unitName;
	Double discount;
	Double amount;
	
	Double taxRate;
	Double sgstRate;
	Double cgstRate;
	Double sgstAmount;
	Double cgstAmount;
	Double igstRate;
	Double igstAmount;
	Double cessRate;
	Double cessAmount;
	
	

	String companyName;
	String customerPlace;
	String customerState;
	String customerPhone;
	String customerGst;
	String addressLine1;
	String addressLine2;
	String companyGst;
	
	String branchWebsite;
	
	String localCustomerName;
	String localCustomerPlace;
	String localCustomerState;
	String localCustomerPhone;
	String localCustomerAddres;
	String localCustomerPhone2;
	String customerAddress;
	Double taxableAmount;
	
	Double invoiceDiscount;
	String discountPercent;
	String salesMode;
	String batch;
	Double standrdPrice;
	
	String warranty;
	
	Double listPrice;
	String itemCode;
	Date expiryDate;


 
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getSalesMode() {
		return salesMode;
	}
	public void setSalesMode(String salesMode) {
		this.salesMode = salesMode;
	}
	public Double getCessRate() {
		return cessRate;
	}
	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}
	public Double getCessAmount() {
		return cessAmount;
	}
	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}
	public Double getInvoiceDiscount() {
		return invoiceDiscount;
	}
	public void setInvoiceDiscount(Double invoiceDiscount) {
		this.invoiceDiscount = invoiceDiscount;
	}
	public String getDiscountPercent() {
		return discountPercent;
	}
	public void setDiscountPercent(String discountPercent) {
		this.discountPercent = discountPercent;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchAddress1() {
		return branchAddress1;
	}
	public void setBranchAddress1(String branchAddress1) {
		this.branchAddress1 = branchAddress1;
	}
	public String getBranchAddress2() {
		return branchAddress2;
	}
	public void setBranchAddress2(String branchAddress2) {
		this.branchAddress2 = branchAddress2;
	}
	public String getBranchPlace() {
		return branchPlace;
	}
	public void setBranchPlace(String branchPlace) {
		this.branchPlace = branchPlace;
	}
	public String getBranchTelNo() {
		return branchTelNo;
	}
	public void setBranchTelNo(String branchTelNo) {
		this.branchTelNo = branchTelNo;
	}
	public String getBranchGst() {
		return branchGst;
	}
	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}
	public String getBranchState() {
		return branchState;
	}
	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getBankBranch() {
		return bankBranch;
	}
	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}
	public String getIfsc() {
		return ifsc;
	}
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerPlace() {
		return customerPlace;
	}
	public void setCustomerPlace(String customerPlace) {
		this.customerPlace = customerPlace;
	}
	public String getCustomerState() {
		return customerState;
	}
	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}
	public String getCustomerGst() {
		return customerGst;
	}
	public void setCustomerGst(String customerGst) {
		this.customerGst = customerGst;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getSgstRate() {
		return sgstRate;
	}
	public void setSgstRate(Double sgstRate) {
		this.sgstRate = sgstRate;
	}
	public Double getCgstRate() {
		return cgstRate;
	}
	public void setCgstRate(Double cgstRate) {
		this.cgstRate = cgstRate;
	}
	public Double getSgstAmount() {
		return sgstAmount;
	}
	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}
	public Double getCgstAmount() {
		return cgstAmount;
	}
	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}
	public Double getIgstRate() {
		return igstRate;
	}
	public void setIgstRate(Double igstRate) {
		this.igstRate = igstRate;
	}
	public Double getIgstAmount() {
		return igstAmount;
	}
	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCompanyGst() {
		return companyGst;
	}
	public void setCompanyGst(String companyGst) {
		this.companyGst = companyGst;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getLocalCustomerName() {
		return localCustomerName;
	}
	public void setLocalCustomerName(String localCustomerName) {
		this.localCustomerName = localCustomerName;
	}
	public String getLocalCustomerPlace() {
		return localCustomerPlace;
	}
	public void setLocalCustomerPlace(String localCustomerPlace) {
		this.localCustomerPlace = localCustomerPlace;
	}
	public String getLocalCustomerState() {
		return localCustomerState;
	}
	public void setLocalCustomerState(String localCustomerState) {
		this.localCustomerState = localCustomerState;
	}
	public String getLocalCustomerPhone() {
		return localCustomerPhone;
	}
	public void setLocalCustomerPhone(String localCustomerPhone) {
		this.localCustomerPhone = localCustomerPhone;
	}
	
	
	public String getLocalCustomerAddres() {
		return localCustomerAddres;
	}
	public void setLocalCustomerAddres(String localCustomerAddres) {
		this.localCustomerAddres = localCustomerAddres;
	}
	public String getLocalCustomerPhone2() {
		return localCustomerPhone2;
	}
	public void setLocalCustomerPhone2(String localCustomerPhone2) {
		this.localCustomerPhone2 = localCustomerPhone2;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public Double getTaxableAmount() {
		return taxableAmount;
	}
	public void setTaxableAmount(Double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getStandrdPrice() {
		return standrdPrice;
	}
	public void setStandrdPrice(Double standrdPrice) {
		this.standrdPrice = standrdPrice;
	}
	public String getWarranty() {
		return warranty;
	}
	public void setWarranty(String warranty) {
		this.warranty = warranty;
	}
	

	public Double getListPrice() {
		return listPrice;
	}
	public void setListPrice(Double listPrice) {
		this.listPrice = listPrice;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	@Override
	public String toString() {
		return "SalesInvoiceReport [id=" + id + ", branchName=" + branchName + ", branchAddress1=" + branchAddress1
				+ ", branchAddress2=" + branchAddress2 + ", branchPlace=" + branchPlace + ", branchTelNo=" + branchTelNo
				+ ", branchGst=" + branchGst + ", branchState=" + branchState + ", branchEmail=" + branchEmail
				+ ", bankName=" + bankName + ", accountNumber=" + accountNumber + ", bankBranch=" + bankBranch
				+ ", ifsc=" + ifsc + ", voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber
				+ ", customerName=" + customerName + ", itemName=" + itemName + ", hsnCode=" + hsnCode + ", rate="
				+ rate + ", mrp=" + mrp + ", qty=" + qty + ", unitName=" + unitName + ", discount=" + discount
				+ ", amount=" + amount + ", taxRate=" + taxRate + ", sgstRate=" + sgstRate + ", cgstRate=" + cgstRate
				+ ", sgstAmount=" + sgstAmount + ", cgstAmount=" + cgstAmount + ", igstRate=" + igstRate
				+ ", igstAmount=" + igstAmount + ", cessRate=" + cessRate + ", cessAmount=" + cessAmount
				+ ", companyName=" + companyName + ", customerPlace=" + customerPlace + ", customerState="
				+ customerState + ", customerPhone=" + customerPhone + ", customerGst=" + customerGst
				+ ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", companyGst=" + companyGst
				+ ", branchWebsite=" + branchWebsite + ", localCustomerName=" + localCustomerName
				+ ", localCustomerPlace=" + localCustomerPlace + ", localCustomerState=" + localCustomerState
				+ ", localCustomerPhone=" + localCustomerPhone + ", localCustomerAddres=" + localCustomerAddres
				+ ", localCustomerPhone2=" + localCustomerPhone2 + ", customerAddress=" + customerAddress
				+ ", taxableAmount=" + taxableAmount + ", invoiceDiscount=" + invoiceDiscount + ", discountPercent="
				+ discountPercent + ", salesMode=" + salesMode + ", batch=" + batch + ", standrdPrice=" + standrdPrice
				+ ", warranty=" + warranty + ", listPrice=" + listPrice + ", itemCode=" + itemCode + ", expiryDate="
				+ expiryDate + "]";
	}
	
	
	
	

}
