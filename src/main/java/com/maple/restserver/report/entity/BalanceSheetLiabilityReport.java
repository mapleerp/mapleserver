package com.maple.restserver.report.entity;

import java.util.Date;

public class BalanceSheetLiabilityReport {

	
	String branchName;
	String accountId;
	String serialNumber;
	String accountName;
	
	Date endDate;
	
	Double amount;
	
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "BalanceSheetLiabilityReport [branchName=" + branchName + ", accountId=" + accountId + ", serialNumber="
				+ serialNumber + ", accountName=" + accountName + ", endDate=" + endDate + ", amount=" + amount + "]";
	}
	
	
	
	
	
}
