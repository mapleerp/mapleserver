package com.maple.restserver.report.entity;

import java.util.Date;

public class SettledAmountDtlReport {
	String voucherNumber;
	Date voucherDate;
	Double amount;
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "SettledAmountDtlReport [voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate + ", amount="
				+ amount + "]";
	}
	
}
