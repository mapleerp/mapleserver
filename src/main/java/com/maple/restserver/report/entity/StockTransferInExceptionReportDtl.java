package com.maple.restserver.report.entity;

public class StockTransferInExceptionReportDtl {
	
	String itemName;
	String qty;
	String batch;
	String rate;
	String itemCode;
	String barcode;
	String reason;
	String unit;
	String amount;
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "StockTransferInExceptionReportDtl [itemName=" + itemName + ", qty=" + qty + ", batch=" + batch
				+ ", rate=" + rate + ", itemCode=" + itemCode + ", barcode=" + barcode + ", reason=" + reason
				+ ", unit=" + unit + ", amount=" + amount + "]";
	}
	
	
	
	
}
