package com.maple.restserver.report.entity;

import java.util.Date;

public class SaleModeWiseSummaryReport {

	
	
	private String id;
	private String paymentMode;
	private Double amount;
	private Double totalAmount;
	private Date date;
	private Double advanceAmount;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Double getAdvanceAmount() {
		return advanceAmount;
	}
	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}
	@Override
	public String toString() {
		return "SaleModeWiseSummaryReport [id=" + id + ", paymentMode=" + paymentMode + ", amount=" + amount
				+ ", totalAmount=" + totalAmount + ", date=" + date + ", advanceAmount=" + advanceAmount + "]";
	}
	 
	 
}
