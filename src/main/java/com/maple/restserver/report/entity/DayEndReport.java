package com.maple.restserver.report.entity;

public class DayEndReport {

	
	private String openingBillNo;
	private String closingBillNo;
	private String totalBillNo;
	private Double cashSales;
	private Double cardSales;
	private Double payment;
	private Double receipt;
	private Double B2BSales;
	public String getOpeningBillNo() {
		return openingBillNo;
	}
	public void setOpeningBillNo(String openingBillNo) {
		this.openingBillNo = openingBillNo;
	}
	public String getClosingBillNo() {
		return closingBillNo;
	}
	public void setClosingBillNo(String closingBillNo) {
		this.closingBillNo = closingBillNo;
	}
	public String getTotalBillNo() {
		return totalBillNo;
	}
	public void setTotalBillNo(String totalBillNo) {
		this.totalBillNo = totalBillNo;
	}
	public Double getCashSales() {
		return cashSales;
	}
	public void setCashSales(Double cashSales) {
		this.cashSales = cashSales;
	}
	public Double getCardSales() {
		return cardSales;
	}
	public void setCardSales(Double cardSales) {
		this.cardSales = cardSales;
	}
	public Double getPayment() {
		return payment;
	}
	public void setPayment(Double payment) {
		this.payment = payment;
	}
	public Double getReceipt() {
		return receipt;
	}
	public void setReceipt(Double receipt) {
		this.receipt = receipt;
	}
	public Double getB2BSales() {
		return B2BSales;
	}
	public void setB2BSales(Double b2bSales) {
		B2BSales = b2bSales;
	}
	@Override
	public String toString() {
		return "DayEndReport [openingBillNo=" + openingBillNo + ", closingBillNo=" + closingBillNo + ", totalBillNo="
				+ totalBillNo + ", cashSales=" + cashSales + ", cardSales=" + cardSales + ", payment=" + payment
				+ ", receipt=" + receipt + ", B2BSales=" + B2BSales + "]";
	}
	
	
}
