package com.maple.restserver.report.entity;

import java.util.Date;

public class CategoryWiseStockMovement {
	
	String category;
	Double openingStock;
	Double inWardQty;
	Double outWardQty;
	Double closingStock;
	Date voucherDate;
	
	
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Double getOpeningStock() {
		return openingStock;
	}
	public void setOpeningStock(Double openingStock) {
		this.openingStock = openingStock;
	}
	public Double getInWardQty() {
		return inWardQty;
	}
	public void setInWardQty(Double inWardQty) {
		this.inWardQty = inWardQty;
	}
	public Double getOutWardQty() {
		return outWardQty;
	}
	public void setOutWardQty(Double outWardQty) {
		this.outWardQty = outWardQty;
	}
	public Double getClosingStock() {
		return closingStock;
	}
	public void setClosingStock(Double closingStock) {
		this.closingStock = closingStock;
	}
	@Override
	public String toString() {
		return "CategoryWiseStockMovement [category=" + category + ", openingStock=" + openingStock + ", inWardQty="
				+ inWardQty + ", outWardQty=" + outWardQty + ", closingStock=" + closingStock + "]";
	}
	
	

}
