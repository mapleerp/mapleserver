package com.maple.restserver.report.entity;

public class StockMigrationReport {
	

	String id;
	String itemId;
	
	
	String itemName;
	String barCode;
	String batch;
	Double qtyIn;
	Double qtyOut;
	String particulars;
	
	Double rate;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getQtyIn() {
		return qtyIn;
	}
	public void setQtyIn(Double qtyIn) {
		this.qtyIn = qtyIn;
	}
	public Double getQtyOut() {
		return qtyOut;
	}
	public void setQtyOut(Double qtyOut) {
		this.qtyOut = qtyOut;
	}
	public String getParticulars() {
		return particulars;
	}
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	@Override
	public String toString() {
		return "StockMigrationReport [id=" + id + ", itemId=" + itemId + ", itemName=" + itemName + ", barCode="
				+ barCode + ", batch=" + batch + ", qtyIn=" + qtyIn + ", qtyOut=" + qtyOut + ", particulars="
				+ particulars + ", rate=" + rate + "]";
	}
	

}
