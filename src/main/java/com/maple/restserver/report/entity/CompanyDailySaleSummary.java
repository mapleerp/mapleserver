package com.maple.restserver.report.entity;

public class CompanyDailySaleSummary {

	String branchName;
	Double total;
	Double card;
	Double card2;
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getCard() {
		return card;
	}
	public void setCard(Double card) {
		this.card = card;
	}
	public Double getCard2() {
		return card2;
	}
	public void setCard2(Double card2) {
		this.card2 = card2;
	}

	
	
}
