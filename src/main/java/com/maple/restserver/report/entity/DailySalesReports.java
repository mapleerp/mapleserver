package com.maple.restserver.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;



public class DailySalesReports {

	
	
	String voucherNumber;
	
	String customerName;
	
	Double amount;
	
	String itemName;
	
	Double qty;
	
	Double taxRate;
	
	Double mrp;
	
	Double cessRate;
	
	Double unit;

	private String voucherNumberProperty;
	
	
	private String customerNameProperty;
	

	private Double amountProperty;


	private String itemNameProperty;
	

	private Double taxRateProperty;
	

	private Double qtyProperty;

	private Double mrpProperty;

	private Double cessRateProperty;

	private Double unitProperty;



	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getCessRate() {
		return cessRate;
	}
	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}
	public Double getUnit() {
		return unit;
	}
	public void setUnit(Double unit) {
		this.unit = unit;
	}
	@Override
	public String toString() {
		return "DailySalesReports [voucherNumber=" + voucherNumber + ", customerName=" + customerName + ", amount="
				+ amount + ", itemName=" + itemName + ", qty=" + qty + ", taxRate=" + taxRate + ", mrp=" + mrp
				+ ", cessRate=" + cessRate + ", unit=" + unit + "]";
	}
	public String getVoucherNumberProperty() {
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(String voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	public String getCustomerNameProperty() {
		return customerNameProperty;
	}
	public void setCustomerNameProperty(String customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}
	public Double getAmountProperty() {
		return amountProperty;
	}
	public void setAmountProperty(Double amountProperty) {
		this.amountProperty = amountProperty;
	}
	public String getItemNameProperty() {
		return itemNameProperty;
	}
	public void setItemNameProperty(String itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public Double getTaxRateProperty() {
		return taxRateProperty;
	}
	public void setTaxRateProperty(Double taxRateProperty) {
		this.taxRateProperty = taxRateProperty;
	}
	public Double getQtyProperty() {
		return qtyProperty;
	}
	public void setQtyProperty(Double qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	public Double getMrpProperty() {
		return mrpProperty;
	}
	public void setMrpProperty(Double mrpProperty) {
		this.mrpProperty = mrpProperty;
	}
	public Double getCessRateProperty() {
		return cessRateProperty;
	}
	public void setCessRateProperty(Double cessRateProperty) {
		this.cessRateProperty = cessRateProperty;
	}
	public Double getUnitProperty() {
		return unitProperty;
	}
	public void setUnitProperty(Double unitProperty) {
		this.unitProperty = unitProperty;
	}
	
	
}
