package com.maple.restserver.report.entity;

import java.util.Date;

public class DailyReceiptsSummaryReport {
	
	String receiptMode;
	Double totalCash;
	String debitAccount;
	

	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public Double getTotalCash() {
		return totalCash;
	}
	public void setTotalCash(Double totalCash) {
		this.totalCash = totalCash;
	}
	public String getDebitAccount() {
		return debitAccount;
	}
	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}
	
	
	

}
