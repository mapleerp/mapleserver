package com.maple.restserver.report.entity;

public class ItemNotInBatchPriceReport {

	String itemName;
	String unitName;
	String batch;
	

	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	@Override
	public String toString() {
		return "ItemNotInBatchPriceReport [itemName=" + itemName + ", unitName=" + unitName + ", batch=" + batch + "]";
	}
	

}
