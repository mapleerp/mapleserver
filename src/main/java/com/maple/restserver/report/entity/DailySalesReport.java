package com.maple.restserver.report.entity;

import java.io.Serializable;

public class DailySalesReport implements  Serializable {
	Double totalSales;
	Double cashPay;
	String branchCode;
	Double card1;
	Double card2;
	Double physicalCash;
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Double getCard1() {
		return card1;
	}
	public void setCard1(Double card1) {
		this.card1 = card1;
	}
	public Double getCard2() {
		return card2;
	}
	public void setCard2(Double card2) {
		this.card2 = card2;
	}
	public Double getPhysicalCash() {
		return physicalCash;
	}
	public void setPhysicalCash(Double physicalCash) {
		this.physicalCash = physicalCash;
	}
	public Double getTotalSales() {
		return totalSales;
	}
	public void setTotalSales(Double totalSales) {
		this.totalSales = totalSales;
	}
	public Double getCashPay() {
		return cashPay;
	}
	public void setCashPay(Double cashPay) {
		this.cashPay = cashPay;
	}
	
}
