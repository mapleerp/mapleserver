package com.maple.restserver.report.entity;

import java.util.Date;

public class IntentInReport {
	
	private String id;
	 String branchCode;
	 String inVoucherNumber;
	 String fromBranch;
	 String intentStatus;
	 Date inVoucherDate;
	 Double Qty;
	 String itemName;
	 String unitName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getInVoucherNumber() {
		return inVoucherNumber;
	}
	public void setInVoucherNumber(String inVoucherNumber) {
		this.inVoucherNumber = inVoucherNumber;
	}
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	public String getIntentStatus() {
		return intentStatus;
	}
	public void setIntentStatus(String intentStatus) {
		this.intentStatus = intentStatus;
	}
	public Date getInVoucherDate() {
		return inVoucherDate;
	}
	public void setInVoucherDate(Date inVoucherDate) {
		this.inVoucherDate = inVoucherDate;
	}
	public Double getQty() {
		return Qty;
	}
	public void setQty(Double qty) {
		Qty = qty;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	 
	 
}
