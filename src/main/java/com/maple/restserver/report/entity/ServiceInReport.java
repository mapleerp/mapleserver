package com.maple.restserver.report.entity;

import java.util.Date;

public class ServiceInReport {
	
	 Double qty;
	 String model;
	 String serialId;
	 String complaints;
	 String underWarranty;
	 String observation;
	 String itemName;
	 String brandName;
	 String productName;
	 String customer;
	 Date voucherDate;
	 
	 
	 
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getSerialId() {
		return serialId;
	}
	public void setSerialId(String serialId) {
		this.serialId = serialId;
	}
	public String getComplaints() {
		return complaints;
	}
	public void setComplaints(String complaints) {
		this.complaints = complaints;
	}
	public String getUnderWarranty() {
		return underWarranty;
	}
	public void setUnderWarranty(String underWarranty) {
		this.underWarranty = underWarranty;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	@Override
	public String toString() {
		return "ServiceInReport [qty=" + qty + ", model=" + model + ", serialId=" + serialId + ", complaints="
				+ complaints + ", underWarranty=" + underWarranty + ", observation=" + observation + ", itemName="
				+ itemName + ", brandName=" + brandName + ", productName=" + productName + ", customer=" + customer
				+ ", voucherDate=" + voucherDate + "]";
	}
	
	 

}
