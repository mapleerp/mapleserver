package com.maple.restserver.report.entity;

public class DayEndProcessing {

	/*
	 * This entity is used only for processing and arrange the data in a particular format
	 * Creatd by Sari(23-02-21)
	 */
	String receiptMode;
	Double salesAmount;
	Double soAmount;
	Double realizedAmount;
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	

	public Double getSalesAmount() {
		return salesAmount;
	}
	public void setSalesAmount(Double salesAmount) {
		this.salesAmount = salesAmount;
	}
	public Double getSoAmount() {
		return soAmount;
	}
	public void setSoAmount(Double soAmount) {
		this.soAmount = soAmount;
	}
	public Double getRealizedAmount() {
		return realizedAmount;
	}
	public void setRealizedAmount(Double realizedAmount) {
		this.realizedAmount = realizedAmount;
	}
	@Override
	public String toString() {
		return "DayEndProcessing [receiptMode=" + receiptMode + ", salesAmount=" + salesAmount + ", soAmount="
				+ soAmount + ", realizedAmount=" + realizedAmount + "]";
	}
	
}
