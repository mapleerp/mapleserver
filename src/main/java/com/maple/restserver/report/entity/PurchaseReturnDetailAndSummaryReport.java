package com.maple.restserver.report.entity;

public class PurchaseReturnDetailAndSummaryReport {
	
	String returnVoucherDate;
	String purchaseReturnVoucher;
	String supplierName;
	String returnVoucherNum;
	String itemName;
	String groupName;
	String itemCode;
	String batch;
	String expiryDate;
	Double qty;
	Double purchaseRate;
	Double amount;
	String userName;
	Double beforeGST;
	Double gst;
	Double total;
	
	
	public String getReturnVoucherDate() {
		return returnVoucherDate;
	}
	public void setReturnVoucherDate(String returnVoucherDate) {
		this.returnVoucherDate = returnVoucherDate;
	}
	public String getPurchaseReturnVoucher() {
		return purchaseReturnVoucher;
	}
	public void setPurchaseReturnVoucher(String purchaseReturnVoucher) {
		this.purchaseReturnVoucher = purchaseReturnVoucher;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getReturnVoucherNum() {
		return returnVoucherNum;
	}
	public void setReturnVoucherNum(String returnVoucherNum) {
		this.returnVoucherNum = returnVoucherNum;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getPurchaseRate() {
		return purchaseRate;
	}
	public void setPurchaseRate(Double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Double getBeforeGST() {
		return beforeGST;
	}
	public void setBeforeGST(Double beforeGST) {
		this.beforeGST = beforeGST;
	}
	public Double getGst() {
		return gst;
	}
	public void setGst(Double gst) {
		this.gst = gst;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	
	
	@Override
	public String toString() {
		return "PurchaseReturnDetailAndSummaryReport [returnVoucherDate=" + returnVoucherDate
				+ ", purchaseReturnVoucher=" + purchaseReturnVoucher + ", supplierName=" + supplierName
				+ ", returnVoucherNum=" + returnVoucherNum + ", itemName=" + itemName + ", groupName=" + groupName
				+ ", itemCode=" + itemCode + ", batch=" + batch + ", expiryDate=" + expiryDate + ", qty=" + qty
				+ ", purchaseRate=" + purchaseRate + ", amount=" + amount + ", userName=" + userName + ", beforeGST="
				+ beforeGST + ", gst=" + gst + ", total=" + total + "]";
	}
	
	
	
}
