package com.maple.restserver.report.entity;

import java.util.Date;

public class WriteOffDetailsAndSummaryReport {
	
	 String voucherNumber;
	 String date;
	 Double amount;
	 String department;
	 String user;
	 String remarks;
	 String itemName;
	 String itemCode;
	 String itemGroup;
	 String batch;
	 String expiryDate;
	 Double quantity;
	 Double rate;
	
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemGroup() {
		return itemGroup;
	}
	public void setItemGroup(String itemGroup) {
		this.itemGroup = itemGroup;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	@Override
	public String toString() {
		return "WriteOffDetailsAndSummaryReport [voucherNumber=" + voucherNumber + ", date=" + date + ", amount="
				+ amount + ", department=" + department + ", user=" + user + ", remarks=" + remarks + ", itemName="
				+ itemName + ", itemCode=" + itemCode + ", itemGroup=" + itemGroup + ", batch=" + batch
				+ ", expiryDate=" + expiryDate + ", quantity=" + quantity + ", rate=" + rate + "]";
	}
	
	

}
