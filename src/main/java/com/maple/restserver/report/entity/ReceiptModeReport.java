package com.maple.restserver.report.entity;

public class ReceiptModeReport {

	String receiptMode;
	Double amount;
	Double saleOrderAmount;
	Double prevSaleOrderAmount;
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getSaleOrderAmount() {
		return saleOrderAmount;
	}
	public void setSaleOrderAmount(Double saleOrderAmount) {
		this.saleOrderAmount = saleOrderAmount;
	}
	public Double getPrevSaleOrderAmount() {
		return prevSaleOrderAmount;
	}
	public void setPrevSaleOrderAmount(Double prevSaleOrderAmount) {
		this.prevSaleOrderAmount = prevSaleOrderAmount;
	}
	@Override
	public String toString() {
		return "ReceiptModeReport [receiptMode=" + receiptMode + ", amount=" + amount + ", saleOrderAmount="
				+ saleOrderAmount + ", prevSaleOrderAmount=" + prevSaleOrderAmount + "]";
	}


	
	
}
