package com.maple.restserver.report.entity;

public class ProductionPrePlaning {

	public ProductionPrePlaning() {
		
	}
	
	String itemId;
	Double qty;
	String batch;
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public ProductionPrePlaning(String itemId, Double qty, String batch) {
		super();
		this.itemId = itemId;
		this.qty = qty;
		this.batch = batch;
	}
	
}
