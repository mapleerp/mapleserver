package com.maple.restserver.report.entity;

public class VoucherNumberDtlReport {

	
	Integer posStartingBillNumber;
	Integer posEndingBillNumber;
	Integer posBillCount;
	Integer wholeSaleStartingBillNumber; 
	Integer wholeSaleEndingBillNumber; 
	Integer  wholeSaleBillCount;
	public Integer getPosStartingBillNumber() {
		return posStartingBillNumber;
	}
	public void setPosStartingBillNumber(Integer posStartingBillNumber) {
		this.posStartingBillNumber = posStartingBillNumber;
	}
	public Integer getPosEndingBillNumber() {
		return posEndingBillNumber;
	}
	public void setPosEndingBillNumber(Integer posEndingBillNumber) {
		this.posEndingBillNumber = posEndingBillNumber;
	}
	public Integer getPosBillCount() {
		return posBillCount;
	}
	public void setPosBillCount(Integer posBillCount) {
		this.posBillCount = posBillCount;
	}
	public Integer getWholeSaleStartingBillNumber() {
		return wholeSaleStartingBillNumber;
	}
	public void setWholeSaleStartingBillNumber(Integer wholeSaleStartingBillNumber) {
		this.wholeSaleStartingBillNumber = wholeSaleStartingBillNumber;
	}
	public Integer getWholeSaleEndingBillNumber() {
		return wholeSaleEndingBillNumber;
	}
	public void setWholeSaleEndingBillNumber(Integer wholeSaleEndingBillNumber) {
		this.wholeSaleEndingBillNumber = wholeSaleEndingBillNumber;
	}
	public Integer getWholeSaleBillCount() {
		return wholeSaleBillCount;
	}
	public void setWholeSaleBillCount(Integer wholeSaleBillCount) {
		this.wholeSaleBillCount = wholeSaleBillCount;
	}
	
	
}
