package com.maple.restserver.report.entity;

public class PurchaseHdrReport {
	String supplierName;
	String voucherNumber;
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	@Override
	public String toString() {
		return "PurchaseHdrReport [supplierName=" + supplierName + ", voucherNumber=" + voucherNumber + "]";
	}
	
}
