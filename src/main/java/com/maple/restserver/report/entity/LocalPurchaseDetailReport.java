package com.maple.restserver.report.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class LocalPurchaseDetailReport {
	String supplierName;
	String voucherNumber;
	Date voucherDate;
	String itemName;
	Double qty;
	Double purchseRate;
	Double taxRate;
	String unit;
	Double amount;
	Double amountTotal;

	String supplierInvNo;
	String supplierGst;
	
	Double taxAmount;
	Double taxSplit;
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getPurchseRate() {
		return purchseRate;
	}
	public void setPurchseRate(Double purchseRate) {
		this.purchseRate = purchseRate;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getAmountTotal() {
		return amountTotal;
	}
	public void setAmountTotal(Double amountTotal) {
		this.amountTotal = amountTotal;
	}
	public String getSupplierInvNo() {
		return supplierInvNo;
	}
	public void setSupplierInvNo(String supplierInvNo) {
		this.supplierInvNo = supplierInvNo;
	}
	public String getSupplierGst() {
		return supplierGst;
	}
	public void setSupplierGst(String supplierGst) {
		this.supplierGst = supplierGst;
	}
	public Double getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	public Double getTaxSplit() {
		return taxSplit;
	}
	public void setTaxSplit(Double taxSplit) {
		this.taxSplit = taxSplit;
	}
	@Override
	public String toString() {
		return "LocalPurchaseDetailReport [supplierName=" + supplierName + ", voucherNumber=" + voucherNumber
				+ ", voucherDate=" + voucherDate + ", itemName=" + itemName + ", qty=" + qty + ", purchseRate="
				+ purchseRate + ", taxRate=" + taxRate + ", unit=" + unit + ", amount=" + amount + ", amountTotal="
				+ amountTotal + ", supplierInvNo=" + supplierInvNo + ", supplierGst=" + supplierGst + ", taxAmount="
				+ taxAmount + ", taxSplit=" + taxSplit + "]";
	}
	
}
