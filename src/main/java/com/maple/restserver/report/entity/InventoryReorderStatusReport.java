package com.maple.restserver.report.entity;

import java.util.Date;

public class InventoryReorderStatusReport {
	String companyName;
	String branchName;
	Date startingDate;
	Date endingDate;
	String itemName;
	Double closingStock;
	Double netAvailable;
	Double reorderLevel;
	Double  shortFall;
	Double minReorderQty;
	Double orderTobePlaced;
	String purcOrdersPending;
	String saleOrdersDue;
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public Date getStartingDate() {
		return startingDate;
	}
	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}
	public Date getEndingDate() {
		return endingDate;
	}
	public void setEndingDate(Date endingDate) {
		this.endingDate = endingDate;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getClosingStock() {
		return closingStock;
	}
	public void setClosingStock(Double closingStock) {
		this.closingStock = closingStock;
	}
	public Double getNetAvailable() {
		return netAvailable;
	}
	public void setNetAvailable(Double netAvailable) {
		this.netAvailable = netAvailable;
	}
	public Double getReorderLevel() {
		return reorderLevel;
	}
	public void setReorderLevel(Double reorderLevel) {
		this.reorderLevel = reorderLevel;
	}
	public Double getShortFall() {
		return shortFall;
	}
	public void setShortFall(Double shortFall) {
		this.shortFall = shortFall;
	}
	public Double getMinReorderQty() {
		return minReorderQty;
	}
	public void setMinReorderQty(Double minReorderQty) {
		this.minReorderQty = minReorderQty;
	}
	public Double getOrderTobePlaced() {
		return orderTobePlaced;
	}
	public void setOrderTobePlaced(Double orderTobePlaced) {
		this.orderTobePlaced = orderTobePlaced;
	}
	public String getPurcOrdersPending() {
		return purcOrdersPending;
	}
	public void setPurcOrdersPending(String purcOrdersPending) {
		this.purcOrdersPending = purcOrdersPending;
	}
	public String getSaleOrdersDue() {
		return saleOrdersDue;
	}
	public void setSaleOrdersDue(String saleOrdersDue) {
		this.saleOrdersDue = saleOrdersDue;
	}
	
	
}
