package com.maple.restserver.report.entity;

import java.util.Date;
import java.time.LocalDate;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


 
public class ItemWiseDtlReport {

	String companyName;
	String itemName;
	Double qty;
	Date voucherDate;
	Double value;
	String branchAddress;
	String branchPhoneNo;
	String branchState;
	String branchEmail;
	String branchWebsite;
	String branchName;
	String customerName;
	
	
	public String getCustomerName() {
		return customerName;
	}




	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}




	public String getCompanyName() {
		return companyName;
	}




	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}




	public String getBranchName() {
		return branchName;
	}




	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}




	public String getItemName() {
		return itemName;
	}




	public void setItemName(String itemName) {
		this.itemName = itemName;
	}




	public Double getQty() {
		return qty;
	}




	public void setQty(Double qty) {
		this.qty = qty;
	}




	public String getBranchEmail() {
		return branchEmail;
	}




	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}




	public String getBranchWebsite() {
		return branchWebsite;
	}




	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}




	public Date getVoucherDate() {
		return voucherDate;
	}




	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}




	




	public Double getValue() {
		return value;
	}




	public void setValue(Double value) {
		this.value = value;
	}









	public String getBranchAddress() {
		return branchAddress;
	}




	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}




	public String getBranchPhoneNo() {
		return branchPhoneNo;
	}




	public void setBranchPhoneNo(String branchPhoneNo) {
		this.branchPhoneNo = branchPhoneNo;
	}




	public String getBranchState() {
		return branchState;
	}




	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}




	@Override
	public String toString() {
		return "ItemWiseDtlReport [companyName=" + companyName + ", branchName=" + branchName + ", itemName=" + itemName
				+ ", qty=" + qty + ", branchEmail=" + branchEmail + ", branchWebsite=" + branchWebsite
				+ ", voucherDate=" + voucherDate + ", value=" + value + ", branchAddress=" + branchAddress
				+ ", branchPhoneNo=" + branchPhoneNo + ", branchState=" + branchState + "]";
	}







	


	
}
