package com.maple.restserver.report.entity;

public class SaleOrderReceiptReport {
	String voucherNumber;
	Double receiptAmount;
	String customerName;
	
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Double getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	@Override
	public String toString() {
		return "SaleOrderReceiptReport [voucherNumber=" + voucherNumber + ", receiptAmount=" + receiptAmount + "]";
	}
	
}
