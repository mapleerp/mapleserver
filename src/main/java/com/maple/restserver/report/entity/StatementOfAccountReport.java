package com.maple.restserver.report.entity;

public class StatementOfAccountReport {

	String accountName;
	String debit;
	String credit;
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getDebit() {
		return debit;
	}
	public void setDebit(String debit) {
		this.debit = debit;
	}
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	@Override
	public String toString() {
		return "StatementOfAccount [accountName=" + accountName + ", debit=" + debit + ", credit=" + credit + "]";
	}
	
	
	
}
