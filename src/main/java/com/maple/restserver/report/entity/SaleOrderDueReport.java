package com.maple.restserver.report.entity;

import java.util.Date;

public class SaleOrderDueReport {
	
private String id ;
	Date dueDate;
	String day;
	String time;
	String otName;
	String paymentMode;
	Double advance;
	Double balance;
    String orderDueTime;
    String orderTimeMode;
	String orderDueDay;
	String messageToPrint;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getOtName() {
		return otName;
	}
	public void setOtName(String otName) {
		this.otName = otName;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public Double getAdvance() {
		return advance;
	}
	public void setAdvance(Double advance) {
		this.advance = advance;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public String getOrderDueTime() {
		return orderDueTime;
	}
	public void setOrderDueTime(String orderDueTime) {
		this.orderDueTime = orderDueTime;
	}
	public String getOrderTimeMode() {
		return orderTimeMode;
	}
	public void setOrderTimeMode(String orderTimeMode) {
		this.orderTimeMode = orderTimeMode;
	}
	public String getOrderDueDay() {
		return orderDueDay;
	}
	public void setOrderDueDay(String orderDueDay) {
		this.orderDueDay = orderDueDay;
	}
	@Override
	public String toString() {
		return "SaleOrderDueReport [id=" + id + ", dueDate=" + dueDate + ", day=" + day + ", time=" + time + ", otName="
				+ otName + ", paymentMode=" + paymentMode + ", advance=" + advance + ", balance=" + balance
				+ ", orderDueTime=" + orderDueTime + ", orderTimeMode=" + orderTimeMode + ", orderDueDay=" + orderDueDay
				+ ", messageToPrint=" + messageToPrint + "]";
	}
	public String getMessageToPrint() {
		return messageToPrint;
	}
	public void setMessageToPrint(String messageToPrint) {
		this.messageToPrint = messageToPrint;
	}
	
	
	
}
