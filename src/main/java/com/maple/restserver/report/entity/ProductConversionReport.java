package com.maple.restserver.report.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class ProductConversionReport {

	String fromItemName;
	String toItemName;
	Double fromItemQty;
	Double toItemQty;
	Date date;
	Double fromAmount;
	Double toAmount;
	Double fromItemStdPrice;
	Double toItemStdPrice;
	public String getFromItemName() {
		return fromItemName;
	}
	public void setFromItemName(String fromItemName) {
		this.fromItemName = fromItemName;
	}
	public String getToItemName() {
		return toItemName;
	}
	public void setToItemName(String toItemName) {
		this.toItemName = toItemName;
	}
	public Double getFromItemQty() {
		return fromItemQty;
	}
	public void setFromItemQty(Double fromItemQty) {
		this.fromItemQty = fromItemQty;
	}
	public Double getToItemQty() {
		return toItemQty;
	}
	public void setToItemQty(Double toItemQty) {
		this.toItemQty = toItemQty;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Double getFromAmount() {
		return fromAmount;
	}
	public void setFromAmount(Double fromAmount) {
		this.fromAmount = fromAmount;
	}
	public Double getToAmount() {
		return toAmount;
	}
	public void setToAmount(Double toAmount) {
		this.toAmount = toAmount;

	}
	public Double getFromItemStdPrice() {
		return fromItemStdPrice;
	}
	public void setFromItemStdPrice(Double fromItemStdPrice) {
		this.fromItemStdPrice = fromItemStdPrice;
	}
	public Double getToItemStdPrice() {
		return toItemStdPrice;
	}
	public void setToItemStdPrice(Double toItemStdPrice) {
		this.toItemStdPrice = toItemStdPrice;
	}
	
	
	
}
