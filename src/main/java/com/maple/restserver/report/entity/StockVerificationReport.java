package com.maple.restserver.report.entity;

public class StockVerificationReport {
	
	String itemName;
	Double qty;
	Double systemQty;
	String branchCode;
	String userName;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getSystemQty() {
		return systemQty;
	}
	public void setSystemQty(Double systemQty) {
		this.systemQty = systemQty;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
	public String toString() {
		return "StockVerificationReport [itemName=" + itemName + ", qty=" + qty + ", systemQty=" + systemQty
				+ ", branchCode=" + branchCode + ", userName=" + userName + "]";
	}

	
}
