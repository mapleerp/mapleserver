package com.maple.restserver.report.entity;

import java.util.Date;

public class ShortExpiryReport {
	
	String itemName;
	String batch;
	Double qty;
	Date expiryDate;
	Date updatedDate;
	String categoryName;
	
	String itemCode;
	Double rate;
	Double amount;
	String daysToExpiry;
	
	
	
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getDaysToExpiry() {
		return daysToExpiry;
	}
	public void setDaysToExpiry(String daysToExpiry) {
		this.daysToExpiry = daysToExpiry;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	@Override
	public String toString() {
		return "ShortExpiryReport [itemName=" + itemName + ", batch=" + batch + ", qty=" + qty + ", expiryDate="
				+ expiryDate + ", updatedDate=" + updatedDate + ", categoryName=" + categoryName + ", itemCode="
				+ itemCode + ", rate=" + rate + ", amount=" + amount + ", daysToExpiry=" + daysToExpiry + "]";
	}
	
	
	
	

}
