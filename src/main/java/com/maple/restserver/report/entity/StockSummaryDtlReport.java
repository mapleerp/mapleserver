package com.maple.restserver.report.entity;

import java.time.LocalDateTime;
import java.util.Date;

public class StockSummaryDtlReport {

	
	String companyName;
	Date fromDate;
	Date toDate;
	String itemName;
	Date rDate;
	String voucherNo;

	String id;
	Date VoucherDate;
	String item_id;
	String particulars;
	String voucherNumber;
	Double inwardQty;

	Double inwardValue;
	Double outwardQty;
	Double outwardValue;
	Double closingQty;
	Double closingValue;

	String unitName;
	String branchCode;
	Double openingStock;
	
	private Date updatedTime;
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Date getrDate() {
		return rDate;
	}
	public void setrDate(Date rDate) {
		this.rDate = rDate;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getVoucherDate() {
		return VoucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		VoucherDate = voucherDate;
	}
	public String getItem_id() {
		return item_id;
	}
	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}
	public String getParticulars() {
		return particulars;
	}
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public Double getInwardValue() {
		return inwardValue;
	}
	public void setInwardValue(Double inwardValue) {
		this.inwardValue = inwardValue;
	}
	public Double getOutwardQty() {
		return outwardQty;
	}
	public void setOutwardQty(Double outwardQty) {
		this.outwardQty = outwardQty;
	}
	public Double getOutwardValue() {
		return outwardValue;
	}
	public void setOutwardValue(Double outwardValue) {
		this.outwardValue = outwardValue;
	}
	public Double getClosingQty() {
		return closingQty;
	}
	public void setClosingQty(Double closingQty) {
		this.closingQty = closingQty;
	}
	public Double getClosingValue() {
		return closingValue;
	}
	public void setClosingValue(Double closingValue) {
		this.closingValue = closingValue;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Double getOpeningStock() {
		return openingStock;
	}
	public void setOpeningStock(Double openingStock) {
		this.openingStock = openingStock;
	}
	
	
	public Double getInwardQty() {
		return inwardQty;
	}
	public void setInwardQty(Double inwardQty) {
		this.inwardQty = inwardQty;
	}
	

	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Date getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}
	@Override
	public String toString() {
		return "StockSummaryDtlReport [companyName=" + companyName + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", itemName=" + itemName + ", rDate=" + rDate + ", voucherNo=" + voucherNo + ", id=" + id
				+ ", VoucherDate=" + VoucherDate + ", item_id=" + item_id + ", particulars=" + particulars
				+ ", voucherNumber=" + voucherNumber + ", inwardQty=" + inwardQty + ", inwardValue=" + inwardValue
				+ ", outwardQty=" + outwardQty + ", outwardValue=" + outwardValue + ", closingQty=" + closingQty
				+ ", closingValue=" + closingValue + ", unitName=" + unitName + ", branchCode=" + branchCode
				+ ", openingStock=" + openingStock + ", updatedTime=" + updatedTime + "]";
	}
	
	

	

	
	
}
