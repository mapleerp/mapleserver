package com.maple.restserver.report.entity;

import java.util.Date;

 
public class ReceiptInvoice {
	
	String id;
	String receiptNumber;
	String invoiceNumber;
	Double recievedAmount;
    String companyName;
    String state;
    String companyGst;
    Date voucherDate;
    String voucherNumber;
    
    String account;
    String remark;
    String modeOfpayment;
	Date instrumentDate;
	 String branchName;
	 String branchPlace;
	 String branchGst;
	 String branchAddress1;
	 String branchTelNo;
   String instrumentNo;
		String familyName;
		String memberName;
	public String getFamilyName() {
			return familyName;
		}
		public void setFamilyName(String familyName) {
			this.familyName = familyName;
		}
		public String getMemberName() {
			return memberName;
		}
		public void setMemberName(String memberName) {
			this.memberName = memberName;
		}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getInstrumentNo() {
		return instrumentNo;
	}
	public void setInstrumentNo(String instrumentNo) {
		this.instrumentNo = instrumentNo;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Double getRecievedAmount() {
		return recievedAmount;
	}
	public void setRecievedAmount(Double recievedAmount) {
		this.recievedAmount = recievedAmount;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCompanyGst() {
		return companyGst;
	}
	public void setCompanyGst(String companyGst) {
		this.companyGst = companyGst;
	}
	

	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	
	

	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getModeOfpayment() {
		return modeOfpayment;
	}
	public void setModeOfpayment(String modeOfpayment) {
		this.modeOfpayment = modeOfpayment;
	}
	public Date getInstrumentDate() {
		return instrumentDate;
	}
	public void setInstrumentDate(Date instrumentDate) {
		this.instrumentDate = instrumentDate;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchPlace() {
		return branchPlace;
	}
	public void setBranchPlace(String branchPlace) {
		this.branchPlace = branchPlace;
	}
	public String getBranchGst() {
		return branchGst;
	}
	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}
	public String getBranchAddress1() {
		return branchAddress1;
	}
	public void setBranchAddress1(String branchAddress1) {
		this.branchAddress1 = branchAddress1;
	}
	public String getBranchTelNo() {
		return branchTelNo;
	}
	public void setBranchTelNo(String branchTelNo) {
		this.branchTelNo = branchTelNo;
	}
	@Override
	public String toString() {
		return "ReceiptInvoice [id=" + id + ", receiptNumber=" + receiptNumber + ", invoiceNumber=" + invoiceNumber
				+ ", recievedAmount=" + recievedAmount + ", companyName=" + companyName + ", state=" + state
				+ ", companyGst=" + companyGst + ", voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber
				+ ", account=" + account + ", remark=" + remark + ", modeOfpayment=" + modeOfpayment
				+ ", instrumentDate=" + instrumentDate + ", branchName=" + branchName + ", branchPlace=" + branchPlace
				+ ", branchGst=" + branchGst + ", branchAddress1=" + branchAddress1 + ", branchTelNo=" + branchTelNo
				+ ", familyName=" + familyName + ", memberName=" + memberName + "]";
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
    
}
