package com.maple.restserver.report.entity;

import java.util.Date;

public class SaleOrderDetailsReport {
    private String id;
	String customerName;
	String customerPhoneNo;
	String voucherNo;
	String convertedToInvoice;
	String itemName;
	String unitName;
	String companyName;
	Double rate;
	Double qty;
	Double mrp;
	Double amount;
	Double totalAmount;
	Double paidAmount;
	String localCustomerName;
	String localCustomerPhoneNo;
	String orderDueTime;
	String deliveryMode;
	Date dueDate;
	
	public String getDeliveryMode() {
		return deliveryMode;
	}
	public void setDeliveryMode(String deliveryMode) {
		this.deliveryMode = deliveryMode;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getOrderDueTime() {
		return orderDueTime;
	}
	public void setOrderDueTime(String orderDueTime) {
		this.orderDueTime = orderDueTime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerPhoneNo() {
		return customerPhoneNo;
	}
	public void setCustomerPhoneNo(String customerPhoneNo) {
		this.customerPhoneNo = customerPhoneNo;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	public String getConvertedToInvoice() {
		return convertedToInvoice;
	}
	public void setConvertedToInvoice(String convertedToInvoice) {
		this.convertedToInvoice = convertedToInvoice;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getLocalCustomerName() {
		return localCustomerName;
	}
	public void setLocalCustomerName(String localCustomerName) {
		this.localCustomerName = localCustomerName;
	}
	public String getLocalCustomerPhoneNo() {
		return localCustomerPhoneNo;
	}
	public void setLocalCustomerPhoneNo(String localCustomerPhoneNo) {
		this.localCustomerPhoneNo = localCustomerPhoneNo;
	}
	
	
	
}
