package com.maple.restserver.report.entity;

public class MobileOrderReports {
	
	String customerName;
	String customerPhoneNo;
	String mobileCartHdrId;
	String itemName;
	String itemId;
	Double qty;
	String status;
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getMobileCartHdrId() {
		return mobileCartHdrId;
	}
	public void setMobileCartHdrId(String mobileCartHdrId) {
		this.mobileCartHdrId = mobileCartHdrId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCustomerPhoneNo() {
		return customerPhoneNo;
	}
	public void setCustomerPhoneNo(String customerPhoneNo) {
		this.customerPhoneNo = customerPhoneNo;
	}
	@Override
	public String toString() {
		return "MobileOrderReports [customerName=" + customerName + ", customerPhoneNo=" + customerPhoneNo
				+ ", mobileCartHdrId=" + mobileCartHdrId + ", itemName=" + itemName + ", itemId=" + itemId + ", qty="
				+ qty + ", status=" + status + "]";
	}
	
}
