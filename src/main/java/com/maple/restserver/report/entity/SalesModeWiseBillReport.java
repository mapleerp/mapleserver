package com.maple.restserver.report.entity;

import java.util.Date;

public class SalesModeWiseBillReport {
	
	String branchCode;
	Long minVoucherNumericNo;
	Long maxVoucherNumericNo;
	String salesMode;
	Date voucherDate;
	int count;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Long getMinVoucherNumericNo() {
		return minVoucherNumericNo;
	}
	public void setMinVoucherNumericNo(Long minVoucherNumericNo) {
		this.minVoucherNumericNo = minVoucherNumericNo;
	}
	public Long getMaxVoucherNumericNo() {
		return maxVoucherNumericNo;
	}
	public void setMaxVoucherNumericNo(Long maxVoucherNumericNo) {
		this.maxVoucherNumericNo = maxVoucherNumericNo;
	}
	public String getSalesMode() {
		return salesMode;
	}
	public void setSalesMode(String salesMode) {
		this.salesMode = salesMode;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	
}
