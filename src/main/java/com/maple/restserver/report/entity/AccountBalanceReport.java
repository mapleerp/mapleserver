package com.maple.restserver.report.entity;

import java.util.Date;

public class AccountBalanceReport {
	Double openingBalance;
	String branchCode;

	String branchAddress;
	String branchPhone;

	String branchName;
	String companyName;
	String branchEmail;
	String branchWebsite;
	String accountHeads;
	Date startDate;
	Date endDate;
	Date date;
	String description;
	String remark;
	Double debit;
	Double credit;
	int slNo;
	
	String vDate;
	
	String openingBalanceType ;
	String closingBalanceType ;
	Double ClosingBalance;
	
	String accountClassId;

	
	public String getAccountClassId() {
		return accountClassId;
	}
	public void setAccountClassId(String accountClassId) {
		this.accountClassId = accountClassId;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getBranchPhone() {
		return branchPhone;
	}
	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	public String getAccountHeads() {
		return accountHeads;
	}
	public void setAccountHeads(String accountHeads) {
		this.accountHeads = accountHeads;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Double getDebit() {
		return debit;
	}
	public void setDebit(Double debit) {
		this.debit = debit;
	}
	public Double getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(Double openingBalance) {
		this.openingBalance = openingBalance;
	}
	public int getSlNo() {
		return slNo;
	}
	public void setSlNo(int slNo) {
		this.slNo = slNo;
	}
	public String getOpeningBalanceType() {
		return openingBalanceType;
	}
	public void setOpeningBalanceType(String openingBalanceType) {
		this.openingBalanceType = openingBalanceType;
	}
	public String getClosingBalanceType() {
		return closingBalanceType;
	}
	public void setClosingBalanceType(String closingBalanceType) {
		this.closingBalanceType = closingBalanceType;
	}
	public Double getClosingBalance() {
		return ClosingBalance;
	}
	public void setClosingBalance(Double closingBalance) {
		ClosingBalance = closingBalance;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}

	

}
