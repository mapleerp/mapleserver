package com.maple.restserver.report.entity;

public class ItemDtlInWeb {
	
	String name;
	Double unitPrice;
	Double unitsInStock;
	Double qty;
	String id;
	String barCode;
	String barCodeLine1;
	String barCodeLine2;
	String barcodeFormat;
	String bestBefore;
	String binNo;
	String branchCode;
	String brandId;
	String categoryId;
	Double cess;
	Double cgst;
	String hsnCode;
	boolean isDeleted;
	boolean isKit;
	String itemCode;
	String itemDescription;
	Double itemDiscount;
	String itemDtl;
	String itemGenericName;
	String itemGroupId;
	String itemName;
	Double taxRate;
	Double standardPrice;
	Double Amount;
	Double productQty;
	String reorderWaitingPeriod;
	String unitId;
	String serviceOrGoods;
	
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Double getUnitsInStock() {
		return unitsInStock;
	}
	public void setUnitsInStock(Double unitsInStock) {
		this.unitsInStock = unitsInStock;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getBarCodeLine1() {
		return barCodeLine1;
	}
	public void setBarCodeLine1(String barCodeLine1) {
		this.barCodeLine1 = barCodeLine1;
	}
	public String getBarCodeLine2() {
		return barCodeLine2;
	}
	public void setBarCodeLine2(String barCodeLine2) {
		this.barCodeLine2 = barCodeLine2;
	}
	public String getBarcodeFormat() {
		return barcodeFormat;
	}
	public void setBarcodeFormat(String barcodeFormat) {
		this.barcodeFormat = barcodeFormat;
	}
	public String getBestBefore() {
		return bestBefore;
	}
	public void setBestBefore(String bestBefore) {
		this.bestBefore = bestBefore;
	}
	public String getBinNo() {
		return binNo;
	}
	public void setBinNo(String binNo) {
		this.binNo = binNo;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBrandId() {
		return brandId;
	}
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public Double getCess() {
		return cess;
	}
	public void setCess(Double cess) {
		this.cess = cess;
	}
	public Double getCgst() {
		return cgst;
	}
	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public boolean isKit() {
		return isKit;
	}
	public void setKit(boolean isKit) {
		this.isKit = isKit;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public Double getItemDiscount() {
		return itemDiscount;
	}
	public void setItemDiscount(Double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}
	public String getItemDtl() {
		return itemDtl;
	}
	public void setItemDtl(String itemDtl) {
		this.itemDtl = itemDtl;
	}
	public String getItemGenericName() {
		return itemGenericName;
	}
	public void setItemGenericName(String itemGenericName) {
		this.itemGenericName = itemGenericName;
	}
	public String getItemGroupId() {
		return itemGroupId;
	}
	public void setItemGroupId(String itemGroupId) {
		this.itemGroupId = itemGroupId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getStandardPrice() {
		return standardPrice;
	}
	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}
	public Double getAmount() {
		return Amount;
	}
	public void setAmount(Double amount) {
		Amount = amount;
	}
	public Double getProductQty() {
		return productQty;
	}
	public void setProductQty(Double productQty) {
		this.productQty = productQty;
	}
	public String getReorderWaitingPeriod() {
		return reorderWaitingPeriod;
	}
	public void setReorderWaitingPeriod(String reorderWaitingPeriod) {
		this.reorderWaitingPeriod = reorderWaitingPeriod;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getServiceOrGoods() {
		return serviceOrGoods;
	}
	public void setServiceOrGoods(String serviceOrGoods) {
		this.serviceOrGoods = serviceOrGoods;
	}
	
	
	
	@Override
	public String toString() {
		return "ItemDtlInWeb [name=" + name + ", unitPrice=" + unitPrice + ", unitsInStock=" + unitsInStock + ", qty="
				+ qty + ", id=" + id + ", barCode=" + barCode + ", barCodeLine1=" + barCodeLine1 + ", barCodeLine2="
				+ barCodeLine2 + ", barcodeFormat=" + barcodeFormat + ", bestBefore=" + bestBefore + ", binNo=" + binNo
				+ ", branchCode=" + branchCode + ", brandId=" + brandId + ", categoryId=" + categoryId + ", cess="
				+ cess + ", cgst=" + cgst + ", hsnCode=" + hsnCode + ", isDeleted=" + isDeleted + ", isKit=" + isKit
				+ ", itemCode=" + itemCode + ", itemDescription=" + itemDescription + ", itemDiscount=" + itemDiscount
				+ ", itemDtl=" + itemDtl + ", itemGenericName=" + itemGenericName + ", itemGroupId=" + itemGroupId
				+ ", itemName=" + itemName + ", taxRate=" + taxRate + ", standardPrice=" + standardPrice + ", Amount="
				+ Amount + ", productQty=" + productQty + ", reorderWaitingPeriod=" + reorderWaitingPeriod + ", unitId="
				+ unitId + ", serviceOrGoods=" + serviceOrGoods + "]";
	}
	
	
	
	
	
}
