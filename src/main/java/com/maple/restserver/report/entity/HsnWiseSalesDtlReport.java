package com.maple.restserver.report.entity;




public class HsnWiseSalesDtlReport {

	String itemName;
	String hsnCode;
	String unitName;
	Double qty;
	Double rate;
	Double value;
	Double cgstAmount;
	Double sgstAmount;
	Double taxRate;
	Double cessRate;
	
	

	public String getItemName() {
		return itemName;
	}



	public void setItemName(String itemName) {
		this.itemName = itemName;
	}



	public String getHsnCode() {
		return hsnCode;
	}



	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}



	public String getUnitName() {
		return unitName;
	}



	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}



	public Double getQty() {
		return qty;
	}



	public void setQty(Double qty) {
		this.qty = qty;
	}



	public Double getRate() {
		return rate;
	}



	public void setRate(Double rate) {
		this.rate = rate;
	}



	public Double getValue() {
		return value;
	}



	public void setValue(Double value) {
		this.value = value;
	}



	public Double getCgstAmount() {
		return cgstAmount;
	}



	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}



	public Double getSgstAmount() {
		return sgstAmount;
	}



	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}



	public Double getTaxRate() {
		return taxRate;
	}



	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}



	public Double getCessRate() {
		return cessRate;
	}



	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}



	@Override
	public String toString() {
		return "HsnWiseSalesDtlReport [itemName=" + itemName + ", hsnCode=" + hsnCode + ", unitName=" + unitName
				+ ", qty=" + qty + ", rate=" + rate + ", value=" + value + ", cgstAmount=" + cgstAmount
				+ ", sgstAmount=" + sgstAmount + ", taxRate=" + taxRate + ", cessRate=" + cessRate + "]";
	}
	
	
	
}
