package com.maple.restserver.report.entity;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Month;
import java.time.Year;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;

public class ExpenseSummaryDailyReport {

	Date voucherDate;
	 
	private CompanyMst companyMst;

	private String  branchCode;
	
    private AccountHeads accountHeads;
	private Month monthOfExpence;
	BigDecimal accountHeadsWiseExpense;
	
	BigDecimal sumOfAccountHeadWiseExpense;
	BigDecimal sumOfExpance;
	
	private Year yearOfExpense;
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public AccountHeads getAccountHeads() {
		return accountHeads;
	}
	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}
	public BigDecimal getAccountHeadsWiseExpense() {
		return accountHeadsWiseExpense;
	}
	public void setAccountHeadsWiseExpense(BigDecimal accountHeadsWiseExpense) {
		this.accountHeadsWiseExpense = accountHeadsWiseExpense;
	}
	public BigDecimal getSumOfAccountHeadWiseExpense() {
		return sumOfAccountHeadWiseExpense;
	}
	public void setSumOfAccountHeadWiseExpense(BigDecimal sumOfAccountHeadWiseExpense) {
		this.sumOfAccountHeadWiseExpense = sumOfAccountHeadWiseExpense;
	}
	
	public BigDecimal getSumOfExpance() {
		return sumOfExpance;
	}
	public void setSumOfExpance(BigDecimal sumOfExpance) {
		this.sumOfExpance = sumOfExpance;
	}
	
	
	public Month getMonthOfExpence() {
		return monthOfExpence;
	}
	public void setMonthOfExpence(Month monthOfExpence) {
		this.monthOfExpence = monthOfExpence;
	}
	
	
	public Year getYearOfExpense() {
		return yearOfExpense;
	}
	public void setYearOfExpense(Year yearOfExpense) {
		this.yearOfExpense = yearOfExpense;
	}
	@Override
	public String toString() {
		return "ExpenseSummaryDailyReport [voucherDate=" + voucherDate + ", companyMst=" + companyMst + ", branchCode="
				+ branchCode + ", accountHeads=" + accountHeads + ", monthOfExpence=" + monthOfExpence
				+ ", accountHeadsWiseExpense=" + accountHeadsWiseExpense + ", sumOfAccountHeadWiseExpense="
				+ sumOfAccountHeadWiseExpense + ", sumOfExpance=" + sumOfExpance + ", yearOfExpense=" + yearOfExpense
				+ "]";
	}

	

	
	
	
}
