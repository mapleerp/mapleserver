package com.maple.restserver.report.entity;

import java.util.Date;

public class ReceiptReports {

	String voucherDate;

	String voucherNumber;
	
	String branchCode;
	
	String transdate;
	
	String voucherType;
	String memberId;
	String id;
	String bankAccountNumber;
	String account;
	String modeOfpayment;
	Double amount;
	String depositBank;
	String remark;
	String instnumber;
	String invoiceNumber;
	String accountName;
	String debitAccountId;
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getModeOfpayment() {
		return modeOfpayment;
	}
	public void setModeOfpayment(String modeOfpayment) {
		this.modeOfpayment = modeOfpayment;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getDepositBank() {
		return depositBank;
	}
	public void setDepositBank(String depositBank) {
		this.depositBank = depositBank;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getInstnumber() {
		return instnumber;
	}
	public void setInstnumber(String instnumber) {
		this.instnumber = instnumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getDebitAccountId() {
		return debitAccountId;
	}
	public void setDebitAccountId(String debitAccountId) {
		this.debitAccountId = debitAccountId;
	}
	public String getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getTransdate() {
		return transdate;
	}
	public void setTransdate(String transdate) {
		this.transdate = transdate;
	}
	@Override
	public String toString() {
		return "ReceiptReports [voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber + ", branchCode="
				+ branchCode + ", transdate=" + transdate + ", voucherType=" + voucherType + ", memberId=" + memberId
				+ ", id=" + id + ", bankAccountNumber=" + bankAccountNumber + ", account=" + account
				+ ", modeOfpayment=" + modeOfpayment + ", amount=" + amount + ", depositBank=" + depositBank
				+ ", remark=" + remark + ", instnumber=" + instnumber + ", invoiceNumber=" + invoiceNumber
				+ ", accountName=" + accountName + ", debitAccountId=" + debitAccountId + "]";
	}
	
	
	
	
}
