package com.maple.restserver.report.entity;

import java.util.Date;

public class SupplierMonthlySummary {
	
	String companyName;
	String branchName;
	String branchAddress1;
	String branchAddress2;
	String branchState;
	String branchGst;
	String branchEmail;
	String branchWebsite;
	String branchPhoneNo;
	String refNo;
	Double overDueByDays;
	String supplierName;
	String supplierCompany;
	String supplierPhoneNo;
	String supplierEmail;
	Date startDate;
	Date endDate;
	String particulars;
	Double debit;
	Double credit;
	Double closingBalance;
	Double openingAmount;
	Double pendingAmount;


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public String getBranchAddress1() {
		return branchAddress1;
	}


	public void setBranchAddress1(String branchAddress1) {
		this.branchAddress1 = branchAddress1;
	}


	public String getBranchAddress2() {
		return branchAddress2;
	}


	public void setBranchAddress2(String branchAddress2) {
		this.branchAddress2 = branchAddress2;
	}


	public String getBranchState() {
		return branchState;
	}


	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}


	public String getBranchGst() {
		return branchGst;
	}


	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}


	public String getBranchEmail() {
		return branchEmail;
	}


	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}


	public String getBranchWebsite() {
		return branchWebsite;
	}


	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}


	public String getBranchPhoneNo() {
		return branchPhoneNo;
	}


	public void setBranchPhoneNo(String branchPhoneNo) {
		this.branchPhoneNo = branchPhoneNo;
	}


	public String getRefNo() {
		return refNo;
	}


	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}


	public Double getOverDueByDays() {
		return overDueByDays;
	}


	public void setOverDueByDays(Double overDueByDays) {
		this.overDueByDays = overDueByDays;
	}


	public String getSupplierName() {
		return supplierName;
	}


	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}


	public String getSupplierCompany() {
		return supplierCompany;
	}


	public void setSupplierCompany(String supplierCompany) {
		this.supplierCompany = supplierCompany;
	}


	public String getSupplierPhoneNo() {
		return supplierPhoneNo;
	}


	public void setSupplierPhoneNo(String supplierPhoneNo) {
		this.supplierPhoneNo = supplierPhoneNo;
	}


	public String getSupplierEmail() {
		return supplierEmail;
	}


	public void setSupplierEmail(String supplierEmail) {
		this.supplierEmail = supplierEmail;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getParticulars() {
		return particulars;
	}


	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}


	public Double getDebit() {
		return debit;
	}


	public void setDebit(Double debit) {
		this.debit = debit;
	}


	public Double getCredit() {
		return credit;
	}


	public void setCredit(Double credit) {
		this.credit = credit;
	}


	public Double getClosingBalance() {
		return closingBalance;
	}


	public void setClosingBalance(Double closingBalance) {
		this.closingBalance = closingBalance;
	}


	public Double getOpeningAmount() {
		return openingAmount;
	}


	public void setOpeningAmount(Double openingAmount) {
		this.openingAmount = openingAmount;
	}


	public Double getPendingAmount() {
		return pendingAmount;
	}


	public void setPendingAmount(Double pendingAmount) {
		this.pendingAmount = pendingAmount;
	}


	@Override
	public String toString() {
		return "SupplierMonthlySummary [companyName=" + companyName + ", branchName=" + branchName + ", branchAddress1="
				+ branchAddress1 + ", branchAddress2=" + branchAddress2 + ", branchState=" + branchState
				+ ", branchGst=" + branchGst + ", branchEmail=" + branchEmail + ", branchWebsite=" + branchWebsite
				+ ", branchPhoneNo=" + branchPhoneNo + ", refNo=" + refNo + ", overDueByDays=" + overDueByDays
				+ ", supplierName=" + supplierName + ", supplierCompany=" + supplierCompany + ", supplierPhoneNo="
				+ supplierPhoneNo + ", supplierEmail=" + supplierEmail + ", startDate=" + startDate + ", endDate="
				+ endDate + ", particulars=" + particulars + ", debit=" + debit + ", credit=" + credit
				+ ", closingBalance=" + closingBalance + ", openingAmount=" + openingAmount + ", pendingAmount="
				+ pendingAmount + "]";
	}
	
	

}
