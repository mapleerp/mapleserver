package com.maple.restserver.report.entity;

import java.util.Date;

public class SupplierBillwiseDueDayReport {

	String supplierName;
	String supplierCompany;
	String supplierPhoneNo;
	String supplierGST;
	String supplierEmail;
	String refNo;
	Date date;
	Double openingAmount;
	Double closingAmount;

	Double dueAmount;
	Double pendingAmount;
	Date dueDate;

	Double overDueByDays;
	

	
	public Double getClosingAmount() {
		return closingAmount;
	}

	public void setClosingAmount(Double closingAmount) {
		this.closingAmount = closingAmount;
	}

	public Double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierCompany() {
		return supplierCompany;
	}

	public void setSupplierCompany(String supplierCompany) {
		this.supplierCompany = supplierCompany;
	}

	public String getSupplierPhoneNo() {
		return supplierPhoneNo;
	}

	public void setSupplierPhoneNo(String supplierPhoneNo) {
		this.supplierPhoneNo = supplierPhoneNo;
	}

	public String getSupplierGST() {
		return supplierGST;
	}

	public void setSupplierGST(String supplierGST) {
		this.supplierGST = supplierGST;
	}

	public String getSupplierEmail() {
		return supplierEmail;
	}

	public void setSupplierEmail(String supplierEmail) {
		this.supplierEmail = supplierEmail;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getOpeningAmount() {
		return openingAmount;
	}

	public void setOpeningAmount(Double openingAmount) {
		this.openingAmount = openingAmount;
	}

	public Double getPendingAmount() {
		return pendingAmount;
	}

	public void setPendingAmount(Double pendingAmount) {
		this.pendingAmount = pendingAmount;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Double getOverDueByDays() {
		return overDueByDays;
	}

	public void setOverDueByDays(Double overDueByDays) {
		this.overDueByDays = overDueByDays;
	}
	
	
}
