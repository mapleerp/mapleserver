package com.maple.restserver.report.entity;

import java.util.Date;

public class ReorderReport {
	
	
	String date;
	String voucherNumber;

	
	String supplierName;
	String supplierAddress;
	String supplierPhn;
	String itemName;
	String unitName;

	Double itemQty;
	String branchCode;
	String branchName;
	String branchAddress;
	String branchPhone;
	String companyName;
	String branchEmail;
	String branchWebsite;
	
	String offerDescription;
	Double minimumQty;
	
	
	
	public String getOfferDescription() {
		return offerDescription;
	}
	public void setOfferDescription(String offerDescription) {
		this.offerDescription = offerDescription;
	}
	public Double getMinimumQty() {
		return minimumQty;
	}
	public void setMinimumQty(Double minimumQty) {
		this.minimumQty = minimumQty;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public void setItemQty(Double itemQty) {
		this.itemQty = itemQty;
	}
	
	public Double getItemQty() {
		return itemQty;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierAddress() {
		return supplierAddress;
	}
	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}
	public String getSupplierPhn() {
		return supplierPhn;
	}
	public void setSupplierPhn(String supplierPhn) {
		this.supplierPhn = supplierPhn;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getBranchPhone() {
		return branchPhone;
	}
	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	
	@Override
	public String toString() {
		return "ReorderReport [date=" + date + ", supplierName=" + supplierName + ", supplierAddress=" + supplierAddress
				+ ", supplierPhn=" + supplierPhn + ", itemName=" + itemName + ", itemQty=" + itemQty + ", branchCode="
				+ branchCode + ", branchName=" + branchName + ", branchAddress=" + branchAddress + ", branchPhone="
				+ branchPhone + ", companyName=" + companyName + ", branchEmail=" + branchEmail + ", branchWebsite="
				+ branchWebsite + "]";
	}
	
	
	
}
