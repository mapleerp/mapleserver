package com.maple.restserver.report.entity;

import java.util.Date;

public class CardReconcileReport {
	
	Date voucherDate;
	String voucherNumber;
	String receiptMode;
	Double receiptAmount;
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public Double getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	@Override
	public String toString() {
		return "CardReconcileReport [voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber + ", receiptMode="
				+ receiptMode + ", receiptAmount=" + receiptAmount + "]";
	}
	
	
	

}
