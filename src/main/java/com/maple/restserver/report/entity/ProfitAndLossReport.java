package com.maple.restserver.report.entity;

import java.util.Date;

public class ProfitAndLossReport {

	Double openingStock;
	Double closingStock;
	Double openingValue;
	Double closingValue;
	Double totalSales;
	Double totalPurchase;
	Double totalProfit;
	private String branchName;
	public Double getOpeningStock() {
		return openingStock;
	}
	public void setOpeningStock(Double openingStock) {
		this.openingStock = openingStock;
	}
	public Double getClosingStock() {
		return closingStock;
	}
	public void setClosingStock(Double closingStock) {
		this.closingStock = closingStock;
	}
	public Double getOpeningValue() {
		return openingValue;
	}
	public void setOpeningValue(Double openingValue) {
		this.openingValue = openingValue;
	}
	public Double getClosingValue() {
		return closingValue;
	}
	public void setClosingValue(Double closingValue) {
		this.closingValue = closingValue;
	}
	public Double getTotalSales() {
		return totalSales;
	}
	public void setTotalSales(Double totalSales) {
		this.totalSales = totalSales;
	}
	public Double getTotalPurchase() {
		return totalPurchase;
	}
	public void setTotalPurchase(Double totalPurchase) {
		this.totalPurchase = totalPurchase;
	}
	
	public Double getTotalProfit() {
		return totalProfit;
	}
	public void setTotalProfit(Double totalProfit) {
		this.totalProfit = totalProfit;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	
}
