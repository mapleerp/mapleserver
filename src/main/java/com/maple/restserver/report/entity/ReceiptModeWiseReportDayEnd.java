package com.maple.restserver.report.entity;

public class ReceiptModeWiseReportDayEnd {
	
	//reanmesd becose of existing entity present

	String receiptMode;
	Double amount;
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "ReceiptModeWiseReport [receiptMode=" + receiptMode + ", amount=" + amount + "]";
	}
	
}
