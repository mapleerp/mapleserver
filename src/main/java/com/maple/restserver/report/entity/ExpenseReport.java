package com.maple.restserver.report.entity;

import java.util.Date;

public class ExpenseReport {
	String branchCode;
	String branchName;

	String accountId;
	String accountName;

	int slNo;
	String companyName;

	Date endDate;

	Double amount;
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public int getSlNo() {
		return slNo;
	}
	public void setSlNo(int slNo) {
		this.slNo = slNo;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "ExpenseReport [branchCode=" + branchCode + ", branchName=" + branchName + ", accountId=" + accountId
				+ ", accountName=" + accountName + ", slNo=" + slNo + ", companyName=" + companyName + ", endDate="
				+ endDate + ", amount=" + amount + "]";
	}
	
}
