package com.maple.restserver.report.entity;

import java.util.Date;

public class ConsumptionHdrReport {

	String voucherNumber;
	String reason;
	String voucherDate;
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}
	@Override
	public String toString() {
		return "ConsumptionHdrReport [voucherNumber=" + voucherNumber + ", reason=" + reason + ", voucherDate="
				+ voucherDate + "]";
	}
	
}
