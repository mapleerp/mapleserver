package com.maple.restserver.report.entity;

import java.util.Date;

public class ImportPurchaseSummaryReport {
	
    private String invoiceNumber;
	
	private String supplierName;
	
	private String supplierInvNo;
	
	private Date invoiceDate;
	
	private String  purchaseOrder;
	
	private Double  itemTotal;
	
	private Double itemTotalFc;
	
	private Double expense;
	
	private Double fcExpense;

	
	private Double notIncludedExp;
	
	private Double includedExp;
	
	private Double expenseTotal;
	
	private Double grandTotal;
	
	private Double importDuty;
	
	private String currencyName;

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierInvNo() {
		return supplierInvNo;
	}

	public void setSupplierInvNo(String supplierInvNo) {
		this.supplierInvNo = supplierInvNo;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public Double getItemTotal() {
		return itemTotal;
	}

	public void setItemTotal(Double itemTotal) {
		this.itemTotal = itemTotal;
	}

	public Double getItemTotalFc() {
		return itemTotalFc;
	}

	public void setItemTotalFc(Double itemTotalFc) {
		this.itemTotalFc = itemTotalFc;
	}

	public Double getExpense() {
		return expense;
	}

	public void setExpense(Double expense) {
		this.expense = expense;
	}

	public Double getFcExpense() {
		return fcExpense;
	}

	public void setFcExpense(Double fcExpense) {
		this.fcExpense = fcExpense;
	}

	public Double getNotIncludedExp() {
		return notIncludedExp;
	}

	public void setNotIncludedExp(Double notIncludedExp) {
		this.notIncludedExp = notIncludedExp;
	}

	public Double getIncludedExp() {
		return includedExp;
	}

	public void setIncludedExp(Double includedExp) {
		this.includedExp = includedExp;
	}

	public Double getExpenseTotal() {
		return expenseTotal;
	}

	public void setExpenseTotal(Double expenseTotal) {
		this.expenseTotal = expenseTotal;
	}

	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public Double getImportDuty() {
		return importDuty;
	}

	public void setImportDuty(Double importDuty) {
		this.importDuty = importDuty;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	@Override
	public String toString() {
		return "ImportPurchaseSummaryReport [invoiceNumber=" + invoiceNumber + ", supplierName=" + supplierName
				+ ", supplierInvNo=" + supplierInvNo + ", invoiceDate=" + invoiceDate + ", purchaseOrder="
				+ purchaseOrder + ", itemTotal=" + itemTotal + ", itemTotalFc=" + itemTotalFc + ", expense=" + expense
				+ ", fcExpense=" + fcExpense + ", notIncludedExp=" + notIncludedExp + ", includedExp=" + includedExp
				+ ", expenseTotal=" + expenseTotal + ", grandTotal=" + grandTotal + ", importDuty=" + importDuty
				+ ", currencyName=" + currencyName + "]";
	}
	
	

}
