package com.maple.restserver.report.entity;

import java.util.Date;

public class TallyImportReport {

	//--------
	String voucherNumber;
	String invoiceDate;
	String voucherType;
	String customerName;
	String addressLine1;
	String addressLine2;
	String gstRegType;
	String partyType;
	String gstNumber;
	String state;
	String country;
	String bankName;
	String bankAccountName;
	String bankIfsc;
    String dateOfSupply;
	String vehicleNo;
	String driverName;
	String itemName;
	String group;
	String description;

	Double rateBeforeDiscount;
	String taxability;
	Double quantity;
	String unit;
	String uqc;
	Double rate;
	Double value;
	Double discount;
	Double taxable;
	String hsncode;
	Double tax;
	Double itemcgst;
	Double itemsgst;
	Double itemigst;
	Double keralaFloodCess;
	Double roundOff;
	Double netValue;
	
	Double totalAnount;
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Double getRateBeforeDiscount() {
		return rateBeforeDiscount;
	}
	public void setRateBeforeDiscount(Double rateBeforeDiscount) {
		this.rateBeforeDiscount = rateBeforeDiscount;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	
	public String getGstRegType() {
		return gstRegType;
	}
	public void setGstRegType(String gstRegType) {
		this.gstRegType = gstRegType;
	}
	public String getPartyType() {
		return partyType;
	}
	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}
	public String getGstNumber() {
		return gstNumber;
	}
	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccountName() {
		return bankAccountName;
	}
	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}
	public String getBankIfsc() {
		return bankIfsc;
	}
	public void setBankIfsc(String bankIfsc) {
		this.bankIfsc = bankIfsc;
	}
	

	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getDriverName() {
		return driverName;
	}
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	public String getTaxability() {
		return taxability;
	}
	public void setTaxability(String taxability) {
		this.taxability = taxability;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getUqc() {
		return uqc;
	}
	public void setUqc(String uqc) {
		this.uqc = uqc;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	
	

	public Double getTaxable() {
		return taxable;
	}
	public void setTaxable(Double taxable) {
		this.taxable = taxable;
	}
	public String getHsncode() {
		return hsncode;
	}
	public void setHsncode(String hsncode) {
		this.hsncode = hsncode;
	}

	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public Double getItemcgst() {
		return itemcgst;
	}
	public void setItemcgst(Double itemcgst) {
		this.itemcgst = itemcgst;
	}
	public Double getItemsgst() {
		return itemsgst;
	}
	public void setItemsgst(Double itemsgst) {
		this.itemsgst = itemsgst;
	}
	public Double getItemigst() {
		return itemigst;
	}
	public void setItemigst(Double itemigst) {
		this.itemigst = itemigst;
	}
	public Double getKeralaFloodCess() {
		return keralaFloodCess;
	}
	public void setKeralaFloodCess(Double keralaFloodCess) {
		this.keralaFloodCess = keralaFloodCess;
	}
	public Double getRoundOff() {
		return roundOff;
	}
	public void setRoundOff(Double roundOff) {
		this.roundOff = roundOff;
	}
	public Double getNetValue() {
		return netValue;
	}
	public void setNetValue(Double netValue) {
		this.netValue = netValue;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getDateOfSupply() {
		return dateOfSupply;
	}
	public void setDateOfSupply(String dateOfSupply) {
		this.dateOfSupply = dateOfSupply;
	}
	public Double getTotalAnount() {
		return totalAnount;
	}
	public void setTotalAnount(Double totalAnount) {
		this.totalAnount = totalAnount;
	}
	@Override
	public String toString() {
		return "TallyImportReport [voucherNumber=" + voucherNumber + ", invoiceDate=" + invoiceDate + ", voucherType="
				+ voucherType + ", customerName=" + customerName + ", addressLine1=" + addressLine1 + ", addressLine2="
				+ addressLine2 + ", gstRegType=" + gstRegType + ", partyType=" + partyType + ", gstNumber=" + gstNumber
				+ ", state=" + state + ", country=" + country + ", bankName=" + bankName + ", bankAccountName="
				+ bankAccountName + ", bankIfsc=" + bankIfsc + ", dateOfSupply=" + dateOfSupply + ", vehicleNo="
				+ vehicleNo + ", driverName=" + driverName + ", itemName=" + itemName + ", group=" + group
				+ ", description=" + description + ", taxability=" + taxability + ", quantity=" + quantity + ", unit="
				+ unit + ", uqc=" + uqc + ", rate=" + rate + ", value=" + value + ", discount=" + discount
				+ ", taxable=" + taxable + ", hsncode=" + hsncode + ", tax=" + tax + ", itemcgst=" + itemcgst
				+ ", itemsgst=" + itemsgst + ", itemigst=" + itemigst + ", keralaFloodCess=" + keralaFloodCess
				+ ", roundOff=" + roundOff + ", netValue=" + netValue + ", totalAnount=" + totalAnount + "]";
	}
	

	
	
	
	
	
	
	 
	 
	
}
