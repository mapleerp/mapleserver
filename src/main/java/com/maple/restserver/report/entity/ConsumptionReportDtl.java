package com.maple.restserver.report.entity;

public class ConsumptionReportDtl {
	String itemId;
	String unitId;
	Double mrp;
	Double qty;
	Double amount;
	String batch;

	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	@Override
	public String toString() {
		return "ConsumptionReportDtl [itemId=" + itemId + ", unitId=" + unitId + ", mrp=" + mrp + ", qty=" + qty
				+ ", amount=" + amount + ", batch=" + batch + "]";
	}

	
}
