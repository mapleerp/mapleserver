package com.maple.restserver.report.entity;

public class StockValueReports {
	
	String itemName;
	Double closingQty;
	Double closingValue;
	Double dicount;
	Double mrp;
	Double costprice;
	
	
	public Double getDicount() {
		return dicount;
	}
	public void setDicount(Double dicount) {
		this.dicount = dicount;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getCostprice() {
		return costprice;
	}
	public void setCostprice(Double costprice) {
		this.costprice = costprice;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getClosingQty() {
		return closingQty;
	}
	public void setClosingQty(Double closingQty) {
		this.closingQty = closingQty;
	}
	public Double getClosingValue() {
		return closingValue;
	}
	public void setClosingValue(Double closingValue) {
		this.closingValue = closingValue;
	}
	@Override
	public String toString() {
		return "StockValueReports [itemName=" + itemName + ", closingQty=" + closingQty + ", closingValue="
				+ closingValue + ", dicount=" + dicount + ", mrp=" + mrp + ", costprice=" + costprice + "]";
	}
	
	
	

}
