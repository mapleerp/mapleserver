package com.maple.restserver.report.entity;

import java.sql.Date;

public class DailySalesSummaryReport {
	   private String id;
	   String openingBillNo;
		String closingBillNo;
		Integer totalBillNo;
		Double openingPettyCash;
		Double cashReceived;
		Double expensePaid;
		Double closingBalance;
		Double cashSalesOrder;
		Double onlineSalesOrder;
		Double debitCardSalesOrder;
		String customername;
		Double amount;
		Double b2BSales;
		Double uberSales;
		Double sWiggySales;
		Double zomatoSales;
		Double foodPandaSales;
		Double cashSales;
		Double creditSales;
		Double creditCardSales;
		Double SodexoSales;
		Double paytmSales;
		Double previousAdvance;
		 Double totalCashSales;
	    Double totalSalesOrder;
		Date reportDate;
		String accountId;
		String remark;
		Double amount1;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getOpeningBillNo() {
			return openingBillNo;
		}
		public void setOpeningBillNo(String openingBillNo) {
			this.openingBillNo = openingBillNo;
		}
		public String getClosingBillNo() {
			return closingBillNo;
		}
		public void setClosingBillNo(String closingBillNo) {
			this.closingBillNo = closingBillNo;
		}
		public Integer getTotalBillNo() {
			return totalBillNo;
		}
		public void setTotalBillNo(Integer totalBillNo) {
			this.totalBillNo = totalBillNo;
		}
		public Double getOpeningPettyCash() {
			return openingPettyCash;
		}
		public void setOpeningPettyCash(Double openingPettyCash) {
			this.openingPettyCash = openingPettyCash;
		}
		public Double getCashReceived() {
			return cashReceived;
		}
		public void setCashReceived(Double cashReceived) {
			this.cashReceived = cashReceived;
		}
		public Double getExpensePaid() {
			return expensePaid;
		}
		public void setExpensePaid(Double expensePaid) {
			this.expensePaid = expensePaid;
		}
		public Double getClosingBalance() {
			return closingBalance;
		}
		public void setClosingBalance(Double closingBalance) {
			this.closingBalance = closingBalance;
		}
		public Double getCashSalesOrder() {
			return cashSalesOrder;
		}
		public void setCashSalesOrder(Double cashSalesOrder) {
			this.cashSalesOrder = cashSalesOrder;
		}
		public Double getOnlineSalesOrder() {
			return onlineSalesOrder;
		}
		public void setOnlineSalesOrder(Double onlineSalesOrder) {
			this.onlineSalesOrder = onlineSalesOrder;
		}
		public Double getDebitCardSalesOrder() {
			return debitCardSalesOrder;
		}
		public void setDebitCardSalesOrder(Double debitCardSalesOrder) {
			this.debitCardSalesOrder = debitCardSalesOrder;
		}
		public String getCustomername() {
			return customername;
		}
		public void setCustomername(String customername) {
			this.customername = customername;
		}
		public Double getAmount() {
			return amount;
		}
		public void setAmount(Double amount) {
			this.amount = amount;
		}
		public Double getB2BSales() {
			return b2BSales;
		}
		public void setB2BSales(Double b2bSales) {
			b2BSales = b2bSales;
		}
		public Double getUberSales() {
			return uberSales;
		}
		public void setUberSales(Double uberSales) {
			this.uberSales = uberSales;
		}
		public Double getsWiggySales() {
			return sWiggySales;
		}
		public void setsWiggySales(Double sWiggySales) {
			this.sWiggySales = sWiggySales;
		}
		public Double getZomatoSales() {
			return zomatoSales;
		}
		public void setZomatoSales(Double zomatoSales) {
			this.zomatoSales = zomatoSales;
		}
		public Double getFoodPandaSales() {
			return foodPandaSales;
		}
		public void setFoodPandaSales(Double foodPandaSales) {
			this.foodPandaSales = foodPandaSales;
		}
		public Double getCashSales() {
			return cashSales;
		}
		public void setCashSales(Double cashSales) {
			this.cashSales = cashSales;
		}
		public Double getCreditSales() {
			return creditSales;
		}
		public void setCreditSales(Double creditSales) {
			this.creditSales = creditSales;
		}
		public Double getCreditCardSales() {
			return creditCardSales;
		}
		public void setCreditCardSales(Double creditCardSales) {
			this.creditCardSales = creditCardSales;
		}
		public Double getSodexoSales() {
			return SodexoSales;
		}
		public void setSodexoSales(Double sodexoSales) {
			SodexoSales = sodexoSales;
		}
		public Double getPaytmSales() {
			return paytmSales;
		}
		public void setPaytmSales(Double paytmSales) {
			this.paytmSales = paytmSales;
		}
		public Double getPreviousAdvance() {
			return previousAdvance;
		}
		public void setPreviousAdvance(Double previousAdvance) {
			this.previousAdvance = previousAdvance;
		}
		public Double getTotalCashSales() {
			return totalCashSales;
		}
		public void setTotalCashSales(Double totalCashSales) {
			this.totalCashSales = totalCashSales;
		}
		public Double getTotalSalesOrder() {
			return totalSalesOrder;
		}
		public void setTotalSalesOrder(Double totalSalesOrder) {
			this.totalSalesOrder = totalSalesOrder;
		}
		public Date getReportDate() {
			return reportDate;
		}
		public void setReportDate(Date reportDate) {
			this.reportDate = reportDate;
		}
		public String getAccountId() {
			return accountId;
		}
		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}
		public String getRemark() {
			return remark;
		}
		public void setRemark(String remark) {
			this.remark = remark;
		}
		public Double getAmount1() {
			return amount1;
		}
		public void setAmount1(Double amount1) {
			this.amount1 = amount1;
		}
		
}
