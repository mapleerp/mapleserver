package com.maple.restserver.report.entity;




public class ProductionPreplaninngDtlReport

{

	
    private String id;
	private  String itemId;
	private String batch;
	private Double qty;
	private String status;
	private String itemName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	@Override
	public String toString() {
		return "ProductionPrePlaningDtlReport [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", qty=" + qty
				+ ", status=" + status + ", itemName=" + itemName + "]";
	}

	
}
