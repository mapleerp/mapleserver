package com.maple.restserver.report.entity;

public class RemainingEntity {

	Double reqQty;
	String itemId;
	Double machineRemainingCapacity;
	Double RMW;
	String batch;
	public Double getReqQty() {
		return reqQty;
	}
	public void setReqQty(Double reqQty) {
		this.reqQty = reqQty;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getMachineRemainingCapacity() {
		return machineRemainingCapacity;
	}
	public void setMachineRemainingCapacity(Double machineRemainingCapacity) {
		this.machineRemainingCapacity = machineRemainingCapacity;
	}
	public Double getRMW() {
		return RMW;
	}
	public void setRMW(Double rMW) {
		RMW = rMW;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	@Override
	public String toString() {
		return "RemainingEntity [reqQty=" + reqQty + ", itemId=" + itemId + ", machineRemainingCapacity="
				+ machineRemainingCapacity + ", RMW=" + RMW + ", batch=" + batch + "]";
	}
	
}
