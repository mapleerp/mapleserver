package com.maple.restserver.report.entity;

public class SalesAndStockConsolidatedReport {
	
	String itemName;
	String branchName;
	Double openingBalance;
	Double stockIn;
	Double stockOut;
	Double sales;
	Double balanceQty;
	Double balanceAmount;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public Double getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(Double openingBalance) {
		this.openingBalance = openingBalance;
	}
	public Double getStockIn() {
		return stockIn;
	}
	public void setStockIn(Double stockIn) {
		this.stockIn = stockIn;
	}
	public Double getStockOut() {
		return stockOut;
	}
	public void setStockOut(Double stockOut) {
		this.stockOut = stockOut;
	}
	public Double getSales() {
		return sales;
	}
	public void setSales(Double sales) {
		this.sales = sales;
	}
	public Double getBalanceQty() {
		return balanceQty;
	}
	public void setBalanceQty(Double balanceQty) {
		this.balanceQty = balanceQty;
	}
	public Double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	@Override
	public String toString() {
		return "SalesAndStockConsolidatedReport [itemName=" + itemName + ", branchName=" + branchName
				+ ", openingBalance=" + openingBalance + ", stockIn=" + stockIn + ", stockOut=" + stockOut + ", sales="
				+ sales + ", balanceQty=" + balanceQty + ", balanceAmount=" + balanceAmount + "]";
	}
	
	

}
