package com.maple.restserver.report.entity;

import java.io.Serializable;
import java.util.Date;

public class ZippedDB  implements Serializable{
	
	private static final long serialVersionUID = 1L;
 
	String dbName;
	byte[] db;
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public byte[] getDb() {
		return db;
	}
	public void setDb(byte[] db) {
		this.db = db;
	}
	
	

}
