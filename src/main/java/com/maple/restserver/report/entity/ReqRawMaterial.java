package com.maple.restserver.report.entity;

public class ReqRawMaterial {

	
String itemName;
Double currentStock;
Double requiredStock;
String unit;
Double reqQty;

public String getUnit() {
	return unit;
}
public void setUnit(String unit) {
	this.unit = unit;
}
public Double getReqQty() {
	return reqQty;
}
public void setReqQty(Double reqQty) {
	this.reqQty = reqQty;
}
public String getItemName() {
	return itemName;
}
public void setItemName(String itemName) {
	this.itemName = itemName;
}
public Double getCurrentStock() {
	return currentStock;
}
public void setCurrentStock(Double currentStock) {
	this.currentStock = currentStock;
}
public Double getRequiredStock() {
	return requiredStock;
}
public void setRequiredStock(Double requiredStock) {
	this.requiredStock = requiredStock;
}

}
