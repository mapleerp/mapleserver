package com.maple.restserver.report.entity;

public class DayEndPettyCashPaymentAndReceipt {

	Double  pettyCasReceipt;
	Double pettyCashPayment;
	public Double getPettyCasReceipt() {
		return pettyCasReceipt;
	}
	public void setPettyCasReceipt(Double pettyCasReceipt) {
		this.pettyCasReceipt = pettyCasReceipt;
	}
	public Double getPettyCashPayment() {
		return pettyCashPayment;
	}
	public void setPettyCashPayment(Double pettyCashPayment) {
		this.pettyCashPayment = pettyCashPayment;
	}
	@Override
	public String toString() {
		return "DayEndPettyCashPaymentAndReceipt [pettyCasReceipt=" + pettyCasReceipt + ", pettyCashPayment="
				+ pettyCashPayment + "]";
	}
	
	
	
}
