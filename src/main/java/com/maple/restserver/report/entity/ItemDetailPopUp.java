package com.maple.restserver.report.entity;

import java.util.Date;

public class ItemDetailPopUp {

	String itemName;
	String barcode;
	String unitName;
	Double standardPrice;
	Double qty;
	Double cess;
	Double taxRate;
	String itemId;
	String unitId;
	String batch;
	String itemCode;
	Date expDate;
	String itemPriceLock;
	String store;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getStandardPrice() {
		return standardPrice;
	}
	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getCess() {
		return cess;
	}
	public void setCess(Double cess) {
		this.cess = cess;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public String getItemPriceLock() {
		return itemPriceLock;
	}
	public void setItemPriceLock(String itemPriceLock) {
		this.itemPriceLock = itemPriceLock;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	@Override
	public String toString() {
		return "ItemDetailPopUp [itemName=" + itemName + ", barcode=" + barcode + ", unitName=" + unitName
				+ ", standardPrice=" + standardPrice + ", qty=" + qty + ", cess=" + cess + ", taxRate=" + taxRate
				+ ", itemId=" + itemId + ", unitId=" + unitId + ", batch=" + batch + ", itemCode=" + itemCode
				+ ", expDate=" + expDate + ", itemPriceLock=" + itemPriceLock + ", store=" + store + "]";
	}
	
	
}
