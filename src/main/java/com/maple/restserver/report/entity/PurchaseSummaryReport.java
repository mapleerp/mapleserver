package com.maple.restserver.report.entity;

import java.math.BigDecimal;
import java.time.Month;
import java.time.Year;

public class PurchaseSummaryReport {

	private String branchCode;
	private BigDecimal branchPurchaseAmount;
	private BigDecimal companyPurchaseAmount;
	private Month voucherDate;
	
	private Year yearOfPurchease;
	
	public BigDecimal getBranchPurchaseAmount() {
		return branchPurchaseAmount;
	}
	public void setBranchPurchaseAmount(BigDecimal branchPurchaseAmount) {
		this.branchPurchaseAmount = branchPurchaseAmount;
	}
	public BigDecimal getCompanyPurchaseAmount() {
		return companyPurchaseAmount;
	}
	public void setCompanyPurchaseAmount(BigDecimal companyPurchaseAmount) {
		this.companyPurchaseAmount = companyPurchaseAmount;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	
	public Month getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Month voucherDate) {
		this.voucherDate = voucherDate;
	}
	public Year getYearOfPurchease() {
		return yearOfPurchease;
	}
	public void setYearOfPurchease(Year yearOfPurchease) {
		this.yearOfPurchease = yearOfPurchease;
	}
	@Override
	public String toString() {
		return "PurchaseSummaryReport [branchCode=" + branchCode + ", branchPurchaseAmount=" + branchPurchaseAmount
				+ ", companyPurchaseAmount=" + companyPurchaseAmount + ", voucherDate=" + voucherDate
				+ ", yearOfPurchease=" + yearOfPurchease + "]";
	}
	
	
	
}
