package com.maple.restserver.report.entity;

import java.util.Date;

public class QuotationReport {
	private String quotationNumber;
	private String supplierName;
	private String validUpTo;
	private Double purchaseQty;
	private Double unitRate;
	private String contactPerson;
	private Double mrp;
	private Double margin;
	private String itemName;
	private Date validFrom;
	private Date validTo;


	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getValidUpTo() {
		return validUpTo;
	}
	public void setValidUpTo(String validUpTo) {
		this.validUpTo = validUpTo;
	}
	public Double getPurchaseQty() {
		return purchaseQty;
	}
	public void setPurchaseQty(Double purchaseQty) {
		this.purchaseQty = purchaseQty;
	}
	public Double getUnitRate() {
		return unitRate;
	}
	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getMargin() {
		return margin;
	}
	public void setMargin(Double margin) {
		this.margin = margin;
	}
	
	public Date getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	public Date getValidTo() {
		return validTo;
	}
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	public String getQuotationNumber() {
		return quotationNumber;
	}
	public void setQuotationNumber(String quotationNumber) {
		this.quotationNumber = quotationNumber;
	}
	@Override
	public String toString() {
		return "QuotationReport [quotationNumber=" + quotationNumber + ", supplierName=" + supplierName + ", validUpTo="
				+ validUpTo + ", purchaseQty=" + purchaseQty + ", unitRate=" + unitRate + ", contactPerson="
				+ contactPerson + ", mrp=" + mrp + ", margin=" + margin + ", itemName=" + itemName + ", validFrom="
				+ validFrom + ", validTo=" + validTo + "]";
	}
	

	
}
