package com.maple.restserver.report.entity;

import java.util.Date;

public class DayEndWebReport {

	String branchCode;
	Integer posOpeningBillNo;
	Integer posClosingBillNo;
	Integer posTotalBillNo;
	Integer wholesaleOpeningBillNo;
	Integer wholesaleClosingBillNo;
	Integer wholesTotalBillNo;
	Double cashSaleOrder;
	Double cardSaleOrder;
	Double cashSales;

	Double creditSales;
	Double cardSales;
	Double paytmSales;
	Double gpaySales;
	Double ebsSales;
	Double swiggySales;
	Double zomatoSales;
	Double yesBank;
	Double sbi;
	Double totalSales;
	Double closingPettyCashBalance;
	Double closingCashBalance;
	Double totalCash;
	Double totalCashAtDrawer;
	Double cashVarience;

	Double previousCashSaleOrder;
	Double todaysCreditConvertToCash;
	Double neftSales;
	Double cashSalePlusTodaysCashSOPlusCreditToCashMinusPreviousSO;

	Double physicalCash;
	Double card2;
	
	String userName;


	public Double getCashSalePlusTodaysCashSOPlusCreditToCashMinusPreviousSO() {
		return cashSalePlusTodaysCashSOPlusCreditToCashMinusPreviousSO;
	}

	public void setCashSalePlusTodaysCashSOPlusCreditToCashMinusPreviousSO(
			Double cashSalePlusTodaysCashSOPlusCreditToCashMinusPreviousSO) {
		this.cashSalePlusTodaysCashSOPlusCreditToCashMinusPreviousSO = cashSalePlusTodaysCashSOPlusCreditToCashMinusPreviousSO;
	}

	public Double getNeftSales() {
		return neftSales;
	}

	public void setNeftSales(Double neftSales) {
		this.neftSales = neftSales;
	}

	public Double getPreviousCashSaleOrder() {
		return previousCashSaleOrder;
	}

	public void setPreviousCashSaleOrder(Double previousCashSaleOrder) {
		this.previousCashSaleOrder = previousCashSaleOrder;
	}

	public Double getTodaysCreditConvertToCash() {
		return todaysCreditConvertToCash;
	}

	public void setTodaysCreditConvertToCash(Double todaysCreditConvertToCash) {
		this.todaysCreditConvertToCash = todaysCreditConvertToCash;
	}

	public Integer getPosOpeningBillNo() {
		return posOpeningBillNo;
	}

	public void setPosOpeningBillNo(Integer posOpeningBillNo) {
		this.posOpeningBillNo = posOpeningBillNo;
	}

	public Integer getPosClosingBillNo() {
		return posClosingBillNo;
	}

	public void setPosClosingBillNo(Integer posClosingBillNo) {
		this.posClosingBillNo = posClosingBillNo;
	}

	public Integer getWholesaleOpeningBillNo() {
		return wholesaleOpeningBillNo;
	}

	public void setWholesaleOpeningBillNo(Integer wholesaleOpeningBillNo) {
		this.wholesaleOpeningBillNo = wholesaleOpeningBillNo;
	}

	public Integer getWholesaleClosingBillNo() {
		return wholesaleClosingBillNo;
	}

	public void setWholesaleClosingBillNo(Integer wholesaleClosingBillNo) {
		this.wholesaleClosingBillNo = wholesaleClosingBillNo;
	}

	public Double getCashSaleOrder() {
		return cashSaleOrder;
	}

	public void setCashSaleOrder(Double cashSaleOrder) {
		this.cashSaleOrder = cashSaleOrder;
	}

	public Double getCardSaleOrder() {
		return cardSaleOrder;
	}

	public void setCardSaleOrder(Double cardSaleOrder) {
		this.cardSaleOrder = cardSaleOrder;
	}

	public Double getCashSales() {
		return cashSales;
	}

	public void setCashSales(Double cashSales) {
		this.cashSales = cashSales;
	}

	public Double getCreditSales() {
		return creditSales;
	}

	public void setCreditSales(Double creditSales) {
		this.creditSales = creditSales;
	}

	public Double getCardSales() {
		return cardSales;
	}

	public void setCardSales(Double cardSales) {
		this.cardSales = cardSales;
	}

	public Double getPaytmSales() {
		return paytmSales;
	}

	public void setPaytmSales(Double paytmSales) {
		this.paytmSales = paytmSales;
	}

	public Double getGpaySales() {
		return gpaySales;
	}

	public void setGpaySales(Double gpaySales) {
		this.gpaySales = gpaySales;
	}

	public Double getEbsSales() {
		return ebsSales;
	}

	public void setEbsSales(Double ebsSales) {
		this.ebsSales = ebsSales;
	}

	public Double getSwiggySales() {
		return swiggySales;
	}

	public void setSwiggySales(Double swiggySales) {
		this.swiggySales = swiggySales;
	}

	public Double getZomatoSales() {
		return zomatoSales;
	}

	public void setZomatoSales(Double zomatoSales) {
		this.zomatoSales = zomatoSales;
	}

	public Double getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(Double totalSales) {
		this.totalSales = totalSales;
	}

	public Double getClosingPettyCashBalance() {
		return closingPettyCashBalance;
	}

	public void setClosingPettyCashBalance(Double closingPettyCashBalance) {
		this.closingPettyCashBalance = closingPettyCashBalance;
	}

	public Double getClosingCashBalance() {
		return closingCashBalance;
	}

	public void setClosingCashBalance(Double closingCashBalance) {
		this.closingCashBalance = closingCashBalance;
	}

	public Double getTotalCash() {
		return totalCash;
	}

	public void setTotalCash(Double totalCash) {
		this.totalCash = totalCash;
	}

	public Double getTotalCashAtDrawer() {
		return totalCashAtDrawer;
	}

	public void setTotalCashAtDrawer(Double totalCashAtDrawer) {
		this.totalCashAtDrawer = totalCashAtDrawer;
	}

	public Double getCashVarience() {
		return cashVarience;
	}

	public void setCashVarience(Double cashVarience) {
		this.cashVarience = cashVarience;
	}

	public Integer getPosTotalBillNo() {
		return posTotalBillNo;
	}

	public void setPosTotalBillNo(Integer posTotalBillNo) {
		this.posTotalBillNo = posTotalBillNo;
	}

	public Integer getWholesTotalBillNo() {
		return wholesTotalBillNo;
	}

	public void setWholesTotalBillNo(Integer wholesTotalBillNo) {
		this.wholesTotalBillNo = wholesTotalBillNo;
	}

	public Double getYesBank() {
		return yesBank;
	}

	public void setYesBank(Double yesBank) {
		this.yesBank = yesBank;
	}

	public Double getSbi() {
		return sbi;
	}

	public void setSbi(Double sbi) {
		this.sbi = sbi;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public Double getPhysicalCash() {
		return physicalCash;
	}

	public void setPhysicalCash(Double physicalCash) {
		this.physicalCash = physicalCash;
	}

	public Double getCard2() {
		return card2;
	}

	public void setCard2(Double card2) {
		this.card2 = card2;
	}

	
	
	@Override
	public String toString() {
		return "DayEndWebReport [branchCode=" + branchCode + ", posOpeningBillNo=" + posOpeningBillNo
				+ ", posClosingBillNo=" + posClosingBillNo + ", posTotalBillNo=" + posTotalBillNo
				+ ", wholesaleOpeningBillNo=" + wholesaleOpeningBillNo + ", wholesaleClosingBillNo="
				+ wholesaleClosingBillNo + ", wholesTotalBillNo=" + wholesTotalBillNo + ", cashSaleOrder="
				+ cashSaleOrder + ", cardSaleOrder=" + cardSaleOrder + ", cashSales=" + cashSales + ", creditSales="
				+ creditSales + ", cardSales=" + cardSales + ", paytmSales=" + paytmSales + ", gpaySales=" + gpaySales
				+ ", ebsSales=" + ebsSales + ", swiggySales=" + swiggySales + ", zomatoSales=" + zomatoSales
				+ ", yesBank=" + yesBank + ", sbi=" + sbi + ", totalSales=" + totalSales + ", closingPettyCashBalance="
				+ closingPettyCashBalance + ", closingCashBalance=" + closingCashBalance + ", totalCash=" + totalCash
				+ ", totalCashAtDrawer=" + totalCashAtDrawer + ", cashVarience=" + cashVarience
				+ ", previousCashSaleOrder=" + previousCashSaleOrder + ", todaysCreditConvertToCash="
				+ todaysCreditConvertToCash + ", neftSales=" + neftSales
				+ ", cashSalePlusTodaysCashSOPlusCreditToCashMinusPreviousSO="
				+ cashSalePlusTodaysCashSOPlusCreditToCashMinusPreviousSO + ", physicalCash=" + physicalCash
				+ ", card2=" + card2 + "]";
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	

}
