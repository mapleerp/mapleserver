package com.maple.restserver.report.entity;


import org.springframework.stereotype.Component;

@Component
public class RetailSalesDetailReport {

	String invoiceDate;
	String voucherNum;
	String customerName;
	String patientName;
	String patientID;
	String itemName;
	String groupName;
	String expiryDate;
	String batchCode;
	String itemCode;
	Double quantity;
	Double rate;
	Double taxRate;
	Double saleValue;
	Double cost;
	Double costValue;
	
	
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getVoucherNum() {
		return voucherNum;
	}
	public void setVoucherNum(String voucherNum) {
		this.voucherNum = voucherNum;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getPatientID() {
		return patientID;
	}
	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getSaleValue() {
		return saleValue;
	}
	public void setSaleValue(Double saleValue) {
		this.saleValue = saleValue;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public Double getCostValue() {
		return costValue;
	}
	public void setCostValue(Double costValue) {
		this.costValue = costValue;
	}
	@Override
	public String toString() {
		return "RetailSalesDetailReport [invoiceDate=" + invoiceDate + ", voucherNum=" + voucherNum + ", customerName="
				+ customerName + ", patientName=" + patientName + ", patientID=" + patientID + ", itemName=" + itemName
				+ ", groupName=" + groupName + ", expiryDate=" + expiryDate + ", batchCode=" + batchCode + ", itemCode="
				+ itemCode + ", quantity=" + quantity + ", rate=" + rate + ", taxRate=" + taxRate + ", saleValue="
				+ saleValue + ", cost=" + cost + ", costValue=" + costValue + "]";
	}
	
	
}
