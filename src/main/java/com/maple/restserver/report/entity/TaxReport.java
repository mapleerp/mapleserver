package com.maple.restserver.report.entity;

public class TaxReport {

	Double salesTaxRate;
	Double salesSgstAmount;
	Double salesCgstAmount;
    Double salesIgstAmount;
    Double salesCessAmount;
    Double totalSaleTaxAmount;
    Double salesTaxAssesableAmount;
    Double netSalesAmount;
    
	
	Double purchaseTaxRate; 
	Double purchaseSgstAmount; 
	Double purchaseCgstAmount;
	Double purchaseIgstAmount; 
	Double purchaseCessAmount; 
	Double totalPurchaseTaxAmount;
	Double purchaseTaxAssesableAmount; 
	Double netPurchaseAmount;
	 

	Double finalTaxRate; 
	Double finalSgstAmount; 
	Double finalCgstAmount;
	Double finalIgstAmount; 
	Double finalCessAmount; 
	Double totalFinalTaxAmount;
	Double finalTaxAssesableAmount; 
	Double netFinalAmount;
	
	
	public Double getSalesTaxRate() {
		return salesTaxRate;
	}
	public void setSalesTaxRate(Double salesTaxRate) {
		this.salesTaxRate = salesTaxRate;
	}
	public Double getSalesSgstAmount() {
		return salesSgstAmount;
	}
	public void setSalesSgstAmount(Double salesSgstAmount) {
		this.salesSgstAmount = salesSgstAmount;
	}
	public Double getSalesCgstAmount() {
		return salesCgstAmount;
	}
	public void setSalesCgstAmount(Double salesCgstAmount) {
		this.salesCgstAmount = salesCgstAmount;
	}
	public Double getSalesIgstAmount() {
		return salesIgstAmount;
	}
	public void setSalesIgstAmount(Double salesIgstAmount) {
		this.salesIgstAmount = salesIgstAmount;
	}
	public Double getSalesCessAmount() {
		return salesCessAmount;
	}
	public void setSalesCessAmount(Double salesCessAmount) {
		this.salesCessAmount = salesCessAmount;
	}
	public Double getTotalSaleTaxAmount() {
		return totalSaleTaxAmount;
	}
	public void setTotalSaleTaxAmount(Double totalSaleTaxAmount) {
		this.totalSaleTaxAmount = totalSaleTaxAmount;
	}
	public Double getSalesTaxAssesableAmount() {
		return salesTaxAssesableAmount;
	}
	public void setSalesTaxAssesableAmount(Double salesTaxAssesableAmount) {
		this.salesTaxAssesableAmount = salesTaxAssesableAmount;
	}
	public Double getNetSalesAmount() {
		return netSalesAmount;
	}
	public void setNetSalesAmount(Double netSalesAmount) {
		this.netSalesAmount = netSalesAmount;
	}
	
	public Double getPurchaseSgstAmount() {
		return purchaseSgstAmount;
	}
	public void setPurchaseSgstAmount(Double purchaseSgstAmount) {
		this.purchaseSgstAmount = purchaseSgstAmount;
	}
	public Double getPurchaseCgstAmount() {
		return purchaseCgstAmount;
	}
	public void setPurchaseCgstAmount(Double purchaseCgstAmount) {
		this.purchaseCgstAmount = purchaseCgstAmount;
	}
	public Double getPurchaseIgstAmount() {
		return purchaseIgstAmount;
	}
	public void setPurchaseIgstAmount(Double purchaseIgstAmount) {
		this.purchaseIgstAmount = purchaseIgstAmount;
	}
	public Double getPurchaseCessAmount() {
		return purchaseCessAmount;
	}
	public void setPurchaseCessAmount(Double purchaseCessAmount) {
		this.purchaseCessAmount = purchaseCessAmount;
	}
	public Double getTotalPurchaseTaxAmount() {
		return totalPurchaseTaxAmount;
	}
	public void setTotalPurchaseTaxAmount(Double totalPurchaseTaxAmount) {
		this.totalPurchaseTaxAmount = totalPurchaseTaxAmount;
	}
	public Double getPurchaseTaxAssesableAmount() {
		return purchaseTaxAssesableAmount;
	}
	public void setPurchaseTaxAssesableAmount(Double purchaseTaxAssesableAmount) {
		this.purchaseTaxAssesableAmount = purchaseTaxAssesableAmount;
	}
	public Double getNetPurchaseAmount() {
		return netPurchaseAmount;
	}
	public void setNetPurchaseAmount(Double netPurchaseAmount) {
		this.netPurchaseAmount = netPurchaseAmount;
	}

	public Double getFinalSgstAmount() {
		return finalSgstAmount;
	}
	public void setFinalSgstAmount(Double finalSgstAmount) {
		this.finalSgstAmount = finalSgstAmount;
	}
	public Double getFinalCgstAmount() {
		return finalCgstAmount;
	}
	public void setFinalCgstAmount(Double finalCgstAmount) {
		this.finalCgstAmount = finalCgstAmount;
	}
	public Double getFinalIgstAmount() {
		return finalIgstAmount;
	}
	public void setFinalIgstAmount(Double finalIgstAmount) {
		this.finalIgstAmount = finalIgstAmount;
	}
	public Double getFinalCessAmount() {
		return finalCessAmount;
	}
	public void setFinalCessAmount(Double finalCessAmount) {
		this.finalCessAmount = finalCessAmount;
	}
	public Double getTotalFinalTaxAmount() {
		return totalFinalTaxAmount;
	}
	public void setTotalFinalTaxAmount(Double totalFinalTaxAmount) {
		this.totalFinalTaxAmount = totalFinalTaxAmount;
	}
	public Double getFinalTaxAssesableAmount() {
		return finalTaxAssesableAmount;
	}
	public void setFinalTaxAssesableAmount(Double finalTaxAssesableAmount) {
		this.finalTaxAssesableAmount = finalTaxAssesableAmount;
	}
	public Double getNetFinalAmount() {
		return netFinalAmount;
	}
	public void setNetFinalAmount(Double netFinalAmount) {
		this.netFinalAmount = netFinalAmount;
	}
	public Double getPurchaseTaxRate() {
		return purchaseTaxRate;
	}
	public void setPurchaseTaxRate(Double purchaseTaxRate) {
		this.purchaseTaxRate = purchaseTaxRate;
	}
	public Double getFinalTaxRate() {
		return finalTaxRate;
	}
	public void setFinalTaxRate(Double finalTaxRate) {
		this.finalTaxRate = finalTaxRate;
	}
	@Override
	public String toString() {
		return "TaxReport [salesTaxRate=" + salesTaxRate + ", salesSgstAmount=" + salesSgstAmount + ", salesCgstAmount="
				+ salesCgstAmount + ", salesIgstAmount=" + salesIgstAmount + ", salesCessAmount=" + salesCessAmount
				+ ", totalSaleTaxAmount=" + totalSaleTaxAmount + ", salesTaxAssesableAmount=" + salesTaxAssesableAmount
				+ ", netSalesAmount=" + netSalesAmount + ", purchaseTaxRate=" + purchaseTaxRate
				+ ", purchaseSgstAmount=" + purchaseSgstAmount + ", purchaseCgstAmount=" + purchaseCgstAmount
				+ ", purchaseIgstAmount=" + purchaseIgstAmount + ", purchaseCessAmount=" + purchaseCessAmount
				+ ", totalPurchaseTaxAmount=" + totalPurchaseTaxAmount + ", purchaseTaxAssesableAmount="
				+ purchaseTaxAssesableAmount + ", netPurchaseAmount=" + netPurchaseAmount + ", finalTaxRate="
				+ finalTaxRate + ", finalSgstAmount=" + finalSgstAmount + ", finalCgstAmount=" + finalCgstAmount
				+ ", finalIgstAmount=" + finalIgstAmount + ", finalCessAmount=" + finalCessAmount
				+ ", totalFinalTaxAmount=" + totalFinalTaxAmount + ", finalTaxAssesableAmount="
				+ finalTaxAssesableAmount + ", netFinalAmount=" + netFinalAmount + "]";
	}
	
	 
    
	
	
    
    

	
	
	
}
