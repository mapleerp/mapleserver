package com.maple.restserver.report.entity;

public class AMDCDailySalesSummaryReport {
	
	String branch;
	Double totalCashSales;
	Double totalCardSales;
	Double totalCreditSales;
	Double totalBankSales;
	Double totalInsuranceSales;
	Double grandTotal;
	
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public Double getTotalCashSales() {
		return totalCashSales;
	}
	public void setTotalCashSales(Double totalCashSales) {
		this.totalCashSales = totalCashSales;
	}
	public Double getTotalCardSales() {
		return totalCardSales;
	}
	public void setTotalCardSales(Double totalCardSales) {
		this.totalCardSales = totalCardSales;
	}
	public Double getTotalCreditSales() {
		return totalCreditSales;
	}
	public void setTotalCreditSales(Double totalCreditSales) {
		this.totalCreditSales = totalCreditSales;
	}
	public Double getTotalBankSales() {
		return totalBankSales;
	}
	public void setTotalBankSales(Double totalBankSales) {
		this.totalBankSales = totalBankSales;
	}
	public Double getTotalInsuranceSales() {
		return totalInsuranceSales;
	}
	public void setTotalInsuranceSales(Double totalInsuranceSales) {
		this.totalInsuranceSales = totalInsuranceSales;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	

}
