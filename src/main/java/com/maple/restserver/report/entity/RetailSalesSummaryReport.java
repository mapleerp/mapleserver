package com.maple.restserver.report.entity;

import org.springframework.stereotype.Component;

@Component
public class RetailSalesSummaryReport {

	
	String invoiceDate;
	String voucherNum;
	String customerName;
	String patientName;
	String patientID;
	String doctorName;
	Double beforeGST;
	Double gst;
	Double invoiceAmount;
	Double totalPaid;
	Double insurance;
	Double credit;
	Double cardReceipt;
	Double cashReceipt;
	String user;
	
	
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getVoucherNum() {
		return voucherNum;
	}
	public void setVoucherNum(String voucherNum) {
		this.voucherNum = voucherNum;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getPatientID() {
		return patientID;
	}
	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public Double getBeforeGST() {
		return beforeGST;
	}
	public void setBeforeGST(Double beforeGST) {
		this.beforeGST = beforeGST;
	}
	public Double getGst() {
		return gst;
	}
	public void setGst(Double gst) {
		this.gst = gst;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public Double getTotalPaid() {
		return totalPaid;
	}
	public void setTotalPaid(Double totalPaid) {
		this.totalPaid = totalPaid;
	}
	public Double getInsurance() {
		return insurance;
	}
	public void setInsurance(Double insurance) {
		this.insurance = insurance;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	public Double getCardReceipt() {
		return cardReceipt;
	}
	public void setCardReceipt(Double cardReceipt) {
		this.cardReceipt = cardReceipt;
	}
	public Double getCashReceipt() {
		return cashReceipt;
	}
	public void setCashReceipt(Double cashReceipt) {
		this.cashReceipt = cashReceipt;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
	
	
}
