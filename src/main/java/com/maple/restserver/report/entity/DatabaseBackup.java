package com.maple.restserver.report.entity;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;


public class DatabaseBackup implements Serializable {
	private static final long serialVersionUID = 1L;
	String branchCode;
	String fileName;
	byte [] dbData;
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public byte[] getDbData() {
		return dbData;
	}
	public void setDbData(byte[] dbData) {
		this.dbData = dbData;
	}
	@Override
	public String toString() {
		return "DatabaseBackup [branchCode=" + branchCode + ", fileName=" + fileName + ", dbData="
				+ Arrays.toString(dbData) + "]";
	}
	
}
