package com.maple.restserver.report.entity;

import java.sql.Time;
import java.time.LocalDate;

public class WeighBridgeReport {
	
 String vehicleno;
 String netweight;
 Integer rate;
 LocalDate  voucherDate;
public String getVehicleno() {
	return vehicleno;
}
public void setVehicleno(String vehicleno) {
	this.vehicleno = vehicleno;
}
public String getNetweight() {
	return netweight;
}
public void setNetweight(String netweight) {
	this.netweight = netweight;
}
public Integer getRate() {
	return rate;
}
public void setRate(Integer rate) {
	this.rate = rate;
}



public LocalDate getVoucherDate() {
	return voucherDate;
}
public void setVoucherDate(LocalDate voucherDate) {
	this.voucherDate = voucherDate;
}
@Override
public String toString() {
	return "WeighBridgeReport [vehicleno=" + vehicleno + ", netweight=" + netweight + ", rate=" + rate
			+ ", voucherDate=" + voucherDate + "]";
}


}
