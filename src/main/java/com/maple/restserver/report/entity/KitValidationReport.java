package com.maple.restserver.report.entity;

public class KitValidationReport {

	Double qtyInMachineUnit;
	String errorMessage;
	 String machineUnit;
	public Double getQtyInMachineUnit() {
		return qtyInMachineUnit;
	}
	public void setQtyInMachineUnit(Double qtyInMachineUnit) {
		this.qtyInMachineUnit = qtyInMachineUnit;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getMachineUnit() {
		return machineUnit;
	}
	public void setMachineUnit(String machineUnit) {
		this.machineUnit = machineUnit;
	}
	@Override
	public String toString() {
		return "KitValidationReport [qtyInMachineUnit=" + qtyInMachineUnit + ", errorMessage=" + errorMessage
				+ ", machineUnit=" + machineUnit + "]";
	}

}
