package com.maple.restserver.report.entity;

import java.util.Date;

public class ActualProductionReport {
	
	String id;
	String batch;
	Double actualQty;
	String itemName;
	String unitName;
	String voucherNo;
	Date voucherDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getActualQty() {
		return actualQty;
	}
	public void setActualQty(Double actualQty) {
		this.actualQty = actualQty;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	@Override
	public String toString() {
		return "ActualProductionReport [batch=" + batch + ", actualQty=" + actualQty + ", itemName=" + itemName
				+ ", unitName=" + unitName + ", voucherNo=" + voucherNo + ", voucherDate=" + voucherDate + "]";
	}
	
}
