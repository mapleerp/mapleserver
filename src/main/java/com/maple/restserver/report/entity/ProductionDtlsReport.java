package com.maple.restserver.report.entity;

import java.util.Date;

public class ProductionDtlsReport {
	String itemName;
	Double qty;
	String unitName;
	String branchCode;
	Date voucherDate;
	
	


	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	@Override
	public String toString() {
		return "ProductionDtlsReport [itemName=" + itemName + ", qty=" + qty + ", unitName=" + unitName
				+ ", branchCode=" + branchCode + "]";
	}

	

}
