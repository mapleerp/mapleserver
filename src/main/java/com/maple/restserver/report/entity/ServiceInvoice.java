package com.maple.restserver.report.entity;

import java.util.Date;

public class ServiceInvoice {
	
	String voucherNumber;
	String customerName;
	String customerAddress;
	String itemName;
	String model;
	String brandName;
	String productName;
	Double qty;
	Date voucherDate;
	String serialId;
	String warranty;
	String complaints;
	String observation;
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getSerialId() {
		return serialId;
	}
	public void setSerialId(String serialId) {
		this.serialId = serialId;
	}
	public String getWarranty() {
		return warranty;
	}
	public void setWarranty(String warranty) {
		this.warranty = warranty;
	}
	public String getComplaints() {
		return complaints;
	}
	public void setComplaints(String complaints) {
		this.complaints = complaints;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	@Override
	public String toString() {
		return "ServiceInvoice [voucherNumber=" + voucherNumber + ", customerName=" + customerName
				+ ", customerAddress=" + customerAddress + ", itemName=" + itemName + ", model=" + model
				+ ", brandName=" + brandName + ", productName=" + productName + ", qty=" + qty + ", voucherDate="
				+ voucherDate + ", serialId=" + serialId + ", warranty=" + warranty + ", complaints=" + complaints
				+ ", observation=" + observation + "]";
	}
	

}
