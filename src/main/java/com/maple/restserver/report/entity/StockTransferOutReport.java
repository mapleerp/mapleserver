package com.maple.restserver.report.entity;

import java.io.Serializable;
import java.util.Date;

 

public class StockTransferOutReport implements Serializable{

	
	 
    private String id;
    Integer slNo;
    String intentNumber;

    
	Date voucherDate;

	String voucherNumber;
	String voucherType;

	String toBranch;
	
	String fromBranch;


	
     String batch;
     Double qty;
	 Double rate;
	 String itemCode;
	  Double taxRate;
	 Date expiryDate;
	 Double amount;
	 String unitId;
	 String itemId;
	  Double mrp;
	 String barcode;
	 String hsnCode;
	 String branchName;
	 String branchPlace;
	 String branchAddress;
	 String toBranchPlace;
	 
	String unitName;
	String itemName;
	String categoryName;
	
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	
	public String getIntentNumber() {
		return intentNumber;
	}
	public void setIntentNumber(String intentNumber) {
		this.intentNumber = intentNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getToBranch() {
		return toBranch;
	}
	public void setToBranch(String toBranch) {
		this.toBranch = toBranch;
	}
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}

	public Integer getSlNo() {
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchPlace() {
		return branchPlace;
	}
	public void setBranchPlace(String branchPlace) {
		this.branchPlace = branchPlace;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	
	
	@Override
	public String toString() {
		return "StockTransferOutReport [id=" + id + ", slNo=" + slNo + ", intentNumber=" + intentNumber
				+ ", voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber + ", voucherType=" + voucherType
				+ ", toBranch=" + toBranch + ", fromBranch=" + fromBranch + ", batch=" + batch + ", qty=" + qty
				+ ", rate=" + rate + ", itemCode=" + itemCode + ", taxRate=" + taxRate + ", expiryDate=" + expiryDate
				+ ", amount=" + amount + ", unitId=" + unitId + ", itemId=" + itemId + ", mrp=" + mrp + ", barcode="
				+ barcode + ", hsnCode=" + hsnCode + ", branchName=" + branchName + ", branchPlace=" + branchPlace
				+ ", branchAddress=" + branchAddress + "]";
	}
	public String getToBranchPlace() {
		return toBranchPlace;
	}
	public void setToBranchPlace(String toBranchPlace) {
		this.toBranchPlace = toBranchPlace;
	}
	
	 
	
}
