package com.maple.restserver.report.entity;

import java.util.Date;

public class supplierLedger {
	
	String companyName;
	String branchName;
	String branchEmail;
	String branchAddress1;
	String branchAddress2;
	String branchPhoneNo;

	String branchWebsite;

	String customerName;
	String customerAccount;
	String customerAddress;
	Date fromDate;
	Date toDate;

	Date date;
	String particulars;
	String voucherType;
	String voucherNumber;
	Double debit;
	Double credit;
	
	

	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchAddress1() {
		return branchAddress1;
	}
	public void setBranchAddress1(String branchAddress1) {
		this.branchAddress1 = branchAddress1;
	}
	public String getBranchAddress2() {
		return branchAddress2;
	}
	public void setBranchAddress2(String branchAddress2) {
		this.branchAddress2 = branchAddress2;
	}
	public String getBranchPhoneNo() {
		return branchPhoneNo;
	}
	public void setBranchPhoneNo(String branchPhoneNo) {
		this.branchPhoneNo = branchPhoneNo;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAccount() {
		return customerAccount;
	}
	public void setCustomerAccount(String customerAccount) {
		this.customerAccount = customerAccount;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getParticulars() {
		return particulars;
	}
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Double getDebit() {
		return debit;
	}
	public void setDebit(Double debit) {
		this.debit = debit;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	
	

	
	@Override
	public String toString() {
		return "supplierLedger [companyName=" + companyName + ", branchName=" + branchName + ", branchEmail="
				+ branchEmail + ", branchAddress1=" + branchAddress1 + ", branchAddress2=" + branchAddress2
				+ ", branchPhoneNo=" + branchPhoneNo + ", branchWebsite=" + branchWebsite + ", customerName="
				+ customerName + ", customerAccount=" + customerAccount + ", customerAddress=" + customerAddress
				+ ", fromDate=" + fromDate + ", toDate=" + toDate + ", date=" + date + ", particulars=" + particulars
				+ ", voucherType=" + voucherType + ", voucherNumber=" + voucherNumber + ", debit=" + debit + ", credit="
				+ credit + "]";
	}
	
	

}
