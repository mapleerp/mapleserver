package com.maple.restserver.report.entity;

import org.springframework.stereotype.Component;

@Component
public class AMDCShortExpiryReport {
	String id;
	String groupName;
	String itemName;

	private Double amount;
	String batchCode;
	String itemCode;
	String expiryDate;
	Double quantity;
	Double rate;
	String daysToExpiry;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getDaysToExpiry() {
		return daysToExpiry;
	}
	public void setDaysToExpiry(String daysToExpiry) {
		this.daysToExpiry = daysToExpiry;
	}
	@Override
	public String toString() {
		return "AMDCShortExpiryReport [id=" + id + ", groupName=" + groupName + ", itemName=" + itemName + ", amount="
				+ amount + ", batchCode=" + batchCode + ", itemCode=" + itemCode + ", expiryDate=" + expiryDate
				+ ", quantity=" + quantity + ", rate=" + rate + ", daysToExpiry=" + daysToExpiry + "]";
	}

}
