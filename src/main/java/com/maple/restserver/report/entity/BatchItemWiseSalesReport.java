package com.maple.restserver.report.entity;

import java.sql.Date;

public class BatchItemWiseSalesReport {

	
	String customerName;
	Date voucherDate;
	String itemName;
	Double qty;
	Double mrp;
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	@Override
	public String toString() {
		return "BatchItemWiseSalesReport [customerName=" + customerName + ", voucherDate=" + voucherDate + ", itemName="
				+ itemName + ", qty=" + qty + ", mrp=" + mrp + "]";
	}
	
}
