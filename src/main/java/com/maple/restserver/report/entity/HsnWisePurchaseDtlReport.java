package com.maple.restserver.report.entity;

import java.util.Date;



public class HsnWisePurchaseDtlReport {

	Date voucherDate;
	String itemName;
	String hsnCode;
	String unitName;
	Double qty;
	Double value;
	Double rate;
	Double taxRate;
	
	

	public Date getVoucherDate() {
		return voucherDate;
	}



	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}



	public String getItemName() {
		return itemName;
	}



	public void setItemName(String itemName) {
		this.itemName = itemName;
	}



	public String getHsnCode() {
		return hsnCode;
	}



	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}



	public String getUnitName() {
		return unitName;
	}



	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}



	public Double getQty() {
		return qty;
	}



	public void setQty(Double qty) {
		this.qty = qty;
	}



	public Double getValue() {
		return value;
	}



	public void setValue(Double value) {
		this.value = value;
	}



	public Double getRate() {
		return rate;
	}



	public void setRate(Double rate) {
		this.rate = rate;
	}



	public Double getTaxRate() {
		return taxRate;
	}



	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}



	@Override
	public String toString() {
		return "HsnWisePurchaseDtlReport [voucherDate=" + voucherDate + ", itemName=" + itemName + ", hsnCode="
				+ hsnCode + ", unitName=" + unitName + ", qty=" + qty + ", value=" + value + ", rate=" + rate
				+ ", taxRate=" + taxRate + "]";
	}
	
}
