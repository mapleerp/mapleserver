package com.maple.restserver.report.entity;

import java.util.Date;

public class LocalPurchaseSummayReport {

private Date date;
	
	private String invoiceNumber;
	
	private String supplier;
	
	private String tinNo;
	
	private String supplierInvoiceNumber;
	
	private Date invoiceDate;
	
	private String pONumber;
	
	private Double netInvoice;
	
	private Double beforeGst;
	
	private Double gst;
	
	private String user;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getTinNo() {
		return tinNo;
	}

	public void setTinNo(String tinNo) {
		this.tinNo = tinNo;
	}

	public String getSupplierInvoiceNumber() {
		return supplierInvoiceNumber;
	}

	public void setSupplierInvoiceNumber(String supplierInvoiceNumber) {
		this.supplierInvoiceNumber = supplierInvoiceNumber;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getpONumber() {
		return pONumber;
	}

	public void setpONumber(String pONumber) {
		this.pONumber = pONumber;
	}

	public Double getNetInvoice() {
		return netInvoice;
	}

	public void setNetInvoice(Double netInvoice) {
		this.netInvoice = netInvoice;
	}

	public Double getBeforeGst() {
		return beforeGst;
	}

	public void setBeforeGst(Double beforeGst) {
		this.beforeGst = beforeGst;
	}

	public Double getGst() {
		return gst;
	}

	public void setGst(Double gst) {
		this.gst = gst;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "LocalPurchaseSummayReport [date=" + date + ", invoiceNumber=" + invoiceNumber + ", supplier=" + supplier
				+ ", tinNo=" + tinNo + ", supplierInvoiceNumber=" + supplierInvoiceNumber + ", invoiceDate="
				+ invoiceDate + ", pONumber=" + pONumber + ", netInvoice=" + netInvoice + ", beforeGst=" + beforeGst
				+ ", gst=" + gst + ", user=" + user + "]";
	}

	
}
