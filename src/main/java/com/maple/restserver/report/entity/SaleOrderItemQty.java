package com.maple.restserver.report.entity;

public class SaleOrderItemQty {

	String itemId;
	Double qtyInMachUnit;
	String MachineUnitId;
	String machineId;
	String resourceSubCatId;
	String itemUnitId;
	Double qtyOfItemForSingleUnit;
	Double currentStock;
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	

	public String getResourceSubCatId() {
		return resourceSubCatId;
	}
	public void setResourceSubCatId(String resourceSubCatId) {
		this.resourceSubCatId = resourceSubCatId;
	}
	public Double getQtyInMachUnit() {
		return qtyInMachUnit;
	}
	public void setQtyInMachUnit(Double qtyInMachUnit) {
		this.qtyInMachUnit = qtyInMachUnit;
	}
	public String getMachineUnitId() {
		return MachineUnitId;
	}
	public void setMachineUnitId(String machineUnitId) {
		MachineUnitId = machineUnitId;
	}
	public String getMachineId() {
		return machineId;
	}
	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}
	
	public String getItemUnitId() {
		return itemUnitId;
	}
	public void setItemUnitId(String itemUnitId) {
		this.itemUnitId = itemUnitId;
	}
	public Double getQtyOfItemForSingleUnit() {
		return qtyOfItemForSingleUnit;
	}
	public void setQtyOfItemForSingleUnit(Double qtyOfItemForSingleUnit) {
		this.qtyOfItemForSingleUnit = qtyOfItemForSingleUnit;
	}
	@Override
	public String toString() {
		return "SaleOrderItemQty [itemId=" + itemId + ", qtyInMachUnit=" + qtyInMachUnit + ", MachineUnitId="
				+ MachineUnitId + ", machineId=" + machineId + ", resourceSubCatId=" + resourceSubCatId
				+ ", itemUnitId=" + itemUnitId + ", qtyOfItemForSingleUnit=" + qtyOfItemForSingleUnit + "]";
	}
	
	
	
	
	
}
