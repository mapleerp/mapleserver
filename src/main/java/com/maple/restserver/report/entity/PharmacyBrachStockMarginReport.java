package com.maple.restserver.report.entity;

import java.util.Date;

public class PharmacyBrachStockMarginReport {
	
	String groupName;
	String itemName;
	String batchCode;
	String itemCode;
	Date expiryDate;
	Double quantity;
	Double rate;
	Double amount;
	Double purchaseRate;
	Double purchaseValue;
    Double marginRate;
    Double marginValue;
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getPurchaseRate() {
		return purchaseRate;
	}
	public void setPurchaseRate(Double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}
	public Double getPurchaseValue() {
		return purchaseValue;
	}
	public void setPurchaseValue(Double purchaseValue) {
		this.purchaseValue = purchaseValue;
	}
	public Double getMarginRate() {
		return marginRate;
	}
	public void setMarginRate(Double marginRate) {
		this.marginRate = marginRate;
	}
	public Double getMarginValue() {
		return marginValue;
	}
	public void setMarginValue(Double marginValue) {
		this.marginValue = marginValue;
	}
	@Override
	public String toString() {
		return "PharmacyBrachStockMarginReport [groupName=" + groupName + ", itemName=" + itemName + ", batchCode="
				+ batchCode + ", itemCode=" + itemCode + ", expiryDate=" + expiryDate + ", quantity=" + quantity
				+ ", rate=" + rate + ", amount=" + amount + ", purchaseRate=" + purchaseRate + ", purchaseValue="
				+ purchaseValue + ", marginRate=" + marginRate + ", marginValue=" + marginValue + "]";
	}
}
