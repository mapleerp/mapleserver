package com.maple.restserver.report.entity;

import java.util.Date;

import org.springframework.stereotype.Component;
@Component
public class LedgerReport {

	
	Date openingDate;
	Date closingDate;
	String refNo;
	Double openingAmount;
	Double closingAmount;
	Double overDueByDayes;
	public Date getOpeningDate() {
		return openingDate;
	}
	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}
	public Date getClosingDate() {
		return closingDate;
	}
	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public Double getOpeningAmount() {
		return openingAmount;
	}
	public void setOpeningAmount(Double openingAmount) {
		this.openingAmount = openingAmount;
	}
	public Double getClosingAmount() {
		return closingAmount;
	}
	public void setClosingAmount(Double closingAmount) {
		this.closingAmount = closingAmount;
	}
	public Double getOverDueByDayes() {
		return overDueByDayes;
	}
	public void setOverDueByDayes(Double overDueByDayes) {
		this.overDueByDayes = overDueByDayes;
	}
	@Override
	public String toString() {
		return "LedgerReport [openingDate=" + openingDate + ", closingDate=" + closingDate + ", refNo=" + refNo
				+ ", openingAmount=" + openingAmount + ", closingAmount=" + closingAmount + ", overDueByDayes="
				+ overDueByDayes + "]";
	}

	
	
	
}
