package com.maple.restserver.report.entity;

public class AccountHeadsReport {

	String accountName;
	String parentId;
	String groupOnly;
	String machineId;
	String taxId;
	String voucherType;
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getGroupOnly() {
		return groupOnly;
	}
	public void setGroupOnly(String groupOnly) {
		this.groupOnly = groupOnly;
	}
	public String getMachineId() {
		return machineId;
	}
	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	
}
