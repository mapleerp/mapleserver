package com.maple.restserver.report.entity;

import java.util.Date;

import com.maple.restserver.entity.CompanyMst;

public class ItemBatchDtlReport {

	String itemId;
	String batch;
	String barcode;
	Double qtyIn;
	Double qtyOut;
	Double mrp;
	Date expDate;
	String branchcode;
	String store;
	String  companyMst;
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Double getQtyIn() {
		return qtyIn;
	}
	public void setQtyIn(Double qtyIn) {
		this.qtyIn = qtyIn;
	}
	public Double getQtyOut() {
		return qtyOut;
	}
	public void setQtyOut(Double qtyOut) {
		this.qtyOut = qtyOut;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public String getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(String companyMst) {
		this.companyMst = companyMst;
	}
	@Override
	public String toString() {
		return "ItemBatchDtlReport [itemId=" + itemId + ", batch=" + batch + ", barcode=" + barcode + ", qtyIn=" + qtyIn
				+ ", qtyOut=" + qtyOut + ", mrp=" + mrp + ", expDate=" + expDate + ", branchcode=" + branchcode
				+ ", store=" + store + ", companyMst=" + companyMst + "]";
	}

	
}
