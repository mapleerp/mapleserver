package com.maple.restserver.report.entity;

import java.io.Serializable;
import java.util.Date;

public class StockTransferInReport implements Serializable{
	
	 
    private String id;
    String voucherNumber;
	Date invoiceDate;

	String fromBranch;
	String itemName;
	String category;
	String itemCode;
	String batch;
	Date expiryDate;
	Double qty;
	String unitName;
	Double rate;
	Double amount;
	String userName;
	
	String comapnyName;
	String branchName;
	String branchEmail;
	String branchPhone;
	String branchAddress;
	String branchState;
	String branchGst;
	String branchWebsite;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getComapnyName() {
		return comapnyName;
	}
	public void setComapnyName(String comapnyName) {
		this.comapnyName = comapnyName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchPhone() {
		return branchPhone;
	}
	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getBranchState() {
		return branchState;
	}
	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}
	public String getBranchGst() {
		return branchGst;
	}
	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	
	
	
	@Override
	public String toString() {
		return "StockTransferInReport [id=" + id + ", voucherNumber=" + voucherNumber + ", invoiceDate=" + invoiceDate
				+ ", fromBranch=" + fromBranch + ", itemName=" + itemName + ", category=" + category + ", itemCode="
				+ itemCode + ", batch=" + batch + ", expiryDate=" + expiryDate + ", qty=" + qty + ", unitName="
				+ unitName + ", rate=" + rate + ", amount=" + amount + ", userName=" + userName + ", comapnyName="
				+ comapnyName + ", branchName=" + branchName + ", branchEmail=" + branchEmail + ", branchPhone="
				+ branchPhone + ", branchAddress=" + branchAddress + ", branchState=" + branchState + ", branchGst="
				+ branchGst + ", branchWebsite=" + branchWebsite + "]";
	}







	
}
