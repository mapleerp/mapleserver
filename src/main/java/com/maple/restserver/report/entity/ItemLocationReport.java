package com.maple.restserver.report.entity;

public class ItemLocationReport {

	String itemName;
	String floor;
	String shelf;
	String rack;
	Double qty;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getShelf() {
		return shelf;
	}
	public void setShelf(String shelf) {
		this.shelf = shelf;
	}
	public String getRack() {
		return rack;
	}
	public void setRack(String rack) {
		this.rack = rack;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	@Override
	public String toString() {
		return "ItemLocationReport [itemName=" + itemName + ", floor=" + floor + ", shelf=" + shelf + ", rack=" + rack
				+ ", qty=" + qty + "]";
	}
	
}
