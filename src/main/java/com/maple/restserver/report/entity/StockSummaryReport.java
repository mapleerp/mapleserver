package com.maple.restserver.report.entity;

public class StockSummaryReport {

	
	private String id;
	private String particulars;
	private Double value;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParticulars() {
		return particulars;
	}
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "StockSummaryReport [id=" + id + ", particulars=" + particulars + ", value=" + value + "]";
	}
	
	
}
