package com.maple.restserver.report.entity;

public class BatchwiseSalesTransactionReport {

	String vouchernumber;
	Double invoiceAmount;
	Double invoiceDiscount;
	String customername;
	String tinnumber;
	String salesman;
	
	public String getVouchernumber() {
		return vouchernumber;
	}
	public void setVouchernumber(String vouchernumber) {
		this.vouchernumber = vouchernumber;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public String getTinnumber() {
		return tinnumber;
	}
	public void setTinnumber(String tinnumber) {
		this.tinnumber = tinnumber;
	}
	public String getSalesman() {
		return salesman;
	}
	public void setSalesman(String salesman) {
		this.salesman = salesman;
	}
	public Double getInvoiceDiscount() {
		return invoiceDiscount;
	}
	public void setInvoiceDiscount(Double invoiceDiscount) {
		this.invoiceDiscount = invoiceDiscount;
	}
	
	
}
