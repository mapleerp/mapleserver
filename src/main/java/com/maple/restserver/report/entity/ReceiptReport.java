package com.maple.restserver.report.entity;

import java.util.Date;

import org.springframework.stereotype.Component;
@Component
public class ReceiptReport 

{

	String supplierName;
	String voucherNumber;
	Date voucherDate;
	String itemName;
	Double qty;
	Double purchseRate;
	Double taxRate;
	Double amount;
	
	
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getPurchseRate() {
		return purchseRate;
	}
	public void setPurchseRate(Double purchseRate) {
		this.purchseRate = purchseRate;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	@Override
	public String toString() {
		return "ReceiptReport [supplierName=" + supplierName + ", voucherNumber=" + voucherNumber + ", voucherDate="
				+ voucherDate + ", itemName=" + itemName + ", qty=" + qty + ", purchseRate=" + purchseRate
				+ ", taxRate=" + taxRate + ", amount=" + amount + "]";
	}
	
	
}
