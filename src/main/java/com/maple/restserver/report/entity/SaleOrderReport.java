package com.maple.restserver.report.entity;

import java.util.Date;

public class SaleOrderReport {
	
	Date startDate;
	String supplierName;
	String branchCode;
	String branchName;
	String branchAddress;
	String branchPhone;
	String companyName;
	String branchEmail;
	String branchWebsite;
	String branchState;
	String voucherNumber;
	String orderMsgToPrint;
	String orderAdvice;
	Double paidAmount;
	
	
	String state;
    String companyGst;
	
	String paymentMode;
	
	
	
	Date voucherDate;
	String account;
	
	
	
	

	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCompanyGst() {
		return companyGst;
	}
	public void setCompanyGst(String companyGst) {
		this.companyGst = companyGst;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getOrderMsgToPrint() {
		return orderMsgToPrint;
	}
	public void setOrderMsgToPrint(String orderMsgToPrint) {
		this.orderMsgToPrint = orderMsgToPrint;
	}
	public String getOrderAdvice() {
		return orderAdvice;
	}
	public void setOrderAdvice(String orderAdvice) {
		this.orderAdvice = orderAdvice;
	}
	public String getBranchState() {
		return branchState;
	}
	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}
	int slNo;
	String orderNo;
	
	String customerName;
	String customerPhone;
	String customerAddress;
	String deliveryType;
	String dueTime;
	String itemName;
	Date dueDate;
	String description;
	String ingrediant;
	Double qty;
	String unit;
	String deliveryBoy;
	String orderBy;
	String voucherNo;
	Double invoiceAmount;
	Double balance;
	Double invoiceToatal;
	Double advanceAmount;
	
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getBranchPhone() {
		return branchPhone;
	}
	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public String getDueTime() {
		return dueTime;
	}
	public void setDueTime(String dueTime) {
		this.dueTime = dueTime;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIngrediant() {
		return ingrediant;
	}
	public void setIngrediant(String ingrediant) {
		this.ingrediant = ingrediant;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getDeliveryBoy() {
		return deliveryBoy;
	}
	public void setDeliveryBoy(String deliveryBoy) {
		this.deliveryBoy = deliveryBoy;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public Double getInvoiceToatal() {
		return invoiceToatal;
	}
	public void setInvoiceToatal(Double invoiceToatal) {
		this.invoiceToatal = invoiceToatal;
	}
	public Double getAdvanceAmount() {
		return advanceAmount;
	}
	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}
	public int getSlNo() {
		return slNo;
	}
	public void setSlNo(int slNo) {
		this.slNo = slNo;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	@Override
	public String toString() {
		return "SaleOrderReport [startDate=" + startDate + ", supplierName=" + supplierName + ", branchCode="
				+ branchCode + ", branchName=" + branchName + ", branchAddress=" + branchAddress + ", branchPhone="
				+ branchPhone + ", companyName=" + companyName + ", branchEmail=" + branchEmail + ", branchWebsite="
				+ branchWebsite + ", branchState=" + branchState + ", voucherNumber=" + voucherNumber
				+ ", orderMsgToPrint=" + orderMsgToPrint + ", orderAdvice=" + orderAdvice + ", slNo=" + slNo
				+ ", orderNo=" + orderNo + ", customerName=" + customerName + ", customerPhone=" + customerPhone
				+ ", customerAddress=" + customerAddress + ", deliveryType=" + deliveryType + ", dueTime=" + dueTime
				+ ", itemName=" + itemName + ", dueDate=" + dueDate + ", description=" + description + ", ingrediant="
				+ ingrediant + ", qty=" + qty + ", unit=" + unit + ", deliveryBoy=" + deliveryBoy + ", orderBy="
				+ orderBy + ", voucherNo=" + voucherNo + ", invoiceAmount=" + invoiceAmount + ", balance=" + balance
				+ ", invoiceToatal=" + invoiceToatal + ", advanceAmount=" + advanceAmount + "]";
	}
	
}
