package com.maple.restserver.report.entity;

public class PharmacyItemMovementAnalisysReport {
	String trEntryDate;
	String groupName;
	String itemName;
	String itemCode;
	String batchCode;
	String description;
		String unitName;
		String partyName;
		String voucherNumber;
		Double openingBalance;
		Double inwardQty;
		Double outwardQty;
		Double balance;
		Double cost;
		Double value;
		public String getGroupName() {
			return groupName;
		}
		public void setGroupName(String groupName) {
			this.groupName = groupName;
		}
		public String getItemName() {
			return itemName;
		}
		public void setItemName(String itemName) {
			this.itemName = itemName;
		}
		public String getItemCode() {
			return itemCode;
		}
		public void setItemCode(String itemCode) {
			this.itemCode = itemCode;
		}
		public String getBatchCode() {
			return batchCode;
		}
		public void setBatchCode(String batchCode) {
			this.batchCode = batchCode;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getUnitName() {
			return unitName;
		}
		public void setUnitName(String unitName) {
			this.unitName = unitName;
		}
		public String getPartyName() {
			return partyName;
		}
		public void setPartyName(String partyName) {
			this.partyName = partyName;
		}
		public String getVoucherNumber() {
			return voucherNumber;
		}
		public void setVoucherNumber(String voucherNumber) {
			this.voucherNumber = voucherNumber;
		}
		public Double getOpeningBalance() {
			return openingBalance;
		}
		public void setOpeningBalance(Double openingBalance) {
			this.openingBalance = openingBalance;
		}
		public Double getInwardQty() {
			return inwardQty;
		}
		public void setInwardQty(Double inwardQty) {
			this.inwardQty = inwardQty;
		}
		public Double getOutwardQty() {
			return outwardQty;
		}
		public void setOutwardQty(Double outwardQty) {
			this.outwardQty = outwardQty;
		}
		public Double getBalance() {
			return balance;
		}
		public void setBalance(Double balance) {
			this.balance = balance;
		}
		public Double getCost() {
			return cost;
		}
		public void setCost(Double cost) {
			this.cost = cost;
		}
		public Double getValue() {
			return value;
		}
		public void setValue(Double value) {
			this.value = value;
		}
		public String getTrEntryDate() {
			return trEntryDate;
		}
		public void setTrEntryDate(String trEntryDate) {
			this.trEntryDate = trEntryDate;
		}
		@Override
		public String toString() {
			return "PharmacyItemMovementAnalisysReport [trEntryDate=" + trEntryDate + ", groupName=" + groupName
					+ ", itemName=" + itemName + ", itemCode=" + itemCode + ", batchCode=" + batchCode
					+ ", description=" + description + ", unitName=" + unitName + ", partyName=" + partyName
					+ ", voucherNumber=" + voucherNumber + ", openingBalance=" + openingBalance + ", inwardQty="
					+ inwardQty + ", outwardQty=" + outwardQty + ", balance=" + balance + ", cost=" + cost + ", value="
					+ value + "]";
		}
		
		
		
}
