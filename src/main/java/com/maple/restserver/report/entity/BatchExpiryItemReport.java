package com.maple.restserver.report.entity;

import java.sql.Date;

public class BatchExpiryItemReport {
	String category;
	String itemName;
	String itemCode;
	String batch;
	Date expiryDate;
	Double qty;
	Double rate;
	Double amount;
	Integer age;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "BatchExpiryItemReport [category=" + category + ", itemName=" + itemName + ", itemCode=" + itemCode
				+ ", batch=" + batch + ", expiryDate=" + expiryDate + ", qty=" + qty + ", rate=" + rate + ", amount="
				+ amount + ", age=" + age + "]";
	}
	

}
