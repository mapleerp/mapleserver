package com.maple.restserver.report.entity;

import java.util.Date;

public class PharmacyNewCustomerWiseSaleReport {
	private Date invoiceDate;
	private String voucherNumber;
	private String itemName;
	private String batchcode;
	private Double quantity;
	private Double amount;
	private Double tax;
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBatchcode() {
		return batchcode;
	}
	public void setBatchcode(String batchcode) {
		this.batchcode = batchcode;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	@Override
	public String toString() {
		return "AMDCCustomerWiseSaleReport [invoiceDate=" + invoiceDate + ", voucherNumber=" + voucherNumber
				+ ", itemName=" + itemName + ", batchcode=" + batchcode + ", quantity=" + quantity + ", amount="
				+ amount + ", tax=" + tax + "]";
	}
	
	
}
