package com.maple.restserver.report.entity;


import java.util.Date;

import org.springframework.stereotype.Component;


@Component
public class OrgReceiptInvoice {
	
	
	String id;
	String receiptNumber;
	String invoiceNumber;
	Double recievedAmount;
    String companyName;
    String state;
    String companyGst;
    String voucherNumber;
    Date voucherDate;
    String account;
    String remark;
    String modeOfpayment;
	Date instrumentDate;
	String instrumentNumber;
	String branchName;
	String branchAddress;
	String branchTelNo;
	String branchGst;
	
	
	String familyName;
	String memberName;
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	public String getId()
    {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Double getRecievedAmount() {
		return recievedAmount;
	}
	public void setRecievedAmount(Double recievedAmount) {
		this.recievedAmount = recievedAmount;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCompanyGst() {
		return companyGst;
	}
	public void setCompanyGst(String companyGst) {
		this.companyGst = companyGst;
	}
	
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	
	
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getModeOfpayment() {
		return modeOfpayment;
	}
	public void setModeOfpayment(String modeOfpayment) {
		this.modeOfpayment = modeOfpayment;
	}
	public Date getInstrumentDate() {
		return instrumentDate;
	}
	public void setInstrumentDate(Date instrumentDate) {
		this.instrumentDate = instrumentDate;
	}
	
	
	public String getInstrumentNumber() {
		return instrumentNumber;
	}
	public void setInstrumentNumber(String objAray) {
		this.instrumentNumber = objAray;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getBranchTelNo() {
		return branchTelNo;
	}
	public void setBranchTelNo(String branchTelNo) {
		this.branchTelNo = branchTelNo;
	}
	public String getBranchGst() {
		return branchGst;
	}
	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	@Override
	public String toString() {
		return "ReceiptInvoice [id=" + id + ", receiptNumber=" + receiptNumber + ", invoiceNumber=" + invoiceNumber
				+ ", recievedAmount=" + recievedAmount + ", companyName=" + companyName + ", state=" + state
				+ ", companyGst=" + companyGst + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", account=" + account + ", remark=" + remark + ", modeOfpayment=" + modeOfpayment
				+ ", instrumentDate=" + instrumentDate + ", instrumentNumber=" + instrumentNumber + ", branchName="
				+ branchName + ", branchAddress=" + branchAddress + ", branchTelNo=" + branchTelNo + ", branchGst="
				+ branchGst + ", familyName=" + familyName + ", memberName=" + memberName + "]";
	}

}
