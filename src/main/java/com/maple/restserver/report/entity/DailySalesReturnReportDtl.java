package com.maple.restserver.report.entity;

import java.util.Date;
import java.time.LocalDate;

import javax.swing.JScrollBar;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
 

public class DailySalesReturnReportDtl {
	
	String voucherNumber;
	String customerName;
	Double amount;
	String customerType;
	Date voucherDate;
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	@Override
	public String toString() {
		return "DailySalesReturnReportDtl [voucherNumber=" + voucherNumber + ", customerName=" + customerName
				+ ", amount=" + amount + ", customerType=" + customerType + ", voucherDate=" + voucherDate + "]";
	}
	
	
	
	

}
