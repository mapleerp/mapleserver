package com.maple.restserver.report.entity;

public class CategoryWiseSalesReport {

	String categoryName;
	String categorId;
	String itemName;
	Double qty;
	Double rate;
	Double value;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategorId() {
		return categorId;
	}
	public void setCategorId(String categorId) {
		this.categorId = categorId;
	}
	
	
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	@Override
	public String toString() {
		return "CategoryWiseSalesReport [categoryName=" + categoryName + ", categorId=" + categorId + ", itemName="
				+ itemName + ", qty=" + qty + ", rate=" + rate + ", value=" + value + "]";
	}
	

	
}
