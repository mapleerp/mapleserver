package com.maple.restserver.report.entity;

import org.springframework.stereotype.Component;

@Component
public class InsuranceWiseSalesReport {
	
	private String id;
	String  invoiceDate;
	String vocherNumber;
	String customerName;
	String patientId;
	String insComapany;
	String insuCard;
	String policyType;
	Double creditAmount;
	Double cashPaid;
	Double cardPaid;
	Double insuAmount;
	Double invoiceAmount;
	String usrName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getVocherNumber() {
		return vocherNumber;
	}
	public void setVocherNumber(String vocherNumber) {
		vocherNumber = vocherNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getInsComapany() {
		return insComapany;
	}
	public void setInsComapany(String insComapany) {
		this.insComapany = insComapany;
	}
	public String getInsuCard() {
		return insuCard;
	}
	public void setInsuCard(String insuCard) {
		this.insuCard = insuCard;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public Double getCashPaid() {
		return cashPaid;
	}
	public void setCashPaid(Double cashPaid) {
		this.cashPaid = cashPaid;
	}
	public Double getCardPaid() {
		return cardPaid;
	}
	public void setCardPaid(Double cardPaid) {
		this.cardPaid = cardPaid;
	}
	public Double getInsuAmount() {
		return insuAmount;
	}
	public void setInsuAmount(Double insuAmount) {
		this.insuAmount = insuAmount;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getUsrName() {
		return usrName;
	}
	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}
	@Override
	public String toString() {
		return "InsuranceWiseSalesReport [id=" + id + ", invoiceDate=" + invoiceDate + ", vocherNumber=" + vocherNumber
				+ ", customerName=" + customerName + ", patientId=" + patientId + ", insComapany=" + insComapany
				+ ", insuCard=" + insuCard + ", policyType=" + policyType + ", creditAmount=" + creditAmount
				+ ", cashPaid=" + cashPaid + ", cardPaid=" + cardPaid + ", insuAmount=" + insuAmount
				+ ", invoiceAmount=" + invoiceAmount + ", usrName=" + usrName + "]";
	}

}
