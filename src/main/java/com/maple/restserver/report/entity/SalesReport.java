package com.maple.restserver.report.entity;

public class SalesReport {

	private String branch;
	private Double cash;
	private Double card;
	private Double card2;
	private String credit;
	private Double bank;
	private Double total;
	private Double Invovicetotal;

	
	public Double getInvovicetotal() {
		return Invovicetotal;
	}
	public void setInvovicetotal(Double invovicetotal) {
		Invovicetotal = invovicetotal;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public Double getCash() {
		return cash;
	}
	public void setCash(Double cash) {
		this.cash = cash;
	}
	public Double getCard() {
		return card;
	}
	public void setCard(Double card) {
		this.card = card;
	}
	
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	public Double getBank() {
		return bank;
	}
	public void setBank(Double bank) {
		this.bank = bank;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getCard2() {
		return card2;
	}
	public void setCard2(Double card2) {
		this.card2 = card2;
	}
	@Override
	public String toString() {
		return "SalesReport [branch=" + branch + ", cash=" + cash + ", card=" + card + ", card2=" + card2 + ", credit="
				+ credit + ", bank=" + bank + ", total=" + total + ", Invovicetotal=" + Invovicetotal + "]";
	}
	
	
}
