package com.maple.restserver.report.entity;

import java.util.Date;

public class StockSummaryCategoryWiseReport {
	String itemName;
	Double qty;
	Double rate;
	Double value;
	Date transDate;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Date getTransDate() {
		return transDate;
	}
	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}
	@Override
	public String toString() {
		return "StockSummaryCategoryWiseReport [itemName=" + itemName + ", qty=" + qty + ", rate=" + rate + ", value="
				+ value + ", transDate=" + transDate + "]";
	}
	

	
}
