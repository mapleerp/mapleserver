package com.maple.restserver.report.entity;

import java.math.BigDecimal;
import java.time.Month;
import java.time.Year;

public class ProfitSummaryDailyReport {

private String branchCode;

private BigDecimal profitSummaryBranch;
private BigDecimal profitSummaryComany;

private Month monnthOfProfit;
private Year yearOfProfit;


public BigDecimal getProfitSummaryBranch() {
	return profitSummaryBranch;
}
public void setProfitSummaryBranch(BigDecimal profitSummaryBranch) {
	this.profitSummaryBranch = profitSummaryBranch;
}
public BigDecimal getProfitSummaryComany() {
	return profitSummaryComany;
}
public void setProfitSummaryComany(BigDecimal profitSummaryComany) {
	this.profitSummaryComany = profitSummaryComany;
}
public String getBranchCode() {
	return branchCode;
}
public void setBranchCode(String branchCode) {
	this.branchCode = branchCode;
}


public Month getMonnthOfProfit() {
	return monnthOfProfit;
}
public void setMonnthOfProfit(Month monnthOfProfit) {
	this.monnthOfProfit = monnthOfProfit;
}
public Year getYearOfProfit() {
	return yearOfProfit;
}
public void setYearOfProfit(Year yearOfProfit) {
	this.yearOfProfit = yearOfProfit;
}


@Override
public String toString() {
	return "ProfitSummaryDailyReport [branchCode=" + branchCode + ", profitSummaryBranch=" + profitSummaryBranch
			+ ", profitSummaryComany=" + profitSummaryComany + ", monnthOfProfit=" + monnthOfProfit + ", yearOfProfit="
			+ yearOfProfit + "]";
}




	
	
}
