package com.maple.restserver.report.entity;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class BatchWiseCustomerSalesReport {
 String vouchernumber;
 String invoicedate;
 String itemname;
 String batchcode;
 Double qty;
 Double amount;
 Double tax;
 String customername;
 
public String getVouchernumber() {
	return vouchernumber;
}
public void setVouchernumber(String vouchernumber) {
	this.vouchernumber = vouchernumber;
}
public String getInvoicedate() {
	return invoicedate;
}
public void setInvoicedate(String invoicedate) {
	this.invoicedate = invoicedate;
}
public String getItemname() {
	return itemname;
}
public void setItemname(String itemname) {
	this.itemname = itemname;
}
public String getBatchcode() {
	return batchcode;
}
public void setBatchcode(String batchcode) {
	this.batchcode = batchcode;
}
public Double getQty() {
	return qty;
}
public void setQty(Double qty) {
	this.qty = qty;
}
public Double getAmount() {
	return amount;
}
public void setAmount(Double amount) {
	this.amount = amount;
}
public Double getTax() {
	return tax;
}
public void setTax(Double tax) {
	this.tax = tax;
}
public String getCustomername() {
	return customername;
}
public void setCustomername(String customername) {
	this.customername = customername;
}
	
	
}
