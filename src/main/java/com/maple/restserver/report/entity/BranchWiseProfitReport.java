package com.maple.restserver.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

 

public class BranchWiseProfitReport {
	
	String branchName;
	Double profit;
	String itemName;
	String customerName;

	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public Double getProfit() {
		return profit;
	}
	public void setProfit(Double profit) {
		this.profit = profit;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	
	


}
