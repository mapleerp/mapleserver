package com.maple.restserver.report.entity;

import java.util.Date;

public class SalesInvoiceTaxSubReport {

 
	 
	Date voucherDate;
	String voucherNumber;
	String taxRateName;
 
	Double taxAmount;

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getTaxRateName() {
		return taxRateName;
	}

	public void setTaxRateName(String taxRateName) {
		this.taxRateName = taxRateName;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	
	 
}
