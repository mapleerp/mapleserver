package com.maple.restserver.report.entity;

import java.util.Date;

import org.springframework.stereotype.Component;
@Component
public class PurchaseReport {

	String supplierName;
	String voucherNumber;
	Date voucherDate;
	String itemName;
	Double qty;
	Double purchseRate;
	Double taxRate;
	String unit;
	Double amount;
	Double amountTotal;
	Double additionalExpense;
	Double importDuty;

	String supplierInvNo;
	String supplierGst;
	
	Double taxAmount;
	Double taxSplit;
	
	
	String itemCode;
	String batch;
	Date expiry;
	Double unitPrice;
	
	String supplierEmail;
	String incoiceDate;
	Double netCost;
	String supplierAddress;
	String supplierState;

	
	
	public Double getImportDuty() {
		return importDuty;
	}
	public void setImportDuty(Double importDuty) {
		this.importDuty = importDuty;
	}
	public Double getAdditionalExpense() {
		return additionalExpense;
	}
	public void setAdditionalExpense(Double additionalExpense) {
		this.additionalExpense = additionalExpense;
	}
	public String getSupplierEmail() {
		return supplierEmail;
	}
	public void setSupplierEmail(String supplierEmail) {
		this.supplierEmail = supplierEmail;
	}
	public String getIncoiceDate() {
		return incoiceDate;
	}
	public void setIncoiceDate(String incoiceDate) {
		this.incoiceDate = incoiceDate;
	}
	public Double getNetCost() {
		return netCost;
	}
	public void setNetCost(Double netCost) {
		this.netCost = netCost;
	}
	public String getSupplierAddress() {
		return supplierAddress;
	}
	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}
	
	
	public Double getTaxSplit() {
		return taxSplit;
	}
	public void setTaxSplit(Double taxSplit) {
		this.taxSplit = taxSplit;
	}


	



	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getPurchseRate() {
		return purchseRate;
	}
	public void setPurchseRate(Double purchseRate) {
		this.purchseRate = purchseRate;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	
	
	public String getSupplierInvNo() {
		return supplierInvNo;
	}
	public void setSupplierInvNo(String supplierInvNo) {
		this.supplierInvNo = supplierInvNo;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getAmountTotal() {
		return amountTotal;
	}
	public void setAmountTotal(Double amountTotal) {
		this.amountTotal = amountTotal;
	}
	
	public Double getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getSupplierGst() {
		return supplierGst;
	}
	public void setSupplierGst(String supplierGst) {
		this.supplierGst = supplierGst;
	}
	
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Date getExpiry() {
		return expiry;
	}
	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	
	public String getSupplierState() {
		return supplierState;
	}
	public void setSupplierState(String supplierState) {
		this.supplierState = supplierState;
	}
	@Override
	public String toString() {
		return "PurchaseReport [supplierName=" + supplierName + ", voucherNumber=" + voucherNumber + ", voucherDate="
				+ voucherDate + ", itemName=" + itemName + ", qty=" + qty + ", purchseRate=" + purchseRate
				+ ", taxRate=" + taxRate + ", unit=" + unit + ", amount=" + amount + ", amountTotal=" + amountTotal
				+ ", supplierInvNo=" + supplierInvNo + ", supplierGst=" + supplierGst + ", taxAmount=" + taxAmount
				+ ", taxSplit=" + taxSplit + ", itemCode=" + itemCode + ", batch=" + batch + ", expiry=" + expiry
				+ ", unitPrice=" + unitPrice + ", supplierEmail=" + supplierEmail + ", incoiceDate=" + incoiceDate
				+ ", netCost=" + netCost + ", supplierAddress=" + supplierAddress + ", supplierState=" + supplierState
				+ "]";
	}
	
	
	
	
	
}
