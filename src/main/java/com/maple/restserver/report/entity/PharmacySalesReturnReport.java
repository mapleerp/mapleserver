package com.maple.restserver.report.entity;

public class PharmacySalesReturnReport {

	

	String invoiceDate;
	String customerName;
	String voucherNumber;
	String currency;
	
	
	String salesVoucherNumber;
	String itemName;
	String itemCode;
	String groupName;
	String batchCode;
	String expiryDate;
	Double quantity;
	Double rate;
	Double value;
	String salesMan;
	Double amountBeforeGst;
	Double gst;
	Double amount;
	String userName;
	
	
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
	
	
	public String getSalesVoucherNumber() {
		return salesVoucherNumber;
	}
	public void setSalesVoucherNumber(String salesVoucherNumber) {
		this.salesVoucherNumber = salesVoucherNumber;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public String getSalesMan() {
		return salesMan;
	}
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}
	public Double getAmountBeforeGst() {
		return amountBeforeGst;
	}
	public void setAmountBeforeGst(Double amountBeforeGst) {
		this.amountBeforeGst = amountBeforeGst;
	}
	public Double getGst() {
		return gst;
	}
	public void setGst(Double gst) {
		this.gst = gst;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
	public String toString() {
		return "PharmacySalesReturnReport [invoiceDate=" + invoiceDate + ", customerName=" + customerName
				+ ", voucherNumber=" + voucherNumber + ", currency=" + currency + ", salesVoucherNumber="
				+ salesVoucherNumber + ", itemName=" + itemName + ", itemCode=" + itemCode + ", groupName=" + groupName
				+ ", batchCode=" + batchCode + ", expiryDate=" + expiryDate + ", quantity=" + quantity + ", rate="
				+ rate + ", value=" + value + ", salesMan=" + salesMan + ", amountBeforeGst=" + amountBeforeGst
				+ ", gst=" + gst + ", amount=" + amount + ", userName=" + userName + "]";
	}
	
	
	
	
	
	
	
	
}
