package com.maple.restserver.report.entity;

import java.util.Date;

public class ActiveJobCardReport {
	
	String customer;
	String itemName;
	String serviceInNo;
	String complaints;
	String observation;
	String jobCardHdrId;
	Date voucherDate;
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getServiceInNo() {
		return serviceInNo;
	}
	public void setServiceInNo(String serviceInNo) {
		this.serviceInNo = serviceInNo;
	}
	public String getComplaints() {
		return complaints;
	}
	public void setComplaints(String complaints) {
		this.complaints = complaints;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	public String getJobCardHdrId() {
		return jobCardHdrId;
	}
	public void setJobCardHdrId(String jobCardHdrId) {
		this.jobCardHdrId = jobCardHdrId;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	@Override
	public String toString() {
		return "ActiveJobCardReport [customer=" + customer + ", itemName=" + itemName + ", serviceInNo=" + serviceInNo
				+ ", complaints=" + complaints + ", observation=" + observation + ", jobCardHdrId=" + jobCardHdrId
				+ ", voucherDate=" + voucherDate + "]";
	}
	
	
	

}
