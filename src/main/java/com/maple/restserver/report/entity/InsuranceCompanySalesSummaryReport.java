package com.maple.restserver.report.entity;

import java.util.Date;

public class InsuranceCompanySalesSummaryReport {

	Date voucherDate;
	String voucherNumber;
	String customerName;
	String isuranceCompanyName;
	String insuranceCard;
	String policyType;
	Double creditAmount;
	Double cashPaid;
	Double cardPaid;
	Double insuranceAmount;
	Double invoiceAmount;
	String userName;
	String patientName;
	
	
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getIsuranceCompanyName() {
		return isuranceCompanyName;
	}
	public void setIsuranceCompanyName(String isuranceCompanyName) {
		this.isuranceCompanyName = isuranceCompanyName;
	}
	public String getInsuranceCard() {
		return insuranceCard;
	}
	public void setInsuranceCard(String insuranceCard) {
		this.insuranceCard = insuranceCard;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public Double getCashPaid() {
		return cashPaid;
	}
	public void setCashPaid(Double cashPaid) {
		this.cashPaid = cashPaid;
	}
	public Double getCardPaid() {
		return cardPaid;
	}
	public void setCardPaid(Double cardPaid) {
		this.cardPaid = cardPaid;
	}
	public Double getInsuranceAmount() {
		return insuranceAmount;
	}
	public void setInsuranceAmount(Double insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}
