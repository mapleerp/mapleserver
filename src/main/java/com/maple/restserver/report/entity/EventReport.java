package com.maple.restserver.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

 

public class EventReport {

	
	
	  private String memberId;
	  private String memberName;
	  private String familyName;
	  private String headOfFamily;

	  private Double amount;
	  private String eventName;
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getHeadOfFamily() {
		return headOfFamily;
	}
	public void setHeadOfFamily(String headOfFamily) {
		this.headOfFamily = headOfFamily;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	  
	
	  
		
	  
}
