package com.maple.restserver.report.entity;

import java.io.Serializable;

public class TransactionCountModel implements Serializable {
	private static final long serialVersionUID = 1L;

	int productionMstCount;
	int productionDtlCount;
	int purchaseHdrCount;
	int purchaseDtlCount;
	int actualProductionHdrCount;
	int actualProductionDtlCount;
	int itemBatchDtlCount;
	int ledgerClassCount;
	int debitClassCount;
	int creditClassCount;
	int accountClassCount;
	String branchCode;
	String comapnyMstId;
	int paymentCount;
	int receiptCount;
	
	public int getPaymentCount() {
		return paymentCount;
	}
	public void setPaymentCount(int paymentCount) {
		this.paymentCount = paymentCount;
	}
	public int getReceiptCount() {
		return receiptCount;
	}
	public void setReceiptCount(int receiptCount) {
		this.receiptCount = receiptCount;
	}
	public int getProductionMstCount() {
		return productionMstCount;
	}
	public void setProductionMstCount(int productionMstCount) {
		this.productionMstCount = productionMstCount;
	}
	public int getProductionDtlCount() {
		return productionDtlCount;
	}
	public void setProductionDtlCount(int productionDtlCount) {
		this.productionDtlCount = productionDtlCount;
	}
	public int getPurchaseHdrCount() {
		return purchaseHdrCount;
	}
	public void setPurchaseHdrCount(int purchaseHdrCount) {
		this.purchaseHdrCount = purchaseHdrCount;
	}
	public int getPurchaseDtlCount() {
		return purchaseDtlCount;
	}
	public void setPurchaseDtlCount(int purchaseDtlCount) {
		this.purchaseDtlCount = purchaseDtlCount;
	}
	public int getActualProductionHdrCount() {
		return actualProductionHdrCount;
	}
	public void setActualProductionHdrCount(int actualProductionHdrCount) {
		this.actualProductionHdrCount = actualProductionHdrCount;
	}
	public int getActualProductionDtlCount() {
		return actualProductionDtlCount;
	}
	public void setActualProductionDtlCount(int actualProductionDtlCount) {
		this.actualProductionDtlCount = actualProductionDtlCount;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public String getComapnyMstId() {
		return comapnyMstId;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setComapnyMstId(String comapnyMstId) {
		this.comapnyMstId = comapnyMstId;
	}
	
	public int getItemBatchDtlCount() {
		return itemBatchDtlCount;
	}
	public void setItemBatchDtlCount(int itemBatchDtlCount) {
		this.itemBatchDtlCount = itemBatchDtlCount;
	}
	
	public int getLedgerClassCount() {
		return ledgerClassCount;
	}
	public void setLedgerClassCount(int ledgerClassCount) {
		this.ledgerClassCount = ledgerClassCount;
	}
	public int getDebitClassCount() {
		return debitClassCount;
	}
	public void setDebitClassCount(int debitClassCount) {
		this.debitClassCount = debitClassCount;
	}
	public int getCreditClassCount() {
		return creditClassCount;
	}
	public void setCreditClassCount(int creditClassCount) {
		this.creditClassCount = creditClassCount;
	}
	public int getAccountClassCount() {
		return accountClassCount;
	}
	public void setAccountClassCount(int accountClassCount) {
		this.accountClassCount = accountClassCount;
	}
	@Override
	public String toString() {
		return "TransactionCountModel [productionMstCount=" + productionMstCount + ", productionDtlCount="
				+ productionDtlCount + ", purchaseHdrCount=" + purchaseHdrCount + ", purchaseDtlCount="
				+ purchaseDtlCount + ", actualProductionHdrCount=" + actualProductionHdrCount
				+ ", actualProductionDtlCount=" + actualProductionDtlCount + ", itemBatchDtlCount=" + itemBatchDtlCount
				+ ", ledgerClassCount=" + ledgerClassCount + ", debitClassCount=" + debitClassCount
				+ ", creditClassCount=" + creditClassCount + ", accountClassCount=" + accountClassCount
				+ ", branchCode=" + branchCode + ", comapnyMstId=" + comapnyMstId + "]";
	}

	
}
