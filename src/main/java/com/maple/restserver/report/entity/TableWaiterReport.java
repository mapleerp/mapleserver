package com.maple.restserver.report.entity;

public class TableWaiterReport {
 String tableName;
 String waiterName;
 String customiseSalesMode;
 
public String getCustomiseSalesMode() {
	return customiseSalesMode;
}
public void setCustomiseSalesMode(String customiseSalesMode) {
	this.customiseSalesMode = customiseSalesMode;
}
public String getTableName() {
	return tableName;
}
public void setTableName(String tableName) {
	this.tableName = tableName;
}
public String getWaiterName() {
	return waiterName;
}
public void setWaiterName(String waiterName) {
	this.waiterName = waiterName;
}
@Override
public String toString() {
	return "TableWaiterReport [tableName=" + tableName + ", waiterName=" + waiterName + "]";
}
 

}
