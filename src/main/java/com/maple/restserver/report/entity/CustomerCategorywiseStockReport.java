package com.maple.restserver.report.entity;

public class CustomerCategorywiseStockReport {

	String categoryName;
	String itemName;
	Double qty;
	Double standardPrice;
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getStandardPrice() {
		return standardPrice;
	}
	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}
	@Override
	public String toString() {
		return "CustomerCategorywiseStockReport [categoryName=" + categoryName + ", itemName=" + itemName + ", qty="
				+ qty + ", standardPrice=" + standardPrice + "]";
	}
	
	
}
