package com.maple.restserver.report.entity;

import java.util.Date;

public class OrgPaymentVoucher {
	
	String branchName;
	String voucherNO;
	Date voucherDate;
	String remark;
	Double amount;
	String companyName;
	String accountName;
	Double totalAmount;
	String instrumentNumber;
	Date instrumentDate;
	String modeOfPayment;
	String branchEmail;
	String branchWebsite;
	String memberId;
	String familyName;
	String memberName;
	
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	public String getInstrumentNumber() {
		return instrumentNumber;
	}
	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}
	public Date getInstrumentDate() {
		return instrumentDate;
	}
	public void setInstrumentDate(Date instrumentDate) {
		this.instrumentDate = instrumentDate;
	}
	public String getModeOfPayment() {
		return modeOfPayment;
	}
	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getVoucherNO() {
		return voucherNO;
	}
	public void setVoucherNO(String voucherNO) {
		this.voucherNO = voucherNO;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	@Override
	public String toString() {
		return "PaymentVoucher [branchName=" + branchName + ", voucherNO=" + voucherNO + ", voucherDate=" + voucherDate
				+ ", remark=" + remark + ", amount=" + amount + ", companyName=" + companyName + "]";
	}
	
	
	

}
