package com.maple.restserver.report.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.stereotype.Component;


@Component
public class TaxSummaryMst {
	@Id
   	String id;
	String taxPercentage;
	Double taxAmount;
	
	Double sgstTaxRate;
	Double sgstAmount;
	
	Double cgstTaxRate;
	Double cgstAmount;
	
	Double igstTaxRate;
	Double igstAmount;
	
	
	Double rate;
	Double amount;
	
    Double taxRate;
    Double sumOfigstAmount;
    
    Double sumOfsgstAmount;
    Double sumOfcgstAmount;
	
	 
	public Double getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTaxPercentage() {
		return taxPercentage;
	}
	public void setTaxPercentage(String taxPercentage) {
		this.taxPercentage = taxPercentage;
	}
	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}
	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}
	public Double getSgstAmount() {
		return sgstAmount;
	}
	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}
	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}
	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}
	public Double getCgstAmount() {
		return cgstAmount;
	}
	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}
	public Double getIgstTaxRate() {
		return igstTaxRate;
	}
	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}
	public Double getIgstAmount() {
		return igstAmount;
	}
	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getSumOfigstAmount() {
		return sumOfigstAmount;
	}
	public void setSumOfigstAmount(Double sumOfigstAmount) {
		this.sumOfigstAmount = sumOfigstAmount;
	}
	public Double getSumOfsgstAmount() {
		return sumOfsgstAmount;
	}
	public void setSumOfsgstAmount(Double sumOfsgstAmount) {
		this.sumOfsgstAmount = sumOfsgstAmount;
	}
	public Double getSumOfcgstAmount() {
		return sumOfcgstAmount;
	}
	public void setSumOfcgstAmount(Double sumOfcgstAmount) {
		this.sumOfcgstAmount = sumOfcgstAmount;
	}
	
}

 