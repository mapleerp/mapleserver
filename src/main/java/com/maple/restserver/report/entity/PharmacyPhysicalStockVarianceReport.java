package com.maple.restserver.report.entity;

import org.springframework.stereotype.Component;

@Component
public class PharmacyPhysicalStockVarianceReport {
	String id;
	String stockcorrectionDate;
	String voucherNumber;
	String itemGroup;
	String itemName;
	
	String itemCode;
	String batchCode;
	String userName;
	
	Double systemQty;
	Double physicalQty;
	Double varianceQty;
	Double price;
	Double cost;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStockcorrectionDate() {
		return stockcorrectionDate;
	}
	public void setStockcorrectionDate(String stockcorrectionDate) {
		this.stockcorrectionDate = stockcorrectionDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getItemGroup() {
		return itemGroup;
	}
	public void setItemGroup(String itemGroup) {
		this.itemGroup = itemGroup;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Double getSystemQty() {
		return systemQty;
	}
	public void setSystemQty(Double systemQty) {
		this.systemQty = systemQty;
	}
	public Double getPhysicalQty() {
		return physicalQty;
	}
	public void setPhysicalQty(Double physicalQty) {
		this.physicalQty = physicalQty;
	}
	public Double getVarianceQty() {
		return varianceQty;
	}
	public void setVarianceQty(Double varianceQty) {
		this.varianceQty = varianceQty;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	@Override
	public String toString() {
		return "PharmacyPhysicalStockVarianceReport [id=" + id + ", stockcorrectionDate=" + stockcorrectionDate
				+ ", voucherNumber=" + voucherNumber + ", itemGroup=" + itemGroup + ", itemName=" + itemName
				+ ", itemCode=" + itemCode + ", batchCode=" + batchCode + ", userName=" + userName + ", systemQty="
				+ systemQty + ", physicalQty=" + physicalQty + ", varianceQty=" + varianceQty + ", price=" + price
				+ ", cost=" + cost + "]";
	}
	
	
	
	
	
}
