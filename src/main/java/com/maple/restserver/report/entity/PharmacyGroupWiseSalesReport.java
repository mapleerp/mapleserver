package com.maple.restserver.report.entity;

public class PharmacyGroupWiseSalesReport {

	
	String itemName;
	Double amount;
	Double tax;
	String batchCode;
	Double qty;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	@Override
	public String toString() {
		return "PharmacyGroupWiseSalesReport [itemName=" + itemName + ", amount=" + amount + ", tax=" + tax
				+ ", batchCode=" + batchCode + ", qty=" + qty + "]";
	}
	
	
}
