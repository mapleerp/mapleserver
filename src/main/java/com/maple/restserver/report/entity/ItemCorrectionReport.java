package com.maple.restserver.report.entity;

public class ItemCorrectionReport {
	
	
	String itemName;
	String itemId;
	Double stockCount;
	Double salesCount;
	Double purchaseCount;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getStockCount() {
		return stockCount;
	}
	public void setStockCount(Double stockCount) {
		this.stockCount = stockCount;
	}
	public Double getSalesCount() {
		return salesCount;
	}
	public void setSalesCount(Double salesCount) {
		this.salesCount = salesCount;
	}
	public Double getPurchaseCount() {
		return purchaseCount;
	}
	public void setPurchaseCount(Double purchaseCount) {
		this.purchaseCount = purchaseCount;
	}
	@Override
	public String toString() {
		return "ItemCorrectionReport [itemName=" + itemName + ", itemId=" + itemId + ", stockCount=" + stockCount
				+ ", salesCount=" + salesCount + ", purchaseCount=" + purchaseCount + "]";
	}
	
	
}
