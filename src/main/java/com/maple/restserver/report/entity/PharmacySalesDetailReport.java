package com.maple.restserver.report.entity;

public class PharmacySalesDetailReport {

	String voucherNumber;
	String invoiceDate;
	Double qty;
	Double amount;
	Double tax;
	Double invoiceAmount;
	String customerName;
	String itemName;
    String groupName;
    String batchCode;
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}


	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	@Override
	public String toString() {
		return "PharmacySalesDetailReport [voucherNumber=" + voucherNumber + ", invoiceDate=" + invoiceDate + ", qty="
				+ qty + ", amount=" + amount + ", tax=" + tax + ", invoiceAmount=" + invoiceAmount + ", customerName="
				+ customerName + ", itemName=" + itemName + ", groupName=" + groupName + ", batchCode=" + batchCode
				+ "]";
	}
	
	
}
