package com.maple.restserver.report.entity;

public class GSTInputDtlAndSmryReport {
	
	
	String id;
	String invoiceDate;
	String invoiceNumber;
	String supplierName;
	String supplierGst;
	String branch;
	Double totalPurchaseExcludingGst;
	Double gstAmount;
	Double excemptedPurchase;
	Double totalPurchase;
	String gstPercent;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierGst() {
		return supplierGst;
	}
	public void setSupplierGst(String supplierGst) {
		this.supplierGst = supplierGst;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public Double getTotalPurchaseExcludingGst() {
		return totalPurchaseExcludingGst;
	}
	public void setTotalPurchaseExcludingGst(Double totalPurchaseExcludingGst) {
		this.totalPurchaseExcludingGst = totalPurchaseExcludingGst;
	}
	public Double getGstAmount() {
		return gstAmount;
	}
	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}
	public Double getExcemptedPurchase() {
		return excemptedPurchase;
	}
	public void setExcemptedPurchase(Double excemptedPurchase) {
		this.excemptedPurchase = excemptedPurchase;
	}
	public Double getTotalPurchase() {
		return totalPurchase;
	}
	public void setTotalPurchase(Double totalPurchase) {
		this.totalPurchase = totalPurchase;
	}
	public String getGstPercent() {
		return gstPercent;
	}
	public void setGstPercent(String gstPercent) {
		this.gstPercent = gstPercent;
	}
	
	
	

}
