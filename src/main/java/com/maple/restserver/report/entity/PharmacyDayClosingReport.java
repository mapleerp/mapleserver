
package com.maple.restserver.report.entity;

import org.springframework.stereotype.Component;

@Component
public class PharmacyDayClosingReport {
	
	private String branchCode;
	private Double totalcashSale;
	private Double totalcardSale;
	private Double totalcreditsale;
	private Double physicalCash;
	private Double totalbanksales;
	private Double totalinsurancesales;
	private Double grandtotal;
	private Double diffenceamount;
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Double getTotalcashSale() {
		return totalcashSale;
	}
	public void setTotalcashSale(Double totalcashSale) {
		this.totalcashSale = totalcashSale;
	}
	public Double getTotalcardSale() {
		return totalcardSale;
	}
	public void setTotalcardSale(Double totalcardSale) {
		this.totalcardSale = totalcardSale;
	}
	public Double getTotalcreditsale() {
		return totalcreditsale;
	}
	public void setTotalcreditsale(Double totalcreditsale) {
		this.totalcreditsale = totalcreditsale;
	}
	public Double getPhysicalCash() {
		return physicalCash;
	}
	public void setPhysicalCash(Double physicalCash) {
		this.physicalCash = physicalCash;
	}
	public Double getTotalbanksales() {
		return totalbanksales;
	}
	public void setTotalbanksales(Double totalbanksales) {
		this.totalbanksales = totalbanksales;
	}
	public Double getTotalinsurancesales() {
		return totalinsurancesales;
	}
	public void setTotalinsurancesales(Double totalinsurancesales) {
		this.totalinsurancesales = totalinsurancesales;
	}
	public Double getGrandtotal() {
		return grandtotal;
	}
	public void setGrandtotal(Double grandtotal) {
		this.grandtotal = grandtotal;
	}
	public Double getDiffenceamount() {
		return diffenceamount;
	}
	public void setDiffenceamount(Double diffenceamount) {
		this.diffenceamount = diffenceamount;
	}
	@Override
	public String toString() {
		return "PharmacyDayClosingReport [branchCode=" + branchCode + ", totalcashSale=" + totalcashSale
				+ ", totalcardSale=" + totalcardSale + ", totalcreditsale=" + totalcreditsale + ", physicalCash="
				+ physicalCash + ", totalbanksales=" + totalbanksales + ", totalinsurancesales=" + totalinsurancesales
				+ ", grandtotal=" + grandtotal + ", diffenceamount=" + diffenceamount + "]";
	}
	
	
	
	
	
}
