package com.maple.restserver.report.entity;


import java.util.Date;

public class FinishedProduct {

	
	Double actualQty;
	Double planingQty;
	String product;
	Double costPrice;
	Double value;
	String batch;
	Date voucherDate;
	
	

	public Double getActualQty() {
		return actualQty;
	}
	public void setActualQty(Double actualQty) {
		this.actualQty = actualQty;
	}
	public Double getPlaningQty() {
		return planingQty;
	}
	public void setPlaningQty(Double planingQty) {
		this.planingQty = planingQty;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Double getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	@Override
	public String toString() {
		return "FinishedProduct [actualQty=" + actualQty + ", planingQty=" + planingQty + ", product=" + product
				+ ", costPrice=" + costPrice + ", value=" + value + ", batch=" + batch + ", voucherDate=" + voucherDate
				+ "]";
	}
	
	
}
