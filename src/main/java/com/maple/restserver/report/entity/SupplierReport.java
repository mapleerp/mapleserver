package com.maple.restserver.report.entity;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SupplierReport {
	private String id;

	Integer userId;

	String deleted;

	
	private String supplierId;

	private String supplierName;


	private String company;


	private String address;

	private String phoneNo;

	
	private String emailid;

	
	private String supGST;

	String account_id;

	String state;
	
	
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getSupGST() {
		return supGST;
	}

	public void setSupGST(String supGST) {
		this.supGST = supGST;
	}

	public String getAccount_id() {
		return account_id;
	}

	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "SupplierReport [id=" + id + ", userId=" + userId + ", deleted=" + deleted + ", supplierId=" + supplierId
				+ ", supplierName=" + supplierName + ", company=" + company + ", address=" + address + ", phoneNo="
				+ phoneNo + ", emailid=" + emailid + ", supGST=" + supGST + ", account_id=" + account_id + ", state="
				+ state + ", getUserId()=" + getUserId() + ", getDeleted()=" + getDeleted() + ", getId()=" + getId()
				+ ", getSupplierId()=" + getSupplierId() + ", getSupplierName()=" + getSupplierName()
				+ ", getCompany()=" + getCompany() + ", getAddress()=" + getAddress() + ", getPhoneNo()=" + getPhoneNo()
				+ ", getEmailid()=" + getEmailid() + ", getSupGST()=" + getSupGST() + ", getAccount_id()="
				+ getAccount_id() + ", getState()=" + getState() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}

}
