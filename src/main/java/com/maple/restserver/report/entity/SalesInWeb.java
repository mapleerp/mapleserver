package com.maple.restserver.report.entity;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Entity;

import com.maple.restserver.entity.AccountHeads;

import com.maple.restserver.entity.ItemMst;


public class SalesInWeb {

//	private ArrayList<ItemMst> itemMst;
	private ArrayList<ItemDtlInWeb> itemDtlInWeb;
	private AccountHeads accountHeads;
	Date loginDate;
	Double invoiceAmount;
	String voucherNumber;
	String branch;


	
	
	
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public ArrayList<ItemDtlInWeb> getItemDtlInWeb() {
		return itemDtlInWeb;
	}
	public void setItemDtlInWeb(ArrayList<ItemDtlInWeb> itemDtlInWeb) {
		this.itemDtlInWeb = itemDtlInWeb;
	}
	
	
	public Date getLoginDate() {
		return loginDate;
	}
	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public AccountHeads getAccountHeads() {
		return accountHeads;
	}
	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}
	@Override
	public String toString() {
		return "SalesInWeb [itemDtlInWeb=" + itemDtlInWeb + ", accountHeads=" + accountHeads + ", loginDate="
				+ loginDate + ", invoiceAmount=" + invoiceAmount + ", voucherNumber=" + voucherNumber + ", branch="
				+ branch + "]";
	}

	
	
	
}
