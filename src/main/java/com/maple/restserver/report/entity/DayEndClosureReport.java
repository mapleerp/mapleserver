package com.maple.restserver.report.entity;

import java.sql.Date;

public class DayEndClosureReport {

	private String userId;
	private String userName;
	private String branchCode;
	private Double cashSale;
	private Double cardSale;
	private Double cardSale2;
	private Double physicalCash;
    private Date processDate;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Double getCashSale() {
		return cashSale;
	}
	public void setCashSale(Double cashSale) {
		this.cashSale = cashSale;
	}
	public Double getCardSale() {
		return cardSale;
	}
	public void setCardSale(Double cardSale) {
		this.cardSale = cardSale;
	}
	public Double getCardSale2() {
		return cardSale2;
	}
	public void setCardSale2(Double cardSale2) {
		this.cardSale2 = cardSale2;
	}
	public Double getPhysicalCash() {
		return physicalCash;
	}
	public void setPhysicalCash(Double physicalCash) {
		this.physicalCash = physicalCash;
	}
	public Date getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	@Override
	public String toString() {
		return "DayEndClosureReport [userId=" + userId + ", userName=" + userName + ", branchCode=" + branchCode
				+ ", cashSale=" + cashSale + ", cardSale=" + cardSale + ", cardSale2=" + cardSale2 + ", physicalCash="
				+ physicalCash + ", processDate=" + processDate + "]";
	}
    
    
}
