package com.maple.restserver.report.entity;

public class SaleOrderProductDtls {

	Double currentStock;
	Double saleOrderQty;
	Double totalRawMaterialWeight;
	String itemId;
	String mixName;
	String saleOrderTranHdrId;
	public Double getCurrentStock() {
		return currentStock;
	}
	public void setCurrentStock(Double currentStock) {
		this.currentStock = currentStock;
	}
	public Double getSaleOrderQty() {
		return saleOrderQty;
	}
	public void setSaleOrderQty(Double saleOrderQty) {
		this.saleOrderQty = saleOrderQty;
	}
	public Double getTotalRawMaterialWeight() {
		return totalRawMaterialWeight;
	}
	public void setTotalRawMaterialWeight(Double totalRawMaterialWeight) {
		this.totalRawMaterialWeight = totalRawMaterialWeight;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getMixName() {
		return mixName;
	}
	public void setMixName(String mixName) {
		this.mixName = mixName;
	}
	public String getSaleOrderTranHdrId() {
		return saleOrderTranHdrId;
	}
	public void setSaleOrderTranHdrId(String saleOrderTranHdrId) {
		this.saleOrderTranHdrId = saleOrderTranHdrId;
	}
	@Override
	public String toString() {
		return "SaleOrderProductDtls [currentStock=" + currentStock + ", saleOrderQty=" + saleOrderQty
				+ ", totalRawMaterialWeight=" + totalRawMaterialWeight + ", itemId=" + itemId + ", mixName=" + mixName
				+ ", saleOrderTranHdrId=" + saleOrderTranHdrId + "]";
	}
	
	
	
}
