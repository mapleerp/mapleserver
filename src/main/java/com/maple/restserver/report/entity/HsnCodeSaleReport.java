package com.maple.restserver.report.entity;

import java.util.Date;

public class HsnCodeSaleReport {

	
	Date voucherDate;
	String hsnCode;
	Double qty;
	Double sgst;
	Double cgst;
	Double value;
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getSgst() {
		return sgst;
	}
	public void setSgst(Double sgst) {
		this.sgst = sgst;
	}
	public Double getCgst() {
		return cgst;
	}
	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "HsnCodeSaleReport [voucherDate=" + voucherDate + ", hsnCode=" + hsnCode + ", qty=" + qty + ", sgst="
				+ sgst + ", cgst=" + cgst + ", value=" + value + "]";
	}

}
