package com.maple.restserver.report.entity;

import org.springframework.stereotype.Component;

@Component
public class PharmacyItemWiseSalesReport {
	String id;
	String invoiceDate;
	String itemName;
	String customerName;
	Double qty;
	Double unitPrice;
	String batch;
	String branchCode;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	@Override
	public String toString() {
		return "PharmacyItemWiseSalesReport [id=" + id + ", invoiceDate=" + invoiceDate + ", itemName=" + itemName
				+ ", customerName=" + customerName + ", qty=" + qty + ", unitPrice=" + unitPrice + ", batch=" + batch
				+ ", branchCode=" + branchCode + "]";
	}
	
	
	
}
