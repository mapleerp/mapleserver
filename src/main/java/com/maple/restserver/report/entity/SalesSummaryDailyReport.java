package com.maple.restserver.report.entity;

import java.math.BigDecimal;
import java.time.Month;
import java.time.Year;

public class SalesSummaryDailyReport {

	private String branchCode;
	
	private BigDecimal branchSales;
	private BigDecimal companySales;
	private Month voucherDate;
	private Year yearOfSales;
	
	public BigDecimal getBranchSales() {
		return branchSales;
	}
	public void setBranchSales(BigDecimal branchSales) {
		this.branchSales = branchSales;
	}
	public BigDecimal getCompanySales() {
		return companySales;
	}
	public void setCompanySales(BigDecimal companySales) {
		this.companySales = companySales;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	
	public Month getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Month voucherDate) {
		this.voucherDate = voucherDate;
	}
	public Year getYearOfSales() {
		return yearOfSales;
	}
	public void setYearOfSales(Year yearOfSales) {
		this.yearOfSales = yearOfSales;
	}
	@Override
	public String toString() {
		return "SalesSummaryDailyReport [branchCode=" + branchCode + ", branchSales=" + branchSales + ", companySales="
				+ companySales + ", voucherDate=" + voucherDate + ", yearOfSales=" + yearOfSales + "]";
	}
	
	
}
