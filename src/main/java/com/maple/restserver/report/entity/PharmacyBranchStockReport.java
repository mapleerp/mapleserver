package com.maple.restserver.report.entity;

import java.util.Date;

import org.springframework.stereotype.Component;
@Component
public class PharmacyBranchStockReport {
	
	private String groupName;
	
	private String itemName;
	
	private String batchCode;
	
	private String itemCode;
	
	private Date expiryDate;
	
	private Double qty;
	
	private Double rate;
	
	private Double amount;

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "PharmacyBranchStockReport [groupName=" + groupName + ", itemName=" + itemName + ", batchCode="
				+ batchCode + ", itemCode=" + itemCode + ", expiryDate=" + expiryDate + ", qty=" + qty + ", rate="
				+ rate + ", amount=" + amount + "]";
	}

	
}
