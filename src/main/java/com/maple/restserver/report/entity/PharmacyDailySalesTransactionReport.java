package com.maple.restserver.report.entity;

import org.springframework.stereotype.Component;

@Component
public class PharmacyDailySalesTransactionReport {
	String id;
	String date;
	String voucherNumber;
	String customerName;
	
	String tinNumber;
	String salesMan;
	private Double invoiceDiscount;
	private Double invoiceAmount;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getTinNumber() {
		return tinNumber;
	}
	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}
	public String getSalesMan() {
		return salesMan;
	}
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}
	public Double getInvoiceDiscount() {
		return invoiceDiscount;
	}
	public void setInvoiceDiscount(Double invoiceDiscount) {
		this.invoiceDiscount = invoiceDiscount;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	@Override
	public String toString() {
		return "PharmacyDailySalesTransactionReport [id=" + id + ", date=" + date + ", voucherNumber=" + voucherNumber
				+ ", customerName=" + customerName + ", tinNumber=" + tinNumber + ", salesMan=" + salesMan
				+ ", invoiceDiscount=" + invoiceDiscount + ", invoiceAmount=" + invoiceAmount + "]";
	}
	
	
}
