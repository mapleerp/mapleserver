package com.maple.restserver.jms.send;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Component
public class KafkaMapleEvent {

	int hashCode;
	String sourceFile ;

	String destination ;
	String source ="";
	
	String voucherNumber ;
    private Integer libraryEventId;
    private KafkaMapleEventType libraryEventType;
    @NotNull
    @Valid
    private String  payLoad;
    
    private String objectType ;
    
    
	public int getHashCode() {
		return hashCode;
	}
	public void setHashCode(int hashCode) {
		this.hashCode = hashCode;
	}
	public String getSourceFile() {
		return sourceFile;
	}
	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}

	
	
	public Integer getLibraryEventId() {
		return libraryEventId;
	}
	public void setLibraryEventId(Integer libraryEventId) {
		this.libraryEventId = libraryEventId;
	}
	public KafkaMapleEventType getLibraryEventType() {
		return libraryEventType;
	}
	public void setLibraryEventType(KafkaMapleEventType libraryEventType) {
		this.libraryEventType = libraryEventType;
	}
	public String getPayLoad() {
		return payLoad;
	}
	public void setPayLoad(String payLoad) {
		this.payLoad = payLoad;
	}
	 
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	@Override
	public String toString() {
		return "KafkaMapleEvent [sourceFile=" + sourceFile + ", destination=" + destination + ", source=" + source
				+ ", voucherNumber=" + voucherNumber + ", libraryEventId=" + libraryEventId + ", libraryEventType="
				+ libraryEventType + ", payLoad=" + payLoad + ", objectType=" + objectType + "]";
	}
	 
	 
 
}
