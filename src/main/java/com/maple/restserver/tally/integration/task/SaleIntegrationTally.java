package com.maple.restserver.tally.integration.task;

import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.httpclient.URI;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
 
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;



import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;

import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.service.accounting.task.SalesAccounting;
import com.maple.restserver.xml.sales.ALLINVENTORYENTRIES;
import com.maple.restserver.xml.sales.SALESDR_LEDGERENTRIES;
import com.maple.restserver.xml.sales.SALES_ENVELOPE;

@Component

public class SaleIntegrationTally {

	private static final Logger logger = LoggerFactory.getLogger(SalesAccounting.class);

 

	@Autowired
	private SalesTransHdrRepository salesTransHdrRepo;

	@Autowired
	CCommonXML cCommonXML;

	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	@Autowired
	ItemMstRepository itemMstRepository;

	@Autowired
	UnitMstRepository unitMstRepository;

	@Autowired
	CategoryMstRepository categoryMstRepository;

	@Autowired
	private CompanyMstRepository companyMstRepository;

	

	@Autowired
	private SalesDetailsRepository salesDtlRepo;

	@Autowired
	private AccountClassRepository accountClassRepo;

	@Autowired
	private AccountHeadsRepository accountHeadsRepo;

	@Autowired
	private CreditClassRepository creditClassRepo;

	@Autowired
	private DebitClassRepository debitClassRepo;

	@Autowired
	private LedgerClassRepository ledgerClassRepo;

	@Autowired
	private SalesReceiptsRepository salesReceiptsRepo;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;
	
	
	@Value("${TALLY_COMPANY}")
	private String TALLY_COMPANY;

	public boolean SaveCashSale(CompanyMst companyMst, String CostCenter, String Godown, String tallyVoucherType,
			String voucherNumber, String invoiceDate, AccountHeads ach, List<DebitClass> debitList,
			List<CreditClass> creditList,String tallyServer) throws SQLException {

		
		 

		boolean hasJournal = false;
		boolean JournalCompleted = true;

		String VoucherId = "";
		String ErrorCode = "";
		String Linerror = "";

		// "http://localhost:9050";

		invoiceDate = invoiceDate.replace("-", "");

		SALES_ENVELOPE env = new SALES_ENVELOPE();

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setNatureofsales("Domestic");

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setVCHTYPE(tallyVoucherType);
		// env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setClassname("VAT");

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setVouchertypename(tallyVoucherType);

		if(voucherNumber.length() > 15) {
			
			voucherNumber = voucherNumber.replaceAll("2019-2020", "19-20");
			voucherNumber = voucherNumber.replaceAll("2020-2021", "20-21");
			voucherNumber = voucherNumber.replaceAll("2021-2022", "21-22");
			
		}
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setVouchernumber(voucherNumber);

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setFbtpaymenttype("Default");

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher()
				.setPersistedview("Invoice Voucher View");

		env.getBody().getImpdate().getSrequestdec().setReportname("Vouchers");
		env.getBody().getImpdate().getSrequestdec().getStaticvar().setSvcurrentcompany(TALLY_COMPANY);

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setExciseopening("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setUseforfinalproduction("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIscancelled("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setHascashflow("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIspostdated("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setUsetrackingnumber("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIsinvoice("Yes");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setMfjournal("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setHasdiscounts("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setAspayslip("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIscostcenter("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIsstxnonrealizedvch("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIsdeleted("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setAsorginal("No");
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setVchfromsync("No");

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getBasicbuyeraddresslist()
				.setBASICBUYERADDRESS(ach.getAccountName());
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getBasicbuyeraddresslist()
				.setBASICBUYERADDRESS2(ach.getAccountName());
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setIsvatoverridden("Yes");

		boolean CustomerFound = true;
		boolean InsuranceFound = false;
		String DebitAccountName = "";
		String tempDebitAccountName = "";
		String InsuranceDebitAccountName = "";
		String CustomerDebitAccountName = "";

		BigDecimal bInsuranceAmount = new BigDecimal("0");

		String Remark = "";

		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher()
				.setPartyledgername(ach.getAccountName());
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setPartyname(ach.getAccountName());
		env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher()
				.setBasicbuyername(ach.getAccountName());

		
		
		
		/*
		 * if (diff.doubleValue() <1 && diff.doubleValue() > -1) {
				bdDrAmount = bdDrAmount.subtract(diff);
			}
			if (diff.doubleValue() > 0.0 && diff.doubleValue() < 1) {
				bdDrAmount = bdDrAmount.add(diff);
			}
		 */
		for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			DebitClass debitclass = (DebitClass) itr.next();

			BigDecimal bdDrAmount = new BigDecimal("0");

			bdDrAmount = debitclass.getDrAmount();

			BigDecimal crediutAmt = getCreditTotal(creditList);

			BigDecimal diff = bdDrAmount.subtract(crediutAmt);

			

			Remark = debitclass.getRemark();

			String drAccountID = debitclass.getAccountId();

			AccountHeads accountHeadsDR = accountHeadsRepository.findByIdAndCompanyMstId(drAccountID,
					companyMst.getId());

			BigDecimal bdRDrAmount = bdDrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			env.getHdr().setTallyrequest("Import Data");

			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setDate(invoiceDate);
			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setNarration(Remark);
			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher()
					.setBasicdatetimeofinvoice(invoiceDate);
			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher()
					.setBasicdatetimeofremoval(invoiceDate);

			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setEnteredby("125");
			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setDiffactualqty("No");

			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().setEffectivedate(invoiceDate);

			SALESDR_LEDGERENTRIES sl = new SALESDR_LEDGERENTRIES();

			sl.getSalesoldauditentry().setOLDAUDITENTRYIDS("-1");

			sl.setLEDGERNAME(accountHeadsDR.getAccountName());
			//sl.setISDEEMEDPOSITIVE("Yes");

			sl.setLEDGERFROMITEM("No");
			sl.setREMOVEZEROENTRIES("No");
			sl.setISPARTYLEDGER("Yes");
			
			
			if(bdDrAmount.doubleValue() < 0) {
				sl.setISLASTDEEMEDPOSITIVE("No");
				sl.setAMOUNT(bdRDrAmount.abs().toPlainString());
			}else {
				sl.setISLASTDEEMEDPOSITIVE("Yes");
				sl.setAMOUNT("-" + bdRDrAmount.toPlainString());
			}
			
			

			sl.getSalesbillalloc().setNAME(voucherNumber);
			sl.getSalesbillalloc().setBILLTYPE("New Ref");
			sl.getSalesbillalloc().setTDSDEDUCTEEISSPECIALRATE("No");
			sl.getSalesbillalloc().setAMOUNT("-" + bdRDrAmount.toPlainString());

			env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getLedgerentries().add(sl);
		}

		if (!CustomerFound) {
			throw new SQLException("Error CUSTOMER_NOT_FOUND ");
		}

		for (Iterator citr = creditList.iterator(); citr.hasNext();) {
			CreditClass creditclass = (CreditClass) citr.next();

			// String CreditAccountName =
			// APlicationWindow.cSqlFunctions.GetAccountNameFromAccountID(creditclass.getAccountID(),
			// APlicationWindow.LocalConn);

			// AccountHeads creditAccountHead
			// =accountHeadsRepository.findByIdAndCompanyMstId(creditclass.getAccountId(),
			// companyMst.getId());
			// String CreditAccountName = creditAccountHead.getAccountName() ;

			AccountHeads accountHeadsCR = accountHeadsRepository.findByIdAndCompanyMstId(creditclass.getAccountId(),
					companyMst.getId());

			String CreditAccountName = accountHeadsCR.getAccountName();
			if (null != creditclass.getItemId() && creditclass.getItemId().length() > 0) {

				ItemMst item = itemMstRepository.findById(creditclass.getItemId()).get();

				String unitid = item.getUnitId();

				UnitMst unitMst = unitMstRepository.findById(unitid).get();

				String unitname = unitMst.getUnitName();

				//CategoryMst categoryMst = categoryMstRepository.findById(item.getCategoryId()).get();

				/*
				 * 
				 * Create Item Name before each insertion
				 * 
				 * 
				 */

				/*try {

					cCommonXML.CreateUnitXML(unitMst.getUnitName(), "2", unitMst.getUnitDescription(), TallyServer,
							TALLY_COMPANY);

					cCommonXML.NewStockGroup(categoryMst.getCategoryName(), TallyServer, unitMst.getUnitName(),
							item.getBarCode(), TALLY_COMPANY, "Y");

					String GSTDTLAPPLICABLEFROM = "2018-04-01";

					GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("-", "");
					GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("/", "");

					GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("-", "");
					GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("/", "");

					cCommonXML.NewItem(item.getItemName(), TallyServer, item.getTaxRate().doubleValue(),
							unitMst.getUnitName(), item.getBarCode(), TALLY_COMPANY, "Y",
							categoryMst.getCategoryName(), item.getHsnCode(), GSTDTLAPPLICABLEFROM);

				} catch (Exception e) {

				}*/
				

				BigDecimal bdRate = new BigDecimal(creditclass.getRate());
				BigDecimal bdRRate = bdRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				String RatePerUnit = bdRRate.toPlainString() + "/" + unitname;
				BigDecimal bdQty = new BigDecimal(creditclass.getQty());
				BigDecimal bdRQty = bdQty.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				String QTYandUnit = bdRQty.toPlainString() + " " + unitname;

				BigDecimal bdAssessibleValue = creditclass.getCrAmount();
				BigDecimal bdRAssessibleValue = bdAssessibleValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				BigDecimal QTYINTORATE = bdRQty.multiply(bdRRate);
				BigDecimal TaxRate = new BigDecimal(item.getTaxRate());

				TaxRate = TaxRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				BigDecimal TAXINTOAMOUNT = QTYINTORATE.multiply(TaxRate);
				BigDecimal Hundred = new BigDecimal("100");
				BigDecimal bdVatAmount = TAXINTOAMOUNT.divide(Hundred);
				bdVatAmount = bdVatAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				ALLINVENTORYENTRIES alinv = new ALLINVENTORYENTRIES();

				alinv.setISDEEMEDPOSITIVE("No");
				alinv.setISLASTDEEMEDPOSITIVE("No");
				alinv.setISAUTONEGATE("No");
				alinv.setRATE(RatePerUnit);
				alinv.setAMOUNT(bdRAssessibleValue.toPlainString());
				alinv.setACTUALQTY(QTYandUnit);
				alinv.setBILLEDQTY(QTYandUnit);

				alinv.getBatchAllocation().setGODOWNNAME(CostCenter);
				// alinv.getBatchAllocation().setBATCHNAME("Primary Batch");
				alinv.getBatchAllocation().setAMOUNT(bdRAssessibleValue.toPlainString());
				alinv.getBatchAllocation().setACTUALQUANTITY(QTYandUnit);
				alinv.getBatchAllocation().setBILLEDQUANTITY(QTYandUnit);
				// alinv.getAccountingAllocation().setTAXCLASSIFICATIONNAME("Output VAT@5%");

				alinv.getAccountingAllocation().setLEDGERNAME(CreditAccountName);

				alinv.getAccountingAllocation().setCLASSRATE("0.00000");
				alinv.getAccountingAllocation().setISDEEMEDPOSITIVE("No");
				alinv.getAccountingAllocation().setLEDGERFROMITEM("No");
				alinv.getAccountingAllocation().setREMOVEZEROENTRIES("No");
				alinv.getAccountingAllocation().setISPARTYLEDGER("No");
				alinv.getAccountingAllocation().setISLASTDEEMEDPOSITIVE("No");
				alinv.getAccountingAllocation().setAMOUNT(bdRAssessibleValue.toPlainString());
				alinv.getAccountingAllocation().getCatagoryallocation().setCATEGORY("Primary Cost Category");
				alinv.getAccountingAllocation().getCatagoryallocation().setISDEEMEDPOSITIVE("No");
				//alinv.getAccountingAllocation().getCatagoryallocation().getCostcenterallocation().setNAME(CostCenter);
				//alinv.getAccountingAllocation().getCatagoryallocation().getCostcenterallocation()
					//	.setAMOUNT(bdRAssessibleValue.toPlainString());

				alinv.setSTOCKITEMNAME(item.getItemName());
				env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getAllinventoryList()
						.add(alinv);

				if (bdVatAmount.doubleValue() > 0) {
					alinv.getAccountingAllocation().setSTATNATURENAME("Sales Taxable");
					alinv.getAccountingAllocation().setVATTAXRATE(TaxRate.toPlainString());
				} else {
					alinv.getAccountingAllocation().setSTATNATURENAME("Sales Exempt");
				}

			} else {

				SALESDR_LEDGERENTRIES sl12 = new SALESDR_LEDGERENTRIES();

				sl12.setLEDGERNAME(CreditAccountName);

				sl12.setISDEEMEDPOSITIVE("No");
				sl12.setLEDGERFROMITEM("No");

				BigDecimal bdTaxAmt = creditclass.getCrAmount();

				BigDecimal bdRTaxAmt = bdTaxAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				sl12.setAMOUNT(bdRTaxAmt.toPlainString());

				env.getBody().getImpdate().getSalesrequestdata().getTmsg().getVoucher().getLedgerentries().add(sl12);

			}
		}

		/* init jaxb marshaler */
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(SALES_ENVELOPE.class);

			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			/* set this flag to true to format the output */
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			// jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING "xml" );
			// jaxbMarshaller.setProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
			// jaxbMarshaller.setProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			// jaxbMarshaller.setProperty(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION,
			// "yes");

			/* marshaling of java objects in xml (output to file and standard output) */
			jaxbMarshaller.marshal(env, new File("xout.xml"));
			jaxbMarshaller.marshal(env, System.out);

			boolean DeleteXml = false;

			try {

				// set the connection timeout value to 30 seconds (10000
				// milliseconds)
				// final HttpParams httpParams = new BasicHttpParams();
				// HttpConnectionParams.setConnectionTimeout(httpParams,
				// connectionTimeOut);
				// httpParams

				final HttpParams httpParams = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
				// httpParams
				HttpClient httpclient = new DefaultHttpClient(httpParams);

				
				
				// TallyServer ="http://192.168.1.6:9051";

				HttpPost httppost = new HttpPost(tallyServer);
				File file = new File("xout.xml");
				FileEntity entity = new FileEntity(file, "text/plain; charset=\"UTF-8\""); // ,

				httppost.setEntity(entity);
				System.out.println("executing request " + httppost.getRequestLine());
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity resEntity = response.getEntity();
				System.out.println("----------------------------------------");
				System.out.println(response.getStatusLine());
				if (resEntity != null) {
					try {
						DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
						DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
						Document doc = dBuilder.parse(resEntity.getContent());

						NodeList nList = doc.getElementsByTagName("RESPONSE");
						for (int temp = 0; temp < nList.getLength(); temp++) {
							Node nNode = nList.item(temp);

							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								Element eElement = (Element) nNode;

								try {
									VoucherId = eElement.getElementsByTagName("LASTVCHID").item(0).getTextContent();
								} catch (Exception e) {
									return true;
								}
								System.out.println("Response VoucherId =  " + VoucherId);

								ErrorCode = eElement.getElementsByTagName("ERRORS").item(0).getTextContent();

								if (ErrorCode.equalsIgnoreCase("0")) {

									if (hasJournal && !JournalCompleted) {
										return false;
									} else if (!hasJournal) {

										return true;
									}
								}
								System.out.println("ErrorCode =  " + ErrorCode);

								Linerror = eElement.getElementsByTagName("LINEERROR").item(0).getTextContent();
								System.out.println("Linerror =  " + Linerror);

								if (Linerror.contains("Voucher Number") && Linerror.contains("already exists!")) {

									System.out.println(Linerror);
									return true;

								}
								cCommonXML.processReturnStatus(Linerror, companyMst, tallyServer,TALLY_COMPANY, "Sales");

								if (ErrorCode.equalsIgnoreCase("1")) {

									return false;
								}

							}

						}

						System.out.println("Response content length: " + resEntity.getContentLength());
						System.out.println(resEntity.toString());
						System.out.println("Chunked?: " + resEntity.isChunked());

					} catch (Exception e2) {
						throw new SQLException(e2.toString());

					}
				}
				/*
				 * delete the file
				 */

				if (DeleteXml) {
					if (file.delete()) {
						System.out.println(file.getName() + " is deleted!");
					} else {
						System.out.println("Delete operation is failed.");
					}
				}

			} catch (Exception est) {
				throw new SQLException(est.toString());
			}

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		return false;

	}

	public  BigDecimal getCreditTotal(List<CreditClass> creditList) {
		double roundOff = 0.0;
		BigDecimal bdAssessibleValue = new BigDecimal("0");

		for (Iterator citr = creditList.iterator(); citr.hasNext();) {
			CreditClass creditclass = (CreditClass) citr.next();

			/*
			 * if (null != creditclass.getRate()) {
			 * 
			 * BigDecimal bdRate = new BigDecimal(creditclass.getRate()); BigDecimal bdRRate
			 * = bdRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			 * 
			 * BigDecimal bdQty = new BigDecimal(creditclass.getQty()); BigDecimal bdRQty =
			 * bdQty.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			 * 
			 * BigDecimal bdTaxAmt = creditclass.getCrAmount();
			 * 
			 * bdAssessibleValue =
			 * bdAssessibleValue.add(creditclass.getCrAmount().add(bdTaxAmt)); } else {
			 * BigDecimal bdTaxAmt = creditclass.getCrAmount(); bdAssessibleValue =
			 * bdAssessibleValue.add(bdTaxAmt); }
			 */
			BigDecimal bdTaxAmt = creditclass.getCrAmount();
			bdAssessibleValue = bdAssessibleValue.add(bdTaxAmt);

		}

		return bdAssessibleValue;
	}
	
	
	public BigDecimal getDebitTotal(List<DebitClass> debitList) {
		double roundOff = 0.0;
		BigDecimal bdAssessibleValue = new BigDecimal("0");

		for (Iterator citr = debitList.iterator(); citr.hasNext();) {
			DebitClass debitclass = (DebitClass) citr.next();

			if (null != debitclass.getDrAmount()) {

				bdAssessibleValue = bdAssessibleValue.add(debitclass.getDrAmount());
			}
			 

		}

		return bdAssessibleValue;
	}


 /*
	protected void executeInternal (String  companyMst  , 	String branchMst) {

		List<LmsQueueMst> jobInfoList = lmsQueueMstRepository.findLmsQueueMstNotPosted();
		if (jobInfoList != null) {

			jobInfoList.forEach(jobInfo -> {

				BigDecimal zero = new BigDecimal("0");

				String voucherNumber = jobInfo.getVoucherNumber();
				Date voucherDate = jobInfo.getVoucherDate();

				String sourvceId = jobInfo.getSourceObjectId();
				String lmsqueueId = jobInfo.getId();

				String id = jobInfo.getSourceObjectId();

		 
			 

				String goDownName = branchMst;

				java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

				Optional<SalesTransHdr> salesTransHdrOpt = salesTransHdrRepo.findById(id);
				SalesTransHdr salesTransHdr = salesTransHdrOpt.get();

				String strVoucherDate = SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

				System.out.println("Company ID " + companyMst);

				List<AccountClass> accountClassPreviousList = accountClassRepo
						.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

				if (accountClassPreviousList.size() > 0) {
					logger.info("Found previous records.. Deleting");

					Iterator iter = accountClassPreviousList.iterator();
					while (iter.hasNext()) {
						AccountClass accountClass = (AccountClass) iter.next();
						logger.info("Found previous records.. Deleting" + accountClass.getSourceVoucherNumber()
								+ accountClass.getBrachCode());

						List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
						List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);

						List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);

						if (creditClassList.size() > 0) {

							try {
								boolean result = SaveCashSale(accountClass.getCompanyMst(),
										salesTransHdr.getBranchCode(), "GODOWN", salesTransHdr.getSalesMode(),
										voucherNumber, strVoucherDate, salesTransHdr.getCustomerMst(), debitClassList,
										creditClassList,tallyServer);
								if (result) {
									LmsQueueMst lmsQueueMst = lmsQueueMstRepository.findById(lmsqueueId).get();
									lmsQueueMst.setPostedToServer("YES");
									lmsQueueMstRepository.save(lmsQueueMst);
									//deleteJob( context) ;
								}
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								logger.error(e.getMessage());
							}
						}

					}
				}

			});
		}
	}
	*/

	
}
