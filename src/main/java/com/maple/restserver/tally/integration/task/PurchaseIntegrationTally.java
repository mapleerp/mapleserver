package com.maple.restserver.tally.integration.task;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Connection;
 
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
 
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
 
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
 
import com.jamesmurty.utils.XMLBuilder;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;

import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesReceiptsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.service.accounting.task.SalesAccounting;
import com.maple.restserver.xml.purchase.ALLINVENTORYENTRIESLIST;
import com.maple.restserver.xml.purchase.ENVELOPE;
import com.maple.restserver.xml.purchase.LEDGERENTRIESLIST;

@Component
 
public class PurchaseIntegrationTally  {

	private static final Logger logger = LoggerFactory.getLogger(SalesAccounting.class);


	@Value("${tallyServer}")
	private String tallyServer;
	
	static int connectionTimeOut = 10000;
	 
	 
	
	@Autowired
	private SalesTransHdrRepository salesTransHdrRepo;
	
	@Autowired
	CCommonXML cCommonXML;

	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	@Autowired
	ItemMstRepository itemMstRepository;

	@Autowired
	UnitMstRepository unitMstRepository;

	@Autowired
	CategoryMstRepository categoryMstRepository;
	
	
	@Autowired
	private CompanyMstRepository  companyMstRepository;
	
 
	
 

	@Autowired
	private AccountClassRepository accountClassRepo;

	@Autowired
	private AccountHeadsRepository accountHeadsRepo;

	@Autowired
	private CreditClassRepository creditClassRepo;

	@Autowired
	private DebitClassRepository debitClassRepo;

	@Autowired
	private LedgerClassRepository ledgerClassRepo;
	
	
	@Autowired
	PurchaseHdrRepository purchaseHdrRepository;
	



 
	@Value("${TALLY_COMPANY}")
	private String TALLY_COMPANY;
	
	
	public boolean SavePurchase(CompanyMst companyMst , String TallyServer,
			String branchCode, String voucherNumber, String invoiceDate, Date SupplierInvoiceDate , 
			String SupplierInvoice,
			String supplierId,
			List<DebitClass> debitList, List<CreditClass> creditList , String voucherType) throws SQLException {
		
		 
	 
		String ErrorCode = "";
		String Linerror = "";
		 
		 
		String VoucherId ="";
		
		
	  
		
		
		String CostCenter = branchCode ; 
		String Godown ="GODOWN" ; 
		
		 
		invoiceDate = invoiceDate.replace("-", "");
		
		
		String SupinvoiceDate =ClientSystemSetting.SqlDateTostringYYMMDD(SupplierInvoiceDate);
		
		SupinvoiceDate = SupinvoiceDate.replace("-", "");
		 
		
		
		ENVELOPE env = new ENVELOPE();

		
		env.getHEADER().setTALLYREQUEST("Import Data");

		env.getBODY().getIMPORTDATA().getREQUESTDESC().setREPORTNAME("Vouchers");
		env.getBODY().getIMPORTDATA().getREQUESTDESC().getSTATICVARIABLES().setSVCURRENTCOMPANY(TALLY_COMPANY);
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setACTION("Create");
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER()
				.setOBJVIEW("Invoice Voucher View");

		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setPERSISTEDVIEW("Invoice Voucher View");
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().getOLDAUDITENTRYIDSLIST()
				.setOLDAUDITENTRYIDS("-1");
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setDATE(invoiceDate);
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setREFERENCEDATE(SupinvoiceDate);
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setREFERENCE(SupplierInvoice);
		env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setEFFECTIVEDATE(invoiceDate);
	
		BigDecimal bdCrAmount = new BigDecimal("0");
		
		String CreditAccountName  = "";
		boolean FoundSupplier= false;
		for (Iterator itr = creditList.iterator(); itr.hasNext();) {
			CreditClass creditclass = (CreditClass) itr.next();
			
			creditclass.setCrAmount(creditclass.getCrAmount().setScale(2, BigDecimal.ROUND_HALF_EVEN));
			
			bdCrAmount = bdCrAmount.add(creditclass.getCrAmount());
		}
		
		BigDecimal bdRCrAmount = bdCrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		
		
		
		BigDecimal bdDrAmount = new BigDecimal("0");
		
		for (Iterator itr =debitList.iterator(); itr.hasNext();) {
			DebitClass debitclass = (DebitClass) itr.next();
			
			debitclass.setDrAmount(debitclass.getDrAmount().setScale(2, BigDecimal.ROUND_HALF_EVEN));
			
			bdDrAmount = bdDrAmount.add(debitclass.getDrAmount());
		}
		
		BigDecimal bd = bdRCrAmount.subtract(bdDrAmount);
		if(bd.doubleValue()>0) {
			bdRCrAmount = bdCrAmount.subtract(bd);
		}
		
		if(bd.doubleValue()<0) {
			bdRCrAmount = bdCrAmount.add(bd.abs());
		}
		
		  bdRCrAmount = bdRCrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		  
		AccountHeads sup = accountHeadsRepo.findById(supplierId).get();
		
	if(null!=sup) {
		FoundSupplier = true;
	}
	
	
		
			if(!FoundSupplier){
				
				
				throw new SQLException("Supplier Not Found");
				
			}
			CreditAccountName = sup.getAccountName();
			  
			 
			env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setSTATENAME(sup.getCustomerState());
		
			env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setBASICBUYERSSALESTAXNO(sup.getPartyGst());
			
		//	BigDecimal bdCrAmount = creditclass.getCrAmount();
	
			
				

			

			env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setPARTYNAME(CreditAccountName);
			env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setVOUCHERTYPENAME(voucherType);

			env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setVOUCHERNUMBER(voucherNumber);
			
			env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setPARTYLEDGERNAME(CreditAccountName);
			env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setBASICBASEPARTYNAME(CreditAccountName);
			env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setISINVOICE("Yes");
			env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().setISVATOVERRIDDEN("Yes");
			env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER()
					.setBASICDATETIMEOFINVOICE(invoiceDate);

			LEDGERENTRIESLIST sl = new LEDGERENTRIESLIST();
			sl.getOLDAUDITENTRYIDSLIST().setOLDAUDITENTRYIDS("1");
			sl.setLEDGERNAME(CreditAccountName);
			sl.setISDEEMEDPOSITIVE("No");
			sl.setLEDGERFROMITEM("No");
			
		 
			sl.setAMOUNT(bdRCrAmount.toPlainString());

			sl.getBILLALLOCATIONSLIST().setNAME(SupplierInvoice);
			sl.getBILLALLOCATIONSLIST().setBILLTYPE("New Ref");
			sl.getBILLALLOCATIONSLIST().setTDSDEDUCTEEISSPECIALRATE("No");
			sl.getBILLALLOCATIONSLIST().setAMOUNT(bdRCrAmount.toPlainString());

			env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().getLEDGERENTRIESLIST().add(sl);
		
		
		for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			DebitClass debitclass = (DebitClass) itr.next();
		 
			 
			AccountHeads accountHeadsCR = accountHeadsRepository.findByIdAndCompanyMstId(debitclass.getAccountId(),
					companyMst.getId());
			String AccountName = accountHeadsCR.getAccountName();
			
			
			if ( (AccountName.contains("CGST")) || (AccountName.contains("SGST") ) || (AccountName.contains("IGST") ))  {
					
					LEDGERENTRIESLIST sl12 = new LEDGERENTRIESLIST();
					sl12.getOLDAUDITENTRYIDSLIST().setOLDAUDITENTRYIDS("1");
					sl12.setLEDGERNAME(AccountName);
					sl12.setISDEEMEDPOSITIVE("Yes");
					sl12.setLEDGERFROMITEM("No");
					
					BigDecimal bdTaxAmt  = debitclass.getDrAmount();
							
						 
					BigDecimal bdRTaxAmt = bdTaxAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					
					 
					
					String MinusBdRTaxAmt= "-"+bdRTaxAmt.toPlainString();
		
					sl12.setAMOUNT(MinusBdRTaxAmt);
		
					env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().getLEDGERENTRIESLIST().add(sl12);
			}else if (null!=debitclass.getItemId()  && debitclass.getItemId() .length()>0){
				
				ItemMst item =itemMstRepository.findById(debitclass.getItemId()).get();
				
				String unitid = item.getUnitId();
				UnitMst  unit = unitMstRepository.findById(unitid).get();
				//APlicationWindow.cSqlFunctions.getUnitNameFormID(unitid, APlicationWindow.LocalConn);
				BigDecimal bdRate = new BigDecimal(debitclass.getRate());
				BigDecimal bdRRate = bdRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				
				String RatePerUnit = bdRRate.toPlainString() +"/"+unit.getUnitName();
				
				
				BigDecimal bdQty = new BigDecimal(debitclass.getQty());
				BigDecimal bdRQty = bdQty.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				
				String QTYandUnit = bdRQty.toPlainString() +" "+unit.getUnitName();
				BigDecimal QTYINTORATE = bdRQty.multiply(bdRRate);
				BigDecimal TaxRate = new BigDecimal( item.getTaxRate()) ;
				
				TaxRate = TaxRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				
				BigDecimal TAXINTOAMOUNT = QTYINTORATE.multiply(TaxRate);
				BigDecimal Hundred = new BigDecimal("100");
				BigDecimal bdVatAmount = TAXINTOAMOUNT.divide(Hundred);
				bdVatAmount = bdVatAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				
				String MinusVatAcceptedTaxAmt =  "-" + bdVatAmount.toPlainString();

				BigDecimal bdAssessibleValue =debitclass.getDrAmount();
				BigDecimal bdRAssessibleValue = bdAssessibleValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				
				String MinusBdrAssessibleValue= "-"+bdRAssessibleValue.toPlainString();
				
 				
				ALLINVENTORYENTRIESLIST ae1 = new ALLINVENTORYENTRIESLIST();
				ae1.setSTOCKITEMNAME(item.getItemName());
				ae1.setISDEEMEDPOSITIVE("Yes");
				ae1.setISAUTONEGATE("No");
				ae1.setRATE(RatePerUnit);

				ae1.setAMOUNT(MinusBdrAssessibleValue);
				ae1.setACTUALQTY(QTYandUnit);
				ae1.setBILLEDQTY(QTYandUnit);
				ae1.getBATCHALLOCATIONSLIST().setGODOWNNAME(Godown);
				ae1.getBATCHALLOCATIONSLIST().setBATCHNAME("Primary Batch");
				ae1.getBATCHALLOCATIONSLIST().setAMOUNT(MinusBdrAssessibleValue);
				ae1.getBATCHALLOCATIONSLIST().setACTUALQTY(QTYandUnit);
				ae1.getBATCHALLOCATIONSLIST().setBILLEDQTY(QTYandUnit);
				ae1.getACCOUNTINGALLOCATIONSLIST().getOLDAUDITENTRYIDSLIST().setOLDAUDITENTRYIDS("-1");
				ae1.getACCOUNTINGALLOCATIONSLIST().setLEDGERNAME(AccountName);
				if(bdVatAmount.doubleValue()>0){
					ae1.getACCOUNTINGALLOCATIONSLIST().setSTATNATURENAME("Purchase Taxable");
					ae1.getACCOUNTINGALLOCATIONSLIST().setVATTAXRATE(TaxRate.toPlainString());
				}else{
					ae1.getACCOUNTINGALLOCATIONSLIST().setSTATNATURENAME("Purchase Exempt");
				}
				  
				ae1.getACCOUNTINGALLOCATIONSLIST().setISLASTDEEMEDPOSITIVE("yes");
				ae1.getACCOUNTINGALLOCATIONSLIST().setLEDGERFROMITEM("No");
				ae1.getACCOUNTINGALLOCATIONSLIST().setISPARTYLEDGER("No");
				ae1.getACCOUNTINGALLOCATIONSLIST().setAMOUNT(MinusBdrAssessibleValue);
				ae1.getACCOUNTINGALLOCATIONSLIST().setVATACCEPTEDTAXAMT(MinusVatAcceptedTaxAmt);

				env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().getALLINVENTORYENTRIESLIST()
						.add(ae1);
				
			}else {
					
					LEDGERENTRIESLIST sl12 = new LEDGERENTRIESLIST();
					sl12.getOLDAUDITENTRYIDSLIST().setOLDAUDITENTRYIDS("1");
					sl12.setLEDGERNAME(AccountName);
					sl12.setISDEEMEDPOSITIVE("Yes");
					sl12.setLEDGERFROMITEM("No");
					
					BigDecimal bdTaxAmt  = debitclass.getDrAmount();
							
						 
					BigDecimal bdRTaxAmt = bdTaxAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					
					 
					
					String MinusBdRTaxAmt= "-"+bdRTaxAmt.toPlainString();
		
					sl12.setAMOUNT(MinusBdRTaxAmt);
		
					env.getBODY().getIMPORTDATA().getREQUESTDATA().getTallymessage().getVOUCHER().getLEDGERENTRIESLIST().add(sl12);
			}
			
			
			
		}


		/* init jaxb marshaler */
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(ENVELOPE.class);

			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
			jaxbMarshaller.marshal(env, new File("pxout.xml"));
			jaxbMarshaller.marshal(env, System.out);

			 
			boolean DeleteXml = false;

			try {

				final HttpParams httpParams = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParams, connectionTimeOut);
				// httpParams
				HttpClient httpclient = new DefaultHttpClient(httpParams);

				HttpPost httppost = new HttpPost(TallyServer);
				File file = new File("pxout.xml");
				FileEntity entity = new FileEntity(file, "text/plain; charset=\"UTF-8\""); // ,

				httppost.setEntity(entity);
				System.out.println("executing request " + httppost.getRequestLine());
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity resEntity = response.getEntity();
				System.out.println("----------------------------------------");
				System.out.println(response.getStatusLine());

				if (resEntity != null) {
					try {
						DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
						DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
						Document doc = dBuilder.parse(resEntity.getContent());

						NodeList nList = doc.getElementsByTagName("RESPONSE");
						for (int temp = 0; temp < nList.getLength(); temp++) {
							Node nNode = nList.item(temp);

							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								Element eElement = (Element) nNode;
								VoucherId = eElement.getElementsByTagName("LASTVCHID").item(0).getTextContent();
								System.out.println("Response VoucherId =  " + VoucherId);

								ErrorCode  = eElement.getElementsByTagName("ERRORS").item(0)
										.getTextContent();

								 
								System.out.println("ErrorCode =  " + ErrorCode);

								Linerror = eElement.getElementsByTagName("LINEERROR").item(0).getTextContent();
								System.out.println("Linerror =  " + Linerror);

								if (Linerror.contains("Voucher Number") && Linerror.contains("already exists!")) {
									 
									System.out.println(Linerror);
									return true;
									
								}
								processReturnStatus(Linerror,companyMst,TallyServer);
							
								if(ErrorCode.equalsIgnoreCase("1")){
									
									return false;
								}

							}

						}

						System.out.println("Response content length: " + resEntity.getContentLength());
						System.out.println(resEntity.toString());
						System.out.println("Chunked?: " + resEntity.isChunked());

					} catch (Exception e2) {
						throw new SQLException(e2.toString());

					}
				}

				/*
				 * delete the file
				 */

				if (DeleteXml) {
					if (file.delete()) {
						System.out.println(file.getName() + " is deleted!");
					} else {
						System.out.println("Delete operation is failed.");
					}
				}

			} catch (Exception est) {
				System.out.println(est);
				
				if(est.toString().contains("Connection") && est.toString().contains("refused")){
					throw new SQLException("CONNECTION_REFUSED");
				}else{
					throw new SQLException(est.toString());
				}
				
			}

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}

		 

		return true;

	}
	
	


	private void processReturnStatus(String Linerror, CompanyMst companyMst, String TallyServer) throws SQLException{
		if (Linerror.contains("Cost Centre") && Linerror.contains("does not exist!")) {
			String goDownName = Linerror.replace("Cost Centre", "").replace("does not exist!", "")
					.replace("'", "").trim();
 
			 
			cCommonXML.CreateCostCenterXML(  goDownName, 
			              TallyServer,   TALLY_COMPANY 
			            ) ;
			 
			 
			

			throw new SQLException("TRYAGAIN");
		}

		
		if (Linerror.contains("Voucher totals do not match!")) {
			 

			throw new SQLException("Error"  + Linerror);
		}
		
		if (Linerror.contains("No Accounting Allocations for")) {
			 

			throw new SQLException("Error"  + Linerror);
		}
		
		if (Linerror.contains("Ledger") && Linerror.contains("does not exist!")) {
			String missingLedger = Linerror.replace("Ledger", "").replace("does not exist!", "")
					.replace("'", "").trim();

			System.out.println("missingLedger =  " + missingLedger);
			
			
			String parentAccount =getParentAccount(missingLedger);
			
			
		
			
			String affectstock = "YES";
			if(null!=parentAccount && parentAccount.equalsIgnoreCase("0")){
				if(missingLedger.toUpperCase().contains("SALES@")){
					parentAccount = "Sales Accounts";
					missingLedger= missingLedger+"%";
					
				}else if (missingLedger.toUpperCase().contains("PURCHASE@")){
					parentAccount = "Purchase Accounts";
					missingLedger = missingLedger + "%";
				}else if (missingLedger.toUpperCase().contains("OUTPUT VAT@")){
					parentAccount = "Duties & Taxes";
					missingLedger = missingLedger + "%";
				}else  if (missingLedger.contains("_CASH")){
					parentAccount = "Cash-in-hand";
					affectstock ="NO";
				}
			}
		
	
	 
			AccountHeads supOpt = accountHeadsRepo.findByAccountNameAndCompanyMst(missingLedger,companyMst) ;
			
			if(null!=supOpt) {
			parentAccount = "Sundry Creditors";
			affectstock = "NO";
			AccountHeads sup = supOpt;
			cCommonXML.CreateSupplier(missingLedger,sup.getPartyGst(), sup.getPartyAddress1(),
								 sup.getId(), parentAccount,
					            TallyServer,     TALLY_COMPANY,
					           affectstock,
					             sup.getPartyMail(),"INDIA", 
					           "Regular", sup.getCustomerState());
					            
					 
					
				
			}else{
				affectstock = "YES";
				  if(!missingLedger.equalsIgnoreCase("0")){
					  
					  /*
					   * String AcName, String TinNo, String Address, String Alias, String ParentAccountHead,
            String TallyServer,   CompanyMst companyMst,
             String affectstock
					   */
					  cCommonXML.LedgerXML(missingLedger,"", "", "", parentAccount,
				            TallyServer,     companyMst,
				              affectstock,TALLY_COMPANY);
				  }
			
			}
			throw new SQLException("TRYAGAIN");
		}

		
		if (Linerror.contains("Voucher Type") && Linerror.contains("does not exist!")) {
			String missingLedger = Linerror.replace("Voucher Type", "").replace("does not exist!", "")
					.replace("'", "").trim();

			System.out.println("missingLedger =  " + missingLedger);
			
			
			String parentAccount = "Purchase";
			
			 
			
			  
			cCommonXML.CreateVoucherTypeXML( missingLedger,  parentAccount,
			 		     "Manual",  "Yes",
			             TallyServer ,  TALLY_COMPANY
			            );
			 
			 
			

			throw new SQLException("TRYAGAIN");
		}

		
		if (Linerror.contains("Stock Item") && Linerror.contains("does not exist!")) {
			String missingStockItem = Linerror.replace("Stock Item", "")
					.replace("does not exist!", "").replace("'", "").trim();

			System.out.println("missingLedger =  " + missingStockItem);
			
			ItemMst itemMst = itemMstRepository.findByItemName(missingStockItem);
			UnitMst unitMst = unitMstRepository.findById(itemMst.getUnitId()).get();
			
			
			String affectstock ="YES";
			
			
			CategoryMst categoryMst = categoryMstRepository.findById(itemMst.getCategoryId()).get();
					
					
		 
			
	  
			
			cCommonXML. CreateUnitXML(  unitMst.getUnitName(), "2",  unitMst.getUnitDescription(),
		              TallyServer,     TALLY_COMPANY
		                ) ;
	
			
			
			cCommonXML.NewStockGroup(categoryMst.getCategoryName(),  TallyServer,    
					 unitMst.getUnitName(),    itemMst.getBarCode(),   TALLY_COMPANY,  affectstock);
			
			 		

				String GSTDTLAPPLICABLEFROM = "2018-01-01";

				GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("-", "");
				GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("/", "");

				
				
				cCommonXML.NewItemVerson5( missingStockItem,   TallyServer,   itemMst.getTaxRate(),
					 unitMst.getUnitName(),   itemMst.getBarCode(),
					TALLY_COMPANY,affectstock,categoryMst.getCategoryName(),itemMst.getHsnCode(),GSTDTLAPPLICABLEFROM);
			 
			 
		 
	    		
			throw new SQLException("TRYAGAIN");
		}
		
		if (Linerror.contains("Voucher Type") && Linerror.contains("does not exist!")) {
			String missingVoucherType = Linerror.replace("Voucher Type", "")
					.replace("does not exist!", "").replace("'", "").trim();

			System.out.println("Missing Voucher Type =  " + missingVoucherType);
			throw new SQLException("TRYAGAIN");
		}
	}


	 private String getParentAccount(String groupName) {
			// TODO Auto-generated method stub
			return null;
		}



 
	protected void executeInternal(String voucherNumber,Date voucherDate,String sourvceId ,	String lmsqueueId ,
			String hdrid, String TallyServer , String companyMst , String branchMst,String id )   {

		
		
		BigDecimal zero = new BigDecimal("0");
			
		
		 
		 
			java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

			 Optional<PurchaseHdr> purchaseHdrOpt = purchaseHdrRepository.findById(id);
			 PurchaseHdr purchaseHdr  =purchaseHdrOpt.get();
			  
					 String strVoucherDate = ClientSystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");
			 

			 
	    	 System.out.println("Company ID "+companyMst );

			
		 List<AccountClass> accountClassPreviousList = accountClassRepo.findBySourceVoucherNumberAndTransDate(voucherNumber,voucherDate);
	    	 
	    	 if(accountClassPreviousList.size()>0) {
	    		 logger.info("Found previous records.. Deleting");
	    		 
	    		 Iterator iter = accountClassPreviousList.iterator();
	    		 while(iter.hasNext()) {
	    			 AccountClass accountClass = (AccountClass) iter.next();
	    			 logger.info("Found previous records.. Deleting" +accountClass.getSourceVoucherNumber()+accountClass.getBrachCode());
	    			 
	    			 List<LedgerClass> ledgerClassList	 =	 ledgerClassRepo.findByAccountClassId(accountClass.getId());
	    			 List<CreditClass> creditClassList = creditClassRepo.findByAccountClass(accountClass);
	    			
	    			 List<DebitClass> debitClassList = debitClassRepo.findByAccountClass(accountClass);
	    	    		
	    			 
	    			 
	    			 if(creditClassList.size()>0) {
	    			 
	    				 try {
							SavePurchase(accountClass.getCompanyMst() ,
								 tallyServer, 
								 purchaseHdr.getBranchCode(), 
								 	 
									  voucherNumber, 
									  strVoucherDate, 
									  purchaseHdr.getSupplierInvDate(),
									  purchaseHdr.getSupplierInvNo() ,
									  purchaseHdr.getSupplierId(),
									  debitClassList,creditClassList,
									  purchaseHdr.getVoucherType());
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							logger.error(e.getMessage());
						}
	    			 }
	    			 
	    			 
	    		 }
	    	 }
		

		
	
		
	}
	   
	
}
