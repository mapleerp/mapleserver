package com.maple.restserver.tally.integration.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
 
import org.springframework.beans.factory.annotation.Autowired;
 
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
 
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.jamesmurty.utils.XMLBuilder;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.DamageHdr;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ProductConversionDtl;
import com.maple.restserver.entity.ProductConversionMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.ActualProductionDtlRepository;
import com.maple.restserver.repository.ActualProductionHdrRepository;
import com.maple.restserver.repository.CategoryMstRepository;
import com.maple.restserver.repository.DamageDtlRepository;
import com.maple.restserver.repository.DamageHdrRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.ProductConversionDtlRepository;
import com.maple.restserver.repository.ProductConversionMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.SystemSetting;

@Transactional
@Service
public class PostItemInTally   {
	private static final Logger logger = LoggerFactory.getLogger(PostItemInTally.class);

	@Autowired
	private SalesTransHdrRepository salesTransHdrRepository;

	@Autowired
	private VoucherNumberService voucherService;

	@Autowired
	ItemMstRepository itemMstRepository;

	@Autowired
	SalesDetailsRepository salesDetailsRepository;

	@Autowired
	UnitMstRepository unitMstRepository;

	@Autowired
	CategoryMstRepository categoryMstRepository;

 	
	
	 public void NewStockGroup(String missingGroup, String TallyServer, 
	    		  String barcode, String CompanyName,String affectstock, UnitMst unit) {
	    	  String iNfileName = missingGroup+".xml";
	       
	        try {
	          

	            XMLBuilder en = XMLBuilder.create("ENVELOPE");
	            XMLBuilder Hd = en.element("HEADER");
	            XMLBuilder vr = Hd.element("VERSION");
	            vr.t("1");
	            XMLBuilder TRq = Hd.element("TALLYREQUEST");
	            TRq.t("import");
	            XMLBuilder tYP = Hd.element("TYPE");
	            tYP.t("Data");
	            XMLBuilder rid = Hd.element("ID");
	            rid.t("All Masters");

	            XMLBuilder id = en.element("BODY");
	            XMLBuilder rd = id.element("DESC");
	            XMLBuilder stv = rd.element("STATICVARIABLES");
	            XMLBuilder imp = stv.element("IMPORTDUPS");
	            imp.t("@@DUPIGNORECOMBINE");
	            XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
	            cc.t(CompanyName);

	            XMLBuilder rdata = id.element("DATA");
	            XMLBuilder tm = rdata.element("TALLYMESSAGE");
	            XMLBuilder mstr = tm.element("STOCKGROUP");
	            mstr.a("Action", "Create");
	            mstr.a("NAME", missingGroup);
	            XMLBuilder namelst = mstr.element("NAME.LIST");
	            XMLBuilder name1 = namelst.element("NAME");
	            name1.t(missingGroup);
	            //XMLBuilder name2 = namelst.element("NAME");
	           // name2.t(barcode);
	            XMLBuilder base = namelst.element("BASEUNITS");
	     
	           // String str1 = StockItem;
	            
	            base.t(unit.getUnitName());
	            
	          //   XMLBuilder vatrate = namelst.element("RATEOFVAT");
	          //  vatrate.t(vat+" ");


	            Properties outputProperties = new Properties();
	            // Explicitly identify the output as an XML document
	            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
	            // Pretty-print the XML output (doesn't work in all cases)
	            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
	            // Get 2-space indenting when using the Apache transformer
	            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
	            // Omit the XML declaration header
	            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

	            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
	            en.toWriter(writer, outputProperties);
	            writer.close();

	        } catch (Exception e) {
	            System.out.println(e.toString());
	        }

	        try {
	            HttpClient httpclient = new DefaultHttpClient();

	            HttpPost httppost = new HttpPost(TallyServer);
	            File file = new File(iNfileName); 

	            FileEntity entity = new FileEntity(file,
	                    "text/plain; charset=\"UTF-8\"");

	            httppost.setEntity(entity);
	            System.out.println("executing request "
	                    + httppost.getRequestLine());
	            HttpResponse response = httpclient.execute(httppost);
	            HttpEntity resEntity = response.getEntity();
	            System.out.println("----------------------------------------");
	            System.out.println(response.getStatusLine());
	            String VoucherId="";
	            String Linerror = "";
	            String ErrorCode = "";
	            
	            if (resEntity != null) {
					try {
						DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
						DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
						Document doc = dBuilder.parse(resEntity.getContent());

						NodeList nList = doc.getElementsByTagName("RESPONSE");
						for (int temp = 0; temp < nList.getLength(); temp++) {
							Node nNode = nList.item(temp);

							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								Element eElement = (Element) nNode;
								VoucherId = eElement.getElementsByTagName("LASTVCHID").item(0).getTextContent();
								System.out.println("Response VoucherId =  " + VoucherId);

								ErrorCode = VoucherId = eElement.getElementsByTagName("ERRORS").item(0)
										.getTextContent();

								System.out.println("ErrorCode =  " + ErrorCode);

								Linerror = eElement.getElementsByTagName("LINEERROR").item(0).getTextContent();
								System.out.println("Linerror =  " + Linerror);

							

								if (Linerror.contains("Unit") && Linerror.contains("does not exist!")) {
									String missingStockItem = Linerror.replace("Unit", "")
											.replace("does not exist!", "").replace("'", "").trim();

									System.out.println("missingLedger =  " + missingStockItem);
									
								 			
								 	String XML_PATH = unit.getUnitName() +".xml" ; 
									
									CreateUnitXML( unit.getUnitName(), "2",  
											unit.getUnitName(),
								              TallyServer,    
								              CompanyName,
								               XML_PATH) ;
									
								 
						            
									
																	
									throw new SQLException("TRYAGAIN");
								}
								
								if (Linerror.contains("Voucher Type") && Linerror.contains("does not exist!")) {
									String missingVoucherType = Linerror.replace("Voucher Type", "")
											.replace("does not exist!", "").replace("'", "").trim();

									System.out.println("Missing Voucher Type =  " + missingVoucherType);
									throw new SQLException("TRYAGAIN");
								}

							}

						}

						System.out.println("Response content length: " + resEntity.getContentLength());
						System.out.println(resEntity.toString());
						System.out.println("Chunked?: " + resEntity.isChunked());

					} catch (Exception e2) {
						throw new SQLException(e2.toString());

					}
				}

	            /*
	             * delete the file
	            
	*/
	            

	            if (file.delete()) {
	                System.out.println(file.getName() + " is deleted!");
	            } else {
	                System.out.println("Delete operation is failed.");
	            }
	             	
	            

	        } catch (Exception est) {
	            System.out.println(est);
	            
	        }
	        
	    }

	private void setAmbersandForGST(String fileName, String search, String replacement, String outFile)
			throws FileNotFoundException {

		try {
			PrintWriter writer = new PrintWriter(outFile, "UTF-8");

			File log = new File(fileName);

			String search2 = "<STATENAME>Any</STATENAME>";
			String replace2 = "<STATENAME>&#4; Any</STATENAME>";
			FileReader fr = new FileReader(log);
			String s;

			BufferedReader br = new BufferedReader(fr);

			while ((s = br.readLine()) != null) {
				s = s.replaceAll(search, replacement);
				s = s.replaceAll(search2, replace2);
				writer.print(s);
			}
			writer.close();
		} catch (Exception e) {

		}
	}
	
	public String CreateUnitXML(String UnitName, String DecimalPalce, String FormalName,
            String TallyServer,   String CompanyName,
             String XML_PATH) { 
 	  
        String iNfileName = XML_PATH + "Unit" + UnitName + ".xml";
        try {
     	   
     	   
     	    

        	  XMLBuilder en = XMLBuilder.create("ENVELOPE");
              XMLBuilder Hd = en.element("HEADER");
              //XMLBuilder vr = Hd.element("VERSION");

  
              //vr.t("1");
              XMLBuilder TRq = Hd.element("TALLYREQUEST");
              TRq.t("import Data");
             
           
              XMLBuilder bd = en.element("BODY");
              XMLBuilder de = bd.element("IMPORTDATA");
              XMLBuilder req = de.element("REQUESTDESC");
              XMLBuilder rep = req.element("REPORTNAME");
              
              rep.t("All Masters");  //All Masters Units
              
              XMLBuilder stv = req.element("STATICVARIABLES");
              XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
              cc.t(CompanyName);
              XMLBuilder idu = stv.element("IMPORTDUPS");
              idu.t("@@DUPCOMBINE");

              XMLBuilder dt = bd.element("DATA");
              XMLBuilder tm = dt.element("TALLYMESSAGE");
              
    
              XMLBuilder lg = tm.element("UNIT");
            
              lg.a("NAME", UnitName);
              lg.a("RESERVEDNAME", "");
              XMLBuilder unm = lg.element("NAME");
              
              unm.t(UnitName);
              XMLBuilder nm = lg.element("ORIGINALNAME");
              nm.t(FormalName);
              XMLBuilder pt = lg.element("DECIMALPLACES");
              pt.t(DecimalPalce);
              XMLBuilder issim = lg.element("ISSIMPLEUNIT");
              issim.t("Yes");
             
          
          
            
            
            
            Properties outputProperties = new Properties();
            // Explicitly identify the output as an XML document
            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
            // Pretty-print the XML output (doesn't work in all cases)
            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
            // Get 2-space indenting when using the Apache transformer
            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
            // Omit the XML declaration header
            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
            en.toWriter(writer, outputProperties);
            writer.close();

        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {

     	   final HttpParams httpParams = new BasicHttpParams();
 		    HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
 			HttpClient httpclient = new DefaultHttpClient(httpParams);
   
   
            HttpPost httppost = new HttpPost(TallyServer);
            File file = new File(iNfileName);
            FileEntity entity = new FileEntity(file,
                    "text/plain; charset=\"UTF-8\"");

            httppost.setEntity(entity);
            System.out.println("executing request "
                    + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            if (resEntity != null) {
                System.out.println("Response content length: "
                        + resEntity.getContentLength());
                System.out.println(resEntity.toString());
                System.out.println("Chunked?: "
                        + resEntity.isChunked());
            }
            /*
             * delete the file
            
  */

           if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }


        } catch (Exception est) {
            System.out.println(est);
            return "failed";
        }
        return "success";
    }


 
	protected void executeInternal(String voucherNumber,Date voucherDate,String sourvceId ,	String lmsqueueId ,
			String hdrid, String TallyServer , String companyMst , String branchMst )   {

		
	 	  

		SalesTransHdr salesTransHdr = salesTransHdrRepository.findById(hdrid).get();

		List<SalesDtl> salesDtlList = salesDetailsRepository.findBySalesTransHdrId(salesTransHdr.getId());

		String GSTDTLAPPLICABLEFROM = "2018-01-01";

		GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("-", "");
		GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("/", "");

		Iterator iter = salesDtlList.iterator();
		while (iter.hasNext()) {
			SalesDtl salesDtl = (SalesDtl) iter.next();

			ItemMst itemMst = itemMstRepository.findById(salesDtl.getItemId()).get();

			UnitMst unitMst = unitMstRepository.findById(itemMst.getUnitId()).get();

			String iNfileName = "tallyItem" + itemMst.getId() + ".xml";

			CategoryMst categoryMst = categoryMstRepository.findById(itemMst.getCategoryId()).get();

			String outFile = "";

			double cess = 0;
			double vat = itemMst.getTaxRate();

			BigDecimal bdIgst = new BigDecimal(vat);
			bdIgst = bdIgst.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal bdCess = new BigDecimal(cess);
			bdCess = bdCess.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			double sgst = vat / 2;
			double cgst = vat / 2;

			BigDecimal bdSgst = new BigDecimal(sgst);
			BigDecimal bdCgst = new BigDecimal(cgst);

			bdSgst = bdSgst.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			bdCgst = bdCgst.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			try {

				XMLBuilder en = XMLBuilder.create("ENVELOPE");
				XMLBuilder Hd = en.element("HEADER");
				XMLBuilder vr = Hd.element("VERSION");
				vr.t("1");
				XMLBuilder TRq = Hd.element("TALLYREQUEST");
				TRq.t("import");
				XMLBuilder tYP = Hd.element("TYPE");
				tYP.t("Data");
				XMLBuilder rid = Hd.element("ID");
				rid.t("All Masters");

				XMLBuilder id = en.element("BODY");
				XMLBuilder rd = id.element("DESC");
				XMLBuilder stv = rd.element("STATICVARIABLES");
				XMLBuilder imp = stv.element("IMPORTDUPS");
				imp.t("@@DUPIGNORECOMBINE");
				XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
				cc.t(companyMst);

				XMLBuilder rdata = id.element("DATA");
				XMLBuilder tm = rdata.element("TALLYMESSAGE");
				XMLBuilder mstr = tm.element("STOCKITEM");
				mstr.a("Action", "Create");
				mstr.a("NAME", itemMst.getItemName());

				XMLBuilder pr = mstr.element("PARENT");
				pr.t(categoryMst.getCategoryName());
				// XMLBuilder namelst = mstr.element("NAME.LIST");

				// XMLBuilder name1 = namelst.element("NAME");
				// name1.t(StockItem);
				// XMLBuilder name2 = namelst.element("NAME");
				// name2.t(barcode);
				XMLBuilder base = mstr.element("BASEUNITS");

				String str1 = itemMst.getItemName();

				base.t(unitMst.getUnitName());

				// XMLBuilder vatrate = namelst.element("RATEOFVAT");
				// vatrate.t(vat+" ");

				XMLBuilder gstapl = mstr.element("GSTAPPLICABLE");

				gstapl.t("Applicable");
				XMLBuilder gstsply = mstr.element("GSTTYPEOFSUPPLY");

				gstsply.t("Goods");

				// XMLBuilder vatapl = mstr.element("VATAPPLICABLE");
				// vatapl.t("Not Applicable");

				XMLBuilder gstdtl = mstr.element("GSTDETAILS.LIST");
				XMLBuilder aplfrom = gstdtl.element("APPLICABLEFROM");
				aplfrom.t(GSTDTLAPPLICABLEFROM);
				XMLBuilder gslcalc = gstdtl.element("CALCULATIONTYPE");
				gslcalc.t("On Value");

				XMLBuilder gsttax = gstdtl.element("TAXABILITY");
				gsttax.t("Taxable");

				XMLBuilder nongst = gstdtl.element("ISNONGSTGOODS");
				nongst.t("No");

				XMLBuilder statedtl = gstdtl.element("STATEWISEDETAILS.LIST");
				XMLBuilder stname = statedtl.element("STATENAME");
				stname.t("Any");

				XMLBuilder RATEDETAILSLIST = statedtl.element("RATEDETAILS.LIST");
				XMLBuilder GSTRATEDUTYHEAD = RATEDETAILSLIST.element("GSTRATEDUTYHEAD");
				GSTRATEDUTYHEAD.t("Central Tax");
				XMLBuilder GSTRATEVALUATIONTYPE = RATEDETAILSLIST.element("GSTRATEVALUATIONTYPE");
				GSTRATEVALUATIONTYPE.t("Based on Value");
				XMLBuilder GSTRATE = RATEDETAILSLIST.element("GSTRATE");
				GSTRATE.t(bdCgst.toPlainString());

				XMLBuilder RATEDETAILSLIST2 = statedtl.element("RATEDETAILS.LIST");
				XMLBuilder GSTRATEDUTYHEAD2 = RATEDETAILSLIST2.element("GSTRATEDUTYHEAD");
				GSTRATEDUTYHEAD2.t("State Tax");
				XMLBuilder GSTRATEVALUATIONTYPE2 = RATEDETAILSLIST2.element("GSTRATEVALUATIONTYPE");
				GSTRATEVALUATIONTYPE2.t("Based on Value");
				XMLBuilder GSTRATE2 = RATEDETAILSLIST2.element("GSTRATE");
				GSTRATE2.t(bdSgst.toPlainString());

				XMLBuilder RATEDETAILSLIST3 = statedtl.element("RATEDETAILS.LIST");
				XMLBuilder GSTRATEDUTYHEAD3 = RATEDETAILSLIST3.element("GSTRATEDUTYHEAD");
				GSTRATEDUTYHEAD3.t("Integrated Tax");
				XMLBuilder GSTRATEVALUATIONTYPE3 = RATEDETAILSLIST3.element("GSTRATEVALUATIONTYPE");
				GSTRATEVALUATIONTYPE3.t("Based on Value");
				XMLBuilder GSTRATE3 = RATEDETAILSLIST3.element("GSTRATE");
				GSTRATE3.t(bdIgst.toPlainString());

				XMLBuilder RATEDETAILSLIST4 = statedtl.element("RATEDETAILS.LIST");
				XMLBuilder GSTRATEDUTYHEAD4 = RATEDETAILSLIST4.element("GSTRATEDUTYHEAD");
				GSTRATEDUTYHEAD4.t("Cess");
				XMLBuilder GSTRATEVALUATIONTYPE4 = RATEDETAILSLIST4.element("GSTRATEVALUATIONTYPE");
				GSTRATEVALUATIONTYPE4.t("Based on Value");
				XMLBuilder GSTRATE4 = RATEDETAILSLIST4.element("GSTRATE");
				GSTRATE4.t(bdCess.toPlainString());

				XMLBuilder HSN = gstdtl.element("HSNCODE");

				HSN.t(itemMst.getHsnCode());

				XMLBuilder LANGUAGENAMELIST = mstr.element("LANGUAGENAME.LIST");
				XMLBuilder NAMELIST = LANGUAGENAMELIST.element("NAME.LIST");
				NAMELIST.a("TYPE", "String");
				XMLBuilder NAME = NAMELIST.element("NAME");
				NAME.t(itemMst.getItemName());

				Properties outputProperties = new Properties();
				// Explicitly identify the output as an XML document
				outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
				// Pretty-print the XML output (doesn't work in all cases)
				outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
				// Get 2-space indenting when using the Apache transformer
				outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
				// Omit the XML declaration header
				outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

				PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
				en.toWriter(writer, outputProperties);
				writer.close();

				String search = "<GSTAPPLICABLE>Applicable</GSTAPPLICABLE>"; // <- changed to work with
																				// String.replaceAll()
				String replacement = "<GSTAPPLICABLE>&#4;Applicable</GSTAPPLICABLE>";

				outFile = "xout.xml";
				setAmbersandForGST(iNfileName, search, replacement, outFile);

				// <GSTAPPLICABLE>Applicable</GSTAPPLICABLE>
			} catch (Exception e) {
				System.out.println(e.toString());
			}

			try {
				HttpClient httpclient = new DefaultHttpClient();

				HttpPost httppost = new HttpPost(TallyServer);
				File file = new File(outFile);

				FileEntity entity = new FileEntity(file, "text/html; charset=\"UTF-8\"");

				httppost.setEntity(entity);
				System.out.println("executing request " + httppost.getRequestLine());
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity resEntity = response.getEntity();
				System.out.println("----------------------------------------");
				System.out.println(response.getStatusLine());
				String VoucherId = "";
				String Linerror = "";
				String ErrorCode = "";

				if (resEntity != null) {
					try {
						DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
						DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
						Document doc = dBuilder.parse(resEntity.getContent());

						NodeList nList = doc.getElementsByTagName("RESPONSE");
						for (int temp = 0; temp < nList.getLength(); temp++) {
							Node nNode = nList.item(temp);

							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								Element eElement = (Element) nNode;
								VoucherId = eElement.getElementsByTagName("LASTVCHID").item(0).getTextContent();
								System.out.println("Response VoucherId =  " + VoucherId);

								ErrorCode = VoucherId = eElement.getElementsByTagName("ERRORS").item(0)
										.getTextContent();

								System.out.println("ErrorCode =  " + ErrorCode);

								Linerror = eElement.getElementsByTagName("LINEERROR").item(0).getTextContent();
								System.out.println("Linerror =  " + Linerror);

								if (Linerror.contains("Group") && Linerror.contains("does not exist!")) {
									String missingLedger = Linerror.replace("Group", "").replace("does not exist!", "")
											.replace("'", "").trim();

									System.out.println("missingLedger =  " + missingLedger);

								 

									String XML_PATH = "";

									NewStockGroup(missingLedger, TallyServer,  itemMst.getBarCode(), 
											companyMst,
											"Y",unitMst);
									
								 

									throw new SQLException("TRYAGAIN");
								}

								if (Linerror.contains("Unit") && Linerror.contains("does not exist!")) {
									String missingStockItem = Linerror.replace("Unit", "")
											.replace("does not exist!", "").replace("'", "").trim();

									System.out.println("missingLedger =  " + missingStockItem);

								 
									String XML_PATH = unitMst.getId() + ".xml";

									CreateUnitXML(unitMst.getUnitName(), "2", unitMst.getUnitName(), TallyServer,  companyMst,
											  XML_PATH);

									throw new SQLException("TRYAGAIN");
								}

								if (Linerror.contains("Voucher Type") && Linerror.contains("does not exist!")) {
									String missingVoucherType = Linerror.replace("Voucher Type", "")
											.replace("does not exist!", "").replace("'", "").trim();

									System.out.println("Missing Voucher Type =  " + missingVoucherType);
									throw new SQLException("TRYAGAIN");
								}

							}

						}

						System.out.println("Response content length: " + resEntity.getContentLength());
						System.out.println(resEntity.toString());
						System.out.println("Chunked?: " + resEntity.isChunked());

					} catch (Exception e2) {
						throw new SQLException(e2.toString());

					}
				}

				/*
				 * delete the file
				 * 
				 */

				if (file.delete()) {
					System.out.println(file.getName() + " is deleted!");
				} else {
					System.out.println("Delete operation is failed.");
				}

			} catch (Exception est) {
				System.out.println(est);

			}
		}

	
		
	}
	
}
