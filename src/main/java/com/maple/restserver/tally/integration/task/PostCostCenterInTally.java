package com.maple.restserver.tally.integration.task;

 
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
 
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
 
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.jamesmurty.utils.XMLBuilder;
import com.maple.restserver.entity.ActualProductionDtl;
import com.maple.restserver.entity.ActualProductionHdr;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DamageDtl;
import com.maple.restserver.entity.DamageHdr;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ProductConversionDtl;
import com.maple.restserver.entity.ProductConversionMst;
import com.maple.restserver.entity.PurchaseDtl;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.SalesDtl;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.ActualProductionDtlRepository;
import com.maple.restserver.repository.ActualProductionHdrRepository;
import com.maple.restserver.repository.DamageDtlRepository;
import com.maple.restserver.repository.DamageHdrRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.ItemBatchMstRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.ProductConversionDtlRepository;
import com.maple.restserver.repository.ProductConversionMstRepository;
import com.maple.restserver.repository.PurchaseDtlRepository;
import com.maple.restserver.repository.PurchaseHdrRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;





@Service
public class PostCostCenterInTally {
	 private static final Logger logger = LoggerFactory.getLogger(PostCostCenterInTally.class);

	 
	 @Autowired
		private SalesTransHdrRepository   salesTransHdrRepository;
	

	 
	  @Autowired
	  private VoucherNumberService voucherService;
 	  
	  @Autowired
	  ItemMstRepository itemMstRepository ;
	
	  @Autowired
	  SalesDetailsRepository  salesDetailsRepository ;
	  
	  
	  @Autowired
	  UnitMstRepository  unitMstRepository ;
	
	  
	  
	  
 



	 
	protected void executeInternal(String voucherNumber ,Date voucherDate,String sourvceId ,String lmsqueueId ,
			String hdrid , String TallyServer, 	String companyMst ,  String branchMst  )  {

		
		 
		 
		 
		String goDownName = branchMst ; 
		
		 
		 
		 
		   String iNfileName = goDownName+"CC.xml";
	        try {
	     	   
	     	   System.out.println("Company "+companyMst );
	     	   
	     	   System.out.println("cost center  "+goDownName );
	     	   

	        	  XMLBuilder en = XMLBuilder.create("ENVELOPE");
	              XMLBuilder Hd = en.element("HEADER");
	              //XMLBuilder vr = Hd.element("VERSION");

	  
	              //vr.t("1");
	              XMLBuilder TRq = Hd.element("TALLYREQUEST");
	              TRq.t("import Data");
	             
	           
	              XMLBuilder bd = en.element("BODY");
	              XMLBuilder de = bd.element("IMPORTDATA");
	              XMLBuilder req = de.element("REQUESTDESC");
	              XMLBuilder rep = req.element("REPORTNAME");
	              
	              rep.t("All Masters");  //All Masters Units
	              
	              XMLBuilder stv = req.element("STATICVARIABLES");
	              XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
	              cc.t(companyMst);
	              XMLBuilder idu = stv.element("IMPORTDUPS");
	              idu.t("@@DUPCOMBINE");

	              XMLBuilder dt = bd.element("DATA");
	              XMLBuilder tm = dt.element("TALLYMESSAGE");
	              //****Start from Here ********//
	              
	    
	              XMLBuilder lg = tm.element("COSTCENTRE");
	            
	              lg.a("NAME", goDownName);
	              lg.a("RESERVEDNAME", "");
	              
	              XMLBuilder cg = tm.element("CATEGORY");
	              cg.t("Primary Cost Category");
	               
	             
	              XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
	              XMLBuilder langnmlst = langlst.element("NAME.LIST");
	               langlst.a("TYPE","String");
	               XMLBuilder lname = langnmlst.element("NAME");
	               lname.t(goDownName);
	               //XMLBuilder alname = langnmlst.element("NAME");
	               //alname.t(AccountName);
	          
	            
	            
	            
	            Properties outputProperties = new Properties();
	            // Explicitly identify the output as an XML document
	            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
	            // Pretty-print the XML output (doesn't work in all cases)
	            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
	            // Get 2-space indenting when using the Apache transformer
	            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
	            // Omit the XML declaration header
	            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

	            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
	            en.toWriter(writer, outputProperties);
	            writer.close();

	        } catch (Exception e) {
	            System.out.println(e.toString());
	        }

	        try {
	     	   
	     	   final HttpParams httpParams = new BasicHttpParams();
	 		    HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
	 			HttpClient httpclient = new DefaultHttpClient(httpParams);
	    
	     	   
	    
	            HttpPost httppost = new HttpPost(TallyServer);
	            File file = new File(iNfileName);
	            FileEntity entity = new FileEntity(file,
	                    "text/plain; charset=\"UTF-8\"");

	            httppost.setEntity(entity);
	            System.out.println("executing request "
	                    + httppost.getRequestLine());
	            HttpResponse response = httpclient.execute(httppost);
	            HttpEntity resEntity = response.getEntity();
	            System.out.println("----------------------------------------");
	            System.out.println(response.getStatusLine());
	            if (resEntity != null) {
	                System.out.println("Response content length: "
	                        + resEntity.getContentLength());
	                System.out.println(resEntity.toString());
	                System.out.println("Chunked?: "
	                        + resEntity.isChunked());
	            }
	            /*
	             * delete the file
	            
	*/

	            if (file.delete()) {
	                System.out.println(file.getName() + " is deleted!");
	            } else {
	                System.out.println("Delete operation is failed.");
	            }
	  

	        } catch (Exception est) {
	            System.out.println(est);
	           
	        }
	        
	    
		
	}
 

}
