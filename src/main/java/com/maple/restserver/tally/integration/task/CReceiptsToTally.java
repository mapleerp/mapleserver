package com.maple.restserver.tally.integration.task;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.jamesmurty.utils.XMLBuilder;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.accounting.repository.AccountClassRepository;
import com.maple.restserver.accounting.repository.CreditClassRepository;
import com.maple.restserver.accounting.repository.DebitClassRepository;
import com.maple.restserver.accounting.repository.LedgerClassRepository;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.ReceiptHdrRepository;
import com.maple.restserver.service.accounting.task.SalesAccounting;

@Component

public class CReceiptsToTally   {

	static int connectionTimeOut = 10000;

	private static final Logger logger = LoggerFactory.getLogger(SalesAccounting.class);

	 

	@Autowired
	private ReceiptHdrRepository receiptHdrrepo;

	@Autowired
	private AccountClassRepository accountClassRepo;

	@Autowired
	private CreditClassRepository creditClassRepo;

	@Autowired
	private DebitClassRepository debitClassRepo;

	@Autowired
	private LedgerClassRepository ledgerClassRepo;

	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	@Autowired
	CCommonXML cCommonXML;
	
	
	
	@Value("${TALLY_COMPANY}")
	private String TALLY_COMPANY;
	

	CCommonXML commonXML = new CCommonXML();

	public boolean SaveReceipts(CompanyMst companyMst, String CostCenter, String tallyVoucherType, String Godown,
			String voucherNumber, String invoiceDate, List<DebitClass> debitList, List<CreditClass> creditList, String tallyServer )
			throws TransformerException, ClientProtocolException, IOException, ParserConfigurationException,
			FactoryConfigurationError {

		
		String VoucherId = "";
		String ErrorCode = "";
		String Linerror = "";

		invoiceDate = invoiceDate.replace("-", "");

		// String VoucherType = APlicationWindow.MachineName+"_RECEIPTS" ;

		String ItemLoopXML = "";
		String ItemXML = "";
		XMLBuilder TAXOBJL = null;
		XMLBuilder LEDGERENTRYL2 = null;
		String AccountName = "";
		String remark = "";
		BigDecimal bdCrAmount = new BigDecimal("0");
		BigDecimal bdRCrAmount = new BigDecimal("0");

		for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			DebitClass debitclass = (DebitClass) itr.next();

			String drAccountID = debitclass.getAccountId();

			AccountHeads accountHeadsDR = accountHeadsRepository.findByIdAndCompanyMstId(drAccountID,
					companyMst.getId());

			remark = debitclass.getRemark();
			bdCrAmount = debitclass.getDrAmount();
			bdRCrAmount = bdCrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		}

		XMLBuilder en = XMLBuilder.create("ENVELOPE");
		XMLBuilder hd = en.element("HEADER");
		XMLBuilder tr = hd.element("TALLYREQUEST");
		tr.t("ImportData");

		XMLBuilder bd = en.element("BODY");
		XMLBuilder id = bd.element("IMPORTDATA");
		XMLBuilder rd = id.element("REQUESTDESC");
		XMLBuilder rn = rd.element("REPORTNAME");
		rn.t("Vouchers");

		XMLBuilder sv = rd.element("STATICVARIABLES");
		XMLBuilder cc = sv.element("SVCURRENTCOMPANY");
		cc.t(TALLY_COMPANY);

		XMLBuilder rdt = id.element("REQUESTDATA");
		XMLBuilder tm = rdt.element("TALLYMESSAGE");
		tm.a("xmlns:UDF", "TallyUDF");

		XMLBuilder vchr = tm.element("VOUCHER");

		vchr.a("VCHTYPE", tallyVoucherType);
		vchr.a("ACTION", "Create");
		vchr.a("OBJVIEW", "Accounting Voucher View");

		XMLBuilder dt = vchr.element("DATE");
		dt.t(invoiceDate);

		XMLBuilder gd = vchr.element("GUID");
		gd.t("");
		XMLBuilder vtn = vchr.element("VOUCHERTYPENAME");
		vtn.t(tallyVoucherType);
		XMLBuilder vn = vchr.element("VOUCHERNUMBER");
		vn.t(voucherNumber);
		XMLBuilder pldn = vchr.element("PARTYLEDGERNAME");
		pldn.t(AccountName);
		XMLBuilder fpt = vchr.element("FBTPAYMENTTYPE");
		fpt.t("Default");

		XMLBuilder pvw = vchr.element("PERSISTEDVIEW");
		pvw.t("Accounting Voucher View");
		XMLBuilder eby = vchr.element("ENTEREDBY");
		eby.t("System");
		XMLBuilder difq = vchr.element("DIFFACTUALQTY");
		difq.t("NO");
		XMLBuilder audi = vchr.element("AUDITED");
		audi.t("NO");
		XMLBuilder fjbc = vchr.element("FORJOBCOSTING");
		fjbc.t("NO");
		XMLBuilder isoptn = vchr.element("ISOPTIONAL");
		isoptn.t("NO");

		XMLBuilder efdt = vchr.element("EFFECTIVEDATE");
		efdt.t(invoiceDate);
		XMLBuilder frjbw = vchr.element("ISFORJOBWORKIN");
		frjbw.t("NO");
		XMLBuilder alwcm = vchr.element("ALLOWCONSUMPTION");
		alwcm.t("NO");
		XMLBuilder usfintr = vchr.element("USEFORINTEREST");
		usfintr.t("NO");
		XMLBuilder usfrgl = vchr.element("USEFORGAINLOSS");
		usfrgl.t("NO");

		XMLBuilder gdwntrans = vchr.element("USEFORGODOWNTRANSFER");
		gdwntrans.t("NO");
		XMLBuilder usfrcom = vchr.element("USEFORCOMPOUND");
		usfrcom.t("NO");
		XMLBuilder altrid = vchr.element("ALTERID");
		altrid.t("");
		XMLBuilder exopn = vchr.element("EXCISEOPENING");
		exopn.t("NO");

		XMLBuilder usfin = vchr.element("USEFORFINALPRODUCTION");
		usfin.t("NO");
		XMLBuilder can = vchr.element("ISCANCELLED");
		can.t("NO");
		XMLBuilder hahflw = vchr.element("HASCASHFLOW");
		hahflw.t("Yes");

		XMLBuilder pstdt = vchr.element("ISPOSTDATED");
		pstdt.t("NO");
		XMLBuilder trkno = vchr.element("USETRACKINGNUMBER");
		trkno.t("NO");
		XMLBuilder invno = vchr.element("ISINVOICE");
		invno.t("NO");
		XMLBuilder mfgj = vchr.element("MFGJOURNAL");
		mfgj.t("NO");
		XMLBuilder hdis = vchr.element("HASDISCOUNTS");
		hdis.t("NO");
		XMLBuilder pays = vchr.element("ASPAYSLIP");
		pays.t("NO");
		XMLBuilder cst = vchr.element("ISCOSTCENTRE");
		cst.t("NO");

		XMLBuilder txrl = vchr.element("ISSTXNONREALIZEDVCH");
		txrl.t("NO");

		XMLBuilder isdel = vchr.element("ISDELETED");
		isdel.t("NO");
		XMLBuilder orgn = vchr.element("ASORIGINAL");
		orgn.t("NO");
		XMLBuilder frmsyn = vchr.element("VCHISFROMSYNC");
		frmsyn.t("NO");
		XMLBuilder mastid = vchr.element("MASTERID");
		mastid.t("");
		XMLBuilder vchky = vchr.element("VOUCHERKEY");
		vchky.t("");

		XMLBuilder Nar = vchr.element("NARRATION");
		Nar.t(remark);

		for (Iterator itr = debitList.iterator(); itr.hasNext();) {
			DebitClass debitclass = (DebitClass) itr.next();

			String drAccountID = debitclass.getAccountId();

			AccountHeads accountHeadsDR = accountHeadsRepository.findByIdAndCompanyMstId(drAccountID,
					companyMst.getId());

			BigDecimal bdDrAmount = debitclass.getDrAmount();
			BigDecimal bdRDrAmount = bdDrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			XMLBuilder alleden = vchr.element("ALLLEDGERENTRIES.LIST");

			XMLBuilder ldrnm = alleden.element("LEDGERNAME");
			// ldrnm.t(aHdr.getSuppliername());
			ldrnm.t(accountHeadsDR.getAccountName());

			XMLBuilder idempst = alleden.element("ISDEEMEDPOSITIVE");
			idempst.t("Yes");
			XMLBuilder ledfrm = alleden.element("LEDGERFROMITEM");
			ledfrm.t("NO");
			XMLBuilder remzren = alleden.element("REMOVEZEROENTRIES");
			remzren.t("NO");
			XMLBuilder partyld = alleden.element("ISPARTYLEDGER");
			partyld.t("YES");
			XMLBuilder ldp = alleden.element("ISLASTDEEMEDPOSITIVE");
			ldp.t("Yes");
			XMLBuilder anmt = alleden.element("AMOUNT");
			anmt.t("-" + bdRDrAmount.toPlainString());

		}

		for (Iterator itr = creditList.iterator(); itr.hasNext();) {
			CreditClass creditclass = (CreditClass) itr.next();

			String crAccountID = creditclass.getAccountId();

			AccountHeads accountHeadsCR = accountHeadsRepository.findByIdAndCompanyMstId(crAccountID,
					companyMst.getId());

			BigDecimal bdDrAmount = creditclass.getCrAmount();
			BigDecimal bdRDrAmount = bdDrAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			XMLBuilder alleden1 = vchr.element("ALLLEDGERENTRIES.LIST");

			XMLBuilder ldrnm1 = alleden1.element("LEDGERNAME");

			ldrnm1.t(accountHeadsCR.getAccountName());

			XMLBuilder idempst1 = alleden1.element("ISDEEMEDPOSITIVE");
			idempst1.t("No");
			XMLBuilder ledfrm1 = alleden1.element("LEDGERFROMITEM");
			ledfrm1.t("NO");
			XMLBuilder remzren1 = alleden1.element("REMOVEZEROENTRIES");
			remzren1.t("NO");
			XMLBuilder partyld1 = alleden1.element("ISPARTYLEDGER");
			partyld1.t("Yes");
			XMLBuilder ldp1 = alleden1.element("ISLASTDEEMEDPOSITIVE");
			ldp1.t("No");
			XMLBuilder anmt1 = alleden1.element("AMOUNT");
			anmt1.t(bdRDrAmount.toPlainString());

		}

		XMLBuilder tmsg = rdt.element("TALLYMESSAGE");
		tmsg.a("xmlns:UDF", "TallyUDF");
		XMLBuilder cmpy = tmsg.element("COMPANY");

		XMLBuilder remtinf = cmpy.element("REMOTECMPINFO.LIST");
		remtinf.a("MERGE", "Yes");
		XMLBuilder nme = remtinf.element("NAME");
		nme.t("");
		XMLBuilder remn = remtinf.element("REMOTECMPNAME");
		remn.t(TALLY_COMPANY);
		XMLBuilder remst = remtinf.element("REMOTECMPSTATE");
		remst.t("Kerala");

		Properties outputProperties = new Properties();

		outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");

		outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
		outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");

		outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

		PrintWriter writer = new PrintWriter(new FileOutputStream("xout.xml"));
		en.toWriter(writer, outputProperties);
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(tallyServer);

		File file = new File("xout.xml");
		FileEntity entity = new FileEntity(file, "text/plain; charset=\"UTF-8\"");

		httppost.setEntity(entity);
		System.out.println("executing request " + httppost.getRequestLine());
		HttpResponse response = httpclient.execute(httppost);
		HttpEntity resEntity = response.getEntity();
		writer.close();
		if (resEntity != null) {
			try {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(resEntity.getContent());

				NodeList nList = doc.getElementsByTagName("RESPONSE");
				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);
					System.out.println("\nCurrent Element :" + nNode.getNodeName());

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						VoucherId = eElement.getElementsByTagName("LASTVCHID").item(0).getTextContent();
						System.out.println("ErrorCode =  " + ErrorCode);

						ErrorCode = eElement.getElementsByTagName("ERRORS").item(0).getTextContent();

						if (ErrorCode.equalsIgnoreCase("1")) {
							Linerror = eElement.getElementsByTagName("LINEERROR").item(0).getTextContent();
							System.out.println("Linerror =  " + Linerror);

							if (Linerror.contains("Voucher Number") && Linerror.contains("already exists!")) {
								return true;
							} else {

								// processReturnStatus(Linerror,TALLY_COMPANY,tallyServer);

								cCommonXML.processReturnStatus(Linerror, companyMst, tallyServer,TALLY_COMPANY, "Receipts");

							}

							return false;
						}

					}
				}
			} catch (Exception e2) {
				System.out.print(e2.toString());
				System.out.println(e2.toString() + "Generic Tally Posting Line 310 " + VoucherId);
				return false;

			}
		}
		httpclient.getConnectionManager().shutdown();

		return true;
	}

	/*@Override
	public void execute(DelegateExecution execution) throws Exception {
		String voucherNumber = (String) execution.getVariablesTyped().get("voucherNumber");
		Date voucherDate = (Date) execution.getVariablesTyped().get("voucherDate");
		String id = (String) execution.getVariablesTyped().get("id");

		java.sql.Date sqlDate = new java.sql.Date(voucherDate.getTime());

		String strVoucherDate = SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

		CompanyMst companyMst = (CompanyMst) execution.getVariable("companyid");
		System.out.println("Company ID " + companyMst.getId());

		List<ReceiptHdr> receiptHdrList = receiptHdrrepo.findByVoucherNumberAndVoucherDate(voucherNumber, voucherDate);

		ReceiptHdr receiptHdr = receiptHdrList.get(0);

		List<AccountClass> accountClassPreviousList = accountClassRepo
				.findBySourceVoucherNumberAndTransDate(voucherNumber, voucherDate);

		if (accountClassPreviousList.size() > 0) {
			logger.info("Found previous records.. Deleting");

			Iterator iter = accountClassPreviousList.iterator();
			while (iter.hasNext()) {
				AccountClass accountClass = (AccountClass) iter.next();
				logger.info("Found previous records.. Deleting" + accountClass.getSourceVoucherNumber()
						+ accountClass.getBrachCode());

				List<LedgerClass> ledgerClassList = ledgerClassRepo.findByAccountClassId(accountClass.getId());
				List<CreditClass> creditClassList = creditClassRepo.findByAccountClassId(accountClass.getId());

				List<DebitClass> debitClassList = debitClassRepo.findByAccountClassId(accountClass.getId());

				if (creditClassList.size() > 0) {

					SaveReceipts(companyMst, receiptHdr.getBranchCode(), "RECEIPTS", receiptHdr.getBranchCode(),
							voucherNumber, strVoucherDate, debitClassList, creditClassList);

				}

			}

		}

	}
	*/
	

}
