package com.maple.restserver.tally.integration.task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.jamesmurty.utils.XMLBuilder;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CategoryMstRepository;

import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.UnitMstRepository;
import com.maple.restserver.utils.SystemSetting;

public class LedgerCreation {

	@Autowired
	ItemMstRepository itemMstRepository;



	@Autowired
	UnitMstRepository unitMstRepository;

	@Autowired
	CategoryMstRepository categoryMstRepository;
	
	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	
	
	@Autowired
	CCommonXML cCommonXML;
	
	
	@Value("${TALLY_COMPANY}")
	private String TALLY_COMPANY;
	

	public String LedgerXML(String AcName, String TinNo, String Address, String Alias, String ParentAccountHead,
			String TallyServer,   CompanyMst companyMst,  String affectstock) {

		String iNfileName = "ledger.xml";
		try {

			XMLBuilder en = XMLBuilder.create("ENVELOPE");
			XMLBuilder Hd = en.element("HEADER");
			XMLBuilder vr = Hd.element("VERSION");
			vr.t("1");
			XMLBuilder TRq = Hd.element("TALLYREQUEST");
			TRq.t("import");
			XMLBuilder tp = Hd.element("TYPE");
			tp.t("Data");
			XMLBuilder id = Hd.element("ID");
			id.t("All Masters");
			XMLBuilder bd = en.element("BODY");
			XMLBuilder de = bd.element("DESC");
			XMLBuilder stv = de.element("STATICVARIABLES");
			XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
			cc.t(TALLY_COMPANY);
			XMLBuilder idu = stv.element("IMPORTDUPS");
			idu.t("@@DUPCOMBINE");

			XMLBuilder dt = bd.element("DATA");
			XMLBuilder tm = dt.element("TALLYMESSAGE");
			XMLBuilder lg = tm.element("LEDGER");
			lg.a("Action", "Create");
			lg.a("NAME", AcName);
			XMLBuilder nm = lg.element("NAME");
			nm.t(AcName);
			XMLBuilder pt = lg.element("PARENT");
			pt.t(ParentAccountHead);

			if (ParentAccountHead.equalsIgnoreCase("Duties & Taxes")) {
				XMLBuilder TAXTYPE = lg.element("TAXTYPE");
				TAXTYPE.t("GST");

				if (AcName.contains("CGST")) {
					XMLBuilder GSTDUTYHEAD = lg.element("GSTDUTYHEAD");
					GSTDUTYHEAD.t("Central Tax");

				} else if (AcName.contains("SGST")) {
					XMLBuilder GSTDUTYHEAD = lg.element("GSTDUTYHEAD");
					GSTDUTYHEAD.t("State Tax");
				}
			}
			XMLBuilder adlst = lg.element("ADDRESS.LIST");
			adlst.a("TYPE", "String");
			if (null != Address) {
				XMLBuilder adr = adlst.element("ADDRESS");
				adr.t(Address);
			}
			XMLBuilder mailname = adlst.element("MAILINGNAME.LIST");
			mailname.a("TYPE", "String");
			XMLBuilder maname = adlst.element("MAILINGNAME");
			maname.t(AcName);

			if (null != TinNo) {
				XMLBuilder intax = lg.element("VATTINNUMBER");
				intax.t(TinNo);
			}

			XMLBuilder afstk = lg.element("AFFECTSSTOCK");
			afstk.t(affectstock);

			XMLBuilder ASORIGINAL = lg.element("ASORIGINAL");
			ASORIGINAL.t("Yes");

			XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
			XMLBuilder langnmlst = lg.element("NAME.LIST");
			langlst.a("TYPE", "String");
			XMLBuilder lname = langnmlst.element("NAME");
			lname.t(AcName);
			if (null != Alias) {
				XMLBuilder alname = langnmlst.element("NAME");
				alname.t(Alias);
			}

			Properties outputProperties = new Properties();
			// Explicitly identify the output as an XML document
			outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
			// Pretty-print the XML output (doesn't work in all cases)
			outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
			// Get 2-space indenting when using the Apache transformer
			outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
			// Omit the XML declaration header
			outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

			PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
			en.toWriter(writer, outputProperties);
			writer.close();

		} catch (Exception e) {
			System.out.println(e.toString());
		}

		try {

			final HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
			HttpClient httpclient = new DefaultHttpClient(httpParams);

			HttpPost httppost = new HttpPost(TallyServer);
			File file = new File(iNfileName);
			FileEntity entity = new FileEntity(file, "text/plain; charset=\"UTF-8\"");

			httppost.setEntity(entity);
			System.out.println("executing request " + httppost.getRequestLine());
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity resEntity = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());

			String ErrorCode = "";
			String Linerror = "";
			if (resEntity != null) {
				try {
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(resEntity.getContent());

					NodeList nList = doc.getElementsByTagName("RESPONSE");
					for (int temp = 0; temp < nList.getLength(); temp++) {
						Node nNode = nList.item(temp);

						if (nNode.getNodeType() == Node.ELEMENT_NODE) {
							Element eElement = (Element) nNode;

							ErrorCode = eElement.getElementsByTagName("ERRORS").item(0).getTextContent();

							if (ErrorCode.equalsIgnoreCase("0")) {

								return "success";
							}
							System.out.println("ErrorCode =  " + ErrorCode);

							Linerror = eElement.getElementsByTagName("LINEERROR").item(0).getTextContent();
							System.out.println("Linerror =  " + Linerror);

							if (Linerror.contains("Voucher Number") && Linerror.contains("already exists!")) {

								System.out.println(Linerror);
								return "success";

							}
							 processReturnStatus(Linerror, companyMst,
									 TallyServer );

							if (ErrorCode.equalsIgnoreCase("1")) {

								return "failed";
							}

						}

					}

					System.out.println("Response content length: " + resEntity.getContentLength());
					System.out.println(resEntity.toString());
					System.out.println("Chunked?: " + resEntity.isChunked());

				} catch (Exception e2) {
					throw new SQLException(e2.toString());

				}
			}
			/*
			 * delete the file
			 */

			if (file.delete()) {
				System.out.println(file.getName() + " is deleted!");
			} else {
				System.out.println("Delete operation is failed.");
			}

		} catch (Exception est) {
			System.out.println(est);
			return "failed";
		}
		return "success";
	}

	private void processReturnStatus(String Linerror, 
			CompanyMst companyMst, String TallyServer )
			throws SQLException {

		if (Linerror.contains("Group") && Linerror.contains("does not exist!")) {
			String GroupName = Linerror.replace("Group", "").replace("does not exist!", "").replace("'", "").trim();

			System.out.println("missingLedger =  " + GroupName);

			String ParentAccountHead = getParentAccount(GroupName);

			LedgerGroupXML(GroupName, ParentAccountHead, TallyServer, TALLY_COMPANY, true);

			throw new SQLException("TRYAGAIN");
		}

		if (Linerror.contains("Cost Centre") && Linerror.contains("does not exist!")) {
			String goDownName = Linerror.replace("Cost Centre", "").replace("does not exist!", "").replace("'", "")
					.trim();

			System.out.println("missingLedger =  " + goDownName);

			CreateCostCenterXML(goDownName, TallyServer, TALLY_COMPANY);

			throw new SQLException("TRYAGAIN");
		}

		if (Linerror.contains("Voucher totals do not match!")) {

			throw new SQLException("Error" + Linerror);
		}

		if (Linerror.contains("No Accounting Allocations for")) {

			throw new SQLException("Error" + Linerror);
		}

		if (Linerror.contains("Ledger") && Linerror.contains("does not exist!")) {
			String missingLedger = Linerror.replace("Ledger", "").replace("does not exist!", "").replace("'", "")
					.trim();

			System.out.println("missingLedger =  " + missingLedger);

			String parentAccount = getParentAccount(missingLedger);
			String affectstock = "YES";
			if (parentAccount.equalsIgnoreCase("0")) {
				if (missingLedger.contains("Sales@")) {
					parentAccount = "Sales Accounts";
					missingLedger = missingLedger + "%";

				} else if (missingLedger.contains("Purchase@")) {
					parentAccount = "Purchase Accounts";
					missingLedger = missingLedger + "%";
				} else if (missingLedger.contains("Output VAT@")) {
					parentAccount = "Duties & Taxes";
					missingLedger = missingLedger + "%";
				} else if (missingLedger.contains("_CASH")) {
					parentAccount = "Cash-in-hand";
					affectstock = "NO";
				}
			}

			// if(null!=customerMst.getCustomerName() &&
			// customerMst.getCustomerName().length() > 0 &&
			// !customerMst.getCustomerName().contains("_CASH") ){

			AccountHeads CusOpt = accountHeadsRepository.findByAccountNameAndCompanyMstId(missingLedger,
					companyMst.getId());
			AccountHeads customerMst = CusOpt;
			if (null!=CusOpt) {

				parentAccount = getParentAccount(missingLedger);
				if (parentAccount.equalsIgnoreCase("")) {
					parentAccount = "Sundry Debtors";
					affectstock = "NO";
				}
				cCommonXML.CreateSupplier(missingLedger, customerMst.getPartyGst(), customerMst.getPartyAddress1(),
						customerMst.getId(), parentAccount, TallyServer, TALLY_COMPANY,  
						affectstock, customerMst.getCustomerPin(), customerMst.getCustomerCountry(),
						customerMst.getCustomerGstType(), customerMst.getCustomerState());

			} else {

				String OrgParent = parentAccount;
				while (true) {

					String tempParent = parentAccount;

					parentAccount = getParentAccount(parentAccount);

					if (parentAccount.equals("Liabilities")) {
						break;
					}

					if (parentAccount.equals("Assets")) {
						break;
					}

					if (parentAccount.equals("Income")) {
						break;
					}
					if (parentAccount.equals("Expenses")) {
						break;
					}

					LedgerGroupXML(tempParent, parentAccount, TallyServer, TALLY_COMPANY, true);

				}

				LedgerXML(missingLedger, "", "", "", OrgParent, TallyServer, companyMst ,   affectstock);

			}
			throw new SQLException("TRYAGAIN");
		}

		if (Linerror.contains("Voucher Type") && Linerror.contains("does not exist!")) {
			String missingLedger = Linerror.replace("Voucher Type", "").replace("does not exist!", "").replace("'", "")
					.trim();

			System.out.println("missingLedger =  " + missingLedger);

			String parentAccount = "Sales";

			cCommonXML.CreateVoucherTypeXML(missingLedger, parentAccount, "Manual", "Yes", TallyServer, TALLY_COMPANY);

			throw new SQLException("TRYAGAIN");
		}

		if (Linerror.contains("Stock Item") && Linerror.contains("does not exist!")) {
			String missingStockItem = Linerror.replace("Stock Item", "").replace("does not exist!", "").replace("'", "")
					.trim();

			System.out.println("missingLedger =  " + missingStockItem);

			ItemMst item = itemMstRepository.findByItemName(missingStockItem);

			UnitMst unitMst = unitMstRepository.findById(item.getUnitId()).get();

		 
			String affectstock = "YES";

			CategoryMst categoryMst = categoryMstRepository.findById(item.getCategoryId()).get();

			cCommonXML.CreateUnitXML(unitMst.getUnitName(), "2", unitMst.getUnitDescription(), TallyServer, TALLY_COMPANY);

			cCommonXML.NewStockGroup(categoryMst.getCategoryName(), TallyServer, unitMst.getUnitName(), item.getBarCode(),
					TALLY_COMPANY, affectstock);

		 	

			String GSTDTLAPPLICABLEFROM = "2018-01-01";

			GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("-", "");
			GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("/", "");


			cCommonXML.NewItemVerson5(missingStockItem, TallyServer,  item.getTaxRate(), unitMst.getUnitName(), item.getBarCode(),
					TALLY_COMPANY , affectstock, categoryMst.getCategoryName(), item.getHsnCode(), GSTDTLAPPLICABLEFROM);
			
		 
			throw new SQLException("TRYAGAIN");
		}

		if (Linerror.contains("Voucher Type") && Linerror.contains("does not exist!")) {
			String missingVoucherType = Linerror.replace("Voucher Type", "").replace("does not exist!", "")
					.replace("'", "").trim();

			System.out.println("Missing Voucher Type =  " + missingVoucherType);
			throw new SQLException("TRYAGAIN");
		}
	}

	public void LedgerGroupXML(String Group, String ParentAccountHead, String TallyServer, String CmpNmae,
			boolean DeleteXML) {

		/*
		 * Var declaration
		 */
		String iNfileName = "ledgergroup.xml";

		DateFormat formatter;
		formatter = new SimpleDateFormat("yyyyMMdd");

		try {

			XMLBuilder en = XMLBuilder.create("ENVELOPE");
			XMLBuilder Hd = en.element("HEADER");
			XMLBuilder vr = Hd.element("VERSION");
			vr.t("1");
			XMLBuilder TRq = Hd.element("TALLYREQUEST");
			TRq.t("import");
			XMLBuilder tp = Hd.element("TYPE");
			tp.t("Data");
			XMLBuilder id = Hd.element("ID");
			id.t("All Masters");
			XMLBuilder bd = en.element("BODY");
			XMLBuilder de = bd.element("DESC");
			XMLBuilder stv = de.element("STATICVARIABLES");

			XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
			cc.t(CmpNmae);

			XMLBuilder idu = stv.element("IMPORTDUPS");
			idu.t("@@DUPCOMBINE");
			XMLBuilder dt = bd.element("DATA");
			XMLBuilder tm = dt.element("TALLYMESSAGE");
			/*
			 * Create Group
			 */
			XMLBuilder lg = tm.element("GROUP");
			lg.a("Action", "Create");
			lg.a("NAME", Group);
			XMLBuilder subleger = lg.element("ISSUBLEDGER");
			subleger.t("No");
			XMLBuilder nm = lg.element("NAME");
			nm.t(Group);
			XMLBuilder pt = lg.element("PARENT");
			pt.t(ParentAccountHead);

			/*
			 * Code below will create Alias for the account
			 */
			XMLBuilder languageNameList = lg.element("LANGUAGENAME.LIST");
			XMLBuilder Nlist = languageNameList.element("NAME.LIST");
			Nlist.a("TYPE", "String");
			XMLBuilder Nme = Nlist.element("NAME");
			Nme.t(Group);
			// XMLBuilder Nme2 = Nlist.element("NAME");
			// Nme2.t(Alias);

			Properties outputProperties = new Properties();
			// Explicitly identify the output as an XML document
			outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
			// Pretty-print the XML output (doesn't work in all cases)
			outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
			// Get 2-space indenting when using the Apache transformer
			outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
			// Omit the XML declaration header
			outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

			PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
			en.toWriter(writer, outputProperties);

			writer.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		try {
			HttpClient httpclient = new DefaultHttpClient() {
			};
			HttpPost httppost = new HttpPost(TallyServer);

			// For Direct XML Test
			// File file = new File("c:\\projects10.xml");
			File file = new File(iNfileName);
			FileEntity entity = new FileEntity(file, "text/plain; charset=\"UTF-8\"");

			httppost.setEntity(entity);

			System.out.println("executing request " + httppost.getRequestLine());
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity resEntity = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());
			if (resEntity != null) {
				System.out.println("Response content length: " + resEntity.getContentLength());
				System.out.println(resEntity.toString());
				System.out.println("Chunked?: " + resEntity.isChunked());
			}
			if (DeleteXML) {
				if (file.delete()) {
					System.out.println(file.getName() + " is deleted!");
				} else {
					System.out.println("Delete operation is failed.");
				}

			}

		} catch (Exception est) {
			System.out.println(est);
		}
	}

	public String CreateCostCenterXML(String goDownName, String TallyServer, String CompanyName) {

		String iNfileName = goDownName + "CC.xml";
		try {

			System.out.println("Company " + CompanyName);

			System.out.println("cost center  " + goDownName);

			XMLBuilder en = XMLBuilder.create("ENVELOPE");
			XMLBuilder Hd = en.element("HEADER");
			// XMLBuilder vr = Hd.element("VERSION");

			// vr.t("1");
			XMLBuilder TRq = Hd.element("TALLYREQUEST");
			TRq.t("import Data");

			XMLBuilder bd = en.element("BODY");
			XMLBuilder de = bd.element("IMPORTDATA");
			XMLBuilder req = de.element("REQUESTDESC");
			XMLBuilder rep = req.element("REPORTNAME");

			rep.t("All Masters"); // All Masters Units

			XMLBuilder stv = req.element("STATICVARIABLES");
			XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
			cc.t(CompanyName);
			XMLBuilder idu = stv.element("IMPORTDUPS");
			idu.t("@@DUPCOMBINE");

			XMLBuilder dt = bd.element("DATA");
			XMLBuilder tm = dt.element("TALLYMESSAGE");
			// ****Start from Here ********//

			XMLBuilder lg = tm.element("COSTCENTRE");

			lg.a("NAME", goDownName);
			lg.a("RESERVEDNAME", "");

			XMLBuilder cg = tm.element("CATEGORY");
			cg.t("Primary Cost Category");

			XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
			XMLBuilder langnmlst = langlst.element("NAME.LIST");
			langlst.a("TYPE", "String");
			XMLBuilder lname = langnmlst.element("NAME");
			lname.t(goDownName);
			// XMLBuilder alname = langnmlst.element("NAME");
			// alname.t(AccountName);

			Properties outputProperties = new Properties();
			// Explicitly identify the output as an XML document
			outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
			// Pretty-print the XML output (doesn't work in all cases)
			outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
			// Get 2-space indenting when using the Apache transformer
			outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
			// Omit the XML declaration header
			outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

			PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
			en.toWriter(writer, outputProperties);
			writer.close();

		} catch (Exception e) {
			System.out.println(e.toString());
		}

		try {

			final HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
			HttpClient httpclient = new DefaultHttpClient(httpParams);

			HttpPost httppost = new HttpPost(TallyServer);
			File file = new File(iNfileName);
			FileEntity entity = new FileEntity(file, "text/plain; charset=\"UTF-8\"");

			httppost.setEntity(entity);
			System.out.println("executing request " + httppost.getRequestLine());
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity resEntity = response.getEntity();
			System.out.println("----------------------------------------");
			System.out.println(response.getStatusLine());
			if (resEntity != null) {
				System.out.println("Response content length: " + resEntity.getContentLength());
				System.out.println(resEntity.toString());
				System.out.println("Chunked?: " + resEntity.isChunked());
			}
			/*
			 * delete the file
			 * 
			 */

			if (file.delete()) {
				System.out.println(file.getName() + " is deleted!");
			} else {
				System.out.println("Delete operation is failed.");
			}

		} catch (Exception est) {
			System.out.println(est);
			return "failed";
		}
		return "success";
	}

	private String getParentAccount(String groupName) {
		// TODO Auto-generated method stub
		return null;
	}
}
