package com.maple.restserver.tally.integration.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

 
import com.jamesmurty.utils.XMLBuilder;
import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CategoryMst;
import com.maple.restserver.entity.CompanyMst;

import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CategoryMstRepository;

import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.UnitMstRepository;

@Component
public class CCommonXML {
	public final static Logger log = LoggerFactory.getLogger(CCommonXML.class);
	
	@Autowired
	ItemMstRepository itemMstRepository;

	@Autowired
	UnitMstRepository unitMstRepository;

	
	
	@Autowired
	CategoryMstRepository categoryMstRepository;
	

	@Autowired
	AccountHeadsRepository accountHeadsRepository;
	
	
	
    public String LedgerXML(String AcName, String TinNo, String Address, String Alias, String ParentAccountHead,
            String TallyServer,   CompanyMst companyMst,
             String affectstock, String TALLY_COMPANY) { 
        
    		 
    		
        String iNfileName = "ledger.xml";
        try {

        	  XMLBuilder en = XMLBuilder.create("ENVELOPE");
              XMLBuilder Hd = en.element("HEADER");
              XMLBuilder vr = Hd.element("VERSION");
              vr.t("1");
              XMLBuilder TRq = Hd.element("TALLYREQUEST");
              TRq.t("import");
              XMLBuilder tp = Hd.element("TYPE");
              tp.t("Data");
              XMLBuilder id = Hd.element("ID");
              id.t("All Masters");
              XMLBuilder bd = en.element("BODY");
              XMLBuilder de = bd.element("DESC");
              XMLBuilder stv = de.element("STATICVARIABLES");
              XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
              cc.t(TALLY_COMPANY);
              XMLBuilder idu = stv.element("IMPORTDUPS");
              idu.t("@@DUPCOMBINE");

              XMLBuilder dt = bd.element("DATA");
              XMLBuilder tm = dt.element("TALLYMESSAGE");
              XMLBuilder lg = tm.element("LEDGER");
              lg.a("Action", "Create");
              lg.a("NAME", AcName);
              XMLBuilder nm = lg.element("NAME");
              nm.t(AcName);
              XMLBuilder pt = lg.element("PARENT");
              pt.t(ParentAccountHead);
              
              if(ParentAccountHead.equalsIgnoreCase("Duties & Taxes")){
            	  XMLBuilder TAXTYPE = lg.element("TAXTYPE");
            	  TAXTYPE.t("GST");
            	  
            	  if(AcName.contains("CGST")){
            		  XMLBuilder GSTDUTYHEAD = lg.element("GSTDUTYHEAD");
            		  GSTDUTYHEAD.t("Central Tax");
            		 
            	  }else if (AcName.contains("SGST")){
            		  XMLBuilder GSTDUTYHEAD = lg.element("GSTDUTYHEAD");
            		  GSTDUTYHEAD.t("State Tax");
            	  }
              }
              XMLBuilder adlst = lg.element("ADDRESS.LIST");
              adlst.a("TYPE", "String");
              if(null!=Address){
            	  XMLBuilder adr = adlst.element("ADDRESS");
            	  adr.t(Address);
              }
              XMLBuilder mailname = adlst.element("MAILINGNAME.LIST");
              mailname.a("TYPE", "String");
              XMLBuilder maname = adlst.element("MAILINGNAME");
              maname.t(AcName);
              
              if(null!=TinNo){
            	  XMLBuilder intax = lg.element("VATTINNUMBER");
            	  intax.t(TinNo);
              }
              
              XMLBuilder afstk = lg.element("AFFECTSSTOCK");
              afstk.t(affectstock);
              
              XMLBuilder ASORIGINAL = lg.element("ASORIGINAL");
              ASORIGINAL.t("Yes");
              
              XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
              XMLBuilder langnmlst = lg.element("NAME.LIST");
              langlst.a("TYPE","String");
              XMLBuilder lname = langnmlst.element("NAME");
              lname.t(AcName);
              if(null!=Alias){
            	  XMLBuilder alname = langnmlst.element("NAME");
            	  alname.t(Alias);
              }

            
            
            
            Properties outputProperties = new Properties();
            // Explicitly identify the output as an XML document
            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
            // Pretty-print the XML output (doesn't work in all cases)
            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
            // Get 2-space indenting when using the Apache transformer
            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
            // Omit the XML declaration header
            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
            en.toWriter(writer, outputProperties);
            writer.close();

        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {
        	 log.info(" LedgerXML - httpclient");
       	   final HttpParams httpParams = new BasicHttpParams();
  		    HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
  			HttpClient httpclient = new DefaultHttpClient(httpParams);
    
  			
  			

            HttpPost httppost = new HttpPost(TallyServer);
            File file = new File(iNfileName);
            FileEntity entity = new FileEntity(file,
                    "text/plain; charset=\"UTF-8\"");

            httppost.setEntity(entity);
            System.out.println("executing request "
                    + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            
            String ErrorCode = "";
            String Linerror  = "";
            if (resEntity != null) {
				try {
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(resEntity.getContent());

					NodeList nList = doc.getElementsByTagName("RESPONSE");
					for (int temp = 0; temp < nList.getLength(); temp++) {
						Node nNode = nList.item(temp);

						if (nNode.getNodeType() == Node.ELEMENT_NODE) {
							Element eElement = (Element) nNode;
							

							ErrorCode  = eElement.getElementsByTagName("ERRORS").item(0)
									.getTextContent();

							if(ErrorCode.equalsIgnoreCase("0")){
								
								 return "success";
							}
							System.out.println("ErrorCode =  " + ErrorCode);

							Linerror = eElement.getElementsByTagName("LINEERROR").item(0).getTextContent();
							System.out.println("Linerror =  " + Linerror);

							if (Linerror.contains("Voucher Number") && Linerror.contains("already exists!")) {
								 
								System.out.println(Linerror);
								 return "success";
								
							}
							processReturnStatus(Linerror,companyMst,TallyServer,TALLY_COMPANY,"Ledger");
						
							if(ErrorCode.equalsIgnoreCase("1")){
								
								return "failed";
							}

						}

					}

					System.out.println("Response content length: " + resEntity.getContentLength());
					System.out.println(resEntity.toString());
					System.out.println("Chunked?: " + resEntity.isChunked());

				} catch (Exception e2) {
					throw new SQLException(e2.toString());

				}
			}
            /*
             * delete the file
             */


            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }


        } catch (Exception est) {
            System.out.println(est);
            return "failed";
        }
        return "success";
    }
    
    
    public void NewItem_version6OLD(String StockItem, String TallyServer,  double vat,
    		String Unit, String barcode, String CompanyName,
    		String affectstock, String parentgroup, String hsnCode,String GSTDTLAPPLICABLEFROM, 
    		String orginalname, String gstUnitName, String category, double cess
    		) {
    	//REGY
    	
    	String outFile = "";
    	
    		 
    	  String iNfileName =  "StockItem.xml";
       
    	  BigDecimal bdIgst = new BigDecimal(vat);
    	  bdIgst = bdIgst.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    	  
    	  BigDecimal bdCess = new BigDecimal(cess);
    	  bdCess = bdCess.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    	  
    	  
    	  double sgst = vat/2;
    	  double cgst = vat/2;
    	  
    	  BigDecimal bdSgst = new BigDecimal(sgst);
    	  BigDecimal bdCgst = new BigDecimal(cgst);
    	  
    	   bdSgst = bdSgst.setScale(2, BigDecimal.ROUND_HALF_EVEN) ;
    	   bdCgst= bdCgst.setScale(2, BigDecimal.ROUND_HALF_EVEN) ;
    	   
    	   
    	  
        try {
          

            XMLBuilder ENVELOPE = XMLBuilder.create("ENVELOPE");
            XMLBuilder HEADER = ENVELOPE.element("HEADER");
           
            XMLBuilder TALLYREQUEST = HEADER.element("TALLYREQUEST");
            TALLYREQUEST.t("import Data");
            XMLBuilder BODY = HEADER.element("BODY");
            
            XMLBuilder TYPE = HEADER.element("TYPE");
            TYPE.t("Data");
            
             
            XMLBuilder IMPORTDATA = BODY.element("IMPORTDATA");
            XMLBuilder REQUESTDESC = IMPORTDATA.element("REQUESTDESC");
            XMLBuilder REPORTNAME = REQUESTDESC.element("REPORTNAME");
             REPORTNAME.t("All Masters");
 
           
             
            XMLBuilder STATICVARIABLES = REQUESTDESC.element("STATICVARIABLES");
            
            XMLBuilder IMPORTDUPS = STATICVARIABLES.element("IMPORTDUPS");
            IMPORTDUPS.t("@@DUPIGNORECOMBINE");
            
            XMLBuilder SVCURRENTCOMPANY = STATICVARIABLES.element("SVCURRENTCOMPANY");
            SVCURRENTCOMPANY.t(CompanyName);
            
            XMLBuilder REQUESTDATA = IMPORTDATA.element("REQUESTDATA");
            XMLBuilder TALLYMESSAGE = REQUESTDATA.element("TALLYMESSAGE");
            TALLYMESSAGE.a("xmlns:UDF", "TallyUDF");
            XMLBuilder UNIT = TALLYMESSAGE.element("UNIT");
           
            UNIT.a("NAME",  Unit );  //Variable
            UNIT.a("RESERVEDNAME","");
            XMLBuilder NAME = UNIT.element("NAME");
            NAME.t(Unit); //Variable
            
        
            XMLBuilder GSTREPUOM = UNIT.element("GSTREPUOM");
            GSTREPUOM.t(gstUnitName );  //"NOS-NUMBERS" - Variable
            
            
            
            
            XMLBuilder TALLYMESSAGE2 = REQUESTDATA.element("TALLYMESSAGE");
            TALLYMESSAGE2.a("xmlns:UDF", "TallyUDF");
            
            XMLBuilder STOCKGROUP = TALLYMESSAGE2.element("STOCKGROUP");
            STOCKGROUP.a("NAME", category ); // "MISC" Variable
            STOCKGROUP.a("RESERVEDNAME", "");  
            
            
            XMLBuilder LANGUAGENAME_LIST = STOCKGROUP.element("LANGUAGENAME.LIST");
            XMLBuilder NAME_LIST = STOCKGROUP.element("NAME.LIST");
            XMLBuilder NAME_CAT = STOCKGROUP.element("NAME");
            NAME_CAT.t(category);
            
            
            
            XMLBuilder TALLYMESSAGE3 = REQUESTDATA.element("TALLYMESSAGE");
            STOCKGROUP.a("xmlns:UDF", "TallyUDF");  
           
            XMLBuilder STOCKITEM = TALLYMESSAGE3.element("STOCKITEM");
            STOCKITEM.a("NAME", StockItem ); // "TISCON 8 MM" Variable
            STOCKITEM.a("RESERVEDNAME", "");  
            
            
            XMLBuilder PARENT = STOCKITEM.element("PARENT");
            PARENT.t(category ); //"MISC"  Variable
            XMLBuilder GSTAPPLICABLE = STOCKITEM.element("GSTAPPLICABLE");
            GSTAPPLICABLE.t("Applicable");
            
            //<GSTAPPLICABLE>Applicable</GSTAPPLICABLE>
            
            XMLBuilder TAXCLASSIFICATIONNAME = STOCKITEM.element("TAXCLASSIFICATIONNAME");
            
            
            XMLBuilder GSTTYPEOFSUPPLY = STOCKITEM.element("GSTTYPEOFSUPPLY");
            
            GSTTYPEOFSUPPLY.t("Goods");
            
            XMLBuilder BASEUNITS = STOCKITEM.element("BASEUNITS");
            
            BASEUNITS.t(Unit);
            
            
       
             
            
            XMLBuilder GSTDETAILS_LIST = STOCKITEM.element("GSTDETAILS.LIST");
            
            
        
            
            XMLBuilder APPLICABLEFROM = GSTDETAILS_LIST.element("APPLICABLEFROM");
            APPLICABLEFROM.t("20190401");
            
            XMLBuilder CALCULATIONTYPE = GSTDETAILS_LIST.element("CALCULATIONTYPE");
            CALCULATIONTYPE.t("On Value");
            
            XMLBuilder HSNCODE = GSTDETAILS_LIST.element("HSNCODE");
            HSNCODE.t(hsnCode ); //"1234567" Variable 
            
            XMLBuilder TAXABILITY = GSTDETAILS_LIST.element("TAXABILITY");
            TAXABILITY.t("Taxable");
            
            
            XMLBuilder STATEWISEDETAILS_LIST = GSTDETAILS_LIST.element("STATEWISEDETAILS.LIST");
            XMLBuilder STATENAME = STATEWISEDETAILS_LIST.element("STATENAME");
            STATENAME.t("Any");
            
            XMLBuilder RATEDETAILS_LIST = STATEWISEDETAILS_LIST.element("RATEDETAILS.LIST");
            
            XMLBuilder GSTRATEDUTYHEAD = RATEDETAILS_LIST.element("GSTRATEDUTYHEAD");
            GSTRATEDUTYHEAD.t("Central Tax");
            
            
            XMLBuilder GSTRATEVALUATIONTYPE = RATEDETAILS_LIST.element("GSTRATEVALUATIONTYPE");
            GSTRATEVALUATIONTYPE.t("Based on Value");
            XMLBuilder GSTRATE = RATEDETAILS_LIST.element("GSTRATE");
            GSTRATE.t(bdSgst.toPlainString());  //Variable
            
            
            XMLBuilder RATEDETAILS_LIST2 = STATEWISEDETAILS_LIST.element("RATEDETAILS.LIST");
            XMLBuilder GSTRATEDUTYHEAD2 = RATEDETAILS_LIST2.element("GSTRATEDUTYHEAD");
            GSTRATEDUTYHEAD2.t("State Tax");
            XMLBuilder GSTRATEVALUATIONTYPE2 = RATEDETAILS_LIST2.element("GSTRATEVALUATIONTYPE");
            GSTRATEVALUATIONTYPE2.t("Based on Value");
            XMLBuilder GSTRATE2 = RATEDETAILS_LIST2.element("GSTRATE");
            GSTRATE2.t(bdCgst.toPlainString());  //Variable
            
            
            XMLBuilder RATEDETAILS_LIST3 = STATEWISEDETAILS_LIST.element("RATEDETAILS.LIST");
            XMLBuilder GSTRATEDUTYHEAD3 = RATEDETAILS_LIST3.element("GSTRATEDUTYHEAD");
            GSTRATEDUTYHEAD3.t("Integrated Tax");
            XMLBuilder GSTRATEVALUATIONTYPE3 = RATEDETAILS_LIST3.element("GSTRATEVALUATIONTYPE");
            GSTRATEVALUATIONTYPE3.t("Based on Value");
            XMLBuilder GSTRATE3 = RATEDETAILS_LIST3.element("GSTRATE");
            GSTRATE3.t(bdIgst.toPlainString());  //Variable
            
            XMLBuilder RATEDETAILS_LIST4 = STATEWISEDETAILS_LIST.element("RATEDETAILS.LIST");
            XMLBuilder GSTRATEDUTYHEAD4 = RATEDETAILS_LIST4.element("GSTRATEDUTYHEAD");
            GSTRATEDUTYHEAD4.t("Cess");
            XMLBuilder GSTRATEVALUATIONTYPE4 = RATEDETAILS_LIST4.element("GSTRATEVALUATIONTYPE");
            GSTRATEVALUATIONTYPE4.t("Based on Value");
            XMLBuilder GSTRATE4 = RATEDETAILS_LIST4.element("GSTRATE");
            GSTRATE4.t(bdCess.toPlainString());  //Variable
       
            
            XMLBuilder LANGUAGENAME_LIST2 = STOCKITEM.element("LANGUAGENAME.LIST");
            LANGUAGENAME_LIST2.a("TYPE", "String");
            
            XMLBuilder NAME_LANGUAGENAME_LIST = LANGUAGENAME_LIST2.element("NAME");
            NAME_LANGUAGENAME_LIST.t(StockItem); //Variable
            
           // XMLBuilder NAME_LANGUAGENAME_LIST2 = LANGUAGENAME_LIST.element("NAME");
           // NAME_LANGUAGENAME_LIST2.t("BARCODE"); //Variable
           // XMLBuilder imp = stv.element("IMPORTDUPS");
            //imp.t("@@DUPIGNORECOMBINE");
          
            
            
				
            Properties outputProperties = new Properties();
            // Explicitly identify the output as an XML document
            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
            // Pretty-print the XML output (doesn't work in all cases)
            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
            // Get 2-space indenting when using the Apache transformer
            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
            // Omit the XML declaration header
            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
            ENVELOPE.toWriter(writer, outputProperties);
            writer.close();

        	String search = "<GSTAPPLICABLE>Applicable</GSTAPPLICABLE>";  // <- changed to work with String.replaceAll()
        	String replacement = "<GSTAPPLICABLE>&#4; Applicable</GSTAPPLICABLE>";
       
           outFile = "allitemmaster.xml";
           setAmbersandForGST(iNfileName,search,replacement,outFile);
            
           
           

      // 	String search2 = "<STATENAME>Any</STATENAME>";  // <- changed to work with String.replaceAll()
      // 	String replacement2 = "<STATENAME>&#4;Any</STATENAME>";
      
      //    outFile = "xout.xml";
        //  setAmbersandForGST(iNfileName,search,replacement,outFile);
           
        
          
            
            
            //<GSTAPPLICABLE>Applicable</GSTAPPLICABLE>
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {
            HttpClient httpclient = new DefaultHttpClient();

            HttpPost httppost = new HttpPost(TallyServer);
           
            
            File file = new File(outFile); 
            //File file = new File(iNfileName); 

            FileEntity entity = new FileEntity(file,
                    "text/html; charset=\"UTF-8\"");

            httppost.setEntity(entity);
            System.out.println("executing request "
                    + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            String VoucherId="";
            String Linerror = "";
            String ErrorCode = "";
            
            if (resEntity != null) {
				try {
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(resEntity.getContent());

					NodeList nList = doc.getElementsByTagName("RESPONSE");
					for (int temp = 0; temp < nList.getLength(); temp++) {
						Node nNode = nList.item(temp);

						if (nNode.getNodeType() == Node.ELEMENT_NODE) {
							Element eElement = (Element) nNode;
							VoucherId = eElement.getElementsByTagName("LASTVCHID").item(0).getTextContent();
							System.out.println("Response VoucherId =  " + VoucherId);

							ErrorCode = VoucherId = eElement.getElementsByTagName("ERRORS").item(0)
									.getTextContent();

							System.out.println("ErrorCode =  " + ErrorCode);

							Linerror = eElement.getElementsByTagName("LINEERROR").item(0).getTextContent();
							System.out.println("Linerror =  " + Linerror);

							if (Linerror.contains("Group") && Linerror.contains("does not exist!")) {
								String missingLedger = Linerror.replace("Group", "").replace("does not exist!", "")
										.replace("'", "").trim();

								System.out.println("missingLedger =  " + missingLedger);
								 
							 
								 
								 
								 String XML_PATH = "";
								 
								 NewStockGroup(missingLedger,  TallyServer,   
								    		 Unit,   barcode,   CompanyName,  affectstock);

								throw new SQLException("TRYAGAIN");
							}

							if (Linerror.contains("Unit") && Linerror.contains("does not exist!")) {
								String missingStockItem = Linerror.replace("Unit", "")
										.replace("does not exist!", "").replace("'", "").trim();

								System.out.println("missingLedger =  " + missingStockItem);
								
								ItemMst item = itemMstRepository.findByItemName(missingStockItem);
 								UnitMst unitMst = unitMstRepository.findById(item.getUnitId()).get();
										
									 
								String XML_PATH =  unitMst.getUnitName() +".xml" ; 
								
								CreateUnitXML(  unitMst.getUnitName(), "2", unitMst.getUnitName(),
							              TallyServer,     CompanyName
							               ) ;
								
																
								throw new SQLException("TRYAGAIN");
							}
							
							if (Linerror.contains("Voucher Type") && Linerror.contains("does not exist!")) {
								String missingVoucherType = Linerror.replace("Voucher Type", "")
										.replace("does not exist!", "").replace("'", "").trim();

								System.out.println("Missing Voucher Type =  " + missingVoucherType);
								throw new SQLException("TRYAGAIN");
							}

						}

					}

					System.out.println("Response content length: " + resEntity.getContentLength());
					System.out.println(resEntity.toString());
					System.out.println("Chunked?: " + resEntity.isChunked());

				} catch (Exception e2) {
					throw new SQLException(e2.toString());

				}
			}

            /*
             * delete the file
            
*/
            

          /*  if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }
            */
            
          
            

        } catch (Exception est) {
            System.out.println(est);
            
        }
        
    }
    
    
    public void NewItemVerson5(String StockItem, String TallyServer,  double vat,
    		String Unit, String barcode, String CompanyName,
    		String affectstock, String parentgroup, String hsnCode,String GSTDTLAPPLICABLEFROM
    		) {
    	//REGY
    	
    	String outFile = "";
    	
    		double cess = 0;
    	  String iNfileName =  "StockItem.xml";
       
    	  BigDecimal bdIgst = new BigDecimal(vat);
    	  bdIgst = bdIgst.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    	  
    	  BigDecimal bdCess = new BigDecimal(cess);
    	  bdCess = bdCess.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    	  
    	  
    	  double sgst = vat/2;
    	  double cgst = vat/2;
    	  
    	  BigDecimal bdSgst = new BigDecimal(sgst);
    	  BigDecimal bdCgst = new BigDecimal(cgst);
    	  
    	   bdSgst = bdSgst.setScale(2, BigDecimal.ROUND_HALF_EVEN) ;
    	   bdCgst= bdCgst.setScale(2, BigDecimal.ROUND_HALF_EVEN) ;
    	   
    	   
    	  
        try {
          

            XMLBuilder en = XMLBuilder.create("ENVELOPE");
            XMLBuilder Hd = en.element("HEADER");
            XMLBuilder vr = Hd.element("VERSION");
            vr.t("1");
            XMLBuilder TRq = Hd.element("TALLYREQUEST");
            TRq.t("import");
            XMLBuilder tYP = Hd.element("TYPE");
            tYP.t("Data");
            XMLBuilder rid = Hd.element("ID");
            rid.t("All Masters");

            XMLBuilder id = en.element("BODY");
            XMLBuilder rd = id.element("DESC");
            XMLBuilder stv = rd.element("STATICVARIABLES");
            XMLBuilder imp = stv.element("IMPORTDUPS");
            imp.t("@@DUPIGNORECOMBINE");
            XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
            cc.t(CompanyName);

            XMLBuilder rdata = id.element("DATA");
            XMLBuilder tm = rdata.element("TALLYMESSAGE");
            XMLBuilder mstr = tm.element("STOCKITEM");
            mstr.a("Action", "Create");
            mstr.a("NAME", StockItem);
            
            
            XMLBuilder pr = mstr.element("PARENT");
            pr.t(parentgroup);
           // XMLBuilder namelst = mstr.element("NAME.LIST");
            
            
           // XMLBuilder name1 = namelst.element("NAME");
           // name1.t(StockItem);
           // XMLBuilder name2 = namelst.element("NAME");
           // name2.t(barcode);
            XMLBuilder base = mstr.element("BASEUNITS");
     
            String str1 = StockItem;
            
            base.t(Unit);
            
            // XMLBuilder vatrate = namelst.element("RATEOFVAT");
            // vatrate.t(vat+" ");

            XMLBuilder gstapl = mstr.element("GSTAPPLICABLE");

            
            gstapl.t("Applicable");
            XMLBuilder gstsply = mstr.element("GSTTYPEOFSUPPLY");
            
            gstsply.t("Goods");
            
           // XMLBuilder vatapl = mstr.element("VATAPPLICABLE");
          //  vatapl.t("Not Applicable");  
            
            XMLBuilder gstdtl = mstr.element("GSTDETAILS.LIST");
            XMLBuilder aplfrom = gstdtl.element("APPLICABLEFROM");
            aplfrom.t(GSTDTLAPPLICABLEFROM);
            XMLBuilder gslcalc = gstdtl.element("CALCULATIONTYPE");
            gslcalc.t("On Value");
            
            
            XMLBuilder gsttax = gstdtl.element("TAXABILITY");
            
            if(vat==0) {
            	 gsttax.t("Exempt");
            }else {
            	 gsttax.t("Taxable");
            }
           
            
            XMLBuilder nongst = gstdtl.element("ISNONGSTGOODS");
            nongst.t("No");
             
            
            XMLBuilder statedtl = gstdtl.element("STATEWISEDETAILS.LIST");
            XMLBuilder stname = statedtl.element("STATENAME");
            stname.t("Any");
            
            XMLBuilder RATEDETAILSLIST = statedtl.element("RATEDETAILS.LIST");
            XMLBuilder GSTRATEDUTYHEAD = RATEDETAILSLIST.element("GSTRATEDUTYHEAD");
            GSTRATEDUTYHEAD.t("Central Tax");
            XMLBuilder GSTRATEVALUATIONTYPE = RATEDETAILSLIST.element("GSTRATEVALUATIONTYPE");
            GSTRATEVALUATIONTYPE.t("Based on Value");
            XMLBuilder GSTRATE = RATEDETAILSLIST.element("GSTRATE");
            GSTRATE.t(bdCgst.toPlainString());
            
            
            XMLBuilder RATEDETAILSLIST2 = statedtl.element("RATEDETAILS.LIST");
            XMLBuilder GSTRATEDUTYHEAD2 = RATEDETAILSLIST2.element("GSTRATEDUTYHEAD");
            GSTRATEDUTYHEAD2.t("State Tax");
            XMLBuilder GSTRATEVALUATIONTYPE2 = RATEDETAILSLIST2.element("GSTRATEVALUATIONTYPE");
            GSTRATEVALUATIONTYPE2.t("Based on Value");
            XMLBuilder GSTRATE2 = RATEDETAILSLIST2.element("GSTRATE");
            GSTRATE2.t(bdSgst.toPlainString());
            
            
            
            XMLBuilder RATEDETAILSLIST3 = statedtl.element("RATEDETAILS.LIST");
            XMLBuilder GSTRATEDUTYHEAD3 = RATEDETAILSLIST3.element("GSTRATEDUTYHEAD");
            GSTRATEDUTYHEAD3.t("Integrated Tax");
            XMLBuilder GSTRATEVALUATIONTYPE3 = RATEDETAILSLIST3.element("GSTRATEVALUATIONTYPE");
            GSTRATEVALUATIONTYPE3.t("Based on Value");
            XMLBuilder GSTRATE3 = RATEDETAILSLIST3.element("GSTRATE");
            GSTRATE3.t(bdIgst.toPlainString());
            
            
            XMLBuilder RATEDETAILSLIST4 = statedtl.element("RATEDETAILS.LIST");
            XMLBuilder GSTRATEDUTYHEAD4 = RATEDETAILSLIST4.element("GSTRATEDUTYHEAD");
            GSTRATEDUTYHEAD4.t("Cess");
            XMLBuilder GSTRATEVALUATIONTYPE4 = RATEDETAILSLIST4.element("GSTRATEVALUATIONTYPE");
            GSTRATEVALUATIONTYPE4.t("Based on Value");
            XMLBuilder GSTRATE4 = RATEDETAILSLIST4.element("GSTRATE");
            GSTRATE4.t(bdCess.toPlainString());
            
            
            
            
            XMLBuilder HSN = gstdtl.element("HSNCODE");
            
            HSN.t(hsnCode);
             
            
            
            
            
            XMLBuilder LANGUAGENAMELIST = mstr.element("LANGUAGENAME.LIST");
            XMLBuilder NAMELIST = LANGUAGENAMELIST.element("NAME.LIST");
            NAMELIST.a("TYPE", "String");
            XMLBuilder NAME = NAMELIST.element("NAME");
            NAME.t(StockItem);
  
            
            
				
            Properties outputProperties = new Properties();
            // Explicitly identify the output as an XML document
            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
            // Pretty-print the XML output (doesn't work in all cases)
            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
            // Get 2-space indenting when using the Apache transformer
            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
            // Omit the XML declaration header
            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
            en.toWriter(writer, outputProperties);
            writer.close();

        	String search = "<GSTAPPLICABLE>Applicable</GSTAPPLICABLE>";  // <- changed to work with String.replaceAll()
        	String replacement = "<GSTAPPLICABLE>&#4; Applicable</GSTAPPLICABLE>";
       
        	 outFile = "xout.xml";
            setAmbersandForGST(iNfileName,search,replacement,outFile);
            
            
            
            //<GSTAPPLICABLE>Applicable</GSTAPPLICABLE>
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {
            HttpClient httpclient = new DefaultHttpClient();

            HttpPost httppost = new HttpPost(TallyServer);
            File file = new File(outFile); 

            FileEntity entity = new FileEntity(file,
                    "text/html; charset=\"UTF-8\"");

            httppost.setEntity(entity);
            System.out.println("executing request "
                    + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            String VoucherId="";
            String Linerror = "";
            String ErrorCode = "";
            
            if (resEntity != null) {
				try {
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(resEntity.getContent());

					NodeList nList = doc.getElementsByTagName("RESPONSE");
					for (int temp = 0; temp < nList.getLength(); temp++) {
						Node nNode = nList.item(temp);

						if (nNode.getNodeType() == Node.ELEMENT_NODE) {
							Element eElement = (Element) nNode;
							VoucherId = eElement.getElementsByTagName("LASTVCHID").item(0).getTextContent();
							System.out.println("Response VoucherId =  " + VoucherId);

							ErrorCode = VoucherId = eElement.getElementsByTagName("ERRORS").item(0)
									.getTextContent();

							System.out.println("ErrorCode =  " + ErrorCode);

							Linerror = eElement.getElementsByTagName("LINEERROR").item(0).getTextContent();
							System.out.println("Linerror =  " + Linerror);

							if (Linerror.contains("Group") && Linerror.contains("does not exist!")) {
								String missingLedger = Linerror.replace("Group", "").replace("does not exist!", "")
										.replace("'", "").trim();

								System.out.println("missingLedger =  " + missingLedger);
								 
							 
								 
								 
								 String XML_PATH = "";
								 
								 NewStockGroup(missingLedger,  TallyServer,   
								    		 Unit,   barcode,   CompanyName,  affectstock);

								throw new SQLException("TRYAGAIN");
							}

							if (Linerror.contains("Unit") && Linerror.contains("does not exist!")) {
								String missingStockItem = Linerror.replace("Unit", "")
										.replace("does not exist!", "").replace("'", "").trim();

								System.out.println("missingLedger =  " + missingStockItem);
								
								ItemMst item = itemMstRepository.findByItemName(missingStockItem);
 								UnitMst unitMst = unitMstRepository.findById(item.getUnitId()).get();
										
									 
								String XML_PATH =  unitMst.getUnitName() +".xml" ; 
								
								CreateUnitXML(  unitMst.getUnitName(), "2", unitMst.getUnitName(),
							              TallyServer,     CompanyName
							               ) ;
								
																
								throw new SQLException("TRYAGAIN");
							}
							
							if (Linerror.contains("Voucher Type") && Linerror.contains("does not exist!")) {
								String missingVoucherType = Linerror.replace("Voucher Type", "")
										.replace("does not exist!", "").replace("'", "").trim();

								System.out.println("Missing Voucher Type =  " + missingVoucherType);
								throw new SQLException("TRYAGAIN");
							}

						}

					}

					System.out.println("Response content length: " + resEntity.getContentLength());
					System.out.println(resEntity.toString());
					System.out.println("Chunked?: " + resEntity.isChunked());

				} catch (Exception e2) {
					throw new SQLException(e2.toString());

				}
			}

            /*
             * delete the file
            
*/
            

            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }
            
          
            

        } catch (Exception est) {
            System.out.println(est);
            
        }
        
    }
 
    
    public void NewStockGroup(String StockItem, String TallyServer, 
    		String Unit, String barcode, String CompanyName,String affectstock) {
    	  String iNfileName = StockItem+".xml";
       
        try {
          

            XMLBuilder en = XMLBuilder.create("ENVELOPE");
            XMLBuilder Hd = en.element("HEADER");
            XMLBuilder vr = Hd.element("VERSION");
            vr.t("1");
            XMLBuilder TRq = Hd.element("TALLYREQUEST");
            TRq.t("import");
            XMLBuilder tYP = Hd.element("TYPE");
            tYP.t("Data");
            XMLBuilder rid = Hd.element("ID");
            rid.t("All Masters");

            XMLBuilder id = en.element("BODY");
            XMLBuilder rd = id.element("DESC");
            XMLBuilder stv = rd.element("STATICVARIABLES");
            XMLBuilder imp = stv.element("IMPORTDUPS");
            imp.t("@@DUPIGNORECOMBINE");
            XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
            cc.t(CompanyName);

            XMLBuilder rdata = id.element("DATA");
            XMLBuilder tm = rdata.element("TALLYMESSAGE");
            XMLBuilder mstr = tm.element("STOCKGROUP");
            mstr.a("Action", "Create");
            mstr.a("NAME", StockItem);
            XMLBuilder namelst = mstr.element("NAME.LIST");
            XMLBuilder name1 = namelst.element("NAME");
            name1.t(StockItem);
            //XMLBuilder name2 = namelst.element("NAME");
           // name2.t(barcode);
            XMLBuilder base = namelst.element("BASEUNITS");
     
           // String str1 = StockItem;
            
            //base.t(Unit);
            
          //   XMLBuilder vatrate = namelst.element("RATEOFVAT");
          //  vatrate.t(vat+" ");


            Properties outputProperties = new Properties();
            // Explicitly identify the output as an XML document
            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
            // Pretty-print the XML output (doesn't work in all cases)
            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
            // Get 2-space indenting when using the Apache transformer
            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
            // Omit the XML declaration header
            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
            en.toWriter(writer, outputProperties);
            writer.close();

        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {
            HttpClient httpclient = new DefaultHttpClient();

            HttpPost httppost = new HttpPost(TallyServer);
            File file = new File(iNfileName); 

            FileEntity entity = new FileEntity(file,
                    "text/plain; charset=\"UTF-8\"");

            httppost.setEntity(entity);
            System.out.println("executing request "
                    + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            String VoucherId="";
            String Linerror = "";
            String ErrorCode = "";
            
            if (resEntity != null) {
				try {
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(resEntity.getContent());

					NodeList nList = doc.getElementsByTagName("RESPONSE");
					for (int temp = 0; temp < nList.getLength(); temp++) {
						Node nNode = nList.item(temp);

						if (nNode.getNodeType() == Node.ELEMENT_NODE) {
							Element eElement = (Element) nNode;
							VoucherId = eElement.getElementsByTagName("LASTVCHID").item(0).getTextContent();
							System.out.println("Response VoucherId =  " + VoucherId);

							ErrorCode = VoucherId = eElement.getElementsByTagName("ERRORS").item(0)
									.getTextContent();

							System.out.println("ErrorCode =  " + ErrorCode);

							Linerror = eElement.getElementsByTagName("LINEERROR").item(0).getTextContent();
							System.out.println("Linerror =  " + Linerror);

						

							if (Linerror.contains("Unit") && Linerror.contains("does not exist!")) {
								String missingStockItem = Linerror.replace("Unit", "")
										.replace("does not exist!", "").replace("'", "").trim();

								System.out.println("missingLedger =  " + missingStockItem);
								
								ItemMst item = itemMstRepository.findByItemName(missingStockItem);
										
								UnitMst unitMst = 	unitMstRepository.findById(item.getId()).get();
								
										 
								 								String XML_PATH = unitMst.getUnitName() +".xml" ; 
								
								CreateUnitXML(  unitMst.getUnitName(), "2", unitMst.getUnitName(),
							              TallyServer,     CompanyName
							                ) ;
								
																
								throw new SQLException("TRYAGAIN");
							}
							
							if (Linerror.contains("Voucher Type") && Linerror.contains("does not exist!")) {
								String missingVoucherType = Linerror.replace("Voucher Type", "")
										.replace("does not exist!", "").replace("'", "").trim();

								System.out.println("Missing Voucher Type =  " + missingVoucherType);
								throw new SQLException("TRYAGAIN");
							}

						}

					}

					System.out.println("Response content length: " + resEntity.getContentLength());
					System.out.println(resEntity.toString());
					System.out.println("Chunked?: " + resEntity.isChunked());

				} catch (Exception e2) {
					throw new SQLException(e2.toString());

				}
			}

            /*
             * delete the file
            
*/
            

            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }
             	
            

        } catch (Exception est) {
            System.out.println(est);
            
        }
        
    }
    
    
    public String CreateUnitXML(String UnitName, String DecimalPalce, String FormalName,
            String TallyServer,  String CompanyName ) { 
 	  
        String iNfileName =   "Unit" + UnitName + ".xml";
        try {
     	   
     	   
     	    

        	  XMLBuilder en = XMLBuilder.create("ENVELOPE");
              XMLBuilder Hd = en.element("HEADER");
              //XMLBuilder vr = Hd.element("VERSION");

  
              //vr.t("1");
              XMLBuilder TRq = Hd.element("TALLYREQUEST");
              TRq.t("import Data");
             
           
              XMLBuilder bd = en.element("BODY");
              XMLBuilder de = bd.element("IMPORTDATA");
              XMLBuilder req = de.element("REQUESTDESC");
              XMLBuilder rep = req.element("REPORTNAME");
              
              rep.t("All Masters");  //All Masters Units
              
              XMLBuilder stv = req.element("STATICVARIABLES");
              XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
              cc.t(CompanyName);
              XMLBuilder idu = stv.element("IMPORTDUPS");
              idu.t("@@DUPCOMBINE");

              XMLBuilder dt = bd.element("DATA");
              XMLBuilder tm = dt.element("TALLYMESSAGE");
              
    
              XMLBuilder lg = tm.element("UNIT");
            
              lg.a("NAME", UnitName);
              lg.a("RESERVEDNAME", "");
              XMLBuilder unm = lg.element("NAME");
              
              unm.t(UnitName);
              XMLBuilder nm = lg.element("ORIGINALNAME");
              nm.t(FormalName);
              XMLBuilder pt = lg.element("DECIMALPLACES");
              pt.t(DecimalPalce);
              XMLBuilder issim = lg.element("ISSIMPLEUNIT");
              issim.t("Yes");
             
          
          
            
            
            
            Properties outputProperties = new Properties();
            // Explicitly identify the output as an XML document
            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
            // Pretty-print the XML output (doesn't work in all cases)
            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
            // Get 2-space indenting when using the Apache transformer
            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
            // Omit the XML declaration header
            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
            en.toWriter(writer, outputProperties);
            writer.close();

        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {

     	   final HttpParams httpParams = new BasicHttpParams();
 		    HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
 			HttpClient httpclient = new DefaultHttpClient(httpParams);
   
   
            HttpPost httppost = new HttpPost(TallyServer);
            File file = new File(iNfileName);
            FileEntity entity = new FileEntity(file,
                    "text/plain; charset=\"UTF-8\"");

            httppost.setEntity(entity);
            System.out.println("executing request "
                    + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            if (resEntity != null) {
                System.out.println("Response content length: "
                        + resEntity.getContentLength());
                System.out.println(resEntity.toString());
                System.out.println("Chunked?: "
                        + resEntity.isChunked());
            }
            /*
             * delete the file
            
  */

           if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }
            
           
           
        } catch (Exception est) {
            System.out.println(est);
            return "failed";
        }
        return "success";
    }
    
    public String CreateVoucherTypeXML(String VoucherName, String ParentName,
 		   String NumberingMethod, String PreventDuplicate,
            String TallyServer,   String CompanyName
        ) { 
 	  
        String iNfileName =   "vouchertype.xml";
        try {
     	   
     	   
     	    

        	  XMLBuilder en = XMLBuilder.create("ENVELOPE");
              XMLBuilder Hd = en.element("HEADER");
              //XMLBuilder vr = Hd.element("VERSION");

  
              //vr.t("1");
              XMLBuilder TRq = Hd.element("TALLYREQUEST");
              TRq.t("import Data");
             
           
              XMLBuilder bd = en.element("BODY");
              XMLBuilder de = bd.element("IMPORTDATA");
              XMLBuilder req = de.element("REQUESTDESC");
              XMLBuilder rep = req.element("REPORTNAME");
              
              rep.t("All Masters");  //All Masters Units
              
              XMLBuilder stv = req.element("STATICVARIABLES");
              XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
              cc.t(CompanyName);
              XMLBuilder idu = stv.element("IMPORTDUPS");
              idu.t("@@DUPCOMBINE");

              XMLBuilder dt = bd.element("DATA");
              XMLBuilder tm = dt.element("TALLYMESSAGE");
              //****Start from Here ********//
              
    
              XMLBuilder lg = tm.element("VOUCHERTYPE");
            
              lg.a("NAME", VoucherName);
              lg.a("RESERVEDNAME", "");
              XMLBuilder unm = lg.element("PARENT");
              
              unm.t(ParentName);
              XMLBuilder nm = lg.element("MAILINGNAME");
              nm.t(ParentName);
              XMLBuilder pt = lg.element("NUMBERINGMETHOD");
              pt.t(NumberingMethod);
              XMLBuilder issim = lg.element("ISDEEMEDPOSITIVE");
              issim.t("Yes");
              XMLBuilder prevdup = lg.element("PREVENTDUPLICATES");
              prevdup.t(PreventDuplicate);
              
             
              XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
              XMLBuilder langnmlst = langlst.element("NAME.LIST");
               langlst.a("TYPE","String");
               XMLBuilder lname = langnmlst.element("NAME");
               lname.t(VoucherName);
               //XMLBuilder alname = langnmlst.element("NAME");
               //alname.t(AccountName);
          
            
            
            
            Properties outputProperties = new Properties();
            // Explicitly identify the output as an XML document
            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
            // Pretty-print the XML output (doesn't work in all cases)
            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
            // Get 2-space indenting when using the Apache transformer
            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
            // Omit the XML declaration header
            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
            en.toWriter(writer, outputProperties);
            writer.close();

        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {
     	   
     	   final HttpParams httpParams = new BasicHttpParams();
 		    HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
 			HttpClient httpclient = new DefaultHttpClient(httpParams);
   
   
            HttpPost httppost = new HttpPost(TallyServer);
            File file = new File(iNfileName);
            FileEntity entity = new FileEntity(file,
                    "text/plain; charset=\"UTF-8\"");

            httppost.setEntity(entity);
            System.out.println("executing request "
                    + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            if (resEntity != null) {
                System.out.println("Response content length: "
                        + resEntity.getContentLength());
                System.out.println(resEntity.toString());
                System.out.println("Chunked?: "
                        + resEntity.isChunked());
            }
            /*
             * delete the file
            */


            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }
 

        } catch (Exception est) {
            System.out.println(est);
            return "failed";
        }
        return "success";
    }

    public String CreateCostCenterXML(String goDownName, 
            String TallyServer,  String CompanyName 
            ) { 
 	 
        String iNfileName = goDownName+"CC.xml";
        try {
     	   
     	   System.out.println("Company "+CompanyName );
     	   
     	   System.out.println("cost center  "+goDownName );
     	   

        	  XMLBuilder en = XMLBuilder.create("ENVELOPE");
              XMLBuilder Hd = en.element("HEADER");
              //XMLBuilder vr = Hd.element("VERSION");

  
              //vr.t("1");
              XMLBuilder TRq = Hd.element("TALLYREQUEST");
              TRq.t("import Data");
             
           
              XMLBuilder bd = en.element("BODY");
              XMLBuilder de = bd.element("IMPORTDATA");
              XMLBuilder req = de.element("REQUESTDESC");
              XMLBuilder rep = req.element("REPORTNAME");
              
              rep.t("All Masters");  //All Masters Units
              
              XMLBuilder stv = req.element("STATICVARIABLES");
              XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
              cc.t(CompanyName);
              XMLBuilder idu = stv.element("IMPORTDUPS");
              idu.t("@@DUPCOMBINE");

              XMLBuilder dt = bd.element("DATA");
              XMLBuilder tm = dt.element("TALLYMESSAGE");
              //****Start from Here ********//
              
    
              XMLBuilder lg = tm.element("COSTCENTRE");
            
              lg.a("NAME", goDownName);
              lg.a("RESERVEDNAME", "");
              
              XMLBuilder cg = tm.element("CATEGORY");
              cg.t("Primary Cost Category");
               
             
              XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
              XMLBuilder langnmlst = langlst.element("NAME.LIST");
               langlst.a("TYPE","String");
               XMLBuilder lname = langnmlst.element("NAME");
               lname.t(goDownName);
               //XMLBuilder alname = langnmlst.element("NAME");
               //alname.t(AccountName);
          
            
            
            
            Properties outputProperties = new Properties();
            // Explicitly identify the output as an XML document
            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
            // Pretty-print the XML output (doesn't work in all cases)
            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
            // Get 2-space indenting when using the Apache transformer
            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
            // Omit the XML declaration header
            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
            en.toWriter(writer, outputProperties);
            writer.close();

        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {
     	   
     	   final HttpParams httpParams = new BasicHttpParams();
 		    HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
 			HttpClient httpclient = new DefaultHttpClient(httpParams);
    
     	   
    
            HttpPost httppost = new HttpPost(TallyServer);
            File file = new File(iNfileName);
            FileEntity entity = new FileEntity(file,
                    "text/plain; charset=\"UTF-8\"");

            httppost.setEntity(entity);
            System.out.println("executing request "
                    + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            if (resEntity != null) {
                System.out.println("Response content length: "
                        + resEntity.getContentLength());
                System.out.println(resEntity.toString());
                System.out.println("Chunked?: "
                        + resEntity.isChunked());
            }
            /*
             * delete the file
            
*/

            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }
  

        } catch (Exception est) {
            System.out.println(est);
            return "failed";
        }
        return "success";
    }
    
    public String CreateGoDownXML(String goDownName, 
            String TallyServer, int instId , String CompanyName ) { 
 	  
        String iNfileName ="TallyInter.xml";
        try {
     	   
     	   
     	    

        	  XMLBuilder en = XMLBuilder.create("ENVELOPE");
              XMLBuilder Hd = en.element("HEADER");
              //XMLBuilder vr = Hd.element("VERSION");

  
              //vr.t("1");
              XMLBuilder TRq = Hd.element("TALLYREQUEST");
              TRq.t("import Data");
             
           
              XMLBuilder bd = en.element("BODY");
              XMLBuilder de = bd.element("IMPORTDATA");
              XMLBuilder req = de.element("REQUESTDESC");
              XMLBuilder rep = req.element("REPORTNAME");
              
              rep.t("All Masters");  //All Masters Units
              
              XMLBuilder stv = req.element("STATICVARIABLES");
              XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
              cc.t(CompanyName);
              XMLBuilder idu = stv.element("IMPORTDUPS");
              idu.t("@@DUPCOMBINE");

              XMLBuilder dt = bd.element("DATA");
              XMLBuilder tm = dt.element("TALLYMESSAGE");
              //****Start from Here ********//
              
    
              XMLBuilder lg = tm.element("GODOWN");
            
              lg.a("NAME", goDownName);
              lg.a("RESERVEDNAME", "");
              
             
              XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
              XMLBuilder langnmlst = langlst.element("NAME.LIST");
               langlst.a("TYPE","String");
               XMLBuilder lname = langnmlst.element("NAME");
               lname.t(goDownName);
               //XMLBuilder alname = langnmlst.element("NAME");
               //alname.t(AccountName);
          
            
            
            
            Properties outputProperties = new Properties();
            // Explicitly identify the output as an XML document
            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
            // Pretty-print the XML output (doesn't work in all cases)
            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
            // Get 2-space indenting when using the Apache transformer
            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
            // Omit the XML declaration header
            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
            en.toWriter(writer, outputProperties);
            writer.close();

        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {
            HttpClient httpclient = new DefaultHttpClient();

            HttpPost httppost = new HttpPost(TallyServer);
            File file = new File(iNfileName);
            FileEntity entity = new FileEntity(file,
                    "text/plain; charset=\"UTF-8\"");

            httppost.setEntity(entity);
            System.out.println("executing request "
                    + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            if (resEntity != null) {
                System.out.println("Response content length: "
                        + resEntity.getContentLength());
                System.out.println(resEntity.toString());
                System.out.println("Chunked?: "
                        + resEntity.isChunked());
            }
            /*
             * delete the file
            */


            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }
  

        } catch (Exception est) {
            System.out.println(est);
            return "failed";
        }
        return "success";
    }

    
 
    public String CreateSupplier(String AcName, String TinNo, String Address, String Alias, String ParentAccountHead,
            String TallyServer,    String CompanyName,
               String affectstock,
            String PinCode,String country, String dealerType, String state) { 
    	
     
    	  String outFile  = "";
        
        String iNfileName = "Supplier.xml";
        try {

        	  XMLBuilder en = XMLBuilder.create("ENVELOPE");
              XMLBuilder Hd = en.element("HEADER");
              XMLBuilder vr = Hd.element("VERSION");
              vr.t("1");
              XMLBuilder TRq = Hd.element("TALLYREQUEST");
              TRq.t("import");
              XMLBuilder tp = Hd.element("TYPE");
              tp.t("Data");
              XMLBuilder id = Hd.element("ID");
              id.t("All Masters");
              XMLBuilder bd = en.element("BODY");
              XMLBuilder de = bd.element("DESC");
              XMLBuilder stv = de.element("STATICVARIABLES");
              XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
              cc.t(CompanyName);
              XMLBuilder idu = stv.element("IMPORTDUPS");
              idu.t("@@DUPCOMBINE");

              XMLBuilder dt = bd.element("DATA");
              XMLBuilder tm = dt.element("TALLYMESSAGE");
              XMLBuilder lg = tm.element("LEDGER");
              lg.a("Action", "Create");
              lg.a("NAME", AcName);
              XMLBuilder nm = lg.element("NAME");
              nm.t(AcName);
              XMLBuilder pt = lg.element("PARENT");
              pt.t(ParentAccountHead);
              XMLBuilder adlst = lg.element("ADDRESS.LIST");
              adlst.a("TYPE", "String");
              if(null!=Address){
            	  XMLBuilder adr = adlst.element("ADDRESS");
            	  adr.t(Address);
              }
              
              
              XMLBuilder COUNTRYOFRESIDENCE = lg.element("COUNTRYOFRESIDENCE");
              COUNTRYOFRESIDENCE.t("India");
              
              XMLBuilder COUNTRYNAME = lg.element("COUNTRYNAME");
              COUNTRYNAME.t("India");
              
              
              XMLBuilder GSTREGISTRATIONTYPE = lg.element("GSTREGISTRATIONTYPE");
              
              if(null!=TinNo && TinNo.length() > 7){
              GSTREGISTRATIONTYPE.t("Regular");
              }else{
            	  GSTREGISTRATIONTYPE.t("Unregistered");
              }
              
              
              XMLBuilder PARTYGSTIN = lg.element("PARTYGSTIN");
              if(null==TinNo) {
            	  TinNo="";
              }
              PARTYGSTIN.t(TinNo);
              
              
         XMLBuilder LEDSTATENAME = lg.element("LEDSTATENAME");
              
         LEDSTATENAME.t("Kerala");
              
         
              
              
              XMLBuilder mailname = adlst.element("MAILINGNAME.LIST");
              mailname.a("TYPE", "String");
              XMLBuilder maname = adlst.element("MAILINGNAME");
              maname.t(AcName);
              
              if(null!=TinNo){
            	  XMLBuilder intax = lg.element("VATTINNUMBER");
            	  intax.t(TinNo);
              }
              
              
              
              if(null!=PinCode){
            	  XMLBuilder intax = lg.element("PINCODE");
            	  intax.t(PinCode);
              }
              if(null!=country){
            	  XMLBuilder intax = lg.element("COUNTRYNAME");
            	  intax.t(country);
              }
              if(null!=dealerType){
            	  XMLBuilder intax = lg.element("VATDEALERTYPE");
            	  intax.t(dealerType);
              }
              
              if(null!=state){
            	  XMLBuilder intax = lg.element("LEDSTATENAME");
            	  intax.t(state);
              }
              XMLBuilder afstk = lg.element("AFFECTSSTOCK");
              afstk.t(affectstock);
              
              XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
              XMLBuilder langnmlst = lg.element("NAME.LIST");
              langlst.a("TYPE","String");
              XMLBuilder lname = langnmlst.element("NAME");
              lname.t(AcName);
              if(null!=Alias){
            	  XMLBuilder alname = langnmlst.element("NAME");
            	  alname.t(Alias);
              }
             
      	 
          
            
            
            
            Properties outputProperties = new Properties();
            // Explicitly identify the output as an XML document
            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
            // Pretty-print the XML output (doesn't work in all cases)
            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
            // Get 2-space indenting when using the Apache transformer
            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
            // Omit the XML declaration header
            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
            en.toWriter(writer, outputProperties);
            writer.close();
            
      /*
         	String search = "<COUNTRYNAME>India</COUNTRYNAME>";  // <- changed to work with String.replaceAll()
        	String replacement = "<COUNTRYNAME>&#4;India</COUNTRYNAME>";
      
        	String  outFile1 = "xout1.xml";
            setAmbersandForGST(iNfileName,search,replacement,outFile1);
            

            
        	  search = "<LEDSTATENAME>Kerala</LEDSTATENAME>";  // <- changed to work with String.replaceAll()
        	  replacement = "<LEDSTATENAME>&#4;Kerala</LEDSTATENAME>";
    
        	    outFile = "xout2.xml";
        
            setAmbersandForGST(outFile1,search,replacement,outFile);
            */
            
            
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {
        	 log.info(" LedgerXML - httpclient");
       	   final HttpParams httpParams = new BasicHttpParams();
  		    HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
  			HttpClient httpclient = new DefaultHttpClient(httpParams);
    
  			
  			

            HttpPost httppost = new HttpPost(TallyServer);
           // File file = new File(iNfileName);
            
            File file = new File(iNfileName);
            
            
            FileEntity entity = new FileEntity(file,
                    "text/plain; charset=\"UTF-8\"");

            httppost.setEntity(entity);
            System.out.println("executing request "
                    + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            if (resEntity != null) {
                System.out.println("Response content length: "
                        + resEntity.getContentLength());
                System.out.println(resEntity.toString());
                System.out.println("Chunked?: "
                        + resEntity.isChunked());
            }
            /*
             * delete the file
             */


            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }


        } catch (Exception est) {
            System.out.println(est);
            return "failed";
        }
        return "success";
    }
    
    
    
    public String CreateCustomer(String AcName, String TinNo, 
    		String Address, String Alias, String ParentAccountHead,
            String TallyServer, int instId , CompanyMst companyMst,
            Connection Conn, String XML_PATH, String affectstock,
            String PinCode,String country, String dealerType, String state, String TALLY_COMPANY) { 
    	
    	  String outFile  = "";
        
        String iNfileName = "Supplier.xml";
        try {

        	  XMLBuilder en = XMLBuilder.create("ENVELOPE");
              XMLBuilder Hd = en.element("HEADER");
              XMLBuilder vr = Hd.element("VERSION");
              vr.t("1");
              XMLBuilder TRq = Hd.element("TALLYREQUEST");
              TRq.t("import");
              XMLBuilder tp = Hd.element("TYPE");
              tp.t("Data");
              XMLBuilder id = Hd.element("ID");
              id.t("All Masters");
              XMLBuilder bd = en.element("BODY");
              XMLBuilder de = bd.element("DESC");
              XMLBuilder stv = de.element("STATICVARIABLES");
              XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
              cc.t(companyMst.getCompanyName());
              XMLBuilder idu = stv.element("IMPORTDUPS");
              idu.t("@@DUPCOMBINE");

              XMLBuilder dt = bd.element("DATA");
              XMLBuilder tm = dt.element("TALLYMESSAGE");
              XMLBuilder lg = tm.element("LEDGER");
              lg.a("Action", "Create");
              lg.a("NAME", AcName);
              XMLBuilder nm = lg.element("NAME");
              nm.t(AcName);
              XMLBuilder pt = lg.element("PARENT");
              pt.t(ParentAccountHead);
              XMLBuilder adlst = lg.element("ADDRESS.LIST");
              adlst.a("TYPE", "String");
              if(null!=Address){
            	  XMLBuilder adr = adlst.element("ADDRESS");
            	  adr.t(Address);
              }
              
              
              XMLBuilder COUNTRYOFRESIDENCE = lg.element("COUNTRYOFRESIDENCE");
              COUNTRYOFRESIDENCE.t("India");
              
              XMLBuilder COUNTRYNAME = lg.element("COUNTRYNAME");
              
              COUNTRYNAME.t(country);
             
              
              
              
              XMLBuilder GSTREGISTRATIONTYPE = lg.element("GSTREGISTRATIONTYPE");
              
              if(null!=TinNo && TinNo.length() > 0){
            	  	GSTREGISTRATIONTYPE.t("Regular");
              }else{
            	  	GSTREGISTRATIONTYPE.t("Consumer");
            	  	TinNo="";
              }
              
              
              XMLBuilder PARTYGSTIN = lg.element("PARTYGSTIN");
              
              PARTYGSTIN.t(TinNo);
              
              
         XMLBuilder LEDSTATENAME = lg.element("LEDSTATENAME");
              
         LEDSTATENAME.t(state);
              
         
              
              
              XMLBuilder mailname = adlst.element("MAILINGNAME.LIST");
              mailname.a("TYPE", "String");
              XMLBuilder maname = adlst.element("MAILINGNAME");
              maname.t(AcName);
              
              if(null!=TinNo){
            	  XMLBuilder intax = lg.element("VATTINNUMBER");
            	  intax.t(TinNo);
              }
              
              
              
              if(null!=PinCode){
            	  XMLBuilder intax = lg.element("PINCODE");
            	  intax.t(PinCode);
              }
              if(null!=country){
            	  XMLBuilder intax = lg.element("COUNTRYNAME");
            	  intax.t(country);
              }
              if(null!=dealerType){
            	  XMLBuilder intax = lg.element("VATDEALERTYPE");
            	  intax.t(dealerType);
              }
              
              if(null!=state){
            	  XMLBuilder intax = lg.element("LEDSTATENAME");
            	  intax.t(state);
              }
              XMLBuilder afstk = lg.element("AFFECTSSTOCK");
              afstk.t(affectstock);
              
              XMLBuilder langlst = lg.element("LANGUAGENAME.LIST");
              XMLBuilder langnmlst = lg.element("NAME.LIST");
              langlst.a("TYPE","String");
              XMLBuilder lname = langnmlst.element("NAME");
              lname.t(AcName);
              if(null!=Alias){
            	  XMLBuilder alname = langnmlst.element("NAME");
            	  alname.t(Alias);
              }
             
      	 
          
            
            
            
            Properties outputProperties = new Properties();
            // Explicitly identify the output as an XML document
            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
            // Pretty-print the XML output (doesn't work in all cases)
            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
            // Get 2-space indenting when using the Apache transformer
            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
            // Omit the XML declaration header
            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
            en.toWriter(writer, outputProperties);
            writer.close();
            
      /*
         	String search = "<COUNTRYNAME>India</COUNTRYNAME>";  // <- changed to work with String.replaceAll()
        	String replacement = "<COUNTRYNAME>&#4;India</COUNTRYNAME>";
      
        	String  outFile1 = "xout1.xml";
            setAmbersandForGST(iNfileName,search,replacement,outFile1);
            

            
        	  search = "<LEDSTATENAME>Kerala</LEDSTATENAME>";  // <- changed to work with String.replaceAll()
        	  replacement = "<LEDSTATENAME>&#4;Kerala</LEDSTATENAME>";
    
        	    outFile = "xout2.xml";
        
            setAmbersandForGST(outFile1,search,replacement,outFile);
            */
            
            
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        try {
        	 log.info(" LedgerXML - httpclient");
       	   final HttpParams httpParams = new BasicHttpParams();
  		    HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
  			HttpClient httpclient = new DefaultHttpClient(httpParams);
    
  			
  			

            HttpPost httppost = new HttpPost(TallyServer);
           // File file = new File(iNfileName);
            
            File file = new File(iNfileName);
            
            
            FileEntity entity = new FileEntity(file,
                    "text/plain; charset=\"UTF-8\"");

            httppost.setEntity(entity);
            System.out.println("executing request "
                    + httppost.getRequestLine());
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            if (resEntity != null) {
                System.out.println("Response content length: "
                        + resEntity.getContentLength());
                System.out.println(resEntity.toString());
                System.out.println("Chunked?: "
                        + resEntity.isChunked());
            }
            String VoucherId = "";
            String ErrorCode = "";
            String Linerror = "";
            
            if (resEntity != null) {
				try {
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(resEntity.getContent());

					NodeList nList = doc.getElementsByTagName("RESPONSE");
					for (int temp = 0; temp < nList.getLength(); temp++) {
						Node nNode = nList.item(temp);

						if (nNode.getNodeType() == Node.ELEMENT_NODE) {
							Element eElement = (Element) nNode;
							VoucherId = eElement.getElementsByTagName("LASTVCHID").item(0).getTextContent();
							System.out.println("Response VoucherId =  " + VoucherId);

							ErrorCode  = eElement.getElementsByTagName("ERRORS").item(0)
									.getTextContent();

							if(ErrorCode.equalsIgnoreCase("0")){
								
								
								 
								
									return "OK";
								
							}
							System.out.println("ErrorCode =  " + ErrorCode);

							Linerror = eElement.getElementsByTagName("LINEERROR").item(0).getTextContent();
							System.out.println("Linerror =  " + Linerror);

							if (Linerror.contains("Voucher Number") && Linerror.contains("already exists!")) {
								 
								System.out.println(Linerror);
								return "OK";
								
							}
							processReturnStatus(Linerror,companyMst,TallyServer,TALLY_COMPANY,"Customer");
						
							if(ErrorCode.equalsIgnoreCase("1")){
								
								return "OK";
							}

						}

					}

					System.out.println("Response content length: " + resEntity.getContentLength());
					System.out.println(resEntity.toString());
					System.out.println("Chunked?: " + resEntity.isChunked());

				} catch (Exception e2) {
					throw new SQLException(e2.toString());

				}
			}
            /*
             * delete the file
             */


            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }


        } catch (Exception est) {
            System.out.println(est);
            return "failed";
        }
        return "success";
    }
    private void setAmbersandForGST(String fileName, String search,
    		String replacement	, String outFile) throws FileNotFoundException{
    	
    	
    	try{
    	    PrintWriter writer = new PrintWriter(outFile, "UTF-8");
      	    
    	File log= new File(fileName);
     	 
    	
    	
    	String search2 = "<STATENAME>Any</STATENAME>";
    	String replace2 = "<STATENAME>&#4; Any</STATENAME>";
    	
    	
    	String search3 = "<GSTAPPLICABLE>Applicable</GSTAPPLICABLE>";  // <- changed to work with String.replaceAll()
    	String replacemen3 = "<GSTAPPLICABLE>&#4; Applicable</GSTAPPLICABLE>";

    	
    	
    	FileReader fr = new FileReader(log);
    	String s;
    	 
    	    BufferedReader br = new BufferedReader(fr);

    	    while ((s = br.readLine()) != null) {
    	        s = s.replaceAll(search2, replace2);
    	        s = s.replaceAll(search3, replacemen3);
    	        writer.print(s);
    	    }
    	    writer.close();
    	}catch(Exception e){
    		
    	}
    }
    
	void processReturnStatus(String Linerror, CompanyMst companyMst, String TallyServer,
			String TALLY_COMPANY, String voucherType) throws SQLException{
		
		
		if (Linerror.contains("Group") && Linerror.contains("does not exist!")) {
			String GroupName = Linerror.replace("Group", "").replace("does not exist!", "")
					.replace("'", "").trim();

			System.out.println("missingLedger =  " + GroupName);
			
			String ParentAccountHead =getParentAccount(GroupName) ;
			
			 
			
		 
			 String XML_PATH = "";
			 
			 LedgerGroupXML(  GroupName, ParentAccountHead,
			              TallyServer,    TALLY_COMPANY , true
			            ) ;
			 
			 
			 
			

			throw new SQLException("TRYAGAIN");
		}
		
		if (Linerror.contains("Cost Centre") && Linerror.contains("does not exist!")) {
			String goDownName = Linerror.replace("Cost Centre", "").replace("does not exist!", "")
					.replace("'", "").trim();

			System.out.println("missingLedger =  " + goDownName);
			
			
		 
			
			 
			 	 
			 String XML_PATH = "";
			 
			 CreateCostCenterXML(  goDownName, 
			              TallyServer,     TALLY_COMPANY 
			            ) ;
			 
			 
			

			throw new SQLException("TRYAGAIN");
		}

		
		if (Linerror.contains("Voucher totals do not match!")) {
			 

			throw new SQLException("Error"  + Linerror);
		}
		
		if (Linerror.contains("No Accounting Allocations for")) {
			 

			throw new SQLException("Error"  + Linerror);
		}
		
		if (Linerror.contains("Ledger") && Linerror.contains("does not exist!")) {
			String missingLedger = Linerror.replace("Ledger", "").replace("does not exist!", "")
					.replace("'", "").trim();

			System.out.println("missingLedger =  " + missingLedger);
			
			
			String parentAccount = getParentAccount(missingLedger) ;
			 
			String affectstock = "YES";
			
			if(null!=parentAccount) {
				if(parentAccount.equalsIgnoreCase("0")){
					if(missingLedger.contains("Sales@")){
						parentAccount = "Sales Accounts";
						missingLedger= missingLedger+"%";
						
					}else if (missingLedger.contains("Purchase@")){
						parentAccount = "Purchase Accounts";
						missingLedger = missingLedger + "%";
					}else if (missingLedger.contains("Output VAT@")){
						parentAccount = "Duties & Taxes";
						missingLedger = missingLedger + "%";
						affectstock ="YES";
					}else  if (missingLedger.contains("_CASH")){
						parentAccount = "Cash-in-hand";
						affectstock ="NO";
					}
				}
			}else if (missingLedger.contains("-CASH")){
				parentAccount = "Cash-in-hand";
				affectstock ="NO";
			}else {
				 
				affectstock ="YES";
			}
	 
		
			AccountHeads CusOpt = accountHeadsRepository.findByAccountNameAndCompanyMstId(missingLedger,
					companyMst.getId());
		
			
		   if(null!=CusOpt) {
			   AccountHeads customerMst = CusOpt;
				parentAccount = getParentAccount(missingLedger) ;


				if(null==parentAccount || parentAccount.equalsIgnoreCase("")){
					parentAccount = "Sundry Debtors";
					affectstock = "NO";
				}
						 CreateSupplier(missingLedger,
								 customerMst.getPartyGst(), 
								 customerMst.getPartyAddress1(),customerMst.getId(), parentAccount,
					            TallyServer,   TALLY_COMPANY,
					             affectstock,
					             customerMst.getCustomerPin(),
					             customerMst.getCustomerCountry(), 
					             customerMst.getCustomerGstType(),
					             customerMst.getCustomerState());
						 
					            
					 
					
				
			}else{
				
				String OrgParent = parentAccount;
				while (true){
					
					String tempParent= parentAccount;
					
			
				 
				if(missingLedger.equalsIgnoreCase("SALES ACCOUNT")) {
					affectstock ="YES";
					parentAccount ="Sales Accounts"; 
					OrgParent ="Sales Accounts";
					

					 LedgerXML(missingLedger,"", "", "", OrgParent,
					            TallyServer,    companyMst ,
					         affectstock,TALLY_COMPANY);

					 
					 break;
					
					
				}else if(missingLedger.equalsIgnoreCase("SGST ACCOUNT")) {
					OrgParent ="Duties & Taxes"; 
					parentAccount = "Duties & Taxes"; 
					
					
					

					 LedgerGroupXML(missingLedger,   parentAccount,
					              TallyServer,      TALLY_COMPANY, 
					             true) ;
					 
					 break;
					
					
				} else if(missingLedger.equalsIgnoreCase("CGST ACCOUNT")) {
					OrgParent ="Duties & Taxes"; 
					parentAccount = "Duties & Taxes"; 
					
					
					

					 LedgerGroupXML(missingLedger,   parentAccount,
					              TallyServer,     TALLY_COMPANY, 
					             true) ;
					 
					 break;
					 
				} else if(missingLedger.equalsIgnoreCase("KERALA FLOOD CESS")) {
					OrgParent ="Duties & Taxes"; 
					parentAccount = "Duties & Taxes"; 
					
					

					 LedgerGroupXML(missingLedger,   parentAccount,
					              TallyServer,     TALLY_COMPANY, 
					             true) ;
					 break;
					 
					 
				} else if(missingLedger.contains("-CASH")) {
					OrgParent ="Cash-in-hand"; 
					parentAccount = "Cash-in-hand"; 
					
					

					 LedgerGroupXML(missingLedger,   parentAccount,
					              TallyServer,      TALLY_COMPANY, 
					             true) ;
					 
					 break;
					 
					 
				} else if(parentAccount.equals("Liabilities")){
					break;
				} else if(parentAccount.equals("Duties & Taxes")){
					 
					 
					break;
				} else if(parentAccount.equals("Sales Accounts")){
					break;
				} else if(parentAccount.equals("Assets")){
					break;
				} else if(parentAccount.equals("Income")){
					break;
				}else if(parentAccount.equals("Expenses")){
					break;
				}else if(parentAccount.equals("Cash-in-hand")){
					break;
				}else {
					parentAccount = getParentAccount(parentAccount)  ;
					if(null==parentAccount) {
						parentAccount="Assets";
					}
				}
				
				if(null!=tempParent && !tempParent.equalsIgnoreCase(parentAccount)) {
				
				 LedgerGroupXML(tempParent,   parentAccount,
				              TallyServer,      TALLY_COMPANY, 
				             true) ;
				 break;
				}
				 
				
				}
				
				affectstock ="YES";
			
				 LedgerXML(missingLedger,"", "", "", OrgParent,
				            TallyServer,    companyMst ,
				         affectstock,TALLY_COMPANY);
				 
				
			
			}
			throw new SQLException("TRYAGAIN");
		}

		
		if (Linerror.contains("Voucher Type") && Linerror.contains("does not exist!")) {
			String missingLedger = Linerror.replace("Voucher Type", "").replace("does not exist!", "")
					.replace("'", "").trim();

			System.out.println("missingLedger =  " + missingLedger);
			
			
			String parentAccount = voucherType;
			
			 
			
		 	 
			 String XML_PATH = "";
			 
			String rtnValue =  CreateVoucherTypeXML( missingLedger,  parentAccount,
			 		     "Manual",  "Yes",
			             TallyServer,    TALLY_COMPANY
			              );
			 
			if(rtnValue.equalsIgnoreCase("success")) {
				return ;
			}
			
			

			throw new SQLException("TRYAGAIN");
		}

		
		if (Linerror.contains("Stock Item") && Linerror.contains("does not exist!")) {
			String missingStockItem = Linerror.replace("Stock Item", "")
					.replace("does not exist!", "").replace("'", "").trim();

			System.out.println("missingLedger =  " + missingStockItem);
			
			ItemMst itemMst = itemMstRepository.findByItemName(missingStockItem);
			UnitMst unitMst = unitMstRepository.findById(itemMst.getUnitId()).get();
			
			
			String affectstock ="YES";
			CategoryMst categoryMst = null;
			
			if(null==itemMst.getCategoryId()) {
				categoryMst =categoryMstRepository.findSearchAll().get(0);
			}else {
			  categoryMst = categoryMstRepository.findById(itemMst.getCategoryId()).get();
			}
					
		 
			
	  
			
			  CreateUnitXML(  unitMst.getUnitName(), "2",  unitMst.getUnitDescription(),
		              TallyServer,     TALLY_COMPANY
		                ) ;
	
			
			
			 NewStockGroup(categoryMst.getCategoryName(),  TallyServer,    
					 unitMst.getUnitName(),    itemMst.getBarCode(),  
					 TALLY_COMPANY,  affectstock);
			
			 		

				String GSTDTLAPPLICABLEFROM = "2020-04-01";

				GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("-", "");
				GSTDTLAPPLICABLEFROM = GSTDTLAPPLICABLEFROM.replaceAll("/", "");

				
				
			 
				NewItemVerson5( missingStockItem,   TallyServer,   itemMst.getTaxRate(),
					 unitMst.getUnitName(),   itemMst.getBarCode(),
					 TALLY_COMPANY,affectstock,
					categoryMst.getCategoryName(),
					itemMst.getHsnCode(),GSTDTLAPPLICABLEFROM);
					
				//String orginalname, String gstUnitName, String category
				
			/*	String  gstUnitName = "NOS";
				
				if(unitMst.getUnitName().equalsIgnoreCase("KGS")) {
					gstUnitName="KGS-KILOGRAMS";
				}else if (unitMst.getUnitName().equalsIgnoreCase("NOS")) {
					gstUnitName="NOS-NUMBERS";
				}
				
			 
			 NewItem_version6(missingStockItem,   TallyServer,   itemMst.getTaxRate(),
					 unitMst.getUnitName(),   itemMst.getBarCode(),
					companyMst.getCompanyName(),affectstock,
					categoryMst.getCategoryName(),
					itemMst.getHsnCode(),GSTDTLAPPLICABLEFROM , unitMst.getUnitDescription(),
					gstUnitName, "MISC", 1);
			 
			 */
			 
		 
	    		
											
			throw new SQLException("TRYAGAIN");
		}
		
		if (Linerror.contains("Voucher Type") && Linerror.contains("does not exist!")) {
			String missingVoucherType = Linerror.replace("Voucher Type", "")
					.replace("does not exist!", "").replace("'", "").trim();

			System.out.println("Missing Voucher Type =  " + missingVoucherType);
			throw new SQLException("TRYAGAIN");
		}
	}
	   public void LedgerGroupXML(String Group, String ParentAccountHead,
	            String TallyServer,    String CmpNmae, 
	             boolean DeleteXML) {

	    

	        /*
	         * Var declaration
	         */
	        String iNfileName =  "ledgergroup.xml";


	        DateFormat formatter;
	        formatter = new SimpleDateFormat("yyyyMMdd");

	        try {

	            XMLBuilder en = XMLBuilder.create("ENVELOPE");
	            XMLBuilder Hd = en.element("HEADER");
	            XMLBuilder vr = Hd.element("VERSION");
	            vr.t("1");
	            XMLBuilder TRq = Hd.element("TALLYREQUEST");
	            TRq.t("import");
	            XMLBuilder tp = Hd.element("TYPE");
	            tp.t("Data");
	            XMLBuilder id = Hd.element("ID");
	            id.t("All Masters");
	            XMLBuilder bd = en.element("BODY");
	            XMLBuilder de = bd.element("DESC");
	            XMLBuilder stv = de.element("STATICVARIABLES");
	            
	            XMLBuilder cc = stv.element("SVCURRENTCOMPANY");
	            cc.t(CmpNmae);
	            
	            
	            XMLBuilder idu = stv.element("IMPORTDUPS");
	            idu.t("@@DUPCOMBINE");
	            XMLBuilder dt = bd.element("DATA");
	            XMLBuilder tm = dt.element("TALLYMESSAGE");
	            /*
	             * Create Group
	             */
	            XMLBuilder lg = tm.element("GROUP");
	            lg.a("Action", "Create");
	            lg.a("NAME", Group);
	            XMLBuilder subleger = lg.element("ISSUBLEDGER");
	            subleger.t("No");
	            XMLBuilder nm = lg.element("NAME");
	            nm.t(Group);
	            XMLBuilder pt = lg.element("PARENT");
	            pt.t(ParentAccountHead);

	            /*
	             * Code below will create Alias for the account
	             */
	            XMLBuilder languageNameList = lg.element("LANGUAGENAME.LIST");
	            XMLBuilder Nlist = languageNameList.element("NAME.LIST");
	            Nlist.a("TYPE", "String");
	            XMLBuilder Nme = Nlist.element("NAME");
	            Nme.t(Group);
	          //  XMLBuilder Nme2 = Nlist.element("NAME");
	           // Nme2.t(Alias);



	            Properties outputProperties = new Properties();
	// Explicitly identify the output as an XML document
	            outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
	// Pretty-print the XML output (doesn't work in all cases)
	            outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
	// Get 2-space indenting when using the Apache transformer
	            outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
	// Omit the XML declaration header
	            outputProperties.put(javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");

	            PrintWriter writer = new PrintWriter(new FileOutputStream(iNfileName));
	            en.toWriter(writer, outputProperties);

	            writer.close();
	        } catch (Exception e) {
	            System.out.println(e.toString());
	        }

	        try {
	            HttpClient httpclient = new DefaultHttpClient() {
	            };
	            HttpPost httppost = new HttpPost(TallyServer);


	//For Direct XML Test
	// File file = new File("c:\\projects10.xml");
	            File file = new File(iNfileName);
	            FileEntity entity = new FileEntity(file,
	                    "text/plain; charset=\"UTF-8\"");

	            httppost.setEntity(entity);

	            System.out.println("executing request "
	                    + httppost.getRequestLine());
	            HttpResponse response = httpclient.execute(httppost);
	            HttpEntity resEntity = response.getEntity();
	            System.out.println("----------------------------------------");
	            System.out.println(response.getStatusLine());
	            if (resEntity != null) {
	                System.out.println("Response content length: "
	                        + resEntity.getContentLength());
	                System.out.println(resEntity.toString());
	                System.out.println("Chunked?: "
	                        + resEntity.isChunked());
	            }
	            if(DeleteXML){
		            if (file.delete()) {
		                System.out.println(file.getName() + " is deleted!");
		            } else {
		                System.out.println("Delete operation is failed.");
		            }
	            
	            }
	            

	        } catch (Exception est) {
	            System.out.println(est);
	        }
	    }

	   
	   private String getParentAccount(String accountName) {

		   AccountHeads accountHeads = accountHeadsRepository.findByAccountName(accountName);
		   
		   if(null==accountHeads.getParentId()) {
			   return null;
		   }
		   
		   
		   if(accountHeads.getParentId().equalsIgnoreCase("0")) {
			   return null;
		   }
		   
		   AccountHeads parentAccountHeads = accountHeadsRepository.findById(accountHeads.getParentId()).get();
		   
			return parentAccountHeads.getAccountName();
		}
	   
	   
	   
}
