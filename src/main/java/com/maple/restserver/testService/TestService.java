package com.maple.restserver.testService;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.maple.restserver.entity.IntentDtl;
import com.maple.restserver.entity.IntentHdr;
import com.maple.restserver.entity.IntentInDtl;
import com.maple.restserver.entity.IntentInHdr;
import com.maple.restserver.repository.IntentDtlRepository;
import com.maple.restserver.repository.IntentInDtlRepository;
import com.maple.restserver.repository.IntentInHdrRepository;

public class TestService {

	@Autowired
	IntentInHdrRepository intentInHdrRepository;

	@Autowired
	IntentInDtlRepository intentInDtlRepository;

	@Autowired
	IntentDtlRepository intentDtlRepository;

	public void SaveIntentInDtls(IntentHdr intentHdr) {

		System.out.println("Test class");
//		IntentDtl intentdtsample = msg.get(0);
		IntentInHdr intentInHdr = new IntentInHdr();
		intentInHdr.setBranchCode(intentHdr.getToBranch());
		intentInHdr.setFromBranch(intentHdr.getFromBranch());
		intentInHdr.setIntentStatus("PENDING");
		intentInHdr.setInVoucherNumber(intentHdr.getVoucherNumber());
		intentInHdr.setInVoucherDate(intentHdr.getVoucherDate());
		intentInHdr.setCompanyMst(intentHdr.getCompanyMst());
		intentInHdrRepository.save(intentInHdr);

		List<IntentDtl> msg = intentDtlRepository.findByIntentHdrId(intentInHdr.getId());

		for (IntentDtl intentDtl : msg) {
			IntentInDtl intentInDtl = new IntentInDtl();

			intentInDtl.setIntentInHdr(intentInHdr);
			intentInDtl.setItemId(intentDtl.getItemId());
			intentInDtl.setQty(intentDtl.getQty());
			intentInDtl.setRate(intentDtl.getRate());
			intentInDtl.setUnitId(intentDtl.getUnitId());
			intentInDtl.setStatus("PENDING");
			intentInDtl.setAllocatedQty(0.0);
			intentInDtlRepository.save(intentInDtl);

		}

	}

}
