package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
@Entity
public class ConsumptionDtl implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	@Column(length = 50)
	String itemId;
	@Column(length = 50)
	String unitId;
	Double mrp;
	Double qty;
	Double amount;
	@Column(length = 50)
	String batch;
	@Column(length = 50)
	String reasonId;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "consumptionHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ConsumptionHdr consumptionHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	String store;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public ConsumptionHdr getConsumptionHdr() {
		return consumptionHdr;
	}
	public void setConsumptionHdr(ConsumptionHdr consumptionHdr) {
		this.consumptionHdr = consumptionHdr;
	}
	
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	
	public String getReasonId() {
		return reasonId;
	}
	public void setReasonId(String reasonId) {
		this.reasonId = reasonId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	@Override
	public String toString() {
		return "ConsumptionDtl [id=" + id + ", itemId=" + itemId + ", unitId=" + unitId + ", mrp=" + mrp + ", qty="
				+ qty + ", amount=" + amount + ", batch=" + batch + ", reasonId=" + reasonId + ", consumptionHdr="
				+ consumptionHdr + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", store="
				+ store + "]";
	}
	
	
	
}
