package com.maple.restserver.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
public class ServiceInDtl {
	@Column(length = 50)
		@Id
	    @GeneratedValue(generator = "uuid")
	    @GenericGenerator(name = "uuid", strategy = "uuid2")
	    private String id;
		
		private Double qty;
	
		


		@ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "serviceInHdr", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		private ServiceInHdr serviceInHdr;
		@Column(length = 50)
		private String serialId;
		@Column(length = 50)
		 private String itemName;
		@Column(length = 50)
		 private String model;
		@Column(length = 50)
		 private String productId;
		@Column(length = 50)
		 private String brandId;
		 private String complaints;
		 @Column(length = 50)
		 private String underWarranty;
		 private String observation;
		 @Column(length = 50)
		
		 private String LadiesOrGents;
		 @Column(length = 50)
		 private String strap;
		 @Column(length = 50)
		 private String Battery;
		 @Column(length = 50)
		 private String  processInstanceId;
		 @Column(length = 50)
			private String taskId;

		public String getSerialId() {
			return serialId;
		}

		public void setSerialId(String serialId) {
			this.serialId = serialId;
		}

		public String getModel() {
			return model;
		}

		public void setModel(String model) {
			this.model = model;
		}

		public String getProductId() {
			return productId;
		}

		public void setProductId(String productId) {
			this.productId = productId;
		}

		public String getBrandId() {
			return brandId;
		}

		public void setBrandId(String brandId) {
			this.brandId = brandId;
		}

		public String getComplaints() {
			return complaints;
		}

		public void setComplaints(String complaints) {
			this.complaints = complaints;
		}

		public String getUnderWarranty() {
			return underWarranty;
		}

		public void setUnderWarranty(String underWarranty) {
			this.underWarranty = underWarranty;
		}

		public String getObservation() {
			return observation;
		}

		public void setObservation(String observation) {
			this.observation = observation;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}


	

		public Double getQty() {
			return qty;
		}

		public void setQty(Double qty) {
			this.qty = qty;
		}

		public ServiceInHdr getServiceInHdr() {
			return serviceInHdr;
		}

		public void setServiceInHdr(ServiceInHdr serviceInHdr) {
			this.serviceInHdr = serviceInHdr;
		}

		public String getItemName() {
			return itemName;
		}

		public void setItemName(String itemName) {
			this.itemName = itemName;
		}
		
		

		public String getLadiesOrGents() {
			return LadiesOrGents;
		}

		public void setLadiesOrGents(String ladiesOrGents) {
			LadiesOrGents = ladiesOrGents;
		}

		public String getStrap() {
			return strap;
		}

		public void setStrap(String strap) {
			this.strap = strap;
		}

		public String getBattery() {
			return Battery;
		}

		public void setBattery(String battery) {
			Battery = battery;
		}

		

		public String getProcessInstanceId() {
			return processInstanceId;
		}

		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}

		public String getTaskId() {
			return taskId;
		}

		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}

		@Override
		public String toString() {
			return "ServiceInDtl [id=" + id + ", qty=" + qty + ", serviceInHdr=" + serviceInHdr + ", serialId="
					+ serialId + ", itemName=" + itemName + ", model=" + model + ", productId=" + productId
					+ ", brandId=" + brandId + ", complaints=" + complaints + ", underWarranty=" + underWarranty
					+ ", observation=" + observation + ", LadiesOrGents=" + LadiesOrGents + ", strap=" + strap
					+ ", Battery=" + Battery + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
		}

		

	

	
}
