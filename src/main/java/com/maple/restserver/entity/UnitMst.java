package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;

 


@Entity
public class UnitMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(length = 50)
    private String id;
 
	@Column(unique = true)
    String unitName;
	@JsonProperty("unitDescription")
    String unitDescription;

	double unitPrecision;
	@Column(length = 50)
	String branchCode;
	@Column(length = 50)
	String oldId;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getOldId() {
		return oldId;
	}




	public void setOldId(String oldId) {
		this.oldId = oldId;
	}




	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	 
 
	
	 
	public UnitMst(){
		
	}
   

	 

	public String getUnitName() {
		return unitName;
	}


	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}


	public String getBranchCode() {
		return branchCode;
	}




	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}




	public String getUnitDescription() {
		return unitDescription;
	}


	public void setUnitDescription(String unitDescription) {
		this.unitDescription = unitDescription;
	}
	public double getUnitPrecision() {
		return unitPrecision;
	}
	public void setUnitPrecision(double unitPrecision) {
		this.unitPrecision = unitPrecision;
	}
	


	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	public CompanyMst getCompanyMst() {
		return companyMst;
	}




	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}




	public String getProcessInstanceId() {
		return processInstanceId;
	}




	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}




	public String getTaskId() {
		return taskId;
	}




	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}




	@Override
	public String toString() {
		return "UnitMst [id=" + id + ", unitName=" + unitName + ", unitDescription=" + unitDescription
				+ ", unitPrecision=" + unitPrecision + ", branchCode=" + branchCode + ", oldId=" + oldId
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst
				+ "]";
	}


	 

	
	
	
}
