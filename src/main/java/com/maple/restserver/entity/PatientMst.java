
package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class PatientMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(length = 50)
	String id;
	@Column(length = 50)
	String patientName;
	@Column(length = 50)
	String nationalId;
	String address1;
	String address2;
	@Column(length = 50)
	String hospitalId;
	@Column(length = 50)
	String contactPerson;
	@Column(length = 50)
	String insuranceCompanyId;
	@Column(length = 50)
	String insuranceCard;
	@Column(length = 50)
	String policyType;
	@Column(length = 50)
	String gender;
	Double percentageCoverage;
	@Column(length = 50)
	String phoneNumber;
	Date dateOfBirth;
	@Column(length = 50)
	String nationality;
	@Column(length = 50)
	String patientType;
	Date insuranceCardExpiry;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	private CompanyMst companyMst;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "accountHeads", nullable = true)
	private AccountHeads accountHeads;
	
	
	
	
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNationalId() {
		return nationalId;
	}
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(String hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getInsuranceCompanyId() {
		return insuranceCompanyId;
	}
	public void setInsuranceCompanyId(String insuranceCompanyId) {
		this.insuranceCompanyId = insuranceCompanyId;
	}
	public String getInsuranceCard() {
		return insuranceCard;
	}
	public void setInsuranceCard(String insuranceCard) {
		this.insuranceCard = insuranceCard;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Double getPercentageCoverage() {
		return percentageCoverage;
	}
	public void setPercentageCoverage(Double percentageCoverage) {
		this.percentageCoverage = percentageCoverage;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public Date getInsuranceCardExpiry() {
		return insuranceCardExpiry;
	}
	public void setInsuranceCardExpiry(Date insuranceCardExpiry) {
		this.insuranceCardExpiry = insuranceCardExpiry;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getPatientType() {
		return patientType;
	}
	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}
	public AccountHeads getAccountHeads() {
		return accountHeads;
	}
	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}
	@Override
	public String toString() {
		return "PatientMst [id=" + id + ", patientName=" + patientName + ", nationalId=" + nationalId + ", address1="
				+ address1 + ", address2=" + address2 + ", hospitalId=" + hospitalId + ", contactPerson="
				+ contactPerson + ", insuranceCompanyId=" + insuranceCompanyId + ", insuranceCard=" + insuranceCard
				+ ", policyType=" + policyType + ", gender=" + gender + ", percentageCoverage=" + percentageCoverage
				+ ", phoneNumber=" + phoneNumber + ", dateOfBirth=" + dateOfBirth + ", nationality=" + nationality
				+ ", patientType=" + patientType + ", insuranceCardExpiry=" + insuranceCardExpiry + ", companyMst="
				+ companyMst + ", accountHeads=" + accountHeads + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
	
}
