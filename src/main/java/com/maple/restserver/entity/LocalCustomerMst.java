package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
public class LocalCustomerMst implements Serializable  {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	 String oldId;
	 
	 
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	
	@Column(length = 50)
	String localcustomerName;
	

	String address;
//	@Column(nullable = true, columnDefinition = "varchar(15)")
	@Column(length = 50)
	String phoneNo;
//	@Column(nullable = true, columnDefinition = "varchar(15)")
	@Column(length = 50)
	String phoneNO2;
	@Column(length = 50)
	String customerId;

	String addressLine1;
	String addressLine2;
//	@Column(nullable = true, columnDefinition = "varchar(15)")
	String landMark;
	
//	@Column(nullable = true, columnDefinition = "varchar(15)")
	String companyMstId;
	
	Date dateOfBirth;
	Date weddingDate;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	 
	
	/*
	 * @ManyToOne(fetch = FetchType.EAGER, optional = false)
	 * 
	 * @JoinColumn(name = "companyMst", nullable = false)
	 * 
	 * @OnDelete(action = OnDeleteAction.CASCADE) CompanyMst companyMst;
	 */
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getCustomerId() {
		return customerId;
	}
	public String getCompanyMstId() {
		return companyMstId;
	}
	public void setCompanyMstId(String companyMstId) {
		this.companyMstId = companyMstId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLocalcustomerName() {
		return localcustomerName;
	}
	public void setLocalcustomerName(String localcustomerName) {
		this.localcustomerName = localcustomerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getPhoneNO2() {
		return phoneNO2;
	}
	public void setPhoneNO2(String phoneNO2) {
		this.phoneNO2 = phoneNO2;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getLandMark() {
		return landMark;
	}
	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public Date getWeddingDate() {
		return weddingDate;
	}
	public void setWeddingDate(Date weddingDate) {
		this.weddingDate = weddingDate;
	}
	public String getOldId() {
		return oldId;
	}
	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "LocalCustomerMst [id=" + id + ", oldId=" + oldId + ", companyMst=" + companyMst + ", localcustomerName="
				+ localcustomerName + ", address=" + address + ", phoneNo=" + phoneNo + ", phoneNO2=" + phoneNO2
				+ ", customerId=" + customerId + ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2
				+ ", landMark=" + landMark + ", companyMstId=" + companyMstId + ", dateOfBirth=" + dateOfBirth
				+ ", weddingDate=" + weddingDate + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	
	
	

}
