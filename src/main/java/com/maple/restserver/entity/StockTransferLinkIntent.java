package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
public class StockTransferLinkIntent implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	@Column(length = 50)
	private String intentDtlId;
	@Column(length = 50)
	private String stocktransferDtlId;
	@Column(length = 50)
	private String allocatedQty;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIntentDtlId() {
		return intentDtlId;
	}

	public void setIntentDtlId(String intentDtlId) {
		this.intentDtlId = intentDtlId;
	}

	
	public String getAllocatedQty() {
		return allocatedQty;
	}

	public void setAllocatedQty(String allocatedQty) {
		this.allocatedQty = allocatedQty;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getStocktransferDtlId() {
		return stocktransferDtlId;
	}

	public void setStocktransferDtlId(String stocktransferDtlId) {
		this.stocktransferDtlId = stocktransferDtlId;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "StockTransferLinkIntent [id=" + id + ", intentDtlId=" + intentDtlId + ", stocktransferDtlId="
				+ stocktransferDtlId + ", allocatedQty=" + allocatedQty + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
	

}
