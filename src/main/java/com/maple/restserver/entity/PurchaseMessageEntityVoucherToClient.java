package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;

import org.springframework.stereotype.Component;
@Component
public class PurchaseMessageEntityVoucherToClient implements Serializable {
	private static final long serialVersionUID = 1L;
	PurchaseHdr purchaseHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	ArrayList<PurchaseDtl> purchaseDtlList =new ArrayList();
	ArrayList<AccountPayable> accountPayableList = new ArrayList();
	public PurchaseHdr getPurchaseHdr() {
		return purchaseHdr;
	}
	public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
		this.purchaseHdr = purchaseHdr;
	}
	public ArrayList<PurchaseDtl> getPurchaseDtlList() {
		return purchaseDtlList;
	}
	public void setPurchaseDtlList(ArrayList<PurchaseDtl> purchaseDtlList) {
		this.purchaseDtlList = purchaseDtlList;
	}
	public ArrayList<AccountPayable> getAccountPayableList() {
		return accountPayableList;
	}
	public void setAccountPayableList(ArrayList<AccountPayable> accountPayableList) {
		this.accountPayableList = accountPayableList;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PurchaseMessageEntityVoucherToClient [purchaseHdr=" + purchaseHdr + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", purchaseDtlList=" + purchaseDtlList
				+ ", accountPayableList=" + accountPayableList + "]";
	}
	


 
	
}
