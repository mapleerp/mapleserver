package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.lang.Nullable;

@Entity
public class ProductConversionMst implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	   private String id;
	   private Date conversionDate;
	   @Column(length = 50)
	   private String voucherNo;
	   @ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "companyMst", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		CompanyMst companyMst;
	   @Column(length = 50)
		private String branchCode;
	   @Column(length = 50)
		private String  processInstanceId;
	   @Column(length = 50)
		private String taskId;
		
	public String getId() {
		return id;
	}

	public Date getConversionDate() {
		return conversionDate;
	}
	public void setConversionDate(Date conversionDate) {
		this.conversionDate = conversionDate;
	}
	

	public String getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}

	public void setId(String id) {
		this.id = id;
	}



	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ProductConversionMst [id=" + id + ", conversionDate=" + conversionDate + ", voucherNo=" + voucherNo
				+ ", companyMst=" + companyMst + ", branchCode=" + branchCode + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

	
	
	
	

}
