package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class SgstDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
 @Id
 @GeneratedValue
 Integer id;
 @JsonProperty("warrentyTransDetails")
 String warrentyTransDetails;
 @JsonProperty("parentId")
 @Column(length = 50)
 String parentId;
 @JsonProperty("itemId")
 Integer itemId;
 @JsonProperty("extendedWarrentyYear")
 Integer extentedWarrentyYear;
 @JsonProperty("warrentyCode")
 @Column(length = 50)
 String warrentyCode;
 @Column(length = 50)
 private String  processInstanceId;
 @Column(length = 50)
	private String taskId;
public Integer getId() {
	return id;
}

public String getWarrentyTransDetails() {
	return warrentyTransDetails;
}
public void setWarrentyTransDetails(String warrentyTransDetails) {
	this.warrentyTransDetails = warrentyTransDetails;
}
public String getParentId() {
	return parentId;
}
public void setParentId(String parentId) {
	this.parentId = parentId;
}
public Integer getItemId() {
	return itemId;
}
public void setItemId(Integer itemId) {
	this.itemId = itemId;
}
public Integer getExtentedWarrentyYear() {
	return extentedWarrentyYear;
}
public void setExtentedWarrentyYear(Integer extentedWarrentyYear) {
	this.extentedWarrentyYear = extentedWarrentyYear;
}
public String getWarrentyCode() {
	return warrentyCode;
}
public void setWarrentyCode(String warrentyCode) {
	this.warrentyCode = warrentyCode;
}


public String getProcessInstanceId() {
	return processInstanceId;
}

public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}

public String getTaskId() {
	return taskId;
}

public void setTaskId(String taskId) {
	this.taskId = taskId;
}

@Override
public String toString() {
	return "SgstDtl [id=" + id + ", warrentyTransDetails=" + warrentyTransDetails + ", parentId=" + parentId
			+ ", itemId=" + itemId + ", extentedWarrentyYear=" + extentedWarrentyYear + ", warrentyCode=" + warrentyCode
			+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
}
 
 
 
 
}
