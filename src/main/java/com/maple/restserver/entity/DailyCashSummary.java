package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
@Entity
public class DailyCashSummary implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	
Date reportDate;
Double totalCashAtDrawer;
Double openingCashAtDrawer;
Double cashVariance;
@Column(length = 50)
private String  processInstanceId;
@Column(length = 50)
private String taskId;
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public Date getReportDate() {
	return reportDate;
}
public void setReportDate(Date reportDate) {
	this.reportDate = reportDate;
}
public Double getTotalCashAtDrawer() {
	return totalCashAtDrawer;
}
public void setTotalCashAtDrawer(Double totalCashAtDrawer) {
	this.totalCashAtDrawer = totalCashAtDrawer;
}
public Double getOpeningCashAtDrawer() {
	return openingCashAtDrawer;
}
public void setOpeningCashAtDrawer(Double openingCashAtDrawer) {
	this.openingCashAtDrawer = openingCashAtDrawer;
}
public Double getCashVariance() {
	return cashVariance;
}
public void setCashVariance(Double cashVariance) {
	this.cashVariance = cashVariance;
}
public String getProcessInstanceId() {
	return processInstanceId;
}
public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}
public String getTaskId() {
	return taskId;
}
public void setTaskId(String taskId) {
	this.taskId = taskId;
}
@Override
public String toString() {
	return "DailyCashSummary [id=" + id + ", reportDate=" + reportDate + ", totalCashAtDrawer=" + totalCashAtDrawer
			+ ", openingCashAtDrawer=" + openingCashAtDrawer + ", cashVariance=" + cashVariance + ", processInstanceId="
			+ processInstanceId + ", taskId=" + taskId + "]";
}

}
