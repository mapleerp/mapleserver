package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class MonthlySalesReceipts implements Serializable{

	

	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	String Id;
	String receiptMode;
	@Column(length = 50)
	String branchCode;
	@Column(length = 50)
	String userId;
	@Column(length = 50)
	String accountId;
	Double receiptAmount;
	@Column(length = 50)
	String voucherNumber; 
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "monthly_sales_trans_hdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private MonthlySalesTransHdr monthlySalesTransHdrId;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;


	public String getId() {
		return Id;
	}


	public void setId(String id) {
		Id = id;
	}


	public String getReceiptMode() {
		return receiptMode;
	}


	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}


	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getAccountId() {
		return accountId;
	}


	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}


	public Double getReceiptAmount() {
		return receiptAmount;
	}


	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}


	public String getVoucherNumber() {
		return voucherNumber;
	}


	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}


	



	public MonthlySalesTransHdr getMonthlySalesTransHdrId() {
		return monthlySalesTransHdrId;
	}


	public void setMonthlySalesTransHdrId(MonthlySalesTransHdr monthlySalesTransHdrId) {
		this.monthlySalesTransHdrId = monthlySalesTransHdrId;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "MonthlySalesReceipts [Id=" + Id + ", receiptMode=" + receiptMode + ", branchCode=" + branchCode
				+ ", userId=" + userId + ", accountId=" + accountId + ", receiptAmount=" + receiptAmount
				+ ", voucherNumber=" + voucherNumber + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", monthlySalesTransHdrId=" + monthlySalesTransHdrId + ", companyMst=" + companyMst + "]";
	}


	

	
	
}
