package com.maple.restserver.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class SaleOrderEdit implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

	@Column(length = 50)
	String itemId;

	Double rate;

	Double cgstTaxRate;

	Double sgstTaxRate;

	Double cessRate;


	Double addCessRate;
	@Column(length = 50)
	String itemTaxaxId;
	@Column(length = 50)
	String unitId;
	@Column(length = 50)
	String itemName;
	
	LocalDate expiryDate;
	@Column(length = 50)
	String batch;
	@Column(length = 50)
	String barode;

	Double taxRate;
	Double amount;

	Double mrp;
	@Column(length = 50)
	String itemCode;
	@Column(length = 50)
	String unitName;
	
	@Column(length = 50)
	String branchCode;
	
	Double discount;
	Double igstTaxRate;
	Double cgstAmount;
	Double sgstAmount;
	Double igstAmount;
	Double cessAmount;
	Double qty;
	
    Double standardPrice;

    Double addCessAmount;
    Double costPrice;

	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "sales_order_trans_hdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private SalesOrderTransHdr salesOrderTransHdr;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getItemId() {
		return itemId;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	public Double getRate() {
		return rate;
	}


	public void setRate(Double rate) {
		this.rate = rate;
	}


	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}


	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}


	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}


	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}


	public Double getCessRate() {
		return cessRate;
	}


	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}


	public Double getAddCessRate() {
		return addCessRate;
	}


	public void setAddCessRate(Double addCessRate) {
		this.addCessRate = addCessRate;
	}


	public String getItemTaxaxId() {
		return itemTaxaxId;
	}


	public void setItemTaxaxId(String itemTaxaxId) {
		this.itemTaxaxId = itemTaxaxId;
	}


	public String getUnitId() {
		return unitId;
	}


	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public LocalDate getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}


	public String getBatch() {
		return batch;
	}


	public void setBatch(String batch) {
		this.batch = batch;
	}


	public String getBarode() {
		return barode;
	}


	public void setBarode(String barode) {
		this.barode = barode;
	}


	public Double getTaxRate() {
		return taxRate;
	}


	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}


	public Double getMrp() {
		return mrp;
	}


	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}


	public String getItemCode() {
		return itemCode;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public String getUnitName() {
		return unitName;
	}


	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}


	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public Double getDiscount() {
		return discount;
	}


	public void setDiscount(Double discount) {
		this.discount = discount;
	}


	public Double getIgstTaxRate() {
		return igstTaxRate;
	}


	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}


	public Double getCgstAmount() {
		return cgstAmount;
	}


	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}


	public Double getSgstAmount() {
		return sgstAmount;
	}


	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}


	public Double getIgstAmount() {
		return igstAmount;
	}


	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}


	public Double getCessAmount() {
		return cessAmount;
	}


	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}


	public SalesOrderTransHdr getSalesOrderTransHdr() {
		return salesOrderTransHdr;
	}


	public void setSalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr) {
		this.salesOrderTransHdr = salesOrderTransHdr;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public Double getQty() {
		return qty;
	}


	public void setQty(Double qty) {
		this.qty = qty;
	}




	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}


	
	public Double getAddCessAmount() {
		return addCessAmount;
	}


	public void setAddCessAmount(Double addCessAmount) {
		this.addCessAmount = addCessAmount;
	}

	

	public Double getStandardPrice() {
		return standardPrice;
	}


	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}


	
	public Double getCostPrice() {
		return costPrice;
	}


	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}


	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "SaleOrderEdit [id=" + id + ", itemId=" + itemId + ", rate=" + rate + ", cgstTaxRate=" + cgstTaxRate
				+ ", sgstTaxRate=" + sgstTaxRate + ", cessRate=" + cessRate + ", addCessRate=" + addCessRate
				+ ", itemTaxaxId=" + itemTaxaxId + ", unitId=" + unitId + ", itemName=" + itemName + ", expiryDate="
				+ expiryDate + ", batch=" + batch + ", barode=" + barode + ", taxRate=" + taxRate + ", amount=" + amount
				+ ", mrp=" + mrp + ", itemCode=" + itemCode + ", unitName=" + unitName + ", branchCode=" + branchCode
				+ ", discount=" + discount + ", igstTaxRate=" + igstTaxRate + ", cgstAmount=" + cgstAmount
				+ ", sgstAmount=" + sgstAmount + ", igstAmount=" + igstAmount + ", cessAmount=" + cessAmount + ", qty="
				+ qty + ", standardPrice=" + standardPrice + ", addCessAmount=" + addCessAmount + ", costPrice="
				+ costPrice + ", salesOrderTransHdr=" + salesOrderTransHdr + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


	
}
