package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class KotTableDtl implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "kotTableHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	KotTableHdr kotTableHdr;
	String customization;
	Double customizationRate;
	private Double qty;
	private Double rate;
	private String itemId;
	
	String kotNumber;
	
	Date orderTakerTime;
    String kitchenStatus;
	Date kitchenStatusUpdatedTime;
	Date orderDeliveryTime;
	
	
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public KotTableHdr getKotTableHdr() {
		return kotTableHdr;
	}
	public void setKotTableHdr(KotTableHdr kotTableHdr) {
		this.kotTableHdr = kotTableHdr;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Date getOrderTakerTime() {
		return orderTakerTime;
	}
	public void setOrderTakerTime(Date orderTakerTime) {
		this.orderTakerTime = orderTakerTime;
	}
	public String getCustomization() {
		return customization;
	}
	public void setCustomization(String customization) {
		this.customization = customization;
	}
	public Double getCustomizationRate() {
		return customizationRate;
	}
	public void setCustomizationRate(Double customizationRate) {
		this.customizationRate = customizationRate;
	}
	public String getKitchenStatus() {
		return kitchenStatus;
	}
	public void setKitchenStatus(String kitchenStatus) {
		this.kitchenStatus = kitchenStatus;
	}
	public Date getKitchenStatusUpdatedTime() {
		return kitchenStatusUpdatedTime;
	}
	public void setKitchenStatusUpdatedTime(Date kitchenStatusUpdatedTime) {
		this.kitchenStatusUpdatedTime = kitchenStatusUpdatedTime;
	}
	public Date getOrderDeliveryTime() {
		return orderDeliveryTime;
	}
	public void setOrderDeliveryTime(Date orderDeliveryTime) {
		this.orderDeliveryTime = orderDeliveryTime;
	}

	public String getKotNumber() {
		return kotNumber;
	}

	public void setKotNumber(String kotNumber) {
		this.kotNumber = kotNumber;
	}

	@Override
	public String toString() {
		return "KotTableDtl [id=" + id + ", companyMst=" + companyMst + ", kotTableHdr=" + kotTableHdr
				+ ", customization=" + customization + ", customizationRate=" + customizationRate + ", qty=" + qty
				+ ", rate=" + rate + ", itemId=" + itemId + ", kotNumber=" + kotNumber + ", orderTakerTime="
				+ orderTakerTime + ", kitchenStatus=" + kitchenStatus + ", kitchenStatusUpdatedTime="
				+ kitchenStatusUpdatedTime + ", orderDeliveryTime=" + orderDeliveryTime + "]";
	}

	
	
	
}
