package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class WebsiteFeatureMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(length = 50)
    private String id;
	
	private String featureboximgurl1;
	private String featuresstrong1;
	private String featuresspan1;
	public String getId() {
		return id;
	}
	public String getFeatureboximgurl1() {
		return featureboximgurl1;
	}
	public String getFeaturesstrong1() {
		return featuresstrong1;
	}
	public String getFeaturesspan1() {
		return featuresspan1;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setFeatureboximgurl1(String featureboximgurl1) {
		this.featureboximgurl1 = featureboximgurl1;
	}
	public void setFeaturesstrong1(String featuresstrong1) {
		this.featuresstrong1 = featuresstrong1;
	}
	public void setFeaturesspan1(String featuresspan1) {
		this.featuresspan1 = featuresspan1;
	}
	@Override
	public String toString() {
		return "WebsiteFeatureMst [id=" + id + ", featureboximgurl1=" + featureboximgurl1 + ", featuresstrong1="
				+ featuresstrong1 + ", featuresspan1=" + featuresspan1 + "]";
	}
	
	
	

}
