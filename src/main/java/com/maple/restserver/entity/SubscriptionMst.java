package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class SubscriptionMst implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)

	
	@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	
	
	private Date subscriptionEnds;
	
	
	private String subscriptionKey;
	


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getSubscriptionEnds() {
		return subscriptionEnds;
	}

	public void setSubscriptionEnds(Date subscriptionEnds) {
		this.subscriptionEnds = subscriptionEnds;
	}

	public String getSubscriptionKey() {
		return subscriptionKey;
	}

	public void setSubscriptionKey(String subscriptionKey) {
		this.subscriptionKey = subscriptionKey;
	}

	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	


}
