package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
public class TallyFailedVoucherMst implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	
 
	@Column(length = 50)
	 
	private String voucherNumber;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;


	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	public String getVoucherNumber() {
		return voucherNumber;
	}




	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}




	public String getProcessInstanceId() {
		return processInstanceId;
	}




	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}




	public String getTaskId() {
		return taskId;
	}




	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}




	@Override
	public String toString() {
		return "TallyFailedVoucherMst [id=" + id + ", voucherNumber=" + voucherNumber + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

 
	
  
	
	
	

}
