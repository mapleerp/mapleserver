package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class AccountPayable implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)

	@Id
	   @GeneratedValue(generator = "uuid")
	   @GenericGenerator(name = "uuid", strategy = "uuid2")
	   private String id;
		
		 
		
		
		@ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "companyMst", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		private CompanyMst companyMst;
		
		
		@ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "accountHeads", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		private AccountHeads accountHeads;
		
		
		@OneToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "purchaseHdr", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		private PurchaseHdr purchaseHdr;
		
		
		
		@Column(length = 50)

		String voucherNumber;
		Date voucherDate;
		
		Date dueDate;
		@Column(length = 50)

		String accountId;
		 

		String remark;
		Double dueAmount;
		Double paidAmount;
		@Column(length = 50)

		private String  processInstanceId;
		@Column(length = 50)

		private String taskId;
	

		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public CompanyMst getCompanyMst() {
			return companyMst;
		}
		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}
		
		public AccountHeads getAccountHeads() {
			return accountHeads;
		}
		public void setAccountHeads(AccountHeads accountHeads) {
			this.accountHeads = accountHeads;
		}
		public PurchaseHdr getPurchaseHdr() {
			return purchaseHdr;
		}
		public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
			this.purchaseHdr = purchaseHdr;
		}
		public String getVoucherNumber() {
			return voucherNumber;
		}
		public void setVoucherNumber(String voucherNumber) {
			this.voucherNumber = voucherNumber;
		}
		public Date getVoucherDate() {
			return voucherDate;
		}
		public void setVoucherDate(Date voucherDate) {
			this.voucherDate = voucherDate;
		}
		public Date getDueDate() {
			return dueDate;
		}
		public void setDueDate(Date dueDate) {
			this.dueDate = dueDate;
		}
		public String getAccountId() {
			return accountId;
		}
		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}
		public String getRemark() {
			return remark;
		}
		public void setRemark(String remark) {
			this.remark = remark;
		}
		public Double getDueAmount() {
			return dueAmount;
		}
		public void setDueAmount(Double dueAmount) {
			this.dueAmount = dueAmount;
		}
		public Double getPaidAmount() {
			return paidAmount;
		}
		public void setPaidAmount(Double paidAmount) {
			this.paidAmount = paidAmount;
		}
		
		public String getProcessInstanceId() {
			return processInstanceId;
		}
		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}
		public String getTaskId() {
			return taskId;
		}
		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}
		@Override
		public String toString() {
			return "AccountPayable [id=" + id + ", companyMst=" + companyMst + ", accountHeads=" + accountHeads
					+ ", purchaseHdr=" + purchaseHdr + ", voucherNumber=" + voucherNumber + ", voucherDate="
					+ voucherDate + ", dueDate=" + dueDate + ", accountId=" + accountId + ", remark=" + remark
					+ ", dueAmount=" + dueAmount + ", paidAmount=" + paidAmount + ", processInstanceId="
					+ processInstanceId + ", taskId=" + taskId + "]";
		}
		
		
		
		

}
