package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
public class AcceptStock implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)

	
	@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	
	@JsonProperty("userId")
	Integer userId;
	
	
	@JsonProperty("intent")
	private Integer intent;
	@Column(length = 50)
	@JsonProperty("branch")
	private String branch;
	@Column(length = 50)

	@JsonProperty("recId")
	private String recId;
	
	@JsonProperty("voucherNumber")
	private Integer voucherNumber;
	@Column(length = 50)

	@JsonProperty("intentDate")
	private String intentDate;
	@Column(length = 50)

	@JsonProperty("pendingStatus")
	private String pendingStatus;
	@Column(length = 50)

	private String  processInstanceId;
	@Column(length = 50)

	private String taskId;
	
	
 


 


	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	
	
	
	public String getPendingStatus() {
		return pendingStatus;
	}



	public void setPendingStatus(String pendingStatus) {
		this.pendingStatus = pendingStatus;
	}


 
	

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getIntent() {
		return intent;
	}

	public void setIntent(Integer intent) {
		this.intent = intent;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getRecId() {
		return recId;
	}

	public void setRecId(String recId) {
		this.recId = recId;
	}

	public Integer getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(Integer voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getIntentDate() {
		return intentDate;
	}

	public void setIntentDate(String intentDate) {
		this.intentDate = intentDate;
	}



	

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



 

	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	@Override
	public String toString() {
		return "AcceptStock [id=" + id + ", userId=" + userId + ", intent=" + intent + ", branch=" + branch + ", recId="
				+ recId + ", voucherNumber=" + voucherNumber + ", intentDate=" + intentDate + ", pendingStatus="
				+ pendingStatus + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", companyMst="
				+ companyMst + "]";
	}



	
	
	
	

}
