package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

 
@Entity
public class PurchaseDtl implements Serializable{
	private static final long serialVersionUID = 1L;
	public PurchaseDtl() {

	}
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
 
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "purchase_hdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	PurchaseHdr purchaseHdr;

	
	//version1.7
	String purchaseOrderDtl;
//version1.7 ends gets and sets also there
	@Column(length = 256)
	private String itemName;
//	private String itemId;

	 Double purchseRate;

	 Double fcPurchaseRate;
	private Double amount;

	private Double fcAmount;
	@Column(length = 50)
	private String batch;

	private Integer itemSerial;
	@Column(length = 50)

	private String barcode;

	private Double taxAmt;

	private Double fcTaxAmt;
	private Double cessAmt;

	private Double fcCessAmt;
	private Double previousMRP;

	private Date expiryDate;

	private Integer freeQty;

	private Double qty;

	private Double taxRate;

	private Double fcTaxRate;
	private Double discount;

	private Double fcDiscount;
	private String changePriceStatus;

	private Integer unit;

	private Double mrp;

	private Double cessRate;
	private Double fcCessRate;
	private Date manufactureDate;
	@Column(length = 50)
	private String itemId;
	@Column(length = 50)
	private String BinNo;
	private Double fcNetCost;
	@Column(length = 50)
	private String unitId;

	private Double CessRate2;
	private Double fcCessRate2;
	private Double NetCost;
	private Double fcMrp;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	private Integer displaySerial;
	
	private String store;
	
	

	
	
	public Double getFcMrp() {
		return fcMrp;
	}

	public void setFcMrp(Double fcMrp) {
		this.fcMrp = fcMrp;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getFcPurchaseRate() {
		return fcPurchaseRate;
	}

	public void setFcPurchaseRate(Double fcPurchaseRate) {
		this.fcPurchaseRate = fcPurchaseRate;
	}

	public Double getFcAmount() {
		return fcAmount;
	}

	public void setFcAmount(Double fcAmount) {
		this.fcAmount = fcAmount;
	}

	public Double getFcTaxAmt() {
		return fcTaxAmt;
	}

	public void setFcTaxAmt(Double fcTaxAmt) {
		this.fcTaxAmt = fcTaxAmt;
	}

	public Double getFcCessAmt() {
		return fcCessAmt;
	}

	public void setFcCessAmt(Double fcCessAmt) {
		this.fcCessAmt = fcCessAmt;
	}

	public Double getFcTaxRate() {
		return fcTaxRate;
	}

	public void setFcTaxRate(Double fcTaxRate) {
		this.fcTaxRate = fcTaxRate;
	}

	public Double getFcDiscount() {
		return fcDiscount;
	}

	public void setFcDiscount(Double fcDiscount) {
		this.fcDiscount = fcDiscount;
	}

	public Double getFcCessRate() {
		return fcCessRate;
	}

	public void setFcCessRate(Double fcCessRate) {
		this.fcCessRate = fcCessRate;
	}

	public Double getFcCessRate2() {
		return fcCessRate2;
	}

	public void setFcCessRate2(Double fcCessRate2) {
		this.fcCessRate2 = fcCessRate2;
	}

	public PurchaseHdr getPurchaseHdr() {
		return purchaseHdr;
	}

	public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
		this.purchaseHdr = purchaseHdr;
	}

	
	 public String getItemName() { return itemName; }
	 
	  public void setItemName(String itemName) { this.itemName = itemName; }
	 

	public Double getPurchseRate() {
		return purchseRate;
	}

	public Double getFcNetCost() {
		return fcNetCost;
	}

	public void setFcNetCost(Double fcNetCost) {
		this.fcNetCost = fcNetCost;
	}

	public void setPurchseRate(Double purchseRate) {
		this.purchseRate = purchseRate;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Integer getItemSerial() {
		return itemSerial;
	}

	public void setItemSerial(Integer itemSerial) {
		this.itemSerial = itemSerial;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Double getTaxAmt() {
		return taxAmt;
	}

	public void setTaxAmt(Double taxAmt) {
		this.taxAmt = taxAmt;
	}

	public Double getCessAmt() {
		return cessAmt;
	}

	public void setCessAmt(Double cessAmt) {
		this.cessAmt = cessAmt;
	}

	public Double getPreviousMRP() {
		return previousMRP;
	}

	public void setPreviousMRP(Double previousMRP) {
		this.previousMRP = previousMRP;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Integer getFreeQty() {
		return freeQty;
	}

	public void setFreeQty(Integer freeQty) {
		this.freeQty = freeQty;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String getChangePriceStatus() {
		return changePriceStatus;
	}

	public void setChangePriceStatus(String changePriceStatus) {
		this.changePriceStatus = changePriceStatus;
	}

	public Integer getUnit() {
		return unit;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public Double getCessRate() {
		return cessRate;
	}

	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}

	public Date getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}


	public String getBinNo() {
		return BinNo;
	}

	public void setBinNo(String binNo) {
		BinNo = binNo;
	}

	public Double getCessRate2() {
		return CessRate2;
	}

	public void setCessRate2(Double cessRate2) {
		CessRate2 = cessRate2;
	}

	public Double getNetCost() {
		return NetCost;
	}

	public void setNetCost(Double netCost) {
		NetCost = netCost;
	}

	 
	public String getPurchaseOrderDtl() {
		return purchaseOrderDtl;
	}

	public void setPurchaseOrderDtl(String purchaseOrderDtl) {
		this.purchaseOrderDtl = purchaseOrderDtl;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Integer getDisplaySerial() {
		return displaySerial;
	}

	public void setDisplaySerial(Integer displaySerial) {
		this.displaySerial = displaySerial;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}


	

	@Override
	public String toString() {
		return "PurchaseDtl [id=" + id + ", purchaseHdr=" + purchaseHdr + ", purchaseOrderDtl=" + purchaseOrderDtl
				+ ", itemName=" + itemName + ", purchseRate=" + purchseRate + ", fcPurchaseRate=" + fcPurchaseRate
				+ ", amount=" + amount + ", fcAmount=" + fcAmount + ", batch=" + batch + ", itemSerial=" + itemSerial
				+ ", barcode=" + barcode + ", taxAmt=" + taxAmt + ", fcTaxAmt=" + fcTaxAmt + ", cessAmt=" + cessAmt
				+ ", fcCessAmt=" + fcCessAmt + ", previousMRP=" + previousMRP + ", expiryDate=" + expiryDate
				+ ", freeQty=" + freeQty + ", qty=" + qty + ", taxRate=" + taxRate + ", fcTaxRate=" + fcTaxRate
				+ ", discount=" + discount + ", fcDiscount=" + fcDiscount + ", changePriceStatus=" + changePriceStatus
				+ ", unit=" + unit + ", mrp=" + mrp + ", cessRate=" + cessRate + ", fcCessRate=" + fcCessRate
				+ ", manufactureDate=" + manufactureDate + ", itemId=" + itemId + ", BinNo=" + BinNo + ", fcNetCost="
				+ fcNetCost + ", unitId=" + unitId + ", CessRate2=" + CessRate2 + ", fcCessRate2=" + fcCessRate2
				+ ", NetCost=" + NetCost + ", fcMrp=" + fcMrp + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + ", displaySerial=" + displaySerial + ", store=" + store + "]";
	}

	 

	 

  
}
