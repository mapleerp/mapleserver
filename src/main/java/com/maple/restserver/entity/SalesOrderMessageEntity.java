package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.SalesTransHdr;
@Component
public class SalesOrderMessageEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	SalesOrderTransHdr salesOrderTransHdr;
	ArrayList<SalesOrderDtl> salesOrderDtlList =new ArrayList();
	ArrayList<SaleOrderReceipt> salesOrderReceiptsList =new ArrayList();
	
	AccountHeads accountHeads;
	LocalCustomerMst localCustomerMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	
	
	public SalesOrderTransHdr getSalesOrderTransHdr() {
		return salesOrderTransHdr;
	}
	public void setSalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr) {
		this.salesOrderTransHdr = salesOrderTransHdr;
	}
	public ArrayList<SalesOrderDtl> getSalesOrderDtlList() {
		return salesOrderDtlList;
	}
	public void setSalesOrderDtlList(ArrayList<SalesOrderDtl> salesOrderDtlList) {
		this.salesOrderDtlList = salesOrderDtlList;
	}
	
	public ArrayList<SaleOrderReceipt> getSalesOrderReceiptsList() {
		return salesOrderReceiptsList;
	}
	public void setSalesOrderReceiptsList(ArrayList<SaleOrderReceipt> salesOrderReceiptsList) {
		this.salesOrderReceiptsList = salesOrderReceiptsList;
	}
	
	public LocalCustomerMst getLocalCustomerMst() {
		return localCustomerMst;
	}
	public void setLocalCustomerMst(LocalCustomerMst localCustomerMst) {
		this.localCustomerMst = localCustomerMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public AccountHeads getAccountHeads() {
		return accountHeads;
	}
	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}
	@Override
	public String toString() {
		return "SalesOrderMessageEntity [salesOrderTransHdr=" + salesOrderTransHdr + ", salesOrderDtlList="
				+ salesOrderDtlList + ", salesOrderReceiptsList=" + salesOrderReceiptsList + ", accountHeads="
				+ accountHeads + ", localCustomerMst=" + localCustomerMst + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	  
	
	
 
	
}
