package com.maple.restserver.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
public class JobCardInHdr {
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	@Column(length = 50)
	String fromBranch;
	@Column(length = 50)
	String branchCode;
	@Column(length = 50)
	String voucherNumber;
	String voucherDate;
	@Column(length = 50)
	private String jobCardVoucherNumber;
	private Date jobCardVoucherDate;
	@Column(length = 50)
	private String customerId;
	@Column(length = 50)
	String status;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "serviceInDtl", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	ServiceInDtl serviceInDtl;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getFromBranch() {
		return fromBranch;
	}


	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}


	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public String getVoucherNumber() {
		return voucherNumber;
	}


	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}


	public String getVoucherDate() {
		return voucherDate;
	}


	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}


	public String getJobCardVoucherNumber() {
		return jobCardVoucherNumber;
	}


	public void setJobCardVoucherNumber(String jobCardVoucherNumber) {
		this.jobCardVoucherNumber = jobCardVoucherNumber;
	}


	public Date getJobCardVoucherDate() {
		return jobCardVoucherDate;
	}


	public void setJobCardVoucherDate(Date jobCardVoucherDate) {
		this.jobCardVoucherDate = jobCardVoucherDate;
	}


	public String getCustomerId() {
		return customerId;
	}


	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public ServiceInDtl getServiceInDtl() {
		return serviceInDtl;
	}


	public void setServiceInDtl(ServiceInDtl serviceInDtl) {
		this.serviceInDtl = serviceInDtl;
	}


	

	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "JobCardInHdr [id=" + id + ", fromBranch=" + fromBranch + ", branchCode=" + branchCode
				+ ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate + ", jobCardVoucherNumber="
				+ jobCardVoucherNumber + ", jobCardVoucherDate=" + jobCardVoucherDate + ", customerId=" + customerId
				+ ", status=" + status + ", companyMst=" + companyMst + ", serviceInDtl=" + serviceInDtl
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
