package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Entity

public class PhysicalStockDtl implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	@Column(length = 50)
	private String itemId;
	@Column(length = 50)
	private String batch ;
	@Column(length = 50)
	private String barcode;
	private Double qtyIn;
	private Double qtyOut;
	private Double mrp;
	 
	private Date expiryDate;
	private Date manufactureDate;
	@Column(length = 50)
	private String itemName;
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "physical_stock_hdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private PhysicalStockHdr physicalStockHdrId;


	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	private Double systemQty;
	
	
	
	String store;
	
	
	
	
	public String getStore() {
		return store;
	}



	public void setStore(String store) {
		this.store = store;
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getItemId() {
		return itemId;
	}



	public void setItemId(String itemId) {
		this.itemId = itemId;
	}



	public String getBatch() {
		return batch;
	}



	public void setBatch(String batch) {
		this.batch = batch;
	}



	public String getBarcode() {
		return barcode;
	}



	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}



	public Double getQtyIn() {
		return qtyIn;
	}



	public void setQtyIn(Double qtyIn) {
		this.qtyIn = qtyIn;
	}



	public Double getQtyOut() {
		return qtyOut;
	}



	public void setQtyOut(Double qtyOut) {
		this.qtyOut = qtyOut;
	}



	public Double getMrp() {
		return mrp;
	}



	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}



	public Date getExpiryDate() {
		return expiryDate;
	}



	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}



	public Date getManufactureDate() {
		return manufactureDate;
	}



	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}



	public PhysicalStockHdr getPhysicalStockHdrId() {
		return physicalStockHdrId;
	}



	public void setPhysicalStockHdrId(PhysicalStockHdr physicalStockHdrId) {
		this.physicalStockHdrId = physicalStockHdrId;
	}





	public String getItemName() {
		return itemName;
	}



	public void setItemName(String itemName) {
		this.itemName = itemName;
	}



	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}






	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	public Double getSystemQty() {
		return systemQty;
	}



	public void setSystemQty(Double systemQty) {
		this.systemQty = systemQty;
	}



	@Override
	public String toString() {
		return "PhysicalStockDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", barcode=" + barcode
				+ ", qtyIn=" + qtyIn + ", qtyOut=" + qtyOut + ", mrp=" + mrp + ", expiryDate=" + expiryDate
				+ ", manufactureDate=" + manufactureDate + ", itemName=" + itemName + ", physicalStockHdrId="
				+ physicalStockHdrId + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", systemQty=" + systemQty + ", store=" + store + "]";
	}



	



	
	
	
}
