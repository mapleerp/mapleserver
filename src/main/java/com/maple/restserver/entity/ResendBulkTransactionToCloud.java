package com.maple.restserver.entity;

import java.util.ArrayList;


public class ResendBulkTransactionToCloud {
	
	ArrayList<SalesTransHdr> salesTransHdrList;
	ArrayList<SalesDtl> salesDtlList;
	ArrayList<SalesReceipts> salesReceiptsList;
	
	ArrayList<PurchaseHdr> purchaseHdrList;
	ArrayList<PurchaseDtl> purchaseDtlList;
	
	ArrayList<ItemBatchDtl> itemBatchDtlList;
	
	
	public ArrayList<SalesTransHdr> getSalesTransHdrList() {
		return salesTransHdrList;
	}
	public void setSalesTransHdrList(ArrayList<SalesTransHdr> salesTransHdrList) {
		this.salesTransHdrList = salesTransHdrList;
	}
	public ArrayList<SalesDtl> getSalesDtlList() {
		return salesDtlList;
	}
	public void setSalesDtlList(ArrayList<SalesDtl> salesDtlList) {
		this.salesDtlList = salesDtlList;
	}
	public ArrayList<SalesReceipts> getSalesReceiptsList() {
		return salesReceiptsList;
	}
	public void setSalesReceiptsList(ArrayList<SalesReceipts> salesReceiptsList) {
		this.salesReceiptsList = salesReceiptsList;
	}
	public ArrayList<PurchaseHdr> getPurchaseHdrList() {
		return purchaseHdrList;
	}
	public void setPurchaseHdrList(ArrayList<PurchaseHdr> purchaseHdrList) {
		this.purchaseHdrList = purchaseHdrList;
	}
	public ArrayList<PurchaseDtl> getPurchaseDtlList() {
		return purchaseDtlList;
	}
	public void setPurchaseDtlList(ArrayList<PurchaseDtl> purchaseDtlList) {
		this.purchaseDtlList = purchaseDtlList;
	}
	
	public ArrayList<ItemBatchDtl> getItemBatchDtlList() {
		return itemBatchDtlList;
	}
	public void setItemBatchDtlList(ArrayList<ItemBatchDtl> itemBatchDtlList) {
		this.itemBatchDtlList = itemBatchDtlList;
	}
	@Override
	public String toString() {
		return "ResendBulkTransactionToCloud [salesTransHdrList=" + salesTransHdrList + ", salesDtlList=" + salesDtlList
				+ ", salesReceiptsList=" + salesReceiptsList + ", purchaseHdrList=" + purchaseHdrList
				+ ", purchaseDtlList=" + purchaseDtlList + ", itemBatchDtlList=" + itemBatchDtlList + "]";
	}
	
	
	
	
	
}
