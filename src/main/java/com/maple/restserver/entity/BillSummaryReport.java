package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;

public class BillSummaryReport implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Column(length = 50)
	String slNo;
	@Column(length = 50)
    String  description;
	@Column(length = 50)
    String amount;
	@Column(length = 50)
    private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
    
public String getSlNo() {
	return slNo;
}
public void setSlNo(String slNo) {
	this.slNo = slNo;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getAmount() {
	return amount;
}
public void setAmount(String amount) {
	this.amount = amount;
}
public String getProcessInstanceId() {
	return processInstanceId;
}
public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}
public String getTaskId() {
	return taskId;
}
public void setTaskId(String taskId) {
	this.taskId = taskId;
}
@Override
public String toString() {
	return "BillSummaryReport [slNo=" + slNo + ", description=" + description + ", amount=" + amount
			+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
}



}
