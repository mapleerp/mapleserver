package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class UrsMst implements Serializable{
private static final long serialVersionUID = 1L;

	
@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
@Column(length = 50)
	String itemId;
	Double systemStock;
	Double enteredStock;
	@Column(length = 50)
	String userId;
	@Column(length = 50)
	String branchCode;
	@UpdateTimestamp
	@Column(nullable=true)
	private LocalDateTime updatedTime;
	Date transDate;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	String oldId;
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getSystemStock() {
		return systemStock;
	}
	public void setSystemStock(Double systemStock) {
		this.systemStock = systemStock;
	}
	public Double getEnteredStock() {
		return enteredStock;
	}
	public void setEnteredStock(Double enteredStock) {
		this.enteredStock = enteredStock;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}
	public Date getTransDate() {
		return transDate;
	}
	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}
	
	
	public String getOldId() {
		return oldId;
	}
	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "UrsMst [id=" + id + ", itemId=" + itemId + ", systemStock=" + systemStock + ", enteredStock="
				+ enteredStock + ", userId=" + userId + ", branchCode=" + branchCode + ", updatedTime=" + updatedTime
				+ ", transDate=" + transDate + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", oldId=" + oldId + "]";
	}
	
		
	
}
