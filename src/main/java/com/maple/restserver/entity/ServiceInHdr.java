package com.maple.restserver.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
public class ServiceInHdr {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	

		
	@Column(length = 50)	 
	private String voucherNumber;
	
	private Date voucherDate;
	@Column(length = 50)
	private String branchCode;
	 Date estimatedDeliveryDate;
	 Double estimatedCharge;
	 Double advanceAmount;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "accountHeads", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private  AccountHeads accountHeads;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public String getVoucherNumber() {
		return voucherNumber;
	}


	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}


	public Date getVoucherDate() {
		return voucherDate;
	}


	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}




	
	public Date getEstimatedDeliveryDate() {
		return estimatedDeliveryDate;
	}


	public void setEstimatedDeliveryDate(Date estimatedDeliveryDate) {
		this.estimatedDeliveryDate = estimatedDeliveryDate;
	}


	public Double getEstimatedCharge() {
		return estimatedCharge;
	}


	public void setEstimatedCharge(Double estimatedCharge) {
		this.estimatedCharge = estimatedCharge;
	}


	public Double getAdvanceAmount() {
		return advanceAmount;
	}


	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}


	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	public AccountHeads getAccountHeads() {
		return accountHeads;
	}


	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}


	@Override
	public String toString() {
		return "ServiceInHdr [id=" + id + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", branchCode=" + branchCode + ", estimatedDeliveryDate=" + estimatedDeliveryDate
				+ ", estimatedCharge=" + estimatedCharge + ", advanceAmount=" + advanceAmount + ", accountHeads="
				+ accountHeads + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}


}
