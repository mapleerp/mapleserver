package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;


import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class ActualProductionDtl implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	String itemId;
	@Column(length = 50)
	String batch;
	Double actualQty;
	
	@Column( nullable = false, columnDefinition = "double default 0.0") 
	Double productionCost;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;

	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "actualProductionHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ActualProductionHdr actualProductionHdr;
	
	
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "productionDtl", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	
	 ProductionDtl productionDtl;
	@Column(length = 50)

	private String  processInstanceId;
	@Column(length = 50)

	private String taskId;
	
	
	private String store;


	public ProductionDtl getProductionDtl() {
		return productionDtl;
	}


	public void setProductionDtl(ProductionDtl productionDtl) {
		this.productionDtl = productionDtl;
	}


	public String getId() {
		return id;
	}


	public Double getProductionCost() {
		return productionCost;
	}


	public void setProductionCost(Double productionCost) {
		this.productionCost = productionCost;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getItemId() {
		return itemId;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	

	public String getBatch() {
		return batch;
	}


	public void setBatch(String batch) {
		this.batch = batch;
	}


	public Double getActualQty() {
		return actualQty;
	}


	public void setActualQty(Double actualQty) {
		this.actualQty = actualQty;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public ActualProductionHdr getActualProductionHdr() {
		return actualProductionHdr;
	}


	public void setActualProductionHdr(ActualProductionHdr actualProductionHdr) {
		this.actualProductionHdr = actualProductionHdr;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	public String getStore() {
		return store;
	}


	public void setStore(String store) {
		this.store = store;
	}


	@Override
	public String toString() {
		return "ActualProductionDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", actualQty=" + actualQty
				+ ", productionCost=" + productionCost + ", companyMst=" + companyMst + ", actualProductionHdr="
				+ actualProductionHdr + ", productionDtl=" + productionDtl + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", store=" + store + "]";
	}

	
}
