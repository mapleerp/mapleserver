package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.message.entity.SalesDtlMessage;
import com.maple.restserver.message.entity.SalesTransHdrMessage;
@Component
public class SalesMessageEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	SalesTransHdr salesTransHdr;
	ArrayList<SalesDtl> salesDtlList =new ArrayList();
	ArrayList<SalesReceipts> salesReceiptsList =new ArrayList();
	ArrayList<AccountReceivable> accountReceivable = new ArrayList();
	 BranchMst branchMst;
	 @Column(length = 50)
	 private String  processInstanceId;
	 @Column(length = 50)
		private String taskId;


	public BranchMst getBranchMst() {
		return branchMst;
	}
	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}
	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}
	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}
	public ArrayList<SalesDtl> getSalesDtlList() {
		return salesDtlList;
	}
	public void setSalesDtlList(ArrayList<SalesDtl> salesDtlList) {
		this.salesDtlList = salesDtlList;
	}
	public ArrayList<SalesReceipts> getSalesReceiptsList() {
		return salesReceiptsList;
	}
	public void setSalesReceiptsList(ArrayList<SalesReceipts> salesReceiptsList) {
		this.salesReceiptsList = salesReceiptsList;
	}
	


	public ArrayList<AccountReceivable> getAccountReceivable() {
		return accountReceivable;
	}
	public void setAccountReceivable(ArrayList<AccountReceivable> accountReceivable) {
		this.accountReceivable = accountReceivable;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SalesMessageEntity [salesTransHdr=" + salesTransHdr + ", salesDtlList=" + salesDtlList
				+ ", salesReceiptsList=" + salesReceiptsList + ", accountReceivable=" + accountReceivable
				+ ", branchMst=" + branchMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	  
	
	
 
	
}
