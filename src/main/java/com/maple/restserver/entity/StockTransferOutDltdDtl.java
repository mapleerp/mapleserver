package com.maple.restserver.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class StockTransferOutDltdDtl implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String itemId;
	@Column(length = 50)
	private String unitId;
	@Column(length = 50)
	private Double qty;

	@Column(nullable = false, columnDefinition = "double default 0.0")
	Double mrp;
	@Column(length = 50)
	String branchCode;

	@UpdateTimestamp
	@Column(nullable = true)
	private LocalDateTime updatedTime;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "stockTransferOutHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	StockTransferOutHdr stockTransferOutHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}

	public StockTransferOutHdr getStockTransferOutHdr() {
		return stockTransferOutHdr;
	}

	public void setStockTransferOutHdr(StockTransferOutHdr stockTransferOutHdr) {
		this.stockTransferOutHdr = stockTransferOutHdr;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "StockTransferOutDltdDtl [id=" + id + ", companyMst=" + companyMst + ", itemId=" + itemId + ", unitId="
				+ unitId + ", qty=" + qty + ", mrp=" + mrp + ", branchCode=" + branchCode + ", updatedTime="
				+ updatedTime + ", stockTransferOutHdr=" + stockTransferOutHdr + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

}
