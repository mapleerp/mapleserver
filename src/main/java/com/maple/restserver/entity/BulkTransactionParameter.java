package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class BulkTransactionParameter implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String branchCode;
	Date fDate;
	Date tDate;
	String vouchertype;
	public String getBranchCode() {
		return branchCode;
	}
	public Date getfDate() {
		return fDate;
	}
	public Date gettDate() {
		return tDate;
	}
	public String getVouchertype() {
		return vouchertype;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setfDate(Date fDate) {
		this.fDate = fDate;
	}
	public void settDate(Date tDate) {
		this.tDate = tDate;
	}
	public void setVouchertype(String vouchertype) {
		this.vouchertype = vouchertype;
	}
	@Override
	public String toString() {
		return "BulkTransactionParameter [branchCode=" + branchCode + ", fDate=" + fDate + ", tDate=" + tDate
				+ ", vouchertype=" + vouchertype + "]";
	}
	
	
	
	
	
	
	

}
