package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.stereotype.Component;


@Component
@Entity
public class SchOfferAttrInst implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	
	private String id;
	@Column(length = 50)
	String schemeId ;
	@Column(length = 50)
	String offerId ;
	@Column(length = 50)
	String attributeName ; 
	@Column(length = 50)
	String attributeValue ; 
	@Column(length = 50)
	String attributeType ;
	@Column(length = 50)
	
	String branchCode;
	@Column(length = 50)
	
	
	String oldId ;

	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;


	
	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public String getSchemeId() {
		return schemeId;
	}


	public void setSchemeId(String schemeId) {
		this.schemeId = schemeId;
	}


	public String getOfferId() {
		return offerId;
	}


	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}


	public String getAttributeName() {
		return attributeName;
	}


	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}


	public String getAttributeValue() {
		return attributeValue;
	}


	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}


	public String getAttributeType() {
		return attributeType;
	}


	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getOldId() {
		return oldId;
	}


	public void setOldId(String oldId) {
		this.oldId = oldId;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "SchOfferAttrInst [id=" + id + ", schemeId=" + schemeId + ", offerId=" + offerId + ", attributeName="
				+ attributeName + ", attributeValue=" + attributeValue + ", attributeType=" + attributeType
				+ ", branchCode=" + branchCode + ", oldId=" + oldId + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

 
	
	
 
}
