package com.maple.restserver.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class PharmacyPurchaseReport {
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	
	@Column(length = 50)
	String trEntryDate;
	@Column(length = 50)
	String supplierName;
	@Column(length = 50)
	String ourVoucherDate;
	@Column(length = 50)
	String supplierVoucherDate;
	Double netInvoiceTotal;
	@Column(length = 50)
	String tinNo;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	public String getTrEntryDate() {
		return trEntryDate;
	}
	public void setTrEntryDate(String trEntryDate) {
		this.trEntryDate = trEntryDate;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getOurVoucherDate() {
		return ourVoucherDate;
	}
	public void setOurVoucherDate(String ourVoucherDate) {
		this.ourVoucherDate = ourVoucherDate;
	}
	public String getSupplierVoucherDate() {
		return supplierVoucherDate;
	}
	public void setSupplierVoucherDate(String supplierVoucherDate) {
		this.supplierVoucherDate = supplierVoucherDate;
	}
	public Double getNetInvoiceTotal() {
		return netInvoiceTotal;
	}
	public void setNetInvoiceTotal(Double netInvoiceTotal) {
		this.netInvoiceTotal = netInvoiceTotal;
	}
	public String getTinNo() {
		return tinNo;
	}
	public void setTinNo(String tinNo) {
		this.tinNo = tinNo;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PharmacyPurchaseReport [trEntryDate=" + trEntryDate + ", supplierName=" + supplierName
				+ ", ourVoucherDate=" + ourVoucherDate + ", supplierVoucherDate=" + supplierVoucherDate
				+ ", netInvoiceTotal=" + netInvoiceTotal + ", tinNo=" + tinNo + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
