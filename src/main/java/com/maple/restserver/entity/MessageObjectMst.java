package com.maple.restserver.entity;



import java.io.Serializable;
import java.sql.Date;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;



@Entity
public class MessageObjectMst implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	@Id
 @GeneratedValue(generator = "uuid")
 @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
 private String id;

	
	private String hrdId;

	@Lob
	byte[] messageEntity;
	
	String vouvcherNumber;
	
	Date voucherDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHrdId() {
		return hrdId;
	}

	public void setHrdId(String hrdId) {
		this.hrdId = hrdId;
	}

	public byte[] getMessageEntity() {
		return messageEntity;
	}

	public void setMessageEntity(byte[] messageEntity) {
		this.messageEntity = messageEntity;
	}

	public String getVouvcherNumber() {
		return vouvcherNumber;
	}

	public void setVouvcherNumber(String vouvcherNumber) {
		this.vouvcherNumber = vouvcherNumber;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	@Override
	public String toString() {
		return "MessageObjectMst [id=" + id + ", hrdId=" + hrdId + ", messageEntity=" + Arrays.toString(messageEntity)
				+ ", vouvcherNumber=" + vouvcherNumber + ", voucherDate=" + voucherDate + "]";
	}
	
	
	 
	
	

}
