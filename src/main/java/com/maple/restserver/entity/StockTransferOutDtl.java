package com.maple.restserver.entity;

 

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;



import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
 
import com.fasterxml.jackson.annotation.JsonIgnore;
 

@Entity
public class StockTransferOutDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	Integer slNo;
	@Column(length = 50 ,nullable = false)
	private String batch;
	private Double qty;
	private Double rate;
	@Column(length = 50)
	private String itemCode;
	private Double taxRate;
	private Date expiryDate;
	private Double amount;
	@Column(length = 50)
	private String unitId;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "itemId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	ItemMst itemId;
	
	
	private Double mrp;
	@Column(length = 50)
	private String barcode;
	@Column(length = 50)
	private String intentDtlId;
	Integer displaySerial;

	@Column(length = 50)
	private String storeName;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "stockTransferOutHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	StockTransferOutHdr stockTransferOutHdr;
	
	
	  @ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "companyMst", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		CompanyMst companyMst;
	  @Column(length = 50)
	  private String  processInstanceId;
	  @Column(length = 50)
		private String taskId;
	

	public StockTransferOutDtl() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	 
	

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getBatch() {
		return batch;
	}

	public String getIntentDtlId() {
		return intentDtlId;
	}

	public void setIntentDtlId(String intentDtlId) {
		this.intentDtlId = intentDtlId;
	}

	public void setBatch(String batchCode) {
		this.batch = batchCode;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public StockTransferOutHdr getStockTransferOutHdr() {
		return stockTransferOutHdr;
	}

	public void setStockTransferOutHdr(StockTransferOutHdr stockTransferOutHdr) {
		this.stockTransferOutHdr = stockTransferOutHdr;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	
	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	

	

	public Integer getDisplaySerial() {
		return displaySerial;
	}

	public void setDisplaySerial(Integer displaySerial) {
		this.displaySerial = displaySerial;
	}

 

 

	public ItemMst getItemId() {
		return itemId;
	}

	public void setItemId(ItemMst itemId) {
		this.itemId = itemId;
	}

	@Override
	public String toString() {
		return "StockTransferOutDtl [id=" + id + ", slNo=" + slNo + ", batch=" + batch + ", qty=" + qty + ", rate="
				+ rate + ", itemCode=" + itemCode + ", taxRate=" + taxRate + ", expiryDate=" + expiryDate + ", amount="
				+ amount + ", unitId=" + unitId + ", itemId=" + itemId + ", mrp=" + mrp + ", barcode=" + barcode
				+ ", intentDtlId=" + intentDtlId + ", displaySerial=" + displaySerial + ", storeName=" + storeName
				+ ", stockTransferOutHdr=" + stockTransferOutHdr + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

	

	 
}
