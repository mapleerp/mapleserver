package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class MenuConfigMst implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private String menuName;
	private String menuFxml;
	private String menuDescription;
	private String customiseSalesMode;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;


	private String oldId;

	
	public String getCustomiseSalesMode() {
		return customiseSalesMode;
	}

	public void setCustomiseSalesMode(String customiseSalesMode) {
		this.customiseSalesMode = customiseSalesMode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuFxml() {
		return menuFxml;
	}

	public void setMenuFxml(String menuFxml) {
		this.menuFxml = menuFxml;
	}

	public String getMenuDescription() {
		return menuDescription;
	}

	public void setMenuDescription(String menuDescription) {
		this.menuDescription = menuDescription;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "MenuConfigMst [id=" + id + ", menuName=" + menuName + ", menuFxml=" + menuFxml + ", menuDescription="
				+ menuDescription + ", customiseSalesMode=" + customiseSalesMode + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", oldId=" + oldId + "]";
	}
	
	
	

}
