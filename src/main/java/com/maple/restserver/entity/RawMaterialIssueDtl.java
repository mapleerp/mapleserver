package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class RawMaterialIssueDtl implements Serializable {


	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "rawMaterialIssueHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	RawMaterialIssueHdr rawMaterialIssueHdr;
	@Column(length = 50)
	String itemId;
	@Column(length = 50)
	String batch;
	@Column(length = 50)
	String unitId;
	Double qty;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public RawMaterialIssueHdr getRawMaterialIssueHdr() {
		return rawMaterialIssueHdr;
	}
	public void setRawMaterialIssueHdr(RawMaterialIssueHdr rawMaterialIssueHdr) {
		this.rawMaterialIssueHdr = rawMaterialIssueHdr;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "RawMaterialIssueDtl [id=" + id + ", rawMaterialIssueHdr=" + rawMaterialIssueHdr + ", itemId=" + itemId
				+ ", batch=" + batch + ", unitId=" + unitId + ", qty=" + qty + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
