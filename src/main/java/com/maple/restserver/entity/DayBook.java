package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

@Entity
public class DayBook implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")

	String id;
	@Column(length = 50)
	String voucheNumber;
	Date voucherDate;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	
	String sourceVoucheNumber;
	Date sourceVoucherDate;
	@Column(length = 50)
	String sourceVoucherType;
	@Type(type="text")
	String narration;
	
	Double drAmount;
	Double crAmount;
	@Column(length = 50)
	String crAccountName;
	@Column(length = 50)
	String drAccountName;
	@Column(length = 50)
	String branchCode;
	@Column(length = 50)
	String userId;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucheNumber() {
		return voucheNumber;
	}
	public void setVoucheNumber(String voucheNumber) {
		this.voucheNumber = voucheNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getSourceVoucheNumber() {
		return sourceVoucheNumber;
	}
	public void setSourceVoucheNumber(String sourceVoucheNumber) {
		this.sourceVoucheNumber = sourceVoucheNumber;
	}
	public Date getSourceVoucherDate() {
		return sourceVoucherDate;
	}
	public void setSourceVoucherDate(Date sourceVoucherDate) {
		this.sourceVoucherDate = sourceVoucherDate;
	}
	public String getSourceVoucherType() {
		return sourceVoucherType;
	}
	public void setSourceVoucherType(String sourceVoucherType) {
		this.sourceVoucherType = sourceVoucherType;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public Double getDrAmount() {
		return drAmount;
	}
	public void setDrAmount(Double drAmount) {
		this.drAmount = drAmount;
	}
	public Double getCrAmount() {
		return crAmount;
	}
	public void setCrAmount(Double crAmount) {
		this.crAmount = crAmount;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCrAccountName() {
		return crAccountName;
	}
	public void setCrAccountName(String crAccountName) {
		this.crAccountName = crAccountName;
	}
	public String getDrAccountName() {
		return drAccountName;
	}
	public void setDrAccountName(String drAccountName) {
		this.drAccountName = drAccountName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "DayBook [id=" + id + ", voucheNumber=" + voucheNumber + ", voucherDate=" + voucherDate + ", companyMst="
				+ companyMst + ", sourceVoucheNumber=" + sourceVoucheNumber + ", sourceVoucherDate=" + sourceVoucherDate
				+ ", sourceVoucherType=" + sourceVoucherType + ", narration=" + narration + ", drAmount=" + drAmount
				+ ", crAmount=" + crAmount + ", crAccountName=" + crAccountName + ", drAccountName=" + drAccountName
				+ ", branchCode=" + branchCode + ", userId=" + userId + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
}
