package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity

public class DepartmentMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue
	String Id;
	
	@Column(length = 50)
	String departmentName;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	@JsonProperty("UserId")
	String UserId;
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String getDeleted() {
		return Deleted;
	}
	public void setDeleted(String deleted) {
		Deleted = deleted;
	}
	@JsonProperty("Deleted")
	String Deleted;
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "DepartmentMst [Id=" + Id + ", departmentName=" + departmentName + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", UserId=" + UserId + ", Deleted=" + Deleted + "]";
	}
	
	
}
