package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
public class BarcodeConfigurationMst implements Serializable {


	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	   @GeneratedValue(generator = "uuid")
	   @GenericGenerator(name = "uuid", strategy = "uuid2")
	   private String id;
	@Column(length = 50)
       String branchCode;
	@Column(length = 50)
     	String barcodeX;
	@Column(length = 50)
	    String barcodeY;
	@Column(length = 50)
        String manufactureDateX;
	@Column(length = 50)
        String manufactureDateY;
	@Column(length = 50)
        String expiryDateX;
	@Column(length = 50)
        String expiryDateY;
	@Column(length = 50)
        String itemNameX;
	@Column(length = 50)
        String itemNameY;
	@Column(length = 50)
        String ingLine1X;
	@Column(length = 50)
        String ingLine1Y;
	@Column(length = 50)
        String ingLine2X;
	@Column(length = 50)
        String ingLine2Y;
	@Column(length = 50)
        String mrpX;
	@Column(length = 50)
        String mrpy;
	
	  @ManyToOne(fetch = FetchType.EAGER, optional = false)
	  @JoinColumn(name = "companyMst", nullable = false)
	  @OnDelete(action = OnDeleteAction.CASCADE)
	  CompanyMst companyMst;
	  @Column(length = 50)
	  private String  processInstanceId;
	  @Column(length = 50)
		private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBarcodeX() {
		return barcodeX;
	}

	public void setBarcodeX(String barcodeX) {
		this.barcodeX = barcodeX;
	}

	public String getBarcodeY() {
		return barcodeY;
	}

	public void setBarcodeY(String barcodeY) {
		this.barcodeY = barcodeY;
	}

	public String getManufactureDateX() {
		return manufactureDateX;
	}

	public void setManufactureDateX(String manufactureDateX) {
		this.manufactureDateX = manufactureDateX;
	}

	public String getManufactureDateY() {
		return manufactureDateY;
	}

	public void setManufactureDateY(String manufactureDateY) {
		this.manufactureDateY = manufactureDateY;
	}

	public String getExpiryDateX() {
		return expiryDateX;
	}

	public void setExpiryDateX(String expiryDateX) {
		this.expiryDateX = expiryDateX;
	}

	public String getExpiryDateY() {
		return expiryDateY;
	}

	public void setExpiryDateY(String expiryDateY) {
		this.expiryDateY = expiryDateY;
	}

	public String getItemNameX() {
		return itemNameX;
	}

	public void setItemNameX(String itemNameX) {
		this.itemNameX = itemNameX;
	}

	public String getItemNameY() {
		return itemNameY;
	}

	public void setItemNameY(String itemNameY) {
		this.itemNameY = itemNameY;
	}

	public String getIngLine1X() {
		return ingLine1X;
	}

	public void setIngLine1X(String ingLine1X) {
		this.ingLine1X = ingLine1X;
	}

	public String getIngLine1Y() {
		return ingLine1Y;
	}

	public void setIngLine1Y(String ingLine1Y) {
		this.ingLine1Y = ingLine1Y;
	}

	public String getIngLine2X() {
		return ingLine2X;
	}

	public void setIngLine2X(String ingLine2X) {
		this.ingLine2X = ingLine2X;
	}

	public String getIngLine2Y() {
		return ingLine2Y;
	}

	public void setIngLine2Y(String ingLine2Y) {
		this.ingLine2Y = ingLine2Y;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getMrpX() {
		return mrpX;
	}

	public void setMrpX(String mrpX) {
		this.mrpX = mrpX;
	}

	public String getMrpy() {
		return mrpy;
	}

	public void setMrpy(String mrpy) {
		this.mrpy = mrpy;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "BarcodeConfigurationMst [id=" + id + ", branchCode=" + branchCode + ", barcodeX=" + barcodeX
				+ ", barcodeY=" + barcodeY + ", manufactureDateX=" + manufactureDateX + ", manufactureDateY="
				+ manufactureDateY + ", expiryDateX=" + expiryDateX + ", expiryDateY=" + expiryDateY + ", itemNameX="
				+ itemNameX + ", itemNameY=" + itemNameY + ", ingLine1X=" + ingLine1X + ", ingLine1Y=" + ingLine1Y
				+ ", ingLine2X=" + ingLine2X + ", ingLine2Y=" + ingLine2Y + ", mrpX=" + mrpX + ", mrpy=" + mrpy
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}

	

	
}
