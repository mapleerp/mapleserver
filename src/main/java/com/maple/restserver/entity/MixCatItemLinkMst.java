package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class MixCatItemLinkMst implements Serializable{

	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	   @Id
	   @GeneratedValue(generator = "uuid")
	   @GenericGenerator(name = "uuid", strategy = "uuid2")
	   private String id;
	@Column(length = 50)
	   String itemId;
	@Column(length = 50)
	   String resourceCatId;
	
	   
		@ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "companyMst", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		CompanyMst companyMst;
		@Column(length = 50)
		private String  processInstanceId;
		@Column(length = 50)
		private String taskId;


		public String getId() {
			return id;
		}


		public void setId(String id) {
			this.id = id;
		}


		public String getItemId() {
			return itemId;
		}


		public void setItemId(String itemId) {
			this.itemId = itemId;
		}


		public String getResourceCatId() {
			return resourceCatId;
		}


		public void setResourceCatId(String resourceCatId) {
			this.resourceCatId = resourceCatId;
		}


		public CompanyMst getCompanyMst() {
			return companyMst;
		}


		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}


		public static long getSerialversionuid() {
			return serialVersionUID;
		}




		public String getProcessInstanceId() {
			return processInstanceId;
		}


		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}


		public String getTaskId() {
			return taskId;
		}


		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}


		@Override
		public String toString() {
			return "MixCatItemLinkMst [id=" + id + ", itemId=" + itemId + ", resourceCatId=" + resourceCatId
					+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
					+ "]";
		}
	
		
	
}
