package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Transient;
 
public class FCSummarySalesDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
     
	private Double totalQty;
    
	private Double fctotalAmount;
    
	private Double fctotalDiscount;
   
	private Double fctotalTax;
    
    private Double fctotalCessAmt;
    @Column(length = 50)
    private String  processInstanceId;
    @Column(length = 50)
	private String taskId;
   

	public Double getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(Double totalQty) {
		this.totalQty = totalQty;
	}
	public Double getFctotalAmount() {
		return fctotalAmount;
	}
	public void setFctotalAmount(Double fctotalAmount) {
		this.fctotalAmount = fctotalAmount;
	}
	public Double getFctotalDiscount() {
		return fctotalDiscount;
	}
	public void setFctotalDiscount(Double fctotalDiscount) {
		this.fctotalDiscount = fctotalDiscount;
	}
	public Double getFctotalTax() {
		return fctotalTax;
	}
	public void setFctotalTax(Double fctotalTax) {
		this.fctotalTax = fctotalTax;
	}
	public Double getFctotalCessAmt() {
		return fctotalCessAmt;
	}
	public void setFctotalCessAmt(Double fctotalCessAmt) {
		this.fctotalCessAmt = fctotalCessAmt;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "FCSummarySalesDtl [totalQty=" + totalQty + ", fctotalAmount=" + fctotalAmount + ", fctotalDiscount="
				+ fctotalDiscount + ", fctotalTax=" + fctotalTax + ", fctotalCessAmt=" + fctotalCessAmt
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


    
    
}
