package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class PDCReceipts implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
    	String id;
	@Column(length = 50)
		String account;
		Double amount;
		@Column(length = 50)
		String depositBank;
		String remark;
		@Column(length = 50)
		String instrumentnumber;
		@Column(length = 50)
		String bankAccountNumber;
		Date instrumentDate;
		Date transDate;
		@Column(length = 50)
		String instrumentBank;
		@Column(length = 50)
		String branchCode;
		@Column(length = 50)
		String status;

		
		@JsonIgnore
		@ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "companyMst", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		CompanyMst companyMst;
		@Column(length = 50)
		private String  processInstanceId;
		@Column(length = 50)
		private String taskId;
		
		

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getAccount() {
			return account;
		}

		public void setAccount(String account) {
			this.account = account;
		}

		public Double getAmount() {
			return amount;
		}

		public void setAmount(Double amount) {
			this.amount = amount;
		}

		public String getDepositBank() {
			return depositBank;
		}

		public void setDepositBank(String depositBank) {
			this.depositBank = depositBank;
		}

		public String getRemark() {
			return remark;
		}

		public void setRemark(String remark) {
			this.remark = remark;
		}

		public String getInstrumentnumber() {
			return instrumentnumber;
		}

		public void setInstrumentnumber(String instrumentnumber) {
			this.instrumentnumber = instrumentnumber;
		}

		public String getBankAccountNumber() {
			return bankAccountNumber;
		}

		public void setBankAccountNumber(String bankAccountNumber) {
			this.bankAccountNumber = bankAccountNumber;
		}

		public Date getInstrumentDate() {
			return instrumentDate;
		}

		public void setInstrumentDate(Date instrumentDate) {
			this.instrumentDate = instrumentDate;
		}

		public Date getTransDate() {
			return transDate;
		}

		public void setTransDate(Date transDate) {
			this.transDate = transDate;
		}

		public String getInstrumentBank() {
			return instrumentBank;
		}

		public void setInstrumentBank(String instrumentBank) {
			this.instrumentBank = instrumentBank;
		}

		public String getBranchCode() {
			return branchCode;
		}

		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}

		public CompanyMst getCompanyMst() {
			return companyMst;
		}

		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}

		public String getProcessInstanceId() {
			return processInstanceId;
		}

		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}

		public String getTaskId() {
			return taskId;
		}

		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}

		@Override
		public String toString() {
			return "PDCReceipts [id=" + id + ", account=" + account + ", amount=" + amount + ", depositBank="
					+ depositBank + ", remark=" + remark + ", instrumentnumber=" + instrumentnumber
					+ ", bankAccountNumber=" + bankAccountNumber + ", instrumentDate=" + instrumentDate + ", transDate="
					+ transDate + ", instrumentBank=" + instrumentBank + ", branchCode=" + branchCode + ", status="
					+ status + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId="
					+ taskId + "]";
		}
		
		
		

}
