package com.maple.restserver.entity;

import java.util.Date;

import javax.persistence.Column;

public class ReceiptReport {

	Date voucherDate;
	@Column(length = 50)
	String voucherNumber;
	@Column(length = 50)
	String branchCode;
	
	Date transdate;
	@Column(length = 50)
	String voucherType;
	@Column(length = 50)
	String memberId;
	@Column(length = 50)
	String id;
	@Column(length = 50)
	String bankAccountNumber;
	@Column(length = 50)
	String account;
	@Column(length = 50)
	String modeOfpayment;
	Double amount;
	@Column(length = 50)
	String depositBank;
	String remark;
	@Column(length = 50)
	String instnumber;
	@Column(length = 50)
	String invoiceNumber;
	@Column(length = 50)
	String accountName;
	@Column(length = 50)
	String debitAccountId;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Date getTransdate() {
		return transdate;
	}
	public void setTransdate(Date transdate) {
		this.transdate = transdate;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getModeOfpayment() {
		return modeOfpayment;
	}
	public void setModeOfpayment(String modeOfpayment) {
		this.modeOfpayment = modeOfpayment;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getDepositBank() {
		return depositBank;
	}
	public void setDepositBank(String depositBank) {
		this.depositBank = depositBank;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getInstnumber() {
		return instnumber;
	}
	public void setInstnumber(String instnumber) {
		this.instnumber = instnumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getDebitAccountId() {
		return debitAccountId;
	}
	public void setDebitAccountId(String debitAccountId) {
		this.debitAccountId = debitAccountId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ReceiptReport [voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber + ", branchCode="
				+ branchCode + ", transdate=" + transdate + ", voucherType=" + voucherType + ", memberId=" + memberId
				+ ", id=" + id + ", bankAccountNumber=" + bankAccountNumber + ", account=" + account
				+ ", modeOfpayment=" + modeOfpayment + ", amount=" + amount + ", depositBank=" + depositBank
				+ ", remark=" + remark + ", instnumber=" + instnumber + ", invoiceNumber=" + invoiceNumber
				+ ", accountName=" + accountName + ", debitAccountId=" + debitAccountId + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
}
