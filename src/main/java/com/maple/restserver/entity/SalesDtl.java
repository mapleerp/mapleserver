
package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
 
 
 

@Entity
public class SalesDtl implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	 
	@Column(nullable=false)
	String itemId;
	Double qty;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double rate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double cgstTaxRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double sgstTaxRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double cessRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double addCessRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double igstTaxRate;
	 
	String itemTaxaxId;
	@Column(nullable=false)
	String unitId;
	String itemName;
	Date expiryDate;
	@Column(nullable=false)
	String batch;
	@Column(length = 50)
	String barcode;

	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double taxRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double mrp;

	@Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double amount;
	String unitName;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
    Double discount;
    @Column(  nullable = false, columnDefinition = "double default 0.0") 
    Double cgstAmount;
    @Column(  nullable = false, columnDefinition = "double default 0.0") 
    Double sgstAmount;
    @Column(  nullable = false, columnDefinition = "double default 0.0") 
    Double igstAmount;
    
    @Column( nullable = false, columnDefinition = "double default 0.0") 
    Double cessAmount;
    Double returnedQty;
    String status;
    Double addCessAmount;
    Double costPrice;
    String printKotStatus;
    Double fcAmount;
    Double fcRate;
    Double fcIgstRate;
    Double fcCgst;
    Double fcSgst;
    Double fcTaxRate;
    Double fcTaxAmount;
    Double fcCessRate;
    Double fcCessAmount;
    Double fcMrp;
    Double fcStandardPrice;
    Double fcDiscount;
    Double fcIgstAmount;
    Double rateBeforeDiscount;
  //-------------------------------new version 1.11 surya 
  	String kotDescription;
  	//-------------------------------new version 1.11 surya  end
  	@Column(length = 50)
  	String offerReferenceId;
  	@Column(length = 50)
	String schemeId;
	 
	Double standardPrice;
	 
	String fbKey;
	@Column(length = 50)
	String warrantySerial;

    Double listPrice;
    @Column(length = 50)
    private String  processInstanceId;
    @Column(length = 50)
	private String taskId;
    @Column(length = 50)
	
	private String oldId;

    @JsonIgnore
    @UpdateTimestamp
	@Column(nullable=true)
	private LocalDateTime updatedTime;
	 	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "sales_trans_hdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private SalesTransHdr salesTransHdr;


	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	String store;
	String kotId;
	
	
	
	public String getStore() {
		return store;
	}


	public void setStore(String store) {
		this.store = store;
	}


	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}


	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}


	


	public Double getRateBeforeDiscount() {
		return rateBeforeDiscount;
	}


	public void setRateBeforeDiscount(Double rateBeforeDiscount) {
		this.rateBeforeDiscount = rateBeforeDiscount;
	}


	public String getPrintKotStatus() {
		return printKotStatus;
	}


	public void setPrintKotStatus(String printKotStatus) {
		this.printKotStatus = printKotStatus;
	}


	public Double getStandardPrice() {
		return standardPrice;
	}


	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}


	


	public String getSchemeId() {
		return schemeId;
	}


	public void setSchemeId(String schemeId) {
		this.schemeId = schemeId;
	}


	public Double getFcAmount() {
		return fcAmount;
	}


	public void setFcAmount(Double fcAmount) {
		this.fcAmount = fcAmount;
	}


	public Double getFcRate() {
		return fcRate;
	}


	public void setFcRate(Double fcRate) {
		this.fcRate = fcRate;
	}



	public Double getFcIgstRate() {
		return fcIgstRate;
	}


	public void setFcIgstRate(Double fcIgstRate) {
		this.fcIgstRate = fcIgstRate;
	}


	public Double getFcIgstAmount() {
		return fcIgstAmount;
	}


	public void setFcIgstAmount(Double fcIgstAmount) {
		this.fcIgstAmount = fcIgstAmount;
	}


	public Double getFcCgst() {
		return fcCgst;
	}


	public void setFcCgst(Double fcCgst) {
		this.fcCgst = fcCgst;
	}


	public Double getFcSgst() {
		return fcSgst;
	}


	public void setFcSgst(Double fcSgst) {
		this.fcSgst = fcSgst;
	}


	public Double getFcTaxRate() {
		return fcTaxRate;
	}


	public void setFcTaxRate(Double fcTaxRate) {
		this.fcTaxRate = fcTaxRate;
	}


	public Double getFcTaxAmount() {
		return fcTaxAmount;
	}


	public void setFcTaxAmount(Double fcTaxAmount) {
		this.fcTaxAmount = fcTaxAmount;
	}


	public Double getFcCessRate() {
		return fcCessRate;
	}


	public void setFcCessRate(Double fcCessRate) {
		this.fcCessRate = fcCessRate;
	}


	public Double getFcCessAmount() {
		return fcCessAmount;
	}


	public void setFcCessAmount(Double fcCessAmount) {
		this.fcCessAmount = fcCessAmount;
	}


	public Double getFcMrp() {
		return fcMrp;
	}


	public void setFcMrp(Double fcMrp) {
		this.fcMrp = fcMrp;
	}


	public Double getFcStandardPrice() {
		return fcStandardPrice;
	}


	public void setFcStandardPrice(Double fcStandardPrice) {
		this.fcStandardPrice = fcStandardPrice;
	}


	public Double getFcDiscount() {
		return fcDiscount;
	}


	public void setFcDiscount(Double fcDiscount) {
		this.fcDiscount = fcDiscount;
	}


	public Double getAddCessAmount() {
		return addCessAmount;
	}


	public void setAddCessAmount(Double addCessAmount) {
		this.addCessAmount = addCessAmount;
	}


	public String getOfferReferenceId() {
		return offerReferenceId;
	}


	public void setOfferReferenceId(String offerReferenceId) {
		this.offerReferenceId = offerReferenceId;
	}


	public SalesDtl()
	{
		
		
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getItemId() {
		return itemId;
	}


	


	public Double getReturnedQty() {
		return returnedQty;
	}


	public void setReturnedQty(Double returnedQty) {
		this.returnedQty = returnedQty;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	public Double getQty() {
		return qty;
	}


	public void setQty(Double qty) {
		this.qty = qty;
	}


	public Double getRate() {
		return rate;
	}


	public void setRate(Double rate) {
		this.rate = rate;
	}


	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}


	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}


	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}


	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}


	public Double getCessRate() {
		return cessRate;
	}


	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}


	public Double getAddCessRate() {
		return addCessRate;
	}


	public void setAddCessRate(Double addCessRate) {
		this.addCessRate = addCessRate;
	}


	public String getItemTaxaxId() {
		return itemTaxaxId;
	}


	public void setItemTaxaxId(String itemTaxaxId) {
		this.itemTaxaxId = itemTaxaxId;
	}


	public String getUnitId() {
		return unitId;
	}


	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public Date getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}


	 

	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}


	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}


 


	public Double getMrp() {
		return mrp;
	}


	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}


	public Double getTaxRate() {
		return taxRate;
	}


	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
 

	public String getUnitName() {
		return unitName;
	}


	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}


	 

	public String getBatch() {
		return batch;
	}


	public void setBatch(String batch) {
		this.batch = batch;
	}


	public String getBarcode() {
		return barcode;
	}


	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}


 


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public Double getDiscount() {
		return discount;
	}


	public void setDiscount(Double discount) {
		this.discount = discount;
	}


	public Double getIgstTaxRate() {
		return igstTaxRate;
	}


	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}


	public Double getCgstAmount() {
		return cgstAmount;
	}


	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}


	public Double getSgstAmount() {
		return sgstAmount;
	}


	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}


	public Double getIgstAmount() {
		return igstAmount;
	}


	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}




	public Double getCessAmount() {
		return cessAmount;
	}


	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}


	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}

	

	public Double getCostPrice() {
		return costPrice;
	}


	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}


	public String getFbKey() {
		return fbKey;
	}


	public void setFbKey(String fbKey) {
		this.fbKey = fbKey;
	}

	
	

	public String getWarrantySerial() {
		return warrantySerial;
	}


	public void setWarrantySerial(String warrantySerial) {
		this.warrantySerial = warrantySerial;
	}


	  //-------------------------------new version 1.11 surya 

	public String getKotDescription() {
		return kotDescription;
	}


	public void setKotDescription(String kotDescription) {
		this.kotDescription = kotDescription;
	}
	  //-------------------------------new version 1.11 surya 


	

	public Double getListPrice() {
		return listPrice;
	}


	public void setListPrice(Double listPrice) {
		this.listPrice = listPrice;
	}


	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	public String getOldId() {
		return oldId;
	}


	public void setOldId(String oldId) {
		
		this.oldId = oldId;
	}


	public String getKotId() {
		return kotId;
	}


	public void setKotId(String kotId) {
		this.kotId = kotId;
	}


	@Override
	public String toString() {
		return "SalesDtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", rate=" + rate + ", cgstTaxRate="
				+ cgstTaxRate + ", sgstTaxRate=" + sgstTaxRate + ", cessRate=" + cessRate + ", addCessRate="
				+ addCessRate + ", igstTaxRate=" + igstTaxRate + ", itemTaxaxId=" + itemTaxaxId + ", unitId=" + unitId
				+ ", itemName=" + itemName + ", expiryDate=" + expiryDate + ", batch=" + batch + ", barcode=" + barcode
				+ ", taxRate=" + taxRate + ", mrp=" + mrp + ", amount=" + amount + ", unitName=" + unitName
				+ ", discount=" + discount + ", cgstAmount=" + cgstAmount + ", sgstAmount=" + sgstAmount
				+ ", igstAmount=" + igstAmount + ", cessAmount=" + cessAmount + ", returnedQty=" + returnedQty
				+ ", status=" + status + ", addCessAmount=" + addCessAmount + ", costPrice=" + costPrice
				+ ", printKotStatus=" + printKotStatus + ", fcAmount=" + fcAmount + ", fcRate=" + fcRate
				+ ", fcIgstRate=" + fcIgstRate + ", fcCgst=" + fcCgst + ", fcSgst=" + fcSgst + ", fcTaxRate="
				+ fcTaxRate + ", fcTaxAmount=" + fcTaxAmount + ", fcCessRate=" + fcCessRate + ", fcCessAmount="
				+ fcCessAmount + ", fcMrp=" + fcMrp + ", fcStandardPrice=" + fcStandardPrice + ", fcDiscount="
				+ fcDiscount + ", fcIgstAmount=" + fcIgstAmount + ", rateBeforeDiscount=" + rateBeforeDiscount
				+ ", kotDescription=" + kotDescription + ", offerReferenceId=" + offerReferenceId + ", schemeId="
				+ schemeId + ", standardPrice=" + standardPrice + ", fbKey=" + fbKey + ", warrantySerial="
				+ warrantySerial + ", listPrice=" + listPrice + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + ", oldId=" + oldId + ", updatedTime=" + updatedTime + ", salesTransHdr=" + salesTransHdr
				+ ", companyMst=" + companyMst + ", store=" + store + ", kotId=" + kotId + "]";
	}


	

	

	

}
