
package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class PayableDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	Integer id;
	@Column(length = 50)
	String Entry;
	
	Double debitAmount;
	Double creditAmount;
	Double closingBalance;
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "payable_hdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	PayableHdr payableHdr;
	
	

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "account_heads_id", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	AccountHeads  accountHeads;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public Integer getId() {
		return id;
	}
	
	public String getEntry() {
		return Entry;
	}
	public void setEntry(String entry) {
		Entry = entry;
	}

	public Double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public Double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Double getClosingBalance() {
		return closingBalance;
	}

	public void setClosingBalance(Double closingBalance) {
		this.closingBalance = closingBalance;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	

	public PayableHdr getPayableHdr() {
		return payableHdr;
	}

	public void setPayableHdr(PayableHdr payableHdr) {
		this.payableHdr = payableHdr;
	}

	

	

	public AccountHeads getAccountHeads() {
		return accountHeads;
	}

	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "PayableDtl [id=" + id + ", Entry=" + Entry + ", debitAmount=" + debitAmount + ", creditAmount="
				+ creditAmount + ", closingBalance=" + closingBalance + ", payableHdr=" + payableHdr + ", accountHeads="
				+ accountHeads + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	



	
	
	

}
