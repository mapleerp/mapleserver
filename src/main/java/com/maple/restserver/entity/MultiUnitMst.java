package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class MultiUnitMst implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	private String id;
	@Column(length = 50)
	private String itemId;
	@Column(length = 50)
	private String unit1;
	@Column(length = 50)
	private String unit2;
	@Column(length = 50)
	private Double qty1;
	@Column(length = 50)
	private Double qty2;
	@Column(length = 50)
	private String barCode;
	private Double price;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	String branchCode;
	
	@Column(length = 50)
	String oldId;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUnit1() {
		return unit1;
	}
	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}
	public String getUnit2() {
		return unit2;
	}
	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}
	public Double getQty1() {
		return qty1;
	}
	public void setQty1(Double qty1) {
		this.qty1 = qty1;
	}
	public Double getQty2() {
		return qty2;
	}
	public void setQty2(Double qty2) {
		this.qty2 = qty2;
	}
	
	
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	
	
	public String getOldId() {
		return oldId;
	}
	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "MultiUnitMst [id=" + id + ", itemId=" + itemId + ", unit1=" + unit1 + ", unit2=" + unit2 + ", qty1="
				+ qty1 + ", qty2=" + qty2 + ", barCode=" + barCode + ", price=" + price + ", companyMst=" + companyMst
				+ ", branchCode=" + branchCode + ", oldId=" + oldId + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
}
