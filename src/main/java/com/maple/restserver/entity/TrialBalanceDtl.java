package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
@Entity
public class TrialBalanceDtl implements Serializable {


	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	String accountId;
	Double creditAmount;
	Double debitAmount;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "trialBalanceHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	TrialBalanceHdr trialBalanceHdr;

//	@ManyToOne(fetch = FetchType.EAGER, optional = false)
//	@JoinColumn(name = "companyMst", nullable = false)
//	@OnDelete(action = OnDeleteAction.CASCADE)
//	private CompanyMst companyMst;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public TrialBalanceHdr getTrialBalanceHdr() {
		return trialBalanceHdr;
	}

	public void setTrialBalanceHdr(TrialBalanceHdr trialBalanceHdr) {
		this.trialBalanceHdr = trialBalanceHdr;
	}

//	public CompanyMst getCompanyMst() {
//		return companyMst;
//	}
//
//	public void setCompanyMst(CompanyMst companyMst) {
//		this.companyMst = companyMst;
//	}
	
	
	
	
}
