package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class ItemMergeMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	String fromItemName;
	@Column(length = 50)
	String toItemName;
	@Column(length = 50)
    String fromItemId;
	@Column(length = 50)
	String toItemId;
	@Column(length = 50)
	String toItemBarCode;
	@Column(length = 50)
	String fromItemBarCode;
	@Column(length = 50)
	String userId;
	Date voucherDate;
	@Column(length = 50)
	String branchCode;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFromItemName() {
		return fromItemName;
	}
	public void setFromItemName(String fromItemName) {
		this.fromItemName = fromItemName;
	}
	public String getToItemName() {
		return toItemName;
	}
	public void setToItemName(String toItemName) {
		this.toItemName = toItemName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getFromItemId() {
		return fromItemId;
	}
	public void setFromItemId(String fromItemId) {
		this.fromItemId = fromItemId;
	}
	public String getToItemId() {
		return toItemId;
	}
	public void setToItemId(String toItemId) {
		this.toItemId = toItemId;
	}
	public String getToItemBarCode() {
		return toItemBarCode;
	}
	public void setToItemBarCode(String toItemBarCode) {
		this.toItemBarCode = toItemBarCode;
	}
	public String getFromItemBarCode() {
		return fromItemBarCode;
	}
	public void setFromItemBarCode(String fromItemBarCode) {
		this.fromItemBarCode = fromItemBarCode;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ItemMergeMst [id=" + id + ", fromItemName=" + fromItemName + ", toItemName=" + toItemName
				+ ", fromItemId=" + fromItemId + ", toItemId=" + toItemId + ", toItemBarCode=" + toItemBarCode
				+ ", fromItemBarCode=" + fromItemBarCode + ", userId=" + userId + ", voucherDate=" + voucherDate
				+ ", branchCode=" + branchCode + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
}
