package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class OrderTakerMst implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)


	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	String orderTakerName;
	@Column(length = 50)
	String branchCode;
	@Column(length = 50)
	String status;
	@Column(length = 50)
	String oldId;


	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	@Column(length = 50)

	private String  processInstanceId;
	@Column(length = 50)

	private String taskId;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getOrderTakerName() {
		return orderTakerName;
	}
	public void setOrderTakerName(String orderTakerName) {
		this.orderTakerName = orderTakerName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	
	public String getOldId() {
		return oldId;
	}
	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "OrderTakerMst [id=" + id + ", orderTakerName=" + orderTakerName + ", branchCode=" + branchCode
				+ ", status=" + status + ", oldId=" + oldId + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	

}
