package com.maple.restserver.entity;

import java.io.Serializable;

 
public class SchEligibilityId implements Serializable {
	private static final long serialVersionUID = 1L;
 
	
	  private String id;

	    private String attributeName;


	    private String schemeId;
	    
	    public SchEligibilityId(){
	    	
	    }
	   

	    public SchEligibilityId(String id, String attributeName, String schemeId ) {
	        this.id = id;
	        this.attributeName = attributeName;
	        this.schemeId = schemeId;
	    }

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((attributeName == null) ? 0 : attributeName.hashCode());
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			result = prime * result + ((schemeId == null) ? 0 : schemeId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			SchEligibilityId other = (SchEligibilityId) obj;
			if (attributeName == null) {
				if (other.attributeName != null)
					return false;
			} else if (!attributeName.equals(other.attributeName))
				return false;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			if (schemeId == null) {
				if (other.schemeId != null)
					return false;
			} else if (!schemeId.equals(other.schemeId))
				return false;
			return true;
		}
	

}
