package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
public class SchEligiAttrListDef implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id

	private String id;
	@Column(length = 50)
	String  eligibilityId ;
	@Column(length = 50)
	String attribName ;
	@Column(length = 50)
	String attribType ;
	@Column(length = 50)
	String branchCode ;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;


	
	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getEligibilityId() {
		return eligibilityId;
	}


	public void setEligibilityId(String eligibilityId) {
		this.eligibilityId = eligibilityId;
	}


	public String getAttribName() {
		return attribName;
	}


	public void setAttribName(String attribName) {
		this.attribName = attribName;
	}


	public String getAttribType() {
		return attribType;
	}


	public void setAttribType(String attribType) {
		this.attribType = attribType;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	

	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "SchEligiAttrListDef [id=" + id + ", eligibilityId=" + eligibilityId + ", attribName=" + attribName
				+ ", attribType=" + attribType + ", branchCode=" + branchCode + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


	

	
}
