package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/*
 * mapping table to connect sales Order receipts and
 * Sales trans Hdr.
 * Fort each invoice against advance will be inserted into this table
 * 
 */
@Entity
public class SOReceiptsSTHdrMap implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
 	String Id;
	@Column(length = 50)
	String salesOrderReceiptsId;
	@Column(length = 50)
	String salesTransHdrId;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getSalesOrderReceiptsId() {
		return salesOrderReceiptsId;
	}
	public void setSalesOrderReceiptsId(String salesOrderReceiptsId) {
		this.salesOrderReceiptsId = salesOrderReceiptsId;
	}
	public String getSalesTransHdrId() {
		return salesTransHdrId;
	}
	public void setSalesTransHdrId(String salesTransHdrId) {
		this.salesTransHdrId = salesTransHdrId;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SOReceiptsSTHdrMap [Id=" + Id + ", salesOrderReceiptsId=" + salesOrderReceiptsId + ", salesTransHdrId="
				+ salesTransHdrId + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	

}
