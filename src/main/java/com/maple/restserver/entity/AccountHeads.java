

package com.maple.restserver.entity;

import java.io.Serializable;
import java.security.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
  public class AccountHeads implements Serializable {
		private static final long serialVersionUID = 1L;
		@Column(length = 50)

	@Id
    String id;
	
	@Column(unique = true)

	String accountName;
	@Column(length = 50)

	String parentId;
	@Column(length = 50)

	String groupOnly;
	@Column(length = 50)

	String machineId;
	@Column(length = 50)

	String taxId;
	@Column(length = 50)

	String oldId;
	@Column(length = 50)



	String voucherType;
	@Column(length = 50)

	String currencyId;
	@Column(length = 50)

	String rootParentId;
	
	
	Integer serialNumber;

	@Column(length = 50)

	private String  processInstanceId;
	@Column(length = 50)

	private String taskId;
	
	private String customerPin;
	private String customerPlace;
	
	
	
	
	@Column(length = 50)
	String partyAddress1;
	@Column(length = 50)
	String partyAddress2;
	@Column(length = 50)
	String partyMail;
	@Column(length = 50)
	String partyGst;
	@Column(length = 50)
	String customerContact;
	
	@Column(length = 50)
	private String priceTypeId;
	@Column(length = 50)
	private Integer creditPeriod;
	@Column(length = 50)
	private Double customerDiscount;
	@Column(length = 50)
	private String customerCountry;
	@Column(length = 50)
	private String customerGstType;
	@Column(length = 50)
	private String customerType;
	@Column(length = 50)
	private String discountProperty;
	@Column(length = 50)
	private String bankName;
	@Column(length = 50)
	String taxInvoiceFormat;
	@Column(length = 50)
	private String  bankAccountName;
	@Column(length = 50)
	private String bankIfsc;
	@Column(length = 50)
	String drugLicenseNumber;
    String customerGroup;
	private String customerState;
	String companyName;
	private Integer customerRank;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	@Column(length = 50)
	String customerOrSupplier;

	public String getId() {
		return id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAccountName() {
		return accountName;
	}

	public String getParentId() {
		return parentId;
	}

	public String getGroupOnly() {
		return groupOnly;
	}

	public String getMachineId() {
		return machineId;
	}

	public String getTaxId() {
		return taxId;
	}

	public String getOldId() {
		return oldId;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public String getRootParentId() {
		return rootParentId;
	}

	public Integer getSerialNumber() {
		return serialNumber;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public String getPartyAddress1() {
		return partyAddress1;
	}

	public String getCustomerState() {
		return customerState;
	}

	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}

	public String getPartyAddress2() {
		return partyAddress2;
	}

	public String getPartyMail() {
		return partyMail;
	}

	public String getPartyGst() {
		return partyGst;
	}

	public String getCustomerGroup() {
		return customerGroup;
	}

	public void setCustomerGroup(String customerGroup) {
		this.customerGroup = customerGroup;
	}

	public String getCustomerContact() {
		return customerContact;
	}

	public String getPriceTypeId() {
		return priceTypeId;
	}

	public Integer getCreditPeriod() {
		return creditPeriod;
	}

	public Double getCustomerDiscount() {
		return customerDiscount;
	}

	public String getCustomerCountry() {
		return customerCountry;
	}

	public String getCustomerGstType() {
		return customerGstType;
	}

	public String getCustomerType() {
		return customerType;
	}

	public String getDiscountProperty() {
		return discountProperty;
	}

	public String getBankName() {
		return bankName;
	}

	public String getTaxInvoiceFormat() {
		return taxInvoiceFormat;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public String getBankIfsc() {
		return bankIfsc;
	}

	public String getDrugLicenseNumber() {
		return drugLicenseNumber;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public String getCustomerOrSupplier() {
		return customerOrSupplier;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public void setGroupOnly(String groupOnly) {
		this.groupOnly = groupOnly;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public void setRootParentId(String rootParentId) {
		this.rootParentId = rootParentId;
	}

	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public void setPartyAddress1(String partyAddress1) {
		this.partyAddress1 = partyAddress1;
	}

	public void setPartyAddress2(String partyAddress2) {
		this.partyAddress2 = partyAddress2;
	}

	public void setPartyMail(String partyMail) {
		this.partyMail = partyMail;
	}

	public void setPartyGst(String partyGst) {
		this.partyGst = partyGst;
	}

	public void setCustomerContact(String customerContact) {
		this.customerContact = customerContact;
	}

	public void setPriceTypeId(String priceTypeId) {
		this.priceTypeId = priceTypeId;
	}

	public void setCreditPeriod(Integer creditPeriod) {
		this.creditPeriod = creditPeriod;
	}

	public void setCustomerDiscount(Double customerDiscount) {
		this.customerDiscount = customerDiscount;
	}

	public void setCustomerCountry(String customerCountry) {
		this.customerCountry = customerCountry;
	}

	public void setCustomerGstType(String customerGstType) {
		this.customerGstType = customerGstType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public void setDiscountProperty(String discountProperty) {
		this.discountProperty = discountProperty;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public void setTaxInvoiceFormat(String taxInvoiceFormat) {
		this.taxInvoiceFormat = taxInvoiceFormat;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public void setBankIfsc(String bankIfsc) {
		this.bankIfsc = bankIfsc;
	}

	public void setDrugLicenseNumber(String drugLicenseNumber) {
		this.drugLicenseNumber = drugLicenseNumber;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public void setCustomerOrSupplier(String customerOrSupplier) {
		this.customerOrSupplier = customerOrSupplier;
	}

	public Integer getCustomerRank() {
		return customerRank;
	}

	public void setCustomerRank(Integer customerRank) {
		this.customerRank = customerRank;
	}
	
	

	public String getCustomerPin() {
		return customerPin;
	}

	public void setCustomerPin(String customerPin) {
		this.customerPin = customerPin;
	}

	public String getCustomerPlace() {
		return customerPlace;
	}

	public void setCustomerPlace(String customerPlace) {
		this.customerPlace = customerPlace;
	}

	@Override
	public String toString() {
		return "AccountHeads [id=" + id + ", accountName=" + accountName + ", parentId=" + parentId + ", groupOnly="
				+ groupOnly + ", machineId=" + machineId + ", taxId=" + taxId + ", oldId=" + oldId + ", voucherType="
				+ voucherType + ", currencyId=" + currencyId + ", rootParentId=" + rootParentId + ", serialNumber="
				+ serialNumber + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", customerPin="
				+ customerPin + ", customerPlace=" + customerPlace + ", partyAddress1=" + partyAddress1
				+ ", partyAddress2=" + partyAddress2 + ", partyMail=" + partyMail + ", partyGst=" + partyGst
				+ ", customerContact=" + customerContact + ", priceTypeId=" + priceTypeId + ", creditPeriod="
				+ creditPeriod + ", customerDiscount=" + customerDiscount + ", customerCountry=" + customerCountry
				+ ", customerGstType=" + customerGstType + ", customerType=" + customerType + ", discountProperty="
				+ discountProperty + ", bankName=" + bankName + ", taxInvoiceFormat=" + taxInvoiceFormat
				+ ", bankAccountName=" + bankAccountName + ", bankIfsc=" + bankIfsc + ", drugLicenseNumber="
				+ drugLicenseNumber + ", customerGroup=" + customerGroup + ", customerState=" + customerState
				+ ", companyName=" + companyName + ", customerRank=" + customerRank + ", companyMst=" + companyMst
				+ ", customerOrSupplier=" + customerOrSupplier + "]";
	}

	
	
	

	

	

}
