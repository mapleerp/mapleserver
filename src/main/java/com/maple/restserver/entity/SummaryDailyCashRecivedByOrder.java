package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;

public class SummaryDailyCashRecivedByOrder implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
 	private String customerName;
	Double amount;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SummaryDailyCashRecivedByOrder [customerName=" + customerName + ", amount=" + amount
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
