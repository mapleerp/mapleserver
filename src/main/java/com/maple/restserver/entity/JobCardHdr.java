package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class JobCardHdr implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 50)
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	@Column(length = 50)
	private String voucherNumber;
	private Date voucherDate;
	@Column(length = 50)
	private String customerId;
	private String status;
	@Column(length = 50)
	private String branchCode;
	@Column(length = 50)
	private String salesTransHdrId;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "serviceInDtl", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ServiceInDtl serviceInDtl;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	public String getSalesTransHdrId() {
		return salesTransHdrId;
	}

	public void setSalesTransHdrId(String salesTransHdrId) {
		this.salesTransHdrId = salesTransHdrId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public ServiceInDtl getServiceInDtl() {
		return serviceInDtl;
	}

	public void setServiceInDtl(ServiceInDtl serviceInDtl) {
		this.serviceInDtl = serviceInDtl;
	}
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "JobCardHdr [id=" + id + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", customerId=" + customerId + ", status=" + status + ", branchCode=" + branchCode
				+ ", salesTransHdrId=" + salesTransHdrId + ", companyMst=" + companyMst + ", serviceInDtl="
				+ serviceInDtl + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


	
	
	


}
