
package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity

public class RoomBedMst implements Serializable {


	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	Integer Id;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public Integer getId() {
		return Id;
	}
	@JsonProperty("UserId")
	@Column(length = 50)
	String UserId;
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String getDeleted() {
		return Deleted;
	}
	public void setDeleted(String deleted) {
		Deleted = deleted;
	}
	@JsonProperty("Deleted")
	@Column(length = 50)
	String Deleted;
	

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "RoomBedMst [Id=" + Id + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", UserId="
				+ UserId + ", Deleted=" + Deleted + "]";
	}
	
}
