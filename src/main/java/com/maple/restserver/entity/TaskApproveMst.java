package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.Valid;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class TaskApproveMst implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2") 
	String id;
	@Column(length = 50)
	String voucherType;
	@Column(length = 50)
	String voucherNumber;
	String reason;
	@Column(length = 50)
	String instanceId;
	public String getInstanceId() {
		return instanceId;
	}



	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}


	String comment;
	@Column(length = 50)
	String userId;
	@Column(length = 50)
	String taskId;
	Date voucherDate;
	Date actionDate;
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst ;

	
	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getVoucherType() {
		return voucherType;
	}



	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}



	public String getVoucherNumber() {
		return voucherNumber;
	}



	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}



	public String getReason() {
		return reason;
	}



	public void setReason(String reason) {
		this.reason = reason;
	}





	public String getComment() {
		return comment;
	}



	public void setComment(String comment) {
		this.comment = comment;
	}



	public String getUserId() {
		return userId;
	}



	public void setUserId(String userId) {
		this.userId = userId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	public Date getVoucherDate() {
		return voucherDate;
	}



	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}



	public Date getActionDate() {
		return actionDate;
	}



	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}



	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public String toString() {
		return "TaskApproveMst [id=" + id + ", voucherType=" + voucherType + ", voucherNumber=" + voucherNumber
				+ ", reason=" + reason + ", instanceId=" + instanceId + ", comment=" + comment + ", userId=" + userId
				+ ", taskId=" + taskId + ", voucherDate=" + voucherDate + ", actionDate=" + actionDate + ", companyMst="
				+ companyMst + "]";
	}







}
