package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
@Entity
public class JobCardOutDtl implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	private String itemId;
	private Double qty;
	@Column(length = 50)
	private String locationId;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "jobCardOutHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private JobCardOutHdr jobCardOutHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public JobCardOutHdr getJobCardOutHdr() {
		return jobCardOutHdr;
	}

	public void setJobCardOutHdr(JobCardOutHdr jobCardOutHdr) {
		this.jobCardOutHdr = jobCardOutHdr;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "JobCardOutDtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", locationId=" + locationId
				+ ", jobCardOutHdr=" + jobCardOutHdr + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}


	
	

}
