package com.maple.restserver.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity

public class StockVerificationDtl implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	private LocalDateTime updatedTime;
	@Column(length = 50)
	String itemId;
	Double qty;
	Double systemQty;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "stockVerificationMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private StockVerificationMst stockVerificationMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	public StockVerificationMst getStockVerificationMst() {
		return stockVerificationMst;
	}
	public void setStockVerificationMst(StockVerificationMst stockVerificationMst) {
		this.stockVerificationMst = stockVerificationMst;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getSystemQty() {
		return systemQty;
	}
	public void setSystemQty(Double systemQty) {
		this.systemQty = systemQty;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "StockVerificationDtl [id=" + id + ", updatedTime=" + updatedTime + ", itemId=" + itemId + ", qty=" + qty
				+ ", systemQty=" + systemQty + ", stockVerificationMst=" + stockVerificationMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
