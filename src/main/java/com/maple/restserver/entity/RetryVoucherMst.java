

package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
  public class RetryVoucherMst implements Serializable {
		private static final long serialVersionUID = 1L;
		@Column(length = 50)
	@Id
    String id;
		@Column(length = 50)
	String voucherType;
		@Column(length = 50)
	String branchCode;
	Date voucherDate;
	Integer serverCount;
	Integer clientCount;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public Integer getServerCount() {
		return serverCount;
	}
	public void setServerCount(Integer serverCount) {
		this.serverCount = serverCount;
	}
	public Integer getClientCount() {
		return clientCount;
	}
	public void setClientCount(Integer clientCount) {
		this.clientCount = clientCount;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "RetryVoucherMst [id=" + id + ", voucherType=" + voucherType + ", branchCode=" + branchCode
				+ ", voucherDate=" + voucherDate + ", serverCount=" + serverCount + ", clientCount=" + clientCount
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	 
	 

}
