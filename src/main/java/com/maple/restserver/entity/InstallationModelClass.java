package com.maple.restserver.entity;

import java.util.ArrayList;

public class InstallationModelClass {

	ArrayList<UserMst>  userMstList =new ArrayList();
	InvoiceFormatMst invoiceFormatMst;
	FinancialYearMst financialYearMst;
	SysDateMst sysDateMst;
	ArrayList<ProcessPermissionMst>  processPermissionMstList =new ArrayList();
	ArrayList<TermsAndConditionsMst>  termsAndConditionsMstList =new ArrayList();
	ArrayList<SalesTypeMst>  salesTypeMstList =new ArrayList();
	
	BranchMst branchMst;
	
	
	
	
	
	
	
	
	public ArrayList<UserMst> getUserMstList() {
		return userMstList;
	}
	public InvoiceFormatMst getInvoiceFormatMst() {
		return invoiceFormatMst;
	}
	public FinancialYearMst getFinancialYearMst() {
		return financialYearMst;
	}
	public ArrayList<ProcessPermissionMst> getProcessPermissionMstList() {
		return processPermissionMstList;
	}
	public ArrayList<TermsAndConditionsMst> getTermsAndConditionsMstList() {
		return termsAndConditionsMstList;
	}
	public ArrayList<SalesTypeMst> getSalesTypeMstList() {
		return salesTypeMstList;
	}
	public void setUserMstList(ArrayList<UserMst> userMstList) {
		this.userMstList = userMstList;
	}
	public void setInvoiceFormatMst(InvoiceFormatMst invoiceFormatMst) {
		this.invoiceFormatMst = invoiceFormatMst;
	}
	public void setFinancialYearMst(FinancialYearMst financialYearMst) {
		this.financialYearMst = financialYearMst;
	}
	public void setProcessPermissionMstList(ArrayList<ProcessPermissionMst> processPermissionMstList) {
		this.processPermissionMstList = processPermissionMstList;
	}
	public void setTermsAndConditionsMstList(ArrayList<TermsAndConditionsMst> termsAndConditionsMstList) {
		this.termsAndConditionsMstList = termsAndConditionsMstList;
	}
	public void setSalesTypeMstList(ArrayList<SalesTypeMst> salesTypeMstList) {
		this.salesTypeMstList = salesTypeMstList;
	}
	public SysDateMst getSysDateMst() {
		return sysDateMst;
	}
	public void setSysDateMst(SysDateMst sysDateMst) {
		this.sysDateMst = sysDateMst;
	}
	
	public BranchMst getBranchMst() {
		return branchMst;
	}
	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}
	@Override
	public String toString() {
		return "InstallationModelClass [userMstList=" + userMstList + ", invoiceFormatMst=" + invoiceFormatMst
				+ ", financialYearMst=" + financialYearMst + ", sysDateMst=" + sysDateMst
				+ ", processPermissionMstList=" + processPermissionMstList + ", termsAndConditionsMstList="
				+ termsAndConditionsMstList + ", salesTypeMstList=" + salesTypeMstList + ", branchMst=" + branchMst
				+ "]";
	}
	
	
	
}
