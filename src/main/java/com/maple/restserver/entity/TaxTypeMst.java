package com.maple.restserver.entity;

import javax.persistence.Column;
import javax.persistence.Id;

//This class is not persisted
public class TaxTypeMst {

	@Id
	@Column(length = 50)
	String Id;
	@Column(length = 50)
	String taxName;
	@Column(length = 50)
	String taxId;
	@Column(length = 50)
	String calculationId;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getTaxName() {
		return taxName;
	}
	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public String getCalculationId() {
		return calculationId;
	}
	public void setCalculationId(String calculationId) {
		this.calculationId = calculationId;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "TaxTypeMst [Id=" + Id + ", taxName=" + taxName + ", taxId=" + taxId + ", calculationId=" + calculationId
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
