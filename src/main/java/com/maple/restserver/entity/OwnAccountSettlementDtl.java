package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class OwnAccountSettlementDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	String invoiceNumber;
	Double dueAmt;
	Double paidAmount;
	Double newPaidAmount;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
			
	 @ManyToOne(fetch = FetchType.EAGER, optional = true)
	 @JoinColumn(name = "ownAccountSettlementMst", nullable = true)
	 @OnDelete(action = OnDeleteAction.CASCADE)
	 OwnAccountSettlementMst ownAccountSettlementMst;
	
	 @ManyToOne(fetch = FetchType.EAGER, optional = true)
	 @JoinColumn(name = "ownAccount", nullable = true)
	 @OnDelete(action = OnDeleteAction.CASCADE)
 	 OwnAccount ownAccount;
	 
	 
	 
	 @ManyToOne(fetch = FetchType.EAGER, optional = true)
	 @JoinColumn(name = "accountReceivable", nullable = true)
	 @OnDelete(action = OnDeleteAction.CASCADE)
	 AccountReceivable accountReceivable;
	 
	 
	 @ManyToOne(fetch = FetchType.EAGER, optional = true)
	 @JoinColumn(name = "accountPayable", nullable = true)
	 @OnDelete(action = OnDeleteAction.CASCADE)
	 AccountPayable accountPayable;
	 @Column(length = 50)
	 private String  processInstanceId;
	 @Column(length = 50)
		private String taskId;
	 public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public Double getDueAmt() {
		return dueAmt;
	}

	public void setDueAmt(Double dueAmt) {
		this.dueAmt = dueAmt;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public OwnAccountSettlementMst getOwnAccountSettlementMst() {
		return ownAccountSettlementMst;
	}

	public void setOwnAccountSettlementMst(OwnAccountSettlementMst ownAccountSettlementMst) {
		this.ownAccountSettlementMst = ownAccountSettlementMst;
	}

	public Double getNewPaidAmount() {
		return newPaidAmount;
	}

	public void setNewPaidAmount(Double newPaidAmount) {
		this.newPaidAmount = newPaidAmount;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public OwnAccount getOwnAccount() {
		return ownAccount;
	}

	public void setOwnAccount(OwnAccount ownAccount) {
		this.ownAccount = ownAccount;
	}

	public AccountReceivable getAccountReceivable() {
		return accountReceivable;
	}

	public void setAccountReceivable(AccountReceivable accountReceivable) {
		this.accountReceivable = accountReceivable;
	}

	public AccountPayable getAccountPayable() {
		return accountPayable;
	}

	public void setAccountPayable(AccountPayable accountPayable) {
		this.accountPayable = accountPayable;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "OwnAccountSettlementDtl [id=" + id + ", invoiceNumber=" + invoiceNumber + ", dueAmt=" + dueAmt
				+ ", paidAmount=" + paidAmount + ", newPaidAmount=" + newPaidAmount + ", companyMst=" + companyMst
				+ ", ownAccountSettlementMst=" + ownAccountSettlementMst + ", ownAccount=" + ownAccount
				+ ", accountReceivable=" + accountReceivable + ", accountPayable=" + accountPayable
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}



	
	
}
