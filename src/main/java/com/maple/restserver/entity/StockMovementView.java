package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

public class StockMovementView implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	Integer id;
	@Column(length = 50)
	String branchCode;
	@Column(length = 50)
	String itemName;
	@Column(length = 50)
	Double  qtyIn;
	@Column(length = 50)
	Double  qtyOut;
	public Integer getId() {
		return id;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public String getItemName() {
		return itemName;
	}
	public Double getQtyIn() {
		return qtyIn;
	}
	public Double getQtyOut() {
		return qtyOut;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public void setQtyIn(Double qtyIn) {
		this.qtyIn = qtyIn;
	}
	public void setQtyOut(Double qtyOut) {
		this.qtyOut = qtyOut;
	}
	@Override
	public String toString() {
		return "StockMovementView [id=" + id + ", branchCode=" + branchCode + ", itemName=" + itemName + ", qtyIn="
				+ qtyIn + ", qtyOut=" + qtyOut + "]";
	}
	
	
	

}
