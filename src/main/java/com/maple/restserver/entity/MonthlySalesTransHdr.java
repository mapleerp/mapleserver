package com.maple.restserver.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class MonthlySalesTransHdr implements Serializable{

	
	private static final long serialVersionUID = 1L;
	 
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
 
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "accountHeads", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private AccountHeads accountHeads;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "localCustomerMst", nullable = true)

	private LocalCustomerMst localCustomerMst;
	@Column(length = 50)
	private String customerId;
	@Column(length = 50)
	private String salesManId;
	@Column(length = 50)
	private String deliveryBoyId;
	
	private String salesMode;
	@Column(length = 50)
	private String creditOrCash;
	@Column(length = 50)
	private String userId;
	
	//version2.9
	@Column(length = 50)
	private String sourceBranchCode;
	//version2.9ends
	@Column(length = 50)
	String takeOrderNumber;
	@Column(nullable=false)
	String branchCode;
	 
	String fbKey;


	@Column(length = 50)
	private String invoiceNumberPrefix;
	
	private Long numericVoucherNumber;
	@Column(length = 50)
	private String machineId;
	@Column(length = 50)
	private String voucherNumber;
	
 
	private Date voucherDate;
  
	private Double  invoiceAmount;
 
	private Double invoiceDiscount;
 
	public String getDiscount() {
		return discount;
	}

	private Double  cashPay;
	@Column(length = 50)
	private String discount;
 
	

	private Double amountTendered;
 
	private Double itemDiscount;
 
	private Double paidAmount;
 
	private Double changeAmount;
	@Column(length = 50)
	private String cardNo;
	@Column(length = 50)
	private String cardType;
	 
	private Double cardamount;
	@Column(length = 50)
	private String voucherType;
	@Column(length = 50)
	private String servingTableName;
	@Column(length = 50)
	private String isBranchSales;
	
	private Double sodexoAmount;
	
	private Double paytmAmount;
	
	private Double creditAmount;
	@Column(length = 50)
	private String saleOrderHrdId;
	@Column(length = 50)
	private String salesReceiptsVoucherNumber;
	@Column(length = 50)
	private String performaInvoicePrinted;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	 
	@Column(name="SOURCE_IP")
	private String SourceIP ;
	
	@Column(name="SOURCE_PORT")
	private String  SourcePort ;
	 
	public MonthlySalesTransHdr(){
		
	 
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public LocalCustomerMst getLocalCustomerMst() {
		return localCustomerMst;
	}

	public void setLocalCustomerMst(LocalCustomerMst localCustomerMst) {
		this.localCustomerMst = localCustomerMst;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getSalesManId() {
		return salesManId;
	}

	public void setSalesManId(String salesManId) {
		this.salesManId = salesManId;
	}

	public String getDeliveryBoyId() {
		return deliveryBoyId;
	}

	public void setDeliveryBoyId(String deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}

	public String getSalesMode() {
		return salesMode;
	}

	public void setSalesMode(String salesMode) {
		this.salesMode = salesMode;
	}

	public String getCreditOrCash() {
		return creditOrCash;
	}

	public void setCreditOrCash(String creditOrCash) {
		this.creditOrCash = creditOrCash;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSourceBranchCode() {
		return sourceBranchCode;
	}

	public void setSourceBranchCode(String sourceBranchCode) {
		this.sourceBranchCode = sourceBranchCode;
	}

	public String getTakeOrderNumber() {
		return takeOrderNumber;
	}

	public void setTakeOrderNumber(String takeOrderNumber) {
		this.takeOrderNumber = takeOrderNumber;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getFbKey() {
		return fbKey;
	}

	public void setFbKey(String fbKey) {
		this.fbKey = fbKey;
	}

	public String getInvoiceNumberPrefix() {
		return invoiceNumberPrefix;
	}

	public void setInvoiceNumberPrefix(String invoiceNumberPrefix) {
		this.invoiceNumberPrefix = invoiceNumberPrefix;
	}

	public Long getNumericVoucherNumber() {
		return numericVoucherNumber;
	}

	public void setNumericVoucherNumber(Long numericVoucherNumber) {
		this.numericVoucherNumber = numericVoucherNumber;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public Double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public Double getInvoiceDiscount() {
		return invoiceDiscount;
	}

	public void setInvoiceDiscount(Double invoiceDiscount) {
		this.invoiceDiscount = invoiceDiscount;
	}

	public Double getCashPay() {
		return cashPay;
	}

	public void setCashPay(Double cashPay) {
		this.cashPay = cashPay;
	}

	public Double getAmountTendered() {
		return amountTendered;
	}

	public void setAmountTendered(Double amountTendered) {
		this.amountTendered = amountTendered;
	}

	public Double getItemDiscount() {
		return itemDiscount;
	}

	public void setItemDiscount(Double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Double getChangeAmount() {
		return changeAmount;
	}

	public void setChangeAmount(Double changeAmount) {
		this.changeAmount = changeAmount;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public Double getCardamount() {
		return cardamount;
	}

	public void setCardamount(Double cardamount) {
		this.cardamount = cardamount;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public String getServingTableName() {
		return servingTableName;
	}

	public void setServingTableName(String servingTableName) {
		this.servingTableName = servingTableName;
	}

	public String getIsBranchSales() {
		return isBranchSales;
	}

	public void setIsBranchSales(String isBranchSales) {
		this.isBranchSales = isBranchSales;
	}

	public Double getSodexoAmount() {
		return sodexoAmount;
	}

	public void setSodexoAmount(Double sodexoAmount) {
		this.sodexoAmount = sodexoAmount;
	}

	public Double getPaytmAmount() {
		return paytmAmount;
	}

	public void setPaytmAmount(Double paytmAmount) {
		this.paytmAmount = paytmAmount;
	}

	public Double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public String getSaleOrderHrdId() {
		return saleOrderHrdId;
	}

	public void setSaleOrderHrdId(String saleOrderHrdId) {
		this.saleOrderHrdId = saleOrderHrdId;
	}

	public String getSalesReceiptsVoucherNumber() {
		return salesReceiptsVoucherNumber;
	}

	public void setSalesReceiptsVoucherNumber(String salesReceiptsVoucherNumber) {
		this.salesReceiptsVoucherNumber = salesReceiptsVoucherNumber;
	}

	public String getPerformaInvoicePrinted() {
		return performaInvoicePrinted;
	}

	public void setPerformaInvoicePrinted(String performaInvoicePrinted) {
		this.performaInvoicePrinted = performaInvoicePrinted;
	}



	public String getSourceIP() {
		return SourceIP;
	}

	public void setSourceIP(String sourceIP) {
		SourceIP = sourceIP;
	}

	public String getSourcePort() {
		return SourcePort;
	}

	public void setSourcePort(String sourcePort) {
		SourcePort = sourcePort;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public AccountHeads getAccountHeads() {
		return accountHeads;
	}

	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}

	@Override
	public String toString() {
		return "MonthlySalesTransHdr [id=" + id + ", companyMst=" + companyMst + ", accountHeads=" + accountHeads
				+ ", localCustomerMst=" + localCustomerMst + ", customerId=" + customerId + ", salesManId=" + salesManId
				+ ", deliveryBoyId=" + deliveryBoyId + ", salesMode=" + salesMode + ", creditOrCash=" + creditOrCash
				+ ", userId=" + userId + ", sourceBranchCode=" + sourceBranchCode + ", takeOrderNumber="
				+ takeOrderNumber + ", branchCode=" + branchCode + ", fbKey=" + fbKey + ", invoiceNumberPrefix="
				+ invoiceNumberPrefix + ", numericVoucherNumber=" + numericVoucherNumber + ", machineId=" + machineId
				+ ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate + ", invoiceAmount="
				+ invoiceAmount + ", invoiceDiscount=" + invoiceDiscount + ", cashPay=" + cashPay + ", discount="
				+ discount + ", amountTendered=" + amountTendered + ", itemDiscount=" + itemDiscount + ", paidAmount="
				+ paidAmount + ", changeAmount=" + changeAmount + ", cardNo=" + cardNo + ", cardType=" + cardType
				+ ", cardamount=" + cardamount + ", voucherType=" + voucherType + ", servingTableName="
				+ servingTableName + ", isBranchSales=" + isBranchSales + ", sodexoAmount=" + sodexoAmount
				+ ", paytmAmount=" + paytmAmount + ", creditAmount=" + creditAmount + ", saleOrderHrdId="
				+ saleOrderHrdId + ", salesReceiptsVoucherNumber=" + salesReceiptsVoucherNumber
				+ ", performaInvoicePrinted=" + performaInvoicePrinted + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", SourceIP=" + SourceIP + ", SourcePort=" + SourcePort + "]";
	}

	
	
	
}
