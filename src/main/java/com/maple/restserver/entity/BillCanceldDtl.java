package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class BillCanceldDtl implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	 
	@Column(nullable=false)
	String itemId;
	Double qty;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double rate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double cgstTaxRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double sgstTaxRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double cessRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double addCessRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double igstTaxRate;
	 
	String itemTaxaxId;
	@Column(nullable=false)
	String unitId;
	String itemName;
	Date expiryDate;
	@Column(nullable=false)
	String batch;
	String barcode;
	
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double taxRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double mrp;
	@Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double amount;
	String unitName;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
    Double discount;
    @Column(  nullable = false, columnDefinition = "double default 0.0") 
    Double cgstAmount;
    @Column(  nullable = false, columnDefinition = "double default 0.0") 
    Double sgstAmount;
    @Column(  nullable = false, columnDefinition = "double default 0.0") 
    Double igstAmount;
    
    @Column( nullable = false, columnDefinition = "double default 0.0") 
    Double cessAmount;
    
	 	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "bill_canceld_hdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private BillCanceldHdr billCanceldHdr;


	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
 
}
