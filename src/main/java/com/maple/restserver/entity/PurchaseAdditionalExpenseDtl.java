package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class PurchaseAdditionalExpenseDtl implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	@Column(length = 50)
	String expense;
	Double amount;
	@Column(length = 50)
	String calculated;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "purchaseAdditionalExpenseHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	PurchaseAdditionalExpenseHdr purchaseAdditionalExpenseHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	

	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public PurchaseAdditionalExpenseHdr getPurchaseAdditionalExpenseHdr() {
		return purchaseAdditionalExpenseHdr;
	}
	public void setPurchaseAdditionalExpenseHdr(PurchaseAdditionalExpenseHdr purchaseAdditionalExpenseHdr) {
		this.purchaseAdditionalExpenseHdr = purchaseAdditionalExpenseHdr;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCalculated() {
		return calculated;
	}
	public void setCalculated(String calculated) {
		this.calculated = calculated;
	}
	public String getExpense() {
		return expense;
	}
	public void setExpense(String expense) {
		this.expense = expense;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PurchaseAdditionalExpenseDtl [id=" + id + ", expense=" + expense + ", amount=" + amount
				+ ", calculated=" + calculated + ", purchaseAdditionalExpenseHdr=" + purchaseAdditionalExpenseHdr
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	

}
