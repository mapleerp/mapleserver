package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;




@Entity
public class NutritionValueDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	@Column(length = 50)
	String nutrition;
	String value;
	@Column(length = 50)
	String serial;
	@Column(nullable=true)
	String percentage;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "nutritionValueMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	NutritionValueMst nutritionValueMst;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNutrition() {
		return nutrition;
	}

	public void setNutrition(String nutrition) {
		this.nutrition = nutrition;
	}



	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public NutritionValueMst getNutritionValueMst() {
		return nutritionValueMst;
	}

	public void setNutritionValueMst(NutritionValueMst nutritionValueMst) {
		this.nutritionValueMst = nutritionValueMst;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "NutritionValueDtl [id=" + id + ", nutrition=" + nutrition + ", value=" + value + ", serial=" + serial
				+ ", percentage=" + percentage + ", nutritionValueMst=" + nutritionValueMst + ", companyMst="
				+ companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
	
}
