package com.maple.restserver.entity;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
public class LocalCustomerDtl implements Serializable  {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "localCustomerMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	LocalCustomerMst localCustomerMst;
	@Column(length = 50)
	private String name;
	@Column(length = 50)
	private String relation;
	private Date dateOfBirth;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public LocalCustomerMst getLocalCustomerMst() {
		return localCustomerMst;
	}
	public void setLocalCustomerMst(LocalCustomerMst localCustomerMst) {
		this.localCustomerMst = localCustomerMst;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "LocalCustomerDtl [id=" + id + ", companyMst=" + companyMst + ", localCustomerMst=" + localCustomerMst
				+ ", name=" + name + ", relation=" + relation + ", dateOfBirth=" + dateOfBirth + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	} 
	
}
