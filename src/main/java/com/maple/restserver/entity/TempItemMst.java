package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class TempItemMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(length = 50)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
   String id;
	@Column(length = 50)
   String itemId;
	@Column(length = 50)
   private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getItemId() {
	return itemId;
}
public void setItemId(String itemId) {
	this.itemId = itemId;
}
public String getProcessInstanceId() {
	return processInstanceId;
}
public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}
public String getTaskId() {
	return taskId;
}
public void setTaskId(String taskId) {
	this.taskId = taskId;
}
@Override
public String toString() {
	return "TempItemMst [id=" + id + ", itemId=" + itemId + ", processInstanceId=" + processInstanceId + ", taskId="
			+ taskId + "]";
}	
   
}
