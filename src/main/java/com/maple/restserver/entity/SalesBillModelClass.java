package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javassist.bytecode.ByteArray;

public class SalesBillModelClass implements Serializable{
	private static final long serialVersionUID = 1L;
	
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "salesTransHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	SalesTransHdr salesTransHdr;
	
	ArrayList<SalesDtl> arrayOfSalesDtl;
	
	ArrayList<SalesReceipts> arrayOfSalesReceipts;
	
	ByteArray logo;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "branchMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	BranchMst branchMst;
	
	
	Double taxAmount;
	
	Double balanceAmount;

	Double totalAmount;
	
	

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}

	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}

	public ArrayList<SalesDtl> getArrayOfSalesDtl() {
		return arrayOfSalesDtl;
	}

	public void setArrayOfSalesDtl(ArrayList<SalesDtl> arrayOfSalesDtl) {
		this.arrayOfSalesDtl = arrayOfSalesDtl;
	}

	public ArrayList<SalesReceipts> getArrayOfSalesReceipts() {
		return arrayOfSalesReceipts;
	}

	public void setArrayOfSalesReceipts(ArrayList<SalesReceipts> arrayOfSalesReceipts) {
		this.arrayOfSalesReceipts = arrayOfSalesReceipts;
	}

	public ByteArray getLogo() {
		return logo;
	}

	public void setLogo(ByteArray logo) {
		this.logo = logo;
	}

	public BranchMst getBranchMst() {
		return branchMst;
	}

	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}

	

	public Double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	@Override
	public String toString() {
		return "SalesBillModelClass [companyMst=" + companyMst + ", salesTransHdr=" + salesTransHdr
				+ ", arrayOfSalesDtl=" + arrayOfSalesDtl + ", arrayOfSalesReceipts=" + arrayOfSalesReceipts + ", logo="
				+ logo + ", branchMst=" + branchMst + ", taxAmount=" + taxAmount + ", balanceAmount=" + balanceAmount
				+ ", totalAmount=" + totalAmount + "]";
	}

	

	

	
	
	
}
