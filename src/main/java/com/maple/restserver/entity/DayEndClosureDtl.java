package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
 
@Entity
 

public class DayEndClosureDtl implements Serializable {
  
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	private Double denomination;
	private Double count;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "day_end_closure_hdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private DayEndClosureHdr dayEndClosureHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getId() {
		return id;
	}

	public Double getDenomination() {
		return denomination;
	}

	public void setDenomination(Double denomination) {
		this.denomination = denomination;
	}

	public Double getCount() {
		return count;
	}

	public void setCount(Double count) {
		this.count = count;
	}

	public DayEndClosureHdr getDayEndClosureHdr() {
		return dayEndClosureHdr;
	}

	public void setDayEndClosureHdr(DayEndClosureHdr dayEndClosureHdr) {
		this.dayEndClosureHdr = dayEndClosureHdr;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "DayEndClosureDtl [id=" + id + ", denomination=" + denomination + ", count=" + count
				+ ", dayEndClosureHdr=" + dayEndClosureHdr + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
}
