package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class StockTransferOutHdr implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	
    private String id;
	//Integer slNo;
	@Column(length = 50)
	String intentNumber;
	Date voucherDate;
	@Column(length = 50)
	String voucherNumber;
	@Column(length = 50)
	String voucherType;
	
	@Column(nullable = false) 
	String toBranch;
	@Column(length = 50)
	String deleted;
	@Column(length = 50)
	String fromBranch;
	@Column(length = 50)
	String inVoucherNumber;
	Date inVoucherDate;
	@Column(length = 50)
	String status;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
		CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	@Column(length = 50)
	private String postedToServer;
	  
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIntentNumber() {
		return intentNumber;
	}

	public void setIntentNumber(String intentNumber) {
		this.intentNumber = intentNumber;
	}

 

	
	public String getToBranch() {
		return toBranch;
	}

	public void setToBranch(String toBranch) {
		this.toBranch = toBranch;
	}

	public String getDeleted() {
		return deleted;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
 

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	

	public String getFromBranch() {
		return fromBranch;
	}

	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}

	public String getInVoucherNumber() {
		return inVoucherNumber;
	}

	public void setInVoucherNumber(String inVoucherNumber) {
		this.inVoucherNumber = inVoucherNumber;
	}

	public Date getInVoucherDate() {
		return inVoucherDate;
	}

	public void setInVoucherDate(Date inVoucherDate) {
		this.inVoucherDate = inVoucherDate;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "StockTransferOutHdr [id=" + id + ", intentNumber=" + intentNumber + ", voucherDate=" + voucherDate
				+ ", voucherNumber=" + voucherNumber + ", voucherType=" + voucherType + ", toBranch=" + toBranch
				+ ", deleted=" + deleted + ", fromBranch=" + fromBranch + ", inVoucherNumber=" + inVoucherNumber
				+ ", inVoucherDate=" + inVoucherDate + ", status=" + status + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	

	

	/*
	 * public Integer getSlNo() { return slNo; }
	 * 
	 * public void setSlNo(Integer slNo) { this.slNo = slNo; }
	 */
	
	
}
