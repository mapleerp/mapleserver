package com.maple.restserver.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class HeartBeatsMst implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	private String brnchCode;
	@Column(length = 50)
	private String branchName;

	@UpdateTimestamp
	@Column(nullable = true)
	private LocalDateTime updatedTime;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	//------------version 6.10 surya
	@Column(length = 50)
	private String heartBeatsId;
	@Column(length = 50)
	private String status;
//-----------------version 6.10 surya end
	
	

	public String getBrnchCode() {
		return brnchCode;
	}

	public void setBrnchCode(String brnchCode) {
		this.brnchCode = brnchCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}
	
	

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	//-----------------version 6.10 surya 

	
	public String getHeartBeatsId() {
		return heartBeatsId;
	}

	public void setHeartBeatsId(String heartBeatsId) {
		this.heartBeatsId = heartBeatsId;
	}

	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "HeartBeatsMst [brnchCode=" + brnchCode + ", branchName=" + branchName + ", updatedTime=" + updatedTime
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", heartBeatsId=" + heartBeatsId + ", status=" + status + "]";
	}

	
	//-----------------version 6.10 surya end

	

}
