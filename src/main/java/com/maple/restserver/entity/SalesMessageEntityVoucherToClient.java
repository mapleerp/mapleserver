package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;

import org.springframework.stereotype.Component;
@Component
public class SalesMessageEntityVoucherToClient implements Serializable {
	private static final long serialVersionUID = 1L;
	SalesTransHdr salesTransHdr;
	ArrayList<SalesDtl> salesDtlList =new ArrayList();
	ArrayList<SalesReceipts> salesReceiptsList =new ArrayList();
//	ArrayList<AccountReceivable> accountReceivable = new ArrayList();
	
	
	
//	public ArrayList<AccountReceivable> getAccountReceivable() {
//		return accountReceivable;
//	}
//	public void setAccountReceivable(ArrayList<AccountReceivable> accountReceivable) {
//		this.accountReceivable = accountReceivable;
//	}
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}
	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}
	public ArrayList<SalesDtl> getSalesDtlList() {
		return salesDtlList;
	}
	public void setSalesDtlList(ArrayList<SalesDtl> salesDtlList) {
		this.salesDtlList = salesDtlList;
	}
	public ArrayList<SalesReceipts> getSalesReceiptsList() {
		return salesReceiptsList;
	}
	public void setSalesReceiptsList(ArrayList<SalesReceipts> salesReceiptsList) {
		this.salesReceiptsList = salesReceiptsList;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SalesMessageEntityVoucherToClient [salesTransHdr=" + salesTransHdr + ", salesDtlList=" + salesDtlList
				+ ", salesReceiptsList=" + salesReceiptsList + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
	  
	
	
 
	
}
