
package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
public class ReceiptDtl implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
    	String id;
	@Column(length = 50)
		String account;
		Date instdate;
		@Column(length = 50)
		String modeOfpayment;
		Double amount;
		@Column(length = 50)
		String depositBank;
		String remark;
		@Column(length = 50)
		String instnumber;
		@Column(length = 50)
		String invoiceNumber;
		@Column(length = 50)
		String bankAccountNumber;
		Date instrumentDate;
		Date transDate;
		@Column(length = 50)
		String memberId;
		@Column(length = 50)
		String oldId;
		@Column(length = 50)
		String branchCode;
		@Column(length = 50)
		String debitAccountId;
		
		java.util.Date invoiceDate;

		@JsonIgnore
		@ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "receipt_hdr_id", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		ReceiptHdr receiptHdr;
		@Column(length = 50)
		private String  processInstanceId;
		@Column(length = 50)
		private String taskId;


		public String getId() {
			return id;
		}



		public void setId(String id) {
			this.id = id;
		}



		public String getAccount() {
			return account;
		}



		public void setAccount(String account) {
			this.account = account;
		}



		public Date getInstdate() {
			return instdate;
		}



		public void setInstdate(Date instdate) {
			this.instdate = instdate;
		}
		public String getMemberId() {
			return memberId;
		}


		public void setMemberId(String memberId) {
			this.memberId = memberId;
		}


		public String getModeOfpayment() {
			return modeOfpayment;
		}



		public void setModeOfpayment(String modeOfpayment) {
			this.modeOfpayment = modeOfpayment;
		}



		public Double getAmount() {
			return amount;
		}



		public void setAmount(Double amount) {
			this.amount = amount;
		}



		public String getDepositBank() {
			return depositBank;
		}



		public void setDepositBank(String depositBank) {
			this.depositBank = depositBank;
		}



		public String getRemark() {
			return remark;
		}



		public void setRemark(String remark) {
			this.remark = remark;
		}



		public String getInstnumber() {
			return instnumber;
		}



		public void setInstnumber(String instnumber) {
			this.instnumber = instnumber;
		}



		public String getInvoiceNumber() {
			return invoiceNumber;
		}



		public void setInvoiceNumber(String invoiceNumber) {
			this.invoiceNumber = invoiceNumber;
		}



		public String getBankAccountNumber() {
			return bankAccountNumber;
		}



		public void setBankAccountNumber(String bankAccountNumber) {
			this.bankAccountNumber = bankAccountNumber;
		}



		public Date getInstrumentDate() {
			return instrumentDate;
		}



		public void setInstrumentDate(Date instrumentDate) {
			this.instrumentDate = instrumentDate;
		}



		public Date getTransDate() {
			return transDate;
		}



		public void setTransDate(Date transDate) {
			this.transDate = transDate;
		}



		public String getBranchCode() {
			return branchCode;
		}



		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}



		public ReceiptHdr getReceiptHdr() {
			return receiptHdr;
		}



		public void setReceiptHdr(ReceiptHdr receiptHdr) {
			this.receiptHdr = receiptHdr;
		}


 



		public String getDebitAccountId() {
			return debitAccountId;
		}



		public void setDebitAccountId(String debitAccountId) {
			this.debitAccountId = debitAccountId;
		}



		



		public String getProcessInstanceId() {
			return processInstanceId;
		}



		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}



		public String getTaskId() {
			return taskId;
		}



		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}



		public String getOldId() {
			return oldId;
		}



		public void setOldId(String oldId) {
			this.oldId = oldId;
		}



		public java.util.Date getInvoiceDate() {
			return invoiceDate;
		}



		public void setInvoiceDate(java.util.Date invoiceDate) {
			this.invoiceDate = invoiceDate;
		}



		@Override
		public String toString() {
			return "ReceiptDtl [id=" + id + ", account=" + account + ", instdate=" + instdate + ", modeOfpayment="
					+ modeOfpayment + ", amount=" + amount + ", depositBank=" + depositBank + ", remark=" + remark
					+ ", instnumber=" + instnumber + ", invoiceNumber=" + invoiceNumber + ", bankAccountNumber="
					+ bankAccountNumber + ", instrumentDate=" + instrumentDate + ", transDate=" + transDate
					+ ", memberId=" + memberId + ", oldId=" + oldId + ", branchCode=" + branchCode + ", debitAccountId="
					+ debitAccountId + ", invoiceDate=" + invoiceDate + ", receiptHdr=" + receiptHdr
					+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
		}



}
