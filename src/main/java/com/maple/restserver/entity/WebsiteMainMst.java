package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class WebsiteMainMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(length = 50)
    private String id;
	
	private String searchbannertexth1;
	private String aboutimageurl;
	private String aboutp;
	private String  newsletterrh2;
	private String newsletterh2br;
	private String location;
	private String websitephone1;
	private String websitephone2;
	private String email;
	private String copyrightp;
	public String getId() {
		return id;
	}
	public String getSearchbannertexth1() {
		return searchbannertexth1;
	}
	public String getAboutimageurl() {
		return aboutimageurl;
	}
	public String getAboutp() {
		return aboutp;
	}
	public String getNewsletterrh2() {
		return newsletterrh2;
	}
	public String getNewsletterh2br() {
		return newsletterh2br;
	}
	public String getLocation() {
		return location;
	}
	public String getWebsitephone1() {
		return websitephone1;
	}
	public String getWebsitephone2() {
		return websitephone2;
	}
	public String getEmail() {
		return email;
	}
	public String getCopyrightp() {
		return copyrightp;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setSearchbannertexth1(String searchbannertexth1) {
		this.searchbannertexth1 = searchbannertexth1;
	}
	public void setAboutimageurl(String aboutimageurl) {
		this.aboutimageurl = aboutimageurl;
	}
	public void setAboutp(String aboutp) {
		this.aboutp = aboutp;
	}
	public void setNewsletterrh2(String newsletterrh2) {
		this.newsletterrh2 = newsletterrh2;
	}
	public void setNewsletterh2br(String newsletterh2br) {
		this.newsletterh2br = newsletterh2br;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public void setWebsitephone1(String websitephone1) {
		this.websitephone1 = websitephone1;
	}
	public void setWebsitephone2(String websitephone2) {
		this.websitephone2 = websitephone2;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setCopyrightp(String copyrightp) {
		this.copyrightp = copyrightp;
	}
	@Override
	public String toString() {
		return "WebsiteMainMst [id=" + id + ", searchbannertexth1=" + searchbannertexth1 + ", aboutimageurl="
				+ aboutimageurl + ", aboutp=" + aboutp + ", newsletterrh2=" + newsletterrh2 + ", newsletterh2br="
				+ newsletterh2br + ", location=" + location + ", websitephone1=" + websitephone1 + ", websitephone2="
				+ websitephone2 + ", email=" + email + ", copyrightp=" + copyrightp + "]";
	}
	
	
	
	
	

}
