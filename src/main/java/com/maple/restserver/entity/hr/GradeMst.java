package com.maple.restserver.entity.hr;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;

@Entity
public class GradeMst implements Serializable {
	
	private static final long serialVersionUID=1L;
	@Id 
	@GeneratedValue(generator="uuid")
	@GenericGenerator(name="uuid",strategy="uuid2")
	@Column(length = 50)
	private String id;
	@Column(length = 50)
	private String employeeGrade;
	@Column(length = 10)
	private String branchCode;
	@Column(length = 50)
	private String basicPay;
	@Column(length = 50)
	private String allowance;
	@Column(length = 50)
	private String dailyAllowance;
	
	@ManyToOne(fetch=FetchType.EAGER,optional=false)
	@JoinColumn(name="companyMst",nullable=false)
	@OnDelete(action=OnDeleteAction.CASCADE)
	CompanyMst companyMst;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmployeeGrade() {
		return employeeGrade;
	}

	public void setEmployeeGrade(String employeeGrade) {
		this.employeeGrade = employeeGrade;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBasicPay() {
		return basicPay;
	}

	public void setBasicPay(String basicPay) {
		this.basicPay = basicPay;
	}

	public String getAllowance() {
		return allowance;
	}

	public void setAllowance(String allowance) {
		this.allowance = allowance;
	}

	public String getDailyAllowance() {
		return dailyAllowance;
	}

	public void setDailyAllowance(String dailyAllowance) {
		this.dailyAllowance = dailyAllowance;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	
	}

	@Override
	public String toString() {
		return "GradeMst [id=" + id + ", employeeGrade=" + employeeGrade + ", branchCode=" + branchCode + ", basicPay="
				+ basicPay + ", allowance=" + allowance + ", dailyAllowance=" + dailyAllowance + ", companyMst="
				+ companyMst + "]";
	}
	
}
