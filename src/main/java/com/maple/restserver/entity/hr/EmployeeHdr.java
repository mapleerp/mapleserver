package com.maple.restserver.entity.hr;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.Valid;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;

@Entity
public class  EmployeeHdr implements Serializable {
	
	private static final long serialVersionUID=1L;
	@Id 
	@GeneratedValue(generator="uuid")
	@GenericGenerator(name="uuid",strategy="uuid2")
	@Column(length = 50)
	private String id;
	@Column(length = 50)
	private String employeeId;
	@Column(length = 50)
	private String employeeName;
	private Date dateOfBirth;
	private Date dateOfJoining;
	@Column(length = 50)
	private String previousCompany;
	
	@Column(length = 50)
	private String gradMst;
	@Column(length = 50)
	private String gender;
	@Column(length = 50)
	private String address;
	@Column(length = 20)
	private String contactNumber;
	@Column(length = 50)
	private String emailId;
	@Column(length = 10)
	private String branchCode;
	@Column(length = 50)
    private String paymentMode;
	

	@ManyToOne(fetch=FetchType.EAGER,optional=false)
	@JoinColumn(name="companyMst",nullable=false)
	@OnDelete(action=OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	


	
	public String getId() {
		return id;
	}





	public void setId(String id) {
		this.id = id;
	}








	public String getEmployeeName() {
		return employeeName;
	}





	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}





	public Date getDateOfBirth() {
		return dateOfBirth;
	}





	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}





	public Date getDateOfJoining() {
		return dateOfJoining;
	}





	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}





	public String getPreviousCompany() {
		return previousCompany;
	}





	public void setPreviousCompany(String previousCompany) {
		this.previousCompany = previousCompany;
	}





	public String getGradMst() {
		return gradMst;
	}





	public void setGradMst(String gradMst) {
		this.gradMst = gradMst;
	}





	public String getGender() {
		return gender;
	}





	public void setGender(String gender) {
		this.gender = gender;
	}





	public String getAddress() {
		return address;
	}





	public void setAddress(String address) {
		this.address = address;
	}





	public String getContactNumber() {
		return contactNumber;
	}





	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}







	public String getEmailId() {
		return emailId;
	}





	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}





	public String getBranchCode() {
		return branchCode;
	}





	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}





	public String getPaymentMode() {
		return paymentMode;
	}





	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}





	public CompanyMst getCompanyMst() {
		return companyMst;
	}





	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}





	public static long getSerialversionuid() {
		return serialVersionUID;
	}





	public String getEmployeeId() {
		return employeeId;
	}





	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}





	@Override
	public String toString() {
		return "EmployeeHdr [id=" + id + ", employeeId=" + employeeId + ", employeeName=" + employeeName
				+ ", dateOfBirth=" + dateOfBirth + ", dateOfJoining=" + dateOfJoining + ", previousCompany="
				+ previousCompany + ", gradMst=" + gradMst + ", gender=" + gender + ", address=" + address
				+ ", contactNumber=" + contactNumber + ", emailId=" + emailId + ", branchCode=" + branchCode
				+ ", paymentMode=" + paymentMode + ", companyMst=" + companyMst + "]";
	}





	
	
	
	






	
	
	
	

}
