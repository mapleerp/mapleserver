package com.maple.restserver.entity.hr;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;

@Entity

public class WorkingDaysMst implements Serializable {
	
	private static final long serialVersionUID=1L;
	@Id 
	@GeneratedValue(generator="uuid")
	@GenericGenerator(name="uuid",strategy="uuid2")
	@Column(length = 50)
	private String id;
	@Column(length = 50)
	private String workingDays;
	@Column(length = 50)
	private String workingHour;
	@Column(length = 10)
	private String branchCode;

	@ManyToOne(fetch=FetchType.EAGER,optional=false)
	@JoinColumn(name="companyMst",nullable=false)
	@OnDelete(action=OnDeleteAction.CASCADE)
	CompanyMst companyMst;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWorkingDays() {
		return workingDays;
	}

	public void setWorkingDays(String workingDays) {
		this.workingDays = workingDays;
	}

	public String getWorkingHour() {
		return workingHour;
	}

	public void setWorkingHour(String workingHour) {
		this.workingHour = workingHour;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "WorkingDaysMst [id=" + id + ", workingDays=" + workingDays + ", workingHour=" + workingHour
				+ ", branchCode=" + branchCode + ", companyMst=" + companyMst + "]";
	}
	
	
	
	
	
}
