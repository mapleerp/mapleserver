package com.maple.restserver.entity.hr;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;

@Entity
public class AttendenceMst implements Serializable {
	private static final long serialVersionUID=1L;
	@Id 
	@GeneratedValue(generator="uuid")
	@GenericGenerator(name="uuid",strategy="uuid2")
	@Column(length = 50)
	private String id;
	private Date date;
	@Column(length = 50)
	private String startingTime;
	@Column(length = 10)
	private String branchCode;
	@Column(length = 20)
	private String endingTime;
	@Column(length = 20)
	private String workingHours;
	@Column(length = 20)
	private String workingDay;
	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getStartingTime() {
		return startingTime;
	}

	public void setStartingTime(String startingTime) {
		this.startingTime = startingTime;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getEndingTime() {
		return endingTime;
	}

	public void setEndingTime(String endingTime) {
		this.endingTime = endingTime;
	}

	public String getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(String workingHours) {
		this.workingHours = workingHours;
	}

	public String getWorkingDay() {
		return workingDay;
	}

	public void setWorkingDay(String workingDay) {
		this.workingDay = workingDay;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public EmployeeHdr getEmployeeHdr() {
		return employeeHdr;
	}

	public void setEmployeeHdr(EmployeeHdr employeeHdr) {
		this.employeeHdr = employeeHdr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@ManyToOne(fetch=FetchType.EAGER,optional=false)
	@JoinColumn(name="companyMst",nullable=false)
	@OnDelete(action=OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	@ManyToOne(fetch=FetchType.EAGER,optional=false)
	@JoinColumn(name="EmployeeHdr",nullable=false)
	@OnDelete(action=OnDeleteAction.CASCADE)
	EmployeeHdr employeeHdr;



	@Override
	public String toString() {
		return "AttendanceMst [id=" + id + ", date=" + date + ", startingTime=" + startingTime + ", branchCode="
				+ branchCode + ", endingTime=" + endingTime + ", workingHours=" + workingHours + ", workingDay="
				+ workingDay + ", companyMst=" + companyMst + ", employeeHdr=" + employeeHdr + "]";
	}



}
