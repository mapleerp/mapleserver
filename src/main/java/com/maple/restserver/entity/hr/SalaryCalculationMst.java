package com.maple.restserver.entity.hr;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;

@Entity

public class SalaryCalculationMst implements Serializable {
	
	private static final long serialVersionUID=1L;
	@Id 
	@GeneratedValue(generator="uuid")
	@GenericGenerator(name="uuid",strategy="uuid2")
	@Column(length = 50)
	private String  id;
	@Column(length = 50)
	private String employeemst;
	@Column(length = 50)
	private String totalworkingdays;
	@Column(length = 50)
	private String totalworkinghours;
	@Column(length = 50)
	private String salarytobepaid;
	@Column(length = 50)
	private String branchcode;
	@Column(length = 50)
	private String allowancepaid;
	
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getEmployeemst() {
		return employeemst;
	}


	public void setEmployeemst(String employeemst) {
		this.employeemst = employeemst;
	}


	public String getTotalworkingdays() {
		return totalworkingdays;
	}


	public void setTotalworkingdays(String totalworkingdays) {
		this.totalworkingdays = totalworkingdays;
	}


	public String getTotalworkinghours() {
		return totalworkinghours;
	}


	public void setTotalworkinghours(String totalworkinghours) {
		this.totalworkinghours = totalworkinghours;
	}


	public String getSalarytobepaid() {
		return salarytobepaid;
	}


	public void setSalarytobepaid(String salarytobepaid) {
		this.salarytobepaid = salarytobepaid;
	}


	public String getBranchcode() {
		return branchcode;
	}


	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}


	public String getAllowancepaid() {
		return allowancepaid;
	}


	public void setAllowancepaid(String allowancepaid) {
		this.allowancepaid = allowancepaid;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@ManyToOne(fetch=FetchType.EAGER,optional=false)
	@JoinColumn(name="companyMst",nullable=false)
	@OnDelete(action=OnDeleteAction.CASCADE)
	CompanyMst companyMst;


	@Override
	public String toString() {
		return "SalaryCalculationMst [id=" + id + ", employeemst=" + employeemst + ", totalworkingdays="
				+ totalworkingdays + ", totalworkinghours=" + totalworkinghours + ", salarytobepaid=" + salarytobepaid
				+ ", branchcode=" + branchcode + ", allowancepaid=" + allowancepaid + ", companyMst=" + companyMst
				+ "]";
	}
	
	
	
}
