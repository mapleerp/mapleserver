package com.maple.restserver.entity.hr;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.maple.restserver.entity.CompanyMst;


@Entity
public class EmployeeDtl implements Serializable {
	
	private static final long serialVersionUID=1L;
	@Id
	@GeneratedValue(generator="uuid")
	@GenericGenerator(name="uuid",strategy="uuid2")
	 
	@Column(length = 50)
		private String id;
	@Column(length = 50)
		private String employeeLocation;
	@Column(length = 50)
		private String maritalStatus;
	@Column(length = 50)
		private String bloodGroup;
	@Column(length = 50)
		private String placeofBirth;
	@Column(length = 50)
		private String educationQualification;
	@Column(length = 10)
		private String branchCode;
		
		@ManyToOne(fetch=FetchType.EAGER,optional=false)
		@JoinColumn(name="companyMst",nullable=false)
		@OnDelete(action=OnDeleteAction.CASCADE)
		CompanyMst companyMst;

		@ManyToOne(fetch=FetchType.EAGER,optional=false)
		@JoinColumn(name="employeeHdr",nullable=false)
		@OnDelete(action=OnDeleteAction.CASCADE)
		EmployeeHdr employeeHdr;

		@Override
		public String toString() {
			return "EmployeeDtl [id=" + id + ", employeeLocation=" + employeeLocation + ", maritalStatus="
					+ maritalStatus + ", bloodGroup=" + bloodGroup + ", placeofBirth=" + placeofBirth
					+ ", educationQualification=" + educationQualification + ", branchCode=" + branchCode
					+ ", companyMst=" + companyMst + ", employeeHdr=" + employeeHdr + "]";
		}



		public String getId() {
			return id;
		}



		public void setId(String id) {
			this.id = id;
		}



		public String getEmployeeLocation() {
			return employeeLocation;
		}



		public void setEmployeeLocation(String employeeLocation) {
			this.employeeLocation = employeeLocation;
		}



		public String getMaritalStatus() {
			return maritalStatus;
		}



		public void setMaritalStatus(String maritalStatus) {
			this.maritalStatus = maritalStatus;
		}



		public String getBloodGroup() {
			return bloodGroup;
		}



		public void setBloodGroup(String bloodGroup) {
			this.bloodGroup = bloodGroup;
		}



		public String getPlaceofBirth() {
			return placeofBirth;
		}



		public void setPlaceofBirth(String placeofBirth) {
			this.placeofBirth = placeofBirth;
		}



		public String getEducationQualification() {
			return educationQualification;
		}



		public void setEducationQualification(String educationQualification) {
			this.educationQualification = educationQualification;
		}



		public String getBranchCode() {
			return branchCode;
		}



		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}



		public CompanyMst getCompanyMst() {
			return companyMst;
		}



		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}



		public static long getSerialversionuid() {
			return serialVersionUID;
		}



		public EmployeeHdr getEmployeeHdr() {
			return employeeHdr;
		}



		public void setEmployeeHdr(EmployeeHdr employeeHdr) {
			this.employeeHdr = employeeHdr;
		}



	
	
	 }
		
		
		
		

