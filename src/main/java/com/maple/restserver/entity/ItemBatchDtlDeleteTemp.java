package com.maple.restserver.entity;

 

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class ItemBatchDtlDeleteTemp implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	private String id;
	@Column(length = 50)
	private String itemId;
	@Column(length = 50)
	private String batch ;
	@Column(length = 50)
	private String barcode;
	private Double qtyIn;
	private Double qtyOut;
	private Double mrp;
	@Column(length = 50)
	private String voucherNumber;
	private Date voucherDate;
	@Column(length = 50)
	private String sourceVoucherNumber;
	private  Date sourceVoucherDate;
	private Date expDate;
	@Column(length = 50)
	private String branchCode;
	@Column(length = 50)
	private String particulars;
	@Column(length = 50)
	private String sourceParentId;
	@Column(length = 50)
	private String sourceDtlId;
	 @Column(nullable = false ,columnDefinition = "varchar(255) default 'MAIN'") 
	private String store;
	 
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	@UpdateTimestamp
	@Column(nullable=true)
	private LocalDateTime updatedTime;


	
	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
 
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	 

	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getQtyIn() {
		return qtyIn;
	}
	public void setQtyIn(Double qtyIn) {
		this.qtyIn = qtyIn;
	}
	public Double getQtyOut() {
		return qtyOut;
	}
	public void setQtyOut(Double qtyOut) {
		this.qtyOut = qtyOut;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getSourceVoucherNumber() {
		return sourceVoucherNumber;
	}
	public void setSourceVoucherNumber(String sourceVoucherNumber) {
		this.sourceVoucherNumber = sourceVoucherNumber;
	}
	public  Date  getSourceVoucherDate() {
		return sourceVoucherDate;
	}
	public void setSourceVoucherDate( Date sourceVoucherDate) {
		this.sourceVoucherDate = sourceVoucherDate;
	}
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getParticulars() {
		return particulars;
	}
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	
	public String getSourceParentId() {
		return sourceParentId;
	}
	public void setSourceParentId(String sourceParentId) {
		this.sourceParentId = sourceParentId;
	}
	public String getSourceDtlId() {
		return sourceDtlId;
	}
	public void setSourceDtlId(String sourceDtlId) {
		this.sourceDtlId = sourceDtlId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ItemBatchDtlDeleteTemp [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", barcode=" + barcode
				+ ", qtyIn=" + qtyIn + ", qtyOut=" + qtyOut + ", mrp=" + mrp + ", voucherNumber=" + voucherNumber
				+ ", voucherDate=" + voucherDate + ", sourceVoucherNumber=" + sourceVoucherNumber
				+ ", sourceVoucherDate=" + sourceVoucherDate + ", expDate=" + expDate + ", branchCode=" + branchCode
				+ ", particulars=" + particulars + ", sourceParentId=" + sourceParentId + ", sourceDtlId=" + sourceDtlId
				+ ", store=" + store + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", updatedTime=" + updatedTime + "]";
	}
	
 
	
	 

}
