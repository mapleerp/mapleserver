package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.*;

@JsonAutoDetect(fieldVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY)
@Entity
public class IntentDtl  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	
	@Column(length = 50)
	private String id;
	@Column(length = 50)
	private String itemId;
	@Column(length = 50)
	private String UnitId;

	private Double Qty;
	
	private Double rate;
	@Column(length = 50)
	private String toBranch;
	
	private String remark;
	private String itemPropertyAsJson;
	
 
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "intentHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private IntentHdr intentHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;


	
	public IntentDtl() {
		
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



	public String getUnitId() {
		return UnitId;
	}

	public void setUnitId(String unitId) {
		UnitId = unitId;
	}

	public String getToBranch() {
		return toBranch;
	}


	public void setToBranch(String toBranch) {
		this.toBranch = toBranch;
	}


	public Double getQty() {
		return Qty;
	}

	public void setQty(Double qty) {
		Qty = qty;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public IntentHdr getIntentHdr() {
		return intentHdr;
	}

	public void setIntentHdr(IntentHdr intentHdr) {
		this.intentHdr = intentHdr;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	

	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	 


	public String getItemPropertyAsJson() {
		return itemPropertyAsJson;
	}


	public void setItemPropertyAsJson(String itemPropertyAsJson) {
		this.itemPropertyAsJson = itemPropertyAsJson;
	}




	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "IntentDtl [id=" + id + ", itemId=" + itemId + ", UnitId=" + UnitId + ", Qty=" + Qty + ", rate=" + rate
				+ ", toBranch=" + toBranch + ", remark=" + remark + ", itemPropertyAsJson=" + itemPropertyAsJson
				+ ", intentHdr=" + intentHdr + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


	


}
