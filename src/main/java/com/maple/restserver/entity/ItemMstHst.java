package com.maple.restserver.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

 
@Entity
public class ItemMstHst implements Serializable{
	
	private static final long serialVersionUID = 1L;
 
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	

	@Column(unique=false, nullable = false)
	
 	private String itemName;

	@Column(nullable = false)
	private String unitId;

	private Double standardPrice;
	@Column(length = 50)
	private String barCode;
	@Column(length = 50)
	private String branchCode;
	@Column(length = 50)
	private String machineId;
	@Column(length = 50)
	private String itemGroupId;

	

	private String itemGenericName;
	@Column(length = 50)
	private String itemCode;
	@Column(length = 50)
	private String binNo;

	private Double itemDiscount;

	private String itemDtl;

	private Integer bestBefore;
	@Column(length = 50)
	private String barCodeLine1;
	@Column(length = 50)
	private String barCodeLine2;
	@Column(length = 50)
	private String supplierId;

	private Double taxRate;

	private Double sgst;

	private Double cgst;
	@Column(length = 50)
	private String hsnCode;
	@Column(length = 50)
	private String isDeleted;
	
	private Integer reorderWaitingPeriod;
 

	private Double cess ;
	@Column(length = 50)
	private String categoryId;
	
	private int isKit;
	@Column(length = 50)
	private String itemId;
	
	
	@CreationTimestamp
	@Column(nullable=false)
	private LocalDateTime updatedTime;
	
	
	@Column(length = 50)
	
	private String userid;
	
/*	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "categoryMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CategoryMst categoryMst;
	
*/
	
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	
	public Integer getReorderWaitingPeriod() {
		return reorderWaitingPeriod;
	}



	public void setReorderWaitingPeriod(Integer reorderWaitingPeriod) {
		this.reorderWaitingPeriod = reorderWaitingPeriod;
	}



	public String getId() {
		return id;
	}

	 

	public String getItemId() {
		return itemId;
	}



	public void setItemId(String itemId) {
		this.itemId = itemId;
	}



	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public Double getStandardPrice() {
		return standardPrice;
	}

	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getItemGroupId() {
		return itemGroupId;
	}

	public void setItemGroupId(String itemGroupId) {
		this.itemGroupId = itemGroupId;
	}
 
	public String getItemGenericName() {
		return itemGenericName;
	}

	public void setItemGenericName(String itemGenericName) {
		this.itemGenericName = itemGenericName;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getBinNo() {
		return binNo;
	}

	public void setBinNo(String binNo) {
		this.binNo = binNo;
	}

	 

	public String getItemDtl() {
		return itemDtl;
	}

	public void setItemDtl(String itemDtl) {
		this.itemDtl = itemDtl;
	}

	public Integer getBestBefore() {
		return bestBefore;
	}

	public void setBestBefore(Integer bestBefore) {
		this.bestBefore = bestBefore;
	}

	public String getBarCodeLine1() {
		return barCodeLine1;
	}

	public void setBarCodeLine1(String barCodeLine1) {
		this.barCodeLine1 = barCodeLine1;
	}

	public String getBarCodeLine2() {
		return barCodeLine2;
	}

	public void setBarCodeLine2(String barCodeLine2) {
		this.barCodeLine2 = barCodeLine2;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getSgst() {
		return sgst;
	}

	public void setSgst(Double sgst) {
		this.sgst = sgst;
	}

	public Double getCgst() {
		return cgst;
	}

	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsmCode) {
		this.hsnCode = hsmCode;
	}



	public Double getItemDiscount() {
		return itemDiscount;
	}



	public void setItemDiscount(Double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getIsDeleted() {
		return isDeleted;
	}



	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}



	public Double getCess() {
		return cess;
	}



	public void setCess(Double cess) {
		this.cess = cess;
	}






	 


	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public String getCategoryId() {
		return categoryId;
	}



	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}





	public int getIsKit() {
		return isKit;
	}



	public void setIsKit(int isKit) {
		this.isKit = isKit;
	}



	


	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}



	


	public String getUserid() {
		return userid;
	}



	public void setUserid(String userid) {
		this.userid = userid;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	@Override
	public String toString() {
		return "ItemMstHst [id=" + id + ", itemName=" + itemName + ", unitId=" + unitId + ", standardPrice="
				+ standardPrice + ", barCode=" + barCode + ", branchCode=" + branchCode + ", machineId=" + machineId
				+ ", itemGroupId=" + itemGroupId + ", itemGenericName=" + itemGenericName + ", itemCode=" + itemCode
				+ ", binNo=" + binNo + ", itemDiscount=" + itemDiscount + ", itemDtl=" + itemDtl + ", bestBefore="
				+ bestBefore + ", barCodeLine1=" + barCodeLine1 + ", barCodeLine2=" + barCodeLine2 + ", supplierId="
				+ supplierId + ", taxRate=" + taxRate + ", sgst=" + sgst + ", cgst=" + cgst + ", hsnCode=" + hsnCode
				+ ", isDeleted=" + isDeleted + ", reorderWaitingPeriod=" + reorderWaitingPeriod + ", cess=" + cess
				+ ", categoryId=" + categoryId + ", isKit=" + isKit + ", itemId=" + itemId + ", updatedTime="
				+ updatedTime + ", userid=" + userid + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}


 
	 
}
