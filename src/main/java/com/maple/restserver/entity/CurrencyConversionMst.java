package com.maple.restserver.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class CurrencyConversionMst implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	
	String id;
	@Column(length = 50)
	String toCurrencyId;
	Double conversionRate;
	@Column(length = 50)
	String branchCode;

	@Column(length = 50)
	String fromCurrencyId;
	
	Double fromQunatity;

	@CreationTimestamp
	@Column(nullable=false)
	private LocalDateTime updatedTime;
	 
	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	
	public String getToCurrencyId() {
		return toCurrencyId;
	}
	public void setToCurrencyId(String toCurrencyId) {
		this.toCurrencyId = toCurrencyId;
	}
	
	public Double getConversionRate() {
		return conversionRate;
	}
	public void setConversionRate(Double conversionRate) {
		this.conversionRate = conversionRate;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "CurrencyConversionMst [id=" + id + ", currencyId=" + toCurrencyId + ", conversionRate=" + conversionRate
				+ ", branchCode=" + branchCode + ", updatedTime=" + updatedTime + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	public String getFromCurrencyId() {
		return fromCurrencyId;
	}
	public void setFromCurrencyId(String fromCurrencyId) {
		this.fromCurrencyId = fromCurrencyId;
	}
	public Double getFromQunatity() {
		return fromQunatity;
	}
	public void setFromQunatity(Double fromQunatity) {
		this.fromQunatity = fromQunatity;
	}
	
}
