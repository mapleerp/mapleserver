package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
public class ItemPropertyInstance implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	@Column(length = 50)
 
	private String branchCode;
	@Column(length = 50)
	
	
	private String itemId;
	@Column(length = 50)
	private String propertyValue;
	@Column(length = 50)
	private String propertyName;
	@Column(length = 50)
	private String batch ;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "purchaseDtl", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	PurchaseDtl purchaseDtl;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;






	public String getId() {
		return id;
	}






	public PurchaseDtl getPurchaseDtl() {
		return purchaseDtl;
	}






	public void setPurchaseDtl(PurchaseDtl purchaseDtl) {
		this.purchaseDtl = purchaseDtl;
	}






	public void setId(String id) {
		this.id = id;
	}



	public String getBranchCode() {
		return branchCode;
	}



	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}



	public String getItemId() {
		return itemId;
	}



	public void setItemId(String itemId) {
		this.itemId = itemId;
	}



	public String getPropertyValue() {
		return propertyValue;
	}



	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}



	public String getPropertyName() {
		return propertyName;
	}



	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}



	public String getBatch() {
		return batch;
	}



	public void setBatch(String batch) {
		this.batch = batch;
	}



	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}










	public String getProcessInstanceId() {
		return processInstanceId;
	}






	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}






	public String getTaskId() {
		return taskId;
	}






	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}






	@Override
	public String toString() {
		return "ItemPropertyInstance [id=" + id + ", branchCode=" + branchCode + ", itemId=" + itemId
				+ ", propertyValue=" + propertyValue + ", propertyName=" + propertyName + ", batch=" + batch
				+ ", purchaseDtl=" + purchaseDtl + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
	
	
	 
	
	
	
	

}
