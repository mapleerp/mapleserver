package com.maple.restserver.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class SessionEndClosureMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	String userId;
	@Column(length = 50)
	String branchCode;
	Double cashSale;
	Double cardSale;
	Double cardSale2;
	Double physicalCash;
	Date sessionDate;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	@CreationTimestamp
	@Column(nullable=false)
	
	private LocalDateTime updatedTime;
	Integer slNo;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public Double getCashSale() {
		return cashSale;
	}

	public void setCashSale(Double cashSale) {
		this.cashSale = cashSale;
	}

	public Double getCardSale() {
		return cardSale;
	}

	public void setCardSale(Double cardSale) {
		this.cardSale = cardSale;
	}

	public Double getCardSale2() {
		return cardSale2;
	}

	public void setCardSale2(Double cardSale2) {
		this.cardSale2 = cardSale2;
	}

	public Double getPhysicalCash() {
		return physicalCash;
	}

	public void setPhysicalCash(Double physicalCash) {
		this.physicalCash = physicalCash;
	}

	public Date getSessionDate() {
		return sessionDate;
	}

	public void setSessionDate(Date sessionDate) {
		this.sessionDate = sessionDate;
	}

	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SessionEndClosureMst [id=" + id + ", userId=" + userId + ", branchCode=" + branchCode + ", cashSale="
				+ cashSale + ", cardSale=" + cardSale + ", cardSale2=" + cardSale2 + ", physicalCash=" + physicalCash
				+ ", sessionDate=" + sessionDate + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", updatedTime=" + updatedTime + ", slNo=" + slNo + ", companyMst=" + companyMst + "]";
	}
	
	

}
