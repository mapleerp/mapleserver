package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.message.entity.SalesDtlMessage;
import com.maple.restserver.message.entity.SalesTransHdrMessage;
@Component
public class OtherBranchSalesMessageEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	OtherBranchSalesTransHdr otherBranchSalesTransHdr;
	ArrayList<OtherBranchSalesDtl> salesDtlList =new ArrayList();
	ArrayList<ItemMst> itemList = new ArrayList();
	ArrayList<CategoryMst> categoryList =new ArrayList();
	 BranchMst branchMst;
	 @Column(length = 50)
	 private String  processInstanceId;
	 @Column(length = 50)
		private String taskId;
 
	
	public ArrayList<ItemMst> getItemList() {
		return itemList;
	}
	public void setItemList(ArrayList<ItemMst> itemList) {
		this.itemList = itemList;
	}
	public ArrayList<CategoryMst> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(ArrayList<CategoryMst> categoryList) {
		this.categoryList = categoryList;
	}
	public BranchMst getBranchMst() {
		return branchMst;
	}
	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}
	public OtherBranchSalesTransHdr getOtherBranchSalesTransHdr() {
		return otherBranchSalesTransHdr;
	}
	public void setOtherBranchSalesTransHdr(OtherBranchSalesTransHdr otherBranchSalesTransHdr) {
		this.otherBranchSalesTransHdr = otherBranchSalesTransHdr;
	}
	public ArrayList<OtherBranchSalesDtl> getSalesDtlList() {
		return salesDtlList;
	}
	public void setSalesDtlList(ArrayList<OtherBranchSalesDtl> salesDtlList) {
		this.salesDtlList = salesDtlList;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "OtherBranchSalesMessageEntity [otherBranchSalesTransHdr=" + otherBranchSalesTransHdr + ", salesDtlList="
				+ salesDtlList + ", itemList=" + itemList + ", categoryList=" + categoryList + ", branchMst="
				+ branchMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	  
	
	
 
	
}
