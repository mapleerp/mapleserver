package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class StockTransferInReturnHdr  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	 @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	@Column(length = 50)
	
	String intentNumber;
	Date 	voucherDate;
	@Column(length = 50)
	String voucherNumber;
	@Column(length = 50)
	String voucherType;
	@Column(length = 50)
	String branchCode;
	@Column(length = 50)
	String deleted;
	@Column(length = 50)
	String fromBranch;
	@Column(length = 50)
	String inVoucherNumber;
	Date inVoucherDate;
	@Column(length = 50)
	String statusAcceptStock;
	
	 @Column(unique=true )
	String stockTransferOutHdrId;
	 @Column(length = 50)
	String narration;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "stockTransferInHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	StockTransferInHdr stockTransferInHdr;
	@Column(length = 50)
	
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIntentNumber() {
		return intentNumber;
	}
	public void setIntentNumber(String intentNumber) {
		this.intentNumber = intentNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	public String getInVoucherNumber() {
		return inVoucherNumber;
	}
	public void setInVoucherNumber(String inVoucherNumber) {
		this.inVoucherNumber = inVoucherNumber;
	}
	public Date getInVoucherDate() {
		return inVoucherDate;
	}
	public void setInVoucherDate(Date inVoucherDate) {
		this.inVoucherDate = inVoucherDate;
	}
	public String getStatusAcceptStock() {
		return statusAcceptStock;
	}
	public void setStatusAcceptStock(String statusAcceptStock) {
		this.statusAcceptStock = statusAcceptStock;
	}
	public String getStockTransferOutHdrId() {
		return stockTransferOutHdrId;
	}
	public void setStockTransferOutHdrId(String stockTransferOutHdrId) {
		this.stockTransferOutHdrId = stockTransferOutHdrId;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public StockTransferInHdr getStockTransferInHdr() {
		return stockTransferInHdr;
	}
	public void setStockTransferInHdr(StockTransferInHdr stockTransferInHdr) {
		this.stockTransferInHdr = stockTransferInHdr;
	}
	@Override
	public String toString() {
		return "StockTransferInReturnHdr [id=" + id + ", intentNumber=" + intentNumber + ", voucherDate=" + voucherDate
				+ ", voucherNumber=" + voucherNumber + ", voucherType=" + voucherType + ", branchCode=" + branchCode
				+ ", deleted=" + deleted + ", fromBranch=" + fromBranch + ", inVoucherNumber=" + inVoucherNumber
				+ ", inVoucherDate=" + inVoucherDate + ", statusAcceptStock=" + statusAcceptStock
				+ ", stockTransferOutHdrId=" + stockTransferOutHdrId + ", narration=" + narration + ", companyMst="
				+ companyMst + ", stockTransferInHdr=" + stockTransferInHdr + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
	
	
	
	
	
}
