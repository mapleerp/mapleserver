package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ProfitLoss implements Serializable {


	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	Integer id;
	@Column(length = 50)
	String Entry;
	@Column(length = 50)
	
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public Integer getId() {
		return id;
	}
	
	public String getEntry() {
		return Entry;
	}
	public void setEntry(String entry) {
		Entry = entry;
	}

	
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ProfitLoss [id=" + id + ", Entry=" + Entry + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
	
	

}
