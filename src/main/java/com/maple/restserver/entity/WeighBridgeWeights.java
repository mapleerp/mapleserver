package com.maple.restserver.entity;

import java.io.Serializable;
 
import java.time.LocalDateTime;
 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
public class WeighBridgeWeights implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	
	@JsonProperty("machineweight")
	Integer machineweight;
	@Column(length = 50)
	@JsonProperty("vehicleno")
	private String vehicleno;

	
	
	
	
 

	
	
	@JsonProperty("previousweight")
	private Integer previousweight;
	
	@JsonProperty("previousweightid")
	private String previousweightid;

	
	@JsonProperty("nextweight")
	private Integer nextweight;
	@Column(length = 50)
	@JsonProperty("nextweightid")
	private String nextweightid;
	
	
	
	
	
	@Column(length = 50)
	@JsonProperty("netweight")
	private String netweight;
	@Column(length = 50)
	@JsonProperty("vehicletypeid")
	private String vehicletypeid;
	@Column(length = 50)
	@JsonProperty("materialtypeid")
	private String materialtypeid;
	@Column(length = 50)
	@JsonProperty("firstweightdate")
	private String firstweightdate;
	
	@JsonProperty("rate")
	private Integer rate;

	@JsonProperty("voucherDate")
	private LocalDateTime voucherDate;
	
	@UpdateTimestamp
	LocalDateTime updatedTime;
	
	@Column(length = 50)
	@JsonProperty("voucherNumber")
	private String voucherNumber;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "branchMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private BranchMst branchMst;
	
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;


	

	public LocalDateTime getVoucherDate() {
		return voucherDate;
	}



	public void setVoucherDate(LocalDateTime voucherDate) {
		this.voucherDate = voucherDate;
	}



	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}



	public String getFirstweightdate() {
		return firstweightdate;
	}



	public void setFirstweightdate(String firstweightdate) {
		this.firstweightdate = firstweightdate;
	}



	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}


 

	public String getVoucherNumber() {
		return voucherNumber;
	}



	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public Integer getMachineweight() {
		return machineweight;
	}



	public void setMachineweight(Integer machineweight) {
		this.machineweight = machineweight;
	}


 


	public String getNetweight() {
		return netweight;
	}



	public void setNetweight(String netweight) {
		this.netweight = netweight;
	}



	public String getVehicletypeid() {
		return vehicletypeid;
	}



	public void setVehicletypeid(String vehicletypeid) {
		this.vehicletypeid = vehicletypeid;
	}



	public String getMaterialtypeid() {
		return materialtypeid;
	}



	public void setMaterialtypeid(String materialtypeid) {
		this.materialtypeid = materialtypeid;
	}



	public Integer getRate() {
		return rate;
	}



	public void setRate(Integer rate) {
		this.rate = rate;
	}


 


 


	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public BranchMst getBranchMst() {
		return branchMst;
	}



	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}


	

	public String getVehicleno() {
		return vehicleno;
	}



	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}



	public Integer getPreviousweight() {
		return previousweight;
	}



	public void setPreviousweight(Integer previousweight) {
		this.previousweight = previousweight;
	}



	public String getPreviousweightid() {
		return previousweightid;
	}



	public void setPreviousweightid(String previousweightid) {
		this.previousweightid = previousweightid;
	}



	public Integer getNextweight() {
		return nextweight;
	}



	public void setNextweight(Integer nextweight) {
		this.nextweight = nextweight;
	}



	public String getNextweightid() {
		return nextweightid;
	}



	public void setNextweightid(String nextweightid) {
		this.nextweightid = nextweightid;
	}



	



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	@Override
	public String toString() {
		return "WeighBridgeWeights [id=" + id + ", machineweight=" + machineweight + ", vehicleno=" + vehicleno
				+ ", previousweight=" + previousweight + ", previousweightid=" + previousweightid + ", nextweight="
				+ nextweight + ", nextweightid=" + nextweightid + ", netweight=" + netweight + ", vehicletypeid="
				+ vehicletypeid + ", materialtypeid=" + materialtypeid + ", firstweightdate=" + firstweightdate
				+ ", rate=" + rate + ", voucherDate=" + voucherDate + ", updatedTime=" + updatedTime
				+ ", voucherNumber=" + voucherNumber + ", branchMst=" + branchMst + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


 

 	

	
	
	

}
