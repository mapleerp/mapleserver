package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
 

@Entity

public class ProductionDtlDtl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(length = 50)
	
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	@Column(length = 50)
	private String rawMaterialItemId;
	@Column(length = 50)
	private String itemName;
	private Double qty;
	@Column(length = 50)
	private String unitId;
	
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "production_dtl_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	ProductionDtl productionDtl;
	@Column(length = 50)
	String oldId;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRawMaterialItemId() {
		return rawMaterialItemId;
	}
	public void setRawMaterialItemId(String rawMaterialItemId) {
		this.rawMaterialItemId = rawMaterialItemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public ProductionDtl getProductionDtl() {
		return productionDtl;
	}
	public void setProductionDtl(ProductionDtl productionDtl) {
		this.productionDtl = productionDtl;
	}
	
	
	
	public String getOldId() {
		return oldId;
	}
	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ProductionDtlDtl [id=" + id + ", rawMaterialItemId=" + rawMaterialItemId + ", itemName=" + itemName
				+ ", qty=" + qty + ", unitId=" + unitId + ", productionDtl=" + productionDtl + ", oldId=" + oldId
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
	 
	
	

}
