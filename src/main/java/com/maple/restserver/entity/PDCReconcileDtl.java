package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class PDCReconcileDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
    String id;
	@Column(length = 50)
	String bank;
	
	Date transDate;
	@Column(length = 50)
	String intrumentNo;
	@Column(length = 50)
	String account;
	
	Double amount;
	String transactionDetails;
	
	String remark;

	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "pdcReconcileHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	PDCReconcileHdr pdcReconcileHdr;
	@Column(length = 50)
	String pdcId;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPdcId() {
		return pdcId;
	}

	public void setPdcId(String pdcId) {
		this.pdcId = pdcId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public Date getTransDate() {
		return transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public String getIntrumentNo() {
		return intrumentNo;
	}

	public void setIntrumentNo(String intrumentNo) {
		this.intrumentNo = intrumentNo;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getTransactionDetails() {
		return transactionDetails;
	}

	public void setTransactionDetails(String transactionDetails) {
		this.transactionDetails = transactionDetails;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public PDCReconcileHdr getPdcReconcileHdr() {
		return pdcReconcileHdr;
	}

	public void setPdcReconcileHdr(PDCReconcileHdr pdcReconcileHdr) {
		this.pdcReconcileHdr = pdcReconcileHdr;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "PDCReconcileDtl [id=" + id + ", bank=" + bank + ", transDate=" + transDate + ", intrumentNo="
				+ intrumentNo + ", account=" + account + ", amount=" + amount + ", transactionDetails="
				+ transactionDetails + ", remark=" + remark + ", companyMst=" + companyMst + ", pdcReconcileHdr="
				+ pdcReconcileHdr + ", pdcId=" + pdcId + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}

	




	
	

	
	

	


}
