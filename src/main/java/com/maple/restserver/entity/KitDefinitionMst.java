package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.stereotype.Component;

@Entity
public class KitDefinitionMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id

	private String id;
	@Column(length = 50)
	private String oldId;
	@Column(length = 50)
    String itemId;
	@Column(length = 50)
	String kitName;
	@Column(length = 50)
	String unitId;
	@Column(length = 50)
	String unitName;
	Double minimumQty;
	@Column(length = 50)
	String barcode;
	Double productionCost;
	Double taxRate;
	@Column(length = 50)
	String itemCode;
	@Column(length = 50)
	String finalSave;
	@Column(length = 50)
	String branchCode;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getKitName() {
		return kitName;
	}
	public void setKitName(String kitName) {
		this.kitName = kitName;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getMinimumQty() {
		return minimumQty;
	}
	public void setMinimumQty(Double minimumQty) {
		this.minimumQty = minimumQty;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Double getProductionCost() {
		return productionCost;
	}
	public void setProductionCost(Double productionCost) {
		this.productionCost = productionCost;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	

	public String getFinalSave() {
		return finalSave;
	}
	public void setFinalSave(String finalSave) {
		this.finalSave = finalSave;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	public String getOldId() {
		return oldId;
	}
	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
	@Override
	public String toString() {
		return "KitDefinitionMst [id=" + id + ", oldId=" + oldId + ", itemId=" + itemId + ", kitName=" + kitName
				+ ", unitId=" + unitId + ", unitName=" + unitName + ", minimumQty=" + minimumQty + ", barcode="
				+ barcode + ", productionCost=" + productionCost + ", taxRate=" + taxRate + ", itemCode=" + itemCode
				+ ", finalSave=" + finalSave + ", branchCode=" + branchCode + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	

}