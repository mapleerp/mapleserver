package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity

public class ItemBatchExpiryDtl  implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	
	@Column(nullable = false)
	String itemId;
	@Column(nullable = false)
	String batch;
	@Column(nullable = false)
	Date expiryDate;
	@Column(nullable = false)
	Date updatedDate;
	String branchCode;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "purchaseDtl", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private PurchaseDtl purchaseDtl;
	
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "goodReceiveNoteDtl", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private GoodReceiveNoteDtl goodReceiveNoteDtl;
	
	
	
	
	
	
	
	
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "actualProductionDtl", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ActualProductionDtl actualProductionDtl;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "physicalStockDtl", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private PhysicalStockDtl physicalStockDtl;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "stockTransferInDtl", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private StockTransferInDtl stockTransferInDtl;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public PurchaseDtl getPurchaseDtl() {
		return purchaseDtl;
	}
	public void setPurchaseDtl(PurchaseDtl purchaseDtl) {
		this.purchaseDtl = purchaseDtl;
	}

	public ActualProductionDtl getActualProductionDtl() {
		return actualProductionDtl;
	}
	public void setActualProductionDtl(ActualProductionDtl actualProductionDtl) {
		this.actualProductionDtl = actualProductionDtl;
	}
	public PhysicalStockDtl getPhysicalStockDtl() {
		return physicalStockDtl;
	}
	public void setPhysicalStockDtl(PhysicalStockDtl physicalStockDtl) {
		this.physicalStockDtl = physicalStockDtl;
	}
	public StockTransferInDtl getStockTransferInDtl() {
		return stockTransferInDtl;
	}
	public void setStockTransferInDtl(StockTransferInDtl stockTransferInDtl) {
		this.stockTransferInDtl = stockTransferInDtl;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	
	public GoodReceiveNoteDtl getGoodReceiveNoteDtl() {
		return goodReceiveNoteDtl;
	}
	public void setGoodReceiveNoteDtl(GoodReceiveNoteDtl goodReceiveNoteDtl) {
		this.goodReceiveNoteDtl = goodReceiveNoteDtl;
	}
	@Override
	public String toString() {
		return "ItemBatchExpiryDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", expiryDate=" + expiryDate
				+ ", updatedDate=" + updatedDate + ", branchCode=" + branchCode + ", companyMst=" + companyMst
				+ ", purchaseDtl=" + purchaseDtl + ", goodReceiveNoteDtl=" + goodReceiveNoteDtl
				+ ", actualProductionDtl=" + actualProductionDtl + ", physicalStockDtl=" + physicalStockDtl
				+ ", stockTransferInDtl=" + stockTransferInDtl + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
	

}
