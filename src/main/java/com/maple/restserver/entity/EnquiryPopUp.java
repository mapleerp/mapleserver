package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;

public class EnquiryPopUp implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	String itemName;
	@Column(length = 50)
	String voucherType;
	@Column(length = 50)
	String unitName;
	Double mrp;
	Double qtyIn;
	Double qtyOut;
	@Column(length = 50)
	String itemId;
	@Column(length = 50)
	String unitId;
	@Column(length = 50)
	String batch;
	@Column(length = 50)
	
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getQtyIn() {
		return qtyIn;
	}
	public void setQtyIn(Double qtyIn) {
		this.qtyIn = qtyIn;
	}
	public Double getQtyOut() {
		return qtyOut;
	}
	public void setQtyOut(Double qtyOut) {
		this.qtyOut = qtyOut;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "EnquiryPopUp [itemName=" + itemName + ", voucherType=" + voucherType + ", unitName=" + unitName
				+ ", mrp=" + mrp + ", qtyIn=" + qtyIn + ", qtyOut=" + qtyOut + ", itemId=" + itemId + ", unitId="
				+ unitId + ", batch=" + batch + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
