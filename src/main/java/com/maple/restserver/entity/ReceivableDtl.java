package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
public class ReceivableDtl implements Serializable {


	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	Integer id;
	@JsonProperty("username")
	@Column(length = 50)
	String UserName;
	
	

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "receivableHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	ReceivableHdr receivableHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}

	public ReceivableHdr getReceivableHdr() {
		return receivableHdr;
	}
	public void setReceivableHdr(ReceivableHdr receivableHdr) {
		this.receivableHdr = receivableHdr;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ReceivableDtl [id=" + id + ", UserName=" + UserName + ", receivableHdr=" + receivableHdr
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	

}
