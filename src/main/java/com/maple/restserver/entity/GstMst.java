package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
 
@Entity
public class GstMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	private String id;

	double igst;
	double cgst;
	double sgst;
	@Column(length = 50)
	   String  igstOutputAccountId;
	@Column(length = 50)
	    String  igstInputputAccountId;
	@Column(length = 50)
	    String  cgstOutputAccountId;
	@Column(length = 50)
	    String  cgstInputputAccountId;
	@Column(length = 50)
	    String  sgstOutputAccountId;
	@Column(length = 50)
	    String  sgstInputputAccountId;

		@ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "companyMst", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		CompanyMst companyMst;
		@Column(length = 50)
		private String  processInstanceId;
		@Column(length = 50)
		private String taskId;
		
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getIgst() {
		return igst;
	}
	public void setIgst(double igst) {
		this.igst = igst;
	}
	public double getCgst() {
		return cgst;
	}
	public void setCgst(double cgst) {
		this.cgst = cgst;
	}
	public double getSgst() {
		return sgst;
	}
	public void setSgst(double sgst) {
		this.sgst = sgst;
	}
	
	public String getIgstOutputAccountId() {
		return igstOutputAccountId;
	}
	public void setIgstOutputAccountId(String igstOutputAccountId) {
		this.igstOutputAccountId = igstOutputAccountId;
	}
	public String getIgstInputputAccountId() {
		return igstInputputAccountId;
	}
	public void setIgstInputputAccountId(String igstInputputAccountId) {
		this.igstInputputAccountId = igstInputputAccountId;
	}
	public String getCgstOutputAccountId() {
		return cgstOutputAccountId;
	}
	public void setCgstOutputAccountId(String cgstOutputAccountId) {
		this.cgstOutputAccountId = cgstOutputAccountId;
	}
	public String getCgstInputputAccountId() {
		return cgstInputputAccountId;
	}
	public void setCgstInputputAccountId(String cgstInputputAccountId) {
		this.cgstInputputAccountId = cgstInputputAccountId;
	}
	public String getSgstOutputAccountId() {
		return sgstOutputAccountId;
	}
	public void setSgstOutputAccountId(String sgstOutputAccountId) {
		this.sgstOutputAccountId = sgstOutputAccountId;
	}
	

	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "GstMst [id=" + id + ", igst=" + igst + ", cgst=" + cgst + ", sgst=" + sgst + ", igstOutputAccountId="
				+ igstOutputAccountId + ", igstInputputAccountId=" + igstInputputAccountId + ", cgstOutputAccountId="
				+ cgstOutputAccountId + ", cgstInputputAccountId=" + cgstInputputAccountId + ", sgstOutputAccountId="
				+ sgstOutputAccountId + ", sgstInputputAccountId=" + sgstInputputAccountId + ", companyMst="
				+ companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
}
