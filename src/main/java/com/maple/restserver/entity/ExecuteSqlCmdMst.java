package com.maple.restserver.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

 

 
@Entity
public class ExecuteSqlCmdMst {
	
	

	public ExecuteSqlCmdMst(String id, String sql, String executeStatus) {
		super();
		this.id = id;
 
		this.sqlCommand = sql;
 
	 
 
		this.executeStatus = executeStatus;
	}
	@Column(length = 50)
	@Id
	private String id;
	
	 
	@Column(nullable = false, columnDefinition = "varchar(1024)")
 
	private String sqlCommand;
 
 
 
	
	@Column(nullable = false, columnDefinition = "varchar(10)")
	private String executeStatus;




	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	public String getSqlCommand() {
		return sqlCommand;
	}




	public void setSqlCommand(String sqlCommand) {
		this.sqlCommand = sqlCommand;
	}




	public String getExecuteStatus() {
		return executeStatus;
	}




	public void setExecuteStatus(String executeStatus) {
		this.executeStatus = executeStatus;
	}




	@Override
	public String toString() {
		return "ExecuteSqlCmdMst [id=" + id + ", sqlCommand=" + sqlCommand + ", executeStatus=" + executeStatus + "]";
	}

	 
	

}
