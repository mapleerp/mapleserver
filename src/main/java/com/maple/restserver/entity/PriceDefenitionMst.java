package com.maple.restserver.entity;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import net.bytebuddy.implementation.bind.MethodDelegationBinder.BindingResolver.Unique;
@Entity
public class PriceDefenitionMst implements Serializable{

	private static final long serialVersionUID =1L;

	@Column(length = 50)
	@Id
	String id;
	@Column(unique=true)
	private String priceLevelName;
	@Column(length = 50)
	private String priceCalculator;
	@Column(length = 50)
	String oldId;
	
	
	public String getOldId() {
		return oldId;
	}
	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "companyMst", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPriceLevelName() {
		return priceLevelName;
	}
	public void setPriceLevelName(String priceLevelName) {
		this.priceLevelName = priceLevelName;
	}
	public String getPriceCalculator() {
		return priceCalculator;
	}
	public void setPriceCalculator(String priceCalculator) {
		this.priceCalculator = priceCalculator;
	}
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PriceDefenitionMst [id=" + id + ", priceLevelName=" + priceLevelName + ", priceCalculator="
				+ priceCalculator + ", oldId=" + oldId + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

	
}
