package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class ItemImageMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "item_mst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ItemMst itemMst;
	@Column(length = 50)
	String branchCode;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	 
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	private byte[] itemImage;
	
	private int priorioty;
	
	private Date dateUploaded;
	
	private int activePicture;
	
	private int pictureNumber;

	private String pictureName;
	
	
	private Double offerPrice;
	
	private Double mrp;
	
	private String ingredients;
	
	private String applicableTime;
	
	private String specialOfferDescription;
	  
	private String category;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ItemMst getItemMst() {
		return itemMst;
	}

	public void setItemMst(ItemMst itemMst) {
		this.itemMst = itemMst;
	}

	 
	public int getPriorioty() {
		return priorioty;
	}

	public void setPriorioty(int priorioty) {
		this.priorioty = priorioty;
	}

	public Date getDateUploaded() {
		return dateUploaded;
	}

	public void setDateUploaded(Date dateUploaded) {
		this.dateUploaded = dateUploaded;
	}

	public int getActivePicture() {
		return activePicture;
	}

	public void setActivePicture(int activePicture) {
		this.activePicture = activePicture;
	}

	public int getPictureNumber() {
		return pictureNumber;
	}

	public void setPictureNumber(int pictureNumber) {
		this.pictureNumber = pictureNumber;
	}

	public String getPictureName() {
		return pictureName;
	}

	public void setPictureName(String pictureName) {
		this.pictureName = pictureName;
	}

 
	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	 
	public Double getOfferPrice() {
		return offerPrice;
	}

	public void setOfferPrice(Double offerPrice) {
		this.offerPrice = offerPrice;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public String getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	public String getApplicableTime() {
		return applicableTime;
	}

	public void setApplicableTime(String applicableTime) {
		this.applicableTime = applicableTime;
	}

	public String getSpecialOfferDescription() {
		return specialOfferDescription;
	}

	public void setSpecialOfferDescription(String specialOfferDescription) {
		this.specialOfferDescription = specialOfferDescription;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

 
	public byte[] getItemImage() {
		return itemImage;
	}

	public void setItemImage(byte[] itemImage) {
		this.itemImage = itemImage;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ItemImageMst [id=" + id + ", companyMst=" + companyMst + ", itemMst=" + itemMst + ", branchCode="
				+ branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", itemImage="
				+ Arrays.toString(itemImage) + ", priorioty=" + priorioty + ", dateUploaded=" + dateUploaded
				+ ", activePicture=" + activePicture + ", pictureNumber=" + pictureNumber + ", pictureName="
				+ pictureName + ", offerPrice=" + offerPrice + ", mrp=" + mrp + ", ingredients=" + ingredients
				+ ", applicableTime=" + applicableTime + ", specialOfferDescription=" + specialOfferDescription
				+ ", category=" + category + "]";
	}

 

	
}
