package com.maple.restserver.entity;

import java.io.Serializable;

public class SchemeCategory implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String categoryId;
	Double qty;
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	@Override
	public String toString() {
		return "SchemeCategory [categoryId=" + categoryId + ", qty=" + qty + "]";
	}
	
	

}
