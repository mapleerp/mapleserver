package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class ChequePrintMst  implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	  @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	@Column(length = 50)
	String chequeProperty;
	@Column(length = 50)
	String chequeValue;
	@Column(length = 50)
	String accountId;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getChequeProperty() {
		return chequeProperty;
	}
	public void setChequeProperty(String chequeProperty) {
		this.chequeProperty = chequeProperty;
	}
	
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getChequeValue() {
		return chequeValue;
	}
	public void setChequeValue(String chequeValue) {
		this.chequeValue = chequeValue;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ChequePrintMst [id=" + id + ", chequeProperty=" + chequeProperty + ", chequeValue=" + chequeValue
				+ ", accountId=" + accountId + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	

}
