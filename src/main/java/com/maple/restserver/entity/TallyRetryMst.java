package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class TallyRetryMst  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Id
	@Column(length = 50)
	   @GeneratedValue(generator = "uuid")
	   @GenericGenerator(name = "uuid", strategy = "uuid2")
	   private String id;
	
	private Date voucherDate;
	@Column(length = 50)
	private String voucherNumber;
	@Column(length = 50)
	private String integrationStatus;
	private Integer retryNumber;
	@Column(length = 50)
	private String voucherType;
	private Date lastRetryDate;
//To creating a TallyRetryMst for retrying sales to tally according  to integration status	
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public Date getVoucherDate() {
	return voucherDate;
}
public void setVoucherDate(Date voucherDate) {
	this.voucherDate = voucherDate;
}
public String getVoucherNumber() {
	return voucherNumber;
}
public void setVoucherNumber(String voucherNumber) {
	this.voucherNumber = voucherNumber;
}
public String getIntegrationStatus() {
	return integrationStatus;
}
public void setIntegrationStatus(String integrationStatus) {
	this.integrationStatus = integrationStatus;
}

public String getVoucherType() {
	return voucherType;
}
public void setVoucherType(String voucherType) {
	this.voucherType = voucherType;
}
public Date getLastRetryDate() {
	return lastRetryDate;
}
public void setLastRetryDate(Date lastRetryDate) {
	this.lastRetryDate = lastRetryDate;
}

public CompanyMst getCompanyMst() {
	return companyMst;
}
public void setCompanyMst(CompanyMst companyMst) {
	this.companyMst = companyMst;
}
public void setRetryNumber(Integer retryNumber) {
	this.retryNumber = retryNumber;
}

public String getTaskId() {
	return taskId;
}
public void setTaskId(String taskId) {
	this.taskId = taskId;
}
public Integer getRetryNumber() {
	return retryNumber;
}
@Override
public String toString() {
	return "TallyRetryMst [id=" + id + ", voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber
			+ ", integrationStatus=" + integrationStatus + ", retryNumber=" + retryNumber + ", voucherType="
			+ voucherType + ", lastRetryDate=" + lastRetryDate + ", companyMst=" + companyMst + ", processInstanceId="
			+ processInstanceId + ", taskId=" + taskId + "]";
}
}
