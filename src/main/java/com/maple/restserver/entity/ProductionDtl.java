package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
 
@Entity
public class ProductionDtl implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	@Column(length = 50)
	private  String itemId;
	@Column(length = 50)
	private String batch;
	private Double qty;
	private String status;
	@Column(length = 50)
	String oldId;
	
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "production_mst_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	ProductionMst productionMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	private String store;

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getItemId() {
		return itemId;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	public String getBatch() {
		return batch;
	}


	public void setBatch(String batch) {
		this.batch = batch;
	}


	public Double getQty() {
		return qty;
	}


	public void setQty(Double qty) {
		this.qty = qty;
	}


	public ProductionMst getProductionMst() {
		return productionMst;
	}


	public void setProductionMst(ProductionMst productionMst) {
		this.productionMst = productionMst;
	}


	


	public String getOldId() {
		return oldId;
	}


	public void setOldId(String oldId) {
		this.oldId = oldId;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	public String getStore() {
		return store;
	}


	public void setStore(String store) {
		this.store = store;
	}


	@Override
	public String toString() {
		return "ProductionDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", qty=" + qty + ", status="
				+ status + ", oldId=" + oldId + ", productionMst=" + productionMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", store=" + store + "]";
	}


  

}
