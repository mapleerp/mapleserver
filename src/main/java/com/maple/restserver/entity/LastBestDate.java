package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class LastBestDate implements Serializable {
	
private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	
   private Date  lastSuccessDate;
   private String activityType;
   private Double count;
   private Date  dayEndDate;
   
   @ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	
	
	
	
	
	public Double getCount() {
		return count;
	}
	public void setCount(Double count) {
		this.count = count;
	}

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getLastSuccessDate() {
		return lastSuccessDate;
	}
	public void setLastSuccessDate( Date lastSuccessDate) {
		this.lastSuccessDate = lastSuccessDate;
	}
	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public Date getDayEndDate() {
		return dayEndDate;
	}
	public void setDayEndDate(Date dayEndDate) {
		this.dayEndDate = dayEndDate;
	}
	@Override
	public String toString() {
		return "LastBestDate [id=" + id + ", lastSuccessDate=" + lastSuccessDate + ", activityType=" + activityType
				+ ", count=" + count + ", dayEndDate=" + dayEndDate + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
	
	
	
	
	}
