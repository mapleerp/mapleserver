package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class CardSalesDtl implements Serializable{

	

	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	   @GeneratedValue(generator = "uuid")
	   @GenericGenerator(name = "uuid", strategy = "uuid2")
	   private String id;
		
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	String branchCode;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "salesTransHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private SalesTransHdr salesTransHdrId;
	
	@Column(length = 50)
	String accountId;
	@Column(length = 50)
	String salesReceiptId;
	@Column(length = 50)
	String userId;
	@Column(length = 50)
	String receiptMode;
	Double receiptAmount;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	java.util.Date rereceiptDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public Double getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public SalesTransHdr getSalesTransHdrId() {
		return salesTransHdrId;
	}
	public void setSalesTransHdrId(SalesTransHdr salesTransHdrId) {
		this.salesTransHdrId = salesTransHdrId;
	}
	public java.util.Date getRereceiptDate() {
		return rereceiptDate;
	}
	public void setRereceiptDate(java.util.Date rereceiptDate) {
		this.rereceiptDate = rereceiptDate;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public String getSalesReceiptId() {
		return salesReceiptId;
	}
	public void setSalesReceiptId(String salesReceiptId) {
		this.salesReceiptId = salesReceiptId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "CardSalesDtl [id=" + id + ", companyMst=" + companyMst + ", branchCode=" + branchCode
				+ ", salesTransHdrId=" + salesTransHdrId + ", accountId=" + accountId + ", salesReceiptId="
				+ salesReceiptId + ", userId=" + userId + ", receiptMode=" + receiptMode + ", receiptAmount="
				+ receiptAmount + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", rereceiptDate="
				+ rereceiptDate + "]";
	}
	

	
	
	
}
