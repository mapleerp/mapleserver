package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity

public class DayEndClosureHdr implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	private String userId;
	@Column(length = 50)
	private String branchCode;
	private Double cashSale;
	private Double cardSale;
	private Double cardSale2;
	private Double physicalCash;
	private Date processDate;
	@Column(length = 50)
	private String dayEndStatus;
	
	private Double totalSales;

	@Column(nullable = true, columnDefinition = "double default 0.0")
	private Double changeInCash;
	@Column(length = 50)
	String voucheNumber;
	Date voucherDate;
	@Column(length = 50)
	private String processInstanceId;
	@Column(length = 50)
	private String taskId;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;

	private Double cardSettlementAmount;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public Double getCashSale() {
		return cashSale;
	}

	public void setCashSale(Double cashSale) {
		this.cashSale = cashSale;
	}

	public Double getChangeInCash() {
		return changeInCash;
	}

	public void setChangeInCash(Double changeInCash) {
		this.changeInCash = changeInCash;
	}

	public Double getCardSale() {
		return cardSale;
	}

	public void setCardSale(Double cardSale) {
		this.cardSale = cardSale;
	}

	public Double getCardSale2() {
		return cardSale2;
	}

	public void setCardSale2(Double cardSale2) {
		this.cardSale2 = cardSale2;
	}

	public Double getPhysicalCash() {
		return physicalCash;
	}

	public void setPhysicalCash(Double physicalCash) {
		this.physicalCash = physicalCash;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getDayEndStatus() {
		return dayEndStatus;
	}

	public void setDayEndStatus(String dayEndStatus) {
		this.dayEndStatus = dayEndStatus;
	}

	public String getVoucheNumber() {
		return voucheNumber;
	}

	public void setVoucheNumber(String voucheNumber) {
		this.voucheNumber = voucheNumber;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Double getCardSettlementAmount() {
		return cardSettlementAmount;
	}

	public void setCardSettlementAmount(Double cardSettlementAmount) {
		this.cardSettlementAmount = cardSettlementAmount;
	}
	

	public Double getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(Double totalSales) {
		this.totalSales = totalSales;
	}

	@Override
	public String toString() {
		return "DayEndClosureHdr [id=" + id + ", userId=" + userId + ", branchCode=" + branchCode + ", cashSale="
				+ cashSale + ", cardSale=" + cardSale + ", cardSale2=" + cardSale2 + ", physicalCash=" + physicalCash
				+ ", processDate=" + processDate + ", dayEndStatus=" + dayEndStatus + ", totalSales=" + totalSales
				+ ", changeInCash=" + changeInCash + ", voucheNumber=" + voucheNumber + ", voucherDate=" + voucherDate
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst
				+ ", cardSettlementAmount=" + cardSettlementAmount + "]";
	}

	

}
