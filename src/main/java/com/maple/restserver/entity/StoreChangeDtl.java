package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class StoreChangeDtl implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	String batch;
	Double qty;
	Double rate;
	@Column(length = 50)
	String itemCode;
	Double taxRate;
	Double amount;
	@Column(length = 50)
	String unitId;
	@Column(length = 50)
	String itemName;
	@Column(length = 50)
	String itemId;
	@Column(length = 50)
	String barcode; 
	Double mrp;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "storeChangeMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private StoreChangeMst storeChangeMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getBatch() {
		return batch;
	}


	public void setBatch(String batch) {
		this.batch = batch;
	}


	public Double getQty() {
		return qty;
	}


	public void setQty(Double qty) {
		this.qty = qty;
	}


	public Double getRate() {
		return rate;
	}


	public void setRate(Double rate) {
		this.rate = rate;
	}


	public String getItemCode() {
		return itemCode;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public Double getTaxRate() {
		return taxRate;
	}


	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}


	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}


	public String getUnitId() {
		return unitId;
	}


	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getItemId() {
		return itemId;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	public String getBarcode() {
		return barcode;
	}


	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}


	public Double getMrp() {
		return mrp;
	}


	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}


	public StoreChangeMst getStoreChangeMst() {
		return storeChangeMst;
	}


	public void setStoreChangeMst(StoreChangeMst storeChangeMst) {
		this.storeChangeMst = storeChangeMst;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}





	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "StoreChangeDtl [id=" + id + ", batch=" + batch + ", qty=" + qty + ", rate=" + rate + ", itemCode="
				+ itemCode + ", taxRate=" + taxRate + ", amount=" + amount + ", unitId=" + unitId + ", itemName="
				+ itemName + ", itemId=" + itemId + ", barcode=" + barcode + ", mrp=" + mrp + ", companyMst="
				+ companyMst + ", storeChangeMst=" + storeChangeMst + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}


	
	

}
