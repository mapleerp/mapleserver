package com.maple.restserver.entity;

import java.util.Date;

import javax.persistence.Column;

public class BatchWiseSalesReturnReport {
	@Column(length = 50)
	String billTo;
	@Column(length = 50)
	String itemcode;
	@Column(length = 50)
	String itemname;
	@Column(length = 50)
	String batch;
	Double qty;
	@Column(length = 50)
	String unitprice;
	@Column(length = 50)
	String returnvouchernumber;
	Date invoiceDate;
	Double amount;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getBillTo() {
		return billTo;
	}
	public void setBillTo(String billTo) {
		this.billTo = billTo;
	}
	public String getItemcode() {
		return itemcode;
	}
	public void setItemcode(String itemcode) {
		this.itemcode = itemcode;
	}
	public String getItemname() {
		return itemname;
	}
	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getUnitprice() {
		return unitprice;
	}
	public void setUnitprice(String unitprice) {
		this.unitprice = unitprice;
	}
	public String getReturnvouchernumber() {
		return returnvouchernumber;
	}
	public void setReturnvouchernumber(String returnvouchernumber) {
		this.returnvouchernumber = returnvouchernumber;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "BatchWiseSalesReturnReport [billTo=" + billTo + ", itemcode=" + itemcode + ", itemname=" + itemname
				+ ", batch=" + batch + ", qty=" + qty + ", unitprice=" + unitprice + ", returnvouchernumber="
				+ returnvouchernumber + ", invoiceDate=" + invoiceDate + ", amount=" + amount + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
