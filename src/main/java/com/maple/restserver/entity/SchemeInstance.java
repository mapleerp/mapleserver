package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.stereotype.Component;
@Component
@Entity
public class SchemeInstance implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
	private String id;
	
	@Column(unique=true, nullable = false)
	private String schemeName;
	@Column(length = 50)
	private String selectionId;
	@Column(length = 50)
	private String eligibilityId;
	@Column(length = 50)
	private String  offerId;
	@Column(length = 50)
	private String isActive;
	@Column(length = 50)
	private String schemeWholesaleRetail;
	@Column(length = 50)
	private String branchCode;
	@Column(length = 50)
	String oldId;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;


	
	public String getOldId() {
		return oldId;
	}
	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;

	
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSchemeName() {
		return schemeName;
	}
	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}
	public String getSelectionId() {
		return selectionId;
	}
	public void setSelectionId(String selectionId) {
		this.selectionId = selectionId;
	}
	public String getEligibilityId() {
		return eligibilityId;
	}
	public void setEligibilityId(String eligibilityId) {
		this.eligibilityId = eligibilityId;
	}
	public String getOfferId() {
		return offerId;
	}
	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getSchemeWholesaleRetail() {
		return schemeWholesaleRetail;
	}
	public void setSchemeWholesaleRetail(String schemeWholesaleRetail) {
		this.schemeWholesaleRetail = schemeWholesaleRetail;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SchemeInstance [id=" + id + ", schemeName=" + schemeName + ", selectionId=" + selectionId
				+ ", eligibilityId=" + eligibilityId + ", offerId=" + offerId + ", isActive=" + isActive
				+ ", schemeWholesaleRetail=" + schemeWholesaleRetail + ", branchCode=" + branchCode + ", oldId=" + oldId
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst
				+ "]";
	}
	


}
