package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class SalesPropertyTransHdr implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	@Column(length = 50)
	String propertyId;
	String value;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "sales_trans_hdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private SalesTransHdr salesTransHdr;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	String propertyName;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}

	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SalesPropertyTransHdr [id=" + id + ", propertyId=" + propertyId + ", value=" + value
				+ ", salesTransHdr=" + salesTransHdr + ", companyMst=" + companyMst + ", propertyName=" + propertyName
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
	
	
}
