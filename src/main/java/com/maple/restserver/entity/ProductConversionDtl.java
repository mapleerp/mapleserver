package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ProductConversionDtl implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	   private String id;
	@Column(length = 50)
	private String fromItem;
	private Double fromQty;
	@Column(length = 50)
	private String toItem;
	private Double toQty;
	@Column(length = 50)
	private String batchCode;
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "product_conversion_mst_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	ProductConversionMst productConversionMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return id;
	}
	
	public String getFromItem() {
		return fromItem;
	}
	public void setFromItem(String fromItem) {
		this.fromItem = fromItem;
	}
	public Double getFromQty() {
		return fromQty;
	}
	public void setFromQty(Double fromQty) {
		this.fromQty = fromQty;
	}
	public String getToItem() {
		return toItem;
	}
	public void setToItem(String toItem) {
		this.toItem = toItem;
	}
	public Double getToQty() {
		return toQty;
	}
	public void setToQty(Double toQty) {
		this.toQty = toQty;
	}

	public ProductConversionMst getProductConversionMst() {
		return productConversionMst;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setProductConversionMst(ProductConversionMst productConversionMst) {
		this.productConversionMst = productConversionMst;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ProductConversionDtl [id=" + id + ", fromItem=" + fromItem + ", fromQty=" + fromQty + ", toItem="
				+ toItem + ", toQty=" + toQty + ", batchCode=" + batchCode + ", productConversionMst="
				+ productConversionMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
}
