package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class DailySalesDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	Double cashSales;
	Double creditSales;
	Double creditCardSales;
	Double SodexoSales;
	Double paytmSales;
	@Column(length = 50)
	String branchCode;
	Date reportDate;
	

	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public Double getCashSales() {
		return cashSales;
	}



	public void setCashSales(Double cashSales) {
		this.cashSales = cashSales;
	}



	public Double getCreditSales() {
		return creditSales;
	}



	public void setCreditSales(Double creditSales) {
		this.creditSales = creditSales;
	}



	public Double getCreditCardSales() {
		return creditCardSales;
	}



	public void setCreditCardSales(Double creditCardSales) {
		this.creditCardSales = creditCardSales;
	}



	public Double getSodexoSales() {
		return SodexoSales;
	}



	public void setSodexoSales(Double sodexoSales) {
		SodexoSales = sodexoSales;
	}



	public Double getPaytmSales() {
		return paytmSales;
	}



	public void setPaytmSales(Double paytmSales) {
		this.paytmSales = paytmSales;
	}



	public String getBranchCode() {
		return branchCode;
	}



	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}



	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public Date getReportDate() {
		return reportDate;
	}



	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	@Override
	public String toString() {
		return "DailySalesDtl [id=" + id + ", cashSales=" + cashSales + ", creditSales=" + creditSales
				+ ", creditCardSales=" + creditCardSales + ", SodexoSales=" + SodexoSales + ", paytmSales=" + paytmSales
				+ ", branchCode=" + branchCode + ", reportDate=" + reportDate + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}








}
