package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class LoyalityVouchers  implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	String voucherNumber;
	String voucherLabel;
	Date expiryDate;
	Double percentage;
	Double amount;
	@Column(length = 50)
	String cardType;
	Double minAmountForRedemption;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	@Column(length = 50)
	String branchCode;
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherLabel() {
		return voucherLabel;
	}
	public void setVoucherLabel(String voucherLabel) {
		this.voucherLabel = voucherLabel;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getPercentage() {
		return percentage;
	}
	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public Double getMinAmountForRedemption() {
		return minAmountForRedemption;
	}
	public void setMinAmountForRedemption(Double minAmountForRedemption) {
		this.minAmountForRedemption = minAmountForRedemption;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "LoyalityVouchers [id=" + id + ", voucherNumber=" + voucherNumber + ", voucherLabel=" + voucherLabel
				+ ", expiryDate=" + expiryDate + ", percentage=" + percentage + ", amount=" + amount + ", cardType="
				+ cardType + ", minAmountForRedemption=" + minAmountForRedemption + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", branchCode=" + branchCode
				+ "]";
	}
	
}
