package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.message.entity.SalesDtlMessage;
import com.maple.restserver.message.entity.SalesTransHdrMessage;
@Component
public class ProcessCompleteMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	 String ProcessType;
	 String ProcessStatus;
	public String getProcessType() {
		return ProcessType;
	}
	public void setProcessType(String processType) {
		ProcessType = processType;
	}
	public String getProcessStatus() {
		return ProcessStatus;
	}
	public void setProcessStatus(String processStatus) {
		ProcessStatus = processStatus;
	}
	 
	
}
