package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;

public class KitchenOrder implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	String itemName;
	Double qty;
	Double mrp;
	@Column(length = 50)
	String ServingTableName;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getServingTableName() {
		return ServingTableName;
	}
	public void setServingTableName(String servingTableName) {
		ServingTableName = servingTableName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "KitchenOrder [itemName=" + itemName + ", qty=" + qty + ", mrp=" + mrp + ", ServingTableName="
				+ ServingTableName + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
