package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;

import org.springframework.stereotype.Component;

@Component
public class PurchaseHdrMessageEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	PurchaseHdr purchaseHdr;
	ArrayList<PurchaseDtl> purchaseDtlList =new ArrayList();
	ArrayList<AccountHeads> accountHeadsList =new ArrayList();
	ArrayList<CategoryMst> categoryList = new ArrayList();
	ArrayList<ItemMst> itemList = new ArrayList();
	ArrayList<AccountPayable> accountPayablelist = new ArrayList();
	ArrayList<UnitMst> unitMstList = new ArrayList();
	BranchMst branchMst;
	Integer itemCount;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public ArrayList<UnitMst> getUnitMstList() {
		return unitMstList;
	}
	public void setUnitMstList(ArrayList<UnitMst> unitMstList) {
		this.unitMstList = unitMstList;
	}
	public PurchaseHdr getPurchaseHdr() {
		return purchaseHdr;
	}
	public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
		this.purchaseHdr = purchaseHdr;
	}
	public ArrayList<PurchaseDtl> getPurchaseDtlList() {
		return purchaseDtlList;
	}
	public void setPurchaseDtlList(ArrayList<PurchaseDtl> purchaseDtlList) {
		this.purchaseDtlList = purchaseDtlList;
	}
	
	public ArrayList<AccountHeads> getAccountHeadsList() {
		return accountHeadsList;
	}
	public void setAccountHeadsList(ArrayList<AccountHeads> accountHeadsList) {
		this.accountHeadsList = accountHeadsList;
	}
	public ArrayList<CategoryMst> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(ArrayList<CategoryMst> categoryList) {
		this.categoryList = categoryList;
	}
	public ArrayList<ItemMst> getItemList() {
		return itemList;
	}
	public void setItemList(ArrayList<ItemMst> itemList) {
		this.itemList = itemList;
	}

	public ArrayList<AccountPayable> getAccountPayablelist() {
		return accountPayablelist;
	}
	public void setAccountPayablelist(ArrayList<AccountPayable> accountPayablelist) {
		this.accountPayablelist = accountPayablelist;
	}
	public BranchMst getBranchMst() {
		return branchMst;
	}
	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}
	public Integer getItemCount() {
		return itemCount;
	}
	public void setItemCount(Integer itemCount) {
		this.itemCount = itemCount;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PurchaseHdrMessageEntity [purchaseHdr=" + purchaseHdr + ", purchaseDtlList=" + purchaseDtlList
				+ ", accountHeadsList=" + accountHeadsList + ", categoryList=" + categoryList + ", itemList=" + itemList
				+ ", accountPayablelist=" + accountPayablelist + ", unitMstList=" + unitMstList + ", branchMst="
				+ branchMst + ", itemCount=" + itemCount + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
	

	
}
