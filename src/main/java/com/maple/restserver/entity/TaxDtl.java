package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
 
@Entity
public class TaxDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
    String  taxId;
	@Column(length = 50)
	 String  itemId;
	
     java.util.Date startingDate;
     java.util.Date  endingDate;
     
     
     @ManyToOne(fetch = FetchType.EAGER, optional = false)
 	@JoinColumn(name = "companyMst", nullable = false)
 	@OnDelete(action = OnDeleteAction.CASCADE)
 	CompanyMst companyMst;
     @Column(length = 50)
     private String  processInstanceId;
     @Column(length = 50)
 	private String taskId;
     
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public  java.util.Date  getStartingDate() {
		return startingDate;
	}
	public void setStartingDate( java.util.Date  startingDate) {
		this.startingDate = startingDate;
	}
	public  java.util.Date  getEndingDate() {
		return endingDate;
	}
	public void setEndingDate( java.util.Date  endingDate) {
		this.endingDate = endingDate;
	}
	
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "TaxDtl [taxId=" + taxId + ", itemId=" + itemId + ", startingDate=" + startingDate + ", endingDate="
				+ endingDate + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	

	
	
	
	
}
