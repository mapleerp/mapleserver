package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

 
@Entity
public class PaymentDtl implements Serializable{
	private static final long serialVersionUID =1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
 
	
	@Column(length = 50)
	String accountId;
	@Column(length = 50)
	private String modeOfPayment;
	private String remark;
	private Double amount;
	@Column(length = 50)
	private String instrumentNumber;
	@Column(length = 50)
	private String bankAccountNumber;
	private  Date instrumentDate;
	private Date transDate;
	@Column(length = 50)
	private String creditAccountId;
	@Column(length = 50)
	private String memberId;
	@Column(length = 50)
	private String oldId;

	


	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "paymenthdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private PaymentHdr paymenthdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public PaymentDtl() {

	}

	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

 

	public String getMemberId() {
		return memberId;
	}



	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}



	public String getModeOfPayment() {
		return modeOfPayment;
	}

	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getInstrumentNumber() {
		return instrumentNumber;
	}

	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public  Date getInstrumentDate() {
		return instrumentDate;
	}

	public void setInstrumentDate( Date instrumentDate) {
		this.instrumentDate = instrumentDate;
	}

 
	
	public PaymentHdr getPaymenthdr() {
		return paymenthdr;
	}

	public void setPaymenthdr(PaymentHdr paymenthdr) {
		this.paymenthdr = paymenthdr;
	}



	public String getAccountId() {
		return accountId;
	}



	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}



	
	



	public Date getTransDate() {
		return transDate;
	}


	
	
	
	

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}



	public String getCreditAccountId() {
		return creditAccountId;
	}



	public void setCreditAccountId(String creditAccountId) {
		this.creditAccountId = creditAccountId;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	public String getOldId() {
		return oldId;
	}



	public void setOldId(String oldId) {
		this.oldId = oldId;
	}




 

	
}
