

package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
public class  BankMst  extends MapleObject  implements Serializable{
private static final long serialVersionUID =1L;

 
/*@Id
@GeneratedValue(generator = "uuid")
@GenericGenerator(name = "uuid", strategy = "uuid2")
private String id;
	*/
@Column(length = 50)
	String bankName;
@Column(length = 50)
	String bankPlace;
@Column(length = 50)
	String bankAccount;
@Column(length = 50)
	String ifcCode;
@Column(length = 50)
	String printInInvoice;
@Column(length = 50)
	 
	String UserName;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
 
	 
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankPlace() {
		return bankPlace;
	}
	public void setBankPlace(String bankPlace) {
		this.bankPlace = bankPlace;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public String getIfcCode() {
		return ifcCode;
	}
	public void setIfcCode(String ifcCode) {
		this.ifcCode = ifcCode;
	}
	public String getPrintInInvoice() {
		return printInInvoice;
	}
	public void setPrintInInvoice(String printInInvoice) {
		this.printInInvoice = printInInvoice;
	}
	

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
 
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "BankMst [id=" + getId() + ", bankName=" + bankName + ", bankPlace=" + bankPlace + ", bankAccount="
				+ bankAccount + ", ifcCode=" + ifcCode + ", printInInvoice=" + printInInvoice + ", UserName=" + UserName
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	 
	
	 

}
