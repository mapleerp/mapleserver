package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class InsuranceCompanyMst implements Serializable{
			
			private static final long serialVersionUID = 1L;
			@Column(length = 50)
			@Id
			String id;
			
			@Column(length = 50)
			String insuranceCompanyName;
			
			
			@Column(length = 50)
			String contactPerson;
			@Column(length = 50)
			String phoneNumber;
			
			@ManyToOne(fetch = FetchType.EAGER, optional = false)
			@JoinColumn(name = "companyMst", nullable = false)
			@OnDelete(action = OnDeleteAction.CASCADE)
			private CompanyMst companyMst;
			@Column(length = 50)
			private String  processInstanceId;
			@Column(length = 50)
			private String taskId;
			
			public CompanyMst getCompanyMst() {
				return companyMst;
			}
			public void setCompanyMst(CompanyMst companyMst) {
				this.companyMst = companyMst;
			}
			public String getId() {
				return id;
			}
			public void setId(String id) {
				this.id = id;
			}
			public String getInsuranceCompanyName() {
				return insuranceCompanyName;
			}
			public void setInsuranceCompanyName(String insuranceCompanyName) {
				this.insuranceCompanyName = insuranceCompanyName;
			}
			
			public String getContactPerson() {
				return contactPerson;
			}
			public void setContactPerson(String contactPerson) {
				this.contactPerson = contactPerson;
			}
			public String getPhoneNumber() {
				return phoneNumber;
			}
			public void setPhoneNumber(String phoneNumber) {
				this.phoneNumber = phoneNumber;
			}
		
			public String getProcessInstanceId() {
				return processInstanceId;
			}
			public void setProcessInstanceId(String processInstanceId) {
				this.processInstanceId = processInstanceId;
			}
			public String getTaskId() {
				return taskId;
			}
			public void setTaskId(String taskId) {
				this.taskId = taskId;
			}
			@Override
			public String toString() {
				return "InsuranceCompanyMst [id=" + id + ", insuranceCompanyName=" + insuranceCompanyName
						+ ", contactPerson=" + contactPerson + ", phoneNumber=" + phoneNumber + ", companyMst="
						+ companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
			}
			
}
