package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class AdditionalExpense implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
	
	
   private String id;
	
	@JsonProperty("userId")
	Integer userId;
	@Column(length = 50)
	@JsonProperty("deleted")
	String deleted;
	
	@Column(length = 50)
	@JsonProperty("expenseHead")
	String expenseHead;
	@Column(length = 50)
	@JsonProperty("accountHead")
	String accountHead;
	
	@JsonProperty("amount")
	double amount;
	@Column(length = 50)
	@JsonProperty("currency")
	String currency;
	

	private Double fcAmount;
	@Column(length = 50)
	private String currencyId;
	@Column(length = 50)
	private String accountId;
	@JsonProperty("conversionRate")
	Double conversionRate;

	

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "purchaseHdr", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	PurchaseHdr purchaseHdr;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;


	public Integer getUserId() {
		return userId;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}


	public Double getFcAmount() {
		return fcAmount;
	}


	public void setFcAmount(Double fcAmount) {
		this.fcAmount = fcAmount;
	}


	public String getCurrencyId() {
		return currencyId;
	}


	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}


	public String getAccountId() {
		return accountId;
	}


	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}


	public PurchaseHdr getPurchaseHdr() {
		return purchaseHdr;
	}


	public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
		this.purchaseHdr = purchaseHdr;
	}


	public String getDeleted() {
		return deleted;
	}


	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}


	public String getExpenseHead() {
		return expenseHead;
	}


	public void setExpenseHead(String expenseHead) {
		this.expenseHead = expenseHead;
	}


	public String getAccountHead() {
		return accountHead;
	}


	public void setAccountHead(String accountHead) {
		this.accountHead = accountHead;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public double getConversionRate() {
		return conversionRate;
	}


	public void setConversionRate(double conversionRate) {
		this.conversionRate = conversionRate;
	}



	


	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "AdditionalExpense [id=" + id + ", userId=" + userId + ", deleted=" + deleted + ", expenseHead="
				+ expenseHead + ", accountHead=" + accountHead + ", amount=" + amount + ", currency=" + currency
				+ ", fcAmount=" + fcAmount + ", currencyId=" + currencyId + ", accountId=" + accountId
				+ ", conversionRate=" + conversionRate + ", purchaseHdr=" + purchaseHdr + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
}
