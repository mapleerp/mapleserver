package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;



@Entity
public class StockTransferIn implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	Integer id;
	@Column(length = 50)
	String userId;
	@Column(length = 50)
	String BranchId;
	@Column(length = 50)
	String machineId;
	@Column(length = 50)
	String supplierVoucherId;
	Date voucherDate;
	@Column(length = 50)
	String customerBranchId;
	@Column(length = 50)
	String statutoryVoucher;
	@Column(length = 50)
	String voucherTime;
	@Column(length = 50)
	String intentNumber;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranchId() {
		return BranchId;
	}
	public void setBranchId(String branchId) {
		BranchId = branchId;
	}
	public String getMachineId() {
		return machineId;
	}
	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}
	public String getSupplierVoucherId() {
		return supplierVoucherId;
	}
	public void setSupplierVoucherId(String supplierVoucherId) {
		this.supplierVoucherId = supplierVoucherId;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getCustomerBranchId() {
		return customerBranchId;
	}
	public void setCustomerBranchId(String customerBranchId) {
		this.customerBranchId = customerBranchId;
	}
	public String getStatutoryVoucher() {
		return statutoryVoucher;
	}
	public void setStatutoryVoucher(String statutoryVoucher) {
		this.statutoryVoucher = statutoryVoucher;
	}
	public String getVoucherTime() {
		return voucherTime;
	}
	public void setVoucherTime(String voucherTime) {
		this.voucherTime = voucherTime;
	}
	public String getIntentNumber() {
		return intentNumber;
	}
	public void setIntentNumber(String intentNumber) {
		this.intentNumber = intentNumber;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "StockTransferIn [id=" + id + ", userId=" + userId + ", BranchId=" + BranchId + ", machineId="
				+ machineId + ", supplierVoucherId=" + supplierVoucherId + ", voucherDate=" + voucherDate
				+ ", customerBranchId=" + customerBranchId + ", statutoryVoucher=" + statutoryVoucher + ", voucherTime="
				+ voucherTime + ", intentNumber=" + intentNumber + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
	

}
