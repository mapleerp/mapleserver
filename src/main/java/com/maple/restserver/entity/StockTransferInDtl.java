package com.maple.restserver.entity;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class StockTransferInDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	 
	@Id
	 @GeneratedValue(generator = "uuid")

   @GenericGenerator(name = "uuid", strategy = "uuid2")
  
	
    private String id;
	@Column(length = 50)
	private String batch;
	private Double qty;
	private Double rate;
	@Column(length = 50)
	private String itemCode;
	private Double taxRate;
	private Date expiryDate;
	private Double amount;
	@Column(length = 50)
	private String unitId;
	
	private Double mrp;
	@Column(length = 50)
	private String barcode;
	@Column(length = 50)
	String processStatus;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "stockTransferInHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	StockTransferInHdr stockTransferInHdr;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "itemId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	ItemMst itemId;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getId() {
		return id;
	}


	public String getProcessStatus() {
		return processStatus;
	}


	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getBatch() {
		return batch;
	}


	public void setBatch(String batch) {
		this.batch = batch;
	}


	public Double getQty() {
		return qty;
	}


	public void setQty(Double qty) {
		this.qty = qty;
	}


	public Double getRate() {
		return rate;
	}


	public void setRate(Double rate) {
		this.rate = rate;
	}


	public String getItemCode() {
		return itemCode;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public Double getTaxRate() {
		return taxRate;
	}


	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}


	public Date getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}


	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}


	public String getUnitId() {
		return unitId;
	}


	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}


	

	public Double getMrp() {
		return mrp;
	}


	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}


	public String getBarcode() {
		return barcode;
	}


	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}


	public StockTransferInHdr getStockTransferInHdr() {
		return stockTransferInHdr;
	}


	public void setStockTransferInHdr(StockTransferInHdr stockTransferInHdr) {
		this.stockTransferInHdr = stockTransferInHdr;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "StockTransferInDtl [id=" + id + ", batch=" + batch + ", qty=" + qty + ", rate=" + rate + ", itemCode="
				+ itemCode + ", taxRate=" + taxRate + ", expiryDate=" + expiryDate + ", amount=" + amount + ", unitId="
				+ unitId + ", mrp=" + mrp + ", barcode=" + barcode + ", processStatus=" + processStatus
				+ ", stockTransferInHdr=" + stockTransferInHdr + ", itemId=" + itemId + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


	public ItemMst getItemId() {
		return itemId;
	}


	public void setItemId(ItemMst itemId) {
		this.itemId = itemId;
	}




	
	
	 
	
}
