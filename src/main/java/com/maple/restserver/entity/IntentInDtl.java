package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class IntentInDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	
	private String id;
	@Column(length = 50)
	private String itemId;
	@Column(length = 50)
	private String UnitId;
	
	private Double Qty;
	
	private Double rate;
	private String status;
	private Double allocatedQty;
	private Double balanceQty;
	private String itemPropertyAsJson;
	
	
	
	public String getItemPropertyAsJson() {
		return itemPropertyAsJson;
	}
	public void setItemPropertyAsJson(String itemPropertyAsJson) {
		this.itemPropertyAsJson = itemPropertyAsJson;
	}
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "intent_In_Hdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private IntentInHdr intentInHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUnitId() {
		return UnitId;
	}
	public void setUnitId(String unitId) {
		UnitId = unitId;
	}
	public Double getQty() {
		return Qty;
	}
	public void setQty(Double qty) {
		Qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public IntentInHdr getIntentInHdr() {
		return intentInHdr;
	}
	public void setIntentInHdr(IntentInHdr intentInHdr) {
		this.intentInHdr = intentInHdr;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Double getAllocatedQty() {
		return allocatedQty;
	}
	public void setAllocatedQty(Double allocatedQty) {
		this.allocatedQty = allocatedQty;
	}
	
	
	public Double getBalanceQty() {
		return balanceQty;
	}
	public void setBalanceQty(Double balanceQty) {
		this.balanceQty = balanceQty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "IntentInDtl [id=" + id + ", itemId=" + itemId + ", UnitId=" + UnitId + ", Qty=" + Qty + ", rate=" + rate
				+ ", status=" + status + ", allocatedQty=" + allocatedQty + ", balanceQty=" + balanceQty
				+ ", itemPropertyAsJson=" + itemPropertyAsJson + ", intentInHdr=" + intentInHdr + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

	
	
}
