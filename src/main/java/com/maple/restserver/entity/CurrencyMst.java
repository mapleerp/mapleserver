package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class CurrencyMst implements	Serializable{
		private static final long serialVersionUID = 1L;
		@Column(length = 50)
		@Id
		private String id;
		@Column(length = 50)
		String currencyName;
		@Column(length = 50)
		String branchCode;
		
		@ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "companyMst", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		CompanyMst companyMst;
		@Column(length = 50)
		private String  processInstanceId;
		@Column(length = 50)
		private String taskId;
		
		
		public String getBranchCode() {
			return branchCode;
		}
		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}
		public CompanyMst getCompanyMst() {
			return companyMst;
		}
		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getCurrencyName() {
			return currencyName;
		}
		public void setCurrencyName(String currencyName) {
			this.currencyName = currencyName;
		}
		
		public String getProcessInstanceId() {
			return processInstanceId;
		}
		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}
		public String getTaskId() {
			return taskId;
		}
		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}
		@Override
		public String toString() {
			return "CurrencyMst [id=" + id + ", currencyName=" + currencyName + ", branchCode=" + branchCode
					+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
					+ "]";
		}
		
		
}
