package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class MobileCustomerMst implements Serializable{
	@Column(length = 50)
	@Id
	@GeneratedValue
	String Id;
	@Column(length = 50)
	private String customerName;
	private String customerAddress;
	@Column(length = 50)
	private String customerContact;
	@Column(length = 50)
	private String customerMail;
	@Column(length = 50)
	private String customerGst;
	@Column(length = 50)
	private String customerState;
	@Column(length = 50)
	private String customerPlace;
	@Column(length = 50)
	private String priceTypeId;
	@Column(length = 50)
	private String customerCountry;
	@Column(length = 50)
	private String customerPin;
	@Column(nullable = true)
	private String bankName;
	@Column(nullable = true)
	private String  bankAccountName;
	@Column(nullable = true)
	private String bankIfsc;
	private String customerGstType;
	private String discountProperty;
	@Column(nullable = true, columnDefinition = "double default 0.0") 

	private Double customerDiscount;

	@Column( nullable = false, columnDefinition = "Integer default 0") 
	private Integer customerRank;

	private String customerGroup;

	
	@Column(nullable = true)
	private Integer creditPeriod;

	private String customerType;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCustomerContact() {
		return customerContact;
	}
	public void setCustomerContact(String customerContact) {
		this.customerContact = customerContact;
	}
	public String getCustomerMail() {
		return customerMail;
	}
	public void setCustomerMail(String customerMail) {
		this.customerMail = customerMail;
	}
	public String getCustomerGst() {
		return customerGst;
	}
	public void setCustomerGst(String customerGst) {
		this.customerGst = customerGst;
	}
	public String getCustomerState() {
		return customerState;
	}
	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}
	public String getCustomerPlace() {
		return customerPlace;
	}
	public void setCustomerPlace(String customerPlace) {
		this.customerPlace = customerPlace;
	}
	public String getPriceTypeId() {
		return priceTypeId;
	}
	public void setPriceTypeId(String priceTypeId) {
		this.priceTypeId = priceTypeId;
	}
	public String getCustomerCountry() {
		return customerCountry;
	}
	public void setCustomerCountry(String customerCountry) {
		this.customerCountry = customerCountry;
	}
	public String getCustomerPin() {
		return customerPin;
	}
	public void setCustomerPin(String customerPin) {
		this.customerPin = customerPin;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccountName() {
		return bankAccountName;
	}
	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}
	public String getBankIfsc() {
		return bankIfsc;
	}
	public void setBankIfsc(String bankIfsc) {
		this.bankIfsc = bankIfsc;
	}
	public String getCustomerGstType() {
		return customerGstType;
	}
	public void setCustomerGstType(String customerGstType) {
		this.customerGstType = customerGstType;
	}
	public String getDiscountProperty() {
		return discountProperty;
	}
	public void setDiscountProperty(String discountProperty) {
		this.discountProperty = discountProperty;
	}
	public Double getCustomerDiscount() {
		return customerDiscount;
	}
	public void setCustomerDiscount(Double customerDiscount) {
		this.customerDiscount = customerDiscount;
	}
	public Integer getRank() {
		return customerRank;
	}
	public void setRank(Integer rank) {
		this.customerRank = rank;
	}
	public String getCustomerGroup() {
		return customerGroup;
	}
	public void setCustomerGroup(String customerGroup) {
		this.customerGroup = customerGroup;
	}
	public Integer getCreditPeriod() {
		return creditPeriod;
	}
	public void setCreditPeriod(Integer creditPeriod) {
		this.creditPeriod = creditPeriod;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "MobileCustomerMst [Id=" + Id + ", customerName=" + customerName + ", customerAddress=" + customerAddress
				+ ", customerContact=" + customerContact + ", customerMail=" + customerMail + ", customerGst="
				+ customerGst + ", customerState=" + customerState + ", customerPlace=" + customerPlace
				+ ", priceTypeId=" + priceTypeId + ", customerCountry=" + customerCountry + ", customerPin="
				+ customerPin + ", bankName=" + bankName + ", bankAccountName=" + bankAccountName + ", bankIfsc="
				+ bankIfsc + ", customerGstType=" + customerGstType + ", discountProperty=" + discountProperty
				+ ", customerDiscount=" + customerDiscount + ", customerRank=" + customerRank + ", customerGroup="
				+ customerGroup + ", creditPeriod=" + creditPeriod + ", customerType=" + customerType + ", companyMst="
				+ companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
