package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
public class InvoiceFormatMst implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/*
	 insert into invoice_format_mst ( price_type_id, jaster_name, gst, discount, batch, id, company_mst) 
	   values (null,'HWTaxInvoice','NO','NO','001','STM');
	   
	   insert into invoice_format_mst ( price_type_id, jasper_name, gst, discount, batch, id, company_mst)   values (null,'HWTaxInvoice','NO','YES','NO','001','STM')
	 */
	@Column(length = 50)
	@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	
	@Column(length = 50)
	String priceTypeId;
	@Column(length = 50)
	String jasperName;
	@Column(length = 50)
	String gst;
	@Column(length = 50)
	String discount;
	@Column(length = 50)
	String batch;
	@Column(length = 50)
	String reportName;
	@Column(length = 50)
	
	String status;
	@Column(length = 50)
	String oldId;



	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPriceTypeId() {
		return priceTypeId;
	}

	public void setPriceTypeId(String priceTypeId) {
		this.priceTypeId = priceTypeId;
	}

	public String getJasperName() {
		return jasperName;
	}

	public void setJasperName(String jasperName) {
		this.jasperName = jasperName;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}


	
	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	


	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "InvoiceFormatMst [id=" + id + ", priceTypeId=" + priceTypeId + ", jasperName=" + jasperName + ", gst="
				+ gst + ", discount=" + discount + ", batch=" + batch + ", reportName=" + reportName + ", status="
				+ status + ", oldId=" + oldId + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

	

}
