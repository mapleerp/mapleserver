package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.stereotype.Component;

@Component
@Entity
public class ItemNutrition implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
 
	@Column(length = 50)
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	@Column(length = 50)
	 String itemId;
	@Column(length = 50)
	 String nutritionFact;
	 @Column(length = 50)
	String value;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "companyMst", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getItemId() {
		return itemId;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	public String getNutritionFact() {
		return nutritionFact;
	}


	public void setNutritionFact(String nutritionFact) {
		this.nutritionFact = nutritionFact;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "ItemNutrition [id=" + id + ", itemId=" + itemId + ", nutritionFact=" + nutritionFact + ", value="
				+ value + ", companyMst=" + companyMst + "]";
	}
	

}
