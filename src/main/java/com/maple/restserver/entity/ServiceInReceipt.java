package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
@Entity
public class ServiceInReceipt implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	String Id;
	@Column(length = 50)
	String receiptMode;
	@Column(length = 50)
	String branchCode;
	@Column(length = 50)
	String userId;
	Double receiptAmount;
	Date rereceiptDate;
	@Column(length = 50)
	String accountId;
	@Column(length = 50)
	String voucherNumber;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "serviceInHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ServiceInHdr serviceInHdr;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getReceiptMode() {
		return receiptMode;
	}

	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Double getReceiptAmount() {
		return receiptAmount;
	}

	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}

	public Date getRereceiptDate() {
		return rereceiptDate;
	}

	public void setRereceiptDate(Date rereceiptDate) {
		this.rereceiptDate = rereceiptDate;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public ServiceInHdr getServiceInHdr() {
		return serviceInHdr;
	}

	public void setServiceInHdr(ServiceInHdr serviceInHdr) {
		this.serviceInHdr = serviceInHdr;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ServiceInReceipt [Id=" + Id + ", receiptMode=" + receiptMode + ", branchCode=" + branchCode
				+ ", userId=" + userId + ", receiptAmount=" + receiptAmount + ", rereceiptDate=" + rereceiptDate
				+ ", accountId=" + accountId + ", voucherNumber=" + voucherNumber + ", serviceInHdr=" + serviceInHdr
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	
	
	
	
	
	
}
