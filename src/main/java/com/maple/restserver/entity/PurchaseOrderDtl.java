package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class PurchaseOrderDtl implements Serializable{

	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "purchase_order_hdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	PurchaseOrderHdr purchaseOrderHdr;

	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	private String itemName;
//	private String itemId;

	 Double purchseRate;

	private Double amount;
	String status;
	private String batch;
	Double receivedQty;

	private Integer itemSerial;

	private String barcode;

	private Double taxAmt;

	private Double cessAmt;

	private Double previousMRP;

	private Date expiryDate;

	private Integer freeQty;

	private Double qty;

	private Double taxRate;

	private Double discount;

	private String changePriceStatus;

	private Integer unit;

	private Double mrp;

	private Double cessRate;

	private Date manufactureDate;

	private String ItemId;

	private String BinNo;

 
	private String unitId;

	private Double CessRate2;

	private Double NetCost;
	
	private String itemPropertyAsJson;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PurchaseOrderHdr getPurchaseOrderHdr() {
		return purchaseOrderHdr;
	}

	public void setPurchaseOrderHdr(PurchaseOrderHdr purchaseOrderHdr) {
		this.purchaseOrderHdr = purchaseOrderHdr;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getPurchseRate() {
		return purchseRate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getReceivedQty() {
		return receivedQty;
	}

	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}

	public void setPurchseRate(Double purchseRate) {
		this.purchseRate = purchseRate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Integer getItemSerial() {
		return itemSerial;
	}

	public void setItemSerial(Integer itemSerial) {
		this.itemSerial = itemSerial;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Double getTaxAmt() {
		return taxAmt;
	}

	public void setTaxAmt(Double taxAmt) {
		this.taxAmt = taxAmt;
	}

	public Double getCessAmt() {
		return cessAmt;
	}

	public void setCessAmt(Double cessAmt) {
		this.cessAmt = cessAmt;
	}

	public Double getPreviousMRP() {
		return previousMRP;
	}

	public void setPreviousMRP(Double previousMRP) {
		this.previousMRP = previousMRP;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Integer getFreeQty() {
		return freeQty;
	}

	public void setFreeQty(Integer freeQty) {
		this.freeQty = freeQty;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String getChangePriceStatus() {
		return changePriceStatus;
	}

	public void setChangePriceStatus(String changePriceStatus) {
		this.changePriceStatus = changePriceStatus;
	}

	public Integer getUnit() {
		return unit;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public Double getCessRate() {
		return cessRate;
	}

	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}

	public Date getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	public String getItemId() {
		return ItemId;
	}

	public void setItemId(String itemId) {
		ItemId = itemId;
	}

	public String getBinNo() {
		return BinNo;
	}

	public void setBinNo(String binNo) {
		BinNo = binNo;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public Double getCessRate2() {
		return CessRate2;
	}

	public void setCessRate2(Double cessRate2) {
		CessRate2 = cessRate2;
	}

	public Double getNetCost() {
		return NetCost;
	}

	public void setNetCost(Double netCost) {
		NetCost = netCost;
	}

	

	public String getItemPropertyAsJson() {
		return itemPropertyAsJson;
	}

	public void setItemPropertyAsJson(String itemPropertyAsJson) {
		this.itemPropertyAsJson = itemPropertyAsJson;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "PurchaseOrderDtl [id=" + id + ", purchaseOrderHdr=" + purchaseOrderHdr + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", itemName=" + itemName
				+ ", purchseRate=" + purchseRate + ", amount=" + amount + ", status=" + status + ", batch=" + batch
				+ ", receivedQty=" + receivedQty + ", itemSerial=" + itemSerial + ", barcode=" + barcode + ", taxAmt="
				+ taxAmt + ", cessAmt=" + cessAmt + ", previousMRP=" + previousMRP + ", expiryDate=" + expiryDate
				+ ", freeQty=" + freeQty + ", qty=" + qty + ", taxRate=" + taxRate + ", discount=" + discount
				+ ", changePriceStatus=" + changePriceStatus + ", unit=" + unit + ", mrp=" + mrp + ", cessRate="
				+ cessRate + ", manufactureDate=" + manufactureDate + ", ItemId=" + ItemId + ", BinNo=" + BinNo
				+ ", unitId=" + unitId + ", CessRate2=" + CessRate2 + ", NetCost=" + NetCost + ", itemPropertyAsJson="
				+ itemPropertyAsJson + "]";
	}



	
	
	
}
