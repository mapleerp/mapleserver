package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity

public class PaymentInvoiceDtl implements Serializable{
	

	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	String voucherNumber;
	@Column(length = 50)
	String invoiceNumber;
	
	Double paidAmount;
	@JsonProperty("voucherDate")
	private java.util.Date voucherDate;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "accountHeads", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	AccountHeads accountHeads;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "paymentHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	PaymentHdr paymentHdr;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "purchaseHdr", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	PurchaseHdr purchaseHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public java.util.Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(java.util.Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	

	public AccountHeads getAccountHeads() {
		return accountHeads;
	}

	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public PaymentHdr getPaymentHdr() {
		return paymentHdr;
	}

	public void setPaymentHdr(PaymentHdr paymentHdr) {
		this.paymentHdr = paymentHdr;
	}

	public PurchaseHdr getPurchaseHdr() {
		return purchaseHdr;
	}

	public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
		this.purchaseHdr = purchaseHdr;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "PaymentInvoiceDtl [id=" + id + ", voucherNumber=" + voucherNumber + ", invoiceNumber=" + invoiceNumber
				+ ", paidAmount=" + paidAmount + ", voucherDate=" + voucherDate + ", companyMst=" + companyMst
				+ ", accountHeads=" + accountHeads + ", paymentHdr=" + paymentHdr + ", purchaseHdr=" + purchaseHdr
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
	

}
