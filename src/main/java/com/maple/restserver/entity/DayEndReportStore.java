package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;



@Entity
public class DayEndReportStore implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	@Column(length = 50)
private String branchName;
	@Column(length = 50)
	String branchCode;
	
	 @Column(length=20)
	private String posMaxBillNo;
	
	 @Column(length=20)
	 private String posMinBillNo;
	
	 @Column(length=20)
	 private String posBillCount;
	
	 @Column(length=20)
	private String wholeSaleMaxBillNo;
	 
	 @Column(length=20)
	private String wholeSaleMinBillNo;
	 
	 @Column(length=20)
	private String wholeSaleBillCount;
	
	 @Column(length=20)
	private String pettyCash;
	 
	 @Column(length=20)
	private String closingCash;
	 
	 @Column(length=20)
	private String totalCash;
	private String physicalCash;
	 Date date;
	 
	 @Column(length=20)
	private String cashReceipt;
	 
	 @Column(length=20)
	private String creditReceipt;
	
	 @Column(length=20)
	 private String ebsReceipt;
	 
	 @Column(length=20)
	private String gpayReceipt;
	 
	 @Column(length=20)
	private String swiggyReceipt;
	 
	 @Column(length=20)
	private String yesBankReceipt;
	 
	 @Column(length=20)
	private String cardSale;
	private String zomatoReceipt;

	 @Column(length=20)
	private String paytmReceipt;
	 
	 @Column(length=20)
	private String totalSales;
	
	private String cashSaleOrder;
	 @Column(length=20)
	private String cardSaleOrder;
	 
	 @Column(length=20)
	private String creditSaleOrder;
	 
	 @Column(length=20)
	private String ebsSaleOrder;
	 
	 @Column(length=20)
	private String gpaySaleOrder;
	
	 @Column(length=20)
	private String yesBankSaleOrder;
	 
	 @Column(length=20)
	private String paytmSaleOrder;
	
	 @Column(length=20)
	private String cashVarience;
	
	private String previousCashSaleOrder;
	private String cash;
	private String neftSales;
	
	//--------------version 6.7 surya 
	private Double card2; 
	//-------------version 6.7 surya end


	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	


	public String getPreviousCashSaleOrder() {
		return previousCashSaleOrder;
	}


	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public void setPreviousCashSaleOrder(String previousCashSaleOrder) {
		this.previousCashSaleOrder = previousCashSaleOrder;
	}


	public String getCash() {
		return cash;
	}


	public void setCash(String cash) {
		this.cash = cash;
	}


	public String getNeftSales() {
		return neftSales;
	}


	public void setNeftSales(String neftSales) {
		this.neftSales = neftSales;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public String getPosMaxBillNo() {
		return posMaxBillNo;
	}


	public void setPosMaxBillNo(String posMaxBillNo) {
		this.posMaxBillNo = posMaxBillNo;
	}


	public String getPosMinBillNo() {
		return posMinBillNo;
	}


	public void setPosMinBillNo(String posMinBillNo) {
		this.posMinBillNo = posMinBillNo;
	}


	public String getPosBillCount() {
		return posBillCount;
	}


	public void setPosBillCount(String posBillCount) {
		this.posBillCount = posBillCount;
	}


	public String getWholeSaleMaxBillNo() {
		return wholeSaleMaxBillNo;
	}


	public void setWholeSaleMaxBillNo(String wholeSaleMaxBillNo) {
		this.wholeSaleMaxBillNo = wholeSaleMaxBillNo;
	}


	public String getWholeSaleMinBillNo() {
		return wholeSaleMinBillNo;
	}


	public void setWholeSaleMinBillNo(String wholeSaleMinBillNo) {
		this.wholeSaleMinBillNo = wholeSaleMinBillNo;
	}


	public String getWholeSaleBillCount() {
		return wholeSaleBillCount;
	}


	public void setWholeSaleBillCount(String wholeSaleBillCount) {
		this.wholeSaleBillCount = wholeSaleBillCount;
	}


	public String getPettyCash() {
		return pettyCash;
	}


	public void setPettyCash(String pettyCash) {
		this.pettyCash = pettyCash;
	}


	public String getClosingCash() {
		return closingCash;
	}


	public void setClosingCash(String closingCash) {
		this.closingCash = closingCash;
	}


	public String getTotalCash() {
		return totalCash;
	}


	public void setTotalCash(String totalCash) {
		this.totalCash = totalCash;
	}


	public String getPhysicalCash() {
		return physicalCash;
	}


	public void setPhysicalCash(String physicalCash) {
		this.physicalCash = physicalCash;
	}


	


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getCashReceipt() {
		return cashReceipt;
	}


	public void setCashReceipt(String cashReceipt) {
		this.cashReceipt = cashReceipt;
	}


	public String getCreditReceipt() {
		return creditReceipt;
	}


	public void setCreditReceipt(String creditReceipt) {
		this.creditReceipt = creditReceipt;
	}


	public String getEbsReceipt() {
		return ebsReceipt;
	}


	public void setEbsReceipt(String ebsReceipt) {
		this.ebsReceipt = ebsReceipt;
	}


	public String getGpayReceipt() {
		return gpayReceipt;
	}


	public void setGpayReceipt(String gpayReceipt) {
		this.gpayReceipt = gpayReceipt;
	}


	public String getSwiggyReceipt() {
		return swiggyReceipt;
	}


	public void setSwiggyReceipt(String swiggyReceipt) {
		this.swiggyReceipt = swiggyReceipt;
	}


	public String getYesBankReceipt() {
		return yesBankReceipt;
	}


	public void setYesBankReceipt(String yesBankReceipt) {
		this.yesBankReceipt = yesBankReceipt;
	}


	public String getCardSale() {
		return cardSale;
	}


	public void setCardSale(String cardSale) {
		this.cardSale = cardSale;
	}


	public String getZomatoReceipt() {
		return zomatoReceipt;
	}


	public void setZomatoReceipt(String zomatoReceipt) {
		this.zomatoReceipt = zomatoReceipt;
	}


	public String getPaytmReceipt() {
		return paytmReceipt;
	}


	public void setPaytmReceipt(String paytmReceipt) {
		this.paytmReceipt = paytmReceipt;
	}


	public String getTotalSales() {
		return totalSales;
	}


	public void setTotalSales(String totalSales) {
		this.totalSales = totalSales;
	}


	public String getCashSaleOrder() {
		return cashSaleOrder;
	}


	public void setCashSaleOrder(String cashSaleOrder) {
		this.cashSaleOrder = cashSaleOrder;
	}


	public String getCardSaleOrder() {
		return cardSaleOrder;
	}


	public void setCardSaleOrder(String cardSaleOrder) {
		this.cardSaleOrder = cardSaleOrder;
	}


	public String getCreditSaleOrder() {
		return creditSaleOrder;
	}


	public void setCreditSaleOrder(String creditSaleOrder) {
		this.creditSaleOrder = creditSaleOrder;
	}


	public String getEbsSaleOrder() {
		return ebsSaleOrder;
	}


	public void setEbsSaleOrder(String ebsSaleOrder) {
		this.ebsSaleOrder = ebsSaleOrder;
	}


	public String getGpaySaleOrder() {
		return gpaySaleOrder;
	}


	public void setGpaySaleOrder(String gpaySaleOrder) {
		this.gpaySaleOrder = gpaySaleOrder;
	}


	public String getYesBankSaleOrder() {
		return yesBankSaleOrder;
	}


	public void setYesBankSaleOrder(String yesBankSaleOrder) {
		this.yesBankSaleOrder = yesBankSaleOrder;
	}


	public String getPaytmSaleOrder() {
		return paytmSaleOrder;
	}


	public void setPaytmSaleOrder(String paytmSaleOrder) {
		this.paytmSaleOrder = paytmSaleOrder;
	}


	public String getCashVarience() {
		return cashVarience;
	}


	public void setCashVarience(String cashVarience) {
		this.cashVarience = cashVarience;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	//-------------version 6.7 surya 

	public Double getCard2() {
		return card2;
	}


	public void setCard2(Double card2) {
		this.card2 = card2;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "DayEndReportStore [id=" + id + ", branchName=" + branchName + ", branchCode=" + branchCode
				+ ", posMaxBillNo=" + posMaxBillNo + ", posMinBillNo=" + posMinBillNo + ", posBillCount=" + posBillCount
				+ ", wholeSaleMaxBillNo=" + wholeSaleMaxBillNo + ", wholeSaleMinBillNo=" + wholeSaleMinBillNo
				+ ", wholeSaleBillCount=" + wholeSaleBillCount + ", pettyCash=" + pettyCash + ", closingCash="
				+ closingCash + ", totalCash=" + totalCash + ", physicalCash=" + physicalCash + ", date=" + date
				+ ", cashReceipt=" + cashReceipt + ", creditReceipt=" + creditReceipt + ", ebsReceipt=" + ebsReceipt
				+ ", gpayReceipt=" + gpayReceipt + ", swiggyReceipt=" + swiggyReceipt + ", yesBankReceipt="
				+ yesBankReceipt + ", cardSale=" + cardSale + ", zomatoReceipt=" + zomatoReceipt + ", paytmReceipt="
				+ paytmReceipt + ", totalSales=" + totalSales + ", cashSaleOrder=" + cashSaleOrder + ", cardSaleOrder="
				+ cardSaleOrder + ", creditSaleOrder=" + creditSaleOrder + ", ebsSaleOrder=" + ebsSaleOrder
				+ ", gpaySaleOrder=" + gpaySaleOrder + ", yesBankSaleOrder=" + yesBankSaleOrder + ", paytmSaleOrder="
				+ paytmSaleOrder + ", cashVarience=" + cashVarience + ", previousCashSaleOrder=" + previousCashSaleOrder
				+ ", cash=" + cash + ", neftSales=" + neftSales + ", card2=" + card2 + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	//-------------version 6.7 surya end

	
	
	
	
	

}
