package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
public class BankBook  extends MapleObject  implements Serializable {
	
	private static final long serialVersionUID = 1L;

	 
	/*@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	*/
	@Column(length = 50)
	 String bankAccount;
	 Date transDate;
	 Double debitAmount;
	 Double creditAmount;
	 @Column(length = 50)
	 String reconciled;
	 @Column(length = 50)
	 String reconcileRemark;
	 Date reconcileDate;
	 

		@ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "companyMst", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		CompanyMst companyMst;
		@Column(length = 50)
		private String  processInstanceId;
		@Column(length = 50)
		private String taskId;
		
	 
 
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public Date getTransDate() {
		return transDate;
	}
	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}
	public Double getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public String getReconciled() {
		return reconciled;
	}
	public void setReconciled(String reconciled) {
		this.reconciled = reconciled;
	}
	public String getReconcileRemark() {
		return reconcileRemark;
	}
	public void setReconcileRemark(String reconcileRemark) {
		this.reconcileRemark = reconcileRemark;
	}
	public Date getReconcileDate() {
		return reconcileDate;
	}
	public void setReconcileDate(Date reconcileDate) {
		this.reconcileDate = reconcileDate;
	}
	
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "BankBook [id=" + getId() + ", bankAccount=" + bankAccount + ", transDate=" + transDate + ", debitAmount="
				+ debitAmount + ", creditAmount=" + creditAmount + ", reconciled=" + reconciled + ", reconcileRemark="
				+ reconcileRemark + ", reconcileDate=" + reconcileDate + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	 

}
