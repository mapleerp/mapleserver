package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;



@Entity
public class TallyReceiptsHdrLink  implements Serializable{

	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	@Column(length = 50)
	String voucherNumber;
	Date voucherDate;
	@Column(length = 50)
	String receiptsHdr;
	@Column(length = 50)
	String tallyvoucherId ;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getReceiptsHdr() {
		return receiptsHdr;
	}
	public void setReceiptsHdr(String receiptsHdr) {
		this.receiptsHdr = receiptsHdr;
	}
	public String getTallyvoucherId() {
		return tallyvoucherId;
	}
	public void setTallyvoucherId(String tallyvoucherId) {
		this.tallyvoucherId = tallyvoucherId;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "TallyReceiptsHdrLink [id=" + id + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", receiptsHdr=" + receiptsHdr + ", tallyvoucherId=" + tallyvoucherId + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	 
	
	
	
}
