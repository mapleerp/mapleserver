package com.maple.restserver.entity;

import java.io.Serializable;
 
 
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

 

@Entity
public class OtherBranchPurchaseHdr implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	@Column(length = 50)
	private String supplierId;
	private String finalSavedStatus;
	@Column(length = 50)
	private String machineId;
	@Column(length = 50)
	private String branchCode;
	private String deletedStatus;
	@Column(length = 50)
 	private String narration;
	@Column(length = 50)
	private String purchaseType;
	@Column(length = 50)
	private String pONum;
	private Date poDate;
	@Column(length = 50)
	private String supplierInvNo;
	@Column(length = 50)
	private String voucherNumber;
	private Double invoiceTotal;
	private Double fcInvoiceTotal;
	@Column(length = 50)
	private String currency;
	private Date supplierInvDate;
	private Date voucherDate;
	private Date tansactionEntryDate;
 
	private Integer enableBatchStatus;
	@Column(length = 50)
	private String userId;
	@Column(length = 50)
	private String voucherType;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getFinalSavedStatus() {
		return finalSavedStatus;
	}
	public void setFinalSavedStatus(String finalSavedStatus) {
		this.finalSavedStatus = finalSavedStatus;
	}
	public String getMachineId() {
		return machineId;
	}
	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public Double getFcInvoiceTotal() {
		return fcInvoiceTotal;
	}
	public void setFcInvoiceTotal(Double fcInvoiceTotal) {
		this.fcInvoiceTotal = fcInvoiceTotal;
	}
	public String getDeletedStatus() {
		return deletedStatus;
	}
	public void setDeletedStatus(String deletedStatus) {
		this.deletedStatus = deletedStatus;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public String getPurchaseType() {
		return purchaseType;
	}
	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}
	public String getpONum() {
		return pONum;
	}
	public void setpONum(String pONum) {
		this.pONum = pONum;
	}
	public Date getPoDate() {
		return poDate;
	}
	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}
	public String getSupplierInvNo() {
		return supplierInvNo;
	}
	public void setSupplierInvNo(String supplierInvNo) {
		this.supplierInvNo = supplierInvNo;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Double getInvoiceTotal() {
		return invoiceTotal;
	}
	public void setInvoiceTotal(Double invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Date getSupplierInvDate() {
		return supplierInvDate;
	}
	public void setSupplierInvDate(Date supplierInvDate) {
		this.supplierInvDate = supplierInvDate;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public Date getTansactionEntryDate() {
		return tansactionEntryDate;
	}
	public void setTansactionEntryDate(Date tansactionEntryDate) {
		this.tansactionEntryDate = tansactionEntryDate;
	}
	public Integer getEnableBatchStatus() {
		return enableBatchStatus;
	}
	public void setEnableBatchStatus(Integer enableBatchStatus) {
		this.enableBatchStatus = enableBatchStatus;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "OtherBranchPurchaseHdr [id=" + id + ", supplierId=" + supplierId + ", finalSavedStatus="
				+ finalSavedStatus + ", machineId=" + machineId + ", branchCode=" + branchCode + ", deletedStatus="
				+ deletedStatus + ", narration=" + narration + ", purchaseType=" + purchaseType + ", pONum=" + pONum
				+ ", poDate=" + poDate + ", supplierInvNo=" + supplierInvNo + ", voucherNumber=" + voucherNumber
				+ ", invoiceTotal=" + invoiceTotal + ", fcInvoiceTotal=" + fcInvoiceTotal + ", currency=" + currency
				+ ", supplierInvDate=" + supplierInvDate + ", voucherDate=" + voucherDate + ", tansactionEntryDate="
				+ tansactionEntryDate + ", enableBatchStatus=" + enableBatchStatus + ", userId=" + userId
				+ ", voucherType=" + voucherType + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	 


}
