package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

public class SummaryReportSalesOrder implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Date voucherDate;
	@Column(length = 50)
	private String VoucherNumber;
	@Column(length = 50)
	private String itemName;
	private Double mrp;
	private Double qty;
	@Column(length = 50)
	private String orderMessageToPrint;
	@Column(length = 50)
	private String orderAdvice;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return VoucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		VoucherNumber = voucherNumber;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getOrderMessageToPrint() {
		return orderMessageToPrint;
	}
	public void setOrderMessageToPrint(String orderMessageToPrint) {
		this.orderMessageToPrint = orderMessageToPrint;
	}
	public String getOrderAdvice() {
		return orderAdvice;
	}
	public void setOrderAdvice(String orderAdvice) {
		this.orderAdvice = orderAdvice;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SummaryReportSalesOrder [voucherDate=" + voucherDate + ", VoucherNumber=" + VoucherNumber
				+ ", itemName=" + itemName + ", mrp=" + mrp + ", qty=" + qty + ", orderMessageToPrint="
				+ orderMessageToPrint + ", orderAdvice=" + orderAdvice + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
}
