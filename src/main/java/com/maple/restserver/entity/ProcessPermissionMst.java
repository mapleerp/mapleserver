package com.maple.restserver.entity;



import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
 
@Entity

public class ProcessPermissionMst implements Serializable {


	private static final long serialVersionUID = 1L;

	@Column(length = 50)
	@Id
     private String id;
	@Column(length = 50)
	private String oldId;
	public String getOldId() {
		return oldId;
	}
	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
	private String userId;
	private String processId;
	Date endDate;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ProcessPermissionMst [id=" + id + ", oldId=" + oldId + ", userId=" + userId + ", processId=" + processId
				+ ", endDate=" + endDate + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
	
	
	
}
