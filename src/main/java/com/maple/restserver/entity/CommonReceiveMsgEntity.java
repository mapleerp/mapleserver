package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;

import org.springframework.stereotype.Component;

import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.accounting.entity.CreditClass;
import com.maple.restserver.accounting.entity.DebitClass;
import com.maple.restserver.accounting.entity.LedgerClass;
import com.maple.restserver.message.entity.AccountMessage;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
@Component
public class CommonReceiveMsgEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	private String messageKey;

	private SalesMessageEntity salesMessageEntity;

	private ArrayList<PurchaseDtl> purchaseDtl;
	private ArrayList<AccountClass> accountClass;
	private ArrayList<AccountHeads> accountHeads;
	private ArrayList<AccountPayable> accountPayable;
	private ArrayList<AccountReceivable> accountReceivable;
	private ArrayList<ActualProductionDtl> actualProductionDtl;
	private ArrayList<AdditionalExpense> additionalExpense;
	private ArrayList<BatchPriceDefinition>  batchPriceDefinition;
	private ArrayList<ItemDeviceMst>itemDeviceMst;
	private ArrayList<PurchaseSchemeMst>purchaseSchemeMst;
	private ArrayList<ItemImageMst>itemImageMst;
	private ArrayList<PDCPayment>pDCPayment;
	
	private ArrayList<GoodReceiveNoteDtl> goodReceiveNoteDtl;
	private ArrayList<GoodReceiveNoteHdr> goodReceiveNoteHdr;
	
	private ArrayList<BarcodeBatchMst> barcodeBatchMstList;
	
	@Column(length = 50)
	private String cmdFetchItemMst ;  
	
	
	private ArrayList<BranchMst> branchMst;
	
	private ArrayList<CategoryMst> categoryMst;
	private ArrayList<BrandMst> brandMst;
	
	private ArrayList<CompanyMst> companyMst;
	private ArrayList<ConsumptionDtl> consumptionDtl;
	private ArrayList<ConsumptionReasonMst>consumptionReasonMst;
	private ArrayList<CreditClass> creditClass;
	private ArrayList<CurrencyConversionMst> currencyConversionMst;
	
	private ArrayList<DamageDtl> damageDtl;
	private ArrayList<DayBook> dayBook;
	private ArrayList<DayEndClosureDtl> dayEndClosureDtl;
	private ArrayList<DayEndReportStore> dayEndReportStore;
	private ArrayList<DebitClass> debitClass;
	private ArrayList<DeliveryBoyMst> deliveryBoyMst;
	private ArrayList<GroupMst>  groupMst;
	private ArrayList<GroupPermissionMst> groupPermissionMst;
	private ArrayList<HeartBeatsMst> heartBeatsMst;
	private ArrayList<IntentDtl> intentDtl;
	private ArrayList<InvoiceEditEnableMst> invoiceEditEnableMst;
	private ArrayList<InvoiceFormatMst> invoiceFormatMst;
	private List<ItemBatchDtl> itemBatchDtl;
	private ItemBatchExpiryDtl itemBatchExpiryDtl;
	private ArrayList<ItemLocationMst> itemLocationMst; 
	private ArrayList<ItemMergeMst> itemMergeMst;
	private ArrayList<ItemMst> itemMst;
	private ArrayList<JobCardOutDtl> jobCardOutDtl;
	private ArrayList<JobCardDtl> jobCardDtl;
	private ArrayList<JournalDtl> journalDtl;
	private ArrayList<KitDefenitionDtl> kitDefenitionDtl;
	private byte[] kotMobilePerformaPrint;
	
	
	private byte[] kotMobile;
	
	private ArrayList<AddKotTable> addKotTable;
	private ArrayList<AddKotWaiter> addKotWaiter;
	private ArrayList<LedgerClass> ledgerClass;
	private  String LMSQueueRecieveMessage;
	private ArrayList<LocalCustomerMst>localCustomerMst;
	private ArrayList<LocationMst> locationMst;
	private  String MastersRecieveMessage;
	private ArrayList<MenuWindowMst> menuWindowMst;
	private ArrayList<MultiUnitMst>multiUnitMst;
	private ArrayList<OrderTakerMst> orderTakerMst;
	private ArrayList<OtherBranchPurchaseDtl>otherBranchPurchaseDtl;
	private OtherBranchSalesMessageEntity otherBranchSalesMessageEntity;
	private ArrayList<OwnAccount> ownAccount;
	private ArrayList<OwnAccountSettlementDtl> ownAccountSettlementDtl;
	private ArrayList<ParamValueConfig> paramValueConfig;
	private ArrayList<PaymentInvoiceDtl>paymentInvoiceDtl;
	private ArrayList<PaymentDtl>paymentDtl;
	
	private ArrayList<PhysicalStockDtl>physicalStockDtl;
	private ArrayList<PriceDefenitionMst>priceDefenitionMst;
	private ArrayList<ProcessMst> processMst;
	private ArrayList<ProcessPermissionMst>processPermissionMst;
	private ArrayList<ProductConversionConfigMst>productConversionConfigMst;
	private ArrayList<ProductConversionDtl>productConversionDtl;
	private ArrayList<ProductionDtlDtl>productionDtlDtl;
	private ArrayList<ProductionDtl>productionDtl;
	private ArrayList<ProductMst>productMst;
	private PurchaseMessageEntityVoucherToClient purchaseMessageEntityVoucherToClient;
	private ArrayList<PDCReconcileDtl> pDCReconcileDtl;
	private ArrayList<PDCReceipts> pDCReceipts;
	
	
	
	private ArrayList<RawMaterialIssueDtl>rawMaterialIssueDtl;
	private ArrayList<RawMaterialReturnDtl>rawMaterialReturnDtl;
	private ArrayList<ReceiptInvoiceDtl>receiptInvoiceDtl;
	private ArrayList<ReceiptDtl>receiptDtl;
	private ArrayList<ReceiptModeMst>receiptModeMst;
	private ArrayList<ReorderMst>reorderMst;
	private RetryVoucherMst retryVoucherMst;
	private ArrayList<SalesOrderDtl> salesOrderDtl;
	private ArrayList<SalesAnalysis>salesAnalysis;
	private ArrayList<SaleOrderReceipt>saleOrderReceipt;
	private byte[] salesDtlMobiles;
	private SalesOrderMessageEntity salesOrderMessageEntity;
	private ArrayList<SalesReturnDtl> salesReturnDtl;
	private ArrayList<SalesDtlMobile> salesDtlMobile;
	private ArrayList<SalesTypeMst>salesTypeMst;
	private ArrayList<SchEligiAttrListDef>schEligiAttrListDef;
	private ArrayList<SchEligibilityAttribInst>schEligibilityAttribInst;
	private ArrayList<SchEligibilityDef> schEligibilityDef;
	private ArrayList<SchemeInstance>schemeInstance;
	private ArrayList<SchOfferAttrListDef> schOfferAttrListDef;
	private ArrayList<SchOfferAttrInst> schOfferAttrInst;
	private ArrayList<SchOfferDef>schOfferDef;
	private ArrayList<SchSelectAttrListDef> schSelectAttrListDef;
	private ArrayList<SchSelectDef>schSelectDef;
	private ArrayList<SchSelectionAttribInst> schSelectionAttribInst;
	private ArrayList<ServiceInDtl> serviceInDtl;
	private StockTransferMessageEntity stockTransferMessageEntity;
	private String stockTransferSuccessQueueListen;
	private ArrayList<String> stockVerification;
	private ArrayList<StoreChangeDtl> storeChangeDtl;
	private ArrayList<StoreMst> storeMst;
	private ArrayList<SubBranchSalesDtl> subBranchSalesDtl;
	private  HashMap  taskListener;
	private ArrayList<String> transactionDelete ;
	private ArrayList<UnitMst>   unitMst;
	private ArrayList<UserMst> userMst;
	private ArrayList<String> voucherNo;
	private ArrayList<VoucherNumber> voucherNumber;
	private SalesMessageEntityVoucherToClient salesMessageEntityVoucherToClient;
	private ArrayList<WatchComplaintMst>  watchComplaintMst;
	private ArrayList<WatchObservationMst> watchObservationMst;
	private String sqlReceiveMessage;
	private ArrayList<WatchStrapMst> watchStrapMst;
	private ArrayList<UrsMst>ursMst;
	private ArrayList<PriceDefinition> priceDefinition;

    private ArrayList<PurchaseHdrMessageEntity> purchaseHdrMessageEntity;

	private AccountMessage accountMessage;
	private ArrayList<PurchaseOrderDtl> purchaseOrderDtls;
	private ArrayList<PDCReceipts> pdcReceipt;
	private ArrayList<PDCReconcileDtl> pdcReconcileDtl;
	private ArrayList<PurchaseOrderDtl> purchaseOrderDtl;
	
	
	private ClientStatusDtl clientStatusDtl;
	
	private ClientStatusHdr clientStatusHdr;

	private ArrayList<DayEndMessageEntity> dayEndMessageEntity;
	
	
	private ArrayList<ServerFailureStatusMst> serverFailureStatusMstArray;
	
	
	private String taskHdrId;
	private String taskSssignee;
	private String taskName;


	
	
	
	


	public ArrayList<ServerFailureStatusMst> getServerFailureStatusMstArray() {
		return serverFailureStatusMstArray;
	}

	public void setServerFailureStatusMstArray(ArrayList<ServerFailureStatusMst> serverFailureStatusMstArray) {
		this.serverFailureStatusMstArray = serverFailureStatusMstArray;
	}

	public ArrayList<GoodReceiveNoteHdr> getGoodReceiveNoteHdr() {
		return goodReceiveNoteHdr;
	}

	public void setGoodReceiveNoteHdr(ArrayList<GoodReceiveNoteHdr> goodReceiveNoteHdr) {
		this.goodReceiveNoteHdr = goodReceiveNoteHdr;
	}

	public ArrayList<GoodReceiveNoteDtl> getGoodReceiveNoteDtl() {
		return goodReceiveNoteDtl;
	}

	public void setGoodReceiveNoteDtl(ArrayList<GoodReceiveNoteDtl> goodReceiveNoteDtl) {
		this.goodReceiveNoteDtl = goodReceiveNoteDtl;
	}

	public ArrayList<PDCReconcileDtl> getPdcReconcileDtl() {
		return pdcReconcileDtl;
	}

	public void setPdcReconcileDtl(ArrayList<PDCReconcileDtl> pdcReconcileDtl) {
		this.pdcReconcileDtl = pdcReconcileDtl;
	}

	public ArrayList<PDCReceipts> getPdcReceipt() {
	return pdcReceipt;
}

public void setPdcReceipt(ArrayList<PDCReceipts> pdcReceipt) {
	this.pdcReceipt = pdcReceipt;
}

	public ArrayList<SaleOrderReceipt> getSaleOrderReceipt() {
		return saleOrderReceipt;
	}

	public void setSaleOrderReceipt(ArrayList<SaleOrderReceipt> saleOrderReceipt) {
		this.saleOrderReceipt = saleOrderReceipt;
	}

	public ArrayList<PurchaseSchemeMst> getPurchaseSchemeMst() {
		return purchaseSchemeMst;
	}

	public void setPurchaseSchemeMst(ArrayList<PurchaseSchemeMst> purchaseSchemeMst) {
		this.purchaseSchemeMst = purchaseSchemeMst;
	}

	public ArrayList<ItemImageMst> getItemImageMst() {
		return itemImageMst;
	}

	public void setItemImageMst(ArrayList<ItemImageMst> itemImageMst) {
		this.itemImageMst = itemImageMst;
	}

	public ArrayList<PDCPayment> getpDCPayment() {
		return pDCPayment;
	}

	public void setpDCPayment(ArrayList<PDCPayment> pDCPayment) {
		this.pDCPayment = pDCPayment;
	}

	public ArrayList<UrsMst> getUrsMst() {
		return ursMst;
	}

	public void setUrsMst(ArrayList<UrsMst> ursMst) {
		this.ursMst = ursMst;
	}

	public ArrayList<ItemDeviceMst> getItemDeviceMst() {
		return itemDeviceMst;
	}

	public void setItemDeviceMst(ArrayList<ItemDeviceMst> itemDeviceMst) {
		this.itemDeviceMst = itemDeviceMst;
	}

	public SalesMessageEntity getSalesMessageEntity() {
		return salesMessageEntity;
	}

	public void setSalesMessageEntity(SalesMessageEntity salesMessageEntity) {
		this.salesMessageEntity = salesMessageEntity;
	}
	
	

	
	public String getMessageKey() {
		return messageKey;
	}

	public void setMessageKey(String messageKey) {
		this.messageKey = messageKey;
	}

	

	public ArrayList<PurchaseDtl> getPurchaseDtl() {
		return purchaseDtl;
	}

	public void setPurchaseDtl(ArrayList<PurchaseDtl> purchaseDtl) {
		this.purchaseDtl = purchaseDtl;
	}

	public ArrayList<AccountClass> getAccountClass() {
		return accountClass;
	}

	public void setAccountClass(ArrayList<AccountClass> accountClass) {
		this.accountClass = accountClass;
	}

	public ArrayList<AccountHeads> getAccountHeads() {
		return accountHeads;
	}

	public void setAccountHeads(ArrayList<AccountHeads> accountHeads) {
		this.accountHeads = accountHeads;
	}

	public ArrayList<AccountPayable> getAccountPayable() {
		return accountPayable;
	}

	public void setAccountPayable(ArrayList<AccountPayable> accountPayable) {
		this.accountPayable = accountPayable;
	}

	public ArrayList<AccountReceivable> getAccountReceivable() {
		return accountReceivable;
	}

	public void setAccountReceivable(ArrayList<AccountReceivable> accountReceivable) {
		this.accountReceivable = accountReceivable;
	}

	public ArrayList<ActualProductionDtl> getActualProductionDtl() {
		return actualProductionDtl;
	}

	public void setActualProductionDtl(ArrayList<ActualProductionDtl> actualProductionDtl) {
		this.actualProductionDtl = actualProductionDtl;
	}

	public ArrayList<AdditionalExpense> getAdditionalExpense() {
		return additionalExpense;
	}

	public void setAdditionalExpense(ArrayList<AdditionalExpense> additionalExpense) {
		this.additionalExpense = additionalExpense;
	}

	public ArrayList<BatchPriceDefinition> getBatchPriceDefinition() {
		return batchPriceDefinition;
	}

	public void setBatchPriceDefinition(ArrayList<BatchPriceDefinition> batchPriceDefinition) {
		this.batchPriceDefinition = batchPriceDefinition;
	}

	public ArrayList<BranchMst> getBranchMst() {
		return branchMst;
	}

	public void setBranchMst(ArrayList<BranchMst> branchMst) {
		this.branchMst = branchMst;
	}

	public ArrayList<CategoryMst> getCategoryMst() {
		return categoryMst;
	}

	public void setCategoryMst(ArrayList<CategoryMst> categoryMst) {
		this.categoryMst = categoryMst;
	}

	public ArrayList<BrandMst> getBrandMst() {
		return brandMst;
	}

	public void setBrandMst(ArrayList<BrandMst> brandMst) {
		this.brandMst = brandMst;
	}

	public ArrayList<CompanyMst> getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(ArrayList<CompanyMst> companyMst) {
		this.companyMst = companyMst;
	}

	public ArrayList<ConsumptionDtl> getConsumptionDtl() {
		return consumptionDtl;
	}

	public void setConsumptionDtl(ArrayList<ConsumptionDtl> consumptionDtl) {
		this.consumptionDtl = consumptionDtl;
	}

	public ArrayList<ConsumptionReasonMst> getConsumptionReasonMst() {
		return consumptionReasonMst;
	}

	public void setConsumptionReasonMst(ArrayList<ConsumptionReasonMst> consumptionReasonMst) {
		this.consumptionReasonMst = consumptionReasonMst;
	}

	public ArrayList<CreditClass> getCreditClass() {
		return creditClass;
	}

	public void setCreditClass(ArrayList<CreditClass> creditClass) {
		this.creditClass = creditClass;
	}

	public ArrayList<CurrencyConversionMst> getCurrencyConversionMst() {
		return currencyConversionMst;
	}

	public void setCurrencyConversionMst(ArrayList<CurrencyConversionMst> currencyConversionMst) {
		this.currencyConversionMst = currencyConversionMst;
	}

	

	public ArrayList<DamageDtl> getDamageDtl() {
		return damageDtl;
	}

	public void setDamageDtl(ArrayList<DamageDtl> damageDtl) {
		this.damageDtl = damageDtl;
	}

	public ArrayList<DayBook> getDayBook() {
		return dayBook;
	}

	public void setDayBook(ArrayList<DayBook> dayBook) {
		this.dayBook = dayBook;
	}

	public ArrayList<DayEndClosureDtl> getDayEndClosureDtl() {
		return dayEndClosureDtl;
	}

	public void setDayEndClosureDtl(ArrayList<DayEndClosureDtl> dayEndClosureDtl) {
		this.dayEndClosureDtl = dayEndClosureDtl;
	}

	public ArrayList<DayEndReportStore> getDayEndReportStore() {
		return dayEndReportStore;
	}

	public void setDayEndReportStore(ArrayList<DayEndReportStore> dayEndReportStore) {
		this.dayEndReportStore = dayEndReportStore;
	}

	public ArrayList<DebitClass> getDebitClass() {
		return debitClass;
	}

	public void setDebitClass(ArrayList<DebitClass> debitClass) {
		this.debitClass = debitClass;
	}

	public ArrayList<DeliveryBoyMst> getDeliveryBoyMst() {
		return deliveryBoyMst;
	}

	public void setDeliveryBoyMst(ArrayList<DeliveryBoyMst> deliveryBoyMst) {
		this.deliveryBoyMst = deliveryBoyMst;
	}

	public ArrayList<GroupMst> getGroupMst() {
		return groupMst;
	}

	public void setGroupMst(ArrayList<GroupMst> groupMst) {
		this.groupMst = groupMst;
	}

	public ArrayList<GroupPermissionMst> getGroupPermissionMst() {
		return groupPermissionMst;
	}

	public void setGroupPermissionMst(ArrayList<GroupPermissionMst> groupPermissionMst) {
		this.groupPermissionMst = groupPermissionMst;
	}

	public ArrayList<HeartBeatsMst> getHeartBeatsMst() {
		return heartBeatsMst;
	}

	public void setHeartBeatsMst(ArrayList<HeartBeatsMst> heartBeatsMst) {
		this.heartBeatsMst = heartBeatsMst;
	}

	public ArrayList<IntentDtl> getIntentDtl() {
		return intentDtl;
	}

	public void setIntentDtl(ArrayList<IntentDtl> intentDtl) {
		this.intentDtl = intentDtl;
	}

	public ArrayList<InvoiceEditEnableMst> getInvoiceEditEnableMst() {
		return invoiceEditEnableMst;
	}

	public void setInvoiceEditEnableMst(ArrayList<InvoiceEditEnableMst> invoiceEditEnableMst) {
		this.invoiceEditEnableMst = invoiceEditEnableMst;
	}

	public ArrayList<InvoiceFormatMst> getInvoiceFormatMst() {
		return invoiceFormatMst;
	}

	public void setInvoiceFormatMst(ArrayList<InvoiceFormatMst> invoiceFormatMst) {
		this.invoiceFormatMst = invoiceFormatMst;
	}

	public List<ItemBatchDtl> getItemBatchDtl() {
		return itemBatchDtl;
	}

	public void setItemBatchDtl(List<ItemBatchDtl> itemBatchDtl) {
		this.itemBatchDtl = itemBatchDtl;
	}

	public ItemBatchExpiryDtl getItemBatchExpiryDtl() {
		return itemBatchExpiryDtl;
	}

	public void setItemBatchExpiryDtl(ItemBatchExpiryDtl itemBatchExpiryDtl) {
		this.itemBatchExpiryDtl = itemBatchExpiryDtl;
	}

	public ArrayList<ItemLocationMst> getItemLocationMst() {
		return itemLocationMst;
	}

	public void setItemLocationMst(ArrayList<ItemLocationMst> itemLocationMst) {
		this.itemLocationMst = itemLocationMst;
	}

	public ArrayList<ItemMergeMst> getItemMergeMst() {
		return itemMergeMst;
	}

	public void setItemMergeMst(ArrayList<ItemMergeMst> itemMergeMst) {
		this.itemMergeMst = itemMergeMst;
	}

	public ArrayList<ItemMst> getItemMst() {
		return itemMst;
	}

	public void setItemMst(ArrayList<ItemMst> itemMst) {
		this.itemMst = itemMst;
	}

	public ArrayList<JobCardOutDtl> getJobCardOutDtl() {
		return jobCardOutDtl;
	}

	public void setJobCardOutDtl(ArrayList<JobCardOutDtl> jobCardOutDtl) {
		this.jobCardOutDtl = jobCardOutDtl;
	}

	public ArrayList<JobCardDtl> getJobCardDtl() {
		return jobCardDtl;
	}

	public void setJobCardDtl(ArrayList<JobCardDtl> jobCardDtl) {
		this.jobCardDtl = jobCardDtl;
	}

	public ArrayList<JournalDtl> getJournalDtl() {
		return journalDtl;
	}

	public void setJournalDtl(ArrayList<JournalDtl> journalDtl) {
		this.journalDtl = journalDtl;
	}

	public ArrayList<KitDefenitionDtl> getKitDefenitionDtl() {
		return kitDefenitionDtl;
	}

	public void setKitDefenitionDtl(ArrayList<KitDefenitionDtl> kitDefenitionDtl) {
		this.kitDefenitionDtl = kitDefenitionDtl;
	}

	public byte[] getKotMobilePerformaPrint() {
		return kotMobilePerformaPrint;
	}

	public void setKotMobilePerformaPrint(byte[] kotMobilePerformaPrint) {
		this.kotMobilePerformaPrint = kotMobilePerformaPrint;
	}

	public byte[] getKotMobile() {
		return kotMobile;
	}

	public void setKotMobile(byte[] kotMobile) {
		this.kotMobile = kotMobile;
	}

	public ArrayList<AddKotTable> getAddKotTable() {
		return addKotTable;
	}

	public void setAddKotTable(ArrayList<AddKotTable> addKotTable) {
		this.addKotTable = addKotTable;
	}

	public ArrayList<AddKotWaiter> getAddKotWaiter() {
		return addKotWaiter;
	}

	public void setAddKotWaiter(ArrayList<AddKotWaiter> addKotWaiter) {
		this.addKotWaiter = addKotWaiter;
	}

	public ArrayList<LedgerClass> getLedgerClass() {
		return ledgerClass;
	}

	public void setLedgerClass(ArrayList<LedgerClass> ledgerClass) {
		this.ledgerClass = ledgerClass;
	}

	public String getLMSQueueRecieveMessage() {
		return LMSQueueRecieveMessage;
	}

	public void setLMSQueueRecieveMessage(String lMSQueueRecieveMessage) {
		LMSQueueRecieveMessage = lMSQueueRecieveMessage;
	}

	public ArrayList<LocalCustomerMst> getLocalCustomerMst() {
		return localCustomerMst;
	}

	public void setLocalCustomerMst(ArrayList<LocalCustomerMst> localCustomerMst) {
		this.localCustomerMst = localCustomerMst;
	}

	public ArrayList<LocationMst> getLocationMst() {
		return locationMst;
	}

	public void setLocationMst(ArrayList<LocationMst> locationMst) {
		this.locationMst = locationMst;
	}

	public String getMastersRecieveMessage() {
		return MastersRecieveMessage;
	}

	public void setMastersRecieveMessage(String mastersRecieveMessage) {
		MastersRecieveMessage = mastersRecieveMessage;
	}

	public ArrayList<MenuWindowMst> getMenuWindowMst() {
		return menuWindowMst;
	}

	public void setMenuWindowMst(ArrayList<MenuWindowMst> menuWindowMst) {
		this.menuWindowMst = menuWindowMst;
	}

	public ArrayList<MultiUnitMst> getMultiUnitMst() {
		return multiUnitMst;
	}

	public void setMultiUnitMst(ArrayList<MultiUnitMst> multiUnitMst) {
		this.multiUnitMst = multiUnitMst;
	}

	public ArrayList<OrderTakerMst> getOrderTakerMst() {
		return orderTakerMst;
	}

	public void setOrderTakerMst(ArrayList<OrderTakerMst> orderTakerMst) {
		this.orderTakerMst = orderTakerMst;
	}

	public ArrayList<OtherBranchPurchaseDtl> getOtherBranchPurchaseDtl() {
		return otherBranchPurchaseDtl;
	}

	public void setOtherBranchPurchaseDtl(ArrayList<OtherBranchPurchaseDtl> otherBranchPurchaseDtl) {
		this.otherBranchPurchaseDtl = otherBranchPurchaseDtl;
	}

	public OtherBranchSalesMessageEntity getOtherBranchSalesMessageEntity() {
		return otherBranchSalesMessageEntity;
	}

	public void setOtherBranchSalesMessageEntity(OtherBranchSalesMessageEntity otherBranchSalesMessageEntity) {
		this.otherBranchSalesMessageEntity = otherBranchSalesMessageEntity;
	}

	public ArrayList<OwnAccount> getOwnAccount() {
		return ownAccount;
	}

	public void setOwnAccount(ArrayList<OwnAccount> ownAccount) {
		this.ownAccount = ownAccount;
	}

	public ArrayList<OwnAccountSettlementDtl> getOwnAccountSettlementDtl() {
		return ownAccountSettlementDtl;
	}

	public void setOwnAccountSettlementDtl(ArrayList<OwnAccountSettlementDtl> ownAccountSettlementDtl) {
		this.ownAccountSettlementDtl = ownAccountSettlementDtl;
	}

	public ArrayList<ParamValueConfig> getParamValueConfig() {
		return paramValueConfig;
	}

	public void setParamValueConfig(ArrayList<ParamValueConfig> paramValueConfig) {
		this.paramValueConfig = paramValueConfig;
	}

	public ArrayList<PaymentInvoiceDtl> getPaymentInvoiceDtl() {
		return paymentInvoiceDtl;
	}

	public void setPaymentInvoiceDtl(ArrayList<PaymentInvoiceDtl> paymentInvoiceDtl) {
		this.paymentInvoiceDtl = paymentInvoiceDtl;
	}

	public ArrayList<PaymentDtl> getPaymentDtl() {
		return paymentDtl;
	}

	public void setPaymentDtl(ArrayList<PaymentDtl> paymentDtl) {
		this.paymentDtl = paymentDtl;
	}

	public ArrayList<PhysicalStockDtl> getPhysicalStockDtl() {
		return physicalStockDtl;
	}

	public void setPhysicalStockDtl(ArrayList<PhysicalStockDtl> physicalStockDtl) {
		this.physicalStockDtl = physicalStockDtl;
	}

	public ArrayList<PriceDefenitionMst> getPriceDefenitionMst() {
		return priceDefenitionMst;
	}

	public void setPriceDefenitionMst(ArrayList<PriceDefenitionMst> priceDefenitionMst) {
		this.priceDefenitionMst = priceDefenitionMst;
	}

	public ArrayList<ProcessMst> getProcessMst() {
		return processMst;
	}

	public void setProcessMst(ArrayList<ProcessMst> processMst) {
		this.processMst = processMst;
	}

	public ArrayList<ProcessPermissionMst> getProcessPermissionMst() {
		return processPermissionMst;
	}

	public void setProcessPermissionMst(ArrayList<ProcessPermissionMst> processPermissionMst) {
		this.processPermissionMst = processPermissionMst;
	}

	public ArrayList<ProductConversionConfigMst> getProductConversionConfigMst() {
		return productConversionConfigMst;
	}

	public void setProductConversionConfigMst(ArrayList<ProductConversionConfigMst> productConversionConfigMst) {
		this.productConversionConfigMst = productConversionConfigMst;
	}

	public ArrayList<ProductConversionDtl> getProductConversionDtl() {
		return productConversionDtl;
	}

	public void setProductConversionDtl(ArrayList<ProductConversionDtl> productConversionDtl) {
		this.productConversionDtl = productConversionDtl;
	}

	public ArrayList<ProductionDtlDtl> getProductionDtlDtl() {
		return productionDtlDtl;
	}

	public void setProductionDtlDtl(ArrayList<ProductionDtlDtl> productionDtlDtl) {
		this.productionDtlDtl = productionDtlDtl;
	}

	public ArrayList<ProductionDtl> getProductionDtl() {
		return productionDtl;
	}

	public void setProductionDtl(ArrayList<ProductionDtl> productionDtl) {
		this.productionDtl = productionDtl;
	}

	public ArrayList<ProductMst> getProductMst() {
		return productMst;
	}

	public void setProductMst(ArrayList<ProductMst> productMst) {
		this.productMst = productMst;
	}

	public PurchaseMessageEntityVoucherToClient getPurchaseMessageEntityVoucherToClient() {
		return purchaseMessageEntityVoucherToClient;
	}

	public void setPurchaseMessageEntityVoucherToClient(
			PurchaseMessageEntityVoucherToClient purchaseMessageEntityVoucherToClient) {
		this.purchaseMessageEntityVoucherToClient = purchaseMessageEntityVoucherToClient;
	}

	public ArrayList<RawMaterialIssueDtl> getRawMaterialIssueDtl() {
		return rawMaterialIssueDtl;
	}

	public void setRawMaterialIssueDtl(ArrayList<RawMaterialIssueDtl> rawMaterialIssueDtl) {
		this.rawMaterialIssueDtl = rawMaterialIssueDtl;
	}

	public ArrayList<RawMaterialReturnDtl> getRawMaterialReturnDtl() {
		return rawMaterialReturnDtl;
	}

	public void setRawMaterialReturnDtl(ArrayList<RawMaterialReturnDtl> rawMaterialReturnDtl) {
		this.rawMaterialReturnDtl = rawMaterialReturnDtl;
	}

	public ArrayList<ReceiptInvoiceDtl> getReceiptInvoiceDtl() {
		return receiptInvoiceDtl;
	}

	public void setReceiptInvoiceDtl(ArrayList<ReceiptInvoiceDtl> receiptInvoiceDtl) {
		this.receiptInvoiceDtl = receiptInvoiceDtl;
	}

	public ArrayList<ReceiptDtl> getReceiptDtl() {
		return receiptDtl;
	}

	public void setReceiptDtl(ArrayList<ReceiptDtl> receiptDtl) {
		this.receiptDtl = receiptDtl;
	}

	public ArrayList<ReceiptModeMst> getReceiptModeMst() {
		return receiptModeMst;
	}

	public void setReceiptModeMst(ArrayList<ReceiptModeMst> receiptModeMst) {
		this.receiptModeMst = receiptModeMst;
	}

	public ArrayList<ReorderMst> getReorderMst() {
		return reorderMst;
	}

	public void setReorderMst(ArrayList<ReorderMst> reorderMst) {
		this.reorderMst = reorderMst;
	}

	public RetryVoucherMst getRetryVoucherMst() {
		return retryVoucherMst;
	}

	public void setRetryVoucherMst(RetryVoucherMst retryVoucherMst) {
		this.retryVoucherMst = retryVoucherMst;
	}

	public ArrayList<SalesOrderDtl> getSalesOrderDtl() {
		return salesOrderDtl;
	}

	public void setSalesOrderDtl(ArrayList<SalesOrderDtl> salesOrderDtl) {
		this.salesOrderDtl = salesOrderDtl;
	}

	public ArrayList<SalesAnalysis> getSalesAnalysis() {
		return salesAnalysis;
	}

	public void setSalesAnalysis(ArrayList<SalesAnalysis> salesAnalysis) {
		this.salesAnalysis = salesAnalysis;
	}

	public byte[] getSalesDtlMobiles() {
		return salesDtlMobiles;
	}

	public void setSalesDtlMobiles(byte[] salesDtlMobiles) {
		this.salesDtlMobiles = salesDtlMobiles;
	}

	public SalesOrderMessageEntity getSalesOrderMessageEntity() {
		return salesOrderMessageEntity;
	}

	public void setSalesOrderMessageEntity(SalesOrderMessageEntity salesOrderMessageEntity) {
		this.salesOrderMessageEntity = salesOrderMessageEntity;
	}

	public ArrayList<SalesReturnDtl> getSalesReturnDtl() {
		return salesReturnDtl;
	}

	public void setSalesReturnDtl(ArrayList<SalesReturnDtl> salesReturnDtl) {
		this.salesReturnDtl = salesReturnDtl;
	}

	public ArrayList<SalesDtlMobile> getSalesDtlMobile() {
		return salesDtlMobile;
	}

	public void setSalesDtlMobile(ArrayList<SalesDtlMobile> salesDtlMobile) {
		this.salesDtlMobile = salesDtlMobile;
	}

	public ArrayList<SalesTypeMst> getSalesTypeMst() {
		return salesTypeMst;
	}

	public void setSalesTypeMst(ArrayList<SalesTypeMst> salesTypeMst) {
		this.salesTypeMst = salesTypeMst;
	}

	public ArrayList<SchEligiAttrListDef> getSchEligiAttrListDef() {
		return schEligiAttrListDef;
	}

	public void setSchEligiAttrListDef(ArrayList<SchEligiAttrListDef> schEligiAttrListDef) {
		this.schEligiAttrListDef = schEligiAttrListDef;
	}

	public ArrayList<SchEligibilityAttribInst> getSchEligibilityAttribInst() {
		return schEligibilityAttribInst;
	}

	public void setSchEligibilityAttribInst(ArrayList<SchEligibilityAttribInst> schEligibilityAttribInst) {
		this.schEligibilityAttribInst = schEligibilityAttribInst;
	}

	public ArrayList<SchEligibilityDef> getSchEligibilityDef() {
		return schEligibilityDef;
	}

	public void setSchEligibilityDef(ArrayList<SchEligibilityDef> schEligibilityDef) {
		this.schEligibilityDef = schEligibilityDef;
	}

	public ArrayList<SchemeInstance> getSchemeInstance() {
		return schemeInstance;
	}

	public void setSchemeInstance(ArrayList<SchemeInstance> schemeInstance) {
		this.schemeInstance = schemeInstance;
	}

	public ArrayList<SchOfferAttrListDef> getSchOfferAttrListDef() {
		return schOfferAttrListDef;
	}

	public void setSchOfferAttrListDef(ArrayList<SchOfferAttrListDef> schOfferAttrListDef) {
		this.schOfferAttrListDef = schOfferAttrListDef;
	}

	public ArrayList<SchOfferAttrInst> getSchOfferAttrInst() {
		return schOfferAttrInst;
	}

	public void setSchOfferAttrInst(ArrayList<SchOfferAttrInst> schOfferAttrInst) {
		this.schOfferAttrInst = schOfferAttrInst;
	}

	public ArrayList<SchOfferDef> getSchOfferDef() {
		return schOfferDef;
	}

	public void setSchOfferDef(ArrayList<SchOfferDef> schOfferDef) {
		this.schOfferDef = schOfferDef;
	}

	public ArrayList<SchSelectAttrListDef> getSchSelectAttrListDef() {
		return schSelectAttrListDef;
	}

	public void setSchSelectAttrListDef(ArrayList<SchSelectAttrListDef> schSelectAttrListDef) {
		this.schSelectAttrListDef = schSelectAttrListDef;
	}

	public ArrayList<SchSelectDef> getSchSelectDef() {
		return schSelectDef;
	}

	public void setSchSelectDef(ArrayList<SchSelectDef> schSelectDef) {
		this.schSelectDef = schSelectDef;
	}

	public ArrayList<SchSelectionAttribInst> getSchSelectionAttribInst() {
		return schSelectionAttribInst;
	}

	public void setSchSelectionAttribInst(ArrayList<SchSelectionAttribInst> schSelectionAttribInst) {
		this.schSelectionAttribInst = schSelectionAttribInst;
	}

	public ArrayList<ServiceInDtl> getServiceInDtl() {
		return serviceInDtl;
	}

	public void setServiceInDtl(ArrayList<ServiceInDtl> serviceInDtl) {
		this.serviceInDtl = serviceInDtl;
	}

	public StockTransferMessageEntity getStockTransferMessageEntity() {
		return stockTransferMessageEntity;
	}

	public void setStockTransferMessageEntity(StockTransferMessageEntity stockTransferMessageEntity) {
		this.stockTransferMessageEntity = stockTransferMessageEntity;
	}

	public String getStockTransferSuccessQueueListen() {
		return stockTransferSuccessQueueListen;
	}

	public void setStockTransferSuccessQueueListen(String stockTransferSuccessQueueListen) {
		this.stockTransferSuccessQueueListen = stockTransferSuccessQueueListen;
	}

	public ArrayList<String> getStockVerification() {
		return stockVerification;
	}

	public void setStockVerification(ArrayList<String> stockVerification) {
		this.stockVerification = stockVerification;
	}

	public ArrayList<StoreChangeDtl> getStoreChangeDtl() {
		return storeChangeDtl;
	}

	public void setStoreChangeDtl(ArrayList<StoreChangeDtl> storeChangeDtl) {
		this.storeChangeDtl = storeChangeDtl;
	}

	public ArrayList<StoreMst> getStoreMst() {
		return storeMst;
	}

	public void setStoreMst(ArrayList<StoreMst> storeMst) {
		this.storeMst = storeMst;
	}

	public ArrayList<SubBranchSalesDtl> getSubBranchSalesDtl() {
		return subBranchSalesDtl;
	}

	public void setSubBranchSalesDtl(ArrayList<SubBranchSalesDtl> subBranchSalesDtl) {
		this.subBranchSalesDtl = subBranchSalesDtl;
	}

	

	public HashMap getTaskListener() {
		return taskListener;
	}

	public void setTaskListener(HashMap taskListener) {
		this.taskListener = taskListener;
	}

	public ArrayList<String> getTransactionDelete() {
		return transactionDelete;
	}

	public void setTransactionDelete(ArrayList<String> transactionDelete) {
		this.transactionDelete = transactionDelete;
	}

	public ArrayList<UnitMst> getUnitMst() {
		return unitMst;
	}

	public void setUnitMst(ArrayList<UnitMst> unitMst) {
		this.unitMst = unitMst;
	}

	public ArrayList<UserMst> getUserMst() {
		return userMst;
	}

	public void setUserMst(ArrayList<UserMst> userMst) {
		this.userMst = userMst;
	}

	public ArrayList<String> getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(ArrayList<String> voucherNo) {
		this.voucherNo = voucherNo;
	}

	public ArrayList<VoucherNumber> getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(ArrayList<VoucherNumber> voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public SalesMessageEntityVoucherToClient getSalesMessageEntityVoucherToClient() {
		return salesMessageEntityVoucherToClient;
	}

	public void setSalesMessageEntityVoucherToClient(SalesMessageEntityVoucherToClient salesMessageEntityVoucherToClient) {
		this.salesMessageEntityVoucherToClient = salesMessageEntityVoucherToClient;
	}

	public ArrayList<WatchComplaintMst> getWatchComplaintMst() {
		return watchComplaintMst;
	}

	public void setWatchComplaintMst(ArrayList<WatchComplaintMst> watchComplaintMst) {
		this.watchComplaintMst = watchComplaintMst;
	}

	public ArrayList<WatchObservationMst> getWatchObservationMst() {
		return watchObservationMst;
	}

	public void setWatchObservationMst(ArrayList<WatchObservationMst> watchObservationMst) {
		this.watchObservationMst = watchObservationMst;
	}

	public String getSqlReceiveMessage() {
		return sqlReceiveMessage;
	}

	public void setSqlReceiveMessage(String sqlReceiveMessage) {
		this.sqlReceiveMessage = sqlReceiveMessage;
	}

	public ArrayList<WatchStrapMst> getWatchStrapMst() {
		return watchStrapMst;
	}

	public void setWatchStrapMst(ArrayList<WatchStrapMst> watchStrapMst) {
		this.watchStrapMst = watchStrapMst;
	}

	

	public String getCmdFetchItemMst() {
		return cmdFetchItemMst;
	}

	public void setCmdFetchItemMst(String cmdFetchItemMst) {
		this.cmdFetchItemMst = cmdFetchItemMst;
	}

	public AccountMessage getAccountMessage() {
		return accountMessage;
	}

	public void setAccountMessage(AccountMessage accountMessage) {
		this.accountMessage = accountMessage;
	}

	
	public ArrayList<PurchaseHdrMessageEntity> getPurchaseHdrMessageEntity() {
		return purchaseHdrMessageEntity;
	}

	public void setPurchaseHdrMessageEntity(ArrayList<PurchaseHdrMessageEntity> purchaseHdrMessageEntity) {
		this.purchaseHdrMessageEntity = purchaseHdrMessageEntity;
	}
	
	

	public ArrayList<PurchaseOrderDtl> getPurchaseOrderDtls() {
		return purchaseOrderDtls;
	}

	public void setPurchaseOrderDtls(ArrayList<PurchaseOrderDtl> purchaseOrderDtls) {
		this.purchaseOrderDtls = purchaseOrderDtls;
	}

	
	public ArrayList<PDCReconcileDtl> getpDCReconcileDtl() {
		return pDCReconcileDtl;
	}

	public void setpDCReconcileDtl(ArrayList<PDCReconcileDtl> pDCReconcileDtl) {
		this.pDCReconcileDtl = pDCReconcileDtl;
	}

	

	public ArrayList<PDCReceipts> getpDCReceipts() {
		return pDCReceipts;
	}

	public void setpDCReceipts(ArrayList<PDCReceipts> pDCReceipts) {
		this.pDCReceipts = pDCReceipts;
	}


	public ArrayList<PriceDefinition> getPriceDefinition() {
		return priceDefinition;
	}

	public void setPriceDefinition(ArrayList<PriceDefinition> priceDefinition) {
		this.priceDefinition = priceDefinition;
	}

	

	public ArrayList<PurchaseOrderDtl> getPurchaseOrderDtl() {
		return purchaseOrderDtl;
	}

	public void setPurchaseOrderDtl(ArrayList<PurchaseOrderDtl> purchaseOrderDtl) {
		this.purchaseOrderDtl = purchaseOrderDtl;
	}

	public ArrayList<BarcodeBatchMst> getBarcodeBatchMstList() {
		return barcodeBatchMstList;
	}

	public void setBarcodeBatchMstList(ArrayList<BarcodeBatchMst> barcodeBatchMstList) {
		this.barcodeBatchMstList = barcodeBatchMstList;
	}

	public ClientStatusDtl getClientStatusDtl() {
		return clientStatusDtl;
	}

	public void setClientStatusDtl(ClientStatusDtl clientStatusDtl) {
		this.clientStatusDtl = clientStatusDtl;
	}

	public ClientStatusHdr getClientStatusHdr() {
		return clientStatusHdr;
	}

	public void setClientStatusHdr(ClientStatusHdr clientStatusHdr) {
		this.clientStatusHdr = clientStatusHdr;
	}

	public ArrayList<DayEndMessageEntity> getDayEndMessageEntity() {
		return dayEndMessageEntity;
	}

	public void setDayEndMessageEntity(ArrayList<DayEndMessageEntity> dayEndMessageEntity) {
		this.dayEndMessageEntity = dayEndMessageEntity;
	}

	public String getTaskHdrId() {
		return taskHdrId;
	}

	public void setTaskHdrId(String taskHdrId) {
		this.taskHdrId = taskHdrId;
	}

	public String getTaskSssignee() {
		return taskSssignee;
	}

	public void setTaskSssignee(String taskSssignee) {
		this.taskSssignee = taskSssignee;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	@Override
	public String toString() {
		return "CommonReceiveMsgEntity [messageKey=" + messageKey + ", salesMessageEntity=" + salesMessageEntity
				+ ", purchaseDtl=" + purchaseDtl + ", accountClass=" + accountClass + ", accountHeads=" + accountHeads
				+ ", accountPayable=" + accountPayable + ", accountReceivable=" + accountReceivable
				+ ", actualProductionDtl=" + actualProductionDtl + ", additionalExpense=" + additionalExpense
				+ ", batchPriceDefinition=" + batchPriceDefinition + ", itemDeviceMst=" + itemDeviceMst
				+ ", purchaseSchemeMst=" + purchaseSchemeMst + ", itemImageMst=" + itemImageMst + ", pDCPayment="
				+ pDCPayment + ", goodReceiveNoteDtl=" + goodReceiveNoteDtl + ", goodReceiveNoteHdr="
				+ goodReceiveNoteHdr + ", barcodeBatchMstList=" + barcodeBatchMstList + ", cmdFetchItemMst="
				+ cmdFetchItemMst + ", branchMst=" + branchMst + ", categoryMst=" + categoryMst + ", brandMst="
				+ brandMst + ", companyMst=" + companyMst + ", consumptionDtl=" + consumptionDtl
				+ ", consumptionReasonMst=" + consumptionReasonMst + ", creditClass=" + creditClass
				+ ", currencyConversionMst=" + currencyConversionMst + ", damageDtl=" + damageDtl + ", dayBook="
				+ dayBook + ", dayEndClosureDtl=" + dayEndClosureDtl + ", dayEndReportStore=" + dayEndReportStore
				+ ", debitClass=" + debitClass + ", deliveryBoyMst=" + deliveryBoyMst + ", groupMst=" + groupMst
				+ ", groupPermissionMst=" + groupPermissionMst + ", heartBeatsMst=" + heartBeatsMst + ", intentDtl="
				+ intentDtl + ", invoiceEditEnableMst=" + invoiceEditEnableMst + ", invoiceFormatMst="
				+ invoiceFormatMst + ", itemBatchDtl=" + itemBatchDtl + ", itemBatchExpiryDtl=" + itemBatchExpiryDtl
				+ ", itemLocationMst=" + itemLocationMst + ", itemMergeMst=" + itemMergeMst + ", itemMst=" + itemMst
				+ ", jobCardOutDtl=" + jobCardOutDtl + ", jobCardDtl=" + jobCardDtl + ", journalDtl=" + journalDtl
				+ ", kitDefenitionDtl=" + kitDefenitionDtl + ", kotMobilePerformaPrint="
				+ Arrays.toString(kotMobilePerformaPrint) + ", kotMobile=" + Arrays.toString(kotMobile)
				+ ", addKotTable=" + addKotTable + ", addKotWaiter=" + addKotWaiter + ", ledgerClass=" + ledgerClass
				+ ", LMSQueueRecieveMessage=" + LMSQueueRecieveMessage + ", localCustomerMst=" + localCustomerMst
				+ ", locationMst=" + locationMst + ", MastersRecieveMessage=" + MastersRecieveMessage
				+ ", menuWindowMst=" + menuWindowMst + ", multiUnitMst=" + multiUnitMst + ", orderTakerMst="
				+ orderTakerMst + ", otherBranchPurchaseDtl=" + otherBranchPurchaseDtl
				+ ", otherBranchSalesMessageEntity=" + otherBranchSalesMessageEntity + ", ownAccount=" + ownAccount
				+ ", ownAccountSettlementDtl=" + ownAccountSettlementDtl + ", paramValueConfig=" + paramValueConfig
				+ ", paymentInvoiceDtl=" + paymentInvoiceDtl + ", paymentDtl=" + paymentDtl + ", physicalStockDtl="
				+ physicalStockDtl + ", priceDefenitionMst=" + priceDefenitionMst + ", processMst=" + processMst
				+ ", processPermissionMst=" + processPermissionMst + ", productConversionConfigMst="
				+ productConversionConfigMst + ", productConversionDtl=" + productConversionDtl + ", productionDtlDtl="
				+ productionDtlDtl + ", productionDtl=" + productionDtl + ", productMst=" + productMst
				+ ", purchaseMessageEntityVoucherToClient=" + purchaseMessageEntityVoucherToClient
				+ ", pDCReconcileDtl=" + pDCReconcileDtl + ", pDCReceipts=" + pDCReceipts + ", rawMaterialIssueDtl="
				+ rawMaterialIssueDtl + ", rawMaterialReturnDtl=" + rawMaterialReturnDtl + ", receiptInvoiceDtl="
				+ receiptInvoiceDtl + ", receiptDtl=" + receiptDtl + ", receiptModeMst=" + receiptModeMst
				+ ", reorderMst=" + reorderMst + ", retryVoucherMst=" + retryVoucherMst + ", salesOrderDtl="
				+ salesOrderDtl + ", salesAnalysis=" + salesAnalysis + ", saleOrderReceipt=" + saleOrderReceipt
				+ ", salesDtlMobiles=" + Arrays.toString(salesDtlMobiles) + ", salesOrderMessageEntity="
				+ salesOrderMessageEntity + ", salesReturnDtl=" + salesReturnDtl + ", salesDtlMobile=" + salesDtlMobile
				+ ", salesTypeMst=" + salesTypeMst + ", schEligiAttrListDef=" + schEligiAttrListDef
				+ ", schEligibilityAttribInst=" + schEligibilityAttribInst + ", schEligibilityDef=" + schEligibilityDef
				+ ", schemeInstance=" + schemeInstance + ", schOfferAttrListDef=" + schOfferAttrListDef
				+ ", schOfferAttrInst=" + schOfferAttrInst + ", schOfferDef=" + schOfferDef + ", schSelectAttrListDef="
				+ schSelectAttrListDef + ", schSelectDef=" + schSelectDef + ", schSelectionAttribInst="
				+ schSelectionAttribInst + ", serviceInDtl=" + serviceInDtl + ", stockTransferMessageEntity="
				+ stockTransferMessageEntity + ", stockTransferSuccessQueueListen=" + stockTransferSuccessQueueListen
				+ ", stockVerification=" + stockVerification + ", storeChangeDtl=" + storeChangeDtl + ", storeMst="
				+ storeMst + ", subBranchSalesDtl=" + subBranchSalesDtl + ", taskListener=" + taskListener
				+ ", transactionDelete=" + transactionDelete + ", unitMst=" + unitMst + ", userMst=" + userMst
				+ ", voucherNo=" + voucherNo + ", voucherNumber=" + voucherNumber
				+ ", salesMessageEntityVoucherToClient=" + salesMessageEntityVoucherToClient + ", watchComplaintMst="
				+ watchComplaintMst + ", watchObservationMst=" + watchObservationMst + ", sqlReceiveMessage="
				+ sqlReceiveMessage + ", watchStrapMst=" + watchStrapMst + ", ursMst=" + ursMst + ", priceDefinition="
				+ priceDefinition + ", purchaseHdrMessageEntity=" + purchaseHdrMessageEntity + ", accountMessage="
				+ accountMessage + ", purchaseOrderDtls=" + purchaseOrderDtls + ", pdcReceipt=" + pdcReceipt
				+ ", pdcReconcileDtl=" + pdcReconcileDtl + ", purchaseOrderDtl=" + purchaseOrderDtl
				+ ", clientStatusDtl=" + clientStatusDtl + ", clientStatusHdr=" + clientStatusHdr
				+ ", dayEndMessageEntity=" + dayEndMessageEntity + ", serverFailureStatusMstArray="
				+ serverFailureStatusMstArray + ", taskHdrId=" + taskHdrId + ", taskSssignee=" + taskSssignee
				+ ", taskName=" + taskName + "]";
	}

	



	

	
	
	
	

	
}
