package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class PurchasePriceDefinitionMst implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	@Column(length = 50)
	String purchaseVoucherNumber;

	@Column(length = 50)
	String itemId;

	@Column(length = 50)
	String itemName;

	@Column(length = 50)
	String batch;

	@Column(length = 50)
	String unitId;

	@Column(length = 50)
	String unitName;

	@Column(length = 10)
	String status;
	
	@Column(length = 10)
	String branchCode;
	
	@Column(length = 50)
	Date purchaseVoucherDate;
	
	
	@Column(length = 50)
	String purchaseHdrId;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPurchaseVoucherNumber() {
		return purchaseVoucherNumber;
	}

	public void setPurchaseVoucherNumber(String purchaseVoucherNumber) {
		this.purchaseVoucherNumber = purchaseVoucherNumber;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	

	public Date getPurchaseVoucherDate() {
		return purchaseVoucherDate;
	}

	public void setPurchaseVoucherDate(Date purchaseVoucherDate) {
		this.purchaseVoucherDate = purchaseVoucherDate;
	}
	

	public String getPurchaseHdrId() {
		return purchaseHdrId;
	}

	public void setPurchaseHdrId(String purchaseHdrId) {
		this.purchaseHdrId = purchaseHdrId;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	@Override
	public String toString() {
		return "PurchasePriceDefinitionMst [id=" + id + ", purchaseVoucherNumber=" + purchaseVoucherNumber + ", itemId="
				+ itemId + ", itemName=" + itemName + ", batch=" + batch + ", unitId=" + unitId + ", unitName="
				+ unitName + ", status=" + status + ", branchCode=" + branchCode + ", purchaseVoucherDate="
				+ purchaseVoucherDate + ", purchaseHdrId=" + purchaseHdrId + ", companyMst=" + companyMst + "]";
	}

	
	
	
}
