package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;

public class SummaryReportItemStock implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	private String itemName;
	@Column(length = 50)
	private String barCode;
	private Double cess;
	@Column(length = 50)
	private String hsnCode;
	private Double taxRate;
	@Column(length = 50)
	private String branch;
	private Double qty;
	private Double mrp;
	@Column(length = 50)
    String unitName;
	@Column(length = 50)
    private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public Double getCess() {
		return cess;
	}
	public void setCess(Double cess) {
		this.cess = cess;
	}
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SummaryReportItemStock [itemName=" + itemName + ", barCode=" + barCode + ", cess=" + cess + ", hsnCode="
				+ hsnCode + ", taxRate=" + taxRate + ", branch=" + branch + ", qty=" + qty + ", mrp=" + mrp
				+ ", unitName=" + unitName + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
