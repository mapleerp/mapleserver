package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class DayEndMessageEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	ArrayList<DayEndClosureDtl> dayEndClosureDtlList = new ArrayList<>();
	
	DayEndClosureHdr dayEndClosureHdr;

	public ArrayList<DayEndClosureDtl> getDayEndClosureDtlList() {
		return dayEndClosureDtlList;
	}

	public void setDayEndClosureDtlList(ArrayList<DayEndClosureDtl> dayEndClosureDtlList) {
		this.dayEndClosureDtlList = dayEndClosureDtlList;
	}

	public DayEndClosureHdr getDayEndClosureHdr() {
		return dayEndClosureHdr;
	}

	public void setDayEndClosureHdr(DayEndClosureHdr dayEndClosureHdr) {
		this.dayEndClosureHdr = dayEndClosureHdr;
	}

	@Override
	public String toString() {
		return "DayEndMessageEntity [dayEndClosureDtlList=" + dayEndClosureDtlList + ", dayEndClosureHdr="
				+ dayEndClosureHdr + "]";
	}
	
	
}
