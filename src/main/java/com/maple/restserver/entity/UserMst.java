package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
public class UserMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(length = 50)
    private String id;
	@Column(unique = true)
	private String userName;
	@Column(length = 50)
	private String branchCode;
	@Column(length = 50)
	private String fullName;
	@Column(length = 50)
	private String branchName;
	@Column(length = 50)
	private String password;
	
	private String status;
	@Column(length = 50)
	String oldId;
 
	public UserMst() {
		
	}
	

	public UserMst(String id, String userName, String branchCode, String fullName, String branchName, String password,
			String status) {
		super();
		this.id = id;
		this.userName = userName;
		this.branchCode = branchCode;
		this.fullName = fullName;
		this.branchName = branchName;
		this.password = password;
		this.status = status;
	}
	
 

	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
 
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public String getOldId() {
		return oldId;
	}


	public void setOldId(String oldId) {
		this.oldId = oldId;
	}


	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "UserMst [id=" + id + ", userName=" + userName + ", branchCode=" + branchCode + ", fullName=" + fullName
				+ ", branchName=" + branchName + ", password=" + password + ", status=" + status + ", oldId=" + oldId
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	

	
	
}
