

package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity

public class MultiBedroomMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	Integer Id;
	public Integer getId() {
		return Id;
	}@Column(length = 50)
	@JsonProperty("UserId")
	String UserId;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String getDeleted() {
		return Deleted;
	}
	public void setDeleted(String deleted) {
		Deleted = deleted;
	}
	@JsonProperty("Deleted")
	String Deleted;
	
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "MultiBedroomMst [Id=" + Id + ", UserId=" + UserId + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", Deleted=" + Deleted + "]";
	}
	
}
