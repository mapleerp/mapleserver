package com.maple.restserver.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class KotTableHdr {
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	String tableName;
	String tableStatus;
	String billedStatus;
	
	String waiterName;
	
	
	

	Date lastOrderPlacedTime;
	Date lastDeliveryTime;
	Date waitingTime;
	String overallKitchenStatus;
	Date reservationStartTime;
	Date reservationEndTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getTableStatus() {
		return tableStatus;
	}
	public void setTableStatus(String tableStatus) {
		this.tableStatus = tableStatus;
	}
	public Date getLastOrderPlacedTime() {
		return lastOrderPlacedTime;
	}
	public void setLastOrderPlacedTime(Date lastOrderPlacedTime) {
		this.lastOrderPlacedTime = lastOrderPlacedTime;
	}
	public Date getLastDeliveryTime() {
		return lastDeliveryTime;
	}
	public void setLastDeliveryTime(Date lastDeliveryTime) {
		this.lastDeliveryTime = lastDeliveryTime;
	}
	public Date getWaitingTime() {
		return waitingTime;
	}
	public void setWaitingTime(Date waitingTime) {
		this.waitingTime = waitingTime;
	}
	public String getOverallKitchenStatus() {
		return overallKitchenStatus;
	}
	public void setOverallKitchenStatus(String overallKitchenStatus) {
		this.overallKitchenStatus = overallKitchenStatus;
	}
	public String getBilledStatus() {
		return billedStatus;
	}
	public void setBilledStatus(String billedStatus) {
		this.billedStatus = billedStatus;
	}
	public Date getReservationStartTime() {
		return reservationStartTime;
	}
	public void setReservationStartTime(Date reservationStartTime) {
		this.reservationStartTime = reservationStartTime;
	}
	public Date getReservationEndTime() {
		return reservationEndTime;
	}
	public void setReservationEndTime(Date reservationEndTime) {
		this.reservationEndTime = reservationEndTime;
	}
	public String getWaiterName() {
		return waiterName;
	}
	public void setWaiterName(String waiterName) {
		this.waiterName = waiterName;
	}
	@Override
	public String toString() {
		return "KotTableHdr [id=" + id + ", companyMst=" + companyMst + ", tableName=" + tableName + ", tableStatus="
				+ tableStatus + ", billedStatus=" + billedStatus + ", waiterName=" + waiterName
				+ ", lastOrderPlacedTime=" + lastOrderPlacedTime + ", lastDeliveryTime=" + lastDeliveryTime
				+ ", waitingTime=" + waitingTime + ", overallKitchenStatus=" + overallKitchenStatus
				+ ", reservationStartTime=" + reservationStartTime + ", reservationEndTime=" + reservationEndTime + "]";
	}
	
	
	
}
