
package com.maple.restserver.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class  MonthlyJournalDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	@Column(length = 50)
	String accountId;
	String remarks;
	Double debitAmount;
	Double creditAmount;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "monthlyJournalHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private MonthlyJournalHdr monthlyJournalHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	

	
	public MonthlyJournalHdr getMonthlyJournalHdr() {
		return monthlyJournalHdr;
	}
	public void setMonthlyJournalHdr(MonthlyJournalHdr monthlyJournalHdr) {
		this.monthlyJournalHdr = monthlyJournalHdr;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Double getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "MonthlyJournalDtl [id=" + id + ", accountId=" + accountId + ", remarks=" + remarks + ", debitAmount="
				+ debitAmount + ", creditAmount=" + creditAmount + ", monthlyJournalHdr=" + monthlyJournalHdr
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	 
	
}
