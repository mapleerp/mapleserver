package com.maple.restserver.entity;

import java.util.ArrayList;

public class WebsiteClass {
	
	WebsiteMainMst websiteMainMst;
	ArrayList<WebsiteFeatureMst> websiteFeatureMst =new ArrayList();
	ArrayList<WebsiteServiceMst> websiteServiceMst =new ArrayList();
	public WebsiteMainMst getWebsiteMainMst() {
		return websiteMainMst;
	}
	public ArrayList<WebsiteFeatureMst> getWebsiteFeatureMst() {
		return websiteFeatureMst;
	}
	public ArrayList<WebsiteServiceMst> getWebsiteServiceMst() {
		return websiteServiceMst;
	}
	public void setWebsiteMainMst(WebsiteMainMst websiteMainMst) {
		this.websiteMainMst = websiteMainMst;
	}
	public void setWebsiteFeatureMst(ArrayList<WebsiteFeatureMst> websiteFeatureMst) {
		this.websiteFeatureMst = websiteFeatureMst;
	}
	public void setWebsiteServiceMst(ArrayList<WebsiteServiceMst> websiteServiceMst) {
		this.websiteServiceMst = websiteServiceMst;
	}
	@Override
	public String toString() {
		return "WebsiteClass [websiteMainMst=" + websiteMainMst + ", websiteFeatureMst=" + websiteFeatureMst
				+ ", websiteServiceMst=" + websiteServiceMst + "]";
	}
	
	
	
	
	

}
