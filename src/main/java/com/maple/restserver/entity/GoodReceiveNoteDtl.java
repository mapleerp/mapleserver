package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;



@Entity
public class GoodReceiveNoteDtl implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
 
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "good_receive_note_hdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	GoodReceiveNoteHdr goodReceiveNoteHdr;
	private Double fcMrp;

	
	public Double getFcMrp() {
		return fcMrp;
	}



	public void setFcMrp(Double fcMrp) {
		this.fcMrp = fcMrp;
	}



	String purchaseOrderDtl;
	@Column(length = 256)
	private String itemName;

	 Double goodReceiveNoteRate;

	@Column(length = 50)
	private String batch;

	private Integer itemSerial;
	@Column(length = 50)

	private String barcode;


	private Date expiryDate;

	private Integer freeQty;

	private Double qty;


	private Integer unit;

	private Double mrp;

	private Date manufactureDate;
	@Column(length = 50)
	private String itemId;
	@Column(length = 50)
	private String BinNo;

	@Column(length = 50)
	private String unitId;

	
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	private Integer displaySerial;
	
	private String store;
	
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;




	

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public GoodReceiveNoteHdr getGoodReceiveNoteHdr() {
		return goodReceiveNoteHdr;
	}



	public void setGoodReceiveNoteHdr(GoodReceiveNoteHdr goodReceiveNoteHdr) {
		this.goodReceiveNoteHdr = goodReceiveNoteHdr;
	}



	public String getPurchaseOrderDtl() {
		return purchaseOrderDtl;
	}



	public void setPurchaseOrderDtl(String purchaseOrderDtl) {
		this.purchaseOrderDtl = purchaseOrderDtl;
	}



	public String getItemName() {
		return itemName;
	}



	public void setItemName(String itemName) {
		this.itemName = itemName;
	}



	public Double getGoodReceiveNoteRate() {
		return goodReceiveNoteRate;
	}



	public void setGoodReceiveNoteRate(Double goodReceiveNoteRate) {
		this.goodReceiveNoteRate = goodReceiveNoteRate;
	}



	public String getBatch() {
		return batch;
	}



	public void setBatch(String batch) {
		this.batch = batch;
	}



	public Integer getItemSerial() {
		return itemSerial;
	}



	public void setItemSerial(Integer itemSerial) {
		this.itemSerial = itemSerial;
	}



	public String getBarcode() {
		return barcode;
	}



	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}



	public Date getExpiryDate() {
		return expiryDate;
	}



	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}



	public Integer getFreeQty() {
		return freeQty;
	}



	public void setFreeQty(Integer freeQty) {
		this.freeQty = freeQty;
	}



	public Double getQty() {
		return qty;
	}



	public void setQty(Double qty) {
		this.qty = qty;
	}



	public Integer getUnit() {
		return unit;
	}



	public void setUnit(Integer unit) {
		this.unit = unit;
	}



	public Double getMrp() {
		return mrp;
	}



	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}



	public Date getManufactureDate() {
		return manufactureDate;
	}



	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}



	public String getItemId() {
		return itemId;
	}



	public void setItemId(String itemId) {
		this.itemId = itemId;
	}



	public String getBinNo() {
		return BinNo;
	}



	public void setBinNo(String binNo) {
		BinNo = binNo;
	}



	public String getUnitId() {
		return unitId;
	}



	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	public Integer getDisplaySerial() {
		return displaySerial;
	}



	public void setDisplaySerial(Integer displaySerial) {
		this.displaySerial = displaySerial;
	}



	public String getStore() {
		return store;
	}



	public void setStore(String store) {
		this.store = store;
	}



	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	@Override
	public String toString() {
		return "GoodReceiveNoteDtl [id=" + id + ", goodReceiveNoteHdr=" + goodReceiveNoteHdr + ", fcMrp=" + fcMrp
				+ ", purchaseOrderDtl=" + purchaseOrderDtl + ", itemName=" + itemName + ", goodReceiveNoteRate="
				+ goodReceiveNoteRate + ", batch=" + batch + ", itemSerial=" + itemSerial + ", barcode=" + barcode
				+ ", expiryDate=" + expiryDate + ", freeQty=" + freeQty + ", qty=" + qty + ", unit=" + unit + ", mrp="
				+ mrp + ", manufactureDate=" + manufactureDate + ", itemId=" + itemId + ", BinNo=" + BinNo + ", unitId="
				+ unitId + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", displaySerial="
				+ displaySerial + ", store=" + store + ", companyMst=" + companyMst + "]";
	}



	
	


	


	
	

	

	

}