package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class InsuranceCompanyDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	String id;
	@Column(length = 50)
	String policyType;
	@Column(length = 50)
	String percentageCoverage;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "insuranceCompanyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	InsuranceCompanyMst insuranceCompanyMst;
	
	
	public InsuranceCompanyMst getInsuranceCompanyMst() {
		return insuranceCompanyMst;
	}
	public void setInsuranceCompanyMst(InsuranceCompanyMst insuranceCompanyMst) {
		this.insuranceCompanyMst = insuranceCompanyMst;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public String getPercentageCoverage() {
		return percentageCoverage;
	}
	public void setPercentageCoverage(String percentageCoverage) {
		this.percentageCoverage = percentageCoverage;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	@Override
	public String toString() {
		return "InsuranceCompanyDtl [id=" + id + ", policyType=" + policyType + ", percentageCoverage="
				+ percentageCoverage + ", companyMst=" + companyMst + ", insuranceCompanyMst=" + insuranceCompanyMst
				+ "]";
	}
	
	
	
	
	
	
	
	
	
}
