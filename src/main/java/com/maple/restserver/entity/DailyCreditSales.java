package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class DailyCreditSales implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	
	
	Double b2BSales;
	Double uberSales;
	Double sWiggySales;
	Double zomatoSales;
	Double foodPandaSales;
	Date reportDate;
	@Column(length = 50)
	String branchCode;
	Double onlineSales;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Double getB2BSales() {
		return b2BSales;
	}
	public void setB2BSales(Double b2bSales) {
		b2BSales = b2bSales;
	}
	public Double getUberSales() {
		return uberSales;
	}
	public void setUberSales(Double uberSales) {
		this.uberSales = uberSales;
	}
	public Double getsWiggySales() {
		return sWiggySales;
	}
	public void setsWiggySales(Double sWiggySales) {
		this.sWiggySales = sWiggySales;
	}
	public Double getZomatoSales() {
		return zomatoSales;
	}
	public void setZomatoSales(Double zomatoSales) {
		this.zomatoSales = zomatoSales;
	}
	public Double getFoodPandaSales() {
		return foodPandaSales;
	}
	public void setFoodPandaSales(Double foodPandaSales) {
		this.foodPandaSales = foodPandaSales;
	}
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Double getOnlineSales() {
		return onlineSales;
	}
	public void setOnlineSales(Double onlineSales) {
		this.onlineSales = onlineSales;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "DailyCreditSales [id=" + id + ", b2BSales=" + b2BSales + ", uberSales=" + uberSales + ", sWiggySales="
				+ sWiggySales + ", zomatoSales=" + zomatoSales + ", foodPandaSales=" + foodPandaSales + ", reportDate="
				+ reportDate + ", branchCode=" + branchCode + ", onlineSales=" + onlineSales + ", companyMst="
				+ companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


}
