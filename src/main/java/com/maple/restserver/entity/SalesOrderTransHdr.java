
package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

 

/*
 * R.George
 * Entity Corrected on 5/6
 * 
 */

@Entity
public class SalesOrderTransHdr implements Serializable{
	

	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

//	@Column(nullable = false)

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "accountHeads", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	AccountHeads accountHeads;
	private String customiseSalesMode;
	@Column(length = 50)
	private String salesManId;
	@Column(length = 50)
	private String deliveryBoyId;
	@Column(length = 50)
	private String salesMode;
	@Column(length = 50)
	private String creditOrCash;
	@Column(length = 50)
	private String userId;

	private Date  orderDue;
	@Column(length = 50)
	private String branchCode;
	@Column(length = 50)
	private String machineId;
	@Column(length = 50)
	private String voucherNumber;

	private Date voucherDate;

	private Double invoiceAmount;

	private Double invoiceDiscount;

	private Double cashPay;

	private Double itemDiscount;

	private Double paidAmount;

	public String getCustomiseSalesMode() {
		return customiseSalesMode;
	}



	public void setCustomiseSalesMode(String customiseSalesMode) {
		this.customiseSalesMode = customiseSalesMode;
	}



	private Double changeAmount;
	@Column(length = 50)
	private String cardNo;

	private Double cardamount;
	@Column(length = 50)
	private String convertedToInvoice;
	
	private Date convertedToInvoiceDate;
	
	@Column(length = 50)
	private String orderMessageToPrint;
	private String orderAdvice;
	@Column(length = 50)
	private String targetDepartment;

	@Column(length = 50)
	
	private String servingTableName;
	@Column(length = 50)
	private String orderStatus;
	@Column(length = 50)
	private String deliveryMode;
	@Column(length = 50)
	private String paymentMode;
	@Column(length = 50)
	private String saleOrderReceiptVoucherNumber;
	@Column(length = 50)
	private String performaInvoicePrinted;
    Date statusDate;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "localCustomerId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	LocalCustomerMst localCustomerId;
	@Column(length = 50)
	
	private String orderDueTime;
	@Column(length = 50)
	private String orderTimeMode;
	@Column(length = 50)
	private String orderDueDay;
	@Column(length = 50)
	private String ordedBy;
	
	private Double sodexoAmount;
	@Column(length = 50)
	private String cardType;
	


	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;

	private String cancelationReason;
	@Column(length = 50)
	String oldId;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	String invoicePrintType;

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}





	public String getSalesManId() {
		return salesManId;
	}



	public void setSalesManId(String salesManId) {
		this.salesManId = salesManId;
	}



	public String getDeliveryBoyId() {
		return deliveryBoyId;
	}



	public void setDeliveryBoyId(String deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}



	public String getSalesMode() {
		return salesMode;
	}



	public void setSalesMode(String salesMode) {
		this.salesMode = salesMode;
	}



	public String getCreditOrCash() {
		return creditOrCash;
	}



	public void setCreditOrCash(String creditOrCash) {
		this.creditOrCash = creditOrCash;
	}



	public String getUserId() {
		return userId;
	}



	public String getPerformaInvoicePrinted() {
		return performaInvoicePrinted;
	}



	public void setPerformaInvoicePrinted(String performaInvoicePrinted) {
		this.performaInvoicePrinted = performaInvoicePrinted;
	}



	public void setUserId(String userId) {
		this.userId = userId;
	}



	public Date getOrderDue() {
		return orderDue;
	}



	public void setOrderDue(Date orderDue) {
		this.orderDue = orderDue;
	}



	public String getBranchCode() {
		return branchCode;
	}



	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}



	public String getMachineId() {
		return machineId;
	}



	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}



	public String getVoucherNumber() {
		return voucherNumber;
	}



	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}



	public Date getVoucherDate() {
		return voucherDate;
	}



	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}



	public Double getInvoiceAmount() {
		return invoiceAmount;
	}



	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}



	public Double getInvoiceDiscount() {
		return invoiceDiscount;
	}



	public void setInvoiceDiscount(Double invoiceDiscount) {
		this.invoiceDiscount = invoiceDiscount;
	}



	public Double getCashPay() {
		return cashPay;
	}



	public void setCashPay(Double cashPay) {
		this.cashPay = cashPay;
	}



	public Double getItemDiscount() {
		return itemDiscount;
	}



	public void setItemDiscount(Double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}



	public Double getPaidAmount() {
		return paidAmount;
	}



	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}



	public Double getChangeAmount() {
		return changeAmount;
	}



	public void setChangeAmount(Double changeAmount) {
		this.changeAmount = changeAmount;
	}



	public String getCardNo() {
		return cardNo;
	}



	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}



	public Double getCardamount() {
		return cardamount;
	}



	public void setCardamount(Double cardamount) {
		this.cardamount = cardamount;
	}



	public String getConvertedToInvoice() {
		return convertedToInvoice;
	}



	public void setConvertedToInvoice(String convertedToInvoice) {
		this.convertedToInvoice = convertedToInvoice;
	}



	public Date getConvertedToInvoiceDate() {
		return convertedToInvoiceDate;
	}



	public void setConvertedToInvoiceDate(Date convertedToInvoiceDate) {
		this.convertedToInvoiceDate = convertedToInvoiceDate;
	}



	public String getOrderMessageToPrint() {
		return orderMessageToPrint;
	}



	public void setOrderMessageToPrint(String orderMessageToPrint) {
		this.orderMessageToPrint = orderMessageToPrint;
	}



	public String getOrderAdvice() {
		return orderAdvice;
	}



	public void setOrderAdvice(String orderAdvice) {
		this.orderAdvice = orderAdvice;
	}



	public String getTargetDepartment() {
		return targetDepartment;
	}



	public void setTargetDepartment(String targetDepartment) {
		this.targetDepartment = targetDepartment;
	}



	public String getServingTableName() {
		return servingTableName;
	}



	public void setServingTableName(String servingTableName) {
		this.servingTableName = servingTableName;
	}



	public String getOrderStatus() {
		return orderStatus;
	}



	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}



	public String getDeliveryMode() {
		return deliveryMode;
	}



	public void setDeliveryMode(String deliveryMode) {
		this.deliveryMode = deliveryMode;
	}



	public String getPaymentMode() {
		return paymentMode;
	}



	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}



	public String getSaleOrderReceiptVoucherNumber() {
		return saleOrderReceiptVoucherNumber;
	}



	public void setSaleOrderReceiptVoucherNumber(String saleOrderReceiptVoucherNumber) {
		this.saleOrderReceiptVoucherNumber = saleOrderReceiptVoucherNumber;
	}



	public Date getStatusDate() {
		return statusDate;
	}



	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}



	public LocalCustomerMst getLocalCustomerId() {
		return localCustomerId;
	}



	public void setLocalCustomerId(LocalCustomerMst localCustomerId) {
		this.localCustomerId = localCustomerId;
	}



	public String getOrderDueTime() {
		return orderDueTime;
	}



	public void setOrderDueTime(String orderDueTime) {
		this.orderDueTime = orderDueTime;
	}



	public String getOrderTimeMode() {
		return orderTimeMode;
	}



	public void setOrderTimeMode(String orderTimeMode) {
		this.orderTimeMode = orderTimeMode;
	}



	public String getOrderDueDay() {
		return orderDueDay;
	}



	public void setOrderDueDay(String orderDueDay) {
		this.orderDueDay = orderDueDay;
	}



	public String getOrdedBy() {
		return ordedBy;
	}



	public void setOrdedBy(String ordedBy) {
		this.ordedBy = ordedBy;
	}



	public Double getSodexoAmount() {
		return sodexoAmount;
	}



	public void setSodexoAmount(Double sodexoAmount) {
		this.sodexoAmount = sodexoAmount;
	}



	public String getCardType() {
		return cardType;
	}



	public void setCardType(String cardType) {
		this.cardType = cardType;
	}



	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public String getCancelationReason() {
		return cancelationReason;
	}



	public void setCancelationReason(String cancelationReason) {
		this.cancelationReason = cancelationReason;
	}



	
	
	
	



	public String getOldId() {
		return oldId;
	}



	public void setOldId(String oldId) {
		this.oldId = oldId;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	

	public String getInvoicePrintType() {
		return invoicePrintType;
	}



	public void setInvoicePrintType(String invoicePrintType) {
		this.invoicePrintType = invoicePrintType;
	}



	public AccountHeads getAccountHeads() {
		return accountHeads;
	}



	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}



	@Override
	public String toString() {
		return "SalesOrderTransHdr [id=" + id + ", accountHeads=" + accountHeads + ", customiseSalesMode="
				+ customiseSalesMode + ", salesManId=" + salesManId + ", deliveryBoyId=" + deliveryBoyId
				+ ", salesMode=" + salesMode + ", creditOrCash=" + creditOrCash + ", userId=" + userId + ", orderDue="
				+ orderDue + ", branchCode=" + branchCode + ", machineId=" + machineId + ", voucherNumber="
				+ voucherNumber + ", voucherDate=" + voucherDate + ", invoiceAmount=" + invoiceAmount
				+ ", invoiceDiscount=" + invoiceDiscount + ", cashPay=" + cashPay + ", itemDiscount=" + itemDiscount
				+ ", paidAmount=" + paidAmount + ", changeAmount=" + changeAmount + ", cardNo=" + cardNo
				+ ", cardamount=" + cardamount + ", convertedToInvoice=" + convertedToInvoice
				+ ", convertedToInvoiceDate=" + convertedToInvoiceDate + ", orderMessageToPrint=" + orderMessageToPrint
				+ ", orderAdvice=" + orderAdvice + ", targetDepartment=" + targetDepartment + ", servingTableName="
				+ servingTableName + ", orderStatus=" + orderStatus + ", deliveryMode=" + deliveryMode
				+ ", paymentMode=" + paymentMode + ", saleOrderReceiptVoucherNumber=" + saleOrderReceiptVoucherNumber
				+ ", performaInvoicePrinted=" + performaInvoicePrinted + ", statusDate=" + statusDate
				+ ", localCustomerId=" + localCustomerId + ", orderDueTime=" + orderDueTime + ", orderTimeMode="
				+ orderTimeMode + ", orderDueDay=" + orderDueDay + ", ordedBy=" + ordedBy + ", sodexoAmount="
				+ sodexoAmount + ", cardType=" + cardType + ", companyMst=" + companyMst + ", cancelationReason="
				+ cancelationReason + ", oldId=" + oldId + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + ", invoicePrintType=" + invoicePrintType + "]";
	}







	

}
