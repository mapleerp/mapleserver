package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
@Entity
public class PurchaseSchemeMst implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	@Column(length = 50)
	String supplierId;
	@Column(length = 50)
	String itemId;
	String offerDescription;
	 @Column(  nullable = true)
	Double minimumQty;
	 @Column(  nullable = true)
	Date fromDate;
	 @Column(  nullable = true)
	Date toDate;
	 String branchCode;
		@ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "companyMst", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		private CompanyMst companyMst;
		@Column(length = 50)
		private String  processInstanceId;
		@Column(length = 50)
		private String taskId;

	public CompanyMst getCompanyMst() {
			return companyMst;
		}
		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}
	public String getId() {
		return id;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getOfferDescription() {
		return offerDescription;
	}
	public void setOfferDescription(String offerDescription) {
		this.offerDescription = offerDescription;
	}
	public Double getMinimumQty() {
		return minimumQty;
	}
	public void setMinimumQty(Double minimumQty) {
		this.minimumQty = minimumQty;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PurchaseSchemeMst [id=" + id + ", supplierId=" + supplierId + ", itemId=" + itemId
				+ ", offerDescription=" + offerDescription + ", minimumQty=" + minimumQty + ", fromDate=" + fromDate
				+ ", toDate=" + toDate + ", branchCode=" + branchCode + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
