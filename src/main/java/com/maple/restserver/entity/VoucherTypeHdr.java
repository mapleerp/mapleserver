package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

public class VoucherTypeHdr implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	String voucherTypeName;
	@Column(length = 50)
	String voucherTypePrefix;
	@Column(length = 50)
	String voucherTypeSufix;
	Integer voucherTypeStartingNo;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucherTypeName() {
		return voucherTypeName;
	}
	public void setVoucherTypeName(String voucherTypeName) {
		this.voucherTypeName = voucherTypeName;
	}
	public String getVoucherTypePrefix() {
		return voucherTypePrefix;
	}
	public void setVoucherTypePrefix(String voucherTypePrefix) {
		this.voucherTypePrefix = voucherTypePrefix;
	}
	public String getVoucherTypeSufix() {
		return voucherTypeSufix;
	}
	public void setVoucherTypeSufix(String voucherTypeSufix) {
		this.voucherTypeSufix = voucherTypeSufix;
	}
	public Integer getVoucherTypeStartingNo() {
		return voucherTypeStartingNo;
	}
	public void setVoucherTypeStartingNo(Integer voucherTypeStartingNo) {
		this.voucherTypeStartingNo = voucherTypeStartingNo;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "VoucherTypeHdr [id=" + id + ", voucherTypeName=" + voucherTypeName + ", voucherTypePrefix="
				+ voucherTypePrefix + ", voucherTypeSufix=" + voucherTypeSufix + ", voucherTypeStartingNo="
				+ voucherTypeStartingNo + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", companyMst=" + companyMst + "]";
	}
	
		
}
