package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
 
public class KitDefenitionDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	String itemId;
	@Column(length = 50)
	String barcode;
	Double qty;
	@Column(length = 50)
	String unitId;
	@Column(length = 50)
	String unitName;
	@Column(length = 50)
	String itemName;
	 
	 //version1.8
		Double mltiUnitQty;
		@Column(length = 50)
		String customisedUnitId;
		///version1.8ends	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "kitDefenitionmst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	KitDefinitionMst kitDefenitionmst;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	


	
	
	//version1.8
	
	public Double getMltiUnitQty() {
		return mltiUnitQty;
	}

	public void setMltiUnitQty(Double mltiUnitQty) {
		this.mltiUnitQty = mltiUnitQty;
	}

	public String getCustomisedUnitId() {
		return customisedUnitId;
	}

	public void setCustomisedUnitId(String customisedUnitId) {
		this.customisedUnitId = customisedUnitId;
	}
 //version1.8ends
	
	public String getId() {
		return id;
	}

	public String getBarcode() {
		return barcode;
	}


	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}


	public Double getQty() {
		return qty;
	}


	public void setQty(Double qty) {
		this.qty = qty;
	}


	public String getUnitId() {
		return unitId;
	}


	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}


	public String getUnitName() {
		return unitName;
	}


	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}


	public KitDefinitionMst getKitDefenitionmst() {
		return kitDefenitionmst;
	}


	public void setKitDefenitionmst(KitDefinitionMst kitDefenitionmst) {
		this.kitDefenitionmst = kitDefenitionmst;
	}


	public String getItemId() {
		return itemId;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public void setId(String id) {
		this.id = id;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "KitDefenitionDtl [id=" + id + ", itemId=" + itemId + ", barcode=" + barcode + ", qty=" + qty
				+ ", unitId=" + unitId + ", unitName=" + unitName + ", itemName=" + itemName + ", mltiUnitQty="
				+ mltiUnitQty + ", customisedUnitId=" + customisedUnitId + ", kitDefenitionmst=" + kitDefenitionmst
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}


 
	

}
