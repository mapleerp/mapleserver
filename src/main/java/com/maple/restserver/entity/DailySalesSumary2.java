package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


import org.hibernate.annotations.GenericGenerator;

@Entity
public class DailySalesSumary2 implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	private Double totalCashSales;
	private Double totalSalesOrder;
	@Column(length = 50)
	String branchCode;
	Date reportDate;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Double getTotalCashSales() {
		return totalCashSales;
	}
	public void setTotalCashSales(Double totalCashSales) {
		this.totalCashSales = totalCashSales;
	}
	public Double getTotalSalesOrder() {
		return totalSalesOrder;
	}
	public void setTotalSalesOrder(Double totalSalesOrder) {
		this.totalSalesOrder = totalSalesOrder;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "DailySalesSumary2 [id=" + id + ", totalCashSales=" + totalCashSales + ", totalSalesOrder="
				+ totalSalesOrder + ", branchCode=" + branchCode + ", reportDate=" + reportDate + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

	
}
