package com.maple.restserver.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class DynamicProductionDtl {
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	@Column(length = 50)
	String itemId;
	Double qty;
	@Column(length = 50)
	String unitId;
	@Column(length = 50)
	String batch;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "dynamicProductionHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private DynamicProductionHdr dynamicProductionHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public DynamicProductionHdr getDynamicProductionHdr() {
		return dynamicProductionHdr;
	}

	public void setDynamicProductionHdr(DynamicProductionHdr dynamicProductionHdr) {
		this.dynamicProductionHdr = dynamicProductionHdr;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "DynamicProductionDtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", unitId=" + unitId
				+ ", batch=" + batch + ", dynamicProductionHdr=" + dynamicProductionHdr + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}




}
