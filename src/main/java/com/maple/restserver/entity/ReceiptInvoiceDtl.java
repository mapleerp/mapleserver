package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ReceiptInvoiceDtl implements Serializable {


	private static final long serialVersionUID = 1L;

	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	@Column(length = 50)
	private String receiptNumber;
	@Column(length = 50)
	private String invoiceNumber;
	private Double recievedAmount;
	private Date receiptDate;
	private Date invoiceDate;
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "customerRegistration", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	AccountHeads accountHeads;
	

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "salesTransHdr", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	SalesTransHdr salesTransHdr;
	

	  @ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "companyMst", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		CompanyMst companyMst;

	  @ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "receiptHdr", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		ReceiptHdr receiptHdr;
	  @Column(length = 50)
	  private String  processInstanceId;
	  @Column(length = 50)
		private String taskId;

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getReceiptNumber() {
		return receiptNumber;
	}


	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}


	public String getInvoiceNumber() {
		return invoiceNumber;
	}


	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}


	public Double getRecievedAmount() {
		return recievedAmount;
	}


	public void setRecievedAmount(Double recievedAmount) {
		this.recievedAmount = recievedAmount;
	}


	public Date getReceiptDate() {
		return receiptDate;
	}


	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}


	public Date getInvoiceDate() {
		return invoiceDate;
	}


	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}





	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}


	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public ReceiptHdr getReceiptHdr() {
		return receiptHdr;
	}
	public void setReceiptHdr(ReceiptHdr receiptHdr) {
		this.receiptHdr = receiptHdr;
	}
	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	public AccountHeads getAccountHeads() {
		return accountHeads;
	}


	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}


	@Override
	public String toString() {
		return "ReceiptInvoiceDtl [id=" + id + ", receiptNumber=" + receiptNumber + ", invoiceNumber=" + invoiceNumber
				+ ", recievedAmount=" + recievedAmount + ", receiptDate=" + receiptDate + ", invoiceDate=" + invoiceDate
				+ ", accountHeads=" + accountHeads + ", salesTransHdr=" + salesTransHdr + ", companyMst=" + companyMst
				+ ", receiptHdr=" + receiptHdr + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}


	

	
}
