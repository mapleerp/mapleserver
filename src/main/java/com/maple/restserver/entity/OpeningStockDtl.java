package com.maple.restserver.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class OpeningStockDtl implements Serializable  {
	
	

	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	
	private String id;
	@Column(length = 50)
	private String itemId;
	@Column(length = 50)
	private String batch ;
	@Column(length = 50)
	private String barcode;
	private Double qtyIn;
	private Double qtyOut;
	private Double mrp;
	@Column(length = 50)
	private String voucherNumber;
	private Date voucherDate;
	

	private Date expDate;
	@Column(length = 50)
	private String branchCode;
	@Column(length = 256)
	private String particulars;
	
	
	 @Column(nullable = false ,columnDefinition = "varchar(255) default 'MAIN'") 
	private String store;
	 
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	@UpdateTimestamp
	@Column(nullable=true)
	private LocalDateTime updatedTime;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getItemId() {
		return itemId;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	public String getBatch() {
		return batch;
	}


	public void setBatch(String batch) {
		this.batch = batch;
	}


	public String getBarcode() {
		return barcode;
	}


	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}


	public Double getQtyIn() {
		return qtyIn;
	}


	public void setQtyIn(Double qtyIn) {
		this.qtyIn = qtyIn;
	}


	public Double getQtyOut() {
		return qtyOut;
	}


	public void setQtyOut(Double qtyOut) {
		this.qtyOut = qtyOut;
	}


	public Double getMrp() {
		return mrp;
	}


	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}


	public String getVoucherNumber() {
		return voucherNumber;
	}


	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}


	public Date getVoucherDate() {
		return voucherDate;
	}


	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}


	public Date getExpDate() {
		return expDate;
	}


	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}


	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public String getParticulars() {
		return particulars;
	}


	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}


	public String getStore() {
		return store;
	}


	public void setStore(String store) {
		this.store = store;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}


	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "OpeningStockDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", barcode=" + barcode
				+ ", qtyIn=" + qtyIn + ", qtyOut=" + qtyOut + ", mrp=" + mrp + ", voucherNumber=" + voucherNumber
				+ ", voucherDate=" + voucherDate + ", expDate=" + expDate + ", branchCode=" + branchCode
				+ ", particulars=" + particulars + ", store=" + store + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", updatedTime=" + updatedTime
				+ "]";
	}


}
	
