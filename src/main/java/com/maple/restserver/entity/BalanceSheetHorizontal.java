package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
@Entity
public class BalanceSheetHorizontal extends MapleObject  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	
	
	@Column(length = 50)
	String financialYear;
	@Column(length = 50)
	String assetAccountId;
	@Column(length = 50)
	String liabilityAccountId;
	@Column(length = 50)
	Double assetAmount;
	Double liabilityAmount;
	@Column(length = 50)
	String slNo;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	 
	
	public String getSlNo() {
		return slNo;
	}

	public void setSlNo(String slNo) {
		this.slNo = slNo;
	}

 
	public String getFinancialYear() {
		return financialYear;
	}
	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

	public String getAssetAccountId() {
		return assetAccountId;
	}

	public void setAssetAccountId(String assetAccountId) {
		this.assetAccountId = assetAccountId;
	}

	public String getLiabilityAccountId() {
		return liabilityAccountId;
	}

	public void setLiabilityAccountId(String liabilityAccountId) {
		this.liabilityAccountId = liabilityAccountId;
	}

	public Double getAssetAmount() {
		return assetAmount;
	}

	public void setAssetAmount(Double assetAmount) {
		this.assetAmount = assetAmount;
	}

	public Double getLiabilityAmount() {
		return liabilityAmount;
	}

	public void setLiabilityAmount(Double liabilityAmount) {
		this.liabilityAmount = liabilityAmount;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "BalanceSheetHorizontal [id=" + getId() + ", financialYear=" + financialYear + ", assetAccountId="
				+ assetAccountId + ", liabilityAccountId=" + liabilityAccountId + ", assetAmount=" + assetAmount
				+ ", liabilityAmount=" + liabilityAmount + ", slNo=" + slNo + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	


}
