package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
@Entity
public class ServerFailureStatusMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	Date dateOfRetry;
	String description;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getDateOfRetry() {
		return dateOfRetry;
	}
	public void setDateOfRetry(Date dateOfRetry) {
		this.dateOfRetry = dateOfRetry;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "ServerFailureStatusMst [id=" + id + ", dateOfRetry=" + dateOfRetry + ", description=" + description
				+ "]";
	}
	
	
	
	
	
}
