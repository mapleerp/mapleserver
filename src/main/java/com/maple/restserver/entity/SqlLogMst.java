package com.maple.restserver.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.repository.Modifying;

@Entity
public class SqlLogMst {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(length = 50)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	String sqlMsg;
	@UpdateTimestamp
	@Column(nullable=true)
	 LocalDateTime updatedTime;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getSqlMsg() {
		return sqlMsg;
	}
	public void setSqlMsg(String sqlMsg) {
		this.sqlMsg = sqlMsg;
	}
	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SqlLogMst [id=" + id + ", sqlMsg=" + sqlMsg + ", updatedTime=" + updatedTime + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

}
