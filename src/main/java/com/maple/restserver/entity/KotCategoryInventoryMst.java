package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class KotCategoryInventoryMst implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	private String id;
	
	@Column(length = 50)
	private String processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	private String parentItem;
	
	private String childItem;
	
	private String leaf;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getParentItem() {
		return parentItem;
	}

	public void setParentItem(String parentItem) {
		this.parentItem = parentItem;
	}

	public String getChildItem() {
		return childItem;
	}

	public void setChildItem(String childItem) {
		this.childItem = childItem;
	}

	public String getLeaf() {
		return leaf;
	}

	public void setLeaf(String leaf) {
		this.leaf = leaf;
	}

	@Override
	public String toString() {
		return "KotCategoryInventoryMst [id=" + id + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", companyMst=" + companyMst + ", parentItem=" + parentItem + ", childItem=" + childItem + ", leaf="
				+ leaf + "]";
	}
	
	
}
