package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
@Entity
public class StockTransferInExceptionDtl implements Serializable  {

	@Id
	 @GeneratedValue(generator = "uuid")

  @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
	
   private String id;
	@Column(length = 50)
	private String batch;
	private Double qty;
	private Double rate;
	@Column(length = 50)
	private String itemCode;
	private Double taxRate;
	private Date expiryDate;
	private Double amount;
	@Column(length = 50)
	private String unitId;
	@Column(length = 50)
	private String itemId;
	private Double mrp;
	@Column(length = 50)
	private String barcode;
	@Column(length = 50)
	String processStatus;

	String exceptionReason;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;

	
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "stockTransferInExceptionHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	StockTransferInExceptionHdr stockTransferInExceptionHdr;
	
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getProcessStatus() {
		return processStatus;
	}
	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public StockTransferInExceptionHdr getStockTransferInExceptionHdr() {
		return stockTransferInExceptionHdr;
	}
	public void setStockTransferInExceptionHdr(StockTransferInExceptionHdr stockTransferInExceptionHdr) {
		this.stockTransferInExceptionHdr = stockTransferInExceptionHdr;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getExceptionReason() {
		return exceptionReason;
	}
	public void setExceptionReason(String exceptionReason) {
		this.exceptionReason = exceptionReason;
	}
	@Override
	public String toString() {
		return "StockTransferInExceptionDtl [id=" + id + ", batch=" + batch + ", qty=" + qty + ", rate=" + rate
				+ ", itemCode=" + itemCode + ", taxRate=" + taxRate + ", expiryDate=" + expiryDate + ", amount="
				+ amount + ", unitId=" + unitId + ", itemId=" + itemId + ", mrp=" + mrp + ", barcode=" + barcode
				+ ", processStatus=" + processStatus + ", exceptionReason=" + exceptionReason + ", companyMst="
				+ companyMst + ", stockTransferInExceptionHdr=" + stockTransferInExceptionHdr + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
	
	
}
