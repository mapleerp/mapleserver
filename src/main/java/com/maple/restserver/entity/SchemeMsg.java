package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Entity;

import org.springframework.stereotype.Component;
 
 
public class SchemeMsg implements Serializable {
private static final long serialVersionUID = 1L;
	
	ArrayList <SchEligiAttrListDef>SchEligiAttrListDef=new ArrayList();
	ArrayList <SchEligibilityAttribInst>SchEligibilityAttribInst=new ArrayList();
	ArrayList <SchEligibilityDef>SchEligibilityDef=new ArrayList();
	
	ArrayList <SchemeInstance>SchemeInstance=new ArrayList();
	
	ArrayList <SchOfferAttrInst>SchOfferAttrInst=new ArrayList();
	
	ArrayList <SchOfferAttrListDef>SchOfferAttrListDef=new ArrayList();
	ArrayList <SchOfferDef>SchOfferDef=new ArrayList();
	
	ArrayList <SchSelectAttrListDef>SchSelectAttrListDef=new ArrayList();
	
	ArrayList <SchSelectDef>SchSelectDef=new ArrayList();
	
	ArrayList <SchSelectionAttribInst>SchSelectionAttribInst=new ArrayList();

	public ArrayList<SchEligiAttrListDef> getSchEligiAttrListDef() {
		return SchEligiAttrListDef;
	}

	public void setSchEligiAttrListDef(ArrayList<SchEligiAttrListDef> schEligiAttrListDef) {
		SchEligiAttrListDef = schEligiAttrListDef;
	}

	public ArrayList<SchEligibilityAttribInst> getSchEligibilityAttribInst() {
		return SchEligibilityAttribInst;
	}

	public void setSchEligibilityAttribInst(ArrayList<SchEligibilityAttribInst> schEligibilityAttribInst) {
		SchEligibilityAttribInst = schEligibilityAttribInst;
	}

	public ArrayList<SchEligibilityDef> getSchEligibilityDef() {
		return SchEligibilityDef;
	}

	public void setSchEligibilityDef(ArrayList<SchEligibilityDef> schEligibilityDef) {
		SchEligibilityDef = schEligibilityDef;
	}

	public ArrayList<SchemeInstance> getSchemeInstance() {
		return SchemeInstance;
	}

	public void setSchemeInstance(ArrayList<SchemeInstance> schemeInstance) {
		SchemeInstance = schemeInstance;
	}

	public ArrayList<SchOfferAttrInst> getSchOfferAttrInst() {
		return SchOfferAttrInst;
	}

	public void setSchOfferAttrInst(ArrayList<SchOfferAttrInst> schOfferAttrInst) {
		SchOfferAttrInst = schOfferAttrInst;
	}

	public ArrayList<SchOfferAttrListDef> getSchOfferAttrListDef() {
		return SchOfferAttrListDef;
	}

	public void setSchOfferAttrListDef(ArrayList<SchOfferAttrListDef> schOfferAttrListDef) {
		SchOfferAttrListDef = schOfferAttrListDef;
	}

	public ArrayList<SchOfferDef> getSchOfferDef() {
		return SchOfferDef;
	}

	public void setSchOfferDef(ArrayList<SchOfferDef> schOfferDef) {
		SchOfferDef = schOfferDef;
	}

	public ArrayList<SchSelectAttrListDef> getSchSelectAttrListDef() {
		return SchSelectAttrListDef;
	}

	public void setSchSelectAttrListDef(ArrayList<SchSelectAttrListDef> schSelectAttrListDef) {
		SchSelectAttrListDef = schSelectAttrListDef;
	}

	public ArrayList<SchSelectDef> getSchSelectDef() {
		return SchSelectDef;
	}

	public void setSchSelectDef(ArrayList<SchSelectDef> schSelectDef) {
		SchSelectDef = schSelectDef;
	}

	public ArrayList<SchSelectionAttribInst> getSchSelectionAttribInst() {
		return SchSelectionAttribInst;
	}

	public void setSchSelectionAttribInst(ArrayList<SchSelectionAttribInst> schSelectionAttribInst) {
		SchSelectionAttribInst = schSelectionAttribInst;
	}
	
	
	
	
}
