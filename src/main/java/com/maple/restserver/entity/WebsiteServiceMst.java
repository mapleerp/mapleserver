package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class WebsiteServiceMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(length = 50)
    private String id;
	
	 private String profiletextstrong1;
	 private String  servicesp1;
	 private String iconclass;
	public String getId() {
		return id;
	}
	public String getProfiletextstrong1() {
		return profiletextstrong1;
	}
	public String getServicesp1() {
		return servicesp1;
	}
	public String getIconclass() {
		return iconclass;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setProfiletextstrong1(String profiletextstrong1) {
		this.profiletextstrong1 = profiletextstrong1;
	}
	public void setServicesp1(String servicesp1) {
		this.servicesp1 = servicesp1;
	}
	public void setIconclass(String iconclass) {
		this.iconclass = iconclass;
	}
	@Override
	public String toString() {
		return "WebsiteServiceMst [id=" + id + ", profiletextstrong1=" + profiletextstrong1 + ", servicesp1="
				+ servicesp1 + ", iconclass=" + iconclass + "]";
	}
	 
	 

}
