package com.maple.restserver.entity;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Entity
public class TableOccupiedMst {
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	String id;
	@Column(length = 50)
	String tableId;
	Timestamp occupiedTime;
	Timestamp orderedTime;
	Timestamp billingTime;
	Timestamp deliveryTime;
	Timestamp kitchenReadyTime;
	Double invoiceAmount;
	@Column(length = 50)
	String waiterId;
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "salesTransHdr", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private SalesTransHdr salesTransHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getTableId() {
		return tableId;
	}


	public void setTableId(String tableId) {
		this.tableId = tableId;
	}


	public Timestamp getOccupiedTime() {
		return occupiedTime;
	}


	public void setOccupiedTime(Timestamp occupiedTime) {
		this.occupiedTime = occupiedTime;
	}


	public Timestamp getOrderedTime() {
		return orderedTime;
	}


	public void setOrderedTime(Timestamp orderedTime) {
		this.orderedTime = orderedTime;
	}


	public Timestamp getBillingTime() {
		return billingTime;
	}


	public void setBillingTime(Timestamp billingTime) {
		this.billingTime = billingTime;
	}


	public Timestamp getDeliveryTime() {
		return deliveryTime;
	}


	public void setDeliveryTime(Timestamp deliveryTime) {
		this.deliveryTime = deliveryTime;
	}


	public Timestamp getKitchenReadyTime() {
		return kitchenReadyTime;
	}


	public void setKitchenReadyTime(Timestamp kitchenReadyTime) {
		this.kitchenReadyTime = kitchenReadyTime;
	}


	public Double getInvoiceAmount() {
		return invoiceAmount;
	}


	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}


	public String getWaiterId() {
		return waiterId;
	}


	public void setWaiterId(String waiterId) {
		this.waiterId = waiterId;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}


	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}




	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "TableOccupiedMst [id=" + id + ", tableId=" + tableId + ", occupiedTime=" + occupiedTime
				+ ", orderedTime=" + orderedTime + ", billingTime=" + billingTime + ", deliveryTime=" + deliveryTime
				+ ", kitchenReadyTime=" + kitchenReadyTime + ", invoiceAmount=" + invoiceAmount + ", waiterId="
				+ waiterId + ", companyMst=" + companyMst + ", salesTransHdr=" + salesTransHdr + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
