package com.maple.restserver.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class CustomerServiceStatusDtl {

	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	@Column(length = 50)
	private String jobCardNumber;
	@Column(length = 50)
	private String locationId;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "customerServiceStatusHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CustomerServiceStatusHdr customerServiceStatusHdr;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getJobCardNumber() {
		return jobCardNumber;
	}

	public void setJobCardNumber(String jobCardNumber) {
		this.jobCardNumber = jobCardNumber;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public CustomerServiceStatusHdr getCustomerServiceStatusHdr() {
		return customerServiceStatusHdr;
	}

	public void setCustomerServiceStatusHdr(CustomerServiceStatusHdr customerServiceStatusHdr) {
		this.customerServiceStatusHdr = customerServiceStatusHdr;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "CustomerServiceStatusDtl [id=" + id + ", jobCardNumber=" + jobCardNumber + ", locationId=" + locationId
				+ ", companyMst=" + companyMst + ", customerServiceStatusHdr=" + customerServiceStatusHdr
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
