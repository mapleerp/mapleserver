package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class ItemMst implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	private String id;

	@Column(unique = true, nullable = false)
	private String itemName;

	@Column(nullable = false)
	private String unitId;
	@Column(length = 50)
	private String oldId;
	@Column(length = 50)
	private String processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	private Double standardPrice;

	private String barCode;

	private String branchCode;

	private String machineId;

	private String itemGroupId;

	private String itemGenericName;

	private String itemCode;

	private String binNo;

	private Double itemDiscount;

	private String itemPriceLock;
	private String itemDtl;
	@Column(nullable = false, columnDefinition = "integer default 0")
	private Integer bestBefore;

	private String barCodeLine1;

	private String barCodeLine2;

	private String supplierId;

	private Double taxRate;

	private Double sgst;

	private Double cgst;

	
	@Column(nullable = true, columnDefinition = "varchar(25)")
	private String hsnCode;

	
	@Column(nullable = true, columnDefinition = "varchar(20)")
	private String isDeleted;

	private Integer reorderWaitingPeriod;

	private Double cess;

	@Column(nullable = true, columnDefinition = "varchar(50)")
	private String categoryId;

	private int isKit;

	private String productId;

	private String brandId;
	private String model;

	private String barcodeFormat;

	private String itemDescription;
	
	private String shortDescription;

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	@Column(nullable = false, columnDefinition = "varchar(20) default 'ITEM MST'")
	private String serviceOrGoods;

	// @Column( nullable = false, columnDefinition = "Integer default 0")
	private Integer itemRank;
	@Column(nullable = true)
	String netWeight;

	/*
	 * @ManyToOne(fetch = FetchType.EAGER, optional = false)
	 * 
	 * @JoinColumn(name = "categoryMst", nullable = false)
	 * 
	 * @OnDelete(action = OnDeleteAction.CASCADE) CategoryMst categoryMst;
	 * 
	 */

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;

	private String negativeBillLock;

	@Column(nullable = true, columnDefinition = "varchar(25)")
	private String nutrition1;
	@Column(nullable = true, columnDefinition = "varchar(25)")
	private String nutrition2;
	
	@Column(nullable = true, columnDefinition = "varchar(25)")
	private String nutrition3;
	@Column(nullable = true, columnDefinition = "varchar(25)")

	private String nutrition4;
	@Column(nullable = true, columnDefinition = "varchar(25)")

	private String nutrition5;
	@Column(nullable = true, columnDefinition = "varchar(25)")

	private String nutrition6;
	@Column(nullable = true, columnDefinition = "varchar(25)")

	private String nutrition7;
	@Column(nullable = true, columnDefinition = "varchar(25)")

	private String nutrition8;
	@Column(nullable = true, columnDefinition = "varchar(25)")

	private String nutrition9;
	@Column(nullable = true, columnDefinition = "varchar(25)")

	private String nutrition10;
	@Column(nullable = true, columnDefinition = "varchar(25)")

	private String nutrition11;
	@Column(nullable = true, columnDefinition = "varchar(25)")

	private String nutrition12;
	@Column(nullable = true, columnDefinition = "varchar(25)")

	private String nutrition13;
	@Column(nullable = true, columnDefinition = "varchar(25)")

	private String nutrition14;

	public Integer getReorderWaitingPeriod() {
		return reorderWaitingPeriod;
	}

	public void setReorderWaitingPeriod(Integer reorderWaitingPeriod) {
		this.reorderWaitingPeriod = reorderWaitingPeriod;
	}

	public String getId() {
		return id;
	}

	public String getNetWeight() {
		return netWeight;
	}

	public String getItemPriceLock() {
		return itemPriceLock;
	}

	public void setItemPriceLock(String itemPriceLock) {
		this.itemPriceLock = itemPriceLock;
	}

	public void setNetWeight(String netWeight) {
		this.netWeight = netWeight;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public Double getStandardPrice() {
		return standardPrice;
	}

	public Integer getRank() {
		return itemRank;
	}

	public void setRank(Integer rank) {
		this.itemRank = rank;
	}

	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getItemGroupId() {
		return itemGroupId;
	}

	public void setItemGroupId(String itemGroupId) {
		this.itemGroupId = itemGroupId;
	}

	public String getItemGenericName() {
		return itemGenericName;
	}

	public void setItemGenericName(String itemGenericName) {
		this.itemGenericName = itemGenericName;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getBinNo() {
		return binNo;
	}

	public void setBinNo(String binNo) {
		this.binNo = binNo;
	}

	public String getItemDtl() {
		return itemDtl;
	}

	public void setItemDtl(String itemDtl) {
		this.itemDtl = itemDtl;
	}

	public Integer getBestBefore() {
		return bestBefore;
	}

	public void setBestBefore(Integer bestBefore) {
		this.bestBefore = bestBefore;
	}

	public String getBarCodeLine1() {
		return barCodeLine1;
	}

	public void setBarCodeLine1(String barCodeLine1) {
		this.barCodeLine1 = barCodeLine1;
	}

	public String getBarCodeLine2() {
		return barCodeLine2;
	}

	public void setBarCodeLine2(String barCodeLine2) {
		this.barCodeLine2 = barCodeLine2;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getSgst() {
		return sgst;
	}

	public void setSgst(Double sgst) {
		this.sgst = sgst;
	}

	public Double getCgst() {
		return cgst;
	}

	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsmCode) {
		this.hsnCode = hsmCode;
	}

	public Double getItemDiscount() {
		return itemDiscount;
	}

	public void setItemDiscount(Double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Double getCess() {
		return cess;
	}

	public void setCess(Double cess) {
		this.cess = cess;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public int getIsKit() {
		return isKit;
	}

	public void setIsKit(int isKit) {
		this.isKit = isKit;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getServiceOrGoods() {
		return serviceOrGoods;
	}

	public void setServiceOrGoods(String serviceOrGoods) {
		this.serviceOrGoods = serviceOrGoods;
	}

	public String getNegativeBillLock() {
		return negativeBillLock;
	}

	public void setNegativeBillLock(String negativeBillLock) {
		this.negativeBillLock = negativeBillLock;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getBarcodeFormat() {
		return barcodeFormat;
	}

	public void setBarcodeFormat(String barcodeFormat) {
		this.barcodeFormat = barcodeFormat;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getNutrition1() {
		return nutrition1;
	}

	public void setNutrition1(String nutrition1) {
		this.nutrition1 = nutrition1;
	}

	public String getNutrition2() {
		return nutrition2;
	}

	public void setNutrition2(String nutrition2) {
		this.nutrition2 = nutrition2;
	}

	public String getNutrition3() {
		return nutrition3;
	}

	public void setNutrition3(String nutrition3) {
		this.nutrition3 = nutrition3;
	}

	public String getNutrition4() {
		return nutrition4;
	}

	public void setNutrition4(String nutrition4) {
		this.nutrition4 = nutrition4;
	}

	public String getNutrition5() {
		return nutrition5;
	}

	public void setNutrition5(String nutrition5) {
		this.nutrition5 = nutrition5;
	}

	public String getNutrition6() {
		return nutrition6;
	}

	public void setNutrition6(String nutrition6) {
		this.nutrition6 = nutrition6;
	}

	public String getNutrition7() {
		return nutrition7;
	}

	public void setNutrition7(String nutrition7) {
		this.nutrition7 = nutrition7;
	}

	public String getNutrition8() {
		return nutrition8;
	}

	public void setNutrition8(String nutrition8) {
		this.nutrition8 = nutrition8;
	}

	public String getNutrition9() {
		return nutrition9;
	}

	public void setNutrition9(String nutrition9) {
		this.nutrition9 = nutrition9;
	}

	public String getNutrition10() {
		return nutrition10;
	}

	public void setNutrition10(String nutrition10) {
		this.nutrition10 = nutrition10;
	}

	public String getNutrition11() {
		return nutrition11;
	}

	public void setNutrition11(String nutrition11) {
		this.nutrition11 = nutrition11;
	}

	public String getNutrition12() {
		return nutrition12;
	}

	public void setNutrition12(String nutrition12) {
		this.nutrition12 = nutrition12;
	}

	public String getNutrition13() {
		return nutrition13;
	}

	public void setNutrition13(String nutrition13) {
		this.nutrition13 = nutrition13;
	}

	public String getNutrition14() {
		return nutrition14;
	}

	public void setNutrition14(String nutrition14) {
		this.nutrition14 = nutrition14;
	}

	@Override
	public String toString() {
		return "ItemMst [id=" + id + ", itemName=" + itemName + ", unitId=" + unitId + ", oldId=" + oldId
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", standardPrice=" + standardPrice
				+ ", barCode=" + barCode + ", branchCode=" + branchCode + ", machineId=" + machineId + ", itemGroupId="
				+ itemGroupId + ", itemGenericName=" + itemGenericName + ", itemCode=" + itemCode + ", binNo=" + binNo
				+ ", itemDiscount=" + itemDiscount + ", itemPriceLock=" + itemPriceLock + ", itemDtl=" + itemDtl
				+ ", bestBefore=" + bestBefore + ", barCodeLine1=" + barCodeLine1 + ", barCodeLine2=" + barCodeLine2
				+ ", supplierId=" + supplierId + ", taxRate=" + taxRate + ", sgst=" + sgst + ", cgst=" + cgst
				+ ", hsnCode=" + hsnCode + ", isDeleted=" + isDeleted + ", reorderWaitingPeriod=" + reorderWaitingPeriod
				+ ", cess=" + cess + ", categoryId=" + categoryId + ", isKit=" + isKit + ", productId=" + productId
				+ ", brandId=" + brandId + ", model=" + model + ", barcodeFormat=" + barcodeFormat
				+ ", itemDescription=" + itemDescription + ", shortDescription=" + shortDescription
				+ ", serviceOrGoods=" + serviceOrGoods + ", itemRank=" + itemRank + ", netWeight=" + netWeight
				+ ", companyMst=" + companyMst + ", negativeBillLock=" + negativeBillLock + ", nutrition1=" + nutrition1
				+ ", nutrition2=" + nutrition2 + ", nutrition3=" + nutrition3 + ", nutrition4=" + nutrition4
				+ ", nutrition5=" + nutrition5 + ", nutrition6=" + nutrition6 + ", nutrition7=" + nutrition7
				+ ", nutrition8=" + nutrition8 + ", nutrition9=" + nutrition9 + ", nutrition10=" + nutrition10
				+ ", nutrition11=" + nutrition11 + ", nutrition12=" + nutrition12 + ", nutrition13=" + nutrition13
				+ ", nutrition14=" + nutrition14 + "]";
	}

	
	 
}
