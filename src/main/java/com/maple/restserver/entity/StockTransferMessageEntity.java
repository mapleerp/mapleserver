package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.message.entity.SalesDtlMessage;
import com.maple.restserver.message.entity.SalesTransHdrMessage;
@Component
public class StockTransferMessageEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	StockTransferOutHdr stockTransferOutHdr;
	ArrayList<StockTransferOutDtl> stockTransferOutDtlList =new ArrayList();
	ArrayList<ItemMst> itemmstList =new ArrayList();
	ArrayList<CategoryMst> categoryList = new ArrayList();
	ArrayList<PriceDefinition> priceDefinitionList = new ArrayList();
	ArrayList<PriceDefenitionMst> priceDefinitionMstList  = new ArrayList();
	ArrayList<UnitMst> unitMstList = new ArrayList();
	
	public ArrayList<UnitMst> getUnitMstList() {
		return unitMstList;
	}
	public void setUnitMstList(ArrayList<UnitMst> unitMstList) {
		this.unitMstList = unitMstList;
	}
	BranchMst branchMst;
	Integer itemCount;
	
	public ArrayList<PriceDefenitionMst> getPriceDefinitionMstList() {
		return priceDefinitionMstList;
	}
	public void setPriceDefinitionMstList(ArrayList<PriceDefenitionMst> priceDefinitionMstList) {
		this.priceDefinitionMstList = priceDefinitionMstList;
	}
	public StockTransferOutHdr getStockTransferOutHdr() {
		return stockTransferOutHdr;
	}
	public void setStockTransferOutHdr(StockTransferOutHdr stockTransferOutHdr) {
		this.stockTransferOutHdr = stockTransferOutHdr;
	}
	public ArrayList<StockTransferOutDtl> getStockTransferOutDtlList() {
		return stockTransferOutDtlList;
	}
	public void setStockTransferOutDtlList(ArrayList<StockTransferOutDtl> stockTransferOutDtlList) {
		this.stockTransferOutDtlList = stockTransferOutDtlList;
	}
	public ArrayList<ItemMst> getItemmstList() {
		return itemmstList;
	}
	public void setItemmstList(ArrayList<ItemMst> itemmstList) {
		this.itemmstList = itemmstList;
	}
	public ArrayList<CategoryMst> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(ArrayList<CategoryMst> categoryList) {
		this.categoryList = categoryList;
	}
	public ArrayList<PriceDefinition> getPriceDefinitionList() {
		return priceDefinitionList;
	}
	public void setPriceDefinitionList(ArrayList<PriceDefinition> priceDefinitionList) {
		this.priceDefinitionList = priceDefinitionList;
	}
	public BranchMst getBranchMst() {
		return branchMst;
	}
	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}
	public Integer getItemCount() {
		return itemCount;
	}
	public void setItemCount(Integer itemCount) {
		this.itemCount = itemCount;
	}
	
	
}
