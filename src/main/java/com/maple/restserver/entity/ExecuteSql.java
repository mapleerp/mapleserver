package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
public class ExecuteSql implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)
	@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	 
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	
	@Column(length = 1024)
  String sqlToExecute;
	@Column(length = 50)
  String brachCode;
  
  boolean executeInServer;

public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

public CompanyMst getCompanyMst() {
	return companyMst;
}

public void setCompanyMst(CompanyMst companyMst) {
	this.companyMst = companyMst;
}

public String getSqlToExecute() {
	return sqlToExecute;
}

public void setSqlToExecute(String sqlToExecute) {
	this.sqlToExecute = sqlToExecute;
}

public String getBrachCode() {
	return brachCode;
}

public void setBrachCode(String brachCode) {
	this.brachCode = brachCode;
}

public boolean isExecuteInServer() {
	return executeInServer;
}

public void setExecuteInServer(boolean executeInServer) {
	this.executeInServer = executeInServer;
}

public String getProcessInstanceId() {
	return processInstanceId;
}

public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}

public String getTaskId() {
	return taskId;
}

public void setTaskId(String taskId) {
	this.taskId = taskId;
}

@Override
public String toString() {
	return "ExecuteSql [id=" + id + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId
			+ ", taskId=" + taskId + ", sqlToExecute=" + sqlToExecute + ", brachCode=" + brachCode
			+ ", executeInServer=" + executeInServer + "]";
}
  
	
	

}
