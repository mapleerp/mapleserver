package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class MonthlySalesDtl implements Serializable {

	
	
	
private static final long serialVersionUID = 1L;
	
@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	 
	@Column(nullable=false)
	String itemId;
	Double qty;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double rate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double cgstTaxRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double sgstTaxRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double cessRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double addCessRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double igstTaxRate;
	 
	String itemTaxaxId;
	@Column(nullable=false)
	String unitId;
	String itemName;
	Date expiryDate;
	@Column(nullable=false)
	String batch;
	String barcode;

	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double taxRate;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double mrp;

	@Column(  nullable = false, columnDefinition = "double default 0.0") 
	Double amount;
	String unitName;
	 @Column(  nullable = false, columnDefinition = "double default 0.0") 
    Double discount;
    @Column(  nullable = false, columnDefinition = "double default 0.0") 
    Double cgstAmount;
    @Column(  nullable = false, columnDefinition = "double default 0.0") 
    Double sgstAmount;
    @Column(  nullable = false, columnDefinition = "double default 0.0") 
    Double igstAmount;
    
    @Column( nullable = false, columnDefinition = "double default 0.0") 
    Double cessAmount;
    Double returnedQty;
    String status;
    Double addCessAmount;
    Double costPrice;
    String printKotStatus;
    

	 	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "monthly_sales_trans_hdr_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private MonthlySalesTransHdr monthlySalesTransHdr;


	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	@Column(length = 50)
	String offerReferenceId;
	@Column(length = 50)
	String schemeId;
	 
	Double standardPrice;
	 
	String fbKey;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}

	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}

	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}

	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	public Double getCessRate() {
		return cessRate;
	}

	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}

	public Double getAddCessRate() {
		return addCessRate;
	}

	public void setAddCessRate(Double addCessRate) {
		this.addCessRate = addCessRate;
	}

	public Double getIgstTaxRate() {
		return igstTaxRate;
	}

	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}

	public String getItemTaxaxId() {
		return itemTaxaxId;
	}

	public void setItemTaxaxId(String itemTaxaxId) {
		this.itemTaxaxId = itemTaxaxId;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getCgstAmount() {
		return cgstAmount;
	}

	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}

	public Double getSgstAmount() {
		return sgstAmount;
	}

	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}

	public Double getIgstAmount() {
		return igstAmount;
	}

	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}

	public Double getCessAmount() {
		return cessAmount;
	}

	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}

	public Double getReturnedQty() {
		return returnedQty;
	}

	public void setReturnedQty(Double returnedQty) {
		this.returnedQty = returnedQty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getAddCessAmount() {
		return addCessAmount;
	}

	public void setAddCessAmount(Double addCessAmount) {
		this.addCessAmount = addCessAmount;
	}

	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	public String getPrintKotStatus() {
		return printKotStatus;
	}

	public void setPrintKotStatus(String printKotStatus) {
		this.printKotStatus = printKotStatus;
	}


	

	public MonthlySalesTransHdr getMonthlySalesTransHdr() {
		return monthlySalesTransHdr;
	}

	public void setMonthlySalesTransHdr(MonthlySalesTransHdr monthlySalesTransHdr) {
		this.monthlySalesTransHdr = monthlySalesTransHdr;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getOfferReferenceId() {
		return offerReferenceId;
	}

	public void setOfferReferenceId(String offerReferenceId) {
		this.offerReferenceId = offerReferenceId;
	}

	public String getSchemeId() {
		return schemeId;
	}

	public void setSchemeId(String schemeId) {
		this.schemeId = schemeId;
	}

	public Double getStandardPrice() {
		return standardPrice;
	}

	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public String getFbKey() {
		return fbKey;
	}

	public void setFbKey(String fbKey) {
		this.fbKey = fbKey;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "MonthlySalesDtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", rate=" + rate + ", cgstTaxRate="
				+ cgstTaxRate + ", sgstTaxRate=" + sgstTaxRate + ", cessRate=" + cessRate + ", addCessRate="
				+ addCessRate + ", igstTaxRate=" + igstTaxRate + ", itemTaxaxId=" + itemTaxaxId + ", unitId=" + unitId
				+ ", itemName=" + itemName + ", expiryDate=" + expiryDate + ", batch=" + batch + ", barcode=" + barcode
				+ ", taxRate=" + taxRate + ", mrp=" + mrp + ", amount=" + amount + ", unitName=" + unitName
				+ ", discount=" + discount + ", cgstAmount=" + cgstAmount + ", sgstAmount=" + sgstAmount
				+ ", igstAmount=" + igstAmount + ", cessAmount=" + cessAmount + ", returnedQty=" + returnedQty
				+ ", status=" + status + ", addCessAmount=" + addCessAmount + ", costPrice=" + costPrice
				+ ", printKotStatus=" + printKotStatus + ", monthlySalesTransHdr=" + monthlySalesTransHdr
				+ ", companyMst=" + companyMst + ", offerReferenceId=" + offerReferenceId + ", schemeId=" + schemeId
				+ ", standardPrice=" + standardPrice + ", fbKey=" + fbKey + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}

	

	 
	
	
}
