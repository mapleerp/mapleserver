package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Entity
public class AccountReceivable implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(length = 50)

	@Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   private String id;
	
	 
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "accountHeads", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private AccountHeads accountHeads;
	
	
	@OneToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "salesTransHdr", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private SalesTransHdr salesTransHdr;
	
	
	
	SalesOrderTransHdr salesOrderTransHdr;
	@Column(length = 50)

	String voucherNumber;
	
	Date voucherDate;
	Date dueDatesql;
	Date dueDate;
	@Column(length = 50)

	String accountId;
	 
	String remark;
	Double dueAmount;
	Double paidAmount;
	@Column(length = 50)

	private String  processInstanceId;
	
	@Column(length = 50)

	private String taskId;
	
 

	public Date getDueDatesql() {
		return dueDatesql;
	}



	public void setDueDatesql(Date dueDatesql) {
		this.dueDatesql = dueDatesql;
	}



	public String getId() {
		return id;
	}



	public SalesOrderTransHdr getSalesOrderTransHdr() {
		return salesOrderTransHdr;
	}



	public void setSalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr) {
		this.salesOrderTransHdr = salesOrderTransHdr;
	}



	public void setId(String id) {
		this.id = id;
	}



	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}







	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}



	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}



	public String getVoucherNumber() {
		return voucherNumber;
	}



	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}



	public Date getVoucherDate() {
		return voucherDate;
	}



	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}



	public Date getDueDate() {
		return dueDate;
	}



	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}



	public String getAccountId() {
		return accountId;
	}



	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}



	public String getRemark() {
		return remark;
	}



	public void setRemark(String remark) {
		this.remark = remark;
	}



	public Double getDueAmount() {
		return dueAmount;
	}



	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}



	public Double getPaidAmount() {
		return paidAmount;
	}



	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	public AccountHeads getAccountHeads() {
		return accountHeads;
	}



	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}



	@Override
	public String toString() {
		return "AccountReceivable [id=" + id + ", companyMst=" + companyMst + ", accountHeads=" + accountHeads
				+ ", salesTransHdr=" + salesTransHdr + ", salesOrderTransHdr=" + salesOrderTransHdr + ", voucherNumber="
				+ voucherNumber + ", voucherDate=" + voucherDate + ", dueDatesql=" + dueDatesql + ", dueDate=" + dueDate
				+ ", accountId=" + accountId + ", remark=" + remark + ", dueAmount=" + dueAmount + ", paidAmount="
				+ paidAmount + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}





	

	 


	
	
	
	

}
