package com.maple.restserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
@Entity
public class QuoteTransHdr implements Serializable {

private static final long serialVersionUID = 1L;
@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
@Column(length = 50)
	String userId;
	
	

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	String godownId;
	@Column(length = 50)
	String branchCode;
	@Column(length = 50)
	String financialYear;
	@Column(length = 50)
	String invoiceTypeId;
	@Column(length = 50)
	String customerTypeId;
	
	Double advacedAmount;
	@Column(length = 50)
	String patientId;
	@Column(length = 50)
	String salesManId;
	@Column(length = 50)
	String doctorId;
	@Column(length = 50)
	String approveDate;
	@Column(length = 50)
    String machineId;
	Double materializeAmount;
	String materilizeReason;
	@Column(length = 50)
	String transferToBranch;
	@Column(length = 50)
	String customerApproved;
	@Column(length = 50)
	String transferFromBranch;
	String materilizeStatus;
	@Column(length = 50)
	String printDone;
	String printReason;
	@Column(length = 50)
	String printUserId;
	@Column(length = 50)
	String bookingBankAccount;
	Date bookingChequeDate;
    Date expectedDate;
    Double bookingCashAmount;
    @Column(length = 50)
	String chequeBankAccount;
	
	
	
	
	
		
	
	
}
