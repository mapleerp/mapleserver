package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class ClientStatusDtl implements Serializable{
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "clientStatusHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private ClientStatusHdr ClientStatusHdr;
	
	
	
	@Column(length = 5000)
	private String resultString;
	
	private String branchCode;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getResultString() {
		return resultString;
	}

	public void setResultString(String resultString) {
		this.resultString = resultString;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public ClientStatusHdr getClientStatusHdr() {
		return ClientStatusHdr;
	}

	public void setClientStatusHdr(ClientStatusHdr clientStatusHdr) {
		ClientStatusHdr = clientStatusHdr;
	}
	
	

	
	
}
