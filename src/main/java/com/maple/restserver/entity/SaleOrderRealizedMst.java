package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class SaleOrderRealizedMst implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "salesTransHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private SalesTransHdr salesTransHdr;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "salesOrderTransHdr", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private SalesOrderTransHdr salesOrderTransHdr;
	
	Double advanceAmount;
	Double realizedAmount;
	Double invoiceTotal;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}
	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}
	public SalesOrderTransHdr getSalesOrderTransHdr() {
		return salesOrderTransHdr;
	}
	public void setSalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr) {
		this.salesOrderTransHdr = salesOrderTransHdr;
	}
	
	public Double getAdvanceAmount() {
		return advanceAmount;
	}
	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}
	public Double getRealizedAmount() {
		return realizedAmount;
	}
	public void setRealizedAmount(Double realizedAmount) {
		this.realizedAmount = realizedAmount;
	}
	public Double getInvoiceTotal() {
		return invoiceTotal;
	}
	public void setInvoiceTotal(Double invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SaleOrderRealizedMst [id=" + id + ", companyMst=" + companyMst + ", salesTransHdr=" + salesTransHdr
				+ ", salesOrderTransHdr=" + salesOrderTransHdr + ", advanceAmount=" + advanceAmount
				+ ", realizedAmount=" + realizedAmount + ", invoiceTotal=" + invoiceTotal + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	

	

}
