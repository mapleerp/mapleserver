package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class ProductConversionConfigMst implements Serializable{
	@Id
	@Column(length = 50)
	String id;
	
	@Column(length = 50)
	String fromItemId;
	@Column(length = 50)
	String toItemId;
	@Column(length = 50)
	String branchCode;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	String oldId;
	
	
	public String getOldId() {
		return oldId;
	}
	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFromItemId() {
		return fromItemId;
	}
	public void setFromItemId(String fromItemId) {
		this.fromItemId = fromItemId;
	}
	public String getToItemId() {
		return toItemId;
	}
	public void setToItemId(String toItemId) {
		this.toItemId = toItemId;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ProductConversionConfigMst [id=" + id + ", fromItemId=" + fromItemId + ", toItemId=" + toItemId
				+ ", branchCode=" + branchCode + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", oldId=" + oldId + "]";
	}
	
	

}
