package com.maple.restserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class IngredientsMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(length = 50)
    private String id;
	@Column(length = 50)
	private String IngredientText1;
	@Column(length = 50)
	private String IngredientText2;
	@Column(length = 50)
	private String  duration;

	 

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	
	

	@OneToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "itemMst ", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	ItemMst itemMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 100)
	private String taskId;



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}


	



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public ItemMst getItemMst() {
		return itemMst;
	}



	public void setItemMst(ItemMst itemMst) {
		this.itemMst = itemMst;
	}



	public String getIngredientText1() {
		return IngredientText1;
	}



	public void setIngredientText1(String ingredientText1) {
		IngredientText1 = ingredientText1;
	}



	public String getIngredientText2() {
		return IngredientText2;
	}



	public void setIngredientText2(String ingredientText2) {
		IngredientText2 = ingredientText2;
	}



	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public String getDuration() {
		return duration;
	}



	public void setDuration(String duration) {
		this.duration = duration;
	}






	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	@Override
	public String toString() {
		return "IngredientsMst [id=" + id + ", IngredientText1=" + IngredientText1 + ", IngredientText2="
				+ IngredientText2 + ", duration=" + duration + ", companyMst=" + companyMst + ", itemMst=" + itemMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}





	
	
	
}
