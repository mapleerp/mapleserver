package com.maple.restserver.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
@Entity
public class OwnAccount implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
	
	private Date voucherDate;
	@Column(length = 50)
	private String voucherNo;
	@Column(length = 50)
	private String accountId;
	@Column(length = 50)
	private String accountType;
	private Double debitAmount;
	private Double creditAmount;
	private Double realizedAmount;
	private String ownAccountStatus;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "paymenthdr_id", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private PaymentHdr paymenthdr;
	
	 @ManyToOne(fetch = FetchType.EAGER, optional = false)
		@JoinColumn(name = "companyMst", nullable = false)
		@OnDelete(action = OnDeleteAction.CASCADE)
		CompanyMst companyMst;

	  @ManyToOne(fetch = FetchType.EAGER, optional = true)
		@JoinColumn(name = "receiptHdr", nullable = true)
		@OnDelete(action = OnDeleteAction.CASCADE)
		ReceiptHdr receiptHdr;
	  @Column(length = 50)
	    private String  processInstanceId;
	  @Column(length = 50)
		private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public Double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Double getRealizedAmount() {
		return realizedAmount;
	}

	public void setRealizedAmount(Double realizedAmount) {
		this.realizedAmount = realizedAmount;
	}

	public String getOwnAccountStatus() {
		return ownAccountStatus;
	}

	public void setOwnAccountStatus(String ownAccountStatus) {
		this.ownAccountStatus = ownAccountStatus;
	}

	public PaymentHdr getPaymenthdr() {
		return paymenthdr;
	}

	public void setPaymenthdr(PaymentHdr paymenthdr) {
		this.paymenthdr = paymenthdr;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public ReceiptHdr getReceiptHdr() {
		return receiptHdr;
	}

	public void setReceiptHdr(ReceiptHdr receiptHdr) {
		this.receiptHdr = receiptHdr;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "OwnAccount [id=" + id + ", voucherDate=" + voucherDate + ", voucherNo=" + voucherNo + ", accountId="
				+ accountId + ", accountType=" + accountType + ", debitAmount=" + debitAmount + ", creditAmount="
				+ creditAmount + ", realizedAmount=" + realizedAmount + ", ownAccountStatus=" + ownAccountStatus
				+ ", paymenthdr=" + paymenthdr + ", companyMst=" + companyMst + ", receiptHdr=" + receiptHdr
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	  
	  
}
