//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.01.09 at 12:54:57 PM IST 
//


package com.maple.restserver.xml.purchase;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


 
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "godownname",
    "batchname",
    "indentno",
    "orderno",
    "trackingnumber",
    "dynamiccstiscleared",
    "amount",
    "actualqty",
    "billedqty",
    "additionaldetailslist",
    "vouchercomponentlistlist"
})
@XmlRootElement(name = "BATCHALLOCATIONS.LIST")
public class BATCHALLOCATIONSLIST {

    @XmlElement(name = "GODOWNNAME", required = true)
    protected String godownname;
    @XmlElement(name = "BATCHNAME", required = true)
    protected String batchname;
    @XmlElement(name = "INDENTNO", required = true)
    protected String indentno;
    @XmlElement(name = "ORDERNO", required = true)
    protected String orderno;
    @XmlElement(name = "TRACKINGNUMBER", required = true)
    protected String trackingnumber;
    @XmlElement(name = "DYNAMICCSTISCLEARED", required = true)
    protected String dynamiccstiscleared;
    @XmlElement(name = "AMOUNT")
    protected String amount;
    @XmlElement(name = "ACTUALQTY", required = true)
    protected String actualqty;
    @XmlElement(name = "BILLEDQTY", required = true)
    protected String billedqty;
    @XmlElement(name = "ADDITIONALDETAILS.LIST", required = true)
    protected String additionaldetailslist;
    @XmlElement(name = "VOUCHERCOMPONENTLIST.LIST", required = true)
    protected String vouchercomponentlistlist;

    /**
     * Gets the value of the godownname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGODOWNNAME() {
        return godownname;
    }

    /**
     * Sets the value of the godownname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGODOWNNAME(String value) {
        this.godownname = value;
    }

    /**
     * Gets the value of the batchname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBATCHNAME() {
        return batchname;
    }

    /**
     * Sets the value of the batchname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBATCHNAME(String value) {
        this.batchname = value;
    }

    /**
     * Gets the value of the indentno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINDENTNO() {
        return indentno;
    }

    /**
     * Sets the value of the indentno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINDENTNO(String value) {
        this.indentno = value;
    }

    /**
     * Gets the value of the orderno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORDERNO() {
        return orderno;
    }

    /**
     * Sets the value of the orderno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORDERNO(String value) {
        this.orderno = value;
    }

    /**
     * Gets the value of the trackingnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRACKINGNUMBER() {
        return trackingnumber;
    }

    /**
     * Sets the value of the trackingnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRACKINGNUMBER(String value) {
        this.trackingnumber = value;
    }

    /**
     * Gets the value of the dynamiccstiscleared property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDYNAMICCSTISCLEARED() {
        return dynamiccstiscleared;
    }

    /**
     * Sets the value of the dynamiccstiscleared property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDYNAMICCSTISCLEARED(String value) {
        this.dynamiccstiscleared = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     */
    public String getAMOUNT() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAMOUNT(String value) {
        this.amount = value;
    }

    /**
     * Gets the value of the actualqty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACTUALQTY() {
        return actualqty;
    }

    /**
     * Sets the value of the actualqty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACTUALQTY(String value) {
        this.actualqty = value;
    }

    /**
     * Gets the value of the billedqty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBILLEDQTY() {
        return billedqty;
    }

    /**
     * Sets the value of the billedqty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBILLEDQTY(String value) {
        this.billedqty = value;
    }

    /**
     * Gets the value of the additionaldetailslist property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDITIONALDETAILSLIST() {
        return additionaldetailslist;
    }

    /**
     * Sets the value of the additionaldetailslist property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDITIONALDETAILSLIST(String value) {
        this.additionaldetailslist = value;
    }

    /**
     * Gets the value of the vouchercomponentlistlist property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVOUCHERCOMPONENTLISTLIST() {
        return vouchercomponentlistlist;
    }

    /**
     * Sets the value of the vouchercomponentlistlist property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVOUCHERCOMPONENTLISTLIST(String value) {
        this.vouchercomponentlistlist = value;
    }

}
