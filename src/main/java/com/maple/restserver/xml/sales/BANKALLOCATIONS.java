package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "BANKALLOCATIONS" )
public class BANKALLOCATIONS {
String DATE;
String INSTRUMENTDATE;
String NAME;
String TRANSACTIONTYPE;
String UNIQUEREFERENCENUMBER;
String STATUS;
String PAYMENTMODE;
String BANKPARTYNAME;
String ISCONNECTEDPAYMENT;
String ISSPLIT;
String ISCONTRACTUSED;
String AMOUNT;
public String getDATE() {
	return DATE;
}

@XmlElement( name = "DATE" )
public void setDATE(String dATE) {
	DATE = dATE;
}
public String getINSTRUMENTDATE() {
	return INSTRUMENTDATE;
}

@XmlElement( name = "INSTRUMENTDATE" )
public void setINSTRUMENTDATE(String iNSTRUMENTDATE) {
	INSTRUMENTDATE = iNSTRUMENTDATE;
}
public String getNAME() {
	return NAME;
}


@XmlElement( name = "NAME" )
public void setNAME(String nAME) {
	NAME = nAME;
}
public String getTRANSACTIONTYPE() {
	return TRANSACTIONTYPE;
}

@XmlElement( name = "TRANSACTIONTYPE" )
public void setTRANSACTIONTYPE(String tRANSACTIONTYPE) {
	TRANSACTIONTYPE = tRANSACTIONTYPE;
}
public String getUNIQUEREFERENCENUMBER() {
	return UNIQUEREFERENCENUMBER;
}

@XmlElement( name = "UNIQUEREFERENCENUMBER" )
public void setUNIQUEREFERENCENUMBER(String uNIQUEREFERENCENUMBER) {
	UNIQUEREFERENCENUMBER = uNIQUEREFERENCENUMBER;
}
public String getSTATUS() {
	return STATUS;
}

@XmlElement( name = "STATUS" )
public void setSTATUS(String sTATUS) {
	STATUS = sTATUS;
}
public String getPAYMENTMODE() {
	return PAYMENTMODE;
}

@XmlElement( name = "PAYMENTMODE" )
public void setPAYMENTMODE(String pAYMENTMODE) {
	PAYMENTMODE = pAYMENTMODE;
}
public String getBANKPARTYNAME() {
	return BANKPARTYNAME;
}

@XmlElement( name = "BANKPARTYNAME" )
public void setBANKPARTYNAME(String bANKPARTYNAME) {
	BANKPARTYNAME = bANKPARTYNAME;
}
public String getISCONNECTEDPAYMENT() {
	return ISCONNECTEDPAYMENT;
}

@XmlElement( name = "ISCONNECTEDPAYMENT" )
public void setISCONNECTEDPAYMENT(String iSCONNECTEDPAYMENT) {
	ISCONNECTEDPAYMENT = iSCONNECTEDPAYMENT;
}
public String getISSPLIT() {
	return ISSPLIT;
}

@XmlElement( name = "ISSPLIT" )
public void setISSPLIT(String iSSPLIT) {
	ISSPLIT = iSSPLIT;
}
public String getISCONTRACTUSED() {
	return ISCONTRACTUSED;
}

@XmlElement( name = "ISCONTRACTUSED" )
public void setISCONTRACTUSED(String iSCONTRACTUSED) {
	ISCONTRACTUSED = iSCONTRACTUSED;
}
public String getAMOUNT() {
	return AMOUNT;
}
@XmlElement( name = "AMOUNT" )
public void setAMOUNT(String aMOUNT) {
	AMOUNT = aMOUNT;
}


}
