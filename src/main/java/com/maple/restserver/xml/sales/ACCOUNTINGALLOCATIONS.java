package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "ACCOUNTINGALLOCATIONS" )
public class ACCOUNTINGALLOCATIONS {
String TAXCLASSIFICATIONNAME;
String LEDGERNAME;
String CLASSRATE;
String ISDEEMEDPOSITIVE;
String LEDGERFROMITEM;
String REMOVEZEROENTRIES;
String ISPARTYLEDGER;
String ISLASTDEEMEDPOSITIVE;
String AMOUNT;
String STATNATURENAME;
String VATTAXRATE;

CATEGORYALLOCATIONS catagoryallocation = new CATEGORYALLOCATIONS();

public String getTAXCLASSIFICATIONNAME() {
	return TAXCLASSIFICATIONNAME;
}

@XmlElement( name = "TAXCLASSIFICATIONNAME" )
public void setTAXCLASSIFICATIONNAME(String tAXCLASSIFICATIONNAME) {
	TAXCLASSIFICATIONNAME = tAXCLASSIFICATIONNAME;
}
public String getLEDGERNAME() {
	return LEDGERNAME;
}

@XmlElement( name = "LEDGERNAME" )
public void setLEDGERNAME(String lEDGERNAME) {
	LEDGERNAME = lEDGERNAME;
}
public String getCLASSRATE() {
	return CLASSRATE;
}

@XmlElement( name = "CLASSRATE" )
public void setCLASSRATE(String cLASSRATE) {
	CLASSRATE = cLASSRATE;
}
public String getISDEEMEDPOSITIVE() {
	return ISDEEMEDPOSITIVE;
}

@XmlElement( name = "ISDEEMEDPOSITIVE" )
public void setISDEEMEDPOSITIVE(String iSDEEMEDPOSITIVE) {
	ISDEEMEDPOSITIVE = iSDEEMEDPOSITIVE;
}
public String getLEDGERFROMITEM() {
	return LEDGERFROMITEM;
}

@XmlElement( name = "LEDGERFROMITEM" )
public void setLEDGERFROMITEM(String lEDGERFROMITEM) {
	LEDGERFROMITEM = lEDGERFROMITEM;
}
public String getREMOVEZEROENTRIES() {
	return REMOVEZEROENTRIES;
}

@XmlElement( name = "REMOVEZEROENTRIES" )
public void setREMOVEZEROENTRIES(String rEMOVEZEROENTRIES) {
	REMOVEZEROENTRIES = rEMOVEZEROENTRIES;
}
public String getISPARTYLEDGER() {
	return ISPARTYLEDGER;
}

@XmlElement( name = "ISPARTYLEDGER" )
public void setISPARTYLEDGER(String iSPARTYLEDGER) {
	ISPARTYLEDGER = iSPARTYLEDGER;
}
public String getISLASTDEEMEDPOSITIVE() {
	return ISLASTDEEMEDPOSITIVE;
}

@XmlElement( name = "ISLASTDEEMEDPOSITIVE" )
public void setISLASTDEEMEDPOSITIVE(String iSLASTDEEMEDPOSITIVE) {
	ISLASTDEEMEDPOSITIVE = iSLASTDEEMEDPOSITIVE;
}
public String getAMOUNT() {
	return AMOUNT;
}

@XmlElement( name = "AMOUNT" )
public void setAMOUNT(String aMOUNT) {
	AMOUNT = aMOUNT;
}
public CATEGORYALLOCATIONS getCatagoryallocation() {
	return catagoryallocation;
}

@XmlElement( name = "CATEGORYALLOCATIONS" )
public void setCatagoryallocation(CATEGORYALLOCATIONS catagoryallocation) {
	this.catagoryallocation = catagoryallocation;
}

/**
 * @return the sTATNATURENAME
 */
public String getSTATNATURENAME() {
	return STATNATURENAME;
}

/**
 * @param sTATNATURENAME the sTATNATURENAME to set
 */
@XmlElement( name = "STATNATURENAME" )
public void setSTATNATURENAME(String sTATNATURENAME) {
	STATNATURENAME = sTATNATURENAME;
}

/**
 * @return the vATTAXRATE
 */
public String getVATTAXRATE() {
	return VATTAXRATE;
}

/**
 * @param vATTAXRATE the vATTAXRATE to set
 */
@XmlElement( name = "VATTAXRATE" )
public void setVATTAXRATE(String vATTAXRATE) {
	VATTAXRATE = vATTAXRATE;
}


}
