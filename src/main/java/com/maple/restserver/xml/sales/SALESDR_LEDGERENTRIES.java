package com.maple.restserver.xml.sales;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlType( propOrder = { "LEDGERNAME", "ISDEEMEDPOSITIVE",
		"LEDGERFROMITEM","REMOVEZEROENTRIES",
		"ISPARTYLEDGER","ISLASTDEEMEDPOSITIVE",
		"AMOUNT" ,"METHODTYPE","TAXCLASSIFICATIONNAME",
		 "ROUNDTYPE", "RATEOFCESSONVAT", 
		  "VATASSESSABLEVALUE", "salesbillalloc" ,
		  "salesoldauditentry",
		  "basicrateofinvoicetax",
		  "categoryalloc",
		  "taxobjectallocation","bankallocation"} )
@XmlRootElement( name = "LEDGERENTRIES.LIST" )
public class SALESDR_LEDGERENTRIES {
	
	String LEDGERNAME;
	String ISDEEMEDPOSITIVE;
	String LEDGERFROMITEM;
	String REMOVEZEROENTRIES;
	String ISPARTYLEDGER;
	String ISLASTDEEMEDPOSITIVE;
	String AMOUNT;
	String TAXCLASSIFICATIONNAME;
	String ROUNDTYPE;
	String METHODTYPE;
	String RATEOFCESSONVAT;
	String VATASSESSABLEVALUE;
	
	
	

	SALES_BILLALLOCATIONS salesbillalloc= new SALES_BILLALLOCATIONS();
	

	SALES_OLDAUDITENTRYIDS salesoldauditentry = new SALES_OLDAUDITENTRYIDS();
	
	SALES_BASICRATEOFINVOICETAX basicrateofinvoicetax = new SALES_BASICRATEOFINVOICETAX();
	
	CATEGORYALLOCATIONS categoryalloc = new CATEGORYALLOCATIONS();
	
	TAXOBJECTALLOCATIONS taxobjectallocation = new TAXOBJECTALLOCATIONS();
	
	BANKALLOCATIONS bankallocation = new BANKALLOCATIONS();
	
	
	public String getLEDGERNAME() {
		return LEDGERNAME;
	}
	
	@XmlElement( name = "LEDGERNAME" )
	public void setLEDGERNAME(String lEDGERNAME) {
		LEDGERNAME = lEDGERNAME;
	}
	public String getISDEEMEDPOSITIVE() {
		return ISDEEMEDPOSITIVE;
	}
	
	@XmlElement( name = "ISDEEMEDPOSITIVE" )
	public void setISDEEMEDPOSITIVE(String iSDEEMEDPOSITIVE) {
		ISDEEMEDPOSITIVE = iSDEEMEDPOSITIVE;
	}
	
	public String getLEDGERFROMITEM() {
		return LEDGERFROMITEM;
	}
	
	@XmlElement( name = "LEDGERFROMITEM" )
	public void setLEDGERFROMITEM(String lEDGERFROMITEM) {
		LEDGERFROMITEM = lEDGERFROMITEM;
	}
	public String getREMOVEZEROENTRIES() {
		return REMOVEZEROENTRIES;
	}
	
	@XmlElement( name = "REMOVEZEROENTRIES" )
	public void setREMOVEZEROENTRIES(String rEMOVEZEROENTRIES) {
		REMOVEZEROENTRIES = rEMOVEZEROENTRIES;
	}
	public String getISPARTYLEDGER() {
		return ISPARTYLEDGER;
	}
	
	@XmlElement( name = "ISPARTYLEDGER" )
	public void setISPARTYLEDGER(String iSPARTYLEDGER) {
		ISPARTYLEDGER = iSPARTYLEDGER;
	}
	public String getISLASTDEEMEDPOSITIVE() {
		return ISLASTDEEMEDPOSITIVE;
	}

	@XmlElement( name = "ISLASTDEEMEDPOSITIVE" )
	public void setISLASTDEEMEDPOSITIVE(String iSLASTDEEMEDPOSITIVE) {
		ISLASTDEEMEDPOSITIVE = iSLASTDEEMEDPOSITIVE;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	
	@XmlElement( name = "AMOUNT" )
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public SALES_BILLALLOCATIONS getSalesbillalloc() {
		return salesbillalloc;
	}
	
	
	
	@XmlElement( name = "BILLALLOCATIONS.LIST" )
	public void setSalesbillalloc(SALES_BILLALLOCATIONS salesbillalloc) {
		this.salesbillalloc = salesbillalloc;
	}
	
	
	public SALES_OLDAUDITENTRYIDS getSalesoldauditentry() {
		return salesoldauditentry;
	}
	
	
	@XmlElement( name = "OLDAUDITENTRYIDS.LIST" )
	public void setSalesoldauditentry(SALES_OLDAUDITENTRYIDS salesoldauditentry) {
		this.salesoldauditentry = salesoldauditentry;
	}

	public SALES_BASICRATEOFINVOICETAX getBasicrateofinvoicetax() {
		return basicrateofinvoicetax;
	}

	
	
	@XmlElement( name = "BASICRATEOFINVOICETAX.LIST" )
	public void setBasicrateofinvoicetax(SALES_BASICRATEOFINVOICETAX basicrateofinvoicetax) {
		this.basicrateofinvoicetax = basicrateofinvoicetax;
	}

	public String getTAXCLASSIFICATIONNAME() {
		return TAXCLASSIFICATIONNAME;
	}

	public void setTAXCLASSIFICATIONNAME(String tAXCLASSIFICATIONNAME) {
		TAXCLASSIFICATIONNAME = tAXCLASSIFICATIONNAME;
	}

	public String getROUNDTYPE() {
		return ROUNDTYPE;
	}

	@XmlElement( name = "ROUNDTYPE" )
	public void setROUNDTYPE(String rOUNDTYPE) {
		ROUNDTYPE = rOUNDTYPE;
	}

	public CATEGORYALLOCATIONS getCategoryalloc() {
		return categoryalloc;
	}

	

	@XmlElement( name = "CATEGORYALLOCATIONS.LIST" )
	public void setCategoryalloc(CATEGORYALLOCATIONS categoryalloc) {
		this.categoryalloc = categoryalloc;
	}

	public String getMETHODTYPE() {
		return METHODTYPE;
	}

	@XmlElement( name = "METHODTYPE" )
	public void setMETHODTYPE(String mETHODTYPE) {
		METHODTYPE = mETHODTYPE;
	}

	public String getRATEOFCESSONVAT() {
		return RATEOFCESSONVAT;
	}

	@XmlElement( name = "RATEOFCESSONVAT" )
	public void setRATEOFCESSONVAT(String rATEOFCESSONVAT) {
		RATEOFCESSONVAT = rATEOFCESSONVAT;
	}

	public String getVATASSESSABLEVALUE() {
		return VATASSESSABLEVALUE;
	}

	@XmlElement( name = "VATASSESSABLEVALUE" )
	public void setVATASSESSABLEVALUE(String vATASSESSABLEVALUE) {
		VATASSESSABLEVALUE = vATASSESSABLEVALUE;
	}

	public TAXOBJECTALLOCATIONS getTaxobjectallocation() {
		return taxobjectallocation;
	}

	@XmlElement( name = "TAXOBJECTALLOCATIONS.LIST" )
	public void setTaxobjectallocation(TAXOBJECTALLOCATIONS taxobjectallocation) {
		this.taxobjectallocation = taxobjectallocation;
	}

	public BANKALLOCATIONS getBankallocation() {
		return bankallocation;
	}

	@XmlElement( name = "BANKALLOCATIONS.LIST" )
	public void setBankallocation(BANKALLOCATIONS bankallocation) {
		this.bankallocation = bankallocation;
	}
	
	
}
