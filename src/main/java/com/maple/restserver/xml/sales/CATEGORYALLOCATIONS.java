package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "CATEGORYALLOCATIONS.LIST" )
public class CATEGORYALLOCATIONS {
	String CATEGORY;
	String ISDEEMEDPOSITIVE;
	
	COSTCENTREALLOCATIONS costcenterallocation = new COSTCENTREALLOCATIONS();

	public String getCATEGORY() {
		return CATEGORY;
	}

	@XmlElement( name = "CATEGORY" )
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}

	public String getISDEEMEDPOSITIVE() {
		return ISDEEMEDPOSITIVE;
	}

	@XmlElement( name = "ISDEEMEDPOSITIVE" )
	public void setISDEEMEDPOSITIVE(String iSDEEMEDPOSITIVE) {
		ISDEEMEDPOSITIVE = iSDEEMEDPOSITIVE;
	}

	public COSTCENTREALLOCATIONS getCostcenterallocation() {
		return costcenterallocation;
	}

	
	@XmlElement( name = "COSTCENTREALLOCATIONS" )
	public void setCostcenterallocation(COSTCENTREALLOCATIONS costcenterallocation) {
		this.costcenterallocation = costcenterallocation;
	}
	
}
