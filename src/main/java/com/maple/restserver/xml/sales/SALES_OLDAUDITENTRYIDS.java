package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "OLDAUDITENTRYIDS.LIST" )
public class SALES_OLDAUDITENTRYIDS {

	String OLDAUDITENTRYIDS;
	String type;
	

	public String getOLDAUDITENTRYIDS() {
		return OLDAUDITENTRYIDS;
	}

	
	@XmlElement( name = "OLDAUDITENTRYIDS" )
	public void setOLDAUDITENTRYIDS(String oLDAUDITENTRYIDS) {
		OLDAUDITENTRYIDS = oLDAUDITENTRYIDS;
	}


	@XmlAttribute( name = "TYPE"  )
	public String getType() {
		return "Number";
	}


	public void setType(String type) {
		this.type = type;
	}
	
}
