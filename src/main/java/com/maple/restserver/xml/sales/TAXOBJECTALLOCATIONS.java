package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "TAXOBJECTALLOCATIONS" )
public class TAXOBJECTALLOCATIONS {
String CATEGORY;
String TAXTYPE;
String PARTYLEDGER;
String REFTYPE;
String ISPANVALID;
String ZERORATED;
String EXEMPTED;
String ISSPECIALRATE;

String ISDEDUCTNOW;
String ISPANNOTAVAILABLE;

SUBCATEGORYALLOCATION subcategoryalloc = new SUBCATEGORYALLOCATION();

public String getCATEGORY() {
	return CATEGORY;
}

@XmlElement( name = "CATEGORY" )
public void setCATEGORY(String cATEGORY) {
	CATEGORY = cATEGORY;
}

public String getTAXTYPE() {
	return TAXTYPE;
}

@XmlElement( name = "TAXTYPE" )
public void setTAXTYPE(String tAXTYPE) {
	TAXTYPE = tAXTYPE;
}

public String getPARTYLEDGER() {
	return PARTYLEDGER;
}

@XmlElement( name = "PARTYLEDGER" )
public void setPARTYLEDGER(String pARTYLEDGER) {
	PARTYLEDGER = pARTYLEDGER;
}

public String getREFTYPE() {
	return REFTYPE;
}

@XmlElement( name = "REFTYPE" )
public void setREFTYPE(String rEFTYPE) {
	REFTYPE = rEFTYPE;
}

public String getISPANVALID() {
	return ISPANVALID;
}

@XmlElement( name = "ISPANVALID" )
public void setISPANVALID(String iSPANVALID) {
	ISPANVALID = iSPANVALID;
}

public String getZERORATED() {
	return ZERORATED;
}


@XmlElement( name = "ZERORATED" )
public void setZERORATED(String zERORATED) {
	ZERORATED = zERORATED;
}

public String getEXEMPTED() {
	return EXEMPTED;
}

@XmlElement( name = "EXEMPTED" )
public void setEXEMPTED(String eXEMPTED) {
	EXEMPTED = eXEMPTED;
}

public String getISSPECIALRATE() {
	return ISSPECIALRATE;
}


@XmlElement( name = "ISSPECIALRATE" )
public void setISSPECIALRATE(String iSSPECIALRATE) {
	ISSPECIALRATE = iSSPECIALRATE;
}

public String getISDEDUCTNOW() {
	return ISDEDUCTNOW;
}


@XmlElement( name = "ISDEDUCTNOW" )
public void setISDEDUCTNOW(String iSDEDUCTNOW) {
	ISDEDUCTNOW = iSDEDUCTNOW;
}

public String getISPANNOTAVAILABLE() {
	return ISPANNOTAVAILABLE;
}


@XmlElement( name = "ISPANNOTAVAILABLE" )
public void setISPANNOTAVAILABLE(String iSPANNOTAVAILABLE) {
	ISPANNOTAVAILABLE = iSPANNOTAVAILABLE;
}

public SUBCATEGORYALLOCATION getSubcategoryalloc() {
	return subcategoryalloc;
}


@XmlElement( name = "SUBCATEGORYALLOCATION.LIST" )
public void setSubcategoryalloc(SUBCATEGORYALLOCATION subcategoryalloc) {
	this.subcategoryalloc = subcategoryalloc;
}




}
