package com.maple.restserver.xml.sales;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlType( propOrder = { "BASICBUYERADDRESS","BASICBUYERADDRESS2"} )
@XmlRootElement( name = "BASICBUYERADDRESS.LIST" )
public class SALES_BASICBUYERADDRESSLIST {

	String BASICBUYERADDRESS;
	String  BASICBUYERADDRESS2;
	

	@XmlElement( name = "BASICBUYERADDRESS" )
	public void setBASICBUYERADDRESS(String basicdress) {
		this.BASICBUYERADDRESS = basicdress;
	}
	public String getBASICBUYERADDRESS() {
		return BASICBUYERADDRESS;
	}
	
	@XmlElement( name = "BASICBUYERADDRESS" )
	public void setBASICBUYERADDRESS2(String basicdress) {
		this.BASICBUYERADDRESS2 = basicdress;
	}
	public String getBASICBUYERADDRESS2() {
		return BASICBUYERADDRESS2;
	}
	
	

}
