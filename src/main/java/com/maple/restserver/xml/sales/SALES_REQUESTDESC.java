package com.maple.restserver.xml.sales;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = { "reportname","staticvar"})
@XmlRootElement( name = "REQUESTDESC" )
public class SALES_REQUESTDESC {

	String reportname;
	SALES_STATICVARIABLES staticvar = new SALES_STATICVARIABLES();

	 

	public String getReportname() {
		return reportname;
	}


	@XmlElement( name = "REPORTNAME" )
	public void setReportname(String reportname) {
		this.reportname = reportname;
	}


	public SALES_STATICVARIABLES getStaticvar() {
		return staticvar;
	}


	@XmlElement( name = "STATICVARIABLE" )
	public void setStaticvar(SALES_STATICVARIABLES staticvar) {
		this.staticvar = staticvar;
	}
	
}
