package com.maple.restserver.xml.sales;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlType( propOrder = { "hdr","body" } )
@XmlRootElement( name = "ENVELOPE" )
public class SALES_ENVELOPE {
	SALES_HEADER hdr = new SALES_HEADER();
	SALES_BODY body = new SALES_BODY();
	
	public SALES_HEADER getHdr() {
		return hdr;
	}

	@XmlElement( name = "HEADER" )
	public void setHdr(SALES_HEADER hdr) {
		this.hdr = hdr;
	}

	public SALES_BODY getBody() {
		return body;
	}

	
	@XmlElement( name = "BODY" )
	public void setBody(SALES_BODY body) {
		this.body = body;
	}

	
	

}
