package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = { "BASICRATEOFINVOICETAX"} )
@XmlRootElement( name = "BASICRATEOFINVOICETAX.LIST" )
public class SALES_BASICRATEOFINVOICETAX {

	String type;
	
	String BASICRATEOFINVOICETAX;

	public String getBASICRATEOFINVOICETAX() {
		return BASICRATEOFINVOICETAX;
	}


	@XmlElement( name = "BASICRATEOFINVOICETAX" )
	public void setBASICRATEOFINVOICETAX(String bASICRATEOFINVOICETAX) {
		BASICRATEOFINVOICETAX = bASICRATEOFINVOICETAX;
	}


	@XmlAttribute( name = "TYPE"  )
	public String getType() {
		return "Number";
	}


	public void setType(String type) {
		this.type = type;
	}
	
}
