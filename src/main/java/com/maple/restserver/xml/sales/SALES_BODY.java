package com.maple.restserver.xml.sales;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "BODY" )
public class SALES_BODY {
	
	SALES_IMPORTDATA impdate = new SALES_IMPORTDATA();

	public SALES_IMPORTDATA getImpdate() {
		return impdate;
	}

	
	@XmlElement( name = "IMPORTDATA" )
	public void setImpdate(SALES_IMPORTDATA impdate) {
		this.impdate = impdate;
	}
	
}
