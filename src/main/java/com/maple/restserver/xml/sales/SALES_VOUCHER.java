package com.maple.restserver.xml.sales;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = { "date", "guid","narration","natureofsales",
		"classname","partyname","vouchertypename", "vouchernumber",
		"partyledgername", "basicbasepartyname", "serialmaster",
		"serialnumber","cstformissuetype","cstformrecvtype",
		 "fbtpaymenttype" ,"persistedview", "basicbuyername", 
		 "basicdatetimeofinvoice","basicdatetimeofremoval",
		 "vchgstclass", "enteredby", "diffactualqty", "sudited",
		 "effectivedate","alterid","exciseopening", "useforfinalproduction",
		 "iscancelled","hascashflow","ispostdated","usetrackingnumber",
			"isinvoice", "mfjournal", "hasdiscounts", "aspayslip","iscostcenter",
			"isstxnonrealizedvch","isdeleted","asorginal","vchfromsync","masterid",
			"voucherkey","basicbuyeraddresslist","ledgerentries","allinventoryList",
			"isvatoverridden"
		 } )
@XmlRootElement( name = "VOUCHER" )
public class SALES_VOUCHER {

	String VCHTYPE;
	String action ;

	String date;
	String guid;
	String narration;
	String natureofsales;
	String classname;
	String partyname;
	String vouchertypename;
	String vouchernumber;
	String partyledgername;
	String basicbasepartyname;
	String serialmaster;
	String serialnumber;
	String cstformissuetype;
	String cstformrecvtype;
	String fbtpaymenttype;
	String persistedview;
	String basicbuyername;
	String basicdatetimeofinvoice;
	String basicdatetimeofremoval;
	String vchgstclass;
	String enteredby;
	String diffactualqty;
	String sudited;
	String effectivedate;
	String alterid;
	String exciseopening;
	String useforfinalproduction;
	String iscancelled;
	String hascashflow;
	String ispostdated;
	String usetrackingnumber;
	String isinvoice;
	String mfjournal;
	String hasdiscounts;
	String aspayslip;
	String iscostcenter;
	String isstxnonrealizedvch;
	String isdeleted;
	String asorginal;
	String vchfromsync;
	String masterid;
	String voucherkey;
	String isvatoverridden;
	
	
	SALES_BASICBUYERADDRESSLIST basicbuyeraddresslist = new SALES_BASICBUYERADDRESSLIST();
	
	ArrayList<SALESDR_LEDGERENTRIES> ledgerentries = new ArrayList() ;
	
	ArrayList<ALLINVENTORYENTRIES> allinventoryList = new ArrayList();
	
	
	@XmlAttribute( name = "ACTION"  )
	public String getAction() {
		return "Create";
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDate() {
		return date;
	}

	
	@XmlElement( name = "DATE" )
	public void setDate(String date) {
		this.date = date;
	}

	public String getGuid() {
		return guid;
	}

	@XmlElement( name = "GUID" )
	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getNarration() {
		return narration;
	}

	@XmlElement( name = "NARRATION" )
	public void setNarration(String narration) {
		this.narration = narration;
	}

	
	@XmlElement( name = "ISVATOVERRIDDEN" )
	public void setIsvatoverridden(String value) {
		this.isvatoverridden = value;
	}
	
	public String getIsvatoverridden() {
		return isvatoverridden;
	}

	
	
	public String getNatureofsales() {
		return natureofsales;
	}

	@XmlElement( name = "NATUREOFSALES" )
	public void setNatureofsales(String natureofsales) {
		this.natureofsales = natureofsales;
	}

	public String getClassname() {
		return classname;
	}

	
	@XmlElement( name = "CLASSNAME" )
	public void setClassname(String classname) {
		this.classname = classname;
	}

	public String getPartyname() {
		return partyname;
	}

	
	@XmlElement( name = "PARTYNAME" )
	public void setPartyname(String partyname) {
		this.partyname = partyname;
	}

	public String getVouchertypename() {
		return vouchertypename;
	}

	
	@XmlElement( name = "VOUCHERTYPENAME" )
	public void setVouchertypename(String vouchertypename) {
		this.vouchertypename = vouchertypename;
	}

	public String getVouchernumber() {
		return vouchernumber;
	}

	@XmlElement( name = "VOUCHERNUMBER" )
	public void setVouchernumber(String vouchernumber) {
		this.vouchernumber = vouchernumber;
	}

	public String getPartyledgername() {
		return partyledgername;
	}

	@XmlElement( name = "PARTYLEDGERNAME" )
	public void setPartyledgername(String partyledgername) {
		this.partyledgername = partyledgername;
	}

	public String getBasicbasepartyname() {
		return basicbasepartyname;
	}

	@XmlElement( name = "BASICBASEPARTYNAME" )
	public void setBasicbasepartyname(String basicbasepartyname) {
		this.basicbasepartyname = basicbasepartyname;
	}

	public String getSerialmaster() {
		return serialmaster;
	}

	@XmlElement( name = "SERIALMASTER" )
	public void setSerialmaster(String serialmaster) {
		this.serialmaster = serialmaster;
	}

	public String getSerialnumber() {
		return serialnumber;
	}

	@XmlElement( name = "SERIALNUMBER" )
	public void setSerialnumber(String serialnumber) {
		this.serialnumber = serialnumber;
	}

	public String getCstformissuetype() {
		return cstformissuetype;
	}

	
	@XmlElement( name = "CSTFORMISSUETYPE" )
	public void setCstformissuetype(String cstformissuetype) {
		this.cstformissuetype = cstformissuetype;
	}

	public String getCstformrecvtype() {
		return cstformrecvtype;
	}

	
	@XmlElement( name = "CSTFORMRECVTYPE" )
	public void setCstformrecvtype(String cstformrecvtype) {
		this.cstformrecvtype = cstformrecvtype;
	}

	public String getFbtpaymenttype() {
		return fbtpaymenttype;
	}

	@XmlElement( name = "FBTPAYMENTTYPE" )
	public void setFbtpaymenttype(String fbtpaymenttype) {
		this.fbtpaymenttype = fbtpaymenttype;
	}

	public String getPersistedview() {
		return persistedview;
	}

	
	@XmlElement( name = "PERSISTEDVIEW" )
	public void setPersistedview(String persistedview) {
		this.persistedview = persistedview;
	}

	public String getBasicbuyername() {
		return basicbuyername;
	}

	@XmlElement( name = "BASICBUYERNAME" )
	public void setBasicbuyername(String basicbuyername) {
		this.basicbuyername = basicbuyername;
	}

	public String getBasicdatetimeofinvoice() {
		return basicdatetimeofinvoice;
	}

	
	@XmlElement( name = "BASICDATETIMEOFINVOICE" )
	public void setBasicdatetimeofinvoice(String basicdatetimeofinvoice) {
		this.basicdatetimeofinvoice = basicdatetimeofinvoice;
	}

	public String getBasicdatetimeofremoval() {
		return basicdatetimeofremoval;
	}

	@XmlElement( name = "BASICDATETIMEOFREMOVAL" )
	public void setBasicdatetimeofremoval(String basicdatetimeofremoval) {
		this.basicdatetimeofremoval = basicdatetimeofremoval;
	}

	public String getVchgstclass() {
		return vchgstclass;
	}

	
	@XmlElement( name = "VCHGSTCLASS" )
	public void setVchgstclass(String vchgstclass) {
		this.vchgstclass = vchgstclass;
	}

	public String getEnteredby() {
		return enteredby;
	}

	@XmlElement( name = "ENTEREDBY" )
	public void setEnteredby(String enteredby) {
		this.enteredby = enteredby;
	}

	public String getDiffactualqty() {
		return diffactualqty;
	}

	
	@XmlElement( name = "DIFFACTUALQTY" )
	public void setDiffactualqty(String diffactualqty) {
		this.diffactualqty = diffactualqty;
	}

	public String getSudited() {
		return sudited;
	}

	@XmlElement( name = "AUDITED" )
	public void setSudited(String sudited) {
		this.sudited = sudited;
	}

	 
	public String getEffectivedate() {
		return effectivedate;
	}

	@XmlElement( name = "EFFECTIVEDATE" )
	public void setEffectivedate(String effectivedate) {
		this.effectivedate = effectivedate;
	}

	
	public String getAlterid() {
		return alterid;
	}

	@XmlElement( name = "ALTERID" )
	public void setAlterid(String alterid) {
		this.alterid = alterid;
	}

	public String getExciseopening() {
		return exciseopening;
	}

	@XmlElement( name = "EXCISEOPENING" )
	public void setExciseopening(String exciseopening) {
		this.exciseopening = exciseopening;
	}

	public String getUseforfinalproduction() {
		return useforfinalproduction;
	}

	@XmlElement( name = "USEFORFINALPRODUCTION" )
	public void setUseforfinalproduction(String useforfinalproduction) {
		this.useforfinalproduction = useforfinalproduction;
	}

	public String getIscancelled() {
		return iscancelled;
	}

	
	@XmlElement( name = "ISCANCELLED" )
	public void setIscancelled(String iscancelled) {
		this.iscancelled = iscancelled;
	}

	public String getHascashflow() {
		return hascashflow;
	}

	
	@XmlElement( name = "HASCASHFLOW" )
	public void setHascashflow(String hascashflow) {
		this.hascashflow = hascashflow;
	}

	public String getIspostdated() {
		return ispostdated;
	}

	
	@XmlElement( name = "ISPOSTDATED" )
	public void setIspostdated(String ispostdated) {
		this.ispostdated = ispostdated;
	}

	public String getUsetrackingnumber() {
		return usetrackingnumber;
	}

	
	@XmlElement( name = "USETRACKINGNUMBER" )
	public void setUsetrackingnumber(String usetrackingnumber) {
		this.usetrackingnumber = usetrackingnumber;
	}

	public String getIsinvoice() {
		return isinvoice;
	}

	
	@XmlElement( name = "ISINVOICE" )
	public void setIsinvoice(String isinvoice) {
		this.isinvoice = isinvoice;
	}

	public String getMfjournal() {
		return mfjournal;
	}

	
	@XmlElement( name = "MFGJOURNAL" )
	public void setMfjournal(String mfjournal) {
		this.mfjournal = mfjournal;
	}

	public String getHasdiscounts() {
		return hasdiscounts;
	}

	
	@XmlElement( name = "HASDISCOUNTS" )
	public void setHasdiscounts(String hasdiscounts) {
		this.hasdiscounts = hasdiscounts;
	}

	public String getAspayslip() {
		return aspayslip;
	}

	@XmlElement( name = "ASPAYSLIP" )
	public void setAspayslip(String aspayslip) {
		this.aspayslip = aspayslip;
	}

	public String getIscostcenter() {
		return iscostcenter;
	}

	
	@XmlElement( name = "ASPAYSLIP" )
	public void setIscostcenter(String iscostcenter) {
		this.iscostcenter = iscostcenter;
	}

	public String getIsstxnonrealizedvch() {
		return isstxnonrealizedvch;
	}

	@XmlElement( name = "ISSTXNONREALIZEDVCH" )
	public void setIsstxnonrealizedvch(String isstxnonrealizedvch) {
		this.isstxnonrealizedvch = isstxnonrealizedvch;
	}

	public String getIsdeleted() {
		return isdeleted;
	}

	@XmlElement( name = "ISDELETED" )
	public void setIsdeleted(String isdeleted) {
		this.isdeleted = isdeleted;
	}

	public String getAsorginal() {
		return asorginal;
	}

	@XmlElement( name = "ASORIGINAL" )
	public void setAsorginal(String asorginal) {
		this.asorginal = asorginal;
	}

	public String getVchfromsync() {
		return vchfromsync;
	}

	@XmlElement( name = "VCHISFROMSYNC" )
	public void setVchfromsync(String vchfromsync) {
		this.vchfromsync = vchfromsync;
	}

	public String getMasterid() {
		return masterid;
	}

	@XmlElement( name = "MASTERID" )
	public void setMasterid(String masterid) {
		this.masterid = masterid;
	}

	public String getVoucherkey() {
		return voucherkey;
	}

	@XmlElement( name = "VOUCHERKEY" )
	public void setVoucherkey(String voucherkey) {
		this.voucherkey = voucherkey;
	}

	public SALES_BASICBUYERADDRESSLIST getBasicbuyeraddresslist() {
		return basicbuyeraddresslist;
	}

	@XmlElement( name = "BASICBUYERADDRESS.LIST" )
	public void setBasicbuyeraddresslist(SALES_BASICBUYERADDRESSLIST basicbuyeraddresslist) {
		this.basicbuyeraddresslist = basicbuyeraddresslist;
	}

	
	@XmlAttribute( name = "VCHTYPE"  )
	public String getVCHTYPE() {
		return VCHTYPE;
	}

	public void setVCHTYPE(String vCHTYPE) {
		VCHTYPE = vCHTYPE;
	}

	public ArrayList<SALESDR_LEDGERENTRIES> getLedgerentries() {
		return ledgerentries;
	}
	
	@XmlElement( name = "LEDGERENTRIES.LIST" )
	public void setLedgerentries(ArrayList<SALESDR_LEDGERENTRIES> ledgerentries) {
		this.ledgerentries = ledgerentries;
	}

	public ArrayList<ALLINVENTORYENTRIES> getAllinventoryList() {
		return allinventoryList;
	}

	@XmlElement( name = "ALLINVENTORYENTRIES.LIST" )
	public void setAllinventoryList(ArrayList<ALLINVENTORYENTRIES> allinventoryList) {
		this.allinventoryList = allinventoryList;
	}
	
	
}
