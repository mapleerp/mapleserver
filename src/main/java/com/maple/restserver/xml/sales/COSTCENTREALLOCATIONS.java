package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "COSTCENTREALLOCATIONS.LIST" )
public class COSTCENTREALLOCATIONS {
	String NAME;
	String AMOUNT;
	public String getNAME() {
		return NAME;
	}
	
	@XmlElement( name = "NAME" )
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	
	@XmlElement( name = "AMOUNT" )
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	
	
}
