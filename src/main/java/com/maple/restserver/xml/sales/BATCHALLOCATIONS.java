package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "BATCHALLOCATIONS" )
public class BATCHALLOCATIONS {
String GODOWNNAME;
String BATCHNAME;
String AMOUNT;
String ACTUALQUANTITY;
String BILLEDQUANTITY;
String UDF01;

public String getGODOWNNAME() {
	return GODOWNNAME;
}

@XmlElement( name = "GODOWNNAME" )
public void setGODOWNNAME(String gODOWNNAME) {
	GODOWNNAME = gODOWNNAME;
}
public String getBATCHNAME() {
	return BATCHNAME;
}

@XmlElement( name = "BATCHNAME" )
public void setBATCHNAME(String bATCHNAME) {
	BATCHNAME = bATCHNAME;
}
public String getAMOUNT() {
	return AMOUNT;
}

@XmlElement( name = "AMOUNT" )
public void setAMOUNT(String aMOUNT) {
	AMOUNT = aMOUNT;
}
public String getACTUALQUANTITY() {
	return ACTUALQUANTITY;
}

@XmlElement( name = "ACTUALQTY" )
public void setACTUALQUANTITY(String aCTUALQUANTITY) {
	ACTUALQUANTITY = aCTUALQUANTITY;
}
public String getBILLEDQUANTITY() {
	return BILLEDQUANTITY;
}

@XmlElement( name = "BILLEDQTY" )
public void setBILLEDQUANTITY(String bILLEDQUANTITY) {
	BILLEDQUANTITY = bILLEDQUANTITY;
}


public String getUDF01() {
	return UDF01;
}
 
public void setUDF01(String uDF01) {
	UDF01 = uDF01;
}

}
