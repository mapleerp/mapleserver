package com.maple.restserver.xml.sales;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlType( propOrder = {"voucher"  })
@XmlRootElement( name = "TALLYMESSAGE" )

public class SALES_TALLYMESSAGE {

	SALES_VOUCHER voucher = new SALES_VOUCHER();
	
	String attrib;
	
 
	public SALES_VOUCHER getVoucher() {
		return voucher;
	}



	@XmlElement( name = "VOUCHER" )
	public void setVoucher(SALES_VOUCHER voucher) {
		this.voucher = voucher;
	}


	@XmlAttribute( name = "xmlns:UDF"  )
	public String getAttrib() {
		return "TallyUDF";
	}



	public void setAttrib(String attrib) {
		this.attrib = attrib;
	}



	 
	
}
