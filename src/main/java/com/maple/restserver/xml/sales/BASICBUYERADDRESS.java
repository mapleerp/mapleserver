package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlType( propOrder = { "buyername"} )

@XmlRootElement( name = "BASICBUYERADDRESS" )
public class BASICBUYERADDRESS {
String buyername;

public String getBuyername() {
	return buyername;
}

@XmlElement( name = "BUYERNAME" )
public void setBuyername(String buyername) {
	this.buyername = buyername;
}

}
