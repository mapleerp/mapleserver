package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = { "srequestdec","salesrequestdata" } )
@XmlRootElement( name = "IMPORTDATA" )
public class SALES_IMPORTDATA {

	SALES_REQUESTDESC srequestdec = new SALES_REQUESTDESC();
	SALES_REQUESTDATA salesrequestdata = new SALES_REQUESTDATA();
	
	
	public SALES_REQUESTDESC getSrequestdec() {
		return srequestdec;
	}
	
	@XmlElement( name = "REQUESTDESC" )
	public void setSrequestdec(SALES_REQUESTDESC srequestdec) {
		this.srequestdec = srequestdec;
	}
	public SALES_REQUESTDATA getSalesrequestdata() {
		return salesrequestdata;
	}
	
	@XmlElement( name = "REQUESTDATA" )
	public void setSalesrequestdata(SALES_REQUESTDATA salesrequestdata) {
		this.salesrequestdata = salesrequestdata;
	}
	
}
