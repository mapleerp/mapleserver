package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = { "NAME", "BILLTYPE",
		"TDSDEDUCTEEISSPECIALRATE","AMOUNT",
		"INTERESTCOLLECTION"} )
@XmlRootElement( name = "BILLALLOCATIONS.LIST" )
public class SALES_BILLALLOCATIONS {

	
	String NAME;
	String BILLTYPE;
	String TDSDEDUCTEEISSPECIALRATE;
	String AMOUNT;
	String INTERESTCOLLECTION;
	public String getNAME() {
		return NAME;
	}
	
	@XmlElement( name = "NAME" )
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getBILLTYPE() {
		return BILLTYPE;
	}
	
	@XmlElement( name = "BILLTYPE" )
	public void setBILLTYPE(String bILLTYPE) {
		BILLTYPE = bILLTYPE;
	}
	public String getTDSDEDUCTEEISSPECIALRATE() {
		return TDSDEDUCTEEISSPECIALRATE;
	}
	
	@XmlElement( name = "TDSDEDUCTEEISSPECIALRATE" )
	public void setTDSDEDUCTEEISSPECIALRATE(String tDSDEDUCTEEISSPECIALRATE) {
		TDSDEDUCTEEISSPECIALRATE = tDSDEDUCTEEISSPECIALRATE;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	
	@XmlElement( name = "AMOUNT" )
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getINTERESTCOLLECTION() {
		return INTERESTCOLLECTION;
	}
	
	@XmlElement( name = "INTERESTCOLLECTION" )
	public void setINTERESTCOLLECTION(String iNTERESTCOLLECTION) {
		INTERESTCOLLECTION = iNTERESTCOLLECTION;
	}
	
}
