package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = {  "STOCKITEMNAME", "ISDEEMEDPOSITIVE","ISLASTDEEMEDPOSITIVE",
		"ISAUTONEGATE", "RATE", "AMOUNT","ACTUALQTY", "BILLEDQTY",
		"batchAllocation","accountingAllocation" } )
@XmlRootElement( name = "BATCHALLOCATIONS" )
public class ALLINVENTORYENTRIES {

String ISDEEMEDPOSITIVE;
String ISLASTDEEMEDPOSITIVE;
String ISAUTONEGATE;
String RATE;
String AMOUNT;
String ACTUALQTY;
String BILLEDQTY;
String STOCKITEMNAME;

BATCHALLOCATIONS batchAllocation = new BATCHALLOCATIONS();

ACCOUNTINGALLOCATIONS accountingAllocation = new ACCOUNTINGALLOCATIONS();

public String getSTOCKITEMNAME() {
	return STOCKITEMNAME;
}

@XmlElement( name = "STOCKITEMNAME" )
public void setSTOCKITEMNAME(String sTOCKITEMNAME) {
	STOCKITEMNAME = sTOCKITEMNAME;
}

public String getISDEEMEDPOSITIVE() {
	return ISDEEMEDPOSITIVE;
}

@XmlElement( name = "ISDEEMEDPOSITIVE" )
public void setISDEEMEDPOSITIVE(String iSDEEMEDPOSITIVE) {
	ISDEEMEDPOSITIVE = iSDEEMEDPOSITIVE;
}

public String getISLASTDEEMEDPOSITIVE() {
	return ISLASTDEEMEDPOSITIVE;
}

@XmlElement( name = "ISLASTDEEMEDPOSITIVE" )
public void setISLASTDEEMEDPOSITIVE(String iSLASTDEEMEDPOSITIVE) {
	ISLASTDEEMEDPOSITIVE = iSLASTDEEMEDPOSITIVE;
}

public String getISAUTONEGATE() {
	return ISAUTONEGATE;
}

@XmlElement( name = "ISAUTONEGATE" )
public void setISAUTONEGATE(String iSAUTONEGATE) {
	ISAUTONEGATE = iSAUTONEGATE;
}

public String getRATE() {
	return RATE;
}


@XmlElement( name = "RATE" )
public void setRATE(String rATE) {
	RATE = rATE;
}

public String getAMOUNT() {
	return AMOUNT;
}

@XmlElement( name = "AMOUNT" )
public void setAMOUNT(String aMOUNT) {
	AMOUNT = aMOUNT;
}

public String getACTUALQTY() {
	return ACTUALQTY;
}

@XmlElement( name = "ACTUALQTY" )
public void setACTUALQTY(String aCTUALQTY) {
	ACTUALQTY = aCTUALQTY;
}

public String getBILLEDQTY() {
	return BILLEDQTY;
}

@XmlElement( name = "BILLEDQTY" )
public void setBILLEDQTY(String bILLEDQTY) {
	BILLEDQTY = bILLEDQTY;
}

public BATCHALLOCATIONS getBatchAllocation() {
	return batchAllocation;
}

@XmlElement( name = "BATCHALLOCATIONS.LIST" )
public void setBatchAllocation(BATCHALLOCATIONS batchAllocation) {
	this.batchAllocation = batchAllocation;
}

public ACCOUNTINGALLOCATIONS getAccountingAllocation() {
	return accountingAllocation;
}

@XmlElement( name = "ACCOUNTINGALLOCATIONS.LIST" )
public void setAccountingAllocation(ACCOUNTINGALLOCATIONS accountingAllocation) {
	this.accountingAllocation = accountingAllocation;
}


}
