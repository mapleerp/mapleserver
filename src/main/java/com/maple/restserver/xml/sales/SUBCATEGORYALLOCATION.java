package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "SUBCATEGORYALLOCATION" )
public class SUBCATEGORYALLOCATION {
String STOCKITEMNAME;
String EXCISEUNITNAME;
String SUBCATEGORY;
String DUTYLEDGER;
String TAXRATE;
String ASSESSABLEAMOUNT;
String TAX;
String BILLEDQTY;


public String getSTOCKITEMNAME() {
	return STOCKITEMNAME;
}


@XmlElement( name = "STOCKITEMNAME" )
public void setSTOCKITEMNAME(String sTOCKITEMNAME) {
	STOCKITEMNAME = sTOCKITEMNAME;
}
public String getEXCISEUNITNAME() {
	return EXCISEUNITNAME;
}

@XmlElement( name = "EXCISEUNITNAME" )
public void setEXCISEUNITNAME(String eXCISEUNITNAME) {
	EXCISEUNITNAME = eXCISEUNITNAME;
}
public String getSUBCATEGORY() {
	return SUBCATEGORY;
}

@XmlElement( name = "SUBCATEGORY" )
public void setSUBCATEGORY(String sUBCATEGORY) {
	SUBCATEGORY = sUBCATEGORY;
}
public String getDUTYLEDGER() {
	return DUTYLEDGER;
}

@XmlElement( name = "DUTYLEDGER" )
public void setDUTYLEDGER(String dUTYLEDGER) {
	DUTYLEDGER = dUTYLEDGER;
}
public String getTAXRATE() {
	return TAXRATE;
}

@XmlElement( name = "TAXRATE" )
public void setTAXRATE(String tAXRATE) {
	TAXRATE = tAXRATE;
}
public String getASSESSABLEAMOUNT() {
	return ASSESSABLEAMOUNT;
}

@XmlElement( name = "ASSESSABLEAMOUNT" )
public void setASSESSABLEAMOUNT(String aSSESSABLEAMOUNT) {
	ASSESSABLEAMOUNT = aSSESSABLEAMOUNT;
}
public String getTAX() {
	return TAX;
}


@XmlElement( name = "TAX" )
public void setTAX(String tAX) {
	TAX = tAX;
}
public String getBILLEDQTY() {
	return BILLEDQTY;
}

@XmlElement( name = "BILLEDQTY" )
public void setBILLEDQTY(String bILLEDQTY) {
	BILLEDQTY = bILLEDQTY;
}




}
