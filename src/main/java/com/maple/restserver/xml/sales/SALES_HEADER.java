package com.maple.restserver.xml.sales;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
//"version",
@XmlType( propOrder = {  "tallyrequest", "type","id"} )
@XmlRootElement( name = "HEADER" )
public class SALES_HEADER {
	
	int version;
	String tallyrequest;
	String type;
	String id;
	public int getVersion() {
		return version;
	}
	//@XmlElement( name = "VERSION" )
	//public void setVersion(int version) {
	//	this.version = version;
	//}
	public String getTallyrequest() {
		return tallyrequest;
	}
	
	@XmlElement( name = "TALLYREQUEST" )
	public void setTallyrequest(String tallyrequest) {
		this.tallyrequest = tallyrequest;
	}
	public String getType() {
		return type;
	}
	
	@XmlElement( name = "TYPE" )
	public void setType(String type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	
	
	@XmlElement( name = "ID" )
	public void setId(String i) {
		this.id = i;
	}

}
