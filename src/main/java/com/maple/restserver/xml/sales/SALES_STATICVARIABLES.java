package com.maple.restserver.xml.sales;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = { "svcurrentcompany"} )
@XmlRootElement( name = "STATICVARIABLE" )
public class SALES_STATICVARIABLES {
	
	String svcurrentcompany;
	
	
	
	public String getSvcurrentcompany() {
		return svcurrentcompany;
	}
	
	@XmlElement( name = "SVCURRENTCOMPANY" )
	public void setSvcurrentcompany(String svcurrentcompany) {
		this.svcurrentcompany = svcurrentcompany;
	}


}
