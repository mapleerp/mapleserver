package com.maple.restserver.xml.sales;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "DATA" )
public class SALES_REQUESTDATA {
	
	

	SALES_TALLYMESSAGE tmsg = new SALES_TALLYMESSAGE();

	
	public SALES_TALLYMESSAGE getTmsg() {
		return tmsg;
	}

	
	@XmlElement( name = "TALLYMESSAGE" )
	public void setTmsg(SALES_TALLYMESSAGE tmsg) {
		this.tmsg = tmsg;
	}
	
}
