package com.maple.restserver.xml.sales;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( propOrder = { "TAXCLASSIFICATIONNAME", "ROUNDTYPE",
		"LEDGERNAME","METHODTYPE",
		"CLASSRATE","GSTCLASS",
		"ISDEEMEDPOSITIVE" , "LEDGERFROMITEM" ,
		"REMOVEZEROENTRIES" ,"ISPARTYLEDGER",
		 "ISLASTDEEMEDPOSITIVE" , "RATEOFCESSONVAT",
		  "AMOUNT"} )

@XmlRootElement( name = "LEDGERENTRIES.LIST" )
public class SALESCR_LEDGERENTRIES {
	SALES_OLDAUDITENTRYIDS alodauditentry = new SALES_OLDAUDITENTRYIDS();
	SALES_BASICRATEOFINVOICETAX basicrateofinvoicetax = new SALES_BASICRATEOFINVOICETAX();
	
	
	String TAXCLASSIFICATIONNAME;
	String ROUNDTYPE;
	String LEDGERNAME;
	String METHODTYPE;
	String CLASSRATE;
	String GSTCLASS;
	String ISDEEMEDPOSITIVE;
	String LEDGERFROMITEM;
	String REMOVEZEROENTRIES;
	String ISPARTYLEDGER;
	String ISLASTDEEMEDPOSITIVE;
	String RATEOFCESSONVAT;
	String AMOUNT;
	
	public SALES_OLDAUDITENTRYIDS getAlodauditentry() {
		return alodauditentry;
	}
	
	
	public void setAlodauditentry(SALES_OLDAUDITENTRYIDS alodauditentry) {
		this.alodauditentry = alodauditentry;
	}
	public String getTAXCLASSIFICATIONNAME() {
		return TAXCLASSIFICATIONNAME;
	}
	
	
	
	@XmlElement( name = "TAXCLASSIFICATIONNAME" )
	public void setTAXCLASSIFICATIONNAME(String tAXCLASSIFICATIONNAME) {
		TAXCLASSIFICATIONNAME = tAXCLASSIFICATIONNAME;
	}
	public String getROUNDTYPE() {
		return ROUNDTYPE;
	}
	
	@XmlElement( name = "ROUNDTYPE" )
	public void setROUNDTYPE(String rOUNDTYPE) {
		ROUNDTYPE = rOUNDTYPE;
	}
	public String getLEDGERNAME() {
		return LEDGERNAME;
	}
	
	@XmlElement( name = "LEDGERNAME" )
	public void setLEDGERNAME(String lEDGERNAME) {
		LEDGERNAME = lEDGERNAME;
	}
	public String getMETHODTYPE() {
		return METHODTYPE;
	}
	
	@XmlElement( name = "METHODTYPE" )
	public void setMETHODTYPE(String mETHODTYPE) {
		METHODTYPE = mETHODTYPE;
	}
	public String getCLASSRATE() {
		return CLASSRATE;
	}
	
	@XmlElement( name = "CLASSRATE" )
	public void setCLASSRATE(String cLASSRATE) {
		CLASSRATE = cLASSRATE;
	}
	public String getGSTCLASS() {
		return GSTCLASS;
	}
	
	@XmlElement( name = "GSTCLASS" )
	public void setGSTCLASS(String gSTCLASS) {
		GSTCLASS = gSTCLASS;
	}
	public String getISDEEMEDPOSITIVE() {
		return ISDEEMEDPOSITIVE;
	}
	
	@XmlElement( name = "ISDEEMEDPOSITIVE" )
	public void setISDEEMEDPOSITIVE(String iSDEEMEDPOSITIVE) {
		ISDEEMEDPOSITIVE = iSDEEMEDPOSITIVE;
	}
	public String getLEDGERFROMITEM() {
		return LEDGERFROMITEM;
	}
	
	@XmlElement( name = "LEDGERFROMITEM" )
	public void setLEDGERFROMITEM(String lEDGERFROMITEM) {
		LEDGERFROMITEM = lEDGERFROMITEM;
	}
	public String getREMOVEZEROENTRIES() {
		return REMOVEZEROENTRIES;
	}
	
	
	@XmlElement( name = "REMOVEZEROENTRIES" )
	public void setREMOVEZEROENTRIES(String rEMOVEZEROENTRIES) {
		REMOVEZEROENTRIES = rEMOVEZEROENTRIES;
	}
	public String getISPARTYLEDGER() {
		return ISPARTYLEDGER;
	}
	
	@XmlElement( name = "ISPARTYLEDGER" )
	public void setISPARTYLEDGER(String iSPARTYLEDGER) {
		ISPARTYLEDGER = iSPARTYLEDGER;
	}
	public String getISLASTDEEMEDPOSITIVE() {
		return ISLASTDEEMEDPOSITIVE;
	}
	
	@XmlElement( name = "ISLASTDEEMEDPOSITIVE" )
	public void setISLASTDEEMEDPOSITIVE(String iSLASTDEEMEDPOSITIVE) {
		ISLASTDEEMEDPOSITIVE = iSLASTDEEMEDPOSITIVE;
	}
	public String getRATEOFCESSONVAT() {
		return RATEOFCESSONVAT;
	}
	
	@XmlElement( name = "RATEOFCESSONVAT" )
	public void setRATEOFCESSONVAT(String rATEOFCESSONVAT) {
		RATEOFCESSONVAT = rATEOFCESSONVAT;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	@XmlElement( name = "AMOUNT" )
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}


	public SALES_BASICRATEOFINVOICETAX getBasicrateofinvoicetax() {
		return basicrateofinvoicetax;
	}


	public void setBasicrateofinvoicetax(SALES_BASICRATEOFINVOICETAX basicrateofinvoicetax) {
		this.basicrateofinvoicetax = basicrateofinvoicetax;
	}
	
	
	
	
}
