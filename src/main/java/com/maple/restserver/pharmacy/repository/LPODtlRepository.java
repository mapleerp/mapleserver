package com.maple.restserver.pharmacy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.his.entity.LPODtl;
import com.maple.restserver.his.entity.LPOMst;
@Repository
public interface LPODtlRepository extends JpaRepository<LPODtl, String>{


	@Query(nativeQuery=true,value="select * from LPODtl d where d.lpo_mst=:lpoMst")
	List<LPODtl> findByLpoMst(LPOMst lpoMst);

	@Query(nativeQuery=true,value="select "
			+ "item_id, qty from LPODtl where lpoMst=:id")

	List<LPODtl> findByLpoMstId(String id);

}
