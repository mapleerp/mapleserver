package com.maple.restserver.pharmacy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemTempMst;
import com.maple.restserver.his.entity.LPODtl;
import com.maple.restserver.his.entity.LPOMst;
@Repository
public interface ItemTempRepository extends JpaRepository<ItemTempMst, String>{

	List<ItemTempMst> findByCompanyMst(CompanyMst companyMst);




}
