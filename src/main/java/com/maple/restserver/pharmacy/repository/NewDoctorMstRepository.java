package com.maple.restserver.pharmacy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.his.entity.NewDoctorMst;
@Repository
public interface NewDoctorMstRepository extends JpaRepository<NewDoctorMst, String>{

	List<NewDoctorMst> findByCompanyMst(CompanyMst companyMst);

	List<NewDoctorMst> findByCompanyMstAndName(CompanyMst companyMst, String doctorname);

}
