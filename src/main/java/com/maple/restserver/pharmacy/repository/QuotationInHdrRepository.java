package com.maple.restserver.pharmacy.repository;



import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.his.entity.QuotationInHdr;




public interface QuotationInHdrRepository extends JpaRepository<QuotationInHdr, String>{
//	date(h.valid_to_date)<=currentDate and


	 @Query(nativeQuery=true,value="select "
	 		+ " i.item_name, "
	 		+ "s.supplier_name, "
	 		+ "h.contact_person, "
	 		+ "h.quotation_number, "
	 		+ "h.validfrom_date, "
	 		+ "h.valid_to_date, "
	 		+ "d.offer_qty, "
	 		+ "d.mrp, "
	 		+ "d.rate_per_unit, "
	 		+ "d.margin "
	 		+ "from item_mst i ,quotation_in_dtl d ,quotation_in_hdr h,"
	 		+ " supplier s where d.item_id=i.id and h.id=d.quotation_in_hdr and "
	 		+ "s.id=h.supplier_id and h.company_mst=:companymstid "

	 		+ "and  d.item_id=:itemId  "
	 		+ " and d.offer_qty<=:qty "
	 		+ " and h.validfrom_date <= :date and valid_to_date >= :date "
	 		+ " order by d.rate_per_unit ASC ")

	
	List<Object> getQuotationReports(String itemId,String companymstid,Double qty,Date date);

}
