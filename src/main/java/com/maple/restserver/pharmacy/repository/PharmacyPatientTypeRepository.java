package com.maple.restserver.pharmacy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.maple.restserver.entity.PatientMst;
import com.maple.restserver.entity.PharmacyPatientType;

@Component
@Repository
public interface PharmacyPatientTypeRepository extends JpaRepository<PharmacyPatientType, String> {

	List<PharmacyPatientType> findByCompanyMstId(String companymstid);




	List<PharmacyPatientType> findByCompanyMstIdAndPatientType(String companymstid, String patienttype);








	



	

	

	



	

}
