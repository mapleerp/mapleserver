package com.maple.restserver.pharmacy.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.his.entity.QuotationInDtl;
import com.maple.restserver.his.entity.QuotationInHdr;


public interface QuotationInDtlRepository extends  JpaRepository<QuotationInDtl, String> {

	
	
	List<QuotationInDtl> findByQuotationInHdr(QuotationInHdr quotationInHdr);
	

	 @Query(nativeQuery=true,value="select  distinct h.supplier_id from quotation_in_hdr h where date(h.valid_to_date)>=:currentDate")
	
	List<String>currentofferprovingsupplier(Date currentDate);
}
