package com.maple.restserver.pharmacy.repository;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.his.entity.LPOMst;

public interface LPOMstRepository extends JpaRepository<LPOMst, String> {
	@Query(nativeQuery=true,value="select * from LPOMst m where date(m.date)=:currentDate and m.company_mst=:companyMst")
	Optional<LPOMst> findByDateAndCompanyMst(java.sql.Date currentDate, CompanyMst companyMst);

	@Query(nativeQuery=true,value="select * from LPOMst m where date(m.date)=:sqlDate "
			+ "and m.company_mst=:companyMst "
			+ "and department=:department")
	LPOMst findByDateAndCompanyMstAndDepartment(java.sql.Date sqlDate, CompanyMst companyMst,
			String department);

}
