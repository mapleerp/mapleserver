package com.maple.restserver.pharmacy.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.his.entity.QuotationInDtl;


@Service
@Transactional
public interface ItemTempService {

	List<ItemMst> getAllTempMstByCompanyMst(CompanyMst companyMst);


}
