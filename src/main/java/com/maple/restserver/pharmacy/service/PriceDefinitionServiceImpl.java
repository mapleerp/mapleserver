package com.maple.restserver.pharmacy.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ItemTempMst;
import com.maple.restserver.entity.PriceDefenitionMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.entity.UnitMst;
import com.maple.restserver.his.entity.PriceDefinitionReport;
import com.maple.restserver.his.entity.QuotationInDtl;
import com.maple.restserver.his.entity.QuotationInHdr;
import com.maple.restserver.pharmacy.repository.ItemTempRepository;
import com.maple.restserver.pharmacy.repository.QuotationInDtlRepository;
import com.maple.restserver.pharmacy.repository.QuotationInHdrRepository;
import com.maple.restserver.repository.ItemMstRepository;
import com.maple.restserver.repository.PriceDefinitionMstRepository;
import com.maple.restserver.repository.PriceDefinitionRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.UnitMstRepository;

@Service
@Transactional
@Component
public class PriceDefinitionServiceImpl implements PriceDefinitionService {

	@Autowired
	ItemMstRepository itemMstRepository;

	@Autowired
	UnitMstRepository unitMstRepository;

	@Autowired
	PriceDefinitionMstRepository priceDefinitionMstRepository;

	@Autowired
	PriceDefinitionRepository priceDefinitionRepository;
	
	//--------------------version 6.12 surya
	@Autowired
	SalesDetailsRepository salesDetailsRepository;
	
	//--------------------version 6.12 surya end


	@Override
	public List<PriceDefinitionReport> getPriceDefinitions(CompanyMst companyMst) {

		ArrayList<PriceDefinitionReport> priceDefReportList = new ArrayList();

		List<PriceDefinition> priceDefList = priceDefinitionRepository.findByCompanyMst(companyMst);

		for (PriceDefinition price : priceDefList) {
			PriceDefinitionReport priceDefinitionReport = new PriceDefinitionReport();
			priceDefinitionReport.setAmount(price.getAmount());
			priceDefinitionReport.setStartDate(price.getStartDate());

			
			Optional<ItemMst> item = itemMstRepository.findById(price.getItemId());
			if (null != item) {
				ItemMst itemMst = item.get();
				if (null != itemMst) {
					priceDefinitionReport.setItemName(itemMst.getItemName());
				}
			}

			
			Optional<PriceDefenitionMst> priceMstOpt = priceDefinitionMstRepository.findById(price.getPriceId());
			if (null != priceMstOpt) {
				PriceDefenitionMst priceDefenitionMst = priceMstOpt.get();

				if (null != priceDefenitionMst) {
					priceDefinitionReport.setPriceType(priceDefenitionMst.getPriceLevelName());
				}
			}

			
			Optional<UnitMst> unit = unitMstRepository.findById(price.getUnitId());
			if (null != unit) {
				UnitMst unitMst = unit.get();
				if (null != unitMst) {
					priceDefinitionReport.setUnitName(unitMst.getUnitName());
				}
			}

			priceDefReportList.add(priceDefinitionReport);

		}

		return priceDefReportList;
	}

	//----------------------version 6.12 surya 

	@Override
	public List<ItemMst> getAllExceptionItems(CompanyMst companyMst) {

		ArrayList<ItemMst> exceptionList = new ArrayList<ItemMst>();
		
		List<String> itemIds = priceDefinitionRepository.ItemsInCostPrice(companyMst);
		
//		String str = "ITEM000420,ITEM000422,ITEM000425,ITEM000426,ITEM000435";
		
//		List<Object> objList = priceDefinitionRepository.getAllExceptionItems(companyMst, itemIds);
		
		List<ItemMst> itemMstList = itemMstRepository.findByCompanyMst(companyMst);
		

		for (ItemMst item : itemMstList) {
			
			boolean itemException = false;
			
			for(int i=0; i<itemIds.size(); i++)
			{
				if(itemIds.get(i).equalsIgnoreCase(item.getId()))
				{
					itemException = true;
					break;
				}
			}
			
			if(itemException == false)
			{
				exceptionList.add(item);
			}
			
		}
		
		return exceptionList;
	
	
	}

	@Override
	public void UpdateCostPrice(PriceDefinition priceDefinition) {

		
		salesDetailsRepository.updateCostPrice(priceDefinition.getAmount(),priceDefinition.getItemId());
	}

	//----------------------version 6.12 surya end

}
