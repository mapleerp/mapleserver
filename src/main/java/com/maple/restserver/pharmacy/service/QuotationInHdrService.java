package com.maple.restserver.pharmacy.service;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.his.entity.QuotationInHdr;

@Service
@Transactional
public interface QuotationInHdrService {

	QuotationInHdr saveQuotationInHdr(CompanyMst companyMst,String contactpersion,Date validfromDate,Date validTODate);
}
