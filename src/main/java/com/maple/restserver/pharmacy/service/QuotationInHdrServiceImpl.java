package com.maple.restserver.pharmacy.service;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.his.entity.QuotationInHdr;
import com.maple.restserver.pharmacy.repository.QuotationInHdrRepository;
import com.maple.restserver.repository.AccountHeadsRepository;

@Service
@Transactional
@Component
public class QuotationInHdrServiceImpl implements QuotationInHdrService {

	@Autowired
	QuotationInHdrRepository quotationMstRepository;

	@Autowired
	AccountHeadsRepository 	accountHeadsRepository;
	
	

	@Override
	public QuotationInHdr saveQuotationInHdr(CompanyMst companyMst, String contactpersion,
			Date validfromDate, Date validTODate) {
     QuotationInHdr quotHdr=new QuotationInHdr();
		AccountHeads accountHeads=accountHeadsRepository.findByCustomerContact(contactpersion);
		quotHdr.setCompanyMst(companyMst);
		quotHdr.setContactPerson(contactpersion);
		quotHdr.setValidfromDate(validfromDate);
		quotHdr.setValidToDate(validTODate);
		quotHdr.setSupplierId(accountHeads.getId());
		QuotationInHdr quotationInHdr =quotationMstRepository.saveAndFlush(quotHdr);
		return quotationInHdr;
		
	}
	
	
}
