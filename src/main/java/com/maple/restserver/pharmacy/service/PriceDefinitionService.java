package com.maple.restserver.pharmacy.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.PriceDefinition;
import com.maple.restserver.his.entity.PriceDefinitionReport;
import com.maple.restserver.his.entity.QuotationInDtl;


@Service
@Transactional
public interface PriceDefinitionService {

	List<PriceDefinitionReport> getPriceDefinitions(CompanyMst companyMst);

	//----------------------version 6.12 surya 

	List<ItemMst> getAllExceptionItems(CompanyMst companyMst);

	void UpdateCostPrice(PriceDefinition saved);

	//----------------------version 6.12 surya end


}
