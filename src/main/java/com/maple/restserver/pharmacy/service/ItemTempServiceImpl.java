package com.maple.restserver.pharmacy.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ItemTempMst;
import com.maple.restserver.his.entity.QuotationInDtl;
import com.maple.restserver.his.entity.QuotationInHdr;
import com.maple.restserver.pharmacy.repository.ItemTempRepository;
import com.maple.restserver.pharmacy.repository.QuotationInDtlRepository;
import com.maple.restserver.pharmacy.repository.QuotationInHdrRepository;
import com.maple.restserver.repository.ItemMstRepository;


@Service
@Transactional
@Component
public class ItemTempServiceImpl  implements ItemTempService{

	

	@Autowired
	ItemMstRepository itemMstRepository;
	
	@Autowired
	ItemTempRepository itemTempRepository;

	@Override
	public List<ItemMst> getAllTempMstByCompanyMst(CompanyMst companyMst) {
		
		ArrayList<ItemMst> itemMstList =  new ArrayList();
		List<ItemTempMst> itemTempMstList = itemTempRepository.findByCompanyMst(companyMst);
		
		for(ItemTempMst item : itemTempMstList)
		{
			Optional<ItemMst> itemMStOpt = itemMstRepository.findById(item.getItemId());
			ItemMst itemMst = itemMStOpt.get();
			itemMstList.add(itemMst);
		}
		return itemMstList;

	}
	


	

	
	
	
}
