package com.maple.restserver.pharmacy.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.report.entity.QuotationReport;

@Service
@Transactional
public interface QuotationReportService {

	List<QuotationReport> quotationReport(java.sql.Date currentDate,String companymstid,String department);
}
