package com.maple.restserver.pharmacy.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.his.entity.LPODtl;
import com.maple.restserver.his.entity.LPOMst;
import com.maple.restserver.pharmacy.repository.LPODtlRepository;
import com.maple.restserver.pharmacy.repository.LPOMstRepository;
import com.maple.restserver.pharmacy.repository.QuotationInDtlRepository;
import com.maple.restserver.pharmacy.repository.QuotationInHdrRepository;
import com.maple.restserver.report.entity.QuotationReport;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;


@Service
@Transactional
@Component
public class QuotationReportServiceImp implements QuotationReportService {

	@Autowired
	ItemMstRepository itemMstRepository;

	@Autowired
	LPODtlRepository lpoDtlRepository;

	@Autowired
	LPOMstRepository lpoMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepository;
	@Autowired
	QuotationInHdrRepository quotationInHdrRepository;

	@Autowired
	QuotationInDtlRepository quotationInDtlRepository;

	@Override
	public List<QuotationReport> quotationReport(java.sql.Date currentDate, String companymstid,String departmnet) {
		
		Optional<CompanyMst> companyMstOpt = companyMstRepository.findById(companymstid);
		LPOMst lpoMstOpt = lpoMstRepository.findByDateAndCompanyMstAndDepartment(currentDate, companyMstOpt.get(),departmnet);

		if(null == lpoMstOpt)
			return null;
		
		
		List<LPODtl> lpoDtlList = lpoDtlRepository.findByLpoMst(lpoMstOpt);

		List<QuotationReport> quotationReportList = new ArrayList();

		for (LPODtl lopDtl : lpoDtlList) {
			List<Object> obj = quotationInHdrRepository.getQuotationReports(lopDtl.getItemId(), companymstid,
					lopDtl.getQty(), currentDate);

			if (obj.size() > 0) {
				int i = 0;
				{

					Object[] objAray = (Object[]) obj.get(i);
					QuotationReport quotationReport = new QuotationReport();
					quotationReport.setItemName((String) objAray[0]);
					quotationReport.setSupplierName((String) objAray[1]);
					quotationReport.setContactPerson((String) objAray[2]);
					quotationReport.setQuotationNumber((String) objAray[3]);

					quotationReport.setValidFrom((Date) objAray[4]);
					quotationReport.setValidTo((Date) objAray[5]);
					quotationReport.setPurchaseQty((Double) objAray[6]);
					quotationReport.setMrp((Double) objAray[7]);

					quotationReport.setUnitRate((Double) objAray[8]);
					quotationReport.setMargin((Double) objAray[9]);
					quotationReportList.add(quotationReport);

				}
			}

		}

		return quotationReportList;

	}
}
