package com.maple.restserver.pharmacy.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.his.entity.QuotationInDtl;


@Service
@Transactional
public interface QuotationInDtlService {

	
QuotationInDtl	saveQuotationInDtl(Double offerQty,Double mrp,Double ratePerUnit,Double margin,String quotationhdrid,
		CompanyMst	companyMst,String itemName);
}
