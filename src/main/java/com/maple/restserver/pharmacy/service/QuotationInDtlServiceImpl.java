package com.maple.restserver.pharmacy.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.his.entity.QuotationInDtl;
import com.maple.restserver.his.entity.QuotationInHdr;
import com.maple.restserver.pharmacy.repository.QuotationInDtlRepository;
import com.maple.restserver.pharmacy.repository.QuotationInHdrRepository;
import com.maple.restserver.repository.ItemMstRepository;


@Service
@Transactional
@Component
public class QuotationInDtlServiceImpl  implements QuotationInDtlService{

	
	@Autowired
	QuotationInDtlRepository quotationInDtlRepository;
	@Autowired
	ItemMstRepository itemMstRepository;
	
	@Autowired
	QuotationInHdrRepository quotationInHdrRepository;
	@Autowired
	QuotationInDtlService quotationInDtlService;

	@Override
	public QuotationInDtl saveQuotationInDtl(Double offerQty, Double mrp, Double ratePerUnit, Double margin,
			String quotationhdrid, CompanyMst companyMst,String itemName) {
		
		Optional<QuotationInHdr> OptQuotationInHdr=quotationInHdrRepository.findById(quotationhdrid);
		
		Optional<ItemMst> itemMstOpt=itemMstRepository.findByItemNameAndCompanyMst(itemName, companyMst);
		ItemMst itemMst=itemMstOpt.get();
		QuotationInDtl quotDtl=new QuotationInDtl();
		quotDtl.setMargin(margin);
		quotDtl.setItemName(itemMst.getItemName());
		quotDtl.setCompanyMst(companyMst);
		quotDtl.setMrp(mrp);
		quotDtl.setOfferQty(offerQty);
		quotDtl.setRatePerUnit(ratePerUnit);
		quotDtl.setQuotationInHdr(OptQuotationInHdr.get());
		quotDtl.setItemId(itemMst.getId());
		QuotationInDtl quotationInDtl=quotationInDtlRepository.saveAndFlush(quotDtl);
		return quotationInDtl;
	}
	

	
	
	
}
