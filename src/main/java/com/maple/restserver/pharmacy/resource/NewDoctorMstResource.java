package com.maple.restserver.pharmacy.resource;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.his.entity.NewDoctorMst;
import com.maple.restserver.pharmacy.repository.NewDoctorMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
public class NewDoctorMstResource {
	@Autowired
	CompanyMstRepository CompanyMstRepository;
	@Autowired
	NewDoctorMstRepository newDoctorMstRepository;
	
	
	
	
	//save 
		@PostMapping("{companymstid}/newdoctormstresource/savenewdoctormst")
		public NewDoctorMst createNewDoctorMst(
				@PathVariable(value="companymstid")	String commpanymstid,
				@Valid @RequestBody NewDoctorMst newDoctorMst)
		{
			CompanyMst companymst=CompanyMstRepository.findById(commpanymstid).get();
			newDoctorMst.setCompanyMst(companymst);
			newDoctorMst=newDoctorMstRepository.save(newDoctorMst);
			return newDoctorMst;
		}
		
		
		// delete
		@DeleteMapping("{companymstid}/newdoctormstresource/deletenewdoctormst/{id}")
		public void DeleteNewDoctorMst(@PathVariable(value = "id") String Id) {
			newDoctorMstRepository.deleteById(Id);
		}
		
		
		//showing all 
		@GetMapping("{companymstid}/newdoctormstresource/showallnewdoctormst")
		public List<NewDoctorMst> showallNewDoctorMst(
				
				@PathVariable(value = "companymstid") String companymstid) {

			Optional<CompanyMst> companyMstOpt = CompanyMstRepository.findById(companymstid);
			
			return newDoctorMstRepository.findByCompanyMst(companyMstOpt.get());
		}
		
		
		@GetMapping("{companymstid}/newdoctormstresource/getdoctormstbyname/{doctorname}")
		public List<NewDoctorMst> showallNewDoctorMstByName(
				@PathVariable(value = "doctorname") String doctorname,
				@PathVariable(value = "companymstid") String companymstid) {

			Optional<CompanyMst> companyMstOpt = CompanyMstRepository.findById(companymstid);
			
			return newDoctorMstRepository.findByCompanyMstAndName(companyMstOpt.get(),doctorname);
		}
		
		
}
