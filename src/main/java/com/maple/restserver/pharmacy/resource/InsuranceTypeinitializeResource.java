package com.maple.restserver.pharmacy.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.service.InsuranceTypeinitializeService;

@RestController
public class InsuranceTypeinitializeResource {
	
	
	@Autowired
	InsuranceTypeinitializeService insuranceTypeinitializeService;
	
	
	@GetMapping("{companymstid}/insurancetypeinitializeresource/getinitializeinsurancecompanymst/{branchcode}")
	public String getinitializeinsurancecompanymst
	   (@PathVariable("companymstid") String companymstid,
				@PathVariable("branchcode") String branchcode)
	
	{
		
		return insuranceTypeinitializeService.initializeInsuranceType(companymstid, branchcode);

	
	}

}
