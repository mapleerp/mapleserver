package com.maple.restserver.pharmacy.resource;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.pharmacy.service.QuotationReportService;
import com.maple.restserver.report.entity.QuotationReport;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class QuotationReportResource {

	@Autowired 
	QuotationReportService quotationReportService;
	
	@GetMapping("{companymstid}/quotationreport/{department}")
	public List<QuotationReport> retrieveQuotationReport(
			@PathVariable (value = "companymstid") String companymstid,
			@PathVariable (value = "department") String department,
			@RequestParam ("date") String sdate
			
			){
	
		java.sql.Date ldate =  SystemSetting.StringToSqlDate(sdate, "yyyy-MM-dd");

		return quotationReportService.quotationReport(ldate, companymstid,department);
	}
}
