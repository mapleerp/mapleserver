package com.maple.restserver.pharmacy.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.his.entity.QuotationInDtl;
import com.maple.restserver.his.entity.QuotationInHdr;
import com.maple.restserver.pharmacy.repository.QuotationInDtlRepository;
import com.maple.restserver.pharmacy.repository.QuotationInHdrRepository;
import com.maple.restserver.pharmacy.service.QuotationInDtlService;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
public class QuotationInDtlResource {

	@Autowired
	QuotationInDtlService quotationInDtlService;
	@Autowired
	QuotationInHdrRepository quotationInHdrRepository;
	
	@Autowired
    CompanyMstRepository companyMstRepository;
	@Autowired
	QuotationInDtlRepository quotationInDtlRepository;
	
	@GetMapping("{companymstid}/quotationindtl/{offerQty}/{mrp}/{ratePerUnit}/{quyotationhdrid}/{itemName}")
	public QuotationInDtl saveQuotationInDtl(@PathVariable(value = "offerQty") String offerQty,
			@PathVariable (value = "companymstid") String companymstid,
			@PathVariable (value = "mrp") String mrp,
			@PathVariable (value = "ratePerUnit") String ratePerUnit,
			@PathVariable (value = "quyotationhdrid") String quyotationhdrid,
			@PathVariable (value = "itemName") String itemName)
	
	

	{
		offerQty = offerQty.replaceAll("%20", " ");
		mrp = mrp.replaceAll("%20", " ");
		ratePerUnit = ratePerUnit.replaceAll("%20", " ");
		quyotationhdrid = quyotationhdrid.replaceAll("%20", " ");
		itemName = itemName.replaceAll("%20", " ");
		
		Double margin = Double.parseDouble(mrp)-Double.parseDouble(ratePerUnit);
		
		Optional<CompanyMst>companyMstOpt=companyMstRepository.findById(companymstid);
		
		return quotationInDtlService.saveQuotationInDtl(Double.parseDouble(offerQty),Double.parseDouble(mrp),Double.parseDouble(ratePerUnit),margin,quyotationhdrid,
				companyMstOpt.get(),itemName);
	}
	
	
	@GetMapping("{companymstid}/quotationindtlsbyhdrid/{quotationinhdrid}")
	public List<QuotationInDtl> getQuotationInDtl(
			@PathVariable (value = "companymstid") String companymstid,
			@PathVariable (value = "quotationinhdrid") String quotationinhdrid)
		
	
	

	{
		Optional<QuotationInHdr>quotationInHdrtOpt=quotationInHdrRepository.findById(quotationinhdrid);
		
		return quotationInDtlRepository.findByQuotationInHdr(quotationInHdrtOpt.get());
	
	
	}
	
}
