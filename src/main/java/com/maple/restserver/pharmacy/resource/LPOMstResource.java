package com.maple.restserver.pharmacy.resource;


import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.his.entity.LPOMst;
import com.maple.restserver.pharmacy.repository.LPOMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
public class LPOMstResource {

	@Autowired
	LPOMstRepository lPOMstRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	  
	@GetMapping("{companymstid}/lpomst/{department}")
		public LPOMst createLPOMst(@PathVariable(value = "companymstid") String companymstid,
				@PathVariable(value = "department") String department,
				@RequestParam("date") String sdate)
		{		 
		
		Date udate = ClientSystemSetting.StringToUtilDate(sdate,"yyyy-MM-dd");
		LocalDate ldate = ClientSystemSetting.utilToLocaDate(udate);
		
		java.sql.Date sqlDate = java.sql.Date.valueOf(ldate);
		 Optional<CompanyMst> companyMstOPt= companyMstRepository.findById(companymstid);
		LPOMst lpoMst = lPOMstRepository.findByDateAndCompanyMstAndDepartment(sqlDate,companyMstOPt.get(),department);
			if(null == lpoMst)
			{
				   lpoMst=new LPOMst();
			}
			
				 lpoMst.setCompanyMst(companyMstOPt.get());
				 lpoMst.setDate(sqlDate);
				 lpoMst.setDepartment(department);
		 	return lPOMstRepository.saveAndFlush(lpoMst);
		}
		
	


		@GetMapping("{companymstid}/updatlopmst/{lpomstid}/{vouchernumber}")
		public LPOMst LPOMstFinalSave(
				@PathVariable(value="lpomstid") String lpomstid, 
				@PathVariable(value = "companymstid") String companymstid ,
				@PathVariable(value = "vouchernumber") String vouchernumber )
		{
			
			Optional<LPOMst> lpoMstOpt = lPOMstRepository.findById(lpomstid);
			LPOMst lpoMst = lpoMstOpt.get();
			
			lpoMst.setVoucherNumber(vouchernumber);
			
			lPOMstRepository.saveAndFlush(lpoMst);
			
			return lpoMst;
			
		
			
		}
		
}
