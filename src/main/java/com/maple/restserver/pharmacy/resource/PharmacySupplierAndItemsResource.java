package com.maple.restserver.pharmacy.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.AccountHeads;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.his.entity.PharmacySupplierAndItem;
import com.maple.restserver.his.entity.PharmacySupplierAndItemReport;
import com.maple.restserver.pharmacy.repository.PharmacySupplierAndItemRepository;
import com.maple.restserver.repository.AccountHeadsRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;



@RestController

public class PharmacySupplierAndItemsResource {
	
	@Autowired
	PharmacySupplierAndItemRepository pharmacySupplierAndItemRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepo;
	
	
	@Autowired
	ItemMstRepository itemMstRepository;
	
	@Autowired
	AccountHeadsRepository accountHeadsRepository;

	
	@GetMapping("{companymstid}/web/savesupplieranditem/{suppliername}/{itemname}")
	public PharmacySupplierAndItemReport createPharmacySupplierAndItem(@PathVariable(value = "companymstid") String
			  companymstid, @PathVariable(value = "suppliername") String suppliername,
			  @PathVariable(value = "itemname") String itemname)
	{
		
		PharmacySupplierAndItem pharmacySupplierAndItem = new PharmacySupplierAndItem();
		
		Optional<CompanyMst> companyOpt = companyMstRepo.findById(companymstid);
		pharmacySupplierAndItem.setCompanyMst(companyOpt.get());
		
		suppliername = suppliername.replaceAll("%20", " ");
		AccountHeads accountHeads = accountHeadsRepository.findByAccountNameAndCompanyMst(suppliername, companyOpt.get());
		pharmacySupplierAndItem.setAccountHeads(accountHeads);
		
		itemname = itemname.replaceAll("%20", " ");
		Optional<ItemMst> itemMstOpt = itemMstRepository.findByItemNameAndCompanyMst(itemname,companyOpt.get());
		pharmacySupplierAndItem.setItemMst(itemMstOpt.get());
		
		
		pharmacySupplierAndItem =pharmacySupplierAndItemRepository.saveAndFlush(pharmacySupplierAndItem);
		
		PharmacySupplierAndItemReport pharmacyitems = new PharmacySupplierAndItemReport(); 
		pharmacyitems.setItemGenericName(pharmacySupplierAndItem.getItemMst().getItemGenericName());
		pharmacyitems.setItemName(pharmacySupplierAndItem.getItemMst().getItemName());
		pharmacyitems.setSupplierName(pharmacySupplierAndItem.getAccountHeads().getAccountName());
		return pharmacyitems;
	}
	
	
	 
	@GetMapping("{companyid}/web/findallpharmacysupplieranditem")
	public List<PharmacySupplierAndItemReport> retrieveAllItem()
	{
		
		List<PharmacySupplierAndItemReport> supplierItemsList = new ArrayList();
		
		List< PharmacySupplierAndItem> pharmacyitemList =  pharmacySupplierAndItemRepository.findAll();
		for(PharmacySupplierAndItem items : pharmacyitemList)
		{
			PharmacySupplierAndItemReport pharmacyitems = new PharmacySupplierAndItemReport(); 
			
			pharmacyitems.setItemName(items.getItemMst().getItemName());
			pharmacyitems.setItemGenericName(items.getItemMst().getItemGenericName());
			pharmacyitems.setSupplierName(items.getAccountHeads().getAccountName());
			supplierItemsList.add(pharmacyitems);
		}
		
		return supplierItemsList;
	}
	

}
