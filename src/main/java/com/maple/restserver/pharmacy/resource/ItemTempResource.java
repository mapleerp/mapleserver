package com.maple.restserver.pharmacy.resource;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.entity.ItemTempMst;
import com.maple.restserver.his.entity.LPODtl;
import com.maple.restserver.his.entity.LPOMst;
import com.maple.restserver.pharmacy.repository.ItemTempRepository;
import com.maple.restserver.pharmacy.repository.LPODtlRepository;
import com.maple.restserver.pharmacy.repository.LPOMstRepository;
import com.maple.restserver.pharmacy.service.ItemTempService;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;


@RestController
public class ItemTempResource {
	
	@Autowired
	ItemMstRepository itemMstRepository;
	
	@Autowired
	ItemTempService itemTempService;
	
	@Autowired
	ItemTempRepository itemTempRepository;

	
	@Autowired
	CompanyMstRepository companyMstRepository;


	@GetMapping("{companymstid}/saveitemtemp/{itemname}")
	public ItemTempMst createItemTemp(@PathVariable(
			value = "companymstid") String companymstid,	
			@PathVariable(value = "itemname") String itemname
			)
	{		
		itemname = itemname.replaceAll("%20", " ");
		 Optional<CompanyMst> companyMstOPt=  companyMstRepository.findById(companymstid);
			 
			Optional<ItemMst> itemOpt = itemMstRepository.findByItemNameAndCompanyMst(itemname, companyMstOPt.get());
			ItemMst itemMst = itemOpt.get();
			if(null == itemMst)
			{
				return null;
			}
			ItemTempMst itrmTempMst = new ItemTempMst();
			itrmTempMst.setCompanyMst(companyMstOPt.get());
			itrmTempMst.setItemId(itemOpt.get().getId());
		
			
	 	return itemTempRepository.saveAndFlush(itrmTempMst);
		
	}

	
	@GetMapping("{companymstid}/getalltempitems")
	public List<ItemMst> getItemTemp(@PathVariable(
			value = "companymstid") String companymstid
			)
	{		
		 Optional<CompanyMst> companyMstOPt=  companyMstRepository.findById(companymstid);
			 
			List<ItemTempMst> itemTempMstList = itemTempRepository.findByCompanyMst(companyMstOPt.get());
			
	
		
			
	 	return itemTempService.getAllTempMstByCompanyMst(companyMstOPt.get());
		
	}
	
	
	
}
