package com.maple.restserver.pharmacy.resource;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.his.entity.QuotationInHdr;
import com.maple.restserver.pharmacy.repository.QuotationInHdrRepository;
import com.maple.restserver.pharmacy.service.QuotationInHdrService;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.utils.SystemSetting;

@RestController
public class QuotationInHdrResource {

	@Autowired
	QuotationInHdrService quotationInHdrService;
	
	@Autowired
	QuotationInHdrRepository quotationInHdrRepository;
	@Autowired
     CompanyMstRepository companyMstRepository;
	
	@GetMapping("{companymstid}/quotationinhdr/{contactpersion}")
	public QuotationInHdr saveQuotationInHdr(@PathVariable(value = "contactpersion") String contactpersion,
			@PathVariable (value = "companymstid") String companymstid,
		
			@RequestParam ("validfromDate") String validfromDate,
			@RequestParam ("validtoDate") String validtoDate)
	
	{
		Optional<CompanyMst>companyMstOpt=companyMstRepository.findById(companymstid);
	
		java.util.Date sdate = SystemSetting.StringToUtilDate(validfromDate,"yyyy-MM-dd");
		java.util.Date edate = SystemSetting.StringToUtilDate(validtoDate,"yyyy-MM-dd");
		
		return quotationInHdrService.saveQuotationInHdr(companyMstOpt.get(),contactpersion,sdate,edate);
	}
	
	

	@GetMapping("{companymstid}/updatequotationinhdr/{quotationinhdrid}/{quotationnumber}")
	public QuotationInHdr quotationInHdrFinalSave(
			@PathVariable String quotationinhdrid, 
			@PathVariable(value = "companymstid") String companymstid ,
			@PathVariable(value = "quotationnumber") String quotationnumber )
	{
		
		Optional<QuotationInHdr> quotationInHdrOpt = quotationInHdrRepository.findById(quotationinhdrid);
		QuotationInHdr quotationInHdr = quotationInHdrOpt.get();
		
		quotationInHdr.setQuotationNumber(quotationnumber);
		
		quotationInHdrRepository.saveAndFlush(quotationInHdr);
		
		return quotationInHdr;
		
		
		
	}
	
}
