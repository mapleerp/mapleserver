package com.maple.restserver.pharmacy.resource;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.ItemMst;
import com.maple.restserver.his.entity.LPODtl;
import com.maple.restserver.his.entity.LPOMst;
import com.maple.restserver.pharmacy.repository.LPODtlRepository;
import com.maple.restserver.pharmacy.repository.LPOMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemMstRepository;


@RestController
public class LPODtlResource {

	@Autowired
	LPOMstRepository lPOMstRepository;
	
	@Autowired
	ItemMstRepository itemMstRepository;
	
	@Autowired
	LPODtlRepository lPODtlRepository;
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	private String itemId;
	private Double qty;
	private String itemName;

	@GetMapping("{companymstid}/lpodtl/{itemname}/{qty}/{lopmstid}")
	public LPODtl createLPODtl(@PathVariable(value = "companymstid") String companymstid,	
			@PathVariable(value = "itemname") String itemname,
			@PathVariable(value = "qty") Double qty,@PathVariable(value = "lopmstid") String  lopmstid)
	{		
		itemname = itemname.replaceAll("%20", " ");
	Optional<LPOMst> lopMstOpt=lPOMstRepository.findById(lopmstid);
		 Optional<CompanyMst> companyMstOPt=  companyMstRepository.findById(companymstid);
			 
			Optional<ItemMst> itemOpt = itemMstRepository.findByItemNameAndCompanyMst(itemname, companyMstOPt.get());
			 LPODtl lopDtl=new LPODtl();
			 lopDtl.setCompanyMst(companyMstOPt.get());
			 lopDtl.setQty(qty);
			 lopDtl.setItemId(itemOpt.get().getId());
			 lopDtl.setLpoMst(lopMstOpt.get());
			 lopDtl.setItemName(itemOpt.get().getItemName());
		
			
	 	return lPODtlRepository.saveAndFlush(lopDtl);
		
	}

	
	@GetMapping("{companymstid}/lpodtlbyhdrid/{lopmstid}")
	public List<LPODtl> findLPODtlByhdrID(@PathVariable(value = "companymstid") String companymstid,	
			@PathVariable(value = "lopmstid") String lopmstid
			)
	{		
		
		Optional<LPOMst> lopMstOpt=lPOMstRepository.findById(lopmstid);	
			
	 	return lPODtlRepository.findByLpoMst(lopMstOpt.get());
		
	}
	
	
}
