package com.maple.restserver.pharmacy.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maple.restserver.entity.PatientMst;
import com.maple.restserver.entity.PharmacyPatientType;
import com.maple.restserver.pharmacy.repository.PharmacyPatientTypeRepository;
import com.maple.restserver.repository.CompanyMstRepository;

@RestController
@Transactional
public class PharmacyPatientTypeResource {
	
	@Autowired
	CompanyMstRepository companyMstRepository;
	
	@Autowired
	PharmacyPatientTypeRepository pharmacyPatientTypeRepository;

	
	@GetMapping("{companymstid}/pharmacypatienttyperresource/getallpatienttype")
	public List<PharmacyPatientType> getallpatienttype(@PathVariable(
			value = "companymstid") String companymstid
			)
	{		
		return pharmacyPatientTypeRepository.findByCompanyMstId(companymstid);
			 
			
		
	}
	@GetMapping("{companymstid}/pharmacypatienttyperresource/getpatienttypebyid/{patienttypeid}")
	public Optional<PharmacyPatientType> getPatientMstById(
			@PathVariable(value = "companymstid")String companymstid,
			@PathVariable(value = "patienttypeid")String patienttypeid)
	{
		return pharmacyPatientTypeRepository.findById(patienttypeid);
	}
	@GetMapping("{companymstid}/pharmacypatienttyperresource/getpharmacypatienttypeidbypatienttype/{patienttype}")
	public List<PharmacyPatientType> getpharmacypatienttypeidbypatienttype(@PathVariable(
			value = "companymstid") String companymstid,@PathVariable(value = "patienttype")String patienttype
			)
	{		
		
		return pharmacyPatientTypeRepository.findByCompanyMstIdAndPatientType(companymstid,patienttype);
			 
			
		
	}
	
	
}
