package com.maple.restserver;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;
import org.xml.sax.SAXException;

import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.mapleconnect.MCScheduleConfig;
import com.maple.restserver.mapleconnect.MapleConnect;
import com.maple.restserver.repository.LmsQueueMstRepository;

/*
 * Comment  @Configuration to disable embeded broker.
 * 
 */
@Configuration
public class ScheduleConfig {

	private static final Logger logger = LoggerFactory.getLogger(RestserverApplication.class);

	@Value("${mycompany}")
	private String mycompany;
	@Value("${mybranch}")
	private String mybranch;

	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	@Autowired
	MapleConnect mapleConnect;

	@Autowired
	RestserverApplication restserverApplication;

	@Autowired
	FileProcessorIncoming mFileReader;

	@Autowired
	MapleTallyConnect mapleTallyConnect;

	@Scheduled(fixedDelay = 6000)
	public void scheduleFixedDelayTask() {

		mFileReader.readAndProcessIncomingFile();

//	resendToKafkaTopic.resendFailedMessage();

		// mFileReader.readAndProcessIncomingFile();
//		mapleConnect.readAndProcessOutGoingFile();

		try {
			mapleTallyConnect.readAndProcessOutGoingFile();
		} catch (Exception e) {
		}

		try {
			mapleTallyConnect.readAndProcessOutGoingTallyXmlFile();
		} catch (Exception e) {

		}

	}

	@Scheduled(fixedDelay = 60000) // every 1 hr
	public void scheduleFixedDelayTask2() {

		mapleConnect.readAndProcessOutGoingFile();

//		mapleConnect.readAndProcessOutGoingProcessedFile();

	}

//	@Scheduled(fixedDelay = 6000)
	public void PerformanceTestAndGc() {
		System.out.println("Memory Usage Checking");

		long kb = 1024000;

		Runtime runtime = Runtime.getRuntime();

		long totalMemory = runtime.totalMemory() / kb;

		long freeMemory = runtime.freeMemory() / kb;

		long memoryUsage = totalMemory - freeMemory;
		System.out.println("Total Memory=" + totalMemory);
		System.out.println("Free Memory=" + freeMemory);
		System.out.println("Memory Usage=" + memoryUsage);

		double usedSpace = Double.longBitsToDouble(memoryUsage);
		double totalSpace = Double.longBitsToDouble(totalMemory);

		double consumption = usedSpace / totalSpace * 100;

		System.out.println("Memory Usage in Percentage=" + consumption + "%");

	}

}
