package com.maple.restserver.mobileService;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maple.restserver.mobileEntity.MobileCartHdr;
import com.maple.restserver.mobileRepository.MobileCartHdrRepository;
import com.maple.restserver.report.entity.MobileOrderReports;

 
@Service
@Transactional
public class MobileCartHdrServiceImpl implements MobileCartHdrService{
	
	@Autowired
	private MobileCartHdrRepository mobileCartHdrRepository;

	@Override
	public List<MobileOrderReports> getActiveCarts(String companymstid) {
		List<MobileOrderReports> mobileCartList = new ArrayList<>();
		
		List<MobileCartHdr> objList = mobileCartHdrRepository.findByStatusAndCompanyMst("ACTIVE",companymstid);
	
		for(MobileCartHdr mobileCartHdr : objList)
		{
			MobileOrderReports mobileOrderReports = new  MobileOrderReports();
			mobileOrderReports.setCustomerName(mobileCartHdr.getMobileCustomerMst().getCustomerName());
			mobileOrderReports.setCustomerPhoneNo(mobileCartHdr.getMobileCustomerMst().getCustomerContact());
			mobileOrderReports.setMobileCartHdrId(mobileCartHdr.getId());
			mobileOrderReports.setStatus(mobileCartHdr.getStatus());
			mobileCartList.add(mobileOrderReports);
		}
	
		return mobileCartList;
	}

}
