package com.maple.restserver.mobileService;

import java.util.List;

import com.maple.restserver.report.entity.MobileOrderReports;

public interface MobileCartHdrService {

	List<MobileOrderReports> getActiveCarts(String companymstid);

}
