package com.maple.restserver;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

//import org.apache.derby.drda.NetworkServerControl;
import org.apache.derby.drda.NetworkServerControl;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import org.camunda.bpm.engine.RuntimeService;
//import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
//import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.accounting.entity.AccountClass;
import com.maple.restserver.analytics.Service.ProcessTransactionService;
import com.maple.restserver.cloud.api.ExternalApi;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CommonReceiveMsgEntity;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.DBMaintMst;
import com.maple.restserver.entity.FinancialYearMst;
import com.maple.restserver.entity.GoodReceiveNoteHdr;
import com.maple.restserver.entity.ItemBatchDtl;
import com.maple.restserver.entity.ItemBatchExpiryDtl;
import com.maple.restserver.entity.JournalHdr;
import com.maple.restserver.entity.LastBestDate;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.entity.MessageObjectMst;
import com.maple.restserver.entity.PaymentHdr;
import com.maple.restserver.entity.PurchaseHdr;
import com.maple.restserver.entity.PurchaseHdrMessageEntity;
import com.maple.restserver.entity.ReceiptHdr;
import com.maple.restserver.entity.SalesMessageEntity;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.entity.StockTransferMessageEntity;
import com.maple.restserver.entity.StockTransferOutHdr;
import com.maple.restserver.entity.SysDateMst;
import com.maple.restserver.entity.TallyRetryMst;
import com.maple.restserver.jms.send.KafkaMapleEvent;

import com.maple.restserver.message.entity.AccountMessage;
import com.maple.restserver.message.entity.FailedMessageMst;

import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.DBMaintMstRepository;
import com.maple.restserver.repository.FailedMessageMstRepository;
import com.maple.restserver.repository.FinancialYearMstRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.repository.SysDateMstRepository;
import com.maple.restserver.repository.TallyRetryMstRepository;
import com.maple.restserver.service.CloudDataCount;
import com.maple.restserver.service.DBMaintMstService;
import com.maple.restserver.service.DataSynchronisationService;
import com.maple.restserver.service.ItemBatchDtlDataSync;
import com.maple.restserver.service.ItemBatchDtlService;
import com.maple.restserver.service.LastBestDateService;
import com.maple.restserver.service.LastBestDateServiceSales;

import com.maple.restserver.service.PurchaseDataSync;
import com.maple.restserver.service.SalesDataSync;
import com.maple.restserver.service.StockTransferDataSync;
import com.maple.restserver.service.TallyRetryMstService;
import com.maple.restserver.service.TallyService;

import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

// Uncomment following to stop Spring Deploy of camunda Process

//@EnableProcessApplication

@EnableScheduling
@SpringBootApplication
@ComponentScan({"com.maple.restserver"})

@EntityScan("com.maple.restserver")
@EnableJpaRepositories("com.maple.restserver")


public class RestserverApplication {

	@Autowired
	MessaageToFileWriter mFileWriter;
	
	@Autowired
	ProcessTransactionService processTransactionService;


	@Autowired
	FailedMessageMstRepository failedMessageMstRepository;

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	ExternalApi externalApi;

	@Autowired
	CloudDataCount cloudDataCount;

	@Autowired
	SalesDataSync salesDataSync;

	@Autowired
	PurchaseDataSync purchaseDataSync;

	@Autowired
	StockTransferDataSync stockTransferDataSync;

	@Autowired
	ItemBatchDtlDataSync itemBatchDtlDataSync;

	// private static final Logger logger =
	// Logger.getLogger(ApplicationStartupRunnerOne.class);
	private static final Logger logger = LoggerFactory.getLogger(RestserverApplication.class);
//	public static ApplicationContext applicationContext;
	public static Connection localCn;

	@Value("${migrationdb}")
	private String migrationdb;

	@Autowired
	FinancialYearMstRepository financialYearMstRepository;

	@Autowired
	SysDateMstRepository sysDateMstRepository;

	@Autowired
	ItemBatchDtlService itemBatchDtlService;
	// ----------------version 5.0 surya

	// ----------------------sharon-------------
	@Autowired
	DBMaintMstService dbMaintMstService;
	@Autowired
	CompanyMstRepository companyMstRepo;
	@Value("${mycompany}")
	private String mycompany;
	@Value("${mybranch}")
	private String mybranch;
	@Value("${outgoing_processed_folder}")
	private String outgoing_processed_folder;
	
	@Value("${incoming_processed_folder}")
	private String incoming_processed_folder;
	
	
	@Value("${outgoing_processed_archived_folder}")
	private String outgoing_processed_archived_folder;
	
	
	@Autowired
	DataSynchronisationService dataSynchronisationService;

	@Autowired
	ApplicationContext context;

	EventBus eventBus = EventBusFactory.getEventBus();

	ArrayList<Map> arrayOfHashMap = new ArrayList();

//	@Autowired
//	private RuntimeService runtimeService;

	public static void main(String[] args) {

		// Logger.getRootLogger().setLevel(Level.DEBUG);

		try {

			String currentDirectory = System.getProperty("user.dir");
			Properties p = System.getProperties();

			p.setProperty("derby.system.home", currentDirectory);
			NetworkServerControl server = new NetworkServerControl(InetAddress.getByName("localhost"), 1529);

			server.start(null);

			logger.info("DB Server Started");
		} catch (Exception e) {
			logger.info("DB Server could not be Started");
		}

		// Uncomment following for Data transfer.

		SpringApplication.run(RestserverApplication.class, args);
		// synchWithServer();

	}

	public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {

		PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
		properties.setLocation(new FileSystemResource("application.properties"));
		properties.setIgnoreResourceNotFound(false);

		return properties;
	}

	@Bean
	public Connection getConnection() {
		try {

			// RestserverApplication.localCn = getDbConn("SOURCEDB");
			RestserverApplication.localCn = getDbConn(migrationdb);
			if (null == RestserverApplication.localCn) {
				RestserverApplication.localCn = getDbConn2(migrationdb);

			}

		} catch (UnknownHostException e) {

			logger.error(e.getMessage());

		} catch (Exception e) {

			logger.error(e.getMessage());
		}
		return RestserverApplication.localCn;
	}

	@EventListener(ApplicationReadyEvent.class)
	public String doSomethingAfterStartup() {
		eventBus.register(this);
 
		
		
		/*
		 * 
		 */
		try {
			ArrayList<DBMaintMst> dbMaintMstList = new ArrayList<DBMaintMst>();
			DBMaintMst dbMaintMst = new DBMaintMst();
			dbMaintMst.setId("SQL01");
			dbMaintMst.setSqlString("alter table sales_trans_hdr modify voucher_type varchar(50)");
			dbMaintMst.setExecStatus("NO");	
			
			dbMaintMstList.add(dbMaintMst);
			
			/*
			 * When we want to execute a query which cannot be done done by hibernate like Data insertion,
			 *  and also the query has to be done only  once , we use this format.
			 *  1. Increment the ID
			 *  2. Set the sql
			 *  3. Add to Array List.
			 *  
			 */

			for (DBMaintMst dbMaintMst1 : dbMaintMstList) {

				dbMaintMstService.insertDBmaintmst(dbMaintMst1.getId(), dbMaintMst1.getSqlString(),
						dbMaintMst1.getExecStatus());
			}

		} catch (Exception e) {

			logger.error(e.getMessage());

		}
		try {
			dbMaintMstService.executeDBmaintmst();

		} catch (Exception e) {

			logger.error(e.getMessage());

		}

		
		
		//********************FILE DELETE******************************//
		
				try {
					//creating a file object for directory
					File dirPathfile1=new File(outgoing_processed_folder);
					File fileList[]=dirPathfile1.listFiles();
					
					for(File file: fileList) {
						if(file.isFile()) {
							file.delete();
						}
					}

				} catch (Exception e) {

					logger.error(e.getMessage());

				

				}
				
				try {
					//creating a file object for directory
					File dirPathfile2=new File(outgoing_processed_archived_folder);
					File fileList[]=dirPathfile2.listFiles();
					
					for(File file: fileList) {
						if(file.isFile()) {
							file.delete();
						}
					}

				} catch (Exception e) {

					logger.error(e.getMessage());

				
				}
				try {
					//creating a file object for directory
					File dirPathfile3=new File(incoming_processed_folder);
					File fileList[]=dirPathfile3.listFiles();
					
					for(File file: fileList) {
						if(file.isFile()) {
							file.delete();
						}
					}

				} catch (Exception e) {

					logger.error(e.getMessage());

				

				}
		
		
		
		
		
		
		
		// doDataSynchronization(mycompany, mybranch);

		// doDataSynchronization(mycompany, mybranch);

		String returnstatus;
		try {
			List<SysDateMst> sysDateMstList = sysDateMstRepository.findAll();
			if (sysDateMstList.size() == 0) {
				return "Date not found";
			}
			SysDateMst sysDateMst = sysDateMstList.get(0);

			List<CompanyMst> comapnyMstList = companyMstRepo.findAll();
			CompanyMst companyMst = comapnyMstList.get(0);

//			itemBatchDtlService.updateItemBatchMstStockFromDtl(companyMst, sysDateMst.getAplicationDate());

			returnstatus = "ITEMBATCHDTLDONE";

		} catch (Exception e) {

			logger.error(e.getMessage());

			returnstatus = "ITEMBATCHDTLNODONE";

		}

		try {
			FinancialYearMst financialYear = financialYearMstRepository.getCurrentFinancialYear();
			SystemSetting.setFinancialYear(financialYear);
			ClientSystemSetting.setFinancialYear(financialYear);
			returnstatus = "ITEMBATCHDTLNODONEANDFINDONE";

		} catch (Exception e) {
			System.out.println("Financial Year not Set");
			returnstatus = "ITEMBATCHDTLNODONEANDFINNOTDONE";
		}

		logger.error("hello world, I have just started up2");
		logger.info("hello world, I have just started Info");

		

		returnstatus = "ITEMBATCHDTLNODONEANDFINDONEEVENTREGITER";
		/*
		 * try { Process proc =
		 * Runtime.getRuntime().exec("java -jar mapleclientv2.jar"); } catch
		 * (IOException e) { logger.error(e.getMessage()); }
		 */

		logger.info("ApplicationStartupRunnerOne run method Started !!");

		return returnstatus;
	}

	public static Connection getDbConn(String dbName) throws UnknownHostException, Exception {

		// jdbc:derby:d:/myproject/projectDB;create=true;user=user1;password=psssword

		String driver = "org.apache.derby.jdbc.EmbeddedDriver";
		Connection conn = null;
		// String password = "thisisthepassword913781";

		String password = "male04";

		Class.forName(driver).newInstance();

		try {

			NetworkServerControl server = new NetworkServerControl(InetAddress.getByName("localhost"), 1527);
			server.start(null);
			while (true) {
				Thread.sleep(500);
				try { // server.ping();
					break;
				} catch (Exception e) {
				}
			}

			Properties props = new Properties(); // connection properties

			props.put("user", "maple04");
			props.put("password", password);
			// String dbName = dbNamewithoutDB + "DB"; // the name of the database

			String url = "";

			url = "jdbc:derby:" + dbName + ";create=false";

			conn = DriverManager.getConnection(url, props);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				logger.error(e.getMessage());
			}

			System.out.println("Connected to local db and created database " + dbName);

			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			conn.setAutoCommit(true);

		} catch (SQLException sqle) {
			System.out.println("Error Connecting Client DB");
			return null;
		}

		return conn;

	}

	public static Connection getDbConn2(String dbName) throws UnknownHostException, Exception {

		// jdbc:derby:d:/myproject/projectDB;create=true;user=user1;password=psssword

		String driver = "org.apache.derby.jdbc.EmbeddedDriver";
		Connection conn = null;
		// String password = "thisisthepassword913781";

		String password = "thisisthepassword913781";

		Class.forName(driver).newInstance();

		try {

			NetworkServerControl server = new NetworkServerControl(InetAddress.getByName("localhost"), 1527);
			server.start(null);
			while (true) {
				Thread.sleep(500);
				try { // server.ping();
					break;
				} catch (Exception e) {
				}
			}

			Properties props = new Properties(); // connection properties

			props.put("user", "xposdemo");
			props.put("password", password);
			// String dbName = dbNamewithoutDB + "DB"; // the name of the database

			String url = "";

			url = "jdbc:derby:" + dbName + ";create=false";

			conn = DriverManager.getConnection(url, props);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				logger.error(e.getMessage());
			}

			System.out.println("Connected to local db and created database " + dbName);

			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			conn.setAutoCommit(true);

		} catch (SQLException sqle) {
			System.out.println("Error Connecting Client DB");
			return null;
		}

		return conn;
	}

	public byte[] getbackupEntity() throws IOException {

		DateFormat df = new SimpleDateFormat("ddMMyy");
		Date dateobj = new Date();

		String fileName = df.format(dateobj);
		fileName = "./backup/" + fileName + ".zip";

		byte[] barray = Files.readAllBytes(Paths.get(fileName));

		return barray;
	}

	@Subscribe

	public void kafkaMapleEventSubscriber(KafkaMapleEvent kafkaMapleEvent) throws JsonProcessingException {

	//sendToKafkaTopic.sendKafkaMessageLibraryEvent(kafkaMapleEvent, kafkaMapleEvent.getSource(),
		//	kafkaMapleEvent.getDestination(), objectMapper.writeValueAsString(kafkaMapleEvent));

		int hashCodePayLoad = kafkaMapleEvent.getPayLoad().hashCode();
		
		kafkaMapleEvent.setHashCode(hashCodePayLoad);
		mFileWriter.writeToFile( kafkaMapleEvent);
		mFileWriter.writeToFileTallyOut( kafkaMapleEvent);
		
		//processTransactionService.processTransactionFromKafkaEvent(kafkaMapleEvent);
	}

}
