
package com.maple.javapos.print;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.KitchenOrder;

/**
 *
 * @author All Open source developers
 * @version 1.0.0.0
 * @since 2014/12/22
 */
/*
 * This Printsupport java class was implemented to get printout. This class was
 * specially designed to print a Jtable content to a paper. Specially this class
 * formated to print 7cm width paper. Generally for pos thermel printer. Free to
 * customize this source code as you want. Illustration of basic invoice is in
 * this code. demo by gayan liyanaarachchi
 * 
 */

public class ImperialKitchenPrint {
	public final static Logger log = LoggerFactory.getLogger(PosTaxLayoutPrint.class);
	static JTable itemsTable;
	public static int total_item_count = 0;
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss a";
	public static String title[] = new String[] { "Srl", "Item Name", "Qty", "Table" };
	static String TableName;
	static String KOTNumber;

	static BufferedImage read;

	public void setupLogo() throws IOException {

		if(null==read) {
		FileSystem fs =  FileSystems.getDefault();
		Path path = fs.getPath(ClientSystemSetting.getLogo_name());
		
		//Path path = FileSystems.getDefault().getPath(ClientSystemSetting.getLogo_name());

		// List<String> lines = Files.readAllLines(path, charset);

		InputStream iStream = Files.newInputStream(path);

		read = ImageIO.read(iStream);
		
		iStream.close();
		fs.close();
		}
		
	}

	public static void setItems(Object[][] printitem) {
		Object data[][] = printitem;
		DefaultTableModel model = new DefaultTableModel();
		// assume jtable has 4 columns.
		model.addColumn(title[0]);
		model.addColumn(title[1]);
		model.addColumn(title[2]);
		model.addColumn(title[3]);

		int rowcount = printitem.length;

		addtomodel(model, data, rowcount);

		itemsTable = new JTable(model);
		itemsTable.setRowSorter(null);
	}

	public static void addtomodel(DefaultTableModel model, Object[][] data, int rowcount) {
		int count = 0;
		while (count < rowcount) {
			model.addRow(data[count]);
			count++;
		}
		if (model.getRowCount() != rowcount)
			addtomodel(model, data, rowcount);

		System.out.println("Check Passed.");
	}

	public static Object[][] getTableData(JTable table) {
		int itemcount = table.getRowCount();
		System.out.println("Item Count:" + itemcount);

		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
		Object[][] tableData = new Object[nRow][nCol];
		if (itemcount == nRow) // check is there any data loss.
		{
			for (int i = 0; i < nRow; i++) {
				for (int j = 0; j < nCol; j++) {
					tableData[i][j] = dtm.getValueAt(i, j); // pass data into object array.
				}
			}
			if (tableData.length != itemcount) { // check for data losses in object array
				getTableData(table); // recursively call method back to collect data
			}
			System.out.println("Data check passed");
		} else {
			// collecting data again because of data loss.
			getTableData(table);
		}
		return tableData; // return object array with data.
	}

	public static PageFormat getPageFormat(PrinterJob pj) {
		PageFormat pf = pj.defaultPage();
		Paper paper = pf.getPaper();

		double middleHeight = total_item_count * 1.0; // dynamic----->change with the row count of jtable
		double headerHeight = 10; // fixed----->but can be mod
		double footerHeight = 500.0; // fixed----->but can be mod

		double width = convert_CM_To_PPI(7); // printer know only point per inch.default value is 72ppi
		double height = convert_CM_To_PPI(headerHeight + middleHeight + footerHeight);
		paper.setSize(width, height);
		paper.setImageableArea(convert_CM_To_PPI(0.001), convert_CM_To_PPI(0.05), width - convert_CM_To_PPI(0.35),
				height - convert_CM_To_PPI(1)); // define boarder size after that print area width is about 180 points

		pf.setOrientation(PageFormat.PORTRAIT); // select orientation portrait or landscape but for this time portrait
		pf.setPaper(paper);

		return pf;
	}

	protected static double convert_CM_To_PPI(double cm) {
		return toPPI(cm * 0.393600787);
	}

	protected static double toPPI(double inch) {
		return inch * 72d;
	}

	public static String now() {
		// get current date and time as a String output
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());

	}

	public static class MyPrintable implements Printable {
		@Override
		public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
			int result = NO_SUCH_PAGE;
			if (pageIndex == 0) {

				Graphics2D g2d = (Graphics2D) graphics;

				double width = pageFormat.getImageableWidth();
				double height = pageFormat.getImageableHeight();
				g2d.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
				Font font = new Font("MONOSPACED", Font.BOLD, 12);
				Font font2 = new Font("Monospaced", Font.PLAIN, 7);
				g2d.setFont(font);

				int x = 1; // print start at 100 on x axies
				int y = 1; // print start at 10 on y axies
				int imagewidth = 150;
				int imageheight = 50;

				g2d.setFont(font2);

				// g2d.drawLine(10, y , 180, y ); // draw line
				// } catch (IOException e) {
				// logger.error(e.getMessage());
				// }
				try {
					/* Draw Header */
					y = 30;

					String branch_name = "";
					String branch_address1 = "";
					String branch_address2 = "";
					String branch_address3 = "";
					String branch_pin = "";
					String branch_email = "";

					String SalesManId = "";
					String Voucher_number = "";
					Timestamp Voucher_time = null;
					Date invoice_date = null;

					font = new Font("Arial", Font.BOLD, 8);
					g2d.setFont(font);

					font = new Font(Font.SERIF, Font.BOLD, 12);
					g2d.setFont(font);
					// g2d.drawString(CompanyNameTitle, 30, y);

					font = new Font("Arial", Font.BOLD, 8);
					g2d.setFont(font);

					g2d.drawString(TableName, 45, y + 30); // shift a line by adding 10 to y value

					y += 10;

					g2d.drawString("KOT Number: " + KOTNumber, 10, y + 20);

					/* Draw Colums */
					g2d.drawLine(1, y + 30, 180, y + 30);
					g2d.drawString(title[0], 1, y + 40);
					g2d.drawString(title[1], 30, y + 40);
					g2d.drawString(title[2], 100, y + 40);
					g2d.drawString(title[3], 130, y + 40);

					g2d.drawLine(1, y + 50, 180, y + 50);

					int cH = 0;
					TableModel mod = itemsTable.getModel();
					int lastLine = 0;
					for (int i = 0; i < mod.getRowCount(); i++) {
						/*
						 * Assume that all parameters are in string data type for this situation All
						 * other premetive data types are accepted.
						 */
						String itemid = mod.getValueAt(i, 0).toString();
						String itemname = mod.getValueAt(i, 1).toString();
						if (itemname.length() > 15) {
							itemname = itemname.substring(0, 15);
						}
						String price = mod.getValueAt(i, 2).toString();
						String quantity = mod.getValueAt(i, 3).toString();
						String amount = mod.getValueAt(i, 4).toString();

						cH = (y + 60) + (10 * i); // shifting drawing line

						font = new Font("Arial", Font.BOLD, 8); // changed font size
						g2d.setFont(font);

						g2d.drawString(itemid, 0, cH);
						g2d.drawString(itemname, 10, cH);
						g2d.drawString(price, 100, cH);
						g2d.drawString(quantity, 130, cH);

						// String amountFmt = formatDecimal(Float.parseFloat(amount));
						g2d.drawString(padLeft(amount, 10), 150, cH);
						lastLine = i;

					}
					cH = (y + 60) + (10 * (lastLine + 2));
					g2d.drawString("", 150, cH);

					/* Footer */

					g2d.drawLine(1, cH, 180, cH);
					cH = cH + 20;

					// end of the reciept
				} catch (Exception r) {
					r.printStackTrace();
				}

				result = PAGE_EXISTS;
			}
			return result;
		}
	}

	public static String padLeft(String s, int n) {
		return String.format("%1$" + n + "s", s);
	}

	public static String formatDecimal(float number) {
		float epsilon = 0.004f; // 4 tenths of a cent
		if (Math.abs(Math.round(number) - number) < epsilon) {
			return String.format("%10.0f", number); // sdb
		} else {
			return String.format("%10.2f", number); // dj_segfault
		}
	}

	public static void PrintInvoiceThermalPrinter(ArrayList groceryList) throws SQLException {

		ImperialPOSPrint ps = new ImperialPOSPrint();
		DefaultTableModel model = null;

		int X1 = 0;
		int Y1 = 0;
		float x2 = 0f;
		float y2 = 0f;

		int i = 0;
		BigDecimal TotalAmount = new BigDecimal("0");

		int jj = 1;

		Iterator iter = groceryList.iterator();

		while (iter.hasNext()) {

			KitchenOrder grocery = new KitchenOrder();
			grocery = (KitchenOrder) iter.next();

			if (i == 0) {
				Object[][] insertRowData = {
						{ jj, grocery.getItemName(), grocery.getQty(), grocery.getServingTableName() } };
				model = new DefaultTableModel(insertRowData, title);
				i = i + 1;

			} else {

				Object[] insertRowData2 = { jj, grocery.getItemName(), grocery.getQty(),
						grocery.getServingTableName() };
				model.insertRow(i, insertRowData2);
				i = i + 1;

			}

			jj = jj + 1;
		}

		JTable table = new JTable(model);

		Object printitem[][] = ps.getTableData(table);
		ImperialKitchenPrint.setItems(printitem);

		/*
		 * This printer should be initialized
			The printer Name is Hardcoded to KOT
		 */
		
		
		PrintService KitcherPrinterService = null;
		 PrintService[] printServices = PrinterJob.lookupPrintServices();

	        // Iterates the print services and print out its name.
	        for (PrintService printService : printServices) {
	            String name = printService.getName();
	            System.out.println("Name = " + name);
	            if(name.equalsIgnoreCase("KOT")) {
	            	KitcherPrinterService = printService;
	            }
	        }
	        
	        
		

		PrinterJob pj = (PrinterJob) KitcherPrinterService.createPrintJob();

		PageFormat pf = pj.defaultPage();
		Paper paper = new Paper();
		double margin = 0.1;
		paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
		pf.setPaper(paper);

		pj.setPrintable(new MyPrintable(), ps.getPageFormat(pj));

		try {
			pj.print();

			if (ClientSystemSetting.hasCashDrawer()) {
				// --------- OPEN CASH DRAWER -------
				byte[] open = { 27, 112, 48, 55, 121 };
				DocPrintJob job = pj.getPrintService().createPrintJob();
				DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
				Doc doc = new SimpleDoc(open, flavor, null);
				PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
				try {
					job.print(doc, aset);
				} catch (PrintException ex) {
					System.out.println(ex.getMessage());
				}
			}

		} catch (PrinterException ex) {
			ex.printStackTrace();
		}

	}

	public static double round(double value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

}
/*
 * ################# THIS IS HOW TO USE THIS CLASS #######################
 * 
 * Printsupport ps=new Printsupport(); Object printitem
 * [][]=ps.getTableData(jTable); ps.setItems(printitem);
 * 
 * PrinterJob pj = PrinterJob.getPrinterJob(); pj.setPrintable(new
 * MyPrintable(),ps.getPageFormat(pj)); try { pj.print();
 * 
 * } catch (PrinterException ex) { ex.printStackTrace(); } ##################
 * JOIN TO SHARE KNOWLADGE ###########################
 * 
 */