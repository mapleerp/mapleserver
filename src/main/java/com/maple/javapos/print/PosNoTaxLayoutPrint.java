package com.maple.javapos.print;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
 
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.ClientSystemSetting;
import com.maple.restserver.entity.BranchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.SalesTransHdr;
import com.maple.restserver.repository.BranchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.SalesDetailsRepository;
import com.maple.restserver.repository.SalesTransHdrRepository;

/*
 * This model is deployed in HotCakes
 * Tax is printed 
 * Logo is placed at top
 * Tabular layout for item details
 */

public class PosNoTaxLayoutPrint extends POSThermalPrintABS {
	public final static Logger log = LoggerFactory.getLogger(PosNoTaxLayoutPrint.class);
	static JTable itemsTable;
	public static int total_item_count = 0;
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss a";
	public static String title[] = new String[] { "Srl", "Item Name", "Price", "Qty", "Amount" };
	static String parentID;
	public static String branchCode ="";
	  static SalesTransHdrRepository salesTransHdrRepo;
	  static SalesDetailsRepository salesdtlRepo;
	  static CompanyMstRepository companyMstRepo;
		static List<CompanyMst> compList =  companyMstRepo.findAll();
		static String companymstid = compList.get(0).getId();
		  
	  static BranchMstRepository brnchMstRepo;
	static BufferedImage read;
	
	public static String invoiceBottomLine = "";

	public void setupLogo() throws IOException {

		
		if(null==read) {
			FileSystem fs =  FileSystems.getDefault();
			Path path = fs.getPath(ClientSystemSetting.getLogo_name());
			
	//	Path path = FileSystems.getDefault().getPath(ClientSystemSetting.getLogo_name());

		 

		InputStream iStream = Files.newInputStream(path);

		read = ImageIO.read(iStream);
		
		iStream.close();
		
		fs.close();
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		PosNoTaxLayoutPrint ps = new PosNoTaxLayoutPrint();

		DefaultTableModel model = new DefaultTableModel(
				new Object[][] { { 1, "text", 1, 1 }, { 2, "text", 3, 3 }, { 3, "m4ore", 5, 5 }, { 4, "strings", 5, 5 },
						{ 5, "other", 7, 7 }, { 6, "values", 6, 6 } },
				new Object[] { "itemid", "desc ", "rate", "Qty", "Amt" });

		JTable table = new JTable(model);

		Object printitem[][] = ps.getTableData(table);
		ps.setItems(printitem);

		PrinterJob pj = PrinterJob.getPrinterJob();
		pj.setPrintable(new MyPrintable(), ps.getPageFormat(pj));
		try {
			pj.print();

		} catch (PrinterException ex) {
			ex.printStackTrace();
		}

	}

	public static void setItems(Object[][] printitem) {
		Object data[][] = printitem;
		DefaultTableModel model = new DefaultTableModel();
		 
		model.addColumn(title[0]);
		model.addColumn(title[1]);
		model.addColumn(title[2]);
		model.addColumn(title[3]);
		model.addColumn(title[4]);

		int rowcount = printitem.length;

		addtomodel(model, data, rowcount);

		itemsTable = new JTable(model);
		itemsTable.setRowSorter(null);
	}

	public static void addtomodel(DefaultTableModel model, Object[][] data, int rowcount) {
		int count = 0;
		while (count < rowcount) {
			model.addRow(data[count]);
			count++;
		}
		if (model.getRowCount() != rowcount)
			addtomodel(model, data, rowcount);

		System.out.println("Check Passed.");
	}

	public Object[][] getTableData(JTable table) {
		int itemcount = table.getRowCount();
		System.out.println("Item Count:" + itemcount);

		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
		Object[][] tableData = new Object[nRow][nCol];
		if (itemcount == nRow) // check is there any data loss.
		{
			for (int i = 0; i < nRow; i++) {
				for (int j = 0; j < nCol; j++) {
					tableData[i][j] = dtm.getValueAt(i, j); // pass data into object array.
				}
			}
			if (tableData.length != itemcount) { // check for data losses in object array
				getTableData(table); // recursively call method back to collect data
			}
			System.out.println("Data check passed");
		} else {
			// collecting data again because of data loss.
			getTableData(table);
		}
		return tableData; // return object array with data.
	}

	public static PageFormat getPageFormat(PrinterJob pj) {
		PageFormat pf = pj.defaultPage();
		Paper paper = pf.getPaper();

		double middleHeight = total_item_count * 1.0; // dynamic----->change with the row count of jtable
		double headerHeight = 5.0; // fixed----->but can be mod
		double footerHeight = 200.0; // fixed----->but can be mod

		double width = convert_CM_To_PPI(7); // printer know only point per inch.default value is 72ppi
		double height = convert_CM_To_PPI(headerHeight + middleHeight + footerHeight);
		paper.setSize(width, height);
		paper.setImageableArea(convert_CM_To_PPI(0.25), convert_CM_To_PPI(0.5), width - convert_CM_To_PPI(0.35),
				height - convert_CM_To_PPI(1)); // define boarder size after that print area width is about 180 points

		pf.setOrientation(PageFormat.PORTRAIT); // select orientation portrait or landscape but for this time portrait
		pf.setPaper(paper);

		return pf;
	}

	protected static double convert_CM_To_PPI(double cm) {
		return toPPI(cm * 0.393600787);
	}

	protected static double toPPI(double inch) {
		return inch * 72d;
	}

	public static String now() {
		// get current date and time as a String output
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());

	}

	public static class MyPrintable implements Printable {
		@Override
		public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
			
			double invoice_amount = 0.0;
			double invoice_discount = 0.0;
		 
			 
			String strInvoiceDate = "";
			String voucher_type = "";
			

			String SalesManId = "";
			String Voucher_number = "";
			Timestamp Voucher_time = null;
			Date invoice_date = null;
			
			
			
			//SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdr(parentID);
			Optional<SalesTransHdr> salesTransHdr = salesTransHdrRepo.findByIdAndCompanyMstId(parentID,companymstid);
			
			invoice_date = salesTransHdr.get().getVoucherDate();

			Voucher_number = salesTransHdr.get().getVoucherNumber();

			invoice_amount = salesTransHdr.get().getInvoiceAmount();

			voucher_type = salesTransHdr.get().getSalesMode();

			invoice_discount = salesTransHdr.get().getInvoiceDiscount();
			
			
			
			int result = NO_SUCH_PAGE;
			if (pageIndex == 0) {
				Graphics2D g2d = (Graphics2D) graphics;

				double width = pageFormat.getImageableWidth();
				double height = pageFormat.getImageableHeight();
				g2d.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
				Font font = new Font("Monospaced", Font.PLAIN, 7);
				g2d.setFont(font);

				/*
				 * Draw Image* assume that printing reciept has logo on top that logo image is
				 * in .gif format .png also support image resolution is width 100px and height
				 * 50px image located in root--->image folder
				 */
				int x = 100; // print start at 100 on x axies
				int y = 10; // print start at 10 on y axies
				int imagewidth = 70;
				int imageheight = 50;

				if (null != read) {
					g2d.drawImage(read, x, y, imagewidth, imageheight, null); // draw image
				}
				g2d.drawLine(10, y + 60, 180, y + 60); // draw line

				try {
					/* Draw Header */
					y = 80;
 

/*
 * Pass ID and get Sales Trans HDR
 * 
 */
				 

					/*
					 * Pass ID and get BranchMst Details
					 */
					 
					Optional<BranchMst>  branchMst=  brnchMstRepo.findByBranchCodeAndCompanyMstId(ClientSystemSetting.getSystemBranch(),companymstid);
							//RestCaller.getBranchDtls(SystemSetting.getSystemBranch());
					 
					font = new Font("Arial", Font.BOLD, 8);
					g2d.setFont(font);

					 
					g2d.drawString(branchMst.get().getBranchName(), 10, y);

					if (null != branchMst.get().getBranchState()) {

						g2d.drawString(branchMst.get().getBranchState(), 50, y + 10); // shift a line by adding 10 to y value

					}

					if (null != branchMst.get().getBranchName()) {

						y += 10;
						g2d.drawString(branchMst.get().getBranchName(), 50, y + 10); // shift a line by adding 10 to y value

					}

					if (null != branchMst.get().getBranchName()) {
						y += 10;

						g2d.drawString(branchMst.get().getBranchName(), 50, y + 10); // shift a line by adding 10 to y value

					}

					if (null != branchMst.get().getBranchGst()) {
						y += 10;
						g2d.drawString( branchMst.get().getBranchGst(), 50, y + 10); // shift a line
																											// by adding
																											// 10 to y
																											// value

					}
					 

					y += 10;

					String InvoiceTimeToPrint = "";

					Date date = new Date(Voucher_time.getTime());

					String s1 = ClientSystemSetting.SqlDateTostring(invoice_date);
					String s2 = ClientSystemSetting.SqlDateTostring(Voucher_time);

					if (!s1.equalsIgnoreCase(s2)) {
						InvoiceTimeToPrint = "Inv.Date" + ClientSystemSetting.SqlDateTostring(invoice_date) + "("
								+ Voucher_time.toLocaleString() + ")";

					} else {
						InvoiceTimeToPrint = Voucher_time.toLocaleString();
					}
					g2d.drawString(InvoiceTimeToPrint, 10, y + 20); // print date
					g2d.drawString("Invoice No : " + Voucher_number, 10, y + 30);

					/* Draw Colums */
					g2d.drawLine(10, y + 40, 180, y + 40);
					g2d.drawString(title[0], 10, y + 50);
					g2d.drawString(title[1], 50, y + 50);
					g2d.drawString(title[2], 100, y + 50);
					g2d.drawString(title[3], 130, y + 50);
					g2d.drawString(title[4], 150, y + 50);
					g2d.drawLine(10, y + 60, 180, y + 60);

					int cH = 0;
					TableModel mod = itemsTable.getModel();
					int lastLine = 0;
					for (int i = 0; i < mod.getRowCount(); i++) {
						/*
						 * Assume that all parameters are in string data type for this situation All
						 * other premetive data types are accepted.
						 */
						String itemid = mod.getValueAt(i, 0).toString();
						String itemname = mod.getValueAt(i, 1).toString();
						if (itemname.length() > 15) {
							itemname = itemname.substring(0, 15);
						}
						String price = mod.getValueAt(i, 2).toString();
						String quantity = mod.getValueAt(i, 3).toString();
						String amount = mod.getValueAt(i, 4).toString();

						cH = (y + 70) + (10 * i); // shifting drawing line

						font = new Font("Arial", Font.BOLD, 8); // changed font size
						g2d.setFont(font);

						g2d.drawString(itemid, 0, cH);
						g2d.drawString(itemname, 10, cH);
						g2d.drawString(price, 100, cH);
						g2d.drawString(quantity, 130, cH);

						// String amountFmt = formatDecimal(Float.parseFloat(amount));
						g2d.drawString(padLeft(amount, 10), 150, cH);
						lastLine = i;

					}
					cH = (y + 70) + (10 * (lastLine + 2));
					g2d.drawString("", 150, cH);

					/* Footer */
//
//					String sqlstr = "select distinct tax_rate  " + "  from xpos_sales_dtl where parent_id  =  '"
//							+ parentID + "'";
//
//					
					
					List<Object> ojjList = salesdtlRepo.getTaxRate(parentID);
							//RestCaller.getAllTaxRate(parentID);
					
					
					//PreparedStatement pst = APlicationWindow.LocalConn.prepareStatement(sqlstr);

					 for(int i=0;i<ojjList.size();i++)
					 {
						 Object[] objAray = (Object[]) ojjList.get(i);
					
					
//					ResultSet rs = pst.executeQuery();
//					while (rs.next()) {
						double vatrate = ((double)objAray[0]);
						/*
						 * if (vatrate > 0) {
						 * 
						 * cH = cH + 10; double ItemTotalTax = getItemTotalTax(vatrate, parentID);
						 * 
						 * double sgst = ItemTotalTax / 2; String Taxstr = String.format("%4.2f", sgst);
						 * 
						 * g2d.drawString(vatrate + "% SGST Tax" + "     " + Taxstr, 10, cH);
						 * 
						 * cH = cH + 10;
						 * 
						 * Taxstr = String.format("%4.2f", sgst); g2d.drawString(vatrate + "% CGST Tax"
						 * + "     " + Taxstr, 10, cH);
						 * 
						 * }
						 */
					}

					cH = cH + 10;
					double ItemTotalTax = getItemTotalTaxSummary(parentID);

					//double sgst = ItemTotalTax / 2;
					//double cgst = ItemTotalTax / 2;
					//String Taxstr = String.format("%4.2f", sgst);

					//g2d.drawString("Total  SGST Tax" + "     " + Taxstr, 10, cH);
					//cH = cH + 10;

					//g2d.drawString("Total  CGST Tax" + "     " + Taxstr, 10, cH);
					//cH = cH + 20;

					double GrandTotalIncludingTax = getGrandTotalIncTax(parentID);

					String strGrandTotalIncludingTax = String.format("%9.00f", GrandTotalIncludingTax);

					font = new Font("Arial", Font.BOLD, 10); // changed font size
					g2d.setFont(font);
					String stg = "Grand Total:  ";
					g2d.drawString(stg + "          " + strGrandTotalIncludingTax + " /-", 10, cH);
					cH = cH + 10;

					g2d.drawLine(10, cH, 180, cH);
					cH = cH + 20;

					font = new Font("Arial", Font.BOLD, 8); // changed font size
					g2d.setFont(font);
					g2d.drawString(invoiceBottomLine, 10, cH);
					// end of the reciept
				} catch (Exception r) {
					r.printStackTrace();
				}

				result = PAGE_EXISTS;
			}
			return result;
		}
	}

	public static String padLeft(String s, int n) {
		return String.format("%1$" + n + "s", s);
	}

	public static String formatDecimal(float number) {
		float epsilon = 0.004f; // 4 tenths of a cent
		if (Math.abs(Math.round(number) - number) < epsilon) {
			return String.format("%10.0f", number); // sdb
		} else {
			return String.format("%10.2f", number); // dj_segfault
		}
	}
//================================================================= Get all tax======================//
	public static double getItemTotalTax(double vatrate, String ParentId) {

		double InvTotal = 0.0;

		try {
			
			
			BigDecimal ojjList =salesdtlRepo.retrieveTaxAmount(ParentId,vatrate);
					//RestCaller.getTaxAmount(vatrate,ParentId);

//			String sqlstr = " select sum( rate * qty * tax_rate/100)  "
//					+ "  from xpos_sales_dtl where parent_id  = ?  and tax_rate = ?";
			//PreparedStatement pst = APlicationWindow.LocalConn.prepareStatement(sqlstr);

			//pst.setString(1, ParentId);
			//pst.setDouble(2, vatrate);
			//ResultSet rs = pst.executeQuery();

			//while (rs.next()) {
				InvTotal = ojjList.doubleValue();
			//}
		} catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}
//=============================================================================================================//
	public static double getItemTotalTaxSummary(String ParentId) {

		double InvTotal = 0.0;

		try {

			/*String sqlstr = "select sum( rate * qty * tax_rate/100)  " + "  from xpos_sales_dtl where parent_id  = ?  ";
			PreparedStatement pst = APlicationWindow.LocalConn.prepareStatement(sqlstr);

			pst.setString(1, ParentId);
			// pst.setDouble(2, vatrate);
			ResultSet rs = pst.executeQuery();

			while (rs.next()) {*/
			BigDecimal ojjList =  salesdtlRepo.retrieveAllTaxAmount(ParentId);
			//	RestCaller.getAllTaxAmount(ParentId);

			InvTotal = ojjList.doubleValue();
			}
		 catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

	public static double getGrandTotalIncTax(String ParentId) {

		double InvTotal = 0.0;

		try {
//
//			String sqlstr = "select sum(( rate * qty * tax_rate/100) + (rate * qty))   "
//					+ "  from xpos_sales_dtl where parent_id  = ?  ";
			 
			BigDecimal ojjList =  salesdtlRepo.retrieveAllTaxAmount(ParentId);
			//	RestCaller.getAllTaxAmount(ParentId);

			InvTotal = ojjList.doubleValue();
			}
		 catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

	public void PrintInvoiceThermalPrinter(String ParentID) throws SQLException {

		
	
		
		DefaultTableModel model = null;

		boolean isIGST = false;
		String CustStateCode = " 32 (KL) ";
		this.parentID = ParentID;

		double insurance_comapny = 0.0;
		double cash_paid_amt = 0.0;
	
		BigDecimal TotalTax = new BigDecimal("0");
		
		double CreditAmount = 0.0;
		double card_paid_amt = 0.0;

		double bank_paid_amt = 0.0;
		double OtherChartges = 0.0;
		String remark1 = "";
		String insurance_cardno = "";
		String remark2 = "";

		String TelNo = "";

		String deleivery_address = "";
		String delivery_vehicle = "";

	
		String deliveryAddrerss = "";
		String logoPath = "";
		int X1 = 0;
		int Y1 = 0;
		float x2 = 0f;
		float y2 = 0f;

		PosNoTaxLayoutPrint ps = new PosNoTaxLayoutPrint();

		
		 
		

		 

		// ConnectionManager connectionManager = new
		// ConnectionManager();
		// connection = connectionManager.getConnection();
		/*PreparedStatement preparedStatement = APlicationWindow.LocalConn.prepareStatement(
				"select im.hsn_code,  dtl.item_code ,dtl.tax_rate, dtl.qty , dtl.rate ,dtl.item_name, "
						+ "( dtl.qty * CAST(dtl.rate + 0.005 AS DECIMAL(15,2)) ) as amount , "
						+ " dtl.batch_code , dtl.expiry_date , dtl.item_code from xpos_sales_dtl dtl, "
						+ "item_mst im  where parent_id  = ?  and " + " dtl.item_id = im.record_id "
						+ " order by dtl.item_serial_no ");*/
		int i = 0;
		BigDecimal TotalAmount = new BigDecimal("0");

		int jj = 1;
		List<Object> ojjList = salesTransHdrRepo.getItemBySalesTransHdr(companymstid, parentID);
		
		//List<Object> ojjList = RestCaller.getSalesTransHdrandItem(parentID);
		
		 for(int k=0;k<ojjList.size();k++)
		 {
			 Object[] objAray = (Object[]) ojjList.get(k);

//		preparedStatement.setString(1, ParentID);
//		 preparedStatement.setDate(2, InvoiceDate);
//		preparedStatement.execute();
//		ResultSet rs = preparedStatement.getResultSet();

		
		// model.removeRow(0);
		//while (rs.next()) {

			String itemCode = ((String)objAray[1]);
			String hsn_code = ((String)objAray[0]);

			if (null == hsn_code) {
				hsn_code = "";
			}
			BigDecimal TaxRate = ((BigDecimal)objAray[2]);

			TaxRate = TaxRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal Qty =((BigDecimal)objAray[3]);
			Qty = Qty.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal Rate = ((BigDecimal)objAray[4]);
			Rate = Rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			String itemName = ((String)objAray[5]);
			BigDecimal Amount =((BigDecimal)objAray[6]);
			// String batch_code = rs.getString("batch_code");

			// String expiry_date = rs.getString("expiry_date");
			//String item_code = rs.getString("item_code");

			BigDecimal AmountIncludingTax = (Qty.multiply(Rate))
					.add(Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100")))));
			AmountIncludingTax = AmountIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal rateIncludingTax = Rate.add(Rate.multiply(TaxRate.divide(new BigDecimal("100"))));
			rateIncludingTax = rateIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal TaxAmount = (Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100")))));
			TaxAmount = TaxAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			TotalTax = TotalTax.add(TaxAmount);

			Amount = Amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			TotalAmount = TotalAmount.add(Amount);
			;

			int Mop = 0;
			String hexMOP = "";

			int j = itemName.length();
			String itemNameLeft = "";
			int lineLength = 20;
			int sublineCount = 0;
			int ii = 0;
			String strToPrint = "";

			BigDecimal sgst_rate = TaxRate.divide(new BigDecimal("2"));

			BigDecimal cgst_rate = TaxRate.divide(new BigDecimal("2"));

			BigDecimal bdSgst_rate = sgst_rate;
			BigDecimal bdCgst_rate = cgst_rate;

			BigDecimal bdIgst_rate = cgst_rate.add(sgst_rate);

			bdSgst_rate = bdSgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			bdCgst_rate = bdCgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			bdIgst_rate = bdIgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			// JTable exampleJTable = new JTable(data, columnNames);

			if (i == 0) {
				Object[][] insertRowData = { { jj, itemName, rateIncludingTax, Qty, AmountIncludingTax } };
				model = new DefaultTableModel(insertRowData, title);
				i = i + 1;

				// Object[][] rowData2=new Object[][] { { "", hsn_code, "@"+TaxRate+"%", "",""
				// },new Object[] { "itemid", "desc ", "rate", "Amt" }};
				// model.addRow(rowData2);

				Object[] insertRowDataHsn = { "", "HSN: " + hsn_code, "@" + TaxRate + "%", "", "" };
				model.insertRow(i, insertRowDataHsn);
				i = i + 1;

			} else {

				Object[] insertRowData2 = { jj, itemName, rateIncludingTax, Qty, AmountIncludingTax };
				model.insertRow(i, insertRowData2);
				i = i + 1;
				Object[] insertRowDataHsn = { "", "HSN: " + hsn_code, "@" + TaxRate + "%", "", "" };
				model.insertRow(i, insertRowDataHsn);
				i = i + 1;
			}
			// model = new DefaultTableModel(insertRowData,title);

			// model.addRow(new Object[]{ i, itemName, AmountIncludingTax, Qty,Amount});

			// Object[][] rowData=new Object[][] { { i, itemName, AmountIncludingTax,
			// Qty,Amount },new Object[] { "itemid", "desc ", "rate", "Amt" }};
			// model.addRow(rowData);
			// Object[][] rowData2=new Object[][] { { "", hsn_code, "@"+TaxRate+"%", "",""
			// },new Object[] { "itemid", "desc ", "rate", "Amt" }};
			// model.addRow(rowData2);
			jj = jj + 1;
		}
		// model.removeRow(1);
		BigDecimal dbsgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);
		BigDecimal dbcgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);

		BigDecimal bdSgstAmount = dbsgstAmount;
		BigDecimal bdCgstAmount = dbcgstAmount;
		bdSgstAmount = bdSgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		bdCgstAmount = bdCgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		JTable table = new JTable(model);

		Object printitem[][] = ps.getTableData(table);
		ps.setItems(printitem);

		PrinterJob pj = PrinterJob.getPrinterJob();
		pj.setPrintable(new MyPrintable(), ps.getPageFormat(pj));
		try {
			pj.print();
			
			
			if(ClientSystemSetting.hasCashDrawer()) {
			//--------- OPEN CASH DRAWER -------
				 byte[] open = {27, 112, 48, 55, 121};
		        DocPrintJob job = pj.getPrintService().createPrintJob();
		        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
		        Doc doc = new SimpleDoc(open,flavor,null);
		        PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
		        try {
		            job.print(doc, aset);
		        } catch (PrintException ex) {
		            System.out.println(ex.getMessage());
		        }
			}

		} catch (PrinterException ex) {
			ex.printStackTrace();
		}

	}

	public static double round(double value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	@Override
	public void PrintPerformaInvoiceThermalPrinter(String ParentID) throws SQLException {
		// TODO Auto-generated method stub
		
	}

}
/*
 * ################# THIS IS HOW TO USE THIS CLASS #######################
 * 
 * Printsupport ps=new Printsupport(); Object printitem
 * [][]=ps.getTableData(jTable); ps.setItems(printitem);
 * 
 * PrinterJob pj = PrinterJob.getPrinterJob(); pj.setPrintable(new
 * MyPrintable(),ps.getPageFormat(pj)); try { pj.print();
 * 
 * } catch (PrinterException ex) { ex.printStackTrace(); } ##################
 * JOIN TO SHARE KNOWLADGE ###########################
 * 
 */