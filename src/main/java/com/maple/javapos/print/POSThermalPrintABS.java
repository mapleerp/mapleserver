package com.maple.javapos.print;

import java.io.IOException;
import java.sql.SQLException;

import org.springframework.stereotype.Component;

@Component
public abstract class POSThermalPrintABS {

	public abstract void PrintInvoiceThermalPrinter(String ParentID) throws SQLException ;
	public abstract void PrintPerformaInvoiceThermalPrinter(String ParentID) throws SQLException ;
	
	
 
	public abstract void setupLogo() throws IOException;
	
}
