package com.maple.javapos.print;

 
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.maple.restserver.utils.SystemSetting;
import com.sun.jna.Library;
import com.sun.jna.Native;


public class TSCBarcode {
	
	 private static final Logger log = LoggerFactory.getLogger(SystemSetting.class);
	 
    public interface TscLibDll extends Library {
    //	public final static Logger log = Logger.getLogger(TscLibDll.class);
    	
    	TscLibDll INSTANCE = (TscLibDll) Native.loadLibrary ("D:\\xposserver\\TSCLIB.dll" , TscLibDll.class);
    	
    	
        int about ();
        int openport (String pirnterName);
        int closeport ();
        int sendcommand (String printerCommand);
        int setup (String width,String height,String speed,String density,String sensor,String vertical,String offset);
        int downloadpcx (String filename,String image_name);
        int barcode (String x,String y,String type,String height,String readable,String rotation,String narrow,String wide,String code);
        int printerfont (String x,String y,String fonttype,String rotation,String xmul,String ymul,String text);
        int clearbuffer ();
        int printlabel (String set, String copy);
        int formfeed ();
        int nobackfeed ();
        int windowsfont (int x, int y, int fontheight, int rotation, int fontstyle, int fontunderline, String szFaceName, String content);
    }

    public   void PrintBarcodeOld(String NumberOfCopies,
    		String ManufactureDate,String ExpiryDate, String ItemName ,
    		 String Barcode, String Mrp , String PrinterNAme ) {
    	
    	 
    	
    	 
     //String NumberOfCopies = "3";
    	//String ManufactureDate = "Mfg Date: 01/08/2017" ; 
    	//String ExpiryDate = "Exp Date: 01/08/2017" ; 
    	//String ItemName = "Biscut 100 Gm";
    	// String Barcode = "1234567890122";
    	 
       
        
        // String Mrp = " Rs 100 /No"; 
    	
    	//String PrinterNAme = "TSC TTP-244 Plus" ;
    	
        
    	
        //TscLibDll.INSTANCE.about();
        TscLibDll.INSTANCE.openport(PrinterNAme);
        
        log.info("TSC Loaded");
        
        
        //TscLibDll.INSTANCE.downloadpcx("C:\\UL.PCX", "UL.PCX");
        TscLibDll.INSTANCE.sendcommand("REM ***** This is a print by JAVA. *****");
        
        TscLibDll.INSTANCE.setup("100", "40", "5", "8", "0", "0", "0");
        TscLibDll.INSTANCE.clearbuffer();
        //TscLibDll.INSTANCE.sendcommand("PUTPCX 550,10,\"UL.PCX\"");
     /*   TscLibDll.INSTANCE.printerfont ("100", "10", "3", "0", "1", "1", "(JAVA) DLL Test!!");*/
        
        TscLibDll.INSTANCE.barcode("340", "50", "128", "50", "1", "0", "2", "2", Barcode);
        
        TscLibDll.INSTANCE.windowsfont(340, 130, 28, 0, 0, 0, "arial", ItemName);
        
      
        
        TscLibDll.INSTANCE.windowsfont(310, 170, 24, 0, 0, 0, "arial", ManufactureDate);
        TscLibDll.INSTANCE.windowsfont(310, 200, 24, 0, 0, 0, "arial", ExpiryDate);
        TscLibDll.INSTANCE.windowsfont(510, 180, 35, 0, 0, 0, "arial", Mrp);
        
      //  TscLibDll.INSTANCE.printerfont("300", "200", "48", "0", "3", "1", "DEG 0");
  /*      TscLibDll.INSTANCE.windowsfont(400, 200, 48, 90, 3, 1, "arial", "DEG 90");
        TscLibDll.INSTANCE.windowsfont(400, 200, 48, 180, 3, 1, "arial", "DEG 180");
        TscLibDll.INSTANCE.windowsfont(400, 200, 48, 270, 3, 1, "arial", "DEG 270");*/
        
        // 2 = Number of Copies
        TscLibDll.INSTANCE.printlabel("1", NumberOfCopies);
        TscLibDll.INSTANCE.closeport();
    }
    

public  static void manualPrint(String NumberOfCopies,
		String ManufactureDate,String ExpiryDate, String ItemName ,
		 String Barcode, String Mrp , String IngLine1 , String IngLine2 , String PrinterNAme ){

	//String NumberOfCopies = "5";
	////String ManufactureDate = "Mfg Date: 01/08/2017" ; 
	//String ExpiryDate = "Exp Date: 01/08/2017" ; 
	//String ItemName = "Biscut 100 Gm";
	// String Barcode = "1234567890122";
	// String IngLine1 = "This line 1 ingredients";
	 //String IngLine2 = "This line 2 ingredients";
	 
   
    
    //String Mrp = " Rs 100 / No"; 
    

    TscLibDll.INSTANCE.openport(PrinterNAme);
   
    TscLibDll.INSTANCE.sendcommand("CLS");
   // TscLibDll.INSTANCE.clearbuffer();
    
    String BARCODE_SIZE = SystemSetting.BARCODE_SIZE;
    
    if(null==BARCODE_SIZE||BARCODE_SIZE.length()<3) {
    	BARCODE_SIZE="SIZE 3,1.5";
    }
    
    
    TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);
   // TscLibDll.INSTANCE.sendcommand("SIZE 4,1");
   // TscLibDll.INSTANCE.sendcommand("GAPDETECT");
    //TscLibDll.INSTANCE.sendcommand("SPEED 5");
    //TscLibDll.INSTANCE.sendcommand("DENSITY 8");
    //TscLibDll.INSTANCE.sendcommand("REFERENCE 0,0");
    //TscLibDll.INSTANCE.sendcommand("CLS");

    
    /*
     *  public static extern int downloadpcx(string filename, string image_name);
     *  
     *   public static extern int printerfont(string x, string y, string fonttype,
                        string rotation, string xmul, string ymul,
                        string text);
                        
               public static extern int printlabel(string set, string copy);
               
               public static extern int sendcommand(string printercommand);


     */
    
    /*
     * public static extern int barcode(string x, string y, string type,
                    string height, string readable, string rotation,
                    string narrow, string wide, string code);
     */
    
    
    String BARCODE_FORMAT = SystemSetting.BARCODE_FORMAT;
    
    
    if(null==BARCODE_FORMAT||BARCODE_FORMAT.length()<3) {
    	BARCODE_SIZE="FOMAT0";
    }
    
    if(BARCODE_SIZE.equalsIgnoreCase("FOMAT0")) {
    
    TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
    TscLibDll.INSTANCE.sendcommand("CLS");
    TscLibDll.INSTANCE.sendcommand("BARCODE 300,10, \"128\",50,3,0,2,2,\"" +Barcode +"\" ");
    
    TscLibDll.INSTANCE.sendcommand("TEXT 300,90, \"2\",0,1,1,\"" +ManufactureDate+"\"");
    TscLibDll.INSTANCE.sendcommand("TEXT 300,120, \"2\",0,1,1,\"" +ExpiryDate+"\"");
    
    TscLibDll.INSTANCE.sendcommand("TEXT 300,150, \"2\",0,1,1,\"" +ItemName+"\"");
   
    
    if(IngLine1.length()>0){
    TscLibDll.INSTANCE.sendcommand("TEXT 300,180, \"2\",0,1,1,\"" +IngLine1+"\"");
    }
    
    if(IngLine2.length()>0){
    
   TscLibDll.INSTANCE.sendcommand("TEXT 300,210, \"2\",0,1,1,\"" +IngLine2+"\"");
   }
  
    TscLibDll.INSTANCE.sendcommand("TEXT 250,10, \"3\",90,1,1,\"" +Mrp+"\"");
    
    
    
    }else if(BARCODE_SIZE.equalsIgnoreCase("FOMAT1")) {
    	

        
        TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
        TscLibDll.INSTANCE.sendcommand("CLS");
        TscLibDll.INSTANCE.sendcommand("BARCODE 100,40, \"128\",50,3,0,2,2,\"" +Barcode +"\" ");
        
        TscLibDll.INSTANCE.sendcommand("TEXT 100,120, \"2\",0,1,1,\"" +ManufactureDate+"\"");
        TscLibDll.INSTANCE.sendcommand("TEXT 100,150, \"2\",0,1,1,\"" +ExpiryDate+"\"");
        
        TscLibDll.INSTANCE.sendcommand("TEXT 100,180, \"2\",0,1,1,\"" +ItemName+"\"");
       
        
        if(IngLine1.length()>0){
        TscLibDll.INSTANCE.sendcommand("TEXT 100,210, \"2\",0,1,1,\"" +IngLine1+"\"");
        }
        
        if(IngLine2.length()>0){
        
       TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" +IngLine2+"\"");
       }
      
        TscLibDll.INSTANCE.sendcommand("TEXT 100,270, \"3\",90,1,1,\"" +Mrp+"\"");
        
        
        
    	
    }
    
    TscLibDll.INSTANCE.sendcommand("PRINT " +NumberOfCopies);
    
      
  /*  TscLibDll.INSTANCE.barcode("340", "50", "128", "50", "1", "0", "2", "2", "123456789");
    
    TscLibDll.INSTANCE.windowsfont(340, 130, 28, 0, 0, 0, "arial", ItemName);
    
  
    
    TscLibDll.INSTANCE.windowsfont(310, 170, 24, 0, 0, 0, "arial", ManufactureDate);
    TscLibDll.INSTANCE.windowsfont(310, 200, 24, 0, 0, 0, "arial", ExpiryDate);
    TscLibDll.INSTANCE.windowsfont(510, 180, 35, 0, 0, 0, "arial", Mrp);
    
  
    //TscLibDll.INSTANCE.printlabel("1", NumberOfCopies);
    TscLibDll.INSTANCE.clearbuffer();
    */
    //TscLibDll.INSTANCE.sendcommand("KILL \"*\"");
    TscLibDll.INSTANCE.closeport();
    
}


public  static void manualPrintLekshmi(String NumberOfCopies,
		String ManufactureDate,String ExpiryDate, String ItemName ,
		 String Barcode, String Mrp , String IngLine1 , String IngLine2 , String PrinterNAme ){

	//String NumberOfCopies = "5";
	////String ManufactureDate = "Mfg Date: 01/08/2017" ; 
	//String ExpiryDate = "Exp Date: 01/08/2017" ; 
	//String ItemName = "Biscut 100 Gm";
	// String Barcode = "1234567890122";
	// String IngLine1 = "This line 1 ingredients";
	 //String IngLine2 = "This line 2 ingredients";
	 
   
    
    //String Mrp = " Rs 100 / No"; 
    

    TscLibDll.INSTANCE.openport(PrinterNAme);
   
    TscLibDll.INSTANCE.sendcommand("CLS");
   // TscLibDll.INSTANCE.clearbuffer();
    
    String BARCODE_SIZE = SystemSetting.BARCODE_SIZE ;
    if(null==BARCODE_SIZE||BARCODE_SIZE.length()<3) {
    	BARCODE_SIZE="SIZE 3,1.5";
    }
    
    
    TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);
   // TscLibDll.INSTANCE.sendcommand("SIZE 4,1");
   // TscLibDll.INSTANCE.sendcommand("GAPDETECT");
    //TscLibDll.INSTANCE.sendcommand("SPEED 5");
    //TscLibDll.INSTANCE.sendcommand("DENSITY 8");
    //TscLibDll.INSTANCE.sendcommand("REFERENCE 0,0");
    //TscLibDll.INSTANCE.sendcommand("CLS");

    
    /*
     *  public static extern int downloadpcx(string filename, string image_name);
     *  
     *   public static extern int printerfont(string x, string y, string fonttype,
                        string rotation, string xmul, string ymul,
                        string text);
                        
               public static extern int printlabel(string set, string copy);
               
               public static extern int sendcommand(string printercommand);


     */
    
    /*
     * public static extern int barcode(string x, string y, string type,
                    string height, string readable, string rotation,
                    string narrow, string wide, string code);
     */
    
    
    String BARCODE_FORMAT =SystemSetting.BARCODE_FORMAT;
    
    
    if(null==BARCODE_FORMAT||BARCODE_FORMAT.length()<3) {
    	BARCODE_SIZE="FOMAT0";
    }
    
    if(BARCODE_SIZE.equalsIgnoreCase("FOMAT0")) {
		    TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
		    TscLibDll.INSTANCE.sendcommand("CLS");
		    TscLibDll.INSTANCE.sendcommand("BARCODE 300,10, \"128\",50,3,0,2,2,\"" +Barcode +"\" ");
		    
		    TscLibDll.INSTANCE.sendcommand("TEXT 300,90, \"2\",0,1,1,\"" +ManufactureDate+"\"");
		    TscLibDll.INSTANCE.sendcommand("TEXT 300,120, \"2\",0,1,1,\"" +ExpiryDate+"\"");
		    
		    TscLibDll.INSTANCE.sendcommand("TEXT 300,150, \"2\",0,1,1,\"" +ItemName+"\"");
		   
		    
		    if(IngLine1.length()>0){
		    TscLibDll.INSTANCE.sendcommand("TEXT 300,180, \"2\",0,1,1,\"" +IngLine1+"\"");
		    }
		    
		    if(IngLine2.length()>0){
		    
		   TscLibDll.INSTANCE.sendcommand("TEXT 300,210, \"2\",0,1,1,\"" +IngLine2+"\"");
		   }
		  
		    TscLibDll.INSTANCE.sendcommand("TEXT 250,10, \"3\",90,1,1,\"" +Mrp+"\"");
    }
    
    if(BARCODE_SIZE.equalsIgnoreCase("LEKSHMI")) {
    
    TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
    TscLibDll.INSTANCE.sendcommand("CLS");
    
    TscLibDll.INSTANCE.sendcommand("TEXT 100,10, \"2\",0,1,1,\" JAWAHAR FOODS , KOTTARAKARA \"");
    TscLibDll.INSTANCE.sendcommand("BARCODE 100,30, \"128\",50,3,0,2,2,\"" +Barcode +"\" ");
    
    TscLibDll.INSTANCE.sendcommand("TEXT 100,120, \"2\",0,1,1,\"" +ManufactureDate+"\"");
    TscLibDll.INSTANCE.sendcommand("TEXT 100,150, \"2\",0,1,1,\"" +ExpiryDate+"\"");
    
    TscLibDll.INSTANCE.sendcommand("TEXT 100,180, \"2\",0,1,1,\"" +ItemName+"\"");
   
    
    if(IngLine1.length()>0){
    TscLibDll.INSTANCE.sendcommand("TEXT 100,210, \"2\",0,1,1,\"" +"Ingredients:"+IngLine1+"\"");
    }
    
    if(IngLine2.length()>0){
    
   TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" +IngLine2+"\"");
   }
  
    TscLibDll.INSTANCE.sendcommand("TEXT 100,270, \"3\",0,1,1,\"" +Mrp+"\"");
    
    
    
    }else if(BARCODE_SIZE.equalsIgnoreCase("FOMAT1")) {
    	

        
        TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
        TscLibDll.INSTANCE.sendcommand("CLS");
        TscLibDll.INSTANCE.sendcommand("BARCODE 100,40, \"128\",50,3,0,2,2,\"" +Barcode +"\" ");
        
        TscLibDll.INSTANCE.sendcommand("TEXT 100,120, \"2\",0,1,1,\"" +ManufactureDate+"\"");
        TscLibDll.INSTANCE.sendcommand("TEXT 100,150, \"2\",0,1,1,\"" +ExpiryDate+"\"");
        
        TscLibDll.INSTANCE.sendcommand("TEXT 100,180, \"2\",0,1,1,\"" +ItemName+"\"");
       
        
        if(IngLine1.length()>0){
        TscLibDll.INSTANCE.sendcommand("TEXT 100,210, \"2\",0,1,1,\"" +IngLine1+"\"");
        }
        
        if(IngLine2.length()>0){
        
       TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" +IngLine2+"\"");
       }
      
        TscLibDll.INSTANCE.sendcommand("TEXT 100,270, \"3\",90,1,1,\"" +Mrp+"\"");
        
        
        
    	
    }
    
    TscLibDll.INSTANCE.sendcommand("PRINT " +NumberOfCopies);
    
      
  /*  TscLibDll.INSTANCE.barcode("340", "50", "128", "50", "1", "0", "2", "2", "123456789");
    
    TscLibDll.INSTANCE.windowsfont(340, 130, 28, 0, 0, 0, "arial", ItemName);
    
  
    
    TscLibDll.INSTANCE.windowsfont(310, 170, 24, 0, 0, 0, "arial", ManufactureDate);
    TscLibDll.INSTANCE.windowsfont(310, 200, 24, 0, 0, 0, "arial", ExpiryDate);
    TscLibDll.INSTANCE.windowsfont(510, 180, 35, 0, 0, 0, "arial", Mrp);
    
  
    //TscLibDll.INSTANCE.printlabel("1", NumberOfCopies);
    TscLibDll.INSTANCE.clearbuffer();
    */
    //TscLibDll.INSTANCE.sendcommand("KILL \"*\"");
    TscLibDll.INSTANCE.closeport();
    
}
public  static void caliberate(String PrinterNAme){

	 
   
    
    String BARCODE_SIZE = SystemSetting.BARCODE_SIZE;
    
    if(null==BARCODE_SIZE||BARCODE_SIZE.length()<3) {
    	BARCODE_SIZE="SIZE 3,1.5";
    }
    
    
    
    //String Mrp = " Rs 100 / No"; 
    

    TscLibDll.INSTANCE.openport(PrinterNAme);
   
    TscLibDll.INSTANCE.sendcommand("CLS");
   // TscLibDll.INSTANCE.clearbuffer();
    TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);
   // TscLibDll.INSTANCE.sendcommand("SIZE 4,1");
   TscLibDll.INSTANCE.sendcommand("GAPDETECT");
    //TscLibDll.INSTANCE.sendcommand("SPEED 5");
    //TscLibDll.INSTANCE.sendcommand("DENSITY 8");
    //TscLibDll.INSTANCE.sendcommand("REFERENCE 0,0");
    //TscLibDll.INSTANCE.sendcommand("CLS");
    
    
    TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
    TscLibDll.INSTANCE.sendcommand("CLS");
    
  /*  TscLibDll.INSTANCE.barcode("340", "50", "128", "50", "1", "0", "2", "2", "123456789");
    
    TscLibDll.INSTANCE.windowsfont(340, 130, 28, 0, 0, 0, "arial", ItemName);
    
  
    
    TscLibDll.INSTANCE.windowsfont(310, 170, 24, 0, 0, 0, "arial", ManufactureDate);
    TscLibDll.INSTANCE.windowsfont(310, 200, 24, 0, 0, 0, "arial", ExpiryDate);
    TscLibDll.INSTANCE.windowsfont(510, 180, 35, 0, 0, 0, "arial", Mrp);
    
  
    //TscLibDll.INSTANCE.printlabel("1", NumberOfCopies);
    TscLibDll.INSTANCE.clearbuffer();
    */
    //TscLibDll.INSTANCE.sendcommand("KILL \"*\"");
    TscLibDll.INSTANCE.closeport();
    
}


}


//TscLibDll INSTANCE = (TscLibDll) Native.loadLibrary ("D:\\TscLibDllTestInJave\\TSCLIB.dll", TscLibDll.class);